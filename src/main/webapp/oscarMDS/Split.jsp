<%--

    Copyright (c) 2008-2012 Indivica Inc.

    This software is made available under the terms of the
    GNU General Public License, Version 2, 1991 (GPLv2).
    License details are available via "indivica.ca/gplv2"
    and "gnu.org/licenses/gpl-2.0.html".

--%>
<%@ page import="oscar.dms.*,java.util.*" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/rewrite-tag.tld" prefix="rewrite"%>
<%@page import="oscar.oscarLab.ca.all.*,oscar.oscarMDS.data.*,oscar.oscarLab.ca.all.util.*"%>
<%@page import="org.oscarehr.common.dao.DocumentDao" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="org.oscarehr.util.MiscUtils" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
      String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	  boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_lab" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_lab");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}
%>

<html>
<head>
<title>PDF Sorter</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/jquery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/jquery/jquery.rotate.1-1.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/jquery/jquery-ui-1.8.4.custom_full.min.js"></script>
<link rel="stylesheet" href="<%= request.getContextPath() %>/share/javascript/jquery/jquery-ui-1.8.4.custom.css" type="text/css" />
<link rel="stylesheet" href="<%= request.getContextPath() %>/share/css/sorter.css" type="text/css" />

</head>
<body>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/sorter.js"></script>

<%
	String documentId = request.getParameter("document");
	String queueID = request.getParameter("queueID");
	String demoName = request.getParameter("demoName");
	DocumentDao docdao = SpringUtils.getBean(DocumentDao.class);
	org.oscarehr.common.model.Document thisDocument = docdao.getDocument(documentId);
	MiscUtils.getLogger().info("Loading Document (document_no " + thisDocument.getDocumentNo() + ") previews for split.");

%>

<script type="text/javascript">	
	
	let documentId = <%=thisDocument.getDocumentNo()%>;

	loadPreviews(0);
	window.onbeforeunload = clearDoc;
	
	function loadPreviews(pageNum){
		jQuery.ajax({ 
			url: "<%= request.getContextPath() + "/dms/ManageDocument.do?"%>method=createTempImagePreview&doc_no=" + documentId +"&pageNum=" + pageNum, 
			Type: 'POST', 
			success: function(data) {
		    	document.getElementById('page_'+(pageNum + 1)).src = 'data:image/png;base64, ' + data;
				if(pageNum < <%= thisDocument.getNumberofpages()%> - 1)	{
					loadPreviews(pageNum + 1);
				} else {
					clearDoc();
				}
			},
			error: function() {
				clearDoc();
				console.log('Document preview for page ' + pageNum + ' could not be loaded');
				alert('Something went wrong, please try again later or contact support');
			}
		});
	}
	
	function loadHiResPreview(pageNum){
		document.getElementById('page_'+pageNum).src = '<%= request.getContextPath() %>/dms/ManageDocument.do?method=viewDocPage&doc_no=' + documentId + '&curPage=' + pageNum;
		document.getElementById('page_'+pageNum).onclick = '';
	}
	
	function clearDoc(){
		//clear the pdf document stored on the server
		jQuery.ajax({
			url: "<%= request.getContextPath() + "/dms/ManageDocument.do?"%>method=removeDocFromSession&doc_no=" + documentId,
			Type: 'POST'
		});
	}

	
	
</script>
<div id="mastercontainer">
<div id="buildercontainer">
	<ul id="builder">
	</ul>
</div>

<div id="pickercontainer">
<ul id="picker">
<%
for (int i = 1; i <= thisDocument.getNumberofpages(); i++) {
%>
	<li><img class="page" id="page_<%=i%>" onclick="loadHiResPreview(<%=i%>)"/></li>
<% }

%>
</ul>
</div>

<div id="pickertoolscontainer">
	<ul id="pickertools">
		<li id="tool_add"><img src="../images/icons/103.png"><span>Add</span></li>
		<li id="tool_remove"><img src="../images/icons/101.png"><span>Remove</span></li>
		<li id="tool_rotate"><img src="../images/icons/114.png"><span>Rotate</span></li>
		<li id="tool_savecontinue"><img src="../images/icons/172.png"><span>Save &amp; Continue</span></li>
		<li id="tool_done"><img src="../images/icons/071.png"><span>Done</span></li>
	</ul>
</div>


</div>

<input type="hidden" id="document_no" value="<%=documentId %>" />
<input type="hidden" id="queueID" value="<%=StringUtils.trimToEmpty(queueID) %>" />
<input type="hidden" id="demoName" value="<%=StringEscapeUtils.escapeJavaScript(demoName)%>"/>
</body>
</html>