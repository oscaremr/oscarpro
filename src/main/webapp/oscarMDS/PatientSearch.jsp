<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ page import="java.util.*, java.sql.*,java.net.*, oscar.oscarDB.DBPreparedHandler, oscar.MyDateFormat, oscar.Misc" %>
<%@ page import="oscar.oscarDemographic.data.DemographicMerged" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.DemographicDao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.Demographic" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.text.ParseException" %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%
      String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	  boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_lab" rights="r" reverse="<%=true%>">
	<%authed=false;%>
	<%response.sendRedirect("../securityError.jsp?type=_lab");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}
%>

<% 

// Apologies for the crap code.  Definitely could do with a major rewrite...

  // String curProvider_no = (String) session.getAttribute("user");
  String curProvider_no = request.getParameter("provider_no");
  String strLimit1="0";
  String strLimit2="10";
  StringBuffer bufChart = null, bufName = null, bufNo = null;
  if(request.getParameter("limit1")!=null) strLimit1 = request.getParameter("limit1");
  if(request.getParameter("limit2")!=null) strLimit2 = request.getParameter("limit2");
  String demographicNo = request.getParameter("demographicNo");
  String labNo = request.getParameter("labNo");
  String labType = request.getParameter("labType");
  final String providerNo = request.getParameter("providerNumber");
%>


<jsp:useBean id="providerBean" class="java.util.Properties"
	scope="session" />
<html>
<head>
	<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/global.js"></script>
<title><bean:message
	key="oscarMDS.segmentDisplay.patientSearch.title" /></title>
<script language="JavaScript">
function setfocus() {
  this.focus();
  document.titlesearch.keyword.focus();
  document.titlesearch.keyword.select();
}
function checkTypeIn() {
  var dob = document.titlesearch.keyword ;
  if(document.titlesearch.search_mode[2].checked) {
    if(dob.value.length==8) {
      dob.value = dob.value.substring(0, 4)+"-"+dob.value.substring(4, 6)+"-"+dob.value.substring(6, 8);
      //alert(dob.value.length);
    }
    if(dob.value.length != 10) {
      alert("DOB format is incorrect");
      return false;
    }
  }
}

</SCRIPT>
</head>
<body onLoad="setfocus()" topmargin="0" leftmargin="0" rightmargin="0">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr bgcolor="#486ebd">
		<th align=CENTER NOWRAP><font face="Helvetica" color="#FFFFFF">PATIENT
		MATCHING</font></th>
	</tr>
</table>

<table border="0" cellpadding="1" cellspacing="0" width="100%"
	bgcolor="#C4D9E7">

<%
	String radio = request.getParameter("search_mode");
	String nameChecked = "";
	if (radio.equals("search_name")) {
		nameChecked = "checked";
	}
	String phoneChecked = "";
	if (radio.equals("search_phone")) {
		phoneChecked = "checked";
	}
	String dobChecked = "";
	if (radio.equals("search_dob")) {
		dobChecked = "checked";
	}
	String addressChecked = "";
	if (radio.equals("search_address")) {
		addressChecked = "checked";
	}
	String hinChecked = "";
	if (radio.equals("search_hin")) {
		hinChecked = "checked";
	}
%>

	<form method="post" name="titlesearch" action="PatientSearch.jsp"
		onSubmit="return checkTypeIn();"><input type="hidden"
		name="labNo" value="<%=request.getParameter("labNo")%>" /> <input
		type="hidden" name="labType"
		value="<%=request.getParameter("labType")%>" /> <%--@ include file="zdemographictitlesearch.htm"--%>
		<input type="hidden" name="providerNo" id="providerNo" value="<%= Encode.forHtmlAttribute(providerNo) %>"/>
		<input type="hidden" name="labNo" id="labNo" value="<%= Encode.forHtmlAttribute(labNo) %>"/>
		<input type="hidden" name="method" id="method" value="">
		<input type="hidden" name="demographicNo" id="demographicNo" value="<%= Encode.forHtmlAttribute(demographicNo) %>">
	<tr valign="top">
		<td rowspan="2" ALIGN="right" valign="middle"><font
			face="Verdana" color="#0000FF"><b><i>Search</i></b></font></td>
		<td width="10%" nowrap><font size="1" face="Verdana"
			color="#0000FF"> <input type="radio" <%= nameChecked %>
			name="search_mode" value="search_name"> <bean:message
			key="oscarMDS.segmentDisplay.patientSearch.formName" /> </font></td>
		<td nowrap><font size="1" face="Verdana" color="#0000FF">
		<input type="radio" <%= phoneChecked %> name="search_mode" value="search_phone"> <bean:message
			key="oscarMDS.segmentDisplay.patientSearch.formPhone" /> </font></td>
		<td nowrap><font size="1" face="Verdana" color="#0000FF">
		<input type="radio" <%= dobChecked %> name="search_mode" value="search_dob"> <bean:message
			key="oscarMDS.segmentDisplay.patientSearch.formDOB" /> </font></td>
		<td valign="middle" rowspan="2" ALIGN="left"><input type="text"
			NAME="keyword" SIZE="17" MAXLENGTH="100"
			value="<%=Encode.forHtmlAttribute(request.getParameter("keyword"))%>">
			<INPUT TYPE="hidden" NAME="orderby" VALUE="last_name">
			<INPUT TYPE="hidden" NAME="dboperation" VALUE="search_titlename">
			<INPUT TYPE="hidden" NAME="limit1" VALUE="0">
			<INPUT TYPE="hidden" NAME="limit2" VALUE="5">
			<input type="hidden" name="displaymode" value="Search ">
			<input type="hidden" value="<%=request.getParameter("providerNumber")%>" name="providerNumber"/>
			<input type="SUBMIT" name="displaymode" value="<bean:message key="oscarMDS.segmentDisplay.patientSearch.btnSearch"/>" size="17">
		</td>
	</tr>
	<tr>
		<td nowrap><font size="1" face="Verdana" color="#0000FF">
		<input type="radio" <%= addressChecked %> name="search_mode" value="search_address">
		<bean:message key="oscarMDS.segmentDisplay.patientSearch.formAddress" />
		</font></td>
		<td nowrap><font size="1" face="Verdana" color="#0000FF">
		<input type="radio" <%= hinChecked %> name="search_mode" value="search_hin"> <bean:message
			key="oscarMDS.segmentDisplay.patientSearch.formHIN" /> </font></td>
		<td>&nbsp;</td>
	</tr>
	</form>
</table>

<table width="95%" border="0">
	<tr>
		<td align="left"><font size="-1"> <i><bean:message
			key="oscarMDS.segmentDisplay.patientSearch.msgResults" /></i> : <%=request.getParameter("keyword")%>
		</font></td>
	</tr>
</table>

<script language="JavaScript">
        var fullname="";
        function addName(lastname, firstname, chartno) {
          fullname=lastname+","+firstname;
          document.addform.action="<%=request.getParameter("originalpage")%>?name="+fullname+"&chart_no="+chartno+"&bFirstDisp=false";  //+"\"" ;
          document.addform.submit(); // 
          //return;
        }
   </SCRIPT>

<CENTER>
<table width="100%" border="1" cellpadding="0" cellspacing="1"
	bgcolor="#ffffff">
	<form method="post" id="addform" name="addform" action="PatientMatch.do">
		<input type="hidden" name="labNo" value="<%=request.getParameter("labNo")%>">
		<input type="hidden" name="labType" value="<%=request.getParameter("labType")%>" />
		<input type="hidden" name="providerNo" value="<%=request.getParameter("providerNumber")%>" />
		<input type="hidden" name="providerNo" id="providerSelectedNo" value="<%=Encode.forHtmlAttribute(providerNo)%>"/>
		<input type="hidden" name="labNo" id="labNo" value="<%=Encode.forHtmlAttribute(labNo)%>"/>
		<input type="hidden" name="method" id="method" value="">
		<input type="hidden" name="demographicNo" id="demographicSelectedNo"
				value="<%= Encode.forHtmlAttribute(demographicNo)%>">
	<tr bgcolor="#339999">
		<TH align="center" width="10%"><b><bean:message
			key="oscarMDS.segmentDisplay.patientSearch.msgPatientId" /></b></TH>
		<TH align="center" width="20%"><b><bean:message
			key="oscarMDS.segmentDisplay.patientSearch.msgLastName" /></b></TH>
		<TH align="center" width="20%"><b><bean:message
			key="oscarMDS.segmentDisplay.patientSearch.msgFirstName" /></b></TH>
		<TH align="center" width="5%"><b><bean:message
			key="oscarMDS.segmentDisplay.patientSearch.msgAge" /></b></TH>
		<TH align="center" width="10%"><b><bean:message
			key="oscarMDS.segmentDisplay.patientSearch.msgRosterStatus" /></b></TH>
		<TH align="center" width="10%"><b><bean:message
			key="oscarMDS.segmentDisplay.patientSearch.msgPatientStatus" /></b></TH>
		<TH align="center" width="5%"><b><bean:message
			key="oscarMDS.segmentDisplay.patientSearch.msgSex" /></B></TH>
		<th align="center" width="10%"><b><bean:message
				key="demographic.demographicsearchresults.btnDOB" /><span class="dateFormat">(<bean:message key="demographic.demographicsearchresults.btnDOBFormat" /></span>)</b></th>
		<TH align="center" width="10%"><b><bean:message
			key="oscarMDS.segmentDisplay.patientSearch.msgDoctor" /></B></TH>
	</tr>

<%
  String keyword = StringUtils.trimToEmpty(request.getParameter("keyword"));

  String orderby="", limit="", limit1="", limit2="";
  if(request.getParameter("orderby")!=null) orderby="order by "+request.getParameter("orderby");
  if(request.getParameter("limit1")!=null) limit1=request.getParameter("limit1");
  if(request.getParameter("limit2")!=null) {
    limit2=request.getParameter("limit2");
    limit="limit "+limit2+" offset "+limit1;
  }

  DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
  List<Demographic> demographics = new ArrayList<Demographic>();

  String searchMode = request.getParameter("search_mode");

  int numToReturn = Integer.parseInt(strLimit2);
  if ("search_name".equals(searchMode)) {

	  demographics = demographicDao.searchDemographicByName(
			  keyword,
			  numToReturn,
			  Integer.parseInt(strLimit1),
			  orderby,
			  null,
			  false
	  );
  } else if ("search_dob".equals(searchMode)) {
    Calendar dob = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    try {
      dob.setTime(sdf.parse(keyword));
      demographics = demographicDao.findByDob(dob, strLimit1, numToReturn);
    } catch (ParseException e) {
      demographics = new ArrayList<Demographic>();
    }
  } else if ("search_address".equals(searchMode)) {
    demographics = demographicDao.searchDemographicByAddressAndNotStatus(
        keyword,
        null,
        numToReturn,
        Integer.parseInt(strLimit1),
        orderby,
        null,
        false
    );
  } else if ("search_phone".equals(searchMode)) {
      demographics = demographicDao.findByPhone(keyword + "%", strLimit1, numToReturn);
  } else if ("search_hin".equals(searchMode)) {
      demographics = demographicDao.findByHin(keyword + "%", strLimit1, numToReturn);
  } else if ("search_chart_no".equals(searchMode)) {
    demographics = demographicDao.getClientsByChartNo(keyword);
  }
 
  boolean bodd=false;
  int nItems=0;

  if(demographics == null) {
    out.println("failed!!!");
  } else {
	  DemographicMerged dmDAO = new DemographicMerged();

	  for (int i = 0; i < demographics.size(); i++) {
    	Demographic demographic = demographics.get(i);
			String demographicProviderNumber = demographic.getProviderNo() == null
							? ""
							: demographic.getProviderNo();

    	String dem_no = demographic.getDemographicNo().toString();
    	String head = dmDAO.getHead(dem_no);

    	if (head != null && !head.equals(dem_no)) {
      	//skip non head records
      	continue;
	  }

      bodd = !bodd; //for the color of rows
      nItems++; //to calculate if it is the end of records

      String age = demographic.getAge();

%>

	<tr bgcolor="<%=bodd?"ivory":"white"%>" align="center">
		<td><input type="submit"
			value="<%=demographic.getDemographicNo()%>"
			onclick="update('<%=request.getParameter("labNo")%>','<%=demographic.getProviderNo()%>',
			'<%=demographic.getDemographicNo()%>');"/>
		</td>
    <td><%= nbsp(Misc.toUpperLowerCase(demographic.getLastName())) %></td>
    <td><%= nbsp(Misc.toUpperLowerCase(demographic.getFirstName())) %></td>
    <td><%= age %></td>
    <td><%= nbsp(demographic.getRosterStatus()) %></td>
    <td><%= nbsp(demographic.getPatientStatus()) %></td>
    <td><%= nbsp(demographic.getSex()) %></td>
    <td><%= nbsp(demographic.getBirthDayAsString()) %></td>
    <td><%= providerBean.getProperty(demographicProviderNumber) == null
						? "&nbsp;"
						: providerBean.getProperty(demographicProviderNumber) %>
		</td>

	</tr>
<%
    }
  }
%>
	</form>
</table>

<%
  int nLastPage = 0,nNextPage = 0;
  nNextPage=Integer.parseInt(strLimit2)+Integer.parseInt(strLimit1);
  nLastPage = Integer.parseInt(strLimit1) - Integer.parseInt(strLimit2);
%>
<script language="JavaScript">
	function update(labNo, providerNo, demographicNo) {
		matchProvider(labNo, providerNo, demographicNo);
		window.opener.updateLabDemoStatus(labNo);
	}
	function matchProvider(labNo, providerNo, demographicNo) {
		document.getElementById("labNo").value = labNo;
		document.getElementById("providerSelectedNo").value = providerNo;
		document.getElementById("demographicSelectedNo").value = demographicNo;
		setMethod('matchProviderWithLab');
	}
function last() {
  document.nextform.action="PatientSearch.jsp?keyword=<%=request.getParameter("keyword")%>&search_mode=<%=request.getParameter("search_mode")%>&displaymode=<%=request.getParameter("displaymode")%>&dboperation=<%=request.getParameter("dboperation")%>&orderby=<%=request.getParameter("orderby")%>&limit1=<%=nLastPage%>&limit2=<%=strLimit2%>" ;
}
function next() {
  document.nextform.action="PatientSearch.jsp?keyword=<%=request.getParameter("keyword")%>&search_mode=<%=request.getParameter("search_mode")%>&displaymode=<%=request.getParameter("displaymode")%>&dboperation=<%=request.getParameter("dboperation")%>&orderby=<%=request.getParameter("orderby")%>&limit1=<%=nNextPage%>&limit2=<%=strLimit2%>" ;
}
	function setMethod(val) {
		if (document.getElementById("method") != undefined && document.getElementById("method") != null) {
			document.getElementById("method").value = val;
		}
	}
</script>

<form method="post" name="nextform"
	action="../demographic/demographiccontrol.jsp"><input
	type="hidden" name="labNo" value="<%=request.getParameter("labNo")%>">
<input type="hidden" name="labType"
	value="<%=request.getParameter("labType")%>" /> <%
  if(nLastPage >= 0) {
%> <input type="submit" name="submit"
	value="<bean:message key="oscarMDS.segmentDisplay.patientSearch.btnLastPage"/>"
	onClick="last()"> <%
  }
  if(nItems == numToReturn) {
%> <input type="submit" name="submit"
	value="<bean:message key="oscarMDS.segmentDisplay.patientSearch.btnNextPage"/>"
	onClick="next()"> <%
}
%>
</form>

<bean:message
	key="oscarMDS.segmentDisplay.patientSearch.msgSearchMessage" /></center>
</body>
</html>
<%!
String nbsp(String s){
   String ret = s;
   if (ret == null || ret.equals("")){
       ret = "&nbsp;";
   }
   return ret;
}
%>
