/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
angular.module("emailServices", [])
	.service("emailService", function ($http, $q, $log) {
		return {
		apiPath:'../ws/rs/email',
		configHeaders: { headers: { "Content-Type": "application/json", "Accept":"application/json" } },
		configHeadersWithCache: { headers: { "Content-Type": "application/json", "Accept":"application/json" }, cache: true },
		
		getEmailConfig: function() {
            var deferred = $q.defer();
            $http.get(this.apiPath + '/getEmailConfig').success(function (data) {
                deferred.resolve(data);
            }).error(function () {
                console.log("error");
                deferred.reject("An error occurred");
            });

            return deferred.promise;
        },

        saveEmailConfig: function(emailConfig) {
			let deferred = $q.defer();
			$http.post(this.apiPath + '/saveEmailConfig', emailConfig).then(function (response) {
   				deferred.resolve(response.data);
  			}, function (response) {
                deferred.reject(response);
   			});

   			return deferred.promise;
        },

		testEmailConfig: function(emailConfig) {
            let deferred = $q.defer();
			$http.post(this.apiPath + '/testEmailConfig', emailConfig).then(function (response) {
     			deferred.resolve(response.data);
 			}, function (response) {
                deferred.reject(response);
   			});

			return deferred.promise;
        }
    };
});
