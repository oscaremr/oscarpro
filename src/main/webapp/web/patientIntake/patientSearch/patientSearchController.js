oscarApp.controller('PatientSearchCtrl', function ($scope, $timeout, $resource, securityService, $http, demographicService, $state, formService, $filter) {
    $scope.patient = {
        firstName: '',
        lastName: '',
        gender: '',
        dob: '',
        hin: ''
    };

    // Sets is authorized to true by default so the denied message doesn't briefly appear
     let isAuthorized = false;
     
    $scope.finished = false;
    $scope.eformUrl = "/";

    $scope.dob = {
        day : "Day",
        month : "Month",
        year : "Year"
    };
    
    $scope.years = [];
    
    for(var i = new Date().getFullYear(); i > 1900 ; i--){
        $scope.years.push(i);
    }
    
    $scope.days = [];
    
    for(var i = 1; i <= 31; i++){
        $scope.days.push(i);
    }
    
    let eformIntakeId = "";
    
    securityService.hasRights({items: [{objectName: '_eform', privilege: 'w'}, {objectName: '_demographic', privilege: 'r'}]}).then(function(result){
        if(result.content != null && result.content.length === 2) {
            $scope.eformWriteAccess = result.content[0];
            $scope.demographicReadAccess = result.content[1];
            $scope.isAuthorized = $scope.eformWriteAccess && $scope.demographicReadAccess;

            if($scope.isAuthorized) {
                formService.getIntakeEformId().then(function(response) {
                    eformIntakeId = response.intakeEformId;
                });
            } else {
                alert("Patient intake was not set up completely. Please contact the clinic for additional support.");
            }
        } else {
            alert('Could not load rights');
        }
    },function(reason){
        alert(reason);
    });

    $scope.matchDemographic = function() {
        $scope.patient.dob = $scope.dob.year + "-" + $scope.dob.month + "-" + $scope.dob.day; 
        let errors = validateInput();
        if (errors.length === 0) {
            let searchPatient = angular.copy($scope.patient);
            searchPatient.dob = $filter('date')($scope.patient.dob, 'yyyy-MM-dd');
            demographicService.matchDemographic(searchPatient).then(function (response) {
                if (response.data.code === "A") {
                    openForm(response.data.demographicNo);
                } else {
                    alert(response.data.message);
                }
            }, function (response) {
                alert(response);
            });
        } else {
            alert(errors.join("\n"));
        }
    };
    
    //This is used to ensure that angular updates based on things changed in efmclosewindow.jsp when the form is submitted
    const observer = new MutationObserver(function () {
        $scope.$apply();
        if($scope.isIframeNotDisplayed()){
            observer.disconnect();
        }
    });
    
    observer.observe(document.getElementById('eformFrame'), {
        attributes:true
    });
    
    $scope.isIframeNotDisplayed = function() {
        return document.getElementById('eformFrame').style.display === 'none';
    };

    function openForm(demographicNo) {
        $scope.eformUrl = '/oscar/eform/efmformadd_data.jsp?fid=' + eformIntakeId + '&demographic_no=' + demographicNo;
        $scope.eformWidth = window.innerWidth;
        $scope.eformHeight = window.innerHeight;
        $scope.finished = true;
    }
    
    function validateInput() {
        let errors = [];
        if ($scope.patient.dob === undefined || $scope.patient.dob.length === 0 || $scope.patient.dob.indexOf("Year") !== -1 || $scope.patient.dob.indexOf("Month") !== -1 || $scope.patient.dob.indexOf("Day") !== -1) {
            errors.push("You must enter a valid date of birth");
        }
        if (!$scope.patient.hin.match(/^\d{10}$/)) {
            errors.push("You must enter a 10 digit HIN without the version code");
        }
        return errors;
    }
});

function pad0(n) {
    if (n.length>1) return n;
    else return "0"+n;
}