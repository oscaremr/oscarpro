<%--

   Copyright (c) 2022 WELL EMR Group Inc.
   This software is made available under the terms of the
   GNU General Public License, Version 2, 1991 (GPLv2).
   License details are available via "gnu.org/licenses/gpl-2.0.html".

--%>

<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$
            = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%= roleName$ %>" objectName="_admin, _admin.encounter"
        rights="w" reverse="true">
    <%authed = false;
      response.sendRedirect("../securityError.jsp?type=_admin,_admin.encounter");%>
</security:oscarSec>
<%
    if (!authed) {
        return;
    }
    String curUser_no = (String) session.getAttribute("user");
%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="oscar.form.dao.SmartEncounterTemplateDao" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="oscar.form.dao.SmartEncounterShortCodeDao" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.PropertyDao" %>
<%@ page import="org.oscarehr.common.model.Property" %>
<%@ page import="oscar.form.dao.SmartEncounterHeaderDao" %>
<%@ page import="oscar.form.dao.SmartEncounterFooterDao" %>
<%@ page import="java.util.Set" %>
<%@ page import="oscar.form.model.SmartEncounterFooter" %>
<%@ page import="oscar.form.model.SmartEncounterTemplatePlaceholder" %>
<%@ page import="oscar.form.model.DatabaseTagService" %>
<%@ page import="oscar.form.model.DatabaseTag" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%

    SmartEncounterFooterDao smartEncounterFooterDao = SpringUtils.getBean(SmartEncounterFooterDao.class);
    List<SmartEncounterFooter> footerList = smartEncounterFooterDao.findAllOrderByName();

    List<SmartEncounterTemplatePlaceholder> templatePlaceholderList
            = Arrays.asList(SmartEncounterTemplatePlaceholder.values());

    PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);

    DatabaseTagService databaseTagService = new DatabaseTagService();
    databaseTagService.addDefaultDatabaseTags();
    databaseTagService.addCustomDatabaseTags();
    Set<DatabaseTag> databaseTagSet = databaseTagService.getDatabaseTagMethodMap().keySet();
%>

<html:html locale="true">
    <head>
        <meta charset="utf-8">
        <title>Custom Seting for Smart Form</title>

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/quill/quill.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/forms/forms-quill-template-editor.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/forms/forms-quill-template-placeholder.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/forms/forms-quill-template-block-placeholder.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/forms/forms-quill-image-placeholder.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/forms/forms-quill-register-inline-styles.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-ui-1.10.2.custom.min.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/eforms/APCache.js"></script>

        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/js/quill/quill.snow.css"/>
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/share/javascript/forms/forms-quill-template-editor.css"/>
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/js/jquery_css/smoothness/jquery-ui-1.7.3.custom.css"/>
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/share/yui/css/fonts-min.css"/>
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/share/yui/css/autocomplete.css"/>
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/share/css/demographicProviderAutocomplete.css"/>
        <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/css/bulma/bulma-trimmed.css"/>
    </head>
    <style>
        .table tbody tr:hover {
            cursor: inherit;
            box-shadow: 10px 10px 24px 0 #CDDADF;
        }
    </style>
    <script type="text/javascript">
        function init() {
            // resize window
            window.resizeTo(1150, 800);
        }

        <%
		List<String> jsFooters = new ArrayList<String>();
		for (SmartEncounterFooter footer : footerList) {
			jsFooters.add("[" + footer.getId() + ", { 'name':'" + Encode.forJavaScriptBlock(footer.getName()) +
					"', 'text':" + footer.getText() + ", 'createDate': " + footer.getCreateDate().getTime() + "}]");
		}
		%>
        let footersMap = new Map([<%= StringUtils.join(jsFooters, ",") %>]);


        <%
		List<String> jsPlaceholder = new ArrayList<String>();
		for (DatabaseTag templatePlaceholder : databaseTagSet) {
			jsPlaceholder.add("['" + templatePlaceholder.getSubjectAndField() + "', " +
					"\"" + templatePlaceholder.toJavascriptCreateMethod() + "\"]");
		}
		%>
        let placeholderMap = new Map([<%= StringUtils.join(jsPlaceholder, ",") %>]);

    </script>
    <body>
    <div class="smart-template-layout-main smart-template-layout">
        <div class="container is-fluid" style="width: 100%">
            <div class="main body-inner-section">
                <div class="columns">
                    <div class="column">
                        <h3 style="display: inline;">Custom setting for smart form</h3>
                        <input type="button" class="button is-green is-small is-pulled-right" style="display: none;"
                               id="backButton" onclick="onClickBack()" value="Back">
                    </div>
                    <div class="column">
                        <html:form styleId="smartEncounterTemplateForm" action="/form/SmartFormHeaderFooterAction">
                        <input type="hidden" name="<csrf:tokenname/>" value="<csrf:tokenvalue/>"/>
                        <input type="hidden" id="method" name="method" value="">
                        <input type="hidden" id="id" name="id" value="">
                        <input type="hidden" id="template" name="template" value="">
                        <input type="hidden" id="htmlPreview" name="htmlPreview" value="">
                        <input type="hidden" id="createDateLong" name="createDateLong" value="">
                        <input type="hidden" id="smartFormId" name="smartFormId"
                               value="<%= request.getParameter("smartFormId") %>">
                        <input type="hidden" id="modeType" name="modeType" value="<%= request.getParameter("mode") %>">

                    </div>

                    <div id="addEdit" style="display: none;">
                        <h4 id="addEditHeader">Edit Template:</h4>
                        <div class="columns">
                            <div class="column">
                                <div class="control">
                                </div>
                            </div>
                        </div>
                        <div class="columns is-side-gap is-multiline center-row-content" style="overflow: visible;">
                            <div class="column is12-md-10">

                                <div class="panel-1 page-content smart-template-inner-panel">
                                    <div id="nameInvalidFeedback" style="display: none; color: red;"></div>
                                    <div class="panel-body" style="height: inherit;">
                                        <div id="quillEditor" style="background-color: white; height: 50%;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="column is12-md-2">
                                <div class="panel-1">
                                    <div class="panel-body" style="width: 200px;">
                                        <input type="text" class="input is-small" style="width: 130px;" id="placeholderFilter"
                                               placeholder="filter placeholders" onkeyup="updatePlaceholderFilter(this)"/>
                                        <input type="button" class="button is-small" onclick="clearPlaceholderFilter()" value="clear" />
                                        <div  role="group" id="placeholderList" style="width: 100%; overflow-y: scroll; height: 700px;">
                                            <input type="button" class="button is-green is-small is-block" onclick="addFillInMarkerAtCaret()"
                                                   value="Add Fill In Marker (&laquo;&raquo;)"/>
                                            <%
                                                  for (DatabaseTag templatePlaceholder : databaseTagSet) {
                                            %>
                                            <input type="button"
                                                   class="button is-green is-small is-block quill-placeholder-<%= templatePlaceholder.getSubject() %>"
                                                   onclick="addTemplatePlaceholderAtCaret('<%= templatePlaceholder.getSubjectAndField() %>')"
                                                   value="<%= templatePlaceholder.getTitle() %>"/>
                                            <% } %>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="columns">
                            <div class="column">
                                <div class="columns" style="margin-top: 34px;">
                                    <div class="column is12-md-2">
                                        <input type="button" class="button is-green is-small is-block" id="addEditSave" value="Save"
                                               onclick="onClickSave()"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </html:form>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript"
            src="<%= request.getContextPath() %>/library/bootstrap/3.0.0/js/bootstrap.min.js"></script>

    <script>
        let quillEditor;
        let saveMode = null;

        const methodElement = document.getElementById('method');
        let addEditElement = document.getElementById('addEdit');
        let addEditHeaderElement = document.getElementById('addEditHeader');
        let addEditSubmitElement = document.getElementById('addEditSave');
        let editModePara =document.getElementById('modeType').value;

        function changeView(view) {
            addEditElement.style.display = 'none';
            document.getElementById(view).style.display = 'block';
        }

        function templateSave() {
            methodElement.value = saveMode;
            setInputElementsToQuillValues();
            document.getElementById('smartEncounterTemplateForm').submit();
        }

        function onClickSave() {
            if (saveMode === 'saveTemplate') {
                templateSave();
            } else if (saveMode === 'saveShortCode') {
                shortCodeSave();
            } else if (saveMode === 'saveHeader') {
                headerSave();
            } else if (saveMode === 'saveFooter') {
                footerSave();
            }

        }

        function setInputElementsToQuillValues() {
            const imagesToSaveArray = quillEditor.getDocumentImages();
            let formElement = document.getElementById('smartEncounterTemplateForm');
            for (let i = 0; i < imagesToSaveArray.length; i++) {
                const imageDataToSave = imagesToSaveArray[i].insert.ImagePlaceholder;
                let fileInput = document.createElement('input');
                fileInput.setAttribute('type', 'hidden');
                fileInput.setAttribute('name', 'images');
                fileInput.setAttribute('value', imageDataToSave.identifier + ';' + imageDataToSave.data);
                formElement.appendChild(fileInput);
            }
            document.getElementById('template').value = JSON.stringify(quillEditor.getContentsWithImagePlaceholders());
        }

        function addTemplatePlaceholderAtCaret(placeholderName) {
            const placeholder = placeholderMap.get(placeholderName);
            if (placeholder) {
                quillEditor.insertPlaceholderAtCaret(eval(placeholder));
            }
        }
        function addFillInMarkerAtCaret() {
            quillEditor.insertTextAtCaret(String.fromCharCode(171) + String.fromCharCode(187));
        }

        function onClickEditHeader() {
            quillEditor = new TemplatePlaceholderQuill('quillEditor', null, [], { noToolBar: true});
            // quillEditor.setContents(header['text'])
            addEditHeaderElement.innerHTML = 'Custom Header';
            saveMode = 'saveHeader';
            changeView('addEdit');
        }

        function onClickEditFooter() {
            quillEditor = new TemplatePlaceholderQuill('quillEditor', null, [], { noToolBar: true});

            addEditHeaderElement.innerHTML = 'Custom Footer';

            saveMode = 'saveFooter';
            changeView('addEdit');
        }

        function headerSave() {
            methodElement.value = saveMode;
            setInputElementsToQuillValues();
            document.getElementById('htmlPreview').value = quillEditor.getHtml();
            window.opener.document.getElementById('popUpheader').value = document.getElementById('template').value;
            window.close();
        }

        function footerSave() {
            methodElement.value = saveMode;
            setInputElementsToQuillValues();
            document.getElementById('htmlPreview').value = quillEditor.getHtml();
            window.opener.document.getElementById('popUpfooter').value = document.getElementById('template').value;
            window.close();
        }

        function updatePlaceholderFilter(filterInputElement) {
            const filter = filterInputElement.value.toUpperCase();
            const list = document.getElementById('placeholderList');
            for (let i = 0; i < list.children.length; i++) {
                let value = list.children[i].value;
                if (value.toUpperCase().includes(filter)) {
                    list.children[i].style.display = 'block';
                } else {
                    list.children[i].style.display = 'none';
                }
            }
        }
        function clearPlaceholderFilter() {
            const element = document.getElementById('placeholderFilter');
            element.value = '';
            updatePlaceholderFilter(element);
        }

        if (editModePara === 'footer') {
            onClickEditFooter();
        } else {
            onClickEditHeader();
        }

    </script>
    </body>
</html:html>
