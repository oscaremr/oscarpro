<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.util.StringUtils" %>
<%@ page import="org.oscarehr.common.dao.AppointmentTypeDao" %>
<%@ page import="org.oscarehr.common.model.AppointmentType" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    AppointmentTypeDao appointmentTypeDao = SpringUtils.getBean(AppointmentTypeDao.class);
    List<AppointmentType> appointmentTypeList = appointmentTypeDao.listAll();
    OscarProperties oscarProps = OscarProperties.getInstance();

    if (request.getParameter("dboperation") != null && !request.getParameter("dboperation").isEmpty() && request.getParameter("dboperation").equals("Save")) {
        for(String key : SystemPreferences.SCHEDULE_PREFERENCE_KEYS) {
            SystemPreferences preference = SystemPreferencesUtils.findPreferenceByName(key);
            String newValue = request.getParameter(key);
            
            if (preference != null) {
                if (!preference.getValue().equals(newValue)) {
                    preference.setUpdateDate(new Date());
                    preference.setValue(newValue);
                    SystemPreferencesUtils.merge(preference);
                }
            } else {
                preference = new SystemPreferences();
                preference.setName(key);
                preference.setUpdateDate(new Date());
                preference.setValue(newValue);
                SystemPreferencesUtils.persist(preference);
            }
        }
    }
%>

<html:html locale="true">
    <head>
        <title>Mandatory Fields - Master File</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
        <script type="text/javascript" language="JavaScript">
            function toggleThirdPartyInputs(){
                var thirdPartyInputsEnabled = document.getElementById('schedule_tp_link_enabled-true');
                var thirdPartyAppointmentTypeSelect = document.getElementById('schedule_tp_link_type-select');
                var thirdPartyAppointmentTypeText = document.getElementById('schedule_tp_link_type-text');
                var thirdPartyDisplay = document.getElementById('schedule_tp_link_display-text');
                if(thirdPartyInputsEnabled != null && thirdPartyInputsEnabled.checked){
                    thirdPartyAppointmentTypeSelect.disabled = false;
                    thirdPartyAppointmentTypeText.disabled = false;
                    thirdPartyDisplay.disabled = false;
                } else {
                    thirdPartyAppointmentTypeSelect.disabled = true;
                    thirdPartyAppointmentTypeText.disabled = true;
                    thirdPartyDisplay.disabled = true;
                }
            }

            function defaultTypeSelect() {
                var thirdPartyInputsEnabled = document.getElementById('schedule_tp_link_enabled-true');
                if(thirdPartyInputsEnabled != null && thirdPartyInputsEnabled.checked) {
                    var thirdPartyAppointmentTypeSelect = document.getElementById('schedule_tp_link_type-select');
                    var thirdPartyAppointmentTypeText = document.getElementById('schedule_tp_link_type-text');
                    if (thirdPartyAppointmentTypeSelect.value === 'Custom') {
                        thirdPartyAppointmentTypeText.readOnly = false;
                        thirdPartyAppointmentTypeText.value = "";
                    } else {
                        thirdPartyAppointmentTypeText.readOnly = true;
                        thirdPartyAppointmentTypeText.value = thirdPartyAppointmentTypeSelect.value;
                    }
                }
            }

            defaultTypeSelect();
        </script>
    </head>

    <%
        List<SystemPreferences> preferences = SystemPreferencesUtils.findPreferencesByNames(SystemPreferences.SCHEDULE_PREFERENCE_KEYS);
        for(SystemPreferences preference : preferences) {
            dataBean.setProperty(preference.getName(), preference.getValue());
        }

        String tpLinkType = dataBean.getProperty("schedule_tp_link_type", "");
    %>

    <body vlink="#0000FF" class="BodyStyle">
    <h4>Manage Schedule Display Settings</h4>
    <form name="displaySettingsForm" method="post" action="scheduleDisplaySettings.jsp">
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
                <tr>
                    <td>Display Appointment Type on Schedules: </td>
                    <td>
                        <input id="schedule_display_type-true" type="radio" value="true" name="schedule_display_type"
                                <%=(dataBean.getProperty("schedule_display_type", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="schedule_display_type-false" type="radio" value="false" name="schedule_display_type"
                                <%=(dataBean.getProperty("schedule_display_type", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>Display custom roster status: </td>
                    <td>
                        <input id="schedule_display_custom_roster_status-true" type="radio" value="true" name="schedule_display_custom_roster_status"
                                <%=(dataBean.getProperty("schedule_display_custom_roster_status", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="schedule_display_custom_roster_status-false" type="radio" value="false" name="schedule_display_custom_roster_status"
                                <%=(dataBean.getProperty("schedule_display_custom_roster_status", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>Display Third Party Link from Appointment Notes: </td>
                    <td>
                        <input id="schedule_tp_link_enabled-true" type="radio" value="true" onclick="toggleThirdPartyInputs()" name="schedule_tp_link_enabled"
                                <%=(dataBean.getProperty("schedule_tp_link_enabled", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="schedule_tp_link_enabled-false" type="radio" value="false" onclick="toggleThirdPartyInputs()" name="schedule_tp_link_enabled"
                                <%=(dataBean.getProperty("schedule_tp_link_enabled", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>Appointment Type for Third Party Link: </td>
                    <td>
                        <select id="schedule_tp_link_type-select" name="schedule_tp_link_type-sel" onchange="defaultTypeSelect()" <%= dataBean.getProperty("schedule_tp_link_enabled", "false").equals("false") ? "disabled" : ""%>>
                            <option value="Custom">Custom</option>
                            <%
                                Boolean appointmentTypeExists = false;
                                for (AppointmentType appointmentType : appointmentTypeList)
                                {
                                    appointmentTypeExists = tpLinkType.equals(appointmentType.getName());
                            %>
                            <option value="<%=Encode.forHtmlAttribute(appointmentType.getName())%>" <%=tpLinkType.equals(appointmentType.getName()) ? " selected" : ""%>><%=Encode.forHtmlAttribute(appointmentType.getName())%></option>
                            <%
                                }
                            %>
                        </select>
                        <input type="text" id="schedule_tp_link_type-text" name="schedule_tp_link_type" value="<%= Encode.forHtmlAttribute(tpLinkType)%>" <%= dataBean.getProperty("schedule_tp_link_enabled", "false").equals("false") ? "disabled" : ""%> <%= appointmentTypeExists ? "readOnly" : ""%>/>
                        
                    </td>
                </tr>
                <tr>
                    <td>Display Name for Third Party Link: </td>
                    <td>
                        <input type="text" id="schedule_tp_link_display-text" name="schedule_tp_link_display" value="<%= Encode.forHtmlAttribute(StringUtils.isNullOrEmpty(dataBean.getProperty("schedule_tp_link_display")) ? "" : dataBean.getProperty("schedule_tp_link_display"))%>" <%= dataBean.getProperty("schedule_tp_link_enabled", "false").equals("false") ? "disabled" : ""%>/>
                    </td>
                </tr>
                <% if(oscarProps.getProperty("billregion", "").equals("BC")){ %>
                <tr>
                    <td>Display Teleplan Eligibility next to each appointment: </td>
                    <td>
                        <input id="schedule_eligibility_enabled-true" type="radio" value="true" name="schedule_eligibility_enabled"
                                <%=(dataBean.getProperty("schedule_eligibility_enabled", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="schedule_eligibility_enabled-false" type="radio" value="false" name="schedule_eligibility_enabled"
                                <%=(dataBean.getProperty("schedule_eligibility_enabled", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <% } %>
                <tr>
                    <td><bean:message key="admin.schedule.display.settings.enrollment"/>: </td>
                     <td>
                        <input id="schedule_display_enrollment_dr_enabled-true" type="radio" value="true" name="schedule_display_enrollment_dr_enabled"
                                <%=(dataBean.getProperty("schedule_display_enrollment_dr_enabled", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="schedule_display_enrollment_dr_enabled-false" type="radio" value="false" name="schedule_display_enrollment_dr_enabled"
                                <%=(dataBean.getProperty("schedule_display_enrollment_dr_enabled", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>                   
                </tr>
                <tr>
                    <td>Increase 'Last Name' field width during create/edit appointment: </td>
                    <td>
                        <input id="increase_last_name_field_width-true" type="radio" value="true" name="increase_last_name_field_width_enabled"
                            <%=(dataBean.getProperty("increase_last_name_field_width_enabled", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="increase_last_name_field_width-false" type="radio" value="false" name="increase_last_name_field_width_enabled"
                            <%=(dataBean.getProperty("increase_last_name_field_width_enabled", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>Display Outside Use Icon: </td>
                    <td>
                        <input id="schedule_display_outside_use_enabled-true" type="radio" value="true" name="schedule_display_outside_use_enabled"
                        <%=(dataBean.getProperty("schedule_display_outside_use_enabled", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="schedule_display_outside_use_enabled-false" type="radio" value="false" name="schedule_display_outside_use_enabled"
                        <%=(dataBean.getProperty("schedule_display_outside_use_enabled", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>Display Chart No in Create Appointment screen: </td>
                    <td>
                        <input id="display_chart_no_in_create_appointment-true" type="radio" value="true" name="display_chart_no_in_create_appointment"
                                <%= Boolean.parseBoolean(dataBean.getProperty("display_chart_no_in_create_appointment")) ? "checked" : "" %> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="display_chart_no_in_create_appointment-false" type="radio" value="false" name="display_chart_no_in_create_appointment"
                                <%= !Boolean.parseBoolean(dataBean.getProperty("display_chart_no_in_create_appointment")) ? "checked" : "" %> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </tbody>
        </table>

        <input type="button" onclick="document.forms['displaySettingsForm'].dboperation.value='Save'; document.forms['displaySettingsForm'].submit();" name="saveDisplaySettings" value="Save"/>
    </form>
    </body>
</html:html>