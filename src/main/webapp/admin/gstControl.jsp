<!DOCTYPE html>
<%--

    Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

--%>
<%@ page import="java.util.*,oscar.oscarReport.data.*, java.util.Properties, oscar.oscarBilling.ca.on.administration.*"%>
<%@ page import="oscar.oscarBilling.ca.shared.administration.GstControlAction" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
      String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
      boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.billing" rights="w" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_admin&type=_admin.billing");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}
%>

<html:html locale="true">

<%
if (request.getParameter("dboperation") != null && !request.getParameter("dboperation").isEmpty() && request.getParameter("dboperation").equals("Save")) {
	for(String key : SystemPreferences.GST_SETTINGS_KEYS) {
		SystemPreferences preference = SystemPreferencesUtils.findPreferenceByName(key);
		String newValue = request.getParameter(key);
		if(newValue != null) {
			if (preference != null) {
				if (!preference.getValue().equals(newValue)) {
					preference.setUpdateDate(new Date());
					preference.setValue(newValue);
					SystemPreferencesUtils.merge(preference);
				}
			} else {
				preference = new SystemPreferences();
				preference.setName(key);
				preference.setUpdateDate(new Date());
				preference.setValue(newValue);
				SystemPreferencesUtils.persist(preference);
			}
		}
	}
}
	
Properties props = new Properties();
GstControlAction db = new GstControlAction();
SystemPreferences gstNumberPref = SystemPreferencesUtils.findPreferenceByName("clinic_gst_number");
String gstNumber = gstNumberPref != null? gstNumberPref.getValue() : "";
props = db.readDatabase();
String percent = props.getProperty("gstPercent");
String billRegion = OscarProperties.getInstance().getProperty("billregion", "").trim().toUpperCase();
String titleKey = billRegion.equals("ON") ? "admin.admin.manageHSTControl" : "admin.admin.manageGSTControl";
%>

<script type="text/javascript">
    function submitcheck(){
            document.getElementById("gstPercent").value = extractNums(document.getElementById("gstPercent").value);
			document.forms['GstControlForm'].dboperation.value='Save';
			document.forms['GstControlForm'].submit();
    }
    function extractNums(str){
        return str.replace(/\D/g, "");
    }
</script>
<head>
<title><bean:message key="<%=titleKey%>"/></title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<link href="<%=request.getContextPath() %>/css/bootstrap.min.css" rel="stylesheet">
</head>
<body onload="loadData()">

<h3><bean:message key="<%=titleKey%>"/></h3>

<html:form action="/admin/GstControl">
<input type="hidden" name="dboperation" value="">
	<%=billRegion.equals("ON") ? "HST" : "GST"%>:<br>
<div class="input-append">
	<input type="text" class="span2" maxlength="3" id="gstPercent" name="gstPercent" value="<%=percent%>" />
	<span class="add-on">%</span>
</div>
<br>
<% if(billRegion.equals("BC")){ %>
Clinic GST Number:
<div class="input-append">
	<input type="text" id="clinic_gst_number" name="clinic_gst_number" value="<%=Encode.forHtmlAttribute(gstNumber == null? "" : gstNumber)%>"/>
</div>
<br>
<% } %>
<input class="btn btn-primary" type="submit" value="save" onclick="submitcheck()" />
</html:form>
</body>
</html:html>
