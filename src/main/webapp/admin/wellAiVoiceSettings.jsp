<%--
    Copyright (c) 2023 WELL EMR Group Inc.
    This software is made available under the terms of the
    GNU General Public License, Version 2, 1991 (GPLv2).
    License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
  String roleName$ = session.getAttribute("userrole") + "," + session.getAttribute("user");
  boolean authenticated = true;
%>
<security:oscarSec roleName="<%= roleName$ %>" objectName="_admin" rights="r" reverse="<%= true %>">
  <% authenticated = false; %>
  <% response.sendRedirect("../securityError.jsp?type=_admin"); %>
</security:oscarSec>
<%
  if (!authenticated) {
    return;
  }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Map" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
  Map<String, SystemPreferences> preferenceMap = SystemPreferencesUtils.findByKeysAsPreferenceMap(SystemPreferences.WELL_AI_VOICE_SETTINGS);

  if (
      request.getParameter("dboperation") != null
          && !request.getParameter("dboperation").isEmpty()
          && request.getParameter("dboperation").equals("Save")
  ) {
    for (String key : SystemPreferences.WELL_AI_VOICE_SETTINGS) {
      SystemPreferences preference = preferenceMap.get(key);
      String newValue = request.getParameter(key);
      if (preference != null) {
        if (!preference.getValue().equals(newValue)) {
          preference.setUpdateDate(new Date());
          preference.setValue(newValue);
          SystemPreferencesUtils.merge(preference);
        }
      } else {
        preference = new SystemPreferences();
        preference.setName(key);
        preference.setUpdateDate(new Date());
        preference.setValue(newValue);
        SystemPreferencesUtils.persist(preference);
      }
      preferenceMap.put(key, preference);
    }
  }
%>

<html:html locale="true">
  <head>
    <title><bean:message key="admin.integration.well.ai.voice.title"/></title>
    <script src="<%= request.getContextPath() %>/JavaScriptServlet" type="text/javascript"></script>
    <link href="<%= request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">
    <style>
      input[type=radio] {
        margin: 0 0 2px 0;
      }
    </style>
    <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/js/bootstrap.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
  </head>

  <%
    for (SystemPreferences preference : preferenceMap.values()) {
      dataBean.setProperty(preference.getName(), preference.getValue());
    }
  %>

  <body vlink="#0000FF" class="BodyStyle">
  <h4><bean:message key="admin.integration.well.ai.voice.title"/></h4>
  <form name="wellAiVoiceSettingsForm" method="post" action="wellAiVoiceSettings.jsp">
    <input type="hidden" name="dboperation" value="">
    <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
      <tbody>
      <tr>
        <td><bean:message key="admin.integration.well.ai.voice.enable"/> :</td>
        <td>
          <input id="well-ai-voice-enabled-true" type="radio" value="true" name="well_ai_voice.enabled"
              <%= (dataBean.getProperty("well_ai_voice.enabled", "false").equals("true")) ? "checked" : "" %> />
          Yes
          &nbsp;&nbsp;&nbsp;
          <input id="well-ai-voice-enabled-false" type="radio" value="false" name="well_ai_voice.enabled"
              <%= (dataBean.getProperty("well_ai_voice.enabled", "false").equals("false")) ? "checked" : "" %> />
          No
          &nbsp;&nbsp;&nbsp;
        </td>
      </tr>
      </tbody>
    </table>
    <input
        type="button"
        onclick="document.forms['wellAiVoiceSettingsForm'].dboperation.value='Save'; document.forms['wellAiVoiceSettingsForm'].submit();"
        name="saveWellAiVoiceSettings"
        value="Save"
    />
  </form>
  </body>
</html:html>