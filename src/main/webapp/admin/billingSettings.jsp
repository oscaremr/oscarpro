<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.PropertyDao" %>
<%@ page import="org.oscarehr.common.model.Property" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="oscar.util.StringUtils" %>
<%@ page import="oscar.oscarBilling.ca.bc.data.BillingFormData" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    boolean isToDisplayDemographicFilter = SystemPreferencesUtils.isReadBooleanPreferenceWithDefault(SystemPreferences.DEMOGRAPHIC_FILTER_ENABLED, false);
    List<String> systemSettingsKeys = Arrays.asList(SystemPreferences.DEMOGRAPHIC_FILTER_ENABLED);
    PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
    String billRegion = OscarProperties.getInstance().getProperty("billregion", "").trim();
    List<String> billingSettingsKeys = Arrays.asList("display_patient_dob_on_3rd_party_invoices",
            "display_patient_hin_on_3rd_party_invoices", "enable_3rd_party_billing_footer",
            "3rd_party_billing_footer_text", "enable_footer_patient_id", "auto_populate_refer",
            "bc_billing_min_invoices", "bc_billing_max_invoices", "bc_default_service_location",
            "display_dx23", "load_all_providers_by_default",
            "enable_add_dxCode_to_disease_registry_in_eChart",
            "enable_create_invoice_link_on_patient_search",
            "display_payment_method_column_in_invoice_report");

    StringBuilder errorMessages = new StringBuilder();

    if (request.getParameter("dboperation") != null && !request.getParameter("dboperation").isEmpty() && request.getParameter("dboperation").equals("Save")) {
        for(String key : billingSettingsKeys) {
            List<Property> property = propertyDao.findGlobalByName(key);
            String newValue = request.getParameter(key);

            if (billRegion.equals("BC")) {
                if ("bc_billing_min_invoices".equals(key)) {
                    if (!StringUtils.isInteger(newValue)) {
                        errorMessages.append("<span style=\"color: red;\">BC Billing Min Invoices must be a number from 3 - 6.</span>");
                        continue;
                    }
                    int intValue = Integer.parseInt(newValue);
                    if (intValue < 3 || intValue > 6) {
                        errorMessages.append("<span style=\"color: red;\">BC Billing Min Invoices must be a number from 3 - 6.</span>");
                        continue;
                    }
                } else if ("bc_billing_max_invoices".equals(key)) {
                    if (!StringUtils.isInteger(newValue)) {
                        errorMessages.append("<span style=\"color: red;\">BC Billing Max Invoices must be a number.</span>");
                        continue;
                    }
                    int intValue = Integer.parseInt(newValue);
                    if (StringUtils.isInteger(request.getParameter("bc_billing_min_invoices")) && intValue < Integer.parseInt(request.getParameter("bc_billing_min_invoices"))) {
                        errorMessages.append("<span style=\"color: red;\">BC Billing Max Invoices must be greater than or equal to Min Invoices.</span>");
                        continue;
                    }
                    if (intValue < 3 || intValue > 6) {
                        errorMessages.append("<span style=\"color: red;\">BC Billing Max Invoices must be a number from 3 - 6.</span>");
                        continue;
                    }
                }
            }
            
            if (property.isEmpty()) {
                Property newProperty = new Property();
                newProperty.setName(key);
                newProperty.setValue(newValue);
                propertyDao.persist(newProperty);
            } else {
                for (Property p : property) {
                    p.setValue(newValue);
                    propertyDao.merge(p);
                }
            }
        }

        for (String key : systemSettingsKeys) {
            String newValue = request.getParameter(key);
            SystemPreferencesUtils.mergeOrPersist(key, newValue);
        }
    }
%>

<html:html locale="true">
    <head>
        <title>Billing Settings</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
        <script>
            function hasScrollbar(element_id)
            {
                var elem = document.getElementById(element_id);
                if (elem.clientHeight < elem.scrollHeight) {
                    document.getElementById("warning_text").style.visibility = "visible";
                } else {
                    document.getElementById("warning_text").style.visibility = "hidden";
                }
            }
        </script>
    </head>

    <%
        for(String key : billingSettingsKeys) {
            List<Property> properties = propertyDao.findGlobalByName(key);
            if (!properties.isEmpty() && properties.get(0).getName() != null && properties.get(0).getValue() != null) {
                dataBean.setProperty(properties.get(0).getName(), properties.get(0).getValue());
            }
        }
        isToDisplayDemographicFilter = SystemPreferencesUtils.isReadBooleanPreferenceWithDefault(SystemPreferences.DEMOGRAPHIC_FILTER_ENABLED, false);
    %>

    <body vlink="#0000FF" class="BodyStyle">
    <div id="warning_text" style="color: #e26e6e; visibility: hidden;"><b>Warning: Footer text size is too large! This may cause undesired formatting!</b></div>
    <h4>Manage OSCAR Billing Settings</h4>
    <form name="billingSettingsForm" method="post" action="billingSettings.jsp">
        <%=errorMessages.toString()%>
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <oscar:oscarPropertiesCheck property="billregion" value="ON">
            <tr>
                <td>Display Patient DOB on 3rd Party Invoices: </td>
                <td>
                    <input id="display_patient_dob_on_3rd_party_invoices_true" type="radio" value="true" name="display_patient_dob_on_3rd_party_invoices"
                            <%=(dataBean.getProperty("display_patient_dob_on_3rd_party_invoices", "true").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="display_patient_dob_on_3rd_party_invoices_false" type="radio" value="false" name="display_patient_dob_on_3rd_party_invoices"
                            <%=(dataBean.getProperty("display_patient_dob_on_3rd_party_invoices", "true").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Display Patient HIN on 3rd Party Invoices: </td>
                <td>
                    <input id="display_patient_hin_on_3rd_party_invoices_true" type="radio" value="true" name="display_patient_hin_on_3rd_party_invoices"
                            <%=(dataBean.getProperty("display_patient_hin_on_3rd_party_invoices", "true").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="display_patient_hin_on_3rd_party_invoices_false" type="radio" value="false" name="display_patient_hin_on_3rd_party_invoices"
                            <%=(dataBean.getProperty("display_patient_hin_on_3rd_party_invoices", "true").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Enable Custom 3rd Party Billing Footer: </td>
                <td>
                    <input id="enable_3rd_party_billing_footer-true" type="radio" value="true" name="enable_3rd_party_billing_footer"
                            <%=(dataBean.getProperty("enable_3rd_party_billing_footer", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="enable_3rd_party_billing_footer-false" type="radio" value="false" name="enable_3rd_party_billing_footer"
                            <%=(dataBean.getProperty("enable_3rd_party_billing_footer", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Display Patient ID Number on footer: </td>
                <td>
                    <input id="enable_footer_patient_id-true" type="radio" value="true" name="enable_footer_patient_id"
                            <%=(dataBean.getProperty("enable_footer_patient_id", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="enable_footer_patient_id-false" type="radio" value="false" name="enable_footer_patient_id"
                            <%=(dataBean.getProperty("enable_footer_patient_id", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Display "B" Create Invoice link on Patient Search: </td>
                <td>
                    <input id="enable_create_invoice_link_on_patient_search-true" type="radio"
                           value="true" name="enable_create_invoice_link_on_patient_search"
                            <%= Boolean.parseBoolean(
                                dataBean.getProperty("enable_create_invoice_link_on_patient_search"))
                                ? "checked" : "" %> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="enable_create_invoice_link_on_patient_search-false" type="radio"
                           value="false" name="enable_create_invoice_link_on_patient_search"
                            <%= !Boolean.parseBoolean(
                                dataBean.getProperty("enable_create_invoice_link_on_patient_search"))
                                ? "checked" : "" %> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Display Payment Method columns in Invoice Report: </td>
                <td>
                    <input id="display_payment_method_column_in_invoice_report_true" type="radio" value="true" name="display_payment_method_column_in_invoice_report"
                        <%= Boolean.valueOf(dataBean.getProperty("display_payment_method_column_in_invoice_report")) ? "checked" : "" %> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="display_payment_method_column_in_invoice_report-false" type="radio" value="false" name="display_payment_method_column_in_invoice_report"
                        <%= !Boolean.valueOf(dataBean.getProperty("display_payment_method_column_in_invoice_report")) ? "checked" : "" %> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Add Dx code to Disease Registry in eChart after saving invoice: </td>
                <td>
                    <input id="enable_add_dxCode_to_disease_registry_in_eChart-true" type="radio" value="true" name="enable_add_dxCode_to_disease_registry_in_eChart"
                            <%=(dataBean.getProperty("enable_add_dxCode_to_disease_registry_in_eChart", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="enable_add_dxCode_to_disease_registry_in_eChart-false" type="radio" value="false" name="enable_add_dxCode_to_disease_registry_in_eChart"
                            <%=(dataBean.getProperty("enable_add_dxCode_to_disease_registry_in_eChart", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
              <td>Show include/exclude for specified demographics during Claim Simulation/Generation:</td>
                <td>
                  <input id="display_demographic_filter_on_simulation_or_generation-true" type="radio" value="true"
                          name="<%=SystemPreferences.DEMOGRAPHIC_FILTER_ENABLED%>"
                          <%=(isToDisplayDemographicFilter) ? "checked" : ""%> />
                  Yes
                  &nbsp;&nbsp;&nbsp;
                  <input id="display_demographic_filter_on_simulation_or_generation-false" type="radio" value="false"
                          name="<%=SystemPreferences.DEMOGRAPHIC_FILTER_ENABLED%>"
                          <%=(!isToDisplayDemographicFilter) ? "checked" : ""%> />
                  No
                  &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Load 'All Providers' by default in RA Summary: </td>
                <td>
                    <input id="load_all_providers_by_default-true" type="radio" value="true" name="load_all_providers_by_default"
                            <%=(dataBean.getProperty("load_all_providers_by_default", "true").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="load_all_providers_by_default-false" type="radio" value="false" name="load_all_providers_by_default"
                            <%=(dataBean.getProperty("load_all_providers_by_default", "true").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <textarea id="3rd_party_billing_footer_text" name="3rd_party_billing_footer_text" onKeyUp='hasScrollbar("3rd_party_billing_footer_text");' rows="4" cols="400" maxlength="2000" style="overflow: auto; min-width:572px; max-width:572px; min-height:100px; max-height:100px;"><%=Encode.forHtmlContent(dataBean.getProperty("3rd_party_billing_footer_text", ""))%></textarea>
                </td>
            </tr>
            </oscar:oscarPropertiesCheck>
            <oscar:oscarPropertiesCheck property="billregion" value="BC">
            <tr>
                <td>Auto-populate Referring Physician on Billing Form for All Providers?: </td>
                <td>
                    <input id="auto_populate_refer-true" type="radio" value="true" name="auto_populate_refer"
                            <%=(dataBean.getProperty("auto_populate_refer", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="auto_populate_refer-false" type="radio" value="false" name="auto_populate_refer"
                            <%=(dataBean.getProperty("auto_populate_refer", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Set the minimum/maximum amount of invoices that can be created on one Create invoice form? (eg. Min: 3-6, Max: 3-6): </td>
                <td>
                    Min
                    <input id="bc_billing_min_invoices" type="text" maxlength="6" style="min-width:30px; max-width:30px;" value="<%=Encode.forHtmlContent(dataBean.getProperty("bc_billing_min_invoices", "3"))%>" name="bc_billing_min_invoices" />
                    Max
                    <input id="bc_billing_max_invoices" type="text" maxlength="6" style="min-width:30px; max-width:30px;" value="<%=Encode.forHtmlContent(dataBean.getProperty("bc_billing_max_invoices", "3"))%>" name="bc_billing_max_invoices" />
                </td>
            </tr>
            <tr>
                <td>Set the default Teleplan service location for new invoices: </td>
                <td>
                    <select id="bc_default_service_location" name="bc_default_service_location">
                        <%
                            BillingFormData billingFormData = new BillingFormData();
                            List<BillingFormData.BillingVisit> billingVisits = billingFormData.getVisitType(billRegion);
                            String defaultServiceLocation = dataBean.getProperty("bc_default_service_location", "");
                            if (StringUtils.isNullOrEmpty(defaultServiceLocation)) {
                                // Get the visittype property
                                String visitType = OscarProperties.getInstance().getProperty("visittype");
                                // If the visittype contains a pipe separator, get the value before the pipe separator Eg. A|Practitioner's Office - In Community
                                defaultServiceLocation = visitType != null && visitType.contains("|") ? visitType.split("\\|")[0] : visitType;
                            }
                            for(BillingFormData.BillingVisit billingVisit : billingVisits) {
                                String visitType = billingVisit.getVisitType();
                                String visitDescription = visitType + " | " + billingVisit.getDescription();
                                String selected="";
                                if (visitType.equals(defaultServiceLocation)) {
                                    selected=" selected=\"selected\" ";
                                }
                        %>
                        <option value="<%=visitType%>" <%=selected%>><%=visitDescription%></option>
                        <%  } %>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Display DX2/3 on the Create Invoice page by default?: </td>
                <td>
                    <input id="display_dx23-true" type="radio" value="true" name="display_dx23"
                            <%=(dataBean.getProperty("display_dx23", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="display_dx23-false" type="radio" value="false" name="display_dx23"
                            <%=(dataBean.getProperty("display_dx23", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Display "B" Create Invoice link on Patient Search: </td>
                <td>
                    <input id="enable-create-invoice-link-on-patient-search-true-bc" type="radio"
                           value="true" name="enable_create_invoice_link_on_patient_search"
                        <%= Boolean.parseBoolean(
                            dataBean.getProperty("enable_create_invoice_link_on_patient_search"))
                            ? "checked" : "" %> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="enable-create-invoice-link-on-patient-search-false-bc" type="radio"
                           value="false" name="enable_create_invoice_link_on_patient_search"
                        <%= !Boolean.parseBoolean(
                            dataBean.getProperty("enable_create_invoice_link_on_patient_search"))
                            ? "checked" : "" %> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            </oscar:oscarPropertiesCheck>
            </tbody>
        </table>

        <input type="button" onclick="document.forms['billingSettingsForm'].dboperation.value='Save'; document.forms['billingSettingsForm'].submit();" name="saveBillingSettings" value="Save"/>
    </form>
    </body>
</html:html>