<%--
 /*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
--%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<%
    String roleName$ =
            (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    SystemPreferences kaiUsernameSystemPreference = SystemPreferencesUtils
            .findPreferenceByName("kai_username");

    boolean isSupportUser = kaiUsernameSystemPreference != null && loggedInInfo != null
            && loggedInInfo.getLoggedInSecurity() != null
            && org.apache.commons.lang.StringUtils
            .trimToEmpty(kaiUsernameSystemPreference.getValue())
            .equalsIgnoreCase(loggedInInfo.getLoggedInSecurity().getUserName());

    boolean authed = isSupportUser;
%>
<security:oscarSec roleName="<%= roleName$ %>" objectName="_admin" rights="r" reverse="<%= true %>">
    <% authed = false; %>
    <% response.sendRedirect("../securityError.jsp?type=_admin"); %>
</security:oscarSec>
<%
    if (!authed) {
        return;
    }

    StringBuilder errorMessages = new StringBuilder();
    OscarProperties oscarProps = OscarProperties.getInstance();

    if (request.getParameter("dboperation") != null
            && !request.getParameter("dboperation").isEmpty()
            && request.getParameter("dboperation").equals("Save")) {
        List<SystemPreferences> billingSettingsSystemPreferences = SystemPreferencesUtils
                .findPreferencesByNames(SystemPreferences.OSCAR_PROPERTIES_BILLING_SETTINGS_KEYS);
        // Index retrieved SystemPreferences in a map for direct retrieval later
        Map<String, SystemPreferences> billingSettingsSystemPreferencesMap = new HashMap<>();
        for (SystemPreferences systemPreferenceItem : billingSettingsSystemPreferences) {
            billingSettingsSystemPreferencesMap.put(systemPreferenceItem.getName(),
                    systemPreferenceItem);
        }

        for (String billingSettingsKey : SystemPreferences.OSCAR_PROPERTIES_BILLING_SETTINGS_KEYS) {
            SystemPreferences systemPreference = billingSettingsSystemPreferencesMap.get(
                    billingSettingsKey);
            String newValue = request.getParameter(billingSettingsKey);

            if (systemPreference != null) {
                if (!systemPreference.getValue().equals(newValue)) {
                    systemPreference.setUpdateDate(new Date());
                    systemPreference.setValue(newValue);
                    SystemPreferencesUtils.merge(systemPreference);
                }
            } else {
                systemPreference = new SystemPreferences();
                systemPreference.setName(billingSettingsKey);
                systemPreference.setUpdateDate(new Date());
                systemPreference.setValue(newValue);
                SystemPreferencesUtils.persist(systemPreference);
            }
        }
    }

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html:html locale="true">
    <head>
        <title>
            <bean:message key="admin.admin.oscarProperties.billingSettings.billingSettings"/>
        </title>
        <script src="<%= request.getContextPath() %>/JavaScriptServlet"
                type="text/javascript"></script>
        <link href="<%= request.getContextPath() %>/css/bootstrap.css" rel="stylesheet"
              type="text/css">
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript"
                src="<%= request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript"
                src="<%= request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript"
                src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
    </head>
    <body vlink="#0000FF" class="BodyStyle">
    <h4><bean:message key="admin.admin.oscarProperties.billingSettings.settingsSectionTitle"/></h4>
    <form name="billingSettingsForm" method="post" action="billingSettings.jsp">
        <br>
        <%= errorMessages.toString() %>
        <br>
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable"
               class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.billingSettings.enableMOHFileManagement"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="moh_file_management_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("moh_file_management_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="moh_file_management_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("moh_file_management_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.billingSettings.enableNewONBilling"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="new_on_billing_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("new_on_billing_enabled", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="new_on_billing_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("new_on_billing_enabled", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.billingSettings.enableDefaultBCAltBilling"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="bc_default_alt_billing_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("bc_default_alt_billing_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="bc_default_alt_billing_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("bc_default_alt_billing_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.billingSettings.autoGeneratedBilling"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="auto_generated_billing_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("auto_generated_billing_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="auto_generated_billing_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("auto_generated_billing_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.billingSettings.createNewNoteWithEveryNewInvoice"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="bill_note_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("bill_note_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="bill_note_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("bill_note_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.billingSettings.enableAutoPopulatingPaymentFieldOnBillingReview"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="billing_review_auto_payment_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("billing_review_auto_payment_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="billing_review_auto_payment_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("billing_review_auto_payment_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.billingSettings.hideNameOnTheInvoiceReportsPrint"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="invoice_reports_print_hide_name_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("invoice_reports_print_hide_name_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="invoice_reports_print_hide_name_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("invoice_reports_print_hide_name_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.billingSettings.removePatientDetailsIn3rdPartyInvoice"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="remove_patient_detail_in_third_party"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("remove_patient_detail_in_third_party", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="remove_patient_detail_in_third_party"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("remove_patient_detail_in_third_party", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.billingSettings.rmaBillingEnabled"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="rma_billing_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rma_billing_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="rma_billing_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rma_billing_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.billingSettings.enableCheckAllForUpdatingBillingPrice"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="sob_check_all_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("sob_check_all_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="sob_check_all_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("sob_check_all_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.billingSettings.enableConfirmActionOnDeleteBillFromBillingHistory"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="delete_bill_confirm_action_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("delete_bill_confirm_action_enabled", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="delete_bill_confirm_action_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("delete_bill_confirm_action_enabled", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.billingSettings.enableNewBillingUi"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="new_billing_ui.enabled"
                            <%= (SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault("new_billing_ui.enabled", false))
                                    ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="new_billing_ui.enabled"
                            <%= (!SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault("new_billing_ui.enabled", false))
                                    ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            </tbody>
        </table>
        <br>
        <input type="button"
               onclick="document.forms['billingSettingsForm'].dboperation.value='Save'; document.forms['billingSettingsForm'].submit();"
               name="saveBillingSettings" value="Save"/>
    </form>
    </body>
</html:html>
