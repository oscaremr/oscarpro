<%--
 /*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
--%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>


<%
    String roleName$ =
            (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    SystemPreferences kaiUsernameSystemPreference = SystemPreferencesUtils
            .findPreferenceByName("kai_username");

    boolean isSupportUser = kaiUsernameSystemPreference != null && loggedInInfo != null
            && loggedInInfo.getLoggedInSecurity() != null
            && org.apache.commons.lang.StringUtils
            .trimToEmpty(kaiUsernameSystemPreference.getValue())
            .equalsIgnoreCase(loggedInInfo.getLoggedInSecurity().getUserName());

    boolean authed = isSupportUser;
%>
<security:oscarSec roleName="<%= roleName$ %>" objectName="_admin" rights="r" reverse="<%= true %>">
    <% authed = false; %>
    <% response.sendRedirect("../securityError.jsp?type=_admin"); %>
</security:oscarSec>
<%
    if (!authed) {
        return;
    }

    StringBuilder errorMessages = new StringBuilder();
    OscarProperties oscarProps = OscarProperties.getInstance();

    if (request.getParameter("dboperation") != null
            && !request.getParameter("dboperation").isEmpty()
            && request.getParameter("dboperation").equals("Save")) {
        List<SystemPreferences> faxSettingsSystemPreferences = SystemPreferencesUtils
                .findPreferencesByNames(SystemPreferences.OSCAR_PROPERTIES_FAX_SETTINGS_KEYS);
        // Index retrieved SystemPreferences in a map for direct retrieval later
        Map<String, SystemPreferences> faxSettingsSystemPreferencesMap = new HashMap<>();
        for (SystemPreferences systemPreferenceItem : faxSettingsSystemPreferences) {
            faxSettingsSystemPreferencesMap.put(systemPreferenceItem.getName(),
                    systemPreferenceItem);
        }

        for (String faxSettingsKey : SystemPreferences.OSCAR_PROPERTIES_FAX_SETTINGS_KEYS) {
            SystemPreferences systemPreference = faxSettingsSystemPreferencesMap.get(
                    faxSettingsKey);
            String newValue = request.getParameter(faxSettingsKey);

            if (systemPreference != null) {
                if (!systemPreference.getValue().equals(newValue)) {
                    systemPreference.setUpdateDate(new Date());
                    systemPreference.setValue(newValue);
                    SystemPreferencesUtils.merge(systemPreference);
                }
            } else {
                systemPreference = new SystemPreferences();
                systemPreference.setName(faxSettingsKey);
                systemPreference.setUpdateDate(new Date());
                systemPreference.setValue(newValue);
                SystemPreferencesUtils.persist(systemPreference);
            }
        }
    }

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html:html locale="true">
    <head>
        <title>
            <bean:message key="admin.admin.oscarProperties.faxSettings.faxSettings"/>
        </title>
        <script src="<%= request.getContextPath() %>/JavaScriptServlet"
                type="text/javascript"></script>
        <link href="<%= request.getContextPath() %>/css/bootstrap.css" rel="stylesheet"
              type="text/css">
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript"
                src="<%= request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript"
                src="<%= request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript"
                src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
    <h4><bean:message key="admin.admin.oscarProperties.faxSettings.settingsSectionTitle"/></h4>
    <form name="faxSettingsForm" method="post" action="faxSettings.jsp">
        <br>
        <%= errorMessages.toString() %>
        <br>
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable"
               class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <div class="row">
                <div class="span12">
                    <table class="table table-bordered table-striped table-hover table-condensed">
                        <tbody>
                        <tr>
                            <td>
                                <bean:message key="admin.admin.oscarProperties.faxSettings.enableFax"/>:
                            </td>
                            <td>
                                <input type="radio" value="true" name="fax_enabled"
                                    <%= (SystemPreferencesUtils
                                            .isReadBooleanPreferenceWithDefault("fax_enabled", false))
                                            ? "checked" : "" %> />
                                <bean:message key="global.yes"/>
                                &nbsp;&nbsp;&nbsp;
                                <input type="radio" value="false" name="fax_enabled"
                                    <%= (!SystemPreferencesUtils
                                            .isReadBooleanPreferenceWithDefault("fax_enabled", false))
                                            ? "checked" : "" %> />
                                <bean:message key="global.no"/>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <bean:message key="admin.admin.oscarProperties.faxSettings.enableRxFax"/>:
                            </td>
                            <td>
                                <input type="radio" value="true" name="rx_fax_enabled"
                                    <%= (SystemPreferencesUtils
                                            .isReadBooleanPreferenceWithDefault("rx_fax_enabled", false))
                                            ? "checked" : "" %> />
                                <bean:message key="global.yes"/>
                                &nbsp;&nbsp;&nbsp;
                                <input type="radio" value="false" name="rx_fax_enabled"
                                    <%= (!SystemPreferencesUtils
                                            .isReadBooleanPreferenceWithDefault("rx_fax_enabled", false))
                                            ? "checked" : "" %> />
                                <bean:message key="global.no"/>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <bean:message key="admin.admin.oscarProperties.faxSettings.enableConsultationFax"/>:
                            </td>
                            <td>
                                <input type="radio" value="true" name="consultation_fax_enabled"
                                    <%= (SystemPreferencesUtils
                                            .isReadBooleanPreferenceWithDefault("consultation_fax_enabled", true))
                                            ? "checked" : "" %> />
                                <bean:message key="global.yes"/>
                                &nbsp;&nbsp;&nbsp;
                                <input type="radio" value="false" name="consultation_fax_enabled"
                                    <%= (!SystemPreferencesUtils
                                            .isReadBooleanPreferenceWithDefault("consultation_fax_enabled", true))
                                            ? "checked" : "" %> />
                                <bean:message key="global.no"/>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <bean:message key="admin.admin.oscarProperties.faxSettings.enableEFormFax"/>:
                            </td>
                            <td>
                                <input type="radio" value="true" name="eform_fax_enabled"
                                    <%= (SystemPreferencesUtils
                                            .isReadBooleanPreferenceWithDefault("eform_fax_enabled", false))
                                            ? "checked" : "" %> />
                                <bean:message key="global.yes"/>
                                &nbsp;&nbsp;&nbsp;
                                <input type="radio" value="false" name="eform_fax_enabled"
                                    <%= (!SystemPreferencesUtils
                                            .isReadBooleanPreferenceWithDefault("eform_fax_enabled", false))
                                            ? "checked" : "" %> />
                                <bean:message key="global.no"/>
                                &nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            </tbody>
        </table>
        <br>
        <input type="button"
               onclick="document.forms['faxSettingsForm'].dboperation.value='Save'; document.forms['faxSettingsForm'].submit();"
               name="saveFaxSettings" value="Save"/>
    </form>
    </body>
</html:html>
