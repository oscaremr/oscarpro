<%--
 /*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
--%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>


<%
    String roleName$ =
            (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    SystemPreferences kaiUsernameSystemPreference = SystemPreferencesUtils
            .findPreferenceByName("kai_username");

    boolean isSupportUser = kaiUsernameSystemPreference != null && loggedInInfo != null
            && loggedInInfo.getLoggedInSecurity() != null
            && org.apache.commons.lang.StringUtils
            .trimToEmpty(kaiUsernameSystemPreference.getValue())
            .equalsIgnoreCase(loggedInInfo.getLoggedInSecurity().getUserName());

    boolean authed = isSupportUser;
%>
<security:oscarSec roleName="<%= roleName$ %>" objectName="_admin" rights="r" reverse="<%= true %>">
    <% authed = false; %>
    <% response.sendRedirect("../securityError.jsp?type=_admin"); %>
</security:oscarSec>
<%
    if (!authed) {
        return;
    }

    StringBuilder errorMessages = new StringBuilder();
    OscarProperties oscarProps = OscarProperties.getInstance();

    if (request.getParameter("dboperation") != null
            && !request.getParameter("dboperation").isEmpty()
            && request.getParameter("dboperation").equals("Save")) {
        List<SystemPreferences> eChartSettingsSystemPreferences = SystemPreferencesUtils
                .findPreferencesByNames(SystemPreferences.OSCAR_PROPERTIES_ECHART_SETTINGS_KEYS);
        // Index retrieved SystemPreferences in a map for direct retrieval later
        Map<String, SystemPreferences> eChartSettingsSystemPreferencesMap = new HashMap<>();
        for (SystemPreferences systemPreferenceItem : eChartSettingsSystemPreferences) {
            eChartSettingsSystemPreferencesMap.put(systemPreferenceItem.getName(),
                    systemPreferenceItem);
        }

        for (String eChartSettingsKey : SystemPreferences.OSCAR_PROPERTIES_ECHART_SETTINGS_KEYS) {
            SystemPreferences systemPreference = eChartSettingsSystemPreferencesMap.get(
                    eChartSettingsKey);
            String newValue = request.getParameter(eChartSettingsKey);

            if (systemPreference != null) {
                if (!systemPreference.getValue().equals(newValue)) {
                    systemPreference.setUpdateDate(new Date());
                    systemPreference.setValue(newValue);
                    SystemPreferencesUtils.merge(systemPreference);
                }
            } else {
                systemPreference = new SystemPreferences();
                systemPreference.setName(eChartSettingsKey);
                systemPreference.setUpdateDate(new Date());
                systemPreference.setValue(newValue);
                SystemPreferencesUtils.persist(systemPreference);
            }
        }
    }

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html:html locale="true">
    <head>
        <title>
            <bean:message key="admin.admin.oscarProperties.eChartSettings.eChartSettings"/>
        </title>
        <script src="<%= request.getContextPath() %>/JavaScriptServlet"
                type="text/javascript"></script>
        <link href="<%= request.getContextPath() %>/css/bootstrap.css" rel="stylesheet"
              type="text/css">
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript"
                src="<%= request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript"
                src="<%= request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript"
                src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
    <h4><bean:message key="admin.admin.oscarProperties.eChartSettings.settingsSectionTitle"/></h4>
    <form name="eChartSettingsForm" method="post" action="eChartSettings.jsp">
        <br>
        <%= errorMessages.toString() %>
        <br>
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable"
               class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.eChartSettings.includeCellPhoneInChartPrintHeader"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="chart_print_include_cellphone"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("chart_print_include_cellphone", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="chart_print_include_cellphone"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("chart_print_include_cellphone", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.eChartSettings.includeHinInChartPrintHeader"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="chart_print_include_hin"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("chart_print_include_hin", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="chart_print_include_hin"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("chart_print_include_hin", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.eChartSettings.includeHomePhoneInChartPrintHeader"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="chart_print_include_home_phone"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("chart_print_include_home_phone", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="chart_print_include_home_phone"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("chart_print_include_home_phone", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.eChartSettings.includeWorkPhoneInChartPrintHeader"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="chart_print_include_work_phone"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("chart_print_include_work_phone", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="chart_print_include_work_phone"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("chart_print_include_work_phone", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.eChartSettings.includeMrpInChartPrintHeader"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="chart_print_include_mrp"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("chart_print_include_mrp", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="chart_print_include_mrp"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("chart_print_include_mrp", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.eChartSettings.useProviderCurrentProgramInChartPrint"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="chart_print_use_provider_current_program"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("chart_print_use_provider_current_program", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="chart_print_use_provider_current_program"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("chart_print_use_provider_current_program", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.eChartSettings.showEChartRelations"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="echart_show_relations"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("echart_show_relations", false))
                                ? "checked" : ""%> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="echart_show_relations"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("echart_show_relations", false))
                                ? "checked" : ""%> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.eChartSettings.showEChartDemographicNumber"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="echart_display_demographic_no"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("echart_display_demographic_no", false))
                                ? "checked" : ""%> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="echart_display_demographic_no"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("echart_display_demographic_no", false))
                                ? "checked" : ""%> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.eChartSettings.showEChartInProgramDomain"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="program_domain_show_echart"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("program_domain_show_echart", false))
                                ? "checked" : ""%> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="program_domain_show_echart"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("program_domain_show_echart", false))
                                ? "checked" : ""%> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            </tbody>
        </table>
        <br>
        <input type="button"
               onclick="document.forms['eChartSettingsForm'].dboperation.value='Save'; document.forms['eChartSettingsForm'].submit();"
               name="saveEChartSettings" value="Save"/>
    </form>
    </body>
</html:html>
