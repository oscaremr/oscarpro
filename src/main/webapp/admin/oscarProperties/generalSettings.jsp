<%--
 /*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
--%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="oscar.util.StringUtils" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="java.util.Properties" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ page import="static oscar.PreferencesDefaultConstants.ADMIN_USER_BILLING_CONTROL_DEFAULT" %>


<%
    ResourceBundle oscarResourcesProperties = ResourceBundle
            .getBundle("oscarResources", request.getLocale());
    OscarProperties oscarProps = OscarProperties.getInstance();
    String roleName$ =
            (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    SystemPreferences kaiUsernameSystemPreference = SystemPreferencesUtils
            .findPreferenceByName("kai_username");

    boolean isSupportUser = kaiUsernameSystemPreference != null && loggedInInfo != null
            && loggedInInfo.getLoggedInSecurity() != null
            && org.apache.commons.lang.StringUtils
            .trimToEmpty(kaiUsernameSystemPreference.getValue())
            .equalsIgnoreCase(loggedInInfo.getLoggedInSecurity().getUserName());

    boolean authed = isSupportUser;
%>
<security:oscarSec roleName="<%= roleName$ %>" objectName="_admin" rights="r" reverse="<%= true %>">
    <% authed = false; %>
    <% response.sendRedirect("../securityError.jsp?type=_admin"); %>
</security:oscarSec>
<%
    if (!authed) {
        return;
    }

    StringBuilder errorMessages = new StringBuilder();

    if (request.getParameter("dboperation") != null
            && !request.getParameter("dboperation").isEmpty()
            && request.getParameter("dboperation").equals("Save")) {
        List<SystemPreferences> generalSettingsSystemPreferences = SystemPreferencesUtils
                .findPreferencesByNames(SystemPreferences.OSCAR_PROPERTIES_GENERAL_SETTINGS_KEYS);
        // Index retrieved SystemPreferences in a map for direct retrieval later
        Map<String, SystemPreferences> generalSettingsSystemPreferencesMap = new HashMap<>();
        for (SystemPreferences systemPreferenceItem : generalSettingsSystemPreferences) {
            generalSettingsSystemPreferencesMap.put(systemPreferenceItem.getName(),
                    systemPreferenceItem);
        }

        for (String generalSettingsKey : SystemPreferences.OSCAR_PROPERTIES_GENERAL_SETTINGS_KEYS) {
            SystemPreferences systemPreference = generalSettingsSystemPreferencesMap.get(
                    generalSettingsKey);
            String newValue = request.getParameter(generalSettingsKey);

            if ("number_of_flowsheet_values".equals(generalSettingsKey)) {
                if (!StringUtils.isInteger(request.getParameter("number_of_flowsheet_values"))) {
                    errorMessages.append(
                            "<span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.numberOfFlowsheetValuesMustBeInteger")
                                    + "</span>");
                    continue;
                } else if (Integer.parseInt(request.getParameter("number_of_flowsheet_values"))
                        < 0) {
                    errorMessages.append(
                            "<span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.numberOfFlowsheetValuesMustBeGreaterThanOrEqual")
                                    + "</span>");
                    continue;
                }
            }
            
            if ("daily_appointments_page_refresh_timeout".equals(generalSettingsKey)) {
                if (!StringUtils.isInteger(
                        request.getParameter("daily_appointments_page_refresh_timeout"))) {
                    errorMessages.append(
                            "<span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.dailyAppointmentsPageRefreshTimeoutValueNotAnInteger")
                                    + "</span>");
                    continue;
                } else if (Integer.parseInt(
                        request.getParameter("daily_appointments_page_refresh_timeout")) < 0) {
                    errorMessages.append(
                            "<span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.dailyAppointmentsPageRefreshTimeoutValueCanNotBeNegative")
                                    + "</span>");
                    continue;
                }
            }

            if ("tickler_warn_period".equals(generalSettingsKey)) {
                if (!StringUtils.isInteger(request.getParameter("tickler_warn_period"))) {
                    errorMessages.append(
                            "<span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.ticklerWarnDaysValueMustBeInteger")
                                    + "</span>");
                    continue;
                } else if (Integer.parseInt(request.getParameter("tickler_warn_period")) < -1) {
                    errorMessages.append(
                            "<span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.ticklerWarnDaysValueGreaterThanOrEqual")
                                    + "</span>");
                    continue;
                }
            }

            if ("ect_autosave_timer".equals(generalSettingsKey)) {
                if (!StringUtils.isInteger(request.getParameter("ect_autosave_timer"))) {
                    errorMessages.append(
                            "<br><span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.ectAutosaveTimerMustBeInteger")
                                    + "</span>");
                    continue;
                } else if (Integer.parseInt(request.getParameter("ect_autosave_timer")) < 0) {
                    errorMessages.append(
                            "<br><span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.ectAutosaveTimerGreaterThanOrEqual")
                                    + "</span>");
                    continue;
                }
            } else if ("ect_save_feedback_timer".equals(generalSettingsKey)) {
                if (!StringUtils.isInteger(request.getParameter("ect_save_feedback_timer"))) {
                    errorMessages.append(
                            "<br><span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.ectSaveFeedbackTimerMustBeInteger")
                                    + "</span>");
                    continue;
                } else if (Integer.parseInt(request.getParameter("ect_save_feedback_timer")) <= 0) {
                    errorMessages.append(
                            "<br><span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.ectSaveFeedbackTimerGreaterThan")
                                    + "</span>");
                    continue;
                }
            }

            if ("appointment_locking_timeout".equals(generalSettingsKey)) {
                if (!StringUtils.isInteger(request.getParameter("appointment_locking_timeout"))) {
                    errorMessages.append(
                            "<br><span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.appointmentLockingTimeoutMustBeInteger")
                                    + "</span>");
                    continue;
                } else if (Integer.parseInt(request.getParameter("appointment_locking_timeout"))
                        < 0) {
                    errorMessages.append(
                            "<br><span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.appointmentLockingTimeoutValueGreaterOrEqual")
                                    + "</span>");
                    continue;
                }
            } else if ("encounter_layout_refresh_timeout".equals(generalSettingsKey)) {
                if (!StringUtils.isInteger(
                        request.getParameter("encounter_layout_refresh_timeout"))) {
                    errorMessages.append(
                            "<br><span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.encounterLayoutRefreshTimeoutMustBeInteger")
                                    + "</span>");
                    continue;
                } else if (
                        Integer.parseInt(request.getParameter("encounter_layout_refresh_timeout"))
                                < -1) {
                    errorMessages.append(
                            "<br><span style=\"color: red;\">" + oscarResourcesProperties.getString(
                                    "admin.admin.oscarProperties.generalSettings.encounterLayoutRefreshTimeoutGreaterOrEqual")
                                    + "</span>");
                    continue;
                }
            }

            if (systemPreference != null) {
                if (!systemPreference.getValue().equals(newValue)) {
                    systemPreference.setUpdateDate(new Date());
                    systemPreference.setValue(newValue);
                    SystemPreferencesUtils.merge(systemPreference);
                }
            } else {
                systemPreference = new SystemPreferences();
                systemPreference.setName(generalSettingsKey);
                systemPreference.setUpdateDate(new Date());
                systemPreference.setValue(newValue);
                SystemPreferencesUtils.persist(systemPreference);
            }
        }
    }

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html:html locale="true">
    <head>
        <title>
            <bean:message key="admin.admin.oscarProperties.generalSettings.generalSettings"/>
        </title>
        <script src="<%= request.getContextPath() %>/JavaScriptServlet"
                type="text/javascript"></script>
        <link href="<%= request.getContextPath() %>/css/bootstrap.css" rel="stylesheet"
              type="text/css">
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript"
                src="<%= request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript"
                src="<%= request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript"
                src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
    <h4><bean:message key="admin.admin.oscarProperties.generalSettings.settingsSectionTitle"/></h4>
    <form name="generalSettingsForm" method="post" action="generalSettings.jsp">
        <br>
        <%= errorMessages.toString() %>
        <br>
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable"
               class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.showAdminHamiltonPublicHealthItems"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="display_admin_hph"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("display_admin_hph", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="display_admin_hph"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("display_admin_hph", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.caseloadPageLoadAllProvidersClients"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="caseload_default_all_providers_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("caseload_default_all_providers_enabled", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="caseload_default_all_providers_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("caseload_default_all_providers_enabled", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.ckdScreeningDisabled"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="ckd_screening_disabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("ckd_screening_disabled", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="ckd_screening_disabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("ckd_screening_disabled", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableClientsNamesDropdown"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="client_dropdown_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("client_dropdown_enabled", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="client_dropdown_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("client_dropdown_enabled", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.displayReferralSource"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="display_masterfile_referral_source"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("display_masterfile_referral_source", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="display_masterfile_referral_source"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("display_masterfile_referral_source", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableNewUserPinControl"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_new_user_pin_control"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_new_user_pin_control", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_new_user_pin_control"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_new_user_pin_control", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.numberOfFlowsheetValues"/>
                </td>
                <td>
                    <input type="text" name="number_of_flowsheet_values"
                           value="<%= SystemPreferencesUtils.getPreferenceValueByName("number_of_flowsheet_values", "4") %>"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.showCommentsOnPreventionPrint"/>
                </td>
                <td>
                    <input type="radio" value="true" name="prevention_show_comments"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("prevention_show_comments", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="prevention_show_comments"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("prevention_show_comments", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enablePrintPdfReferringPractitioner"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_printpdf_referring_practitioner"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_printpdf_referring_practitioner", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_printpdf_referring_practitioner"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_printpdf_referring_practitioner", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableInformedConsentCheck"/>
                </td>
                <td>
                    <input type="radio" value="true" name="private_informed_consent_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("private_informed_consent_enabled", false)) ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="private_informed_consent_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("private_informed_consent_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.showSchedulePageMenuToSearchSpecialists"/>
                </td>
                <td>
                    <input type="radio" value="true" name="show_referral_menu"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("show_referral_menu", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="show_referral_menu"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("show_referral_menu", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.requireReferringMd"/>
                </td>
                <td>
                    <input type="radio" value="true" name="require_referring_md"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("require_referring_md", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="require_referring_md"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("require_referring_md", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableResidentReviewWorkflow"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_resident_review_workflow"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_resident_review_workflow", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_resident_review_workflow"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_resident_review_workflow", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableSaveAsXml"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_save_as_xml"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_save_as_xml", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_save_as_xml"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_save_as_xml", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.ticklerWarningDaysNumber"/>
                </td>
                <td>
                    <input type="number" min="-1" name="tickler_warn_period"
                           value="<%= SystemPreferencesUtils
                           .getPreferenceValueByName("tickler_warn_period", "1") %>"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableTicklerEdit"/>
                </td>
                <td>
                    <input type="radio" value="true" name="tickler_edit_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("tickler_edit_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="tickler_edit_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("tickler_edit_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableTicklerEmail"/>
                </td>
                <td>
                    <input type="radio" value="true" name="tickler_email_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("tickler_email_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="tickler_email_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("tickler_email_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message
                            key="admin.admin.oscarProperties.generalSettings.adminUserBillingControl"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="admin_user_billing_control"
                            <%= (SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault(
                                            "admin_user_billing_control",
                                            ADMIN_USER_BILLING_CONTROL_DEFAULT))
                                    ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="admin_user_billing_control"
                            <%= (!SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault(
                                            "admin_user_billing_control",
                                            ADMIN_USER_BILLING_CONTROL_DEFAULT))
                                    ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message
                            key="admin.admin.oscarProperties.generalSettings.allowOnlineBooking"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="allow_online_booking"
                            <%= (SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault("allow_online_booking", true))
                                    ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="allow_online_booking"
                            <%= (!SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault("allow_online_booking", true))
                                    ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message
                            key="admin.admin.oscarProperties.generalSettings.newFlowsheetEnabled"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="new_flowsheet_enabled"
                            <%= (SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault("new_flowsheet_enabled", true))
                                    ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="new_flowsheet_enabled"
                            <%= (!SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault("new_flowsheet_enabled", true))
                                    ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.useNewInbox"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="new_inbox_enabled"
                            <%= (SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault("new_inbox_enabled", true))
                                    ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="new_inbox_enabled"
                            <%= (!SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault("new_inbox_enabled", true))
                                    ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message
                            key="admin.admin.oscarProperties.generalSettings.dailyAppointmentsPageRefreshTimeout"/>:
                </td>
                <td>
                    <input type="text" name="daily_appointments_page_refresh_timeout"
                           value="<%= SystemPreferencesUtils
                           .getPreferenceValueByName("daily_appointments_page_refresh_timeout", "0") %>"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.showSexualHealthLabel"/>
                </td>
                <td>
                    <input type="radio" value="true" name="show_sexual_health_label"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("show_sexual_health_label", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="show_sexual_health_label"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("show_sexual_health_label", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.showSinglePageChartLink"/>
                </td>
                <td>
                    <input type="radio" value="true" name="show_single_page_chart"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("show_single_page_chart", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="show_single_page_chart"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("show_single_page_chart", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.skipPostalCodeValidation"/>
                </td>
                <td>
                    <input type="radio" value="true" name="skip_postal_code_validation"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("skip_postal_code_validation", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="skip_postal_code_validation"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("skip_postal_code_validation", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableSiteSelectionFeature"/>
                </td>
                <td>
                    <input type="radio" value="true" name="use_program_location_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("use_program_location_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="use_program_location_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("use_program_location_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.assignDefaultIssueToWaitlistWhenNoNotes"/>
                </td>
                <td>
                    <input type="radio" value="true" name="assign_def_issue_to_no_notes_waitlist"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("assign_def_issue_to_no_notes_waitlist", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="assign_def_issue_to_no_notes_waitlist"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("assign_def_issue_to_no_notes_waitlist", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.ectAutosaveTimer"/>
                </td>
                <td>
                    <input type="number" name="ect_autosave_timer"
                           value="<%= SystemPreferencesUtils
                           .getPreferenceValueByName("ect_autosave_timer", "0") %>"/>
                    &nbsp;
                    <bean:message key="global.seconds"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.ectSaveFeedbackTimer"/>
                </td>
                <td>
                    <input type="number" name="ect_save_feedback_timer"
                           value="<%= SystemPreferencesUtils
                           .getPreferenceValueByName("ect_save_feedback_timer", "3") %>"/>
                    &nbsp;
                    <bean:message key="global.seconds"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableRenalDosingDs"/></td>
                <td>
                    <input type="radio" value="true" name="renal_dosing_ds_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("renal_dosing_ds_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="renal_dosing_ds_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("renal_dosing_ds_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enablePrevention"/>
                </td>
                <td>
                    <input type="radio" value="true" name="prevention_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("prevention_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="prevention_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("prevention_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableImmunizationInPrevention"/>
                </td>
                <td>
                    <input type="radio" value="true" name="immunization_in_prevention_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("immunization_in_prevention_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="immunization_in_prevention_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("immunization_in_prevention_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableConsultationAppointmentInstructionsLookup"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_consultation_appt_instr_lookup"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_consultation_appt_instr_lookup", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_consultation_appt_instr_lookup"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_consultation_appt_instr_lookup", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableConsultationToAutoIncludeAllergies"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_consultation_auto_incl_allergies"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_consultation_auto_incl_allergies", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_consultation_auto_incl_allergies"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_consultation_auto_incl_allergies", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableConsultationToAutoIncludeMedications"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_consultation_auto_inc_medications"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_consultation_auto_inc_medications", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_consultation_auto_inc_medications"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_consultation_auto_inc_medications", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableConsultationDynamicLabelling"/>
                </td>
                <td>
                    <input type="radio" value="true" name="consultation_dynamic_labelling_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("consultation_dynamic_labelling_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="consultation_dynamic_labelling_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("consultation_dynamic_labelling_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableConsultationLockReferralDate"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_consultation_lock_referral_date"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_consultation_lock_referral_date", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_consultation_lock_referral_date"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_consultation_lock_referral_date", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableConsultationPatientWillBookCheckbox"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_consultation_patient_will_book"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_consultation_patient_will_book", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_consultation_patient_will_book"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_consultation_patient_will_book", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableConsultationSignatures"/>
                </td>
                <td>
                    <input type="radio" value="true" name="consultation_signature_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("consultation_signature_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="consultation_signature_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("consultation_signature_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableAlertsOnScheduleScreen"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_alerts_on_schedule_screen"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_alerts_on_schedule_screen", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_alerts_on_schedule_screen"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_alerts_on_schedule_screen", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableNotesOnScheduleScreen"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_notes_on_schedule_screen"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_notes_on_schedule_screen", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_notes_on_schedule_screen"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_notes_on_schedule_screen", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableDefaultScheduleViewall"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_default_schedule_viewall"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_default_schedule_viewall", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_default_schedule_viewall"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_default_schedule_viewall", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableDemographicPatientClinicStatus"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_demographic_patient_clinic_status"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_demographic_patient_clinic_status", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_demographic_patient_clinic_status"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_demographic_patient_clinic_status", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableDemographicPatientHealthCareTeam"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_demogr_patient_health_care_team"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_demogr_patient_health_care_team", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_demogr_patient_health_care_team"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_demogr_patient_health_care_team", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableDemographicPatientRostering"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_demographic_patient_rostering"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_demographic_patient_rostering", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_demographic_patient_rostering"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_demographic_patient_rostering", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableDemographicWaitingList"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_demographic_waiting_list"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_demographic_waiting_list", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_demographic_waiting_list"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_demographic_waiting_list", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableUrgentMessages"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_urgent_messages"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_urgent_messages", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_urgent_messages"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_urgent_messages", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableExternalNameOnDemographic"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_external_name_on_demographic"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_external_name_on_demographic", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_external_name_on_demographic"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_external_name_on_demographic", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableExternalNameOnSchedule"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_external_name_on_schedule"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_external_name_on_schedule", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_external_name_on_schedule"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_external_name_on_schedule", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableProviderScheduleNote"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_provider_schedule_note"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_provider_schedule_note", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_provider_schedule_note"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_provider_schedule_note", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableReceptionistAltView"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_receptionist_alt_view"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_receptionist_alt_view", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_receptionist_alt_view"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_receptionist_alt_view", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableEFormInAppointment"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_eform_in_appointment"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_eform_in_appointment", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_eform_in_appointment"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_eform_in_appointment", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableMeditechId"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_meditech_id"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_meditech_id", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_meditech_id"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_meditech_id", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableAppointmentMcNumber"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_appointment_mc_number"
                            <%=(SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault(
                                    "enable_appointment_mc_number", false))
                                    ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_appointment_mc_number"
                            <%=(!SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault(
                                    "enable_appointment_mc_number", false))
                                    ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableSignatureInEForm"/>
                </td>
                <td>
                    <input type="radio" value="true" name="eform_signature_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("eform_signature_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="eform_signature_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("eform_signature_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.allowMultipleSameDayGroupAppts"/>
                </td>
                <td>
                    <input type="radio" value="true" name="allow_multiple_same_day_group_appts"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("allow_multiple_same_day_group_appts", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="allow_multiple_same_day_group_appts"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("allow_multiple_same_day_group_appts", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.appointmentLockingTimeout"/>
                </td>
                <td>
                    <input type="number" name="appointment_locking_timeout"
                           value="<%= SystemPreferencesUtils
                           .getPreferenceValueByName("appointment_locking_timeout", "0") %>"/>
                    &nbsp;
                    <bean:message key="global.seconds"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableAppointmentIntakeForm"/>
                </td>
                <td>
                    <input type="radio" value="true" name="enable_appointment_intake_form"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_appointment_intake_form", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_appointment_intake_form"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_appointment_intake_form", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.displayAppointmentDaySheetButton"/>
                </td>
                <td>
                    <input type="radio" value="true" name="display_appointment_daysheet_button"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("display_appointment_daysheet_button", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="display_appointment_daysheet_button"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("display_appointment_daysheet_button", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.encounterLayoutRefreshTimeout"/>
                </td>
                <td>
                    <input type="number" name="encounter_layout_refresh_timeout"
                           value="<%= SystemPreferencesUtils
                           .getPreferenceValueByName("encounter_layout_refresh_timeout", "-1") %>"/>
                    &nbsp;
                    <bean:message key="global.seconds"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.showAppointmentReason"/>
                </td>
                <td>
                    <input type="radio" value="true" name="show_appointment_reason"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("show_appointment_reason", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="show_appointment_reason"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("show_appointment_reason", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.hideConReportLinkInTopNavigationBar"/>
                </td>
                <td>
                    <input type="radio" value="true" name="hide_con_report_link"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("hide_con_report_link", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="hide_con_report_link"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("hide_con_report_link", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.hideEConsultLinkInTopNavigationBar"/>
                </td>
                <td>
                    <input type="radio" value="true" name="hide_econsult_link"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("hide_econsult_link", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="hide_econsult_link"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("hide_econsult_link", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableMeasurementsCreateNewNote"/>
                </td>
                <td>
                    <input type="radio" value="true" name="measurements_create_new_note_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("measurements_create_new_note_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="measurements_create_new_note_enabled"
                        <%= (!SystemPreferencesUtils.
                                isReadBooleanPreferenceWithDefault("measurements_create_new_note_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.monerisUploadEnabled"/>
                </td>
                <td>
                    <input type="radio" value="true" name="moneris_upload_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("moneris_upload_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="moneris_upload_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("moneris_upload_enabled", false))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableManageContactsAndHealthCareTeamLinked"/>
                </td>
                <td>
                    <input type="radio" value="true" name="new_contacts_ui_health_care_team_linked"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("new_contacts_ui_health_care_team_linked", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="new_contacts_ui_health_care_team_linked"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("new_contacts_ui_health_care_team_linked", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.generalSettings.enableNewLabelPrint"/>
                </td>
                <td>
                    <input type="radio" value="true" name="new_label_print_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("new_label_print_enabled", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="new_label_print_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("new_label_print_enabled", true))
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            </tbody>
        </table>
        <br>
        <input type="button"
               onclick="document.forms['generalSettingsForm'].dboperation.value='Save'; document.forms['generalSettingsForm'].submit();"
               name="saveGeneralSettings" value="Save"/>
    </form>
    </body>
</html:html>
