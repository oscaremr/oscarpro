<%--
 /*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
--%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<%
    String roleName$ =
            (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    SystemPreferences kaiUsernameSystemPreference = SystemPreferencesUtils
            .findPreferenceByName("kai_username");

    boolean isSupportUser = kaiUsernameSystemPreference != null && loggedInInfo != null
            && loggedInInfo.getLoggedInSecurity() != null
            && org.apache.commons.lang.StringUtils
            .trimToEmpty(kaiUsernameSystemPreference.getValue())
            .equalsIgnoreCase(loggedInInfo.getLoggedInSecurity().getUserName());

    boolean authed = isSupportUser;
%>
<security:oscarSec roleName="<%= roleName$ %>" objectName="_admin" rights="r" reverse="<%= true %>">
    <% authed = false; %>
    <% response.sendRedirect("../securityError.jsp?type=_admin"); %>
</security:oscarSec>
<%
    if (!authed) {
        return;
    }

    StringBuilder errorMessages = new StringBuilder();
    OscarProperties oscarProps = OscarProperties.getInstance();

    if (request.getParameter("dboperation") != null
            && !request.getParameter("dboperation").isEmpty()
            && request.getParameter("dboperation").equals("Save")) {
        List<SystemPreferences> documentSettingsSystemPreferences = SystemPreferencesUtils
                .findPreferencesByNames(SystemPreferences.OSCAR_PROPERTIES_DOCUMENT_SETTINGS_KEYS);
        // Index retrieved SystemPreferences in a map for direct retrieval later
        Map<String, SystemPreferences> documentSettingsSystemPreferencesMap = new HashMap<>();
        for (SystemPreferences systemPreferenceItem : documentSettingsSystemPreferences) {
            documentSettingsSystemPreferencesMap.put(systemPreferenceItem.getName(),
                    systemPreferenceItem);
        }

        for (String documentSettingsKey : SystemPreferences.OSCAR_PROPERTIES_DOCUMENT_SETTINGS_KEYS) {
            SystemPreferences systemPreference = documentSettingsSystemPreferencesMap.get(
                    documentSettingsKey);
            String newValue = request.getParameter(documentSettingsKey);

            if (systemPreference != null) {
                if (!systemPreference.getValue().equals(newValue)) {
                    systemPreference.setUpdateDate(new Date());
                    systemPreference.setValue(newValue);
                    SystemPreferencesUtils.merge(systemPreference);
                }
            } else {
                systemPreference = new SystemPreferences();
                systemPreference.setName(documentSettingsKey);
                systemPreference.setUpdateDate(new Date());
                systemPreference.setValue(newValue);
                SystemPreferencesUtils.persist(systemPreference);
            }
        }
    }

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html:html locale="true">
    <head>
        <title>
            <bean:message key="admin.admin.oscarProperties.documentSettings.documentSettings"/>
        </title>
        <script src="<%= request.getContextPath() %>/JavaScriptServlet"
                type="text/javascript"></script>
        <link href="<%= request.getContextPath() %>/css/bootstrap.css" rel="stylesheet"
              type="text/css">
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript"
                src="<%= request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript"
                src="<%= request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript"
                src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
    <h4><bean:message key="admin.admin.oscarProperties.documentSettings.settingsSectionTitle"/></h4>
    <form name="documentSettingsForm" method="post" action="documentSettings.jsp">
        <br>
        <%= errorMessages.toString() %>
        <br>
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable"
               class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.documentSettings.enableExceptRxDocumentFilter"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="enable_except_rx_document_filter"
                        <%= SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_except_rx_document_filter", false)
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_except_rx_document_filter"
                        <%= !SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_except_rx_document_filter", false)
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.documentSettings.displayFlagAsAbnormalCheckbox"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="display_flag_as_abnormal"
                        <%= SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("display_flag_as_abnormal", false)
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="display_flag_as_abnormal"
                        <%= !SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("display_flag_as_abnormal", false)
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.documentSettings.enableUpdateOnceDocumentHasBeenAssignedToPatient"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="allow_update_document_content"
                        <%= SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("allow_update_document_content", false)
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="allow_update_document_content"
                        <%= !SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("allow_update_document_content", false)
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.documentSettings.enableMovingDeletedPageOrPdfIntoDeletedFolder"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="enable_move_delete_doc_to_deleted_folder"
                        <%= SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault(
                                    "enable_move_delete_doc_to_deleted_folder", true)
                                ? "checked" : "" %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_move_delete_doc_to_deleted_folder"
                        <%= !SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault(
                                    "enable_move_delete_doc_to_deleted_folder", true)
                                ? "checked" : "" %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            </tbody>
        </table>
        <br>
        <input type="button"
               onclick="document.forms['documentSettingsForm'].dboperation.value='Save'; document.forms['documentSettingsForm'].submit();"
               name="saveDocumentSettings" value="Save"/>
    </form>
    </body>
</html:html>
