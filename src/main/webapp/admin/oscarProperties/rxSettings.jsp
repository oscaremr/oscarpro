<%--
 /*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
--%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.HashSet" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ page import="oscar.PreferencesDefaultConstants" %>


<%
    String roleName$ =
            (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    SystemPreferences kaiUsernameSystemPreference = SystemPreferencesUtils
            .findPreferenceByName("kai_username");

    boolean isSupportUser = kaiUsernameSystemPreference != null && loggedInInfo != null
            && loggedInInfo.getLoggedInSecurity() != null
            && org.apache.commons.lang.StringUtils
            .trimToEmpty(kaiUsernameSystemPreference.getValue())
            .equalsIgnoreCase(loggedInInfo.getLoggedInSecurity().getUserName());

    boolean authed = isSupportUser;
%>
<security:oscarSec roleName="<%= roleName$ %>" objectName="_admin" rights="r" reverse="<%= true %>">
    <% authed = false; %>
    <% response.sendRedirect("../securityError.jsp?type=_admin"); %>
</security:oscarSec>
<%
    if (!authed) {
        return;
    }

    StringBuilder errorMessages = new StringBuilder();
    OscarProperties oscarProps = OscarProperties.getInstance();

    if (request.getParameter("dboperation") != null
            && !request.getParameter("dboperation").isEmpty()
            && request.getParameter("dboperation").equals("Save")) {
        List<SystemPreferences> rxSettingsSystemPreferences = SystemPreferencesUtils
                .findPreferencesByNames(SystemPreferences.OSCAR_PROPERTIES_RX_SETTINGS_KEYS);
        // Index retrieved SystemPreferences in a map for direct retrieval later
        Map<String, SystemPreferences> rxSettingsSystemPreferencesMap = new HashMap<>();
        for (SystemPreferences systemPreferenceItem : rxSettingsSystemPreferences) {
            rxSettingsSystemPreferencesMap.put(systemPreferenceItem.getName(),
                    systemPreferenceItem);
        }

        for (String rxSettingsKey : SystemPreferences.OSCAR_PROPERTIES_RX_SETTINGS_KEYS) {
            SystemPreferences systemPreference = rxSettingsSystemPreferencesMap.get(
                    rxSettingsKey);
            String newValue = request.getParameter(rxSettingsKey);

            if ("rx_footer_text".equals(rxSettingsKey)) {
                // checking for empty input to convert to null
                if (StringUtils.isEmpty(newValue)) {
                    newValue = null;
                }
            }

            if (systemPreference != null) {
                if (!systemPreference.getValue().equals(newValue)) {
                    systemPreference.setUpdateDate(new Date());
                    systemPreference.setValue(newValue);
                    SystemPreferencesUtils.merge(systemPreference);
                }
            } else {
                systemPreference = new SystemPreferences();
                systemPreference.setName(rxSettingsKey);
                systemPreference.setUpdateDate(new Date());
                systemPreference.setValue(newValue);
                SystemPreferencesUtils.persist(systemPreference);
            }
        }
    }

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html:html locale="true">
    <head>
        <title>
            <bean:message key="admin.admin.oscarProperties.rxSettings.rxSettings"/>
        </title>
        <script src="<%= request.getContextPath() %>/JavaScriptServlet"
                type="text/javascript"></script>
        <link href="<%= request.getContextPath() %>/css/bootstrap.css" rel="stylesheet"
              type="text/css">
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript"
                src="<%= request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript"
                src="<%= request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript"
                src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
    <h4><bean:message key="admin.admin.oscarProperties.rxSettings.settingsSectionTitle"/></h4>
    <form name="rxSettingsForm" method="post" action="rxSettings.jsp">
        <br>
        <%= errorMessages.toString() %>
        <br>
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable"
               class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.rxSettings.enableRxSignature"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="rx_signature_enabled"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rx_signature_enabled", false)
                                 ? "checked" : "") %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="rx_signature_enabled"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rx_signature_enabled", false)
                                ? "checked" : "") %> />
                    <bean:message key="global.no"/>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.rxSettings.showRxBandNumber"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="rx_show_band_number"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rx_show_band_number", false)
                                ? "checked" : "") %>/>
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="rx_show_band_number"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rx_show_band_number", false)
                                ? "checked" : "") %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.rxSettings.displayChartNumberOnPrescription"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="rx_show_chart_number"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rx_show_chart_number", false)
                                ? "checked" : "") %>/>
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="rx_show_chart_number"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rx_show_chart_number", false)
                                ? "checked" : "") %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.rxSettings.displayPatientHINOnRx"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="rx_show_hin"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rx_show_hin", true)
                                ? "checked" : "") %>/>
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="rx_show_hin"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rx_show_hin", true)
                                ? "checked" : "") %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.rxSettings.hideDispensingUnitsWhenPrescribingMeds"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="rx_hide_dispensing_units"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rx_hide_dispensing_units", false)
                                ? "checked" : "") %>/>
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="rx_hide_dispensing_units"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rx_hide_dispensing_units", false)
                                ? "checked" : "") %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.rxSettings.hideDrugOfChoiceButtonFromRx3Interface"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="rx_hide_drug_of_choice"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rx_hide_drug_of_choice", false)
                                ? "checked" : "") %>/>
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="rx_hide_drug_of_choice"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("rx_hide_drug_of_choice", false)
                                ? "checked" : "") %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.rxSettings.enableDrugsInternalDispensing"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="enable_rx_internal_dispensing"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_rx_internal_dispensing", false)
                                ? "checked" : "") %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_rx_internal_dispensing"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_rx_internal_dispensing", false)
                                ? "checked" : "") %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.rxSettings.addWatermarkInRxPrescription"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="enable_rx_watermark"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_rx_watermark", PreferencesDefaultConstants.DEFAULT_FALSE_IF_NOT_FOUND)
                                ? "checked" : "") %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_rx_watermark" 
                            <%= (!SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault("enable_rx_watermark", PreferencesDefaultConstants.DEFAULT_FALSE_IF_NOT_FOUND)
                                    ? "checked" : "") %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message
                            key="admin.admin.oscarProperties.rxSettings.enableRxAllergyChecking"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="enable_rx_allergy_checking" 
                            <%= (SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault("enable_rx_allergy_checking", true)
                                    ? "checked" : "") %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_rx_allergy_checking" 
                            <%= (!SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault("enable_rx_allergy_checking", true)
                                    ? "checked" : "") %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message
                            key="admin.admin.oscarProperties.rxSettings.enableCheckingFromDrugrefUsingRegionalIdentifier"/>:
                </td>
                <td>
                    <input type="radio" value="true"
                           name="enable_rx_interact_local_drugref_reg_id" 
                            <%= (SystemPreferencesUtils.isReadBooleanPreferenceWithDefault(
                                    "enable_rx_interact_local_drugref_reg_id", false)
                                    ? "checked" : "") %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false"
                           name="enable_rx_interact_local_drugref_reg_id" 
                            <%= (!SystemPreferencesUtils
                                    .isReadBooleanPreferenceWithDefault(
                                    "enable_rx_interact_local_drugref_reg_id", false)
                                    ? "checked" : "") %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.rxSettings.enableSignaturePadElectronicSigningForRx3"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="enable_signature_tablet"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_signature_tablet", false)
                                ? "checked" : "") %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_signature_tablet"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_signature_tablet", false)
                                ? "checked" : "") %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.rxSettings.rxOrderByDateNotPosition"/>:
                </td>
                <td>
                    <input type="radio" value="true" name="enable_rx_order_by_date"
                        <%= (SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_rx_order_by_date", false)
                                ? "checked" : "") %> />
                    <bean:message key="global.yes"/>
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="enable_rx_order_by_date"
                        <%= (!SystemPreferencesUtils
                                .isReadBooleanPreferenceWithDefault("enable_rx_order_by_date", false)
                                ? "checked" : "") %> />
                    <bean:message key="global.no"/>
                </td>
            </tr>
            <tr>
                <td>
                    <bean:message key="admin.admin.oscarProperties.rxSettings.rxFooterText"/>:
                </td>
                <td>
                    <textarea name="rx_footer_text" rows="4" cols="100" maxlength="1024"
                              style="overflow: auto; min-width:400px"><%=
                        Encode.forHtmlContent(SystemPreferencesUtils
                                .getPreferenceValueByName("rx_footer_text", ""))
                    %></textarea>
                </td>
            </tr>
            </tbody>
        </table>
        <br>
        <input type="button"
               onclick="document.forms['rxSettingsForm'].dboperation.value='Save'; document.forms['rxSettingsForm'].submit();"
               name="saveRxSettings" value="Save"/>
    </form>
    </body>
</html:html>
