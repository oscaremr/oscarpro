<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>

<%@ page import="oscar.oscarMDS.data.ProviderData, java.util.ArrayList, oscar.oscarLab.ForwardingRules, oscar.OscarProperties"%>
<%@ page import="org.oscarehr.common.model.Provider" %>
<%@ page import="java.util.List" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="static org.oscarehr.common.model.IncomingLabRules.FORWARDING_STATUS_KEEP" %>
<%@ page import="static org.oscarehr.common.model.IncomingLabRules.FORWARDING_STATUS_UPDATE" %>
<%@ page import="static org.oscarehr.common.model.IncomingLabRules.FORWARDING_STATUS_FILE_ONLY" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.misc" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_admin&type=_admin.misc");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
%>


<%
	ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
	
ForwardingRules fr = new ForwardingRules();
String providerNo = request.getParameter("providerNo");
Provider selectedProvider = null;
if (providerNo == null) {
	providerNo = "0";
} else {
	selectedProvider = providerDao.getProvider(providerNo);
}

ArrayList frwdProviders = fr.getProviders(providerNo);

String errorMessage = (String) request.getAttribute("errorMessage");

%>

<html>
<head>

<title><bean:message key="admin.admin.labFwdRules" /></title>

<script type="text/javascript">
                        
            function removeProvider(remProviderNo, providerName){
                var answer = confirm ("Are you sure you would like to stop forwarding labs to "+providerName)
                if (answer){
                    document.RULES.operation.value="remove";
                    document.RULES.remProviderNum.value = remProviderNo;
                    return true;
                }else{
                    return false;
                }
                
            }
            
            function setActionClear(){
                var answer = confirm ("Are you sure you would like to clear the forwarding rules?")
                if (answer){
                    document.RULES.operation.value="clear";
                    return true;
                }else{
                    return false;
                }
            }
            
            function confirmUpdate(){
                <%
                OscarProperties props = OscarProperties.getInstance();
                String autoFileLabs = props.getProperty("AUTO_FILE_LABS");
                
                if (providerNo.equals("0")){%>                    
                    alert("You must select a provider to set the rules for.");
                    return false;
                <%}else if(autoFileLabs != null && autoFileLabs.equalsIgnoreCase("yes")){%>
                    return confirm ("Are you sure you would like to update the forwarding rules?")
                <%}else{%>
					var forwardTypes = [];
					for (var i = 0; i < document.RULES.forward_type.length; i++) {
						if (document.RULES.forward_type[i].checked) { 
							forwardTypes.push(document.RULES.forward_type[i].value);
						}
					}
                    if (document.RULES.providerNums.value == '' && document.RULES.status[1].checked && <%= (frwdProviders.size() == 0)%>) {
						alert("You must select a provider to forward the incoming labs to if you wish to automatically file them.");
						return false;
					} else if (forwardTypes.length == 0) {
						alert("You must select at least one type of incoming labs to forward.");
						return false;
                    }else{
                        return confirm ("Are you sure you would like to update the forwarding rules?")
                    }
                <%}%>
            }

        </script>

</head>

<body>

<h3 class="span12"><bean:message key="admin.admin.labFwdRules" /></h3>

<!-- Form for setting to always send Lab results to the patient's MRP You may want to add any future global settings here -->
<form id="sendToMRPForm" action="${ctx}/admin/ForwardingRules.do" method="post" onsubmit="return false">
	<input type="hidden" name="<csrf:tokenname/>" value="<csrf:tokenvalue/>"/>
	<div class="well">
		<input type="hidden" name="operation" value="updateAlwaysSendToMRP">
		Always Send a Copy to MRP: <select name="sendToMRPSelect" id="sendToMRPSelect">
		<option value="Yes" <%=fr.getAlwaysSendToMRP() ? "selected" : ""%>>Yes</option>
		<option value="No" <%=!fr.getAlwaysSendToMRP() ? "selected" : ""%>>No</option>
		</select>
	</div>
</form>

<form id="ForwardRulesForm" name="RULES" action="${ctx}/admin/ForwardingRules.do" method="post">
<input type="hidden" name="<csrf:tokenname/>" value="<csrf:tokenvalue/>"/>

<input type="hidden" name="operation" value="update"> 
<input type="hidden" name="remProviderNum" value="">



<div class="well">


<h5>Select Provider</h5>
Please Select the provider to set forwarding rules for:

<select name="providerNo" id="provider-selection">
	<option value="0">None Selected</option>
	<% List<Provider> providers = ProviderData.getActiveDoctorsAndResidents();
    for (Provider provider : providers) { %>
	    <option value="<%= provider.getProviderNo() %>" <%= provider.getProviderNo().equals(providerNo) ? "selected" : "" %>>
            <%= Encode.forHtmlContent(provider.getFormattedName()) %>
        </option>
	<% }%>
</select> 

<i class="icon-question-sign"></i> <oscar:help keywords="lab forwarding" key="app.top1"/>
<br>
				
</div>

		
		
<div class="span12 well">	
<h5>Current Forwarding Rules</h5>
<%
String status = "N";
if(providerNo.equals("0")){
%>
	<p>No provider has been selected.</p>
<%	
}else if(!fr.isSet(providerNo)){%>
<p class="text-info">There are no forwarding rules set</p>
<%}else{
status = fr.getStatus(providerNo);
%>



<%if (frwdProviders != null && frwdProviders.size() > 0) {%>
<table class="table table-condensed table-striped" style="width:44%;">
    
 <thead>
 <tr>
 <th>Provider</th>
 <th>Incoming Status for <%=selectedProvider != null ? selectedProvider.getFormattedName() : "forwarding provider"%></th>
 <th style="width: 160px;">Update forwarding providers status</th>
 <th>Forward Types</th>
 <th>Forwarded Status</th>
 <th></th>
 </tr>
 </thead>   

<tbody>
<%for (int i=0; i < frwdProviders.size(); i++){
String forwardUpdateStatus = (String)((ArrayList) frwdProviders.get(i)).get(4);
forwardUpdateStatus = forwardUpdateStatus == null || forwardUpdateStatus.equals("K") ? "Keep status" : (forwardUpdateStatus.equals("U") ? "Update" : "Update only when filed");
%> 
<tr> 
<td><%= Encode.forHtmlContent((String) ((ArrayList) frwdProviders.get(i)).get(1)) %> <%= Encode.forHtmlContent((String) ((ArrayList) frwdProviders.get(i)).get(2)) %></td>
<td><%= status.equals("N") ? "New" : "Filed" %></td>
<td><%=forwardUpdateStatus%></td>
<td><%=Encode.forHtml((String) ((ArrayList) frwdProviders.get(i)).get(3))%></td>
<td><%=((String)((ArrayList) frwdProviders.get(i)).get(5))%></td>
<td><button type="submit" class="btn btn-small" onclick="return removeProvider('<%= Encode.forHtmlContent((String) ((ArrayList) frwdProviders.get(i)).get(0)) %>', '<%= StringEscapeUtils.escapeJavaScript((String) ((ArrayList) frwdProviders.get(i)).get(1)) %> <%= StringEscapeUtils.escapeJavaScript((String) ((ArrayList) frwdProviders.get(i)).get(2)) %>')" title="remove provider"><i class="icon-trash"></i> remove</button></td>
</tr> 

<br />
<%}%>

</table>
<%}else{%>



    <div class="alert alert-error">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>Warning!</strong> The incoming labs are not being forwarded.
    </div>
        

<%}%>
<br />
<button type="submit" class="btn btn-danger" onclick="return setActionClear()"><i class="icon-trash"></i> Clear All Forwarding Rules</button>

<%}%>

</div>
		<div class="span12 well">
			<div class="span12">
				<h5>Update Forwarding Rules</h5>
			</div>
			<div class="span2">
				Set incoming lab status for <%=selectedProvider != null ? selectedProvider.getFormattedName() : " forwarding provider"%>:<br/>
				<label><input type="radio" name="status" value="N"	<%= status.equals("F") ? "" : "checked" %>> <bean:message key="oscarMDS.search.formReportStatusNew" /></label>
				<label><input type="radio" name="status" value="F" <%= status.equals("F") ? "checked" : "" %>> Filed</label>
			</div>
			<div class="span2">
				Update <%=selectedProvider != null ? selectedProvider.getFormattedName() : " forwarding provider"%>'s lab status when forwarded to provider changes status:<br/>
				<label><input type="radio" name="updateForwardingProviderStatus" value="<%=FORWARDING_STATUS_KEEP%>" checked="checked">Keep Status</label>
				<label><input type="radio" name="updateForwardingProviderStatus" value="<%=FORWARDING_STATUS_UPDATE%>">Update Status</label>
				<label><input type="radio" name="updateForwardingProviderStatus" value="<%=FORWARDING_STATUS_FILE_ONLY%>">Update Status only when filed</label>
			</div>
			<div class="span2">
				Forward inbox types:<br/>
				<label><input type="checkbox" name="forward_type" value="HL7">Forward HL7 labs</label>
				<label><input type="checkbox" name="forward_type" value="DOC">Forward Documents</label>
				<label><input type="checkbox" name="forward_type" value="HRM">Forward HRM labs</label>
			</div>
			<div class="span3">
				Forward incoming reports to the	following providers:<br/>
				<small>(Hold 'Ctrl' to select multiple providers)</small><br/>
				
				<select multiple name="providerNums" style="height: 200px">
					<optgroup
						label="&#160&#160Doctors&#160&#160&#160&#160&#160&#160&#160&#160">
						<%
							List<Provider> providerList = ProviderData.getActiveDoctorsAndResidents();

							for (Provider provider : providerList) {
								String provNo = provider.getProviderNo();
						%> <option value="<%=provNo%>" <%=provNo.equals(providerNo)?"selected=\'selected\'":""%>><%=Encode.forHtmlContent(provider.getFormattedName())%></option> <%
						    }
					%>
					</optgroup>
				</select>
			</div>
			<div class="span2">
				Forwarded reports status:<br/>
				<label><input type="radio" name="forwardWithStatus" value="N" checked>New Status</label>
				<label><input type="radio" name="forwardWithStatus" value="F">Filed Status</label>
			</div>
			<div class="span12">
				<input type="submit" class="btn btn-primary" value="Update" onclick="return confirmUpdate()">
			</div>

		</div>

</form>

</body>

<script>
var pageTitle = $(document).attr('title');
$(document).attr('title', 'Administration Panel | Lab Forwarding Rules');

registerFormSubmit('sendToMRPForm', 'dynamic-content');
registerFormSubmit('ForwardRulesForm', 'dynamic-content');

$("#provider-selection").change(function(e) {
	e.preventDefault();
	$("#dynamic-content").load('${ctx}/admin/labforwardingrules.jsp?providerNo='+$("#provider-selection").val(), 
		function(response, status, xhr) {
	  		if (status == "error") {
		    	var msg = "Sorry but there was an error: ";
		    	$("#dynamic-content").html(msg + xhr.status + " " + xhr.statusText);
			}
		}
	);
});
$("#sendToMRPSelect").change(function(e) {
	$.ajax({
		beforeSend: function(request) {
			request.setRequestHeader('<csrf:tokenname/>', '<csrf:tokenvalue/>');
		},
		url: '${ctx}/admin/ForwardingRules.do',
		type: 'POST',
		data: {
			operation: 'updateAlwaysSendToMRP',
			sendToMRPSelect: $('#sendToMRPSelect').val()
		},
		success: function(msg) {
			alert('Updated Send To MRP Status');
		}
	});
});

<% if (errorMessage != null && !errorMessage.isEmpty()) { %>
alert('<%=StringEscapeUtils.escapeJavaScript(errorMessage)%>');
<% } %>


</script>
</html>
