<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.TriggerDao" %>
<%@ page import="java.util.List" %>
<%@ page import="org.oscarehr.common.model.Trigger" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.common.dao.OscarLogDao" %>
<%@ page import="org.apache.commons.lang3.math.NumberUtils" %>
<%@ page import="org.oscarehr.common.model.TriggerAction" %>
<%@ page import="org.oscarehr.common.model.TriggerCondition" %>
<%@ page import="org.oscarehr.common.dao.TriggerActionDao" %>
<%@ page import="org.oscarehr.common.dao.TriggerConditionDao" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Date" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="oscar.log.LogAction" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    SystemPreferences reloadPreference = SystemPreferencesUtils.findPreferenceByName("kai_username");
    if (!(reloadPreference != null && loggedInInfo != null && loggedInInfo.getLoggedInSecurity() != null &&
            StringUtils.trimToEmpty(reloadPreference.getValue()).equalsIgnoreCase(loggedInInfo.getLoggedInSecurity().getUserName()))) {
        response.sendRedirect("../logout.jsp");
    }
    oscar.OscarProperties pros = oscar.OscarProperties.getInstance();
    int triggerLimit = NumberUtils.isParsable(pros.getProperty("trigger_limit")) ? Integer.parseInt(pros.getProperty("trigger_limit")) : 10;
    TriggerDao triggerDao = SpringUtils.getBean(TriggerDao.class);
    TriggerActionDao triggerActionDao = SpringUtils.getBean(TriggerActionDao.class);
    TriggerConditionDao triggerConditionDao = SpringUtils.getBean(TriggerConditionDao.class);
    String dboperation = StringUtils.trimToEmpty(request.getParameter("dboperation"));

    if (dboperation.equals("Save")) {
        Trigger trigger = new Trigger();
        
        //If the trigger has an associated triggerId, then we need to pull it from the database and edit it. Else wise it's a new trigger.
        String triggerId = StringUtils.trimToEmpty(request.getParameter("triggerId"));
        if (StringUtils.isNotEmpty(triggerId) && NumberUtils.isParsable(triggerId)) {
            trigger = triggerDao.find(Integer.parseInt(triggerId));
        }
        
        //Saves the trigger type only if one hasn't been set already.
        String triggerType = request.getParameter("triggerType");
        if (StringUtils.isNotEmpty(triggerType) && StringUtils.isEmpty(trigger.getType())) {
            trigger.setType(triggerType);
        }

        trigger.setName(StringUtils.trimToEmpty(request.getParameter("triggerName")));        
        trigger.setLastUpdatedBy(loggedInInfo.getLoggedInProviderNo());
        trigger.setLastUpdatedDate(new Date());

        if (trigger.getId() != null) {
            triggerDao.merge(trigger);
        } else {
            trigger.setTriggerStatus("A");
            triggerDao.persist(trigger);
        }

        LogAction.addLog(loggedInInfo.getLoggedInProviderNo(), "save", "Trigger", "triggerId=" + trigger.getId(), loggedInInfo.getIp());
        
        Map<String, String[]> parameterMap = request.getParameterMap();
        List<TriggerAction> triggerActionList = trigger.getTriggerActionList();

        //Loops through any previous actions from the trigger. If any are not found, that means they were removed and must be deleted.
        for (TriggerAction triggerAction : triggerActionList) {
            if (StringUtils.isNotEmpty(request.getParameter("oldActionKeyValue_" + triggerAction.getId()))) {
                triggerAction.setKeyValue(StringUtils.trimToEmpty(request.getParameter("oldActionKeyValue_" + triggerAction.getId())));
                triggerAction.setValue(StringUtils.trimToEmpty(request.getParameter("oldActionValue_" + triggerAction.getId())));
                triggerAction.setOrderPosition(Integer.parseInt(StringUtils.trimToEmpty(request.getParameter("oldActionOrder_" + triggerAction.getId()))));
                triggerConditionDao.merge(triggerAction);
            } else {
                LogAction.addLog(loggedInInfo.getLoggedInProviderNo(), "remove", "TriggerAction", "triggerId=" + triggerAction.getTriggerId() + "; keyValue=" + triggerAction.getKeyValue()
                        + "; value=" + triggerAction.getValue(), loggedInInfo.getIp());
                triggerActionDao.removeActionById(triggerAction);
            }
        }
        
        List<TriggerCondition> triggerConditionList = trigger.getTriggerConditionList();

        //Loops through any previous conditions from the trigger. If any are not found, that means they were removed and must be deleted.
        for (TriggerCondition triggerCondition : triggerConditionList) {
            if (StringUtils.isNotEmpty(request.getParameter("oldConditionKeyValue_" + triggerCondition.getId()))) {
                triggerCondition.setComparator(StringUtils.trimToEmpty(request.getParameter("oldConditionComparator_" + triggerCondition.getId())));
                triggerCondition.setKeyValue(StringUtils.trimToEmpty(request.getParameter("oldConditionKeyValue_" + triggerCondition.getId())));
                triggerCondition.setValue(StringUtils.trimToEmpty(request.getParameter("oldConditionValue_" + triggerCondition.getId())));
                triggerCondition.setOrderPosition(Integer.parseInt(StringUtils.trimToEmpty(request.getParameter("oldConditionOrder_" + triggerCondition.getId()))));
                triggerConditionDao.merge(triggerCondition);
            } else {
                LogAction.addLog(loggedInInfo.getLoggedInProviderNo(), "remove", "TriggerCondition", "triggerId=" + triggerCondition.getTriggerId() + "; keyValue=" + triggerCondition.getKeyValue()
                        + "; comparator=" + triggerCondition.getComparator() + "; value=" + triggerCondition.getValue(), loggedInInfo.getIp());
                triggerConditionDao.removeConditionById(triggerCondition);
            }
        }
        
        //Loops through all param names in the request to find any new actions or conditions and adds them to the trigger.
        for (Map.Entry<String, String[]> parameter : parameterMap.entrySet()) {
            if (StringUtils.isNotEmpty(parameter.getKey())) {
                if (parameter.getKey().startsWith("newActionKeyValue_")) {
                    String id = parameter.getKey().split("_")[1];
                    TriggerAction triggerAction = new TriggerAction();
                    triggerAction.setTriggerId(trigger.getId());
                    triggerAction.setKeyValue(request.getParameter("newActionKeyValue_" + id));
                    triggerAction.setValue(request.getParameter("newActionValue_" + id));
                    triggerAction.setOrderPosition(Integer.parseInt(request.getParameter("newActionOrder_" + id)));
                    triggerActionDao.persist(triggerAction);
                } else if (parameter.getKey().startsWith("newConditionKeyValue_")) {
                    String id = parameter.getKey().split("_")[1];
                    TriggerCondition triggerCondition = new TriggerCondition();
                    triggerCondition.setTriggerId(trigger.getId());
                    triggerCondition.setKeyValue(request.getParameter("newConditionKeyValue_" + id));
                    triggerCondition.setComparator(request.getParameter("newConditionComparator_" + id));
                    triggerCondition.setValue(request.getParameter("newConditionValue_" + id));
                    triggerCondition.setOrderPosition(Integer.parseInt(request.getParameter("newConditionOrder_" + id)));
                    triggerConditionDao.persist(triggerCondition);
                }
            }
        }
    } else if (dboperation.equals("Remove")) {
        //Removes the trigger from the frontend by archiving it
        String triggerId = StringUtils.trimToEmpty(request.getParameter("triggerId"));
        if (StringUtils.isNotEmpty(triggerId) && NumberUtils.isParsable(triggerId)) {
            Trigger trigger = triggerDao.find(Integer.parseInt(triggerId));
            LogAction.addLog(loggedInInfo.getLoggedInProviderNo(), "archive", "Trigger", "triggerId=" + trigger.getId(), loggedInInfo.getIp());
            trigger.setTriggerStatus("D");
            triggerDao.merge(trigger);
        }
    } else if (dboperation.equals("Disable")) {
        //Disables the trigger so it wont run but keeps it on the frontend
        String triggerId = StringUtils.trimToEmpty(request.getParameter("triggerId"));
        if (StringUtils.isNotEmpty(triggerId) && NumberUtils.isParsable(triggerId)) {
            Trigger trigger = triggerDao.find(Integer.parseInt(triggerId));
            LogAction.addLog(loggedInInfo.getLoggedInProviderNo(), "disable", "Trigger", "triggerId=" + trigger.getId(), loggedInInfo.getIp());
            trigger.setTriggerStatus("I");
            triggerDao.merge(trigger);
        }
    } else if (dboperation.equals("Enable")) {
        //Enables the trigger so it will run
        String triggerId = StringUtils.trimToEmpty(request.getParameter("triggerId"));
        if (StringUtils.isNotEmpty(triggerId) && NumberUtils.isParsable(triggerId)) {
            Trigger trigger = triggerDao.find(Integer.parseInt(triggerId));
            LogAction.addLog(loggedInInfo.getLoggedInProviderNo(), "enable", "Trigger", "triggerId=" + trigger.getId(), loggedInInfo.getIp());
            trigger.setTriggerStatus("A");
            triggerDao.merge(trigger);
        }
    }

    List<Trigger> triggerList = triggerDao.findAll();
    Long totalActive = triggerDao.countAllActive();
    boolean limitReached = totalActive >= triggerLimit;
%>

<html:html locale="true">
    <head>
        <title><bean:message key="admin.admin.triggerSettings"/></title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
        <script>
            function removeTrigger (triggerId) {
                if (confirm("Are you sure you want to delete this trigger?")) {
                    self.location.href = "TriggerSettings.jsp?dboperation=Remove&triggerId=" + triggerId;
                }
            }
        </script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
    <h4><bean:message key="admin.admin.triggerSettings"/></h4>
    <div style="padding-bottom: 10px">
        <a href="<%=limitReached ? "#" : "TriggerAddEdit.jsp"%>" class="btn btn-primary" <%=limitReached ? "disabled" : ""%>>New</a>
        <a href="TriggerLogs.jsp" class="btn btn-primary">Logs</a>
        <a href="TriggerListAddEdit.jsp" class="btn btn-primary">Lists</a>
    </div>
    <div style="padding-bottom: 10px">
        <%="(" + totalActive + "/" + triggerLimit + ")"%>
    </div>
    <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
        <tbody>
        <tr>
            <th>Trigger</th>
            <th>Conditions</th>
            <th>Actions</th>
            <th></th>
        </tr>
        <%
            for (Trigger trigger : triggerList) {
        %>
        <tr <%=trigger.getTriggerStatus().equals("A") ? "class='success'" : "class='error'"%>>
            <td>
                <%=trigger.getInternalId() != null ? "<b>" + Encode.forHtml(trigger.getName()) + "</b>" : Encode.forHtml(trigger.getName())%>
            </td>
            <td>
                <%="Total: " + trigger.getTriggerConditionList().size()%>
                <ul>
                    <%
                        for (TriggerCondition triggerCondition : trigger.getTriggerConditionList()) {
                    %>
                        <li><%=triggerCondition.toString()%></li>
                    <%
                        }
                    %>
                </ul>
            </td>
            <td>
                <%="Total: " + trigger.getTriggerActionList().size()%>
                <ul>
                    <%
                        for (TriggerAction triggerAction : trigger.getTriggerActionList()) {
                    %>
                    <li><%=triggerAction.toString()%></li>
                    <%
                        }
                    %>
                </ul>
            </td>
            <td>
                <a href="TriggerAddEdit.jsp?dboperation=Edit&triggerId=<%=trigger.getId()%>" class="btn btn-primary" <%=StringUtils.isNotEmpty(trigger.getInternalId()) ? "onclick='return false;' disabled" : ""%>>Edit</a>
                <button onclick="removeTrigger('<%=trigger.getId()%>')" class="btn btn-primary" <%=StringUtils.isNotEmpty(trigger.getInternalId()) ? "disabled" : ""%>>Remove</button>
                <%
                    if (trigger.getTriggerStatus().equals("A")) {
                %>
                    <a href="TriggerSettings.jsp?dboperation=Disable&triggerId=<%=trigger.getId()%>" class="btn btn-primary">Disable</a>
                <%
                } else {
                %>
                    <a href="<%=limitReached ? "#" : "TriggerSettings.jsp?dboperation=Enable&triggerId=" + trigger.getId()%>" class="btn btn-primary" <%=limitReached ? "disabled" : ""%>>Enable</a>
                <%
                    }
                %>
            </td>
        </tr>
        <%
            }
        %>
        </tbody>
    </table>
    </body>
</html:html>