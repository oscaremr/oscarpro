<%--

  Copyright (c) 2021 WELL EMR Group Inc.
  This software is made available under the terms of the
  GNU General Public License, Version 2, 1991 (GPLv2).
  License details are available via "gnu.org/licenses/gpl-2.0.html".

--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
  String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
  boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
  <%authed=false; %>
  <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
  if (!authed) {
    return;
  }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.ReferringMdRequiredCodesDao" %>
<%@ page import="org.oscarehr.common.model.ReferringMdRequiredCodes" %>
<%@ page import="oscar.util.StringUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
  ReferringMdRequiredCodesDao mdRequiredCodeDao = SpringUtils.getBean(ReferringMdRequiredCodesDao.class);
  ReferringMdRequiredCodes mdRequiredCodes = new ReferringMdRequiredCodes();
  String codeName = request.getParameter("codeName"); 
  if (request.getParameter("save") != null && request.getParameter("codeName") != null && request.getParameter("save").equals("Save")) {
    mdRequiredCodes.setCode(request.getParameter("codeName"));
    mdRequiredCodeDao.saveEntity(mdRequiredCodes);
  } else {
    if (request.getParameter("edit") != null  ) {
      mdRequiredCodes=mdRequiredCodeDao.findMdRequiredCodeByCode(request.getParameter("edit"));
    } else {
      if (request.getParameter("codeName") != null && request.getParameter("update") != null) {
        mdRequiredCodes = mdRequiredCodeDao.find(Integer.valueOf(request.getParameter("id")));
        mdRequiredCodes.setCode(request.getParameter("codeName"));
        mdRequiredCodeDao.saveEntity(mdRequiredCodes);

      } else {
        if (request.getParameter("delete") != null) {
          mdRequiredCodes = mdRequiredCodeDao.findMdRequiredCodeByCode(request.getParameter("delete"));
          mdRequiredCodeDao.remove(mdRequiredCodes.getId());
        }
      }
    }
  }
%>

<html:html locale="true">
  <head>
    <title>Require Referral No</title>
      <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
      <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">
      <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
      <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
      <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
      <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
      <script>
        function hasScrollbar(element_id) {
          var elem = document.getElementById(element_id);
          if (elem.clientHeight < elem.scrollHeight) {
            document.getElementById("warning_text").style.visibility="visible";
          } else {
            document.getElementById("warning_text").style.visibility="hidden";
          }
        }
      </script>

  </head>

  <body vlink="#0000FF" class="BodyStyle">
  <div id="warning_text" style="color: #e26e6e; visibility: hidden;"></div>
  <h4>Require Referral No</h4>
  <form  method="post" action="requireReferralNo.jsp">  
	
    <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
      <tbody>
        <tr>
          <td style="text-align: center; vertical-align: middle;" colspan="3">
            <% if (request.getParameter("edit")!=null  ) { %>
              <input id="codeName" name="codeName"  value="<%=mdRequiredCodes.getCode()%>"></input>
              <input type="submit" name="update" value="Update" />
              <input type="hidden"  name="id" value="<%=mdRequiredCodes.getId()%>" ></input> 
            <% } else { %>	
              <input id="codeName" name="codeName"  value=""></input>
              <input type="submit" name="save" value="Save" />
            <% } %>				  
          </td>
        </tr>
        <tr>
          <th style="text-align: center; vertical-align: middle;">Code</th>
          <th style="text-align: center; vertical-align: middle;">Edit</th>
          <th style="text-align: center; vertical-align: middle;">Delete</th>
        </tr>

        <% for (ReferringMdRequiredCodes mdRequiredCode : mdRequiredCodeDao.getAllMdRequiredCodes()) { %>
          <tr>
            <td style="text-align: center; vertical-align: middle;">
              <%=mdRequiredCode.getCode()%>
            </td>
            <td style="text-align: center; vertical-align: middle;" >
              <input type="submit" name="edit" value="<%=mdRequiredCode.getCode()%>" />	
            </td>
            <td style="text-align: center; vertical-align: middle;" >
              <input type ="submit" name="delete"  value="<%=mdRequiredCode.getCode()%>" />
            </td>
          </tr>
        <% } %>
      </tbody>
    </table>
  </form>
  </body>
</html:html>