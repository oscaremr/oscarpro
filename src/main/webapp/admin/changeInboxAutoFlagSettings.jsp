<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>

<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${pageContext.request.contextPath}" />

<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.misc" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_admin&type=_admin.misc");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}

	boolean autoFlagAlwaysToMrpOnDocuments = SystemPreferencesUtils.isReadBooleanPreferenceWithDefault(SystemPreferences.AUTO_FLAG_ALWAYS_TO_MRP_ON_DOCUMENTS, true);

%>

<html>
<head>
<title><bean:message key="admin.admin.changeInboxAutoFlagSettings"/></title>
</head>
<body>

<h3 class="span12"><bean:message key="admin.admin.changeInboxAutoFlagSettings" /></h3>

<form id="autoFlagAlwaysToMrpOnDocumentsForm" action="${ctx}/admin/SystemPreferences.do" method="post" onsubmit="return false">
	<input type="hidden" name="<csrf:tokenname/>" value="<csrf:tokenvalue/>"/>
	<input type="hidden" name="systemPreferenceName" value="<%=SystemPreferences.AUTO_FLAG_ALWAYS_TO_MRP_ON_DOCUMENTS%>"/>
	<div class="well">
		<div style="display: inline-flex;">
			<bean:message key="admin.admin.changeInboxAutoFlagAlwaysToMrpOnDocuments"/>:&nbsp;
			<label><input type="radio" style="margin: 0;" name="systemPreferenceNewValue" value="true" <%=autoFlagAlwaysToMrpOnDocuments ? "checked=\"checked\"" : ""%>/>Enabled</label>&nbsp;&nbsp;
			<label><input type="radio" style="margin: 0;" name="systemPreferenceNewValue" value="false" <%=autoFlagAlwaysToMrpOnDocuments ? "" : "checked=\"checked\""%>/>Disabled</label>
		</div>
		<br/>
		<input type="submit" class="btn btn-primary" value="Update"/>
	</div>
</form>
</body>
<script>
var pageTitle = $(document).attr('title');
$(document).attr('title', 'Administration Panel | <bean:message key="admin.admin.changeInboxAutoFlagSettings"/>');
registerFormSubmit('autoFlagAlwaysToMrpOnDocumentsForm', 'dynamic-content');

</script>
</html>
