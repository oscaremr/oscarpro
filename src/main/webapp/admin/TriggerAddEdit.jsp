<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.TriggerDao" %>
<%@ page import="org.oscarehr.common.model.Trigger" %>
<%@ page import="org.apache.commons.lang3.math.NumberUtils" %>
<%@ page import="org.oscarehr.common.model.TriggerCondition" %>
<%@ page import="java.util.List" %>
<%@ page import="org.oscarehr.common.model.TriggerAction" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="org.oscarehr.common.model.TriggerType" %>
<%@ page import="org.oscarehr.common.model.TriggerConditionType" %>
<%@ page import="org.oscarehr.common.model.TriggerValueType" %>
<%@ page import="org.oscarehr.common.model.TriggerComparator" %>
<%@ page import="org.oscarehr.common.model.TriggerActionType" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.Comparator" %>
<%@ page import="org.oscarehr.common.model.Provider" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="org.oscarehr.common.dao.TriggerListDao" %>
<%@ page import="org.oscarehr.common.model.TriggerList" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="net.sf.json.JSONObject" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    SystemPreferences reloadPreference = SystemPreferencesUtils.findPreferenceByName("kai_username");
    if (!(reloadPreference != null && loggedInInfo != null && loggedInInfo.getLoggedInSecurity() != null &&
            StringUtils.trimToEmpty(reloadPreference.getValue()).equalsIgnoreCase(loggedInInfo.getLoggedInSecurity().getUserName()))) {
        response.sendRedirect("../logout.jsp");
    }
    oscar.OscarProperties pros = oscar.OscarProperties.getInstance();
    int triggerConditionLimit = NumberUtils.isParsable(pros.getProperty("trigger_condition_limit")) ? Integer.parseInt(pros.getProperty("trigger_condition_limit")) : 10;
    int triggerActionLimit = NumberUtils.isParsable(pros.getProperty("trigger_action_limit")) ? Integer.parseInt(pros.getProperty("trigger_action_limit")) : 10;
    TriggerDao triggerDao = SpringUtils.getBean(TriggerDao.class);
    ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
    List<Provider> providerList = providerDao.getActiveProviders();
    TriggerListDao triggerListDao = SpringUtils.getBean(TriggerListDao.class);
    List<TriggerList> triggerListList = triggerListDao.findAll();

    String triggerId = StringUtils.trimToEmpty(request.getParameter("triggerId"));
    String triggerType = StringUtils.trimToEmpty(request.getParameter("triggerType"));
    String triggerName = StringUtils.trimToEmpty(request.getParameter("triggerName"));
    Trigger trigger = new Trigger();
    
    //If we have a triggerId, that means it's not a new trigger so we'll need to load it from the database
    if (StringUtils.isNotEmpty(triggerId) && NumberUtils.isParsable(triggerId)) {
        trigger = triggerDao.find(Integer.parseInt(triggerId));
        triggerName = StringUtils.trimToEmpty(trigger.getName());

        //If the triggerType from the request is empty, we want to use the type from the trigger. If it's not empty, that means it was recently set so we want to carry that forward.
        if (triggerType.equals("")) {
            triggerType = StringUtils.trimToEmpty(trigger.getType());
        }
    }

    TriggerType triggerTypeInfo = TriggerType.getByKey(triggerType);
    if (triggerTypeInfo == null) {
        triggerTypeInfo = TriggerType.NEW_TRIGGER;
    }

    //Creates a JSONObject to store all the information about the current trigger type, so that it can be put into the javascript as a dictionary. It stores all the actions and conditions.
    JSONObject triggerDictionary = new JSONObject();
    
    //Creates a JSONObject to store all the trigger actions, and then places them in the trigger dictionary
    JSONObject triggerActionDictionary = new JSONObject();
    for (TriggerActionType triggerActionType : triggerTypeInfo.getTriggerActionTypes()) {
        JSONObject triggerActionObject = new JSONObject();
        triggerActionObject.put("Name", triggerActionType.getDisplayName());
        triggerActionObject.put("Key", triggerActionType.getKey());
        triggerActionObject.put("Type", triggerActionType.getValueType().getKey());
        triggerActionDictionary.put(triggerActionType.getDisplayName(), triggerActionObject.toString());
    }
    triggerDictionary.put("ActionKeyValues", triggerActionDictionary.toString());
    
    //Creates a JSONObject to store all the trigger conditions, and then places them in the trigger dictionary.
    JSONObject triggerConditionDictionary = new JSONObject();
    for (TriggerConditionType triggerConditionType : triggerTypeInfo.getTriggerConditionTypes()) {
        JSONObject triggerConditionObject = new JSONObject();
        triggerConditionObject.put("Name", triggerConditionType.getDisplayName());
        triggerConditionObject.put("Key", triggerConditionType.getKey());
        triggerConditionObject.put("Type", triggerConditionType.getValueType().getKey());
        
        TriggerComparator[] triggerComparatorList = triggerConditionType.getValueType().getTriggerComparator();
        JSONObject triggerComparatorObjectList = new JSONObject();
        
        //Each condition also include multiple comparators, which need to be stored as another dictionary
        for (TriggerComparator triggerComparator : triggerComparatorList) {
            JSONObject triggerComparatorObject = new JSONObject();
            triggerComparatorObject.put("Name", triggerComparator.getDisplayName());
            triggerComparatorObject.put("Key", triggerComparator.getKey());
            triggerComparatorObjectList.put(triggerComparator.getDisplayName(), triggerComparatorObject.toString());
        }
        triggerConditionObject.put("Comparators", triggerComparatorObjectList.toString());
        
        triggerConditionDictionary.put(triggerConditionType.getDisplayName(), triggerConditionObject.toString());
    }
    triggerDictionary.put("ConditionKeyValues", triggerConditionDictionary.toString());
    
    //A list of providers is also needed, as well as their associated provider numbers. This can also be stored as a dictionary.
    JSONObject providerDictionary = new JSONObject();
    int i = 0;
    for (Provider provider : providerList) {
        JSONObject providerObject = new JSONObject();
        providerObject.put("Name", provider.getFormattedName());
        providerObject.put("ProviderNo", provider.getProviderNo());
        providerDictionary.put(i, providerObject.toString());
        i++;
    }
    
    //Some actions and conditions use lists, which are loaded beforehand like everything else as a JSONObject/Dictionary
    JSONObject triggerListDictionary = new JSONObject();
    i = 0;
    for (TriggerList triggerList : triggerListList) {
        JSONObject triggerListObject = new JSONObject();
        triggerListObject.put("Name", triggerList.getName());
        triggerListObject.put("Id", triggerList.getId().toString());
        triggerListDictionary.put(i, triggerListObject);
        i++;
    }
%>

<html:html locale="true">
    <head>
        <title><bean:message key="admin.admin.triggerSettings"/></title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" media="all" href="../share/calendar/calendar.css" title="win2k-cold-1" />

        <script type="text/javascript" src="<%=request.getContextPath()%>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/Oscar.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-3.1.0.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/lang/calendar-en.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar-setup.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.maskedinput.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap.js"></script>
        <script type="text/javascript" src="TriggerAddEditScripts.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/library/bootstrap/3.0.0/css/bootstrap-glyphicons-only.min.css">
        <script type="application/javascript">
            var jQuery_3_1_0 = jQuery.noConflict(true);
        </script>
        <script>
            let newConditionRowNumber = 0;
            let newActionRowNumber = 0;
            let dict = <%=triggerDictionary.toString()%>;
            let providerDict = <%=providerDictionary.toString()%>;
            let triggerListDict = <%=triggerListDictionary.toString()%>;
            let triggerConditionLimit = <%=triggerConditionLimit%>;
            let triggerActionLimit = <%=triggerActionLimit%>;
        </script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
    <h4><bean:message key="admin.admin.triggerSettings"/></h4>
    <form name="TriggerForm" method="post" action="TriggerSettings.jsp">
        <input type="hidden" name="dboperation" value=""/>
        <input type="hidden" name="triggerId" value="<%=triggerId%>"/>
        <input type="hidden" name="triggerType" value="<%=triggerType%>">
        Trigger Name: <input id="triggerName" name="triggerName" type="text" maxlength=60 value="<%=Encode.forHtml(triggerName)%>"/>
        Trigger Type: <select onchange="changeTriggerType(this)" <%=StringUtils.trimToEmpty(triggerType).equals("") ? "" : "disabled"%>>
            <option disabled hidden <%=StringUtils.trimToEmpty(triggerType).equals("") ? "selected" : ""%> value="">Please select a type...</option>
        <%
            for (TriggerType triggerTypeItem : TriggerType.values()) {
                if (triggerTypeItem != TriggerType.NEW_TRIGGER) {
        %>
                    <option <%=StringUtils.trimToEmpty(triggerType).equals(triggerTypeItem.getKey()) ? "selected" : ""%> value="<%=triggerTypeItem.getKey()%>"><%=triggerTypeItem.getDisplayName()%></option>
        <%
                }
            }
        %>
        </select>
        <table id="conditionTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <th>Key Value</th>
                <th>Condition</th>
                <th>Value</th>
                <th></th>
            </tr>
            <%
                List<TriggerConditionType> triggerConditionTypeList = Arrays.asList(triggerTypeInfo.getTriggerConditionTypes());
                Collections.sort(triggerConditionTypeList, TriggerConditionType.triggerConditionTypeComparator());
                
                i = 1;
                for (TriggerCondition triggerCondition : trigger.getTriggerConditionList()) {
            %>
            <tr id="oldCondition_<%=triggerCondition.getId()%>" name="oldCondition_<%=triggerCondition.getId()%>">
                <input type="hidden" id="oldConditionOrder_<%=triggerCondition.getId()%>" name="oldConditionOrder_<%=triggerCondition.getId()%>" value="<%=i%>"/>
                <td>
                    <select id="oldConditionKeyValue_<%=triggerCondition.getId()%>" name="oldConditionKeyValue_<%=triggerCondition.getId()%>" onchange="changeCondition('oldCondition_<%=triggerCondition.getId()%>')">
                        <%
                            for (TriggerConditionType triggerConditionType : triggerConditionTypeList) {
                        %>
                        <option <%=triggerConditionType.getKey().equals(triggerCondition.getKeyValue()) ? "selected" : ""%> value="<%=triggerConditionType.getKey()%>"><%=triggerConditionType.getDisplayName()%></option>
                        <%
                            }
                        %>
                    </select>
                </td>
                <td>
                    <select id="oldConditionComparator_<%=triggerCondition.getId()%>" name="oldConditionComparator_<%=triggerCondition.getId()%>" onfocus="operatorOnFocus('oldCondition_<%=triggerCondition.getId()%>')">
                        <%
                            TriggerValueType triggerValueType = TriggerConditionType.getByKey(triggerCondition.getKeyValue()).getValueType();
                            for (TriggerComparator triggerComparator : triggerValueType.getTriggerComparator()) {
                        %>
                                <option <%=StringUtils.trimToEmpty(triggerCondition.getComparator()).equals(triggerComparator.getKey()) ? "selected" : ""%> value="<%=triggerComparator.getKey()%>"><%=triggerComparator.getDisplayName()%></option>
                        <%
                            }
                        %>
                    </select>
                </td>
                <td>
                    <%
                        switch (triggerValueType) {
                            case STRING:
                            case STRING_LIST:
                                if (triggerCondition.getComparator().equals(TriggerComparator.IN_LIST.getKey()) || triggerCondition.getComparator().equals(TriggerComparator.NOT_IN_LIST.getKey())) {
                    %>
                                    <select id="oldConditionValue_<%=triggerCondition.getId()%>" name="oldConditionValue_<%=triggerCondition.getId()%>">
                    <%
                                        for (TriggerList triggerList : triggerListList) {
                    %>
                                            <option <%=triggerList.getName().equals(triggerCondition.getValue()) ? "selected" : ""%> value="<%=triggerList.getId()%>"><%=triggerList.getName()%></option>
                    <%
                                        }
                    %>
                                    </select>
                    <%
                                } else {
                    %>
                                    <input id="oldConditionValue_<%=triggerCondition.getId()%>" name="oldConditionValue_<%=triggerCondition.getId()%>" type="text" maxlength=60 value="<%=Encode.forHtmlAttribute(StringUtils.trimToEmpty(triggerCondition.getValue()))%>"/>
                    <%
                                }
                                break;
                            case KEY_VALUE:
                    %>
                                <select id="oldConditionValue_<%=triggerCondition.getId()%>" name="oldConditionValue_<%=triggerCondition.getId()%>">
                                    <%
                                        for (TriggerConditionType triggerConditionType : triggerConditionTypeList) {
                                    %>
                                    <option <%=triggerConditionType.getKey().equals(triggerCondition.getValue()) ? "selected" : ""%> value="<%=triggerConditionType.getKey()%>"><%=Encode.forHtmlContent(triggerConditionType.getDisplayName())%></option>
                                    <%
                                        }
                                    %>
                                </select>
                    <%
                                break;
                            case NUMBER:
                    %>
                                <input id="oldConditionValue_<%=triggerCondition.getId()%>" name="oldConditionValue_<%=triggerCondition.getId()%>" type="number" value="<%=Encode.forHtmlAttribute(StringUtils.trimToEmpty(triggerCondition.getValue()))%>"/>
                    <%
                                break;
                            case PROVIDER:
                    %>
                                <select id="oldConditionValue_<%=triggerCondition.getId()%>" name="oldConditionValue_<%=triggerCondition.getId()%>">
                                    <%
                                        for (Provider provider : providerList) {
                                    %>
                                            <option <%=provider.getProviderNo().equals(triggerCondition.getValue()) ? "selected" : ""%> value="<%=Encode.forHtmlAttribute(provider.getProviderNo())%>"><%=Encode.forHtmlContent(provider.getFormattedName())%></option>
                                    <%
                                        }
                                    %>
                                </select>
                    <%
                                break;
                            case BOOLEAN:
                    %>
                                <select id="oldConditionValue_<%=triggerCondition.getId()%>" name="oldConditionValue_<%=triggerCondition.getId()%>">
                                    <option>True</option>
                                    <option>False</option>
                                </select>
                    <%
                                break;
                            case DATE:
                    %>
                                <input type="text" maxlength=60 name="oldConditionValue_<%=triggerCondition.getId()%>" id="oldConditionValue_<%=triggerCondition.getId()%>" size="11" value="<%=triggerCondition.getValue()%>">
                                <img src="../images/cal.gif" id="oldConditionValue_<%=triggerCondition.getId()%>_cal">
                                <script type="application/javascript">createStandardDatepicker(jQuery_3_1_0('#oldConditionValue_<%=triggerCondition.getId()%>'), "oldConditionValue_<%=triggerCondition.getId()%>_cal");</script>
                    <%
                                break;
                            case NO_VALUE:
                    %>
                                <input type="text" disabled/>
                    <%
                                break;
                        }
                    %>
                </td>
                <td>
                    <button type="button" onclick="reorderRow('oldCondition_<%=triggerCondition.getId()%>', 'up')"><span class="glyphicon glyphicon-chevron-up"></span></button>
                    <button type="button" onclick="reorderRow('oldCondition_<%=triggerCondition.getId()%>', 'down')"><span class="glyphicon glyphicon-chevron-down"></span></button>
                    <button type="button" class="btn btn-danger" onclick="removeCondition('oldCondition_<%=triggerCondition.getId()%>')"><span class="glyphicon glyphicon-remove"></span></button>
                </td>
            </tr>
            <%
                    i++;
                }
            %>
            </tbody>
        </table>
        <input id="addConditionButton" <%=StringUtils.trimToEmpty(triggerType).equals("") || trigger.getTriggerConditionList().size() >= triggerConditionLimit ? "disabled" : ""%> type="button" class="btn btn-primary" onclick="addCondition()" name="addConditionButton" value="Add Condition"/><div id="triggerConditionLimitDiv"><%="(" + trigger.getTriggerConditionList().size() + "/" + triggerConditionLimit + ")"%></div>
        <table id="actionTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <th>Action</th>
                <th>Value</th>
                <th></th>
            </tr>
            <%
                i = 1;
                for (TriggerAction triggerAction : trigger.getTriggerActionList()) {
                    int id = triggerAction.getId();
            %>
            <tr id="oldAction_<%=triggerAction.getId()%>" name="oldAction_<%=triggerAction.getId()%>">
                <input type="hidden" id="oldActionOrder_<%=triggerAction.getId()%>" name="oldActionOrder_<%=triggerAction.getId()%>" value="<%=i%>"/>
                <td>
                    <select id="oldActionKeyValue_<%=id%>" name="oldActionKeyValue_<%=id%>" onchange="changeAction(<%=i%>)" <%=StringUtils.trimToEmpty(triggerType).equals("") ? "disabled" : ""%>>
                        <%
                            List<TriggerActionType> triggerActionTypeList = Arrays.asList(triggerTypeInfo.getTriggerActionTypes());
                            Collections.sort(triggerActionTypeList, TriggerActionType.triggerActionTypeComparator());
                            for (TriggerActionType triggerActionType : triggerActionTypeList) {
                        %>
                                <option <%=StringUtils.trimToEmpty(triggerAction.getKeyValue()).equals(triggerActionType.getKey()) ? "selected" : ""%> value="<%=triggerActionType.getKey()%>"><%=triggerActionType.getDisplayName()%></option>
                        <%
                            }
                        %>
                    </select>
                </td>
                <td>
                    <%
                        TriggerValueType triggerValueType = triggerActionTypeList.get(0).getValueType();
                        if (!"".equals(triggerAction.getKeyValue())) {
                            triggerValueType = TriggerActionType.getByKey(triggerAction.getKeyValue()).getValueType();
                        }
                        switch (triggerValueType) {
                            case STRING:
                    %>
                    <input id="oldActionValue_<%=id%>" name="oldActionValue_<%=id%>" type="text" maxlength=60 value="<%=Encode.forHtmlAttribute(StringUtils.trimToEmpty(triggerAction.getValue()))%>"/>
                    <%
                            break;
                        case KEY_VALUE:
                    %>
                    <select id="oldActionValue_<%=id%>" name="oldActionValue_<%=id%>">
                        <%
                            for (TriggerConditionType triggerConditionType : triggerConditionTypeList) {
                        %>
                        <option <%=triggerConditionType.getKey().equals(triggerAction.getValue()) ? "selected" : ""%> value="<%=Encode.forHtmlContent(triggerConditionType.getKey())%>"><%=Encode.forHtmlContent(triggerConditionType.getDisplayName())%></option>
                        <%
                            }
                        %>
                    </select>
                    <%
                            break;
                        case NUMBER:
                    %>
                    <input id="oldActionValue_<%=id%>" name="oldActionValue_<%=id%>" type="number" value="<%=Encode.forHtmlAttribute(StringUtils.trimToEmpty(triggerAction.getValue()))%>"/>
                    <%
                            break;
                        case PROVIDER:
                    %>
                    <select id="oldActionValue_<%=id%>" name="oldActionValue_<%=id%>">
                        <%
                            for (Provider provider : providerList) {
                        %>
                        <option <%=provider.getProviderNo().equals(triggerAction.getValue()) ? "selected" : ""%> value="<%=Encode.forHtmlAttribute(provider.getProviderNo())%>"><%=Encode.forHtmlContent(provider.getFormattedName())%></option>
                        <%
                            }
                        %>
                    </select>
                    <%
                            break;
                        case BOOLEAN:
                    %>
                    <select id="oldActionValue_<%=id%>" name="oldActionValue_<%=id%>">
                        <option>True</option>
                        <option>False</option>
                    </select>
                    <%
                            break;
                        case DATE:
                    %>
                    <input type="text" maxlength=60 name="oldActionValue_<%=id%>" id="oldActionValue_<%=id%>" size="11" value="<%=triggerAction.getValue()%>">
                    <img src="../images/cal.gif" id="triggerActionValue_<%=id%>_cal">
                    <script type="application/javascript">createStandardDatepicker(jQuery_3_1_0('#oldActionValue_<%=id%>'), "oldActionValue_<%=id%>_cal");</script>
                    <%
                            break;
                        case NO_VALUE:
                    %>
                    <input type="text" disabled/>
                    <%
                            break;
                        }
                    %>
                </td>
                <td>
                    <button type="button" onclick="reorderRow('oldAction_<%=triggerAction.getId()%>', 'up')"><span class="glyphicon glyphicon-chevron-up"></span></button>
                    <button type="button" onclick="reorderRow('oldAction_<%=triggerAction.getId()%>', 'down')"><span class="glyphicon glyphicon-chevron-down"></span></button>
                    <button type="button" class="btn btn-danger" onclick="removeAction('oldAction_<%=triggerAction.getId()%>')"><span class="glyphicon glyphicon-remove"></span></button>
                </td>
            </tr>
            <%
                    i++;
                }
            %>
            </tbody>
        </table>
        <input id="addActionButton" <%=StringUtils.trimToEmpty(triggerType).equals("") || trigger.getTriggerActionList().size() >= triggerActionLimit ? "disabled" : ""%> type="button" class="btn btn-primary" onclick="addAction()" name="addActionButton" value="Add Action"/><div id="triggerActionLimitDiv"><%="(" + trigger.getTriggerActionList().size() + "/" + triggerActionLimit + ")"%></div>
        <div style="padding-top: 30px;">
            <a href="TriggerSettings.jsp" class="btn btn-primary">Back</a>
            <input <%=StringUtils.trimToEmpty(triggerType).equals("") ? "disabled" : ""%> id="addBtn" type="button" class="btn btn-primary" onclick="document.forms['TriggerForm'].dboperation.value='Save'; document.forms['TriggerForm'].submit();" name="saveTriggerForm" value="Save"/>
        </div>
    </form>
    </body>
</html:html>