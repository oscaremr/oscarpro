<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="java.util.Date" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    for(String key : SystemPreferences.LAB_DISPLAY_PREFERENCE_KEYS) {
        SystemPreferences preference = SystemPreferencesUtils.findPreferenceByName(key);
        if (request.getParameter("dboperation") != null && !request.getParameter("dboperation").isEmpty() && request.getParameter("dboperation").equals("Save")) {
            String newValue = request.getParameter(key);
            if (preference != null) {
                if (!preference.getValue().equals(newValue)) {
                    preference.setUpdateDate(new Date());
                    preference.setValue(newValue);
                    SystemPreferencesUtils.merge(preference);
                }
            } else {
                preference = new SystemPreferences();
                preference.setName(key);
                preference.setUpdateDate(new Date());
                preference.setValue(newValue);
                SystemPreferencesUtils.persist(preference);
            }
        }
    }
%>

<html:html locale="true">
    <head>
        <title>Mandatory Fields - Master File</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
        <script type="text/javascript">
            function save() {
                var labPdfSize = document.getElementById("lab_pdf_max_size").value;
                console.log(labPdfSize);
                if(labPdfSize.match("^(\\d+(\\.\\d+)?\\s*[kKmMgG]?[bB])?$")){
                    document.forms['labDisplaySettingsForm'].dboperation.value='Save';
                    document.forms['labDisplaySettingsForm'].submit();
                } else {
                    alert("Please enter a valid file size\n(EG. 2.4 MB)");
                }
            }
        </script>
    </head>

    <%
    for(SystemPreferences preference : SystemPreferencesUtils.findPreferencesByNames(SystemPreferences.LAB_DISPLAY_PREFERENCE_KEYS)) { 
        dataBean.setProperty(preference.getName(), preference.getValue());
    }
    %>

    <body vlink="#0000FF" class="BodyStyle">
    <h4>Manage Lab Display Settings</h4>
    <form name="labDisplaySettingsForm" method="post" action="labDisplaySettings.jsp">
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <td>Display codes on lab: </td>
                <td>
                    <input id="code_show_hide_column-true" type="radio" value="true" name="code_show_hide_column"
                            <%=(dataBean.getProperty("code_show_hide_column", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="code_show_hide_column-false" type="radio" value="false" name="code_show_hide_column"
                            <%=(dataBean.getProperty("code_show_hide_column", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Embed PDFs on lab report: </td>
                <td>
                    <input id="lab_embed_pdf-true" type="radio" value="true" name="lab_embed_pdf"
                            <%=(dataBean.getProperty("lab_embed_pdf", "true").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="lab_embed_pdf-false" type="radio" value="false" name="lab_embed_pdf"
                            <%=(dataBean.getProperty("lab_embed_pdf", "true").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Display Discipline as Label in Inbox for Labs: </td>
                <td>
                    <input id="display_discipline_as_label_in_inbox-true" type="radio" value="true" name="display_discipline_as_label_in_inbox"
                            <%=(dataBean.getProperty("display_discipline_as_label_in_inbox", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="display_discipline_as_label_in_inbox-false" type="radio" value="false" name="display_discipline_as_label_in_inbox"
                            <%=(dataBean.getProperty("display_discipline_as_label_in_inbox", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Character Limit for Discipline/Label in Inbox: </td>
                <td>
                    <input type="number" min="3" max="100" id="discipline_character_limit_in_inbox" name="discipline_character_limit_in_inbox" value="<%= Encode.forHtmlAttribute(dataBean.getProperty("discipline_character_limit_in_inbox", "13"))%>"/>
                </td>
            </tr>
            <tr>
                <td>Maximum file size allowed for lab PDFs to display (default: 3MB):</td>
                <td>
                    <input type="text" id="lab_pdf_max_size" name="lab_pdf_max_size" value="<%= Encode.forHtmlAttribute(dataBean.getProperty("lab_pdf_max_size", ""))%>"/>
                </td>
            </tr>
            <tr>
                <td>Apply label to a version even if the type of labs contained differ: </td>
                <td>
                    <input id="sticky_label_different_labs-true" type="radio" value="true" name="sticky_label_different_labs"
                            <%=(dataBean.getProperty("sticky_label_different_labs", "true").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="sticky_label_different_labs-false" type="radio" value="false" name="sticky_label_different_labs"
                            <%=(dataBean.getProperty("sticky_label_different_labs", "true").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Display Perinatal Record Quicklinks: </td>
                <td>
                    <input id="show_obgyn_shortcuts-true" type="radio" value="true" name="show_obgyn_shortcuts"
                            <%=(dataBean.getProperty("show_obgyn_shortcuts", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="show_obgyn_shortcuts-false" type="radio" value="false" name="show_obgyn_shortcuts"
                            <%=(dataBean.getProperty("show_obgyn_shortcuts", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>By Default, display historical HL7 lab values matched by: </td>
                <td>
                    <input id="historic_lab_value_match_test_name-true" type="radio" value="true" name="historic_lab_value_match_test_name"
                            <%=(dataBean.getProperty("historic_lab_value_match_test_name", "true").equals("true")) ? "checked" : ""%> />
                    LOINC Code and Test Name
                    &nbsp;&nbsp;&nbsp;
                    <input id="historic_lab_value_match_test_name-false" type="radio" value="false" name="historic_lab_value_match_test_name"
                            <%=(dataBean.getProperty("historic_lab_value_match_test_name", "true").equals("false")) ? "checked" : ""%> />
                    Only LOINC Code
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Display Accession Number on Main Inbox View (PRO only): </td>
                <td>
                    <input id="showAccessionNumberColumnTrue" type="radio" value="true" name="showAccessionNumberColumn"
                            <%=(dataBean.getProperty("showAccessionNumberColumn", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="showAccessionNumberColumnFalse" type="radio" value="false" name="showAccessionNumberColumn"
                            <%=(dataBean.getProperty("showAccessionNumberColumn", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Display historical HL7 lab value results based on: </td>
                <td>
                    <input id="display_lab_value_based_on-true" type="radio" value="true" name="display_lab_value_based_on"
                            <%=(dataBean.getProperty("display_lab_value_based_on", "true").equals("true")) ? "checked" : ""%> />
                    LOINC Code
                    &nbsp;&nbsp;&nbsp;
                    <input id="display_lab_value_based_on-false" type="radio" value="false" name="display_lab_value_based_on"
                            <%=(dataBean.getProperty("display_lab_value_based_on", "true").equals("false")) ? "checked" : ""%> />
                    Lab Identifier
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            </tbody>
        </table>

        <input type="button" onclick="save();" name="saveLabDisplaySettings" value="Save"/>
    </form>
    </body>
</html:html>