<%--
   Copyright (c) 2022 WELL EMR Group Inc.
   This software is made available under the terms of the
   GNU General Public License, Version 2, 1991 (GPLv2).
   License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>
<%@ page import="org.oscarehr.common.dao.SecurityDao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.Security" %>
<%@ page import="java.util.List" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%
    String roleName$ = (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean isSiteAccessPrivacy = false;
    boolean authed = true;
%>
<security:oscarSec roleName="<%= roleName$ %>" objectName="_admin,_admin.userAdmin" rights="*" reverse="<%= true %>">
    <% authed = false; %>
    <% response.sendRedirect("../securityError.jsp?type=_admin&type=_admin.userAdmin"); %>
</security:oscarSec>
<%
    if (!authed) {
        return;
    }
%>
<security:oscarSec objectName="_site_access_privacy" roleName="<%=roleName$%>" rights="r" reverse="false">
    <% isSiteAccessPrivacy = true; %>
</security:oscarSec>
<%
    SecurityDao securityDao = SpringUtils.getBean(SecurityDao.class);
    List<Security> securityRecords = securityDao.findAllOrderBy("user_name");
    String username = request.getParameter("username");
    String errorMessage = (String) request.getAttribute("errorMessage");
%>
<html>
    <head>
        <title><bean:message key="admin.admin.SecurityRecordIntegration"/></title>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/checkPassword.js.jsp"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h3>Select Username of Security Record to Set Integration Password and Pin for:</h3>
        <html:form action="/admin/SetIntegrationPassword" method="post" onsubmit="return onSubmit(); return false;">
            <div id="user-input">
                <label for="username">Username:</label>
                <select name="username" id="username">
                    <option value="-1">Select Username</option>
                    <% for (Security securityRecord : securityRecords) { %>
                    <option value="<%= securityRecord.getUserName() %>"><%= securityRecord.getUserName() %></option>
                    <% } %>
                </select>

                <label for="password">Password:</label>
                <input type="password" name="password" id="password" maxlength="32" autocomplete="off">

                <label for="confirm-password">Confirm Password:</label>
                <input type="password" name="confirmPassword" id="confirm-password" maxlength="32" autocomplete="off">

                <label for="pin">Pin:</label>
                <input type="password" name="pin" id="pin" size="6" maxlength="6">

                <label for="confirm-pin">Confirm Pin:</label>
                <input type="password" name="confirmPin" id="confirm-pin">
            </div>
            <% if (errorMessage != null) { %>
                <div><%= errorMessage %></div>
            <% } else if (username != null && !username.isEmpty()) { %>
                <div>Integration password and pin of user <%= username %> successfully updated!</div>
            <% } %>
            <button class="btn btn-primary" type="submit" name="submitButton">Update Record</button>
        </html:form>

        <script>
            let userNameSelect = document.getElementById('username')
            let password = document.getElementById('password');
            let passwordCheck = document.getElementById('confirm-password');
            let pin = document.getElementById('pin');
            let pinCheck = document.getElementById('confirm-pin');

            function onSubmit() {
                let passwordValue = password.value.trim();
                let passwordCheckValue = passwordCheck.value.trim();
                let pinValue = pin.value.trim();
                let pinCheckValue = pinCheck.value.trim();

                if (userNameSelect.value === '-1'){
                    alert('Please choose a valid username');
                    return false;
                }
                if (passwordValue === '') {
                    alert('<bean:message key="admin.securityrecord.formPassword" /> <bean:message key="admin.securityrecord.msgIsRequired"/>');
                    return false;
                }
                if (!validatePasswordWithoutOkta(passwordValue)) {
                    return false;
                }
                if (passwordValue !== passwordCheckValue) {
                    alert('<bean:message key="admin.securityrecord.msgPasswordNotConfirmed" />');
                    return false;
                }
                if (!validatePin(pinValue)) {
                    return false;
                }
                if (pinValue !== pinCheckValue) {
                    alert('<bean:message key="admin.securityrecord.msgPinNotConfirmed" />');
                    return false;
                }
                return true;
            }
        </script>
    </body>
</html>
