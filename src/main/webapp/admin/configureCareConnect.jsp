<%--
    Copyright (c) 2021 WELL EMR Group Inc.
    This software is made available under the terms of the
    GNU General Public License, Version 2, 1991 (GPLv2).
    License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>
<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="w" reverse="true">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>
<%@ page import="java.util.*"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.util.CareConnectUtils" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%
    Map<String, SystemPreferences> preferenceMap = SystemPreferencesUtils.findByKeysAsPreferenceMap(SystemPreferences.INTEGRATION_PREFERENCE_KEYS);

    if (request.getParameter("dboperation") != null && !request.getParameter("dboperation").isEmpty() && request.getParameter("dboperation").equals("Save")) {
        for (String key : SystemPreferences.INTEGRATION_PREFERENCE_KEYS) {
            SystemPreferences preference = preferenceMap.get(key);
            String newValue = request.getParameter(key);
            // Do not Save the entry if the OSCAR Properties value is set
            if (!(key.equals("care_connect_url") && (newValue == null || newValue.equals("CUSTOM")))) {
                if (preference != null) {
                    if (!preference.getValue().equals(newValue)) {
                        preference.setUpdateDate(new Date());
                        preference.setValue(newValue);
                        SystemPreferencesUtils.merge(preference);
                    }
                } else {
                    preference = new SystemPreferences();
                    preference.setName(key);
                    preference.setUpdateDate(new Date());
                    preference.setValue(newValue);
                    SystemPreferencesUtils.persist(preference);
                }

                // Updates the preference in the preference map
                preferenceMap.put(key, preference);
            }
        }
    }
    
    String careConnectCode, careConnectURL;
    String[] effectiveCareConnectURL = CareConnectUtils.getEffectiveCareConnectURL();
    if (effectiveCareConnectURL != null && effectiveCareConnectURL.length == 2 && !StringUtils.isBlank(effectiveCareConnectURL[0]) && !StringUtils.isBlank(effectiveCareConnectURL[1])) {
        careConnectCode = effectiveCareConnectURL[0];
        careConnectURL = effectiveCareConnectURL[1];
    } else {
        careConnectCode = "DISABLED";
        careConnectURL = "";
    }
    String careConnectOrgs = CareConnectUtils.getEffectiveCareConnectOrgs();
    if (StringUtils.isBlank(careConnectOrgs)) {
        careConnectOrgs = "oscr,fha,iha,nha,phsa,vcha,viha";
    }
%>

<html:html locale="true">
    <head>
        <title><bean:message key="admin.careConnect.title"/></title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/library/bootstrap/3.0.0/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <!--Body content-->
                    <h3><bean:message key="admin.careConnect.title"/></h3>
                </div>
                <div class="col-sm-12">
                    <form class="well" name="configureCareConnect" method="post" action="configureCareConnect.jsp">
                        <input type="hidden" name="dboperation" value="">
    
                        <div class="alert alert-info">
                            The following page is intended for clinics already registered for CareConnect to configure their connection of the CareConnect EMR Rapid Access Button.
                            <br/>
                            If you have not registered for CareConnect and would like to learn more, please visit <a href="https://apps.health/featured/careconnect/">https://apps.health/featured/careconnect/</a>.
                        </div>
                        
                        <div class="alert alert-warning">
                            <b>NOTE:</b> Please do not change these options unless instructed to do so by PHSA, or OSCAR Pro Support.
                        </div>
    
                        <% if (careConnectCode.equals("DISABLED")) {%>
                        <div class="alert alert-danger">
                            The CareConnect Rapid Access Button is currently disabled.
                        </div>
                        <% } else { %>
                        <div class="alert alert-success">
                            OSCAR is currently configured to redirect the CareConnect Rapid Access Button to the following URL:
                            <br/>
                            <mark><%=Encode.forHtmlContent(careConnectURL)%></mark>
    
                            <% if (careConnectCode.equals("CUSTOM")) { %>
                            <br/>
                            <small>(<bean:message key="admin.careConnect.customURLAbout"/>)</small>
                            <% } %>
                        </div>
                        <% } %>
                        <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
                            <tbody>
                            <tr>
                                <td>
                                    <bean:message key="admin.careConnect.careConnectStatus"/>:
                                    <br/>
                                    <small>(<bean:message key="admin.careConnect.careConnectAbout"/>)</small>
                                </td>
                                <td style="vertical-align: middle;">
                                    <select name="care_connect_url" id="care_connect_url" <%= careConnectCode.equals("CUSTOM") ? "disabled" : ""%>>
                                        <option <%= careConnectCode.equals("DISABLED") ? "selected" : ""%> value="DISABLED">Disabled</option>
                                        <option <%= careConnectCode.equals("PPN") ? "selected" : ""%> value="PPN">Access via PPN</option>
                                        <option <%= careConnectCode.equals("NOPPN") ? "selected" : ""%> value="NOPPN">Access via Internet</option>
                                        <% if (careConnectCode.equals("CUSTOM")) { %>
                                        <option selected value="CUSTOM">Custom URL configured by OSCAR</option>
                                        <% } %>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <bean:message key="admin.careConnect.careConnectOrgs"/>:
                                    <br/>
                                    <small>(<bean:message key="admin.careConnect.careConnectOrgsAbout"/>)</small>
                                </td>
                                <td style="vertical-align: middle;">
                                    <input type="text" id="care_connect_orgs" name="care_connect_orgs" value="<%= Encode.forHtmlAttribute(careConnectOrgs)%>" size="60px" />
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <div>
                            <input type="button" class="btn btn-default" onclick="document.forms['configureCareConnect'].dboperation.value='Save'; document.forms['configureCareConnect'].submit();" name="saveDocumentSettings" value="<bean:message key="admin.resourcebaseurl.btnSave"/>"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html:html>
