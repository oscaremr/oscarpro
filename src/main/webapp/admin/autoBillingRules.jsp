<!DOCTYPE html>
<%--

    Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

--%>
<%@ page import="java.util.*,oscar.oscarReport.data.*, oscar.oscarBilling.ca.on.administration.*"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.billing" rights="w" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_admin&type=_admin.billing");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}
%>

<html ng-app="billingAutoRulesConfig">
<head>
<title>Auto Billing Rules</title>

<script type="text/javascript" src="<%=request.getContextPath() %>/library/angular.min.js"></script>
<script src="<%=request.getContextPath() %>/web/common/billingServices.js"></script>

<link href="<%=request.getContextPath() %>/css/bootstrap.min.css" rel="stylesheet">

<style>
#form-row div[class*="form-div"]{
margin-left: 0px !important;
margin-right: 0px !important;

padding-left: 10px !important;
padding-right: 10px !important;

height:310px;
}

table tr td:first-child input{width:60px;}
table tr td input{width:20px;}
table tr td:last-child input{width:40px;}

.centre{text-align:center;}

.label-state{
position:absolute;
top:0px;
left:50%;

}

.p5{padding-top:5px}
.p10{padding-top:10px}

 .noborder {
	border : 0px !important;
	margin: 0px !important;
	background: transparent !important;
	overflow: hidden;
	width: 100%;

 }

 .well-custom{ position:relative }

 .update-action{display:none};
</style>
</head>


<body>

<div class="container-fluid" ng-controller="billingAutoRulesConfig" ng-cloak>

<!-- // PAGE TITLE -->
<div class="page-header">
<h3>Auto Billing Rules</h3>
</div><!-- page-header -->

<!--  old script form TODO: replace with angular/bootstrap dropdown autocomplete -->
<form></form>


<!-- // NEW AUTO BILL RULE FORM -->
<form id="rulesForm" name="rulesForm">
<input type="hidden" name="id" value="{{populateForm.id}}"/>

<div class="well well-custom">
<span class="label label-state" ng-class="{'label-warning': actionState == 'Edit', 'label-inverse' : actionState != 'Edit'}" ng-bind="actionState"></span>
<div class="row-fluid">

</div>

<div class="row-fluid " id="form-row">
<div class="span6">

<div class="row-fluid">
	<label><h4>Auto Rule Name</h4></label> <input type="text" class="input-xlarge" name="rule_name" placeholder="New Auto Rule Name" ng-model="populateForm.ruleName" style="min-width:340px" required>
</div><!-- span5 -->

<div class="row-fluid">
 <div class="spoan12"><b>Referral Doctor</b></div>
	    <div class="span2">
		<label class="radio"><input type="radio" name="referral_doctor_type" id="referralOption1" value="manual" ng-click="referralOption='manual'" checked> Manual</label>
		<label class="radio"><input type="radio" name="referral_doctor_type" id="referralOption2" value="mrp" ng-click="referralOption='mrp'"> MRP</label>
		</div>

		<div class="span8">
		<input type="hidden" class="input-small" placeholder="Referral No." name="rdohip" value="{{ populateForm.referralDoctor }}">
		<input type="hidden" class="input-small" placeholder="Specialty">
		<input type="hidden" name="referral_doctor" value="" ng-model="populateForm.referralDoctor">

		<!-- input type="text" class="input-large" placeholder="Name"-->
			<div class="input-append referral_input" ng-show="referralOption=='manual'">
			 <input type="text" name="rd" id="referral_doctor_name" value="" class="span9" readonly />
			 <a href="javascript:referralScriptAttach2('referral_doctor','rd')" class="btn"><i class="icon icon-search"></i></a>
			</div>

			<div class="muted" ng-show="referralOption=='mrp'">*MRP has been selected</div>
		</div>
</div>

<div class="row-fluid">
<div class="span12"><b>Billing Physician</b></div>

  <div class="span2">
	 <label class="radio"><input type="radio" name="billing_doctor_type" id="billingOption1" value="manual" ng-click="billingOption='manual'" checked> Manual</label>
	 <label class="radio"><input type="radio" name="billing_doctor_type" id="billingOption2" value="mrp" ng-click="billingOption='mrp'"> MRP</label>
	</div>

	<div class="span8" ng-show="billingOption=='manual'">


	<select name="billing_doctor" ng-model="populateForm.billingDoctor">
		<option value=""></option>
	  <option ng-repeat="provider in allProviders" value="{{provider.providerNo}}" ng-selected="{{provider.providerNo==populateForm.billingDoctor}}">{{provider.name}}</option>
	</select>
	</div>
	<div class="muted" ng-show="billingOption=='mrp'">*MRP has been selected</div>
</div><!-- row -->


<div class="row-fluid form-inline p10">
<label style="width:90px">Visit Type</label>
	<select name="visit_type" ng-model="populateForm.visitType">
		<option value=""></option>
		<option ng-repeat="type in visitTypes" value="{{type.visitValue}}" ng-selected="{{type.visitValue==populateForm.visitType}}">{{type.visitDisplay}}</option>
	</select>
</div>

<div class="row-fluid form-inline p5">
<label style="width:90px">Visit Location</label>
	<select name="visit_location" ng-model="populateForm.visitLocation">
		<option value=""></option>
		<option ng-repeat="clinic in clinicLocations" value="{{clinic.clinicLocationNo}}" ng-selected="{{clinic.clinicLocationNo==populateForm.visitLocation}}">{{clinic.clinicLocationName}}</option>
	</select>
</div>

<div class="row-fluid form-inline p5">
<label style="width:90px">SLI Code</label>
		<select name="sli_code" ng-model="populateForm.sliCode">
			<option value=""></option>
			<option ng-repeat="code in sliCodes" value="{{code.code}}" ng-selected="{{code.code==populateForm.sliCode}}">{{code.code}} | {{code.display}}</option>
		</select>
</div>

</div><!-- span left -->

<div class="span6">

    <div class="row-fluid">
          <div class="span12">
		    <h5>Service Codes <small>Min 3 Characters, e.g. A00 or J88</small></h5>

		    <div class="input-append" id="codes-dropdown">
		    <input class="span9 dropdown-toggle" data-toggle="dropdown" id="codes-dropdown-input" type="text" ng-model="search.codes" ng-change="searchCodes(search.codes)">
		    	<div class="btn-group">
		        <button type="button" class="btn" >
		          <i class="icon-search"></i>
		        </button>
		        <ul class="dropdown-menu pull-right" id="code-dropdown-display">
					<li ng-repeat="code in codesResults"><a href="#"  data-code="{{code.serviceCode}}" data-desc={{code.description}} data-fee="{{code.value}}" ng-click="addToCodeList(code.serviceCode,code.description,code.value)">{{code.serviceCode}}  {{code.description}}</a></li>
		        </ul>
		        </div>
		    </div>

      		<table class="table table-bordered table-hover table-striped table-condensed" style="background-color:white;font-size:10px;">
      		<tr><th>Code</th><th>Description</th><th>Fee</th></tr>
      		<tr ng-repeat="code in selectedCodes"><td>{{code.serviceCode}}</td><td>{{code.description}}</td><td>{{code.fee}}</td></tr>

      		<tr ng-show="!selectedCodes.length"><td colspan="3" class="none">None</td></tr>
      		</table>
      		<input type="hidden" name="service_code_list" id="service_code_list">
          </div>
  </div><!-- row -->


    <div class="row-fluid">
          <div class="span12">
		  	<h5>Diagnostic Code <small>Min 3 Characters, e.g. 250 or DIA</small></h5>

		    <div class="input-append" id="dx-dropdown">
		      <input class="span8 dropdown-toggle" data-toggle="dropdown" id="dx-dropdown-input" type="text" ng-model="search.dx" ng-keyup="searchDx(search.dx)" autocomplete="off">
		      <div class="btn-group">
		        <button type="button" class="btn" >
		          <i class="icon-search"></i>
		        </button>
		        <ul class="dropdown-menu pull-right" id="dx-dropdown-display">
					<li ng-repeat="dx in dxResults"><a href="#" data-id="{{dx.id}}" data-code="{{dx.code}}" data-desc={{dx.description}} ng-click="addToDxList(dx.id,dx.code,dx.description)">{{dx.code}}  {{dx.description}}</a></li>
		        </ul>
		      </div>
		    </div>

			<table class="table table-bordered table-hover table-striped table-condensed" style="background-color:white;font-size:10px;">
			<tr><th>Code</th><th>Description</th></tr>
			<!-- tr><td><input type="text" class="noborder" name="xml_diagnostic_code" id="xml_diagnostic_code"></td><td><input type="text" class="noborder" name="xml_diagnostic_detail" id="xml_diagnostic_detail"></td></tr-->
			<tr ng-repeat="item in selectedDx"><td>{{item.dxCode}}</td><td>{{item.dxDescription}}</td></tr>
			<tr ng-show="!selectedDx.length"><td colspan="2" class="none">None</td></tr>
			</table>
			<input type="hidden" name="dx_code_list" id="dx_code_list">
          </div>
  </div><!-- row -->



</div><!-- span right -->
</div><!-- row -->


<div class="row-fluid" ng-show="actionState!='Edit'">
<div class="span12 p10">
	<button type="submit" role="submit" class="btn btn-inverse" ng-disabled="populateForm.ruleName==='' || !populateForm.ruleName">Save</button>
</div><!-- span12 -->
</div>

<div class="row-fluid" ng-show="actionState=='Edit'">
<div class="span12" style="padding-top:10px">
	<button type="submit" role="button" class="btn btn-warning">Update</button>
	<button type="button" role="button" class="btn" ng-click="actionState='Add'; resetForm()">Close edit</button>
</div><!-- span12 -->
</div><!-- row-fluid -->

</div><!-- well -->
</form>




<!-- // AUTO BILL RULES LIST -->
<div class="row-fluid">
<div class="span12" style="padding-top:40px">

<table cellspacing="0" cellpadding="4" id="auto-bill-rules" width="100%" class="table table-bordered table-hover">
<thead>
<tr><th colspan="8">Auto Bill Rules</th></tr>
<tr><th>Name</th> <th>Service Code</th> <th>DX</th> <th>SLI</th> <th>VL</th> <th>Billing Physician</th> <th>Referral Physician</th><th width="20px"></th></tr>
</thead>

<tbody>
<tr ng-repeat="rule in billingAllActiveRules">
	<td><a href="javascript:void(0);" ng-click="editRule(rule.id)" title="edit">{{rule.ruleName}}</a></td>
	<td>{{rule.serviceCode}}</td><td>{{rule.dx}}</td>
	<td ng-show="rule.sliCode!='3821'">{{rule.sliCode}}</td>
	<td ng-show="rule.sliCode=='3821'">N/A</td>
	<td>{{rule.visitLocation}}</td>
	<td>{{rule.billingDoctor}}</td>
	<td>{{rule.referralDoctor}}</td>
	<td title="delete auto rule"><a href="#confirmDeleteModal" role="button" ng-click="updateActionDetails(rule.id,rule.ruleName); getBillingRuleMappings(rule.id)" data-toggle="modal"><i class="icon-trash"></i></a></td>
</tr>
<tr ng-show="!billingAllActiveRules.length"><td colspan="12" class="none">No rules</td></tr>
</tbody>

<tfoot>
<tr style="display:none">
<td colspan="12">
<button class="btn" type="button" ng-show="!ruleChecked" ng-click="checkAll(true); ruleChecked=true">Check All</button>

<span ng-show="ruleChecked">
<button class="btn" type="button" ng-click="checkAll(false); ruleChecked=false">Uncheck All</button>
<a href="#confirmDeleteModal" role="button" class="btn" data-toggle="modal"><i class="icon-trash"></i> Delete</a>
</span>

<span ng-repeat="item in ruleActionList">{{item}}</span>

</td>
</tr>
</tfoot>
</table>
</div><!-- span12 -->
</div><!-- row-fluid -->



    <!-- Modal -->
    <div id="confirmDeleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
        <h3 id="myModalLabel">Please Confirm</h3>
      </div>
      <div class="modal-body">
        <p>Are you sure you would like to delete the <b title="rule id={{actionId}}">{{actionDesc}}</b> auto billing rule? </p>

			  <div class="alert alert-error" ng-show="billingRuleMappings.length>0">
			   <span class="label label-important">Important</span> This rule is in use, deleting this will also delete the <b>{{billingRuleMappings.length}}</b> mappings.
		  	</div>

      </div>
      <div class="modal-footer">
        <button class="btn" ng-click="updateActionDetails('','')" data-dismiss="modal" aria-hidden="true">No</button>
        <button class="btn btn-danger" ng-click="removeBillingAutoRule(actionId)">Yes delete</button>
      </div>
    </div><!-- modal -->

</div><!-- main container-fluid -->


<script src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>


<script>
var app = angular.module("billingAutoRulesConfig", ['billingServices']);
app.controller("billingAutoRulesConfig", function($scope,$http,billingService) {

  $scope.referralOption = 'manual';
	$scope.billingOption = 'manual';

	$scope.actionId;
	$scope.actionDesc;
	$scope.actionState = 'Add';
	$scope.updateActionDetails = function($id,$desc){
		$scope.actionId = $id;
		$scope.actionDesc = $desc;
	};

	$scope.billRegion = [];
	getBillRegion = function(){
			billingService.getBillingRegion().then(function(data){
			$scope.billRegion = data;
				if(data.success){
					//console.log("billing region: " + data.message);
				}
			});

	};


	$scope.uniqueServiceTypes = [];
	getUniqueServiceTypes = function(){
			billingService.getUniqueServiceTypes().then(function(data){
			$scope.uniqueServiceTypes = data;
				if(data){
					//console.log("unique service types: " + JSON.stringify(data));
				}
			});

	};


	$scope.defaultView = [];
	getDefaultView = function(){
			billingService.getDefaultView().then(function(data){
			$scope.defaultView = data;
				if(data.success){
					//console.log("default view: " + data.message);
				}
			});

	};

	//getBillingAllActiveRules
	$scope.billingAllActiveRules = [];
	getBillingAllActiveRules = function(){
			billingService.getBillingAllActiveRules().then(function(data){
			$scope.billingAllActiveRules = data.content;
				if(data.success){
					//console.log("all rules " + data.message);
				}
			});

	};

//
	$scope.billingRuleMappings = [];
	$scope.getBillingRuleMappings = function($ruleId){
			billingService.getBillingRuleMappings($ruleId).then(function(data){
			$scope.billingRuleMappings = data.content;
				if(data.success){
					console.log("ruleId: " + $ruleId + " Mappings: " + data.message);
				}
			});
	};


	$scope.ruleChecked = false;

	$scope.showHide = function($event){
		$scope.ruleChecked = $scope.ruleChecked = $event;
	}


	$scope.ruleActionList = [];

	$scope.manageActonList = function($event, $id){

		if($event){
			$scope.ruleActionList.push($id);
		}else{
			 var index = $scope.ruleActionList.indexOf($id);
			 $scope.ruleActionList.splice(index, 1);
		}

		if($scope.ruleActionList.length>0){
			$scope.showHide(true);
		}else{
			$scope.showHide(false);
		}

		//alert(JSON.stringify($scope.ruleActionList));
	}

	$scope.checkAll = function($event){
		angular.forEach($scope.billingAllActiveRules, function(rule) {
		      rule.checks = $event;
		});

		if(!$event){
			//empty array
			$scope.ruleActionList = [];
		}
	}


	$scope.returnMessage = [];
	$scope.removeBillingAutoRule = function($id){
			billingService.removeBillingAutoRule($id).then(function(data){
				$scope.returnMessage = data;
				if(data.success){
					//console.log(data.message);
					var index = $scope.billingAllActiveRules.findIndex((element)=> {
						return (element.id == $id);
					});

					$scope.billingAllActiveRules.splice(index, 1);
					$scope.updateActionDetails('','');
					$('#confirmDeleteModal').modal('toggle');
				}else{
					console.log("Error trying to delete rule: " + data.message);
				}
			});
	};


	$scope.findIndexOfId = function($id){
		//console.log(JSON.stringify($scope.billingAllActiveRules));
		var indexN = $scope.billingAllActiveRules.findIndex((element)=> {
			return (element.id == $id);
		});

		return indexN;
	}

	$scope.populateForm = [];
	$scope.editRule = function($id){
	    $scope.actionState = 'Edit';
	    index = $scope.findIndexOfId($id);
	    //console.log("Edit form: " + JSON.stringify($scope.billingAllActiveRules[index]));
	    $scope.populateForm = angular.copy($scope.billingAllActiveRules[index]);
	}

	//clearForm
	$scope.resetForm = function(){
		$scope.populateForm = [];
		$scope.rulesForm.$setPristine();
	}

	$scope.code_result = '';
	$scope.testMe = function($v){
		$scope.code_result=$v;
	}


	$scope.allProviders = [];
	getAllActiveProviders = function (){
		$.getJSON('../ws/rs/providerService/providers_json', function(data) {
			$scope.allProviders = data.content;
		});


	}
	getAllActiveProviders();


	// TODO: This does not exist in Kai oscar
	$scope.allReferralDoctors = [];
	getAllReferralDoctors = function (){
		$.getJSON('../oscarEncounter/oscarConsultationRequest/searchProfessionalSpecialist.json',{keyword:'%'}, function(data) {
			//console.log(JSON.stringify(data));
		});
	}
	//getAllReferralDoctors();

	$scope.codesResults = [];
	//$scope.codesResults = serviceCodeSearchResults;
	$scope.searchCodes = function (phrase){
		if(phrase.length>2){
			$.getJSON('../ws/rs/billing/searchServiceCodes/'+phrase, function(data) {

				$scope.codesResults = data;
				$scope.$apply();
				if(data.length>0){
					toggleDropdown('codes-dropdown','show');
				}else{
					toggleDropdown('codes-dropdown','hide');
				}
			});
		}else{
			toggleDropdown('codes-dropdown','hide');
		}
	}

	$scope.selectedCodes = [];
	$scope.serviceCodeList = [];
	$scope.serviceCodesToSave = '';
	$scope.addToCodeList = function (c,d,f){
		this.selectedCodes.push({serviceCode:c,description:d, fee:f});
		this.serviceCodeList.push(c);
		this.serviceCodesToSave = this.serviceCodeList.toString();
		$('#service_code_list').val(this.serviceCodesToSave);

		toggleDropdown('codes-dropdown','hide');
		//empty search box
		$('#codes-dropdown-input').val('');
	}

	$scope.dxResults = [];
	$scope.searchDx = function (phrase){
		if(phrase.length>2){
			$.getJSON('../ws/rs/dxRegisty/searchDxCodes/'+phrase, function(data) {

				$scope.dxResults = data;
				$scope.$apply();

				if(data.length>0){
					toggleDropdown('dx-dropdown','show');
				}else{
					toggleDropdown('dx-dropdown','hide');
				}
			});

		}else{
			toggleDropdown('dx-dropdown','hide');
		}
	}


	 function toggleDropdown(dropdown, status){
		if(status=="show"){
			$('#'+dropdown+' .dropdown-menu').show();
		}else{
			$('#'+dropdown+' .dropdown-menu').hide();
		}
	};


	$scope.selectedDx = [];
	$scope.dxCodeList = [];
	$scope.dxCodesToSave = '';
	$scope.addToDxList = function (id,code,description){
		this.selectedDx.push({dxId:id,dxCode:code,dxDescription:description});
		this.dxCodeList.push(code);
		this.dxCodesToSave = this.dxCodeList.toString();
		$('#dx_code_list').val(this.dxCodesToSave);
		toggleDropdown('dx-dropdown','hide');
		//empty search box
		$('#dx-dropdown-input').val('');
	}

getBillingAllActiveRules();

function resizeParentIframe(){
 parent.parent.resizeIframe($('html').height()+500);
}

resizeParentIframe();

$scope.clinicLocations = [];
getClinicLocations = function (){
		$.getJSON('../ws/rs/billing/getClinicLocations', function(data) {
			$scope.clinicLocations = data;
		});
		//console.log("Error getting clinic locations");
}

$scope.visitTypes = [{visitValue:'00',visitDisplay:'Clinic Visit'},
	{visitValue:'01',visitDisplay:'Outpatient Visit'},
	{visitValue:'02',visitDisplay:'Hospital Visit'},
	{visitValue:'03',visitDisplay:'ER'},
	{visitValue:'04',visitDisplay:'Nursing Home'},
	{visitValue:'05',visitDisplay:'Home Visit'}];


$scope.sliCodes = [{code:'3821', display:'Not Applicable'},
{code:'HDS', display:'Hospital Day Surgery'},
{code:'HED', display:'Hospital Emergency Department'},
{code:'HIP', display:'Hospital In-Patient'},
{code:'HOP', display:'Hospital Out-Patient'},
{code:'HRP', display:'Hospital Referred Patient'},
{code:'IHF', display:'Independant Health Facility'},
{code:'OFF"', display:'Office of community physician'},
{code:'OTN', display:'Ontario Telemedicine Network'},
{code:'PDF', display:'Private Diagnostic Facility'},
{code:'RTF', display:'Rehabilitation Treatment Facility'}];


getClinicLocations();
});// end of scope



function rs(n,u,w,h,x) {
	args="width="+w+",height="+h+",resizable=yes,scrollbars=yes,status=0,top=60,left=30";
	remote=window.open(u,n,args);
	if (remote != null) {
		if (remote.opener == null)
			remote.opener = self;
	}
	if (x == 1) { return remote; }
}

var awnd=null;
function referralScriptAttach2(elementName, name2) {
    var d = elementName;
    t0 = escape("document.forms[1].elements[\'"+d+"\'].value");
    t1 = escape("document.forms[1].elements[\'"+name2+"\'].value");
    awnd=rs('att',('../billing/CA/ON/searchRefDoc.jsp?param='+t0+'&param2='+t1),600,600,1);
    awnd.focus();
}

function saveForm(){
	$.post('../ws/rs/billing/saveBillingAutoRule',$('#rulesForm').serialize() , function() {
		location.reload(true);
	});
}

$("#rulesForm").submit(function(e){
  e.preventDefault();
	saveForm();
});
</script>
</body>
</html>
