<%--
    Copyright (c) 2021 WELL EMR Group Inc.
    This software is made available under the terms of the
    GNU General Public License, Version 2, 1991 (GPLv2).
    License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>
<%@ taglib prefix="html" uri="http://jakarta.apache.org/struts/tags-html-el" %>
<%@ page import="org.oscarehr.integration.dynacare.eorder.DynacareOrderPreferences" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
	boolean saved = Boolean.parseBoolean(request.getParameter("saved"));
  // Get Dynacare configuration settings from systemPreferences table
  String eorderUrl = DynacareOrderPreferences.getOrderUrl();
  String clinicEmail = DynacareOrderPreferences.getToEmail();
  String instanceId = DynacareOrderPreferences.getInstanceId();
%>
<html>
<head>
	<title>Dynacare eOrder</title>
	<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
	<link href="<%=request.getContextPath() %>/library/bootstrap/3.0.0/css/bootstrap.css" rel="stylesheet">

	<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
</head>
<body>
	<h3>
		Dynacare eOrder Configuration
	</h3>
	<html:form action="/DynacareOrderConfiguration" method="post" styleClass="well">
		<% if (saved) { %>
			<div>
				Configuration saved! <br> <br>
			</div>
		<% } %>
		<div class="control-group">
			<label class="control-label">
				eOrder URL <br>
				<input type="text" name="eorderUrl" style="width:300px" class="form-control" value="<%= eorderUrl %>" disabled>
			</label>
		</div>
		<div class="control-group">
			<label class="control-label">
				Dynacare API Key <br>
				<input type="text" name="apiKey" style="width:300px" class="form-control" autocomplete="off">
			</label>
		</div>
		<div class="control-group">
			<label class="control-label">
				Clinic Email <br>
				<input type="text" name="clinicEmail" style="width:300px" class="form-control" value="<%= clinicEmail %>" disabled>
			</label>
		</div>
		<div class="control-group">
			<label class="control-label">
				Instance ID <br>
				<input type="text" name="instanceId" style="width:300px" class="form-control" value="<%= instanceId %>" disabled>
			</label>
		</div>
		<br>
		<input class="btn btn-primary" type="submit" name="submit" value="Save">
	</html:form>
</body>
</html>
