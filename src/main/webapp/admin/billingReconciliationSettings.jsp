<%--

   Copyright (c) 2022 WELL EMR Group Inc.
   This software is made available under the terms of the
   GNU General Public License, Version 2, 1991 (GPLv2).
   License details are available via "gnu.org/licenses/gpl-2.0.html".

--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed = true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="org.oscarehr.common.dao.PropertyDao" %>
<%@ page import="org.oscarehr.common.model.Property" %>
<%@ page import="org.oscarehr.common.dao.ProviderDataDao" %>
<%@ page import="org.oscarehr.common.model.ProviderData" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
    String key_show_all = "show_all_billing_reconciliation";
    String key_see_all = "see_all_billing_reconciliation";
%>

<html:html locale="true">
    <head>
        <title><bean:message key="admin.admin.btnBillingReconciliationSettings" /></title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
    </head>

    <%
        SystemPreferences showAll = SystemPreferencesUtils.findPreferenceByName(key_show_all);
        if (showAll != null && showAll.getName() != null && showAll.getValue() != null) {
            dataBean.setProperty(showAll.getName(), showAll.getValue());
        } else {
            dataBean.setProperty(key_show_all, "true");
        }
    %>

    <body vlink="#0000FF" class="BodyStyle">
    <h4>Manage Billing Reconciliation Settings</h4>
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
                <tr>
                    <td>Show All Billing Reconciliation: </td>
                    <td>
                        <input id="show_all_billing_reconciliation_true" type="radio" value="true"
                            <%=(dataBean.getProperty(key_show_all, "true").equals("true")) ? "checked" : ""%>
                           onclick="toggleEnabled('../admin/billingReconciliationSettingsAction.do?oper=toggleShowAll&show=true')"
                        />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="show_all_billing_reconciliation_false" type="radio" value="false"
                            <%=(dataBean.getProperty(key_show_all, "true").equals("false")) ? "checked" : ""%>
                           onclick="toggleEnabled('../admin/billingReconciliationSettingsAction.do?oper=toggleShowAll&show=false')"
                        />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </tbody>
        </table>

        <label for="result_filter">
            Provider:
            <input type="text" name="result_filter" id="result_filter"/>
        </label>

        <table class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
                <tr>
                    <td>
                        <label>Provider</label>
                    </td>
                    <td>
                        <label>See All Billing Reconciliation</label>
                    </td>
                </tr>
                <%
                    ProviderDataDao providerDao = SpringUtils.getBean(ProviderDataDao.class);
                    List<ProviderData> activeProviders = providerDao.findAll(false);

                    for (ProviderData providerData : activeProviders) {
                    Property providerSeeAll = new Property();
                    List<Property> seeAllProperties = propertyDao.findByNameAndProvider(key_see_all, providerData.getId());
                    if (seeAllProperties.size() != 0) {
                        for (Property seeAll : seeAllProperties) {
                            providerSeeAll = seeAll;
                            break;
                        }
                    } else {
                        providerSeeAll.setProviderNo(providerData.getId());
                        providerSeeAll.setName(key_see_all);
                        providerSeeAll.setValue("false");
                    }
                %>
                <tr class="provider_row">
                    <td>
                        <label>
                            <%=Encode.forHtmlContent(providerData.getLastName() + ", " + providerData.getFirstName())%>
                        </label>
                    </td>
                    <td>
                        <input type="checkbox" <%=providerSeeAll.getValue().compareTo("true") == 0 ? "checked=\"checked\"":""%>
                               onclick="toggleEnabled('../admin/billingReconciliationSettingsAction.do?oper=toggleProvider&providerNo=<%=providerSeeAll.getProviderNo()%>')">
                    </td>
                </tr>
                <% } %>
            </tbody>
        </table>
    <script>
        function toggleEnabled(url) { window.location = url; }

        $('#result_filter').on('keyup', function() {
            $('.provider_row').each((id,element) => {
                if (element.innerText.toLowerCase().includes($(this).val().toLowerCase())) {
                    element.style.display = ''
                } else {
                    element.style.display = 'none'
                }
            });
        });

    </script>
    </body>
</html:html>