<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%

    if (request.getParameter("dboperation") != null && !request.getParameter("dboperation").isEmpty() && request.getParameter("dboperation").equals("Save")) {
        for(String key : SystemPreferences.ECHART_PREFERENCE_KEYS) {
            SystemPreferences preference = SystemPreferencesUtils.findPreferenceByName(key);
            String newValue = request.getParameter(key);

            if (preference != null) {
                if (!preference.getValue().equals(newValue)) {
                    preference.setUpdateDate(new Date());
                    preference.setValue(newValue);
                    SystemPreferencesUtils.merge(preference);
                }
            } else {
                preference = new SystemPreferences();
                preference.setName(key);
                preference.setUpdateDate(new Date());
                preference.setValue(newValue);
                SystemPreferencesUtils.persist(preference);
            }
        }
    }

    boolean enableDemographicPatientClinicStatus = SystemPreferencesUtils
            .isReadBooleanPreferenceWithDefault("enable_demographic_patient_clinic_status", true);
%>

<html:html locale="true">
    <head>
        <title>eChart Display Preferences</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
    </head>

    <%
        List<SystemPreferences> preferences = SystemPreferencesUtils.findPreferencesByNames(SystemPreferences.ECHART_PREFERENCE_KEYS);
        for(SystemPreferences preference : preferences) {
            dataBean.setProperty(preference.getName(), preference.getValue());
        }
    %>

    <body vlink="#0000FF" class="BodyStyle">
    <h4>Manage eChart Display Settings</h4>
    <form name="displaySettingsForm" method="post" action="echartDisplaySettings.jsp">
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
                <tr>
                    <td>Hide eChart Timer: </td>
                    <td>
                        <input id="echart_hide_timer-true" type="radio" value="true" name="echart_hide_timer"
                                <%=(dataBean.getProperty("echart_hide_timer", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="echart_hide_timer-false" type="radio" value="false" name="echart_hide_timer"
                                <%=(dataBean.getProperty("echart_hide_timer", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td><bean:message key="admin.admin.echartDisplaySettings.hideEformNotes"/>: </td>
                    <td>
                        <input id="hideEformNotesTrue" type="radio" value="true" name="hideEformNotes"
                                <%=(dataBean.getProperty("hideEformNotes", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="hideEformNotesFalse" type="radio" value="false" name="hideEformNotes"
                                <%=(dataBean.getProperty("hideEformNotes", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td><bean:message key="admin.admin.echartDisplaySettings.hideDocumentNotes"/>: </td>
                    <td>
                        <input id="hideDocumentNotesTrue" type="radio" value="true" name="hideDocumentNotes"
                                <%=(dataBean.getProperty("hideDocumentNotes", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="hideDocumentNotesFalse" type="radio" value="false" name="hideDocumentNotes"
                                <%=(dataBean.getProperty("hideDocumentNotes", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>                
                <tr>
                    <td><bean:message key="admin.admin.echartDisplaySettings.hideInvoiceNotes"/>: </td>
                    <td>
                        <input id="hideInvoiceNotesTrue" type="radio" value="true" name="hideInvoiceNotes"
                                <%=(dataBean.getProperty("hideInvoiceNotes", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="hideInvoiceNotesFalse" type="radio" value="false" name="hideInvoiceNotes"
                                <%=(dataBean.getProperty("hideInvoiceNotes", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td><bean:message key="admin.admin.echartDisplaySettings.hideFormNotes"/>: </td>
                    <td>
                        <input id="hideFormNotesTrue" type="radio" value="true" name="hideFormNotes"
                                <%=(dataBean.getProperty("hideFormNotes", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="hideFormNotesFalse" type="radio" value="false" name="hideFormNotes"
                                <%=(dataBean.getProperty("hideFormNotes", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td><bean:message key="admin.admin.echartDisplaySettings.hideCppNotes"/>: </td>
                    <td>
                        <input id="hideCppNotesTrue" type="radio" value="true" name="hideCppNotes"
                                <%=(dataBean.getProperty("hideCppNotes", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="hideCppNotesFalse" type="radio" value="false" name="hideCppNotes"
                                <%=(dataBean.getProperty("hideCppNotes", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>                
                <tr>
                    <td>Display Email Indicator: </td>
                    <td>
                        <input id="echart_email_indicator-true" type="radio" value="true" name="echart_email_indicator"
                                <%=(dataBean.getProperty("echart_email_indicator", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="echart_email_indicator-false" type="radio" value="false" name="echart_email_indicator"
                                <%=(dataBean.getProperty("echart_email_indicator", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <% if (enableDemographicPatientClinicStatus) { %>
                    <tr>
                        <td>Display Family Doctor:</td>
                        <td>
                            <input type="radio" value="true" name="echart_show_fam_doc_widget"
                                    <%=(dataBean.getProperty("echart_show_fam_doc_widget", "false").equals("true")) ? "checked" : ""%> />
                            Yes
                            &nbsp;&nbsp;&nbsp;
                            <input type="radio" value="false" name="echart_show_fam_doc_widget"
                                    <%=(dataBean.getProperty("echart_show_fam_doc_widget", "false").equals("false")) ? "checked" : ""%> />
                            No
                            &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>Display Referral Doctor:</td>
                        <td>
                            <input type="radio" value="true" name="echart_show_ref_doc_widget"
                                    <%=(dataBean.getProperty("echart_show_ref_doc_widget", "false").equals("true")) ? "checked" : ""%> />
                            Yes
                            &nbsp;&nbsp;&nbsp;
                            <input type="radio" value="false" name="echart_show_ref_doc_widget"
                                    <%=(dataBean.getProperty("echart_show_ref_doc_widget", "false").equals("false")) ? "checked" : ""%> />
                            No
                            &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                <% } %>
                    <tr>
                        <td><bean:message key="admin.admin.echartDisplaySettings.displayLargerFontSize"/>:</td>
                        <td>
                            <input type="radio" value="true" name="echart_show_larger_font_size"
                                    <%=(dataBean.getProperty("echart_show_larger_font_size", "false").equals("true")) ? "checked" : ""%> />
                            Yes
                            &nbsp;&nbsp;&nbsp;
                            <input type="radio" value="false" name="echart_show_larger_font_size"
                                    <%=(dataBean.getProperty("echart_show_larger_font_size", "false").equals("false")) ? "checked" : ""%> />
                            No
                            &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
               
                <tr>
                    <td><bean:message key="admin.admin.echartDisplaySettings.groupDocumentByType"/>:</td>
                    <td>
                        <input type="radio" value="true" name="echart_show_group_document_by_type"
                                <%=(dataBean.getProperty("echart_show_group_document_by_type", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input type="radio" value="false" name="echart_show_group_document_by_type"
                                <%=(dataBean.getProperty("echart_show_group_document_by_type", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>Display Chart# in Header</td>
                    <td>
                        <input id="displayChartNo" type="radio" value="true" name="displayChartNo"
                                <%=(dataBean.getProperty("displayChartNo", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="displayChartNo" type="radio" value="false" name="displayChartNo"
                                <%=(dataBean.getProperty("displayChartNo", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>Display Report Module: </td>
                    <td>
                        <input id="echart_show_report_module-true" type="radio" value="true" name="echart_show_report_module"
                                <%=(dataBean.getProperty("echart_show_report_module", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="echart_show_report_module-false" type="radio" value="false" name="echart_show_report_module"
                                <%=(dataBean.getProperty("echart_show_report_module", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td><bean:message key="admin.admin.echartDisplaySettings.caisiNoteFilter"/>: </td>
                    <td>
                        <input id="echart_caisi_note_filter-true" type="radio" value="true" name="caisi_note_filter_enabled"
                                <%=(dataBean.getProperty("caisi_note_filter_enabled", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="echart_caisi_note_filter-false" type="radio" value="false" name="caisi_note_filter_enabled"
                                <%=(dataBean.getProperty("caisi_note_filter_enabled", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td><bean:message key="admin.admin.echartDisplaySettings.displayAppointmentTime"/>:</td>
                    <td>
                        <input type="radio" value="true" name="echartShowAppointmentTime"
                                    <%=(dataBean.getProperty("echartShowAppointmentTime", "false").equals("true")) ? "checked" : ""%> />
                            Yes
                            &nbsp;&nbsp;&nbsp;
                        <input type="radio" value="false" name="echartShowAppointmentTime"
                                    <%=(dataBean.getProperty("echartShowAppointmentTime", "false").equals("false")) ? "checked" : ""%> />
                            No
                            &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td><bean:message key="admin.admin.echartDisplaySettings.displayRosterStatus"/>:</td>
                    <td>
                        <input id="echartShowRosterStatus-true" type="radio" value="true" name="echartShowRosterStatus"
                                <%= (dataBean.getProperty("echartShowRosterStatus", "false").equals("true")) ? "checked" : "" %> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="echartShowRosterStatus-false" type="radio" value="false" name="echartShowRosterStatus"
                                <%= (dataBean.getProperty("echartShowRosterStatus", "false").equals("false")) ? "checked" : "" %> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <bean:message key="admin.admin.echartDisplaySettings.sortMeasurementsInEChart"/>:
                    </td>
                    <td>
                        <input id="sortMeasurementsInEChart-true" type="radio" value="true"
                               name="sortMeasurementsInEChart"
                                <%= Boolean.parseBoolean(dataBean.getProperty("sortMeasurementsInEChart")) ? "checked" : "" %> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="sortMeasurementsInEChart-false" type="radio" value="false"
                               name="sortMeasurementsInEChart"
                                <%= !Boolean.parseBoolean(dataBean.getProperty("sortMeasurementsInEChart")) ? "checked" : "" %> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td><bean:message key="admin.admin.echartDisplaySettings.displayOnlyOverduePreventions"/>:</td>
                    <td>
                        <input id="echartShowOverduePreventionsOnly-true" type="radio" value="true" name="echartShowOverduePreventionsOnly"
                                <%= (dataBean.getProperty("echartShowOverduePreventionsOnly", "false").equals("true")) ? "checked" : "" %> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="echartShowOverduePreventionsOnly-false" type="radio" value="false" name="echartShowOverduePreventionsOnly"
                                <%= (dataBean.getProperty("echartShowOverduePreventionsOnly", "false").equals("false")) ? "checked" : "" %> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td><bean:message key="admin.admin.echartDisplaySettings.displayRecordedOutsideUse"/>:</td>
                    <td>
                        <input id="echartShowOutsideUseNote-true" type="radio" value="true" name="echartShowOutsideUseNote"
                                <%= (dataBean.getProperty("echartShowOutsideUseNote", "false").equals("true")) ? "checked" : "" %> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="echartShowOutsideUseNote-false" type="radio" value="false" name="echartShowOutsideUseNote"
                                <%= (dataBean.getProperty("echartShowOutsideUseNote", "false").equals("false")) ? "checked" : "" %> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>Display Initial Tickler Message for each Tickler:</td>
                    <td>
                        <input id="eChartDisplayInitialTicklerMessage-true" type="radio" value="true" name="eChartDisplayInitialTicklerMessage"
                                <%= Boolean.parseBoolean(dataBean.getProperty("eChartDisplayInitialTicklerMessage", "false")) ? "checked" : "" %> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="eChartDisplayInitialTicklerMessage-false" type="radio" value="false" name="eChartDisplayInitialTicklerMessage"
                                <%= !Boolean.parseBoolean(dataBean.getProperty("eChartDisplayInitialTicklerMessage", "false")) ? "checked" : "" %> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>Display Inbox link at top of eChart for document preview:
                        <span class="label is-green pull-right">New UI Only</span></td>
                    <td>
                        <input id="echartDisplayInboxLink-true" type="radio" value="true" name="echartDisplayInboxLink"
                                <%= (dataBean.getProperty("echartDisplayInboxLink", "false").equals("true")) ? "checked" : "" %> />
                        Yes
                        &nbsp;
                        <input id="echartDisplayInboxLink-false" type="radio" value="false" name="echartDisplayInboxLink"
                                <%= (dataBean.getProperty("echartDisplayInboxLink", "false").equals("false")) ? "checked" : "" %> />
                        No
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td><bean:message key="admin.admin.echartDisplaySettings.displayHealthInsuranceNumber"/>:</td>
                    <td>
                        <input id="displayHealthInsuranceNumber-true" type="radio" value="true" name="display_health_insurance_number"
                                <%= (dataBean.getProperty("display_health_insurance_number", "false").equals("true")) ? "checked" : "" %> />
                        Yes
                        &nbsp;
                        <input id="displayHealthInsuranceNumber-false" type="radio" value="false" name="display_health_insurance_number"
                                <%= (dataBean.getProperty("display_health_insurance_number", "false").equals("false")) ? "checked" : "" %> />
                        No
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>Display Consultation Reason in eChart:
                    <td>
                        <input id="echartDisplayConsultationReason-true" type="radio" value="true" name="echartDisplayConsultationReason"
                                <%= (dataBean.getProperty("echartDisplayConsultationReason", "true").equals("true")) ? "checked" : "" %> />
                        Yes
                        &nbsp;
                        <input id="echartDisplayConsultationReason-false" type="radio" value="false" name="echartDisplayConsultationReason"
                                <%= (dataBean.getProperty("echartDisplayConsultationReason", "true").equals("false")) ? "checked" : "" %> />
                        No
                        &nbsp;
                    </td>
                </tr>
            </tbody>
        </table>
		<h4><bean:message key="admin.admin.manageEchartSettings"/></h4>
		<table id="SettingsTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
                <tr>
                    <td><bean:message key="admin.admin.manageEchartSettingsPasteFaxNote"/>: </td>
                    <td>
                        <input id="echart_paste_fax_note-true" type="radio" value="true" name="echart_paste_fax_note"
                                <%=(dataBean.getProperty("echart_paste_fax_note", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="echart_paste_fax_note-false" type="radio" value="false" name="echart_paste_fax_note"
                                <%=(dataBean.getProperty("echart_paste_fax_note", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td><bean:message key="admin.allergyAlert.enableAllergyAlertPopup"/>:</td>
                    <td>
                        <input id="allergyAlertPopup" type="radio" value="true" name="allergyAlertPopup"
                                <%=(dataBean.getProperty("allergyAlertPopup", "false").equals("true")) ? "checked" : ""%> />
                        Yes
                        &nbsp;&nbsp;&nbsp;
                        <input id="allergyAlertPopup" type="radio" value="false" name="allergyAlertPopup"
                                <%=(dataBean.getProperty("allergyAlertPopup", "false").equals("false")) ? "checked" : ""%> />
                        No
                        &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>Display Chart No on PDF with the following label:</td>
                    <td>
                        <input type="text" max="100" id="inputChartNo" name="oscarEncounter.pdfPrint.chartNo" 
                                value="<%= (dataBean.getProperty("oscarEncounter.pdfPrint.chartNo", "")) %>"/>
                    </td>
                </tr>
                <%
                    String cmeJs = oscar.OscarProperties.getInstance().getProperty("cme_js");
                    if(cmeJs != null && cmeJs.startsWith("eyeform")) {
                %>
                <tr>
                    <td>
                        <bean:message key="admin.echartSettings.hideEncounterLink"/>:
                    </td>
                    <td>
                        <input id="hideEncounterLink-true" type="radio" value="true" name="hideEncounterLink"
                            <%=(dataBean.getProperty("hideEncounterLink", "false").equals("true")) ? "checked" : ""%> />
                        Yes &nbsp;&nbsp;&nbsp;
                        <input id="hideEncounterLink-false" type="radio" value="false" name="hideEncounterLink"
                            <%=(dataBean.getProperty("hideEncounterLink", "false").equals("false")) ? "checked" : ""%> />
                        No &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <bean:message key="admin.echartSettings.excludeMiscFromConsultationReports"/>:
                    </td>
                    <td>
                        <input id="excludeMisc-true" type="radio" value="true" name="excludeMisc"
                            <%= Boolean.parseBoolean(dataBean.getProperty("excludeMisc")) ? "checked" : "" %> />
                        Yes &nbsp;&nbsp;&nbsp;
                        <input id="excludeMisc-false" type="radio" value="false" name="excludeMisc"
                            <%= !Boolean.parseBoolean(dataBean.getProperty("excludeMisc")) ? "checked" : "" %> />
                        No &nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <% } %>
             </tbody>
        </table>
              
        <input type="button" onclick="document.forms['displaySettingsForm'].dboperation.value='Save'; document.forms['displaySettingsForm'].submit();" name="saveDisplaySettings" value="Save"/>
    </form>
    </body>
</html:html>
