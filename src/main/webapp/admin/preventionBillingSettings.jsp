<%--

   Copyright (c) 2022 WELL EMR Group Inc.
   This software is made available under the terms of the
   GNU General Public License, Version 2, 1991 (GPLv2).
   License details are available via "gnu.org/licenses/gpl-2.0.html".

--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%
    String roleName$ =
            (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed = true;
%>
<security:oscarSec roleName="<%= roleName$ %>" objectName="_admin" rights="r" reverse="<%= true %>">
    <% authed = false; %>
    <% response.sendRedirect("../securityError.jsp?type=_admin"); %>
</security:oscarSec>
<%
    if (!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="org.oscarehr.common.dao.PropertyDao" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.oscarehr.common.dao.PreventionBillingConfigDao" %>
<%@ page import="org.oscarehr.common.model.PreventionBillingConfig" %>
<%@ page import="org.oscarehr.common.dao.BillingVisitTypeDao" %>
<%@ page import="org.oscarehr.common.model.BillingVisitType" %>
<%@ page import="org.oscarehr.common.dao.BillingSliCodeDao" %>
<%@ page import="org.oscarehr.common.model.BillingSliCode" %>
<%@ page import="org.oscarehr.common.dao.ClinicLocationDao" %>
<%@ page import="org.oscarehr.common.model.ClinicLocation" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.oscarehr.common.dao.BillingTypeDao" %>
<%@ page import="org.oscarehr.common.model.BillingType" %>
<%@ page import="oscar.oscarPrevention.PreventionDisplayConfig" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.oscarehr.common.service.PreventionBillingSettingsService" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    PreventionBillingSettingsService preventionBillingSettingsService = SpringUtils.getBean(PreventionBillingSettingsService.class);
    List<BillingType> billingTypes = preventionBillingSettingsService.getAllBillingTypes();
    List<BillingVisitType> billingVisitTypes = preventionBillingSettingsService.getAllBillingVisitTypes();
    List<BillingSliCode> billingSliCodes = preventionBillingSettingsService.getBillingSliCodes();
    List<ClinicLocation> clinicLocations = preventionBillingSettingsService.getClinicLocations();
    List<PreventionBillingConfig> preventionBillingConfigs = preventionBillingSettingsService.getAllPreventionBillingConfigs();
    String keyPreventionBillingSettings = "prevention_billing_settings";

    if (preventionBillingSettingsService.hasSave(request)) {
        preventionBillingSettingsService.savePreventionBillingConfig(request);
        preventionBillingConfigs = preventionBillingSettingsService.getAllPreventionBillingConfigs();
    }
%>
<html ng-app="preventionBillingSetting">
<head>
    <title><bean:message key="admin.admin.btnBillingReconciliationSettings"/></title>
    <script src="<%= request.getContextPath() %>/JavaScriptServlet" type="text/javascript"></script>
    <link href="<%= request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/js/bootstrap.js"></script>
    <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
</head>

<%
    SystemPreferences showAll = SystemPreferencesUtils.findPreferenceByName(keyPreventionBillingSettings);
    if (showAll != null && showAll.getName() != null && showAll.getValue() != null) {
        dataBean.setProperty(showAll.getName(), showAll.getValue());
    } else {
        dataBean.setProperty(keyPreventionBillingSettings, "false");
    }
%>

<body vlink="#0000FF" class="BodyStyle">
<h4>Prevention Billing Settings</h4>
<input type="hidden" name="dboperation" value="">
<table id="displaySettingsTable"
       class="table table-bordered table-striped table-hover table-condensed">
    <tbody>
    <tr>
        <td>All Prevention Billing Auto open:</td>
        <td>
            <input id="show_all_billing_reconciliation_true" type="radio" value="true"
                    <%=
                    dataBean.getProperty(keyPreventionBillingSettings, "true").equals("true")
                            ? "checked" : "" %>
                    onclick="toggleEnabled('../admin/preventionBillingSettingsAction.do?oper=toggleAutoOpen&show=true')"
            />
            Yes
            &nbsp;&nbsp;&nbsp;
            <input id="show_all_billing_reconciliation_false" type="radio" value="false"
                    <%=
                    dataBean.getProperty(keyPreventionBillingSettings, "false").equals("false")
                            ? "checked" : "" %>
                    onclick="toggleEnabled('../admin/preventionBillingSettingsAction.do?oper=toggleAutoOpen&show=false')"
            />
            No
            &nbsp;&nbsp;&nbsp;
        </td>
    </tr>
    </tbody>
</table>

<form name="preventionBillingSettingsForm" method="post" action="preventionBillingSettings.jsp">
    <input type="hidden" name="dboperation" value="">
    <table class="table table-bordered table-striped table-hover table-condensed">
        <tbody>
        <tr>
            <td>
            </td>
            <td>
                <label>Preventions</label>
            </td>
            <td>
                <label>Billing Codes</label>
            </td>
            <td>
                <label>Billing Type</label>
            </td>
            <td>
                <label>Dx Code</label>
            </td>
            <td>
                <label>Visit Type</label>
            </td>
            <td>
                <label>Visit location</label>
            </td>
            <td>
                <label>SLI Code</label>
            </td>
        </tr>
        <%
            for (PreventionBillingConfig preventionBillingConfig : preventionBillingConfigs) {
        %>
        <tr class="provider_row" id="preventionBillingConfig" name="preventionBillingConfig">
            <td>
                <input id="preventionId" style="width: 100px;" name="preventionId" value="<%= preventionBillingConfig.getId() %>" type="hidden"/>
            </td>
            <td>
                <input id="preventionType" style="width: 100px;" name="preventionType" value="<%= preventionBillingConfig.getPreventionType() %>" readonly type="text"/>
            </td>
            <td>
                <input id="serviceCode" style="width:80px;" type="text" name="serviceCode" value="<%= preventionBillingConfig.getBillingServiceCodeAndUnit() %>"/>
            </td>
            <td>
                <select id="billingType" name="billingType" style="width: 160px">
                    <option value="none" style="background-color:white"> None</option>
                    <%
                        for (int i = 0; i < billingTypes.size(); i++) {
                    %>
                    <option value="<%= Encode.forHtmlAttribute(billingTypes.get(i).getType()) %>"
                            <%= (billingTypes.get(i).getType()
                                    .equals(preventionBillingConfig.getBillingType())) ? "selected" : "" %> >
                            <%= Encode.forHtmlContent(
                                    billingTypes.get(i).getType() + " | " + billingTypes.get(i).getDisplay()) %>
                    </option>
                    <% } %>
                </select>
            </td>
            <td>
                <input id="dxCode" style="width: 80px;" type="text" name="dxCode"
                       value="<%= preventionBillingConfig.getBillingDxCode() %>"/>
            </td>
            <td>
                <select id="billingVisitType" name="billingVisitType" style="width: 120px">
                    <option value="none" style="width: 120px; background-color:white"> Not
                        Applicable
                    </option>
                    <%
                        for (int i = 0; i < billingVisitTypes.size(); i++) {
                    %>
                    <option value="<%= Encode.forHtmlAttribute(billingVisitTypes.get(i).getType()) %>"
                            <%= (billingVisitTypes.get(i).getType()
                                    .equals(preventionBillingConfig.getVisitType())) ? "selected" : "" %> >
                            <%= Encode.forHtmlContent(
                                    billingVisitTypes.get(i).getType() + " | " + billingVisitTypes.get(i).getDisplay()) %>
                    </option>
                    <% } %>
                </select>
            </td>
            <td>
                <select id="clinicLocation" name="clinicLocation" style="width: 200px">
                    <%
                        for (int i = 0; i < clinicLocations.size(); i++) {
                    %>
                    <option value="<%= Encode.forHtmlAttribute(clinicLocations.get(i).getClinicLocationNo()) %>"
                            <%= (clinicLocations.get(i).getClinicLocationNo()
                                    .equals(preventionBillingConfig.getVisitLocation())) ? "selected" : "" %> >
                            <%= Encode.forHtmlContent(
                                    clinicLocations.get(i).getClinicLocationNo() + " | " + clinicLocations.get(i).getClinicLocationName()) %>
                    </option>
                    <% } %>
                </select>
            </td>
            <td>
                <select id="billingSliCodeType" name="billingSliCodeType">
                    <option value="none" style="background-color:white"> Not Applicable</option>
                    <%
                        for (int i = 0; i < billingSliCodes.size(); i++) {
                    %>
                    <option value="<%= Encode.forHtmlAttribute(billingSliCodes.get(i).getCode()) %>"
                            <%= (billingSliCodes.get(i).getCode()
                                    .equals(preventionBillingConfig.getSliCode())) ? "selected" : "" %> >
                            <%= Encode.forHtmlContent(
                                    billingSliCodes.get(i).getCode() + " | " + billingSliCodes.get(i).getDisplay()) %>
                    </option>
                    <% } %>
                </select>
            </td>
        </tr>
        <% } %>
        </tbody>
    </table>
    <div style="position: absolute; right: 0;">
        <input type="button"
               onclick="document.forms['preventionBillingSettingsForm'].dboperation.value='Save'; document.forms['preventionBillingSettingsForm'].submit();"
               name="savePreventionBillingSettings" value="Save"/>
        <input type="button"
               onclick="document.forms['preventionBillingSettingsForm'].dboperation.value='Cancel'; document.forms['preventionBillingSettingsForm'].submit();"
               name="savePreventionBillingSettings" value="Cancel"/>
    </div>
</form>
<script>
  function toggleEnabled(url) {
    window.location = url;
  }
  $('#result_filter').on('keyup', function () {
    $('.provider_row').each((id, element) => {
      if (element.innerText.toLowerCase().includes($(this).val().toLowerCase())) {
        element.style.display = ''
      } else {
        element.style.display = 'none'
      }
    });
  });
</script>
</body>
<style>
    select, textarea, input[type="text"], input[type="password"], input[type="datetime"], input[type="datetime-local"], input[type="date"], input[type="month"], input[type="time"], input[type="week"], input[type="number"], input[type="email"], input[type="url"], input[type="search"], input[type="tel"], input[type="color"],
    .uneditable-input {
        display: inline-block;
        height: 20px;
        padding: 0px 2px;
        margin-bottom: 0px;
        font-size: 12px;
        line-height: 20px;
        color: #555555;
        vertical-align: middle;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    .iframe {
        position: absolute;
        height: 100%;
        width: 80%;
    }
</style>
</html>