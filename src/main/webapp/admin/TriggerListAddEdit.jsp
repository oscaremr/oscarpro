<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.dao.TriggerListDao" %>
<%@ page import="org.oscarehr.common.model.TriggerList" %>
<%@ page import="java.util.List" %>
<%@ page import="org.oscarehr.common.model.TriggerListItem" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.apache.commons.lang3.math.NumberUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.oscarehr.common.dao.TriggerListItemDao" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="oscar.log.LogAction" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    SystemPreferences reloadPreference = SystemPreferencesUtils.findPreferenceByName("kai_username");
    if (!(reloadPreference != null && loggedInInfo != null && loggedInInfo.getLoggedInSecurity() != null &&
            StringUtils.trimToEmpty(reloadPreference.getValue()).equalsIgnoreCase(loggedInInfo.getLoggedInSecurity().getUserName()))) {
        response.sendRedirect("../logout.jsp");
    }
    TriggerListDao triggerListDao = SpringUtils.getBean(TriggerListDao.class);
    TriggerListItemDao triggerListItemDao = SpringUtils.getBean(TriggerListItemDao.class);
    String triggerListId = StringUtils.trimToEmpty(request.getParameter("triggerListId"));
    String dboperation = StringUtils.trimToEmpty(request.getParameter("dboperation"));
    
    if (dboperation.equals("Save")) {
        TriggerList triggerList = NumberUtils.isParsable(triggerListId) ? triggerListDao.find(Integer.parseInt(triggerListId)) : null;
        
        //Updates the triggerList's name. If it's a new triggerList, also gets the new Id.
        String triggerListName = StringUtils.trimToEmpty(request.getParameter("triggerListName"));
        if (triggerList != null) {
            triggerList.setName(triggerListName);
            triggerListDao.merge(triggerList);
        } else {
            triggerList = new TriggerList();
            triggerList.setName(triggerListName);
            triggerListDao.persist(triggerList);
            triggerListId = triggerList.getId().toString();
        }
        
        //Updates any triggerListItems that were changed and removes any that were deleted.
        for (TriggerListItem triggerListItem : triggerList.getTriggerListItems()) {
            if (StringUtils.isNotEmpty(request.getParameter("oldTriggerListItem_" + triggerListItem.getId()))) {
                triggerListItem.setItemValue(request.getParameter("oldTriggerListItem_" + triggerListItem.getId()));
                triggerListItemDao.merge(triggerListItem);
            } else {
                LogAction.addLog(loggedInInfo.getLoggedInProviderNo(), "remove", "TriggerListItem", "listId=" + triggerListItem.getListId() + "; listValue=" + triggerListItem.getItemValue(), loggedInInfo.getIp());
                triggerListItemDao.removeTriggerListItem(triggerListItem);
            }
        }
    
        //Loops through all the parameters to check for any new list items and then adds them to the database
        Map<String, String[]> parameterMap = request.getParameterMap();
        for (Map.Entry<String, String[]> parameter : parameterMap.entrySet()) {
            if (StringUtils.isNotEmpty(parameter.getKey()) && parameter.getKey().startsWith("newTriggerListItem_")) {
                TriggerListItem triggerListItem = new TriggerListItem();
                triggerListItem.setItemValue(StringUtils.trimToEmpty(request.getParameter(parameter.getKey())));
                triggerListItem.setListId(Integer.parseInt(triggerListId));
                triggerListItemDao.persist(triggerListItem);
            }
        }
    } else if (dboperation.equals("Delete")) {
        TriggerList triggerList = NumberUtils.isParsable(triggerListId) ? triggerListDao.find(Integer.parseInt(triggerListId)) : null;
        if (triggerList != null) {
            triggerList.setArchived(true);
            triggerListDao.merge(triggerList);
            triggerListId = "";
        }
    }
    
    TriggerList triggerList = new TriggerList();
    if (NumberUtils.isParsable(triggerListId)) {
        triggerList = triggerListDao.find(Integer.parseInt(triggerListId));
    }
    List<TriggerList> triggerListList = triggerListDao.findAll();
%>

<html:html locale="true">
    <head>
        <title><bean:message key="admin.admin.triggerList"/></title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/library/bootstrap/3.0.0/css/bootstrap-glyphicons-only.min.css">
        <script>
            var totalNewItems = 0;
            function selectList() {
                let selectList = document.getElementById("select_list");
                let id = selectList.options[selectList.selectedIndex].value;
                if (id !== '-1') {
                    self.location.href = "TriggerListAddEdit.jsp?triggerListId=" + id;
                } else {
                    self.location.href = "TriggerListAddEdit.jsp";
                }
            }
            
            function addListItem() {
                let itemTable = document.getElementById("triggerListItemTable");
                let row = itemTable.insertRow(-1);
                row.id = "newTriggerListRow_" + totalNewItems;
                row.name = "newTriggerListRow_" + totalNewItems;
                
                let inputCell = row.insertCell(0);
                
                //Creates a text input element and appends it to the new list item row
                let input = document.createElement("input");
                input.type = "text";
                input.maxLength = 60;
                input.id = "newTriggerListItem_" + totalNewItems;
                input.name = "newTriggerListItem_" + totalNewItems;
                inputCell.appendChild(input);
                
                let removeCell = row.insertCell(1);

                let removeButton = createButton (function() {removeCondition(row.id);}, 'glyphicon glyphicon-remove');
                removeButton.className = "btn btn-danger";
                removeCell.appendChild(removeButton);
                
                totalNewItems++;
            }

            function createButton (onClickFunction, glyphiconClass) {
                let button = document.createElement("button");
                button.addEventListener ('click', onClickFunction);
                button.type = 'button';

                let glyphiconSpan = document.createElement("span");
                glyphiconSpan.className = glyphiconClass;
                button.appendChild(glyphiconSpan);
                return button;
            }
            
            function removeListItem(rowId) {
                let itemTable = document.getElementById("triggerListItemTable");
                let row = itemTable.rows[rowId];
                itemTable.deleteRow(row.rowIndex);
            }
        </script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
    <h4><bean:message key="admin.admin.triggerList"/></h4>
    <a href="TriggerSettings.jsp" class="btn btn-primary">Back</a>
    <div style="float: right;">
        Load List: <select id="select_list" name="select_list" onchange="selectList()">
            <option disabled hidden <%=StringUtils.trimToEmpty(triggerListId).equals("") ? "selected" : ""%> value="">...</option>
            <option value="-1">[Create New List]</option>
            <%
                for (TriggerList tList : triggerListList) {
            %>
            <option value="<%=tList.getId()%>" <%=triggerListId.equals(tList.getId().toString()) ? "selected" : ""%>><%=Encode.forHtmlContent(StringUtils.trimToEmpty(tList.getName()))%></option>
            <%
                }
            %>
        </select>
    </div>
    <br>
    <br>
    <form name="TriggerListForm" method="post" action="TriggerListAddEdit.jsp">
        <input type="hidden" name="dboperation" value=""/>
        <input type="hidden" name="triggerListId" id="triggerListId" value="<%=triggerListId%>">
        List Name: <input type="text" maxlength=60 placeholder="New List Name" name="triggerListName" id="triggerListName" value="<%=StringUtils.trimToEmpty(triggerList.getName())%>">
        <table id="triggerListItemTable" name="triggerListItemTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <th></th>
            <th></th>
            <%
                for (TriggerListItem triggerListItem : triggerList.getTriggerListItems()) {
            %>
                    <tr id="oldTriggerListRow_<%=triggerListItem.getId()%>" name="oldTriggerListRow_<%=triggerListItem.getId()%>">
                        <td>
                            <input type="text" maxlength=60 id="oldTriggerListItem_<%=triggerListItem.getId()%>" name="oldTriggerListItem_<%=triggerListItem.getId()%>" value="<%=StringUtils.trimToEmpty(triggerListItem.getItemValue())%>">
                        </td>
                        <td>
                            <button type="button" class="btn btn-danger" onclick="removeListItem('oldTriggerListRow_<%=triggerListItem.getId()%>')"><span class="glyphicon glyphicon-remove"></span></button>
                        </td>
                    </tr>
            <%
                }
            %>
            </tbody>
        </table>
        <input type="button" class="btn btn-primary" onclick="addListItem()" value="Add Item">
        <input id="addBtn" type="button" class="btn btn-primary" onclick="document.forms['TriggerListForm'].dboperation.value='Save'; document.forms['TriggerListForm'].submit();" name="saveTriggerListForm" value="Save"/>
        <input id="dltButton" type="button" class="btn btn-primary" onclick="if (confirm('Are you sure you want to delete this list?')) {document.forms['TriggerListForm'].dboperation.value='Delete'; document.forms['TriggerListForm'].submit();}" name="deleteTriggerListForm" value="Delete List"/>
    </form>
    </body>
</html:html>