<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.TriggerDao" %>
<%@ page import="java.util.List" %>
<%@ page import="org.oscarehr.common.model.Trigger" %>
<%@ page import="org.oscarehr.common.dao.TriggerLogDao" %>
<%@ page import="org.oscarehr.common.model.TriggerLog" %>
<%@ page import="org.apache.commons.lang3.math.NumberUtils" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.common.model.TriggerType" %>
<%@ page import="org.oscarehr.common.model.TriggerActionType" %>
<%@ page import="org.oscarehr.common.model.TriggerConditionType" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="org.oscarehr.common.model.Hl7TextInfo" %>
<%@ page import="org.oscarehr.common.dao.Hl7TextInfoDao" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    SystemPreferences reloadPreference = SystemPreferencesUtils.findPreferenceByName("kai_username");
    if (!(reloadPreference != null && loggedInInfo != null && loggedInInfo.getLoggedInSecurity() != null &&
            StringUtils.trimToEmpty(reloadPreference.getValue()).equalsIgnoreCase(loggedInInfo.getLoggedInSecurity().getUserName()))) {
        response.sendRedirect("../logout.jsp");
    }
    TriggerDao triggerDao = SpringUtils.getBean(TriggerDao.class);
    ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
    TriggerLogDao triggerLogDao = SpringUtils.getBean(TriggerLogDao.class);
    Hl7TextInfoDao hl7TextInfoDao = SpringUtils.getBean(Hl7TextInfoDao.class);
    
    int totalLogs = triggerLogDao.getCount();
    int pageTotal = totalLogs / 20;
    if (totalLogs % 20 != 0) {
        pageTotal++;
    }

    int pageNum = NumberUtils.isParsable(request.getParameter("page")) ? Integer.parseInt(request.getParameter("page")) : 0;
    if (pageNum >= pageTotal) {
        pageNum = pageTotal - 1;
    }
    if (pageNum < 0) {
        pageNum = 0;
    }

    List<TriggerLog> triggerLogList = triggerLogDao.findAllByPage(pageNum);
    int pagesStart = pageNum-2;
    int pagesEnd = pageNum+2;
    if (pagesStart < 0) {
        pagesStart = 0;
    }
%>

<html:html locale="true">
    <head>
        <title><bean:message key="admin.admin.triggerLogs"/></title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
        <script>
            function reportWindow(page) {
                windowprops="height=660, width=960, location=no, scrollbars=yes, menubars=no, toolbars=no, resizable=yes, top=0, left=0";
                var popup = window.open(page, "labreport", windowprops);
                popup.focus();
            }
            function goTo() {
                let goToInput = document.getElementById("go_to_input");
                let page = parseInt(goToInput.value) - 1;
                self.location.href = "TriggerLogs.jsp?page=" + page;
            }
        </script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
    <h4><bean:message key="admin.admin.triggerLogs"/></h4>
    <a href="TriggerSettings.jsp" class="btn btn-primary">Back</a>
    <br>
    <br>
    <a href="TriggerLogs.jsp?page=0" class="btn btn-primary">First</a>
    <a href="TriggerLogs.jsp?page=<%=pageNum-1%>" class="btn btn-primary" <%=pageNum > 1 ? "" : "disabled onclick=\"return false;\""%>>Prev</a>
    <%=pageNum > 1 ? "..." : ""%>
    <%
        for (int i = pagesStart; i <= pagesEnd && i < pageTotal; i++) {
    %>
    <a href="TriggerLogs.jsp?page=<%=i%>" class="<%=i == pageNum ? "btn btn-danger" : "btn btn-primary"%>"><%=i+1%></a>
    <%
        }
    %>
    <%=pageNum+1 < pageTotal ? "..." : ""%>
    <a href="TriggerLogs.jsp?page=<%=pageNum+1%>" class="btn btn-primary" <%=pageNum+1 < pageTotal ? "" : "disabled onclick=\"return false;\""%>>Next</a>
    <a href="TriggerLogs.jsp?page=<%=pageTotal-1%>" class="btn btn-primary">Last</a>
    <table id="triggerLogTable" class="table table-bordered table-striped table-hover table-condensed">
        <tbody>
        <th>Trigger Id</th>
        <th>Trigger Name</th>
        <th>Action</th>
        <th>Value</th>
        <th>Trigger Type</th>
        <th>Content</th>
        <th>Time Stamp</th>
        <th></th>
        <%
            for (TriggerLog triggerLog : triggerLogList) {
        %>
                <tr>
        <%
                Trigger trigger = triggerDao.find(triggerLog.getTriggerId());
                TriggerType triggerType = TriggerType.getByKey(triggerLog.getTriggerType());
                TriggerActionType triggerActionType = TriggerActionType.getByKey(triggerLog.getTriggerAction());
                if (trigger != null && triggerType != null && triggerActionType != null) {
                    String errorMessage = "";
                    String value = triggerLog.getTriggerActionValue();
                    switch (triggerActionType.getValueType()) {
                        case KEY_VALUE:
                            if (value.indexOf("||") > 0) {
                                String[] splitValue = value.split("\\|\\|");
                                TriggerConditionType triggerConditionType = TriggerConditionType.getByKey(splitValue[0]);
                                value = (triggerConditionType != null ? triggerConditionType.getDisplayName() : splitValue[0]) + ": " + (splitValue.length > 1 ? splitValue[1] : "");
                            }
                            break;
                        case PROVIDER:
                            value = providerDao.getProviderName(triggerLog.getTriggerActionValue());
                            break;
                    }
                    if (triggerLog.getTimeStamp().compareTo(trigger.getLastUpdatedDate()) < 0) {
                        errorMessage += "WARNING: The original trigger has been updated since this action was completed.<br>";
                    }
                    if (trigger.getTriggerStatus().equals("D")) {
                        errorMessage += "WARNING: The original trigger has been removed since this action was completed.<br>";
                    }
                    String contentId = triggerLog.getContentId();
                    switch (triggerType) {
                        case INCOMING_LAB:
                            Hl7TextInfo hl7TextInfo = hl7TextInfoDao.find(NumberUtils.isParsable(triggerLog.getContentId()) ? Integer.parseInt(triggerLog.getContentId()) : -1);
                            if (hl7TextInfo != null) {
                                String label = hl7TextInfo.getLabelOrDiscipline();
                                if (label != null && label.length() > 20) {
                                    label = label.substring(0, 20) + "...";
                                } else if (label == null) {
                                    label = hl7TextInfo.getId().toString();
                                }
                                contentId = "<a href=\"#\" onclick=\"reportWindow('../lab/CA/ALL/labDisplay.jsp?segmentID=" + hl7TextInfo.getLabNumber() + "')\">" + label + "</a>";
                            }
                            break;
                    }
        %>
                    <td>
                        <%=Encode.forHtmlContent(StringUtils.trimToEmpty(String.valueOf(triggerLog.getTriggerId())))%>
                    </td>
                    <td>
                        <%=Encode.forHtmlContent(StringUtils.trimToEmpty(trigger.getName()))%>
                    </td>
                    <td>
                        <%=Encode.forHtmlContent(triggerActionType.getDisplayName())%>
                    </td>
                    <td>
                        <%=Encode.forHtmlContent(value)%>
                    </td>
                    <td>
                        <%=Encode.forHtmlContent(triggerType.getDisplayName())%>
                    </td>
                    <td>
                        <%=contentId%>
                    </td>
                    <td>
                        <%=triggerLog.getTimeStamp().toString()%>
                    </td>
                    <td>
                        <div style="color: red">
                            <%=errorMessage%>
                        </div>
                    </td>
        <%
                }
        %>
                </tr>
        <%
            }
        %>
        </tbody>
    </table>
    <a href="TriggerLogs.jsp?page=0" class="btn btn-primary">First</a>
    <a href="TriggerLogs.jsp?page=<%=pageNum-1%>" class="btn btn-primary" <%=pageNum > 1 ? "" : "disabled onclick=\"return false;\""%>>Prev</a>
    <%=pageNum > 1 ? "..." : ""%>
    <%
        for (int i = pagesStart; i <= pagesEnd && i < pageTotal; i++) {
    %>
        <a href="TriggerLogs.jsp?page=<%=i%>" class="<%=i == pageNum ? "btn btn-danger" : "btn btn-primary"%>"><%=i+1%></a>
    <%
        }
    %>
    <%=pageNum+1 < pageTotal ? "..." : ""%>
    <a href="TriggerLogs.jsp?page=<%=pageNum+1%>" class="btn btn-primary" <%=pageNum+1 < pageTotal ? "" : "disabled onclick=\"return false;\""%>>Next</a>
    <a href="TriggerLogs.jsp?page=<%=pageTotal-1%>" class="btn btn-primary">Last</a>
    <br>
    <br>
    <button class="btn btn-primary" onclick="goTo()">Go to</button>
    <input type="number" min="1" max="<%=pageTotal%>" id="go_to_input" name="go_to_input" style="width: 30px; padding-top: 10px; padding-bottom: 10px;" value="<%=pageNum+1%>">
    </body>
</html:html>