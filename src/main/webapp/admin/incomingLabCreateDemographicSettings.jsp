<%--
	Copyright (c) 2021 WELL EMR Group Inc.
	This software is made available under the terms of the
	GNU General Public License, Version 2, 1991 (GPLv2).
	License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
	String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="java.util.Date" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
	SystemPreferences preference = SystemPreferencesUtils.findPreferenceByName("createDemographicOnUnmatchedLab");
	if (request.getParameter("dboperation") != null && !request.getParameter("dboperation").isEmpty() && request.getParameter("dboperation").equals("Save")) {
		String newValue = request.getParameter("createDemographicOnUnmatchedLab");
		if (preference != null) {
			if (!preference.getValue().equals(newValue)) {
				preference.setUpdateDate(new Date());
				preference.setValue(newValue);
				SystemPreferencesUtils.merge(preference);
			}
		} else {
			preference = new SystemPreferences();
			preference.setName("createDemographicOnUnmatchedLab");
			preference.setUpdateDate(new Date());
			preference.setValue(newValue);
			SystemPreferencesUtils.persist(preference);
		}
	}
	
	String preferenceValue = preference == null ? "off" : preference.getValue();
%>

<html:html locale="true">
	<head>
		<title><bean:message key="admin.admin.incomingLabCreateDemographicSettings"/></title>
		<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
		<link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

		<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
		<script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
		<script type="text/javascript">
			function save() {
				document.forms['incomingLabCreateDemographicForm'].dboperation.value='Save';
				document.forms['incomingLabCreateDemographicForm'].submit();
			}
		</script>
	</head>


	<body vlink="#0000FF" class="BodyStyle">
	<h4><bean:message key="admin.admin.incomingLabCreateDemographicSettings"/></h4>
	<form name="incomingLabCreateDemographicForm" method="post" action="incomingLabCreateDemographicSettings.jsp">
		<input type="hidden" name="<csrf:tokenname/>" value="<csrf:tokenvalue/>"/>
		<input type="hidden" name="dboperation" value="Save">
		<table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
			<tbody>
			<tr>
				<td>Create demographic when incoming lab does not match an existing one: </td>
				<td>
					<select name="createDemographicOnUnmatchedLab">
						<option value="off" <%="off".equals(preferenceValue) ? "selected" : ""%>>Off</option>
						<option value="always" <%="always".equals(preferenceValue) ? "selected" : ""%>>Always</option>
						<option value="onlyOnBlankHin" <%="onlyOnBlankHin".equals(preferenceValue) ? "selected" : ""%>>Only when HIN is blank</option>
					</select>
				</td>
			</tr>
			</tbody>
		</table>

		<input type="button" onclick="save();" name="saveIncomingLabCreateDemographicSettings" value="Save"/>
	</form>
	</body>
</html:html>