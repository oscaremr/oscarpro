<%--
    Copyright (c) 2021 WELL EMR Group Inc.
    This software is made available under the terms of the
    GNU General Public License, Version 2, 1991 (GPLv2).
    License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>"
	objectName="_admin,_admin.misc" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_admin&type=_admin.misc");%>
</security:oscarSec>
<%
	if (!authed) {
		return;
	}
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<html ng-app="emailConfig">
<head>
	<title><bean:message key="admin.admin.ConfigureSMTPServer"/></title>
	<script src="<%=request.getContextPath() %>/JavaScriptServlet" type="text/javascript"></script>
	<link href="<%=request.getContextPath() %>/library/bootstrap/3.0.0/css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/alertify.core.css" type="text/css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/alertify.default.css" type="text/css">
	<script type="text/javascript" src="<%=request.getContextPath() %>/library/angular.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/alertify.js"></script>	
	<script src="<%=request.getContextPath() %>/web/common/emailServices.js"></script>	
</head>

<body vlink="#0000FF" class="BodyStyle">
	<div ng-controller="emailConfig">
		<div class="page-header">
			<h4><bean:message key="admin.admin.ConfigureSMTPServer"/></h4>
		</div>
		<div data-ng-show="emailActive">
			<form action="Know2actConfiguration.jsp"  method="POST">
				<fieldset>
					<div class="form-group col-xs-5">
					    <div class="checkbox">
                            <label>
                                <input type="checkbox" ng-model="emailConfig.enableEmail" class="ng-pristine ng-valid"> <b><bean:message key="admin.email.enableemail"/></b>
                            </label>
                        </div>

						<label><bean:message key="admin.email.servername"/></label>
						<div class="controls">
							<input class="form-control" name="serverName" ng-model="emailConfig.url" ng-required="true" type="text" maxlength="255"/>  <br/>
						</div>
						
						<label><bean:message key="admin.email.username"/></label>
						<div class="controls">
							<input class="form-control" name="userName" ng-model="emailConfig.username" ng-required="true" type="text" maxlength="255"/>  <br/>
						</div>
						
						<label><bean:message key="admin.email.password"/></label>
						<div class="controls">
							<input class="form-control" name="password" ng-model="emailConfig.password" ng-required="true" type="password" maxlength="255"/>  
						</div>
						
						<div class="checkbox">
				    		<label>
				      			<input type="checkbox" ng-model="emailConfig.useSecurity" class="ng-pristine ng-valid"> <b><bean:message key="admin.email.usetlsssl"/></b>
				    		</label>
				  		</div>
						
						<label><bean:message key="admin.email.portnumber"/></label>
						<div class="controls">
							<input class="form-control" name="port" ng-model="emailConfig.port" ng-required="true" type="number" maxlength="255"/>  <br/>
						</div>
						
						<label><bean:message key="admin.email.replytoemail"/></label>
						<div class="controls">
							<input class="form-control" name="replyToEmail" ng-model="emailConfig.replyToEmail" ng-required="true" type="email" maxlength="255"/>  <br/>
						</div>
						
						<label><bean:message key="admin.email.displayname"/></label>
						<div class="controls">
							<input class="form-control" name="displayName" ng-model="emailConfig.displayName" type="text" maxlength="255"/>  <br/>
						</div>
						<label>Signature</label>
						<div class="controls">
							<textarea class="form-control" style="resize: none;" name="signature" ng-model="emailConfig.signature" rows="5" cols="30"></textarea>  <br/>
						</div>
						<div>
							<p class="text-muted"><bean:message key="admin.email.testmsg"/></em></p>
						</div>
						<div>
							<input type="button" class="btn btn-default" value="Test"  ng-click="test()"/>
							<input type="button" class="btn btn-primary" value="Save"  ng-click="save()"/>
							<input type="button" class="btn btn-default" value="Clear"  ng-click="clear()"/>
							<input type="button" class="btn btn-default" value="Cancel"  ng-click="cancel()"/>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
	
	<script>
		var app = angular.module("emailConfig", ['emailServices']);		
		app.controller("emailConfig", function($scope, emailService) {
			
			$scope.emailConfig = newConfig();
			$scope.emailActive = true;
			
			emailService.getEmailConfig().then(function(emailConfig) {
		        if (emailConfig == null || emailConfig == '') {
		            emailConfig = newConfig();
		        }
		        $scope.origEmailConfig = angular.copy(emailConfig);
		        $scope.emailConfig = emailConfig;
		    });
			
			$scope.save = function() {
				let inputErrors = validateInput();
		        if (inputErrors.length === 0) {
		        	emailService.saveEmailConfig($scope.emailConfig).then(function (emailConfig) {
		                // alerts the user that the config is saved
		                $scope.origEmailConfig = angular.copy(emailConfig);
		                $scope.emailConfig = emailConfig;
		                alertify.success('Configuration has been successfully saved');
		            }, function (response) {
		                if (response.data.error != null) {
		                    alertify.error(response.data.error);
		                } else {
		                    alertify.error('An error occurred while saving the configuration');
		                }
		            });
		        } else {
		            alert(inputErrors);
		        }
			};
			
			$scope.test = function() {	
				let inputErrors = validateInput();
		        if (inputErrors.length === 0) {
		        	emailService.testEmailConfig($scope.emailConfig).then(function (emailConfig) {
		                alertify.success('A test email has been sent to the Reply to Email address');
		            }, function (response) {
		                if (response.data.error != null) {
		                    alertify.error(response.data.error);
		                } else {
		                    alertify.error('Configuration unsuccessful. Please check the settings');
		                }
		            });
		        } else {
		            alert(inputErrors);
		        }
			};
			
			$scope.cancel = function() {
				$scope.emailConfig = angular.copy($scope.origEmailConfig);
		    };
		    
		    $scope.clear = function() {
		    	$scope.emailConfig = newConfig();
		    };
			
			function newConfig() {
		        return {
					id: 0,
		            url: '',
		            username: '',
		            password: '',
		            port: 587,
		            useSecurity: false,
		            replyToEmail: '',
		            displayName: '',
		            signature: ''
		        };
		    }
			
			function validateInput() {

		        let errorMessage = '';

		        //check required fields
		        if ($scope.emailConfig.url == null || $scope.emailConfig.url == "") {
		            errorMessage += "Server name is required\n";
		        }
		        if ($scope.emailConfig.username == null || $scope.emailConfig.username == "") {
		            errorMessage += "Username is required\n";
		        }
		        if ($scope.emailConfig.password == null || $scope.emailConfig.password == "") {
		            errorMessage += "Password is required\n";
		        }
		        if ($scope.emailConfig.port == null || $scope.emailConfig.port == "") {
		            errorMessage += "Port is required\n";
		        }
		        if ($scope.emailConfig.replyToEmail == null || $scope.emailConfig.replyToEmail == "") {
		            errorMessage += "Reply to email address is required\n";
		        }

		        return errorMessage;
		    }
		});
	
	</script>
</html>
