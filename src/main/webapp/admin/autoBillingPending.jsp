<!DOCTYPE html>
<%--

    Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

--%>
<%@ page import="java.util.*,oscar.oscarReport.data.*, oscar.oscarBilling.ca.on.administration.*"%>
<%@ page import="org.oscarehr.common.dao.BillingONCHeader1Dao, org.oscarehr.common.model.BillingONCHeader1, org.oscarehr.common.model.BillingONItem"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.Date, java.text.SimpleDateFormat" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.billing" rights="w" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_admin&type=_admin.billing");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}


BillingONCHeader1Dao cheader1Dao = (BillingONCHeader1Dao) SpringUtils.getBean("billingONCHeader1Dao");
List<BillingONCHeader1> billings = cheader1Dao.getBillCheader1ByPendingStatus();

%>

<html ng-app="billingAutoPendingConfig">

<head>
<title>Pending Auto Billings</title>

<script type="text/javascript" src="<%=request.getContextPath() %>/library/angular.min.js"></script>
<script src="<%=request.getContextPath() %>/web/common/billingServices.js"></script>

<link href="<%=request.getContextPath() %>/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container-fluid" ng-controller="billingAutoPendingConfig">

<!-- // PAGE TITLE -->
<div class="page-header">
<h3>Pending Auto Billing</h3>
</div>


    <table class="table table-bordered table-hover">
      <thead>
      <tr>
        <th><input type="checkbox" id="checkControl" ng-model="allChecked" ng-click="stateCheck()" title="Check all"></th>
      	<th>Service Date</th>
      	<th>Time</th>
      	<th>Patient</th>
      	<th>Appointment</th>
      	<th>Service Code</th>
      	<th>DX</th>
      	<th>SLI</th>
      	<th>VL</th>
      	<th>Billing Physician</th>
      	<th>Referral Physician</th>
      	<th></th>
      </tr>
      </thead>

      <tbody id="pendingItems">
 <%
 //Move to ws and load with angular
 if(billings.size()>0){

 for(BillingONCHeader1 bill:billings){

	String service_codes = "";
	String dx_codes = "";
   	List<BillingONItem> items = bill.getBillingItems();
   	for(BillingONItem item:items){
   		service_codes += item.getServiceCode() + " x " +item.getServiceCount();
   		dx_codes += item.getDx();
   	}

   	//SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
   	Date serviceDate = bill.getBillingDate();
   	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    String billing_date = formatter.format(serviceDate);

   	Date serviceTime = bill.getBillingTime() ;
   	SimpleDateFormat formatter_time = new SimpleDateFormat("HH:mm:ss a");
   	String billing_time = formatter_time.format(serviceTime);
 %>

      <tr id="invoice-<%=bill.getId()%>">
        <!--TODO: Check if wanted => td style="text-align:center"><button class="btn" type="button" title="edit" onClick="popupPage(600,800, '../billing/CA/ON/billingONCorrection.jsp?billing_no=<%=bill.getId()%>')"><i class="icon-edit"></i></button></td-->
				<td><input type="checkbox" id="check-<%=bill.getId()%>" value="<%=bill.getId()%>" ng-click="stateCheck()"></td>
      	<td><%=billing_date%></td>
      	<td><%=billing_time%></td>
      	<td><%=bill.getDemographicName() %></td>
      	<td><%=bill.getAppointmentNo() %></td>
      	<td><%=service_codes %></td>
      	<td><%=dx_codes %></td>
      	<td></td>
      	<td><%=bill.getFaciltyNum() %></td>
      	<td><%=bill.getProviderNo() %></td>
      	<td><%=bill.getRefNum() %></td>
      	<td style="text-align:center">
      	<button role="button" class="btn" ng-click="deleteBilling('<%=bill.getId()%>')" title="delete"><i class="icon-trash"></i></button>
      	<button role="button" class="btn" ng-click="approveBilling('<%=bill.getId()%>')" title="approve"><i class="icon-check"></i></button>
      	</td>
      </tr>
<%} //for loop

 }
%>
      </tbody>

      <tfoot>
      <tr>
      	<td colspan="12"><button role="button" class="btn" ng-click="deleteBulkAction()" ng-disabled="!enableBulkAction"><i class="icon-trash"></i> Delete</button> <button role="button" class="btn" ng-click="approveBulkAction()" ng-disabled="!enableBulkAction"><i class="icon-check"></i> Approve</button></td>
      </tr>
      </tfoot>
    </table>
</div>


<script src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script><!-- TODO: remove jquery and use strictly angular -->
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>

<script>
var app = angular.module("billingAutoPendingConfig", ['billingServices']);
app.controller("billingAutoPendingConfig", function($scope,$http,billingService) {

$scope.enableBulkAction = false;

var bulkItems = [];
$scope.deleteBulkAction = function(){
	getCheckBoxValues();
	angular.forEach(bulkItems, function(value, key) {
		$scope.deleteBilling(value);
	});

  bulkActionCleanUp();
}

$scope.approveBulkAction = function(){
	getCheckBoxValues();
	angular.forEach(bulkItems, function(value, key) {
		$scope.approveBilling(value);
	});

  bulkActionCleanUp();
}

function bulkActionCleanUp(){
	$scope.enableBulkAction=false;
	bulkItems = [];
	$('#checkControl').attr('title', 'Check all');
}

$scope.approveBilling = function($id){
		billingService.approveBilling($id).then(function(data){
			$scope.returnMessage = data;
			if(data.success){
				console.log(data.message);
				$('#invoice-'+$id).remove();
				countRows();
			}else{
				$('#invoice-'+$id).addClass('error');
				console.log("Error trying to approve invoice: " + data.message);
			}
		});
};


$scope.deleteBilling = function($id){
	billingService.deleteBilling($id).then(function(data){
		$scope.returnMessage = data;
		if(data.success){
			console.log(data.message);
			$('#invoice-'+$id).remove();
			countRows();
		}else{
			$('#invoice-'+$id).addClass('error');
			console.log("Error trying to approve invoice: " + data.message);
		}
	});
};

function countRows(){
	if($('table tbody tr').length==0){
		$('table tbody').html("<tr><td colspan=\"12\">No auto generated invoices pending.</td></tr>");
		$('#checkControl').attr("disabled",true);
	}else{
		$('#checkControl').removeAttr("disabled");
	}
}

function getCheckBoxValues(){
			$('#pendingItems input[type=checkbox]').each(function () {
				if($(this).prop("checked")){
					bulkItems.push($(this).val());
			  }
			});
}

$scope.stateCheck = function(){
	var i = 0;
	$('#pendingItems input[type=checkbox]').each(function () {
		if($(this).prop("checked")){
			i++;
			$scope.enableBulkAction = true;
			$('#checkControl').attr('title', 'Uncheck all');
		}
	});

	if(i==0)
  bulkActionCleanUp();
}
countRows();
});

$('#checkControl').click(function(){
	checkState = $(this).prop("checked");
	$('#pendingItems input[type=checkbox]').each(function () {
		$(this).attr('checked',checkState);
	});
});


//open a new popup window
function popupPage(vheight,vwidth,varpage) {
  var page = "" + varpage;
  windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes";
  var popup=window.open(page, "attachment", windowprops);
  if (popup != null) {
    if (popup.opener == null) {
      popup.opener = self;
    }
  }
}
</script>
</body>
</html>
