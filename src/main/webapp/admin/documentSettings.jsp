<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.quatro.model.security.Secuserrole" %>
<%@ page import="com.quatro.dao.security.SecuserroleDao" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    SecuserroleDao secUserRoleDao = SpringUtils.getBean(SecuserroleDao.class);
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    Map<String, SystemPreferences> preferenceMap = SystemPreferencesUtils.findByKeysAsPreferenceMap(SystemPreferences.DOCUMENT_SETTINGS_KEYS);
    boolean isAdmin = secUserRoleDao.isUserAdmin(loggedInInfo.getLoggedInProviderNo());
    
    if (request.getParameter("dboperation") != null && !request.getParameter("dboperation").isEmpty() && request.getParameter("dboperation").equals("Save")) {
        for (String key : SystemPreferences.DOCUMENT_SETTINGS_KEYS) {
            SystemPreferences preference = preferenceMap.get(key);
            
            String newValue = request.getParameter(key);
            if (preference != null) {
                if (!preference.getValue().equals(newValue)) {
                    preference.setUpdateDate(new Date());
                    preference.setValue(newValue);
                    SystemPreferencesUtils.merge(preference);
                }
            } else {
                preference = new SystemPreferences();
                preference.setName(key);
                preference.setUpdateDate(new Date());
                preference.setValue(newValue);
                SystemPreferencesUtils.persist(preference);
            }
            
            // Updates the preference in the preference map
            preferenceMap.put(key, preference);
        }
    }
%>

<html:html locale="true">
    <head>
        <title>Document Settings</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
    </head>

    <%
        for (SystemPreferences preference : preferenceMap.values()) {
            dataBean.setProperty(preference.getName(), preference.getValue());
        }
    %>

    <body vlink="#0000FF" class="BodyStyle">
    <h4>Manage Document Settings</h4>
    <form name="documentSettingsForm" method="post" action="documentSettings.jsp">
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <td>Autocomplete Document Descriptions: </td>
                <td>
                    <input id="document_description_typeahead-true" type="radio" value="true" name="document_description_typeahead"
                            <%=(dataBean.getProperty("document_description_typeahead", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="document_description_typeahead-false" type="radio" value="false" name="document_description_typeahead"
                            <%=(dataBean.getProperty("document_description_typeahead", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Use dropdown for document faxing in the inbox: </td>
                <td>
                    <input id="inbox_use_fax_dropdown" type="checkbox" value="true" name="inbox_use_fax_dropdown"
                            <%=(dataBean.getProperty("inbox_use_fax_dropdown", "false").equals("true")) ? "checked='checked'" : ""%> />
                </td>
            </tr>
            <tr>
                <td>Select default value to display under Discipline column in inbox:</td>
                <td>
                    <%
                        String selectedOption = dataBean.getProperty("document_discipline_column_display", "document_type");
                    %>
                    <select id="document_discipline_column_display" name="document_discipline_column_display">
                        <option value="document_description" <%=selectedOption.equals("document_description") ? "selected=\"selected\"" : ""%>>
                            Document Description
                        </option>
                        <option value="document_type" <%=selectedOption.equals("document_type") ? "selected=\"selected\"" : ""%>>
                            Document Type
                        </option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Open new window when splitting documents: </td>
                <td>
                    <input id="split_new_window-true" type="radio" value="true" name="split_new_window"
                            <%=(dataBean.getProperty("split_new_window", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="split_new_window-false" type="radio" value="false" name="split_new_window"
                            <%=(dataBean.getProperty("split_new_window", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <% if (isAdmin) { %>
	            <tr>
	                <td>Display 'Add New' doc types in eChart and eDoc: </td>
	                <td>
	                    <input id="add_new_document_type-true" type="radio" value="true" name="add_new_button_enabled"
	                            <%= (dataBean.getProperty("add_new_button_enabled", "true").equals("true")) ? "checked" : "" %> />
	                    Yes
	                    &nbsp;&nbsp;&nbsp;
	                    <input id="add_new_document_type-false" type="radio" value="false" name="add_new_button_enabled"
	                            <%= (dataBean.getProperty("add_new_button_enabled", "true").equals("false")) ? "checked" : "" %> />
	                    No
	                    &nbsp;&nbsp;&nbsp;
	                </td>
	            </tr>
            <% } %>
            <tr>
                <td>Allow faxing of unassigned documents: </td>
                <td>
                    <input id="allow_faxing_unassigned_documents-true" type="radio" value="true" name="allow_faxing_unassigned_documents"
                        <%= (dataBean.getProperty("allow_faxing_unassigned_documents", "true").equals("true")) ? "checked" : "" %> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="allow_faxing_unassigned_documents-false" type="radio" value="false" name="allow_faxing_unassigned_documents"
                        <%= (dataBean.getProperty("allow_faxing_unassigned_documents", "true").equals("false")) ? "checked" : "" %> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Display extra prompt if MRP is removed from Document: </td>
                <td>
                    <input type="radio" value="true" name="display_prompt_mrp_remove"
                        <%= Boolean.parseBoolean(dataBean.getProperty("display_prompt_mrp_remove", "true")) ? "checked" : "" %> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="display_prompt_mrp_remove"
                        <%= !Boolean.parseBoolean(dataBean.getProperty("display_prompt_mrp_remove", "true")) ? "checked" : "" %> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            </tbody>
        </table>

        <input type="button" onclick="document.forms['documentSettingsForm'].dboperation.value='Save'; document.forms['documentSettingsForm'].submit();" name="saveDocumentSettings" value="Save"/>
    </form>
    </body>
</html:html>