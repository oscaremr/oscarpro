<!DOCTYPE html>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
	String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.encounter" rights="w" reverse="true">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_admin,_admin.encounter");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
	String curUser_no = (String) session.getAttribute("user");
%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="oscar.form.dao.SmartEncounterTemplateDao" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="oscar.form.dao.SmartEncounterShortCodeDao" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.PropertyDao" %>
<%@ page import="org.oscarehr.common.model.Property" %>
<%@ page import="oscar.form.dao.SmartEncounterHeaderDao" %>
<%@ page import="oscar.form.model.*" %>
<%@ page import="oscar.form.dao.SmartEncounterFooterDao" %>
<%@ page import="oscar.form.pageUtil.SmartEncounterUtil" %>
<%@ page import="java.util.Set" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%

	SmartEncounterTemplateDao smartEncounterTemplateDao = SpringUtils.getBean(SmartEncounterTemplateDao.class);
	List<SmartEncounterTemplate> templateList = smartEncounterTemplateDao.findAllActiveOrderByName();
	SmartEncounterShortCodeDao smartEncounterShortCodeDao = SpringUtils.getBean(SmartEncounterShortCodeDao.class);
	List<SmartEncounterShortCode> shortCodeList = smartEncounterShortCodeDao.findAllOrderByName();
	SmartEncounterHeaderDao smartEncounterHeaderDao = SpringUtils.getBean(SmartEncounterHeaderDao.class);
	List<SmartEncounterHeader> headerList = smartEncounterHeaderDao.findAllOrderByName();
	SmartEncounterFooterDao smartEncounterFooterDao = SpringUtils.getBean(SmartEncounterFooterDao.class);
	List<SmartEncounterFooter> footerList = smartEncounterFooterDao.findAllOrderByName();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	List<SmartEncounterTemplatePlaceholder> templatePlaceholderList = Arrays.asList(SmartEncounterTemplatePlaceholder.values());


    PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
    Property defaultSmartEncounterTemplate = propertyDao.checkByName("default_smart_encounter_template");
    Property defaultSmartEncounterHeader = propertyDao.checkByName("default_smart_encounter_header");
    Property defaultSmartEncounterFooter = propertyDao.checkByName("default_smart_encounter_footer");

	String fileUploadErrorMessage = SmartEncounterUtil.createErrorMessage(request);
	if (StringUtils.isNotBlank(fileUploadErrorMessage)) {
		out.println("<script>alert('" + fileUploadErrorMessage + "');</script>");
	}
	DatabaseTagService databaseTagService = new DatabaseTagService();
	databaseTagService.addDefaultDatabaseTags();
	databaseTagService.addCustomDatabaseTags();
	Set<DatabaseTag> databaseTagSet = databaseTagService.getDatabaseTagMethodMap().keySet();
%>

<html:html locale="true">
	<head>
		<meta charset="utf-8">
		<title>Smart Template Form Settings</title>

		<script type="text/javascript" src="<%=request.getContextPath()%>/js/quill/quill.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-editor.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-placeholder.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-block-placeholder.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-image-placeholder.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-register-inline-styles.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-ui-1.10.2.custom.min.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath() %>/share/javascript/eforms/APCache.js"></script>
	
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/quill/quill.snow.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-editor.css"/>	
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/jquery_css/smoothness/jquery-ui-1.7.3.custom.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/yui/css/fonts-min.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/yui/css/autocomplete.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/css/demographicProviderAutocomplete.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/bulma/bulma-trimmed.css"/>
	</head>
	<style>
		.table tbody tr:hover {
			cursor: inherit;
			box-shadow: 10px 10px 24px 0 #CDDADF;
		}
	</style>
	<script type="text/javascript">
		function init() {
			// resize window
			window.resizeTo(1150, 800);
		}

		<%
		List<String> jsTemplates = new ArrayList<String>();
		for (SmartEncounterTemplate template : templateList) {
			List<String> imageJson = new ArrayList<String>();
			for (SmartEncounterTemplateImage smartEncounterTemplateImage : template.getPlaceholderImageList()) {
				imageJson.add("'" + smartEncounterTemplateImage.getName() + "' : '" + smartEncounterTemplateImage.getValue() + "'");
			}
			String deltaText = template.getTemplate();
			// replace image placeholders with proper urls
			for (SmartEncounterTemplateImage placeHolderImage : template.getPlaceholderImageList()) {
				deltaText = deltaText.replaceAll("\\$\\{" + placeHolderImage.getName() + "}", request.getContextPath() + "/eform/displayImage.do?smartEncounterFormImage=true&imagefile=" + placeHolderImage.getValue());
			}
			jsTemplates.add("[" + template.getId() + ", { 'name':'" + Encode.forJavaScriptBlock(template.getName()) + "', 'template': " + deltaText + ", 'createDate': " + template.getCreateDate().getTime() + ", 'headerId': '" + template.getHeaderId() + "', 'footerId': '" + template.getFooterId() + "'}]");
		}
		%>
		let templatesMap = new Map([<%=StringUtils.join(jsTemplates, ",")%>]);
		<%
		List<String> jsPlaceholder = new ArrayList<String>();
		for (DatabaseTag templatePlaceholder : databaseTagSet) {
			jsPlaceholder.add("['" + templatePlaceholder.getSubjectAndField() + "', " 
				+ "\"" + templatePlaceholder.toJavascriptCreateMethod() + "\"]");
		}
		%>
		let placeholderMap = new Map([<%=StringUtils.join(jsPlaceholder, ",")%>]);
		<%
		List<String> jsShortCodes = new ArrayList<String>();
		for (SmartEncounterShortCode shortCode : shortCodeList) {
			jsShortCodes.add("[" + shortCode.getId() + ", { 'name':'" + Encode.forJavaScriptBlock(shortCode.getName()) + "', 'text':" + shortCode.getText() + ", 'createDate': " + shortCode.getCreateDate().getTime() + "}]");
		}
		%>
		let shortCodesMap = new Map([<%=StringUtils.join(jsShortCodes, ",")%>]);
		<%
		List<String> jsHeaders = new ArrayList<String>();
		for (SmartEncounterHeader header : headerList) {
			jsHeaders.add("[" + header.getId() + ", { 'name':'" + Encode.forJavaScriptBlock(header.getName()) +
					"', 'text':" + header.getText() + ", 'createDate': " + header.getCreateDate().getTime() + "}]");
		}
		%>
		let headersMap = new Map([<%= StringUtils.join(jsHeaders, ",") %>]);

		<%
		List<String> jsFooters = new ArrayList<String>();
		for (SmartEncounterFooter footer : footerList) {
			jsFooters.add("[" + footer.getId() + ", { 'name':'" + Encode.forJavaScriptBlock(footer.getName()) +
					"', 'text':" + footer.getText() + ", 'createDate': " + footer.getCreateDate().getTime() + "}]");
		}
		%>
		let footersMap = new Map([<%= StringUtils.join(jsFooters, ",") %>]);

	</script>
	<body>
	<div class="smart-template-layout-main smart-template-layout">
		<div class="container is-fluid" style="width: 100%">
			<div class="main body-inner-section">
			<div class="columns">
				<div class="column">
					<h3 style="display: inline;">Smart Template Form Settings</h3>
					<input type="button" class="button is-green is-small is-pulled-right" style="display: none;" id="backButton" onclick="onClickBack()" value="Back">
				</div>
				<div class="column">
					<html:form styleId="smartEncounterTemplateForm" action="/form/ManageSmartEncounterTemplateAction">
						<input type="hidden" name="<csrf:tokenname/>" value="<csrf:tokenvalue/>"/>
						<input type="hidden" id="method" name="method" value="">
						<input type="hidden" id="id" name="id" value="">
						<input type="hidden" id="template" name="template" value="">
						<input type="hidden" id="htmlPreview" name="htmlPreview" value="">
						<input type="hidden" id="createDateLong" name="createDateLong" value="">
						<div id="listTemplatesAndShortCodes">
							<div style="height: 30px">
								<h4 style="display: inline">Templates:</h4>
								&nbsp;&nbsp;&nbsp;&nbsp;<input class="button is-green is-small is-pulled-right" type="button" value="Add New" onclick="addNewTemplate()"/>
							</div>
							<div class="panel-1 page-content">
								<div class="panel-body" style="max-height: 350px; overflow-y: scroll;">
									<table class="table is-narrow">
										<thead>
											<tr>
												<th>Name</th>
												<th>Created</th>
												<th>Last Edited</th>
												<th>Options</th>
											</tr>
										</thead>
										<tbody>
										<% for (SmartEncounterTemplate template : templateList) { %>
										<tr>
											<td><%=template.getName()%></td>
											<td><%=sdf.format(template.getCreateDate())%></td>
											<td><%=sdf.format(template.getEditedDate())%></td>
											<td><a href="javascript:void(0);" onclick="onClickEditTemplate(<%=template.getId()%>)">Edit</a> |
												<a href="javascript:void(0);" onclick="onClickDeleteTemplate(<%=template.getId()%>)">Delete</a></td>
										</tr>
										<% } %>
										</tbody>
									</table>
								</div>
							</div>
							<div class="panel-1">
								<div class="panel-body">
									<b>Default Template on Launch</b>
									<select id="defaultTemplate" name="defaultTemplate" class="select is-primary" onchange="updateDefaultTemplate()">
										<option value="None">None</option>
										<% for (SmartEncounterTemplate template : templateList) { %>
											<option value="<%=template.getId()%>" <%=defaultSmartEncounterTemplate != null && defaultSmartEncounterTemplate.getValue() != null && defaultSmartEncounterTemplate.getValue().equals(template.getId().toString()) ? "selected" : ""%>><%=template.getName()%></option>
										<% } %>
									</select>
								</div>
							</div>
							<hr/>
							<div style="height: 30px">
								<h4 style="display: inline;">Short Codes:</h4>
								&nbsp;&nbsp;&nbsp;&nbsp;<input class="button is-green is-small is-pulled-right" type="button" value="Add New" onclick="addNewShortCode()"/>
							</div>
							<div class="panel-1 page-content">
								<div class="panel-body" style="max-height: 350px; overflow-y: scroll;">
									<table class="table" style="table-layout: fixed">
										<thead>
											<tr>
												<th>Name</th>
												<th style="width: 50%">Text</th>
												<th>Created</th>
												<th>Last Edited</th>
												<th>Options</th>
											</tr>
										</thead>
										<tbody>
										<% for (SmartEncounterShortCode shortCode : shortCodeList) { %>
										<tr id="shortCodeRowId<%=shortCode.getId()%>">
											<td><%=Encode.forHtmlAttribute(shortCode.getName())%></td>
											<td id="shortCodeId<%=shortCode.getId()%>Text" class="short-code-preview smart-template-settings-short-code-preview"><%=shortCode.getHtmlPreview()%></td>
											<td><%=sdf.format(shortCode.getCreateDate())%></td>
											<td><%=sdf.format(shortCode.getEditedDate())%></td>
											<td><a href="javascript:void(0);" onclick="onClickEditShortCode(<%=shortCode.getId()%>)">Edit</a> | 
												<a href="javascript:void(0);" onclick="onClickDeleteShortCode(<%=shortCode.getId()%>);">Delete</a></td>
										</tr>
										<% } %>
										</tbody>
									</table>
								</div>
							</div>
							<hr/>
							<div style="height: 30px">
								<h4 style="display: inline;">Headers:</h4>
								&nbsp;&nbsp;&nbsp;&nbsp;<input class="button is-green is-small is-pulled-right" type="button" value="Add New" onclick="addNewHeader()"/>
							</div>
							<div class="panel-1 page-content">
								<div class="panel-body" style="max-height: 350px; overflow-y: scroll;">
									<table class="table" style="table-layout: fixed">
										<thead>
										<tr>
											<th>Name</th>
											<th style="width: 50%">Text</th>
											<th>Created</th>
											<th>Last Edited</th>
											<th>Options</th>
										</tr>
										</thead>
										<tbody>
										<% for (SmartEncounterHeader header : headerList) { %>
										<tr id="headerRowId<%=header.getId()%>">
											<td><%= Encode.forHtmlAttribute(header.getName()) %></td>
											<td id="headerId<%= header.getId() %>Text" class="short-code-preview smart-template-settings-short-code-preview"><%= header.getHtmlPreview() %></td>
											<td><%= sdf.format(header.getCreateDate()) %></td>
											<td><%= sdf.format(header.getEditedDate()) %></td>
											<td><a href="javascript:void(0);" onclick="onClickEditHeader(<%= header.getId() %>)">Edit</a> |
												<a href="javascript:void(0);" onclick="onClickDeleteHeader(<%= header.getId() %>);">Delete</a></td>
										</tr>
										<% } %>
										</tbody>
									</table>
								</div>
							</div>
							<div class="panel-1">
								<div class="panel-body">
									<b>Default Header on Launch</b>
									<select id="defaultHeader" name="defaultHeader" class="select is-primary" onchange="updateDefaultHeader()">
										<option value="None">None</option>
										<% for (SmartEncounterHeader hader : headerList) { %>
										<option value="<%= hader.getId() %>" <%= defaultSmartEncounterHeader != null
												&& defaultSmartEncounterHeader.getValue() != null
												&& defaultSmartEncounterHeader.getValue().equals(hader.getId().toString()) ? "selected" : "" %>>
												<%= hader.getName() %></option>
										<% } %>
									</select>
								</div>
							</div>
							<hr/>
							<div style="height: 30px">
								<h4 style="display: inline;">Footers:</h4>
								&nbsp;&nbsp;&nbsp;&nbsp;<input class="button is-green is-small is-pulled-right" type="button" value="Add New" onclick="addNewFooter()"/>
							</div>
							<div class="panel-1 page-content">
								<div class="panel-body" style="max-height: 350px; overflow-y: scroll;">
									<table class="table" style="table-layout: fixed">
										<thead>
										<tr>
											<th>Name</th>
											<th style="width: 50%">Text</th>
											<th>Created</th>
											<th>Last Edited</th>
											<th>Options</th>
										</tr>
										</thead>
										<tbody>
										<% for (SmartEncounterFooter footer : footerList) { %>
										<tr id="footerRowId<%= footer.getId() %>">
											<td><%= Encode.forHtmlAttribute(footer.getName()) %></td>
											<td id="footerId<%= footer.getId() %>Text" class="short-code-preview smart-template-settings-short-code-preview"><%= footer.getHtmlPreview() %></td>
											<td><%= sdf.format(footer.getCreateDate()) %></td>
											<td><%= sdf.format(footer.getEditedDate()) %></td>
											<td><a href="javascript:void(0);" onclick="onClickEditFooter(<%= footer.getId() %>)">Edit</a> |
												<a href="javascript:void(0);" onclick="onClickDeleteFooter(<%= footer.getId() %>);">Delete</a></td>
										</tr>
										<% } %>
										</tbody>
									</table>
								</div>
							</div>
							<div class="panel-1">
								<div class="panel-body">
									<b>Default Footer on Launch</b>
									<select id="defaultFooter" name="defaultFooter" class="select is-primary" onchange="updateDefaultFooter()">
										<option value="None">None</option>
										<% for (SmartEncounterFooter footer : footerList) { %>
										<option value="<%= footer.getId() %>" <%= defaultSmartEncounterFooter != null
												&& defaultSmartEncounterFooter.getValue() != null
												&& defaultSmartEncounterFooter.getValue().equals(footer.getId().toString()) ? "selected" : "" %>>
												<%= footer.getName() %></option>
										<% } %>
									</select>
								</div>
							</div>
						</div>
				</div>
						<div id="addEdit" style="display: none;">
							<h4 id="addEditHeader">Edit Template:</h4>
							<div id="defaultForTemplate" style="display: none;">
								<b>Default Header With Template</b>
								<select id="defaultTemplateHeader" name="defaultTemplateHeader" class="select is-primary">
									<option value="None">None</option>
									<% for (SmartEncounterHeader header : headerList) { %>
									<option value="<%= header.getId() %>" >
										<%= header.getName() %>
									</option>
									<% } %>
								</select>
								<b>Default Footer With Template</b>
								<select id="defaultTemplateFooter" name="defaultTemplateFooter" class="select is-primary">
									<option value="None">None</option>
									<% for (SmartEncounterFooter footer : footerList) { %>
									<option value="<%= footer.getId() %>" >
										<%= footer.getName() %>
									</option>
									<% } %>
								</select>

							</div>
									<div class="columns">
										<div class="column">
											<div class="control">
											</div>
										</div>
									</div>
									<div class="columns is-side-gap is-multiline center-row-content" style="overflow: visible;">
										<div class="column is12-md-10">
											
											<div class="panel-1 page-content smart-template-inner-panel">
												<input type="text" class="input smart-template-page-width" id="name" name="name" placeholder="Template Name" onkeydown="nameInvalidFeedbackElement.style.display = 'none';"/>
												<div id="nameInvalidFeedback" style="display: none; color: red;"></div>
												<div class="panel-body" style="height: inherit;">
													<div id="quillEditor" style="background-color: white;"></div>
												</div>
											</div>
										</div>
										<div class="column is12-md-2">
											<div class="panel-1">
												<div class="panel-body" style="width: 200px;">
													<input type="text" class="input is-small" style="width: 130px;" id="placeholderFilter" placeholder="filter placeholders" onkeyup="updatePlaceholderFilter(this)"/>
													<input type="button" class="button is-small" onclick="clearPlaceholderFilter()" value="clear" />
													<div  role="group" id="placeholderList" style="width: 100%; overflow-y: scroll; height: 700px;">
														<input type="button" class="button is-green is-small is-block" onclick="addFillInMarkerAtCaret()" value="Add Fill In Marker (&laquo;&raquo;)"/>
														<%
															for (DatabaseTag templatePlaceholder : databaseTagSet) {
														%>
														<input type="button" class="button is-green is-small is-block quill-placeholder-<%=templatePlaceholder.getSubject()%>"
															   onclick="addTemplatePlaceholderAtCaret('<%=templatePlaceholder.getSubjectAndField()%>')"
															   value="<%=templatePlaceholder.getTitle()%>"/>
														<% } %>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="columns">
										<div class="column">
											<div class="columns" style="margin-top: 34px;">
												<div class="column is12-md-2">
													<input type="button" class="button is-green is-small is-block" id="addEditSave" value="Save" onclick="onClickSave()"/>
												</div>
											</div>
										</div>
									</div>
						</div>
					</html:form>
				</div>
			</div>
			<div style="display: none;">
				<div id="shortcodePreviewEditor"></div>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/library/bootstrap/3.0.0/js/bootstrap.min.js"></script>

	<script>
		let quillEditor;
		let saveMode = null;

		let methodElement = document.getElementById('method');
		let listTemplatesAndShortCodesElement = document.getElementById('listTemplatesAndShortCodes');
		let addEditElement = document.getElementById('addEdit');
        let addEditHeaderElement = document.getElementById('addEditHeader');
        let templateDefaultFooterHeader = document.getElementById('defaultForTemplate');
        let addEditNameElement = document.getElementById('name');
        let addEditSubmitElement = document.getElementById('addEditSave');
        let nameInvalidFeedbackElement = document.getElementById('nameInvalidFeedback');
        
		function changeView(view) {
			listTemplatesAndShortCodesElement.style.display = 'none';
			templateDefaultFooterHeader.style.display = 'none';
            addEditElement.style.display = 'none';
			// addEditShortCodeElement.style.display = 'none';
			document.getElementById(view).style.display = 'block';
			document.getElementById('backButton').style.display = 'block';
		}

		function templateSave() {
			methodElement.value = saveMode;
			setInputElementsToQuillValues();
			document.getElementById('smartEncounterTemplateForm').submit();
		}

        function onClickSave() {
		    if (saveMode === 'saveTemplate') {
                templateSave();
			} else if (saveMode === 'saveShortCode') {
                shortCodeSave();
			} else if (saveMode === 'saveHeader') {
			    headerSave();
			} else if (saveMode === 'saveFooter') {
			    footerSave();
			}
		}

		function addNewTemplate() {
            if (typeof quillEditor !== 'undefined') {
                quillEditor.setContents([]); // clear editor
            } else {
                quillEditor = new TemplatePlaceholderQuill('quillEditor', null);
            }
            addEditHeaderElement.innerHTML = 'Edit Template';
            saveMode = 'saveTemplate';
			changeView('addEdit');
			templateDefaultFooterHeader.style.display = 'block';
		}
		
		function onClickEditTemplate(templateId) {
      let template = templatesMap.get(templateId);

      document.getElementById('name').value = template['name'];
      document.getElementById('id').value = templateId;
      document.getElementById('createDateLong').value = template['createDate'];
      if (typeof quillEditor === 'undefined') {
        quillEditor = new TemplatePlaceholderQuill('quillEditor', null);
      }
      if (template['headerId'] != 'None' && template['headerId'] != '') {
				document.getElementById('defaultTemplateHeader').value = template['headerId'];
			}
			if (template['footerId'] != 'None' && template['footerId'] != '') {
				document.getElementById('defaultTemplateFooter').value = template['footerId'];
			}
      quillEditor.setContents(template['template']);
      addEditHeaderElement.innerHTML = 'Edit Template';
      addEditNameElement.placeholder  = 'Template Name';
      saveMode = 'saveTemplate';
			changeView('addEdit');
			templateDefaultFooterHeader.style.display = 'block';
		}

		function onClickDeleteTemplate(templateId) {
			let template = templatesMap.get(templateId);
			if (confirm('Are you sure you want to delete template \'' + template['name'] +  '\'?')) {
				methodElement.value = 'deleteTemplate';
				document.getElementById('id').value = templateId;
				document.getElementById('smartEncounterTemplateForm').submit();
			}
		}

		function setInputElementsToQuillValues() {
			let imagesToSaveArray = quillEditor.getDocumentImages();
			let formElement = document.getElementById('smartEncounterTemplateForm');
			for (let i = 0; i < imagesToSaveArray.length; i++) {
				let imageDataToSave = imagesToSaveArray[i].insert.ImagePlaceholder;
				let fileInput = document.createElement('input');
				fileInput.setAttribute('type', 'hidden');
				fileInput.setAttribute('name', 'images');
				fileInput.setAttribute('value', imageDataToSave.identifier + ';' + imageDataToSave.data);
				formElement.appendChild(fileInput);
			}
			document.getElementById('template').value = JSON.stringify(quillEditor.getContentsWithImagePlaceholders());
		}

		function addTemplatePlaceholderAtCaret(placeholderName) {
			let placeholder = placeholderMap.get(placeholderName);
			if (placeholder) {
				quillEditor.insertPlaceholderAtCaret(eval(placeholder));
			}
		}
		function addFillInMarkerAtCaret() {
			quillEditor.insertTextAtCaret(String.fromCharCode(171) + String.fromCharCode(187));
		}

		function onClickEditShortCode(shortCodeId) {
			let shortCode = shortCodesMap.get(shortCodeId);
			document.getElementById('name').value = shortCode['name'];
			document.getElementById('id').value = shortCodeId;
			document.getElementById('createDateLong').value = shortCode['createDate'];
            quillEditor = new TemplatePlaceholderQuill('quillEditor', null, [], { noToolBar: true});
            quillEditor.setContents(shortCode['text'])
            addEditHeaderElement.innerHTML = 'Edit Short Code';
            addEditNameElement.placeholder  = 'Short Code Name';
            saveMode = 'saveShortCode';
			changeView('addEdit');
		}

		function onClickEditHeader(headerId) {
			const header = headersMap.get(headerId);
			document.getElementById('name').value = header['name'];
			document.getElementById('id').value = headerId;
			document.getElementById('createDateLong').value = header['createDate'];
			quillEditor = new TemplatePlaceholderQuill('quillEditor', null, [], { noToolBar: true});
			quillEditor.setContents(header['text'])
			addEditHeaderElement.innerHTML = 'Edit Header';
			addEditNameElement.placeholder  = 'Header Name';
			saveMode = 'saveHeader';
			changeView('addEdit');
			document.getElementById('quillEditor').style.height = "80%";
		}

		function onClickEditFooter(footerId) {
			const footer = footersMap.get(footerId);
			document.getElementById('name').value = footer['name'];
			document.getElementById('id').value = footerId;
			document.getElementById('createDateLong').value = footer['createDate'];
			quillEditor = new TemplatePlaceholderQuill('quillEditor', null, [], { noToolBar: true});
			quillEditor.setContents(footer['text'])
			addEditHeaderElement.innerHTML = 'Edit Footer';
			addEditNameElement.placeholder  = 'Footer Name';
			saveMode = 'saveFooter';
			changeView('addEdit');
			document.getElementById('quillEditor').style.height = "80%";
		}

		function onClickDeleteShortCode(shortCodeId) {
			let shortCode = shortCodesMap.get(shortCodeId);
			if (confirm('Are you sure you want to delete template \'' + shortCode['name'] +  '\'?')) {
				methodElement.value = 'deleteShortCode';
				document.getElementById('id').value = shortCodeId;
				document.getElementById('smartEncounterTemplateForm').submit();
			}
		}

		function onClickDeleteHeader(headerId) {
			const header = headersMap.get(headerId);
			if (confirm('Are you sure you want to delete header \'' + header['name'] +  '\'?')) {
				methodElement.value = 'deleteHeader';
				document.getElementById('id').value = headerId;
				document.getElementById('smartEncounterTemplateForm').submit();
			}
		}

		function onClickDeleteFooter(footerId) {
			const footer = footersMap.get(footerId);
			if (confirm('Are you sure you want to delete footer \'' + footer['name'] +  '\'?')) {
				methodElement.value = 'deleteFooter';
				document.getElementById('id').value = footerId;
				document.getElementById('smartEncounterTemplateForm').submit();
			}
		}

		function shortCodeSave() {
		    if (isShortCodeValid()) {
                methodElement.value = saveMode;
                setInputElementsToQuillValues();
                document.getElementById('htmlPreview').value = quillEditor.getHtml();
                document.getElementById('smartEncounterTemplateForm').submit();
			}
		}

		function headerSave() {
			if (isHeaderValid()) {
				methodElement.value = saveMode;
				setInputElementsToQuillValues();
				document.getElementById('htmlPreview').value = quillEditor.getHtml();
				document.getElementById('smartEncounterTemplateForm').submit();
			}
		}

		function footerSave() {
			if (isFooterValid()) {
				methodElement.value = saveMode;
				setInputElementsToQuillValues();
				document.getElementById('htmlPreview').value = quillEditor.getHtml();
				document.getElementById('smartEncounterTemplateForm').submit();
			}
		}

		function updatePlaceholderFilter(filterInputElement) {
			let filter = filterInputElement.value.toUpperCase();
			let list = document.getElementById('placeholderList');
			for (let i = 0; i < list.children.length; i++) {
				let value = list.children[i].value;
				if (value.toUpperCase().includes(filter)) {
					list.children[i].style.display = 'block';
				} else {
					list.children[i].style.display = 'none';
				}
			}
		}
        function clearPlaceholderFilter() {
		    let element = document.getElementById('placeholderFilter');
            element.value = '';
            updatePlaceholderFilter(element);
        }
        
        function updateDefaultTemplate() {
            methodElement.value = 'updateDefaultTemplate';
            document.getElementById('smartEncounterTemplateForm').submit();
        }

		function updateDefaultHeader() {
			methodElement.value = 'updateDefaultHeader';
			document.getElementById('smartEncounterTemplateForm').submit();
		}

		function updateDefaultFooter() {
			methodElement.value = 'updateDefaultFooter';
			document.getElementById('smartEncounterTemplateForm').submit();
		}

		function addNewShortCode() {
            if (typeof quillEditor !== 'undefined') {
                quillEditor.setContents([]); // clear editor
			} else {
                quillEditor = new TemplatePlaceholderQuill('quillEditor', null, [], { noToolBar: true});
			}
            addEditHeaderElement.innerHTML = 'Edit Short Code';
            addEditNameElement.placeholder  = 'Short Code Name';
            saveMode = 'saveShortCode';
			changeView('addEdit');
		}

		function addNewHeader() {
			if (typeof quillEditor !== 'undefined') {
				quillEditor.setContents([]); // clear editor
			} else {
				quillEditor = new TemplatePlaceholderQuill('quillEditor', null, [], { noToolBar: true});
			}
			addEditHeaderElement.innerHTML = 'Edit Header';
			addEditNameElement.placeholder  = 'Header Name';
			saveMode = 'saveHeader';
			changeView('addEdit');
			document.getElementById('quillEditor').style.height = "80%";
		}

		function addNewFooter() {
			if (typeof quillEditor !== 'undefined') {
				quillEditor.setContents([]); // clear editor
			} else {
				quillEditor = new TemplatePlaceholderQuill('quillEditor', null, [], { noToolBar: true});
			}
			addEditHeaderElement.innerHTML = 'Edit Footer';
			addEditNameElement.placeholder  = 'Footer Name';
			saveMode = 'saveFooter';
			changeView('addEdit');
			document.getElementById('quillEditor').style.height = "80%";
		}

		function onClickBack() {
			listTemplatesAndShortCodesElement.style.display = 'block';
            addEditElement.style.display = 'none';
			document.getElementById('backButton').style.display = 'none';
			document.getElementById('name').value = '';
			document.getElementById('template').value = '';
			document.getElementById('createDateLong').value = '';
			document.getElementById('id').value = '';
		}
		
		function isShortCodeValid() {
		    if (addEditNameElement.value === '') {
                nameInvalidFeedbackElement.style.display = 'block';
                nameInvalidFeedbackElement.innerText = 'Short Code name cannot be empty.';
                return false;
			}
            // check if short code with name already exists
            for (const [key, shortCode] of shortCodesMap.entries()) {
                if (shortCode['name'] === addEditNameElement.value && key.toString() !== document.getElementById('id').value) {
                    nameInvalidFeedbackElement.style.display = 'block';
                    nameInvalidFeedbackElement.innerText = 'Short Code with name already exists.';
                    return false;
                }
            }
            return true;
            }

            function isHeaderValid() {
                if (addEditNameElement.value === '') {
                    nameInvalidFeedbackElement.style.display = 'block';
                    nameInvalidFeedbackElement.innerText = 'Header name cannot be empty.';
                    return false;
                }
                return true;
            }

            function isFooterValid() {
                if (addEditNameElement.value === '') {
                    nameInvalidFeedbackElement.style.display = 'block';
                    nameInvalidFeedbackElement.innerText = 'Footer name cannot be empty.';
                    return false;
                }
                return true;
		}
	</script>
	</body>
</html:html>
