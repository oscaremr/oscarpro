<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="oscar.oscarClinic.ClinicData" %>
<%@ page import="java.util.Vector" %>
<%@ page import="java.util.StringTokenizer" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.util.StringUtils" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    OscarProperties oscarProps = OscarProperties.getInstance();

    ClinicData clinicData = new ClinicData();

    String strPhones = clinicData.getClinicDelimPhone();
    if (strPhones == null) {
        strPhones = "";
    }
    String strFaxes = clinicData.getClinicDelimFax();
    if (strFaxes == null) {
        strFaxes = "";
    }
    Vector vecPhones = new Vector();
    Vector vecFaxes = new Vector();
    StringTokenizer st = new StringTokenizer(strPhones, "|");
    while (st.hasMoreTokens()) {
        vecPhones.add(st.nextToken());
    }
    st = new StringTokenizer(strFaxes, "|");
    while (st.hasMoreTokens()) {
        vecFaxes.add(st.nextToken());
    }
    
    String defaultClinicInfo = clinicData.getClinicName() + 
            "\n" + clinicData.getClinicAddress() + ", " + clinicData.getClinicCity() + ", " + clinicData.getClinicProvince() + " " + clinicData.getClinicPostal() + 
            "\nTelephone: " + (vecPhones.size() >= 1 ? vecPhones.elementAt(0) : clinicData.getClinicPhone()) + 
            "\nFax: " + (vecFaxes.size() >= 1 ? vecFaxes.elementAt(0) : clinicData.getClinicFax());

	StringBuilder errorMessages = new StringBuilder();


    if (request.getParameter("dboperation") != null && !request.getParameter("dboperation").isEmpty() && request.getParameter("dboperation").equals("Save")) {
        for(String key : SystemPreferences.GENERAL_SETTINGS_KEYS) {
            SystemPreferences preference = SystemPreferencesUtils.findPreferenceByName(key);
            String newValue = request.getParameter(key);
            //If use custom wasn't checked, set to null so that the default gets used instead.
            if("invoice_custom_clinic_info".equals(key) && !"on".equals(request.getParameter("invoice_use_custom_clinic_info"))){
                newValue = null;
            } else if ("force_logout_when_inactive_time".equals(key)) {
                if (!StringUtils.isInteger(request.getParameter("force_logout_when_inactive_time"))) {
                    errorMessages.append("<span style=\"color: red;\">Logout inactive time not updated: selected value is not a whole number</span>");
                    continue;
                } else if (Integer.parseInt(request.getParameter("force_logout_when_inactive_time")) <= 0) {
                    errorMessages.append("<span style=\"color: red;\">Logout inactive time not updated: Value cannot be 0</span>");
                    continue;
                }
            } else if ("reminder_send_window".equals(key)) {
                if (!StringUtils.isInteger(request.getParameter("reminder_send_window"))) {
                    errorMessages.append("<span style=\"color: red;\">Reminder send time window must be a whole number (in minutes)</span>");
                    continue;
                } else if (Integer.parseInt(request.getParameter("reminder_send_window")) <= 0 || Integer.parseInt(request.getParameter("reminder_send_window")) > 59) {
                    errorMessages.append("<span style=\"color: red;\">Reminder send time window must be between 1 - 59 minutes</span>");
                    continue;
                }
            }

            if (preference != null) {
                if (!preference.getValue().equals(newValue)) {
                    preference.setUpdateDate(new Date());
                    preference.setValue(newValue);
                    SystemPreferencesUtils.merge(preference);
                }
            } else {
                preference = new SystemPreferences();
                preference.setName(key);
                preference.setUpdateDate(new Date());
                preference.setValue(newValue);
                SystemPreferencesUtils.persist(preference);
            }
        }
    }
    List<SystemPreferences> preferences = SystemPreferencesUtils.findPreferencesByNames(SystemPreferences.GENERAL_SETTINGS_KEYS);
    for(SystemPreferences preference : preferences) {
        dataBean.setProperty(preference.getName(), preference.getValue());
    }
    
    boolean forceLogoutWhenInactive = dataBean.getProperty("force_logout_when_inactive", "false").equals("true");
    boolean enableValidationOnSpecialist = dataBean.getProperty("enable_validation_on_specialist", "false").equals("true");
    String forceLogoutTime = dataBean.getProperty("force_logout_when_inactive_time", "120");
    String reminderSendWindow = dataBean.getProperty("reminder_send_window", "1");
    boolean assignDemographicsToSites = dataBean.getProperty("assignDemographicsToSites", "true").equals("true");

%>

<html:html locale="true">
    <head>
        <title>General Settings</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
        <script type="text/javascript" language="JavaScript">
            var defaultClinicInvoiceInfo = '<%=Encode.forJavaScript(defaultClinicInfo)%>';
            function setClinicInfo(){
                var useCustom = document.getElementById('invoice_use_custom_clinic_info');
                var clinicInfo = document.getElementById('invoice_custom_clinic_info');
                if(useCustom === null || !useCustom.checked){
                    clinicInfo.value = defaultClinicInvoiceInfo;
                    clinicInfo.disabled = true;
                } else {
                    clinicInfo.disabled = false;
                }
            }
            
        </script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
    <h4>Manage General OSCAR Settings</h4>
    <form name="generalSettingsForm" method="post" action="generalSettings.jsp">
        <%=errorMessages.toString()%>
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <td>Display preferred name instead of demographic name: </td>
                <td>
                    <input id="replace_demographic_name_with_preferred-true" type="radio" value="true" name="replace_demographic_name_with_preferred"
                            <%=(dataBean.getProperty("replace_demographic_name_with_preferred", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="replace_demographic_name_with_preferred-false" type="radio" value="false" name="replace_demographic_name_with_preferred"
                            <%=(dataBean.getProperty("replace_demographic_name_with_preferred", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Use create date instead of next appointment date on messages: </td>
                <td>
                    <input id="msg_use_create_date" type="checkbox" value="true" name="msg_use_create_date"
                            <%=(dataBean.getProperty("msg_use_create_date", "false").equals("true")) ? "checked" : ""%> />
                </td>
            </tr>
            <tr>
                <td>Force logout on inactive users: </td>
                <td>
                    <input id="force_logout_when_inactive_true" type="radio" value="true" name="force_logout_when_inactive"
                            <%=forceLogoutWhenInactive ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="force_logout_when_inactive_false" type="radio" value="false" name="force_logout_when_inactive"
                            <%=!forceLogoutWhenInactive ? "checked" : ""%> />
                    No
                    <br/>Logout after
                    <input id="force_logout_when_inactive_time" type="text" value="<%=forceLogoutTime%>" name="force_logout_when_inactive_time"/> minutes
                </td>
            </tr>
            <% if(oscarProps.getProperty("billregion", "").equals("BC")){ %>
            <tr>
                <td>Set clinic information to display on invoice: </td>
                <td width="327px">
                    <input type="checkbox" id="invoice_use_custom_clinic_info" name="invoice_use_custom_clinic_info" onclick="setClinicInfo()" <%= StringUtils.isNullOrEmpty(dataBean.getProperty("invoice_custom_clinic_info")) ? "" : "checked"%>/>Use Custom
                    <br>
                    <textarea style="resize: none; width: 90%" rows="4" id="invoice_custom_clinic_info" name="invoice_custom_clinic_info" maxlength="250" <%=StringUtils.isNullOrEmpty(dataBean.getProperty("invoice_custom_clinic_info")) ? "disabled" : ""%>><%= Encode.forHtmlAttribute(StringUtils.isNullOrEmpty(dataBean.getProperty("invoice_custom_clinic_info")) ? defaultClinicInfo : dataBean.getProperty("invoice_custom_clinic_info"))%></textarea>
                </td>
	      </tr>
            <% } %>
            <tr>
                <td>Appointment reminder send time window adjustment: </td>
                <td>
                    <input type="text" id="reminder_send_window" name="reminder_send_window" value="<%=reminderSendWindow%>"/> minutes
           	</td>
		</tr>  
            <tr>
                <td><bean:message key="admin.systemManagement.editGeneralSettings.enableValidationOnSpecialist"/> </td>
                <td>
                    <input id="enable_validation_on_specialist_true" type="radio" value="true" name="enable_validation_on_specialist"
                            <%= enableValidationOnSpecialist ? "checked" : "" %> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="enable_validation_on_specialist_false" type="radio" value="false" name="enable_validation_on_specialist"
                            <%= !enableValidationOnSpecialist ? "checked" : "" %> />
                    No
                </td>
            </tr>
            <tr>
                <td><bean:message key="admin.systemManagement.generalSetting.displayPatientNameOnMessagePrint"/>:</td>
                <td>
                    <input id="display_patient_name_on_message_print" type="radio" value="true" name="display_patient_name_on_message_print"
                            <%=(dataBean.getProperty("display_patient_name_on_message_print", "false").equals("true")) ? "checked" : ""%> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="display_patient_name_on_message_print" type="radio" value="false" name="display_patient_name_on_message_print"
                            <%=(dataBean.getProperty("display_patient_name_on_message_print", "false").equals("false")) ? "checked" : ""%> />
                    No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            
            <tr>
                <td><bean:message key="admin.systemManagement.editGeneralSettings.allowDemographicsToBeAssignedToSites"/> </td>
                <td>
                    <input id="assignDemographicsToSites_true" type="radio" value="true" name="assignDemographicsToSites"
                            <%= assignDemographicsToSites ? "checked" : "" %> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="assignDemographicsToSites_false" type="radio" value="false" name="assignDemographicsToSites"
                            <%= !assignDemographicsToSites ? "checked" : "" %> />
                    No
                </td>
            </tr>
            <tr>
                <td><bean:message key="admin.systemManagement.editGeneralSettings.requireCpsidAndDoctorForMrp"/> </td>
                <td>
                    <input id="require_cpsid_and_doctor_for_mrp_true" type="radio" value="true" name="require_cpsid_and_doctor_for_mrp"
                            <%= (dataBean.getProperty("require_cpsid_and_doctor_for_mrp", "false").equals("true")) ? "checked" : "" %> />
                    Yes
                    &nbsp;&nbsp;&nbsp;
                    <input id="require_cpsid_and_doctor_for_mrp_false" type="radio" value="false" name="require_cpsid_and_doctor_for_mrp"
                            <%= (dataBean.getProperty("require_cpsid_and_doctor_for_mrp", "false").equals("false")) ? "checked" : "" %> />
                    No
                </td>
            </tr>
            <tr>
                <td><bean:message key="admin.systemManagement.editGeneralSettings.search_button_during_patient_search"/> </td>
                <td>
                    <select id="patient-search-select" name="patient_search_select">
                        <option value="allDemographics"
                                <%= dataBean.getProperty("patient_search_select", "allDemographics").equals("allDemographics")
                                        ? "selected" : "" %>>
                            Display all demographics
                        </option>
                        <option value="recentlyAccessedDemographics"
                                <%= dataBean.getProperty("patient_search_select", "allDemographics").equals("recentlyAccessedDemographics")
                                        ? "selected" : "" %>>
                            Display previously opened demographics
                        </option>
                        <option value="noDemographics"
                                <%= dataBean.getProperty("patient_search_select", "allDemographics").equals("noDemographics")
                                        ? "selected" : "" %>>
                            Do not display any demographics
                        </option>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>

        <input type="button" onclick="document.forms['generalSettingsForm'].dboperation.value='Save'; document.forms['generalSettingsForm'].submit();" name="saveGeneralSettings" value="Save"/>
    </form>
    </body>
</html:html>
