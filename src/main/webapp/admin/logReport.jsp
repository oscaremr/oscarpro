<!DOCTYPE html>
<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ page import="java.util.*"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>

<%
String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
String curUser_no = (String)session.getAttribute("user");
boolean isSiteAccessPrivacy=false;
boolean authed=true;
%>

<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.reporting" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_admin&type=_admin.reporting");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
%>


<security:oscarSec objectName="_site_access_privacy" roleName="<%=roleName$%>" rights="r" reverse="false">
	<%isSiteAccessPrivacy=true; %>
</security:oscarSec>


<%
  String   tdTitleColor = "#CCCC99";
  String   tdSubtitleColor = "#CCFF99";
  String   tdInterlColor = "white";

  String startDate = StringUtils.trimToEmpty(request.getParameter("startDate")).replaceAll("/", "-");
  String endDate = StringUtils.trimToEmpty(request.getParameter("endDate")).replaceAll("/", "-");
//provider name list, date, action, create button
%>

<%
  List<ProviderData> providers;
  ProviderDataDao providerDataDao = SpringUtils.getBean(ProviderDataDao.class);

  providers = isSiteAccessPrivacy
      ? providerDataDao.findAllBySite(curUser_no)
      : providerDataDao.findAllActiveAndInactiveOrderByLastName();

  Map<String, String> providerNames = new LinkedHashMap<String, String>();

  for (ProviderData providerData : providers) {
    providerNames.put(providerData.getId(), providerData.getFormattedName());
  }
%>

<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.model.OscarLog" %>
<%@ page import="org.oscarehr.common.dao.OscarLogDao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.ParseException" %>
<%@ page import="org.oscarehr.common.model.ProviderData" %>
<%@ page import="org.oscarehr.common.dao.ProviderDataDao" %>
<html:html locale="true">
<head>


<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.9.1.min.js"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap-datepicker.js"></script>

<link href="<%=request.getContextPath() %>/css/bootstrap.min.css" rel="stylesheet">

<link href="<%=request.getContextPath() %>/css/datepicker.css" rel="stylesheet" type="text/css">



<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css">
<title>Log Report</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<script language="JavaScript">

function setfocus() {
	this.focus();
	//  document.titlesearch.keyword.select();
}
function onSub() {
	if( document.myform.startDate.value=="" || document.myform.endDate.value=="") {
		alert("Please set Start and End Dates.");
		return false ;
	} else {
		return true;
	}
}

</script>

<style>

label{margin-top:6px;margin-bottom:0px;}
</style>

</head>
<body>
<form name="myform" class="well form-horizontal" action="logReport.jsp" method="POST" onSubmit="return(onSub());">
  <fieldset>
    <h3>Log Admin Report <small>Please select the provider, start and end dates.</small></h3>

      <div class="span4">
      <label for="providerNo">Provider: </label>
        <select name="providerNo" id="providerNo">
          <option value="*">All</option>
          <%
              for (Map.Entry<String, String> entry : providerNames.entrySet()) {
                String selected = request.getParameter("providerNo");
          %>
          <option value="<%= entry.getKey() %>"
            <%  if ((selected != null) && (selected.equals(entry.getKey()))) { %> selected
            <%  } %>><%= Encode.forHtmlContent((StringUtils.trimToEmpty(entry.getValue()))) %>
          </option>
          <%
              }
          %>
        </select>
      </div>

			<div class="span2">
				<label>Demographic No:</label>
				<input type="text" style="width: 100px;" name="demographicNo"/>
			</div>
			<div class="span3">
			<label>Content Type:</label>
			<select name="content" >
				<option value="admin">Admin</option>
				<option value="login">Log in</option>
			</select>
			</div>
		
			<div class="span3">		
			<label>Start Date: </label>
			<label class="input-append" style="margin-top: 0;">
				<input type="text" style="width: 179px;" name="startDate" id="startDate1" 
					   value="<%= startDate %>" pattern="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" autocomplete="off" />
				<span class="add-on"><i class="icon-calendar"></i></span>
			</label>
			</div>
			
			<div class="span3">		
			<label >End Date: </label>
			<label class="input-append" style="margin-top: 0;">
				<input type="text" style="width: 179px;" name="endDate" id="endDate1" 
					   value="<%= endDate %>" pattern="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" autocomplete="off" />
				<span class="add-on"><i class="icon-calendar"></i></span>
			</label>
			</div>


		
			<div class="span8" style="padding-top:10px;">
			<input class="btn btn-primary" type="submit" name="submit" value="Run Report">
			</div>

	</fieldset>
</form>
<%
	out.flush();

	boolean bAll = false;
	String providerNo = "";
  List<OscarLog> logs = new ArrayList<OscarLog>();

	if (request.getParameter("submit") != null) {
    providerNo = request.getParameter("providerNo");
    String demographicNo = request.getParameter("demographicNo");
    String content = request.getParameter("content");

    if(content.equals("admin")) {
      content = "%";
    }

    OscarLogDao logDao = SpringUtils.getBean(OscarLogDao.class);
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    Date start;
    Date end;

    try {
      start = simpleDateFormat.parse(startDate);
    } catch (ParseException e) {
      start = null;
    }

    try {
      end = simpleDateFormat.parse(endDate);
    } catch (ParseException e) {
      end = null;
    }

    if("*".equals(providerNo)) {
      bAll = true;
      logs = isSiteAccessPrivacy ?
          logDao.findBySite(curUser_no, start, end, content, demographicNo) :
          logDao.findByDateContentDemographic(start, end, content, demographicNo);
    } else {
      logs = logDao.findByProviderDateContentDemographic(
          providerNo,
          start,
          end,
          content,
          demographicNo
      );
    }
  }
%>
<h4><%= StringUtils.isNotBlank(providerNames.get(providerNo)) ? providerNames.get(providerNo) : "All" %> - Log Report</h4>

<button class="btn pull-right" onClick="window.print()" style="margin-bottom:4px">
	<i class="icon-print"></i> Print
</button> 

<p>Period: ( <%= startDate %> ~ <%= endDate %>)</p>
<table class="table table-bordered table-striped table-hover table-condensed">
  <tr bgcolor="<%=tdTitleColor%>">
    <TH>Time</TH>
    <TH>Action</TH>
    <TH>Content</TH>
    <TH>Keyword</TH>
    <TH>IP</TH>
    <% if (bAll) { %>
    <TH>Provider</TH>
    <% } %>
    <TH>Demo</TH>
    <TH>Data</TH>
  </tr>
<%
String color = "";
int colourCounter = 0;
for (OscarLog log : logs) {
  color = colourCounter++ % 2 == 0 ? tdInterlColor : "white";
%>
  <tr bgcolor="<%= color %>" align="center">
    <td><%= StringEscapeUtils.escapeHtml(log.getCreated().toString())%>&nbsp;</td>
    <td><%= StringEscapeUtils.escapeHtml(log.getAction())%>&nbsp;</td>
    <td><%= StringEscapeUtils.escapeHtml(log.getContent())%>&nbsp;</td>
    <td><%= StringEscapeUtils.escapeHtml(StringUtils.trimToEmpty(log.getContentId()))%>&nbsp;</td>
    <td><%= StringEscapeUtils.escapeHtml(log.getIp())%>&nbsp;</td>
    <% if (bAll) { %>
    <td><%= StringEscapeUtils.escapeHtml(StringUtils.trimToEmpty(providerNames.get(log.getProviderNo()))) %>&nbsp;</td>
    <% } %>
    <td><%= StringEscapeUtils.escapeHtml(log.getDemographicId() != null ? log.getDemographicId().toString() : "") %>&nbsp;</td>
    <td><%= StringEscapeUtils.escapeHtml(StringUtils.trimToEmpty(log.getData())).replaceAll("\n", "\n<br/>") %>&nbsp;</td>
  </tr>
  <% } %>

<script type="text/javascript">
var startDate = $("#startDate1").datepicker({
	format : "yyyy-mm-dd"
});

var endDate = $("#endDate1").datepicker({
	format : "yyyy-mm-dd"
});
</script>
</body>
</html:html>
