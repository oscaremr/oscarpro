<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%-- This JSP is the first page you see when you enter 'report by template' --%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>


<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.common.model.Provider" %>
<%@ page import="org.oscarehr.common.dao.RxManageDao" %>
<%@ page import="org.oscarehr.common.model.RxManage" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties" scope="page" />
<%
    LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
    Provider provider = loggedInInfo.getLoggedInProvider();

    RxManageDao rxManageDao = SpringUtils.getBean(RxManageDao.class);
    
    if (request.getParameter("dboperation") != null && !request.getParameter("dboperation").isEmpty() && request.getParameter("dboperation").equals("Save")) {
        RxManage rxManage = rxManageDao.getRxManageAttributes();
        
        if (rxManage!=null)
        {
            rxManage.setMrpOnRx(Boolean.parseBoolean(request.getParameter("mrpPresc")));
            rxManageDao.merge(rxManage);
        }
        else
        {
            rxManage = new RxManage();
            rxManage.setMrpOnRx(Boolean.parseBoolean(request.getParameter("mrpPresc")));
            rxManageDao.persist(rxManage);
        }

        for(String key : SystemPreferences.RX_PREFERENCE_KEYS) {
            SystemPreferences preference = SystemPreferencesUtils.findPreferenceByName(key);
            String newValue = request.getParameter(key);

            if (preference != null) {
                if (!preference.getValue().equals(newValue)) {
                    preference.setUpdateDate(new Date());
                    preference.setValue(newValue);
                    SystemPreferencesUtils.merge(preference);
                }
            } else {
                preference = new SystemPreferences();
                preference.setName(key);
                preference.setUpdateDate(new Date());
                preference.setValue(newValue);
                SystemPreferencesUtils.persist(preference);
            }
        }
    }
    
%>
<html:html locale="true">
    <head>
        <title>Manage Rx</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
    </head>
    
    <%
        RxManage rxManage = rxManageDao.getRxManageAttributes();
        if (rxManage!=null)
        {
            dataBean.setProperty("mrpOnRx", String.valueOf(rxManage.getMrpOnRx())); 
        }

        List<SystemPreferences> preferences = SystemPreferencesUtils.findPreferencesByNames(SystemPreferences.RX_PREFERENCE_KEYS);
        for(SystemPreferences preference : preferences) {
            if (preference.getValue() != null) {
                dataBean.setProperty(preference.getName(), preference.getValue());
            }
        }
    %>

    <body vlink="#0000FF" class="BodyStyle">
    <h4>Rx Settings</h4>
    <form name="rx-settings" method="post" action="manageRx.jsp">
        <input type="hidden" name="dboperation" value="">
        <table id="manageRxTable" name="manageRxTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <td>Add MRP to Prescriptions: </td>
                <td>
                <input type="radio" value="true" name="mrpPresc"<%=(rxManage!=null ? (dataBean.getProperty("mrpOnRx").equals("true")? "checked" : "") : "")%>/> Yes
                    &nbsp;&nbsp;&nbsp;
                <input type="radio" value="false" name="mrpPresc"<%=(rxManage!=null ? (dataBean.getProperty("mrpOnRx").equals("false")? "checked" : "") : "")%>/> No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Display provider name when pasting Rx to echart: </td>
                <td>
                    <input type="radio" value="true" name="rx_paste_provider_to_echart" <%= (dataBean.getProperty("rx_paste_provider_to_echart", "false").equals("true") ? "checked" : "") %> /> Yes
                    &nbsp;&nbsp;&nbsp;
                    <input type="radio" value="false" name="rx_paste_provider_to_echart" <%= (dataBean.getProperty("rx_paste_provider_to_echart", "false").equals("false") ? "checked" : "") %> /> No
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>Display start date on prescriptions: </td>
                <td>
                    <label style="display: inline">
                        <input type="radio" value="true" name="rx_show_start_dates" <%= (dataBean.getProperty("rx_show_start_dates", "false").equals("true") ? "checked" : "") %> /> Yes
                    </label>
                    &nbsp;&nbsp;&nbsp;
                    <label style="display: inline">
                        <input type="radio" value="false" name="rx_show_start_dates" <%= (dataBean.getProperty("rx_show_start_dates", "false").equals("false") ? "checked" : "") %> /> No
                    </label>
                </td>
            </tr>
			<tr>
				<td>Show end date of drugs on prescriptions: </td>
				<td>
					<label style="display: inline">
						<input type="radio" value="true" name="rx_show_end_dates" <%= (dataBean.getProperty("rx_show_end_dates", "false").equals("true") ? "checked" : "") %> /> Yes
					</label>
					&nbsp;&nbsp;&nbsp;
					<label style="display: inline">
						<input type="radio" value="false" name="rx_show_end_dates" <%= (dataBean.getProperty("rx_show_end_dates", "false").equals("false") ? "checked" : "") %> /> No
					</label>
				</td>
			</tr>
            <tr>
                <td>Display refill duration on prescriptions: </td>
                <td>
                    <label style="display: inline">
                        <input type="radio" value="true" name="rx_show_refill_duration" <%= (dataBean.getProperty("rx_show_refill_duration", "false").equals("true") ? "checked" : "") %> /> Yes
                    </label>
                    &nbsp;&nbsp;&nbsp;
                    <label style="display: inline">
                        <input type="radio" value="false" name="rx_show_refill_duration" <%= (dataBean.getProperty("rx_show_refill_duration", "false").equals("false") ? "checked" : "") %> /> No
                    </label>
                </td>
            </tr>
            <tr>
                <td>Paste RX into encounter by default: <span class="label is-green pull-right">New UI Only</span></td>
                <td>
                    <label style="display: inline">
                        <input type="radio" value="true" name="rx_paste_to_encounter_default" <%= (dataBean.getProperty("rx_paste_to_encounter_default", "false").equals("true") ? "checked" : "") %> /> Yes
                    </label>
                    &nbsp;&nbsp;&nbsp;
                    <label style="display: inline">
                        <input type="radio" value="false" name="rx_paste_to_encounter_default" <%= (dataBean.getProperty("rx_paste_to_encounter_default", "false").equals("false") ? "checked" : "") %> /> No
                    </label>
                </td>
            </tr>
            <tr>
                <td>Display refill quantity on prescriptions: </td>
                <td>
                    <label style="display: inline">
                        <input type="radio" value="true" name="rx_show_refill_quantity" <%= (dataBean.getProperty("rx_show_refill_quantity", "false").equals("true") ? "checked" : "") %> /> Yes
                    </label>
                    &nbsp;&nbsp;&nbsp;
                    <label style="display: inline">
                        <input type="radio" value="false" name="rx_show_refill_quantity" <%= (dataBean.getProperty("rx_show_refill_quantity", "false").equals("false") ? "checked" : "") %> /> No
                    </label>
                </td>
            </tr>
            <tr>
                <td>Display Pillway Home Delivery Button: <span class="label is-green pull-right">New UI Only</span></td>
                <td>
                    <label style="display: inline">
                        <input type="radio" value="true" name="rx_show_pillway_button" <%= (dataBean.getProperty("rx_show_pillway_button", "true").equals("true") ? "checked" : "") %> /> Yes
                    </label>
                    &nbsp;&nbsp;&nbsp;
                    <label style="display: inline">
                        <input type="radio" value="false" name="rx_show_pillway_button" <%= (dataBean.getProperty("rx_show_pillway_button", "true").equals("false") ? "checked" : "") %> /> No
                    </label>
                </td>
            </tr>
            <tr>
                <td>Enable Dispense Internally feature: <span class="label is-green pull-right">New UI Only</span></td>
                <td>
                    <label style="display: inline">
                        <input type="radio" value="true" name="rx_enable_internal_dispensing" <%= (dataBean.getProperty("rx_enable_internal_dispensing", "false").equals("true") ? "checked" : "") %> /> Yes
                    </label>
                    &nbsp;&nbsp;&nbsp;
                    <label style="display: inline">
                        <input type="radio" value="false" name="rx_enable_internal_dispensing" <%= (dataBean.getProperty("rx_enable_internal_dispensing", "false").equals("false") ? "checked" : "") %> /> No
                    </label>
                </td>
            </tr>
            <tr>
                <td>Display Pharmacy Info on Prescriptions by default: <span class="label is-green pull-right">New UI Only</span></td>
                <td>
                    <label style="display: inline">
                        <input type="radio" value="true" name="autoprint_rx_pharmacy_info" <%= (dataBean.getProperty("autoprint_rx_pharmacy_info", "false").equals("true") ? "checked" : "") %>/> Yes
                    </label>
                    &nbsp;&nbsp;&nbsp;
                    <label style="display: inline">
                        <input type="radio" value="false" name="autoprint_rx_pharmacy_info" <%= (dataBean.getProperty("autoprint_rx_pharmacy_info", "false").equals("false") ? "checked" : "") %> /> No
                    </label>
                </td>
            </tr>
            <tr>
                <td>Display Methadone End Date Calculation Option:</td>
                <td>
                    <label style="display: inline">
                        <input type="checkbox" value="true" name="rx_methadone_end_date_calc" <%= (dataBean.getProperty("rx_methadone_end_date_calc", "false").equals("true") ? "checked" : "") %> /> 
                    </label>
                </td>
            </tr>
            <tr>
                <td>Save Rx Signature:</td>
                <td>
                    <label style="display: inline">
                        <input type="checkbox" value="true" name="save_rx_signature" <%= (dataBean.getProperty("save_rx_signature", "true").equals("true") ? "checked" : "") %> />
                    </label>
                </td>
            </tr>
            <tr>
                <td>Expand Limited Use Code Descriptions By Default:</td>
                <td>
                    <label style="display: inline">
                        <input type="checkbox" value="true" name="rx_expand_lu_code_descriptions" <%=("true".equals(dataBean.getProperty("rx_expand_lu_code_descriptions", "false")) ? "checked" : "") %> />
                    </label>
                </td>
            </tr>
            <tr>
                <td>Show Lab Display: <span class="label is-green pull-right">New UI Only</span></td>
                <td>
                    <label style="display: inline">
                        <input type="checkbox" value="true" name="rx_show_lab" <%=("true".equals(dataBean.getProperty("rx_show_lab", "false")) ? "checked" : "") %> />
                    </label>
                </td>
            </tr>
            <tr>
                <td>Display Watermark on Rx Printout: <span class="label is-green pull-right">New UI Only</span></td>
                <td>
                    <label style="display: inline">
                        <select name="rx_watermark">
                            <option value="" <%= "".equals(dataBean.getProperty("rx_watermark", "")) ? "selected" : "" %>>None</option>
                            <option value="providerName" <%= "providerName".equals(dataBean.getProperty("rx_watermark", "")) ? "selected" : "" %>>Provider Name</option>
                            <option value="clinicName" <%= "clinicName".equals(dataBean.getProperty("rx_watermark", "")) ? "selected" : "" %>>Clinic Name</option>
                        </select>
                    </label>
                </td>
            </tr>
            <tr>
                <td>Display Drug to Drug interaction results for the following number of days in the past: <span class="label is-green pull-right">New UI Only</span></td>
                <td>
                    <label style="display: inline">
                        <input type="radio" value="true" onclick="document.getElementById('rx_results_for_number_of_days_in_past').style.visibility = 'visible'"  name="use_rx_date_for_interaction" <%= dataBean.getProperty("use_rx_date_for_interaction", "false").equals("true") ? "checked" : "" %>/> Yes
                    </label>
                    &nbsp;&nbsp;&nbsp;
                    <label style="display: inline">
                        <input type="radio" value="false" onclick="document.getElementById('rx_results_for_number_of_days_in_past').style.visibility = 'hidden'" name="use_rx_date_for_interaction" <%= dataBean.getProperty("use_rx_date_for_interaction", "false").equals("false") ? "checked" : "" %> /> No
                    </label>
                    <br><br>
                    <label style="display: block">
                        <input type="number" id="rx_results_for_number_of_days_in_past"  name="rx_results_for_number_of_days_in_past" value="<%= dataBean.getProperty("rx_results_for_number_of_days_in_past", "")%>" style="visibility: <%= "false".equals(dataBean.getProperty("use_rx_date_for_interaction", "")) ? "hidden" : "visible" %>" minlength="10" maxlength="731">
                    </label>
                </td>
            </tr>
            <tr>
                <td>Display Drug Identification Number (DIN) on Prescription output: <span class="label is-green pull-right">New UI Only</span></td>
                <td>
                    <label style="display: inline">
                        <input type="radio" value="true" name="rx_display_din_on_prescription_output"
                                <%= (dataBean.getProperty("rx_display_din_on_prescription_output", "false")
                                        .equals("true") ? "checked" : "") %>/> Yes
                    </label>

                    <label style="display: inline">
                        <input type="radio" value="false" name="rx_display_din_on_prescription_output"
                                <%= (dataBean.getProperty("rx_display_din_on_prescription_output", "false")
                                        .equals("false") ? "checked" : "") %> /> No
                    </label>
                </td>
            </tr>

            </tbody>
        </table>
        
        <input type="button" onclick="document.forms['rx-settings'].dboperation.value='Save'; document.forms['rx-settings'].submit();" name="mrpPrescButton" value="Save"/>
    </form>
    </body>
</html:html>
