function addCondition() {
    let conditionTable = document.getElementById("conditionTable");
    let row = conditionTable.insertRow(-1);
    row.id = "newCondition_" + newConditionRowNumber;
    row.name = "newCondition_" + newConditionRowNumber;

    //Creates a hidden input element that stores what order conditions are checked in a trigger
    let orderInput = document.createElement("input");
    orderInput.type = "hidden";
    orderInput.id = "newConditionOrder_" + newConditionRowNumber;
    orderInput.name = "newConditionOrder_" + newConditionRowNumber;
    orderInput.value = row.rowIndex.toString();
    row.appendChild(orderInput);

    //Creates a select element that allows the user to select which key value the condition evaluates with
    let keyValueCell = row.insertCell(0);
    let keyValueSelect = document.createElement("select");
    keyValueSelect.type = "text";
    keyValueSelect.id = "newConditionKeyValue_" + newConditionRowNumber;
    keyValueSelect.name = "newConditionKeyValue_" + newConditionRowNumber;
    keyValueSelect.onchange = function() {changeCondition(row.id);};

    //Loops through the list of condition key values and appends them as options to the keyValueSelect element
    for (let i in dict.ConditionKeyValues) {
        let option = document.createElement("option");
        option.innerHTML = dict.ConditionKeyValues[i].Name;
        option.value = dict.ConditionKeyValues[i].Key;
        keyValueSelect.appendChild(option);
    }

    keyValueCell.appendChild(keyValueSelect);

    //Creates a select element that allows the user to select which comparator the condition evaluates with based on what's available from the key value
    let comparatorCell = row.insertCell(1);
    let comparatorSelect = document.createElement("select");
    comparatorSelect.id = "newConditionComparator_" + newConditionRowNumber;
    comparatorSelect.name = "newConditionComparator_" + newConditionRowNumber;
    comparatorSelect.onfocus = function () {operatorOnFocus(row.id);};

    let firstConditionKeyValue = Object.keys(dict.ConditionKeyValues)[0];
    //Loops through the list of comparators and appends them as options to the comparatorSelect element
    for (let i in dict.ConditionKeyValues[firstConditionKeyValue].Comparators) {
        let option = document.createElement("option");
        option.innerHTML = dict.ConditionKeyValues[firstConditionKeyValue].Comparators[i].Name;
        option.value = dict.ConditionKeyValues[firstConditionKeyValue].Comparators[i].Key;
        comparatorSelect.appendChild(option);
    }

    comparatorCell.appendChild(comparatorSelect);

    let valueCell = row.insertCell(2);
    changeValue (valueCell, dict.ConditionKeyValues[firstConditionKeyValue].Type, row.id);

    let editCell = row.insertCell(3);

    let upButton = createButton (function() {reorderRow(row.id, "up");}, 'glyphicon glyphicon-chevron-up');
    editCell.appendChild(upButton);

    let downButton = createButton (function() {reorderRow(row.id, "down");}, 'glyphicon glyphicon-chevron-down');
    editCell.appendChild(downButton);

    let removeButton = createButton (function() {removeCondition(row.id);}, 'glyphicon glyphicon-remove');
    removeButton.className = "btn btn-danger";
    editCell.appendChild(removeButton);

    newConditionRowNumber++;
    if (conditionTable.rows.length-1 >= triggerConditionLimit) {
        let addConditionButton = document.getElementById("addConditionButton");
        addConditionButton.disabled = true;
    }

    let triggerConditionLimitDiv = document.getElementById("triggerConditionLimitDiv");
    triggerConditionLimitDiv.innerHTML = "(" + (conditionTable.rows.length-1) + "/" + triggerConditionLimit + ")";
}

function createButton (onClickFunction, glyphiconClass) {
    let button = document.createElement("button");
    button.addEventListener ('click', onClickFunction);
    button.type = 'button';

    let glyphiconSpan = document.createElement("span");
    glyphiconSpan.className = glyphiconClass;
    button.appendChild(glyphiconSpan);
    return button;
}

function changeCondition(rowId) {
    let conditionTable = document.getElementById("conditionTable");
    let row = conditionTable.rows[rowId];
    let keyValueCell = row.cells[0];

    //Loops through the keyValue cell to find the select element. This is because new conditions and old conditions have it on a different index
    let keyValueSelection;
    for (let i = 0; i < keyValueCell.childNodes.length; i++) {
        if (keyValueCell.childNodes[i].tagName === 'SELECT') {
            keyValueSelection = keyValueCell.childNodes[i];
        }
    }

    let newKeyValue = keyValueSelection.options[keyValueSelection.selectedIndex].innerHTML;
    let comparators = dict.ConditionKeyValues[newKeyValue].Comparators;
    let valueType = dict.ConditionKeyValues[newKeyValue].Type;

    let comparatorCell = row.cells[1];
    let comparatorSelect;

    //Loops through the comparator cell to find the select element. This is because new conditions and old conditions have it on a different index
    for (let i = 0; i < comparatorCell.childNodes.length; i++) {
        if (comparatorCell.childNodes[i].tagName === 'SELECT') {
            comparatorSelect = comparatorCell.childNodes[i];
        }
    }

    //Removes all the old options and then appends the new ones.
    while (comparatorSelect.firstChild) {
        comparatorSelect.removeChild(comparatorSelect.lastChild);
    }

    for (let i in comparators) {
        let option = document.createElement("option");
        option.innerHTML = comparators[i].Name;
        option.value = comparators[i].Key;
        comparatorSelect.appendChild(option);
    }

    let valueCell = row.cells[2];
    changeValue(valueCell, valueType, row.id);
}

function addAction() {
    let actionTable = document.getElementById("actionTable");
    let row = actionTable.insertRow(-1);
    row.id = "newAction_" + newActionRowNumber;
    row.name = "newAction_" + newActionRowNumber;

    //Creates a hidden input element that stores what order actions are run in a trigger
    let orderInput = document.createElement("input");
    orderInput.type = "hidden";
    orderInput.id = "newActionOrder_" + newActionRowNumber;
    orderInput.name = "newActionOrder_" + newActionRowNumber;
    orderInput.value = row.rowIndex.toString();
    row.appendChild(orderInput);

    //Creates a select element that allows the user to select which action they want to use
    let keyValueCell = row.insertCell(0);
    let keyValueSelect = document.createElement("select");
    keyValueSelect.type = "text";
    keyValueSelect.id = "newActionKeyValue_" + newActionRowNumber;
    keyValueSelect.name = "newActionKeyValue_" + newActionRowNumber;
    keyValueSelect.onchange = function() {changeAction(row.id);};

    //Loops through the list of actions for the current trigger and appends them to the keyValueSelect element
    for (let i in dict.ActionKeyValues) {
        let option = document.createElement("option");
        option.innerHTML = dict.ActionKeyValues[i].Name;
        option.value = dict.ActionKeyValues[i].Key;
        keyValueSelect.appendChild(option);
    }

    keyValueCell.appendChild(keyValueSelect);

    let firstActionKeyValue = Object.keys(dict.ActionKeyValues)[0];
    
    let valueCell = row.insertCell(1);
    changeValue (valueCell, dict.ActionKeyValues[firstActionKeyValue].Type, row.id);

    let editCell = row.insertCell(2);

    let upButton = createButton (function() {reorderRow(row.id, "up");}, 'glyphicon glyphicon-chevron-up');
    editCell.appendChild(upButton);

    let downButton = createButton (function() {reorderRow(row.id, "down");}, 'glyphicon glyphicon-chevron-down');
    editCell.appendChild(downButton);

    let removeButton = createButton (function() {removeAction(row.id);}, 'glyphicon glyphicon-remove');
    removeButton.className = "btn btn-danger";
    editCell.appendChild(removeButton);

    newActionRowNumber++;
    if (actionTable.rows.length-1 >= triggerActionLimit) {
        let addActionButton = document.getElementById("addActionButton");
        addActionButton.disabled = true;
    }

    let triggerActionLimitDiv = document.getElementById("triggerActionLimitDiv");
    triggerActionLimitDiv.innerHTML = "(" + (actionTable.rows.length-1) + "/" + triggerActionLimit + ")";
}

function changeAction(rowId) {
    let actionTable = document.getElementById("actionTable");
    let row = actionTable.rows[rowId];
    let keyValueCell = row.cells[0];
    let valueCell = row.cells[1];

    let keyValueSelection;
    //Loops through the keyValue cell to find the select element. This is because new actions and old actions have it on a different index
    for (let i = 0; i < keyValueCell.childNodes.length; i++) {
        if (keyValueCell.childNodes[i].tagName === 'SELECT') {
            keyValueSelection = keyValueCell.childNodes[i];
        }
    }

    let newKeyValue = keyValueSelection.options[keyValueSelection.selectedIndex].innerHTML;
    let valueType = dict.ActionKeyValues[newKeyValue].Type;

    changeValue(valueCell, valueType, row.id);
}

function changeValue (valueCell, valueType, rowId) {
    while (valueCell.firstChild) {
        valueCell.removeChild(valueCell.lastChild);
    }

    let appendDateElements = false;
    let newValue;

    switch(valueType) {
        case "STRING":
        case "STRING_LIST":
            newValue = document.createElement("input");
            newValue.type = "text";
            newValue.maxLength = 60;
            break;
        case "KEY_VALUE":
            newValue = document.createElement("select");
            for (let i in dict.ConditionKeyValues) {
                let option = document.createElement("option");
                option.value = dict.ConditionKeyValues[i].Key;
                option.innerHTML = dict.ConditionKeyValues[i].Name;
                newValue.append(option);
            }
            break;
        case "NUMBER":
            newValue = document.createElement("input");
            newValue.type = "number";
            break;
        case "PROVIDER":
            newValue = document.createElement("select");
            for (let i = 0; i < Object.keys(providerDict).length; i++) {
                let option = document.createElement("option");
                option.value = providerDict[i].ProviderNo;
                option.innerHTML = providerDict[i].Name;
                newValue.append(option);
            }
            break;
        case "BOOLEAN":
            newValue = document.createElement("select");
            let option_true = document.createElement("option");
            option_true.innerHTML = "True";
            newValue.append(option_true);

            let option_false = document.createElement("option");
            option_false.innerHTML = "False";
            newValue.append(option_false);
            break;
        case "DATE":
            newValue = document.createElement("input");
            newValue.type = "text";
            newValue.maxLength = 60;
            newValue.size = 11;
            appendDateElements = true;
            break;
        case "NO_VALUE":
            newValue = document.createElement("input");
            newValue.type = "text";
            newValue.disabled = true;
            break;
        case "LIST":
            newValue = document.createElement("select");
            for (let i = 0; i < Object.keys(triggerListDict).length; i++) {
                let option = document.createElement("option");
                option.value = triggerListDict[i].Id;
                option.innerHTML = triggerListDict[i].Name;
                newValue.append(option);
            }
            break;
    }

    let rowIdValues = rowId.split("_");
    newValue.id = rowIdValues[0] + "Value_" + rowIdValues[1];
    newValue.name = rowIdValues[0] + "Value_" + rowIdValues[1];
    valueCell.appendChild(newValue);

    if (appendDateElements) {
        let calendarImg = document.createElement("img");
        calendarImg.src = "../images/cal.gif";
        calendarImg.id = newValue.id + '_cal';
        valueCell.appendChild(calendarImg);
        createStandardDatepicker(jQuery_3_1_0('#' + newValue.id), newValue.id + '_cal');
    }
}

function operatorOnFocus (rowId) {
    //When an operator select is clicked on, this function ensures that the onchange functions are properly attached.
    let conditionTable = document.getElementById("conditionTable");
    let row = conditionTable.rows[rowId];

    let comparatorCell = row.cells[1];
    let comparatorSelect;
    for (let i = 0; i < comparatorCell.childNodes.length; i++) {
        if (comparatorCell.childNodes[i].tagName === 'SELECT') {
            comparatorSelect = comparatorCell.childNodes[i];
        }
    }

    if (comparatorSelect.options[comparatorSelect.selectedIndex].value !== 'IN_LIST' && comparatorSelect.options[comparatorSelect.selectedIndex].value !== 'NOT_IN_LIST') {
        comparatorSelect.setAttribute ('onchange', 'changeToList(\'' + rowId + '\');');
    } else {
        comparatorSelect.setAttribute ('onchange', 'changeFromList(\'' + rowId + '\');');
    }
    comparatorSelect.setAttribute ('onfocus', 'operatorOnFocus(\'' + rowId + '\');');
}

function changeFromList(rowId) {
    //If the operator select is changed from a list, a check needs to be done if it's changing to a non-list operator. If so, the input value of the row needs to be changed to a
    //non-list input.
    let conditionTable = document.getElementById("conditionTable");
    let row = conditionTable.rows[rowId];

    let comparatorCell = row.cells[1];
    let comparatorSelect;
    for (let i = 0; i < comparatorCell.childNodes.length; i++) {
        if (comparatorCell.childNodes[i].tagName === 'SELECT') {
            comparatorSelect = comparatorCell.childNodes[i];
        }
    }

    if (comparatorSelect.options[comparatorSelect.selectedIndex].value !== 'IN_LIST' && comparatorSelect.options[comparatorSelect.selectedIndex].value !== 'NOT_IN_LIST') {
        let keyValueCell = row.cells[0];
        let keyValueSelection;
        for (let i = 0; i < keyValueCell.childNodes.length; i++) {
            if (keyValueCell.childNodes[i].tagName === 'SELECT') {
                keyValueSelection = keyValueCell.childNodes[i];
            }
        }

        let newKeyValue = keyValueSelection.options[keyValueSelection.selectedIndex].innerHTML;
        let valueType = dict.ConditionKeyValues[newKeyValue].Type;

        let valueCell = row.cells[2];
        changeValue(valueCell, valueType, row.id);
        operatorOnFocus(rowId);
    }
}

function changeToList(rowId) {
    //If the operator select is changed from a non-list, a check needs to be done if it's changing to a list operator. If so, the input value of the row needs to be changed to a
    //list input.
    let conditionTable = document.getElementById("conditionTable");
    let row = conditionTable.rows[rowId];

    let comparatorCell = row.cells[1];
    let comparatorSelect;
    for (let i = 0; i < comparatorCell.childNodes.length; i++) {
        if (comparatorCell.childNodes[i].tagName === 'SELECT') {
            comparatorSelect = comparatorCell.childNodes[i];
        }
    }

    if (comparatorSelect.options[comparatorSelect.selectedIndex].value === 'IN_LIST' || comparatorSelect.options[comparatorSelect.selectedIndex].value === 'NOT_IN_LIST') {
        let valueCell = row.cells[2];
        changeValue(valueCell, 'LIST', row.id);
        operatorOnFocus(rowId);
    }
}

function removeCondition(rowId) {
    let conditionTable = document.getElementById("conditionTable");
    let row = conditionTable.rows[rowId];
    conditionTable.deleteRow(row.rowIndex);
    if (conditionTable.rows.length-1 < triggerConditionLimit) {
        let addConditionButton = document.getElementById("addConditionButton");
        addConditionButton.disabled = false;
    }

    let triggerConditionLimitDiv = document.getElementById("triggerConditionLimitDiv");
    triggerConditionLimitDiv.innerHTML = "(" + (conditionTable.rows.length-1) + "/" + triggerConditionLimit + ")";
}

function removeAction (rowId) {
    let actionTable = document.getElementById("actionTable");
    let row = actionTable.rows[rowId];
    actionTable.deleteRow(row.rowIndex);
    if (actionTable.rows.length-1 < triggerActionLimit) {
        let addActionButton = document.getElementById("addActionButton");
        addActionButton.disabled = false;
    }

    let triggerActionLimitDiv = document.getElementById("triggerActionLimitDiv");
    triggerActionLimitDiv.innerHTML = "(" + (actionTable.rows.length-1) + "/" + triggerActionLimit + ")";
}

function changeTriggerType (e) {
    let triggerName = document.getElementById("triggerName");
    self.location.href = "TriggerAddEdit.jsp?triggerType=" + e.options[e.selectedIndex].value + "&triggerName=" + triggerName.value;
}

function reorderRow (rowIdA, direction) {
    //Swaps two rows and changes the order value
    let table = rowIdA.includes("Action") ? document.getElementById("actionTable") : document.getElementById("conditionTable");
    let rowA = table.rows[rowIdA];

    if (rowA != null) {
        let rowIndexB = direction === 'down' ? rowA.rowIndex + 1 : rowA.rowIndex - 1;
        let rowB = table.rows[rowIndexB];

        if (rowB != null && rowIndexB !== 0) {
            let rowIdB = rowB.id;
            let idA = rowA.id.split("_");
            let idB = rowB.id.split("_");

            let rowOrderA = document.getElementById(idA[0] + "Order_" + idA[1]);
            let rowOrderB = document.getElementById(idB[0] + "Order_" + idB[1]);

            let tempOrder = rowOrderA.value;
            rowOrderA.value = rowOrderB.value;
            rowOrderB.value = tempOrder;

            if (direction === 'up') {
                table.childNodes[1].insertBefore(table.rows[rowIdA], table.rows[rowIdB]);
            } else {
                table.childNodes[1].insertBefore(table.rows[rowIdB], table.rows[rowIdA]);
            }
        }
    }
}