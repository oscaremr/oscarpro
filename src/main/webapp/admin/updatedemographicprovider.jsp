<!DOCTYPE html>
<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Properties" %>
<%@ page import="org.oscarehr.common.dao.DemographicExtDao" %>
<%@ page import="org.oscarehr.common.model.DemographicExt" %>
<%@ page import="org.oscarehr.common.model.Provider" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.OscarProperties" %>

<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%
    String roleName$ =
            (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed = true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.misc" rights="r"
                   reverse="<%=true%>">
    <%authed = false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin&type=_admin.misc");%>
</security:oscarSec>
<%
    if (!authed) {
        return;
    }


    DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
    ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
    Properties oscarVariables = OscarProperties.getInstance();
    List<String> names = new ArrayList<String>();
    String nurseMessageKey = "admin.updatedemographicprovider.msgNurse";
    String midwifeMessageKey = "admin.updatedemographicprovider.msgMidwife";
    String residentMessageKey = "admin.updatedemographicprovider.msgResident";
    if (oscarVariables.getProperty("queens_resident_tagging") != null) {
        nurseMessageKey = "admin.updatedemographicprovider.msgAltProvider1";
        midwifeMessageKey = "admin.updatedemographicprovider.msgAltProvider2";
        residentMessageKey = "admin.updatedemographicprovider.msgAltProvider3";
    }
%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<html:html locale="true">
    <head>
        <title><bean:message key="admin.admin.btnUpdatePatientProvider"/></title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet"
                type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <script language="javascript">
      function setregexp() {
        document.ADDAPPT.regexp.value = "^[" + document.ADDAPPT.last_name_from.value + "-"
            + document.ADDAPPT.last_name_to.value + "]";
      }

      function setregexp1() {
        document.ADDAPPT1.regexp.value = "^[" + document.ADDAPPT1.last_name_from.value + "-"
            + document.ADDAPPT1.last_name_to.value + "]";
      }

      function setregexp2() {
        document.ADDAPPT2.regexp.value = "^[" + document.ADDAPPT2.last_name_from.value + "-"
            + document.ADDAPPT2.last_name_to.value + "]";
      }
    </script>
    <%
        for (Provider p : providerDao.getActiveProviders()) {
            names.add(p.getProviderNo());
            names.add(p.getFormattedName());
        }
    %>
    <body onLoad="setfocus()">
    <div class="container-fluid">
        <h3><bean:message key="admin.admin.btnUpdatePatientProvider"/></h3>
        <%
            if (request.getParameter("update") != null
                    && request.getParameter("update").equals("UpdateResident")) {
                // find demographicNos for records with last name starting with and have a resident assigned
                List<Integer> noList = demographicExtDao
                        .getDemographicNumbersByResidentNumberAndDemographicLastNameRegex(
                            request.getParameter("oldcust2"), request.getParameter("regexp")
                        );
                int rowsAffected = 0;
                if (noList != null) {
                    int nosize = noList.size();
                    if (nosize != 0) {
                        String[] param = new String[nosize + 2];
                        param[0] = request.getParameter("newcust2");
                        param[1] = request.getParameter("oldcust2");
                        param[2] = noList.get(0).toString();
                        if (nosize > 1) {
                            for (int i = 1; i < nosize; i++) {
                                param[i + 2] = noList.get(i).toString();
                            }
                        }
                        List<Integer> demoList = new ArrayList<Integer>();
                        for (int x = 2; x < param.length; x++) {
                            demoList.add(Integer.parseInt(param[x]));
                        }
                        // get demographicExt entries in demo list with old provider
                        List<DemographicExt> residents = demographicExtDao
                                .getMultipleResidentForDemographicNumbersByProviderNumber(
                                    demoList, param[1]
                                );
                        for (DemographicExt resident : residents) {
                            resident.setValue(param[0]);
                            demographicExtDao.saveDemographicExt(resident);
                        }
                        rowsAffected = residents.size();
                    }
                }
        %>
        <%=rowsAffected %>
        <bean:message key="admin.updatedemographicprovider.msgRecords"/>
        <br>
        <%
            }
            if (request.getParameter("update") != null
                    && request.getParameter("update").equals("UpdateNurse")) {
                List<Integer> noList = demographicExtDao
                        .getDemographicNumbersByNurseNumberAndDemographicLastNameRegex(
                            request.getParameter("oldcust1"),
                            request.getParameter("regexp")
                        );
                int rowsAffected = 0;
                if (noList != null) {
                    int nosize = noList.size();
                    if (nosize != 0) {
                        String[] param = new String[nosize + 2];
                        param[0] = request.getParameter("newcust1");
                        param[1] = request.getParameter("oldcust1");
                        param[2] = noList.get(0).toString();
                        if (nosize > 1) {
                            for (int i = 1; i < nosize; i++) {
                                param[i + 2] = noList.get(i).toString();
                            }
                        }
                        List<Integer> demoList = new ArrayList<Integer>();
                        for (int x = 2; x < param.length; x++) {
                            demoList.add(Integer.parseInt(param[x]));
                        }
                        List<DemographicExt> nurses = demographicExtDao.
                                getMultipleNurseForDemographicNumbersByProviderNumber(
                                    demoList, param[1]
                                );
                        for (DemographicExt nurse : nurses) {
                            nurse.setValue(param[0]);
                            demographicExtDao.saveDemographicExt(nurse);
                        }
                        rowsAffected = nurses.size();
                    }
                }
        %>
        <%=rowsAffected %>
        <bean:message key="admin.updatedemographicprovider.msgRecords"/>
        <br>
        <%
            }
            if (request.getParameter("update") != null
                    && request.getParameter("update").equals("UpdateMidwife")) {
                List<Integer> noList = demographicExtDao
                        .getDemographicNumbersByMidwifeNumberAndDemographicLastNameRegex(
                            request.getParameter("oldcust4"),
                            request.getParameter("regexp")
                        );
                int rowsAffected = 0;
                if (noList != null) {
                    int nosize = noList.size();
                    if (nosize != 0) {
                        String[] param = new String[nosize + 2];
                        param[0] = request.getParameter("newcust4");
                        param[1] = request.getParameter("oldcust4");
                        param[2] = noList.get(0).toString();

                        if (nosize > 1) {
                            for (int i = 1; i < nosize; i++) {
                                param[i + 2] = noList.get(i).toString();
                            }
                        }

                        List<Integer> demoList = new ArrayList<Integer>();
                        for (int x = 2; x < param.length; x++) {
                            demoList.add(Integer.parseInt(param[x]));
                        }
                        List<DemographicExt> midwives = demographicExtDao
                                .getMultipleMidwifeForDemographicNumbersByProviderNumber(
                                    demoList,
                                    param[1]
                                );
                        for (DemographicExt midwife : midwives) {
                            midwife.setValue(param[0]);
                            demographicExtDao.saveDemographicExt(midwife);
                        }
                        rowsAffected = midwives.size();
                    }
                }
        %>
        <%= rowsAffected %>
        <bean:message key="admin.updatedemographicprovider.msgRecords"/>
        <br>
        <% } %>
        <!-- for nurse -->
        <div class="well well-small">
            <table class="table table-striped  table-condensed">
                <FORM NAME="ADDAPPT1" METHOD="post"
                      ACTION="updatedemographicprovider.jsp" onsubmit="return(setregexp1())">
                    <tr>
                        <td>
                            <b><bean:message key="<%= nurseMessageKey %>"/></b>
                        </td>
                    </tr>
                    <tr>
                        <td><bean:message key="admin.updatedemographicprovider.formReplace"/>
                            <select name="oldcust1">
                                <% for (int i = 0; i < names.size(); i = i + 2) { %>
                                    <option value="<%= Encode.forHtmlContent(names.get(i)) %>">
                                        <%= Encode.forHtmlContent(names.get(i + 1)) %>
                                    </option>
                                <% } %>
                            </select>
                            <bean:message key="admin.updatedemographicprovider.formWith"/>
                            <select name="newcust1">
                                <option value="">
                                    <bean:message key="admin.updatedemographicprovider.msgNoProvider"/>
                                </option>
                                <% for (int i = 0; i < names.size(); i = i + 2) { %>
                                    <option value="<%=Encode.forHtmlContent(names.get(i))%>">
                                        <%=Encode.forHtmlContent(names.get(i + 1))%>
                                    </option>
                                <% } %>
                            </select><br>
                            <bean:message key="admin.updatedemographicprovider.formCondition"/>
                            <select name="last_name_from">
                                <%
                                    char cletter = 'A';
                                    for (int i = 0; i < 26; i++) {
                                %>
                                    <option value="<%= (char) (cletter + i) %>">
                                        <%= (char) (cletter + i) %>
                                    </option>
                                <% } %>
                            </select>
                            <bean:message key="admin.updatedemographicprovider.formTo"/>
                            <select name="last_name_to">
                                <%
                                    cletter = 'A';
                                    for (int i = 0; i < 26; i++) {
                                %>
                                    <option value="<%= (char) (cletter + i) %>">
                                        <%= (char) (cletter + i) %>
                                    </option>
                                <% } %>
                            </select> <br>
                            <INPUT type="hidden" name="regexp" value="">
                            <input type="hidden" name="update" value="UpdateNurse">
                            <INPUT class="btn btn-primary" type="submit"
                                   value="<bean:message key="global.update"/>">
                        </td>
                    </tr>
                </form>
            </table>
        </div>
        <!-- for midwife -->
        <div class="well well-small">
            <table class="table table-striped  table-condensed">
                <FORM NAME="ADDAPPT2" METHOD="post"
                      ACTION="updatedemographicprovider.jsp" onsubmit="return(setregexp2())">
                    <tr>
                        <td><b><bean:message key="<%= midwifeMessageKey %>"/></b></td>
                    </tr>
                    <tr>
                        <td>
                            <bean:message key="admin.updatedemographicprovider.formReplace"/>
                            <select name="oldcust4">
                                <%
                                    for (int i = 0; i < names.size(); i = i + 2) {
                                %>
                                <option value="<%= Encode.forHtmlContent(names.get(i)) %>">
                                    <%= Encode.forHtmlContent(names.get(i + 1)) %>
                                </option>
                                <% } %>
                            </select>
                            <bean:message key="admin.updatedemographicprovider.formWith"/>
                            <select name="newcust4">
                                <option value="">
                                    <bean:message key="admin.updatedemographicprovider.msgNoProvider"/>
                                </option>
                                <% for (int i = 0; i < names.size(); i = i + 2) { %>
                                    <option value="<%= Encode.forHtmlContent(names.get(i)) %>">
                                        <%= Encode.forHtmlContent(names.get(i + 1)) %>
                                    </option>
                                <% } %>
                            </select><br>
                            <bean:message key="admin.updatedemographicprovider.formCondition"/>
                            <select name="last_name_from">
                                <%
                                    cletter = 'A';
                                    for (int i = 0; i < 26; i++) {
                                %>
                                    <option value="<%=(char) (cletter+i) %>">
                                        <%= (char) (cletter + i) %>
                                    </option>
                                <% } %>
                            </select>
                            <bean:message key="admin.updatedemographicprovider.formTo"/>
                            <select
                                    name="last_name_to">
                                <%
                                    cletter = 'A';
                                    for (int i = 0; i < 26; i++) {
                                %>
                                    <option value="<%=(char) (cletter+i) %>">
                                        <%= (char) (cletter + i) %>
                                    </option>
                                <% } %>
                            </select> <br>
                            <input type="hidden" NAME="regexp" value="">
                            <input type="hidden" name="update" value="UpdateMidwife">
                            <input class="btn btn-primary" type="submit"
                                   value="<bean:message key="global.update"/>">
                        </td>
                    </tr>
                </form>
            </table>
        </div>
        <!--  for resident -->
        <div class="well well-small">
            <table class="table table-striped  table-condensed">
                <FORM NAME="ADDAPPT" METHOD="post"
                      ACTION="updatedemographicprovider.jsp" onsubmit="return(setregexp())">
                    <tr>
                        <td><b><bean:message key="<%= residentMessageKey %>"/></b></td>
                    </tr>
                    <tr>
                        <td><bean:message key="admin.updatedemographicprovider.formReplace"/>
                            <select name="oldcust2">
                                <% for (int i = 0; i < names.size(); i = i + 2) { %>
                                <option value="<%= Encode.forHtmlContent(names.get(i)) %>">
                                    <%= Encode.forHtmlContent(names.get(i + 1)) %>
                                </option>
                                <% } %>
                            </select>
                            <bean:message key="admin.updatedemographicprovider.formWith"/>
                            <select name="newcust2">
                                <option value="">
                                    <bean:message key="admin.updatedemographicprovider.msgNoProvider"/>
                                </option>
                                <% for (int i = 0; i < names.size(); i = i + 2) { %>
                                <option value="<%= Encode.forHtmlContent(names.get(i)) %>">
                                    <%= Encode.forHtmlContent(names.get(i + 1)) %>
                                </option>
                                <% } %>
                            </select><br>
                            <bean:message key="admin.updatedemographicprovider.formCondition"/>
                            <select name="last_name_from">
                                <%
                                    cletter = 'A';
                                    for (int i = 0; i < 26; i++) {
                                %>
                                <option value="<%= (char) (cletter+i) %>">
                                    <%= (char) (cletter + i) %>
                                </option>
                                <% } %>
                            </select>
                            <bean:message key="admin.updatedemographicprovider.formTo"/>
                            <select  name="last_name_to">
                                <%
                                    cletter = 'A';
                                    for (int i = 0; i < 26; i++) {
                                %>
                                    <option value="<%=(char) (cletter+i) %>">
                                        <%= (char) (cletter + i) %>
                                    </option>
                                <% } %>
                            </select> <br>
                            <INPUT TYPE="hidden" NAME="regexp" VALUE=""> <input
                                    type="hidden" name="update" value="UpdateResident"> <INPUT
                                    class="btn btn-primary"
                                    TYPE="submit"
                                    VALUE="<bean:message key="global.update"/>">
                        </td>
                    </tr>
                </form>
            </table>
        </div>


    </div>
    </body>
</html:html>
