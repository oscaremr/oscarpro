<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ page import="ca.kai.datasharing.DataSharingService" %>
<%@ page import="ca.kai.datasharing.PatientPortalData" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="org.oscarehr.util.WebUtilsOld"%>
<%@ page import="org.oscarehr.myoscar.utils.MyOscarLoggedInInfo" %>
<%@ page import="org.oscarehr.phr.util.MyOscarUtils" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.util.LocaleUtils"%>
<%@ page import="org.oscarehr.util.MiscUtils" %>
<%@ page import="org.oscarehr.managers.PreventionManager" %>
<%@page import="org.oscarehr.managers.CanadianVaccineCatalogueManager2"%>
<%@page import="org.oscarehr.managers.SecurityInfoManager"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="org.oscarehr.common.model.UserProperty"%>
<%@page import="org.oscarehr.common.dao.UserPropertyDAO"%>
<%@page import="org.oscarehr.common.model.CVCMapping"%>
<%@page import="org.oscarehr.common.dao.CVCMappingDao"%>
<%@page import="org.oscarehr.common.model.DHIRSubmissionLog"%>
<%@page import="org.oscarehr.managers.DHIRSubmissionManager"%>
<%@page import="org.oscarehr.common.model.Consent"%>
<%@page import="org.oscarehr.common.dao.ConsentDao"%>
<%@page import="oscar.OscarProperties"%>
<%@page import="oscar.oscarDemographic.data.*,java.util.*,oscar.oscarPrevention.*"%>
<%@page import="org.oscarehr.common.dao.DemographicDao, org.oscarehr.common.model.Demographic" %>
<%@page import="org.oscarehr.util.WebUtils"%>
<%@ page import="org.oscarehr.common.dao.PreventionsLotNrsDao" %>
<%@ page import="org.oscarehr.common.model.PreventionsLotNrs" %>
<%@ page import="org.oscarehr.common.dao.BillingONItemDao" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.log.LogAction" %>
<%@ page import="oscar.log.LogConst" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="oscar.oscarPrevention.PreventionData" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="oscar.oscarDemographic.data.*,java.util.*,oscar.oscarPrevention.*" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.oscarehr.integration.OneIDTokenUtils" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>


<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>
<%@ taglib uri="/WEB-INF/rewrite-tag.tld" prefix="rewrite"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib uri="/WEB-INF/wellAiVoice.tld" prefix="well-ai-voice"%>
<%
      String roleName$ = session.getAttribute("userrole") + "," + session.getAttribute("user");
	  boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_prevention" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_prevention");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}
%>
<%
	DataSharingService dataSharingService = SpringUtils.getBean(DataSharingService.class);
	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
	DHIRSubmissionManager submissionManager = SpringUtils.getBean(DHIRSubmissionManager.class);
	UserPropertyDAO userPropertyDao = SpringUtils.getBean(UserPropertyDAO.class);
	SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
	String demographic_no = request.getParameter("demographic_no");
	LogAction.addLog(loggedInInfo, LogConst.READ, "Preventions", demographic_no, demographic_no, (String)null);
	boolean preventionShowComments = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("prevention_show_comments", true);
	boolean immunizationInPreventionEnabled = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("immunization_in_prevention_enabled", false);

	Date today = new Date();
	
	PreventionsLotNrsDao plnd = SpringUtils.getBean(PreventionsLotNrsDao.class);
	List<PreventionsLotNrs> pln = plnd.findAll();
	
		for (PreventionsLotNrs pItem : pln)
		{
			if (pItem.getExpiryDate()!=null && !pItem.getExpiryDate().equals(""))
			{
				switch(today.compareTo(pItem.getExpiryDate()))
				{
					case 1:
						pItem.setDeleted(true);
						plnd.merge(pItem);
						break;

					default:
						break;
				}
			}
		}
	
  DemographicData demoData = new DemographicData();
  String nameAge = demoData.getNameAgeString(loggedInInfo, demographic_no);
  org.oscarehr.common.model.Demographic demo = demoData.getDemographic(loggedInInfo, demographic_no);
  String hin = demo.getHin()+demo.getVer();
  String mrp = demo.getProviderNo();
  PreventionManager preventionManager = SpringUtils.getBean(PreventionManager.class);
  SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
  String todayString = simpleDateFormat.format(Calendar.getInstance().getTime());
  PreventionDisplayConfig pdc = PreventionDisplayConfig.getInstance();
  ArrayList<HashMap<String,String>> prevList = pdc.getPreventions();
  ArrayList<Map<String,Object>> configSets = pdc.getConfigurationSets();
  Prevention p = PreventionData.getPrevention(loggedInInfo, Integer.valueOf(demographic_no));

	BillingONItemDao billingONItemDao = SpringUtils.getBean(BillingONItemDao.class);
	List<String> exclusions = new ArrayList<String>();
	String billRegion = OscarProperties.getInstance().getProperty("billregion", "ON").trim().toUpperCase();
	if (billRegion.equals("ON"))
	{
		exclusions = billingONItemDao.getPreventionExclusionsByDemographicNo(Integer.valueOf(demographic_no));
	}
  
  Integer demographicId=Integer.parseInt(demographic_no);
  PreventionData.addRemotePreventions(loggedInInfo, p, demographicId);
  Date demographicDateOfBirth=PreventionData.getDemographicDateOfBirth(loggedInInfo, Integer.valueOf(demographic_no));
  PreventionDS pf = SpringUtils.getBean(PreventionDS.class);

  CVCMappingDao cvcMappingDao = SpringUtils.getBean(CVCMappingDao.class);
  boolean demographicPortalEnabled = dataSharingService.isDemographicPortalEnabled(demographicId);

  boolean dsProblems = false;
  try {
     pf.getMessages(p);
  } catch(Exception dsException) {
	  MiscUtils.getLogger().error("Error running prevention rules",dsException);
      dsProblems = true;
  }

  Map<String, Object> warnings = p.getWarningMsgs();
  ArrayList<String> recommendations = p.getReminder();

  boolean printError = request.getAttribute("printError") != null;


	ConsentDao consentDao = SpringUtils.getBean(ConsentDao.class);
	Consent ispaConsent =  consentDao.findByDemographicAndConsentType(demographicId, "dhir_ispa_consent");
	Consent nonIspaConsent =  consentDao.findByDemographicAndConsentType(demographicId, "dhir_non_ispa_consent");

	boolean isSSOLoggedIn = session.getAttribute("oneIdEmail") != null;
	boolean hasIspaConsent = ispaConsent != null && !ispaConsent.isOptout();
	boolean hasNonIspaConsent = nonIspaConsent != null && !nonIspaConsent.isOptout();

	UserProperty ssoWarningUp = userPropertyDao.getProp(LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo(), UserProperty.PREVENTION_SSO_WARNING);
	boolean hideSSOWarning = ssoWarningUp != null && "true".equals(ssoWarningUp.getValue());

	UserProperty ispaWarningUp = userPropertyDao.getProp(LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo(), UserProperty.PREVENTION_ISPA_WARNING);
	boolean hideISPAWarning = ispaWarningUp != null && "true".equals(ispaWarningUp.getValue());

	UserProperty nonIspaWarningUp = userPropertyDao.getProp(LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo(), UserProperty.PREVENTION_NON_ISPA_WARNING);
	boolean hideNonISPAWarning = nonIspaWarningUp != null && "true".equals(nonIspaWarningUp.getValue());

	boolean canUpdateCVC = securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_prevention.updateCVC", "r", null);


%>

<%!
	public String getFromFacilityMsg(Map<String,Object> ht)
	{
		if (ht.get("id")==null)	return("<br /><span style=\"color:#990000\">(At facility : "+ht.get("remoteFacilityName")+")<span>");
		else return("");
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html:html locale="true">

<head>
	<title><bean:message key="oscarprevention.index.oscarpreventiontitle" /></title>
	<link rel="stylesheet" href="<%= request.getContextPath() %>/share/css/OscarStandardLayout.css"/>
	<script src="<%= request.getContextPath() %>/js/global.js"></script>
	<script src="<%= request.getContextPath() %>/js/jquery.js"></script>
	<script src="<%= request.getContextPath() %>/JavaScriptServlet"></script>
	<script src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
	<script src="<%= request.getContextPath() %>/share/javascript/prototype.js"></script>
	<script src="<%= request.getContextPath() %>/data-sharing/dataSharingServiceForOscar.js"></script>
	<script src="<%= request.getContextPath() %>/share/javascript/oneid.js"></script>

<style type="text/css">
div.ImmSet {
	background-color: #ffffff;
	clear: left;
	margin-top: 10px;
}

div.ImmSet h2 {
}

div.ImmSet h2 span {
	font-size: smaller;
}

div.ImmSet ul,
div.ImmSet li {
}

div.ImmSet li a {
	text-decoration: none;
	color: blue;
}

div.ImmSet li a:hover {
	text-decoration: none;
	color: red;
}

div.ImmSet li a:visited {
	text-decoration: none;
	color: blue;
}

div.onPrint {
	display: none;
}

span.footnote {
    background-color: #ccccee;
    border: 1px solid #000;
    width: 4px;
}
</style>

<link rel="stylesheet" type="text/css" href="../share/css/niftyCorners.css" />
<link rel="stylesheet" type="text/css" href="../share/css/niftyPrint.css" media="print" />
<link rel="stylesheet" type="text/css" href="preventPrint.css" media="print" />

<script type="text/javascript" src="../share/javascript/nifty.js"></script>
<script type="text/javascript">
window.onload=function(){
if(!NiftyCheck())
    return;

Rounded("div.headPrevention","all","#CCF","#efeadc","small border blue");
Rounded("div.preventionProcedure","all","transparent","#F0F0E7","small border #999");
Rounded("div.leftBox","top","transparent","#CCCCFF","small border #ccccff");
Rounded("div.leftBox","bottom","transparent","#EEEEFF","small border #ccccff");
}

function display(elements) {

    for( var idx = 0; idx < elements.length; ++idx )
        elements[idx].style.display = 'block';
}

function EnablePrint(button) {
    if( button.value == "Enable Print" ) {
        button.value = "Print";
        document.getElementById("printImmunizations").style.display = 'none';
        var checkboxes = document.getElementsByName("printHP");
        display(checkboxes);
        checkboxes = document.getElementsByName("immunization");
        display(checkboxes);
        var spaces = document.getElementsByName("printSp");
        display(spaces);
        button.form.sendToPhrButton.style.display = 'block';
    } else if (button.value === "Enable Print Immunizations") {
        document.getElementById("printButton").style.display = 'none';
        document.getElementById("printImmunizations").style.fontSize = 'inherit';
        button.value = "Print Immunizations";
		var checkboxes = document.getElementsByName("immunization");
		display(checkboxes);
        var spaces = document.getElementsByName("printSp");
        display(spaces);
        spaces = document.getElementsByName("printHP");
        for (var i = 0; i < spaces.length; i++) {
            spaces[i].removeAttribute("checked");
		}
        display(spaces);
	} else if (button.value === "Print Immunizations") {
        if (onPrint()) {
            document.printFrm.submit();
		}
	} else {
        if( onPrint() )
            document.printFrm.submit();
    }
}

function onPrint() {
    var checked = document.getElementsByName("printHP");
    var thereIsData = false;

    for( var idx = 0; idx < checked.length; ++idx ) {
        if( checked[idx].checked ) {
            thereIsData = true;
            break;
        }
    }

    if( !thereIsData ) {
        checked = document.getElementsByName("immunization");
        for( var i = 0; i < checked.length; ++i ) {
            if( checked[i].checked ) {
                thereIsData = true;
                break;
            }
        }

        if (!thereIsData) {
            alert("You should check at least one prevention by selecting a checkbox next to a prevention");
            return false;
		}
    }
    return true;
}

function sendToPhr(button) {
    var oldAction = button.form.action;
    button.form.action = "<%=request.getContextPath()%>/phr/SendToPhrPreview.jsp"
    button.form.submit();
    button.form.action = oldAction;
}

function newWindow(file,window) {
  msgWindow=open(file,window,'scrollbars=yes,width=760,height=520,screenX=0,screenY=0,top=0,left=10');
  if (msgWindow.opener == null) msgWindow.opener = self;
}

function addByLot() {
	var lotNbr = $("#lotNumberToAdd").val();

	popup(600,900,'AddPreventionData.jsp?demographic_no=<%=demographic_no%>&lotNumber=' + lotNbr,'addPreventionData' + <%=new java.util.Random().nextInt(10000) + 1%> );

}

function viewDHIRSummary() {
	// Will first check if the sessions exists and is not expired,
	// if expired it will attempt to refresh using the refresh token.
	jQuery.ajax({
		type: "GET",
		url: "<%= OscarProperties.getOscarProBaseUrl() %>/api/one-id/isSessionValid",
		success: function(data) {
			if (data) {
				// If the session exists and is not expired, proceed to the DHIR page
				popupEHRService('ViewDHIRData.jsp?demographic_no=<%=demographic_no%>', '<%=demographic_no%>', '<%= OscarProperties.getOscarProBaseUrl()%>');
			} else {
				// If the session does not exist at all, direct the user to Ontario Health login
				popupEHRService('<%= OscarProperties.getOscarProBaseUrl()%>/api/one-id/login?forward=<%=OscarProperties.getOscarClassicBaseUrl()%>/oscarPrevention/ViewDHIRData.jsp?demographic_no=<%=demographic_no%>', '<%=demographic_no%>', '<%= OscarProperties.getOscarProBaseUrl()%>');
			}
		},
		error: function() {
			alert('There was an error checking your ONE ID Session. Please try again.');
		}
	});
}
</script>

<style type="text/css">
body {
	font-size: 100%
}

div.news {
	width: 100px;
	background: #FFF;
	margin-bottom: 20px;
	margin-left: 20px;
}

div.leftBox {
	width: 90%;
	margin-top: 2px;
	margin-left: 3px;
	margin-right: 3px;
	float: left;
}

div.leftBox h3 {
	background-color: #ccccff;
	/*font-size: 1.25em;*/
	font-size: 8pt;
	font-variant: small-caps;
	font: bold;
	margin-top: 0px;
	padding-top: 0px;
	margin-bottom: 0px;
	padding-bottom: 0px;
}

div.leftBox ul { /*border-top: 1px solid #F11;*/
	/*border-bottom: 1px solid #F11;*/
	font-size: 1.0em;
	list-style: none;
	list-style-type: none;
	list-style-position: outside;
	padding-left: 1px;
	margin-left: 1px;
	margin-top: 0px;
	padding-top: 1px;
	margin-bottom: 0px;
	padding-bottom: 0px;
}

div.leftBox li {
	padding-right: 15px;
	white-space: nowrap;
}

div.headPrevention {
	position: relative;
	float: left;
	width: 8.4em;
	height: 2.5em;
}

div.headPrevention p {
	background: #EEF;
	font-family: verdana, tahoma, sans-serif;
	margin: 0;
	padding: 4px 5px;
	line-height: 1.3;
	text-align: justify height : 2em;
	font-family: sans-serif;
	border-left: 0px;
}

div.headPrevention a {
	text-decoration: none;
}

div.headPrevention a:active,
div.headPrevention a:hover,
div.headPrevention a:link ,
div.headPrevention a:visited {
	color: blue;
}

div.preventionProcedure {
	width: 10em;
	float: left;
	margin-left: 3px;
	margin-bottom: 3px;
}

div.preventionProcedure p {
	font-size: 0.8em;
	font-family: verdana, tahoma, sans-serif;
	background: #F0F0E7;
	margin: 0;
	padding: 1px 2px;
}

div.preventionSection {
	width: 100%;
	postion: relative;
	margin-top: 5px;
	float: left;
	clear: left;
}

div.preventionSet {
	border: thin solid grey;
	clear: left;
}

div.recommendations {
	font-family: verdana, tahoma, sans-serif;
	font-size: 1.2em;
}

div.recommendations ul {
	padding-left: 15px;
	margin-left: 1px;
	margin-top: 0px;
	padding-top: 1px;
	margin-bottom: 0px;
	padding-bottom: 0px;
}

div.recommendations li {
}

table.legend{
border:0;
padding-top:10px;
width:420px;
}

table.legend td{
font-size:8;
text-align:left;
}

table.colour_codes{
width:8px;
height:10px;
border:1px solid #999999;
}
</style>

<!--[if IE]>
<style type="text/css">

table.legend{
border:0;
margin-top:10px;
width:370px;
}

table.legend td{
font-size:10;
text-align:left;
}

</style>
<![endif]-->

<script>
function disableSSOWarning() {
	if(confirm("Are you sure you would like to permanently disable this warning?\nYou may re-enable it from your preferences")) {
        jQuery.ajax({
            type: "POST",
            url:  '<%=request.getContextPath()%>/ws/rs/persona/updatePreference',
            dataType:'json',
            contentType:'application/json',
            data: JSON.stringify({key:'prevention_sso_warning',value:'true'}),
            success: function (data) {
               $("#ssoWarning").hide();
            }
		});
	}
}
function disableISPAWarning() {
	if(confirm("Are you sure you would like to permanently disable this warning?\nYou may re-enable it from your preferences")) {
        jQuery.ajax({
            type: "POST",
            url:  '<%=request.getContextPath()%>/ws/rs/persona/updatePreference',
            dataType:'json',
            contentType:'application/json',
            data: JSON.stringify({key:'prevention_ispa_warning',value:'true'}),
            success: function (data) {
               $("#ispaWarning").hide();
            }
		});
	}
}

function disableNonISPAWarning() {
	if(confirm("Are you sure you would like to permanently disable this warning?\nYou may re-enable it from your preferences")) {
		jQuery.ajax({
            type: "POST",
            url:  '<%=request.getContextPath()%>/ws/rs/persona/updatePreference',
            dataType:'json',
            contentType:'application/json',
            data: JSON.stringify({key:'prevention_non_ispa_warning',value:'true'}),
            success: function (data) {
               $("#nonIspaWarning").hide();
            }
		});
	}
}

<%
	if(canUpdateCVC) {
%>
function updateCVC() {
	jQuery.ajax({
         type: "POST",
         url: "<%=request.getContextPath()%>/cvc.do",
         data: { method : "updateCVC"},
        success: function(data,textStatus) {
        	 alert('CVC has been updated');
         }
	});
}
<% } %>
</script>

</head>

<body class="BodyStyle">
	<well-ai-voice:script/>
<!--  -->
<%=WebUtilsOld.popErrorAndInfoMessagesAsHtml(session)%>
<table class="MainTable" id="scrollNumber1">
	<tr class="MainTableTopRow">
		<td class="MainTableTopRowLeftColumn"><bean:message key="oscarprevention.index.oscarpreventiontitle" /></td>
		<td class="MainTableTopRowRightColumn">
		<table class="TopStatusBar">
			<tr>
				<td>
					<%=Encode.forHtml(nameAge)%>
					<% if (billRegion.equals("ON")) { %>
					<a title="Open Billing Page" 
					   onclick="popupFocusPage(700, 1000, '../billing.do?billRegion=ON&amp;billForm=MFP&amp;hotclick=&amp;appointment_no=0&amp;demographic_name=<%=Encode.forUriComponent(demo.getLastName())%>%2C<%=Encode.forUriComponent(demo.getFirstName())%>&amp;demographic_no=<%=demographic_no%>&amp;providerview=1&amp;user_no=<%=(String) session.getValue("user")%>&amp;apptProvider_no=none&amp;appointment_date=<%=todayString%>&amp;start_time=0:00:00&amp;bNewForm=1&amp;status=t','_self');return false;"
					   href="javascript: function myFunction() {return false; }">
						[B]
					</a>
					<% } else if (billRegion.equals("BC")) {
						String billForm = OscarProperties.getInstance().getProperty("default_view", "");
						String winName = "Billing" + demo.getDemographicNo();
						String demographicName = demo.getLastName() + "," + demo.getFirstName();
					%>
					<a title="Open Billing Page"
					   onclick="popupFocusPage(700, 1000, '../billing.do?billRegion=BC&amp;billForm=<%=URLEncoder.encode(billForm)%>&amp;hotclick=&amp;appointment_no=0&amp;demographic_name=<%=URLEncoder.encode(demographicName)%>&amp;demographic_no=<%=demographic_no%>&amp;providerview=<%=mrp%>&amp;user_no=<%=(String)session.getValue("user")%>&amp;apptProvider_no=none&amp;appointment_date=<%=todayString%>&amp;start_time=00:00:00&amp;bNewForm=1&amp;status=t','<%=winName%>');return false;"
					   href="javascript: function myFunction() {return false; }">
						[B]
					</a>
					<% } %>
				</td>
				<td>&nbsp;</td>
				<td style="text-align: right">
					<% if(canUpdateCVC) { %>
						<a onClick="updateCVC()" href="javascript:void()">Update CVC</a> |
					<% } %>
					<oscar:help keywords="prevention" key="app.top1"/> | <a
					href="javascript:popupStart(300,400,'About.jsp')"><bean:message
					key="global.about" /></a> | <a
					href="javascript:popupStart(300,400,'License.jsp')"><bean:message
					key="global.license" /></a></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td class="MainTableLeftColumn" valign="top">
<% 
	String headingTitle;
	if (prevList.size() > 0) {
		headingTitle = (prevList.get(0).get("headingName") != null) ? prevList.get(0).get("headingName") : "Preventions";
	} else {
		headingTitle = "Preventions";
	}
%>
		<div class="leftBox">
			<h3>&nbsp;<%=headingTitle%></h3>
			<div style="background-color: #EEEEFF;">
			<ul>
<% 
	for (HashMap<String,String> h : prevList) {
		if (!headingTitle.equals(h.get("headingName")) && h.get("headingName") != null) {
			headingTitle = h.get("headingName");
%>
		</ul>
		</div>
		</div>
		<div class="leftBox">
		<h3>&nbsp;<%=headingTitle%></h3>
		<div style="background-color: #EEEEFF;">
		<ul>
<%
		}
		
                String prevName = h.get("name");
				String displayName = h.get("displayName") != null ? h.get("displayName") : prevName;
                           
	            if(!preventionManager.hideItem(prevName)){%>
					<li style="margin-top: 2px;"><a
						href="javascript: function myFunction() {return false; }"
						onclick="javascript:popup(465,635,'AddPreventionData.jsp?prevention=<%= java.net.URLEncoder.encode(prevName) %>&amp;displayName=<%=Encode.forHtmlContent(displayName)%>&amp;demographic_no=<%=demographic_no%>&amp;prevResultDesc=<%= java.net.URLEncoder.encode(h.get("resultDesc")) %>','addPreventionData<%=Math.abs(prevName.hashCode()) %>')" title="<%=h.get("desc")%>">
					<%=displayName%> </a></li>
				<%
				}
			}
			%>
		</ul>
		</div>
		</div>
		<% if (immunizationInPreventionEnabled) { %>
			<a href="javascript: function myFunction() {return false; }"
				onclick="javascript:popup(700,960,'<rewrite:reWrite jspPage="../oscarEncounter/immunization/initSchedule.do"/>?demographic_no=<%=demographic_no%>','oldImms')">Old
			<bean:message key="global.immunizations" /></a>
			<br>
		<% } %>
		</td>

		<form name="printFrm" method="post" onsubmit="return onPrint();"
			action="<rewrite:reWrite jspPage="printPrevention.do"/>">
		<td valign="top" class="MainTableRightColumn">
		<% 
			String immunizationScheduleLink = OscarProperties.getInstance().getProperty("preventions_immunization_link", "http://www.phac-aspc.gc.ca/im/is-cv/index-eng.php");
			String immunizationScheduleLinkText = OscarProperties.getInstance().getProperty("preventions_immunization_link_text", "Immunization Schedules - Public Health Agency of Canada");
		%>
		<a href="#" onclick="popup(600,800,'<%=immunizationScheduleLink %>')"><%=immunizationScheduleLinkText %></a>

		<%
				if (MyOscarUtils.isMyOscarEnabled((String) session.getAttribute("user")))
				{
					MyOscarLoggedInInfo myOscarLoggedInInfo=MyOscarLoggedInInfo.getLoggedInInfo(session);
           		  	boolean enabledMyOscarButton=MyOscarUtils.isMyOscarSendButtonEnabled(myOscarLoggedInInfo, Integer.valueOf(demographic_no));
					if (enabledMyOscarButton)
					{
						String sendDataPath = request.getContextPath() + "/phr/send_medicaldata_to_myoscar.jsp?"
								+ "demographicId=" + demographic_no + "&"
								+ "medicalDataType=Immunizations" + "&"
								+ "parentPage=" + request.getRequestURI() + "?demographic_no=" + demographic_no;
		%>
		| | <a href="<%=sendDataPath%>"><%=LocaleUtils.getMessage(request, "SendToPHR")%></a>
		<%
					}
					else
					{
		%>
		| | <span style="color:grey;text-decoration:underline"><%=LocaleUtils.getMessage(request, "SendToPHR")%></span>
		<%
					}
				}
		%>

		<%
                if (warnings.size() > 0 || recommendations.size() > 0  || dsProblems) { %>
		<div class="recommendations">
		<%
                    if(printError) {
                   %>
		<p style="color: red; font-size: larger">An error occurred while
		trying to print</p>
		<%
                    }
                   %> <span style="font-size: larger;">Prevention
		Recommendations</span>
		<ul>
			<%  Object[] keysObjs = warnings.keySet().toArray();
				String[] warningKeys = Arrays.copyOf(keysObjs, keysObjs.length, String[].class);
				for (int i = 0 ;i < warnings.size(); i++){
				    if (!exclusions.contains(warningKeys[i])) {
					String warn = "";
					if (!preventionManager.hideItem(warningKeys[i])) {
                       warn = (String) warnings.get(warningKeys[i]);
					} %>
			<li style="color: red;"><%=warn%></li>
			<%		}
				} %>
			<% for (int i = 0 ;i < recommendations.size(); i++){
                       String warn = (String) recommendations.get(i);%>
			<li style="color: black;"><%=warn%></li>
			<%}%>
			<!--li style="color: red;">6 month TD overdue</li>
                 <li>12 month MMR due in 2 months</li-->
			<% if (dsProblems){ %>
			<li style="color: red;">Decision Support Had Errors Running.</li>
			<% } %>
		</ul>
		</div>

		<% if (demographicPortalEnabled) { %>
		<div style="margin-top: 10px;">
			<input type="button" id="pushToPortalButton" <%= dataSharingService.getButtonDisabledStatus() %>
				onclick="pushDataToPatientPortal('<%= DataSharingService.getKaiEmrUrl() %>', DataType.PREVENTION, <%= demographic_no %>)"
				value='<bean:message key="yourcare.patientportal.pushToPortal"/>' />
		</div>

		<table style="padding-top: 10px;" cellspacing='0'>
			<tr>
				<td><b>Portal Share Status:</b></td>
				<td class='colour_codes' style="white-space: nowrap;">
					<img src="<%= request.getContextPath() %>/images/pending.png" alt="" style="width: 13px;">
				</td>
				<td align='center' style="white-space: nowrap;">Pending
			 	</td>
				<td class='colour_codes' style="white-space: nowrap;">
					<img src="<%= request.getContextPath() %>/images/check-status.png" alt="" style="width: 13px;">
				</td>
				<td align='center' style="white-space: nowrap;">Read</td>
				<td class='colour_codes' style="white-space: nowrap;">
					<img src="<%= request.getContextPath() %>/images/x.png" alt="" style="width: 13px;">
				</td>
				<td align='center' style="white-space: nowrap;">Removed</td>
			</tr>
		</table> <%
   }
 %> <!-- style="font: Verdana;font-size: 14px;font-weight: bold;background-color: #1A1BFF;color: #FFFFFF;letter-spacing: -0.5px;border-radius: 2px;padding: 9px 12px 0px 12px;margin: 2px 2px 2px 12px; cursor: pointer;" -->
		<% }

	 String[] ColourCodesArray=new String[7];
	 ColourCodesArray[1]="#F0F0E7"; //very light grey - completed or normal
	 ColourCodesArray[2]="#FFDDDD"; //light pink - Refused
	 ColourCodesArray[3]="#FFCC24"; //orange - Ineligible
	 ColourCodesArray[4]="#FF00FF"; //dark pink - pending
	 ColourCodesArray[5]="#ee5f5b"; //dark salmon to match part of bootstraps danger - abnormal
	 ColourCodesArray[6]="#BDFCC9"; //green - other

	 //labels for colour codes
	 String[] lblCodesArray=new String[7];
	 lblCodesArray[1]="Completed or Normal";
	 lblCodesArray[2]="Refused";
	 lblCodesArray[3]="Ineligible";
	 lblCodesArray[4]="Pending";
	 lblCodesArray[5]="Abnormal";
	 lblCodesArray[6]="Other";

	 //Title ie: Legend or Profile Legend
	 String legend_title="Legend: ";

	 //creat empty builder string
	 String legend_builder=" ";


	 	for (int iLegend = 1; iLegend < 7; iLegend++){

			legend_builder +="<td> <table class='colour_codes' style=\"white-space:nowrap;\" bgcolor='"+ColourCodesArray[iLegend]+"'><tr><td> </td></tr></table> </td> <td align='center' style=\"white-space:nowrap;\">"+lblCodesArray[iLegend]+"</td>";

		}

	 	String legend = "<table class='legend' cellspacing='0'><tr><td><b>"+legend_title+"</b></td>"+legend_builder+" </tr></table>";

		out.print(legend);
%>

		<div>
		<input type="hidden" name="demographic_no" value="<%=demographic_no%>">
		<input type="hidden" name="hin" value="<%=hin%>"/>
		<input type="hidden" name="mrp" value="<%=mrp%>" />
                <input type="hidden" name="module" value="prevention">
		<%
                 if (!oscar.OscarProperties.getInstance().getBooleanProperty("PREVENTION_CLASSIC_VIEW","yes")){
                   ArrayList<Map<String,Object>> hiddenlist = new ArrayList<Map<String,Object>>();
                  for (int i = 0 ; i < prevList.size(); i++){
                  		HashMap<String,String> h = prevList.get(i);
                        String prevName = h.get("name");
                        String displayName = h.get("displayName") != null ? h.get("displayName") : prevName;
                        ArrayList<Map<String,Object>> alist = PreventionData.getPreventionData(loggedInInfo, prevName, Integer.valueOf(demographic_no));
                        PreventionData.addRemotePreventions(loggedInInfo, alist, demographicId,prevName,demographicDateOfBirth);
                        boolean show = pdc.display(loggedInInfo, h, demographic_no,alist.size());
                        if(!show){
                            Map<String,Object> h2 = new HashMap<String,Object>();
                            h2.put("prev",h);
                            h2.put("list",alist);
                            hiddenlist.add(h2);
                        }else{
               %>

		<div class="preventionSection">
		<%
			String snomedId = h.get("snomedConceptCode") != null ? h.get("snomedConceptCode") : null;
	        boolean ispa = h.get("ispa") != null ? Boolean.valueOf(h.get("ispa")) : false;
	        String ispa1="";
	        if(ispa) {
	       	 ispa1 = "*";
	        }
                    if( alist.size() > 0 ) {
                    %>
		<div style="position: relative; float: left; padding-right: 10px;">
		<input style="display: none;" type="checkbox" name="<%=(alist.get(0) != null && alist.get(0).get("isImmunization") != null && alist.get(0).get("isImmunization").equals("true")) ? "immunization" : "printHP"%>"
			value="<%=i%>" checked /> <%}else {%>
		<div style="position: relative; float: left; padding-right: 25px;">
		<span style="display: none;" name="printSp">&nbsp;</span> <%}%>
		</div>
		<div class="headPrevention">
		<p><a href="javascript: function myFunction() {return false; }"
			onclick="javascript:popup(465,635,'AddPreventionData.jsp?prevention=<%= response.encodeURL( h.get("name")) %>&amp;displayName=<%=Encode.forHtmlContent(displayName)%>&amp;demographic_no=<%=demographic_no%>&amp;prevResultDesc=<%= java.net.URLEncoder.encode(h.get("resultDesc")) %>','addPreventionData<%=Math.abs( ( h.get("name")).hashCode() ) %>')">
		<span title="<%=h.get("desc")%>" style="font-weight: bold;"><%=displayName%></span>
		</a>
		<br />
		</p>
		</div>
		<%              String result;
                        for (int k = 0; k < alist.size(); k++){
                        Map<String,Object> hdata = alist.get(k);
                        Map<String,String> hExt = PreventionData.getPreventionKeyValues((String)hdata.get("id"));
                        result = hExt.get("result");

                        String onClickCode="javascript:popup(465,635,'AddPreventionData.jsp?id="+hdata.get("id")+"&amp;demographic_no="+demographic_no+"','addPreventionData')";
                        if (hdata.get("id")==null) onClickCode="popup(300,500,'display_remote_prevention.jsp?remoteFacilityId="+hdata.get("integratorFacilityId")+"&remotePreventionId="+hdata.get("integratorPreventionId")+"&amp;demographic_no="+demographic_no+"')";

		//Applied double encoding to account for xhtml causing the comments & provider name to be decoded too early%>
		<div class="preventionProcedure" onclick="<%=onClickCode%>" title="fade=[on] header=[<%=hdata.get("age")%> -- Date:<%=hdata.get("prevention_date_no_time")%>] body=[<%= Encode.forHtml(Encode.forHtml(hExt.get("comments"))) %>&lt;br/&gt;Provider: <%= Encode.forHtml(Encode.forHtml(String.valueOf(hdata.get("provider_name")))) %>&lt;br/&gt;Entered By: <%= Encode.forHtml(Encode.forHtml(String.valueOf(hdata.get("creator_name")))) %>]">
		
		<!--this is setting the style <%=r(hdata.get("refused"),result)%>  -->
		<p <%=r(hdata.get("refused"),result)%> >Age: <%=hdata.get("age")%> <%if(result!=null && result.equals("abnormal")){out.print("result:"+result);}%>
			<% if (demographicPortalEnabled) { %>
				<%=	((PatientPortalData.SyncStatus) hdata.get("syncStatus")).getIconHtml(request.getContextPath()) %>
			<% } %>
		<br />
		<!--<%=refused(hdata.get("refused"))%>-->Date: <%=hdata.get("prevention_date_no_time")%>
		<%if (hExt.get("comments") != null && (hExt.get("comments")).length()>0) {
                    if (preventionShowComments) { %>
                    <div class="comments"><span><%=Encode.forHtml(hExt.get("comments"))%></span></div>
               <%   } else { %>
            <span class="footnote">1</span>
            <%      }
                 }%>


         <%
         /* Integrated results dont have an "ID" key */
         if(hdata.containsKey("id"))
         {
			List<DHIRSubmissionLog> dhirLogs =  submissionManager.findByPreventionId(Integer.parseInt((String)hdata.get("id")));

			if(!dhirLogs.isEmpty()) {
			 	%> <span class="footnote" style="background-color:black;color:white"><%=dhirLogs.get(0).getStatus()%></span> <%
			} else {
				if(SystemPreferencesUtils.isDhirEnabled() && !StringUtils.isEmpty(snomedId)) {
					if((ispa && hasIspaConsent) || (!ispa && hasNonIspaConsent)) {
						%><span class="footnote" style="background-color:orange;color:black;white-space:nowrap">Not Submitted</span> <%
					}
				}
			}
         }
         %>


		<%=getFromFacilityMsg(hdata)%></p>
		</div>
		<%}%>
		</div>
		<%
                        }
                    } %> <a href="#"
			onclick="Element.toggle('otherElements'); return false;"
			style="font-size: xx-small;">show/hide all other Preventions</a>
		<div style="display: none;" id="otherElements">
		<%for (int i = 0 ; i < hiddenlist.size(); i++){
						Map<String,Object> h2 = hiddenlist.get(i);
						HashMap<String,String> h = (HashMap<String,String>) h2.get("prev");
                        String prevName = h.get("name");
                        ArrayList<HashMap<String,Object>> alist = (ArrayList<HashMap<String,Object>>)  h2.get("list");
                        %>
		<div class="preventionSection">
		<%
                            if( alist.size() > 0 ) {
                            %>
		<div style="position: relative; float: left; padding-right: 10px;">
		<input style="display: none;" type="checkbox" name="<%=alist.get(0).get("isImmunization").equals("true") ? "immunization" : "printHP"%>"
			value="<%=i%>" checked /> <%}else {%>
		<div style="position: relative; float: left; padding-right: 25px;">
		<span style="display: none;" name="printSp">&nbsp;</span> <%}%>
		</div>
		<div class="headPrevention">
		<p><a href="javascript: function myFunction() {return false; }"
			onclick="javascript:popup(465,635,'AddPreventionData.jsp?prevention=<%= response.encodeURL( h.get("name")) %>&amp;demographic_no=<%=demographic_no%>&amp;prevResultDesc=<%= java.net.URLEncoder.encode(h.get("resultDesc")) %>','addPreventionData<%=Math.abs( ( h.get("name")).hashCode() ) %>')">
		<span title="<%=h.get("desc")%>" style="font-weight: bold;"><%=h.get("name")%></span>
		</a>
		<br />
		</p>
		</div>
		<%
                            String result;
                            for (int k = 0; k < alist.size(); k++){
                           		Map<String,Object> hdata = alist.get(k);
                            Map<String,String> hExt = PreventionData.getPreventionKeyValues((String)hdata.get("id"));
                            result = hExt.get("result");

							//Applied double encoding to account for xhtml causing the comments & provider name to be decoded too early%>
		<div class="preventionProcedure" onclick="javascript:popup(465,635,'AddPreventionData.jsp?id=<%=hdata.get("id")%>&amp;demographic_no=<%=demographic_no%>','addPreventionData')" title="fade=[on] header=[<%=hdata.get("age")%> -- Date:<%=hdata.get("prevention_date_no_time")%>] body=[<%= Encode.forHtml(Encode.forHtml(hExt.get("comments"))) %>&lt;br/&gt;Provider: <%= Encode.forHtml(Encode.forHtml(String.valueOf(hdata.get("provider_name")))) %>&lt;br/&gt;Entered By: <%= Encode.forHtml(Encode.forHtml(String.valueOf(hdata.get("creator_name")))) %>]">
		<p <%=r(hdata.get("refused"), result)%>>Age: <%=hdata.get("age")%>
			<% if (demographicPortalEnabled) { %>
				<%=	((PatientPortalData.SyncStatus)hdata.get("syncStatus")).getIconHtml(request.getContextPath()) %>
			<% } %>
		<br />
		<!--<%=refused(hdata.get("refused"))%>-->Date: <%=hdata.get("prevention_date_no_time")%>
		<%if (hExt.get("comments") != null && (hExt.get("comments")).length()>0) {
                     if (preventionShowComments) { %>
                     <div class="comments"><span><%=Encode.forHtml(hExt.get("comments"))%></span></div>
               <%   } else { %>                
            <span class="footnote">1</span>
            <%      }
                }%>
		</p>
		</div>
		<%}%>
		</div>

		<%}%>
		</div>
		<%}else{  //OLD
                    if (configSets == null )
                    {
                  	  configSets = new ArrayList<Map<String,Object>>();
                  	}

                    for ( int setNum = 0; setNum < configSets.size(); setNum++){
                  	  Map<String,Object> setHash = configSets.get(setNum);
                    String[] prevs = (String[]) setHash.get("prevList");
                    %>
		<div class="immSet">
		<h2 style="display: block;"><%=setHash.get("title")%> <span><%=setHash.get("effective")%></span></h2>
		<!--a style="font-size:xx-small;" onclick="javascript:showHideItem('<%="prev"+setNum%>')" href="javascript: function myFunction() {return false; }" >show/hide</a-->
		<a href="#"
			onclick="Element.toggle('<%="prev"+setNum%>'); return false;"
			style="font-size: xx-small;">show/hide</a>
		<div class="preventionSet"
			<%=pdc.getDisplay(loggedInInfo, setHash,demographic_no)%>;"  id="<%="prev"+setNum%>">
		<%for (int i = 0; i < prevs.length ; i++) {
			HashMap<String,String> h = pdc.getPrevention(prevs[i]); %>
			if(h == null) { //this happens with private entries
				continue;
			}
			%>
		<div class="preventionSection">
		<div class="headPrevention">
		<p><a href="javascript: function myFunction() {return false; }"
			onclick="javascript:popup(465,635,'AddPreventionData.jsp?prevention=<%= response.encodeURL( h.get("name")) %>&amp;demographic_no=<%=demographic_no%>&amp;prevResultDesc=<%= java.net.URLEncoder.encode(h.get("resultDesc")) %>','addPreventionData<%=Math.abs(h.get("name").hashCode())%>')">
		<span title="<%=h.get("desc")%>" style="font-weight: bold;"><%=h.get("name")%></span>
		</a>  <br />
		</p>
		</div>
		<%
            String prevType=h.get("name");
            ArrayList<Map<String,Object>> alist = PreventionData.getPreventionData(loggedInInfo, prevType, Integer.valueOf(demographic_no));
            PreventionData.addRemotePreventions(loggedInInfo, alist, demographicId, prevType,demographicDateOfBirth);
            String result;
            for (int k = 0; k < alist.size(); k++){
            	Map<String,Object> hdata = alist.get(k);
          	  Map<String,String> hExt = PreventionData.getPreventionKeyValues((String)hdata.get("id"));
            result = hExt.get("result");

            String onClickCode="javascript:popup(465,635,'AddPreventionData.jsp?id="+hdata.get("id")+"&amp;demographic_no="+demographic_no+"','addPreventionData')";
            if (hdata.get("id")==null) onClickCode="popup(300,500,'display_remote_prevention.jsp?remoteFacilityId="+hdata.get("integratorFacilityId")+"&remotePreventionId="+hdata.get("integratorPreventionId")+"&amp;demographic_no="+demographic_no+"')";
        %>
		<div class="preventionProcedure" onclick="<%=onClickCode%>">
		<p <%=r(hdata.get("refused"),result)%>>Age: <%=hdata.get("age")%>
			<% if (demographicPortalEnabled) { %>
				<%=	((PatientPortalData.SyncStatus)hdata.get("syncStatus")).getIconHtml(request.getContextPath()) %>
			<% } %>
              <br />
		<!--<%=refused(hdata.get("refused"))%>-->Date: <%=hdata.get("prevention_date_no_time")%>
		<%=getFromFacilityMsg(hdata)%></p>
		</div>
		<%}%>
		</div>
		<%}%>
		</div>
		</div>
		<!--immSet--> <%}
                    }%>
		</div>
		<%=legend %>
		</td>
	</tr>
	<tr>
		<td class="MainTableBottomRowLeftColumn">
			<input type="button" class="noPrint" name="printButton" id="printButton" onclick="EnablePrint(this)" value="Enable Print">
			<input type="button" class="noPrint" name="printImmunizations" id="printImmunizations" onclick="EnablePrint(this)" value="Enable Print Immunizations" style="font-size: x-small; ">
<!--
			<br>
			<input type="button" name="sendToPhrButton" value="Send To MyOscar (PDF)" style="display: none;" onclick="sendToPhr(this)">
-->
		</td>
            
                <input type="hidden" id="demographicNo" name="demographicNo" value="<%=demographic_no%>"/>

		<%
		    for (int i = 0 ; i < prevList.size(); i++){
		   	 	HashMap<String,String> h = prevList.get(i);
		        String prevName = h.get("name");
		        ArrayList<Map<String,Object>> alist = PreventionData.getPreventionData(loggedInInfo, prevName, Integer.valueOf(demographic_no));
		        PreventionData.addRemotePreventions(loggedInInfo, alist, demographicId, prevName,demographicDateOfBirth);
		        if( alist.size() > 0 ) { %>
		<input type="hidden" id="preventionHeader<%=i%>"
			name="preventionHeader<%=i%>" value="<%=h.get("name")%>">

		<%
		            for (int k = 0; k < alist.size(); k++){
		            	Map<String,Object> hdata = alist.get(k);
                                Map<String,String> hExt = PreventionData.getPreventionKeyValues((String)hdata.get("id"));
		    %>
		<input type="hidden" id="preventProcedureId<%=i%>-<%=k%>"
			   name="preventProcedureId<%=i%>-<%=k%>"
			   value="<%=hdata.get("id")%>">
		<input type="hidden" id="preventProcedureStatus<%=i%>-<%=k%>"
			name="preventProcedureStatus<%=i%>-<%=k%>"
			value="<%=hdata.get("refused")%>">
		<input type="hidden" id="preventProcedureAge<%=i%>-<%=k%>"
			name="preventProcedureAge<%=i%>-<%=k%>"
			value="<%=hdata.get("age")%>">
		<input type="hidden" id="preventProcedureDate<%=i%>-<%=k%>"
			name="preventProcedureDate<%=i%>-<%=k%>"
			value="<%=hdata.get("prevention_date_no_time")%>">
		<input type="hidden" id="preventProcedureBy<%=i%>-<%=k%>"
			   name="preventProcedureBy<%=i%>-<%=k%>"
			   value="<%=StringUtils.trimToEmpty(hdata.get("provider_name") != null ? Encode.forHtml(hdata.get("provider_name").toString()) : "")%>"/>
                    <%  String comments = hExt.get("comments");
                        if (comments != null && !comments.isEmpty() && preventionShowComments) { %>
                <input type="hidden" id="preventProcedureComments<%=i%>-<%=k%>"
			name="preventProcedureComments<%=i%>-<%=k%>"
			value="<%=Encode.forHtml(comments)%>">
                    <% }
                            	     }
                                       }
		    } //for there are preventions
		    %>
		</form>
	</tr>
</table>

<script type="text/javascript" src="../share/javascript/boxover.js"></script>

<script type="text/javascript">

//basic..just makes the brand name ones bold
var resultFormatter2 = function(oResultData, sQuery, sResultMatch) {
	var output = '';

	if(!oResultData[1]) {
		output = '<b>' + oResultData[0] + '</b>';
	} else {
		output = oResultData[0];
	}
   	return output;
}

YAHOO.example.BasicRemote = function() {
    if($("lotNumberToAdd2") && $("lotNumberToAdd2_choices")){
          var url = "../cvc.do?method=query";
          var oDS = new YAHOO.util.XHRDataSource(url,{connMethodPost:true,connXhrMode:'ignoreStaleResponses'});
          oDS.responseType = YAHOO.util.XHRDataSource.TYPE_JSON;
          oDS.responseSchema = {
              resultsList : "results",
              fields : ["name","generic","genericSnomedId","snomedId","lotNumber"]
          };
          oDS.maxCacheEntries = 0;
          var oAC = new YAHOO.widget.AutoComplete("lotNumberToAdd2","lotNumberToAdd2_choices",oDS);
          oAC.queryMatchSubset = true;
          oAC.minQueryLength = 3;
          oAC.maxResultsDisplayed = 25;
          oAC.formatResult = resultFormatter2;
          oAC.queryMatchContains = true;
          oAC.itemSelectEvent.subscribe(function(type, args) {
        	  var myAC = args[0]; // reference back to the AC instance
        	  var elLI = args[1]; // reference to the selected LI element
        	  var oData = args[2]; // object literal of selected item's result data

        	  console.log('selected');

        	  console.log('args:' + oData[0] + ',' + oData[1] + ',' + oData[2] + ',' + oData[3] + ',' + oData[4]);

        	  //We need to load AddPreventionData with possible brand name, and possible lotnumber/exp.
        	  if(oData[4].length > 0) {
        		popup(465,635,'AddPreventionData.jsp?demographic_no=<%=demographic_no%>&lotNumber=' + oData[4],'addPreventionData' + <%=new java.util.Random().nextInt(10000) + 1%> );
        		document.getElementById('lotNumberToAdd2').value = '';
        	  } else {
        		 popup(465,635,'AddPreventionData.jsp?search=true&demographic_no=<%=demographic_no%>&snomedId=' + oData[2] + '&brandSnomedId=' + oData[3],'addPreventionData' + <%=new java.util.Random().nextInt(10000) + 1%> );
          		document.getElementById('lotNumberToAdd2').value = '';
        	  }


          });

           return {
               oDS: oDS,
               oAC: oAC
           };
       }
       }();

</script>
</body>
</html:html> 
<%!
String refused(Object re){
        String ret = "Given";
        if (re instanceof java.lang.String){

        if (re != null && re.equals("1")){
           ret = "Refused";
        }
        }
        return ret;
    }

String r(Object re, String result){
        String ret = "";
        if (re instanceof java.lang.String){
           if (re != null && re.equals("1")){
              ret = "style=\"background: #FFDDDD;\"";
           }else if(re !=null && re.equals("2")){
              ret = "style=\"background: #FFCC24;\"";
           }
           else if( result != null && result.equalsIgnoreCase("pending")) {
               ret = "style=\"background: #FF00FF;\"";
           }
           else if( result != null && result.equalsIgnoreCase("other")) {
               ret = "style=\"background: #BDFCC9;\"";
           }
           else if(result!=null && result.equals("abnormal")){
	        	   ret = "style=\"background: #ee5f5b;\"";
	           
           }
        }
        return ret;
    }
%>
