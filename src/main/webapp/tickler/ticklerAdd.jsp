<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ taglib uri="/WEB-INF/wellAiVoice.tld" prefix="well-ai-voice"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_tickler" rights="w" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_tickler");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
%>

<%
	UserPropertyDAO propertyDao = SpringUtils.getBean(UserPropertyDAO.class);
	SiteDao siteDao = SpringUtils.getBean(SiteDao.class);
	OscarAppointmentDao appointmentDao = SpringUtils.getBean(OscarAppointmentDao.class);
	ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
	
String user_no = (String) session.getAttribute("user");
int  nItems=0;
String strLimit1="0";
String strLimit2="5";
if(request.getParameter("limit1")!=null) strLimit1 = request.getParameter("limit1");
if(request.getParameter("limit2")!=null) strLimit2 = request.getParameter("limit2");
String providerview = request.getParameter("providerview")==null?"all":request.getParameter("providerview") ;
boolean bFirstDisp=true; //this is the first time to display the window
if (request.getParameter("bFirstDisp")!=null) bFirstDisp= (request.getParameter("bFirstDisp")).equals("true");
String ChartNo;
String demoNo = "";
String demoName = request.getParameter("name");
if ( request.getAttribute("demographic_no") != null){
    demoNo = (String) request.getAttribute("demographic_no");
    demoName = Encode.forHtmlAttribute((String) request.getAttribute("demoName"));
    bFirstDisp = false;
}
if(demoName == null){demoName ="";}

String demographicNo = request.getParameter("demographic_no");

//Retrieve encounter id for updating encounter navbar if info this page changes anything
String parentAjaxId;
if( request.getParameter("parentAjaxId") != null )
    parentAjaxId = request.getParameter("parentAjaxId");
else
    parentAjaxId = "";
    
String updateParent;
if( request.getParameter("updateParent") != null )
    updateParent = request.getParameter("updateParent");
else
    updateParent = "true";  

Boolean writeToEncounter = false;
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    Boolean caisiEnabled = OscarProperties.getInstance().isPropertyActive("caisi");
    Integer defaultProgramId = null;
    List<ProgramProvider> programProviders = new ArrayList<ProgramProvider>();
    
    if (caisiEnabled) {
        ProgramProviderDAO programProviderDao = SpringUtils.getBean(ProgramProviderDAO.class);
        programProviders = programProviderDao.getProgramProviderByProviderNo(loggedInInfo.getLoggedInProviderNo());
        if (programProviders.size() == 1) {
            defaultProgramId = programProviders.get(0).getProgram().getId();
        }
    }
    
boolean recall = false;
String taskTo = user_no; //default current user
String priority = "Normal";
if(request.getParameter("taskTo")!=null) taskTo = request.getParameter("taskTo");
if(request.getParameter("priority")!=null) priority = request.getParameter("priority");
if(request.getParameter("recall")!=null) recall = true;

	String defaultTicklerRecipient = loggedInInfo.getLoggedInProviderNo();

	// multisites
	boolean isMultisitesEnabled = org.oscarehr.common.IsPropertiesOn.isMultisitesEnable();
	List<Site> sites = siteDao.getAllActiveSites();
	Integer selectedSiteId = 0;
	String selectedSiteBackgroundColor = "style=\"background-color: white\"";
	List<UserProperty> ticklerDefaultSites = propertyDao.getAllProperties(UserProperty.TICKLER_DEFAULT_SITE_AND_RECIPIENT, loggedInInfo.getLoggedInProviderNo());
    UserProperty ticklerDefaultLocation = propertyDao.getProp(user_no, UserProperty.TICKLER_DEFAULT_SITE);

    if (isMultisitesEnabled) {
		
		String scheduleAppointmentNo = request.getParameter("appointmentNoForSearchResults");
		String appointmentMultisiteName = null;
		if (StringUtils.isNotBlank(scheduleAppointmentNo) && StringUtils.isNumeric(scheduleAppointmentNo) && !scheduleAppointmentNo.equals("0")) {
			Appointment scheduleAppointment = appointmentDao.find(Integer.valueOf(scheduleAppointmentNo));
			appointmentMultisiteName = scheduleAppointment.getLocation();
			defaultTicklerRecipient = scheduleAppointment.getProviderNo();
		}
		
		// Set selected site, first from appointment site, if no appointment, use demographic default
		DemographicSiteDao demographicSiteDao =  SpringUtils.getBean(DemographicSiteDao.class);
		if (StringUtils.isNotEmpty(demographicNo)) {
			List<DemographicSite> demographicSites = demographicSiteDao.findSitesByDemographicId(Integer.parseInt(demographicNo));
			if (!demographicSites.isEmpty()) {
				selectedSiteId = demographicSites.get(0).getSiteId();
				for (Site site : sites) {
					if (selectedSiteId.equals(site.getSiteId())) {
						selectedSiteBackgroundColor = "style=\"background-color: " + site.getBgColor() + "\"";
					}
				}
			}
		}
        if (selectedSiteId == 0) {
            try {
                selectedSiteId = ticklerDefaultLocation != null
                        ? Integer.parseInt(ticklerDefaultLocation.getValue())
                        : 0;
                if (selectedSiteId != 0) {
                    for (Site site : sites) {
                      if (site.getSiteId().equals(selectedSiteId)) {
                          selectedSiteBackgroundColor = "style=\"background-color: "
                                  + site.getBgColor() + "\"";
                      }
                    }
                }
            } catch (NumberFormatException e) {
                // ticklerDefaultLocation is not numeric so ignore and continue
            }
        }

		for (Site s : sites) {
			if (s.getName().equals(appointmentMultisiteName)) {
                selectedSiteId = s.getId();
				selectedSiteBackgroundColor =
						"style=\"background-color: " + s.getBgColor() + "\"";
				break;
			}
		}
		
		// Set elected default recipient from provider setting
		if (selectedSiteId != 0) {
			for (UserProperty defaultSiteProperty : ticklerDefaultSites) {
			  Integer defaultSitePropertySiteId = Integer.valueOf(defaultSiteProperty.getValue().split(":")[0]);
			  String defaultSitePropertyProviderId = defaultSiteProperty.getValue().split(":")[1];
				if (selectedSiteId.equals(defaultSitePropertySiteId)) {
					defaultTicklerRecipient = defaultSitePropertyProviderId;
					break;
				}
			}
		}
		
	} else {
		UserProperty ticklerRecipientProperty = propertyDao.getProp(
				loggedInInfo.getLoggedInProviderNo(), UserProperty.TICKLER_DEFAULT_RECIPIENT);
		if (ticklerRecipientProperty != null && StringUtils.isNotEmpty(
				ticklerRecipientProperty.getValue())) {
			defaultTicklerRecipient = ticklerRecipientProperty.getValue();
		}
	}

%>
<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%@ page import="java.util.*, java.sql.*, oscar.*, java.net.*, oscar.oscarEncounter.pageUtil.EctSessionBean" %>
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="org.oscarehr.common.model.Appointment" %>
<%@page import="org.oscarehr.common.dao.OscarAppointmentDao" %>
<%@page import="org.oscarehr.common.model.Provider" %>
<%@page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@page import="org.oscarehr.common.model.UserProperty" %>
<%@page import="org.oscarehr.common.dao.UserPropertyDAO" %>
<%
GregorianCalendar now=new GregorianCalendar();
  int curYear = now.get(Calendar.YEAR);
  int curMonth = (now.get(Calendar.MONTH)+1);
  int curDay = now.get(Calendar.DAY_OF_MONTH);   
  
  %><% //String providerview=request.getParameter("provider")==null?"":request.getParameter("provider");
   String xml_vdate=request.getParameter("xml_vdate") == null?"":request.getParameter("xml_vdate");
   String xml_appointment_date = request.getParameter("xml_appointment_date")==null?MyDateFormat.getMysqlStandardDate(curYear, curMonth, curDay):request.getParameter("xml_appointment_date");
%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<%@page import="org.oscarehr.common.dao.SiteDao"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.oscarehr.common.model.Site"%>
<%@ page import="org.oscarehr.PMmodule.dao.ProgramDao" %>
<%@ page import="org.oscarehr.PMmodule.model.Program" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProgramProviderDAO" %>
<%@ page import="org.oscarehr.PMmodule.model.ProgramProvider" %>
<%@ page import="org.oscarehr.common.dao.TicklerTextSuggestDao" %>
<%@ page import="org.oscarehr.common.model.TicklerTextSuggest" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.common.dao.DemographicSiteDao" %>
<%@ page import="org.oscarehr.common.model.DemographicSite" %>
<html:html locale="true">
<head>
<title><bean:message key="tickler.ticklerAdd.title"/></title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/js/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/mutisites/mutisiteUtils.js" type="text/javascript"></script>
<link rel="stylesheet" href="../billing/billing.css" >

<style type="text/css">
<!--
.bodytext
{
  font-family: Arial, Helvetica, sans-serif;
  font-size: 14px;
  font-style: bold;
  line-height: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  color: #FFFFFF;
  text-decoration: none;
}
-->
</style>
      <script language="JavaScript">

    Date.prototype.addDays = function(days) {
    	  var dat = new Date(this.valueOf());
    	  dat.setDate(dat.getDate() + days);
    	  return dat;
    }

    Date.prototype.addMonths = function(months) {
  	  var dat = new Date(this.valueOf());
  	  dat.setMonth(dat.getMonth() + months);
  	  return dat;
  }

    function addMonths(months) {
  	  var d = new Date();
  	  d = d.addMonths(months);
  	  var mth = ((d.getMonth()+1)<10)? ("0"+(d.getMonth()+1)) : (d.getMonth()+1);
  	  var day =  d.getDate() > 10 ? d.getDate() : ("0" + d.getDate());
  	  var newD = d.getFullYear() + "-" + mth + "-" + day;
        document.serviceform.xml_appointment_date.value = newD;
    }
    
      
      function addDays(numDays) {
    	  var d = new Date();
    	  d = d.addDays(numDays);  
    	  var mth = ((d.getMonth()+1)<10)? ("0"+(d.getMonth()+1)) : (d.getMonth()+1);
    	  var day =  d.getDate() > 10 ? d.getDate() : ("0" + d.getDate());
    	  var newD = d.getFullYear() + "-" + mth + "-" + day;
          document.serviceform.xml_appointment_date.value = newD;
      }

      function toggleQuickPickDateDisplay(linkToggle)
      {
          var options = document.getElementById("quickPickDateOptions");
          if (options.style.display === "none")
          {
                options.style.display = "block";
                linkToggle.innerHTML = "<bean:message key="tickler.ticklerAdd.btnHideQuickpick"/>";
          }
          else
          {
                options.style.display = "none";
                linkToggle.innerHTML = "<bean:message key="tickler.ticklerAdd.btnShowQuickpick"/>";
          }
      }

    function pasteMessageText() {
        let selectedIdx = document.serviceform.suggestedText.selectedIndex;
        document.serviceform.textarea.value = document.serviceform.suggestedText.options[selectedIdx].text;
    }

function popupPage(vheight,vwidth,varpage) { //open a new popup window
  var page = "" + varpage;
  windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes";
  var popup=window.open(page, "attachment", windowprops);
  if (popup != null) {
    if (popup.opener == null) {
      popup.opener = self; 
    }
  }
}
function selectprovider(s) {
  if(self.location.href.lastIndexOf("&providerview=") > 0 ) a = self.location.href.substring(0,self.location.href.lastIndexOf("&providerview="));
  else a = self.location.href;
	self.location.href = a + "&providerview=" +s.options[s.selectedIndex].value ;
}
function openBrWindow(theURL,winName,features) { 
  window.open(theURL,winName,features);
}
function setfocus() {
  this.focus();
  document.ADDAPPT.keyword.focus();
  document.ADDAPPT.keyword.select();
}

function validate(form){
validate(form, false);
}
function validate(form, writeToEncounter){
    if (validateDemoNo(form) <%=caisiEnabled?"&& validateSelectedProgram()":""%>){
        if(writeToEncounter){
            form.action = "dbTicklerAdd.jsp?writeToEncounter=true";
        }
        else{
            form.action = "dbTicklerAdd.jsp";
        }
        form.submit();
    }
}

function validateDemoNo() {
    if (document.serviceform.demographic_no.value == "") {
        alert("<bean:message key="tickler.ticklerAdd.msgInvalidDemographic"/>");
        return false;
    } else {
        if (document.serviceform.xml_appointment_date.value == "") {
            alert("<bean:message key="tickler.ticklerAdd.msgMissingDate"/>");
            return false;
        }
        <% if (isMultisitesEnabled) { %>
        else if (document.serviceform.site.value == "none") {
            alert("Must assign task to a site and provider.");
            return false;
        }
        <% } %>
        else {
            return true;
        }
    }
}

function validateSelectedProgram() {
    if (document.serviceform.program_assigned_to.value === "none") {
        alert("<bean:message key="tickler.ticklerAdd.msgNoProgramSelected"/>");
        return false;
    }
    return true;
}

function refresh() {
  var u = self.location.href;
  if(u.lastIndexOf("view=1") > 0) {
    self.location.href = u.substring(0,u.lastIndexOf("view=1")) + "view=0" + u.substring(eval(u.lastIndexOf("view=1")+6));
  } else {
    history.go(0);
  }
}


	// multisites and tickler preference
	let providers = [];
	let sites = [];
	<% for (Site site: sites) { %>
	sites.push({id: '<%=site.getSiteId()%>', name: '<%=Encode.forJavaScript(site.getName())%>', providers: []});
	<% }

	for (int i=0; i<sites.size(); i++) {
		Iterator<Provider> iter = sites.get(i).getProvidersOrderByName().iterator();
		while (iter.hasNext()) {
			Provider p=iter.next();
			if ("1".equals(p.getStatus()) && p.doesReceiveTicklers()) {
%>
	addProviderToSite(providers, '<%=sites.get(i).getSiteId()%>', {providerNo: '<%=p.getProviderNo()%>', name: '<%=Encode.forJavaScript(p.getLastName())%>, <%=Encode.forJavaScript(p.getFirstName())%>'});
	<%
				}
			}
		}
	%>
    providers.sort((providerA, providerB) => providerA.name.localeCompare(providerB.name));
    
    let providerSiteRecipientPreferenceMap = new Map();
    <% for (UserProperty defaultSite : ticklerDefaultSites) { 
      String siteId = defaultSite.getValue().split(":")[0];
      String providerId = defaultSite.getValue().split(":")[1]; %>
    providerSiteRecipientPreferenceMap.set('<%=siteId%>', '<%=providerId%>');
    <% } %>
</script>


</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" rightmargin="0" topmargin="10" onLoad="setfocus()">
<well-ai-voice:script/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr bgcolor="#000000"> 
    <td height="40" width="10%"> </td>
    <td width="90%" align="left"> 
      <p><font face="Verdana, Arial, Helvetica, sans-serif" color="#FFFFFF"><b><font face="Arial, Helvetica, sans-serif" size="4"><bean:message key="tickler.ticklerAdd.msgTickler"/></font></b></font> 
      </p>
    </td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0"bgcolor="#EEEEFF">
 <form name="ADDAPPT" method="post" action="../appointment/appointmentcontrol.jsp">
	 <input type="hidden" name="<csrf:tokenname/>" value="<csrf:tokenvalue/>"/>
<tr> 
      <td width="35%"><font color="#003366"><font face="Verdana, Arial, Helvetica, sans-serif" size="2"><b><bean:message key="tickler.ticklerAdd.formDemoName"/>: </b></font></font></td>
      <td colspan="2" width="65%">
<div align="left"><INPUT TYPE="TEXT" NAME="keyword" size="25" VALUE="<%=bFirstDisp?"":Encode.forHtmlAttribute(demoName.equals("")?(String) session.getAttribute("appointmentname"):demoName)%>">
   	 <input type="submit" name="Submit" value="<bean:message key="tickler.ticklerAdd.btnSearch"/>">
  </div>
</td>
    </tr>
  <INPUT TYPE="hidden" NAME="orderby" VALUE="last_name" >
  <%
    String searchMode = request.getParameter("search_mode");
    if (searchMode == null || searchMode.isEmpty()) {
        searchMode = OscarProperties.getInstance().getProperty("default_search_mode","search_name");
    }
%>
				      <INPUT TYPE="hidden" NAME="search_mode" VALUE="<%=searchMode%>" >
				      <INPUT TYPE="hidden" NAME="originalpage" VALUE="<%=request.getContextPath()%>/tickler/ticklerAdd.jsp" >
				      <INPUT TYPE="hidden" NAME="limit1" VALUE="0" >
				      <INPUT TYPE="hidden" NAME="limit2" VALUE="5" >
              <!--input type="hidden" name="displaymode" value="TicklerSearch" -->
              <INPUT TYPE="hidden" NAME="displaymode" VALUE="Search "> 

<% ChartNo = bFirstDisp?"":request.getParameter("chart_no")==null?"":request.getParameter("chart_no"); %>
   <INPUT TYPE="hidden" NAME="appointment_date" VALUE="2002-10-01" WIDTH="25" HEIGHT="20" border="0" hspace="2">
       <INPUT TYPE="hidden" NAME="status" VALUE="t"  WIDTH="25" HEIGHT="20" border="0" hspace="2">
              <INPUT TYPE="hidden" NAME="start_time" VALUE="10:45" WIDTH="25" HEIGHT="20" border="0"  onChange="checkTimeTypeIn(this)">
              <INPUT TYPE="hidden" NAME="type" VALUE="" WIDTH="25" HEIGHT="20" border="0" hspace="2">
              <INPUT TYPE="hidden" NAME="duration" VALUE="15" WIDTH="25" HEIGHT="20" border="0" hspace="2" >
              <INPUT TYPE="hidden" NAME="end_time" VALUE="10:59" WIDTH="25" HEIGHT="20" border="0" hspace="2"  onChange="checkTimeTypeIn(this)">
       

 <input type="hidden" name="demographic_no"  readonly value="" width="25" height="20" border="0" hspace="2">
         <input type="hidden" name="location"  tabindex="4" value="" width="25" height="20" border="0" hspace="2">
              <input type="hidden" name="resources"  tabindex="5" value="" width="25" height="20" border="0" hspace="2">
              <INPUT TYPE="hidden" NAME="user_id" readonly VALUE='oscardoc, doctor' WIDTH="25" HEIGHT="20" border="0" hspace="2">
     	        <INPUT TYPE="hidden" NAME="dboperation" VALUE="search_demorecord">
              <INPUT TYPE="hidden" NAME="createdatetime" readonly VALUE="2002-10-1 17:53:50" WIDTH="25" HEIGHT="20" border="0" hspace="2">
              <INPUT TYPE="hidden" NAME="provider_no" VALUE="115">
              <INPUT TYPE="hidden" NAME="creator" VALUE="oscardoc, doctor">
              <INPUT TYPE="hidden" NAME="remarks" VALUE="">
              <input type="hidden" name="parentAjaxId" value="<%=parentAjaxId%>"/>
              <input type="hidden" name="updateParent" value="<%=updateParent%>"/> 
 </form>
</table>
<table width="100%" border="0" bgcolor="#EEEEFF">
  <form name="serviceform" method="post" >
      <input type="hidden" name="parentAjaxId" value="<%=parentAjaxId%>"/>
      <input type="hidden" name="updateParent" value="<%=updateParent%>"/>
     <tr> 
      <td width="35%"> <div align="left"><font color="#003366"><font face="Verdana, Arial, Helvetica, sans-serif" size="2"><strong><bean:message key="tickler.ticklerAdd.formChartNo"/>:</strong> </font></font></div></td>
      <td colspan="2"> <div align="left"><INPUT TYPE="hidden" NAME="demographic_no" VALUE="<%=bFirstDisp?"":request.getParameter("demographic_no").equals("")?"":request.getParameter("demographic_no")%>"><%=ChartNo%></div></td>
   <input type="hidden" name="appointmentNo" id="appointmentNo" value="<%=request.getParameter("appointmentNoForSearchResults") != null ? request.getParameter("appointmentNoForSearchResults") : "" %>" />
    </tr>

    <tr> 
      <td><font color="#003366" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><bean:message key="tickler.ticklerAdd.formServiceDate"/>:</strong></font></td>
      <td><input type="text" name="xml_appointment_date" value="<%=xml_appointment_date%>"> 
        <font color="#003366" size="1" face="Verdana, Arial, Helvetica, sans-serif">
        <a href="javascript:void(0)" onClick="openBrWindow('../billing/billingCalendarPopup.jsp?type=end&amp;year=<%=curYear%>&amp;month=<%=curMonth%>','','width=300,height=300');return false;" title="<bean:message key="tickler.ticklerAdd.btnCalendarLookup"/>">
            <input type="image" src="../images/cal.gif" width="25" height="22" border="0" align="top" alt="<bean:message key="tickler.ticklerAdd.btnCalendarLookup"/>"/>
        </a>
        <a href="#" onClick="toggleQuickPickDateDisplay(this)" title="<bean:message key="tickler.ticklerAdd.btnToggleQuickpickDates"/>" style="padding-left:5px; vertical-align: middle;"><bean:message key="tickler.ticklerAdd.btnShowQuickpick"/></a>
        <div id="quickPickDateOptions"  style="display:none;">
         <a href="#" onClick="addDays(14)">14d</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(1)">1m</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(2)">2m</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(3)">3m</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(4)">4m</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(5)">5m</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(6)">6m</a>&nbsp; &nbsp;
         <a href="#" onClick="addMonths(7)">7m</a>&nbsp; &nbsp;
         <a href="#" onClick="addMonths(8)">8m</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(9)">9m</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(10)">10m</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(11)">11m</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(12)">1yr</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(24)">2yr</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(36)">3yr</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(60)">5yr</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(96)">8yr</a>&nbsp; &nbsp;
        <a href="#" onClick="addMonths(120)">10yr</a>
        </div>
        </font> </td>
        
        
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td height="21" valign="top"><font color="#003366" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><bean:message key="tickler.ticklerMain.Priority"/>:</strong></font></td>
      <td valign="top"> 
	<select name="priority" style="font-face:Verdana, Arial, Helvetica, sans-serif">
 	<option value="<bean:message key="tickler.ticklerMain.priority.high"/>" <%=priority.equals("High")?"selected":""%>><bean:message key="tickler.ticklerMain.priority.high"/>
	<option value="<bean:message key="tickler.ticklerMain.priority.normal"/>" <%=priority.equals("Normal")?"selected":""%>><bean:message key="tickler.ticklerMain.priority.normal"/>
	<option value="<bean:message key="tickler.ticklerMain.priority.low"/>" <%=priority.equals("Low")?"selected":""%>><bean:message key="tickler.ticklerMain.priority.low"/>	
     	</select>
      </td>
      <td>&nbsp;</td>
    </tr>

    <tr> 
      <td height="21" valign="top"><font color="#003366" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><bean:message key="tickler.ticklerMain.taskAssignedTo"/>:</strong></font></td>
      <td valign="top"> <font face="Verdana, Arial, Helvetica, sans-serif" size="2" color="#333333">
<% if (isMultisitesEnabled) { %>
    <select name="site" onchange="this.style.backgroundColor=this.options[this.selectedIndex].style.backgroundColor; changeSite(this, providers, '<%=defaultTicklerRecipient%>', 'task_assigned_to');" <%=selectedSiteBackgroundColor%>>
        <option value="none" style="background-color:white">---all clinic---</option>
        <% for (Site s:sites) { %>
        <option value="<%=s.getId()%>" style="background-color: <%=s.getBgColor()%>" <%=s.getId().equals(selectedSiteId)?"selected":"" %>><%=Encode.forHtmlContent(s.getName())%></option>
        <% } %>
    </select>
    <select name="task_assigned_to" id="task_assigned_to">
    </select>
    <script>
        let assignedToEl = jQuery('#task_assigned_to');
        providers.forEach(function(p) {
            let newOption = jQuery("<option></option>").text(p.name).attr("value", p.providerNo);

            if ('<%=defaultTicklerRecipient%>' !== 'null' && '<%=defaultTicklerRecipient%>' === p.providerNo) {
                newOption.attr('selected', 'selected');
            }
            assignedToEl.append(newOption);
        });
    </script>
<% } else { %>
  
      <select name="task_assigned_to">        
            <%  String proFirst="";
                String proLast="";
                String proOHIP="";
				
                for(Provider p : providerDao.getActiveProvidersThatReceivesTicklers()) {
               
                    proFirst =p.getFirstName();
                    proLast = p.getLastName();
                    proOHIP = p.getProviderNo();

            %> 
            <option value="<%=proOHIP%>" <%=defaultTicklerRecipient.equals(proOHIP)?"selected":""%>><%=Encode.forHtmlContent(proLast+", "+proFirst)%></option>
            <%
                }
            %>
      </select>
<% } %>
          
           <input type="hidden" name="docType" value="<%=request.getParameter("docType")%>"/>
           <input type="hidden" name="docId" value="<%=request.getParameter("docId")%>"/>
      </td>
    <% if (caisiEnabled) { %>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td height="21" valign="top">
          <font color="#003366" size="2" face="Verdana, Arial, Helvetica, sans-serif">
              <strong><bean:message key="tickler.ticklerMain.programAssignedTo"/>:</strong>
          </font>
      </td>
      <td valign="top">
          <select name="program_assigned_to">
              <option value="none" <%=defaultProgramId == null?"selected=\"selected\"":""%>>None selected</option>
          <%  for (ProgramProvider pp : programProviders) { %>
              <option value="<%=pp.getProgram().getId()%>" <%=pp.getProgram().getId().equals(defaultProgramId)?"selected=\"selected\"":""%>>
                  <%=Encode.forHtmlContent(pp.getProgram().getName())%>
              </option>
          <%  } %>
          </select>
      </td>
    <% } %>
      <td>&nbsp;</td>
    </tr>
    <tr> 
      <td height="21" valign="top"><font color="#003366" size="2" face="Verdana, Arial, Helvetica, sans-serif"><strong><bean:message key="tickler.ticklerAdd.formReminder"/>:</strong></font></td>
      <td valign="top">
          <a href="#" onclick="openBrWindow('./ticklerSuggestedText.jsp','','width=680,height=400')" style="font-weight:bold"><bean:message key="tickler.ticklerEdit.suggestedText"/></a>:&nbsp;
          <select name="suggestedText" onchange="pasteMessageText()">
              <option value="">---</option>
              <%
                  TicklerTextSuggestDao ticklerTextSuggestDao = SpringUtils.getBean(TicklerTextSuggestDao.class);
                  for (TicklerTextSuggest tTextSuggest : ticklerTextSuggestDao. getActiveTicklerTextSuggests()) { 
              %>
              <option><%=Encode.forHtmlContent(tTextSuggest.getSuggestedText())%></option>
              <% } %>
          </select>
      </td>
      <td>&nbsp;</td>
    </tr>
      <tr>
          <td height="21" valign="top">&nbsp;</td>
          <td valign="top"> <textarea style="font-face:Verdana, Arial, Helvetica, sans-serif"name="textarea" cols="50" rows="10"></textarea></td>
          <td>&nbsp;</td>
      </tr>
     <INPUT TYPE="hidden" NAME="user_no" VALUE="<%=user_no%>">
      <input type="hidden" name="writeToEncounter" value="<%=writeToEncounter%>"/>
    <tr> 
      <td><input type="button" name="Button" value="<bean:message key="tickler.ticklerAdd.btnCancel"/>" onClick="window.close()"></td>
      <td><input type="button" name="Button" value="<bean:message key="tickler.ticklerAdd.btnSubmit"/>" onClick="validate(this.form)">
          <input type="button" name="Button" value="Submit & Write to Encounter" onClick="validate(this.form, true)">
      </td>
      <td></td>
	  </tr>
  </form>
</table>
<p><font face="Arial, Helvetica, sans-serif" size="2"> </font></p>
  <p>&nbsp; </p>
<%@ include file="../demographic/zfooterbackclose.jsp" %> 

</body>
</html:html>
