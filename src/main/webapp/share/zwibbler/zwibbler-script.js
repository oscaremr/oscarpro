
/*
*/

var DEFAULT_FONT = "Arial"
var imagepath = "../share/zwibbler/";
function CreateZwibbler(div) {
    var ctx = div.find(".zwibbler-canvas").zwibbler({
        showColourPanel: false,
        showToolbar: false,
        defaultLineWidth: 5,
        defaultBrushWidth: 2,
        defaultFillStyle: "rgba(0,0,0,0.0)",
        defaultArrowStyle: "solid",
        defaultSmoothness: 0,
        allowTextInShape: false,
        defaultArrowSize: 0,
        multilineText: true,
        leaveTextToolOnBlur: true,
        autoPickTool: false
    });

    ctx.createToolbar(div.find(".zwibbler-toolbar"), [
        {
            title: "Pick tool",
            image: imagepath + "button-pointer.png",
            toolName: "pick",
            onclick: function() {
                ctx.clearSelection();
                ctx.usePickTool();
            }
        }, {
            title: "Line",
            image: imagepath + "button-line.png",
            onclick: function() {
                ctx.useArrowTool();
            }
        }, {
            title: "Circle",
            toolName: "circle",
            image: imagepath + "button-circle.png",
            onclick: function() {
                ctx.useCircleTool();
            }
        }, {
            title: "Rectangle",
            toolName: "rectangle",
            image: imagepath + "button-rectangle.png",
            onclick: function() {
                ctx.useRectangleTool();
            }
        }, {
            title: "Pen",
            toolName: "pen",
            image: imagepath + "button-pen.png",
            onclick: function() {
                ctx.useBrushTool();
            }
        }, {
            title: "Text",
            toolName: "text",
            image: imagepath + "button-text.png",
            onclick: function() {
                ctx.useTextTool();
            }
        }, {
            title: "Eraser",
            image: imagepath + "button-eraser.png",
            onclick: function() {
                ctx.useBrushTool({
                    lineWidth: 15,
                    strokeStyle: "erase"
                });
            }
        }, {
            title: "Undo",
            image: imagepath + "button-undo.png",
            onclick: function() {
                ctx.undo();
            }
        }, {
            title: "Redo",
            image: imagepath + "button-redo.png",
            onclick: function() {
                ctx.redo();
            }
        }
    ]);

    // handle drag and drop images
    div.find(".zwibbler-canvas").on("dragover", function(e) {
        e.preventDefault();
    });

    div.find(".zwibbler-canvas").on("drop", function(e) {
        var src = e.originalEvent.dataTransfer.getData("Text");
        e.preventDefault();
        
        var position = ctx.getDocumentCoordinates(e.originalEvent.clientX, 
            e.originalEvent.clientY);

        Zwibbler.getImageSize(src, function(width, height) {
            ctx.beginTransaction();
            var node = ctx.createNode("ImageNode", {
                url: src
            });
            ctx.translateNode(node, position.x-width/2, position.y-height/2);
            ctx.commitTransaction();
        });

    });

    $(".draggable").attr("draggable", "true").on("dragstart", function(e) {
        e.originalEvent.dataTransfer.setData("Text", this.src);
    });

    CreatePropertyPanel(ctx, div.find(".toolbar-right"), div.find(".zwibbler-palette"));
    myCtx = ctx;
}

$(document).ready(function() {
    CreateZwibbler($("#zwibbler-div"));
	myCtx.on("document-changed", function() {
		$("#submit").show();
		$("#saveAndDocument").show();
	});
});


function CreatePropertyPanel(ctx, panel, palette) {
    function refresh() {
        // get a summary of the types of nodes selected and the
        // properties of interest.
        var nodes = ctx.getPropertySummary(ctx.getSelectedNodes());

        // augment the nodes selected with the currently selected tool.
        var toolname = ctx.getCurrentTool();
        switch(toolname) {
            case "line":
            case "arrow":
                nodes.types["PathNode"] = true;
                nodes.types["PathNode-open"] = true;
                nodes.properties["fillStyle"] = ctx.getFillColour();
                nodes.properties["strokeStyle"] = ctx.getStrokeColour();
                nodes.properties["arrowSize"] = 0;
                nodes.properties["lineWidth"] = ctx.getConfig("defaultLineWidth");                
                nodes.properties["dashes"] = "";
                break;

            case "brush":
                nodes.types["BrushNode"] = true;
                nodes.properties["strokeStyle"] = ctx.getStrokeColour();
                nodes.properties["lineWidth"] = ctx.getConfig("defaultBrushWidth");
                break;

            case "text":
                nodes.types["TextNode"] = true;
                nodes.properties["fillStyle"] = ctx.getConfig("defaultTextFillStyle");
                nodes.properties["strokeStyle"] = ctx.getConfig("defaultTextStrokeStyle");
                nodes.properties["fontName"] = ctx.getConfig("defaultFont");
                nodes.properties["fontSize"] = ctx.getConfig("defaultFontSize");
                nodes.properties["lineWidth"] = ctx.getConfig("defaultTextLineWidth");
                break;

            case "circle":
            case "curve":
            case "polygon":
            case "rectangle":
            case "shape":
                nodes.types["PathNode"] = true;
                nodes.types["PathNode-closed"] = true;
                nodes.properties["fillStyle"] = ctx.getFillColour();
                nodes.properties["strokeStyle"] = ctx.getStrokeColour();
                nodes.properties["lineWidth"] = ctx.getConfig("defaultLineWidth");
                nodes.properties["dashes"] = "";
                break;
        }

        // find anything with zwibbler-show attribute and interpret it.
        panel.find("*[zwibbler-show]").each(function(i, elem) {
            var showWhen = $(elem).attr("zwibbler-show").split(" ");
            var show = false;
            for (var i = 0; i < showWhen.length; i++) {
                var item = showWhen[i];

                // check if it is of the form nodetype-selected
                if (item.indexOf("-selected") > 0) {
                    var selitem = item.substr(0, item.length - 9);
                    if (selitem in nodes.types) {
                        show = true;
                    }
                }

                // check if it is of the form "property name"
                if (item in nodes.properties) {
                    show = true;
                }
            }
            $(elem).toggle(show);
        });

        panel.find("*[zwibbler-property]").each(function(i, elem) {
            // if the property has the given value, then select it.
            var value = $(elem).attr("zwibbler-value");
            var name = $(elem).attr("zwibbler-property");
            var selected = false;

            if (name in nodes.properties) {
                if (value === ("" + nodes.properties[name]) ) {
                    selected = true;
                }
            }

            if (selected) {
                $(elem).addClass("selected");
            } else {
                $(elem).removeClass("selected");
            }

            if ((name === "fillStyle" || name === "strokeStyle") &&
                $(elem).hasClass("swatch")) 
            {
                $(elem).css("background-color", nodes.properties[name]);
            }

            if (elem.tagName === "SELECT") {
                elem.value = nodes.properties[name];
            }
        });
    }

    ctx.on("selected-nodes", refresh);
    ctx.on("tool-changed", refresh);
    refresh();

    panel.on("click", "*[zwibbler-property]", function() {
        var name = $(this).attr("zwibbler-property");
        var value = $(this).attr("zwibbler-value");

        if (this.tagName === "SELECT") {
            value = this.value;
        }
        
        if (!isNaN(parseFloat(value))) {
            if ("" + parseFloat(value) === value) {
                value = parseFloat(value);
            }
        }

        ctx.setNodeProperty(ctx.getSelectedNodes(), name, value);
        ctx.setToolProperty(name, value);

        switch(name) {
            case "arrowSize":
                ctx.setConfig("defaultArrowSize", value);
                break;
            case "fontName":
                ctx.setConfig("defaultFont", value);
                break;
            case "fontSize":
                ctx.setConfig("defaultFontSize", value);
                break;

            case "lineWidth":
                ctx.setConfig("defaultBrushWidth", value);
                ctx.setConfig("defaultLineWidth", value);
                break;  
        }

        panel.find("*[zwibbler-property='" + name + "']").removeClass("selected");
        $(this).addClass("selected");
    });

    panel.find("*[zwibbler-click]").on("click", function() {
        var cmd = $(this).attr("zwibbler-click");
        switch(cmd) {
            case "deleteNodes":
                ctx.deleteNodes(ctx.getSelectedNodes());
                break;
            case "bringToFront":
                ctx.bringToFront();
                break;
            case "sendToBack":
                ctx.sendToBack();
                break;
        }
    });

    var colourProperty = null;
    var colourElement = null;

    ctx.generatePalette(palette, 20, {
        onColour: function(colourValue) {
            ctx.setNodeProperty(ctx.getSelectedNodes(), colourProperty, colourValue);
            panel.find("." + colourProperty).css("background-color", colourValue);
            if (colourProperty === "strokeStyle") {
                ctx.setColour(colourValue, false);
            } else if (colourProperty === "fillStyle") {
                ctx.setColour(colourValue, true);
            }
            if (colourElement) {
                $(colourElement).css("background-color", colourValue);
            }
        }
    });

    var palette = Zwibbler.Dialog(palette);
    console.log(palette);

    panel.find(".swatch").on("click", function(e) {
        if ($(this).attr("zwibbler-property") === "fillStyle") {
            colourProperty = "fillStyle";
        } else {
            colourProperty = "strokeStyle";
        }
        colourElement = this;
        palette.show(this, "tl tr", true);
        e.stopPropagation();
    });
}
