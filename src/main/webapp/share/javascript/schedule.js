function storeApptNo(apptNo) {
    var url = "storeApptInSession.jsp?appointment_no="+apptNo;
    new Ajax.Request(url, {method:'get'});
}

function setfocus() {
    this.focus();
}

function popUpEncounter(vheight,vwidth,varpage) {
    var page = "" + varpage;
    windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=50,screenY=50,top=0,left=0";
    var popup=window.open(page, "Encounter", windowprops);

    if (popup != null) {
        if (popup.opener == null) {
            popup.opener = self;
        }
        popup.focus();
    }
}

function popupInboxManager(varpage){
    var page = "" + varpage;
    let widthVal = page.includes('/dms/inboxManage.do') ? '1215' : '1250';
    var windowname="apptProviderInbox";
    windowprops = "height=900,width="+widthVal+",location=no,"
        + "scrollbars=yes,menubars=no,toolbars=no,resizable=yes,top=10,left=0";
    var popup = window.open(page, windowname, windowprops);
    if (popup != null) {
        if (popup.opener == null) {
            popup.opener = self;
        }
        popup.focus();
    }
}

function popupPage2(varpage) {
    popupPage2(varpage, "apptProviderSearch");
}

function popupPage2(varpage, windowname) {
    popupPage2(varpage, windowname, 700, 1024);
}

function popupPage2(varpage, windowname, vheight, vwidth) {
// Provide default values for windowname, vheight, and vwidth incase popupPage2
// is called with only 1 or 2 arguments (must always specify varpage)
    windowname  = typeof(windowname)!= 'undefined' ? windowname : 'apptProviderSearch';
    vheight     = typeof(vheight)   != 'undefined' ? vheight : '700px';
    vwidth      = typeof(vwidth)    != 'undefined' ? vwidth : '1024px';
    var page = "" + varpage;
    windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=50,screenY=50,top=0,left=0";
    var popup = window.open(page, windowname, windowprops);
    if (popup != null) {
        if (popup.opener == null) {
            popup.opener = self;
        }
        popup.focus();
    }
}

function popupWithApptNo(vheight,vwidth,varpage,name,apptNo) {
    if (apptNo) storeApptNo(apptNo);
    if (name=='master')
        popup(vheight,vwidth,varpage,name);
    else if (name=='encounter')
        popup(vheight, vwidth, varpage, name);
    else
        popupOscarRx(vheight,vwidth,varpage);
}



function refresh() {
    document.location.reload();
}

function refresh1() {
    var u = self.location.href;
    if(u.lastIndexOf("view=1") > 0) {
        self.location.href = u.substring(0,u.lastIndexOf("view=1")) + "view=0" + u.substring(eval(u.lastIndexOf("view=1")+6));
    } else {
        document.location.reload();
    }
}

function onUpdatebill(url) {
    popupPage(700,720, url);
}

function IsPopupBlocker() {
    var oWin = window.open("","testpopupblocker","width=100,height=50,top=5000,left=5000");
    if (oWin==null || typeof(oWin)=="undefined") {
        return true;
    } else {
        oWin.close();
        return false;
    }
}

/* Refresh tab alerts */
function refreshAllTabAlerts() {
    refreshTabAlerts("oscar_new_lab");
    refreshTabAlerts("oscar_new_msg");
    refreshTabAlerts("oscar_new_tickler");
    refreshTabAlerts("oscar_aged_consults");
    refreshTabAlerts("oscar_scratch");
    
}

function refreshRenewalAlert(){
    refreshTabAlerts("oscar_new_renewal");
}

function callRefreshTabAlerts(id) {
    setTimeout("refreshTabAlerts('"+id+"')", 10);
}

function refreshTabAlerts(id) {
    var url = "../provider/tabAlertsRefresh.jsp";
    var pars = "id=" + id + "&autoRefresh=true";

    var myAjax = new Ajax.Updater(id, url, {method: 'get', parameters: pars});
}

function refreshSameLoc(mypage) {
    var X =  (window.pageXOffset?window.pageXOffset:window.document.body.scrollLeft);
    var Y =  (window.pageYOffset?window.pageYOffset:window.document.body.scrollTop);
    window.location.href = mypage+"&x="+X+"&y="+Y;
}

function scrollOnLoad() {
    var X = getParameter("x");
    var Y = getParameter("y");
    if(X!=null && Y!=null) {
        window.scrollTo(parseInt(X),parseInt(Y));
    }
}

function getParameter(paramName) {
    var searchString = window.location.search.substring(1);
    var i,val;
    var params = searchString.split("&");

    for (i=0;i<params.length;i++) {
        val = params[i].split("=");
        if (val[0] == paramName) {
            return val[1];
        }
    }
    return null;
}