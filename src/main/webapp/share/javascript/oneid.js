function openViewlet(viewletKey, demographicNo, kaiEmrContext) {
  jQuery.ajax({
    type: 'GET',
    url: `${kaiEmrContext}/api/v1/open-viewlet/launch/${demographicNo}?key=${viewletKey}`,
    success: function (data) {
      console.log(viewletKey + ' URL: ' + data.viewletUrl);
      if (data.viewletUrl === '') {
        alert(`No viewlet URL found for viewlet key: ${viewletKey}.`)
      }
      popupEHRService(data.viewletUrl, demographicNo, kaiEmrContext)
    },
    error: function () {
      alert('There was an unexpected error retrieving the viewlet URL.');
    }
  })
}

function popupEHRService(url, demographicNo, kaiemrContext) {
  if (kaiemrContext === undefined) {
    kaiemrContext = "/kaiemr";
  }
  let windowProperties = "height=800,width=1300,location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=0,screenY=0,top=0,left=0";
  let popup = window.open(url, 'EHR Service', windowProperties);
  if (!demographicNo) {
    return;
  }
  let timer = setInterval(function () {
    if (popup.closed) {
      clearInterval(timer);
      jQuery.ajax({
        type: "GET",
        url: `${kaiemrContext}/api/cms/patientClose/${demographicNo}`,
        success: function (data) {
          console.log(data);
        },
        error: function () {
          alert(
              'There was an error removing the patient from the context. Please try again.');
        }
      });
    }
  }, 1000);
  popup.focus();
}