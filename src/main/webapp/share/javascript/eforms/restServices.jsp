<%--
    Copyright (c) 2021 WELL EMR Group Inc.
    This software is made available under the terms of the
    GNU General Public License, Version 2, 1991 (GPLv2).
    License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>

<%--
<%@page import="org.oscarehr.integration.excelleris.eorder.RestServicesHelper" contentType="text/javascript" %>
--%>

<%

//TODO: There may be some future use for this file, so I won't delete it for now
//String testCodes = RestServicesHelper.getTestCodes(); 
//boolean isTestCodes = testCodes != null;

%>

if (typeof jQuery == "undefined") {
	alert("The restServices library requires jQuery. Please ensure that it is loaded first");
}

var restServices = {
	initialize : function(options) {
        console.log('restServices : initialized');		
	}
};
