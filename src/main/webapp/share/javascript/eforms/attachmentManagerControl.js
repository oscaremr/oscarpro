if (typeof jQuery == "undefined") {
  console.warn("The attachmentManagerControl library requires jQuery. "
      + "Attachment Manager widget will not be loaded on this eForm.");
} else {
  const attachmentManagerControl = {
    initialize: function () {

      // find element with DoNotPrint class and contains a submit button
      const doNotPrintDiv = getDoNotPrintSubmitDiv();
      if (doNotPrintDiv == null
          || doNotPrintDiv.size() === 0
          || doNotPrintDiv.find("input[type='submit']") == null) {
        console.warn("Missing eForm DoNotPrint control div. Please "
            + "ensure a div with class DoNotPrint exists on the page to load "
            + "the Attachment Manager widget.");
        return;
      }
      const demographicNo = new URL(window.location.href).searchParams.get(
          "demographic_no");
      const fdid = new URL(window.location.href).searchParams.get("fdid");

      $.ajax({
        url: "../eform/eformAttachmentManagerWidget.jsp",
        data: "demographicNo=" + demographicNo + "&fdid=" + fdid,
        success: function (data) {
          if (data == null || data.trim() === "") {
            console.log(
                "Error loading attachment manager widget, please contact an administrator.");
          } else {
            // Prepend attachment manager widget to the div
            doNotPrintDiv.prepend(data);
          }
        }
      });
    }
  };

  jQuery(document).ready(function () {
    attachmentManagerControl.initialize();
  });
}

/**
 * Find the div with class DoNotPrint that contains a submit button
 * This prevents the attachment manager from appearing multiple times if
 * an eform contains multiple print-hidden controls using the DoNotPrint class
 */
function getDoNotPrintSubmitDiv() {
  return jQuery(".DoNotPrint").filter(
      function (i) {
        return $(this).find("input[type='submit']").size() > 0
            || $(this).find("input[value='Submit']").size() > 0
            || $(this).find("input[value='submit']").size() > 0;
      });
}