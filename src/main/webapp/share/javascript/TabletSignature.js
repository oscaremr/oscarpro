/*
	Copyright (c) 2010 Alex Gibson, http://miniapps.co.uk/
	Released under MIT license, http://miniapps.co.uk/license/
	Modified 2010 by Noah Daley, indivica.com	
*/

const BASE64_MARKER = "data:image/png;base64,";

var canvas; //canvas element.
var ctx; //drawing context.
var startX = 0; //starting X coordinate.
var startY = 0; //starting Y coordinate.

var moved = false; //has move occured.

var toolbarHeight = 41; //toolbar offset height (pixels).
var penSize = 2; //pen width (pixels).
var r = 0; //red
var g = 0; //green
var b = 0; //blue

var stage = 1; 

var bCX = 0;
var bCY = 0;
var bX = 0;
var bY = 0;

var tmr;

var sigPlus = false;

var saved = false;
var signing = false;

let isSaveToDb;
	
function init() {

	// is different on Consultations vs eForms
	isSaveToDb = document.getElementById("saveToDb").value;

	ctx = canvas.getContext('2d');
    	
	//set height and width to size of device window
	canvas.setAttribute("height", "100px");
	canvas.setAttribute("width", "500px");
    	
    ctx.fillStyle = 'rgb(255,255,255)';
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	ctx.fill();
    	
	//add event listeners
	document.querySelector('#clear').addEventListener('click', clearCanvas, false);
	document.querySelector('#save').addEventListener('click', saveCanvas, false);
	if (enableEformDigitalSig) {
		document.querySelector('#sign').addEventListener('click', signCanvas, false);	
		document.querySelector('#signAndSave').addEventListener('click', signAndSaveCanvas, false);	
	}

	// favourite signature event listeners
	document.querySelector('#displaySaveSignatureMenu').addEventListener('click', displaySaveSignatureMenu, false);
	document.querySelector('#saveFavouriteSignature').addEventListener('click', saveFavouriteSignature, false);
	document.querySelector('#cancelFavouriteSignature').addEventListener('click', closeSaveSignatureMenu, false);
	document.querySelector('#favouriteSignatures').addEventListener('change', onSelectFavouriteSignature, false);

	//finally, add touch and mouse event listener
	canvas.addEventListener('touchstart', onTouchStart, false);
	canvas.addEventListener('mousedown', onMouseDown, false);

    try {
        SigWebSetDisplayTarget(ctx);
        SetDisplayXSize(500);
        SetDisplayYSize(100);
        SetTabletState(0, tmr);
        SetJustifyMode(0);
        ClearTablet();
        SetKeyString("0000000000000000");
        SetEncryptionMode(0);
        if (tmr == null) {
            tmr = SetTabletState(1, ctx, 50);
        }
        else {
            SetTabletState(0, tmr);
            tmr = null;
            tmr = SetTabletState(1, ctx, 50);
        }
        sigPlus = true;
    } catch (e) {
        sigPlus = false;
	}
}

function hideButtonsIfManageSignatures() {
	if (isManageSignatures) {
		document.getElementById("save").style.display = "none";
	}
}

function onTouchStart(e) {
	
	e.preventDefault();
		
	//we are dealing with a single touch event
	if (e.touches.length == 1) {	
		//set touch start defaults
		started = false;
		moved = false;
        	
		ctx.lineCap = 'round';
		ctx.lineJoin = 'round';
		ctx.lineWidth = penSize; 	
        
		//get touch start position
		var pos = jQuery(canvas).offset();
		
		startX = e.touches[0].pageX - pos.left;
		startY = e.touches[0].pageY - pos.top;
        	
		//add event listeners for touch move,end and canvel events
		canvas.addEventListener('touchmove', onTouchMove, false);
		canvas.addEventListener('touchend', onTouchEnd, false);
		canvas.addEventListener('touchcancel', onTouchCancel, false);
		
		OnSignEvent(false, true);
	}
}
	
function onTouchMove(e) {
	
	e.preventDefault();
		
	//value to flag that we have triggered a touch move event
	moved = true;
        
	//if we are dealing with a single touch event
	if (e.touches.length == 1) {
            
		var pos = jQuery(canvas).offset();
		
		//if this is the start of a series of touch move events
		if (stage == 1) {
            ctx.strokeStyle = 'rgb(' + r + ',' + g + ',' + b + ')';
			ctx.beginPath();
			ctx.moveTo(startX, startY);   
			stage = 2;             
		} else if (stage == 2) {
			bCX = e.touches[0].pageX - pos.left;
            bCY = e.touches[0].clientY - pos.top;
			stage = 3;
		} else if (stage == 3) {
            bX = e.touches[0].clientX - pos.left;
            bY = e.touches[0].clientY - pos.top;

            bCY = bCY - 0.9*(bY-bCY);
            bCX = bCX - 0.9*(bX-bCX);
            		
            ctx.quadraticCurveTo(bCX, bCY, bX, bY);		
            ctx.stroke();
            ctx.closePath();
            startX = e.touches[0].pageX - pos.left;
			startY = e.touches[0].clientY - pos.top;
            stage = 1;
		}
		
		OnSignEvent(false, true);
	}
}
	
function onTouchEnd(e) {
	
	e.preventDefault();
		
	//if we are dealing with a single touch event
	if (e.touches.length == 0) {
		
		//if a touch move event has not been triggered, we must be dealing with a tap
		if (!moved) {
			
			var pos = jQuery(canvas).offset();
			
			//in this case, we simply draw a shape in the spot the users finger leaves the screen
			ctx.beginPath();     
			ctx.strokeStyle = 'rgb(' + r + ',' + g + ',' + b + ')';
			ctx.beginPath();
			ctx.moveTo(e.changedTouches[0].pageX - pos.left, e.changedTouches[0].pageY - pos.top);
			ctx.lineTo(e.changedTouches[0].pageX - pos.left, e.changedTouches[0].pageY - pos.top);
			ctx.stroke();
			ctx.closePath();
			ctx.stroke();
			ctx.closePath();
		}
		
		if (stage != 1) {
			ctx.closePath();
			stage = 1;
		}
							
		//remove touchmove, touchend and touchcancel event listeners
		canvas.removeEventListener('touchmove', onTouchMove, false);
		canvas.removeEventListener('touchend', onTouchEnd, false);
		canvas.removeEventListener('touchcancel', onTouchCancel, false);
		
	}		
}
	
function onTouchCancel(e) {
					
	//remove touchmove, touchend and touchcancel event listeners
	canvas.removeEventListener('touchmove', onTouchMove, false);
	canvas.removeEventListener('touchend', onTouchEnd, false);
	canvas.removeEventListener('touchcancel', onTouchCancel, false);
		
}
	
function onMouseDown(e) {
	
	e.preventDefault();
	
	started = false;
	moved = false;
    	
	ctx.lineCap = 'round';
	ctx.lineJoin = 'round';
	ctx.lineWidth = penSize; 	
    
	var pos = jQuery(canvas).offset();
	
	startX = e.pageX - pos.left;
	startY = e.pageY - pos.top;
	
	canvas.addEventListener('mousemove', onMouseMove, false);
	canvas.addEventListener('mouseup', onMouseUp, false);
	canvas.addEventListener('mousecancel', onMouseCancel, false);
	
	OnSignEvent(false, true);
}
	
function onMouseMove(e) {
	e.preventDefault();
	
	moved = true;
	
	var pos = jQuery(canvas).offset();
	
	//if this is the start of a series of touch move events
	if (stage == 1) {
        ctx.strokeStyle = 'rgb(' + r + ',' + g + ',' + b + ')';
		ctx.beginPath();
		ctx.moveTo(startX, startY);   
		stage = 2;             
	} else if (stage == 2) {
		bCX = e.pageX - pos.left;
        bCY = e.clientY - pos.top;
		stage = 3;
	} else if (stage == 3) {
        bX = e.clientX - pos.left;
        bY = e.clientY - pos.top;

        bCY = bCY - 0.9*(bY-bCY);
        bCX = bCX - 0.9*(bX-bCX);
        		
        ctx.quadraticCurveTo(bCX, bCY, bX, bY);		
        ctx.stroke();
        ctx.closePath();
        startX = e.pageX - pos.left;
		startY = e.clientY - pos.top;
        stage = 1;
	}
	
}
	
function onMouseUp(e) {
		
	e.preventDefault();
			
	var pos = jQuery(canvas).offset();
	
	//in this case, we simply draw a shape in the spot the users finger leaves the screen
	ctx.beginPath();     
	ctx.strokeStyle = 'rgb(' + r + ',' + g + ',' + b + ')';
	ctx.beginPath();
	ctx.moveTo(e.pageX - pos.left, e.pageY - pos.top);
	ctx.lineTo(e.pageX - pos.left, e.pageY - pos.top);
	ctx.stroke();
	ctx.closePath();
	ctx.stroke();
	ctx.closePath();
	
	if (stage != 1) {
		ctx.closePath();
		stage = 1;
	}
		
	canvas.removeEventListener('mousemove', onMouseMove, false);
	canvas.removeEventListener('mouseup', onMouseUp, false);		
}

function onMouseCancel(e) {
	
	//remove touchmove, touchend and touchcancel event listeners
	canvas.removeEventListener('mousemove', onMouseMove, false);
	canvas.removeEventListener('mouseend', onMouseEnd, false);
	canvas.removeEventListener('mousecancel', onMouseCancel, false);
		
}

function clearCanvas() {
	// clear canvas
	ctx.fillStyle = 'rgb(255,255,255)';
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	ctx.fill();
	// clear favourite signature selection
	document.getElementById("favouriteSignatures").value = "";
	document.getElementById("signatureImage").value = "";
	// reset buttons
	OnSignEvent(false, false);
	// clear signature from parent
	if (listener) {
		listener({ target: caller,
			isCleared: true,
			isDirty: false,
		});
	}
	if (sigPlus) {
        ClearTablet();
	}
}
   
function saveCanvas(e, label) {
    var strDataURI = canvas.toDataURL("image/png");
    document.getElementById("signatureImage").value = strDataURI;
    // Used to submit the form using Ajax.
    if (_in_window) {
    	jQuery.ajax({
        	type: "POST",
    		url: contextPath + "/signature_pad/uploadSignature.jsp",
    		data: jQuery("#signatureForm").formSerialize(),
    		success: function(data) {    
    			var savedId = jQuery(jQuery(data.trim())[0]).val();
    			OnSignEvent(true, false, savedId);
					// add signature to dropdown if a label is provided
					addFavouriteSignatureToDropdown(label, savedId, strDataURI);
    		}
    	});
    	return false;
    }
    else {
    	document.getElementById("signatureForm").submit();
    }
}

function _internalSaveCanvas(parameters) {
	var strDataURI = canvas.toDataURL("image/png");
    
    document.getElementById("signatureImage").value = strDataURI;
    // Used to submit the form using Ajax.
    if (_in_window) {
    	jQuery.ajax({
        	type: "POST",
    		url: contextPath + "/signature_pad/uploadSignature.jsp" + parameters,
    		data: jQuery("#signatureForm").formSerialize(),
    		success: function(data) {    
    			var savedId = jQuery(jQuery(data.trim())[0]).val();
    			OnSignEvent(true, false, savedId);
    		}
    	});
    	return false;
    }
    else {
    	document.getElementById("signatureForm").submit();
    }	
}

function signCanvas() {
	_internalSaveCanvas("");	
}

function signAndSaveCanvas() {	
	_internalSaveCanvas("?saveAsDefaultSig=true&provNum=" + provNum);
}

//function that runs once the document has loaded
function loaded() {
	
	//prevent default scrolling on document window
	document.addEventListener('touchmove', function(e) {
		e.preventDefault()
	}, false);
    
	canvas = document.querySelector('canvas');
    
	//if the browser supports canvas context
	if (canvas.getContext) {
        
		//initialize the app
		init();
        
	}
	//else alert the user and do nothing more
	else {
		alert('Your browser does not support Canvas 2D drawing, sorry!');
	}
	
	if (_in_window) { 
		if (parent.signatureHandler) { addSignatureListener(parent, parent.signatureHandler); }
	}
}

var caller = null, listener = null;

// Add a listener for signature events with parameters (save, dirty) 
// NOTE: Only supports one listener at a time.
function addSignatureListener(element,listener) {
	caller = element;
	this.listener = listener; 
}

function OnSignEvent(save,dirty,savedId) {	
	if (enableEformDigitalSig) {
		document.getElementById("sign").style.display = dirty ? "inline" : "none";
		document.getElementById("signAndSave").style.display = dirty ? "inline" : "none";		
	} else {
		document.getElementById("save").style.display = dirty ? "inline" : "none";
	}		
	document.getElementById("clear").style.display = dirty || save ? "inline" : "none";
	document.getElementById("signMessage").style.display = !dirty && !save ? "inline" : "none";
	document.getElementById("displaySaveSignatureMenu").style.display = dirty ? "inline" : "none";
	document.getElementById("favouriteSignatures").style.display = !save ? "inline" : "none";
	hideButtonsIfManageSignatures();
	if (listener && save) {
		listener({ target: caller, 
			       isSave: save, 
			       isDirty: dirty,
			       requestIdKey: requestIdKey,
			       previewImageUrl: previewImageUrl,
			       storedImageUrl: storedImageUrl + savedId
		});
	}
	setSigning(dirty);
	setSaved(save);
}

function isSaved() {
	return saved;
}

function setSaved(isSaved) {
	saved = isSaved;
}

function isSigning() {
    return signing;
}

function setSigning(isSigning) {
    signing = isSigning;
}

window.addEventListener("load", loaded, true);

window.addEventListener("unload", function (ev) {
	if (sigPlus) {
        ClearTablet();
        SetTabletState(0, tmr);
	}
}, true);

function displaySaveSignatureMenu() {
	document.getElementById("saveSignatureMenu").style.display = "block";
	showMainSignatureButtons(false);
	disableCanvas();
}

function closeSaveSignatureMenu() {
	document.getElementById("saveSignatureMenu").style.display = "none";
	showMainSignatureButtons(true);
	enableCanvas();
}

function enableCanvas() {
	canvas.addEventListener('touchstart', onTouchStart, false);
	canvas.addEventListener('mousedown', onMouseDown, false);
}

function disableCanvas() {
	canvas.removeEventListener('touchstart', onTouchStart, false);
	canvas.removeEventListener('mousedown', onMouseDown, false);
}

function showMainSignatureButtons(isShowing) {
	document.getElementById("save").style.display = isShowing ? "inline" : "none";
	document.getElementById("clear").style.display = isShowing ? "inline" : "none";
	document.getElementById("displaySaveSignatureMenu").style.display = isShowing ? "inline" : "none";
	document.getElementById("favouriteSignatures").style.display = isShowing ? "inline" : "none";
}

function saveFavouriteSignature() {
	const label = document.getElementById("saveFavouriteSignatureLabel").value;
	if (!label) {
		alert("Please enter a label for your favourite signature");
		return;
	}
	document.getElementById("signatureLabel").value = label;
	// needs to be saved to db
	document.getElementById("saveToDb").value = "true";
	saveCanvas(null, label);
	document.getElementById("saveToDb").value = isSaveToDb;
	closeSaveSignatureMenu();
}

function onSelectFavouriteSignature() {
	document.getElementById("save").style.display = "none";
	document.getElementById("displaySaveSignatureMenu").style.display = "none";
	const signatureId = document.getElementById("favouriteSignatures").value;
	drawSignatureOnCanvasAndSave(signatureId);
}

function drawSignatureOnCanvasAndSave(signatureId) {
	// map value should be a byte array in the form of a string
	const base64String = convertByteArrayStringToBase64String(
			favouriteSignatures.get(signatureId));
	// create image from base64 string
	const img = new Image();
	const src = BASE64_MARKER + base64String;
	// draw image on canvas
	img.src = src;
	img.onload = function() {
		ctx.drawImage(img, 0, 0);
		document.getElementById("signatureImage").value = src;
		// don't re-save signature to database
		document.getElementById("saveToDb").value = "false";
		jQuery.ajax({
			type: "POST",
			url: contextPath + "/signature_pad/uploadSignature.jsp",
			data: jQuery("#signatureForm").formSerialize(),
			success: function(data) {
				OnSignEvent(true, false, signatureId);
				// reset database save value
				document.getElementById("saveToDb").value = isSaveToDb;
			}
		});
	};
}

function addFavouriteSignatureToDropdown(label, signatureId, base64String) {
	if (!label) {
		return;
	}
	// convert image to correct format for map
	favouriteSignatures.set(signatureId,
			convertBase64StringToByteArrayString(base64String.split(",")[1]));
	// add option to favourite signatures select element
	const option = new Option(label, signatureId);
	const select = document.getElementById("favouriteSignatures");
	select.add(option, undefined);
	// select new option
	select.value = signatureId;
}

function convertByteArrayStringToBase64String(byteArrayString) {
	// remove square brackets and split the string by ', ' to get an array of string values
	const byteArrayStringValues = byteArrayString.substring(1, byteArrayString.length - 1).split(", ");
	// convert the string values to numbers and create an array of bytes
	const byteArray = byteArrayStringValues.map(value => parseInt(value));
	// convert the array of bytes to a Uint8Array
	const uint8Array = new Uint8Array(byteArray);
	// convert Uint8Array to a string
	let binaryString = '';
	uint8Array.forEach(byte => {
		binaryString += String.fromCharCode(byte);
	});
	// convert binary string to base64
	return btoa(binaryString);
}

function convertBase64StringToByteArrayString(base64String) {
	// convert base64 string to binary string
	const binaryString = atob(base64String);
	// convert binary string to array of bytes
	const byteArray = [];
	for (let i = 0; i < binaryString.length; i++) {
		byteArray.push(binaryString.charCodeAt(i));
	}
	// convert array of bytes to string
	let byteArrayString = '[';
	byteArray.forEach(byte => {
		byteArrayString += byte + ', ';
	});
	byteArrayString = byteArrayString.substring(0, byteArrayString.length - 2) + ']';
	return byteArrayString;
}

function manageSignatures() {
	var popup = window.open(
			`${contextPath}/signature_pad/providerPreference.jsp`,
			'Signature Favourites',
			'height=650,width=1050,location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes'
	);
	if (popup == null) {
		return;
	}
	if (popup.opener == null) {
		popup.opener = self;
	}
	popup.focus();
}