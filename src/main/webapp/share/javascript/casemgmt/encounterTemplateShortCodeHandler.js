class EncounterTemplateShortCodeHandler {
    constructor(noteElementId, shortCodes, endpointContext, csrfToken) {
        let noteElement = document.getElementById(noteElementId);
        if (!noteElement) {
            console.error(`encounterTemplateShortCodeHandler: element \'${noteElementId}\' does not exist`);
        }
        this.noteElement = noteElement;
        this.endpointContext = endpointContext;
        this.csrfToken = csrfToken;


        this.setShortCodes(shortCodes);
    }

    setShortCodes(shortCodes) {
        this.shortCodes = shortCodes || [];
        if (this.shortCodes.size() > 0) {
            let boundShortCodeHandler = this.shortCodeHandler.bind(this);
            this.noteElement.addEventListener('keydown', boundShortCodeHandler);
        }
    }

    shortCodeHandler(event) {
        let selectionStart = event.target.selectionStart;
        let noteText = this.noteElement.value;
        if (event.code === 'Backslash') {
            event.preventDefault();
            // console.log(event.target.selectionStart);
            let selectionStart = event.target.selectionStart;
            let shortCodeKey = '';
            
            let matchedTemplateName = null;

            // loop backward from selection to find the entire word
            for (let i = selectionStart - 1; i > 0; i--) {
                let char = noteText.charAt(i);
                if (char !== '\n') {
                    shortCodeKey = char + shortCodeKey;
                } else {
                    break;
                }
                if (this.shortCodes.filter((key) => key.toLowerCase() === shortCodeKey.toLowerCase()).length > 0) {
                    matchedTemplateName = shortCodeKey;
                    break;
                }
            }
            
            if (matchedTemplateName) {
                // remove the short code key from the note text
                this.noteElement.value = noteText.substring(0, selectionStart - matchedTemplateName.length) + ' ' + noteText.substring(selectionStart);
                this.sendRequest(matchedTemplateName, selectionStart - matchedTemplateName.length);
                // ajaxInsertTemplate(matchedTemplateName, this.onSendRequestSuccess);
            } else {
                this.noteElement.value = noteText.substring(0, selectionStart) + '\\' + noteText.substring(selectionStart);
                this.noteElement.setSelectionRange(selectionStart + 1, selectionStart + 1);
            }
        }
    }

    sendRequest(templateName, indexToInsertTemplate) {
        const httpRequest = new XMLHttpRequest();
        let parameters = '?templateName=' + encodeURI(templateName) + '&version=2&' + this.csrfToken['name'] + '=' + this.csrfToken['value'];
        
        // bind events
        let onSendRequestSuccessEvent = this.onSendRequestSuccess.bind(this, indexToInsertTemplate);
        httpRequest.addEventListener('load', onSendRequestSuccessEvent);
        let onSendRequestErrorEvent = this.onSendRequestError.bind(this);
        httpRequest.addEventListener('error', onSendRequestErrorEvent);
        

        httpRequest.open('POST', `${this.endpointContext}/oscarEncounter/InsertTemplate.do${parameters}`);
        httpRequest.send();
    }

    onSendRequestSuccess(selectionStart, event) {
        if (event.target.status === 200 && event.target.responseText !== 'null') {
            let text = event.target.responseText;
            // apply transformations from writeToEncounterNote function
            text = text.replace(/\\u000A/g, "\u000A");
            text = text.replace(/\\u000D/g, "");
            text = text.replace(/\\u003E/g, ">");
            text = text.replace(/\\u003C/g, "<");
            text = text.replace(/\\u005C/g, "\\");
            text = text.replace(/\\u0022/g, "\"");
            text = text.replace(/\\u0027/g, "'");

            // remove the short code key from the note text and replace with template text
            let noteText = this.noteElement.value
            this.noteElement.value = noteText.substring(0, selectionStart) + ' ' + text + noteText.substring(selectionStart);
            this.noteElement.setSelectionRange(selectionStart + text.length, selectionStart + text.length);

            if (typeof chartNoteAutosave !== 'undefined') {
                chartNoteAutosave.setChanged();
            }
        }
    }
    onSendRequestError(event) {
        console.log(event);
    }
}