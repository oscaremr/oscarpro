<%--
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>

<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.DynacareCopyToProvider" %>
<%@ page import="org.oscarehr.common.dao.DynacareCopyToProviderDao" %>
<%@ page import="java.util.*" %>
<%@ page import="org.apache.commons.lang.WordUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%
	if (session.getAttribute("user") == null) {
		response.sendRedirect("../logout.jsp");
	}
	DynacareCopyToProviderDao copyToProviderDao =
			SpringUtils.getBean(DynacareCopyToProviderDao.class);
	List<DynacareCopyToProvider> copyToProviders = copyToProviderDao.findAll();
%>

<html:html locale="true">
	<head>
		<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
		<title>Copy To Providers Search</title>
		<script src="<%= request.getContextPath() %>/JavaScriptServlet"
						type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" href="../../share/css/extractedFromPages.css"/>
		<link rel="stylesheet" type="text/css" href="eformSearchEditCopyToProvider.css">
		<script>
      function submitForm(id) {
        let form = document.getElementById("submit-edit-copy-" + id);
        form.submit();
      }
		</script>
	</head>
	<body>
	Select a Copy to Provider to edit:
	<table>
		<tr class="title">
			<th style="width:20%">Last Name</th>
			<th style="width:15%">First Name</th>
			<th style="width:20%">Address</th>
			<th style="width:15%">City</th>
			<th style="width:10%">CPSO</th>
			<th style="width:15%">Billing Number</th>
		</tr>
		<%
			for (int i = 0; i < copyToProviders.size(); i++) {
				DynacareCopyToProvider provider = copyToProviders.get(i);
				String bgColor = i % 2 == 0 ? "#EEEEFF" : "ivory";
		%>
		<tr onMouseOver="this.style.cursor='hand';this.style.backgroundColor='pink';"
				onMouseout="this.style.backgroundColor='<%= bgColor %>';"
				onClick="submitForm('<%= provider.getId() %>')">
			<td class="hidden">
				<form action="editCopyToProvider.jsp" id="submit-edit-copy-<%= provider.getId() %>"
							method="post">
					<input type="hidden" name="id" value="<%= Integer.toString(provider.getId()) %>">
				</form>
			</td>
			<td>
				<%= Encode.forHtml(WordUtils.capitalize(provider.getLastName().toLowerCase())) %>
			</td>
			<td>
				<%= Encode.forHtml(WordUtils.capitalize(provider.getFirstName().toLowerCase())) %>
			</td>
			<td>
				<%= Encode.forHtml(provider.getAddress1()) %>
			</td>
			<td>
				<%= Encode.forHtml(provider.getCity()) %>
			</td>
			<td>
				<%= Encode.forHtml(provider.getCpso()) %>
			</td>
			<td>
				<%= Encode.forHtml(provider.getBillingNumber()) %>
			</td>
		</tr>
		<% } %>
	</table>
	<br>
	<center>
		<a href="eformSearchCopyToProvider.jsp">
			Back
		</a>
	</center>
</html:html>
