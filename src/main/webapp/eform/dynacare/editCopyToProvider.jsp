<%--
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>

<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%
	boolean authed = true;
	if (!authed) {
		return;
	}
%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.oscarehr.common.model.DynacareCopyToProvider" %>
<%@ page import="oscar.oscarDemographic.data.ProvinceNames" %>
<%@ page import="org.oscarehr.common.dao.DynacareCopyToProviderDao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<html:html locale="true">
	<head>
		<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
		<script src="<%= request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
		<title>Edit Copy To Provider</title>
		<script src="<%= request.getContextPath() %>/JavaScriptServlet"
						type="text/javascript"></script>
		<html:base/>
		<link rel="stylesheet" type="text/css" media="all"
					href="../../share/css/extractedFromPages.css"/>
	</head>
	<link rel="stylesheet" type="text/css" href="editCopyToProvider.css">
	<body class="body-style" vlink="#0000FF">
	<html:errors/>
	<table class="main-table" id="scrollNumber1" name="addCopyToProviderTable">
		<tr class="main-table-top-row">
			<td class="main-table-top-row-right-column">
				<table class="top-status-bar">
					<tr>
						<td class="header">Edit Copy To Provider</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr style="vertical-align: top">
			<td class="main-table-right-column">
				<table class="form-table">
					<%
						DynacareCopyToProviderDao copyToProviderDao = (DynacareCopyToProviderDao) SpringUtils
								.getBean(DynacareCopyToProviderDao.class);
						DynacareCopyToProvider copyToProvider = copyToProviderDao.find(
								Integer.valueOf(request.getParameter("id")));
						if (copyToProvider == null) {
							copyToProvider = new DynacareCopyToProvider();
						}
					%>
					<tr>
						<td>
							<html:form action="/eform/dynacare/AddCopyToProvider">
								<%
									Integer id = request.getParameter("id") != null ? Integer
											.parseInt(request.getParameter("id")) : null;
								%>
								<table>
									<html:hidden name="AddCopyToProviderForm" property="id"
															 value="<%= copyToProvider.getId() != null? Integer.toString(copyToProvider.getId()) : \"\" %>"/>
									<html:hidden name="AddCopyToProviderForm" property="type"
															 value="<%= DynacareCopyToProvider.class.getSimpleName() %>"/>
									<tr>
										<td>
											<bean:message
													key="oscarEncounter.oscarConsultationRequest.config.AddSpecialist.firstName"/>
										</td>
										<td>
											<html:text name="AddCopyToProviderForm"
																 property="firstName"
																 value="<%= copyToProvider.getFirstName() %>"/>
										</td>
										<td>
											<bean:message
													key="oscarEncounter.oscarConsultationRequest.config.AddSpecialist.lastName"/>
										</td>
										<td>
											<html:text name="AddCopyToProviderForm"
																 property="lastName"
																 value="<%= copyToProvider.getLastName() %>"/>
										</td>
									</tr>
									<tr>
										<td>CPSO</td>
										<td>
											<html:text name="AddCopyToProviderForm"
																 property="cpso"
																 value="<%= copyToProvider.getCpso() %>"/>
										</td>
										<td>Billing Number</td>
										<td>
											<html:text name="AddCopyToProviderForm"
																 property="billingNumber"
																 value="<%= copyToProvider.getBillingNumber() %>"/>
										</td>
									</tr>
									<tr>
										<td>Address</td>
										<td>
											<html:text name="AddCopyToProviderForm"
																 property="address1"
																 value="<%= copyToProvider.getAddress1() %>"/>
										</td>
										<td>City</td>
										<td>
											<html:text name="AddCopyToProviderForm" property="city"
																 value="<%= copyToProvider.getCity() %>"/>
										</td>
									</tr>
									<tr>
										<td>Province</td>
										<td>
											<html:select name="AddCopyToProviderForm"
																	 property="stateOrProvince">
												<%
													Map<String, String> provinces = ProvinceNames.getCanadianProvinces();
													for (Map.Entry<String, String> entry : provinces.entrySet()) {
														String shortName = entry.getKey();
														String longName = entry.getValue();
														String selected = "";
														if (shortName.equals(copyToProvider.getStateOrProvince())) {
															selected = "selected";
														}
												%>
												<option value="<%=shortName%>" <%= selected %>>
													<%=shortName%>-<%=longName%>
												</option>
												<% } %>
											</html:select>
										</td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td>Postal Code</td>
										<td>
											<html:text name="AddCopyToProviderForm"
																 property="zipOrPostal"
																 value="<%= copyToProvider.getZipOrPostal() %>"/></td>
										<td>
											<input type="hidden" name="country"
														 value="<%= copyToProvider.getCountry() %>"/>
										</td>
										<td></td>
									</tr>
									<tr>
										<td colspan="6">
											<input type="submit" name="Submit"/>
										</td>
									</tr>
								</table>
							</html:form>
						</td>
					</tr>
					<tr height="100%">
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="main-table-bottom-row-right-column"></td>
		</tr>
	</table>
	</body>
</html:html>
