<%--

 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>

<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="org.oscarehr.common.model.Provider" %>
<%@page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="java.util.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="org.apache.commons.lang.WordUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%!
    private String getOnClickForTypeInProvider(Properties prop) {
      return  "typeInProvider('" + StringEscapeUtils
              .escapeJavaScript(prop.getProperty("first_name", "")) + "', '"
              + StringEscapeUtils.escapeJavaScript(prop.getProperty("last_name", ""))
              + "', '" + StringEscapeUtils
              .escapeJavaScript(prop.getProperty("ohip_no", "")) + "', '"
              + StringEscapeUtils.escapeJavaScript(prop.getProperty("cpsid", ""))
              + "', '" + StringEscapeUtils
              .escapeJavaScript(prop.getProperty("address", "")) + "')";
    }
%>
<%
    ProviderDao providerDao = (ProviderDao) SpringUtils.getBean("providerDao");
    if (session.getAttribute("user") == null) {
        response.sendRedirect("../logout.jsp");
    }

    Vector vec = new Vector();
    Properties prop = null;
    String param = request.getParameter("param") == null ? "" : request.getParameter("param");
    String param2 = request.getParameter("param2") == null ? "" : request.getParameter("param2");
    String doctorName = request.getParameter("refDoctorName") == null ? ""
            : request.getParameter("refDoctorName");
    String searchType =
            request.getParameter("searchType") == null ? "" : request.getParameter("searchType");

    String keyword = request.getParameter("keyword");
    Integer doctorId = null;
    String doctorNameData = "";
    List<Provider> providers = null;
    if (request.getParameter("submit") != null && (request.getParameter("submit").equals("Search")
            || request.getParameter("submit").equals("Next Page")
            || request.getParameter("submit").equals("Last Page"))) {
        String[] temp = keyword.split("\\,\\p{Space}*");

        if (temp.length > 1) {
            providers = providerDao.findDynacareProvidersByFullName(temp[0], temp[1]);
        } else if (temp.length == 1 && temp[0] != "") {
            providers = providerDao.findDynacareProvidersByLastName(temp[0]);
        } else {
            providers = providerDao.getActiveProviders();
        }

    }

    if (providers != null && doctorNameData.equals("")) {
        for (Provider provider : providers) {
            prop = new Properties();
            prop.setProperty("last_name",
                    (provider.getLastName() != null ? provider.getLastName() : ""));
            prop.setProperty("first_name",
                    (provider.getFirstName() != null ? provider.getFirstName() : ""));
            prop.setProperty("ohip_no", (provider.getOhipNo() != null ? provider.getOhipNo() : ""));
            prop.setProperty("cpsid",
                    (provider.getPractitionerNo() != null ? provider.getPractitionerNo() : ""));
            prop.setProperty("address",
                    (provider.getAddress() != null ? provider.getAddress() : ""));
            vec.add(prop);
        }
    }
%>

<html:html locale="true">
    <head>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <title>Copy To Providers Search</title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet"
                type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" media="all"
              href="../../share/css/extractedFromPages.css"/>
        <script language="JavaScript">

          function setfocus() {
            this.focus();
            document.forms[0].keyword.focus();
            document.forms[0].keyword.select();
          }

          function check() {
            document.forms[0].submit.value = "Search";
            return true;
          }

          function typeInProvider(firstName, lastName, oHipNo, cpsid, address) {
            opener.document.getElementById('doctor').value = lastName + ", " + firstName;
            opener.document.getElementById('doctor_cpsid').value = cpsid;
            opener.document.getElementById('doctor_ohip_no').value = oHipNo;
            opener.document.getElementById('doctor_first_name').value = firstName;
            opener.document.getElementById('doctor_last_name').value = lastName;
            opener.document.getElementById('doctor_cpsid_hidden').value = cpsid;
            opener.document.getElementById('doctor_ohip_no_hidden').value = oHipNo;

            self.close();
          }

        </script>
    </head>
    <body bgcolor="white" bgproperties="fixed" onload="setfocus()"
          topmargin="0" leftmargin="0" rightmargin="0">
        <form method="post" name="titlesearch" action="eformSearchProvider.jsp">
            <table border="0" cellpadding="1" cellspacing="0" width="100%"
                bgcolor="#CCCCFF">
                onSubmit="return check();">
                <tr>
                    <td class="searchTitle" colspan="4">Search Clinician/Practitioner</td>
                </tr>
                <tr>
                    <td class="blueText" width="10%" nowrap>
                        <input type="radio" name="search_mode" value="search_name" checked> Name
                    </td>

                    <td valign="middle" rowspan="2" align="left">
                        <input type="text"
                               name="keyword"
                               value="<%= Encode.forHtmlAttribute(searchType.equals("name") ? doctorName : "") %>"
                               size="17" maxlength="100">
                        <input type="hidden" name="orderby" value="last_name, first_name">
                        <input type="hidden" name="limit1" value="0">
                        <input type="hidden" name="limit2" value="10">
                        <input type="hidden" name="submit" value='Search'>
                        <input type="submit" value='Search'>
                    </td>
                </tr>
                <input type='hidden' name='param' value="<%=StringEscapeUtils.escapeHtml(param)%>">
                <input type='hidden' name='param2' value="<%=StringEscapeUtils.escapeHtml(param2)%>">
            </table>
        </form>
        <table width="95%" border="0">
            <tr>
                <td align="left">Results based on keyword(s): <%=keyword == null ? "" : keyword%></td>
            </tr>
        </table>
        <center>
        <table width="100%" border="0" cellpadding="0" cellspacing="2"
               bgcolor="#C0C0C0">
            <tr class="title">
                <th width="25%">Last Name</b></th>
                <th width="20%">First Name</b></th>
                <th width="25%">Address</th>
            </tr>
            <%
                for (int i = 0; i < vec.size(); i++) {
                    prop = (Properties) vec.get(i);
                    String bgColor = i % 2 == 0 ? "#EEEEFF" : "ivory";
                    String strOnClick = getOnClickForTypeInProvider(prop);
            %>
            <tr align="center" bgcolor="<%=bgColor%>" align="center"
                onMouseOver="this.style.cursor='hand';this.style.backgroundColor='pink';"
                onMouseout="this.style.backgroundColor='<%=bgColor%>';"
                onClick="<%=strOnClick%>">

                <td>
                    <%=Encode.forHtml(WordUtils.capitalize(prop.getProperty("last_name", "").toLowerCase()))%>
                </td>
                <td>
                    <%=Encode.forHtml(WordUtils.capitalize(prop.getProperty("first_name", "").toLowerCase()))%>
                </td>
                <td>
                    <%= Encode.forHtml(prop.getProperty("address", ""))%>
                </td>
            </tr>
            <% } %>
        </table>
        <% if (vec.size() == 0) { %>
        <bean:message key="demographic.search.noResultsWereFound"/>
        <% } %>
        <form method="post" name="nextform" action="eformSearchProvider.jsp"/>
        <br>
    </body>
</html:html>
