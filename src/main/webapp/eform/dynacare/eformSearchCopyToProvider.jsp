<%--

 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>

<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="org.oscarehr.common.model.DynacareCopyToProvider" %>
<%@page import="org.oscarehr.common.dao.DynacareCopyToProviderDao" %>
<%@page import="java.util.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="org.apache.commons.lang.WordUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%!
    private String getOnClickForTypeInCopyToProvider(Properties prop) {
        return "typeInCopyToProvider('" + StringEscapeUtils
                .escapeJavaScript(prop.getProperty("first_name", "")) + "', '"
                + StringEscapeUtils.escapeJavaScript(prop.getProperty("last_name", ""))
                + "', '" + StringEscapeUtils
                .escapeJavaScript(prop.getProperty("salutation", "")) + "', '"
                + StringEscapeUtils.escapeJavaScript(prop.getProperty("address1", ""))
                + "', '" + StringEscapeUtils
                .escapeJavaScript(prop.getProperty("city", "")) + "', '"
                + StringEscapeUtils
                .escapeJavaScript(prop.getProperty("state_or_province", "")) + "', '"
                + StringEscapeUtils.escapeJavaScript(prop.getProperty("country", ""))
                + "', '" + StringEscapeUtils
                .escapeJavaScript(prop.getProperty("zip_or_postal", "")) + "', '"
                + StringEscapeUtils.escapeJavaScript(prop.getProperty("id", "")) + "', '"
                + StringEscapeUtils.escapeJavaScript(prop.getProperty("cpso", "")) + "', '"
                + StringEscapeUtils.escapeJavaScript(prop.getProperty("billingNumber", "")) + "')";
    }
%>
<%
    DynacareCopyToProviderDao copyToProviderDao = (DynacareCopyToProviderDao) SpringUtils
            .getBean(DynacareCopyToProviderDao.class);

    if (session.getAttribute("user") == null) {
        response.sendRedirect("../logout.jsp");
    }

    Vector vec = new Vector();
    Properties prop = null;
    String param = request.getParameter("param") == null ? "" : request.getParameter("param");
    String param2 = request.getParameter("param2") == null ? "" : request.getParameter("param2");

    String doctorName = request.getParameter("refDoctorName") == null ? ""
            : request.getParameter("refDoctorName");
    String searchType =
            request.getParameter("searchType") == null ? "" : request.getParameter("searchType");

    String keyword = request.getParameter("keyword");
    Integer doctorId = null;
    String doctorNameData = "";

    List<DynacareCopyToProvider> copyToProviders = null;

    if (request.getParameter("submit") != null && (request.getParameter("submit").equals("Search")
            || request.getParameter("submit").equals("Next Page") || request.getParameter("submit")
            .equals("Last Page"))) {

        String[] temp = keyword.split("\\,\\p{Space}*");

        if (temp.length > 1) {
            copyToProviders = copyToProviderDao.findByFullName(temp[0], temp[1]);
        } else if (temp.length == 1) {
            copyToProviders = copyToProviderDao.findByLastName(temp[0]);
        } else {
            copyToProviders = copyToProviderDao.findAll();
        }

    }

    if (copyToProviders != null && doctorNameData.equals("")) {
        for (DynacareCopyToProvider copyToProvider : copyToProviders) {
            prop = new Properties();
            prop.setProperty("salutation",
                    (copyToProvider.getSalutation() != null ? copyToProvider.getSalutation() : ""));
            prop.setProperty("last_name",
                    (copyToProvider.getLastName() != null ? copyToProvider.getLastName() : ""));
            prop.setProperty("first_name",
                    (copyToProvider.getFirstName() != null ? copyToProvider.getFirstName() : ""));
            prop.setProperty("middle_name",
                    (copyToProvider.getMiddleName() != null ? copyToProvider.getMiddleName() : ""));
            prop.setProperty("address1",
                    (copyToProvider.getAddress1() != null ? copyToProvider.getAddress1() : ""));
            prop.setProperty("address2",
                    (copyToProvider.getAddress2() != null ? copyToProvider.getAddress2() : ""));
            prop.setProperty("city",
                    (copyToProvider.getCity() != null ? copyToProvider.getCity() : ""));
            prop.setProperty("state_or_province",
                    (copyToProvider.getStateOrProvince() != null ? copyToProvider
                            .getStateOrProvince() : ""));
            prop.setProperty("zip_or_postal",
                    (copyToProvider.getZipOrPostal() != null ? copyToProvider.getZipOrPostal()
                            : ""));
            prop.setProperty("country",
                    (copyToProvider.getCountry() != null ? copyToProvider.getCountry() : ""));
            prop.setProperty("region",
                    (copyToProvider.getRegion() != null ? copyToProvider.getRegion() : ""));
            prop.setProperty("id", Integer.toString(copyToProvider.getId()));
            prop.setProperty("cpso", copyToProvider.getCpso() != null
                    ? copyToProvider.getCpso()
                    : "");
            prop.setProperty("billingNumber", copyToProvider.getBillingNumber() != null
                ? copyToProvider.getBillingNumber()
                : "");
            vec.add(prop);
        }
    }
%>

<html:html locale="true">
    <head>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <title>Copy To Providers Search</title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet"
                type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" media="all"
              href="../../share/css/extractedFromPages.css"/>
        <script language="JavaScript">

          function setfocus() {
            this.focus();
            document.forms[0].keyword.focus();
            document.forms[0].keyword.select();
          }

          function check() {
            document.forms[0].submit.value = "Search";
            return true;
          }

          function typeInCopyToProvider(firstName, lastName, salutation, street, city, province,
              country, postal, id, cpso, billingNumber) {
            opener.document.getElementById('CopyToProvider1FirstName').value = firstName;
            opener.document.getElementById('CopyToProvider1LastName').value = lastName;
            opener.document.getElementById('CopyToProvider1Address').value = street + '\n' + city
                + ', ' + province + ' ' + postal + '\n' + country;

            opener.document.getElementById('copy_to_doctor_1_first_name').value = firstName;
            opener.document.getElementById('copy_to_doctor_1_last_name').value = lastName;
            opener.document.getElementById('copy_to_doctor_1_title').value = salutation;
            opener.document.getElementById('copy_to_doctor_1_address').value = street + '\n' + city
                + ', ' + province + ' ' + postal + '\n' + country;

            opener.document.getElementById('copy_to_doctor_1_address1').value = street;
            opener.document.getElementById('copy_to_doctor_1_city').value = city;
            opener.document.getElementById('copy_to_doctor_1_province').value = province;
            opener.document.getElementById('copy_to_doctor_1_country').value = country;
            opener.document.getElementById('copy_to_doctor_1_postal').value = postal;
            opener.document.getElementById('copy_to_doctor_1_id').value = id;
            opener.document.getElementById('copy_to_doctor_1_cpsid').value = cpso;
            opener.document.getElementById('copy_to_doctor_1_ohip_no').value = billingNumber;

            self.close();
          }
        </script>
    </head>
    <body bgcolor="white" bgproperties="fixed" onload="setfocus()"
          topmargin="0" leftmargin="0" rightmargin="0">
    <form method="post" name="titlesearch" action="eformSearchCopyToProvider.jsp"
          onSubmit="return check();">
        <table border="0" cellpadding="1" cellspacing="0" width="100%" bgcolor="#CCCCFF">
            <tr>
                <td class="searchTitle" colspan="4">Search Copy To Clinician/Practitioner</td>
            </tr>
            <tr>
                <td class="blueText" width="10%" nowrap>
                    <input type="radio" name="search_mode" value="search_name" checked> Name
                </td>
                <td valign="middle" rowspan="2" align="left">
                   <input type="text" name="keyword"
                           value="<%= Encode.forHtmlAttribute(searchType.equals("name") ? doctorName : "") %>"
                           size="17" maxlength="100">
                    <input type="hidden" name="orderby" value="last_name, first_name">
                    <input type="hidden" name="limit1" value="0">
                    <input type="hidden" name="limit2" value="10">
                    <input type="hidden" name="submit" value='Search'>
                    <input type="submit" value='Search'>
                </td>
            </tr>
        </table>
        <input type='hidden' name='param' value="<%=StringEscapeUtils.escapeHtml(param)%>">
        <input type='hidden' name='param2' value="<%=StringEscapeUtils.escapeHtml(param2)%>">
    </form>
    <table width="95%" border="0">
        <tr>
            <td align="left">Results based on keyword(s): <%=keyword == null ? "" : keyword%></td>
        </tr>
    </table>
    <center>
    <table width="100%" border="0" cellpadding="0" cellspacing="2" bgcolor="#C0C0C0">
         <tr class="title">
            <th style="width:20%">Last Name</th>
            <th style="width:15%">First Name</th>
            <th style="width:20%">Address</th>
            <th style="width:15%">City</th>
            <th style="width:10%">CPSO</th>
            <th style="width:15%">Billing Number</th>
         </tr>
         <%
             for (int i = 0; i < vec.size(); i++) {
                 prop = (Properties) vec.get(i);
                 String bgColor = i % 2 == 0 ? "#EEEEFF" : "ivory";
                 String strOnClick = getOnClickForTypeInCopyToProvider(prop);
         %>
         <tr align="center" bgcolor="<%=bgColor%>" align="center"
             onMouseOver="this.style.cursor='hand';this.style.backgroundColor='pink';"
             onMouseout="this.style.backgroundColor='<%=bgColor%>';"
             onClick="<%=strOnClick%>">
             <td>
                 <%=Encode.forHtml(WordUtils.capitalize(prop.getProperty("last_name", "").toLowerCase()))%>
             </td>
             <td>
                 <%=Encode.forHtml(WordUtils.capitalize(prop.getProperty("first_name", "").toLowerCase()))%>
             </td>
             <td>
                 <%= Encode.forHtml(prop.getProperty("address1", ""))%>
             </td>
             <td>
                 <%=Encode.forHtml(prop.getProperty("city", ""))%>
             </td>
             <td>
                 <%= Encode.forHtml(prop.getProperty("cpso", "")) %>
             </td>
             <td>
                 <%= Encode.forHtml(prop.getProperty("billingNumber")) %>
             </td>
         </tr>
         <% } %>
    </table>
    <% if (vec.size() == 0) { %>
    <bean:message key="demographic.search.noResultsWereFound"/>
    <% } %>

    <form method="post" name="nextform" action="eformSearchCopyToProvider.jsp">
    <br>
    <a href="<%=request.getContextPath() %>/eform/dynacare/AddCopyToProvider.jsp">
        Add Copy To Provider
    </a>
    <br>
    <a href="<%=request.getContextPath() %>/eform/dynacare/eformSearchEditCopyToProvider.jsp">
        Edit Copy to Provider
    </a>
    </center>
    </body>
</html:html>
