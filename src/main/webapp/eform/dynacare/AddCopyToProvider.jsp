<%--

 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".

--%>

<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%
    boolean authed = true;
    if (!authed) {
        return;
    }
%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.LinkedHashMap" %>
<%@page import="org.oscarehr.common.model.DynacareCopyToProvider" %>
<%@page import="oscar.eform.data.eorder.AddCopyToProviderForm" %>
<%@page import="oscar.oscarDemographic.data.ProvinceNames" %>
<html:html locale="true">
    <head>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script src="<%= request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <title>Add New Copy To Provider</title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet"
                type="text/javascript"></script>
        <html:base/>
        <link rel="stylesheet" type="text/css" media="all"
              href="../../share/css/extractedFromPages.css"/>
    </head>
    <script language="javascript">
      function BackToOscar() {
        window.close();
      }
    </script>
    <script language="javascript">
      function setCopyToProvider() {
        if ($("input[name='id']").val() && $("input[name='id']").val().length > 0) {
          opener.document.getElementById('CopyToProvider1FirstName').value = $(
              "input[name='firstName']").val();
          opener.document.getElementById('CopyToProvider1LastName').value = $(
              "input[name='lastName']").val();
          opener.document.getElementById('CopyToProvider1Address').value = $(
              "input[name='address1']").val() + '\n' + $("input[name='city']").val() + ', ' + $(
              "select[name='stateOrProvince'] option:selected").val() + ' ' + $(
              "input[name='zipOrPostal']").val() + '\n' + $("input[name='country']").val();
          opener.document.getElementById('copy_to_doctor_1_first_name').value = $(
              "input[name='firstName']").val();
          opener.document.getElementById('copy_to_doctor_1_last_name').value = $(
              "input[name='lastName']").val();
          opener.document.getElementById('copy_to_doctor_1_address').value = $(
              "input[name='address1']").val() + '\n' + $("input[name='city']").val() + ', ' + $(
              "select[name='stateOrProvince'] option:selected").val() + ' ' + $(
              "input[name='zipOrPostal']").val() + '\n' + $("input[name='country']").val();
          opener.document.getElementById('copy_to_doctor_1_address1').value = $(
              "input[name='lastName']").val();
          opener.document.getElementById('copy_to_doctor_1_city').value = $(
              "input[name='city']").val();
          opener.document.getElementById('copy_to_doctor_1_province').value = $(
              "input[name='stateOrProvince']").val();
          opener.document.getElementById('copy_to_doctor_1_country').value = $(
              "input[name='country']").val();
          opener.document.getElementById('copy_to_doctor_1_postal').value = $(
              "input[name='zipOrPostal']").val();
          opener.document.getElementById('copy_to_doctor_1_id').value = $("input[name='id']").val();
          opener.document.getElementById('copy_to_doctor_1_title').value = "";
          self.close();
        }
      }
    </script>
    <link rel="stylesheet" type="text/css" href="AddCopyToProviderStyles.css">
    <body class="bodyStyle" vlink="#0000FF" onload="setCopyToProvider();">
    <html:errors/>
    <table class="mainTable" id="scrollNumber1" name="addCopyToProviderTable">
        <tr class="mainTableTopRow">
            <td class="mainTableTopRowRightColumn">
                <table class="topStatusBar">
                    <tr>
                        <td class="header">Add New Copy To Provider</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="vertical-align: top">
            <td class="mainTableRightColumn">
                <table cellpadding="0" cellspacing="2"
                       style="border-collapse: collapse" bordercolor="#111111" width="100%"
                       height="100%">
                    <!----Start new rows here-->
                    <%
                        String added = (String) request.getAttribute("Added");
                        if (added != null) { %>
                    <tr>
                        <td><font color="red"> <bean:message
                                key="oscarEncounter.oscarConsultationRequest.config.AddSpecialist.msgSpecialistAdded"
                                arg0="<%=added%>"/> </font></td>
                    </tr>
                    <%}%>
                    <%
                        DynacareCopyToProvider copyToProvider = (DynacareCopyToProvider) request
                                .getAttribute("copyToProvider");
                        if (copyToProvider == null) {
                            copyToProvider = new DynacareCopyToProvider();
                        }
                    %>
                    <tr>
                        <td>
                            <html:form action="/eform/dynacare/AddCopyToProvider">
                                <%
                                    AddCopyToProviderForm thisForm;
                                    thisForm = (AddCopyToProviderForm) request
                                            .getAttribute("AddCopyToProviderForm");
                                    thisForm.setFirstName(
                                            (String) request.getAttribute("firstName"));
                                    thisForm.setLastName((String) request.getAttribute("lastName"));
                                    thisForm.setAddress1((String) request.getAttribute("address1"));
                                    thisForm.setCity((String) request.getAttribute("city"));
                                    thisForm.setStateOrProvince(
                                            (String) request.getAttribute("province"));
                                    thisForm.setCountry((String) request.getAttribute("country"));
                                    thisForm.setZipOrPostal(
                                            (String) request.getAttribute("postalCode"));
                                    thisForm.setType(DynacareCopyToProvider.class.getSimpleName());
                                    thisForm.setId(request.getAttribute("id") != null ? Integer
                                            .parseInt((String) request.getAttribute("id")) : null);
                                    thisForm.setCpso((String) request.getAttribute("cpso"));
                                    thisForm.setBillingNumber((String) request.getAttribute("billingNumber"));
                                %>
                                <table>
                                    <html:hidden name="AddCopyToProviderForm" property="id"
                                                 value="<%= copyToProvider.getId() != null? Integer.toString(copyToProvider.getId()) : \"\" %>"/>
                                    <html:hidden name="AddCopyToProviderForm" property="type"
                                                 value="<%= DynacareCopyToProvider.class.getSimpleName() %>"/>
                                    <tr>
                                        <td>
                                            <bean:message
                                                key="oscarEncounter.oscarConsultationRequest.config.AddSpecialist.firstName"/>
                                        </td>
                                        <td>
                                            <html:text name="AddCopyToProviderForm"
                                                       property="firstName"
                                                       value="<%= copyToProvider.getFirstName() %>"/>
                                        </td>
                                        <td>
                                            <bean:message
                                                key="oscarEncounter.oscarConsultationRequest.config.AddSpecialist.lastName"/>
                                        </td>
                                        <td>
                                            <html:text name="AddCopyToProviderForm"
                                                       property="lastName"
                                                       value="<%= copyToProvider.getLastName() %>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>CPSO</td>
                                        <td>
                                            <html:text name="AddCopyToProviderForm"
                                               property="cpso"
                                               value="<%= copyToProvider.getCpso() %>"/>
                                        </td>
                                        <td>Billing Number</td>
                                        <td>
                                            <html:text name="AddCopyToProviderForm"
                                               property="billingNumber"
                                               value="<%= copyToProvider.getBillingNumber() %>" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Address</td>
                                        <td>
                                            <html:text name="AddCopyToProviderForm"
                                                       property="address1"
                                                       value="<%= copyToProvider.getAddress1() %>"/>
                                        </td>
                                        <td>City</td>
                                        <td>
                                            <html:text name="AddCopyToProviderForm" property="city"
                                                       value="<%= copyToProvider.getCity() %>"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Province</td>
                                        <td>
                                            <html:select name="AddCopyToProviderForm"
                                                         property="stateOrProvince">
                                                <%
                                                    Map<String, String> provinces = ProvinceNames.getCanadianProvinces();
                                                    for (Map.Entry<String, String> entry : provinces.entrySet()) {
                                                        String shortName = entry.getKey();
                                                        String longName = entry.getValue();
                                                        String selected = "";
                                                        if (shortName.equals(copyToProvider.getStateOrProvince())) {
                                                            selected = "selected";
                                                        }
                                                %>
                                                <option value="<%=shortName%>" <%= selected %>>
                                                    <%=shortName%>-<%=longName%>
                                                </option>
                                                <% } %>
                                            </html:select>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Postal Code</td>
                                        <td>
                                            <html:text name="AddCopyToProviderForm"
                                                       property="zipOrPostal"
                                                       value="<%= copyToProvider.getZipOrPostal() %>"/></td>
                                        <td>
                                            <input type="hidden" name="country"
                                                   value="<%= copyToProvider.getCountry() %>"/>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <input type="submit" name="Submit"/>
                                        </td>
                                    </tr>
                                </table>
                            </html:form>
                        </td>
                    </tr>
                    <tr height="100%">
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="mainTableBottomRowRightColumn"></td>
        </tr>
    </table>
    </body>
</html:html>
