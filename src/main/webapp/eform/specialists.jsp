<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%--  

This Page creates the fax form for eforms.
 
--%>
<%@ page import="java.sql.*, java.util.ArrayList, oscar.eform.data.*, oscar.SxmlMisc, org.oscarehr.common.model.Demographic, oscar.oscarDemographic.data.DemographicData,oscar.OscarProperties,org.springframework.web.context.support.WebApplicationContextUtils, org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.common.model.*,org.oscarehr.common.dao.*"%>
<%@page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="displayServiceUtil" scope="request" class="oscar.oscarEncounter.oscarConsultationRequest.config.pageUtil.EctConDisplayServiceUtil" />
<%

	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);

	boolean eFormFaxEnabled = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("eform_fax_enabled", false);

	if (eFormFaxEnabled) {
		
		displayServiceUtil.estSpecialist();		
	  
%> 
<table width="100%">
<tr>

	<td class="tite4" width="10%">  Providers: </td>
	<td class="tite3" width="20%">
		<select id="otherFaxSelect">
		<%
		for (int i=0;i < displayServiceUtil.specIdVec.size(); i++) {
                             String  fName      = (String) displayServiceUtil.fNameVec.elementAt(i);
                             String  lName      = (String) displayServiceUtil.lNameVec.elementAt(i);
                             String  address    = (String) displayServiceUtil.addressVec.elementAt(i);
                             String  fax        = (String) displayServiceUtil.faxVec.elementAt(i);
			if (!"".equals(fax)) {
			%>
			<option value="<%=Encode.forHtmlAttribute(address  + " " + fax)%>"> <%=Encode.forHtmlContent(String.format("%s, %s", lName, fName))%> </option>
			<%						
			}
		} %>		                        
		</select>
	</td>
	<td class="tite3">				
		<button onclick="AddOtherFaxProvider(); return false;">Add Provider</button>
	</td>
</tr>
</table>
<% } // end if (props.isRichEFormFaxEnabled()) { %>
