<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.EFormValueDao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.EFormValue" %>

<%
  EFormValueDao eformValueDao = SpringUtils.getBean(EFormValueDao.class);
  String attachmentWidgetJson = "";
  if (request.getParameter("fdid") != null && !request.getParameter("fdid").equalsIgnoreCase("null")) {
    EFormValue attachmentsData = eformValueDao.findByFormDataIdAndKey(Integer.parseInt(
            request.getParameter("fdid")), "attachment-control-printables");
    attachmentWidgetJson = attachmentsData != null ? attachmentsData.getVarValue() : "";
  }
%>
  <input type="hidden" id="demographicNo" value="<%=request.getParameter("demographicNo")%>"/>
  <input type="hidden" id="providerNo" value="<%=(String) request.getSession().getAttribute("user")%>"/>
  <input type="hidden" id="proContextPath"
         value="<%=OscarProperties.getKaiemrDeployedContext()%>">
  <div id="attachmentManagerWidget" style="padding: 0px 100px 10px;">
    <%@ include file="/well/feature/attachment-manager/attachmentManagerWidget.jspf" %>
  </div>