<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@page import="oscar.OscarProperties"%>
<%@page import="java.sql.*, oscar.eform.data.*"%>
<%@page import="org.oscarehr.common.model.SMTPConfig" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.ConfigureConstants" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Questionnaire" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Question" %>
<%@page import="oscar.log.LogAction" %>
<%@page import="org.oscarehr.managers.EmailManager" %>
<%@page import="org.oscarehr.util.LoggedInInfo" %>
<%@page import="oscar.log.LogConst" %>
<%@page import="oscar.eform.EFormCsrfUtil" %>
<%@page import="org.oscarehr.util.MiscUtils" %>
<%@page import="org.oscarehr.util.SpringUtils"%>
<%@page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@page import="org.oscarehr.common.model.SystemPreferences" %>
<%@page import="java.util.HashMap" %>
<%@page import="java.util.List" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="java.util.Calendar" %>
<%@ page import="org.oscarehr.common.dao.DynacareEorderDao" %>
<%@ page import="org.oscarehr.common.model.DynacareEorder" %>
<%@ page import="org.oscarehr.common.model.EOrder.OrderState" %>
<%@ page import="net.sourceforge.barbecue.BarcodeException" %>
<%@ page import="java.io.IOException" %>
<%@ page import="org.oscarehr.integration.dynacare.eorder.DynacareOrderGateway" %>
<%@ page import="org.oscarehr.integration.dynacare.eorder.DynacareOrderGatewayImpl" %>
<%@ page import="com.dynacare.api.Order" %>
<%@ page import="org.apache.commons.text.WordUtils" %>
<%@ page
		import="static org.oscarehr.integration.dynacare.eorder.DynacareOrderStatusHelper.convertStatusToString" %>
<%@ page import="org.oscarehr.common.dao.EFormDataDao" %>
<%@ page import="org.oscarehr.common.model.EFormData" %>
<%@ page
		import="static org.oscarehr.integration.dynacare.eorder.DynacareOrderStatusHelper.getDescription" %>
<%@ page import="org.omg.SendingContext.RunTime" %>
<%@ page import="org.oscarehr.integration.dynacare.eorder.DynacareOrderStatusHelper" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ page import="org.oscarehr.common.dao.EFormDao" %>
<%
    String questionnaireString = "";
    if (request.getAttribute("questionnaire") != null) {
  	    Questionnaire questionnaire = (Questionnaire) request.getAttribute("questionnaire");
  	
  	    questionnaireString =  "<input type='hidden' id='M_existingFdid' name='M_existingFdid' value='" + request.getAttribute("M_existingFdid") + "'></input>";
   
  	    int seq = 1;
  	    for (Question question : questionnaire.getQuestions()) {	
  		    questionnaireString += "<input class='qAnswer' type='hidden' id='" + "QR_q" + seq + "' name='" + "QR_q" + seq + "' value=''>";
  		    seq++;
  	    }
    }
  	  	
    MiscUtils.getLogger().info("vb_stat : " + request.getAttribute("vb_stat"));
  	String printButtonEnabled = "true";
  	if (request.getAttribute("vb_stat") != null) {
	  	if (request.getAttribute("vb_stat").equals("true")) {
		  	printButtonEnabled = "false";
	  	}
  	}

  	MiscUtils.getLogger().info("disable_order_button : " + request.getAttribute("disable_order_button"));
    String orderButtonEnabled = "true";
    if (Boolean.parseBoolean((String)request.getAttribute("disable_order_button"))) {
      orderButtonEnabled = "false";
    }

    EmailManager emailManager = SpringUtils.getBean(EmailManager.class);
    SMTPConfig smtpConfig = emailManager.getEmailConfig(LoggedInInfo.getLoggedInInfoFromSession(request));
    String enableEmailNotification = ((smtpConfig != null) && smtpConfig.getEnableEmail()) ? "true" : "false";
    MiscUtils.getLogger().info("enable_email_notification : " + enableEmailNotification);

    int pasteFaxNote = 0;
    HashMap<String, Boolean> echartPreferencesMap = new HashMap<String, Boolean>();
    List<SystemPreferences> schedulePreferences = SystemPreferencesUtils.findPreferencesByNames(SystemPreferences.ECHART_PREFERENCE_KEYS);
    DynacareEorderDao dynacareEorderDao = SpringUtils.getBean(DynacareEorderDao.class);
    EFormDataDao eFormDataDao = SpringUtils.getBean(EFormDataDao.class);
    EFormDao eFormDao = SpringUtils.getBean(EFormDao.class);
    for (SystemPreferences preference : schedulePreferences) {
	    if(preference.getName().equals("echart_paste_fax_note")) {
	    	if(preference.getValueAsBoolean()) {
	    		pasteFaxNote = 1;
	    	} else {
	    		pasteFaxNote = 0;
	    	}
	    }
    }
    String timeStamp = new SimpleDateFormat("dd-MMM-yyyy hh:mm a").format(Calendar.getInstance().getTime());
 
    String provider_no = (String) session.getValue("user");
    String eform_data_id = (String) request.getParameter("fdid");
    boolean isAttachmentsEnabled;

	String id = request.getParameter("fid");
	String messageOnFailure = "No eform or appointment is available";
    String formName = "";
    if (id == null) {  // form exists in patient
        id = request.getParameter("fdid");

        Integer fid = eFormDataDao.getFormIdByFormDataId(Integer.parseInt(id));
        isAttachmentsEnabled = eFormDao.isAttachmentsEnabled(fid);

        String appointmentNo = request.getParameter("appointment");
        String eformLink = request.getParameter("eform_link");
        request.setAttribute("submit", "false");

        EForm eForm = new EForm(id);
        DynacareEorder dynacareEorder = dynacareEorderDao.findByExtEFormDataId(
            Integer.parseInt(id));
        if (dynacareEorder != null && dynacareEorder.getState() == OrderState.SUBMITTED) {
					DynacareOrderGateway orderGateway = SpringUtils.getBean(DynacareOrderGatewayImpl.class);
          try {
            Order eorder = orderGateway.getASingleOrder(dynacareEorder.getExternalOrderId());
            DynacareOrderStatusHelper.setStatus(request, eorder.getStatus());
          } catch (RuntimeException e) {
            DynacareOrderStatusHelper.setStatusError(request, e.getMessage());
          }
          request.setAttribute("ext_order_id", dynacareEorder.getExternalOrderId());
          request.setAttribute("barcode", dynacareEorder.getBarcode());
          request.setAttribute("existingFdid", dynacareEorder.getEFormDataId());
          request.setAttribute("submit", "true");
          try {
            request.setAttribute("barcode_img", dynacareEorder.generateBarcodeImage());
          } catch (BarcodeException e) {
            MiscUtils.getLogger().error("An error occurred when generating a barcode image: ", e);
          } catch (IOException e) {
            MiscUtils.getLogger().error("An error occurred when generating a barcode image: ", e);
          }
        }
        eForm.setContextPath(request.getContextPath());
        eForm.setOscarOPEN(request.getRequestURI());
        if ( appointmentNo != null ) eForm.setAppointmentNo(appointmentNo);
        if ( eformLink != null ) eForm.setEformLink(eformLink);

        String parentAjaxId = request.getParameter("parentAjaxId");
        if( parentAjaxId != null ) eForm.setAction(parentAjaxId);
        formName = eForm.getFormName();
	    String logData = "fdid=" + request.getParameter("fdid") + "\nFormName=" + formName;
	    if (request.getParameter("appointment") != null) { logData += "\nappointment_no=" + request.getParameter("appointment"); }
	    LogAction.addLog(LoggedInInfo.getLoggedInInfoFromSession(request), LogConst.READ, "eForm",
			  request.getParameter("fdid"), eForm.getDemographicNo(), logData);
	    String htmlDocument = eForm.getFormHtml();	  
	    htmlDocument = EFormCsrfUtil.addCsrfScriptTagToHtml(htmlDocument, request.getContextPath());
	  
	    // Pass additional data into the eForm - add here as needed
        htmlDocument = htmlDocument.replace("${eo_provider_no}", (provider_no != null ? provider_no : ""));
        htmlDocument = htmlDocument.replace("${eo_eform_data_id}", (eform_data_id != null ? eform_data_id : ""));
        htmlDocument = htmlDocument.replace("${questionnaire}", "");
        htmlDocument = htmlDocument.replace("${isNewQuestionSet}", "");
        htmlDocument = htmlDocument.replace("${existingFdid}", request.getParameter("fdid"));
        htmlDocument = htmlDocument.replace("${existingFdidTag}", "");
        htmlDocument = htmlDocument.replace("${print_button_enabled}", printButtonEnabled);
        htmlDocument = htmlDocument.replace("${order_button_enabled}", orderButtonEnabled);
        htmlDocument = htmlDocument.replace("${enable_email_notification}", enableEmailNotification);
        String dyncareOrderId = "";

        if(request.getAttribute("ext_order_id") != null) {
            dyncareOrderId = "Dynacare order ID: " + (String) request.getAttribute("ext_order_id");
        } else if (request.getParameter("ext_order_id") != null){
            dyncareOrderId = "Dynacare order ID: " + (String) request.getParameter("ext_order_id");
        }

        htmlDocument = htmlDocument.replace("${extOrderId}", dyncareOrderId);

        String barcode = "";
        if(request.getAttribute("barcode") != null) {
    	    barcode = "<img src='data:image/png;base64," + (String) request.getAttribute("barcode_img") + "' />";
        } else if (request.getParameter("barcode") != null){
    	      barcode = "<img src='data:image/png;base64," + (String) request.getParameter("barcode_img") + "' />";
        }
        htmlDocument = htmlDocument.replace("${barcode}", barcode);

        if (request.getAttribute("submit") != null) {
          htmlDocument = htmlDocument.replace("${form_is_submitted}", (String) request.getAttribute("submit"));
        } else {
          htmlDocument = htmlDocument.replace("${form_is_submitted}", "false");
        }

        if (request.getAttribute("status") != null) {
          htmlDocument = htmlDocument.replace("${eorder_status}", (String) request.getAttribute("status"));
        }

        if (request.getAttribute("status_description") != null) {
          htmlDocument = htmlDocument.replace("${status_description}", (String) request.getAttribute("status_description"));
        }

	    out.print(htmlDocument);
	    String providerName = "";
	    if(null != provider_no){
			ProviderDao proDao = SpringUtils.getBean(ProviderDao.class); 
			providerName = proDao.getProviderName(provider_no);
	    }
	    String setDocName = "<script type=\"text/javascript\"> var setDocName='" + providerName + "';</script>";
	    out.print(setDocName);
	    String setEformName = "<script type=\"text/javascript\"> var setEformName='" + formName + "';</script>";
	    out.print(setEformName);
	  
	    String currentTimeStamp = "<script type=\"text/javascript\"> var currentTimeStamp='" + timeStamp + "';</script>";
	    out.print(currentTimeStamp);

      String pasteFaxNoteStr = "<script type=\"text/javascript\"> var pasteFaxNote='" + String.valueOf(pasteFaxNote) + "';</script>";
      out.print(pasteFaxNoteStr);

    } else {  //if form is viewed from admin screen
        EForm eForm = new EForm(id, "-1"); //form cannot be submitted, demographic_no "-1" indicate this specialty

        isAttachmentsEnabled = eForm.isAttachmentsEnabled();

        eForm.setContextPath(request.getContextPath());
        eForm.setupInputFields();
        eForm.setOscarOPEN(request.getRequestURI());
        eForm.setImagePath();
        String logData = "fdid=" + request.getParameter("fdid") + "\nid=" + id;
        if (request.getParameter("appointment") != null) { logData += "\nappointment_no=" + request.getParameter("appointment"); }
        LogAction.addLog(LoggedInInfo.getLoggedInInfoFromSession(request), LogConst.READ, "eForm",
                request.getParameter("fdid"), eForm.getDemographicNo(), logData);
        String htmlDocument = eForm.getFormHtml();
        htmlDocument = EFormCsrfUtil.addCsrfScriptTagToHtml(htmlDocument, request.getContextPath());

        // Pass additional data into the eForm - add here as needed
        htmlDocument = htmlDocument.replace("${eo_provider_no}", (provider_no != null ? provider_no : ""));
        htmlDocument = htmlDocument.replace("${eo_eform_data_id}", (eform_data_id != null ? eform_data_id : ""));
        htmlDocument = htmlDocument.replace("${questionnaire}", questionnaireString);
        String isNewQuestionSet = (String) request.getAttribute("is_new_question_set") != null ? (String) request.getAttribute("is_new_question_set") : "";
        htmlDocument = htmlDocument.replace("${isNewQuestionSet}", isNewQuestionSet);
        String existingFdid = (String) request.getAttribute("M_existingFdid") != null ? (String) request.getAttribute("M_existingFdid") : "";
        htmlDocument = htmlDocument.replace("${existingFdid}", existingFdid);
        String dyncareOrderId = (String) request.getAttribute("ext_order_id") != null ? "Dynacare order ID: " + (String) request.getAttribute("ext_order_id") : "";
        htmlDocument = htmlDocument.replace("${extOrderId}", dyncareOrderId);
        String barcode = (String) request.getAttribute("barcode_img") != null ? "<img src='data:image/png;base64," + (String) request.getAttribute("barcode_img") + "' />" : "";
        htmlDocument = htmlDocument.replace("${barcode}", barcode);
        htmlDocument = htmlDocument.replace("${existingFdidTag}", "<input type='hidden' id='M_existingFdid' name='M_existingFdid' value='" + existingFdid + "'></input>");
        htmlDocument = htmlDocument.replace("${print_button_enabled}", printButtonEnabled);
        htmlDocument = htmlDocument.replace("${order_button_enabled}", orderButtonEnabled);
        htmlDocument = htmlDocument.replace("${enable_email_notification}", enableEmailNotification);
        out.print(htmlDocument);
    }

    boolean attachmentManagerEFormEnabled = SystemPreferencesUtils
            .isReadBooleanPreferenceWithDefault("attachment_manager.eform.enabled", false);
    SystemPreferences excludedEformNames = SystemPreferencesUtils
            .findPreferenceByName("attachment_manager.eform.excludes");
    boolean isEformExcluded = false;
    if (excludedEformNames != null) {
        for (String name : excludedEformNames.getValue().split(",")) {
            if (formName.contains(name.trim())) {
                isEformExcluded = true;
                break;
            }
        }
    }

    if (isAttachmentsEnabled && attachmentManagerEFormEnabled && !isEformExcluded) { %>
<script type="text/javascript"
        src="<%=request.getContextPath() %>/share/javascript/eforms/attachmentManagerControl.js"></script>
<%
    }
    String iframeResize = (String) session.getAttribute("useIframeResizing");
    if (iframeResize !=null && "true".equalsIgnoreCase(iframeResize)) { %>
<script src="<%=request.getContextPath() %>/library/pym.js"></script>
<script>
        var pymChild = new pym.Child({ polling: 500 });
</script>
<%  }%>