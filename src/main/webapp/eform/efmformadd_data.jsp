<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@page import="oscar.OscarProperties"%>
<%@page import="oscar.util.*, oscar.eform.data.*"%>
<%@page import="org.oscarehr.common.model.SMTPConfig" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.ConfigureConstants" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Questionnaire" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Question" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.QuestionType" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Choice" %>
<%@page import="org.oscarehr.managers.EmailManager" %>
<%@page import="org.oscarehr.util.LoggedInInfo" %>
<%@page import="org.oscarehr.util.MiscUtils" %>
<%@page import="org.oscarehr.util.SpringUtils"%>
<%@page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@page import="org.oscarehr.common.model.SystemPreferences" %>
<%@page import="java.util.*"%>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="oscar.eform.EFormCsrfUtil" %>
<%@ page import="sun.java2d.pipe.SpanShapeRenderer.Simple" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%
String eOrderType = request.getParameter("M_ExcellerisEOrderType");
MiscUtils.getLogger().info("eOrderType : " + eOrderType);

if (request.getAttribute("page_errors") != null) {
%>

<script language=javascript type='text/javascript'>
function hideDiv() {
    if (document.getElementById) { // DOM3 = IE5, NS6
        document.getElementById('hideshow').style.display = 'none';
    }
    else {
        if (document.layers) { // Netscape 4
            document.hideshow.display = 'none';
        }
        else { // IE 4
            document.all.hideshow.style.display = 'none';
        }
    }
}
</script>

<div id="hideshow" style="position: relative; z-index: 999; width: 706px; margin-left: 22px;">
	<a href="javascript:hideDiv()">Hide Errors</a> 
	<div style="background-color: #ecc8c5; padding: 0 10px 0 10px; border: 1px solid red; border-radius: 6px; margin-top: 10px;">
		<font style="font-size: 10; font-color: darkred;">
			<html:errors />
		</font>
	</div>
</div>

<% } %>

<%
if (request.getAttribute("page_messages") != null) {
%>

<script language=javascript type='text/javascript'>
function hideDivMsg() {
    if (document.getElementById) { // DOM3 = IE5, NS6
        document.getElementById('hideshowMsg').style.display = 'none';
    }
    else {
        if (document.layers) { // Netscape 4
            document.hideshowMsg.display = 'none';
        }
        else { // IE 4
            document.all.hideshowMsg.style.display = 'none';
        }
    }
}
</script>

<div id="hideshowMsg" style="position: relative; z-index: 999; width: 706px; margin-left: 22px;"><a
	href="javascript:hideDivMsg()">Hide Messages</a>

	<logic:messagesPresent message="true" property="Success messages">
		<div style="background-color: #def2d6; padding: 0 10px 0 10px; border: 1px solid #b0cca5; border-radius: 6px; margin-top: 10px;">
			<html:messages id="message" message="true" property="Success messages">
			    <h3 style="margin-top: 10px; margin-bottom: 10px;">
					<font color="#597151" style="font-size: 12;">
						<bean:write name="message" filter="false"/>
					</font>
				</h3>
			</html:messages>		
		</div>
	</logic:messagesPresent>

	<logic:messagesPresent message="true" property="Error messages">
        <div style="background-color: #ecc8c5; padding: 0 10px 0 10px; border: 1px solid red; border-radius: 6px; margin-top: 10px;">
            <html:messages id="message" message="true" property="Error messages">
                <h3 style="margin-top: 10px; margin-bottom: 10px;">
                    <font color="darkred" style="font-size: 12;">
                        <bean:write name="message" filter="false"/>
                    </font>
                </h3>
            </html:messages>
        </div>
    </logic:messagesPresent>

	<logic:messagesPresent message="true" property="Informational messages">
        <div style="background-color: #cde8f5; padding: 0 10px 0 10px; border: 1px solid #7ebddf; border-radius: 6px; margin-top: 10px;">
		    <html:messages id="message" message="true" property="Informational messages">
			    <h3 style="margin-top: 10px; margin-bottom: 10px;">
				    <font color="#3872a0" style="font-size: 12;">
					    <bean:write name="message" filter="false"/>
					</font>
				</h3>
		    </html:messages>
		</div>		
	</logic:messagesPresent>

</div>

<% } %>

<%
String questionnaireString = "";
  if (request.getAttribute("questionnaire") != null) {
  	Questionnaire questionnaire = (Questionnaire) request.getAttribute("questionnaire");
  	
  	questionnaireString =  "<input type='hidden' id='M_existingFdid' name='M_existingFdid' value='" + request.getAttribute("M_existingFdid") + "'></input>";
   
  	int seq = 1;
  	for (Question question : questionnaire.getQuestions()) {
  	    String requiredString = (question.getRequired() != null) ? question.getRequired().toString() : "true";
  		questionnaireString += "<input class='qAnswer' type='hidden' id='" + "QR_q" + seq + "' name='" + "QR_q" + seq + "' value='' data-required='" + requiredString + "'>";
  		seq++;
  	}
  } 
  
  MiscUtils.getLogger().info("vb_stat : " + request.getAttribute("vb_stat")); 
  String printButtonEnabled = "true";
  if (request.getAttribute("vb_stat") != null) {
	  if (request.getAttribute("vb_stat").equals("true")) {
		  printButtonEnabled = "false";
	  }
  }
  MiscUtils.getLogger().info("disable_order_button : " + request.getAttribute("disable_order_button"));
  String orderButtonEnabled = "true";
  if (Boolean.parseBoolean((String)request.getAttribute("disable_order_button"))) {
    orderButtonEnabled = "false";
  }

  EmailManager emailManager = SpringUtils.getBean(EmailManager.class);
  SMTPConfig smtpConfig = emailManager.getEmailConfig(LoggedInInfo.getLoggedInInfoFromSession(request));
  String enableEmailNotification = ((smtpConfig != null) && smtpConfig.getEnableEmail()) ? "true" : "false";
  MiscUtils.getLogger().info("enable_email_notification : " + enableEmailNotification);
%>

<%
  String provider_no = (String) session.getValue("user");
  String demographic_no = request.getParameter("demographic_no");
  String appointment_no = request.getParameter("appointment");
  String fid = request.getParameter("fid");
  String eform_link = request.getParameter("eform_link");
  String source = request.getParameter("source");
  String eform_data_id = (String) request.getParameter("fdid");
  boolean submit = Boolean.parseBoolean(request.getParameter("submit"));
  if (StringUtils.empty(eform_data_id)) {
    eform_data_id = (String)request.getAttribute("fdid");
  }

  EForm thisEForm = null;
  if (fid == null || demographic_no == null) {
      //if the info is in the request attribute
      thisEForm = (EForm) request.getAttribute("curform");
  } else {
      //if the info is in the request parameter
      thisEForm = new EForm(fid, demographic_no);
      thisEForm.setProviderNo(provider_no);  //needs provider for the action
  }

  int pasteFaxNote = 0;
  HashMap<String, Boolean> echartPreferencesMap = new HashMap<String, Boolean>();
  List<SystemPreferences> schedulePreferences = SystemPreferencesUtils.findPreferencesByNames(SystemPreferences.ECHART_PREFERENCE_KEYS);
  for (SystemPreferences preference : schedulePreferences) {
      if(preference.getName().equals("echart_paste_fax_note")) {
        if(preference.getValueAsBoolean()) {
          pasteFaxNote = 1;
        } else {
          pasteFaxNote = 0;
        }
      }
  }

  String timeStamp = new SimpleDateFormat("dd-MMM-yyyy hh:mm a").format(Calendar.getInstance().getTime());
  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
  ArrayList<String> nullFields = null;
  String formName = "";

  boolean isAttachmentsEnabled = true;

  if (thisEForm != null) {
    isAttachmentsEnabled = thisEForm.isAttachmentsEnabled();
      if (appointment_no != null) thisEForm.setAppointmentNo(appointment_no);
      if (eform_link != null) thisEForm.setEformLink(eform_link);
      thisEForm.setContextPath(request.getContextPath());
      thisEForm.setupInputFields();
      thisEForm.setImagePath();
      thisEForm.setDatabaseAPs(submit);
      thisEForm.setOscarOPEN(request.getRequestURI());
      thisEForm.setAction();
      thisEForm.setSource(source);
      String htmlDocument = thisEForm.getFormHtml();
      htmlDocument = EFormCsrfUtil.addCsrfScriptTagToHtml(htmlDocument, request.getContextPath());
      nullFields = thisEForm.getNullFields();

      // Pass additional data into the eForm - add here as needed
      htmlDocument = htmlDocument.replace("${eo_provider_no}", (provider_no != null ? provider_no : ""));
      htmlDocument = htmlDocument.replace("${eo_eform_data_id}", (eform_data_id != null ? eform_data_id : ""));
      htmlDocument = htmlDocument.replace("${questionnaire}", questionnaireString);
      String isNewQuestionSet = (String) request.getAttribute("is_new_question_set") != null ? (String) request.getAttribute("is_new_question_set") : "";
      htmlDocument = htmlDocument.replace("${isNewQuestionSet}", isNewQuestionSet);
      String existingFdid = (String) request.getAttribute("M_existingFdid") != null ? (String) request.getAttribute("M_existingFdid") : "";
      htmlDocument = htmlDocument.replace("${existingFdid}", existingFdid);
      htmlDocument = htmlDocument.replace("${existingFdidTag}", "<input type='hidden' id='M_existingFdid' name='M_existingFdid' value='" + existingFdid + "'></input>");
      htmlDocument = htmlDocument.replace("${print_button_enabled}", printButtonEnabled);
      htmlDocument = htmlDocument.replace("${order_button_enabled}", orderButtonEnabled);
      htmlDocument = htmlDocument.replace("${enable_email_notification}", enableEmailNotification);
      String dyncareOrderId = (String) request.getAttribute("ext_order_id") != null ? "Dynacare order ID: " + (String) request.getAttribute("ext_order_id") : "";
      htmlDocument = htmlDocument.replace("${extOrderId}", dyncareOrderId);
      String barcode = (String) request.getAttribute("barcode_img") != null ? "<img src='data:image/png;base64," + (String) request.getAttribute("barcode_img") + "' />" : "";
      htmlDocument = htmlDocument.replace("${barcode}", barcode);
      htmlDocument = htmlDocument.replace("${form_is_submitted}", String.valueOf(submit && request.getAttribute("page_errors") == null));
      String status = (String) request.getAttribute("status") != null ? (String) request.getAttribute("status") : "${eorder_status}";
      htmlDocument = htmlDocument.replace("${eorder_status}", status);
      String statusDescription = (String) request.getAttribute("status_description") != null ? (String) request.getAttribute("status_description") : "${status_description}";
      htmlDocument = htmlDocument.replace("${status_description}", statusDescription);
      String lastUpdateDate = request.getAttribute("last_update_date") != null
					? dateFormat.format((Date) request.getAttribute("last_update_date"))
					: "${last_update_date}";
      htmlDocument = htmlDocument.replace("${last_update_date}", lastUpdateDate);
      out.print(htmlDocument);
      String providerName = "";
  	  if(null != provider_no){
  			ProviderDao proDao = SpringUtils.getBean(ProviderDao.class); 
  			providerName = proDao.getProviderName(provider_no);
  	  }
  	  String setDocName = "<script type=\"text/javascript\"> var setDocName='" + providerName + "';</script>";
  	  out.print(setDocName);
      formName = thisEForm.getFormName();
  	  String setEformName = "<script type=\"text/javascript\"> var setEformName='" + formName + "';</script>";
  	  out.print(setEformName);
  	  
  	  String currentTimeStamp = "<script type=\"text/javascript\"> var currentTimeStamp='" + timeStamp + "';</script>";
	  out.print(currentTimeStamp);
	  
  	  String pasteFaxNoteStr = "<script type=\"text/javascript\"> var pasteFaxNote='" + String.valueOf(pasteFaxNote) + "';</script>";
 	  out.print(pasteFaxNoteStr);
 	  
  }
    boolean attachmentManagerEFormEnabled = SystemPreferencesUtils
            .isReadBooleanPreferenceWithDefault("attachment_manager.eform.enabled", false);
    SystemPreferences excludedEformNames = SystemPreferencesUtils
            .findPreferenceByName("attachment_manager.eform.excludes");
    boolean isEformExcluded = false;
    if (excludedEformNames != null) {
        for (String name : excludedEformNames.getValue().split(",")) {
            if (formName.contains(name.trim())) {
                isEformExcluded = true;
                break;
            }
        }
    }

    if (isAttachmentsEnabled && attachmentManagerEFormEnabled && !isEformExcluded) { %>
<script type="text/javascript"
        src="<%=request.getContextPath() %>/share/javascript/eforms/attachmentManagerControl.js"></script>
<%
    }
String iframeResize = (String) session.getAttribute("useIframeResizing");
if(iframeResize !=null && "true".equalsIgnoreCase(iframeResize)){ %>
<script src="<%=request.getContextPath() %>/library/pym.js"></script>
<script>
    var pymChild = new pym.Child({ polling: 500 });
</script>
<%}%>

<% if (nullFields != null && nullFields.size() > 0) { %>
<script>
  alert("The following data failed to load: <%= StringUtils.join(nullFields, ",") %>. " +
      "If the issue persists, please contact support for assistance.");
</script>
<% } %>
