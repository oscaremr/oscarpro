<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
      String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
      boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_eform" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../../securityError.jsp?type=_con");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}
%>

<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@page
	import="java.util.ArrayList, oscar.dms.*, oscar.oscarLab.ca.on.*, oscar.util.StringUtils"%>
<%@page import="org.oscarehr.util.SessionConstants"%>
<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="org.oscarehr.hospitalReportManager.dao.*" %>
<%@page import="org.oscarehr.hospitalReportManager.*" %>
<%@page import="org.oscarehr.hospitalReportManager.model.*" %>
<%@page import="org.oscarehr.common.model.*" %>
<%@page import="org.oscarehr.common.dao.*" %>
<%@page import="oscar.util.ConversionUtils" %>

<%
	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
  String demo = request.getParameter("demo") ;
  String requestId = request.getParameter("requestId");
%>
<script type="text/javascript" src="../share/javascript/eforms/jspdf.debug.js"></script>
<script type="text/javascript" src="../share/javascript/eforms/html2canvas.js"></script>
<script type="text/javascript" src="../share/javascript/eforms/dom-to-image.js"></script>
<script type="text/javascript" src="../share/javascript/eforms/renderPDF.js"></script>
<ul id="attachedList"
	style="background-color: white; padding-left: 20px; list-style-position: outside; list-style-type: lower-roman;">
	<%
            List<EDoc> privatedocs = new ArrayList<EDoc>();
            privatedocs = EDocUtil.listEformDocs(loggedInInfo, demo, requestId, EDocUtil.ATTACHED);
            EDoc curDoc;     
            String selectDoc = "";
            for(int idx = 0; idx < privatedocs.size(); ++idx)
            {                    
                curDoc = (EDoc)privatedocs.get(idx);   
                String desc = StringUtils.maxLenString(curDoc.getDescription(),19,16,"...");
                if (desc == null || desc.trim().isEmpty()) {
                	desc = "D"+curDoc.getDocId();
                }
                desc += " " + curDoc.getObservationDate();
                selectDoc += "D" + curDoc.getDocId() + ",";
        %>
	<li class="doc"><%=desc%></li>
	<%                                           
            }

                CommonLabResultData labData = new CommonLabResultData();
                ArrayList labs = labData.populateLabEformData(loggedInInfo, demo, requestId, CommonLabResultData.ATTACHED);
                LabResultData resData;
                for(int idx = 0; idx < labs.size(); ++idx) 
                {
                    resData = (LabResultData)labs.get(idx);
                    if(resData.isCML()){
                    	selectDoc += "C" + resData.getSegmentID() + ",";
                    }else{
                    	selectDoc += "L" + resData.getLabPatientId() + ",";
                    }
        %>
	<li class="lab"><%=resData.getDiscipline()+" "+resData.getDateTime()%></li>
	<%
                }
        
        		HRMDocumentToDemographicDao hrmToDemoDao = (HRMDocumentToDemographicDao)SpringUtils.getBean("HRMDocumentToDemographicDao");
                HRMDocumentDao hrmDao = (HRMDocumentDao)SpringUtils.getBean("HRMDocumentDao");
                HRMDocumentSubClassDao hrmDocumentSubClassDao = (HRMDocumentSubClassDao) SpringUtils.getBean("HRMDocumentSubClassDao");
                List<HRMDocumentToDemographic> hList = hrmToDemoDao.findByDemographicNoAndEformID(demo, requestId, true);
                for (HRMDocumentToDemographic htd : hList) {
                	HRMDocument hrm = hrmDao.find(htd.getHrmDocumentId());
                	List<HRMDocumentSubClass> subClassList = hrmDocumentSubClassDao.getSubClassesByDocumentId(hrm.getId());
                	HRMReport report = HRMReportParser.parseReport(loggedInInfo, hrm.getReportFile());
    				HRMUtil hRMUtil = new HRMUtil();
    				hRMUtil.findCorrespondingHRMSubClassDescriptions(subClassList, hrm.getReportType(), report.getSendingFacilityId() , report.getFirstReportSubClass());
                	selectDoc += "H" + htd.getHrmDocumentId() + ",";
    				
                	String title = hrm.getReportType();
                	if (subClassList != null && subClassList.size() > 0 && subClassList.get(0) != null && subClassList.get(0).getSubClassDescription() != null && !subClassList.get(0).getSubClassDescription().isEmpty()) {
    					title = subClassList.get(0).getSubClassDescription();
    				}
                	String reportStatus = hrm.getReportStatus();
                	if (reportStatus != null && reportStatus.equalsIgnoreCase("C")) {
    					title = "(Cancelled) " + title;
    				}
                	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                	%>
   					<li class="hrm"><%=title+" "+formatter.format(hrm.getTimeReceived())%></li>
                	<% 
                }
                
                EFormDataDao efromDocs=(EFormDataDao)SpringUtils.getBean("EFormDataDao");
                int reqId=ConversionUtils.fromIntString(requestId);
                int demono=ConversionUtils.fromIntString(demo);
             	List<EFormData> eList = efromDocs.getSavedEfromDataByDemoId(demono,reqId);
             	SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
             	int indexcount=0;
                for (EFormData eform : eList) {
                	if(eform.getFormData().indexOf("id=\"root\"")!=-1){
						%>
						 <iframe id="eformiframe<%=indexcount%>" src="efmshowform_data.jsp?fdid=<%=eform.getId() %>&fid=<%=eform.getFormId() %>&demographic_no=<%=demo %>&parentAjaxId=eforms" width="0" height="0" style="border:0" ></iframe>
						<%
						indexcount++;
					}
                	 %>
                		<li class="eform"><%=formatter2.format(eform.getFormDate()) + " " + eform.getFormName()%></li>
                	<%
                }              
        %>
        <input type="text" name="selectDocs" value="<%= selectDoc%>" style="display:none" />
</ul>
<%
           if( privatedocs.size() == 0 && labs.size() == 0  && hList.size() == 0 && eList.size() == 0) {
        %>
<p id="attachDefault"
	style="background-color: white; text-align: center;"><bean:message
	key="oscarEncounter.oscarConsultationRequest.AttachDoc.Empty" /></p>
<%
           }
         %>
