<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
      String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
      boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_eform" rights="w" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../../securityError.jsp?type=_con");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}
%>


<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ page import="java.math.*, java.util.*, java.io.*, java.sql.*, oscar.*, oscar.util.*, java.net.*,oscar.MyDateFormat, oscar.dms.*, oscar.dms.data.*"%>
<%@ page import="oscar.oscarLab.ca.on.*"%>
<%@ page import="oscar.oscarLab.ca.all.Hl7textResultsData"%>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@ page import="org.oscarehr.util.SessionConstants"%>
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="org.oscarehr.hospitalReportManager.dao.*" %>
<%@page import="org.oscarehr.hospitalReportManager.*" %>
<%@page import="org.oscarehr.hospitalReportManager.model.*" %>
<%@page import="java.text.*" %>
<%@page import="org.oscarehr.util.*" %>
<%@page import="org.oscarehr.common.model.*" %>
<%@page import="org.oscarehr.common.dao.*" %>
<%@page import="oscar.util.ConversionUtils" %>
<%
	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
   		 
	String user_no = (String) session.getAttribute("user");
	String userfirstname = (String) session.getAttribute("userfirstname");
	String userlastname = (String) session.getAttribute("userlastname");

	// "Module" and "function" is the same thing (old dms module)
	String module = "demographic";
	String demoNo = request.getParameter("demo");
	String requestId = request.getParameter("requestId");
	String providerNo = loggedInInfo.getLoggedInProviderNo();

	if (demoNo == null && requestId == null)
		response.sendRedirect("../error.jsp");

/* 	if (demoNo == null || demoNo.equals("null")) {
		ConsultationAttachDocs docsUtil = new ConsultationAttachDocs(requestId);
		demoNo = docsUtil.getDemoNo();

	} */

	String patientName = EDocUtil.getDemographicName(loggedInInfo, demoNo);
	String[] docType = {"D", "L", "H", "E", "C"};
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html:html locale="true">

<head>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/global.js"></script>
<title><bean:message key="oscarEncounter.oscarConsultationRequest.AttachDocPopup.title" /></title>
<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/css/extractedFromPages.css" />
<style type="text/css">
.doc {
    color:blue;
}

.lab {
    color: #CC0099;
}

.hrm {
	color: red;
}
.eform {
	color: green;
}
</style>

<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
	//<!--
	function switchView() {
		var str="";
		var flag_lab=false;
		var flag_doc=false;
		var flag_hrm=false;
		var flage_eform=false;
		var flage_cml= false;
	    var obj = document.getElementsByName("documents")[0];
	    for(var i=0;i<obj.options.length;i++){
	        if(obj.options[i].selected){
	            str+=obj.options[i].value;
	            if(str.substring(0,1)=='D') {
	            	flag_doc=true;
	            } else if(str.substring(0,1)=='L'){
	            	flag_lab=true;
	            } else if (str.substring(0,1)=='H') {
	            	flag_hrm=true;
	            }else if (str.substring(0,1)=='E') {
	            	flage_eform=true;
	            }else if (str.substring(0,1)=='C') {
	            	flage_cml=true;
	            }
	        }
	    }
	    if(flag_doc) {		
			window.open("<%=request.getContextPath()%>/dms/ManageDocument.do?method=display&doc_no=" + str.replace("D",""), "newwindow","height=600,width=750,top=100,left=600,toolbar=no,menubar=no,scrollbars=yes, resizable=yes,location=no, status=no");				 		
	    } else if(flag_lab) {
			window.open("<%=request.getContextPath()%>/lab/CA/ALL/labDisplay.jsp?segmentID="+str.replace("L","")+"&demographicId=<%=demoNo%>&providerNo=<%=providerNo%>","newwindow","height=600,width=750,top=100,left=600,toolbar=no,menubar=no,scrollbars=yes, resizable=yes,location=no, status=no");	
		} else if (flag_hrm) {
			window.open("<%=request.getContextPath()%>/hospitalReportManager/Display.do?segmentID="+str.replace("H","")+"&duplicateLabIds=",
					'hrmPreviewWindow',
					"height=600,width=750,top=100,left=600,toolbar=no,menubar=no,scrollbars=yes, resizable=yes,location=no, status=no")
		}else if(flage_eform){
			var formid=$("select option:checked").attr("id");
			str=str.substring(1,str.length);
			popupPage('<%=request.getContextPath()%>'+'/eform/efmshowform_data.jsp?fdid='+str+'&demographic_no='+<%=demoNo%>+'&parentAjaxId=eforms'); 
		}else if(flage_cml){
			str = str.substring(1, str.length);
			window.open("<%=request.getContextPath()%>/lab/CA/ON/CMLDisplay.jsp?segmentID="+str+"&demographicId=<%=demoNo%>&searchProviderNo=<%=providerNo%>&providerNo=<%=providerNo%>","newwindow","height=600,width=750,top=100,left=600,toolbar=no,menubar=no,scrollbars=yes, resizable=yes,location=no, status=no");
		}
	}
	function popupPage(varpage, windowname) {
	    var page = "" + varpage;
	    windowprops = "height=700,width=800,location=no,"
	    + "scrollbars=yes,menubars=no,status=yes,toolbars=no,resizable=yes,top=10,left=200";
	    var popup = window.open(page, windowname, windowprops);
	    if (popup != null) {
	       if (popup.opener == null) {
	          popup.opener = self;
	       }
	       popup.focus();
	    }
	}
	function setEmpty(selectbox) {
		var emptyTxt = "<bean:message key="oscarEncounter.oscarConsultationRequest.AttachDocPopup.empty"/>";
		var emptyVal = "0";
		var op = document.createElement("option");
		try {
			selectbox.add(op);
		} catch (e) {
			selectbox.add(op, null);
		}
		selectbox.options[0].text = emptyTxt;
		selectbox.options[0].value = emptyVal;
	}

	function swap(srcName, dstName) {
		
		var srcElement = document.getElementsByName(srcName); 
		var src = srcElement[0];
		var dstElement = document.getElementsByName(dstName); 
		var dst = dstElement[0];
		var opt;

		//if nothing or dummy is being transfered do nothing
		if (src.selectedIndex == -1 || src.options[0].value == "0") {
			return;
		}

		//if dst has dummy clobber it with new options
		if (dst.options[0].value == "0") {
			dst.remove(0);
		}

		for ( var idx = src.options.length - 1; idx >= 0; --idx) {

			if (src.options[idx].selected) {
				opt = document.createElement("option");
				try { //ie method of adding option
					dst.add(opt);
					dst.options[dst.options.length - 1].id = src.options[idx].id;
					dst.options[dst.options.length - 1].text = src.options[idx].text;
					dst.options[dst.options.length - 1].value = src.options[idx].value;
					dst.options[dst.options.length - 1].className = src.options[idx].className;
					dst.options[dst.options.length-1].setAttribute("timeval", src.options[idx].getAttribute("timeval"));
					dst.options[dst.options.length-1].setAttribute("isFromEformPortal", src.options[idx].getAttribute("isFromEformPortal"));
					if(src.options[idx].getAttribute("openEformUrl") != null){
						dst.options[dst.options.length-1].setAttribute("openEformUrl", src.options[idx].getAttribute("openEformUrl"));
					}
					src.remove(idx);
				} catch (e) { //firefox method of adding option
					dst.add(src.options[idx], null);
					dst.options[dst.options.length - 1].selected = false;
				}

			}

		} //end for

		if (src.options.length == 0) {
			setEmpty(src);
		}
		
		if (dstName == "documents") {
	    	sortDocs();
	    }
	}

	//if consultation has not been saved, load existing docs into proper select boxes
	function init() {
		var attached = document.getElementsByName("attachedDocs")[0];
		var available = document.getElementsByName("documents")[0];

		if (document.forms[0].requestId.value == "null") {
			var docs = window.opener.document.RichTextLetter.documents.value.split("|");
			var opt;
			for ( var idx = 0; idx < docs.length; ++idx) {
				for ( var i = available.options.length - 1; i >= 0; --i) {
					if (docs[idx] == available.options[i].value) {
						opt = document.createElement("option");
						try { //ie method of adding option
							attached.add(opt);
							attached.options[attached.options.length - 1].text = available.options[i].text;
							attached.options[attached.options.length - 1].value = available.options[i].value;
							attached.options[attached.options.length - 1].className = available.options[i].className;
							dst.options[dst.options.length-1].setAttribute("isfromeformportal", src.options[idx].getAttribute("isfromeformportal"));
							if(src.options[idx].getAttribute("openeformurl") != null){
								dst.options[dst.options.length-1].setAttribute("openeformurl", src.options[idx].getAttribute("openeformurl"));
							}
							
							available.remove(i);
						} catch (e) { //firefox method of adding option
							attached.remove(attached.options.length-1);
							attached.add(available.options[i], null);
						}

						break;
					}

				} //end for

			} //end for
		} //end if

		if (attached.options.length == 0) {
			setEmpty(attached);
		}

		if (available.options.length == 0) {
			setEmpty(available);
		}

	}

	function save() {
		//var ret;
		var ops = document.getElementsByName("attachedDocs")[0];

		//if (document.forms[0].requestId.value == "null") {
			var saved = "";

			//we don't want to initially save dummy
			if (ops.options.length == 1 && ops.options[0].value == "0") {
				ops.options.length = 0;
			}

			var list = window.opener.document.getElementById("attachedList");
			var paragraph = window.opener.document.getElementById("attachDefault");

			//if we are saving something we need to update list on parent form
			if (ops.options.length>0) {
				if(paragraph != null){
					paragraph.innerHTML = "";
				}
			}

			//delete what we have before adding new docs to list
			while (list.firstChild) {
				list.removeChild(list.firstChild);
			}
			var fromEformPortalCount=0;
			for ( var idx = 0; idx < ops.options.length; ++idx) {
				saved += ops.options[idx].value;

				if (idx < ops.options.length - 1) {
					saved += "|";
				}

				listElem = window.opener.document.createElement("li");
				listElem.innerHTML = ops.options[idx].innerHTML;
				listElem.className = ops.options[idx].className;
				
				if($(ops.options[idx]).attr("isfromeformportal") == "true"){
					var iframeElem = "<iframe id='eformiframe" + fromEformPortalCount + "' src='" + $(ops.options[idx]).attr("openeformurl") + "' width='0' height='0' style='border:0' ></iframe>";
					list.innerHTML = list.innerHTML + iframeElem;
		        	   fromEformPortalCount ++;
				}
				
				list.appendChild(listElem);
			}

			window.opener.document.RichTextLetter.documents.value = saved;

			if (list.childNodes.length == 0) {
            	paragraph.innerHTML = "<bean:message key="oscarEncounter.oscarConsultationRequest.AttachDoc.Empty"/>";
			}

			//ret = false;
			window.close();
			return false;
/* 		} else {
			var saved = "";
						
			//but we will use dummy in updating an empty list
			for ( var idx = 0; idx < ops.options.length; ++idx) {
				ops.options[idx].selected = true;
				saved += ops.options[idx].value;
				
				if (idx < ops.options.length - 1) {
					saved += "|";
				}
						
			}
			window.opener.document.RichTextLetter.documents.value = saved;			
			
			window.opener.updateAttached();
	        ret = true;
		} */
		//return ret;
	}
//-->

function sortDocs() {
	var options = jQuery("select[name=documents]:eq(0) option");
	options = options.sort(function(a,b) {
		return parseInt(jQuery(a).attr("timeval")) < parseInt(jQuery(b).attr("timeval"));
	});
	jQuery("select[name=documents]:eq(0)").html(options);
}
</script>
</head>
<body onload="init();sortDocs();" onunload="window.close()"
	style="font-family: Verdana, Tahoma, Arial, sans-serif; background-color: #ddddff">

	<h3 style="text-align: center">
		<bean:message
			key="oscarEncounter.oscarConsultationRequest.AttachDocPopup.header" />
		<%=patientName%></h3>
	<html:form action="/oscarEform/attachDoc?method=attachDoc">
		<table width="100%">
			<tr>
				<th style="text-align: center"><bean:message key="oscarEncounter.oscarConsultationRequest.AttachDocPopup.available" /></th>
				<th>&nbsp;</th>
				<th style="text-align: center"><bean:message key="oscarEncounter.oscarConsultationRequest.AttachDocPopup.attached" /></th>
			</tr>
			<tr>
				<td style="width: 45%; text-align: left" valign="top"><html:hidden
						property="requestId" value="<%=requestId%>" /> <html:hidden
						property="demoNo" value="<%=demoNo%>" /> <html:hidden
						property="providerNo" value="<%=providerNo%>" /> 
						<select
						style="width: 100%;" name="documents" multiple="true" size="10">
						<%!
						private long getTimeVal(String dateTime) {
							if (dateTime == null || dateTime.trim().length() < 10) {
								return 0L;
							}
							dateTime = dateTime.trim();
							java.util.Date date = null;
							try {
								String dmt = "yyyy-MM-dd hh:mm:ss";
								if (dateTime.length() == 10) {
									dateTime += " 00:00:01";
								}
								date = new java.text.SimpleDateFormat(dmt).parse(dateTime);
							} catch (Exception e) {
								
							}
							if (date != null) {
								return date.getTime();
							}
							return 0L;
						}
						%>
						<%{
							long timeVal = 0L;
							ArrayList<EDoc> privatedocs = new ArrayList<EDoc>();
							privatedocs = EDocUtil.listEformDocs(loggedInInfo, demoNo, requestId, EDocUtil.UNATTACHED);
							EDoc curDoc;
							for (int idx = 0; idx < privatedocs.size(); ++idx) {
								curDoc = (EDoc) privatedocs.get(idx);
								String desc = StringUtils.maxLenString(curDoc.getDescription(),30,27,"...");
			                    if (desc == null || desc.trim().isEmpty()) {
			                    	desc = docType[0]+curDoc.getDocId();
			                    }
			                    desc = curDoc.getObservationDate() + " " + desc;
						%>
						<option class="doc" timeval="<%=getTimeVal(curDoc.getObservationDate()) %>"
							value="<%=docType[0] + curDoc.getDocId()%>"><%=desc%></option>
						<%
							}
							CommonLabResultData labData = new CommonLabResultData();

							ArrayList<LabResultData> labs = labData.populateLabEformData(loggedInInfo, demoNo, requestId, CommonLabResultData.UNATTACHED);
							LabResultData resData;
							
							for (int idx = 0; idx < labs.size(); ++idx) {
								resData = (LabResultData) labs.get(idx);
								boolean displayFlag = true;
								if (resData.labType.equals(LabResultData.HL7TEXT)) {
									if (!Hl7textResultsData.getMatchingLabs(resData.segmentID).endsWith(resData.segmentID))
										displayFlag = false;
								}
								if (displayFlag) {
									if(resData.labType.equals("HL7")){
						%>
						<option class="lab" timeval="<%=getTimeVal(resData.getDateTime()) %>"
							value="<%=docType[1] + resData.labPatientId%>"><%=resData.getDateTime() + " " + resData.getDiscipline()%></option>
						<%
									}
									if(resData.labType.equals("CML")){
						%>
						<option class="lab" timeval="<%=getTimeVal(resData.getDateTime()) %>"
							value="<%=docType[4] + resData.segmentID%>"><%=resData.getDateTime() + " " + resData.getDiscipline()%></option>
						<%				
									}
								}
							}
					
						HRMDocumentToDemographicDao hrmToDemoDao = (HRMDocumentToDemographicDao)SpringUtils.getBean("HRMDocumentToDemographicDao");
		                HRMDocumentDao hrmDao = (HRMDocumentDao)SpringUtils.getBean("HRMDocumentDao");
		                HRMDocumentSubClassDao hrmDocumentSubClassDao = (HRMDocumentSubClassDao) SpringUtils.getBean("HRMDocumentSubClassDao");
		                List<HRMDocumentToDemographic> hList = hrmToDemoDao.findByDemographicNoAndEformID(demoNo, requestId, false);
		                for (HRMDocumentToDemographic htd : hList) {
		                	HRMDocument hrm = hrmDao.find(htd.getHrmDocumentId());
		                	List<HRMDocumentSubClass> subClassList = hrmDocumentSubClassDao.getSubClassesByDocumentId(hrm.getId());
		                	HRMReport report = HRMReportParser.parseReport(loggedInInfo,hrm.getReportFile());
		                	if(report == null)
		                		continue;
		    				HRMUtil hRMUtil = new HRMUtil();
		    				hRMUtil.findCorrespondingHRMSubClassDescriptions(subClassList, hrm.getReportType(), report.getSendingFacilityId() , report.getFirstReportSubClass());
		                	
		                	String title = hrm.getReportType();
		                	if (subClassList != null && subClassList.size() > 0 && subClassList.get(0) != null && subClassList.get(0).getSubClassDescription() != null && !subClassList.get(0).getSubClassDescription().isEmpty()) {
		    					title = subClassList.get(0).getSubClassDescription();
		    				}
		                	String reportStatus = hrm.getReportStatus();
		                	if (reportStatus != null && reportStatus.equalsIgnoreCase("C")) {
		    					title = "(Cancelled) " + title;
		    				}
		                	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		
		                	%>
		                	<option class="hrm" timeval="<%=hrm.getTimeReceived().getTime() %>"
							value="<%=docType[2]+htd.getHrmDocumentId()%>"><%=formatter.format(hrm.getTimeReceived()) + " " + title%></option>
		                	<% 
		                }
		             }%>
		             		             <% 
              //get new eforms
               EFormDataDao efromDocs1 = (EFormDataDao)SpringUtils.getBean("EFormDataDao");
             	boolean status1=true; 
             	int demono=Integer.parseInt(demoNo);
             	List<EFormData> eList2 = efromDocs1.getSavedEfromDataByDemoId(demono,ConversionUtils.fromIntString(requestId));
             	String ids="";
             	for (EFormData edata:eList2){
             		if(eList2.size()==1)
             		ids=ids+edata.getId();
             		if(eList2.size()>1){
             		ids=ids+edata.getId()+",";
             		}
             	}
             	if(eList2.size()>1){
             	ids=ids.substring(0, ids.length()-1);
             	}
             	//String sqlStr="SELECT * FROM eform_data WHERE demographic_no=? and fdid IN(SELECT SUBSTRING_INDEX(GROUP_CONCAT(fdid ORDER BY form_date DESC),',',1) FROM eform_data GROUP BY fid) ORDER BY form_date DESC";
             	String sqlStr = "SELECT * FROM eform_data WHERE demographic_no=? and fdid IN(select max(fdid) from eform_data where demographic_no=" + demoNo + " and status=1 group by fid DESC) ORDER BY form_date DESC";

             	if(eList2!=null&&eList2.size()>0){
             		if(eList2.size()>1){
                 	ids=ids.substring(0, ids.length());
                 	}
                     //sqlStr="SELECT * FROM eform_data WHERE fdid not in( "+ids+") and demographic_no=? and fdid IN(SELECT SUBSTRING_INDEX(GROUP_CONCAT(fdid ORDER BY form_date DESC),',',1) FROM eform_data GROUP BY fid) ORDER BY form_date DESC";
                    sqlStr="SELECT * FROM eform_data WHERE fdid not in( "+ids+") and demographic_no=? and fdid IN(select max(fdid) from eform_data where demographic_no=" + demoNo + " and status=1 group by fid DESC) ORDER BY form_date DESC";

             	} 
             	 
             	List<Object> eList1 = efromDocs1.getEfromDataByDemo4(sqlStr,demoNo);
             SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd"); 
             
           for(Object eform:eList1){
        	   Object[] cells = (Object[]) eform; 
        	   boolean isFromEformPortal = false;
        	   String openEformUrl = "";
        	   if(cells[9].toString().indexOf("id=\"root\"") > -1){
        		   isFromEformPortal = true;
        		   openEformUrl = request.getContextPath() + "/eform/efmshowform_data.jsp?fdid=" + cells[0] + "&fid=" + cells[1] + "&demographic_no=" + demoNo + "&parentAjaxId=eforms";
        	   }
             	%>
             <option class="eform" id="<%=cells[1]%>" timeval="<%=getTimeVal(formatter1.format(cells[6])) %>" <%= isFromEformPortal ? "isFromEformPortal='true'":"isFromEformPortal='false'" %> <%= isFromEformPortal ? ("openEformUrl='" + openEformUrl + "'"):"" %>
					value="E<%=cells[0]%>"><%=formatter1.format(cells[6]) + " " + cells[2]%></option>
             <%
							}
						%>
					</select></td>
				<td style="width: 10%; text-align: center"><input type="button"
					class="btn" onclick="swap('documents','attachedDocs');" value=">>" /><br />
					<input type="button" class="btn"
					onclick="swap('attachedDocs','documents');" value="<<"/> <br />
					<input id="readerSwitcher" class="smallButton" type="button" onclick="switchView();" value="Preview"/> <br />
					</td>
				<td style="width: 45%; text-align: right"><select
						style="width: 100%;" name="attachedDocs" multiple="true"
						size="10">
						<%
							ArrayList<EDoc> privatedocs = new ArrayList<EDoc>();
							privatedocs = EDocUtil.listEformDocs(loggedInInfo, demoNo, requestId, EDocUtil.ATTACHED);
							EDoc curDoc;
							
							for (int idx = 0; idx < privatedocs.size(); ++idx) {
								curDoc = (EDoc) privatedocs.get(idx);
								String desc = StringUtils.maxLenString(curDoc.getDescription(),30,27,"...");
			                    if (desc == null || desc.trim().isEmpty()) {
			                    	desc = docType[0]+curDoc.getDocId();
			                    }
			                    desc += curDoc.getObservationDate() + " " + desc;
						%>
						<option class="doc" timeval="<%=getTimeVal(curDoc.getObservationDate()) %>" 
							value="<%=docType[0] + curDoc.getDocId()%>"><%=desc%></option>
						<%
							}

							CommonLabResultData labData = new CommonLabResultData();
							ArrayList<LabResultData> labs = labData.populateLabEformData(loggedInInfo, demoNo, requestId, CommonLabResultData.ATTACHED);
							LabResultData resData;
							
							for (int idx = 0; idx < labs.size(); ++idx) {
								resData = (LabResultData) labs.get(idx);
						%>
						<option class="lab" timeval="<%=getTimeVal(resData.getDateTime()) %>"
							value="<%=docType[1] + resData.labPatientId%>"><%=resData.getDateTime() + " " + resData.getDiscipline()%></option>
						<%
							}
						%>
						<%
             HRMDocumentToDemographicDao hrmToDemoDao = (HRMDocumentToDemographicDao)SpringUtils.getBean("HRMDocumentToDemographicDao");
             HRMDocumentDao hrmDao = (HRMDocumentDao)SpringUtils.getBean("HRMDocumentDao");
             HRMDocumentSubClassDao hrmDocumentSubClassDao = (HRMDocumentSubClassDao) SpringUtils.getBean("HRMDocumentSubClassDao");
             List<HRMDocumentToDemographic> hList = hrmToDemoDao.findByDemographicNoAndEformID(demoNo, requestId, true);
             
             for (HRMDocumentToDemographic htd : hList) {
             	HRMDocument hrm = hrmDao.find(htd.getHrmDocumentId());
             	List<HRMDocumentSubClass> subClassList = hrmDocumentSubClassDao.getSubClassesByDocumentId(hrm.getId());
             	HRMReport report = HRMReportParser.parseReport(loggedInInfo,hrm.getReportFile());
 				HRMUtil hRMUtil = new HRMUtil();
 				hRMUtil.findCorrespondingHRMSubClassDescriptions(subClassList, hrm.getReportType(), report.getSendingFacilityId() , report.getFirstReportSubClass());
             	
             	String title = hrm.getReportType();
             	if (subClassList != null && subClassList.size() > 0 && subClassList.get(0) != null && subClassList.get(0).getSubClassDescription() != null && !subClassList.get(0).getSubClassDescription().isEmpty()) {
 					title = subClassList.get(0).getSubClassDescription();
 				}
             	String reportStatus = hrm.getReportStatus();
             	if (reportStatus != null && reportStatus.equalsIgnoreCase("C")) {
 					title = "(Cancelled) " + title;
 				}
             	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

             	%>
             	<option class="hrm" timeval="<%=hrm.getTimeReceived().getTime() %>"
					value="<%=docType[2]+htd.getHrmDocumentId()%>"><%=formatter.format(hrm.getTimeReceived()) + " " + title%></option>
             	<% 
             }
             %>
             <% 
              //get saved eforms
                int reqId=ConversionUtils.fromIntString(requestId);
             	List<EFormData> eList = efromDocs1.getSavedEfromDataByDemoId(demono,reqId);
             SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
            
           for(EFormData eform:eList){
        	   boolean isFromEformPortal = false;
        	   String openEformUrl = "";
        	   if(eform.getFormData().indexOf("id=\"root\"") > -1){
        		   isFromEformPortal = true;
        		   openEformUrl = request.getContextPath() + "/eform/efmshowform_data.jsp?fdid=" + eform.getId() + "&fid=" + eform.getFormId() + "&demographic_no=" + demoNo + "&parentAjaxId=eforms";
        	   }
             	%>
             <option class="eform" id="<%=eform.getFormId()%>" timeval="<%=getTimeVal(formatter2.format(eform.getFormDate())) %>" <%= isFromEformPortal ? "isFromEformPortal='true'":"isFromEformPortal='false'" %> <%= isFromEformPortal ? ("openEformUrl='" + openEformUrl + "'"):"" %>
					value="E<%=eform.getId()%>"><%=formatter2.format(eform.getFormDate()) + " " + eform.getFormName()%></option>
             <%
							}
						%> 
					</select></td>
			</tr>
		</table>
		<table width="100%">
			<tr>
				<td style="text-align: center"><input type="submit" class="btn"
					name="submit"
					value="<bean:message key="oscarEncounter.oscarConsultationRequest.AttachDocPopup.submit"/>"
					onclick="return save();" /></td>
			</tr>
			<tr>
				<td style="text-align: center"><span class="legend"><bean:message
							key="oscarEncounter.oscarConsultationRequest.AttachDoc.Legend" /></span><br />
				<span class="doc legend"><bean:message
							key="oscarEncounter.oscarConsultationRequest.AttachDoc.LegendDocs" /></span><br />
				<span class="lab legend"><bean:message
							key="oscarEncounter.oscarConsultationRequest.AttachDoc.LegendLabs" /></span><br />
				<span class="hrm legend"><bean:message key="oscarEncounter.oscarConsultationRequest.AttachDoc.LegendHRMs"/></span><br />
				<span class="eform legend"><bean:message key="oscarEncounter.oscarConsultationRequest.AttachDoc.LegendEForms"/></span>
				</td>
			
			</tr>
		</table>
	</html:form>
</body>
</html:html>
