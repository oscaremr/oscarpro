<%--

    Copyright (c) 2008-2012 Indivica Inc.

    This software is made available under the terms of the
    GNU General Public License, Version 2, 1991 (GPLv2).
    License details are available via "indivica.ca/gplv2"
    and "gnu.org/licenses/gpl-2.0.html".

--%>
<%--  

This Page creates the fax form for eforms.
 
--%>
<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="java.sql.*, java.util.ArrayList, oscar.eform.data.*, oscar.SxmlMisc, org.oscarehr.common.model.Demographic, oscar.oscarDemographic.data.DemographicData,oscar.OscarProperties,org.springframework.web.context.support.WebApplicationContextUtils, org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.common.model.*,org.oscarehr.common.dao.*"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<jsp:useBean id="displayServiceUtil" scope="request" class="oscar.oscarEncounter.oscarConsultationRequest.config.pageUtil.EctConDisplayServiceUtil" />
<%

	boolean eFormFaxEnabled = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("eform_fax_enabled", false);
	String provider = (String) request.getSession().getAttribute("user");
	String attachmentWidgetJson = "";

	if (eFormFaxEnabled) {
		
		displayServiceUtil.estSpecialist();		
		String demo = request.getParameter("demographicNo");
		DemographicData demoData = null;
		Demographic demographic = null;
		String rdohip = "";
		if (!"".equals(demo))
		{
			demoData = new oscar.oscarDemographic.data.DemographicData();
			demographic = demoData.getDemographic(LoggedInInfo.getLoggedInInfoFromSession(request), demo);
			rdohip = demographic.getReferralPhysicianOhip();
		}
		String formId = request.getParameter("fdid");
		List<String> faxRecipients = new ArrayList<String>();
		List<String> faxRecipientsNames = new ArrayList<String>();
		if (formId != null && !"".equals(formId)) {
			EFormValueDao eFormValueDao = SpringUtils.getBean(EFormValueDao.class);
			List<EFormValue> eFormValues = eFormValueDao.findByFormDataId(Integer.parseInt(formId));
			for (EFormValue value : eFormValues) {
			    if (value.getVarName().equals("faxRecipients")) {
					faxRecipients.add(value.getVarValue());
				} else if (value.getVarName().equals("faxRecipientsName")) {
					faxRecipientsNames.add(value.getVarValue());
				} else if (value.getVarName().equals("attachment-control-printables")) {
					attachmentWidgetJson = value.getVarValue();
				}
			}
		}

%> 
<table width="100%">
	<tr>

	<td class="tite4" width="10%">  Providers: </td>
	<td class="tite3" width="20%">
		<select id="otherFaxSelect">
		<%
		String rdName = "";
		String rdFaxNo = "";
		for (int i=0;i < displayServiceUtil.specIdVec.size(); i++) {
                             String  specId     = (String) displayServiceUtil.specIdVec.elementAt(i);
                             String  fName      = (String) displayServiceUtil.fNameVec.elementAt(i);
                             String  lName      = (String) displayServiceUtil.lNameVec.elementAt(i);
                             String  proLetters = (String) displayServiceUtil.proLettersVec.elementAt(i);
                             String  address    = (String) displayServiceUtil.addressVec.elementAt(i);
                             String  phone      = (String) displayServiceUtil.phoneVec.elementAt(i);
                             String  fax        = (String) displayServiceUtil.faxVec.elementAt(i);
                             String  referralNo = (displayServiceUtil.referralNoVec.size() > 0 ? displayServiceUtil.referralNoVec.get(i).trim() : "");
                             String  annotation  = displayServiceUtil.annotationVec.elementAt(i);
                             boolean annotateInSearch = displayServiceUtil.annotateInSearchVec.elementAt(i) && StringUtils.isNotEmpty(annotation);
                             if (rdohip != null && !"".equals(rdohip) && rdohip.equals(referralNo)) {
                            	 rdName = String.format("%s, %s", lName, fName);
                            	 rdFaxNo = fax;
                             }
			if (!"".equals(fax)) {
			%>
			<option value="<%= Encode.forHtmlContent(fax) %>"> <%= Encode.forHtmlContent(String.format("%s, %s %s", lName, fName, (annotateInSearch ? "(" + annotation + ")" : ""))) %> </option>
			<%						
			}
		} %>		                        
		</select>
	</td>
	<td class="tite3">				
		<button onclick="AddOtherFaxProvider(); return false;">Add Provider</button>
	</td>
</tr>
<tr>
	<td class="tite4" width="10%"> Other Fax Number: </td>											
	<td class="tite3" width="20%">
		<input type="text" id="otherFaxInput"></input>	
		<font size="1">(xxx-xxx-xxxx)  </font>					
	</td>
	<td class="tite3">
		<button onclick="AddOtherFax(); return false;">Add Other Fax Recipient</button>
	</td>		
</tr>
<tr>
	<td colspan=3>
		<ul id="faxRecipients">
		<%
		if (!faxRecipients.isEmpty()) {
			//make sure that the faxRecipients count matches up with the names
			if (faxRecipients.size() == faxRecipientsNames.size()) {
		    for (int i = 0; i < faxRecipients.size(); i++) { %>
		            <li>
						<%=faxRecipientsNames.get(i)%> <b>Fax No: </b><%=faxRecipients.get(i)%> <a href="javascript:void(0);" onclick="removeRecipient(this)">remove</a>
						<input type="hidden" name="faxRecipients" value="<%=faxRecipients.get(i)%>"/>
						<input type="hidden" name="faxRecipientsName" value="<%=faxRecipientsNames.get(i)%>"/>
					</li>
				<% }
			}
		} else {
			WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
			UserPropertyDAO userPropertyDAO = (UserPropertyDAO) ctx.getBean("UserPropertyDAO");
			UserProperty prop = userPropertyDAO.getProp(provider, UserProperty.EFORM_REFER_FAX);
			boolean eformFaxRefer = prop != null && !"no".equals(prop.getValue());
			
			if (eformFaxRefer && !"".equals(rdName) && !"".equals(rdFaxNo)) {
				%>
				<li>
				<%=rdName %> <b>Fax No: </b><%= rdFaxNo %> <a href="javascript:void(0);" onclick="removeRecipient(this)">remove</a>
					<input type="hidden" name="faxRecipients" value="<%= rdFaxNo %>" />
					<input type="hidden" name="faxRecipientsName" value="<%=rdName%>"/>
				</li>
				<%
			}
		}
		%>
		</ul>
	</td>	
</tr>
</table>
<% } // end if (props.isRichEFormFaxEnabled()) { %>