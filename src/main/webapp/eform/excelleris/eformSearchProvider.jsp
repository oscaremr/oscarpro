<%--

    Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

--%>
<!--
/*
 *
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
 *
 * <OSCAR TEAM>
 */
-->
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="org.oscarehr.util.MiscUtils"%>
<%@page import="org.oscarehr.common.model.Provider" %>
<%@page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@page import="java.util.*,org.apache.log4j.Logger" %>


<%
	ProviderDao providerDao = (ProviderDao) SpringUtils.getBean("providerDao");
%>
<%
	if (session.getAttribute("user") == null) {
    response.sendRedirect("../logout.jsp");
  }



  int nItems = 0;
  Vector vec = new Vector();
  Properties prop = null;
  String param = request.getParameter("param")==null?"":request.getParameter("param");
  String param2 = request.getParameter("param2")==null?"":request.getParameter("param2");

  String doctorName = request.getParameter("refDoctorName")==null? "" :request.getParameter("refDoctorName");
  String searchType = request.getParameter("searchType") == null ? "" : request.getParameter("searchType");

  String keyword = request.getParameter("keyword");
  Integer doctorId = null;
  String doctorNameData = "";

  List<Provider> providers = null;
  
	if (request.getParameter("submit") != null && (request.getParameter("submit").equals("Search")
		|| request.getParameter("submit").equals("Next Page") || request.getParameter("submit").equals("Last Page")) ) {
  
	  String search_mode = request.getParameter("search_mode")==null?"search_name":request.getParameter("search_mode");
	  String orderBy = request.getParameter("orderby")==null?"last_name,first_name":request.getParameter("orderby");
	  String where = "";


	  
	    String[] temp = keyword.split("\\,\\p{Space}*");

	
	     if (temp.length>1) {		
	    	providers = providerDao.findExcellerisProvidersByFullName(temp[0], temp[1]);
	    } else if (temp.length ==  1 && temp[0] != "") {		
	    	providers = providerDao.findExcellerisProvidersByLastName(temp[0]);
	    } else {
	    	providers = providerDao.findAllExcellerisProviders();
	    } 
	     
	}
	
	//If prefessionalSpecialists is not null and there is nothing in the doctorNameData , 
	if (providers != null  && doctorNameData.equals("")) {
	  //Creates an array of all the found specialists
	  for (Provider provider : providers) {
	    prop = new Properties();
	    prop.setProperty("lifelabs_id", (provider.getLifelabsId() != null ? provider.getLifelabsId() : ""));
	    prop.setProperty("excelleris_id", (provider.getExcellerisId() != null ? provider.getExcellerisId() : ""));
	    prop.setProperty("last_name", (provider.getLastName() != null ? provider.getLastName() : ""));
	    prop.setProperty("first_name", (provider.getFirstName() != null ? provider.getFirstName() : ""));
	    prop.setProperty("ohip_no", (provider.getOhipNo() != null ? provider.getOhipNo() : ""));
	    prop.setProperty("cpsid", (provider.getPractitionerNo() != null ? provider.getPractitionerNo() : ""));
	    prop.setProperty("address", (provider.getAddress() != null ? provider.getAddress() : ""));
	    prop.setProperty("work_phone", (provider.getWorkPhone() != null ? provider.getWorkPhone() : ""));
	    vec.add(prop);
	  }
	}
%>
<%@ page import="java.util.*,java.sql.*,java.net.*"%>

<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@ page import="org.apache.commons.lang.WordUtils"%>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<html:html locale="true">
<head>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<title>Copy To Providers Search</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="all" href="../../share/css/extractedFromPages.css"  />
<script language="JavaScript">
    function setfocus() {
        this.focus();
		document.forms[0].keyword.focus();
		document.forms[0].keyword.select();
	}
	
	function check() {
        document.forms[0].submit.value = "Search";
        return true;
	}
        
    function typeInProvider(lifeLabsId, excellerisId, firstName, lastName, oHipNo, cpsid, address, workPhone) {
        opener.document.getElementById('doctor').value = lastName + ", " + firstName;
        opener.document.getElementById('doctor_cpsid').value = cpsid;
        opener.document.getElementById('doctor_ohip_no').value = oHipNo;
        opener.document.getElementById('doctor_excelleris_id').value = excellerisId;
        opener.document.getElementById('doctor_lifelab_id').value = lifeLabsId;
        opener.document.getElementById('doctor_first_name').value = firstName;
        opener.document.getElementById('doctor_last_name').value = lastName;
        opener.document.getElementById('doctor_address').value = address;
        opener.document.getElementById('doctor_cpsid_hidden').value = cpsid;
        opener.document.getElementById('doctor_ohip_no_hidden').value = oHipNo;
        opener.document.getElementById('set_by_search').value = 'true';
        self.close();
    }
</script>
</head>
<body bgcolor="white" bgproperties="fixed" onload="setfocus()"
	topmargin="0" leftmargin="0" rightmargin="0">
<table border="0" cellpadding="1" cellspacing="0" width="100%"
	bgcolor="#CCCCFF">
	<form method="post" name="titlesearch" action="eformSearchProvider.jsp"
		onSubmit="return check();">
	<tr>
		<td class="searchTitle" colspan="4">Search Copy To Clinician/Practitioner</td>
	</tr>
	<tr>
		<td class="blueText" width="10%" nowrap><input type="radio"
			name="search_mode" value="search_name" checked> Name</td>
		
		<td valign="middle" rowspan="2" align="left"><input type="text"
				name="keyword" value="<%= Encode.forHtmlAttribute(searchType.equals("name") ? doctorName : "") %>" size="17" maxlength="100"> <input
			type="hidden" name="orderby" value="last_name, first_name"> <input
			type="hidden" name="limit1" value="0"> <input type="hidden"
			name="limit2" value="10"> <input type="hidden" name="submit"
			value='Search'> <input type="submit" value='Search'>
		</td>
	</tr>
	<input type='hidden' name='param'
		value="<%=StringEscapeUtils.escapeHtml(param)%>">
	<input type='hidden' name='param2'
		value="<%=StringEscapeUtils.escapeHtml(param2)%>">

</table>
<table width="95%" border="0">
	<tr>
		<td align="left">Results based on keyword(s): <%=keyword==null?"":keyword%></td>
	</tr>
	</form>
</table>
<center>
<table width="100%" border="0" cellpadding="0" cellspacing="2"
	bgcolor="#C0C0C0">
	<tr class="title">
		
		<th width="25%">Last Name</b></th>
		<th width="20%">First Name</b></th>
		<th width="25%">Address</th>
	</tr>
	<% for (int i=0; i<vec.size(); i++) {
         prop = (Properties) vec.get(i);
		 String bgColor = i%2==0?"#EEEEFF":"ivory";
		 String strOnClick;
		 strOnClick = "typeInProvider('" +
		   StringEscapeUtils.escapeJavaScript(prop.getProperty("lifelabs_id","")) + "', '" +
		   StringEscapeUtils.escapeJavaScript(prop.getProperty("excelleris_id", "")) + "', '" +
		   StringEscapeUtils.escapeJavaScript(prop.getProperty("first_name", "")) + "', '" +
		   StringEscapeUtils.escapeJavaScript(prop.getProperty("last_name", "")) + "', '" +
		   StringEscapeUtils.escapeJavaScript(prop.getProperty("ohip_no", "")) + "', '" +
		   StringEscapeUtils.escapeJavaScript(prop.getProperty("cpsid", "")) + "', '" +
		   StringEscapeUtils.escapeJavaScript(prop.getProperty("address", "")) + "', '" +
		   StringEscapeUtils.escapeJavaScript(prop.getProperty("work_phone", "")) +  "')" ;
    %>
	<tr align="center" bgcolor="<%=bgColor%>" align="center"
		onMouseOver="this.style.cursor='hand';this.style.backgroundColor='pink';"
		onMouseout="this.style.backgroundColor='<%=bgColor%>';"
		onClick="<%=strOnClick%>">
		
		<td><%=Encode.forHtml(WordUtils.capitalize(prop.getProperty("last_name", "").toLowerCase()))%></td>
		<td><%=Encode.forHtml(WordUtils.capitalize(prop.getProperty("first_name", "").toLowerCase()))%></td>
		<td><%= Encode.forHtml(prop.getProperty("address", ""))%></td>
	</tr>
	<% } %>
</table>

<%
  nItems=vec.size();
  int nLastPage=0,nNextPage=0;


%> 
<%
  if(nItems==0) {
%> <bean:message key="demographic.search.noResultsWereFound" /> <%
  }
%> <script language="JavaScript">
<!--



//-->
</SCRIPT>

<form method="post" name="nextform" action="eformSearchProvider.jsp">


<br>
</body>
</html:html>
