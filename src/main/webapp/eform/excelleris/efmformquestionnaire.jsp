<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ page import="oscar.util.*, oscar.eform.data.*"%>
<%@page
	import="org.oscarehr.integration.excelleris.eorder.api.Questionnaire"%>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Question"%>
<%@page
	import="org.oscarehr.integration.excelleris.eorder.api.QuestionType"%>
<%@page import="org.oscarehr.integration.excelleris.eorder.api.Choice"%>
<%@page import="org.oscarehr.common.dao.EOrderDao" %>
<%@page import="org.oscarehr.common.model.ExcellerisEorder" %>
<%@page import="org.oscarehr.common.model.EFormValue" %>
<%@page import="org.oscarehr.common.dao.EFormValueDao" %>
<%@page import="org.oscarehr.common.dao.OrderLabTestCodeCacheDao" %>
<%@page import="org.oscarehr.integration.eOrder.common.EFormValueHelper" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.converter.QuestionnaireConverter" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialBusinessLogic" %>
<%@page import="org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialFactory" %>
<%@page import="org.oscarehr.integration.eOrder.common.ServiceException" %>
<%@page import="org.slf4j.Logger"%>
<%@page import="org.slf4j.LoggerFactory"%>
<%@page import="org.hl7.fhir.r4.formats.JsonParser" %>
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>


<head>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/moment.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/library/bootstrap/3.0.0/css/bootstrap.min.css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/library/bootstrap-datetimepicker.min.js" ></script>
<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/css/bootstrap-datetimepicker-standalone.css" />
<link href="<%=request.getContextPath()%>/css/excelleris/eOrderQuestionnaire.css" rel="stylesheet" type="text/css"/>

<style>
  .required-indicator {
    color: red;
    font-size: 22px;
    margin-left: 5px;
  }
</style>

<% Boolean isOtherTest = false; %>
<script> 

var isOtherTest = false;

const QuestionTypeEnum = {
        CHOICE: 'CHOICE',
        STRING: 'STRING',
        TEXT: 'TEXT',
        DECIMAL: 'DECIMAL',
        INTEGER: 'INTEGER',
        DATE: 'DATE',
        DATETIME: 'DATETIME',
        TIME: 'TIME'
    }

function displayQuestionnaire() {           
    $('#questionnaireContatiner').show();
    updateQuestionDisplay(1);
    updateQuestionnaireNavigation(1);
    displaySavedAnswers();
    $('.time').datetimepicker({
		format : "LT",
	});
    
    $('.date').datetimepicker({
        format: 'DD-MMM-YYYY',
        ignoreReadonly: true
    });
    
    $('.datetime').datetimepicker({
        format: 'L LT'
    });
}

function updateQuestionDisplay(questionNum) {
    $('.question').hide();
    $('#questionnaireContainer').attr("current_question", questionNum);
    updateQuestionnaireNavigation(questionNum);
    $('.question[sequence=' + questionNum + ']').show();
}

function displayNextQuestion() {
    setQuestionAnswer();
    var currentQuestionNum = getCurrentQuestionNum();
    if(isQuestionAnswered(currentQuestionNum)){
        $('#answerRequiredMessage').hide();
        updateQuestionDisplay(getCurrentQuestionNum() + 1);
    } else {
        $('#answerRequiredMessage').show();
    }
}

function displayPreviousQuestion() {
     setQuestionAnswer();
     var currentQuestionNum = getCurrentQuestionNum();
     $('#answerRequiredMessage').hide();
     updateQuestionDisplay(getCurrentQuestionNum() - 1);
}

function isQuestionAnswered(questionNum) {
    var answerField = $('.question[sequence=' + questionNum + '] .qAnswer');
    var isRequired = $('#question_' + questionNum + '_required').val();
    if(isRequired === 'true' && answerField.val() == '') {
        return false;
    }

    return true;
}

function getCurrentQuestionNum() {
    return parseInt($('#questionnaireContainer').attr("current_question"));
}

function setQuestionAnswer() {
    var questionNum = getCurrentQuestionNum();
    var answerField = $(".question[sequence=" + questionNum + "] .qAnswer");
    var answer = "";
    var questionType = $(".question[sequence=" + questionNum + "]").attr('answerType');
    
    if(questionType === QuestionTypeEnum.CHOICE) {
        if($("input[name=question_" + questionNum +"]:checked").length > 0) {
    		answer = $("input[name=question_" + questionNum +"]:checked").val();
        }
    	
    } else {
    	answer = $("#question_" + questionNum).val();
    	if(questionType === QuestionTypeEnum.INTEGER) {
    		if(!/^-?\d*$/.test(answer)) {
    			 $('#invalidInt').show();
    			return;
    		}else {
    			 $('#invalidInt').hide();
    		}
    	}
    	
    }
    
    if (isOtherTest) {
      answerField.val(answer);
      console.log("answer: "+answer);
    }
    var parentAnswerFieldId = "QR_q" + questionNum;
    opener.document.getElementById(parentAnswerFieldId).value = answerField.val();
    var isRequired = $('#question_' + questionNum + '_required').val();
    opener.document.getElementById(parentAnswerFieldId).dataset.required = isRequired;
    $('#answerRequiredMessage').hide();
}

function displaySavedAnswers() {
	var i;
	for(i = 1; i <= $('.question').length; i++) {
		var questionType = $(".question[sequence=" + i + "]").attr('answerType');
		var value = opener.document.getElementById('QR_q' + i).value;
		if(value === '') {
			continue;
		}
		if (questionType === QuestionTypeEnum.CHOICE) {	    	
	    	$(".question[sequence=" + i + "] input[value='" + value + "']").attr('checked', 'checked');
	    } else if(questionType === QuestionTypeEnum.STRING){
	    	$("input[name=question_" + i +"]").val(value);	
	    }
	}
}

function updateQuestionnaireNavigation(currentQuestionNum) {
    if(currentQuestionNum <= 1) {
        $('#previousQuestion').hide();
    } else {
        $('#previousQuestion').show();
    }

    if($('.question').length < 2 || currentQuestionNum >= $('.question').length) {
        $('#nextQuestion').hide();
    } else {
        $('#nextQuestion').show();
    } 
    
    if(currentQuestionNum >= $('.question').length){
    	$('#finishAndSubmit').show();
    } else {
    	$('#finishAndSubmit').hide()
    }
}

function finishAndSubmit() {
	setQuestionAnswer();
	if(validQuestionnaire()){
		opener.submitEform();
		self.close();
	}
}


function validQuestionnaire() {
        var i;
        for(i = 1; i <= $('.question').length; i++) {
            if(!isQuestionAnswered(i)){
                $('#answerRequiredMessage').show();
                return false;
            }
        }
        $('#answerRequiredMessage').hide();
        return true;
}
	
</script>
</head>



<%
	Logger log = LoggerFactory.getLogger(getClass());
   
    String fdid = (String) request.getParameter("fdid");
    
	String woundSource = "";
	String childAgeDays = "";
	String childAgeHours = "";
	String practitionerPhoneNumber = "";
	String patientPhoneNumber = "";
	String vaginalRectalGroupBSource = "";
	String otherSwabsPusSource = "";
	String chlamydiaSource = "";
	String gonorrhoeaSource = "";
   
    EOrderDao eOrderDao=(EOrderDao)SpringUtils.getBean(EOrderDao.class);
	ExcellerisEorder eOrder = (ExcellerisEorder) eOrderDao.findByExtEFormDataId(Integer.parseInt(fdid));
    Questionnaire questionnaire = null;
    
 	// get eform values for the current eform data
	EFormValueDao eformValueDao = (EFormValueDao)SpringUtils.getBean("EFormValueDao");
	List<EFormValue> eformValues = eformValueDao.findByFormDataId(new Integer(fdid));					
    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
    
    if(eOrder != null && eOrder.getQuestionnaire() != null) {
    	// Convert to api questionnaire
	
    	JsonParser parser = new JsonParser();
//				ByteArrayOutputStream writer = new ByteArrayOutputStream();
		org.hl7.fhir.r4.model.Questionnaire fHirQuestionnaire = null;
			try {
				fHirQuestionnaire = (org.hl7.fhir.r4.model.Questionnaire) parser.parse(eOrder.getQuestionnaire());
			} catch (Exception e) {
				log.error("Error converting Parameters to JSON.", e);
				throw new ServiceException("Error converting Parameters to JSON.");
			}
		
    	
    	OrderLabTestCodeCacheDao orderLabTestCodeCacheDao = (OrderLabTestCodeCacheDao) SpringUtils
				.getBean(OrderLabTestCodeCacheDao.class);
		
    	if (valueMap.get("T_wound_src") != null
				&& valueMap.get("T_wound_src").getVarValue() != null) {
			woundSource = valueMap.get("T_wound_src").getVarValue();
			log.info("woundSource: "+woundSource);
		}
		if (valueMap.get("EO_child_age_days") != null
				&& valueMap.get("EO_child_age_days").getVarValue() != null) {
			childAgeDays = valueMap.get("EO_child_age_days").getVarValue();
			log.info("childAgeDays: "+childAgeDays);
		}
		if (valueMap.get("EO_child_age_hours") != null
				&& valueMap.get("EO_child_age_hours").getVarValue() != null) {
			childAgeHours = valueMap.get("EO_child_age_hours").getVarValue();
			log.info("childAgeHours: "+childAgeHours);
		}
		if (valueMap.get("EO_practitioner_phone") != null
				&& valueMap.get("EO_practitioner_phone").getVarValue() != null) {
			practitionerPhoneNumber = valueMap.get("EO_practitioner_phone").getVarValue();
			log.info("practitionerPhoneNumber: "+practitionerPhoneNumber);
		}
		if (valueMap.get("EO_patient_phone") != null
				&& valueMap.get("EO_patient_phone").getVarValue() != null) {
			patientPhoneNumber = valueMap.get("EO_patient_phone").getVarValue();
			log.info("patientPhoneNumber: "+patientPhoneNumber);
		}
		if (valueMap.get("T_vaginal_rectal_group_b_src") != null
				&& valueMap.get("T_vaginal_rectal_group_b_src").getVarValue() != null) {
			vaginalRectalGroupBSource = valueMap.get("T_vaginal_rectal_group_b_src").getVarValue();
			log.info("vaginalRectalGroupBSource: "+vaginalRectalGroupBSource);
		}
		if (valueMap.get("T_other_swabs_pus_src") != null
				&& valueMap.get("T_other_swabs_pus_src").getVarValue() != null) {
			otherSwabsPusSource = valueMap.get("T_other_swabs_pus_src").getVarValue();
			log.info("otherSwabsPusSource: "+otherSwabsPusSource);
		}
		if (valueMap.get("T_chlamydia_src") != null
				&& valueMap.get("T_chlamydia_src").getVarValue() != null) {
			chlamydiaSource = valueMap.get("T_chlamydia_src").getVarValue();
			log.info("chlamydiaSource: "+chlamydiaSource);
		}
		if (valueMap.get("T_gc_src") != null
				&& valueMap.get("T_gc_src").getVarValue() != null) {
			gonorrhoeaSource = valueMap.get("T_gc_src").getVarValue();
			log.info("gonorrhoeaSource: "+gonorrhoeaSource);
		}
    	
		final ProvincialBusinessLogic provincial = new ProvincialFactory(valueMap).getProvincialBusinessLogic();

		questionnaire = QuestionnaireConverter.toApiObject(fHirQuestionnaire, provincial, orderLabTestCodeCacheDao);
    }
    
    
	if (questionnaire != null) {
	
%>

	
	<body onload="displayQuestionnaire()">
	
	<div id="questionnaireContainer" current_question='1'>
		<input type='hidden' id='M_existingFdid' name='M_existingFdid' value='<%= fdid %>'></input>
<%
		isOtherTest = false;
		int seq = 1;
		for (Question question : questionnaire.getQuestions()) {
		    boolean required = question.getRequired();
		    String requiredIndicator = "";
		    if (required) {
		        requiredIndicator = "<span class='required-indicator'>&lowast;</span>";
		    }

		    String answer = "";
		    
			if (question.getTestCode().equals("TR11980-0")) {
				answer = woundSource;
			}
			else if (question.getTestCode().equals("TR11471-0V")
					&& question.getQuestionText().equals("ChildAgeDays")) {
				answer = childAgeDays;
			}
			else if (question.getTestCode().equals("TR11471-0V")
					&& question.getQuestionText().equals("ChildAgeHours")) {
				answer = childAgeHours;
			}
			else if (question.getTestCode().equals("TR11471-0V")
					&& question.getQuestionText().equals("PractitionerNo")) {
				answer = practitionerPhoneNumber;
			}
			else if (question.getTestCode().equals("TR11471-0V")
					&& question.getQuestionText().equals("PatientNo")) {
				answer = patientPhoneNumber;
			}
			else if (question.getTestCode().equals("TR10703-7")) {
				answer = vaginalRectalGroupBSource;
			}
			else if (question.getTestCode().equals("TR10694-8")) {
				answer = otherSwabsPusSource;
			}
			else if (question.getTestCode().equals("TR10714-4")) {
				answer = gonorrhoeaSource;
			}
			else if (question.getTestCode().equals("TR10690-6")) {
				answer = chlamydiaSource;
			}
			else  {
				isOtherTest = true;
				log.error("isOtherTest: "+isOtherTest);
			}
			
			log.info("Answer: "+answer);
%>
		<div class='question' answerType='<%= question.getType() %>'
			sequence='<%= seq %>'
			hardCodedTestFieldId=' <%= question.getHardCodedTestFieldId() %>'
			otherTestFieldId='<%= question.getOtherTestFieldId() %>'>
			<input type="hidden" id="question_<%=seq%>_required" name="question_<%=seq%>_required" value="<%= required %>" />
			<div class="instruction">
				<bean:message
					key="eform.instruction.submit_questionnaire.answerQuestionnaire_1" />
				<b> <%= question.getTestName() %>
				</b>
				<bean:message
					key="eform.instruction.submit_questionnaire.answerQuestionnaire_2" />
			</div>

			<div class="questionContainer">
				<p><%=question.getQuestionText()%><%= requiredIndicator %></p>
				<%
					if (question.getType().equals(QuestionType.CHOICE)) {

								for (Choice choice : question.getChoices()) {
				%>
				<input type='radio' id='<%=choice.getCode()%>'
					name='question_<%=seq%>' value='<%=choice.getCode()%>'> <label
					for='<%=choice.getCode()%>'><%=choice.getText()%> </label><br>
				<%
					}
							} else if (question.getType().equals(QuestionType.STRING)) {
				%>
				<input type='text' maxlength='120' id='question_<%=seq%>'
					name='question_<%= seq %>' value='<%= answer %>'>
				<%
					} else if (question.getType().equals(QuestionType.TEXT)) {
				%>
				<textarea id='question_<%=seq%>' name='question_<%=seq%>' rows='4' cols='60'
					style="font-family: sans-serif; font-style: normal; font-weight: normal; font-size: 12px; text-align: left;"><%= answer %></textarea>
				<%
					} else if (question.getType().equals(QuestionType.DECIMAL)) {
				%>
				<input type="number" id='question_<%=seq%>'
					name='question_<%= seq %>' value='<%= answer %>'>
				<%
					} else if (question.getType().equals(QuestionType.INTEGER)) {
				%>
				<input type="text" id='question_<%=seq%>' name='question_<%=seq%>'>
				<%
					} else if (question.getType().equals(QuestionType.TIME)) {
				%>

				<div class='input-group time'>
					<input type='text' id='question_<%=seq%>'
						name='question_<%= seq %>' class="form-control" value='<%= answer %>'/> <span
						class="input-group-addon"> <span
						class="glyphicon glyphicon-time"></span>
					</span>
				</div>

				<%
					} else if (question.getType().equals(QuestionType.DATE)) {
				%>


				<div class='input-group date'>
					<input type='text' id='question_<%=seq%>'
						name='question_<%=seq%>' class="form-control" readonly /> <span
						class="input-group-addon"> <span
						class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

				<%
					} else if (question.getType().equals(QuestionType.DATETIME)) {
				%>


				<div class='input-group datetime'>
					<input type='text' id='question_<%=seq%>'
						name='question_<%=seq%>' class="form-control" /> <span
						class="input-group-addon"> <span
						class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>

				<%
					}
				%>

				<input class='qAnswer' type='hidden' id='QR_q<%=seq%>'
					name='QR_q<%= seq %>' value='<%= answer %>' data-required='<%= required %>'>
			</div>
		</div>
		<%
			seq++;
				}
		%>
		<div id='invalidInt' style="display: none; color: red; margin-left:20px;"><bean:message
					key="eform.instruction.submit_questionnaire.invalidInt" /></div>
    <div id='answerRequiredMessage' style="display: none; color: red; margin-left:20px;"><bean:message
					key="eform.instruction.submit_questionnaire.answerRequired" /></div>
    <div id='questionnaireNavigation' style="margin-top: 10px; text-align: center;">
       
         <input value="Previous"  name="previousQuestion" id="previousQuestion" type="button" style="display: none;" onclick="displayPreviousQuestion()">

          <input value="Next"  name="nextQuestion" id="nextQuestion" type="button" onclick="displayNextQuestion()" style="display: none;">
          <input value="Finish" name="Done" id="finishQuestionnaire" type="button" onclick="finishQuestionnaire()" style="display: none;">
          <input value="eOrder"  name="finishAndSubmit" id="finishAndSubmit" type="button" onclick="finishAndSubmit()" style="display: none;">
    </div>
</div>
<%
	}
%>
<script defer>
var currentQuestionNum = getCurrentQuestionNum();
isOtherTest = <%= isOtherTest %>;
if (!isOtherTest) {
	var isQuestion = true;
	while (isQuestion) {
		currentQuestionNum = getCurrentQuestionNum();
		if (currentQuestionNum >= $('.question').length) {
			finishAndSubmit();
			isQuestion = false;
		} else {
			displayNextQuestion();
		}
	}
}
</script>

</body>
