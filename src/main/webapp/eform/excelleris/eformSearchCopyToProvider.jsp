<%--

    Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

--%>
<!--
/*
 *
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
 *
 * <OSCAR TEAM>
 */
-->
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="org.oscarehr.util.MiscUtils"%>
<%@page import="org.oscarehr.common.model.ExcellerisCopyToProvider" %>
<%@page import="org.oscarehr.common.dao.ExcellerisCopyToProviderDao" %>
<%@page import="java.util.*,org.apache.log4j.Logger" %>

<%
	ExcellerisCopyToProviderDao excellerisCopyToProviderDao = (ExcellerisCopyToProviderDao) SpringUtils.getBean("excellerisCopyToProviderDao");
    Logger logger = MiscUtils.getLogger();
%>
<%
	if (session.getAttribute("user") == null) {
    	response.sendRedirect("../logout.jsp");
  	}

    /* String strLimit1="0";
    String strLimit2="10";
    if(request.getParameter("limit1")!=null) strLimit1 = request.getParameter("limit1");
    if(request.getParameter("limit2")!=null) strLimit2 = request.getParameter("limit2");
    */

    final int MAGIC_NUMBER = 8; // Excelleris 'magic number'
	int nItems = 0;
    boolean searchRequested = false;
	List<Properties> practitionerMetadata = Collections.EMPTY_LIST;
	Properties prop = null;
	String param = request.getParameter("param") == null ? "":request.getParameter("param");
	String param2 = request.getParameter("param2") == null ? "":request.getParameter("param2");
	
	String doctorName = request.getParameter("refDoctorName") == null ? "" :request.getParameter("refDoctorName");
	String searchType = request.getParameter("searchType") == null ? "" : request.getParameter("searchType");
	
	String keyword = request.getParameter("keyword");
	
	//Integer doctorId = null;
	//String doctorNameData = "";	
	//List<ExcellerisCopyToProvider> excellerisCopyToProviders = null;
  
	if (request.getParameter("submit") != null && (request.getParameter("submit").equals("Search")
		|| request.getParameter("submit").equals("Next Page") || request.getParameter("submit").equals("Last Page")) ) {
  
		String search_mode = request.getParameter("search_mode") == null ? "search_name" : request.getParameter("search_mode");
	  	String orderBy = request.getParameter("orderby") == null ? "last_name,first_name" : request.getParameter("orderby");
	  	String where = "";
	  
	    //String[] temp = keyword.split("\\,\\p{Space}*");
	    //if (temp.length > 1) {		
	    //	excellerisCopyToProviders = excellerisCopyToProviderDao.findByFullName(temp[0], temp[1]);
	    //} else if (temp.length == 1) {		
	    //	excellerisCopyToProviders = excellerisCopyToProviderDao.findByLastName(temp[0]);
	    //} else {
	    //	excellerisCopyToProviders = excellerisCopyToProviderDao.findAll();
	    //}
	    
	    String[] temp = keyword.split("\\,\\p{Space}*");
	    String family = "", given = null;
	    if (temp.length > 1) {		
	    	family = temp[0]; given = temp[1];
	    } else if (temp.length == 1) {		
	    	family = temp[0]; 
	    }
	    
		final ExcellerisEOrder excellerisEOrder = (ExcellerisEOrder) SpringUtils.getBean("excellerisEOrder");
		final AuthenticationToken authToken = excellerisEOrder.getAuthenticationToken(request);
		if (authToken != null && !authToken.hasExpired()) {
			practitionerMetadata = PractitionerConverter.toProperties(excellerisEOrder.searchPractitioners(authToken, given, family));
		}

	    searchRequested = true;
	    
		// If there is only one specialist, assign the data appropriately for check later
 		//if (excellerisCopyToProviders != null && excellerisCopyToProviders.size() == 1) {
		//	doctorId = excellerisCopyToProviders.get(0).getId();
		//	doctorNameData = excellerisCopyToProviders.get(0).getLastName() + ", " + excellerisCopyToProviders.get(0).getFirstName();
		//	doctorNumberData = excellerisCopyToProviders.get(0).getReferralNo();
		// } 
	}



	
	//If prefessionalSpecialists is not null and there is nothing in the doctorNameData,
	//if (excellerisCopyToProviders != null  && doctorNameData.equals("")) {
		//Creates an array of all the found specialis
		//for (ExcellerisCopyToProvider excellerisCopyToProvider : excellerisCopyToProviders) {
			//prop = new Properties();
			//prop.setProperty("doctor_id", (excellerisCopyToProvider.getId() != null ? excellerisCopyToProvider.getId() : "").toString());
			//prop.setProperty("ontario_lifelabs_id", (excellerisCopyToProvider.getOntarioLifeLabsId() != null ? excellerisCopyToProvider.getOntarioLifeLabsId() : ""));
			//prop.setProperty("rover_id", (excellerisCopyToProvider.getRoverId() != null ? excellerisCopyToProvider.getRoverId() : ""));
			//prop.setProperty("on_provincial_govt_id", (excellerisCopyToProvider.getOnProvincialGovtId() != null ? excellerisCopyToProvider.getOnProvincialGovtId() : ""));
			//prop.setProperty("pathnet_code", (excellerisCopyToProvider.getPathnetCode() != null ? excellerisCopyToProvider.getPathnetCode() : ""));
			//prop.setProperty("salutation", (excellerisCopyToProvider.getSalutation() != null ? excellerisCopyToProvider.getSalutation() : ""));
			//prop.setProperty("last_name", (excellerisCopyToProvider.getLastName() != null ? excellerisCopyToProvider.getLastName() : ""));
			//prop.setProperty("first_name", (excellerisCopyToProvider.getFirstName() != null ? excellerisCopyToProvider.getFirstName() : ""));
	
			//prop.setProperty("middle_name", (excellerisCopyToProvider.getMiddleName() != null ? excellerisCopyToProvider.getMiddleName() : ""));
			//prop.setProperty("address1", (excellerisCopyToProvider.getAddress1() != null ? excellerisCopyToProvider.getAddress1() : ""));
			//prop.setProperty("address2", (excellerisCopyToProvider.getAddress2() != null ? excellerisCopyToProvider.getAddress2() : ""));
			//prop.setProperty("city", (excellerisCopyToProvider.getCity() != null ? excellerisCopyToProvider.getCity() : ""));
			//prop.setProperty("state_or_province", (excellerisCopyToProvider.getStateOrProvince() != null ? excellerisCopyToProvider.getStateOrProvince() : ""));
			//prop.setProperty("zip_or_postal", (excellerisCopyToProvider.getZipOrPostal() != null ? excellerisCopyToProvider.getZipOrPostal() : ""));
			//prop.setProperty("country", (excellerisCopyToProvider.getCountry() != null ? excellerisCopyToProvider.getCountry() : ""));
			//prop.setProperty("region", (excellerisCopyToProvider.getRegion() != null ? excellerisCopyToProvider.getRegion() : ""));
			//prop.setProperty("specialty", (excellerisCopyToProvider.getSpecialty() != null ? excellerisCopyToProvider.getSpecialty() : ""));
			//	prop.setProperty("deleted", (excellerisCopyToProvider.isDeleted() ? excellerisCopyToProvider.isDeleted() : ""));
			//prop.setProperty("visible", (excellerisCopyToProvider.isVisible() ? excellerisCopyToProvider.isVisible() : ""));
			//vec.add(prop);
		//}
	//}
%>
<%@ page import="java.util.*,java.sql.*,java.net.*"%>

<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@ page import="org.apache.commons.lang.WordUtils"%>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.integration.excelleris.eorder.ExcellerisEOrder" %>
<%@ page import="org.oscarehr.integration.excelleris.eorder.AuthenticationToken" %>
<%@ page import="org.oscarehr.integration.excelleris.eorder.converter.PractitionerConverter" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<html:html locale="true">
<head>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<title>Copy To Providers Search</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" media="all" href="../../share/css/extractedFromPages.css"  />
<script language="JavaScript">

<!--

	var offLine = <%=((practitionerMetadata == null) ? "true" : "false") %>

	function setfocus() {
		this.focus();
		document.forms[0].keyword.focus();
		document.forms[0].keyword.select();
		
		if (offLine) {
			opener.document.getElementById('SubmitButton').value = 'Save';
			opener.document.getElementById('M_IsOffLine').value = 'true';
		}
	}
	
	function check() {
		document.forms[0].submit.value="Search";
		return true;
	}
        
	function typeInCopyToProvider (lifeLabsId, firstName, lastName, salutation, roverId, street, city, province, country, postal ) {
		opener.document.getElementById('CopyToProvider1FirstName').value = firstName;
		opener.document.getElementById('CopyToProvider1LastName').value = lastName;
		opener.document.getElementById('CopyToProvider1Address').value = street + '\n' + city + ', ' + province + '\n' + country +'\n' +postal;
         	
		opener.document.getElementById('copy_to_doctor_1_excelleris_id').value = roverId;
 		opener.document.getElementById('copy_to_doctor_1_lifelab_id').value = lifeLabsId;
 		opener.document.getElementById('copy_to_doctor_1_first_name').value = firstName;
		opener.document.getElementById('copy_to_doctor_1_last_name').value = lastName;
		opener.document.getElementById('copy_to_doctor_1_title').value = salutation;
		opener.document.getElementById('copy_to_doctor_1_address').value = street + '\n' + city + ', ' + province + '\n' + country +'\n' +postal;
		                   	
		self.close();
          	<%-- <%if( firstName.length() > 0 ) {%>
             	opener.referral_first_name = firstName;
             <%} 
               if( toname.length() > 0 ) {%>
             	opener.<%=toname%> = toname;
             <%}
               if( toaddress1.length() > 0 ) {%>
             	opener.<%=toaddress1%> = toaddress;
             <%}
               if( tophone.length() > 0 ) {%>
             	opener.<%=tophone%> = tophone;
             <%}
               if( tofax.length() > 0 ) {%>
             	opener.<%=tofax%> = tofax;
             <%}%> --%>            
	}
                
-->

      </script>
</head>
<body bgcolor="white" bgproperties="fixed" onload="setfocus()"
	topmargin="0" leftmargin="0" rightmargin="0">
<table border="0" cellpadding="1" cellspacing="0" width="100%"
	bgcolor="#CCCCFF">
	<form method="post" name="titlesearch" action="eformSearchCopyToProvider.jsp"
		onSubmit="return check();">
	<tr>
		<td class="searchTitle" colspan="4">Search Copy To Clinician/Practitioner</td>
	</tr>
	<tr>
		<td class="blueText" width="10%" nowrap><input type="radio"
			name="search_mode" value="search_name" checked> Name</td>
		
		<td valign="middle" rowspan="2" align="left"><input type="text"
				name="keyword" value="<%= Encode.forHtmlAttribute(searchType.equals("name") ? doctorName : "") %>" size="17" maxlength="100"> <input
			type="hidden" name="orderby" value="last_name, first_name"> <input
			type="hidden" name="limit1" value="0"> <input type="hidden"
			name="limit2" value="10"> <input type="hidden" name="submit"
			value='Search'> <input type="submit" value='Search'>
		</td>
	</tr>
	<input type='hidden' name='param'
		value="<%=StringEscapeUtils.escapeHtml(param)%>">
	<input type='hidden' name='param2'
		value="<%=StringEscapeUtils.escapeHtml(param2)%>">

</table>
<table width="95%" border="0">
	<tr>
		<td align="left">Results based on keyword(s): <%=keyword == null ? "" : keyword%></td>
	</tr>
	</form>
</table>
<center>
<table width="100%" border="0" cellpadding="0" cellspacing="2" bgcolor="#C0C0C0">
	<tr class="title">		
		<th width="25%">Last Name</b></th>
		<th width="20%">First Name</b></th>
		<th width="25%">Address</th>
		<th width="20%">City</b></th>
	</tr>
	<%  if (practitionerMetadata != null) {
	    	for (int i = 0; i < practitionerMetadata.size(); i++) {
        		prop = (Properties) practitionerMetadata.get(i);
				String bgColor = i%2==0 ? "#EEEEFF" : "ivory";
				String strOnClick;
				strOnClick = "typeInCopyToProvider('"
						+ StringEscapeUtils.escapeJavaScript(prop.getProperty("ontario_lifelabs_id","")) + "', '"
						+ StringEscapeUtils.escapeJavaScript(prop.getProperty("first_name", "")) + "', '"
						+ StringEscapeUtils.escapeJavaScript(prop.getProperty("last_name", "")) + "', '"
						+ StringEscapeUtils.escapeJavaScript(prop.getProperty("salutation", "")) + "', '"
						+ StringEscapeUtils.escapeJavaScript(prop.getProperty("excellerisId", "")) + "', '"
						+ StringEscapeUtils.escapeJavaScript(prop.getProperty("address1", ""))
						+ "', '" + StringEscapeUtils.escapeJavaScript(prop.getProperty("city", "")) + "', '"
						+ StringEscapeUtils.escapeJavaScript(prop.getProperty("state_or_province", "")) + "', '"
						+ StringEscapeUtils.escapeJavaScript(prop.getProperty("country", ""))
						+ "', '" + StringEscapeUtils.escapeJavaScript(prop.getProperty("zip_or_postal", "")) + "')" ;
            
    %>
	<tr align="center" bgcolor="<%=bgColor%>" align="center"
		onMouseOver="this.style.cursor='hand';this.style.backgroundColor='pink';"
		onMouseout="this.style.backgroundColor='<%=bgColor%>';"
		onClick="<%=strOnClick%>">
		
		<td><%=Encode.forHtml(WordUtils.capitalize(prop.getProperty("last_name", "").toLowerCase()))%></td>
		<td><%=Encode.forHtml(WordUtils.capitalize(prop.getProperty("first_name", "").toLowerCase()))%></td>
		<td><%= Encode.forHtml(prop.getProperty("address1", ""))%></td>
		<td><%=Encode.forHtml(prop.getProperty("city", ""))%></td>
	</tr>
	<% }} %>
</table>

<%
  nItems = (practitionerMetadata != null) ? practitionerMetadata.size() : -1;
  int nLastPage = 0, nNextPage = 0;

  //nNextPage = Integer.parseInt(strLimit2) + Integer.parseInt(strLimit1);
  //nLastPage = Integer.parseInt(strLimit1) - Integer.parseInt(strLimit2);  
%> 
<%
  if (nItems == 0 && searchRequested) {
%> <bean:message key="demographic.search.noResultsWereFound" /> <%
  } else if (nItems > MAGIC_NUMBER) {
%> <bean:message key="demographic.search.ccProvider.refineSearch" /> <%
  } else if (nItems == -1) {
%> <bean:message key="demographic.search.ccProvider.serviceUnavailable" /> <%	  
  }
%>
 
<script language="JavaScript">
<!--



//-->
</script>

<form method="post" name="nextform" action="eformSearchCopyToProvider.jsp">
<%-- 
  if(nLastPage>=0) {
%> <input type="submit" class="mbttn" name="submit"
	value="<bean:message key="demographic.demographicsearch2apptresults.btnPrevPage"/>"
	onClick="last()"> <%
  }
  if(nItems==Integer.parseInt(strLimit2)) {
%> <input type="submit" class="mbttn" name="submit"
	value="<bean:message key="demographic.demographicsearch2apptresults.btnNextPage"/>"
	onClick="next()"> <%
}

  
%> --%>

<br>

<!-- <a href="<%=request.getContextPath() %>/eform/excelleris/AddCopyToProvider.jsp">Add Copy To Provider</a> -->
</center>

<div style="margin-left: 10px; margin-right: 10px;"><bean:message key="demographic.search.ccProvider.unableToAdd1" /></div>
<br/>
<div style="margin-left: 10px; margin-right: 10px;"><bean:message key="demographic.search.ccProvider.unableToAdd2" /></div>

</body>
</html:html>
