<%--
    Copyright (c) 2021 WELL EMR Group Inc.
    This software is made available under the terms of the
    GNU General Public License, Version 2, 1991 (GPLv2).
    License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@page import="org.apache.log4j.Logger" %>
<%@page import="org.oscarehr.managers.EmailManager" %>
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="org.oscarehr.util.MiscUtils"%>

<%
	if (session.getAttribute("user") == null) {
    	response.sendRedirect("../logout.jsp");
  	}

	Logger logger = MiscUtils.getLogger();
    EmailManager emailManager = SpringUtils.getBean(EmailManager.class);
     
	String firstName = request.getParameter("firstname") == null ? "John" : request.getParameter("firstname");
	String lastName = request.getParameter("lastname") == null ? "Smith" : request.getParameter("lastname");
	String orderId = request.getParameter("orderid") == null ? "10001" : request.getParameter("orderid");
	
	String requisitionEmailContent = emailManager.getRequisitionEmailContent(firstName, lastName, orderId);
%>

<html:html locale="true">
<head>
<title>Sample Email</title>
</head>
<body bgcolor="white" bgproperties="fixed" onload="setfocus()" style="margin:15px; padding:0">
	<div><%=requisitionEmailContent%></div>		
</body>
</html:html>
