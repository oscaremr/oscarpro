<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@page import="org.oscarehr.common.dao.EFormDao"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
      String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
      boolean authed=true;
%>
<%-- <security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.consult" rights="w" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../../../securityError.jsp?type=_admin&type=_admin.consult");%>
</security:oscarSec> --%>
<%
if(!authed) {
	return;
}
%>

<%@ page import="java.util.ResourceBundle"%>
<% java.util.Properties oscarVariables = oscar.OscarProperties.getInstance(); %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<%@page import="java.util.List" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>
<%@page import="org.oscarehr.util.SpringUtils" %>


<%@page import="org.oscarehr.common.dao.ExcellerisCopyToProviderDao" %>
<%@page import="org.oscarehr.common.model.ExcellerisCopyToProvider" %>
<%@page import="oscar.eform.data.exelleris.AddCopyToProviderForm" %>
<%@page import="oscar.oscarDemographic.data.ProvinceNames" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%
	
    
    ExcellerisCopyToProviderDao copyToProviderDao = SpringUtils.getBean(ExcellerisCopyToProviderDao.class);
    
    
    
%>

<html:html locale="true">

<%
  ResourceBundle oscarR = ResourceBundle.getBundle("oscarResources",request.getLocale());

  String transactionType = "Add New Copy To Provider";
 
  
%>

<head>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<script src="<%= request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
<title><%=transactionType%></title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<html:base />
<link rel="stylesheet" type="text/css" media="all" href="../../share/css/extractedFromPages.css"  />



</head>
<script language="javascript">
function BackToOscar() {
       window.close();
}
</script>

<script language="javascript">
function setCopyToProvider() {
		if($("input[name='id']").val() && $("input[name='id']").val().length > 0) {
		
		opener.document.getElementById('CopyToProvider1FirstName').value = $("input[name='firstName']").val();
      	opener.document.getElementById('CopyToProvider1LastName').value = $("input[name='lastName']").val();
      	opener.document.getElementById('CopyToProvider1Address').value = $("input[name='address1']").val() + '\n' + $("input[name='city']").val() + ', ' + $("select[name='stateOrProvince'] option:selected").val()  + '\n' + $("input[name='country']").val() +'\n' + $("input[name='zipOrPostal']").val();
      	
      	opener.document.getElementById('copy_to_doctor_1_first_name').value = $("input[name='firstName']").val();
      	opener.document.getElementById('copy_to_doctor_1_last_name').value =  $("input[name='lastName']").val();
      	opener.document.getElementById('copy_to_doctor_1_address').value = $("input[name='address1']").val() + '\n' + $("input[name='city']").val() + ', ' + $("select[name='stateOrProvince'] option:selected").val() + '\n' + $("input[name='country']").val() +'\n' + $("input[name='zipOrPostal']").val();

      	// Null the hiddens fields since these are not know, when a new provider is created
      	opener.document.getElementById('copy_to_doctor_1_excelleris_id').value = "";
      	opener.document.getElementById('copy_to_doctor_1_lifelab_id').value = "";
      	opener.document.getElementById('copy_to_doctor_1_title').value = "";
      	
      	self.close();
		}
}
</script>


<link rel="stylesheet" type="text/css" href="AddCopyToProviderStyles.css">
<body class="BodyStyle" vlink="#0000FF" onload="setCopyToProvider();">

<html:errors />
<!--  -->
<table class="MainTable" id="scrollNumber1" name="encounterTable">
	<tr class="MainTableTopRow">
		<td class="MainTableTopRowRightColumn">
		<table class="TopStatusBar">
			<tr>
				<td class="Header">Add New Copy To Provider</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr style="vertical-align: top">
		<td class="MainTableRightColumn">
		<table cellpadding="0" cellspacing="2"
			style="border-collapse: collapse" bordercolor="#111111" width="100%"
			height="100%">

			<!----Start new rows here-->
			<%
				   String added = Encode.forHtml((String) request.getAttribute("Added"));
                   if (added != null){  %>
			<tr>
				<td><font color="red"> <bean:message
					key="oscarEncounter.oscarConsultationRequest.config.AddSpecialist.msgSpecialistAdded"
					arg0="<%=added%>" /> </font></td>
			</tr>
			
				
			<%}%>
			<% 
			ExcellerisCopyToProvider copyToProvider = (ExcellerisCopyToProvider) request.getAttribute("copyToProvider");
            if (copyToProvider == null){  
            	copyToProvider = new ExcellerisCopyToProvider();
            }
            	%>
             
			
			<tr>
				<td>

				<html:form action="/eform/excelleris/AddCopyToProvider">
						<%
						  /*  if (request.getAttribute("id") != null ){ */
							   AddCopyToProviderForm thisForm;
							   thisForm = (AddCopyToProviderForm) request.getAttribute("AddCopyToProviderForm");
							   thisForm.setFirstName( (String) request.getAttribute("firstName"));
							   thisForm.setLastName( (String) request.getAttribute("lastName"));
							   thisForm.setAddress1( (String) request.getAttribute("address1"));
							   thisForm.setCity( (String) request.getAttribute("city"));
							   thisForm.setStateOrProvince( (String) request.getAttribute("province"));
							   thisForm.setCountry( (String) request.getAttribute("country"));
							   thisForm.setZipOrPostal( (String) request.getAttribute("postalCode"));

						   %>
						   
						   <%
						  /*  } */
						%>				
					<table>

						<html:hidden name="AddCopyToProviderForm" property="id" />
						<tr>
							<td><bean:message key="oscarEncounter.oscarConsultationRequest.config.AddSpecialist.firstName" /></td>
							<td><html:text name="AddCopyToProviderForm" property="firstName" value="<%= copyToProvider.getFirstName() %>"/></td>
							<td><bean:message key="oscarEncounter.oscarConsultationRequest.config.AddSpecialist.lastName" /></td>
							<td><html:text name="AddCopyToProviderForm" property="lastName" value="<%= copyToProvider.getLastName() %>" /></td>
							</tr>
						<tr>
						<td>Address
							</td>
							<td><html:text name="AddCopyToProviderForm" property="address1" value="<%= copyToProvider.getAddress1() %>"/></td>
						
							<td>City
							</td>
							<td><html:text name="AddCopyToProviderForm" property="city" value="<%= copyToProvider.getCity() %>" /> 
							</td>
                            
							
						</tr>
						<tr>
							<td> Province or State
							</td>
							<td>
							    <html:select name="AddCopyToProviderForm" property="stateOrProvince">
									<%
										Map<String, String> provinces = ProvinceNames.getDefaultProvinces();
										for (Map.Entry<String, String> entry : provinces.entrySet()) {
											String shortName = entry.getKey();
											String longName = entry.getValue();
									%>
									<% if(shortName.equals(copyToProvider.getStateOrProvince())){ 
										
									%>	
										
										<option selected value="<%=shortName%>"><%=shortName%>-<%=longName%></option>
									<%} else { %>
											<option value="<%=shortName%>"><%=shortName%>-<%=longName%></option>
									<% } %>
									<% } %>
									
								</html:select>
							</td>
							<td>
							</td>
							<td></td>
						</tr>
						<tr>
							<td> Postal Code or ZIP</td>
							<td><html:text name="AddCopyToProviderForm" property="zipOrPostal" value="<%= copyToProvider.getZipOrPostal() %>"/></td>
							<td><input type="hidden" name="country" value="<%= copyToProvider.getCountry() %>"/></td>
							<td></td>
						</tr>
						
						<%-- <tr>
							<td colspan="4">
								<html:select name="AddCopyToProviderForm" property="salutation">
									<html:option value=""><bean:message key="demographic.demographiceditdemographic.msgNotSet"/></html:option>
									<html:option value="Dr."><bean:message key="oscarEncounter.oscarConsultationRequest.config.AddSpecialist.msgDr"/></html:option>
									<html:option value="Mr."><bean:message key="oscarEncounter.oscarConsultationRequest.config.AddSpecialist.msgMr"/></html:option>
									<html:option value="Mrs."><bean:message key="oscarEncounter.oscarConsultationRequest.config.AddSpecialist.msgMrs"/></html:option>
									<html:option value="Miss"><bean:message key="oscarEncounter.oscarConsultationRequest.config.AddSpecialist.msgMiss"/></html:option>
									<html:option value="Ms."><bean:message key="oscarEncounter.oscarConsultationRequest.config.AddSpecialist.msgMs"/></html:option>
								</html:select>
							</td>
						</tr> --%>
						
						<tr>		<tr>
							<td colspan="6">
								
								<input type="submit" name="Submit" />
							</td>
						</tr>
					</table>
				</html:form>
				</td>
			</tr>
			<!----End new rows here-->

			<tr height="100%">
				<td></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		
		<td class="MainTableBottomRowRightColumn"></td>
	</tr>
</table>
</body>
</html:html>
