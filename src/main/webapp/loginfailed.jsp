<%@ page import="oscar.login.LoginAction.LoginFailureType" %>
<%@ page import="oscar.login.LoginAction" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%
    String errorMessage = StringUtils.trimToEmpty(request.getParameter("errormsg"));
    LoginFailureType failureType = LoginFailureType.fromString(request.getParameter("type"))
            .orElse(LoginFailureType.UNKNOWN);
%>
<html lang="en">
<head>
    <script type="text/javascript" src="/oscar/js/global.js"></script>
    <base href="https://cambian-test-gamma.kai-oscar.com/oscar/loginfailed.jsp">
    <title>Login Failure</title>
    <script src="/oscar/JavaScriptServlet" type="text/javascript"></script>
</head>
<body style="font-family: Helvetica, Arial">
<% switch (failureType) {
    case BLOCKED: {
%>
<h4><%=LoginAction.ERR_PW_ATTEMPTS%></h4>

<h4>Next Steps:</h4>
<ul>
    <li>By default the account will automatically unlock after 15 minutes for you to try again (may vary depending on system settings).</li><br>
    <li>If another user with admin access in your clinic is currently logged in, they can navigate to <b>Administration > User Management > Unlock Account</b> in order to unlock you immediately. <Br>Click <a href="https://oscarsupport.zendesk.com/hc/en-us/articles/7340247631636-How-to-Unlock-a-User-After-Too-Many-Failed-Attempts-Classic-" target="_blank">HERE</a> for instructions</li><br>
    <li>If another user with admin access is available, you may ask them to reset your password. Click <a href="https://oscarsupport.zendesk.com/hc/en-us/articles/6910105394196-How-to-Change-A-Users-Password-in-OSCAR-i-e-Reset-Password-" target="_blank">HERE</a> for instructions. </li><br>
</ul>
<%
        break;
    }
    case EXPIRED_LOGIN: {
%>
<h4><%=LoginAction.ERR_PW_EXPIRED%></h4>
<h4>Next Steps:</h4>
<ul>
    <li>
        If another user with admin-rights in your clinic is currently logged in, they can click
        "Administration &gt; User Management &gt; Search/Edit/Delete Security Records" and select
        your account and change the Expiry Date.
    </li>
</ul>
<%
        break;
    }
    default: {
%>
<h4>Oops! Something went wrong while logging into your account.</h4>
<h4><%=errorMessage%></h4>
<%
        break;
    }
}
%>
<br>
<div>
    <span>If additional assistance is required, please email OSCAR Pro:
        <a href="mailto:help@oscarprodesk.ca">help@oscarprodesk.ca</a>
        . Note that OSCAR Pro support may not be able to directly reset your password.
    </span>
</div>

</body>
</html>