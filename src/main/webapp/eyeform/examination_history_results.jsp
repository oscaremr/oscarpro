<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ include file="/taglibs.jsp"%>
<%@page import="org.oscarehr.eyeform.web.EyeformAction"%>
<%@page import="org.oscarehr.common.model.Appointment"%>
<%@page import="org.oscarehr.common.model.Measurement"%>
<%@page import="java.util.List"%>
<%@page import="oscar.util.StringUtils" %>
<%@page import="java.text.SimpleDateFormat" %>
<%@page import="oscar.dms.*,java.util.*" %>
<%@ page import="oscar.OscarProperties"%>
<%
	String sdate = StringUtils.transformNullInEmptyString((String)request.getAttribute("sdate"));
	String edate = StringUtils.transformNullInEmptyString((String)request.getAttribute("edate"));
    
    oscar.OscarProperties props = oscar.OscarProperties.getInstance();
	String[] fields = request.getParameterValues("fromlist2");
	List<String> fieldList = new ArrayList<String>();
	if(fields != null) {
		for(String field:fields) {
			fieldList.add(field);
		}
	}
	String[] field1 = request.getParameterValues("fromlist1");
	List<String> fieldList1 = new ArrayList<String>();
	if(field1 != null) {
		for(String field:field1) {
			fieldList1.add(field);
		}
	}
%>

<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request" />

<html>
	<head>
    	<title>Examination History Results</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
    	<link rel="stylesheet" type="text/css" href='<html:rewrite page="/jsCalendar/skins/aqua/theme.css" />' />
		<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/calendar/calendar.css" title="win2k-cold-1" />
		<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/lang/<bean:message key="global.javascript.calendar"/>"></script>
		<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar-setup.js"></script>
    	<link rel="stylesheet" href="<%=request.getContextPath()%>/eyeform/display2.css" type="text/css">
		
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.js"></script>
		
    	<style type="text/css">
			* { font-family: Trebuchet MS, Lucida Sans Unicode, Arial, Helvetica, sans-serif; color: #000; margin: 0px; padding: 0px; }
			body { padding: 10px; }

			td.inner{
			border:1px solid #666;
			}

			table.common{
			border:0;
			font-size: 10pt;
			}
			h5{
				margin-top: 1px;
				border-bottom: 1px solid #000;
				font-weight: bold;
				list-style-type: none;
				font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;
				font-size: 10pt;
				overflow: hidden;
				background-color: #ccccff;
				padding: 0px;
				color: black;
				width: 300px;
			}
			th {white-space:nowrap}

			.centered {text-align:center}
			
			.title { font-family: Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold; color: #333333; }
			.title1 { font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #333333; }
		</style>
		<script>
			function allsubmit(){
				document.getElementById("sdate").value = "";
				document.getElementById("edate").value = "";
				inputForm.submit();
			}
		</script>
		<script>
			function insert_title(){			
				var glasshxElm = $("h5").filter(function(){
					if(this.innerHTML.indexOf("Glasses History") != -1){
						return true;
					}
					return false;
				});
				if(glasshxElm != null && glasshxElm.length > 0){
					glasshxElm = glasshxElm[0];
					var targetTbl = $(glasshxElm).parent().children("table");
					if(targetTbl != null){					
						var title = "<tr><th class=\"centered\"></th><th colspan=\"5\" style=\"text-align:center\">OD</th><th colspan=\"5\" style=\"text-align:center\">OS</th><th></th><th></th></tr>"
						$($(targetTbl).find("thead")).prepend(title);
					}
				}
				
				var assessmentElm = $("h5").filter(function(){
					if (this.innerHTML.indexOf("VISION ASSESSMENT") != -1) {
						return true;
					}
					return false;
					});

				if (assessmentElm != null && assessmentElm.length > 0) {
					assessmentElm = assessmentElm[0];
					var targetTbl = $(assessmentElm).parent().children("table");
					if (targetTbl != null) {
						var title = "<tr><th class=\"centered\"></th>";
						<%if(fieldList.contains("Distance vision (sc)")){%>
							title = title + "<th colspan=\"3\" style=\"text-align:center\">Distance SC</th>";
						<%}%>
						<%if(fieldList.contains("Distance vision (cc)")){%>
							title = title + "<th colspan=\"3\" style=\"text-align:center\">Distance CC</th>";
						<%}%>
						<%if(fieldList.contains("Distance vision (ph)")){%>
							title = title + "<th colspan=\"2\" style=\"text-align:center\">Pin Hole</th>";
						<%}%>
						<%if(fieldList.contains("Intermediate vision (sc)")){%>
							title = title + "<th colspan=\"3\" style=\"text-align:center\">Intermediate SC</th>";
						<%}%>
						<%if(fieldList.contains("Intermediate vision (cc)")){%>
							title = title + "<th colspan=\"3\" style=\"text-align:center\">Intermediate CC</th>";
						<%}%>
						<%if(fieldList.contains("Near vision (sc)")){%>
							title = title + "<th colspan=\"3\" style=\"text-align:center\">Near SC</th>";
						<%}%>
						<%if(fieldList.contains("Near vision (cc)")){%>
							title = title + "<th colspan=\"3\" style=\"text-align:center\">Near CC</th>";
						<%}%>
						title = title + "</tr>";
						//var title = "<tr><th class=\"centered\"></th><th colspan=\"3\" style=\"text-align:center\">Distance SC</th><th colspan=\"3\" style=\"text-align:center\">Distance CC</th><th colspan=\"2\" style=\"text-align:center\">Pin Hole</th><th colspan=\"3\" style=\"text-align:center\">Intermediate SC</th><th colspan=\"3\" style=\"text-align:center\">Intermediate CC</th><th colspan=\"3\" style=\"text-align:center\">Near SC</th><th colspan=\"3\" style=\"text-align:center\">Near CC</th></tr>";
						$($(targetTbl).find("thead")).prepend(title);
					}
				}
				
				var visionElm = $("h5").filter(function(){
					if (this.innerHTML.indexOf("VISION MEASUREMENT") != -1) {
						return true;
					}
					return false;
					});

				if (visionElm != null && visionElm.length > 0) {
					visionElm = visionElm[0];
					var targetTbl = $(visionElm).parent().children("table");
					if (targetTbl != null) {
						var title = "<tr><th class=\"centered\"></th>";
						<%if(fieldList.contains("Keratometry")){%>
						title = title + "<th colspan=\"6\" style=\"text-align:center\">Keratometry</th>";
						<%}%>
						<%if(null != props.getProperty("eyeform_optometry_device") && props.getProperty("eyeform_optometry_device").equals("yes")){
							if(fieldList.contains("Refraction")){
						%>
						title = title + "<th colspan=\"4\" style=\"text-align:center\">Refraction</th>";	
						<%}}%>
						<%if(null != props.getProperty("eyeform_vision_measurement_has_two_AR_M") && props.getProperty("eyeform_vision_measurement_has_two_AR_M").equals("yes")){
								if(fieldList.contains("Auto-refraction(-)")){
						%>
						title = title + "<th colspan=\"8\" style=\"text-align:center\">Autorefraction(-)</th>";
						<%
								}
								if(fieldList.contains("Auto-refraction(+)")){
						%>
						title = title + "<th colspan=\"8\" style=\"text-align:center\">Autorefraction(+)</th>";
						<%
								}
								if(fieldList.contains("Manifest distance(-)")){
						%>
						title = title + "<th colspan=\"11\" style=\"text-align:center\">Manifest Distance(-)</th>";
						<%
								}
								if(fieldList.contains("Manifest distance(+)")){
						%>
						title = title + "<th colspan=\"11\" style=\"text-align:center\">Manifest Distance(+)</th>";
						<%
								}
							}else{
								if(fieldList.contains("Auto-refraction")){
						%>
						title = title + "<th colspan=\"8\" style=\"text-align:center\">Autorefraction</th>";		
						<%
								}
								if(fieldList.contains("Manifest distance")){
						%>
						title = title + "<th colspan=\"11\" style=\"text-align:center\">Manifest Distance</th>";
						<%	}
							}%>
						<%if(null != props.getProperty("eyeform_optometry_device") && props.getProperty("eyeform_optometry_device").equals("yes")){
							if(fieldList.contains("Finial")){
						%>
						title = title + "<th colspan=\"6\" style=\"text-align:center\">Finial</th>";	
						<%}}%>
						<%if(fieldList.contains("Manifest near")){
							if(null != props.getProperty("eyeform_optometry_device") && props.getProperty("eyeform_optometry_device").equals("yes")){
						%>
						title = title + "<th colspan=\"12\" style=\"text-align:center\">Manifest Near</th>";
						<%}else{%>
						title = title + "<th colspan=\"11\" style=\"text-align:center\">Manifest Near</th>";
						<%}}%>
						<%if(fieldList.contains("Cycloplegic refraction")){%>
						title = title + "<th colspan=\"10\" style=\"text-align:center\">Cycloplegic</th>";
						<%}%>
						title = title + "</tr>";
						//var title = "<tr><th class=\"centered\"></th><th colspan=\"6\" style=\"text-align:center\">Keratometry</th><th colspan=\"8\" style=\"text-align:center\">Autorefraction</th><th colspan=\"9\" style=\"text-align:center\">Manifest Distance</th><th colspan=\"9\" style=\"text-align:center\">Manifest Near</th><th colspan=\"8\" style=\"text-align:center\">Cycloplegic</th></tr>";
						$($(targetTbl).find("thead")).prepend(title);
					}
				}
				
				var intraocularElm = $("h5").filter(function(){
					if(this.innerHTML.indexOf("INTRAOCULAR PRESSURE") != -1){
						return true;
					}
					return false;
				});
				if(intraocularElm != null && intraocularElm.length > 0){
					intraocularElm = intraocularElm[0];
					var targetTbl = $(intraocularElm).parent().children("table");
					if(targetTbl != null){
						var title = "<tr><th class=\"centered\"></th>";
						<%if(fieldList.contains("NCT")){%>
						title = title + "<th colspan=\"3\" style=\"text-align:center\">Non-contact</th>";
						<%}%>
						<%if(fieldList.contains("Applanation")){%>
						title = title + "<th colspan=\"3\" style=\"text-align:center\">Applanation</th>";
						<%}%>
						<%if(fieldList.contains("Central corneal thickness")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Pachymetry</th>";
						<%}%>
						title = title + "</tr>";
						//var title = "<tr><th class=\"centered\"></th><th colspan=\"3\" style=\"text-align:center\">Non-contact</th><th colspan=\"3\" style=\"text-align:center\">Applanation</th><th colspan=\"2\" style=\"text-align:center\">Pachymetry</th></tr>"
						$($(targetTbl).find("thead")).prepend(title);
					}
				}
				
				var refractiveElm = $("h5").filter(function(){
					if(this.innerHTML.indexOf("REFRACTIVE") != -1){
						return true;
					}
					return false;
				});
				if(refractiveElm != null && refractiveElm.length > 0){
					refractiveElm = refractiveElm[0];
					var targetTbl = $(refractiveElm).parent().children("table");
					if(targetTbl != null){
						var title = "<tr><th class=\"centered\"></th>";
						<%if(fieldList.contains("Dominance")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Dominance</th>";
						<%}%>
						<%if(fieldList.contains("Mesopic pupil size")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Scotopic Pupil</th>";
						<%}%>
						<%if(fieldList.contains("Angle Kappa")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Kappa</th>";
						<%}%>
						title = title + "</tr>";
						//var title = "<tr><th class=\"centered\"></th><th colspan=\"2\" style=\"text-align:center\">Dominance</th><th colspan=\"2\" style=\"text-align:center\">Scotopic Pupil</th><th colspan=\"2\" style=\"text-align:center\">Kappa</th></tr>"
						$($(targetTbl).find("thead")).prepend(title);
					}
				}
				
				var otherElm = $("h5").filter(function(){
					if(this.innerHTML.indexOf("OTHER EXAM") != -1){
						return true;
					}
					return false;
				});
				if(otherElm != null && otherElm.length > 0){
					otherElm = otherElm[0];
					var targetTbl = $(otherElm).parent().children("table");
					if(targetTbl != null){
						var title = "<tr><th class=\"centered\"></th>";
						<%if(fieldList.contains("Colour vision")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Colour vision</th>";
						<%}%>
						<%if(fieldList.contains("Pupil")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Pupils</th>";
						<%}%>
						<%if(fieldList.contains("Amsler grid")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Amsler</th>";
						<%}%>
						<%if(fieldList.contains("Potential acuity meter")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">PAM</th>";
						<%}%>
						<%if(fieldList.contains("Confrontation fields")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Confrontation </th>";
						<%}%>
						<%if(fieldList.contains("Maddox rod")){%>
						title = title + "<th></th>";
						<%}%>
						<%if(fieldList.contains("Bagolini test")){%>
						title = title + "<th></th>";
						<%}%>
						<%if(fieldList.contains("Worth 4 Dot (distance)") || fieldList.contains("Worth 4 Dot (near)")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Worth 4 dot</th>";
						<%}%>
						title = title + "</tr>";
						//var title = "<tr><th class=\"centered\"></th><th colspan=\"2\" style=\"text-align:center\">Colour vision</th><th colspan=\"2\" style=\"text-align:center\">Pupils</th><th colspan=\"2\" style=\"text-align:center\">Amsler</th><th colspan=\"2\" style=\"text-align:center\">PAM</th><th colspan=\"2\" style=\"text-align:center\">Confrontation </th><th></th><th></th><th colspan=\"2\" style=\"text-align:center\">Worth 4 dot</th></tr>"
						$($(targetTbl).find("thead")).prepend(title);
					}
				}
				
				var externalElm = $("h5").filter(function(){
					if(this.innerHTML.indexOf("EXTERNAL/ORBIT") != -1){
						return true;
					}
					return false;
				});
				if(externalElm != null && externalElm.length > 0){
					externalElm = externalElm[0];
					var targetTbl = $(externalElm).parent().children("table");
					if(targetTbl != null){
						var title = "<tr><th class=\"centered\"></th>";
						<%if(fieldList.contains("Face")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Face</th>";
						<%}%>
						<%if(fieldList.contains("Retropulsion")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Retropulsion</th>";
						<%}%>
						<%if(fieldList.contains("Hertel")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Hertel</th>";
						<%}%>
						title = title + "</tr>";
						//var title = "<tr><th class=\"centered\"></th><th colspan=\"2\" style=\"text-align:center\">Face</th><th colspan=\"2\" style=\"text-align:center\">Retropulsion</th><th colspan=\"2\" style=\"text-align:center\">Hertel</th></tr>"
						$($(targetTbl).find("thead")).prepend(title);
					}
				}
				
				var ductElm = $("h5").filter(function(){
					if(this.innerHTML.indexOf("EYELID/NASOLACRIMAL DUCT") != -1){
						return true;
					}
					return false;
				});
				if(ductElm != null && ductElm.length > 0){
					ductElm = ductElm[0];
					var targetTbl = $(ductElm).parent().children("table");
					if(targetTbl != null){
						var title = "<tr><th class=\"centered\"></th>";
						<%if(fieldList.contains("Upper lid")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Upper Lid</th>";
						<%}%>
						<%if(fieldList.contains("Lower lid")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Lower Lid</th>";
						<%}%>
						<%if(fieldList.contains("Lacrimal lake")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Lacrimal Lake</th>";
						<%}%>
						<%if(fieldList.contains("Lacrimal irrigation")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Irrigation</th>";
						<%}%>
						<%if(fieldList.contains("Punctum")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Punctum</th>";
						<%}%>
						<%if(fieldList.contains("Nasolacrimal duct")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">NLD</th>";
						<%}%>
						<%if(fieldList.contains("Dye disappearance")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Dye Disappearance</th>";
						<%}%>
						title = title + "</tr>";
						//var title = "<tr><th class=\"centered\"></th><th colspan=\"2\" style=\"text-align:center\">Upper Lid</th><th colspan=\"2\" style=\"text-align:center\">Lower Lid</th><th colspan=\"2\" style=\"text-align:center\">Lacrimal Lake</th><th colspan=\"2\" style=\"text-align:center\">Irrigation</th><th colspan=\"2\" style=\"text-align:center\">Punctum</th><th colspan=\"2\" style=\"text-align:center\">NLD</th><th colspan=\"2\" style=\"text-align:center\">Dye Disappearance</th></tr>"
						$($(targetTbl).find("thead")).prepend(title);
					}
				}
				
				var eyelidElm = $("h5").filter(function(){
					if(this.innerHTML.indexOf("EYELID MEASUREMENT") != -1){
						return true;
					}
					return false;
				});
				if(eyelidElm != null && eyelidElm.length > 0){
					eyelidElm = eyelidElm[0];
					var targetTbl = $(eyelidElm).parent().children("table");
					if(targetTbl != null){
						var title = "<tr><th class=\"centered\"></th>";
						<%if(fieldList.contains("Margin reflex distance")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">MRD</th>";
						<%}%>
						<%if(fieldList.contains("Inferior scleral show")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">ISS</th>";
						<%}%>
						<%if(fieldList.contains("Levator function")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Levator Function</th>";
						<%}%>
						<%if(fieldList.contains("Lagophthalmos")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Lag Ophthalmos</th>";
						<%}%>
						<%if(fieldList.contains("Blink reflex")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Blink reflex</th>";
						<%}%>
						<%if(fieldList.contains("Cranial Nerve VII function")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">CNVII</th>";
						<%}%>
						<%if(fieldList.contains("Bells phenomenon")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Bell</th>";
						<%}%>
						<%if(fieldList.contains("Schirmer test")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Schirmer</th>";
						<%}%>
						title = title + "</tr>";
						//var title = "<tr><th class=\"centered\"></th><th colspan=\"2\" style=\"text-align:center\">MRD</th><th colspan=\"2\" style=\"text-align:center\">ISS</th><th colspan=\"2\" style=\"text-align:center\">Levator Function</th><th colspan=\"2\" style=\"text-align:center\">Lag Ophthalmos</th><th colspan=\"2\" style=\"text-align:center\">Blink reflex</th><th colspan=\"2\" style=\"text-align:center\">CNVII</th><th colspan=\"2\" style=\"text-align:center\">Bell</th><th colspan=\"2\" style=\"text-align:center\">Schirmer</th></tr>"
						$($(targetTbl).find("thead")).prepend(title);
					}
				}
				
				var eyelidElm = $("h5").filter(function(){
					if(this.innerHTML.indexOf("ANTERIOR SEGMENT") != -1){
						return true;
					}
					return false;
				});
				if(eyelidElm != null && eyelidElm.length > 0){
					eyelidElm = eyelidElm[0];
					var targetTbl = $(eyelidElm).parent().children("table");
					if(targetTbl != null){
						var title = "<tr><th class=\"centered\"></th>";
						<%if(fieldList.contains("Cornea")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Cornea</th>";
						<%}%>
						<%if(fieldList.contains("Conjunctiva/Sclera")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Conj/Sclera</th>";
						<%}%>
						<%if(fieldList.contains("Anterior chamber")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Anterior Chamber</th>";
						<%}%>
						<%if(fieldList.contains("Angle")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Angle</th>";
						<%}%>
						<%if(fieldList.contains("Iris")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Iris</th>";
						<%}%>
						<%if(fieldList.contains("Lens")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Lens</th>";
						<%}%>
						title = title + "</tr>";
						//var title = "<tr><th class=\"centered\"></th><th colspan=\"2\" style=\"text-align:center\">Cornea</th><th colspan=\"2\" style=\"text-align:center\">Conj/Sclera</th><th colspan=\"2\" style=\"text-align:center\">Anterior Chamber</th><th colspan=\"2\" style=\"text-align:center\">Angle</th><th colspan=\"2\" style=\"text-align:center\">Iris</th><th colspan=\"2\" style=\"text-align:center\">Lens</th></tr>"
						$($(targetTbl).find("thead")).prepend(title);
					}
				}
				
				var posteriorElm = $("h5").filter(function(){
					if(this.innerHTML.indexOf("POSTERIOR SEGMENT") != -1){
						return true;
					}
					return false;
				});
				if(posteriorElm != null && posteriorElm.length > 0){
					posteriorElm = posteriorElm[0];
					var targetTbl = $(posteriorElm).parent().children("table");
					if(targetTbl != null){
						var title = "<tr><th class=\"centered\"></th>";
						<%if(fieldList.contains("Optic disc")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Disc</th>";
						<%}%>
						<%if(fieldList.contains("C/D ratio")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">C/D Ratio</th>";
						<%}%>
						<%if(fieldList.contains("Macula")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Macula</th>";
						<%}%>
						<%if(fieldList.contains("Retina")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Retina</th>";
						<%}%>
						<%if(fieldList.contains("Vitreous")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Vitreous</th>";
						<%}%>
						<%if(fieldList.contains("CRT")){%>
						title = title + "<th colspan=\"2\" style=\"text-align:center\">CRT</th>";
						<%}%>
						title = title + "</tr>";
						//var title = "<tr><th class=\"centered\"></th><th colspan=\"2\" style=\"text-align:center\">Disc</th></th><th colspan=\"2\" style=\"text-align:center\">C/D Ratio</th></th><th colspan=\"2\" style=\"text-align:center\">Macula</th></th><th colspan=\"2\" style=\"text-align:center\">Retina</th></th><th colspan=\"2\" style=\"text-align:center\">Vitreous</th></tr>"
						$($(targetTbl).find("thead")).prepend(title);
					}
				}
				
				<%if(null != props.getProperty("eyeform_optometry_device") && props.getProperty("eyeform_optometry_device").equals("yes")){
					if(fieldList.contains("OCT")){
				%>
				var octElm = $("h5").filter(function(){
					if(this.innerHTML.indexOf("OCT") != -1){
						return true;
					}
					return false;
				});
				if(octElm != null && octElm.length > 0){
					octElm = octElm[0];
					var targetTbl = $(octElm).parent().children("table");
					if(targetTbl != null){
						var title = "<tr><th class=\"centered\"></th>";
						title = title + "<th colspan=\"2\" style=\"text-align:center\">S</th>";
						title = title + "<th colspan=\"2\" style=\"text-align:center\">I</th>";
						title = title + "<th colspan=\"8\" style=\"text-align:center\">TSNIT</th>";
						title = title + "<th colspan=\"2\" style=\"text-align:center\"> </th>";
						title = title + "<th colspan=\"2\" style=\"text-align:center\">Macula</th>";
						
						title = title + "</tr>";
						$($(targetTbl).find("thead")).prepend(title);
					}
				}
				<%}
					if(fieldList.contains("VF")){
				%>
				var vfElm = $("h5").filter(function(){
					if(this.innerHTML.indexOf("VF") != -1){
						return true;
					}
					return false;
				});
				if(vfElm != null && vfElm.length > 0){
					vfElm = vfElm[0];
					var targetTbl = $(vfElm).parent().children("table");
					if(targetTbl != null){
						var title = "<tr><th class=\"centered\"></th>";
						title = title + "<th colspan=\"2\" style=\"text-align:center\">VF</th>";
						title = title + "</tr>";
						$($(targetTbl).find("thead")).prepend(title);
					}
				}
				<%
					}
				}%>
			}
			
			function adjust_title(){
				var title = ["Glasses History","VISION ASSESSMENT","STEREO VISION","VISION MEASUREMENT","INTRAOCULAR PRESSURE","REFRACTIVE","OTHER EXAM","DUCTION/DIPLOPIA TESTING","DEVIATION MEASUREMENT","EXTERNAL/ORBIT","EYELID/NASOLACRIMAL DUCT","EYELID MEASUREMENT","ANTERIOR SEGMENT","POSTERIOR SEGMENT", "OCT", "VF"];
				var i = 0;
				for(i = 0; i < title.length; i ++){
					var Elm = $("h5").filter(function(){
						if(this.innerHTML.indexOf(title[i]) != -1){
							return true;
						}
						return false;
					});
					if(Elm != null && Elm.length > 0){
						Elm = Elm[0];
						Elm.innerHTML = title[i] + ":";
						var targetSpan = $(Elm).parent().children("span");
						if(targetSpan != null && targetSpan.length > 0){
							if(targetSpan.length == 4){
								var addspan = targetSpan[0];							
								targetSpan[0].style.display = "none";
								targetSpan[2].style.display = "none";
								targetSpan[3].style.display = "none";
								addspan.style.display = "inline";
								$(Elm).append(addspan);
								
								var targetTbl = $(Elm).parent().children("table"); 
								var tabWidth = $(targetTbl).innerWidth();
								targetSpan[1].style.width = tabWidth;
								Elm.style.width = tabWidth;
							}
							if(targetSpan.length == 2){
								var addspan = targetSpan[0];							
								targetSpan[0].style.display = "none";
								targetSpan[1].style.display = "none";							
								addspan.style.display = "inline";
								$(Elm).append(addspan);
								
								var targetTbl = $(Elm).parent().children("table"); 
								var tabWidth = $(targetTbl).innerWidth();
								Elm.style.width = tabWidth;
							}
						}	
					}
				}
			}
		</script>
	</head>

	<body onload="insert_title();adjust_title();">
		<form action="<%=request.getContextPath()%>/eyeform/ExaminationHistory.do" method="POST" id="inputForm" name="inputForm">
		<input type="hidden" name="method" value="query"/>
		<input type="hidden" name="demographicNo" value="<c:out value="${demographic.demographicNo}"/>"/>
		<input type="hidden" name="refPage" value="<c:out value="${refPage}"/>"/>
		<c:forEach var="field" items="${fields}">
			<input type="hidden" name="fromlist2" value="<c:out value="${field}"/>"/>
		</c:forEach>
		<table class="common">
	  		<tr>
	  			<td>
	  				<h4 style="background-color: #69c">Demographic name:<c:out value="${demographic.formattedName}"/></h4>
	  			</td>
	  		</tr>
			<tr>
				<table style="background-color: #efefff">
					<tr>
						<td>Start Date:</td>
						<td>
			 				<input type="text" class="plain" name="sdate" id="sdate" size="12" onfocus="this.blur()" readonly="readonly" value="<%=sdate%>"/>
			 				<img src="<%=request.getContextPath()%>/images/cal.gif" id="sdate_cal">
			 				<script type="text/javascript">
								Calendar.setup({ inputField : "sdate", ifFormat : "%Y-%m-%d", showsTime :false, button : "sdate_cal", singleClick : true, step : 1 });
							</script>
		    			</td>
						<td>End Date:</td>
						<td>
							<input type="text" class="plain" name="edate" id="edate" size="12" onfocus="this.blur()" readonly="readonly" value="<%=edate%>"/>
							<img src="<%=request.getContextPath()%>/images/cal.gif" id="edate_cal">
			 				<script type="text/javascript">
								Calendar.setup({ inputField : "edate", ifFormat : "%Y-%m-%d", showsTime :false, button : "edate_cal", singleClick : true, step : 1 });
							</script>
						</td>
						<td></td>
						<td>
							<input type="submit" onclick="this.form.refPage.value=null" value="Search"/>
						</td>
					</tr>
				</table>
				</tr>
</table>

<table class="display">
<%
//if(fieldList.contains("Glasses Rx")){
if(fieldList1.contains("GLASSES HISTORY")){
%>
<tr>
<td><h5>Glasses History</h5>
	<display:table name="glasses" requestURI="/eyeform/ExaminationHistory.do" class="display"  id="glassesMap" pagesize="5">
		<display:column title="Type" style="width:30px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${glassesMap.gl_type}"/>
		</display:column>
		<display:column title="OD s" style="width:30px;white-space: nowrap;">
			<c:out value="${glassesMap.gl_rs}"/>
		</display:column>
		<display:column title="OD c" style="width:30px;white-space: nowrap;">
			<c:out value="${glassesMap.gl_rc}"/>
		</display:column>
		<display:column title="OD x" style="width:30px;white-space: nowrap;">
			<c:out value="${glassesMap.gl_rx}"/>
		</display:column>
		<display:column title="OD add" style="width:30px;white-space: nowrap;">
			<c:out value="${glassesMap.gl_ra}"/>
		</display:column>
		<display:column title="OD prism" style="width:30px;white-space: nowrap;">
			<c:out value="${glassesMap.gl_rp}"/>
		</display:column>
		<display:column title="Os s" style="width:30px;white-space: nowrap;">
			<c:out value="${glassesMap.gl_ls}"/>
		</display:column>
		<display:column title="Os c" style="width:30px;white-space: nowrap;">
			<c:out value="${glassesMap.gl_lc}"/>
		</display:column>
		<display:column title="Os x" style="width:30px;white-space: nowrap;">
			<c:out value="${glassesMap.gl_lx}"/>
		</display:column>
		<display:column title="Os add" style="width:30px;white-space: nowrap;">
			<c:out value="${glassesMap.gl_la}"/>
		</display:column>
		<display:column title="Os prism" style="width:30px;white-space: nowrap;">
			<c:out value="${glassesMap.gl_lp}"/>
		</display:column>
		<display:column title="date" style="width:30px;white-space: nowrap;">
			<c:out value="${glassesMap.gl_date}"/>
		</display:column>
		<display:column title="note" style="width:30px;white-space: nowrap;">
			<c:out value="${glassesMap.gl_note}"/>
		</display:column>
	</display:table>
</td>
</tr>
<%
}
//if(fieldList.contains("Distance vision (sc)")){
if(fieldList1.contains("VISION ASSESSMENT")){
%>
<tr>
<td><h5>VISION ASSESSMENT</h5>
	<display:table name="distance_vision" requestURI="/eyeform/ExaminationHistory.do" class="display"  id="distance_visionMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${distance_visionMap.date}"/>
		</display:column>
	<%if(fieldList.contains("Distance vision (sc)")){%>
		<display:column title="OD dsc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_rdsc}"/>
		</display:column>
		<display:column title="OS dsc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_ldsc}"/>
		</display:column>
		<display:column title="OU dsc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_dsc}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Distance vision (cc)")){
	%>
		<display:column title="OD dcc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_rdcc}"/>
		</display:column>
		<display:column title="OS dcc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_ldcc}"/>
		</display:column>
		<display:column title="OU dcc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_dcc}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Distance vision (ph)")){
	%>
		<display:column title="OD ph" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_rph}"/>
		</display:column>
		<display:column title="OS ph" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_lph}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Intermediate vision (sc)")){
	%>
		<display:column title="OD isc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_risc}"/>
		</display:column>
		<display:column title="OS isc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_lisc}"/>
		</display:column>
		<display:column title="OU isc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_isc}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Intermediate vision (cc)")){
	%>
		<display:column title="OD icc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_ricc}"/>
		</display:column>
		<display:column title="OS icc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_licc}"/>
		</display:column>
		<display:column title="OU icc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_icc}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Near vision (sc)")){
	%>
		<display:column title="OD nsc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_rnsc}"/>
		</display:column>
		<display:column title="OS nsc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_lnsc}"/>
		</display:column>
		<display:column title="OU nsc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_nsc}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Near vision (cc)")){
	%>
		<display:column title="OD ncc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_rncc}"/>
		</display:column>
		<display:column title="OS ncc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_lncc}"/>
		</display:column>
		<display:column title="OU ncc" style="width:30px;white-space: nowrap;">
			<c:out value="${distance_visionMap.v_ncc}"/>
		</display:column>
	<%}%>
	</display:table>
</td>
</tr>
<%
}
//if(fieldList.contains("Fly test")){
if(fieldList1.contains("STEREO VISION")){
%>
<tr>
<td><h5>STEREO VISION</h5>
	<display:table name="fly_test" requestURI="/eyeform/ExaminationHistory.do" class="display"  id="flyMap" pagesize="5">
		<display:column title="Date" style="width:30px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${flyMap.date}"/>
		</display:column>
	<%
	if(fieldList.contains("Fly test")){
	%>
		<display:column title="Fly test" style="width:30px;white-space: nowrap;">
			<c:out value="${flyMap.v_fly}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Stereo-acuity")){
	%>
		<display:column title="Stereo-acuity" style="width:30px;white-space: nowrap;">
			<c:out value="${flyMap.v_stereo}"/>
		</display:column>
	<%}
	if(null != props.getProperty("eyeform_optometry_device") && props.getProperty("eyeform_optometry_device").equals("yes")){
		if(fieldList.contains("PD")){
	%>
		<display:column title="PD" style="width:30px;white-space: nowrap;">
			<c:out value="${flyMap.v_pd}"/>
		</display:column>	
	<%	
		}
	}
	%>
	
	</display:table>
</td>
</tr>
<%
}
//if(fieldList.contains("Keratometry")){
if(fieldList1.contains("VISION MEASUREMENT")){
%>

<tr>
<td><h5>VISION MEASUREMENT</h5>
	<display:table name="keratometry" requestURI="/eyeform/ExaminationHistory.do" class="display" id="measurementMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${measurementMap.date}"/>
		</display:column>
	<%if(fieldList.contains("Keratometry")){%>
		<display:column title="OD k1" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rk1}"/>
		</display:column>
		<display:column title="OD k2" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rk2}"/>
		</display:column>
		<display:column title="OD kx" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rkx}"/>
		</display:column>
		<display:column title="OS k1" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lk1}"/>
		</display:column>
		<display:column title="OS k2" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lk2}"/>
		</display:column>
		<display:column title="OS kx" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lkx}"/>
		</display:column>
	<%
	}
	if(null != props.getProperty("eyeform_optometry_device") && props.getProperty("eyeform_optometry_device").equals("yes")){
		if(fieldList.contains("Refraction")){
	%>
		<display:column title="OD r1" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rr1}"/>
		</display:column>
		<display:column title="OD r2" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rr2}"/>
		</display:column>
		<display:column title="OS r1" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lr1}"/>
		</display:column>
		<display:column title="OS r2" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lr2}"/>
		</display:column>
	<%
		}
	}
	if(null != props.getProperty("eyeform_vision_measurement_has_two_AR_M") && props.getProperty("eyeform_vision_measurement_has_two_AR_M").equals("yes")){
		if(fieldList.contains("Auto-refraction(-)")){
	%>
		<display:column title="OD as" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rs}"/>
		</display:column>
		<display:column title="OD ac" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rc}"/>
		</display:column>
		<display:column title="OD ax" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rx}"/>
		</display:column>
		<display:column title="OD ar" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rar}"/>
		</display:column>
		<display:column title="OS as" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ls}"/>
		</display:column>
		<display:column title="OS ac" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lc}"/>
		</display:column>
		<display:column title="OS ax" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lx}"/>
		</display:column>
		<display:column title="OS ar" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lar}"/>
		</display:column>
	<%		
		}
		if(fieldList.contains("Auto-refraction(+)")){
	%>
		<display:column title="OD as" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rs_add}"/>
		</display:column>
		<display:column title="OD ac" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rc_add}"/>
		</display:column>
		<display:column title="OD ax" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rx_add}"/>
		</display:column>
		<display:column title="OD ar" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rar_add}"/>
		</display:column>
		<display:column title="OS as" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ls_add}"/>
		</display:column>
		<display:column title="OS ac" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lc_add}"/>
		</display:column>
		<display:column title="OS ax" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lx_add}"/>
		</display:column>
		<display:column title="OS ar" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lar_add}"/>
		</display:column>
	<%	
		}
		if(fieldList.contains("Manifest distance(-)")){
	%>
		<display:column title="OD ds" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rds}"/>
		</display:column>
		<display:column title="OD dc" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rdc}"/>
		</display:column>
		<display:column title="OD dx" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rdx}"/>
		</display:column>
		<display:column title="OD dp" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rdp}"/>
		</display:column>
		<display:column title="OD dv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rdv}"/>
		</display:column>
		<display:column title="OS ds" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lds}"/>
		</display:column>
		<display:column title="OS dc" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ldc}"/>
		</display:column>
		<display:column title="OS dx" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ldx}"/>
		</display:column>
		<display:column title="OS dp" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ldp}"/>
		</display:column>
		<display:column title="OS dv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ldv}"/>
		</display:column>
		<display:column title="OU dv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_dv}"/>
		</display:column>
	<%		
		}
		if(fieldList.contains("Manifest distance(+)")){
	%>
		<display:column title="OD ds" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rds_add}"/>
		</display:column>
		<display:column title="OD dc" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rdc_add}"/>
		</display:column>
		<display:column title="OD dx" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rdx_add}"/>
		</display:column>
		<display:column title="OD dp" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rdp_add}"/>
		</display:column>
		<display:column title="OD dv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rdv_add}"/>
		</display:column>
		<display:column title="OS ds" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lds_add}"/>
		</display:column>
		<display:column title="OS dc" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ldc_add}"/>
		</display:column>
		<display:column title="OS dx" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ldx_add}"/>
		</display:column>
		<display:column title="OS dp" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ldp_add}"/>
		</display:column>
		<display:column title="OS dv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ldv_add}"/>
		</display:column>
		<display:column title="OU dv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_dv_add}"/>
		</display:column>
	<%	
		}
	}else{
		if(fieldList.contains("Auto-refraction")){
	%>
		<display:column title="OD as" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rs}"/>
		</display:column>
		<display:column title="OD ac" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rc}"/>
		</display:column>
		<display:column title="OD ax" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rx}"/>
		</display:column>
		<display:column title="OD ar" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rar}"/>
		</display:column>
		<display:column title="OS as" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ls}"/>
		</display:column>
		<display:column title="OS ac" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lc}"/>
		</display:column>
		<display:column title="OS ax" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lx}"/>
		</display:column>
		<display:column title="OS ar" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lar}"/>
		</display:column>
	<%		
		}
		if(fieldList.contains("Manifest distance")){
	%>
		<display:column title="OD ds" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rds}"/>
		</display:column>
		<display:column title="OD dc" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rdc}"/>
		</display:column>
		<display:column title="OD dx" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rdx}"/>
		</display:column>
		<display:column title="OD dp" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rdp}"/>
		</display:column>
		<display:column title="OD dv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rdv}"/>
		</display:column>
		<display:column title="OS ds" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lds}"/>
		</display:column>
		<display:column title="OS dc" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ldc}"/>
		</display:column>
		<display:column title="OS dx" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ldx}"/>
		</display:column>
		<display:column title="OS dp" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ldp}"/>
		</display:column>
		<display:column title="OS dv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ldv}"/>
		</display:column>
		<display:column title="OU dv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_dv}"/>
		</display:column>
	<%	}
		}
	if(null != props.getProperty("eyeform_optometry_device") && props.getProperty("eyeform_optometry_device").equals("yes")){
		if(fieldList.contains("Finial")){
	%>
		<display:column title="OD s" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rs_finial}"/>
		</display:column>
		<display:column title="OD c" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rc_finial}"/>
		</display:column>
		<display:column title="OD x" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rx_finial}"/>
		</display:column>
		<display:column title="OS s" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_ls_finial}"/>
		</display:column>
		<display:column title="OS c" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lc_finial}"/>
		</display:column>
		<display:column title="OS x" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lx_finial}"/>
		</display:column>
	<%
		}
	}
	if(fieldList.contains("Manifest near")){
	%>
		<display:column title="OD ns" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rns}"/>
		</display:column>
		<display:column title="OD nc" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rnc}"/>
		</display:column>
		<display:column title="OD nx" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rnx}"/>
		</display:column>
		<display:column title="OD np" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rnp}"/>
		</display:column>
		<display:column title="OD nv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rnv}"/>
		</display:column>
		<display:column title="OS ns" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lns}"/>
		</display:column>
		<display:column title="OS nc" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lnc}"/>
		</display:column>
		<display:column title="OS nx" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lnx}"/>
		</display:column>
		<display:column title="OS np" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lnp}"/>
		</display:column>
		<display:column title="OS nv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lnv}"/>
		</display:column>
		<display:column title="OU nv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_nv}"/>
		</display:column>
		<%if(null != props.getProperty("eyeform_optometry_device") && props.getProperty("eyeform_optometry_device").equals("yes")){ %>
		<display:column title="OU alt add" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_alt_add}"/>
		</display:column>
		<%} %>
	<%
	}
	if(fieldList.contains("Cycloplegic refraction")){
	%>
		<display:column title="OD cs" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rcs}"/>
		</display:column>
		<display:column title="OD cc" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rcc}"/>
		</display:column>
		<display:column title="OD cx" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rcx}"/>
		</display:column>
		<display:column title="OD cp" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rcp}"/>
		</display:column>
		<display:column title="OD cv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_rcv}"/>
		</display:column>
		<display:column title="OS cs" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lcs}"/>
		</display:column>
		<display:column title="OS cc" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lcc}"/>
		</display:column>
		<display:column title="OS cx" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lcx}"/>
		</display:column>
		<display:column title="OS cp" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lcp}"/>
		</display:column>
		<display:column title="OS cv" style="width:30px;white-space: nowrap;">
			<c:out value="${measurementMap.v_lcv}"/>
		</display:column>
	<%}%>
	</display:table>
</td>
</tr>
<%
}
//if(fieldList.contains("NCT")){
if(fieldList1.contains("INTRAOCULAR PRESSURE")){
%>
<tr>
<td><h5>INTRAOCULAR PRESSURE</h5>
	<display:table name="nct" requestURI="/eyeform/ExaminationHistory.do" class="display" id="nctMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${nctMap.date}"/>
		</display:column>
	<%if(fieldList.contains("NCT")){%>	
		<display:column title="OD n" style="width:30px;white-space: nowrap;">
			<c:out value="${nctMap.iop_rn}"/>
		</display:column>
		<display:column title="OS n" style="width:30px;white-space: nowrap;">
			<c:out value="${nctMap.iop_ln}"/>
		</display:column>
		<display:column title="ntime" style="width:30px;white-space: nowrap;">
			<c:out value="${nctMap.iop_ntime}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Applanation")){
	%>
		<display:column title="OD a" style="width:30px;white-space: nowrap;">
			<c:out value="${nctMap.iop_ra}"/>
		</display:column>
		<display:column title="OS a" style="width:30px;white-space: nowrap;">
			<c:out value="${nctMap.iop_la}"/>
		</display:column>
		<display:column title="atime" style="width:30px;white-space: nowrap;">
			<c:out value="${nctMap.iop_atime}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Central corneal thickness")){
	%>
		<display:column title="OD cct" style="width:30px;white-space: nowrap;">
			<c:out value="${nctMap.cct_r}"/>
		</display:column>
		<display:column title="OS cct" style="width:30px;white-space: nowrap;">
			<c:out value="${nctMap.cct_l}"/>
		</display:column>
	<%}%>
	</display:table>
</td>
</tr>
<%
}
//if(fieldList.contains("Dominance")){
if(fieldList1.contains("REFRACTIVE")){
%>
<tr>
<td><h5>REFRACTIVE</h5>
	<display:table name="dominance" requestURI="/eyeform/ExaminationHistory.do" class="display" id="dominanceMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${dominanceMap.date}"/>
		</display:column>
	<%if(fieldList.contains("Dominance")){%>
		<display:column title="OD dom" style="width:30px;white-space: nowrap;">
			<c:out value="${dominanceMap.ref_rdom}"/>
		</display:column>
		<display:column title="OS dom" style="width:30px;white-space: nowrap;">
			<c:out value="${dominanceMap.ref_ldom}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Mesopic pupil size")){
	%>
		<display:column title="OD pdim" style="width:30px;white-space: nowrap;">
			<c:out value="${dominanceMap.ref_rpdim}"/>
		</display:column>
		<display:column title="OS pdim" style="width:30px;white-space: nowrap;">
			<c:out value="${dominanceMap.ref_lpdim}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Angle Kappa")){
	%>
		<display:column title="OD kappa" style="width:30px;white-space: nowrap;">
			<c:out value="${dominanceMap.ref_rkappa}"/>
		</display:column>
		<display:column title="OS kappa" style="width:30px;white-space: nowrap;">
			<c:out value="${dominanceMap.ref_lkappa}"/>
		</display:column>
	<%}%>
	</display:table>
</td>
</tr>
<%
}
//if(fieldList.contains("Colour vision")){
if(fieldList1.contains("OTHER EXAM")){
%>
<tr>
<td><h5>OTHER EXAM</h5>
	<display:table name="colour_vision" requestURI="/eyeform/ExaminationHistory.do" class="display" id="colour_visionMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${colour_visionMap.date}"/>
		</display:column>
	<%if(fieldList.contains("Colour vision")){%>
		<display:column title="OD colour" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_rcolour}"/>
		</display:column>
		<display:column title="OS colour" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_lcolour}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Pupil")){
	%>
		<display:column title="OD pupil" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_rpupil}"/>
		</display:column>
		<display:column title="OS pupil" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_lpupil}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Amsler grid")){
	%>
		<display:column title="OD amsler" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_ramsler}"/>
		</display:column>
		<display:column title="OS amsler" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_lamsler}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Potential acuity meter")){
	%>
		<display:column title="OD pam" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_rpam}"/>
		</display:column>
		<display:column title="OS pam" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_lpam}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Confrontation fields")){
	%>
		<display:column title="OD conf" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_rconf}"/>
		</display:column>
		<display:column title="OS conf" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_lconf}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Maddox rod")){
	%>
		<display:column title="maddox" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_mad}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Bagolini test")){
	%>
		<display:column title="bagolini" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_bag}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Worth 4 Dot (distance)")){
	%>
		<display:column title="distance" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_w4dd}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Worth 4 Dot (near)")){
	%>
		<display:column title="near" style="width:30px;white-space: nowrap;">
			<c:out value="${colour_visionMap.o_w4dn}"/>
		</display:column>
	<%}%>
	</display:table>
</td>
</tr>
<%
}
//if(fieldList.contains("DUCTION/DIPLOPIA TESTING")){
if(fieldList1.contains("DUCTION/DIPLOPIA TESTING")){
%>
<tr>
<td><h5>DUCTION/DIPLOPIA TESTING</h5>
	<display:table name="ductlion" requestURI="/eyeform/ExaminationHistory.do" class="display" id="ductlionMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${ductlionMap.date}"/>
		</display:column>
		<display:column title="OD" headerClass="centered">
			<table style="border:0px;font-size: 10pt">
			<tr>
			<td class="inner"><c:out value="${ductlionMap.duc_rur}"/></td>
			<td class="inner"><c:out value="${ductlionMap.duc_rul}"/></td>
			</tr>
			<tr>
			<td class="inner"><c:out value="${ductlionMap.duc_rr}"/></td>
			<td class="inner"><c:out value="${ductlionMap.duc_rl}"/></td>
			</tr>
			<tr>
			<td class="inner"><c:out value="${ductlionMap.duc_rdr}"/></td>
			<td class="inner"><c:out value="${ductlionMap.duc_rdl}"/></td>
			</tr>
			</table>
		</display:column>
		<display:column title="OS" headerClass="centered">
			<table style="border:0px;font-size: 10pt">
			<tr>
			<td class="inner"><c:out value="${ductlionMap.duc_lur}"/></td>
			<td class="inner"><c:out value="${ductlionMap.duc_lul}"/></td>
			</tr>
			<tr>
			<td class="inner"><c:out value="${ductlionMap.duc_lr}"/></td>
			<td class="inner"><c:out value="${ductlionMap.duc_ll}"/></td>
			</tr>
			<tr>
			<td class="inner"><c:out value="${ductlionMap.duc_ldr}"/></td>
			<td class="inner"><c:out value="${ductlionMap.duc_ldl}"/></td>
			</tr>
			</table>
		</display:column>
		<display:column title="OU" headerClass="centered">
			<table style="border:0px;font-size: 10pt">
			<tr>
			<td class="inner"><c:out value="${ductlionMap.dip_ur}"/></td>
			<td class="inner"><c:out value="${ductlionMap.dip_u}"/></td>
			</tr>
			<tr>
			<td class="inner"><c:out value="${ductlionMap.dip_r}"/></td>
			<td class="inner"><c:out value="${ductlionMap.dip_p}"/></td>
			</tr>
			<tr>
			<td class="inner"><c:out value="${ductlionMap.dip_dr}"/></td>
			<td class="inner"><c:out value="${ductlionMap.dip_d}"/></td>
			</tr>
			</table>
		</display:column>
	</display:table>
</td>
</tr>
<%
}
//if(fieldList.contains("Primary gaze")){
if(fieldList1.contains("DEVIATION MEASUREMENT")){
%>
<tr>
<td><h5>DEVIATION MEASUREMENT</h5>
	<display:table name="primary" requestURI="/eyeform/ExaminationHistory.do" class="display" id="primaryMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${primaryMap.date}"/>
		</display:column>
	<%
	if(fieldList.contains("Primary gaze") || fieldList.contains("Up gaze") || fieldList.contains("Down gaze") || fieldList.contains("Right gaze") || fieldList.contains("Left gaze") || fieldList.contains("Right head tilt") || fieldList.contains("Left head tilt")){
	%>
		<display:column style="width:60px;">
			<table style="border:0px;font-size: 10pt">
			<tr>
			<td class="inner"></td>
			<td class="inner"><c:out value="${primaryMap.dev_u}"/></td>
			<td class="inner"></td>
			</tr>
			<tr>
			<td class="inner"><c:out value="${primaryMap.dev_r}"/></td>
			<td class="inner"><c:out value="${primaryMap.dev_p}"/></td>
			<td class="inner"><c:out value="${primaryMap.dev_l}"/></td>
			</tr>
			<tr>
			<td class="inner"><c:out value="${primaryMap.dev_rt}"/></td>
			<td class="inner"><c:out value="${primaryMap.dev_d}"/></td>
			<td class="inner"><c:out value="${primaryMap.dev_lt}"/></td>
			</tr>
			</table>
		</display:column>
	<%
	}
	if(fieldList.contains("Near")){
	%>
		<display:column title="Near" style="width:30px;white-space: nowrap;">
			<c:out value="${primaryMap.dev_near}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Near with +3D add")){
	%>
		<display:column title="Near with +3D add" style="width:30px;white-space: nowrap;">
			<c:out value="${primaryMap.dev_plus3}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Far distance")){
	%>
		<display:column title="Far distance" style="width:30px;white-space: nowrap;">
			<c:out value="${primaryMap.dev_far}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("NPC")){
	%>
		<display:column title="NPC" style="width:30px;white-space: nowrap;">
			<c:out value="${primaryMap.dev_npc}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("A.O.ACC")){
	%>
		<display:column title="A.O.ACC OD" style="width:30px;white-space: nowrap;">
			<c:out value="${primaryMap.dev_aoacc_od}"/>
		</display:column>
		<display:column title="A.O.ACC OS" style="width:30px;white-space: nowrap;">
			<c:out value="${primaryMap.dev_aoacc_os}"/>
		</display:column>
	<%}%>
	</display:table>
</td>
</tr>
<%
}
//if(fieldList.contains("Retropulsion")){
if(fieldList1.contains("EXTERNAL/ORBIT")){
%>
<tr>
<td><h5>EXTERNAL/ORBIT</h5>
	<display:table name="retropulsion" requestURI="/eyeform/ExaminationHistory.do" class="display" id="retropulsionMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${retropulsionMap.date}"/>
		</display:column>
	<%if(fieldList.contains("Face")){%>
		<display:column title="OD face" style="width:30px;white-space: nowrap;">
			<c:out value="${retropulsionMap.ext_rface}"/>
		</display:column>
		<display:column title="OS face" style="width:30px;white-space: nowrap;">
			<c:out value="${retropulsionMap.ext_lface}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Retropulsion")){
	%>
		<display:column title="OD retro" style="width:30px;white-space: nowrap;">
			<c:out value="${retropulsionMap.ext_rretro}"/>
		</display:column>
		<display:column title="OS retro" style="width:30px;white-space: nowrap;">
			<c:out value="${retropulsionMap.ext_lretro}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Hertel")){
	%>
		<display:column title="OD hertel" style="width:30px;white-space: nowrap;">
			<c:out value="${retropulsionMap.ext_rhertel}"/>
		</display:column>
		<display:column title="OS hertel" style="width:30px;white-space: nowrap;">
			<c:out value="${retropulsionMap.ext_lhertel}"/>
		</display:column>
	<%}%>
	</display:table>
</td>
</tr>
<%
}
//if(fieldList.contains("Upper lid")){
if(fieldList1.contains("EYELID/NASOLACRIMAL DUCT")){
%>
<tr>
<td><h5>EYELID/NASOLACRIMAL DUCT</h5>
	<display:table name="upper" requestURI="/eyeform/ExaminationHistory.do" class="display" id="upperMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${upperMap.date}"/>
		</display:column>
	<%if(fieldList.contains("Upper lid")){%>
		<display:column title="OD ul" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_rul}"/>
		</display:column>
		<display:column title="OS ul" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_lul}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Lower lid")){
	%>
		<display:column title="OD ll" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_rll}"/>
		</display:column>
		<display:column title="OS ll" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_lll}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Lacrimal lake")){
	%>
		<display:column title="OD lake" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_rlake}"/>
		</display:column>
		<display:column title="OS lake" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_llake}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Lacrimal irrigation")){
	%>
		<display:column title="OD irrig" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_rirrig}"/>
		</display:column>
		<display:column title="OS irrig" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_lirrig}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Punctum")){
	%>
		<display:column title="OD punc" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_rpunc}"/>
		</display:column>
		<display:column title="OS punc" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_lpunc}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Nasolacrimal duct")){
	%>
		<display:column title="OD nld" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_rnld}"/>
		</display:column>
		<display:column title="OS nld" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_lnld}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Dye disappearance")){
	%>
		<display:column title="OD dye" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_rdye}"/>
		</display:column>
		<display:column title="OS dye" style="width:30px;white-space: nowrap;">
			<c:out value="${upperMap.ext_ldye}"/>
		</display:column>
	<%}%>
	</display:table>
</td>
</tr>
<%
}
//if(fieldList.contains("Margin reflex distance")){
if(fieldList1.contains("EYELID MEASUREMENT")){
%>
<tr>
<td><h5>EYELID MEASUREMENT</h5>
	<display:table name="margin" requestURI="/eyeform/ExaminationHistory.do" class="display" id="marginMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${marginMap.date}"/>
		</display:column>
	<%if(fieldList.contains("Margin reflex distance")){%>
		<display:column title="OD mrd" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_rmrd}"/>
		</display:column>
		<display:column title="OS mrd" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_lmrd}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Inferior scleral show")){
	%>
		<display:column title="OD iss" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_riss}"/>
		</display:column>
		<display:column title="OS iss" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_liss}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Levator function")){
	%>
		<display:column title="OD lev" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_rlev}"/>
		</display:column>
		<display:column title="OS lev" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_llev}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Lagophthalmos")){
	%>
		<display:column title="OD lag" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_rlag}"/>
		</display:column>
		<display:column title="OS lag" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_llag}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Blink reflex")){
	%>
		<display:column title="OD blink" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_rblink}"/>
		</display:column>
		<display:column title="OS blink" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_lblink}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Cranial Nerve VII function")){
	%>
		<display:column title="OD cn7" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_rcn7}"/>
		</display:column>
		<display:column title="OS cn7" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_lcn7}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Bells phenomenon")){
	%>
		<display:column title="OD bell" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_rbell}"/>
		</display:column>
		<display:column title="OS bell" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_lbell}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Schirmer test")){
	%>
		<display:column title="OD schirm" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_rschirm}"/>
		</display:column>
		<display:column title="OS schirm" style="width:30px;white-space: nowrap;">
			<c:out value="${marginMap.lid_lschirm}"/>
		</display:column>
	<%}%>
	</display:table>
</td>
</tr>
<%
}
//if(fieldList.contains("Cornea")){
if(fieldList1.contains("ANTERIOR SEGMENT")){
%>
<tr>
<td><h5>ANTERIOR SEGMENT</h5>
	<display:table name="cornea" requestURI="/eyeform/ExaminationHistory.do" class="display" id="corneaMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${corneaMap.date}"/>
		</display:column>
	<%if(fieldList.contains("Cornea")){%>
		<display:column title="OD cornea" style="width:30px;white-space: nowrap;">
			<c:out value="${corneaMap.a_rk}"/>
		</display:column>
		<display:column title="OS cornea" style="width:30px;white-space: nowrap;">
			<c:out value="${corneaMap.a_lk}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Conjunctiva/Sclera")){
	%>
		<display:column title="OD conj" style="width:30px;white-space: nowrap;">
			<c:out value="${corneaMap.a_rconj}"/>
		</display:column>
		<display:column title="OS conj" style="width:30px;white-space: nowrap;">
			<c:out value="${corneaMap.a_lconj}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Anterior chamber")){
	%>
		<display:column title="OD ac" style="width:30px;white-space: nowrap;">
			<c:out value="${corneaMap.a_rac}"/>
		</display:column>
		<display:column title="OS ac" style="width:30px;white-space: nowrap;">
			<c:out value="${corneaMap.a_lac}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Angle")){
	%>
		<display:column title="OD" >
			<table border="1" style="font-size: 10pt">
			<tr>
			<td width="33%"></td>
			<td class="inner" width="34%"><c:out value="${corneaMap.a_rangle_1}"/></td>
			<td width="33%"></td>
			</tr>
			<tr>
			<td class="inner"><c:out value="${corneaMap.a_rangle_4}"/></td>
			<td class="inner"><c:out value="${corneaMap.a_rangle_3}"/></td>
			<td class="inner"><c:out value="${corneaMap.a_rangle_2}"/></td>
			</tr>
			<tr>
			<td></td>
			<td class="inner"><c:out value="${corneaMap.a_rangle_5}"/></td>
			<td></td>
			</tr>
			</table>
		</display:column>
		<display:column title="OS" >
			<table border="1" style="font-size: 10pt">
			<tr>
			<td width="33%"></td>
			<td class="inner" width="34%"><c:out value="${corneaMap.a_langle_1}"/></td>
			<td width="33%"></td>
			</tr>
			<tr>
			<td class="inner"><c:out value="${corneaMap.a_langle_4}"/></td>
			<td class="inner"><c:out value="${corneaMap.a_langle_3}"/></td>
			<td class="inner"><c:out value="${corneaMap.a_langle_2}"/></td>
			</tr>
			<tr>
			<td width="33%"></td>
			<td class="inner" width="34%"><c:out value="${corneaMap.a_langle_5}"/></td>
			<td width="33%"></td>
			</tr>
			</table>
		</display:column>
	<%
	}
	if(fieldList.contains("Iris")){
	%>
		<display:column title="OD iris" style="width:30px;white-space: nowrap;">
			<c:out value="${corneaMap.a_riris}"/>
		</display:column>
		<display:column title="OS iris" style="width:30px;white-space: nowrap;">
			<c:out value="${corneaMap.a_liris}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Lens")){
	%>
		<display:column title="OD lens" style="width:30px;white-space: nowrap;">
			<c:out value="${corneaMap.a_rlens}"/>
		</display:column>
		<display:column title="OS lens" style="width:30px;white-space: nowrap;">
			<c:out value="${corneaMap.a_llens}"/>
		</display:column>
	<%}%>
	</display:table>
</td>
</tr>
<%
}
//if(fieldList.contains("Optic disc")){
if(fieldList1.contains("POSTERIOR SEGMENT")){
%>
<tr>
<td><h5>POSTERIOR SEGMENT</h5>
	<display:table name="optic" requestURI="/eyeform/ExaminationHistory.do" class="display" id="opticMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${opticMap.date}"/>
		</display:column>
	<%if(fieldList.contains("Optic disc")){%>
		<display:column title="OD disc" style="width:30px;white-space: nowrap;">
			<c:out value="${opticMap.p_rdisc}"/>
		</display:column>
		<display:column title="OS disc" style="width:30px;white-space: nowrap;">
			<c:out value="${opticMap.p_ldisc}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("C/D ratio")){
	%>
		<display:column title="OD cd" style="width:30px;white-space: nowrap;">
			<c:out value="${opticMap.p_rcd}"/>
		</display:column>
		<display:column title="OS cd" style="width:30px;white-space: nowrap;">
			<c:out value="${opticMap.p_lcd}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Macula")){
	%>
		<display:column title="OD mac" style="width:30px;white-space: nowrap;">
			<c:out value="${opticMap.p_rmac}"/>
		</display:column>
		<display:column title="OS mac" style="width:30px;white-space: nowrap;">
			<c:out value="${opticMap.p_lmac}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Retina")){
	%>
		<display:column title="OD ret" style="width:30px;white-space: nowrap;">
			<c:out value="${opticMap.p_rret}"/>
		</display:column>
		<display:column title="OS ret" style="width:30px;white-space: nowrap;">
			<c:out value="${opticMap.p_lret}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("Vitreous")){
	%>
		<display:column title="OD vit" style="width:30px;white-space: nowrap;">
			<c:out value="${opticMap.p_rvit}"/>
		</display:column>
		<display:column title="OS vit" style="width:30px;white-space: nowrap;">
			<c:out value="${opticMap.p_lvit}"/>
		</display:column>
	<%
	}
	if(fieldList.contains("CRT")){
	%>
		<display:column title="OD crt" style="width:30px;white-space: nowrap;">
			<c:out value="${opticMap.p_rcrt}"/>
		</display:column>
		<display:column title="OS crt" style="width:30px;white-space: nowrap;">
			<c:out value="${opticMap.p_lcrt}"/>
		</display:column>
	<%}%>
	</display:table>
</td>
</tr>
<%}%>
<%
if(null != props.getProperty("eyeform_optometry_device") && props.getProperty("eyeform_optometry_device").equals("yes")){ 
	if(fieldList1.contains("OCT")){
%>
<tr>
	<td>
	<h5>OCT</h5>
	<display:table name="oct" requestURI="/eyeform/ExaminationHistory.do" class="display" id="octMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${octMap.date}"/>
		</display:column>
		<display:column title="OD" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_s_l}"/>
		</display:column>
		<display:column title="OS" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_s_r}"/>
		</display:column>
		<display:column title="OD" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_i_l}"/>
		</display:column>
		<display:column title="OS" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_i_r}"/>
		</display:column>
		<display:column title="OD" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_tsnit_l_1}"/>
		</display:column>
		<display:column title="OD" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_tsnit_l_2}"/>
		</display:column>
		<display:column title="OD" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_tsnit_l_3}"/>
		</display:column>
		<display:column title="OD" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_tsnit_l_4}"/>
		</display:column>
		<display:column title="OS" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_tsnit_r_1}"/>
		</display:column>
		<display:column title="OS" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_tsnit_r_2}"/>
		</display:column>
		<display:column title="OS" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_tsnit_r_3}"/>
		</display:column>
		<display:column title="OS" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_tsnit_r_4}"/>
		</display:column>
		<display:column title="OD" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_l}"/>
		</display:column>
		<display:column title="OS" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_r}"/>
		</display:column>
		<display:column title="OD" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_macula_l}"/>
		</display:column>
		<display:column title="OS" style="width:30px;white-space: nowrap;">
			<c:out value="${octMap.oct_macula_r}"/>
		</display:column>
	</display:table>
	</td>
</tr>
<%
	}
	if(fieldList1.contains("VF")){
%>
<tr>
	<td>
	<h5>VF</h5>
	<display:table name="vf" requestURI="/eyeform/ExaminationHistory.do" class="display" id="vfMap" pagesize="5">
		<display:column title="Date" style="width:60px;white-space: nowrap;text-align:center" headerClass="centered">
			<c:out value="${vfMap.date}"/>
		</display:column>
		<display:column title="OD" style="width:30px;white-space: nowrap;">
			<c:out value="${vfMap.vf_l}"/>
		</display:column>
		<display:column title="OS" style="width:30px;white-space: nowrap;">
			<c:out value="${vfMap.vf_r}"/>
		</display:column>
	</display:table>
	</td>
</tr>
<%
	}
}
%>
</table>

	</body>
</html>
