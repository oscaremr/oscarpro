
<%--


    Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for
    Centre for Research on Inner City Health, St. Michael's Hospital,
    Toronto, Ontario, Canada

--%>


 <%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
 <%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@ page import="oscar.oscarEncounter.data.*, oscar.oscarProvider.data.*, oscar.util.UtilDateUtilities" %>
<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.util.SessionConstants" %>
<%@ page import="org.oscarehr.common.model.Demographic" %>
<%@ page import="org.oscarehr.common.dao.DemographicDao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
      String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
      boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_eChart" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_eChart");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}
%>


<%
    oscar.oscarEncounter.pageUtil.EctSessionBean bean = null;
    if((bean=(oscar.oscarEncounter.pageUtil.EctSessionBean)request.getSession().getAttribute("EctSessionBean"))==null) {
        response.sendRedirect("error.jsp");
        return;
    }
    boolean enhancedEnabled = "E".equals(request.getSession().getAttribute(SessionConstants.LOGIN_TYPE));
    String demoNo = bean.demographicNo;
    String providerNo = bean.providerNo;
    EctPatientData.Patient pd = new EctPatientData().getPatient(LoggedInInfo.getLoggedInInfoFromSession(request) , demoNo);
    String famDocName = "", famDocSurname = "", famDocColour, inverseUserColour, userColour;
    String user = (String) session.getAttribute("user");
    ProviderColourUpdater colourUpdater = new ProviderColourUpdater(user);
    userColour = colourUpdater.getColour();
    //we calculate inverse of provider colour for text
    int base = 16;
    if( userColour.length() == 0 )
        userColour = "#CCCCFF";   //default blue if no preference set

    int num = Integer.parseInt(userColour.substring(1), base);      //strip leading # sign and convert
    int inv = ~num;                                                 //get inverse
    inverseUserColour = Integer.toHexString(inv).substring(2);    //strip 2 leading digits as html colour codes are 24bits

    DemographicDao demographicDao = (DemographicDao) SpringUtils.getBean(DemographicDao.class);
    Demographic demographic = demographicDao.getDemographic(demoNo);

    if(bean.familyDoctorNo.equals("")) {
        famDocName = "";
        famDocSurname = "";
        famDocColour = "";
    }
    else {
        EctProviderData.Provider prov = new EctProviderData().getProvider(bean.familyDoctorNo);
        if(prov !=null) {
	        famDocName = prov.getFirstName();
	        famDocSurname = prov.getSurname();
        }
        colourUpdater = new ProviderColourUpdater(bean.familyDoctorNo);
        famDocColour = colourUpdater.getColour();
        if( famDocColour.length() == 0 )
            famDocColour = "#CCCCFF";
    }

    String patientName = pd.getFirstName()+" "+pd.getSurname();
    String patientAge = pd.getAge();
    String patientSex = pd.getSex();
    String pAge = Integer.toString(UtilDateUtilities.calcAge(bean.yearOfBirth,bean.monthOfBirth,bean.dateOfBirth));

    java.util.Locale vLocale =(java.util.Locale)session.getAttribute(org.apache.struts.Globals.LOCALE_KEY);
    
    //referring doctor
    org.oscarehr.common.dao.DemographicDao dao = (org.oscarehr.common.dao.DemographicDao)org.oscarehr.util.SpringUtils.getBean("demographicDao");
	org.oscarehr.common.model.Demographic d= dao.getDemographic(demoNo);
	String referralPhysician = d.getReferralPhysicianName();
	//appointment reason
	String apptNo = request.getParameter("appointmentNo");
	String reason = new String();
	if(apptNo != null && apptNo.length()>0 && !apptNo.equals("null")) {
		org.oscarehr.common.dao.OscarAppointmentDao appointmentDao = org.oscarehr.util.SpringUtils.getBean(org.oscarehr.common.dao.OscarAppointmentDao.class);
		org.oscarehr.common.model.Appointment a = appointmentDao.find(Integer.parseInt(apptNo));
		if(a != null) {
			reason = a.getReason();
		}
	}
	
	String patientLastName = d.getLastName()==null?"":d.getLastName();
	patientLastName = patientLastName.split(" ")[0];
    
	String patientFirstName = d.getFirstName()==null?"":d.getFirstName();
	patientFirstName = patientFirstName.split(" ")[0];
    
	String dob = d.getFormattedDob();
	//dob=dob.replace("-", "/");
	String synergy_url=oscar.OscarProperties.getInstance().getProperty("SYNERGY_URL");
	if(synergy_url == null)   	
    	synergy_url = "";
    synergy_url = synergy_url + "?lt=1&u=Oscar&ln="+patientLastName+"&fn="+patientFirstName+"&dob="+dob+"&n=" + (int)(Math.random() * 10000);
    %>

    <c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
   
    <span class="Header" style="color:<%=inverseUserColour%>; background-color:<%=userColour%>; font-weight:normal;">
        <%
            String winName = "Master" + bean.demographicNo;
            String url = "/demographic/demographiccontrol.jsp?demographic_no=" + bean.demographicNo + "&amp;displaymode=edit&amp;dboperation=search_detail";
        %>
        <span style="font-weight:bold;">
        	<a href="#" onClick="popupPage(700,1000,'<%=winName%>','<c:out value="${ctx}"/><%=url%>'); return false;" title="<bean:message key="provider.appointmentProviderAdminDay.msgMasterFile"/>"><%=bean.patientLastName %>, <%=bean.patientFirstName%> <%= null != bean.patientMiddleName && bean.patientMiddleName.length() > 0 ? bean.patientMiddleName : ""%> <%= null != bean.patientPreferredName && bean.patientPreferredName.length() > 0 ? ("(" + bean.patientPreferredName) + ")" : ""%></a> <%=bean.patientSex%> <%=bean.patientAge%> DOB: <%=bean.yearOfBirth%>-<%=bean.monthOfBirth%>-<%=bean.dateOfBirth%>
       	</span>  
	<bean:message key="oscarEncounter.Index.msgMRP"/>:&nbsp;<span style="font-weight:bold;"><%=Encode.forHtml(famDocName+" "+famDocSurname)%></span> 
	REF:&nbsp;<span style="font-weight:bold;"><%= referralPhysician %></span>
 	REASON:&nbsp;<span style="font-weight:bold;"><%=reason%></span>
 	<% if("yes".equalsIgnoreCase(oscar.OscarProperties.getInstance().getProperty("SYNERGY_LINK_ENABLE", "no"))) {%>
 		&nbsp; &nbsp;<span style="font-weight:bold;"><a href=<%=synergy_url%> target="_blank">SYNERGY</a></span>
 	<%} %>
 	<span style="font-weight:bold;">
        <% if ("true".equals(
                SystemPreferencesUtils.getPreferenceValueByName("echartDisplayInboxLink", ""))) { %>
            <% if (enhancedEnabled) { %>
                <a href="javascript:popupPage(900, 1100, 'inbox', '/<%= OscarProperties.getKaiemrDeployedContext() %>/#/inbox/?providerNo=-1&hin=<%= demographic.getHin() %>&displayInListMode=false');" >Inbox</a>
                &nbsp;&nbsp;
            <% } else { %>
                <a href="javascript:popupPage(900, 1100, 'inbox', '../dms/inboxManage.do?method=prepareForIndexPage&searchProviderAll=-1&isListView=false&hnum=<%= demographic.getHin() %>', 'Lab')">Inbox</a>
                &nbsp;&nbsp;
            <% } %>
        <% } %>
 	&nbsp; &nbsp;
 	<span style="font-weight:bold;">
 		<a href="javascript:void(0)" onClick="popupPage(700, 1000, 'OLIS Search', '<c:out value="${ctx}"/>/olis/Search.jsp?demographicNo=<%=demoNo %>&providerNo=<%=providerNo %>')"><bean:message key="olis.olisSearch" /></a>
 	</span>
 	&nbsp; &nbsp;
 	
 </span>
