<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ page import="oscar.OscarProperties"%>

<%@ include file="/casemgmt/taglibs.jsp" %>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html"%>
<%@page import="org.oscarehr.eyeform.model.EyeformSpecsHistory"%>
<%@page import="oscar.oscarEncounter.pageUtil.*,oscar.oscarEncounter.data.*,java.util.List,org.oscarehr.eyeform.model.EyeformSpecsHistory"%>
<%@ page import="org.oscarehr.util.*"%>
<%@ page import="org.oscarehr.eyeform.dao.*"%>
<%@ page import="org.oscarehr.common.dao.MeasurementDao"%>
<%@ page import="org.oscarehr.common.model.Measurement"%>
<%@ page import="java.util.*"%>


<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<link rel="stylesheet" href="<c:out value="${ctx}"/>/oscarEncounter/encounterStyles.css" type="text/css">
<%
	oscar.OscarProperties props1 = oscar.OscarProperties.getInstance();
	boolean glasses_hx = props1.getBooleanProperty("eyeform_glasses_hx_disable", "true");
	boolean vision_assessment = props1.getBooleanProperty("eyeform_vision_assessment_disable", "true");
	boolean vision_measurement = props1.getBooleanProperty("eyeform_vision_measurement_disable", "true");
	boolean refractive = props1.getBooleanProperty("eyeform_refractive_disable", "true");
	boolean iop = props1.getBooleanProperty("eyeform_iop_disable", "true");
	boolean other_exam = props1.getBooleanProperty("eyeform_other_exam_disable", "true");
	boolean duction = props1.getBooleanProperty("eyeform_duction_disable", "true");
	boolean deviation_measurement = props1.getBooleanProperty("eyeform_deviation_measurement_disable", "true");
	boolean external_orbit = props1.getBooleanProperty("eyeform_external_orbit_disable", "true");
	boolean eyelid_nld = props1.getBooleanProperty("eyeform_eyelid_nld_disable", "true");
	boolean eyelid_measurement = props1.getBooleanProperty("eyeform_eyelid_measurement_disable", "true");
	boolean posterior_segment = props1.getBooleanProperty("eyeform_posterior_segment_disable", "true");
	boolean anterior_segment = props1.getBooleanProperty("eyeform_anterior_segment_disable", "true");
	boolean eyeform_normal_value_open = props1.getBooleanProperty("eyeform_angle_normal_value_open", "true");
	
	String demo = request.getParameter("demographic_no");
	String appo = request.getParameter("appointment_no");
	int appo_now = 0;
	List<String> glassType = Arrays.asList("distance", "bifocal", "invis bfocal", "reading", "trifocal");
	String glass_show = "style=\"display:none\"";
	
    EyeformSpecsHistoryDao dao = (EyeformSpecsHistoryDao) SpringUtils.getBean(EyeformSpecsHistoryDao.class);

    Map<String, List<EyeformSpecsHistory>> specsList = new HashMap<String, List<EyeformSpecsHistory>>();
    for(int i = 0;i < glassType.size();i ++){
    	List<EyeformSpecsHistory> specs = dao.getRecentRecord(Integer.parseInt(demo), glassType.get(i));
    	if(null != specs && specs.size() > 0){
    		specsList.put(glassType.get(i), specs);
    	}
	}
	if(null != appo && appo.length() > 0 && !appo.equals("null")){
		appo_now = Integer.parseInt(appo);
	}
    Map<String, List<String>> specsValueList = new HashMap<String, List<String>>();
    Map<String, List<Integer>> bgColorList = new HashMap<String, List<Integer>>();
    for(int i = 0;i < glassType.size();i ++){
    	List<String> valueList = new ArrayList<String>();
    	List<Integer> bgColor = new ArrayList<Integer>();
		List<EyeformSpecsHistory> specs = specsList.get(glassType.get(i));
		if(null != specs){
			glass_show = "";
			//1
			String valueStr = specs.get(0).getOdSph();
			if(null == valueStr){
				for(int j = 1;j < specs.size();j ++){
					valueStr = specs.get(j).getOdSph();
					if(null != valueStr){
						if(appo_now == specs.get(j).getAppointmentNo()){
							bgColor.add(1);
						}else{
							bgColor.add(0);
						}
						break;
					}
				}
				if(null == valueStr){
					valueStr = "";
				}
			}else{
				if(appo_now == specs.get(0).getAppointmentNo()){
					bgColor.add(1);
				}else{
					bgColor.add(0);
				}
			}	
			valueList.add(valueStr);

			//2
			valueStr = specs.get(0).getOdCyl();
			if(null == valueStr){
				for(int j = 1;j < specs.size();j ++){
					valueStr = specs.get(j).getOdCyl();
					if(null != valueStr){
						if(appo_now == specs.get(j).getAppointmentNo()){
							bgColor.add(1);
						}else{
							bgColor.add(0);
						}
						break;
					}
				}
				if(null == valueStr){
					valueStr = "";
				}
			}else{
				if(appo_now == specs.get(0).getAppointmentNo()){
					bgColor.add(1);
				}else{
					bgColor.add(0);
				}
			}	
			valueList.add(valueStr);

			//3
			valueStr = specs.get(0).getOdAxis();
			if(null == valueStr){
				for(int j = 1;j < specs.size();j ++){
					valueStr = specs.get(j).getOdAxis();
					if(null != valueStr){
						if(appo_now == specs.get(j).getAppointmentNo()){
							bgColor.add(1);
						}else{
							bgColor.add(0);
						}
						break;
					}
				}
				if(null == valueStr){
					valueStr = "";
				}
			}else{
				if(appo_now == specs.get(0).getAppointmentNo()){
					bgColor.add(1);
				}else{
					bgColor.add(0);
				}
			}	
			valueList.add(valueStr);

			//4
			valueStr = specs.get(0).getOdAdd();
			if(null == valueStr){
				for(int j = 1;j < specs.size();j ++){
					valueStr = specs.get(j).getOdAdd();
					if(null != valueStr){
						if(appo_now == specs.get(j).getAppointmentNo()){
							bgColor.add(1);
						}else{
							bgColor.add(0);
						}
						break;
					}
				}
				if(null == valueStr){
					valueStr = "";
				}
			}else{
				if(appo_now == specs.get(0).getAppointmentNo()){
					bgColor.add(1);
				}else{
					bgColor.add(0);
				}
			}	
			valueList.add(valueStr);

			//5
			valueStr = specs.get(0).getOdPrism();
			if(null == valueStr){
				for(int j = 1;j < specs.size();j ++){
					valueStr = specs.get(j).getOdPrism();
					if(null != valueStr){
						if(appo_now == specs.get(j).getAppointmentNo()){
							bgColor.add(1);
						}else{
							bgColor.add(0);
						}
						break;
					}
				}
				if(null == valueStr){
					valueStr = "";
				}
			}else{
				if(appo_now == specs.get(0).getAppointmentNo()){
					bgColor.add(1);
				}else{
					bgColor.add(0);
				}
			}	
			valueList.add(valueStr);

			//6
			valueStr = specs.get(0).getOsSph();
			if(null == valueStr){
				for(int j = 1;j < specs.size();j ++){
					valueStr = specs.get(j).getOsSph();
					if(null != valueStr){
						if(appo_now == specs.get(j).getAppointmentNo()){
							bgColor.add(1);
						}else{
							bgColor.add(0);
						}
						break;
					}
				}
				if(null == valueStr){
					valueStr = "";
				}
			}else{
				if(appo_now == specs.get(0).getAppointmentNo()){
					bgColor.add(1);
				}else{
					bgColor.add(0);
				}
			}	
			valueList.add(valueStr);

			//7
			valueStr = specs.get(0).getOsCyl();
			if(null == valueStr){
				for(int j = 1;j < specs.size();j ++){
					valueStr = specs.get(j).getOsCyl();
					if(null != valueStr){
						if(appo_now == specs.get(j).getAppointmentNo()){
							bgColor.add(1);
						}else{
							bgColor.add(0);
						}
						break;
					}
				}
				if(null == valueStr){
					valueStr = "";
				}
			}else{
				if(appo_now == specs.get(0).getAppointmentNo()){
					bgColor.add(1);
				}else{
					bgColor.add(0);
				}
			}	
			valueList.add(valueStr);

			//8
			valueStr = specs.get(0).getOsAxis();
			if(null == valueStr){
				for(int j = 1;j < specs.size();j ++){
					valueStr = specs.get(j).getOsAxis();
					if(null != valueStr){
						if(appo_now == specs.get(j).getAppointmentNo()){
							bgColor.add(1);
						}else{
							bgColor.add(0);
						}
						break;
					}
				}
				if(null == valueStr){
					valueStr = "";
				}
			}else{
				if(appo_now == specs.get(0).getAppointmentNo()){
					bgColor.add(1);
				}else{
					bgColor.add(0);
				}
			}	
			valueList.add(valueStr);

			//9
			valueStr = specs.get(0).getOsAdd();
			if(null == valueStr){
				for(int j = 1;j < specs.size();j ++){
					valueStr = specs.get(j).getOsAdd();
					if(null != valueStr){
						if(appo_now == specs.get(j).getAppointmentNo()){
							bgColor.add(1);
						}else{
							bgColor.add(0);
						}
						break;
					}
				}
				if(null == valueStr){
					valueStr = "";
				}
			}else{
				if(appo_now == specs.get(0).getAppointmentNo()){
					bgColor.add(1);
				}else{
					bgColor.add(0);
				}
			}	
			valueList.add(valueStr);

			//10
			valueStr = specs.get(0).getOsPrism();
			if(null == valueStr){
				for(int j = 1;j < specs.size();j ++){
					valueStr = specs.get(j).getOsPrism();
					if(null != valueStr){
						if(appo_now == specs.get(j).getAppointmentNo()){
							bgColor.add(1);
						}else{
							bgColor.add(0);
						}
						break;
					}
				}
				if(null == valueStr){
					valueStr = "";
				}
			}else{
				if(appo_now == specs.get(0).getAppointmentNo()){
					bgColor.add(1);
				}else{
					bgColor.add(0);
				}
			}	
			valueList.add(valueStr);

			//11
			valueStr = specs.get(0).getDateStr();
			if(null == valueStr){
				for(int j = 1;j < specs.size();j ++){
					valueStr = specs.get(j).getDateStr();
					if(null != valueStr){
						if(appo_now == specs.get(j).getAppointmentNo()){
							bgColor.add(1);
						}else{
							bgColor.add(0);
						}
						break;
					}
				}
				if(null == valueStr){
					valueStr = "";
				}
			}else{
				if(appo_now == specs.get(0).getAppointmentNo()){
					bgColor.add(1);
				}else{
					bgColor.add(0);
				}
			}	
			valueList.add(valueStr);

			//add list to map
			bgColorList.put(glassType.get(i), bgColor);
			specsValueList.put(glassType.get(i), valueList);
		}
	}
%>
<style type="text/css">
.exam
{
	padding: 0px;
	border-spacing: 0px;
	margin: 0px;
	border: 0px;
}

.exam td
{
	text-align: center;
	vertical-align:middle;
	padding:1px !important;
	margin:0px;
	border: 0px;
	font-size: 9px;
}

.examfieldgrey
{
	background-color: #DDDDDD;
}

.examfieldwhite
{
	background-color: white;
}

.slidey { margin-bottom: 6px;font-size: 10pt;}
.slidey .title {
        margin-top: 1px;

        list-style-type: none;
       /* font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif; */

        overflow: hidden;
        background-color: #f6f6ff;
        text-align:left;
        padding: 0px;

         /*font-size: 1.0em;*/
        font-size: 9px;
        /*font-variant:small-caps;*/

        font-weight: bold;
       font-family: Verdana,Tahoma,Arial,sans-serif;
       font-style:normal;
       text-transform:none;
       text-decoration:none;
       letter-spacing:normal;
       word-spacing:0;
       line-height:11px;
       vertical-align:baseline;
  }

.section_title {
	color:black;
}

.slidey .title2 {
        margin-top: 1px;
        font-size:10pt;
        list-style-type: none;
        font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;
        overflow: hidden;
        background-color: #ccccff;
        padding: 0px; }
/* the only noteworthy thing here is the overflow is hidden,
it's really a sleight-of-hand kind of thing, we're playing
with the height and that makes it 'slide' */
.slidey .slideblock { overflow: hidden; background-color:  #ccccff;  padding: 2px; font-size: 10pt; }

span.ge{
        margin-top: 1px;
        border-bottom: 1px solid #000;
        font-weight: bold;
        list-style-type: none;
        font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;
        font-size: 12pt;
        overflow: hidden;
        background-color: #99bbee;
        padding: 0px;
        color: black;;
        width: 300px;
}
</style>
<script type="text/javascript">
function changeclass(objIndex){
	var type1 = new Array();
	var type2 = new Array();
	var type3 = new Array();
	var type4 = new Array();
	var type5 = new Array();
	<%
	for(int i = 0;i < glassType.size();i ++){
		List<Integer> bg_color = bgColorList.get(glassType.get(i));
		if(null != bg_color){
			for(int j = 0;j < bg_color.size();j ++){
				if(i == 0){
					if(bg_color.get(j) == 0){
	%>					
						type1[<%=j%>] = "examfieldgrey";
	<%					
					}else{
	%>					
						type1[<%=j%>] = "examfieldwhite";
	<%					
					}
				}
				if(i == 1){
					if(bg_color.get(j) == 0){
	%>					
						type2[<%=j%>] = "examfieldgrey";
	<%					
					}else{
	%>					
						type2[<%=j%>] = "examfieldwhite";
	<%					
					}
				}
				if(i == 2){
					if(bg_color.get(j) == 0){
	%>					
						type3[<%=j%>] = "examfieldgrey";
	<%					
					}else{
	%>					
						type3[<%=j%>] = "examfieldwhite";
	<%					
					}
				}
				if(i == 3){
					if(bg_color.get(j) == 0){
	%>					
						type4[<%=j%>] = "examfieldgrey";
	<%					
					}else{
	%>					
						type4[<%=j%>] = "examfieldwhite";
	<%					
					}
				}
				if(i == 4){
					if(bg_color.get(j) == 0){
	%>					
						type5[<%=j%>] = "examfieldgrey";
	<%					
					}else{
	%>					
						type5[<%=j%>] = "examfieldwhite";
	<%					
					}
				}
			}
		}else{
			if(i == 0){
	%>			
				type1 = new Array("examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey");
	<%
			}
			if(i == 1){
	%>			
				type2 = new Array("examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey");
	<%		
			}
			if(i == 2){
	%>			
				type3 = new Array("examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey");
	<%		
			}
			if(i == 3){
	%>			
				type4 = new Array("examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey");
	<%		
			}
			if(i == 4){
	%>			
				type5 = new Array("examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey","examfieldgrey");
	<%		
			}
		}
	}
	%>
	if(typeof(objIndex) != "undefined"){
		var glass_type = document.getElementById("type00" + objIndex).value;
		var selectType;
		if(glassType=="distance"){
			selectType = type1; 
		}
		if(glassType=="bifocal"){
			selectType = type2; 
		}
		if(glassType=="invis bfocal"){
			selectType = type3; 
		}
		if(glassType=="reading"){
			selectType = type4; 
		}
		if(glassType=="trifocal"){
			selectType = type5; 
		}

		document.getElementById("odSph" + objIndex).className = selectType[0] ;
		document.getElementById("odCyl" + objIndex).className = selectType[1];
		document.getElementById("odAxis" + objIndex).className = selectType[2];
		document.getElementById("odAdd" + objIndex).className = selectType[3];
		document.getElementById("odPrism" + objIndex).className = selectType[4];
		document.getElementById("osSph" + objIndex).className = selectType[5];
		document.getElementById("osCyl" + objIndex).className = selectType[6];
		document.getElementById("osAxis"+ objIndex).className = selectType[7];
		document.getElementById("osAdd"+ objIndex).className = selectType[8];
		document.getElementById("osPrism"+ objIndex).className = selectType[9];
		document.getElementById("gl_date"+ objIndex).className = selectType[10];
	}else{
		var num = jQuery("#myTable select").length;
		if(num > 0){
			for(var i = 0;i < num; i++){
				var selectType;
				var showGlassNum = i + 1;
				if(jQuery("#myTable select")[i].value == "distance"){
					selectType = type1;
				}
				if(jQuery("#myTable select")[i].value == "bifocal"){
					selectType = type2;
				}
				if(jQuery("#myTable select")[i].value == "invis bfocal"){
					selectType = type3;
				}
				if(jQuery("#myTable select")[i].value == "reading"){
					selectType = type4;
				}
				if(jQuery("#myTable select")[i].value == "trifocal"){
					selectType = type5;
				}

				document.getElementById("odSph" + showGlassNum).className = selectType[0] ;
				document.getElementById("odCyl" + showGlassNum).className = selectType[1];
				document.getElementById("odAxis" + showGlassNum).className = selectType[2];
				document.getElementById("odAdd" + showGlassNum).className = selectType[3];
				document.getElementById("odPrism" + showGlassNum).className = selectType[4];
				document.getElementById("osSph" + showGlassNum).className = selectType[5];
				document.getElementById("osCyl" + showGlassNum).className = selectType[6];
				document.getElementById("osAxis"+ showGlassNum).className = selectType[7];
				document.getElementById("osAdd"+ showGlassNum).className = selectType[8];
				document.getElementById("osPrism"+ showGlassNum).className = selectType[9];
				document.getElementById("gl_date"+ showGlassNum).className = selectType[10];
			}
		}
	}
}

function changeValue(objIndex) {
	var type001=document.getElementById("type00" + objIndex).value;

	var odSph="";
	var odCyl="";
	var odAxis="";
	var odAdd="";
	var odPrism="";
	var osSph="";
	var osCyl="";
	var osAxis="";
	var osAdd="";
	var osPrism="";
	var gl_date="";
	if(type001=="distance"){
		<%
		List<String> specs_val = specsValueList.get("distance");
		if(null != specs_val){
		%>
			odSph="<%=specs_val.get(0)%>";
			odCyl="<%=specs_val.get(1)%>";
			odAxis="<%=specs_val.get(2)%>";
			odAdd="<%=specs_val.get(3)%>";
			odPrism="<%=specs_val.get(4)%>";
			osSph="<%=specs_val.get(5)%>";
			osCyl="<%=specs_val.get(6)%>";
			osAxis="<%=specs_val.get(7)%>";
			osAdd="<%=specs_val.get(8)%>";
			osPrism="<%=specs_val.get(9)%>";
			gl_date="<%=specs_val.get(10)%>";
		<%	
		}
		%>
	}
	if(type001=="bifocal"){
		<%
		specs_val = specsValueList.get("bifocal");
		if(null != specs_val){
		%>
			odSph="<%=specs_val.get(0)%>";
			odCyl="<%=specs_val.get(1)%>";
			odAxis="<%=specs_val.get(2)%>";
			odAdd="<%=specs_val.get(3)%>";
			odPrism="<%=specs_val.get(4)%>";
			osSph="<%=specs_val.get(5)%>";
			osCyl="<%=specs_val.get(6)%>";
			osAxis="<%=specs_val.get(7)%>";
			osAdd="<%=specs_val.get(8)%>";
			osPrism="<%=specs_val.get(9)%>";
			gl_date="<%=specs_val.get(10)%>";
		<%	
		}
		%>
	}
	if(type001=="invis bfocal"){
		<%
		specs_val = specsValueList.get("invis bfocal");
		if(null != specs_val){
		%>
			odSph="<%=specs_val.get(0)%>";
			odCyl="<%=specs_val.get(1)%>";
			odAxis="<%=specs_val.get(2)%>";
			odAdd="<%=specs_val.get(3)%>";
			odPrism="<%=specs_val.get(4)%>";
			osSph="<%=specs_val.get(5)%>";
			osCyl="<%=specs_val.get(6)%>";
			osAxis="<%=specs_val.get(7)%>";
			osAdd="<%=specs_val.get(8)%>";
			osPrism="<%=specs_val.get(9)%>";
			gl_date="<%=specs_val.get(10)%>";
		<%	
		}
		%>
	}
	if(type001=="reading"){
		<%
		 specs_val = specsValueList.get("reading");
		if(null != specs_val){
		%>
			odSph="<%=specs_val.get(0)%>";
			odCyl="<%=specs_val.get(1)%>";
			odAxis="<%=specs_val.get(2)%>";
			odAdd="<%=specs_val.get(3)%>";
			odPrism="<%=specs_val.get(4)%>";
			osSph="<%=specs_val.get(5)%>";
			osCyl="<%=specs_val.get(6)%>";
			osAxis="<%=specs_val.get(7)%>";
			osAdd="<%=specs_val.get(8)%>";
			osPrism="<%=specs_val.get(9)%>";
			gl_date="<%=specs_val.get(10)%>";
		<%	
		}
		%>
	}
	if(type001=="trifocal"){
		<%
		specs_val = specsValueList.get("trifocal");
		if(null != specs_val){
		%>
			odSph="<%=specs_val.get(0)%>";
			odCyl="<%=specs_val.get(1)%>";
			odAxis="<%=specs_val.get(2)%>";
			odAdd="<%=specs_val.get(3)%>";
			odPrism="<%=specs_val.get(4)%>";
			osSph="<%=specs_val.get(5)%>";
			osCyl="<%=specs_val.get(6)%>";
			osAxis="<%=specs_val.get(7)%>";
			osAdd="<%=specs_val.get(8)%>";
			osPrism="<%=specs_val.get(9)%>";
			gl_date="<%=specs_val.get(10)%>";
		<%	
		}
		%>
	}

	document.getElementById("odSph" + objIndex).value=odSph;
	document.getElementById("odCyl" + objIndex).value=odCyl;
	document.getElementById("odAxis" + objIndex).value=odAxis;
	document.getElementById("odAdd" + objIndex).value=odAdd;
	document.getElementById("odPrism" + objIndex).value=odPrism;
	document.getElementById("osSph" + objIndex).value=osSph;
	document.getElementById("osCyl" + objIndex).value=osCyl;
	document.getElementById("osAxis" + objIndex).value=osAxis;
	document.getElementById("osAdd" + objIndex).value=osAdd;
	document.getElementById("osPrism" + objIndex).value=osPrism;
	document.getElementById("gl_date" + objIndex).value=gl_date;

	changeclass(objIndex);
}

function hxOpen(objIndex){
    var id=document.getElementById("specs.id" + objIndex).value;
    var type=document.getElementById("type00" + objIndex).value;  
	var date=document.getElementById("gl_date" + objIndex).value;
	var odSph=document.getElementById("odSph" + objIndex).value;
	var odCyl=document.getElementById("odCyl" + objIndex).value;
    var odAxis=document.getElementById("odAxis" + objIndex).value;
	var odAdd=document.getElementById("odAdd" + objIndex).value;
   	var odPrism=document.getElementById("odPrism" + objIndex).value;
	var osSph=document.getElementById("osSph" + objIndex).value;
	var osCyl=document.getElementById("osCyl" + objIndex).value;
	var osAxis=document.getElementById("osAxis" + objIndex).value;
    var osAdd=document.getElementById("osAdd" + objIndex).value;
	var osPrism=document.getElementById("osPrism" + objIndex).value;  
	var appno=document.getElementById("appointment_no").value; 
	var demno=document.getElementById("demographic_no").value; 
 	window.open("../eyeform/glassHX.jsp?specs.id="+id+"&type="+type+"&dateStr="+date+"&odSph="+odSph+"&odCyl="+odCyl+"&odAxis="+odAxis+"&odAdd="+odAdd+"&odPrism="+odPrism+"&osSph="+osSph+"&osCyl="+osCyl+"&osAxis="+osAxis+"&osAdd="+osAdd+"&osPrism="+osPrism+"&appointment_no="+appno+"&demographic_no="+demno+"",'anwin','width=400,height=300');
}

var sumbitform = false;
function insRow(){
	sumbitform = true;
	var tab_num = 0;
	var rowNum = jQuery("#myTable select").length;
	tab_num = document.getElementById("osPrism"+rowNum).tabIndex + 2;
	
	if(rowNum < 5){
		var rowNo = rowNum + 1;
		var x=document.getElementById('myTable').insertRow(document.getElementById('myTable').rows.length);
		var h1=x.insertCell(0);
		var h2=x.insertCell(1);
		var h3=x.insertCell(2);
		var h4=x.insertCell(3);
		var h5=x.insertCell(4);
		var h6=x.insertCell(5);
		var h7=x.insertCell(6);
		var h8=x.insertCell(7);
		var h9=x.insertCell(8);
		var h10=x.insertCell(9);
		var h11=x.insertCell(10);
		var h12=x.insertCell(11);
		var h13=x.insertCell(12);
		var h14=x.insertCell(13);
		var h15=x.insertCell(14);
		var h16=x.insertCell(15);

		h1.innerHTML="<div><select style='width:100%' onchange='demo("+rowNo+")' name='specs.type"+rowNo+"' id='type00"+rowNo+"'><option value='distance'>distance</option><option value='bifocal'>bifocal</option><option value='invis bfocal'>invis bfocal</option><option value='reading'>reading</option><option value='trifocal'>trifocal</option><option value='blank'></option></select></div>";
		h2.innerHTML="<div><input name='specs.odSph"+rowNo+"' id='odSph"+rowNo+"' type='text' tabindex='"+tab_num+"' maxlength='6' class='examfieldgrey' style='width:95%'  onfocus='whiteField(this);'></div>";
		tab_num ++;
		h3.innerHTML="<div><input name='specs.odCyl"+rowNo+"' id='odCyl"+rowNo+"' type='text' tabindex='"+tab_num+"' maxlength='6' class='examfieldgrey' style='width:95%'  onfocus='whiteField(this);'></div>";
		tab_num ++;
		h4.innerHTML="<div><input name='specs.odAxis"+rowNo+"' id='odAxis"+rowNo+"' type='text' tabindex='"+tab_num+"' maxlength='6' class='examfieldgrey' style='width:95%'  onfocus='whiteField(this);'></div>";
		tab_num ++;
		h5.innerHTML="<div><input name='specs.odAdd"+rowNo+"' id='odAdd"+rowNo+"' type='text' tabindex='"+tab_num+"' maxlength='6' class='examfieldgrey' style='width:95%'  onfocus='whiteField(this);'></div>";
		tab_num ++;
		h6.innerHTML="<div><input name='specs.odPrism"+rowNo+"' id='odPrism"+rowNo+"' type='text' tabindex='"+tab_num+"' maxlength='12' class='examfieldgrey' style='width:95%'  onfocus='whiteField(this);'></div>";
		tab_num ++;
		h7.innerHTML="<div  width='4%'></div>";
		h8.innerHTML="<div><input name='specs.osSph"+rowNo+"' id='osSph"+rowNo+"' type='text' tabindex='"+tab_num+"' maxlength='6' class='examfieldgrey' style='width:95%'  onfocus='whiteField(this);'></div>";
		tab_num ++;
		h9.innerHTML="<div><input name='specs.osCyl"+rowNo+"' id='osCyl"+rowNo+"' type='text' tabindex='"+tab_num+"' maxlength='6' class='examfieldgrey' style='width:95%'  onfocus='whiteField(this);'></div>";
		tab_num ++;
		h10.innerHTML="<div><input name='specs.osAxis"+rowNo+"' id='osAxis"+rowNo+"' type='text' tabindex='"+tab_num+"' maxlength='6' class='examfieldgrey' style='width:95%'  onfocus='whiteField(this);'></div>";
		tab_num ++;
		h11.innerHTML="<div><input name='specs.osAdd"+rowNo+"' id='osAdd"+rowNo+"' type='text' tabindex='"+tab_num+"' maxlength='6' class='examfieldgrey' style='width:95%'  onfocus='whiteField(this);'></div>";
		tab_num ++;
		h12.innerHTML="<div><input name='specs.osPrism"+rowNo+"' id='osPrism"+rowNo+"' type='text' tabindex='"+tab_num+"' maxlength='12' class='examfieldgrey' style='width:95%'  onfocus='whiteField(this);'></div>";
		tab_num ++;
		h13.innerHTML="<div><input name='specs.dateStr"+rowNo+"'  type='text' tabindex='"+tab_num+"' class='examfieldgrey' style='width:115%'  id='gl_date"+rowNo+"' onfocus='whiteField(this);'></div>";
		h14.innerHTML="<div  width='1%'><img src='"+ctx+"/images/cal.gif' id='pdate_cal"+rowNo+"'></div>";
		h15.innerHTML="<div><a href='javascript:void(0)' onclick='hxOpen("+rowNo+");'><img src='../images/notes.gif' /> </a></div>";
		h16.innerHTML="<div><input type='hidden' value='' name='specs.id"+rowNo+"' id='specs.id"+rowNo+"'></div>";

		Calendar.setup({ inputField : "gl_date"+rowNo, ifFormat : "%Y-%m-%d", showsTime :false, button : "pdate_cal"+rowNo, singleClick : true, step : 1 });
	}
}

function hxForm_sumbit(){	
	var num = jQuery("#myTable select").length;
	var postData = "";
	
	if(num > 0){
		for(var i = 1;i <= num; i++){
			if(jQuery("#type00" + i).val() == "blank"){
				alert("Please select a glasses type!");
				return false;
			}
			if(postData.length > 0) {
				postData += "&";
				var name = jQuery("#type00" + i).attr("name");
				var value = jQuery("#type00" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}else{
				var name = jQuery("#type00" + i).attr("name");
				var value = jQuery("#type00" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}
			if(jQuery("#specs.id" + i).attr("class") == "examfieldwhite"){
				if(postData.length > 0) {
					postData += "&";
				}
				sumbitform = true;
				var name = jQuery("#specs.id" + i).attr("name");
				var value = jQuery("#specs.id" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}
			if(jQuery("#gl_date" + i).attr("class") == "examfieldwhite") {
				if(postData.length > 0) {
					postData += "&";
				}
				sumbitform = true;
				var name = jQuery("#gl_date" + i).attr("name");
				var value = jQuery("#gl_date" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}
			if(jQuery("#odSph" + i).attr("class") == "examfieldwhite") {
				if(postData.length > 0) {
					postData += "&";
				}
				sumbitform = true;
				var name = jQuery("#odSph" + i).attr("name");
				var value = jQuery("#odSph" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}
			if(jQuery("#odCyl" + i).attr("class") == "examfieldwhite") {
				if(postData.length > 0) {
					postData += "&";
				}
				sumbitform = true;
				var name = jQuery("#odCyl" + i).attr("name");
				var value = jQuery("#odCyl" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}
			if(jQuery("#odAxis" + i).attr("class") == "examfieldwhite") {
				if(postData.length > 0) {
					postData += "&";
				}
				sumbitform = true;
				var name = jQuery("#odAxis" + i).attr("name");
				var value = jQuery("#odAxis" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}
			if(jQuery("#odAdd" + i).attr("class") == "examfieldwhite") {
				if(postData.length > 0) {
					postData += "&";
				}
				sumbitform = true;
				var name = jQuery("#odAdd" + i).attr("name");
				var value = jQuery("#odAdd" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}
			if(jQuery("#odPrism" + i).attr("class") == "examfieldwhite") {
				if(postData.length > 0) {
					postData += "&";
				}
				sumbitform = true;
				var name = jQuery("#odPrism" + i).attr("name");
				var value = jQuery("#odPrism" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}
			if(jQuery("#osSph" + i).attr("class") == "examfieldwhite") {
				if(postData.length > 0) {
					postData += "&";
				}
				sumbitform = true;
				var name = jQuery("#osSph" + i).attr("name");
				var value = jQuery("#osSph" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}
			if(jQuery("#osCyl" + i).attr("class") == "examfieldwhite") {
				if(postData.length > 0) {
					postData += "&";
				}
				sumbitform = true;
				var name = jQuery("#osCyl" + i).attr("name");
				var value = jQuery("#osCyl" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}
			if(jQuery("#osAxis" + i).attr("class") == "examfieldwhite") {
				if(postData.length > 0) {
					postData += "&";
				}
				sumbitform = true;
				var name = jQuery("#osAxis" + i).attr("name");
				var value = jQuery("#osAxis" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}
			if(jQuery("#osAdd" + i).attr("class") == "examfieldwhite") {
				if(postData.length > 0) {
					postData += "&";
				}
				sumbitform = true;
				var name = jQuery("#osAdd" + i).attr("name");
				var value = jQuery("#osAdd" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}
			if(jQuery("#osPrism" + i).attr("class") == "examfieldwhite") {
				if(postData.length > 0) {
					postData += "&";
				}
				sumbitform = true;
				var name = jQuery("#osPrism" + i).attr("name");
				var value = jQuery("#osPrism" + i).val();
				var data = name + "=" + encodeURIComponent(value);
				postData += data;
			}
		}
	}

	if(sumbitform){
		var appointmentNo=document.getElementById("appointment_no").value; 
		var demographicNo=document.getElementById("demographic_no").value;
		jQuery.ajax({type:'POST',async:false,url:ctx+'/eyeform/SpecsHistory.do?method=save&specs.demographicNo='+demographicNo+'&specs.appointmentNo='+appointmentNo,data:postData,success: function(){}});
	}
}

function touchColor() {
	var divs = document.getElementsByClassName(document, "div", "slidey");
    for (var i=0; i<divs.length; i++) {
            var inputs = divs[i].getElementsByTagName("INPUT");
            var teinputs=divs[i].getElementsByTagName("TEXTAREA");
            for (var j=0; j<inputs.length; j++)
                    if (inputs[j].value.length>0 && inputs[j].className=="examfieldwhite")
                            break;
            for (var k=0; k<teinputs.length; k++)
                    if (teinputs[k].value.length>0 && teinputs[k].className=="examfieldwhite")
                            break;
            var color=(j<inputs.length || k<teinputs.length)?"brown":"black";
            divs[i].getElementsByTagName("A")[0].style.color=color;
    }
}

var effectMap = {};
jQuery("document").ready(function() {
	for(var x=21;x<32;x++) {
		var name = 's_'+x;
		var el = document.getElementById(name);
		if(el) {
			el.style.display='none';
			effectMap[name] = 'up';
		}

	}
});

function togglediv(el) {
	jQuery('#s_' + el.id.substring(el.id.indexOf('_')+1)).toggle();
}

function expanddiv(el) {
	jQuery('#s_' + el.id.substring(el.id.indexOf('_')+1)).show();
}

function collapsediv(el) {
	jQuery('#s_' + el.id.substring(el.id.indexOf('_')+1)).hide();
}
function syncFields(el){
		return true;
}
function whiteField(el){
  		el.className="examfieldwhite";
}

function setfieldvalue(name,value) {
	jQuery("input[measurement='"+name+"']").each(function() {
		jQuery(this).val(value);
		whiteField(this);
	});
}

function getfieldvalue(name) {
	var val = undefined;
	jQuery("input[measurement='"+name+"']").each(function() {
		val = jQuery(this).val();
	});
	return val;
}

var copy_gl_rs = "";
var copy_gl_rc = "";
var copy_gl_rx = "";
var copy_gl_ls = "";
var copy_gl_lc = "";
var copy_gl_lx = "";
var copy_from = true;
function getglasshxvalue(name){
	var val;
	val = document.getElementById(name).value;
	return val;
}

function copyglasses(){
	copy_form = true;
	copy_gl_rs = getglasshxvalue("odSph1");
	copy_gl_rc = getglasshxvalue("odCyl1");
	copy_gl_rx = getglasshxvalue("odAxis1");
	copy_gl_ls = getglasshxvalue("osSph1");
	copy_gl_lc = getglasshxvalue("osCyl1");	
	copy_gl_lx = getglasshxvalue("osAxis1");
}

var copy_v_rs = "";
var copy_v_rc = "";
var copy_v_rx = "";
var copy_v_ls = "";
var copy_v_lc = "";
var copy_v_lx = "";

function copyar(){
	copy_form = false;
	copy_v_rs = getfieldvalue("v_rs");
	copy_v_rc = getfieldvalue("v_rc");
	copy_v_rx = getfieldvalue("v_rx");
	copy_v_ls = getfieldvalue("v_ls");
	copy_v_lc = getfieldvalue("v_lc");	
	copy_v_lx = getfieldvalue("v_lx");
	
	copy_v_rs_add = "";
	copy_v_rc_add = "";
	copy_v_rx_add = "";
	copy_v_ls_add = "";
	copy_v_lc_add = "";
	copy_v_lx_add = "";
}

var copy_v_rs_add = "";
var copy_v_rc_add = "";
var copy_v_rx_add = "";
var copy_v_ls_add = "";
var copy_v_lc_add = "";
var copy_v_lx_add = "";

function copyar_add(){
	copy_form = false;
	copy_v_rs_add = getfieldvalue("v_rs_add");
	copy_v_rc_add = getfieldvalue("v_rc_add");
	copy_v_rx_add = getfieldvalue("v_rx_add");
	copy_v_ls_add = getfieldvalue("v_ls_add");
	copy_v_lc_add = getfieldvalue("v_lc_add");	
	copy_v_lx_add = getfieldvalue("v_lx_add");
	
	copy_v_rs = "";
	copy_v_rc = "";
	copy_v_rx = "";
	copy_v_ls = "";
	copy_v_lc = "";
	copy_v_lx = "";
}

var copy_v_rds = "";
var copy_v_rdc = "";
var copy_v_rdx = "";
var copy_v_lds = "";
var copy_v_ldc = "";
var copy_v_ldx = "";

function copy_mdist(){
	copy_v_rds = getfieldvalue("v_rds");
	copy_v_rdc = getfieldvalue("v_rdc");
	copy_v_rdx = getfieldvalue("v_rdx");
	copy_v_lds = getfieldvalue("v_lds");
	copy_v_ldc = getfieldvalue("v_ldc");
	copy_v_ldx = getfieldvalue("v_ldx");
}

function paste_final(){
	setfieldvalue("v_rs_finial",copy_v_rds);
	setfieldvalue("v_rc_finial",copy_v_rdc);
	setfieldvalue("v_rx_finial",copy_v_rdx);
	setfieldvalue("v_ls_finial",copy_v_lds);
	setfieldvalue("v_lc_finial",copy_v_ldc);
	setfieldvalue("v_lx_finial",copy_v_ldx);
}
function paste_mdist(){
	if(!copy_form){
		if(copy_v_rs == "" && copy_v_rc == "" && copy_v_rx == "" && copy_v_ls == "" && copy_v_lc == "" && copy_v_lx == ""){
			setfieldvalue("v_rds",copy_v_rs_add);
			setfieldvalue("v_rdc",copy_v_rc_add);
			setfieldvalue("v_rdx",copy_v_rx_add);
			setfieldvalue("v_lds",copy_v_ls_add);
			setfieldvalue("v_ldc",copy_v_lc_add);
			setfieldvalue("v_ldx",copy_v_lx_add);
		}else{
			setfieldvalue("v_rds",copy_v_rs);
			setfieldvalue("v_rdc",copy_v_rc);
			setfieldvalue("v_rdx",copy_v_rx);
			setfieldvalue("v_lds",copy_v_ls);
			setfieldvalue("v_ldc",copy_v_lc);
			setfieldvalue("v_ldx",copy_v_lx);
		}
	}else{
		setfieldvalue("v_rds",copy_gl_rs);
		setfieldvalue("v_rdc",copy_gl_rc);
		setfieldvalue("v_rdx",copy_gl_rx);
		setfieldvalue("v_lds",copy_gl_ls);
		setfieldvalue("v_ldc",copy_gl_lc);
		setfieldvalue("v_ldx",copy_gl_lx);
	}
}

function paste_mdist_add(){
	if(!copy_form){
		if(copy_v_rs == "" && copy_v_rc == "" && copy_v_rx == "" && copy_v_ls == "" && copy_v_lc == "" && copy_v_lx == ""){
			setfieldvalue("v_rds_add",copy_v_rs_add);
			setfieldvalue("v_rdc_add",copy_v_rc_add);
			setfieldvalue("v_rdx_add",copy_v_rx_add);
			setfieldvalue("v_lds_add",copy_v_ls_add);
			setfieldvalue("v_ldc_add",copy_v_lc_add);
			setfieldvalue("v_ldx_add",copy_v_lx_add);
		}else{
			setfieldvalue("v_rds_add",copy_v_rs);
			setfieldvalue("v_rdc_add",copy_v_rc);
			setfieldvalue("v_rdx_add",copy_v_rx);
			setfieldvalue("v_lds_add",copy_v_ls);
			setfieldvalue("v_ldc_add",copy_v_lc);
			setfieldvalue("v_ldx_add",copy_v_lx);
		}
	}else{
		setfieldvalue("v_rds_add",copy_gl_rs);
		setfieldvalue("v_rdc_add",copy_gl_rc);
		setfieldvalue("v_rdx_add",copy_gl_rx);
		setfieldvalue("v_lds_add",copy_gl_ls);
		setfieldvalue("v_ldc_add",copy_gl_lc);
		setfieldvalue("v_ldx_add",copy_gl_lx);
	}
}

function paste_mnear(){
	if(!copy_form){
		if(copy_v_rs == "" && copy_v_rc == "" && copy_v_rx == "" && copy_v_ls == "" && copy_v_lc == "" && copy_v_lx == ""){
			setfieldvalue("v_rns",copy_v_rs_add);
			setfieldvalue("v_rnc",copy_v_rc_add);
			setfieldvalue("v_rnx",copy_v_rx_add);
			setfieldvalue("v_lns",copy_v_ls_add);
			setfieldvalue("v_lnc",copy_v_lc_add);
			setfieldvalue("v_lnx",copy_v_lx_add);
		}else{
			setfieldvalue("v_rns",copy_v_rs);
			setfieldvalue("v_rnc",copy_v_rc);
			setfieldvalue("v_rnx",copy_v_rx);
			setfieldvalue("v_lns",copy_v_ls);
			setfieldvalue("v_lnc",copy_v_lc);
			setfieldvalue("v_lnx",copy_v_lx);
		}
	}else{
		setfieldvalue("v_rns",copy_gl_rs);
		setfieldvalue("v_rnc",copy_gl_rc);
		setfieldvalue("v_rnx",copy_gl_rx);
		setfieldvalue("v_lns",copy_gl_ls);
		setfieldvalue("v_lnc",copy_gl_lc);
		setfieldvalue("v_lnx",copy_gl_lx);
	}
}

function paste_Cyclo(){
	if(!copy_form){
		if(copy_v_rs == "" && copy_v_rc == "" && copy_v_rx == "" && copy_v_ls == "" && copy_v_lc == "" && copy_v_lx == ""){
			setfieldvalue("v_rcs",copy_v_rs_add);
			setfieldvalue("v_rcc",copy_v_rc_add);
			setfieldvalue("v_rcx",copy_v_rx_add);
			setfieldvalue("v_lcs",copy_v_ls_add);
			setfieldvalue("v_lcc",copy_v_lc_add);
			setfieldvalue("v_lcx",copy_v_lx_add);
		}else{
			setfieldvalue("v_rcs",copy_v_rs);
			setfieldvalue("v_rcc",copy_v_rc);
			setfieldvalue("v_rcx",copy_v_rx);
			setfieldvalue("v_lcs",copy_v_ls);
			setfieldvalue("v_lcc",copy_v_lc);
			setfieldvalue("v_lcx",copy_v_lx);
		}
	}else{
		setfieldvalue("v_rcs",copy_gl_rs);
		setfieldvalue("v_rcc",copy_gl_rc);
		setfieldvalue("v_rcx",copy_gl_rx);
		setfieldvalue("v_lcs",copy_gl_ls);
		setfieldvalue("v_lcc",copy_gl_lc);
		setfieldvalue("v_lcx",copy_gl_lx);
	}
}

function clearMeasurement_od(){
	setfieldvalue("v_rk1","");
	setfieldvalue("v_rk2","");
	setfieldvalue("v_rkx","");
	setfieldvalue("v_rs","");
	setfieldvalue("v_rc","");
	setfieldvalue("v_rx","");
	setfieldvalue("v_rar","");
	setfieldvalue("v_rds","");
	setfieldvalue("v_rdc","");
	setfieldvalue("v_rdx","");
	setfieldvalue("v_rdv","");
	setfieldvalue("v_rns","");
	setfieldvalue("v_rnc","");
	setfieldvalue("v_rnx","");
	setfieldvalue("v_rnv","");
	setfieldvalue("v_rcs","");
	setfieldvalue("v_rcc","");
	setfieldvalue("v_rcx","");
	setfieldvalue("v_rcv","");
}

function clearMeasurement_os(){
	setfieldvalue("v_lk1","");
	setfieldvalue("v_lk2","");
	setfieldvalue("v_lkx","");
	setfieldvalue("v_ls","");
	setfieldvalue("v_lc","");
	setfieldvalue("v_lx","");
	setfieldvalue("v_lar","");
	setfieldvalue("v_lds","");
	setfieldvalue("v_ldc","");
	setfieldvalue("v_ldx","");
	setfieldvalue("v_ldv","");
	setfieldvalue("v_lns","");
	setfieldvalue("v_lnc","");
	setfieldvalue("v_lnx","");
	setfieldvalue("v_lnv","");
	setfieldvalue("v_lcs","");
	setfieldvalue("v_lcc","");
	setfieldvalue("v_lcx","");
	setfieldvalue("v_lcv","");
}

function setDuc_od(){
	setfieldvalue("duc_rur","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_rul","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_rr","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_rl","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_rdr","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_rdl","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearDuc_od(){
	setfieldvalue("duc_rur","");
	setfieldvalue("duc_rul","");
	setfieldvalue("duc_rr","");
	setfieldvalue("duc_rl","");
	setfieldvalue("duc_rdr","");
	setfieldvalue("duc_rdl","");
}

function setDuc_os(){
	setfieldvalue("duc_lur","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_lul","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_lr","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_ll","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_ldr","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_ldl","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearDuc_os(){
	setfieldvalue("duc_lur","");
	setfieldvalue("duc_lul","");
	setfieldvalue("duc_lr","");
	setfieldvalue("duc_ll","");
	setfieldvalue("duc_ldr","");
	setfieldvalue("duc_ldl","");
}

function setDuc_ou(){
	setfieldvalue("dip_ur","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dip_u","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dip_r","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dip_p","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dip_dr","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dip_d","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearDuc_ou(){
	setfieldvalue("dip_ur","");
	setfieldvalue("dip_u","");
	setfieldvalue("dip_r","");
	setfieldvalue("dip_p","");
	setfieldvalue("dip_dr","");
	setfieldvalue("dip_d","");
}

function setDeviation(){
	setfieldvalue("dev_u","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_near","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_r","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_p","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_l","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_plus3","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_rt","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_d","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_lt","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_far","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearDeviation(){
	setfieldvalue("dev_u","");
	setfieldvalue("dev_near","");
	setfieldvalue("dev_r","");
	setfieldvalue("dev_p","");
	setfieldvalue("dev_l","");
	setfieldvalue("dev_plus3","");
	setfieldvalue("dev_rt","");
	setfieldvalue("dev_d","");
	setfieldvalue("dev_lt","");
	setfieldvalue("dev_far","");
}

function setExternal_od(){
	setfieldvalue("ext_rface","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rretro","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rhertel","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearExternal_od(){
	setfieldvalue("ext_rface","");
	setfieldvalue("ext_rretro","");
	setfieldvalue("ext_rhertel","");
}

function setExternal_os(){
	setfieldvalue("ext_lface","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_lretro","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_lhertel","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearExternal_os(){
	setfieldvalue("ext_lface","");
	setfieldvalue("ext_lretro","");
	setfieldvalue("ext_lhertel","");
}

function setEyelid_od(){
	setfieldvalue("ext_rul","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rll","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rlake","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rirrig","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rpunc","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rnld","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rdye","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearEyelid_od(){
	setfieldvalue("ext_rul","");
	setfieldvalue("ext_rll","");
	setfieldvalue("ext_rlake","");
	setfieldvalue("ext_rirrig","");
	setfieldvalue("ext_rpunc","");
	setfieldvalue("ext_rnld","");
	setfieldvalue("ext_rdye","");
}

function setEyelid_os(){
	setfieldvalue("ext_lul","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_lll","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_llake","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_lirrig","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_lpunc","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_lnld","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_ldye","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearEyelid_os(){
	setfieldvalue("ext_lul","");
	setfieldvalue("ext_lll","");
	setfieldvalue("ext_llake","");
	setfieldvalue("ext_lirrig","");
	setfieldvalue("ext_lpunc","");
	setfieldvalue("ext_lnld","");
	setfieldvalue("ext_ldye","");
}

function setAnterior_od(){
	setfieldvalue("a_rk","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
	setfieldvalue("a_rconj","<bean:message key="oscarEncounter.eyeExam.white"/>");
	setfieldvalue("a_rac","<bean:message key="oscarEncounter.eyeExam.deepAndQuite"/>");
	//setfieldvalue("a_rangle_1","");
	//setfieldvalue("a_rangle_2","");
	
	//setfieldvalue("a_rangle_3","<bean:message key="oscarEncounter.eyeExam.open"/>");	
	<%	if(eyeform_normal_value_open) {	%>
	setfieldvalue("a_rangle_3","<bean:message key="oscarEncounter.eyeExam.open"/>");
	<%} else { %>		
	setfieldvalue("a_rangle_3","<bean:message key="oscarEncounter.eyeExam.open.blank"/>");
	<% } %>
	
	//setfieldvalue("a_rangle_4","");
	//setfieldvalue("a_rangle_5","");
	setfieldvalue("a_riris","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("a_rlens","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
}

function clearAnterior_od(){
	setfieldvalue("a_rk","");
	setfieldvalue("a_rconj","");
	setfieldvalue("a_rac","");
	setfieldvalue("a_rangle_1","");
	setfieldvalue("a_rangle_2","");
	setfieldvalue("a_rangle_3","");
	setfieldvalue("a_rangle_4","");
	setfieldvalue("a_rangle_5","");
	setfieldvalue("a_riris","");
	setfieldvalue("a_rlens","");
}

function setAnterior_os(){
	setfieldvalue("a_lk","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
	setfieldvalue("a_lconj","<bean:message key="oscarEncounter.eyeExam.white"/>");
	setfieldvalue("a_lac","<bean:message key="oscarEncounter.eyeExam.deepAndQuite"/>");
	//setfieldvalue("a_langle_1","");
	//setfieldvalue("a_langle_2","");
	
	//setfieldvalue("a_langle_3","<bean:message key="oscarEncounter.eyeExam.open"/>");
	<%	if(eyeform_normal_value_open) {	%>
	setfieldvalue("a_langle_3","<bean:message key="oscarEncounter.eyeExam.open"/>");
	<%} else { %>		
	setfieldvalue("a_langle_3","<bean:message key="oscarEncounter.eyeExam.open.blank"/>");
	<% } %>
	
	//setfieldvalue("a_langle_4","");
	//setfieldvalue("a_langle_5","");
	setfieldvalue("a_liris","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("a_llens","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
}

function clearAnterior_os(){
	setfieldvalue("a_lk","");
	setfieldvalue("a_lconj","");
	setfieldvalue("a_lac","");
	setfieldvalue("a_langle_1","");
	setfieldvalue("a_langle_2","");
	setfieldvalue("a_langle_3","");
	setfieldvalue("a_langle_4","");
	setfieldvalue("a_langle_5","");
	setfieldvalue("a_liris","");
	setfieldvalue("a_llens","");
}

function setPosterior_od(){
	setfieldvalue("p_rdisc","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	//setfieldvalue("p_rcd","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("p_rmac","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("p_rret","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("p_rvit","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
	//setfieldvalue("p_rcrt","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
}

function clearPosterior_od(){
	setfieldvalue("p_rdisc","");
	setfieldvalue("p_rcd","");
	setfieldvalue("p_rmac","");
	setfieldvalue("p_rret","");
	setfieldvalue("p_rvit","");
	setfieldvalue("p_rcrt","");
}

function setPosterior_os(){
	setfieldvalue("p_ldisc","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	//setfieldvalue("p_lcd","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("p_lmac","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("p_lret","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("p_lvit","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
	//setfieldvalue("p_lcrt","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
}

function clearPosterior_os(){
	setfieldvalue("p_ldisc","");
	setfieldvalue("p_lcd","");
	setfieldvalue("p_lmac","");
	setfieldvalue("p_lret","");
	setfieldvalue("p_lvit","");
	setfieldvalue("p_lcrt","");
}

function copySpecs(){
	jQuery.ajax({dataType: "script", url:ctx+"/eyeform/SpecsHistory.do?demographicNo="+demographicNo+"&method=copySpecs"});
}

function expandAll() {
	jQuery(".title a").each(function() {
		if(this.id.indexOf('a_')!=-1) {
			expanddiv(this);
		}
	});
}

function collapseAll() {
	jQuery(".title a").each(function() {
		if(this.id.indexOf('a_')!=-1) {
			collapsediv(this);
		}
	});
}

<%if(null != props1.getProperty("eyeform_optometry_device") && props1.getProperty("eyeform_optometry_device").equals("yes")){%>
jQuery(function(){
	jQuery.ajax({
		url: "<%=request.getContextPath()%>/oscarEncounter/DeviceData.do?action=getAllDevicesName",
		type: "POST",
		async: false,
		success: function(data){
			if(typeof(data) == "string"){
				data = JSON.parse(data);
			}
			if(data.length > 0){
				var deviceHtml = "";
				for(var i  = 0;i < data.length;i ++){
					deviceHtml += "<option value='" + data[i] + "'>" + data[i] + "</option>";
				}
				jQuery("#devicesName").html(deviceHtml);
			}
		}
	});
});
<%}%>

function getDeviceData(){
	var deviceName = jQuery("#devicesName").val();
	//get data
	jQuery.ajax({
		url: "<%=request.getContextPath()%>/oscarEncounter/DeviceData.do?action=getDeviceDataByName&deviceName=" + deviceName + "&demographicNo="+demographicNo,
		type: "POST",
		async: false,
		success: function(data){
			if(typeof(data) == "string"){
				data = JSON.parse(data);
			}
			//if(data){
				if(data.dataIsRight == "false"){
					alert("Incompleted data, please upload again.");
				}else{
					//set data to page
					if(deviceName.indexOf("LM-600Pd") > -1){
						var open_s_20 = false;
						//add new row fo glass hx
						var rowNum = jQuery("#myTable select").length;
						if(rowNum < 5){
							if(data.odSph || data.osSph){
								insRow();
								rowNum = rowNum + 1;
								jQuery("#type00" + rowNum).val(data.dataType);
							}
							//OD
							if(data.odSph){
								open_s_20 = true;
								
								jQuery("#odSph" + rowNum).val(data.odSph);
								jQuery("#odCyl" + rowNum).val(data.odCyl);
								jQuery("#odAxis" + rowNum).val(data.odAxis);
								jQuery("#odAdd" + rowNum).val(data.odAdd);
								jQuery("#odPrism" + rowNum).val(data.odPrism);
							}
							
							//OS
							if(data.osSph){
								open_s_20 = true;
								
								jQuery("#osSph" + rowNum).val(data.osSph);
								jQuery("#osCyl" + rowNum).val(data.osCyl);
								jQuery("#osAxis" + rowNum).val(data.osAxis);
								jQuery("#osAdd" + rowNum).val(data.osAdd);
								jQuery("#osPrism" + rowNum).val(data.osPrism);
							}
						}else{
							if(data.odSph || data.osSph){
								rowNum = 1;
								jQuery("#type00" + rowNum).val(data.dataType);
							}
							//OD
							if(data.odSph){
								open_s_20 = true;
								
								jQuery("#odSph" + rowNum).val(data.odSph);
								jQuery("#odCyl" + rowNum).val(data.odCyl);
								jQuery("#odAxis" + rowNum).val(data.odAxis);
								jQuery("#odAdd" + rowNum).val(data.odAdd);
								jQuery("#odPrism" + rowNum).val(data.odPrism);
							}
							
							//OS
							if(data.osSph){
								open_s_20 = true;
								
								jQuery("#osSph" + rowNum).val(data.osSph);
								jQuery("#osCyl" + rowNum).val(data.osCyl);
								jQuery("#osAxis" + rowNum).val(data.osAxis);
								jQuery("#osAdd" + rowNum).val(data.osAdd);
								jQuery("#osPrism" + rowNum).val(data.osPrism);
							}
						}
						if(data.odSph){
							jQuery("#odSph" + rowNum)[0].className = "examfieldwhite";
							jQuery("#odCyl" + rowNum)[0].className = "examfieldwhite";
							jQuery("#odAxis" + rowNum)[0].className = "examfieldwhite";
							jQuery("#odAdd" + rowNum)[0].className = "examfieldwhite";
							jQuery("#odPrism" + rowNum)[0].className = "examfieldwhite";
						}
						
						if(data.osSph){
							jQuery("#osSph" + rowNum)[0].className = "examfieldwhite";
							jQuery("#osCyl" + rowNum)[0].className = "examfieldwhite";
							jQuery("#osAxis" + rowNum)[0].className = "examfieldwhite";
							jQuery("#osAdd" + rowNum)[0].className = "examfieldwhite";
							jQuery("#osPrism" + rowNum)[0].className = "examfieldwhite";
							
						}
						if(open_s_20){
							var time = new Date();
						  	var y = time.getFullYear();
						  	var m = time.getMonth()+1;
						  	var d = time.getDate();
						  	if(m < 10){
						  		m = "0" + m;
						  	}
						  	if(d < 10){
						  		d = "0" + d;
						  	}
						  	var nowTime = y + "-" + m + "-" + d;
						  	jQuery("#gl_date" + rowNum).val(nowTime);  
							jQuery("#gl_date" + rowNum)[0].className = "examfieldwhite";
							
							if(jQuery('#s_20').is(":hidden")){
								jQuery('#s_20').toggle();
							}
						}
					}
					if(deviceName.indexOf("TONOREF3") > -1){
						var open_s_23 = false;
						var open_s_25 = false;
						//AR
						if(data.v_ls){
							setfieldvalue("v_ls", data.v_ls);
							setfieldvalue("v_lc", data.v_lc);
							setfieldvalue("v_lx", data.v_lx);
							open_s_23 = true;
						}
						if(data.v_rs){
							setfieldvalue("v_rs", data.v_rs);
							setfieldvalue("v_rc", data.v_rc);
							setfieldvalue("v_rx", data.v_rx);
							open_s_23 = true;
						}
						
						//PD
						/* if(data.v_pd){
							setfieldvalue("v_pd", data.v_pd);
						} */
						
						//IOP
						if(data.iop_ln){
							setfieldvalue("iop_ln", data.iop_ln);
							open_s_25 = true;
						}
						if(data.iop_rn){
							setfieldvalue("iop_rn", data.iop_rn);
							open_s_25 = true;
						}
						if(data.cct_l){
							setfieldvalue("cct_l", data.cct_l);
							open_s_25 = true;
						}
						if(data.cct_r){
							setfieldvalue("cct_r", data.cct_r);
							open_s_25 = true;
						}
						
						//R
						if(data.v_lr1){
							setfieldvalue("v_lr1", data.v_lr1);
							setfieldvalue("v_lr2", data.v_lr2);
							open_s_23 = true;
						}
						if(data.v_rr1){
							setfieldvalue("v_rr1", data.v_rr1);
							setfieldvalue("v_rr2", data.v_rr2);
							open_s_23 = true;
						}
						
						//K
						if(data.v_lk1){
							setfieldvalue("v_lk1", data.v_lk1);
							setfieldvalue("v_lk2", data.v_lk2);
							setfieldvalue("v_lkx", data.v_lkx);
							open_s_23 = true;
						}
						if(data.v_rk1){
							setfieldvalue("v_rk1", data.v_rk1);
							setfieldvalue("v_rk2", data.v_rk2);
							setfieldvalue("v_rkx", data.v_rkx);
							open_s_23 = true;
						}
						
						if(open_s_23 == true){
							if(jQuery('#s_23').is(":hidden")){
								jQuery('#s_23').toggle();
							}
						}
						if(open_s_25 == true){
							if(jQuery('#s_25').is(":hidden")){
								jQuery('#s_25').toggle();
							}
						}
					}
					if(deviceName.indexOf("RT5100") > -1){
						/* setfieldvalue("v_rs_finial", data.v_rs_finial);
						setfieldvalue("v_rc_finial", data.v_rc_finial);
						setfieldvalue("v_rx_finial", data.v_rx_finial);
						
						setfieldvalue("v_ls_finial", data.v_ls_finial);
						setfieldvalue("v_lc_finial", data.v_lc_finial);
						setfieldvalue("v_lx_finial", data.v_lx_finial); */
						//M dist
						var open_s_23 = false;
						if(data.v_rds){
							setfieldvalue("v_rds", data.v_rds);
							setfieldvalue("v_rdc", data.v_rdc);
							setfieldvalue("v_rdx", data.v_rdx);
							open_s_23 = true;
						}
						if(data.v_lds){
							setfieldvalue("v_lds", data.v_lds);
							setfieldvalue("v_ldc", data.v_ldc);
							setfieldvalue("v_ldx", data.v_ldx);
							open_s_23 = true;
						}
						//M near
						if(data.v_lns){
							setfieldvalue("v_lns", data.v_lns);
							open_s_23 = true;
						}
						if(data.v_rns){
							setfieldvalue("v_rns", data.v_rns);
							open_s_23 = true;
						}
						//PD
						if(data.v_pd){
							setfieldvalue("v_pd", data.v_pd);
							open_s_23 = true;
						}
						
						if(open_s_23 == true){
							if(jQuery('#s_23').is(":hidden")){
								jQuery('#s_23').toggle();
							}
						}
					}
				}
			//}
		}
	});
	
	/* var measurementName = "";
	var data1;
	setfieldvalue(measurementName, data1); */
}
</script>

<span style="font-size:10px">
	<a id="save_measurements" href="javascript:void(0)"  onclick="hxForm_sumbit();">[Save Measurements]</a>
</span>
<%if(null != props1.getProperty("eyeform_optometry_device") && props1.getProperty("eyeform_optometry_device").equals("yes")){%>
<span style="margin-left: 50px">
	Devices Name:
	<select id="devicesName" style="width:100px">
	</select>
</span>
<span>
	<input type="button" value="Get Device Data" onclick="getDeviceData()">
</span>
<%} %>
<span style="float:right;font-size:10px">
	<a href="javascript:void(0);" onclick="expandAll();">[expand all sections]</a>&nbsp;
	<a href="javascript:void(0);" onclick="collapseAll();">[collapse all sections]</a>
</span>

<table border="0" width="100%">
<% if (!glasses_hx) {%>
<tr>
<td>
<div class="slidey">
 <form action="<c:out value="${ctx}"/>/eyeform/SpecsHistory.do" method="post" name="specsHistoryForm" id="hxForm" target="ifFrame">
	<div class="title">
		<a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" href="javascript:void(0)" tabindex="31" id="a_20" onclick="togglediv(this);">Glasses Hx:</a>
		<span>&nbsp;&nbsp;</span>

             <a href="javascript:void(0)" tabindex="32" onclick="popupPage(500,900,'examinationhistory1','<c:out value="${ctx}"/>/eyeform/ExaminationHistory.do?demographicNo=<%=request.getParameter("demographic_no")%>&method=query&oldglasses=true&fromlist1=GLASSES HISTORY'); return false;">[old glasses]</a>
	          <span>&nbsp;&nbsp;</span>
              <a href="javascript:void(0)" onclick="insRow()">[add]</a>
			  
			  <span>&nbsp;&nbsp;</span>
			  
			  <a href="javascript:void(0)"  onclick="document.getElementById('hxForm').submit();" style="display:none">[save]</a>
	</div>
	<input type="hidden" value="save" name="method">
	 <input type="hidden" value='<%=request.getParameter("demographic_no")%>' name="specs.demographicNo" id="demographic_no">
     <input type="hidden" value='<%= request.getParameter("appointment_no")%>' name="specs.appointmentNo" id="appointment_no">
	<div id="s_20"  <%=glass_show%>>
		<table class="exam" width="100%" id="myTable">
		<tr>
			<td width="10%"><a href="javascript:void(0)" tabindex="33" onclick="copyglasses();return false;">[copy]</a></td>
			
			<td colspan="5" width="36%">OD</td>
			<td width="4%"></td>
			<td colspan="5"width="36%">OS</td>
			<td width="8%"></td>
			<td width="1%"></td>
			<td width="1%"></td>
		</tr>
		<tr>
		    <td >Type</td>
			<td>s</td>
			<td>c</td>
			<td>x</td>
			<td>add</td>
			<td>prism</td>
			<td width="4%"></td>
			<td>s</td>
			<td>c</td>
			<td>x</td>
			<td>add</td>
			<td>prism</td>
			<td>date</td>				
			<td width="8%"></td>			
			<td></td>
		</tr>
		<%
		int alreadyGlassNum = 0;
		int num_tab = 33;
		for(int i = 0;i < glassType.size();i ++){
			List<String> setSpecsValue = specsValueList.get(glassType.get(i));
			if(null != setSpecsValue){
				alreadyGlassNum ++;
		%>
		<tr>
		  	<td>
             <select name="specs.type<%=alreadyGlassNum%>" style="width:100%" id="type00<%=alreadyGlassNum%>"  onchange="changeValue(<%=alreadyGlassNum%>)">
   	            <option value="distance" <%= glassType.get(i).equals("distance")? "selected":""%> >distance</option>
            	<option value="bifocal" <%= glassType.get(i).equals("bifocal")? "selected":""%>>bifocal</option>
            	<option value="invis bfocal" <%= glassType.get(i).equals("invis bfocal")? "selected":""%>>invis bfocal</option>
                <option value="reading" <%= glassType.get(i).equals("reading")? "selected":""%>>reading</option>
                <option value="trifocal" <%= glassType.get(i).equals("trifocal")? "selected":""%>>trifocal</option>
                <option value="blank"></option>
            </select>
         	</td>
         	<td>
         		<input name="specs.odSph<%=alreadyGlassNum%>" id="odSph<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value="<%=setSpecsValue.get(0)%>"/>
         	</td>
         	<td>
         		<input name="specs.odCyl<%=alreadyGlassNum%>" id="odCyl<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value="<%=setSpecsValue.get(1)%>"/>
         	</td>
			 <td>
			 	<input name="specs.odAxis<%=alreadyGlassNum%>" id="odAxis<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"   onfocus="whiteField(this);" value="<%=setSpecsValue.get(2)%>"/>
			 </td>
			 <td>
			 	<input name="specs.odAdd<%=alreadyGlassNum%>" id="odAdd<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value="<%=setSpecsValue.get(3)%>"/>
			 </td>
			 <td>
			 	<input name="specs.odPrism<%=alreadyGlassNum%>" id="odPrism<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="12" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value="<%=setSpecsValue.get(4)%>"/>
			 </td>		
			 <td width="4%"></td>
			 <td>
			 	<input name="specs.osSph<%=alreadyGlassNum%>" id="osSph<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value="<%=setSpecsValue.get(5)%>"/>
			 </td>
			 <td>
			 	<input name="specs.osCyl<%=alreadyGlassNum%>" id="osCyl<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value="<%=setSpecsValue.get(6)%>"/>
			 </td>
			 <td>
			 	<input name="specs.osAxis<%=alreadyGlassNum%>"id="osAxis<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value="<%=setSpecsValue.get(7)%>"/>
			 </td>
			 <td>
			 	<input name="specs.osAdd<%=alreadyGlassNum%>" id="osAdd<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"   onfocus="whiteField(this);" value="<%=setSpecsValue.get(8)%>"/>
			 </td>
			 <td>
			 	<input name="specs.osPrism<%=alreadyGlassNum%>" id="osPrism<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="12" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value="<%=setSpecsValue.get(9)%>"/>
			 </td>
			 <td>
			 	<input name="specs.dateStr<%=alreadyGlassNum%>"  type="text" tabindex="<%=num_tab ++%>" class="examfieldgrey" style="width:115%"  id="gl_date<%=alreadyGlassNum%>" onfocus="whiteField(this);" value="<%=setSpecsValue.get(10)%>"/>		 
			 </td>			 
			 <td width="8%">
			 	<img src="<%=request.getContextPath()%>/images/cal.gif" id="pdate_cal<%=alreadyGlassNum%>">
			 	<script type="text/javascript">
					Calendar.setup({ inputField : "gl_date<%=alreadyGlassNum%>", ifFormat : "%Y-%m-%d", showsTime :false, button : "pdate_cal<%=alreadyGlassNum%>", singleClick : true, step : 1 });
				</script>
			 <td>
			 	<a href="javascript:void(0)" onclick="hxOpen(<%=alreadyGlassNum%>);"><img src="../images/notes.gif"  /> </a>
			 </td>
             <td>
             	<input type="hidden" value="" name="specs.id<%=alreadyGlassNum%>" id="specs.id<%=alreadyGlassNum%>">
             </td>
         </tr>	
		<%
			}
		}
		if(alreadyGlassNum == 0){
			alreadyGlassNum = 1;
		%>
		<tr>
		  	<td>
             <select name="specs.type<%=alreadyGlassNum%>" style="width:100%" id="type00<%=alreadyGlassNum%>"  onchange="changeValue(<%=alreadyGlassNum%>)">
   	            <option value="distance" >distance</option>
            	<option value="bifocal" >bifocal</option>
            	<option value="invis bfocal" >invis bfocal</option>
                <option value="reading" >reading</option>
                <option value="trifocal" >trifocal</option>
                <option value="blank"></option>
            </select>
         	</td>
         	<td>
         		<input name="specs.odSph<%=alreadyGlassNum%>" id="odSph<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value=""/>
         	</td>
         	<td>
         		<input name="specs.odCyl<%=alreadyGlassNum%>" id="odCyl<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value=""/>
         	</td>
			 <td>
			 	<input name="specs.odAxis<%=alreadyGlassNum%>" id="odAxis<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"   onfocus="whiteField(this);" value=""/>
			 </td>
			 <td>
			 	<input name="specs.odAdd<%=alreadyGlassNum%>" id="odAdd<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value=""/>
			 </td>
			 <td>
			 	<input name="specs.odPrism<%=alreadyGlassNum%>" id="odPrism<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="12" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value=""/>
			 </td>		
			 <td width="4%"></td>
			 <td>
			 	<input name="specs.osSph<%=alreadyGlassNum%>" id="osSph<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value=""/>
			 </td>
			 <td>
			 	<input name="specs.osCyl<%=alreadyGlassNum%>" id="osCyl<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value=""/>
			 </td>
			 <td>
			 	<input name="specs.osAxis<%=alreadyGlassNum%>"id="osAxis<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value=""/>
			 </td>
			 <td>
			 	<input name="specs.osAdd<%=alreadyGlassNum%>" id="osAdd<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="6" class="examfieldgrey" style="width:95%"   onfocus="whiteField(this);" value=""/>
			 </td>
			 <td>
			 	<input name="specs.osPrism<%=alreadyGlassNum%>" id="osPrism<%=alreadyGlassNum%>" type="text" tabindex="<%=num_tab ++%>" maxlength="12" class="examfieldgrey" style="width:95%"  onfocus="whiteField(this);" value=""/>
			 </td>
			 <td>
			 	<input name="specs.dateStr<%=alreadyGlassNum%>"  type="text" tabindex="<%=num_tab ++%>" class="examfieldgrey" style="width:115%"  id="gl_date<%=alreadyGlassNum%>" onfocus="whiteField(this);" value=""/>		 
			 </td>			 
			 <td width="8%">
			 	<img src="<%=request.getContextPath()%>/images/cal.gif" id="pdate_cal<%=alreadyGlassNum%>">
			 	<script type="text/javascript">
					Calendar.setup({ inputField : "gl_date<%=alreadyGlassNum%>", ifFormat : "%Y-%m-%d", showsTime :false, button : "pdate_cal<%=alreadyGlassNum%>", singleClick : true, step : 1 });
				</script>
			 <td>
			 	<a href="javascript:void(0)" onclick="hxOpen(<%=alreadyGlassNum%>);"><img src="../images/notes.gif"  /> </a>
			 </td>
             <td>
             	<input type="hidden" value="" name="specs.id<%=alreadyGlassNum%>" id="specs.id<%=alreadyGlassNum%>">
             </td>
         </tr>	
		<%
		}
		%>
		</table>		
	</div>
	</form>
	<iframe style="display:none" id="ifFrame" name="ifFrame" src="about:blank">
	</iframe>
</div>
</td>
</tr>
<%}%>
<% if (!vision_assessment){%>
<tr>
<td>
<div class="slidey">
	 <div class="title">
            <a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" href="javascript:void(0)" tabindex="77" id="a_22" onclick="togglediv(this);">Vision Assessment:</a>
            <!--
            <span>&nbsp;&nbsp;</span>

            
            <a href="javascript:void(0)" tabindex="357" onclick="setPretest();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
            <a href="javascript:void(0)" tabindex="358" onclick="clearPretest();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
            -->
        </div>
        <div id="s_22" class="slideblock">
            <table class="exam" width="100%">
            	<tr>
            		<td width="8%"></td>
            		<td width="24%" colspan="3">OD</td>
					<td width="8%"></td>
            		<td width="24%" colspan="3">OS</td>
					<td width="8%"></td>
            		<td width="24%" colspan="3">OU</td>
					<td width="8%"></td>
            	</tr>
            	<tr>
            		<td width="8%"></td>
            		<td width="8%">sc</td>
            		<td width="8%">cc</td>
            		<td width="8%">ph</td>
					<td width="8%"></td>
            		<td width="8%">sc</td>
            		<td width="8%">cc</td>
            		<td width="8%">ph</td>
            		<td width="8%"></td>
					<td width="8%"></td>
            		<td width="8%">sc</td>
            		<td width="8%">cc</td>
					<td width="8%"></td>
            	</tr>
            	<tr>
            		<td class="label">Dist</td>
            		<td><input type="text" tabindex="78" maxlength="15" class="examfieldgrey" style="width:99%" measurement="v_rdsc" onfocus="whiteField(this);"/></td>
            		<td><input type="text" tabindex="79" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_rdcc" onfocus="whiteField(this);"/></td>
            		<td><input type="text" tabindex="80" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_rph" onfocus="whiteField(this);"/></td>
					<td></td>
            		<td><input type="text" tabindex="81" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_ldsc" onfocus="whiteField(this);"/></td>
            		<td><input type="text" tabindex="82" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_ldcc" onfocus="whiteField(this);"/></td>
            		<td><input type="text" tabindex="83" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_lph" onfocus="whiteField(this);"/></td>
					<td></td>
            		<td colspan="2">Dist <input type="text" tabindex="84" maxlength="15" class="examfieldgrey" style="width:50%" measurement="v_dsc" onfocus="whiteField(this);"/></td>
            		<td><input type="text" tabindex="85" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_dcc" onfocus="whiteField(this);"/></td>
					<td></td>
            	</tr>
            	<tr>
            		<td class="label">Int</td>
            		<td><input type="text" tabindex="86" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_risc" onfocus="whiteField(this);"/></td>
            		<td><input type="text" tabindex="87" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_ricc" onfocus="whiteField(this);"/></td>
            		<td></td>
					<td></td>
            		<td><input type="text" tabindex="88" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_lisc" onfocus="whiteField(this);"/></td>
            		<td><input type="text" tabindex="89" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_licc" onfocus="whiteField(this);"/></td>
            		<td></td>
					<td></td>
            		<td colspan="2">Int &nbsp;&nbsp;<input type="text" tabindex="90" maxlength="15" class="examfieldgrey" style="width:50%" measurement="v_isc" onfocus="whiteField(this);"/></td>
            		<td><input type="text" tabindex="91" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_icc" onfocus="whiteField(this);"/></td>
					<td></td>
            	</tr>
            	<tr>
            		<td class="label">Near</td>
            		<td><input type="text" tabindex="92" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_rnsc" onfocus="whiteField(this);"/></td>
            		<td><input type="text" tabindex="93" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_rncc" onfocus="whiteField(this);"/></td>
            		<td></td>
					<td></td>
            		<td><input type="text" tabindex="94" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_lnsc" onfocus="whiteField(this);"/></td>
            		<td><input type="text" tabindex="95" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_lncc" onfocus="whiteField(this);"/></td>
            		<td></td>
					<td></td>
            		<td colspan="2">Near <input type="text" tabindex="96" maxlength="15" class="examfieldgrey" style="width:50%" measurement="v_nsc" onfocus="whiteField(this);"/></td>
            		<td><input type="text" tabindex="97" maxlength="15" class="examfieldgrey" style="width:94%" measurement="v_ncc" onfocus="whiteField(this);"/></td>
					<td></td>
            	</tr>
            </table>
        </div>
</div>
</td>
</tr>
<%}%>
<% if (!vision_measurement) {%>
<tr>
<td>
<div class="slidey">
	<div class="title">
        <a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" tabindex="98" href="javascript:void(0)" id="a_23" onclick="togglediv(this);">Vision Measurement:</a>
        <span>&nbsp;&nbsp;</span>
		<!--
    	<a href="javascript:void(0)" tabindex="66" onclick="clearPretest();return false;">[PRINT Rx]</a>   
		-->
    </div>
    <div id="s_23" class="slideblock">
    	<table class="exam" width="100%">
    		<tr>
    			<td width="8%"></td>
    			<td colspan="5" width="32%">OD
    				<span>&nbsp;&nbsp;</span>
    				<a href="javascript:void(0)" tabindex="99" onclick="clearMeasurement_od();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
    			</td>
				<td width="6%"></td>
    			<td colspan="5" width="32%">OS
    				<span>&nbsp;&nbsp;</span>
    				<a href="javascript:void(0)" tabindex="100" onclick="clearMeasurement_os();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
    			</td>
				<td width="6%"></td>
    			<td colspan="2" width="16%">OU</td>
    		</tr>
    		<tr>
    			<td></td>
    			<td>K1</td>
    			<td>K2</td>
    			<td>K2 axis</td>
    			<td rowspan="2"></td>
				<td></td>
				<td></td>
    			<td>K1</td>
    			<td>K2</td>
    			<td>K2 axis</td>
    			<td rowspan="2"></td>
				<td></td>
				<td></td>
    			<td colspan="2"></td>
    		</tr>
    		<tr>
    			<td>K</td>
    			<td><input type="text"  tabindex="101" maxlength="8" class="examfieldgrey" style="width:99%"  measurement="v_rk1" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="102" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rk2" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="103" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rkx" onfocus="whiteField(this);"/></td>
				<td></td>
				<td></td>
    			<td><input type="text"  tabindex="104" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lk1" onfocus="whiteField(this);"/></td>				
    			<td><input type="text"  tabindex="105" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lk2" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="106" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lkx" onfocus="whiteField(this);"/></td>
				<td></td>
				<td></td>
    			<td>Fly</td>
    			<td><input type="text"  tabindex="142" maxlength="8" class="examfieldgrey" style="width:80%" measurement="v_fly" onfocus="whiteField(this);"/></td>
    		</tr>
    		<%if(null != props1.getProperty("eyeform_optometry_device") && props1.getProperty("eyeform_optometry_device").equals("yes")){%>
    		<tr>
    			<td></td>
    			<td>R1</td>
    			<td>R2</td>
    			<td></td>
    			<td rowspan="2"></td>
				<td></td>
				<td></td>
    			<td>R1</td>
    			<td>R2</td>
    			<td></td>
    			<td rowspan="2"></td>
				<td></td>
				<td></td>
    			<td colspan="2"></td>
    		</tr>
    		<tr>
    			<td>R</td>
    			<td><input type="text"  tabindex="106" maxlength="8" class="examfieldgrey" style="width:99%"  measurement="v_rr1" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="106" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rr2" onfocus="whiteField(this);"/></td>
    			<td></td>
				<td></td>
				<td></td>
    			<td><input type="text"  tabindex="106" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lr1" onfocus="whiteField(this);"/></td>				
    			<td><input type="text"  tabindex="106" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lr2" onfocus="whiteField(this);"/></td>
    			<td></td>
				<td></td>
				<td></td>
    			<td></td>
    			<td></td>
    		</tr>	
    		<%} %>
    		<tr>
    			<td></td>
    			<td>S</td>
    			<td>C</td>
    			<td>x</td>
    			<td>prism</td>
    			<td>VA</td>
				<td></td>
    			<td>S</td>
    			<td>C</td>
    			<td>x</td>
    			<td>prism</td>
    			<td>VA</td>
				<td></td>
    			<td>Stereo</td>
    			<td><input type="text"  tabindex="143" maxlength="15" class="examfieldgrey" style="width:80%" measurement="v_stereo" onfocus="whiteField(this);"/></td>
    		</tr>
    		<tr>
    			<td>
   				<%if(null != props1.getProperty("eyeform_vision_measurement_has_two_AR_M") && props1.getProperty("eyeform_vision_measurement_has_two_AR_M").equals("yes")){%>
	    		AR (-)
	    		<%}else{%>
	    		AR
	    		<%}%>
    				<span>&nbsp;&nbsp;</span>
    				<a href="javascript:void(0)" tabindex="115" onclick="copyar();return false;">[copy]</a>
    			</td>
    			<td><input type="text"  tabindex="107" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rs" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="108" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rc" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="109" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rx" onfocus="whiteField(this);"/></td>
    			<td></td>
    			<td><input type="text"  tabindex="110" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rar" onfocus="whiteField(this);"/></td>
				<td></td>
    			<td><input type="text"  tabindex="111" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_ls" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="112" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lc" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="113" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lx" onfocus="whiteField(this);"/></td>
    			<td></td>
    			<td><input type="text"  tabindex="114" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lar" onfocus="whiteField(this);"/></td>
				<td></td>
				<%if(null != props1.getProperty("eyeform_optometry_device") && props1.getProperty("eyeform_optometry_device").equals("yes")){%>
				<td>PD</td>
				<td><input type="text"  tabindex="143" maxlength="15" class="examfieldgrey" style="width:80%" measurement="v_pd" onfocus="whiteField(this);"/></td>
				<%}else{ %>
				<td colspan="2"></td>
				<%} %>
    			
    		</tr>
    		<%if(null != props1.getProperty("eyeform_vision_measurement_has_two_AR_M") && props1.getProperty("eyeform_vision_measurement_has_two_AR_M").equals("yes")){%>
    		<tr>
    			<td>
	    			AR (+)
    				<span>&nbsp;&nbsp;</span>
    				<a href="javascript:void(0)" tabindex="115" onclick="copyar_add();return false;">[copy]</a>
    			</td>
    			<td><input type="text"  tabindex="115" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rs_add" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="115" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rc_add" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="115" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rx_add" onfocus="whiteField(this);"/></td>
    			<td></td>
    			<td><input type="text"  tabindex="115" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rar_add" onfocus="whiteField(this);"/></td>
				<td></td>
    			<td><input type="text"  tabindex="115" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_ls_add" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="115" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lc_add" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="115" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lx_add" onfocus="whiteField(this);"/></td>
    			<td></td>
    			<td><input type="text"  tabindex="115" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lar_add" onfocus="whiteField(this);"/></td>
				<td></td>
    			<td colspan="2"></td>
    		</tr>
    		<%} %>
    		<tr>
    			<td>
    			<%if(null != props1.getProperty("eyeform_vision_measurement_has_two_AR_M") && props1.getProperty("eyeform_vision_measurement_has_two_AR_M").equals("yes")){%>
	    		M (- dist)
	    		<%}else{%>
	    		M (dist)
	    		<%}%>
    				<span>&nbsp;&nbsp;</span>
    				<a href="javascript:void(0)" tabindex="115" onclick="copy_mdist();return false;">[copy]</a>
    				<a href="javascript:void(0)" tabindex="115" onclick="paste_mdist();return false;">[paste]</a>
    			</td>
    			<td><input type="text"  tabindex="116" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rds" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="117" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rdc" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="118" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rdx" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="119" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rdp" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="119" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rdv" onfocus="whiteField(this);"/></td>
				<td></td>
    			<td><input type="text"  tabindex="120" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lds" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="121" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_ldc" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="122" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_ldx" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="122" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_ldp" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_ldv" onfocus="whiteField(this);"/></td>
				<td></td>
    			<td>Dist</td>
    			<td><input type="text"  tabindex="144" maxlength="8" class="examfieldgrey" style="width:80%" measurement="v_dv" onfocus="whiteField(this);"/></td>
    		</tr>
    		<%if(null != props1.getProperty("eyeform_optometry_device") && props1.getProperty("eyeform_optometry_device").equals("yes")){%>
    		<tr>
    			<td>
	    			Final
    				<span>&nbsp;&nbsp;</span>
    				<a href="javascript:void(0)" tabindex="123" onclick="paste_final();return false;">[paste]</a>
    			</td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rs_finial" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rc_finial" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rx_finial" onfocus="whiteField(this);"/></td>
    			<td></td>
    			<td></td>
				<td></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_ls_finial" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lc_finial" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lx_finial" onfocus="whiteField(this);"/></td>
    			<td></td>
    			<td></td>
				<td></td>
    			<td colspan="2"></td>
    		</tr>
    		<%} %>
    		<%if(null != props1.getProperty("eyeform_vision_measurement_has_two_AR_M") && props1.getProperty("eyeform_vision_measurement_has_two_AR_M").equals("yes")){%>
    		<tr>
    			<td>
	    			M (+ dist)
    				<span>&nbsp;&nbsp;</span>
    				<a href="javascript:void(0)" tabindex="123" onclick="paste_mdist_add();return false;">[paste]</a>
    			</td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rds_add" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rdc_add" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rdx_add" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rdp_add" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rdv_add" onfocus="whiteField(this);"/></td>
				<td></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lds_add" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_ldc_add" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_ldx_add" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_ldp_add" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="123" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_ldv_add" onfocus="whiteField(this);"/></td>
				<td></td>
    			<td>Dist</td>
    			<td><input type="text"  tabindex="144" maxlength="8" class="examfieldgrey" style="width:80%" measurement="v_dv_add" onfocus="whiteField(this);"/></td>
    		</tr>
    		<%} %>
			<tr>
    			<td>Cyclo
    				<span>&nbsp;&nbsp;</span>
    				<a href="javascript:void(0)" tabindex="124" onclick="paste_Cyclo();return false;">[paste]</a>
    			</td>
    			<td><input type="text"  tabindex="125" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rcs" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="126" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rcc" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="127" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rcx" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="127" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rcp" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="128" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rcv" onfocus="whiteField(this);"/></td>
				<td></td>
    			<td><input type="text"  tabindex="129" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lcs" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="130" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lcc" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="131" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lcx" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="131" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lcp" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="132" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lcv" onfocus="whiteField(this);"/></td>
				<td></td>
    			<td colspan="2"></td>
    		</tr>
    		<tr>
    			<td>M (near)
    				<span>&nbsp;&nbsp;</span>
    				<a href="javascript:void(0)" tabindex="133" onclick="paste_mnear();return false;">[paste]</a>
    			</td>
    			<td><input type="text"  tabindex="134" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rns" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="135" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rnc" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="136" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rnx" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="136" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rnp" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="137" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_rnv" onfocus="whiteField(this);"/></td>
				<td></td>
    			<td><input type="text"  tabindex="138" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lns" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="139" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lnc" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="140" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lnx" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="140" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lnp" onfocus="whiteField(this);"/></td>
    			<td><input type="text"  tabindex="141" maxlength="8" class="examfieldgrey" style="width:99%" measurement="v_lnv" onfocus="whiteField(this);"/></td>
				<td></td>
    			<td>Near</td>
    			<td><input type="text"  tabindex="145" maxlength="8" class="examfieldgrey" style="width:80%" measurement="v_nv" onfocus="whiteField(this);"/></td>
    		</tr>
    		<%if(null != props1.getProperty("eyeform_optometry_device") && props1.getProperty("eyeform_optometry_device").equals("yes")){%>
    		<tr>
    			<td></td>
    			<td></td>
    			<td></td>
    			<td></td>
    			<td></td>
    			<td></td>
    			<td></td>
    			<td></td>
    			<td></td>
    			<td></td>
    			<td></td>
    			<td></td>
    			<td colspan="2" style="text-align: right">Alt Add</td>
    			<td><input type="text"  tabindex="145" maxlength="8" class="examfieldgrey" style="width:80%" measurement="v_alt_add" onfocus="whiteField(this);"/></td>
    		</tr>
    		<%} %>
    	</table>
    </div>
</div>
</td>
</tr>
<%}%>
<% if (!refractive) {%>
<tr>
<td>
<div class="slidey">
	 <div class="title">
		<a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" tabindex="146" href="javascript:void(0)" id="a_24" onclick="togglediv(this);">Refractive:</a>
		
	</div>
	<div id="s_24" class="slideblock">       	
		<table class="exam" width="100%">
			<tr>
				<td width="8%"></td>
				<td width="8%">OD</td>
				<td width="8%"></td>
				<td width="8%">OS</td>
				<td width="8%"></td>
				<td width="8%"></td>
				<td width="8%"></td>
				<td width="8%"></td>
				<td width="8%"></td>
				<td width="8%"></td>
				<td width="8%"></td>
				<td width="8%"></td>
				<td width="8%"></td>
			</tr>
			<tr>
				<td class="label">Dominance</td>				
				<td><input type="text" tabindex="147" maxlength="10" style="width:100%" measurement="ref_rdom" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td></td>
				<td><input type="text" tabindex="148" maxlength="10" style="width:100%" measurement="ref_ldom" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="label">Pupil dim</td>
				<td><input type="text" tabindex="149" maxlength="10" style="width:100%" measurement="ref_rpdim" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td></td>
				<td><input type="text" tabindex="150" maxlength="10" style="width:100%" measurement="ref_lpdim" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="label">Kappa</td>
				<td><input type="text" tabindex="151" maxlength="10" style="width:100%" measurement="ref_rkappa" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td></td>
				<td><input type="text" tabindex="152" maxlength="10" style="width:100%" measurement="ref_lkappa" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="label">W2W</td>
				<td><input type="text" tabindex="153" maxlength="10" style="width:100%" measurement="ref_rw2w" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td></td>
				<td><input type="text" tabindex="154" maxlength="10" style="width:100%" measurement="ref_lw2w" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
</div>
</td>
</tr>
<%}%>
<% if (!iop) {%>
<tr>
<td>
<div class="slidey">
	<div class="title">
		<a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" href="javascript:void(0)" tabindex="155" id="a_25" onclick="togglediv(this);">IOP:</a>

	</div>
	<div id="s_25" class="slideblock">
	<table class="exam" width="100%">
		<tr>
				<td width="8%"></td>
				<td width="44%">OD</td>
				<td width="2%"></td>
				<td width="44%">OS</td>
				<td width="2%"></td>
				
			</tr>
			<tr>
				<td width="8%" nowrap="nowrap" class="label">NCT(<span id="nct_ts"></span>)</td>
				<td width="44%"><input type="text" tabindex="156" maxlength="50" style="width:100%" measurement="iop_rn" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td width="2%"></td>
				<td width="44%"><input type="text" tabindex="157" maxlength="50" style="width:100%" measurement="iop_ln" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td width="2%"></td>
				
			</tr>
			<tr>
				<td width="8%" class="label">Applanation(<span id="applanation_ts"></span>)</td>
				<td width="44%"><input type="text" tabindex="158" maxlength="50" style="width:100%" measurement="iop_ra" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td width="2%"></td>
				<td width="44%"><input type="text" tabindex="159" maxlength="50" style="width:100%" measurement="iop_la" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td width="2%"></td>
				
			</tr>
			<tr>
				<td width="8%" class="label">CCT</td>
				<td width="44%"><input type="text" tabindex="160" maxlength="50" style="width:100%" measurement="cct_r" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td width="2%"></td>
				<td width="44%"><input type="text" tabindex="161" maxlength="50" style="width:100%" measurement="cct_l" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td width="2%"></td>
				
			</tr>
	</table>
	</div>
</div>
</td>
</tr>
<%}%>
<% if (!other_exam) {%>
<tr>
<td>
<div class="slidey">
	<div class="title">
		<a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" tabindex="162" href="javascript:void(0)" id="a_26" onclick="togglediv(this);">Other Exam:</a>

	</div>
	<div id="s_26" class="slideblock">
	<table class="exam" width="100%">
		<tr>
			<td width="8%"></td>
			<td width="24%" colspan="3">OD</td>
			<td width="4%"></td>
			<td width="24%" colspan="3">OS</td>
			<td width="8%"></td>
			<td width="32%" colspan="4">OU</td>
			<td width="4%"></td>
		</tr>
		<tr>
			<td class="label">Colour vision</td>
			<td colspan="3"><input type="text" tabindex="163" maxlength="50" style="width:98%" measurement="o_rcolour" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td width="4%"></td>
			<td colspan="3"><input type="text" tabindex="164" maxlength="50" style="width:98%" measurement="o_lcolour" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td></td>
			<td colspan="4">Maddox &nbsp; &nbsp;<input type="text" tabindex="173" maxlength="50" style="width:71%" measurement="o_mad" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td></td>
		</tr>
		<tr>
			<td class="label">Pupil</td>
			<td colspan="3"><input type="text" tabindex="165" maxlength="50" style="width:98%" measurement="o_rpupil" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td width="4%"></td>
			<td colspan="3"><input type="text" tabindex="166" maxlength="50" style="width:98%" measurement="o_lpupil" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td ></td>
			<td colspan="4">Bagolini &nbsp; &nbsp;<input type="text" tabindex="174" maxlength="50" style="width:71%" measurement="o_bag" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td ></td>
		</tr>
		<tr>
			<td class="label">Amsler grid</td>
			<td colspan="3"><input type="text" tabindex="167" maxlength="50" style="width:98%" maxlength="50" measurement="o_ramsler" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td width="4%"></td>
			<td colspan="3"><input type="text" tabindex="168" maxlength="50" style="width:98%" measurement="o_lamsler" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td ></td>
			<td colspan="4">W4D(dist)&nbsp;<input type="text" tabindex="175" maxlength="50" style="width:71%" maxlength="50" measurement="o_w4dd" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td ></td>
		</tr>
		<tr>
			<td class="label">PAM</td>
			<td colspan="3"><input type="text" tabindex="169" maxlength="50" style="width:98%" measurement="o_rpam" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td width="4%"></td>
			<td colspan="3"><input type="text" tabindex="170" maxlength="50" style="width:98%" measurement="o_lpam" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td ></td>
			<td colspan="4">W4D(near)<input type="text" tabindex="176" maxlength="50" style="width:71%" measurement="o_w4dn" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td ></td>
		</tr>
		<tr>
			<td class="label">Confrontation</td>
			<td colspan="3"><input type="text" tabindex="171" maxlength="50" style="width:98%" measurement="o_rconf" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td width="4%"></td>
			<td colspan="3"><input type="text" tabindex="172" maxlength="50" style="width:98%" measurement="o_lconf" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
			<td ></td>
			<td colspan="4"></td>
			<td ></td>
		</tr>
	</table>
	</div>
</div>
</td>
</tr>
<%}%>

<% if (!anterior_segment) {%>
<tr>
<td>

	<div class="slidey">
		 <div class="title">
            <a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" tabindex="173" href="javascript:void(0)" id="a_32" onclick="togglediv(this);">Anterior Segment:</a>
			
        </div>
        <div id="s_32" class="slideblock">
        <table class="exam" width="100%">
        	<tr>
        		<td width="8%"></td>
        		<td width="46%">OD
        			<a href="javascript:void(0)" tabindex="174" onclick="setAnterior_od();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
            		<a href="javascript:void(0)" tabindex="175" onclick="clearAnterior_od();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
        		</td>
        		<td width="46%">OS
        			<a href="javascript:void(0)" tabindex="176" onclick="setAnterior_os();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
            		<a href="javascript:void(0)" tabindex="177" onclick="clearAnterior_os();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
        		</td>
        	</tr>
        	<tr>
        		<td class="label">Cornea</td>
        		<td><input type="text" tabindex="178" style="width:93%" measurement="a_rk" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="179" style="width:93%" measurement="a_lk" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">Conj/Sclera</td>
        		<td><input type="text" tabindex="180" style="width:93%" measurement="a_rconj" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="181" style="width:93%" measurement="a_lconj" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">AC</td>
        		<td><input type="text" tabindex="182" style="width:93%" measurement="a_rac" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="183" style="width:93%" measurement="a_lac" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
				<td class="label">Angle</td>
				<td>
        			<table class="exam" style="width:100%">
        				<tr>
        					<td width="100%" align="center"><input type="text" style="width:30%" tabindex="184" measurement="a_rangle_1" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        				</tr>
        				<tr>
        					<td style="text-align:center">
	        					<input style="width:30%" type="text" tabindex="185" measurement="a_rangle_4" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
        						<input style="width:30%" type="text" tabindex="186" measurement="a_rangle_3" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
								<input style="width:30%" type="text" tabindex="187" measurement="a_rangle_2" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
        					</td>
        				</tr>
        				<tr>
        					<td><input type="text" style="width:30%" tabindex="188" measurement="a_rangle_5" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        				</tr>
        			</table>
        		</td>
        		
        		<td>
        			<table class="exam" style="width:100%">
        				<tr>
        					<td width="100%" align="center"><input style="width:30%" type="text" tabindex="189" measurement="a_langle_1" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        				</tr>

        				<tr>
        					<td style="text-align:center">
        						<input style="width:30%" type="text" tabindex="190" measurement="a_langle_4" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
								<input style="width:30%" type="text" tabindex="191" measurement="a_langle_3" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
								<input style="width:30%" type="text" tabindex="192" measurement="a_langle_2" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
        					</td>
        				</tr>

        				<tr>
        					<td><input type="text" style="width:30%" tabindex="193" measurement="a_langle_5" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        				</tr>
        			</table>
        		</td>
        	</tr>
        	<tr>
        		<td class="label">Iris</td>
        		<td><input type="text" tabindex="194" style="width:93%" measurement="a_riris" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="195" style="width:93%" measurement="a_liris" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">Lens</td>
        		<td><input type="text" tabindex="196" style="width:93%" measurement="a_rlens" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="197" style="width:93%" measurement="a_llens" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        </table>
        </div>
    </div>
</td>
</tr>
<%}%>

<% if (!posterior_segment) {%>
<tr>
<td>
	<div class="slidey">
		 <div class="title">
            <a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" tabindex="198" href="javascript:void(0)" id="a_33" onclick="togglediv(this);">Posterior Segment:</a>
			
        </div>
        <div id="s_33" class="slideblock">
        <table class="exam" width="100%">
        	<tr>
        		<td width="8%"></td>
        		<td width="46%">OD
        			<a href="javascript:void(0)" tabindex="199" onclick="setPosterior_od();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
            		<a href="javascript:void(0)" tabindex="200" onclick="clearPosterior_od();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
        		</td>
        		<td width="46%">OS
        			<a href="javascript:void(0)" tabindex="201" onclick="setPosterior_os();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
            		<a href="javascript:void(0)" tabindex="202" onclick="clearPosterior_os();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
        		</td>
        	</tr>
        	<tr>
        		<td class="label">Disc</td>
        		<td><input type="text" tabindex="203" style="width:93%" measurement="p_rdisc" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="204" style="width:93%" measurement="p_ldisc" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">C/D ratio</td>
        		<td><input type="text" tabindex="205" style="width:93%" measurement="p_rcd" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="206" style="width:93%" measurement="p_lcd" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">Macula</td>
        		<td><input type="text" tabindex="207" style="width:93%" measurement="p_rmac" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="208" style="width:93%" measurement="p_lmac" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">Retina</td>
        		<td><input type="text" tabindex="209" style="width:93%" measurement="p_rret" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="210" style="width:93%" measurement="p_lret" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">Vitreous</td>
        		<td><input type="text" tabindex="211" style="width:93%" measurement="p_rvit" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="212" style="width:93%" measurement="p_lvit" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">CRT</td>
        		<td><input type="text" tabindex="212" style="width:93%" measurement="p_rcrt" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="212" style="width:93%" measurement="p_lcrt" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        </table>
        </div>
     </div>
</td>
</tr>
<%}%>

<% if (!duction) {%>
<tr>
<td>
	<div class="slidey">
		 <div class="title">
            <a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" tabindex="213" href="javascript:void(0)" id="a_27" onclick="togglediv(this);">Duction/Diplopia:</a>
			
        </div>
        <div id="s_27" class="slideblock">
        <table class="exam" width="100%">
        	<tr>
			<td width="4%"></td>
        	<td colspan="2" width="25%">OD
        		<a href="javascript:void(0)" tabindex="214" onclick="setDuc_od();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
            	<a href="javascript:void(0)" tabindex="215" onclick="clearDuc_od();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
        	</td>
			<td width="4%"></td>
        	<td colspan="2" width="25%">OS
        		<a href="javascript:void(0)" tabindex="216" onclick="setDuc_os();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
            	<a href="javascript:void(0)" tabindex="217" onclick="clearDuc_os();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
        	</td>
			<td ></td>
        	<td colspan="2" width="25%">OU
        		<a href="javascript:void(0)" tabindex="218" onclick="setDuc_ou();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
            	<a href="javascript:void(0)" tabindex="219" onclick="clearDuc_ou();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
        	</td>
        	</tr>
        	<tr>
				<td></td>
        		<td><input type="text" style="width:90%" maxlength="10" tabindex="220" measurement="duc_rur" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" style="width:90%" maxlength="10" tabindex="221" measurement="duc_rul" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td width="4%"></td>
        		<td><input type="text" style="width:90%" maxlength="10" tabindex="222" measurement="duc_lur" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" style="width:90%" maxlength="10" tabindex="223" measurement="duc_lul" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
				<td></td>
        		<td><input type="text" style="width:90%" maxlength="10" tabindex="224" measurement="dip_ur" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" style="width:90%" maxlength="10" tabindex="225" measurement="dip_u" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>       	
        	</tr>
        	<tr>
				<td></td>
        		<td><input type="text" style="width:90%" tabindex="226" maxlength="10" measurement="duc_rr" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" style="width:90%" tabindex="227" maxlength="10" measurement="duc_rl" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td width="4%"></td>
        		<td><input type="text" style="width:90%" tabindex="228" maxlength="10" measurement="duc_lr" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" style="width:90%" tabindex="229" maxlength="10" measurement="duc_ll" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td></td>
        		<td><input type="text" style="width:90%" tabindex="230" maxlength="10" measurement="dip_r" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" style="width:90%" tabindex="231" maxlength="10" measurement="dip_p" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>       	
        	</tr>
        	<tr>
				<td></td>
        		<td><input type="text" style="width:90%" tabindex="232" maxlength="10" measurement="duc_rdr" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" style="width:90%" tabindex="233" maxlength="10" measurement="duc_rdl" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td width="4%"></td>
        		<td><input type="text" style="width:90%" tabindex="234" maxlength="10" measurement="duc_ldr" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" style="width:90%" tabindex="235" maxlength="10" measurement="duc_ldl" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td></td>
        		<td><input type="text" style="width:90%" tabindex="236" maxlength="10" measurement="dip_dr" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" style="width:90%" tabindex="237" maxlength="10" measurement="dip_d" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>       	
        	</tr>
        </table>
        </div>
	</div>
</td>
</tr>
<%}%>

<% if (!deviation_measurement) {%>
<tr>
<td>

	<div class="slidey">
		 <div class="title">
            <a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" tabindex="238" href="javascript:void(0)" id="a_28" onclick="togglediv(this);">Deviation Measurement:</a>
			
        </div>
        <div id="s_28" class="slideblock">
        <table class="exam" style="width: 100%">
			<tr>
			<td align="center" width="11%">
				<a href="javascript:void(0)" tabindex="239" onclick="setDeviation();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
            	<a href="javascript:void(0)" tabindex="240" onclick="clearDeviation();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
			</td>
			<td width="30%">
				<table style="width: 100%">
					<tr>
						<td width="30%"></td>
						<td width="30%"><input type="text" tabindex="241" maxlength="30" measurement="dev_u" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
						<td width="30%"></td>
					</tr>
					<tr>
						<td width="30%"><input type="text" tabindex="242" maxlength="30" measurement="dev_r" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
						<td width="30%"><input type="text" tabindex="243" maxlength="30" measurement="dev_p" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
						<td width="30%"><input type="text" tabindex="244" maxlength="30" measurement="dev_l" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
					</tr>
					<tr>
						<td width="30%"><input type="text" tabindex="245" maxlength="30" measurement="dev_rt" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
						<td width="30%"><input type="text" tabindex="246" maxlength="30" measurement="dev_d" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
						<td width="30%"><input type="text" tabindex="247" maxlength="30" measurement="dev_lt" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
					</tr>
				</table>
			</td>
			<td width="20%">
				<table>
					<tr>
						<td class="label">Near</td>
						<td><input type="text" tabindex="248" maxlength="30" measurement="dev_near" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
					</tr>
					<tr>
						<td class="label">+3.00D</td>
						<td><input type="text" tabindex="249" maxlength="30" measurement="dev_plus3" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
					</tr>
					<tr>
						<td class="label">Far Distance</td>
						<td><input type="text" tabindex="250" maxlength="30" measurement="dev_far" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
					</tr>
				</table>
			</td>
			<td width="25%">
				<table>
					<tr>
						<td class="label">NPC</td>
						<td><input type="text" tabindex="248" maxlength="30" measurement="dev_npc" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 140px"/></td>
					</tr>
					<tr>
						<td class="label">A.O.ACC</td>
						<td>
							<label>OD:</label>
							<input type="text"  tabindex="248" maxlength="30" measurement="dev_aoacc_od" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 40px"/>
							<label>OS:</label>
							<input type="text" tabindex="248" maxlength="30" measurement="dev_aoacc_os" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 40px"/>
						</td>
					</tr>
				</table>
			</td>
			</tr>
			
        </table>
        </div>
    </div>

</td>
</tr>
<%}%>

<% if (!external_orbit) {%>
<tr>
<td>

	<div class="slidey">
		 <div class="title">
            <a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" tabindex="251" href="javascript:void(0)" id="a_29" onclick="togglediv(this);">External/Orbit:</a>
			
        </div>
        <div id="s_29" class="slideblock">
        <table class="exam" width="100%">
        	<tr>
        		<td width="8%"></td>
        		<td width="46%">OD
        			<a href="javascript:void(0)" tabindex="252" onclick="setExternal_od();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
            		<a href="javascript:void(0)" tabindex="253" onclick="clearExternal_od();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
        		</td>
        		<td width="46%">OS
        			<a href="javascript:void(0)" tabindex="254" onclick="setExternal_os();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
            		<a href="javascript:void(0)" tabindex="255" onclick="clearExternal_os();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
        		</td>
        	</tr>
        	<tr>
        		<td class="label">Face</td>
        		<td><input type="text" tabindex="256" style="width:93%" measurement="ext_rface" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="257" style="width:93%" measurement="ext_lface" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">Retropulsion</td>
        		<td><input type="text" tabindex="258" style="width:93%" measurement="ext_rretro" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="259" style="width:93%" measurement="ext_lretro" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">Hertel</td>
        		<td><input type="text" tabindex="260" style="width:93%" measurement="ext_rhertel" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="261" style="width:93%" measurement="ext_lhertel" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        </table>
        </div>
    </div>

</td>
</tr>
<%}%>

<% if (!eyelid_nld) {%>
<tr>
<td>

	<div class="slidey">
		 <div class="title">
            <a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" tabindex="262" href="javascript:void(0)" id="a_30" onclick="togglediv(this);">Eyelid/NLD:</a>
			
        </div>
        <div id="s_30" class="slideblock">
        <table class="exam" width="100%">
        	<tr>
        		<td width="8%"></td>
        		<td width="46%">OD
        			<a href="javascript:void(0)" tabindex="263" onclick="setEyelid_od();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
            		<a href="javascript:void(0)" tabindex="264" onclick="clearEyelid_od();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
        		</td>
        		<td width="46%">OS
        			<a href="javascript:void(0)" tabindex="265" onclick="setEyelid_os();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
            		<a href="javascript:void(0)" tabindex="266" onclick="clearEyelid_os();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
        		</td>
        	</tr>
        	<tr>
        		<td class="label">Upper lid</td>
        		<td><input type="text" tabindex="267" style="width:93%" measurement="ext_rul" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="268" style="width:93%" measurement="ext_lul" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">Lower lid</td>
        		<td><input type="text" tabindex="269" style="width:93%" measurement="ext_rll" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="270" style="width:93%" measurement="ext_lll" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">Lac lake</td>
        		<td><input type="text" tabindex="271" style="width:93%" measurement="ext_rlake" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="272" style="width:93%" measurement="ext_llake" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">Lac Irrig</td>
        		<td><input type="text" tabindex="273" style="width:93%" measurement="ext_rirrig" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="274" style="width:93%" measurement="ext_lirrig" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">Punctum</td>
        		<td><input type="text" tabindex="275" style="width:93%" measurement="ext_rpunc" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="276" style="width:93%" measurement="ext_lpunc" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">NLD</td>
        		<td><input type="text" tabindex="277" style="width:93%" measurement="ext_rnld" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="278" style="width:93%" measurement="ext_lnld" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        	<tr>
        		<td class="label">Dye Disapp</td>
        		<td><input type="text" tabindex="279" style="width:93%" measurement="ext_rdye" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        		<td><input type="text" tabindex="280" style="width:93%" measurement="ext_ldye" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
        	</tr>
        </table>
        </div>
	</div>

</td>
</tr>
<%}%>

<% if (!eyelid_measurement) {%>
<tr>
<td>

	<div class="slidey">
		 <div class="title">
            <a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" tabindex="281" href="javascript:void(0)" id="a_31" onclick="togglediv(this);">Eyelid Measurement:</a>
			
        </div>
        <div id="s_31" class="slideblock">
        <table class="exam" width="100%">
     		<tr>
     			<td width="8%"></td>
     			<td colspan="2" align="center" width="46%">OD</td>
     			<td colspan="2" align="center" width="46%">OS</td>
     		</tr>
     		<tr>
     			<td></td>
     			<td>MRD</td>
     			<td>ISS</td>
     			<td>MRD</td>
     			<td>ISS</td>
     		</tr>
     		<tr>
     			<td class="label">Lid margins</td>
     			<td colspan="2"><input type="text" tabindex="282" maxlength="50" style="width:45%" measurement="lid_rmrd" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>&nbsp&nbsp
								<input type="text" tabindex="283" maxlength="50" style="width:45%" measurement="lid_riss" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
				</td>
     			<td colspan="2"><input type="text" tabindex="284" maxlength="50" style="width:45%" measurement="lid_lmrd" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>&nbsp&nbsp
								<input type="text" tabindex="285" maxlength="50" style="width:45%" measurement="lid_liss" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
				</td>
     		</tr>
     		<tr>
     			<td class="label">Levator Fn</td>
     			<td colspan="2" align="center"><input type="text" style="width:93%" tabindex="286" maxlength="50" measurement="lid_rlev" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
     			<td colspan="2" align="center"><input type="text" style="width:93%" tabindex="287" maxlength="50" measurement="lid_llev" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
     		</tr>
     		<tr>
     			<td class="label">Lag</td>
     			<td colspan="2" align="center"><input type="text" style="width:93%" tabindex="288" maxlength="50" measurement="lid_rlag" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
     			<td colspan="2" align="center"><input type="text" style="width:93%" tabindex="289" maxlength="50" measurement="lid_llag" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
     		</tr>
     		<tr>
     			<td class="label">Blink</td>
     			<td colspan="2" align="center"><input type="text" style="width:93%" tabindex="290" maxlength="50" measurement="lid_rblink" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
     			<td colspan="2" align="center"><input type="text" style="width:93%" tabindex="291" maxlength="50" measurement="lid_lblink" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
     		</tr>
     		<tr>
     			<td class="label">CN VII</td>
     			<td colspan="2" align="center"><input type="text" style="width:93%" tabindex="292" maxlength="50" measurement="lid_rcn7" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
     			<td colspan="2" align="center"><input type="text" style="width:93%" tabindex="293" maxlength="50" measurement="lid_lcn7" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
     		</tr>
     		<tr>
     			<td class="label">Bells</td>
     			<td colspan="2" align="center"><input type="text" style="width:93%" tabindex="294" maxlength="50" measurement="lid_rbell" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
     			<td colspan="2" align="center"><input type="text" style="width:93%" tabindex="295" maxlength="50" measurement="lid_lbell" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
     		</tr>
     		<tr>
     			<td class="label">Schirmer</td>
     			<td colspan="2" align="center"><input type="text" style="width:93%" tabindex="296" maxlength="50" measurement="lid_rschirm" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
     			<td colspan="2" align="center"><input type="text" style="width:93%" tabindex="297" maxlength="50" measurement="lid_lschirm" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
     		</tr>		
     	</table>
     	</div>
   	</div>

</td>
</tr>
<%}%>
<%if(null != props1.getProperty("eyeform_optometry_device") && props1.getProperty("eyeform_optometry_device").equals("yes")){%>
<tr>
	<td>
		<div class="slidey">
			 <div class="title">
	            <a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" tabindex="262" href="javascript:void(0)" id="a_34" onclick="togglediv(this);">OCT:</a>
	        </div>
	        <div id="s_34" class="slideblock">
		        <table class="exam" width="100%">
		        	<tr>
		     			<td width="8%"></td>
		     			<td colspan="2" align="center" width="46%">OD</td>
		     			<td colspan="2" align="center" width="46%">OS</td>
		     		</tr>
	     			<tr>
		     			<td class="label">S</td>
		     			<td colspan="2">
		     				<input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="oct_s_l" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
						</td>
		     			<td colspan="2">
		     				<input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="oct_s_r" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
						</td>
		     		</tr>
		     		<tr>
		     			<td class="label">I</td>
		     			<td colspan="2">
		     				<input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="oct_i_l" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
						</td>
		     			<td colspan="2">
		     				<input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="oct_i_r" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
						</td>
		     		</tr>
		     		<tr>
		     			<td width="8%"></td>
		     			<td colspan="2">
		     				<table>
		     					<tr>
		     						<td width="15%"></td>
		     						<td width="15%"><input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="oct_tsnit_l_1" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     						<td width="15%"></td>
		     					</tr>
		     				</table>
						</td>
		     			<td colspan="2">
		     				<table>
		     					<tr>
		     						<td width="15%"></td>
		     						<td width="15%"><input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="oct_tsnit_r_1" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     						<td width="15%"></td>
		     					</tr>
		     				</table>
						</td>
		     		</tr>
		     		<tr>
		     			<td class="label">TSNIT</td>
		     			<td colspan="2">
		     				<table>
		     					<tr>
		     						<td width="10%"></td>
		     						<td width="13%"><input type="text" tabindex="301" maxlength="50" style="width:80%" measurement="oct_tsnit_l_2" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     						<td width="13%"><input type="text" tabindex="301" maxlength="50" style="width:80%" measurement="oct_tsnit_l_3" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     						<td width="10%"></td>
		     					</tr>
		     				</table>
		     			</td>
		     			<td colspan="2">
		     				<table>
		     					<tr>
		     						<td width="10%"></td>
		     						<td width="13%"><input type="text" tabindex="301" maxlength="50" style="width:80%" measurement="oct_tsnit_r_2" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     						<td width="13%"><input type="text" tabindex="301" maxlength="50" style="width:80%" measurement="oct_tsnit_r_3" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     						<td width="10%"></td>
		     					</tr>
		     				</table>
		     			</td>
		     		</tr>
		     		<tr>
		     			<td></td>
		     			<td colspan="2">
		     				<table>
		     					<tr>
		     						<td width="15%"></td>
		     						<td width="15%"><input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="oct_tsnit_l_4" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     						<td width="15%"></td>
		     					</tr>
		     				</table>
						</td>
		     			<td colspan="2">
		     				<table>
		     					<tr>
		     						<td width="15%"></td>
		     						<td width="15%"><input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="oct_tsnit_r_4" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     						<td width="15%"></td>
		     					</tr>
		     				</table>
						</td>
		     		</tr>
		     		<tr>
		     			<td></td>
		     			<td colspan="2">
		     				<input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="oct_l" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
						</td>
		     			<td colspan="2">
		     				<input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="oct_r" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
						</td>
		     		</tr>
		     		<tr>
		     			<td class="label">Macula</td>
		     			<td colspan="2">
		     				<input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="oct_macula_l" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
						</td>
		     			<td colspan="2">
		     				<input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="oct_macula_r" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
						</td>
		     		</tr>
		        </table>
	        </div>
        </div>
	</td>
</tr>
<tr>
	<td>
		<div class="slidey">
			 <div class="title">
	            <a style="font-weight: bold;color:black;text-decoration:none;font-size:12px" tabindex="300" href="javascript:void(0)" id="a_35" onclick="togglediv(this);">VF:</a>
	        </div>
	        <div id="s_35" class="slideblock">
		        <table class="exam" width="100%">
		        	<tr>
		     			<td align="center" width="46%">OD</td>
		     			<td align="center" width="46%">OS</td>
		     		</tr>
		     		<tr>
		     			<td><input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="vf_l" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     			<td><input type="text" tabindex="301" maxlength="50" style="width:90%" measurement="vf_r" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     		</tr>
		        </table>
	        </div>
        </div>
	</td>
</tr>
<%} %>
</table>
<span style="font-size:10px">
	<a id="save_measurements" href="javascript:void(0)"  onclick="hxForm_sumbit();">[Save Measurements]</a>
</span>
<span style="float:right;font-size:10px">
	<a href="javascript:void(0);" onclick="expandAll();">[expand all sections]</a>&nbsp;
	<a href="javascript:void(0);" onclick="collapseAll();">[collapse all sections]</a>
</span>
<script type="text/javascript">
for(var x=21;x<36;x++) {
	var name = 's_'+x;
	var el = document.getElementById(name);
	if(el) {
		el.style.display='none';
		effectMap[name] = 'up';
	}
}
changeclass();
</script>