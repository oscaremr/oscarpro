<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ include file="/taglibs.jsp"%>
<%@page import="java.util.List" %>
<%@page import="org.oscarehr.eyeform.web.MacroAction" %>
<%@page import="org.apache.struts.util.LabelValueBean" %>
<%@page import="oscar.oscarBilling.ca.on.data.JdbcBillingPageUtil" %>
<%@page import="org.oscarehr.eyeform.dao.*" %>
<%@page import="org.oscarehr.eyeform.model.*" %>
<%@page import="org.oscarehr.util.SpringUtils" %>

<%
	List<LabelValueBean> sliCodes = MacroAction.sliCodeList;
	request.setAttribute("sliCodes", sliCodes);
    		
    String macroBillingTo = (String)request.getAttribute("macroBillingTo");
    
    if(macroBillingTo == null){
    	macroBillingTo = "";
    }
    String macroSiteId = (String)request.getAttribute("macroSiteId");
    String macroProviderNo = (String)request.getAttribute("macroProviderNo");
    if(macroSiteId == null){
    	macroSiteId = "";
    }
    Integer closeEncounter = (Integer)request.getAttribute("closeEncounter");
    if(closeEncounter == null){
    	closeEncounter = 1;
    }
    boolean bMultisites=org.oscarehr.common.IsPropertiesOn.isMultisitesEnable();
%>

<html lang="en">
<head>
    <title>Eyeform Macro Details</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>


<link rel="stylesheet" href="css/displaytag.css" type="text/css">
<style type="text/css">
.boldRow {
	color:red;
}
.commonRow{
	color:black;
}
span.h5 {
  margin-top: 1px;
  border-bottom: 1px solid #000;
  width: 90%;
  font-weight: bold;
  list-style-type: none;
  padding: 2px 2px 2px 2px;
  color: black;
  background-color: #69c;
  font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;
  font-size: 10pt;
  text-decoration: none;
  display: block;
  clear: both;
  white-space: nowrap;

}
</style>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
function loadDefaultSiteAndProvider(){
	var macroSite = $("#macroSiteId").val();
	var macroBillingTo = $("#macroBillingTo").val();
	var macroProviderNo = $("#macroProvider_No").val();
	if(null != macroSite && macroSite.length > 0){
		$("#site").val(macroSite);
		
		getProvider();
	}
	
	if(null != macroBillingTo && macroBillingTo.length > 0){
		$("select[name='macro.billingBillto']").val(macroBillingTo);
	}
	
	$("#macroProvider_No").val(macroProviderNo);
	getAllProviders();
}
function getProvider(){
	var siteId=$('#site option:selected').val();
	$.ajax({
		url : "../eyeform/Macro.do?method=getProviderBySiteId&siteId="+siteId,
		async: false,
		success : function(data) {
			var json=eval(data);
			var html = "";
			for (var i in json) {
				html += '<option value="'+ json[i].providerNo+'">'+ json[i].formattedName+'</option>';
            }  
			$("select[name='macro.billingBillto']").html(html);
		}
	});
}

function getSites(){
	$.ajax({
		url : "../admin/ManageSites.do?method=view2",
		async: false,
		success : function(data) {
			var json=eval(data);
			for (var i in json) {  
                 $('#site').append('<option value="'+ json[i].siteId+'">'+ json[i].name+'</option>');
            }

			getProvider();
		}
	});
}

function selectProviders(){
	
}

function getAllProviders(){
	$.ajax({
		url : "../eyeform/Macro.do?method=getAllProviders",
		async: false,
		success : function(data) {
			var json=eval(data);
			var html = "";
			html = '<option selected value=""></option>'+html;
			
			for (var i in json) {
				if($("#macroProvider_No").val()==json[i].providerNo){
					html += '<option selected value="'+ json[i].providerNo+'">'+ json[i].fullName+'</option>';
				} else {
					html += '<option value="'+ json[i].providerNo+'">'+ json[i].fullName+'</option>';
				} 
            }
			if($("#macroProvider_No").val()==null || $("#macroProvider_No").val().length==0){
				html = '<option selected value=""></option>'+html;
			}
			$("#macroProviderNo").html(html);
		}
	});
}

function selectAllCheckBox(){
	var form=document.inputForm;
	for (i=0;i<form.elements.length;i++){
		if (form.elements[i].name.indexOf("cbox")==0){
			form.elements[i].checked=true;
			form.elements[i].value="1";
		}
	}
}
function clearAllCheckBox(){
	var form=document.inputForm;
	for (i=0;i<form.elements.length;i++){
		if (form.elements[i].name.indexOf("cbox")==0){
			form.elements[i].checked=false;
			form.elements[i].value="0";
		}
	}
}
function turnCheckBox(el){
	if (el.checked==true) {
		el.value="1";
	}else {
		el.value="0";
	}
}

function fixCheckboxes(form) {
	var cbs = form.getElementsByTagName("input");
	form.followupFlags.value = '';
	for (var i=0; i<cbs.length; i++) {
		if (cbs[i].type=="checkbox" && cbs[i].name.indexOf("Flag")>0 && cbs[i].checked) {
			form.followupFlags.value += cbs[i].name + "|";
		}
	}
}
$(function(){
	$("#closeEncounter").click( function(){
		if($(this).is(":checked")){
			$("#saveCoseEncounter").val(1);
		}else{
			$("#saveCoseEncounter").val(0);
		}
	})
})
</script>

</head>
<%if(bMultisites){ %>
<body onload="getSites();loadDefaultSiteAndProvider()">
<%}else{ %>
<body onload="getAllProviders();">
<%} %>

<html:form action="/eyeform/Macro.do" onsubmit="fixCheckboxes(this); return true;">
	<input name="method" value="save" type="hidden">
	<input name="macro.closeEncounter" id="saveCoseEncounter" value="<%=closeEncounter %>" type="hidden">
	<table style="border: 0px none;">
	<html:hidden property="macro.id"/>
	<input name="macroSiteId" id="macroSiteId" value="<%=macroSiteId %>" type="hidden">
	<input name="macroBillingTo" id="macroBillingTo" value="<%=macroBillingTo %>" type="hidden">
	<input type="hidden" id="macroProvider_No"  name="macroProvider_No" value="<%=macroProviderNo %>"/>
	<tbody><tr>
	<td>
	<input value="Save" type="submit">
	<input value="Cancel" onclick="this.form.method.value='list'" type="submit">
	<input value="Close" onclick="window.opener.location.reload();window.close();return false;" type="button">
	</td></tr>
	</tbody></table>

	<c:if test="${not empty errors }">
		<span style="color:red;"><c:out value="${errors}" escapeXml="false"/></span>
	</c:if>

<fieldset>
	<legend>Macro Details</legend>
	<table style="border: 0px none;">

	<tbody>
	<tr>
	<td colspan="2">
	<input type="checkbox" id="closeEncounter"  <%=closeEncounter == 1?"checked":"" %>>Close the Encounter After Running Macro.
	</td> 
	</tr>

	<tr>
	<td>Macro Provider</td>
	<td><select name="macro.macroProviderNo" id="macroProviderNo"
	onchange="selectProviders()"></select></td>
	</tr>
	<tr>
	<td width="150">Label</td>
	<td><html:text property="macro.label"/></td>
	</tr>
	<tr>
	<td>Display Order</td>
	<td><html:text property="macro.displayOrder" /></td>
	</tr>

	</tbody></table>

	<fieldset>
	<legend>Impression &amp; Followup</legend>
	<table style="border: 0px none;">
	<tbody><tr>
	<td width="150">Impression Text<br> <font size="-1">(to be appended)</font></td>

	<td><html:textarea property="macro.impression" cols="40" rows="10"></html:textarea></td>
	</tr>
	<tr>
	<td>Followup in</td>
	<td nowrap="nowrap"><html:text property="macro.followupNo" style="width: 40px;"/>
             <html:select property="macro.followupUnit" style="width: 80px;">
             	<html:option value="days">days</html:option>
            	<html:option value="weeks">weeks</html:option>
	            <html:option value="months">months</html:option>
	         </html:select>
    &nbsp;
    with doctor
    <html:select property="macro.followupDoctorId">
    	<html:options collection="providers" property="providerNo" labelProperty="formattedName" />
	</html:select>
	&nbsp;Comment <html:textarea property="macro.followupComment" cols="40" rows="2"></html:textarea>
	</td>
	</tr>	
	
	<tr><td></td>

	<td nowrap="nowrap">

	<html:checkbox property="macro.statFlag" value="statFlag" /> STAT/PRN
	<html:checkbox property="macro.optFlag" value="optFlag" /> Optom Routine
	<html:checkbox property="macro.dischargeFlag" value="dischargeFlag"/> Discharge
	</td></tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>

	<tr>
	<td>Book Procedure</td>
	<td nowrap="nowrap"> 		
        <html:select property="macro.bookProcedureEye">
        	<html:option value="OU">OU</html:option>
        	<html:option value="OD">OD</html:option>
        	<html:option value="OS">OS</html:option>
	    	<html:option value="OD then OS">OD then OS</html:option>
	    	<html:option value="OS then OD">OS then OD</html:option>
	     </html:select>
	&nbsp; Proc: <html:text property="macro.bookProcedureProc" maxlength="30"></html:text>
	&nbsp; Loc: <html:text property="macro.bookProcedureLoc" maxlength="50"></html:text>
	&nbsp; <br>
		<html:select property="macro.bookProcedureUrgency" style="width: 80px;">
			<html:option value="routine">routine</html:option>
        	<html:option value="ASAP">ASAP</html:option>
        	<html:option value="URGENT">URGENT</html:option>
        </html:select>
    &nbsp; Comment: <html:textarea property="macro.bookProcedureComment" cols="40" rows="2"></html:textarea>
	</td>
	</tr>
	<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
	<tr>
	<td>Book Tests</td>

	<td><html:textarea property="macro.testRecords" cols="40" rows="4"></html:textarea></td>
	<td><font size="-1">Add a test booking per line with the following format:<br>
	&lt;test_name&gt;|&lt;OU,OD,OS&gt;|&lt;routine,ASAP,urgent&gt;|&lt;comment&gt;<br>
	e.g. OCT disc|OU|routine|book for Fridays</font></td>

	</tr>
	<tr><td>send tickler to </td><td>
			<html:select property="macro.ticklerRecipient">
				<html:option value="">Nobody</html:option>
				<html:options collection="ticklerProviders" property="providerNo" labelProperty="formattedName" />
			</html:select>
    </td></tr>
	</tbody></table>
	</fieldset>


	<fieldset>
	<legend>Billing</legend>
	<table style="border: 0px none;">

	<tbody>
	<%if(bMultisites){ %>
	<tr>
		<td>
		Site
		</td>
		<td>
			<select name="macro.siteId" id="site" onchange="getProvider()"></select>
		</td>
	</tr>
	<%} %>
		
		<td>Billing Physician
		</td>
		<td>
			<html:select property="macro.billingBillto">
				<html:option value=""> -- </html:option>
				<html:options collection="providers" property="providerNo" labelProperty="formattedName" />
			</html:select>
		</td>
	</tr>
	<tr>
		<td>
		SLI Code<br/>(Required for some billing codes)
		</td>
		<td>
			<html:select property="macro.sliCode">
				<html:options collection="sliCodes" property="value" labelProperty="label"/>
			</html:select>
		</td>
	</tr>
	<tr>
	<td>Billing Codes<br>(won't bill if empty)<br></td>
	<td><html:textarea property="macro.billingCodes" rows="4"></html:textarea></td>
	<td><font size="-1">Add a billing code per line in the following format:
	<br>&lt;unit_code&gt;|&lt;unit_count&gt;|&lt;sli_code&gt;
	<br>e.g. A001A|2|NA</font></td>

	</tr>
	<tr>
	<td>DX Code<br></td>
	<td><html:text property="macro.billingDxcode" /></td>
	</tr>
	<tr><td width="150">Visit Type</td>
	<td>
					<html:select property="macro.billingVisitType">

					    <html:option value="00| Clinic Visit">00 | Clinic Visit</html:option>
					    <html:option value="01| Outpatient Visit">01 | Outpatient Visit</html:option>
					    <html:option value="02| Hospital Visit">02 | Hospital Visit</html:option>
					    <html:option value="03| ER">03 | ER</html:option>
					    <html:option value="04| Nursing Home">04 | Nursing Home</html:option>
					    <html:option value="05| Home Visit">05 | Home Visit</html:option>

					</html:select>
	</td></tr>
	<tr><td>Visit Location</td>
	<td>
			<html:select property="macro.billingVisitLocation">
			
			<%  JdbcBillingPageUtil tdbObj = new JdbcBillingPageUtil();
				String billLocationNo="", billLocation="";
			 	List lLocation = tdbObj.getFacilty_num();
			    for (int i = 0; i < lLocation.size(); i = i + 2) {
			    	billLocation = (String) lLocation.get(i + 1);
					billLocationNo = (String) lLocation.get(i)+ "|" + billLocation;
					
					
			%>
					<html:option value="<%=billLocationNo%>" >	<%=billLocation%>
					</html:option>
			<%
				}
			%>
			
			
		<!-- 	
				<html:option value="0000|Not Applicable">Not Applicable</html:option>
				<html:option value="1972|Chedoke Hospital">Chedoke Hospital</html:option>
				<html:option value="1983|Henderson General">Henderson General</html:option>
				<html:option value="1985|Hamilton General">Hamilton General</html:option>
				<html:option value="1994|McMaster University Medical Center">McMaster University Medical Center</html:option>
				<html:option value="2003|St. Joseph&quot;s Hospital">St. Joseph"s Hospital</html:option>
				<html:option value="3226|Stonechurch Family Health PCN">Stonechurch Family Health PCN</html:option>
				<html:option value="3642|The Wellington Lodge">The Wellington Lodge</html:option>
				<html:option value="3831|Maternity Centre of Hamilton">Maternity Centre of Hamilton</html:option>
				<html:option value="3866|Stonechurch Family Health Center">Stonechurch Family Health Center</html:option>
				<html:option value="9999|Home Visit">Home Visit</html:option>
				-->
			</html:select>

	</td></tr>
	<tr>
	<td>Billing Type<br></td>
	<td>
					<html:select property="macro.billingBilltype">
						<html:option value="ODP | Bill OHIP">Bill OHIP</html:option>
					    <html:option value="WCB | Worker's Compensation Board">WSIB</html:option>
					    <!--
					    <option value="NOT | Do Not Bill" >Do Not Bill</option>
					    <option value="IFH | Interm Federal Health" >IFH</option>
					    <option value="PAT | Bill Patient" >3rd Party</option>
					    <option value="OCF | " > -OCF</option>
					    <option value="ODS | "> -ODSP</option>
					    <option value="CPP | Canada Pension Plan" > -CPP</option>
					    <option value="STD | Short Term Disability / Long Term Disability" >-STD/LTD</option>
					    -->

                        <html:option value="BON | Bonus Codes">Bonus Codes</html:option>
                      </html:select>
	</td>
	</tr>
	<tr>
	<td>Comment<br></td>
	<td><html:textarea property="macro.billingComment"></html:textarea></td>
	</tr>

	</tbody></table>
	</fieldset>
	
	<fieldset>
	<legend>Enter Ocular Procedure</legend>
	<table style="border: 0px none;">
		<tbody>
			<tr>
				<td>Eye</td>
				<td>
					<html:select property="macro.procedureEye">
					<html:option value=""></html:option>
					<html:option value="OD">OD</html:option>
					<html:option value="OS">OS</html:option>
					<html:option value="OU">OU</html:option>
				</html:select>
				</td>
			</tr>
			<tr>
				<td>Procedure</td>
				<td>
					<html:text property="macro.procedureName" size="35"/>
					<select id='procSelect' onchange='selProc(this);'>
						<option value=""></option>
						<%
						EyeformOcularProcedureNameListDao eopnDao = (EyeformOcularProcedureNameListDao)SpringUtils.getBean(EyeformOcularProcedureNameListDao.class);
						List<EyeformOcularProcedureNameList> eList = eopnDao.findAll();
						for (EyeformOcularProcedureNameList e:eList) {%>
							<option value="<%=e.getProcedureName() %>"><%=e.getProcedureName() %></option>
						<%} %>
					</select>
				</td>
			</tr>
			<tr>
				<td>Doctor Name</td>
				<td>
					<html:select property="macro.procedureDoctor">
						<html:option value=""></html:option>					
						<html:options collection="providers" property="providerNo" labelProperty="formattedName" />
					</html:select>
				</td>
			</tr>
			<tr>
				<td>Location</td>
				<td>
					<html:text property="macro.procedureLocation" size="35"/>		
				</td>
			</tr>
			<tr>
				<td>Procedure Notes</td>
				<td>
					<html:textarea rows="5" cols="40" property="macro.procedureNote"></html:textarea>
				</td>
			</tr>
		</tbody>
	</table>
	</fieldset>

</fieldset>
</html:form>
<script type="text/javascript">
function selProc(which) {
	document.getElementsByName("macro.procedureName")[0].value = which.value;
}
</script>
</body></html>
