<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ page import="java.util.*, oscar.util.*, oscar.OscarProperties, oscar.dms.*, oscar.dms.data.*, org.oscarehr.common.dao.CtlDocClassDao"%>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
      String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
      boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_con" rights="w" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_con");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}
%>


<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@page import="oscar.oscarRx.data.RxPatientData"%>
<%@ include file="/taglibs.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>

<%@page import="org.oscarehr.eyeform.model.*"%>
<%@page import="org.oscarehr.eyeform.web.EyeformAction"%>
<%@page import="java.util.List"%>
<%@page import="oscar.OscarProperties" %>
<%@page import="org.oscarehr.common.model.DemographicContact"%>
<%@page import="org.oscarehr.common.dao.DemographicExtDao" %>
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="org.oscarehr.common.dao.ProfessionalSpecialistDao" %>
<%@page import="org.oscarehr.common.model.ProfessionalSpecialist" %>
<%@page import="org.oscarehr.common.dao.ContactDao" %>
<%@page import="org.oscarehr.common.model.Contact" %>
<%@page import="org.oscarehr.util.MiscUtils" %>
<%@page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@page import="org.oscarehr.common.model.Provider" %>
<%@page import="org.oscarehr.eyeform.model.EyeformConsultationReport" %>
<%@ page import="org.owasp.encoder.Encode" %>

<html:html>
<head>
<html:base />
<title>generate consultation report</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>

<style type="text/css">
table td {
	width: 100% padding : 0px;
	cell-spacing: 0;
	margin: 0;
	border: 0;
	font-size: 10pt;
}

.full input {
	border: 1px solid gray;
}

select {
	border: 1px solid gray;
}
</style>
<%
	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
	String demographicNo = (String) request.getAttribute("demographicNo");
	org.oscarehr.common.model.Allergy[] allergies = RxPatientData.getPatient(loggedInInfo, Integer.parseInt(demographicNo)).getAllergies(loggedInInfo);
		String aller = "";
		for (int j = 0; j < allergies.length; j++) {
			aller += allergies[j].getShortDesc(13, 8,
					"...")
					+ "; ";
		}
		String presc = "";

		oscar.oscarRx.data.RxPrescriptionData prescriptData = new oscar.oscarRx.data.RxPrescriptionData();
		oscar.oscarRx.data.RxPrescriptionData.Prescription[] arr = {};
		arr = prescriptData.getUniquePrescriptionsByPatient(Integer
				.parseInt(demographicNo));
		if (arr.length > 0) {
			for (int i = 0; i < arr.length; i++) { 
				String rxD = arr[i].getRxDate().toString();

				String rxP = arr[i].getFullOutLine().replaceAll(";",
						" ");
				rxP = rxP + "   " + arr[i].getEndDate();
				
				//If the end date is today, it should be considered as active meds and should show up in con report.  
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(arr[i].getEndDate());
				calendar.add(Calendar.DAY_OF_MONTH, 1);
				Date tomorrows = calendar.getTime();
				
				if (tomorrows.after(new java.util.Date()))
					presc += rxD + "  " + rxP + "\n";
			} 
		}
%>

<%
	request.setAttribute("sections",EyeformAction.getMeasurementSections());
	request.setAttribute("headers",EyeformAction.getMeasurementHeaders());
	request.setAttribute("providers",EyeformAction.getActiveProviders());
	
	oscar.OscarProperties props1 = oscar.OscarProperties.getInstance();
	String exam_val = (String)request.getAttribute("old_examination");
	if(exam_val == null){
		exam_val = "";
	}
	boolean faxEnabled = props1.getBooleanProperty("faxEnable", "yes");
	
	boolean isShowUpload = props1.getBooleanProperty("CON_REPORT_PDF_UPLOADED_TO_DOCUMENTS", "yes");
	
	EyeformConsultationReport cp = (EyeformConsultationReport)request.getAttribute("cp");
	
	String clDoctor = "", otherDocFax = "";
	if (cp != null && cp.getOtherReferralId() != 0) {
		ProfessionalSpecialistDao professionalSpecialistDao = (ProfessionalSpecialistDao) SpringUtils.getBean("professionalSpecialistDao");
		ProfessionalSpecialist professionalSpecialist = null;
		professionalSpecialist = professionalSpecialistDao.getById(cp.getOtherReferralId());
		if(professionalSpecialist != null){
			clDoctor = professionalSpecialist.getFormattedName();
			otherDocFax = professionalSpecialist.getFaxNumber();
		}
	}
%>
<%
String user_no = (String) session.getAttribute("user");
String appointment = request.getParameter("appointmentNo");

String module = "";
String moduleid = "";
String siteId = "";
if (request.getParameter("function") != null) {
    module = request.getParameter("function");
    moduleid = request.getParameter("functionid");
    siteId = request.getParameter("siteId");
} else if (request.getAttribute("function") != null) {
    module = (String) request.getAttribute("function");
    moduleid = (String) request.getAttribute("functionid");
    siteId = (String)request.getAttribute("siteId");
}

String curUser = "";
if (request.getParameter("curUser") != null) {
    curUser = request.getParameter("curUser");
} else if (request.getAttribute("curUser") != null) {
    curUser = (String) request.getAttribute("curUser");
}

OscarProperties props = OscarProperties.getInstance();

AddEditDocumentForm formdata = new AddEditDocumentForm();
formdata.setAppointmentNo(appointment);
String defaultType = (String) props.getProperty("eDocAddTypeDefault", "");
String defaultDesc = "Enter Title"; //if defaultType isn't defined, this value is used for the title/description
String defaultHtml = "Enter Link URL";

if(request.getParameter("defaultDocType") != null) {
	defaultType = request.getParameter("defaultDocType");
}

//for "add document" link from the patient master page - the "mode" variable allows the add div to open up
String mode = "";
if (request.getAttribute("mode") != null) {
    mode = (String) request.getAttribute("mode");
} else if (request.getParameter("mode") != null) {
    mode = request.getParameter("mode");
}

//Retrieve encounter id for updating encounter navbar if info this page changes anything
String parentAjaxId;
if( request.getParameter("parentAjaxId") != null )
    parentAjaxId = request.getParameter("parentAjaxId");
else if( request.getAttribute("parentAjaxId") != null )
    parentAjaxId = (String)request.getAttribute("parentAjaxId");
else
    parentAjaxId = "";

if (request.getAttribute("completedForm") != null) {
formdata = (AddEditDocumentForm) request.getAttribute("completedForm");
} else {
    formdata.setFunction(module);  //"module" and "function" are the same
    formdata.setFunctionId(moduleid);
    formdata.setDocType(defaultType);
    formdata.setDocDesc(defaultType.equals("")?defaultDesc:defaultType);
    formdata.setDocCreator(user_no);
    formdata.setObservationDate(UtilDateUtilities.DateToString(new Date(), "yyyy/MM/dd"));
    formdata.setHtml(defaultHtml);
    formdata.setAppointmentNo(appointment);
}
ArrayList doctypes = EDocUtil.getActiveDocTypes(formdata.getFunction());

CtlDocClassDao docClassDao = (CtlDocClassDao)SpringUtils.getBean("ctlDocClassDao");
List<String> reportClasses = docClassDao.findUniqueReportClasses();
ArrayList<String> subClasses = new ArrayList<String>();
ArrayList<String> consultA = new ArrayList<String>();
ArrayList<String> consultB = new ArrayList<String>();
for (String reportClass : reportClasses) {
    List<String> subClassList = docClassDao.findSubClassesByReportClass(reportClass);
    if (reportClass.equals("Consultant ReportA")) consultA.addAll(subClassList);
    else if (reportClass.equals("Consultant ReportB")) consultB.addAll(subClassList);
    else subClasses.addAll(subClassList);

    if (!consultA.isEmpty() && !consultB.isEmpty()) {
        for (String partA : consultA) {
            for (String partB : consultB) {
                subClasses.add(partA+" "+partB);
            }
        }
    }
}

//add special list to fax
ProfessionalSpecialistDao professionalSpecialistDao = (ProfessionalSpecialistDao) SpringUtils.getBean("professionalSpecialistDao");
DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
String demographic_no = request.getParameter("demographicNo");
Map<String,String> demoExt = demographicExtDao.getAllValuesForDemo(Integer.parseInt(demographic_no));
String[] propDemoExt = {};
boolean hasDemoExt=false;
String demographicExt = props.getProperty("demographicExt");

if (demographicExt!=null && !demographicExt.trim().isEmpty()) {
	hasDemoExt = true;
	propDemoExt = demographicExt.split("\\|");
}		
%>
<style type="text/css">
/* Used for "import from enctounter" button */
input.btn {
	color: black;
	font-family: 'trebuchet ms', helvetica, sans-serif;
	font-size: 84%;
	font-weight: bold;
	background-color: #B8B8FF;
	border: 1px solid;
	border-top-color: #696;
	border-left-color: #696;
	border-right-color: #363;
	border-bottom-color: #363;
}

td.tite {
	background-color: #bbbbFF;
	color: black;
	font-size: 12pt;
}

td.tite1 {
	background-color: #ccccFF;
	color: black;
	font-size: 12pt;
}

th,td.tite2 {
	background-color: #BFBFFF;
	color: black;
	font-size: 12pt;
}

td.tite3 {
	background-color: #B8B8FF;
	color: black;
	font-size: 12pt;
}

td.tite4 {
	background-color: #ddddff;
	color: black;
	font-size: 12pt;
}

td.stat {
	font-size: 10pt;
}

input.righty {
	text-align: right;
}
</style>

</head>

<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request" />

<script type="text/javascript" src="<c:out value="${ctx}"/>/js/jquery.js"></script>
   <script>
     jQuery.noConflict();
   </script>
   
<script type="text/javascript" language=javascript>
var hasRwoArM = false;
var isIntegrateOptometry = false;
<%if(null != props1.getProperty("eyeform_vision_measurement_has_two_AR_M") && props1.getProperty("eyeform_vision_measurement_has_two_AR_M").equals("yes")){%>
hasRwoArM = true;
<%}
if(null != props1.getProperty("eyeform_optometry_device") && props1.getProperty("eyeform_optometry_device").equals("yes")){
%>
	isIntegrateOptometry = true;
<%}%>

function confirmCompleted(btn) {
	var str="This report is tracked by online referral system and can be automatically completed if you select the 'completed, and not sent' option.\nAre you sure you want to mark it as 'complete and sent', which means you have to do everything manually?";
	return confirm(str);
}

function confirmPrint(btn) {
	var str="This referral is created and monitored automatically by CRM. Are you sure you want to print & fax it manually?";
	return confirm(str);
}


    con_cHis='<%=request.getAttribute("currentHistory")%>';
	con_oHis='<%=request.getAttribute("otherMeds")%>';
	con_pHis='<%=request.getAttribute("pastOcularHistory")%>';
	con_sHis='<%=request.getAttribute("specs")%>';
	con_diag='<%=request.getAttribute("diagnosticNotes")%>';
	con_impress='<%=request.getAttribute("impression")%>';
	con_mHis='<%=request.getAttribute("medHistory")%>';
	con_fHis='<%=request.getAttribute("famHistory")%>';
	con_oMeds='<%=request.getAttribute("ocularMedication")%>';
	con_probook='<%=request.getAttribute("probooking")%>';
	con_testbook='<%=request.getAttribute("testbooking")%>';
	con_ocularpro='<%=request.getAttribute("ocularProc")%>';
	con_follow='<%=request.getAttribute("followup")%>';
	con_aller='<%=StringEscapeUtils.escapeJavaScript(aller) %>';
	con_presc='<%=StringEscapeUtils.escapeJavaScript(presc) %>';

<%
	String customCppIssues[] = oscar.OscarProperties.getInstance().getProperty("encounter.custom_cpp_issues", "").split(",");
	for(String customCppIssue:customCppIssues) {
		%>
		con_<%=customCppIssue%>='<%=request.getAttribute(customCppIssue)%>';
		<%
	}
	
	String whichEyeForm =  oscar.OscarProperties.getInstance().getProperty("cme_js","");
%>



  function trim(s) {
  	if (s==null || s=='') return s;
  	while (s.substring(0,1) == ' ') {
    	s = s.substring(1,s.length);
  	}
  	while (s.substring(s.length-1,s.length) == ' ') {
    	s = s.substring(0,s.length-1);
  	}
  return s;
}
    function getOSCARData(){
    	addAllFax();
    	addToFax();
    	addFamFax();
    	
      	var whichEyeForm = "<%=whichEyeForm%>";
	if(whichEyeForm !=null && whichEyeForm=="eyeform_DrJinapriya") { 
		clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.CurrentHistory"/>:',con_cHis);
		clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.PastOcularHistory"/>:',con_pHis);
		clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.MedicalHistory"/>:',con_mHis);
		clinicalInfoAdd('Family history:',con_fHis);
		clinicalInfoAdd('Specs history:',con_sHis);

		clinicalInfoAdd('Ocular meds:',con_oMeds);
		clinicalInfoAdd('Other meds:',con_oHis);
		clinicalInfoAdd('Diagnostics notes:',con_diag);
		ocluarproAdd('Ocular procedure:',con_ocularpro);
		
        } else if(whichEyeForm !=null && whichEyeForm=="eyeform_simple") {
		
		clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.CurrentHistory"/>:',con_cHis);
		clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.PastOcularHistory"/>:',con_pHis);
		clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.MedicalHistory"/>:',con_mHis);
		clinicalInfoAdd('Diagnostics notes:',con_diag);	
 	} else { 
		clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.CurrentHistory"/>:',con_cHis)
		clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.PastOcularHistory"/>:',con_pHis);
		clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.MedicalHistory"/>:',con_mHis);
		clinicalInfoAdd('Family history:',con_fHis);
		clinicalInfoAdd('Specs history:',con_sHis);

		clinicalInfoAdd('Ocular meds:',con_oMeds);
		clinicalInfoAdd('Other meds:',con_oHis);
		clinicalInfoAdd('Diagnostics notes:',con_diag);
		ocluarproAdd('Ocular procedure:',con_ocularpro)
 	} 
		allergiesAdd();
		prescriptionsAdd();
		impressionAdd();
		
		for(var i = 0;i < jQuery("select[name='fromlist1']")[0].options.length;i ++){
			jQuery("select[name='fromlist1']")[0].options[i].selected = true;
		}
		for(var i = 0;i < jQuery("#fromlist2")[0].options.length;i ++){
			jQuery("#fromlist2")[0].options[i].selected = true;
		}
		addSection1(document.eyeForm.elements['fromlist1'],document.eyeForm.elements['fromlist2'], hasRwoArM, isIntegrateOptometry);
		addExam(ctx,'fromlist2',document.getElementById('cp_examination'),appointmentNo);
		
		<%
		for(String customCppIssue:customCppIssues) {
			if ("Misc".equals(customCppIssue) && SystemPreferencesUtils.isReadBooleanPreference("excludeMisc")) {
				continue;
			}
			%>clinicalInfoAdd('<%=customCppIssue%>:',con_<%=customCppIssue%>);<%
		}
	%>
    }
     
    
    function uploadPDF(){
  	document.eyeForm.method.value='uploadPDF';
    document.eyeForm.submit();    
    //window.close(); //does not work with chrome
	}
    function faxPreview(){
    	document.eyeForm.target='_blank';
    	document.eyeForm.method.value='faxPreview';
 		document.eyeForm.submit();
	}
 	function printsubmit(){
 		document.eyeForm.target='_top';
 		document.eyeForm.method.value='printConRequest';
 		document.eyeForm.submit();
 	}
 	function savesubmit(){
 		document.eyeForm.method.value='saveConRequest';
 		document.eyeForm.submit();
 		//window.close();  		// does not work with chrome
 	}
 	function faxSubmit() {
 		document.eyeForm.method.value='faxConRequest';
 		document.eyeForm.submit(); 		
 	}
 	
 	function faxSubmit1() { 		
 		  var referral = jQuery("#faxRecipients").find("li");
          if(referral.length>0 && referral != null && referral != undefined) {
             document.eyeForm.method.value='faxConRequest';
             document.eyeForm.submit();   
             //closeWindow(); //does not work with chrome
         } else {
             alert("Please add to the fax recipient first!");
             return false;
          }
 	}
	function checkform(){
		/* Do not need to have referral no. as a must?
		if (document.eyeForm.elements['cp.referralNo'].value=='')
		{
			alert("Please choose the referral doctor.");
			return false;
		}else return true; */
		return true;
	}
	function addDoc(){
		if (document.eyeForm.elements["cp.cc"].value.length<=0)
			document.eyeForm.elements["cp.cc"].value=document.eyeForm.clDoctor.value;
		else document.eyeForm.elements["cp.cc"].value=document.eyeForm.elements["cp.cc"].value+"; "+document.eyeForm.clDoctor.value;
	}
	function addFamDoc(){
		var fd = jQuery("#fam_doc").val();
		if (document.eyeForm.elements["cp.cc"].value.length<=0)
			document.eyeForm.elements["cp.cc"].value=fd;
		else document.eyeForm.elements["cp.cc"].value=document.eyeForm.elements["cp.cc"].value+"; "+ fd;
	}
	function clinicalInfoAdd(str,name){
		if (document.eyeForm.elements["cp.clinicalInfo"].value.length>0 && name!=null && trim(name)!='')
			document.eyeForm.elements["cp.clinicalInfo"].value+='\n\n';

		if (name!=null && trim(name)!='')
			document.eyeForm.elements["cp.clinicalInfo"].value+=name;
	}
	function ocluarproAdd(str,name){
		if (document.eyeForm.elements["cp.clinicalInfo"].value.length>0 && name!=null && trim(name)!='')
			document.eyeForm.elements["cp.clinicalInfo"].value+='\n\n';

		if (name!=null && trim(name)!='')
			document.eyeForm.elements["cp.clinicalInfo"].value+=name;
	}

	function allergiesAdd(){
		if (con_aller!=null && trim(con_aller)!='')
			document.eyeForm.elements["cp.allergies"].value+="Allergies:"+con_aller+"\n";
	}

	function prescriptionsAdd(){
		if (con_presc!=null && trim(con_presc)!='')
			document.eyeForm.elements["cp.allergies"].value+="Current Prescriptions:\n"+con_presc+"\n";
	}
	function currentMedsAdd(str){
		if (str!=null && trim(str)!='')
			document.eyeForm.elements["cp.allergies"].value+="Current Medications:\n"+str+"\n";
	}
	function ocularHisAdd(str){
		if (str!=null && trim(str)!='')
			document.eyeForm.elements["cp.allergies"].value+="Past Ocular History:\n"+str+"\n";
	}

	function impressionAdd(){
		if (con_impress!=null && trim(con_impress)!='')
			document.eyeForm.elements["cp.impression"].value+=con_impress+"\n";
	}
	function planAdd(val){
		document.eyeForm.elements["cp.plan"].value+=val;

	}
	function addExaminationOptions(ob){
		var selected = new Array();
		for (var i = 0; i < ob.options.length; i++) {
			if (ob.options[ i ].selected) {
				selected.push(ob.options[ i ].value);
			}	
		}		
		for (var i = 0; i < selected.length; i++)
			addField(selected[i]);

	}
	function addField(val){
		var temps='';
		switch (val){
			case "specs":

				temps+=(trim(specs['od_'+val+'_sph'])=='')?'':('OD '+trim(specs['od_'+val+'_sph']));
				temps+=(trim(specs['od_'+val+'_cyl'])=='')?'':(trim(specs['od_'+val+'_cyl']));
				temps+=(trim(specs['od_'+val+'_axis'])=='')?'':('x'+trim(specs['od_'+val+'_axis']));
				temps+=(trim(specs['od_'+val+'_add'])=='')?'':(' add '+trim(specs['od_'+val+'_add']));
				temps+=(trim(specs['od_'+val+'_prism'])=='')?'\n':(' prism '+trim(specs['od_'+val+'_prism'])+'\n');
				temps+='      ';
				temps+=(trim(specs['os_'+val+'_sph'])=='')?'':('OS '+trim(specs['os_'+val+'_sph']));
				temps+=(trim(specs['os_'+val+'_cyl'])=='')?'':(trim(specs['os_'+val+'_cyl']));
				temps+=(trim(specs['os_'+val+'_axis'])=='')?'':('x'+trim(specs['os_'+val+'_axis']));
				temps+=(trim(specs['os_'+val+'_add'])=='')?'':(' add '+trim(specs['os_'+val+'_add']));
				temps+=(trim(specs['os_'+val+'_prism'])=='')?'\n':(' prism '+trim(specs['os_'+val+'_prism'])+'\n');
				if (trim(temps)!='\n      \n')
					document.eyeForm.elements['cp.examination'].value+='Specs:'+temps;
				break;
			case "ar":

				temps+=(trim(odMap['od_'+val+'_sph'])=='')?'':('OD '+trim(odMap['od_'+val+'_sph']));
				temps+=(trim(odMap['od_'+val+'_cyl'])=='')?'':(trim(odMap['od_'+val+'_cyl']));
				temps+=(trim(odMap['od_'+val+'_axis'])=='')?'\n':('x'+trim(odMap['od_'+val+'_axis'])+'\n');
				temps+='   ';
				temps+=(trim(osMap['os_'+val+'_sph'])=='')?'':('OS '+trim(osMap['os_'+val+'_sph']));
				temps+=(trim(osMap['os_'+val+'_cyl'])=='')?'':(trim(osMap['os_'+val+'_cyl']));
				temps+=(trim(osMap['os_'+val+'_axis'])=='')?'\n':('x'+trim(osMap['os_'+val+'_axis'])+'\n');
				if (trim(temps)!='\n   \n')
					document.eyeForm.elements['cp.examination'].value+='AR:'+temps;
				break;

			case "k":
				temps+=(trim(odMap['od_'+val+'1'])==''&&trim(odMap['od_'+val+'2'])==''&&trim(odMap['od_'+val+'2_axis'])=='')?'':'OD ';
				temps+=(trim(odMap['od_'+val+'1'])=='')?'':(''+trim(odMap['od_'+val+'1']));
				temps+=(trim(odMap['od_'+val+'2'])=='')?'':('x'+trim(odMap['od_'+val+'2']));
				temps+=(trim(odMap['od_'+val+'2_axis'])=='')?'\n':('@'+trim(odMap['od_'+val+'2_axis'])+'\n');
				temps+='  ';
				temps+=(trim(osMap['os_'+val+'1'])==''&&trim(osMap['os_'+val+'2'])==''&&trim(osMap['os_'+val+'2_axis'])=='')?'':'OS ';
				temps+=(trim(osMap['os_'+val+'1'])=='')?'':(''+trim(osMap['os_'+val+'1']));
				temps+=(trim(osMap['os_'+val+'2'])=='')?'':('x'+trim(osMap['os_'+val+'2']));
				temps+=(trim(osMap['os_'+val+'2_axis'])=='')?'\n':('@'+trim(osMap['os_'+val+'2_axis'])+'\n');
				if (trim(temps)!='\n  \n')
					document.eyeForm.elements['cp.examination'].value+='K:'+temps;
				break;
			case "manifest_refraction":

				temps+=(trim(odMap['od_'+val+'_sph'])=='')?'':('OD '+trim(odMap['od_'+val+'_sph']));
				temps+=(trim(odMap['od_'+val+'_cyl'])=='')?'':(trim(odMap['od_'+val+'_cyl']));
				temps+=(trim(odMap['od_'+val+'_axis'])=='')?'':('x'+trim(odMap['od_'+val+'_axis']));
				temps+=(trim(odMap['od_'+val+'_add'])=='')?'\n':(' add '+trim(odMap['od_'+val+'_add'])+'\n');
				temps+='                    ';
				temps+=(trim(osMap['os_'+val+'_sph'])=='')?'':('OS '+trim(osMap['os_'+val+'_sph']));
				temps+=(trim(osMap['os_'+val+'_cyl'])=='')?'':(trim(osMap['os_'+val+'_cyl']));
				temps+=(trim(osMap['os_'+val+'_axis'])=='')?'':('x'+trim(osMap['os_'+val+'_axis']));
				temps+=(trim(osMap['os_'+val+'_add'])=='')?'\n':(' add '+trim(osMap['os_'+val+'_add'])+'\n');
				if (trim(temps)!='\n                    \n')
					document.eyeForm.elements['cp.examination'].value+='Manifest refraction:'+temps;
				break;
			case "cycloplegic_refraction":

				temps+=(trim(odMap['od_'+val+'_sph'])=='')?'':('OD '+trim(odMap['od_'+val+'_sph']));
				temps+=(trim(odMap['od_'+val+'_cyl'])=='')?'':(trim(odMap['od_'+val+'_cyl']));
				temps+=(trim(odMap['od_'+val+'_axis'])=='')?'':('x'+trim(odMap['od_'+val+'_axis']));
				temps+=(trim(odMap['od_'+val+'_add'])=='')?'\n':(' add '+trim(odMap['od_'+val+'_add'])+'\n');
				temps+='                       ';
				temps+=(trim(osMap['os_'+val+'_sph'])=='')?'':('OS '+trim(osMap['os_'+val+'_sph']));
				temps+=(trim(osMap['os_'+val+'_cyl'])=='')?'':(trim(osMap['os_'+val+'_cyl']));
				temps+=(trim(osMap['os_'+val+'_axis'])=='')?'':('x'+trim(osMap['os_'+val+'_axis']));
				temps+=(trim(osMap['os_'+val+'_add'])=='')?'\n':(' add '+trim(osMap['os_'+val+'_add'])+'\n');
				if (trim(temps)!='\n                       \n')
				document.eyeForm.elements['cp.examination'].value+='Cycloplegic refraction:'+temps;
				break;
			case "EOM":
				temps+=(ouMap['EOM']=='')?'':'EOM:'+(trim(ouMap['EOM'])+'\n');
				if (trim(temps)!='')
					document.eyeForm.elements['cp.examination'].value+=temps;
				break;
			case "cd_ratio_horizontal":

				temps+=(trim(odMap['od_'+val])=='')?'':('OD '+trim(odMap['od_'+val])+'\n');
				temps+='          ';
				temps+=(trim(osMap['os_'+val])=='')?'\n':('OS '+trim(osMap['os_'+val])+'\n');
				if (trim(temps)!='' && trim(temps)!='\n')
					document.eyeForm.elements['cp.examination'].value+='c/d ratio:'+temps;
				break;
			case "angle":
				temps+=(trim(odMap['od_'+val+'_middle1'])=='')?'':('OD '+trim(odMap['od_'+val+'_middle1'])+'\n');
				temps+='      ';
				temps+=(trim(osMap['os_'+val+'_middle1'])=='')?'\n':('OS '+trim(osMap['os_'+val+'_middle1'])+'\n');
				if (trim(temps)!='' && trim(temps)!='\n')
					document.eyeForm.elements['cp.examination'].value+='angle:'+temps;
				break;
			default:
				var ts="";
				for(var i=0;i<val.length;i++) ts+=" ";

				temps+=(trim(odMap['od_'+val])=='')?'':('OD '+trim(odMap['od_'+val])+'\n');
				temps+=ts+' ';
				temps+=(trim(osMap['os_'+val])=='')?'\n':('OS '+trim(osMap['os_'+val])+'\n');
				if (trim(temps)!=null && trim(temps)!='' && trim(temps)!='\n')
					document.eyeForm.elements['cp.examination'].value+=val+':'+temps;
		}
		return;
	}
	var specs=[];
	specs['od_specs_sph']='';
	specs['od_specs_cyl']='';
	specs['od_specs_axis']='';
	specs['od_specs_add']='';
	specs['od_specs_prism']='';
	specs['os_specs_sph']='';
	specs['os_specs_cyl']='';
	specs['os_specs_axis']='';
	specs['os_specs_add']='';
	specs['os_specs_prism']='';
	var osMap=[];
	<c:forEach items="${sessionScope.osMap}" var="field">
    osMap['']='';
    </c:forEach>
    var ouMap=[];
	<c:forEach items="${sessionScope.ouMap}" var="field">
    ouMap['']='';
    </c:forEach>
    var odMap=[];
	<c:forEach items="${sessionScope.odMap}" var="field">
    odMap['>']='';
    </c:forEach>

	 function popupPageSmall(varpage,name) {
        var page = "" + varpage;
        windowprops = "height=300,width=700,location=no,"
          + "scrollbars=yes,menubars=no,toolbars=no,resizable=yes,top=0,left=0";
        window.open(page, name, windowprops);
    }
    function sendTickler(){
    	var req="<%=request.getContextPath()%>/eyeform/Eyeform.do?method=specialRepTickler&demographicNo=<%=request.getAttribute("demographicNo")%>";
    	if (document.eyeForm.ack.checked==true){
    		req+='&docFlag=true';
    		popupPageSmall(req,'sendTickler');
    	}else{
    		alert("Please check the checkbox before click this button!");
    	}
    }
	function rs(n,u,w,h,x) {
  		args="width="+w+",height="+h+",resizable=yes,scrollbars=yes,status=0,top=360,left=30";
  		remote=window.open(u,n,args);
  		if (remote != null) {
    		if (remote.opener == null)
      		remote.opener = self;
  		}
  		if (x == 1) { return remote; }
	}
	function referralScriptAttach2(elementName, name2, name3, name4, name5) {
		 var e = name3;
		 var t0,t1,t2,t3,t4;
		 if (elementName != null) {
			 t0 = escape("document.forms[0].elements[\'"+elementName+"\'].value");
		 } else {
			 t0 = "";
		 }
		 if (name2 != null) {
			 if(name2 == "referral_doc_name_specialist"){
				 t1 = escape("document.forms[0].elements[\'"+name2+"\'][0].value");
			 }else{
				 t1 = escape("document.forms[0].elements[\'"+name2+"\'].value");
			 }
		 } else {
			 t1 = "";
		 }
		 
		 if (name3 != null) {
			 t2 = escape("document.forms[0].elements[\'"+name3+"\'].value");;
		 } else {
			 t2 = "";
		 }
		 
		 if (name4 != null) {
			 t3 = escape("document.forms[0].elements[\'"+name4+"\'].value");;
		 } else {
			 t3 = "";
		 }
		 
		 if (name5 != null) {
			 t4 = escape("document.forms[0].elements[\'"+name5+"\'].value");;
		 } else {
			 t4 = "";
		 }
	     
	     var url = '<%=(String)session.getAttribute("oscar_context_path")%>/billing/CA/ON/searchRefDoc.jsp?'
	    		 + 'param=' + t0 + '&param2=' + t1
	    		 + '&param3='+ t2 + '&param4=' + t3 + '&param5=' + t4;
		
	     rs('att',(url),600,600,1);
		}
		
		function selectFamDoc() {
			// set cp.contactId and cp.contactType
			jQuery("input[name='cp.contactId']").val(jQuery("#fam_doc option:selected")[0].getAttribute("contactId"));
			jQuery("input[name='cp.contactType']").val(jQuery("#fam_doc option:selected")[0].getAttribute("contactType"));	
		}
  </script>
   <c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
   <script src="<c:out value="${ctx}/js/jquery.js"/>"></script>
   <script>
     jQuery.noConflict();
   </script>

	<oscar:customInterface section="conreport"/>

<script>
var ctx;
var appointmentNo;

jQuery(document).ready(function() {
	ctx = '<%=request.getContextPath()%>';
	demoNo = '<%=demographicNo%>';
	appointmentNo = document.eyeForm.elements['cp.appointmentNo'].value;
	if(appointmentNo == "0"){
		alert("You are creating a consultation report from an incorrect appointment, please close it and create it from a correct appointment");
	}
	selectFamDoc();
});
</script>

</head>
<body topmargin="0" leftmargin="0" vlink="#0000FF">

<html:form action="/eyeform/Eyeform">
	<input type="hidden" name="method" value="saveConRequest"/>
	<input type="hidden" name="demographicNo" value="<%=EyeformAction.getField(request,"demographicNo")%>"/>
	<input type="hidden" name="referralNo" value=""/>
	<input type="hidden" name="referralId" value=""/>
	<input type="hidden" name="otherDocId" value=""/>
	<input type="hidden" name="famDoctor" value=""/>
	<input type="hidden" name="apptno" value=""/>

	<html:hidden property="cp.id"/>
	<html:hidden property="cp.demographicNo"/>
	<html:hidden property="cp.providerNo"/>
	<html:hidden property="cp.appointmentNo"/>
	<html:hidden property="cp.urgency"/>
	<html:hidden property="cp.reason"/>
	<html:hidden property="cp.referralId"/>
	<html:hidden property="cp.referralNo"/>
	<html:hidden property="cp.examination"/>
	<html:hidden property="cp.referralFax"/>
	<html:hidden property="cp.otherReferralId"/>
	<html:hidden property="cp.contactId"/>
	<html:hidden property="cp.contactType"/>

	<table class="MainTable" id="scrollNumber1" name="encounterTable">
		<tr class="MainTableTopRow">
			<td class="MainTableTopRowLeftColumn">Consultation report</td>
			<td class="MainTableTopRowRightColumn">
			<table class="TopStatusBar">
				<tr>
					<td class="Header"
						style="padding-left: 1px; padding-right: 1px; border-right: 1px solid #003399; text-align: left; font-size: 80%; font-weight: bold; width: 100%;"
						NOWRAP><%=Encode.forHtml((String) request.getAttribute("demographicName"))%>
						<nested:equal
						property="isRefOnline" value="true">
						<img align="absmiddle" src="${pageContext.request.contextPath}/images/onlineicon.gif" height="20"
							width="20" border="0">
					</nested:equal> </td>
				</tr>
			</table>
			</td>
		</tr>

		<tr style="vertical-align: top">
			<td class="MainTableLeftColumn">
			<table>
				<tr>
					<td class="tite4" colspan="2"><c:if
						test="${requestScope.newFlag!='true' }">
						<table>
							<tr>
								<td class="stat" colspan="2">Created by:</td>

							</tr>
							<tr>
								<td><c:out value="${providerName}" /></td>
							</tr>
						</table>
					</c:if></td>
				</tr>
				<tr>
					<td class="tite4" colspan="2">consultation report status:</td>
				</tr>
				<tr>
					<td class="tite4" colspan="2">
					<table>
						<tr>
							<td class="stat"><html:radio property="cp.status"
								value="Incomplete" /></td>
							<td class="stat">Incomplete</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td class="tite4" colspan="2">
					<table>
						<tr>
							<td class="stat">
							<nested:equal property="isRefOnline"
								value="true">
								<html:radio property="cp.status" value="Completed,and sent"
									onclick="return confirmCompleted(this)" />
							</nested:equal> <nested:notEqual property="isRefOnline" value="true">
							<html:radio property="cp.status"
								value="Completed,not sent" /></nested:notEqual></td>
							<td class="stat">Completed,not sent</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td class="tite4" colspan="2">
					<table>
						<tr>
							<td class="stat"><html:radio property="cp.status" value="Completed,and sent" /></td>
							<td class="stat">Completed,and sent</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
			<td class="MainTableRightColumn">
			<table cellpadding="0" cellspacing="2"
				style="border-collapse: collapse; width: 100%" bordercolor="#111111"
				width="100%" height="100%" border=1>


				<tr>
					<td>
					<table border=0 width="100%">


						<tr>
							<td class="tite4">to:</td>
							<%
								String referralDocName = (String)request.getAttribute("referral_doc_name");
								if(referralDocName==null)
									referralDocName=new String();
								
								String specialty = "";
								
								Integer referralId = (Integer)request.getAttribute("referral_id");
								if((referralId != null) && (referralId > 0)){
									ProfessionalSpecialist professionalSpecialist = null;
									professionalSpecialist = professionalSpecialistDao.getById(referralId);
									if(professionalSpecialist != null){
										specialty = professionalSpecialist.getSpecialtyType();
									}
								}else{
									List<ProfessionalSpecialist> professionalSpecialists = null;
									String re_na = referralDocName==null?"":referralDocName;
									if(!re_na.equals("")){
										String[] temp = re_na.split("\\,\\p{Space}*");
										if (temp.length>1) {		
											professionalSpecialists = professionalSpecialistDao.findByFullName(temp[0], temp[1]);
										}else{		
											professionalSpecialists = professionalSpecialistDao.findByLastName(temp[0]);
										}
									}
									if(professionalSpecialists != null){
										specialty = professionalSpecialists.get(0).getSpecialtyType();
									}
								}
								if (specialty == null) {
									specialty = "";
								}
							%>
							<td align="left" class="tite1"><input type="text"
								name="referral_doc_name" value="<%=referralDocName%>"/><a
								href="javascript:referralScriptAttach2('cp.referralNo','referral_doc_name','cp.referralId', 'cp.referralFax')"><span
								style="font-size: 10;">Search #</span></a>&nbsp;&nbsp;&nbsp;<p id="specialty_value" name="specialty_value" style="display:inline"><%=specialty%></p>
								<%if (faxEnabled) {%>
								<input type="button" class="btn" onclick="addToFax();" value="Add to fax recipients" />
								<%} %>
							</td>
					</table>
					</td>
					<td valign="top" cellspacing="1" class="tite4">
					<table border=0 width="100%" bgcolor="white">
						<tr>
							<td class="tite4">re:</td>

							<td class="tite1"><c:out value="${reason}"/></td>
						</tr>
					</table>
					</td>
				</tr>
				<%
				for(int i = 0 ;i < propDemoExt.length;i ++){
					if(propDemoExt[i].replace(' ', '_').trim().contains("_No")){
						continue;
					} else{
						String mainkey = propDemoExt[i].replace(' ', '_').trim()+"_No";
						if(!demoExt.keySet().contains(mainkey)){
							continue;
						}
						String re_no = demoExt.get(mainkey).trim();
						if(re_no==null){
							continue;
						}
						
						String re_name = demoExt.get(propDemoExt[i].replace(' ', '_')).trim();
						if(!re_no.equals("") || !re_name.equals("")){
						   List<ProfessionalSpecialist> professionalSpecialists = null;
						    if(!re_no.equals("") ){
							  professionalSpecialists = professionalSpecialistDao.findByReferralNo(re_no);
							}else{
							    String[] temp = re_name.split("\\,\\p{Space}*");
						    
						    	if (temp.length>1) {		
						     		professionalSpecialists = professionalSpecialistDao.findByFullName(temp[0], temp[1]);
						  	 	} else {		
						    		professionalSpecialists = professionalSpecialistDao.findByLastName(temp[0]);
						     	}
							}	
				       	  	if (professionalSpecialists != null) {
								 ProfessionalSpecialist professionalSpecialist = professionalSpecialists.get(0);
			%>
			<tr>
				<td>
					<table border="0" width="100%">
						<tr>
							<td class="tite4">to:</td>
							<td align="left" class="tite1">
								<input type="text" name="referral_doc_name_specialist" value="<%=professionalSpecialist.getFormattedName()%>"/>
								<%if (faxEnabled) {%>
								<input type="button" class="btn" onclick="addSpeciallListToFax('<%= professionalSpecialist.getFaxNumber()%>', '<%=professionalSpecialist.getFormattedName()%>');" value="Add to fax recipients" />
								<%} %>
							</td>
						</tr>
					</table>	
				</td>
				<td></td>
			</tr>
			<%				       	  	
							}
						}
					}
				}
				%>

				<tr>
					<td width="50%" class="tite4">
					<table width="100%">
						<tr>
							<td class="tite4">
								<select id="fam_doc" onchange="selectFamDoc(this);">

									<%
									List<DemographicContact> contacts = (List<DemographicContact>)request.getAttribute("contacts");
									ContactDao contactDao = (ContactDao)SpringUtils.getBean(ContactDao.class);
									ProviderDao providerDao = (ProviderDao)SpringUtils.getBean(ProviderDao.class);
									if(contacts.isEmpty()) {
									%>	  
										<option selected faxNum="" contactId="" contactType="" value=""></option>
									<%	
									} else {
										for(DemographicContact c:contacts) {
											String faxNum = "";
											try {												
												if (c.getType() == 0) { // internal provider
													Provider prov = providerDao.getProvider(c.getContactId());
													if (prov != null) {
														faxNum = oscar.SxmlMisc.getXmlContent(prov.getComments(), "xml_p_fax");
													}
												} else if (c.getType() == 2) { // external professional contact
													Contact contactTmp = contactDao.find(Integer.parseInt(c.getContactId()));
													faxNum = contactTmp.getFax();
												} else if (c.getType() == 3) { //external specialist
												  ProfessionalSpecialist specialist = professionalSpecialistDao.find(Integer.parseInt(c.getContactId()));
												  faxNum = specialist.getFaxNumber();
												}
											} catch (Exception e) {
												MiscUtils.getLogger().info(e.toString());
											}
											faxNum = faxNum.replace("-","").replace(" ","");
											String selected = "";
											if (cp != null && cp.getContactId()!=null && c.getContactId()!=null && cp.getContactId().equals(c.getContactId()) && cp.getContactType() == c.getType()) {
												selected = "selected";
											}
											
											%><option <%=selected %> faxNum="<%=faxNum%>" contactId="<%=c.getContactId() %>" contactType="<%=c.getType() %>" value="<%=c.getContactName() %>"><%=c.getRole()%> - <%=c.getContactName() %></option><%
										}
									}
									%>
								</select>
								&nbsp;
								<input type="button" class="btn" onclick="addFamDoc();" value="add to cc">
								<%if (faxEnabled) {%>
								<input type="button" class="btn" onclick="addFamFax();" value="Add to fax recipients" />
								<%} %>
							</td>

							<td></td>
						</tr>
					</table>
					</td>
					<td class="tite4">
					<table width="100%">
						<tr>
							<td class="tite1" colspan="2">
								<input type="text" style="width:120px;" name="clDoctor" value="<%=clDoctor%>"/>
								<input type="hidden" name="otherDocFax" value="<%=otherDocFax%>"/>
								<input type="button" class="btn" onclick="addDoc();" value="add to cc">
								<a href="javascript:referralScriptAttach2('otherDocId','clDoctor','cp.otherReferralId','otherDocFax')">
								<span style="font-size: 10;">Search #</span></a>
								<%if (faxEnabled) {%>
								<input type="button" class="btn" onclick="addCcFax()" value="Add to fax recipients" />
								<%} %>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
					<table style="width: 100%">
						<tr>
							<td width="10%" class="tite4">cc:</td>
							<td width="90%" class="tite4"><html:text style="width:100%"
								property="cp.cc" /></td>
						</tr>
					</table>
					</td>
				</tr>
				
				<%if (faxEnabled) {%>
				<tr>
					<td colspan="2">
						<table style="width: 100%">
							<tr><td>
								<ul id="faxRecipients">
									<!--
									var remove = "<a href='javascript:void(0);' onclick='removeRecipient(this)'>remove</a>";
									var html = "<li>"+name+"<b>, Fax No: </b>"+number+ " " +remove
										+"<input type='hidden' name='faxRecipients' value='"+number+"'></input></li>";
									jQuery("#faxRecipients").append(jQuery(html));
									-->
								</ul></td>
							</tr>
						</table>
					</td>
				</tr>
				<%} %>

				<tr>
				<% if (org.oscarehr.common.IsPropertiesOn.isMultisitesEnable()) { %>
					<td>
					<table style="background-color: #ddddff;" width="100%">
						<tr>
							<td class="tite4" width="10%">Greeting:</td>
							<td class="tite4" width="60%"><html:select
								property="cp.greeting">
								<html:option value="1">standard consult report</html:option>
								<html:option value="2">assessment report</html:option>
							</html:select></td>
						</tr>
					</table>
					</td>
					<td>
					<table style="background-color: #ddddff;" width="100%">
						<tr>
							<td class="tite4" width="10%">Site:</td>
							<td class="tite4" width="60%">
								<html:select property="cp.siteId">
								     <c:forEach var="site" items="${sites}">
								         <html:option value="${site.id}"><c:out value="${site.name}"/></html:option>
								     </c:forEach>
							        </html:select>
							</td>
						</tr>
					</table>
					</td>
				<%  } else { %>
					<td colspan="4">
					<table style="background-color: #ddddff;" width="100%">
						<tr>
							<td class="tite4" width="10%">Greeting:</td>
							<td class="tite4" width="60%"><html:select
								property="cp.greeting">
								<html:option value="1">standard consult report</html:option>
								<html:option value="2">assessment report</html:option>
							</html:select></td>
						</tr>
					</table>
					</td>				
				<%  }  %>
				</tr>

				<tr>
					<td colspan=2 class="tite4">
					<table width="100%">
						<tr>
							<td width="27%" class="tite4">Clinical information:</td>
							<td>
							<%if(whichEyeForm !=null && whichEyeForm.equalsIgnoreCase("eyeform_DrJinapriya")) { %>
							<input type="button" class="btn" value="subjective"	name="chis"	onclick="clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.CurrentHistory"/>:',con_cHis)">
							<input type="button" class="btn" value="past ocular hx" name="phis"	onclick="clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.PastOcularHistory"/>:',con_pHis)">
							<input type="button" class="btn" value="medical hx" name="mhis"	onclick="clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.MedicalHistory"/>:',con_mHis)">
							<input type="button" class="btn" value="ocular diag" name="fhis" onclick="clinicalInfoAdd('Family history:',con_fHis)">
							<input type="button" class="btn" value="specs hx" name="shis" onclick="clinicalInfoAdd('Specs history:',con_sHis)">

							<input type="button" class="btn" value="drops admin" name="ohis" onclick="clinicalInfoAdd('Ocular meds:',con_oMeds)">
							<input type="button" class="btn" value="systemic meds" name="ohis"	onclick="clinicalInfoAdd('Other meds:',con_oHis)">
							<input type="button" class="btn" value="objective" name="dnote" onclick="clinicalInfoAdd('Diagnostics notes:',con_diag)">
							<input type="button" class="btn" value="ocular proc" name="opro" onclick="ocluarproAdd('Ocular procedure:',con_ocularpro)">
					<%} else if(whichEyeForm !=null && whichEyeForm.equalsIgnoreCase("eyeform_simple")){ %>		
							<input type="button" class="btn" value="POH"	name="chis"	onclick="clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.CurrentHistory"/>:',con_cHis)">
							<input type="button" class="btn" value="Drops" name="phis"	onclick="clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.PastOcularHistory"/>:',con_pHis)">
							<input type="button" class="btn" value="PMH/meds" name="mhis"	onclick="clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.MedicalHistory"/>:',con_mHis)">

							<input type="button" class="btn" value="Sx/Laser" name="dnote" onclick="clinicalInfoAdd('Diagnostics notes:',con_diag)">
					<%} else { %>
							<input type="button" class="btn" value="current hx"	name="chis"	onclick="clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.CurrentHistory"/>:',con_cHis)">
							<input type="button" class="btn" value="past ocular hx" name="phis"	onclick="clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.PastOcularHistory"/>:',con_pHis)">
							<input type="button" class="btn" value="medical hx" name="mhis"	onclick="clinicalInfoAdd('<bean:message key="oscarEncounter.oscarConsultationReport.PrintReport.MedicalHistory"/>:',con_mHis)">
							<input type="button" class="btn" value="family hx" name="fhis" onclick="clinicalInfoAdd('Family history:',con_fHis)">
							<input type="button" class="btn" value="specs hx" name="shis" onclick="clinicalInfoAdd('Specs history:',con_sHis)">

							<input type="button" class="btn" value="ocular meds" name="ohis" onclick="clinicalInfoAdd('Ocular meds:',con_oMeds)">
							<input type="button" class="btn" value="other meds" name="ohis"	onclick="clinicalInfoAdd('Other meds:',con_oHis)">
							<input type="button" class="btn" value="diag notes" name="dnote" onclick="clinicalInfoAdd('Diagnostics notes:',con_diag)">
							<input type="button" class="btn" value="ocular proc" name="opro" onclick="ocluarproAdd('Ocular procedure:',con_ocularpro)">
					<% } %>
<%
	for(String customCppIssue:customCppIssues) {
		if(customCppIssue.equals("")) continue;
		%><input type="button" class="btn" value="<%=customCppIssue %>" name="<%=customCppIssue %>" onclick="clinicalInfoAdd('<%=customCppIssue%>:',con_<%=customCppIssue%>)"><%
	}
%>

							</td>
						</tr>
					</table>
					</td>
				</tr>



				<tr>
					<td colspan=2><html:textarea rows="4" style="width:100%"
						property="cp.clinicalInfo" /></td>
				<tr>
				<tr>
					<td colspan=2 class="tite4">
					<table width="100%">
						<tr>
							<td width="30%" class="tite4">Allergies and Medications:</td>
							<td><input type="button" class="btn" value="Allergies"
								name="allergies" onclick="allergiesAdd()"> <input
								type="button" class="btn" value="Prescriptions" name="prescript"
								onclick="prescriptionsAdd()"></td>
						</tr>
					</table>
					</td>
				</tr>

				<tr>
					<td colspan=2><html:textarea rows="4" style="width:100%"
						property="cp.allergies" /></td>
				</tr>

				<tr>
					<td colspan=2 class="tite4">
					<table width="100%">
						<tr>
							<td width="30%" class="tite4">Examination field:</td>
						</tr>
					</table>

					</td>
				</tr>

				<tr>
					<td colspan=2 style="width: 100%">
					<table>
						<tr>
               		<td>
                			<select id="fromlist1" name="fromlist1" multiple="multiple" size="9" ondblclick="addSection1(document.eyeForm.elements['fromlist1'],document.eyeForm.elements['fromlist2'], hasRwoArM, isIntegrateOptometry);">
                				<c:forEach var="item" items="${sections}">
                					<option value="<c:out value="${item.value}"/>"><c:out value="${item.label}"/></option>
                				</c:forEach>
                			</select>
                		</td>
                		<td valign="middle">
                			<input type="button" value=">>" onclick="addSection1(document.eyeForm.elements['fromlist1'],document.eyeForm.elements['fromlist2'], hasRwoArM, isIntegrateOptometry);"/>
                		</td>
                		<td>
                			<select id="fromlist2" name="fromlist2" multiple="multiple" size="9" ondblclick="addExam(ctx,'fromlist2',document.getElementById('cp.examination'),appointmentNo);">
                				<c:forEach var="item" items="${headers}">
                					<option value="<c:out value="${item.value}"/>"><c:out value="${item.label}"/></option>
                				</c:forEach>
                			</select>
							<input style="vertical-align: middle;" type="button" value="add" onclick="addExam(ctx,'fromlist2',document.getElementById('cp_examination'),appointmentNo);">
						</td>
						</tr>
					</table>
					</td>
				</tr>

				<tr>
					<td colspan=2 style="width: 100%">
					<table style="width: 100%">
						<tr>
							<td width="74%">
								<div contentEditable="true" name="cp_examination" id="cp_examination" onKeyUp="change_examination();" style="display:block;border:1px solid gray;overflow:scroll;height:150px;width:970px;overflow-x:hidden;word-wrap:break-word;"><%=exam_val%></div>
							</td>
						</tr>
					</table>
					</td>
				</tr>
<script type="text/javascript">
function newDocType(){
	var newOpt = prompt("Please enter new document type:", "");

	if(newOpt == null)
		return;

	if (newOpt != "") {
	    document.getElementById("docType").options[document.getElementById("docType").length] = new Option(newOpt, newOpt);
	    document.getElementById("docType").options[document.getElementById("docType").length-1].selected = true;
		
	    } else {
	    alert("Invalid entry");
	}
	
}

function showhide(hideelement, button) {
    var plus = "+";
    var minus = "--";
    if (document.getElementById) { // DOM3 = IE5, NS6
        if (document.getElementById(hideelement).style.display == 'none') {
              document.getElementById(hideelement).style.display = 'block';
              document.getElementById(button).innerHTML = document.getElementById(button).innerHTML.replace(plus, minus);
              if ((hideelement == "addDocDiv") && (document.getElementById("addLinkDiv").style.display != "none")) {
                   showhide("addLinkDiv", "plusminusLinkA");
              } else if ((hideelement == "addLinkDiv") && (document.getElementById("addDocDiv").style.display != "none")) {
                   showhide("addDocDiv", "plusminusAddDocA");
              }
        }
        else {
              document.getElementById(hideelement).style.display = 'none';
              document.getElementById(button).innerHTML = document.getElementById(button).innerHTML.replace(minus, plus);;
        }
    }
}

function submitUpload(object) {
    object.Submit.disabled = true;
    if (!validDate("observationDate")) {
        alert("Invalid Date: must be in format: yyyy/mm/dd");
        object.Submit.disabled = false;
        return false;
    }
    return true;
}
	function change_examination(){
		var str = jQuery("#cp_examination").html();
		str = str.replace(new RegExp("<b>", 'g'),"");
		str = str.replace(new RegExp("</b>", 'g'),"");
		str = str.replace(new RegExp("<br>", 'g'),"");
		if(str.length == 0){
			str = "";
			jQuery("#cp_examination").html("");
			document.getElementsByName("cp.examination")[0].value = "";
		}else{
			document.getElementsByName("cp.examination")[0].value = jQuery("#cp_examination").html();
		}
	}
</script>
				<tr>
					<td colspan=2 class="tite4">
					<table width="100%">
						<tr>
							<td width="30%" class="tite4">Impression/Plan:</td>
							<td><input type="button" class="btn" value="Impression"
								name="impre" onclick="impressionAdd()"></td>
						</tr>
					</table>
					</td>
				</tr>

				<tr>
					<td colspan="2"><html:textarea rows="3" style="width:100%"
						property="cp.impression" /></td>
				</tr>
<!--
				<tr>
					<td colspan=2 class="tite4">
					<table width="100%">
						<tr>
							<td width="30%" class="tite4">Treatment/Follow-up:</td>
							<td><input type="button" class="btn" value="procedure"
								name="proce" onclick="planAdd(con_probook)"> <input
								type="button" class="btn" value="test" name="digtest"
								onclick="planAdd(con_testbook)"> <input type="button"
								class="btn" value="follow-up" name="follow"
								onclick="planAdd(con_follow)"></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan="2"><html:textarea rows="4" style="width:100%"
						property="cp.plan" /></td>
				</tr>
-->
				<c:if test='${param.from!="out"}'>
					<tr>
						<td colspan=2 class="tite4">Send Tickler:</td>
					</tr>

					<tr>
						<td colspan="2"><input type="checkbox" name="ack" checked>
						remind me to complete it <input type="button" name="sendtickler"
							value="send tickler" onclick="sendConReportTickler(ctx,demoNo);"></td>
					</tr>
				</c:if>
				<tr>
					<td colspan="2" align="right">
					<input type="button" name="getOSCARDataBtn" id=getOSCARDataBtn value="Get OSCAR Data" onclick="if (checkform()) {getOSCARData();} else {return false;}"/>
					<% if(isShowUpload) { %>
					<input type="button" name="uploadPDFBtn" id="uploadPDFBtn" value="Upload PDF" onclick="if (checkform()) {uploadPDF();} else {return false;}"/>
					<%} %>
					<input type="button" name="faxPreBtn" id="faxPreBtn" value="Fax Preview" onclick="if (checkform()) {faxPreview();} else {return false;}"/>
					<% if(faxEnabled) { %>
					<input type="button" name="faxBtn" id="faxBtn" value="save and fax" onclick="if (checkform()) {faxSubmit1();} else {return false;}"/>
					<%} %>
					<nested:equal property="isRefOnline" value="true">
						<input type="button" value="save and print"
							onclick="if (confirmPrint()) if (checkform())printsubmit();else return false;">
					</nested:equal> 
					<nested:notEqual property="isRefOnline" value="true">
						<input type="button" value="save and print"
							onclick="if (checkform())printsubmit();else return false;">
					</nested:notEqual>
					<input type="button" value="save and close"
						onclick="if (checkform())savesubmit();else return false;">
					</td>
				</tr>
			
			</table>
			</td>
		</tr>
	</table>
	
</html:form>
<script type="text/javascript">
	var myFaxStr="";
	var myNameStr="";
	var myArray=new Array();
	var myNameArray=new Array();
	function addToFax() {
		var referralfax = jQuery("input[name='cp.referralFax']")[0];
		if (referralfax == null) {
			return;
		}
		var f = referralfax.value.trim();
		if(isInArray(myArray,f)){
			myArray[myArray.length]=f;
			myNameArray[myNameArray.length]=jQuery("input[name='referral_doc_name']")[0].value;
			myFaxStr=myArray.join('|');
			myNameStr=myNameArray.join('|');
			_addFaxRecipient("To", jQuery("input[name='referral_doc_name']")[0].value, f); 
		}
	}
	
	function addSpeciallListToFax(faxNo, faxName){
		if (faxNo.length == 0) {
			return;
		}
		var f = faxNo.trim();
		if(isInArray(myArray,f)){
			myArray[myArray.length]=f;
			myNameArray[myNameArray.length]=faxName;
			myFaxStr=myArray.join('|');
			myNameStr=myNameArray.join('|');
			_addAllFaxRecipient("AllTo", faxName, f); 
		}
	}
	
	function addAllFax() {
		
		<% // customized key + "Has Primary Care Physician" & "Employment Status"
		if(demoExt == null || demoExt.size()==0){
			return;
		}
		
		for(int i=0 ; i<propDemoExt.length;i++){
			if(propDemoExt[i].replace(' ', '_').trim().contains("_No")){
				continue;
			} else{
				String mainkey = propDemoExt[i].replace(' ', '_').trim()+"_No";
				if(!demoExt.keySet().contains(mainkey)){
					continue;
				}
				String re_no = demoExt.get(mainkey).trim();
				if(re_no==null){
					continue;
				}
				String re_na = demoExt.get(propDemoExt[i].replace(' ', '_')).trim();
				if(!re_no.equals("") || !re_na.equals("")){
					   List<ProfessionalSpecialist> professionalSpecialists = null;
					    if(!re_no.equals("") ){
						  professionalSpecialists = professionalSpecialistDao.findByReferralNo(re_no);
						}else{
						    String[] temp = re_na.split("\\,\\p{Space}*");
					    
					    if (temp.length>1) {		
					     professionalSpecialists = professionalSpecialistDao.findByFullName(temp[0], temp[1]);
					   } else {		
					    	professionalSpecialists = professionalSpecialistDao.findByLastName(temp[0]);
					     }
						}	
				       	  if (professionalSpecialists != null) {
							 ProfessionalSpecialist professionalSpecialist = professionalSpecialists.get(0);
							 %>
							if(isInArray(myArray,"<%=professionalSpecialist.getFaxNumber()%>")){
								 myArray[<%=i+1%>]="<%=professionalSpecialist.getFaxNumber()%>";
								 myNameArray[<%=i+1%>]="<%=re_na%>";
								 myFaxStr=myArray.join('|');
								 myNameStr=myNameArray.join('|');
								 _addAllFaxRecipient("AllTo", "<%=re_na%>", "<%=professionalSpecialist.getFaxNumber()%>");
							}
							<%
					  }
				    		
					  }
			}
		}
		%>
	}
	
	function _addAllFaxRecipient(type, referalName, faxNum) {
		if (faxNum == null || faxNum.length==0) {
			return;
		}
		/*   var obj = jQuery("#faxRecipients").find("a[type='" + type + "']");
		if (obj != null) {
			removeRecipient(obj[0]);
		}  */ 
		 var remove = "<a type='"+type+"' href='javascript:void(0);' onclick='removeRecipient(this)'>remove</a>";
		 var referral = jQuery("input[name='faxRecipientsAllTo']");
			var referralName = jQuery("input[name='faxRecipientsNameAllTo']");
            if(referral.length>0 && referral != null && referral != undefined) {
            	referral.remove();
            	referralName.remove();
	         } 
         var html = "<li>" + referalName + "&nbsp;&nbsp;<b>Fax:</b>"+faxNum+ " " +remove+ "<input type='hidden' name='faxRecipients"+type+"' value='"+myFaxStr+"'></input><input type='hidden' name='faxRecipientsName" + type + "' value='" + myNameStr+"'></input></li>";
		
		jQuery("#faxRecipients").append(html);
	}
	
	function isInArray(arr,value){
	    if(arr.indexOf&&typeof(arr.indexOf)=='function'){
	        var index = arr.indexOf(value);
	        if(index >= 0){
	            return false;
	        }
	    }
	    return true;
	}
	
	function addFamFax() {
		var sel = jQuery("#fam_doc option:selected");
		if (sel == null || sel=='') {
			return;
		}
		var fam = sel[0].getAttribute("faxNum");
		if(isInArray(myArray,fam)){
			myArray[myArray.length]=sel[0].getAttribute("faxNum");
			myNameArray[myNameArray.length]=sel.text();
			myFaxStr=myArray.join('|');
			myNameStr=myNameArray.join('|');
			_addFaxRecipient("Fam", sel.text(), fam);
		}
	}
	
	function addCcFax() {
		var otherDocFax = jQuery("input[name='otherDocFax']")[0];
		if (otherDocFax == null) {
			return;
		}
		var c = otherDocFax.value.trim();
		if(isInArray(myArray,c)){
			myArray[myArray.length]=otherDocFax.value.trim();
			myNameArray[myNameArray.length]=jQuery("input[name='clDoctor']")[0].value;
			myFaxStr=myArray.join('|');
			myNameStr=myNameArray.join('|');
			_addFaxRecipient("CC", jQuery("input[name='clDoctor']")[0].value, c);
		}
	}
	
	function _addFaxRecipient(type, referalName, faxNum) {
		if (faxNum == null || faxNum.length==0) {
			return;
		}
		 var obj = jQuery("#faxRecipients").find("a[type='" + type + "']");
		if (obj != null) {
			removeRecipient(obj[0]);
		} 
		var remove = "<a type='"+type+"' href='javascript:void(0);' onclick='removeRecipient(this)'>remove</a>";
		var html = "<li>" + referalName + "&nbsp;&nbsp;<b>Fax:</b>"+faxNum+ " " +remove+ "<input type='hidden' name='faxRecipients"+type+"' value='"+faxNum+"'></input><input type='hidden' name='faxRecipientsName" + type + "' value='" + referalName+"'></input></li>";
		
		jQuery("#faxRecipients").append(html);
	}
	
	function removeRecipient(el) {
		var el = jQuery(el);
		if (el) { 
				var mytext = jQuery(el).parent().text();
				if(mytext != undefined && mytext != null && mytext.trim().length>0){
					var a = mytext.split("remove");
					mytext = a.join(' ');
					a = mytext.split("Fax:");
					var mname = a[0].trim();
					var mfax = a[1].trim();
					if(!isInArray(myArray,mfax)){
						removeByValue(myArray,mfax);
					}
					if(!isInArray(myNameArray,mname)){
						removeByValue(myNameArray,mname);
					}
				}
			el.parent().remove(); 
		}
		else { 
			alert("Unable to remove recipient."); 
		}
	}
	function removeByValue(arr, val) {
		  for(var i=0; i<arr.length; i++) {
		    if(arr[i] == val) {
		      arr.splice(i, 1);
		      break;
		    }
		  }
		}
	</script>
</body>
</html:html>
