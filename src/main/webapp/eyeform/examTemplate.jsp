<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<html lang="en">
<head>
    <title>Eyeform Exam Template</title>
<%@ include file="/taglibs.jsp"%>
<%@ page import="java.util.*"%>
<%@ page import="org.oscarehr.common.model.Provider" %>
<%@ page import="org.oscarehr.eyeform.model.ExamTemplateItem" %>

<style type="text/css">
.boldRow {
	color:red;
}
.commonRow{
	color:black;
}
span.h5 {
  margin-top: 1px;
  border-bottom: 1px solid #000;
  width: 90%;
  font-weight: bold;
  list-style-type: none;
  padding: 2px 2px 2px 2px;
  color: black;
  background-color: #69c;
  font-family: Trebuchet MS, Lucida Sans Unicode, Arial, sans-serif;
  font-size: 10pt;
  text-decoration: none;
  display: block;
  clear: both;
  white-space: nowrap;
}

.exam{
	padding: 0px;
	border-spacing: 0px;
	margin: 0px;
	border: 0px;
}

.exam td{
	text-align: center;
	vertical-align:middle;
	padding:1px !important;
	margin:0px;
	border: 0px;
	font-size: 9px;
}

.label{
	text-align: left;
}
</style>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<%
Provider provider = (Provider)request.getAttribute("provider");
String providerNo = request.getParameter("providerNo");
String examTemplate_providerNo = (String)request.getAttribute("examTemplate_providerNo");
if(null == examTemplate_providerNo){
	examTemplate_providerNo = request.getParameter("examTemplate_providerNo");
}
if(null == examTemplate_providerNo){
	examTemplate_providerNo = "";
}
List<ExamTemplateItem> examTemplateItems = (List<ExamTemplateItem>)request.getAttribute("examTemplateItem");
%>
<script type="text/javascript">
function loadDefaultSiteAndProvider(){
	var macroProviderNo = $("#macroProvider_No").val();
	$("#macroProvider_No").val(macroProviderNo);
	getAllProviders();
}

function getAllProviders(){
	$.ajax({
		url : "../eyeform/Macro.do?method=getAllProviders",
		async: false,
		success : function(data) {
			var json=eval(data);
			var html = "";
			
			for (var i in json) {
				if($("#examTemplate_providerNo").val()==json[i].providerNo){
					html += '<option selected value="'+ json[i].providerNo+'">'+ json[i].fullName+'</option>';
				} else {
					html += '<option value="'+ json[i].providerNo+'">'+ json[i].fullName+'</option>';
				} 
            }
			
			if($("#examTemplate_providerNo").val()==""){
				html = '<option selected value=""></option>'+html;
			}else{
				html = '<option value=""></option>'+html;
			}
			$("#examTemplateProviderNo").html(html);
		}
	});
}

function selectProvider(){
	$("#examTemplate_providerNo").val($("#examTemplateProviderNo").val());
}

function loadMeasurementValue(){
	<%
	if(null != examTemplateItems && examTemplateItems.size() > 0){
		for(int i = 0;i < examTemplateItems.size();i ++){
	%>
		setfieldvalue('<%=examTemplateItems.get(i).getType()%>', '<%=examTemplateItems.get(i).getDataField()%>');
	<%
		}
	}
	%>
}

function setfieldvalue(name,value) {
	jQuery("input[measurement='"+name+"']").each(function() {
		jQuery(this).val(decodeURIComponent(value));
	});
}

function getfieldvalue(name) {
	var val = undefined;
	jQuery("input[measurement='"+name+"']").each(function() {
		val = jQuery(this).val();
	});
	return val;
}

function setAnterior_od(){
	setfieldvalue("a_rk","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
	setfieldvalue("a_rconj","<bean:message key="oscarEncounter.eyeExam.white"/>");
	setfieldvalue("a_rac","<bean:message key="oscarEncounter.eyeExam.deepAndQuite"/>");
	//setfieldvalue("a_rangle_1","");
	//setfieldvalue("a_rangle_2","");
	
	//setfieldvalue("a_rangle_3","<bean:message key="oscarEncounter.eyeExam.open"/>");	
	setfieldvalue("a_rangle_3","<bean:message key="oscarEncounter.eyeExam.open"/>");
	
	//setfieldvalue("a_rangle_4","");
	//setfieldvalue("a_rangle_5","");
	setfieldvalue("a_riris","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("a_rlens","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
}

function clearAnterior_od(){
	setfieldvalue("a_rk","");
	setfieldvalue("a_rconj","");
	setfieldvalue("a_rac","");
	setfieldvalue("a_rangle_1","");
	setfieldvalue("a_rangle_2","");
	setfieldvalue("a_rangle_3","");
	setfieldvalue("a_rangle_4","");
	setfieldvalue("a_rangle_5","");
	setfieldvalue("a_riris","");
	setfieldvalue("a_rlens","");
}

function setAnterior_os(){
	setfieldvalue("a_lk","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
	setfieldvalue("a_lconj","<bean:message key="oscarEncounter.eyeExam.white"/>");
	setfieldvalue("a_lac","<bean:message key="oscarEncounter.eyeExam.deepAndQuite"/>");
	//setfieldvalue("a_langle_1","");
	//setfieldvalue("a_langle_2","");
	
	//setfieldvalue("a_langle_3","<bean:message key="oscarEncounter.eyeExam.open"/>");
	setfieldvalue("a_langle_3","<bean:message key="oscarEncounter.eyeExam.open"/>");
	
	//setfieldvalue("a_langle_4","");
	//setfieldvalue("a_langle_5","");
	setfieldvalue("a_liris","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("a_llens","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
}

function clearAnterior_os(){
	setfieldvalue("a_lk","");
	setfieldvalue("a_lconj","");
	setfieldvalue("a_lac","");
	setfieldvalue("a_langle_1","");
	setfieldvalue("a_langle_2","");
	setfieldvalue("a_langle_3","");
	setfieldvalue("a_langle_4","");
	setfieldvalue("a_langle_5","");
	setfieldvalue("a_liris","");
	setfieldvalue("a_llens","");
}

function setPosterior_od(){
	setfieldvalue("p_rdisc","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	//setfieldvalue("p_rcd","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("p_rmac","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("p_rret","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("p_rvit","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
	//setfieldvalue("p_rcrt","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
}

function clearPosterior_od(){
	setfieldvalue("p_rdisc","");
	setfieldvalue("p_rcd","");
	setfieldvalue("p_rmac","");
	setfieldvalue("p_rret","");
	setfieldvalue("p_rvit","");
	setfieldvalue("p_rcrt","");
}

function setPosterior_os(){
	setfieldvalue("p_ldisc","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	//setfieldvalue("p_lcd","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("p_lmac","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("p_lret","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("p_lvit","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
	//setfieldvalue("p_lcrt","<bean:message key="oscarEncounter.eyeExam.Clear"/>");
}

function clearPosterior_os(){
	setfieldvalue("p_ldisc","");
	setfieldvalue("p_lcd","");
	setfieldvalue("p_lmac","");
	setfieldvalue("p_lret","");
	setfieldvalue("p_lvit","");
	setfieldvalue("p_lcrt","");
}

function setDuc_od(){
	setfieldvalue("duc_rur","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_rul","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_rr","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_rl","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_rdr","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_rdl","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearDuc_od(){
	setfieldvalue("duc_rur","");
	setfieldvalue("duc_rul","");
	setfieldvalue("duc_rr","");
	setfieldvalue("duc_rl","");
	setfieldvalue("duc_rdr","");
	setfieldvalue("duc_rdl","");
}

function setDuc_os(){
	setfieldvalue("duc_lur","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_lul","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_lr","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_ll","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_ldr","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("duc_ldl","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearDuc_os(){
	setfieldvalue("duc_lur","");
	setfieldvalue("duc_lul","");
	setfieldvalue("duc_lr","");
	setfieldvalue("duc_ll","");
	setfieldvalue("duc_ldr","");
	setfieldvalue("duc_ldl","");
}

function setDuc_ou(){
	setfieldvalue("dip_ur","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dip_u","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dip_r","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dip_p","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dip_dr","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dip_d","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearDuc_ou(){
	setfieldvalue("dip_ur","");
	setfieldvalue("dip_u","");
	setfieldvalue("dip_r","");
	setfieldvalue("dip_p","");
	setfieldvalue("dip_dr","");
	setfieldvalue("dip_d","");
}

function setDeviation(){
	setfieldvalue("dev_u","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_near","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_r","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_p","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_l","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_plus3","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_rt","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_d","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_lt","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_far","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_npc","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_aoacc_od","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("dev_aoacc_os","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearDeviation(){
	setfieldvalue("dev_u","");
	setfieldvalue("dev_near","");
	setfieldvalue("dev_r","");
	setfieldvalue("dev_p","");
	setfieldvalue("dev_l","");
	setfieldvalue("dev_plus3","");
	setfieldvalue("dev_rt","");
	setfieldvalue("dev_d","");
	setfieldvalue("dev_lt","");
	setfieldvalue("dev_far","");
	setfieldvalue("dev_npc","");
	setfieldvalue("dev_aoacc_od","");
	setfieldvalue("dev_aoacc_os","");
}

function setExternal_od(){
	setfieldvalue("ext_rface","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rretro","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rhertel","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearExternal_od(){
	setfieldvalue("ext_rface","");
	setfieldvalue("ext_rretro","");
	setfieldvalue("ext_rhertel","");
}

function setExternal_os(){
	setfieldvalue("ext_lface","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_lretro","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_lhertel","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearExternal_os(){
	setfieldvalue("ext_lface","");
	setfieldvalue("ext_lretro","");
	setfieldvalue("ext_lhertel","");
}

function setEyelid_od(){
	setfieldvalue("ext_rul","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rll","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rlake","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rirrig","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rpunc","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rnld","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_rdye","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearEyelid_od(){
	setfieldvalue("ext_rul","");
	setfieldvalue("ext_rll","");
	setfieldvalue("ext_rlake","");
	setfieldvalue("ext_rirrig","");
	setfieldvalue("ext_rpunc","");
	setfieldvalue("ext_rnld","");
	setfieldvalue("ext_rdye","");
}

function setEyelid_os(){
	setfieldvalue("ext_lul","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_lll","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_llake","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_lirrig","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_lpunc","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_lnld","<bean:message key="oscarEncounter.eyeExam.normal"/>");
	setfieldvalue("ext_ldye","<bean:message key="oscarEncounter.eyeExam.normal"/>");
}

function clearEyelid_os(){
	setfieldvalue("ext_lul","");
	setfieldvalue("ext_lll","");
	setfieldvalue("ext_llake","");
	setfieldvalue("ext_lirrig","");
	setfieldvalue("ext_lpunc","");
	setfieldvalue("ext_lnld","");
	setfieldvalue("ext_ldye","");
}

function saveExamTemplate(){
	var postData = "";
	jQuery("input[measurement]").each(function() {
		if(postData.length > 0) {
			postData += "&";
		}
		
		var name = jQuery(this).attr("measurement");
		var value = jQuery(this).val();
		var data = name + "=" + encodeURIComponent(value);
		postData += data;
	});
	$("#measurementList").val(postData);
}
</script>
</head>
<body onload="loadMeasurementValue();loadDefaultSiteAndProvider()">
	<html:form action="/eyeform/examTemplate.do" onsubmit="saveExamTemplate(); return true;">
	<input name="method" value="save" type="hidden">
	<html:hidden property="examTemplate.id"/>
	<input name="providerNo" value="<%=providerNo %>" type="hidden">
	<input name="examTemplate_providerNo" id="examTemplate_providerNo" value="<%=examTemplate_providerNo %>" type="hidden">
	<input name="measurementList" id="measurementList" value="" type="hidden">
	
	<table style="border: 0px none;">
		<tbody>
			<tr>
				<td>
					<input value="Save" type="submit">
					<input value="Cancel" onclick="this.form.method.value='list'" type="submit">
					<input value="Close" onclick="window.opener.location.reload();window.close();return false;" type="button">
				</td>
			</tr>
		</tbody>
	</table>
	<fieldset>
		<legend>Exam Template Details</legend>
		<table style="border: 0px none;">
			<tbody>
			<tr>
				<td width="150">Exam Template Name</td>
				<td><html:text property="examTemplate.examTemplateName"/></td>
			</tr>
			<tr>
				<td width="150">Exam Template Provider</td>
				<td><select id="examTemplateProviderNo" onchange="selectProvider()"></select></td>
			</tr>
			</tbody>
		</table>
		
		<fieldset>
			<legend>Other &amp; Exam</legend>
			<table style="border: 0px none;">
				<tbody>
					<tr style="text-align: center">
						<td width="8%"></td>
						<td width="24%" colspan="3">OD</td>
						<td width="4%"></td>
						<td width="24%" colspan="3">OS</td>
						<td width="8%"></td>
						<td width="32%" colspan="4">OU</td>
						<td width="4%"></td>
					</tr>
					<tr>
						<td class="label">Colour vision</td>
						<td colspan="3"><input type="text" tabindex="1" maxlength="50" style="width:98%" measurement="o_rcolour" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td width="4%"></td>
						<td colspan="3"><input type="text" tabindex="2" maxlength="50" style="width:98%" measurement="o_lcolour" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td></td>
						<td colspan="4">Maddox &nbsp; &nbsp;<input type="text" tabindex="3" maxlength="50" style="width:71%" measurement="o_mad" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td></td>
					</tr>
					<tr>
						<td class="label">Pupil</td>
						<td colspan="3"><input type="text" tabindex="4" maxlength="50" style="width:98%" measurement="o_rpupil" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td width="4%"></td>
						<td colspan="3"><input type="text" tabindex="5" maxlength="50" style="width:98%" measurement="o_lpupil" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td ></td>
						<td colspan="4">Bagolini &nbsp; &nbsp;<input type="text" tabindex="6" maxlength="50" style="width:71%" measurement="o_bag" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td ></td>
					</tr>
					<tr>
						<td class="label">Amsler grid</td>
						<td colspan="3"><input type="text" tabindex="7" maxlength="50" style="width:98%" maxlength="50" measurement="o_ramsler" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td width="4%"></td>
						<td colspan="3"><input type="text" tabindex="8" maxlength="50" style="width:98%" measurement="o_lamsler" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td ></td>
						<td colspan="4">W4D(dist)&nbsp;<input type="text" tabindex="9" maxlength="50" style="width:71%" maxlength="50" measurement="o_w4dd" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td ></td>
					</tr>
					<tr>
						<td class="label">PAM</td>
						<td colspan="3"><input type="text" tabindex="10" maxlength="50" style="width:98%" measurement="o_rpam" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td width="4%"></td>
						<td colspan="3"><input type="text" tabindex="11" maxlength="50" style="width:98%" measurement="o_lpam" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td ></td>
						<td colspan="4">W4D(near)<input type="text" tabindex="12" maxlength="50" style="width:71%" measurement="o_w4dn" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td ></td>
					</tr>
					<tr>
						<td class="label">Confrontation</td>
						<td colspan="3"><input type="text" tabindex="13" maxlength="50" style="width:98%" measurement="o_rconf" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td width="4%"></td>
						<td colspan="3"><input type="text" tabindex="14" maxlength="50" style="width:98%" measurement="o_lconf" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td ></td>
						<td colspan="4"></td>
						<td ></td>
					</tr>
				</tbody>
			</table>
		</fieldset>
		<fieldset>
			<legend>Anterior &amp; Segment</legend>
			<table style="border: 0px none;text-align: center">
				<tbody>
					<tr style="text-align: center">
		        		<td width="8%"></td>
		        		<td width="46%">OD
		        			<a href="javascript:void(0)" tabindex="15" onclick="setAnterior_od();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
		            		<a href="javascript:void(0)" tabindex="16" onclick="clearAnterior_od();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
		        		</td>
		        		<td width="46%">OS
		        			<a href="javascript:void(0)" tabindex="17" onclick="setAnterior_os();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
		            		<a href="javascript:void(0)" tabindex="18" onclick="clearAnterior_os();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
		        		</td>
		        	</tr>
		        	<tr>
		        		<td class="label">Cornea</td>
		        		<td><input type="text" tabindex="19" style="width:93%" measurement="a_rk" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="20" style="width:93%" measurement="a_lk" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">Conj/Sclera</td>
		        		<td><input type="text" tabindex="21" style="width:93%" measurement="a_rconj" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="22" style="width:93%" measurement="a_lconj" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">AC</td>
		        		<td><input type="text" tabindex="23" style="width:93%" measurement="a_rac" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="24" style="width:93%" measurement="a_lac" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
						<td class="label">Angle</td>
						<td>
		        			<table class="exam" style="width:100%">
		        				<tr>
		        					<td width="100%" align="center"><input type="text" style="width:30%" tabindex="25" measurement="a_rangle_1" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        				</tr>
		        				<tr>
		        					<td style="text-align:center">
			        					<input style="width:30%" type="text" tabindex="26" measurement="a_rangle_4" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
		        						<input style="width:30%" type="text" tabindex="27" measurement="a_rangle_3" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
										<input style="width:30%" type="text" tabindex="28" measurement="a_rangle_2" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
		        					</td>
		        				</tr>
		        				<tr>
		        					<td><input type="text" style="width:30%" tabindex="29" measurement="a_rangle_5" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        				</tr>
		        			</table>
		        		</td>
		        		
		        		<td>
		        			<table class="exam" style="width:100%">
		        				<tr>
		        					<td width="100%" align="center"><input style="width:30%" type="text" tabindex="30" measurement="a_langle_1" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        				</tr>
		
		        				<tr>
		        					<td style="text-align:center">
		        						<input style="width:30%" type="text" tabindex="31" measurement="a_langle_4" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
										<input style="width:30%" type="text" tabindex="32" measurement="a_langle_3" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
										<input style="width:30%" type="text" tabindex="33" measurement="a_langle_2" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
		        					</td>
		        				</tr>
		
		        				<tr>
		        					<td><input type="text" style="width:30%" tabindex="34" measurement="a_langle_5" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        				</tr>
		        			</table>
		        		</td>
		        	</tr>
		        	<tr>
		        		<td class="label">Iris</td>
		        		<td><input type="text" tabindex="35" style="width:93%" measurement="a_riris" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="36" style="width:93%" measurement="a_liris" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">Lens</td>
		        		<td><input type="text" tabindex="37" style="width:93%" measurement="a_rlens" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="38" style="width:93%" measurement="a_llens" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
				</tbody>
			</table>
		</fieldset>
		<fieldset>
			<legend>Posterior &amp; Segment</legend>
			<table style="border: 0px none;">
				<tbody>
					<tr  style="text-align: center">
		        		<td width="8%"></td>
		        		<td width="46%">OD
		        			<a href="javascript:void(0)" tabindex="39" onclick="setPosterior_od();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
		            		<a href="javascript:void(0)" tabindex="40" onclick="clearPosterior_od();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
		        		</td>
		        		<td width="46%">OS
		        			<a href="javascript:void(0)" tabindex="41" onclick="setPosterior_os();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
		            		<a href="javascript:void(0)" tabindex="42" onclick="clearPosterior_os();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
		        		</td>
		        	</tr>
		        	<tr>
		        		<td class="label">Disc</td>
		        		<td><input type="text" tabindex="43" style="width:93%" measurement="p_rdisc" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="44" style="width:93%" measurement="p_ldisc" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">C/D ratio</td>
		        		<td><input type="text" tabindex="45" style="width:93%" measurement="p_rcd" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="46" style="width:93%" measurement="p_lcd" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">Macula</td>
		        		<td><input type="text" tabindex="47" style="width:93%" measurement="p_rmac" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="48" style="width:93%" measurement="p_lmac" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">Retina</td>
		        		<td><input type="text" tabindex="49" style="width:93%" measurement="p_rret" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="50" style="width:93%" measurement="p_lret" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">Vitreous</td>
		        		<td><input type="text" tabindex="51" style="width:93%" measurement="p_rvit" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="52" style="width:93%" measurement="p_lvit" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">CRT</td>
		        		<td><input type="text" tabindex="53" style="width:93%" measurement="p_rcrt" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="54" style="width:93%" measurement="p_lcrt" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
				</tbody>
			</table>
		</fieldset>
		<fieldset>
			<legend>Duction/Diplopia</legend>
			<table style="border: 0px none;">
				<tbody>
					<tr  style="text-align: center">
						<td width="4%"></td>
			        	<td colspan="2" width="25%">OD
			        		<a href="javascript:void(0)" tabindex="55" onclick="setDuc_od();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
			            	<a href="javascript:void(0)" tabindex="56" onclick="clearDuc_od();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
			        	</td>
						<td width="4%"></td>
			        	<td colspan="2" width="25%">OS
			        		<a href="javascript:void(0)" tabindex="57" onclick="setDuc_os();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
			            	<a href="javascript:void(0)" tabindex="58" onclick="clearDuc_os();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
			        	</td>
						<td ></td>
			        	<td colspan="2" width="25%">OU
			        		<a href="javascript:void(0)" tabindex="59" onclick="setDuc_ou();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
			            	<a href="javascript:void(0)" tabindex="60" onclick="clearDuc_ou();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
			        	</td>
		        	</tr>
		        	<tr>
						<td></td>
		        		<td><input type="text" style="width:90%" maxlength="10" tabindex="61" measurement="duc_rur" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" style="width:90%" maxlength="10" tabindex="62" measurement="duc_rul" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td width="4%"></td>
		        		<td><input type="text" style="width:90%" maxlength="10" tabindex="63" measurement="duc_lur" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" style="width:90%" maxlength="10" tabindex="64" measurement="duc_lul" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
						<td></td>
		        		<td><input type="text" style="width:90%" maxlength="10" tabindex="65" measurement="dip_ur" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" style="width:90%" maxlength="10" tabindex="66" measurement="dip_u" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>       	
		        	</tr>
		        	<tr>
						<td></td>
		        		<td><input type="text" style="width:90%" tabindex="67" maxlength="10" measurement="duc_rr" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" style="width:90%" tabindex="68" maxlength="10" measurement="duc_rl" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td width="4%"></td>
		        		<td><input type="text" style="width:90%" tabindex="69" maxlength="10" measurement="duc_lr" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" style="width:90%" tabindex="70" maxlength="10" measurement="duc_ll" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td></td>
		        		<td><input type="text" style="width:90%" tabindex="71" maxlength="10" measurement="dip_r" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" style="width:90%" tabindex="72" maxlength="10" measurement="dip_p" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>       	
		        	</tr>
		        	<tr>
						<td></td>
		        		<td><input type="text" style="width:90%" tabindex="73" maxlength="10" measurement="duc_rdr" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" style="width:90%" tabindex="74" maxlength="10" measurement="duc_rdl" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td width="4%"></td>
		        		<td><input type="text" style="width:90%" tabindex="75" maxlength="10" measurement="duc_ldr" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" style="width:90%" tabindex="76" maxlength="10" measurement="duc_ldl" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td></td>
		        		<td><input type="text" style="width:90%" tabindex="77" maxlength="10" measurement="dip_dr" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" style="width:90%" tabindex="78" maxlength="10" measurement="dip_d" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>       	
		        	</tr>
				</tbody>
			</table>
		</fieldset>
		<fieldset>
			<legend>Deviation &amp; Measurement</legend>
			<table style="border: 0px none;">
				<tbody>
					<tr>
						<td align="center" width="11%">
							<a href="javascript:void(0)" tabindex="79" onclick="setDeviation();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
			            	<a href="javascript:void(0)" tabindex="80" onclick="clearDeviation();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
						</td>
						<td width="30%">
							<table>
								<tr>
									<td width="30%"></td>
									<td width="30%"><input type="text" tabindex="81" maxlength="30" measurement="dev_u" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
									<td width="30%"></td>
								</tr>
								<tr>
									<td width="30%"><input type="text" tabindex="82" maxlength="30" measurement="dev_r" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
									<td width="30%"><input type="text" tabindex="83" maxlength="30" measurement="dev_p" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
									<td width="30%"><input type="text" tabindex="84" maxlength="30" measurement="dev_l" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
								</tr>
								<tr>
									<td width="30%"><input type="text" tabindex="85" maxlength="30" measurement="dev_rt" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
									<td width="30%"><input type="text" tabindex="86" maxlength="30" measurement="dev_d" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
									<td width="30%"><input type="text" tabindex="87" maxlength="30" measurement="dev_lt" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
								</tr>
							</table>
						</td>
						<td width="20%">
							<table>
								<tr>
									<td class="label">Near</td>
									<td><input type="text" tabindex="88" maxlength="30" measurement="dev_near" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
								</tr>
								<tr>
									<td class="label">+3.00D</td>
									<td><input type="text" tabindex="89" maxlength="30" measurement="dev_plus3" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
								</tr>
								<tr>
									<td class="label">Far Distance</td>
									<td><input type="text" tabindex="90" maxlength="30" measurement="dev_far" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 80px"/></td>
								</tr>
							</table>
						</td>
						<td width="25%">
							<table>
								<tr>
									<td class="label">NPC</td>
									<td><input type="text" tabindex="248" maxlength="30" measurement="dev_npc" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 140px"/></td>
								</tr>
								<tr>
									<td class="label">A.O.ACC</td>
									<td>
										<label>OD:</label>
										<input type="text"  tabindex="248" maxlength="30" measurement="dev_aoacc_od" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 40px"/>
										<label>OS:</label>
										<input type="text" tabindex="248" maxlength="30" measurement="dev_aoacc_os" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);" style="width: 40px"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</fieldset>
		<fieldset>
			<legend>External/Orbit</legend>
			<table style="border: 0px none;">
				<tbody>
					<tr  style="text-align: center">
		        		<td width="8%"></td>
		        		<td width="46%">OD
		        			<a href="javascript:void(0)" tabindex="91" onclick="setExternal_od();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
		            		<a href="javascript:void(0)" tabindex="92" onclick="clearExternal_od();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
		        		</td>
		        		<td width="46%">OS
		        			<a href="javascript:void(0)" tabindex="93" onclick="setExternal_os();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
		            		<a href="javascript:void(0)" tabindex="94" onclick="clearExternal_os();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
		        		</td>
		        	</tr>
		        	<tr>
		        		<td class="label">Face</td>
		        		<td><input type="text" tabindex="95" style="width:93%" measurement="ext_rface" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="96" style="width:93%" measurement="ext_lface" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">Retropulsion</td>
		        		<td><input type="text" tabindex="97" style="width:93%" measurement="ext_rretro" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="98" style="width:93%" measurement="ext_lretro" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">Hertel</td>
		        		<td><input type="text" tabindex="99" style="width:93%" measurement="ext_rhertel" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="100" style="width:93%" measurement="ext_lhertel" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
				</tbody>
			</table>
		</fieldset>
		<fieldset>
			<legend>Eyelid/NLD</legend>
			<table style="border: 0px none;">
				<tbody>
					<tr style="text-align: center">
		        		<td width="8%"></td>
		        		<td width="46%">OD
		        			<a href="javascript:void(0)" tabindex="101" onclick="setEyelid_od();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
		            		<a href="javascript:void(0)" tabindex="102" onclick="clearEyelid_od();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
		        		</td>
		        		<td width="46%">OS
		        			<a href="javascript:void(0)" tabindex="103" onclick="setEyelid_os();return false;">[<bean:message key="oscarEncounter.eyeExam.normal"/>]</a>
		            		<a href="javascript:void(0)" tabindex="104" onclick="clearEyelid_os();return false;">[<bean:message key="oscarEncounter.eyeExam.Clear"/>]</a>
		        		</td>
		        	</tr>
		        	<tr>
		        		<td class="label">Upper lid</td>
		        		<td><input type="text" tabindex="105" style="width:93%" measurement="ext_rul" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="106" style="width:93%" measurement="ext_lul" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">Lower lid</td>
		        		<td><input type="text" tabindex="107" style="width:93%" measurement="ext_rll" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="108" style="width:93%" measurement="ext_lll" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">Lac lake</td>
		        		<td><input type="text" tabindex="109" style="width:93%" measurement="ext_rlake" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="110" style="width:93%" measurement="ext_llake" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">Lac Irrig</td>
		        		<td><input type="text" tabindex="111" style="width:93%" measurement="ext_rirrig" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="112" style="width:93%" measurement="ext_lirrig" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">Punctum</td>
		        		<td><input type="text" tabindex="113" style="width:93%" measurement="ext_rpunc" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="114" style="width:93%" measurement="ext_lpunc" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">NLD</td>
		        		<td><input type="text" tabindex="115" style="width:93%" measurement="ext_rnld" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="116" style="width:93%" measurement="ext_lnld" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
		        	<tr>
		        		<td class="label">Dye Disapp</td>
		        		<td><input type="text" tabindex="117" style="width:93%" measurement="ext_rdye" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        		<td><input type="text" tabindex="118" style="width:93%" measurement="ext_ldye" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		        	</tr>
				</tbody>
			</table>
		</fieldset>
		<fieldset>
			<legend>Eyelid &amp; Measurement</legend>
			<table style="border: 0px none;">
				<tbody>
					<tr  style="text-align: center">
		     			<td width="12%"></td>
		     			<td colspan="2" align="center" width="44%">OD</td>
		     			<td colspan="2" align="center" width="44%">OS</td>
		     		</tr>
		     		<tr style="text-align: center">
		     			<td></td>
		     			<td>MRD</td>
		     			<td>ISS</td>
		     			<td>MRD</td>
		     			<td>ISS</td>
		     		</tr>
		     		<tr>
		     			<td class="label">Lid margins</td>
		     			<td colspan="2" style="text-align: center">
		     				<input type="text" tabindex="119" maxlength="50" style="width:45%" measurement="lid_rmrd" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>&nbsp&nbsp
							<input type="text" tabindex="120" maxlength="50" style="width:45%" measurement="lid_riss" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
						</td>
		     			<td colspan="2" style="text-align: center">
		     				<input type="text" tabindex="121" maxlength="50" style="width:45%" measurement="lid_lmrd" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>&nbsp&nbsp
							<input type="text" tabindex="122" maxlength="50" style="width:45%" measurement="lid_liss" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/>
						</td>
		     		</tr>
		     		<tr>
		     			<td class="label">Levator Fn</td>
		     			<td colspan="2" align="center"><input type="text" style="width:94%" tabindex="123" maxlength="50" measurement="lid_rlev" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     			<td colspan="2" align="center"><input type="text" style="width:94%" tabindex="124" maxlength="50" measurement="lid_llev" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     		</tr>
		     		<tr>
		     			<td class="label">Lag</td>
		     			<td colspan="2" align="center"><input type="text" style="width:94%" tabindex="125" maxlength="50" measurement="lid_rlag" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     			<td colspan="2" align="center"><input type="text" style="width:94%" tabindex="126" maxlength="50" measurement="lid_llag" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     		</tr>
		     		<tr>
		     			<td class="label">Blink</td>
		     			<td colspan="2" align="center"><input type="text" style="width:94%" tabindex="127" maxlength="50" measurement="lid_rblink" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     			<td colspan="2" align="center"><input type="text" style="width:94%" tabindex="128" maxlength="50" measurement="lid_lblink" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     		</tr>
		     		<tr>
		     			<td class="label">CN VII</td>
		     			<td colspan="2" align="center"><input type="text" style="width:94%" tabindex="129" maxlength="50" measurement="lid_rcn7" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     			<td colspan="2" align="center"><input type="text" style="width:94%" tabindex="130" maxlength="50" measurement="lid_lcn7" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     		</tr>
		     		<tr>
		     			<td class="label">Bells</td>
		     			<td colspan="2" align="center"><input type="text" style="width:94%" tabindex="131" maxlength="50" measurement="lid_rbell" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     			<td colspan="2" align="center"><input type="text" style="width:94%" tabindex="132" maxlength="50" measurement="lid_lbell" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     		</tr>
		     		<tr>
		     			<td class="label">Schirmer</td>
		     			<td colspan="2" align="center"><input type="text" style="width:94%" tabindex="133" maxlength="50" measurement="lid_rschirm" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     			<td colspan="2" align="center"><input type="text" style="width:94%" tabindex="134" maxlength="50" measurement="lid_lschirm" onchange="syncFields(this)" class="examfieldgrey" onfocus="whiteField(this);"/></td>
		     		</tr>
				</tbody>
			</table>
		</fieldset>
	</fieldset>
	</html:form>
</body>
</html>