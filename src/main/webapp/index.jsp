<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.oscarSecurity.CookieSecurity" %>
<%@ page import="org.oscarehr.common.service.AcceptableUseAgreementManager" %>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="oscar.util.OneIDUtil" %>
<%@ page import="org.oscarehr.util.MiscUtils" %>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="org.oscarehr.util.SpringUtils"%>
<%@ page import="org.oscarehr.managers.OktaManager" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ page import="java.net.URI" %>
<%@ page import="java.net.URISyntaxException" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>
<%
	OscarProperties oscarProperties = OscarProperties.getInstance();
	OktaManager oktaManager = SpringUtils.getBean(OktaManager.class);

	// Clear old cookies
	Cookie prvCookie = new Cookie(CookieSecurity.providerCookie, "");
	prvCookie.setPath("/");
	response.addCookie(prvCookie);

	//Gets the redirect URL
	String oscarUrl = OneIDUtil.getLoginRedirectUrl(request);
	String econsultUrl = OscarProperties.getEconsultBackendUrl();

	String kaiemrLandingPage = OscarProperties.getKaiemrDeployedContextUrl(request);

	boolean enableOktaAuth = oktaManager.isOktaEnabled();

	boolean enableKaiEmr = Boolean.parseBoolean(oscarProperties.getProperty("enable_kai_emr", "false"));
	boolean loginFailed = request.getParameter("login")!=null && request.getParameter("login").equals("failed");

	Logger logger = MiscUtils.getLogger();
	String loginParam = request.getParameter("login");
	logger.debug("________________________________________________________________");
	logger.debug(" - enableOktaAuth=" +enableOktaAuth +",  enableKaiEmr=" +enableKaiEmr);
	logger.debug(" - servlet path= " + request.getServletPath());
	logger.debug(" - request URL= " + request.getRequestURL());
	logger.debug(" - request URI= " + request.getRequestURI());
	logger.debug(" - request login param (?login=)= " + loginParam);

	// Do not know if it is kai_username until they enter their login creds
	if (enableKaiEmr && enableOktaAuth && !(loginParam!=null && loginParam.equals("use-classic"))) {
		logger.debug(" ===>  Redirecting to OSCAR Pro Okta Login page at " + kaiemrLandingPage);
		response.sendRedirect(kaiemrLandingPage);
	}

	String oneIdKey = request.getParameter("nameId") == null ? "" : request.getParameter("nameId");

	String loginMessage = OneIDUtil.getLoginMessage(request);
	if (loginFailed) {
		loginMessage = "Login failed. Username or Password is incorrect." + (!loginMessage.isEmpty() ? "<br><br>" : "") + loginMessage;
	}
	boolean marketingWidgetEnabled =
			SystemPreferencesUtils.isReadBooleanPreferenceWithDefault("marketingWidgetEnabled", true);
	String marketingWidgetUrl = SystemPreferencesUtils
			.getPreferenceValueByName("marketingWidgetUrl", "https://loginwidget.oscarpro.ca/");

	try {
		URI marketingUri = new URI(marketingWidgetUrl);
		String marketingWidgetQuery = marketingUri.getQuery();
		if (marketingWidgetQuery == null) {
			marketingWidgetQuery = "billregion=" + oscarProperties.getProperty("billregion");
		} else {
			marketingWidgetQuery += "&" + "billregion=" + oscarProperties.getProperty("billregion");
		}
		marketingWidgetUrl = new URI(marketingUri.getScheme(), marketingUri.getAuthority(),
				marketingUri.getPath(), marketingWidgetQuery, marketingUri.getFragment()).toString();
	} catch (URISyntaxException e) {
		e.printStackTrace();
	}

	if (marketingWidgetEnabled) {
		try {
			InputStream marketingInputStream = new URL(marketingWidgetUrl).openStream();
			if (marketingInputStream.read() < 1) {
				marketingWidgetEnabled = false;
			}
			marketingInputStream.close();
		} catch (Exception exception) {
			marketingWidgetEnabled = false;
		}
	}
	boolean oneIdLoginEnabled =
			SystemPreferencesUtils.isReadBooleanPreferenceWithDefault("oneid.sso.enabled", false);
%>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<title>OSCAR Professional</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="<%=request.getContextPath()%>/well/css/login.css">
	<script type="text/javascript">
		function enhancedOrClassic(choice) {
			document.getElementById("loginType").value = choice;
		}

		function addStartTime() {
			document.getElementById("oneIdLogin").href += (Math.round(new Date().getTime() / 1000).toString());
		}

		async function urlExists(url) {
			try {
				await fetch(url, {mode: "no-cors"});
				return true;
			} catch (err) {
				return false;
			}
		}

		function showHideMarketingWidget(widgetEnabled = true) {
			if (widgetEnabled) {
				const leftColumnHasMarketingWidget = document.getElementById('leftColumnHasWidget');
				const leftColumnNoMarketingWidget = document.getElementById('leftColumnNoWidget');
				const iFrameSrc = document.getElementById('iFrameWidget').src;
				urlExists(iFrameSrc).then(exists => {
					if (exists) {
						leftColumnHasMarketingWidget.style.display = 'block';
						leftColumnNoMarketingWidget.style.display = 'none';
					} else {
						leftColumnHasMarketingWidget.style.display = 'none';
						leftColumnNoMarketingWidget.style.display = 'block';
					}
				});
			}
		}

		function setLoginType() {
			document.getElementById("")
		}
	</script>
</head>
<body onload="showHideMarketingWidget(<%=marketingWidgetEnabled%>)">
<div class="login-page">
	<div class="login-page-content">
		<!-- Marketing Widget START-->
		<div class="left" id="leftColumnHasWidget" style="padding: 0 !important; <%=!marketingWidgetEnabled ? "display: none;" : ""%>">
			<iframe style="height: 100% !important; width: 100% !important;" id="iFrameWidget"
					src="<%=marketingWidgetUrl%>"></iframe>
		</div>
		<div class="left" id="leftColumnNoWidget" <%=marketingWidgetEnabled ? "style=\"display: none;\"" : ""%>>
			<img src="images/icons/login/Oval-2.svg"  class="background-oval">

			<div class="oscar-logo">
				<img src="images/icons/login/Oscar_ProLogo_white.svg" alt="">
			</div>

			<div class="cta-container">
				<div class="whats-new">WHAT'S NEW</div>

				<h2 class="oscar-headline">Enhance OSCAR Pro with connected apps and services!</h2>
				<div class="oscar-description">WELL's apps.health marketplace provides clinics with products and services to streamline clinic workflow and boost productivity.</div>

				<button class="button is-white is-outlined learn-more-button" onclick="window.location.href = 'https://apps.health/?utm_source=oscarpro&utm_medium=login-banner&utm_campaign=A.H-oscarpro-login'">Learn more</button>
			</div>

			<div class="bottom-container" >
				<div class="apps-health-logo">
					<img src="images/icons/login/apps-health.svg" alt="">
				</div>


				<div class="well-emr-logo">
					<img src="images/icons/login/WELLEMRGroup-white.svg" alt="">
				</div>



			</div>
		</div>
		<!-- Marketing Widget END-->
		<div class="right">
			<div class="right-container">
				<html:form action="login" styleClass="login-form">
					<h3 class="title">OSCAR Pro Login</h3>

					<% if (!loginMessage.isEmpty()) { %>
					<div class="login-failed-container">
						<p class="login-failed-text"><%= loginMessage %></p>
					</div>
					<% } %>
					<% if (oneIdKey != null && !oneIdKey.isEmpty()) { %>
						<div class="login-failed-container">
							<p class="login-failed-text">
								ONE ID account is not linked to an provider. Please login to link.
							</p>
						</div>
					<% } %>
					<br/>
					<div class="control">
						<input type="text" class="input" name="username" id="username" placeholder="Username:" maxlength="15" autocomplete="off" autofocus>
					</div>
					<div class="control">
						<input type="password" class="input" name="password" id="password" placeholder="Password:" maxlength="32">
					</div>
					<div class="control">
						<input type="password" class="input" name="pin" id="pin" placeholder="PIN:" maxlength="15">
					</div>
					<div class="WAN-text">for external Wide Area Network access</div>
					<input type="hidden" id="loginType" name="loginType" value=""/>
					<input type="hidden" id="email" name="email" value="<%=request.getParameter("email") != null ? request.getParameter("email") : ""%>"/>
					<input type=hidden name='propname' value='<bean:message key="loginApplication.propertyFile"/>' />
					<input type="hidden" name="nameId" value="<%=oneIdKey%>"/>


					<div class="buttons">
						<button class="button is-green btn-primary" onclick="">Sign In</button>
					</div>
					<p>OSCAR McMaster Professional Edition</p>
					<span>Build: <%=OscarProperties.getBuildTag()%> </span>
				</html:form>

				<oscar:oscarPropertiesCheck property="enable_econsult" value="true" defaultVal="false">
					<div class="login-with-one-id">
						<div class="login-with-text">Or Login With</div>
						<a href="<%=econsultUrl%>/SAML2/login?oscarReturnURL=<%=URLEncoder.encode(oscarUrl, "UTF-8") + "?loginStart="%>" id="oneIdLogin" onclick="addStartTime()">
							<button class="one-id-login-button">
								<img src="images/icons/login/ONE-ID.png" alt="">
							</button>
						</a>
					</div>
				</oscar:oscarPropertiesCheck>
				<% if (oneIdLoginEnabled) { %>
					<div class="login-with-one-id">
						<div class="login-with-text">Or Login With</div>
						<a href="<%=OscarProperties.getOscarProBaseUrl()%>/#/one-id/login?forward=<%=request.getContextPath()%>/provider/providercontrol.jsp"
						   id="oneIdLoginOauth">
							<div class="btn btn-primary btn-block oneIDLogin">
								<button class="one-id-login-button">
									<img src="images/icons/login/ONE-ID.png" alt="">
								</button>
							</div>
						</a>
					</div>
				<% } %>


				<%if (AcceptableUseAgreementManager.hasAUA() || AcceptableUseAgreementManager.auaAlwaysShow()) { %>
					<div class="confidentiality-agreement content">
						<p><%=AcceptableUseAgreementManager.getAUAText()%></p>
					</div>
				<% } %>
			</div>
		</div>
	</div>
</div>
</body>
</html>