function addProviderToSite(providers, siteId, provider) {
    if (providers.find( p => p.providerNo === provider.providerNo ) == null) {
        providers.push(provider);
    }

    let site = sites.find( site => site.id === siteId );
    if (site != null) {
        site.providers.push(provider);
    }
}

function buildProviderList(providers, siteId, selectElementId){
    let assignedToEl = jQuery('#' + selectElementId);
    assignedToEl.empty();
    let theProviders = [];
    if (siteId === "all") {
        theProviders = providers;
        assignedToEl.append(jQuery("<option></option>")
            .text("All Providers")
            .attr("value", "all")
        );
    } else {
        let site = sites.find( site => site.id === siteId );
        theProviders = site.providers;
    }

    theProviders.sort((providerA, providerB) => providerA.name.localeCompare(providerB.name));
    theProviders.forEach(function(p) {
        let optionElement = jQuery("<option></option>").text(p.name).attr("value", p.providerNo);
        if (typeof providerSiteRecipientPreferenceMap != 'undefined' 
                && providerSiteRecipientPreferenceMap.get(siteId) === p.providerNo) {
            optionElement.attr('selected', 'selected');
        }
        assignedToEl.append(optionElement);
    });
}

function changeSite(sel, providers, defaultProviderId, selectElementId) {
    // if none is selected, add all providers to select element
    if (sel.value === 'none') {
        let assignedToEl = jQuery('#' + selectElementId);
        assignedToEl.empty();
        providers.forEach(function(p) {
            let optionEle = jQuery("<option></option>")
                .text(p.name)
                .attr("value", p.providerNo);
            if (defaultProviderId === p.providerNo) {
                optionEle.attr("selected", "selected");
            }
            assignedToEl.append(optionEle);
        });
    } else {
        buildProviderList(providers, sel.value, selectElementId);
    }
}