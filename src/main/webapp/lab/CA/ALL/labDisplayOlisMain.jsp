<%--
    Copyright (c) 2021 WELL EMR Group Inc.
    This software is made available under the terms of the
    GNU General Public License, Version 2, 1991 (GPLv2).
    License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>
<%@ page import="java.util.*,org.oscarehr.common.dao.PatientLabRoutingDao, org.oscarehr.util.SpringUtils, org.oscarehr.common.model.PatientLabRouting,oscar.oscarLab.ca.all.*,oscar.oscarLab.ca.all.parsers.*,oscar.log.*" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="org.oscarehr.common.dao.DemographicDao" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.olis.model.OlisSessionManager" %>
<%@ page import="org.oscarehr.olis.model.ProviderOlisSession" %>
<%@ page import="org.oscarehr.olis.model.OlisLabResultDisplay" %>
<%@ page import="org.oscarehr.common.dao.ReadLabDao" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="/WEB-INF/oscarProperties-tag.tld" prefix="oscarProperties"%>
<%@ taglib uri="/WEB-INF/indivo-tag.tld" prefix="indivo"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib prefix="csrf"
           uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%
      String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	  boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_lab" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../../../securityError.jsp?type=_lab");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}
%>

<%
	LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
	String segmentID = request.getParameter("segmentID");
String originalSegmentID = segmentID;
String providerNo = request.getParameter("providerNo");

boolean preview = oscar.Misc.getStr(request.getParameter("preview"), "").equals("true");

PatientLabRoutingDao plrDao = preview ? null : (PatientLabRoutingDao) SpringUtils.getBean("patientLabRoutingDao");
PatientLabRouting plr = preview ? null : plrDao.findDemographicByLabId(Integer.valueOf(segmentID));
String demographicID = preview || plr==null  || plr.getDemographicNo() == null ? "" : plr.getDemographicNo().toString();

	if (demographicID != null && !demographicID.equals("")) {
		LogAction.addLog((String) session.getAttribute("user"), LogConst.READ, LogConst.CON_HL7_LAB,
				segmentID, LoggedInInfo.obtainClientIpAddress(request), demographicID);
	} else {
		LogAction.addLog((String) session.getAttribute("user"), LogConst.READ, LogConst.CON_HL7_LAB,
				segmentID, LoggedInInfo.obtainClientIpAddress(request));
	}
boolean obgynShortcuts = SystemPreferencesUtils.isReadBooleanPreferenceWithDefault("show_obgyn_shortcuts", false) && oscar.util.StringUtils.filled(demographicID) && !"-1".equals(demographicID);
String billRegion=(OscarProperties.getInstance().getProperty("billregion","")).trim().toUpperCase();
String billForm=OscarProperties.getInstance().getProperty("default_view");
DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);

boolean isLinkedToDemographic=false;
ArrayList ackList = preview ? null : AcknowledgementData.getAcknowledgements(segmentID);
MessageHandler handlerMain = null;

if (!preview) {
    if(demographicID != null && !demographicID.equals("")&& !demographicID.equals("0")) {
        isLinkedToDemographic = true;
    }
	handlerMain = Factory.getHandler(segmentID);

} else {
    isLinkedToDemographic = true;
	ProviderOlisSession providerOlisSession = OlisSessionManager.getSession(loggedInInfo);
	String placerGroupNo = request.getParameter("placerGroupNo");
	List<OlisLabResultDisplay> labResultDisplays = providerOlisSession.getLabResultDisplayByPlacerGroupNo(placerGroupNo);
	if (!labResultDisplays.isEmpty()) {
		handlerMain = labResultDisplays.get(0).getOlisHl7Handler();
	}
}


OLISHL7Handler handler = null;
if (handlerMain instanceof OLISHL7Handler) {
	handler = (OLISHL7Handler) handlerMain;
}
if (!preview && "true".equals(request.getParameter("showLatest"))) {

	String multiLabId = Hl7textResultsData.getMatchingLabs(segmentID);
	segmentID = multiLabId.split(",")[multiLabId.split(",").length - 1];
}

String multiLabId = preview ? "" :  Hl7textResultsData.getMatchingLabs(segmentID);

for (String tempId : multiLabId.split(",")) {
	if (tempId.equals(segmentID) || tempId.equals("")) { continue; }
	else {
		try {
			handler.importSourceOrganizations((OLISHL7Handler)Factory.getHandler(tempId));
		} catch (Exception e) {
			org.oscarehr.util.MiscUtils.getLogger().error("error",e);
		}
	}
}

// check for errors printing
if (request.getAttribute("printError") != null && (Boolean) request.getAttribute("printError")){
%>
<script language="JavaScript">
    alert("The lab could not be printed due to an error. Please see the server logs for more detail.");
</script>
<%}
%>
<%!
public String strikeOutInvalidContent(String content, String status) {
     return status != null && status.startsWith("W") ? "<s>" + content + "</s>" : content;
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">

<html>
	<!--  This is an OLIS lab display -->
    <head>
        <html:base/>
        <title><%=handler.getPatientName()+" Lab Results"%></title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script language="javascript" type="text/javascript" src="../../../share/javascript/Oscar.js" ></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/prototype.js"></script>

        <script language="javascript" type="text/javascript">
            function updateLabDemoStatus(labno){
                if(document.getElementById("DemoTable"+labno)){
                    document.getElementById("DemoTable"+labno).style.backgroundColor="#FFF";
                }
                let newUIStatusTd = null;
                try {
                    newUIStatusTd = window.opener.document.getElementsByClassName('hl7-'+labno)[0];
                } catch(e) {
                    //do nothing
                }
                if (newUIStatusTd != null) {
                    newUIStatusTd.innerHTML = '';
                }
            }
        </script>
        <link rel="stylesheet" type="text/css" href="../../../share/css/OscarStandardLayout.css">
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/css/oscarOlis.css"/>

        <script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/jquery/jquery-1.4.2.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/jquery/jquery.form.js"></script>

        <script type="text/javascript">
		    jQuery.noConflict();
		</script>


        <script type="text/javaScript">
        function popupStart(vheight,vwidth,varpage,windowname) {
            var page = varpage;
            windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes";
            var popup=window.open(varpage, windowname, windowprops);
        }
        // open a new popup window
        function popupPage(vheight,vwidth,varpage) {
            var page = "" + varpage;
            windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes";
            var popup=window.open(page, "attachment", windowprops);
            if (popup != null) {
                if (popup.opener == null) {
                    popup.opener = self;
                }
            }
        }

        function popupONAREnhanced(vheight,vwidth,varpage) {
            var page = "" + varpage;
            windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=no,menubars=no,toolbars=no,resizable=yes";
            var popup=window.open(page, "attachment", windowprops);
            if (popup != null) {
                if (popup.opener == null) {
                    popup.opener = self;
                }
            }
        }



	function linkreq(rptId, reqId) {
	    var link = "../../LinkReq.jsp?table=hl7TextMessage&rptid="+rptId+"&reqid="+reqId;
	    window.open(link, "linkwin", "width=500, height=200");
	}

    function sendToPHR(labId, demographicNo) {
        popup(300, 600, "<%=request.getContextPath()%>/phr/SendToPhrPreview.jsp?labId=" + labId + "&demographic_no=" + demographicNo, "sendtophr");
    }

       function submitLabel(lblval, segmentID){
           document.forms['TDISLabelForm_'+segmentID].label.value = document.forms['acknowledgeForm_<%=segmentID%>'].label.value;


           jQuery.ajax( {
                   type: "POST",
                   url: '<%=request.getContextPath()%>'+"/lab/CA/ALL/createLabelTDIS.do",
                   dataType: "json",
                   data: { lab_no: jQuery("#labNum_<%=segmentID%>").val(),accessionNum: jQuery("#accNum").val(), label: jQuery("#label_<%=segmentID%>").val(), ajaxcall: true },
                   success: function(result) {
                       jQuery("#labelspan_<%=segmentID%>").children().get(0).innerHTML = "Label: " +  jQuery("#label_<%=segmentID%>").val();
                       document.forms['acknowledgeForm_<%=segmentID%>'].label.value = "";
                   }
               }
           );
       }

	function unlinkDemographic(labNo){
        var reason = "Incorrect demographic";
        reason = prompt('<bean:message key="oscarMDS.segmentDisplay.msgUnlink"/>', reason);

        //must include reason
        if( reason == null || reason.length == 0) {
            return false;
        }

        var urlStr='<%=request.getContextPath()%>'+"/lab/CA/ALL/UnlinkDemographic.do";
        var dataStr="reason="+reason+"&labNo="+labNo;
        jQuery.ajax({
            type: "POST",
            url:  urlStr,
            data: dataStr,
            success: function (data) {
                let newUIStatusTd = null;
                try {
                    newUIStatusTd = window.opener.document.getElementsByClassName('hl7-'+labNo)[0];
                } catch(e) {
                    //do nothing
                }
                if (newUIStatusTd != null) {
                    newUIStatusTd.innerHTML = '  <div class="tooltip-container">' +
                        '    <span class="fa fa-exclamation-triangle warning-icon"></span>' +
                        '    <div class="tooltip tooltip-warning">' +
                        '      <div class="tooltip-text">Unmatched Result</div>' +
                        '    </div>' +
                        '  </div>';
                } else {
                    top.opener.location.reload();
                }

                window.close();
            }
        });
    }
	
	function matchDemographic() {
        <% if ( !isLinkedToDemographic) { %>
        popupStart(360, 680, '../../../oscarMDS/SearchPatient.do?labType=HL7&segmentID=<%= segmentID %>&name=<%=java.net.URLEncoder.encode(handler.getLastName()+", "+handler.getFirstName())%>', 'searchPatientWindow');
        <% } %>
    }

        </script>

    </head>

    <body style="width:800px" onLoad="javascript:matchDemographic();">
        <jsp:include page="labDisplayOlis.jspf"/>
    </body>
<%
	// Jsp successfully loaded, mark lab read
	ReadLabDao readLabDao = SpringUtils.getBean(ReadLabDao.class);
	readLabDao.markAsRead(loggedInInfo.getLoggedInProviderNo(), "HL7", Integer.valueOf(originalSegmentID));
%>
</html>
