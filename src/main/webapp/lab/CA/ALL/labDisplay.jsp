<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="oscar.util.ConversionUtils"%>
<%@page import="org.oscarehr.common.dao.PatientLabRoutingDao"%>
<%@page import="org.oscarehr.common.model.PatientLabRouting"%>
<%@page import="org.oscarehr.myoscar.utils.MyOscarLoggedInInfo"%>
<%@page import="org.oscarehr.phr.util.MyOscarUtils"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="org.apache.commons.lang.builder.ReflectionToStringBuilder"%>
<%@page import="org.oscarehr.util.MiscUtils"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="org.oscarehr.caisi_integrator.ws.CachedDemographicLabResult"%>
<%@page import="oscar.oscarLab.ca.all.web.LabDisplayHelper"%>
<%@ page import="java.util.*,
		 java.sql.*,
		 oscar.oscarDB.*, oscar.oscarLab.FileUploadCheck, oscar.util.UtilDateUtilities,
		 oscar.oscarLab.ca.all.*,
		 oscar.oscarLab.ca.all.util.*,
		 oscar.oscarLab.ca.all.parsers.*,
		 oscar.oscarLab.LabRequestReportLink,
		 oscar.oscarMDS.data.ReportStatus,oscar.log.*,
		 org.apache.commons.codec.binary.Base64,
         oscar.OscarProperties" %>
<%@ page import="org.oscarehr.casemgmt.model.CaseManagementNoteLink"%>
<%@ page import="org.oscarehr.casemgmt.model.CaseManagementNote"%>
<%@ page import="org.oscarehr.util.SpringUtils"%>
<%@ page import="org.oscarehr.common.dao.UserPropertyDAO, org.oscarehr.common.model.UserProperty" %>
<%@ page import="org.oscarehr.common.dao.DemographicDao" %>
<%@ page import="org.oscarehr.common.model.MeasurementMap, org.oscarehr.common.dao.MeasurementMapDao" %>
<%@ page import="org.oscarehr.common.model.Tickler" %>
<%@ page import="org.oscarehr.managers.TicklerManager" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.casemgmt.service.CaseManagementManager, org.oscarehr.common.dao.Hl7TextMessageDao, org.oscarehr.common.model.Hl7TextMessage,org.oscarehr.common.dao.Hl7TextInfoDao,org.oscarehr.common.model.Hl7TextInfo"%>
<jsp:useBean id="oscarVariables" class="java.util.Properties" scope="session" />
<%@	page import="javax.swing.text.rtf.RTFEditorKit"%>
<%@	page import="java.io.ByteArrayInputStream"%>
<%@ page import="oscar.oscarEncounter.data.EctFormData" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.ReadLabDao" %>
<%@ page import="org.oscarehr.util.SessionConstants" %>
<%@page import="org.oscarehr.common.model.DataSharingSettings" %>
<%@page import="org.oscarehr.common.dao.DataSharingSettingsDao" %>
<%@ page import="oscar.form.dao.ONPerinatal2017Dao" %>
<%@ page import="ca.kai.datasharing.DataSharingService" %>
<%@ page import="oscar.oscarLab.ca.all.pageUtil.LabUtils" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="/WEB-INF/oscarProperties-tag.tld" prefix="oscarProperties"%>
<%@ taglib uri="/WEB-INF/indivo-tag.tld" prefix="indivo"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib prefix="csrf"
           uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%@ taglib uri="/WEB-INF/wellAiVoice.tld" prefix="well-ai-voice"%>
<%
      String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	  boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_lab" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../../../securityError.jsp?type=_lab");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}
%>

<%
  LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
oscar.OscarProperties props = oscar.OscarProperties.getInstance();
String segmentID = request.getParameter("segmentID");
String providerNo =request.getParameter("providerNo");
String searchProviderNo = StringUtils.trimToEmpty(request.getParameter("searchProviderNo"));
String patientMatched = request.getParameter("patientMatched");
String remoteFacilityIdString = request.getParameter("remoteFacilityId");
String remoteLabKey = request.getParameter("remoteLabKey");
String demographicID = request.getParameter("demographicId");
String showAllstr = request.getParameter("all");
boolean hideLinkDemographicPopup = Boolean.valueOf(request.getParameter("hideLinkDemographicPopup"));
String billRegion=(props.getProperty("billregion","")).trim().toUpperCase();
String billForm=props.getProperty("default_view");
boolean showCodes = SystemPreferencesUtils.isReadBooleanPreference("code_show_hide_column");
SystemPreferences embedPdfPref = SystemPreferencesUtils.findPreferenceByName("lab_embed_pdf");
boolean embedPdf = embedPdfPref == null || embedPdfPref.getValueAsBoolean();
String selectedProvince = (props.getProperty("billregion","")).trim().toUpperCase();
Boolean enhancedEnabled = "E".equals(session.getAttribute(SessionConstants.LOGIN_TYPE));
boolean hideEncounterLink = SystemPreferencesUtils.isReadBooleanPreferenceWithDefault("hideEncounterLink", false);
boolean hideClassicButtons = Boolean.parseBoolean(request.getParameter("hideClassicButtons"));
boolean requireAcknowledgedDocsConfirmDialog = SystemPreferencesUtils
		.isReadBooleanPreferenceWithDefault("require_acknowledged_docs_confirm_dialog", false);

List<String> allLicenseNames = new ArrayList<String>();
String lastLicenseNo = null, currentLicenseNo = null;

if(providerNo == null) {
	providerNo = loggedInInfo.getLoggedInProviderNo();
}

DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
UserPropertyDAO userPropertyDAO = (UserPropertyDAO)SpringUtils.getBean("UserPropertyDAO");
UserProperty uProp = userPropertyDAO.getProp(providerNo, UserProperty.LAB_ACK_COMMENT);
boolean skipComment = false;
if( uProp != null && uProp.getValue().equalsIgnoreCase("yes")) {
	skipComment = true;
}

UserProperty  getRecallDelegate = userPropertyDAO.getProp(providerNo, UserProperty.LAB_RECALL_DELEGATE);
UserProperty  getRecallTicklerAssignee = userPropertyDAO.getProp(providerNo, UserProperty.LAB_RECALL_TICKLER_ASSIGNEE);
UserProperty  getRecallTicklerPriority = userPropertyDAO.getProp(providerNo, UserProperty.LAB_RECALL_TICKLER_PRIORITY);
boolean recall = false;
String recallDelegate = "";
String ticklerAssignee = "";
String recallTicklerPriority = "";

if(getRecallDelegate!=null){
recall = true;
recallDelegate = getRecallDelegate.getValue();
recallTicklerPriority = getRecallTicklerPriority.getValue();
if(getRecallTicklerAssignee.getValue().equals("yes")){
	ticklerAssignee = "&taskTo="+recallDelegate;
}
}


//Need date lab was received by OSCAR
Hl7TextMessageDao hl7TxtMsgDao = (Hl7TextMessageDao)SpringUtils.getBean("hl7TextMessageDao");
MeasurementMapDao measurementMapDao = (MeasurementMapDao) SpringUtils.getBean("measurementMapDao");
Hl7TextMessage hl7TextMessage = hl7TxtMsgDao.find(Integer.parseInt(segmentID));

String dateLabReceived = "n/a";
String stringFormat = "yyyy-MM-dd HH:mm";

boolean isLinkedToDemographic=false;
ArrayList<ReportStatus> ackList=null;
String multiLabId = null;
MessageHandler handler=null;
String hl7 = null;
String reqID = null, reqTableID = null;
String remoteFacilityIdQueryString="";

boolean bShortcutForm = OscarProperties.getInstance().getProperty("appt_formview", "").equalsIgnoreCase("on") ? true : false;
String formName = bShortcutForm ? OscarProperties.getInstance().getProperty("appt_formview_name") : "";
String formNameShort = formName.length() > 3 ? (formName.substring(0,2)+".") : formName;
String formName2 = bShortcutForm ? OscarProperties.getInstance().getProperty("appt_formview_name2", "") : "";
String formName2Short = formName2.length() > 3 ? (formName2.substring(0,2)+".") : formName2;
boolean bShortcutForm2 = bShortcutForm && !formName2.equals("");
boolean obgynShortcuts = SystemPreferencesUtils.isReadBooleanPreferenceWithDefault("show_obgyn_shortcuts", false);

List<MessageHandler>handlers = new ArrayList<MessageHandler>();
String []segmentIDs = null;
Boolean showAll = showAllstr != null && !"null".equalsIgnoreCase(showAllstr);

DataSharingService dataSharingService = SpringUtils.getBean(DataSharingService.class);
boolean demographicPortalEnabled = false;


PatientLabRoutingDao dao = SpringUtils.getBean(PatientLabRoutingDao.class);
String syncStatus = "Not available";
  for (PatientLabRouting r : dao.findByLabNoAndLabType(ConversionUtils.fromIntString(segmentID), "HL7")) {
    boolean isAvailableForPatientPortal = r.isAvailable()
            || (r.getAutoSyncDate() != null && r.getAutoSyncDate().before(new java.util.Date()));
    if (r.getLastSyncedDate() == null && !r.isAvailable()
            && (r.getAutoSyncDate() != null && r.getAutoSyncDate().after(new java.util.Date()))) {
      syncStatus = "Pending";
    } else if (r.getLastSyncedDate() != null && isAvailableForPatientPortal) {
      syncStatus = "Read";
    } else if (r.getLastSyncedDate() == null && (isAvailableForPatientPortal || r.getAutoSyncDate() != null)) {
      syncStatus = "Available";
    } else if (r.getLastSyncedDate() == null && !isAvailableForPatientPortal) {
      syncStatus = "";
    } else if (r.getLastSyncedDate() != null && !isAvailableForPatientPortal) {
      syncStatus = "Removed";
    }
  }

  if (remoteFacilityIdString == null) // local lab
  {

    Long reqIDL = LabRequestReportLink.getIdByReport("hl7TextMessage", Long.valueOf(segmentID));
    reqID = reqIDL == null ? "" : reqIDL.toString();
    reqIDL = LabRequestReportLink.getRequestTableIdByReport("hl7TextMessage", Long.valueOf(segmentID));
    reqTableID = reqIDL == null ? "" : reqIDL.toString();

    if (hl7TextMessage != null) {
      java.util.Date date = hl7TextMessage.getCreated();
      dateLabReceived = UtilDateUtilities.DateToString(date, stringFormat);
    }

    for (PatientLabRouting r : dao.findByLabNoAndLabType(ConversionUtils.fromIntString(segmentID), "HL7")) {
      demographicID = "" + r.getDemographicNo();
    }

    if (demographicID != null && !demographicID.equals("") && !demographicID.equals("0")) {
      isLinkedToDemographic = true;
        LogAction.addLog((String) session.getAttribute("user"), LogConst.READ, LogConst.CON_HL7_LAB,
                segmentID, LoggedInInfo.obtainClientIpAddress(request), demographicID);
      demographicPortalEnabled = StringUtils.isNotEmpty(demographicID)
              && dataSharingService.isDemographicPortalEnabled(Integer.valueOf(demographicID));
    } else {
        LogAction.addLog((String) session.getAttribute("user"), LogConst.READ, LogConst.CON_HL7_LAB,
                segmentID, LoggedInInfo.obtainClientIpAddress(request));
    }

    if (showAll) {
      multiLabId = request.getParameter("multiID");
      segmentIDs = multiLabId.split(",");
      for (int i = 0; i < segmentIDs.length; ++i) {
        handlers.add(Factory.getHandler(segmentIDs[i]));
      }

      handler = handlers.get(0);
    } else {
      multiLabId = Hl7textResultsData.getMatchingLabs(segmentID);
      segmentIDs = multiLabId.split(",");

      List<String> segmentIdList = new ArrayList<String>();
      handler = Factory.getHandler(segmentID);
      handlers.add(handler);
      segmentIdList.add(segmentID);

      //this is where it gets weird. We want to show all messages with different filler order num but same accession in a single report
      if ("CLS".equals(handler.getMsgType())) {
        for (int i = 0; i < segmentIDs.length; ++i) {
          MessageHandler handler2 = Factory.getHandler(segmentIDs[i]);
          if (!handler.getFillerOrderNumber().equals(handler2.getFillerOrderNumber())) {
            handlers.add(handler2);
            segmentIdList.add(segmentIDs[i]);
          }
        }
        if (handlers.size() > 1) {
          multiLabId = segmentID;
        }
      }
      segmentIDs = segmentIdList.toArray(new String[segmentIdList.size()]);

      hl7 = Factory.getHL7Body(segmentID);
      if (handler instanceof OLISHL7Handler) {
%>
			<jsp:forward page="labDisplayOlisMain.jsp" />
			<%
		}
	}
    
}
else // remote lab
{
	CachedDemographicLabResult remoteLabResult=LabDisplayHelper.getRemoteLab(loggedInInfo, Integer.parseInt(remoteFacilityIdString), remoteLabKey,Integer.parseInt(demographicID));
	MiscUtils.getLogger().debug("retrieved remoteLab:"+ReflectionToStringBuilder.toString(remoteLabResult));
	isLinkedToDemographic=true;

	LogAction.addLog((String) session.getAttribute("user"), LogConst.READ, LogConst.CON_HL7_LAB, "segmentId="+segmentID+", remoteFacilityId="+remoteFacilityIdString+", remoteDemographicId="+demographicID);

	Document cachedDemographicLabResultXmlData=LabDisplayHelper.getXmlDocument(remoteLabResult);
	ackList=LabDisplayHelper.getReportStatus(cachedDemographicLabResultXmlData);
	multiLabId=LabDisplayHelper.getMultiLabId(cachedDemographicLabResultXmlData);
	handler=LabDisplayHelper.getMessageHandler(cachedDemographicLabResultXmlData);
	handlers.add(handler);
	segmentIDs = new String[] {"0"};  //fake segment ID for the for loop below to execute
	hl7=LabDisplayHelper.getHl7Body(cachedDemographicLabResultXmlData);
	dateLabReceived = LabDisplayHelper.getDateLabReceived(cachedDemographicLabResultXmlData);

	try {
		remoteFacilityIdQueryString="&remoteFacilityId="+remoteFacilityIdString+"&remoteLabKey="+URLEncoder.encode(remoteLabKey, "UTF-8");
	} catch (Exception e) {
		MiscUtils.getLogger().error("Error", e);
	}
}

/********************** Converted to this sport *****************************/


// check for errors printing
if (request.getAttribute("printError") != null && (Boolean) request.getAttribute("printError")){
%>
<script language="JavaScript">
    alert("The lab could not be printed due to an error. Please see the server logs for more detail.");
</script>
<%}


	String annotation_display = org.oscarehr.casemgmt.model.CaseManagementNoteLink.DISP_LABTEST;
	CaseManagementManager caseManagementManager = (CaseManagementManager) SpringUtils.getBean("caseManagementManager");

%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <html:base/>
        <title><%=handler.getPatientName()+" Lab Results"%></title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <script language="javascript" type="text/javascript" src="../../../share/javascript/Oscar.js" ></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/prototype.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/scriptaculous.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/effects.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/jquery/jquery-1.4.2.js"></script>
      	<script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/jquery/jquery.form.js"></script>
      	

       <script  type="text/javascript" charset="utf-8">

     	  jQuery.noConflict();
		</script>
		
	
	<oscar:customInterface section="labView"/>

		
        <script language="javascript" type="text/javascript">
            // alternately refer to this function in oscarMDSindex.js as labDisplayAjax.jsp does
		function updateLabDemoStatus(labno){
                                    if(document.getElementById("DemoTable"+labno)){
                                       document.getElementById("DemoTable"+labno).style.backgroundColor="#FFF";
                                    }
                                    let newUIStatusTd = null;
                                    try {
                                        newUIStatusTd = window.opener.document.getElementsByClassName('hl7-'+labno)[0];
                                    } catch(e) {
                                        //do nothing
                                    }
                                    if (newUIStatusTd != null) {
                                        newUIStatusTd.innerHTML = '';
                                    }
                                }
	</script>
        <link rel="stylesheet" type="text/css" href="../../../share/css/OscarStandardLayout.css">
        <style type="text/css">
            <!--
.RollRes     { font-weight: 700; font-size: 8pt; color: white; font-family:
               Verdana, Arial, Helvetica }
.RollRes a:link { color: white }
.RollRes a:hover { color: white }
.RollRes a:visited { color: white }
.RollRes a:active { color: white }
.AbnormalRollRes { font-weight: 700; font-size: 8pt; color: red; font-family:
               Verdana, Arial, Helvetica }
.AbnormalRollRes a:link { color: red }
.AbnormalRollRes a:hover { color: red }
.AbnormalRollRes a:visited { color: red }
.AbnormalRollRes a:active { color: red }
.CorrectedRollRes { font-weight: 700; font-size: 8pt; color: yellow; font-family:
               Verdana, Arial, Helvetica }
.CorrectedRollRes a:link { color: yellow }
.CorrectedRollRes a:hover { color: yellow }
.CorrectedRollRes a:visited { color: yellow }
.CorrectedRollRes a:active { color: yellow }
.AbnormalRes { font-weight: bold; font-size: 8pt; color: red; font-family:
               Verdana, Arial, Helvetica }
.AbnormalRes a:link { color: red }
.AbnormalRes a:hover { color: red }
.AbnormalRes a:visited { color: red }
.AbnormalRes a:active { color: red }
.NormalRes   { font-weight: bold; font-size: 8pt; color: black; font-family:
                      Verdana, Arial, Helvetica }
.TDISRes	{font-weight: bold; font-size: 10pt; color: black; font-family:
               Verdana, Arial, Helvetica}
.NormalRes a:link { color: black }
.NormalRes a:hover { color: black }
.NormalRes a:visited { color: black }
.NormalRes a:active { color: black }
.HiLoRes     { font-weight: bold; font-size: 8pt; color: blue; font-family:
               Verdana, Arial, Helvetica }
.HiLoRes a:link { color: blue }
.HiLoRes a:hover { color: blue }
.HiLoRes a:visited { color: blue }
.HiLoRes a:active { color: blue }
.CorrectedRes { font-weight: bold; font-size: 8pt; color: #E000D0; font-family:
               Verdana, Arial, Helvetica }
.CorrectedRes         a:link { color: #6da997 }
.CorrectedRes a:hover { color: #6da997 }
.CorrectedRes a:visited { color: #6da997 }
.CorrectedRes a:active { color: #6da997 }
.Field       { font-weight: bold; font-size: 8.5pt; color: black; font-family:
               Verdana, Arial, Helvetica }
.NarrativeRes { font-weight: 700; font-size: 10pt; color: black; font-family:
               Courier New, Courier, mono }
div.Field a:link { color: black }
div.Field a:hover { color: black }
div.Field a:visited { color: black }
div.Field a:active { color: black }
.Field2      { font-weight: bold; font-size: 8pt; color: #ffffff; font-family:
               Verdana, Arial, Helvetica }
div.Field2   { font-weight: bold; font-size: 8pt; color: #ffffff; font-family:
               Verdana, Arial, Helvetica }
div.FieldData { font-weight: normal; font-size: 8pt; color: black; font-family:
               Verdana, Arial, Helvetica }
div.Field3   { font-weight: normal; font-size: 8pt; color: black; font-style: italic;
               font-family: Verdana, Arial, Helvetica }
div.Title    { font-weight: 800; font-size: 10pt; color: white; font-family:
               Verdana, Arial, Helvetica; padding-top: 4pt; padding-bottom:
               2pt }
div.Title a:link { color: white }
div.Title a:hover { color: white }
div.Title a:visited { color: white }
div.Title a:active { color: white }
div.Title2   { font-weight: bolder; font-size: 9pt; color: black; text-indent: 5pt;
               font-family: Verdana, Arial, Helvetica; padding: 10pt 15pt 2pt 2pt}
div.Title2 a:link { color: black }
div.Title2 a:hover { color: black }
div.Title2 a:visited { color: black }
div.Title2 a:active { color: black }
.Cell        { background-color: #9999CC; border-left: thin solid #CCCCFF;
               border-right: thin solid #6666CC;
               border-top: thin solid #CCCCFF;
               border-bottom: thin solid #6666CC }
.Cell2       { background-color: #376c95; border-left-style: none; border-left-width: medium;
               border-right-style: none; border-right-width: medium;
               border-top: thin none #bfcbe3; border-bottom-style: none;
               border-bottom-width: medium }
.Cell3       { background-color: #add9c7; border-left: thin solid #dbfdeb;
               border-right: thin solid #5d9987;
               border-top: thin solid #dbfdeb;
               border-bottom: thin solid #5d9987 }
.CellHdr     { background-color: #cbe5d7; border-right-style: none; border-right-width:
               medium; border-bottom-style: none; border-bottom-width: medium }
.Nav         { font-weight: bold; font-size: 8pt; color: black; font-family:
               Verdana, Arial, Helvetica }
.PageLink a:link { font-size: 8pt; color: white }
.PageLink a:hover { color: red }
.PageLink a:visited { font-size: 9pt; color: yellow }
.PageLink a:active { font-size: 12pt; color: yellow }
.PageLink    { font-family: Verdana }
.labReqDate {
    color: #ffffff;
    float: right;
    margin-right: 10px;
}
.text1       { font-size: 8pt; color: black; font-family: Verdana, Arial, Helvetica }
div.txt1     { font-size: 8pt; color: black; font-family: Verdana, Arial }
div.txt2     { font-weight: bolder; font-size: 6pt; color: black; font-family: Verdana, Arial }
div.Title3   { font-weight: bolder; font-size: 12pt; color: black; font-family:
               Verdana, Arial }
.red         { color: red }
.text2       { font-size: 7pt; color: black; font-family: Verdana, Arial }
.white       { color: white }
.title1      { font-size: 9pt; color: black; font-family: Verdana, Arial }
div.Title4   { font-weight: 600; font-size: 8pt; color: white; font-family:
               Verdana, Arial, Helvetica }
pre {
	display: block;
    font-family:  Verdana, Arial, Helvetica;
    white-space: -moz-pre-space;
    margin:0px;
    font-size: x-small;
    font-weight:600;
} 
            -->
            
input[type=button], button, input[id^='acklabel_']{ font-size:12px !important;padding:0px;}    
#ticklerWrap{position:relative;top:0px;background-color:#FF6600;width:100%;}  

.completedTickler{
    opacity: 0.8;
    filter: alpha(opacity=80); /* For IE8 and earlier */
}

@media print { 
.DoNotPrint{display:none;}
}      
        </style>

        <script language="JavaScript">
        var providerNo = '<%=providerNo%>';
        function popupStart(vheight,vwidth,varpage,windowname) {
            var page = varpage;
            windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes";
            var popup=window.open(varpage, windowname, windowprops);
        }
        function getComment(action, segmentId) {

            var ret = true;
            var comment = "";
            var text = providerNo + "_" + segmentId + "commentText";
            if( $(text) != null ) {
	            comment = $(text).innerHTML;
	            if( comment == null ) {
	            	comment = "";
                } else {
                    comment = htmlDecode(comment);
                }
            }
            var commentVal = prompt('<bean:message key="oscarMDS.segmentDisplay.msgComment"/>', comment);

            if( commentVal == null ) {
            	ret = false;
            }
            else if( commentVal != null && commentVal.length > 0 )
                document.forms['acknowledgeForm_'+ segmentId].comment.value = commentVal;
            else
            	document.forms['acknowledgeForm_'+ segmentId].comment.value = comment;

           if(ret) handleLab('acknowledgeForm_'+segmentId,segmentId, action);

            return false;
        }

        function printPDF(labid){
        	var frm = "acknowledgeForm_" + labid;
        	document.forms[frm].action="PrintPDF.do";
        	document.forms[frm].submit();            
        }

        window.addEventListener('message', (event) => {
            if (event.data.includes('printFromFrameHL7')) {
                printPDF(<%= segmentID %>);
           }
        });

	function linkreq(rptId, reqId) {
	    var link = "../../LinkReq.jsp?table=hl7TextMessage&rptid="+rptId+"&reqid="+reqId;
	    window.open(link, "linkwin", "width=500, height=200");
	}

        function sendToPHR(labId, demographicNo) {
        	<%
        		MyOscarLoggedInInfo myOscarLoggedInInfo=MyOscarLoggedInInfo.getLoggedInInfo(session);

        		if (myOscarLoggedInInfo==null || !myOscarLoggedInInfo.isLoggedIn())
        		{
        			%>
    					alert('Please Login to MyOscar before performing this action.');
        			<%
        		}
        		else
        		{
        			%>
	                    popup(450, 600, "<%=request.getContextPath()%>/phr/SendToPhrPreview.jsp?labId=" + labId + "&demographic_no=" + demographicNo, "sendtophr");
        			<%
        		}
        	%>
        }

        function matchMe() {
            <% if (!isLinkedToDemographic && !hideLinkDemographicPopup) { %>
              popupStart(360, 680, '../../../oscarMDS/SearchPatient.do?providerNumber=<%= providerNo %>&labType=HL7&segmentID=<%= segmentID %>&name=<%=java.net.URLEncoder.encode(handler.getLastName()+", "+handler.getFirstName())%>', 'searchPatientWindow');
            <% } %>
	}


        function handleLab(formid,labid,action){
            let headers = {'<csrf:tokenname/>': '<csrf:tokenvalue/>'};
            var url='../../../dms/inboxManage.do';
                                           var data='method=isLabLinkedToDemographic&labid='+labid;
                                           new Ajax.Request(url, {method: 'post',parameters:data, requestHeaders: headers,onSuccess:function(transport){
                                                                    var json=transport.responseText.evalJSON();
                                                                    if(json!=null){
                                                                        var success=json.isLinkedToDemographic;
                                                                        var demoid='';
                                                                        //check if lab is linked to a provider
                                                                        if(success){
                                                                            if(action=='ackLab'){
                                                                                if(confirmAck()){
                                                                                	$("labStatus_"+labid).value = "A";
                                                                                    updateStatus(formid,labid);
                                                                                }
                                                                            }else if(action=='msgLab'){
                                                                                demoid=json.demoId;
                                                                                if(demoid!=null && demoid.length>0)
                                                                                    window.popup(700,960,'../../../oscarMessenger/SendDemoMessage.do?demographic_no='+demoid,'msg');
                                                                            }else if(action=='msgLabRecall'){
                                                                                demoid=json.demoId;
                                                                                if(demoid!=null && demoid.length>0)
                                                                                    window.popup(700,980,'../../../oscarMessenger/SendDemoMessage.do?demographic_no='+demoid+"&recall",'msgRecall');
                                                                                    window.popup(450,600,'../../../tickler/ForwardDemographicTickler.do?docType=HL7&docId='+labid+'&demographic_no='+demoid+'<%=ticklerAssignee%>&priority=<%=recallTicklerPriority%>&recall','ticklerRecall');
                                                                            }else if(action=='ticklerLab'){
                                                                                demoid=json.demoId;
                                                                                if(demoid!=null && demoid.length>0)
                                                                                    window.popup(450,600,'../../../tickler/ForwardDemographicTickler.do?docType=HL7&docId='+labid+'&demographic_no='+demoid,'tickler')
                                                                            }
                                                                            else if( action == 'addComment' ) {
                                                                            	addComment(formid,labid);
                                                                            } else if (action == 'unlinkDemo') {
                                                                                unlinkDemographic(labid);
                                                                            }

                                                                        }else{
                                                                            if(action=='ackLab'){
                                                                                if(confirmAckUnmatched()) {
                                                                                	$("labStatus_"+labid).value = "A";
                                                                                    updateStatus(formid,labid);
                                                                                }
                                                                                else {
                                                                                    matchMe();
                                                                                }

                                                                            }else{
                                                                                alert("Please relate lab to a patient");
                                                                                matchMe();
                                                                            }
                                                                        }
                                                                    }
                                                            }});
        }
        function confirmAck(){
		<% if (requireAcknowledgedDocsConfirmDialog) { %>
            		return confirm('<bean:message key="oscarMDS.index.msgConfirmAcknowledge"/>');
            	<% } else { %>
            		return true;
            	<% } %>
	}

        function confirmCommentUnmatched(){
            return confirm('<bean:message key="oscarMDS.index.msgConfirmAcknowledgeUnmatched"/>');
        }

        function confirmAckUnmatched(){
            return confirm('<bean:message key="oscarMDS.index.msgConfirmAcknowledgeUnmatched"/>');
        }
        function updateStatus(formid,labid){
        	
            var url='<%=request.getContextPath()%>'+"/oscarMDS/UpdateStatus.do";
            var data=$(formid).serialize(true);

            new Ajax.Request(url,{method:'post',parameters:data,onSuccess:function(transport){
                if( <%=showAll%> ) {
                    window.location.reload();
                } else if (window.opener != null && typeof window.opener.openedWindowLabAcknowledged === 'function') {
                    // opener has openedWindowLabAcknowledged, meaning the lab was opened from the new UI
                    window.opener.openedWindowLabAcknowledged(labid, 'HL7');
                    window.close();
                } else if (window.parent != null && typeof window.parent.openedWindowLabAcknowledged === 'function') {
                    // parent has openedWindowLabAcknowledged, meaning the lab was opened from the new UI detail view
                    window.parent.openedWindowLabAcknowledged(labid, 'HL7');
                } else if( window.opener.document.getElementById('labdoc_'+labid) != null ) {
                    window.opener.Effect.BlindUp('labdoc_'+labid);
                    window.opener.refreshCategoryList();  
                    window.close();
                }
                else {
                    window.close();
                }
                
        }});

        }
        
        function pushToPortal(formId){
          var url='<%=request.getContextPath()%>'+"/oscarMDS/PushToPortal.do";
            var data=$(formId).serialize(true);
            new Ajax.Request(url,{method:'post',parameters:data,onSuccess:function(transport){
              window.location.reload();
              }
            });
        }


        function unlinkDemographic(labNo){
            var reason = "Incorrect demographic";
            reason = prompt('<bean:message key="oscarMDS.segmentDisplay.msgUnlink"/>', reason);

            //must include reason
            if( reason == null || reason.length == 0) {
            	return false;
            }
            
            var demographicId = '<%= request.getParameter("demographicId") %>';
            var urlStr='<%=request.getContextPath()%>'+"/lab/CA/ALL/UnlinkDemographic.do";
            var dataStr = "reason=" + reason
                + "&labNo=" + labNo
                + "&demographicNo=" + demographicId;
            jQuery.ajax({
    			type: "POST",
    			url:  urlStr,
    			data: dataStr,
    			success: function (data) {
                            let newUIStatusTd = null;
                            try {
                                newUIStatusTd = window.opener.document.getElementsByClassName('hl7-'+labNo)[0];
                            } catch(e) {
                                //do nothing
                            }
                            if (newUIStatusTd != null) {
                                newUIStatusTd.innerHTML = '  <div class="tooltip-container">' +
                                    '    <span class="fa fa-exclamation-triangle warning-icon"></span>' +
                                    '    <div class="tooltip tooltip-warning">' +
                                    '      <div class="tooltip-text">Unmatched Result</div>' +
                                    '    </div>' +
                                    '  </div>';
                            } else {
                                top.opener.location.reload();
                            }
                    
                            window.close();
    			}
            });                            
        }

        function addComment(formid,labid) {
        	var url='<%=request.getContextPath()%>'+"/oscarMDS/UpdateStatus.do?method=addComment";
			if( $F("labStatus_"+labid) == "" ) {
				$("labStatus_"+labid).value = "N";
			}
			
        	var data=$(formid).serialize(true);
        	
            new Ajax.Request(url,{method:'post',parameters:data,onSuccess:function(transport){				
            		window.location.reload();
            	
        }});
        }

        window.ForwardSelectedRows = function() {
    		var query = jQuery(document.reassignForm).formSerialize();
    		jQuery.ajax({
    			type: "POST",
    			url:  "<%=request.getContextPath()%>/oscarMDS/ReportReassign.do",
    			data: query,
    			success: function (data) {
    				self.close();
    			}
    		});
    	}

        function fileLab(labId, csrfToken) {
          let headers = {};
          if (typeof csrfToken !== 'undefined' && csrfToken !== null) {
            headers = csrfToken;
          }
          jQuery.ajax({
            type: "POST",
            url: "<%= request.getContextPath() %>/oscarMDS/FileLabs.do",
            data: "method=fileLabAjax&flaggedLabId=" + labId + "&labType=HL7",
            success: function (data) {
              if (window.parent != null
                  && typeof window.parent.openedWindowClosing === 'function') {
                // parent has openedWindowLabAcknowledged, meaning the lab was opened from the new UI detail view
                window.parent.openedWindowClosing(labId, 'HL7');
              }
              self.opener.removeReport(labId, 'HL7', 'F');
              self.close();
            }
          });
        }

        function submitLabel(lblval, segmentID){
       		document.forms['TDISLabelForm_'+segmentID].label.value = document.forms['acknowledgeForm_'+segmentID].label.value;
       	}
        </script>

    </head>

    <body onLoad="javascript:matchMe();">
      <% if (!Boolean.parseBoolean((request.getParameter("hideTaliToolbar")))) { %>
          <well-ai-voice:script/>
      <% } %>
        <!-- form forwarding of the lab -->
        <%        
        	for( int idx = 0; idx < segmentIDs.length; ++idx ) {
        		       		
        		if (remoteFacilityIdString==null) {
        			ackList = AcknowledgementData.getAcknowledgements(segmentID);
        			segmentID = segmentIDs[idx];          		
                	handler = handlers.get(idx);
        		}
        		
        		boolean notBeenAcked = ackList.size() == 0;
        		boolean ackFlag = false;
        		String labStatus = "";
        		String providerComment = "";
        		if (ackList != null){
        		    for (int i=0; i < ackList.size(); i++){
        		        ReportStatus reportStatus = ackList.get(i);
        		        if (reportStatus.getOscarProviderNo() != null && reportStatus.getOscarProviderNo().equals(providerNo) ) {
        		        	labStatus = reportStatus.getStatus();
                            providerComment = reportStatus.getRoutingComment() != null ? reportStatus.getRoutingComment() : "";
        		        	if( labStatus.equals("A") ){
        		            	ackFlag = true;//lab has been ack by this provider.
        		            	break;
        		        	}
        		        }
        		    }
        		}
        		
        		Hl7TextInfoDao hl7TextInfoDao = (Hl7TextInfoDao) SpringUtils.getBean("hl7TextInfoDao");
        		int lab_no = Integer.parseInt(segmentID);
        		Hl7TextInfo hl7Lab = hl7TextInfoDao.findLabId(lab_no);
        		String label = "";
        		if (hl7Lab != null && hl7Lab.getLabel()!=null) label = hl7Lab.getLabel();
        		
        		String ackLabFunc;
        		if( skipComment ) {
        			ackLabFunc = "handleLab('acknowledgeForm_" + segmentID + "','" + segmentID + "','ackLab');";
        		}
        		else {
        			ackLabFunc = "getComment('ackLab', " + segmentID + ");";
        		}

        %>
        <script type="text/javascript">

        jQuery(function() {
      	  jQuery("#createLabel_<%=segmentID%>").click(function() {
      	    jQuery.ajax( {
      	      type: "POST",
      	      url: '<%=request.getContextPath()%>'+"/lab/CA/ALL/createLabelTDIS.do",
      	      dataType: "json",
      	      data: { lab_no: jQuery("#labNum_<%=segmentID%>").val(), label: jQuery("#label_<%=segmentID%>").val(), ajaxcall: true },
      	      success: function(result) {
      	    	jQuery("#labelspan_<%=segmentID%>").children().get(0).innerHTML = "Label: " +  jQuery("#label_<%=segmentID%>").val();
        	  	document.forms['acknowledgeForm_<%=segmentID%>'].label.value = "";    
      	      }
      	    }
      	   );
      	});
      });

		</script>
		<div id="lab_<%=segmentID%>">
        <form name="reassignForm_<%=segmentID%>" method="post" action="Forward.do">
            <input type="hidden" name="flaggedLabs" value="<%=segmentID%>" />
            <input type="hidden" name="selectedProviders" value="" />
            <input type="hidden" name="favorites" value="" />
            <input type="hidden" name="labType" value="HL7" />
            <input type="hidden" name="labType<%= segmentID %>HL7" value="imNotNull" />
            <input type="hidden" id="providerNo_<%=segmentID %>" name="providerNo" value="<%= providerNo %>" />
            <input type="hidden" id="searchProviderNo" name="searchProviderNo" value="<%= searchProviderNo %>" />
        </form>

        <form name="TDISLabelForm_<%=segmentID%>"  method='POST' action="../../../lab/CA/ALL/createLabelTDIS.do">
					<input type="hidden" id="labNum_<%=segmentID %>" name="lab_no" value="<%=lab_no%>">
					<input type="hidden" id="label_<%=segmentID %>" name="label" value="<%=label%>">
		</form>

        <form name="acknowledgeForm_<%=segmentID%>" id="acknowledgeForm_<%=segmentID%>" method="post" onsubmit="javascript:void(0);" method="post" action="javascript:void(0);" >

            <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="3">
                            <tr>
                                <td align="left" class="MainTableTopRowRightColumn" width="100%">
                                    <input type="hidden" name="segmentID" value="<%= segmentID %>"/>
                                    <% if (remoteFacilityIdString != null) { %>
                                    	<input type="hidden" name="remoteFacilityId" value="<%= remoteFacilityIdString %>"/>
                                    	<input type="hidden" name="remoteLabKey" value="<%= remoteLabKey %>"/>
                                    	<input type="hidden" name="demographicId" value="<%= demographicID %>"/>
                                    <% } %>
                                    <input type="hidden" name="multiID" value="<%= multiLabId %>" />
									<input type="hidden" name="dateLabReceived" value="<%= dateLabReceived %>" />
                                    <input type="hidden" name="providerNo" id="providerNo" value="<%= providerNo %>"/>
                                    <input type="hidden" name="status" value="<%=labStatus%>" id="labStatus_<%=segmentID%>"/>
                                    <input type="hidden" name="comment" value="<%=Encode.forHtmlAttribute(providerComment)%>"/>
                                    <input type="hidden" name="labType" value="HL7"/>
                                    <% if (!hideClassicButtons && !ackFlag) { %>
                                    <input type="button" value="<bean:message key="oscarMDS.segmentDisplay.btnAcknowledge"/>" onclick="<%=ackLabFunc%>" >
                                    <input type="button" value="<bean:message key="oscarMDS.segmentDisplay.btnComment"/>" onclick="return getComment('addComment',<%=segmentID%>);">
                                    <% } %>
                                    <input type="button" class="smallButton" value="<bean:message key="oscarMDS.index.btnForward"/>" onClick="popupStart(355, 675, '../../../oscarMDS/SelectProvider.jsp?docId=<%=segmentID%>&labDisplay=true', 'providerselect')">
                                    <% if (!hideClassicButtons) { %>
                                    <input type="button" value=" <bean:message key="oscarMDS.index.btnFile"/> " onClick="fileLab(<%= segmentID %>, {'<csrf:tokenname/>': '<csrf:tokenvalue/>'});">
                                    <input type="button" value=" <bean:message key="global.btnClose"/> " onClick="window.close();window.parent.openedWindowClosing();">
                                    <% } %>
                                    <input type="button" value=" <bean:message key="global.btnPrint"/> " onClick="printPDF('<%=segmentID%>')">
                                    <input type="button" value="Msg" onclick="handleLab('','<%=segmentID%>','msgLab');"/>
                                    <input type="button" value="Tickler" onclick="handleLab('','<%=segmentID%>','ticklerLab');"/>
                                    <input type="button" value="<bean:message key="oscarMDS.segmentDisplay.btnUnlinkDemo"/>" onclick="handleLab('','<%=segmentID%>','unlinkDemo');"/>

                                    <%
                                    if ( searchProviderNo != null && !hideEncounterLink) { // null if we were called from e-chart
                                    %>
                                    <input type="button" value=" <bean:message key="oscarMDS.segmentDisplay.btnEChart"/>"
                                            onClick="popupStart(360, 680,
                                            '../../../oscarMDS/SearchPatient.do?providerNo=<%= providerNo %>&labType=HL7&segmentID=<%=
                                            segmentID %>&name=<%=java.net.URLEncoder.encode(handler.getLastName() + ","
                                            + handler.getFirstName())%>', 'encounter')">
                                    <%
                                    }

                                    if ( demographicID != null && demographicDao.getDemographic(demographicID)!=null ) {
                                        String demographicName = demographicDao.getDemographic(demographicID).getFormattedName();
                                        String demographicProvider = demographicDao.getDemographic(demographicID).getProviderNo()!=null?demographicDao.getDemographic(demographicID).getProviderNo():"";
                                        String billPopupLink = request.getContextPath() + "/billing.do?billRegion=" + URLEncoder.encode(billRegion, "UTF-8") + "&billForm=" + URLEncoder.encode(billForm, "UTF-8") + "&hotclick=&appointment_no=0&demographic_name=" + URLEncoder.encode(demographicName, "UTF-8") + "&demographic_no=" + demographicID + "&providerview=" + demographicProvider + "&user_no=" + providerNo + "&apptProvider_no=none&appointment_date=&start_time=00:00:00&bNewForm=1&status=t";
                                        if (enhancedEnabled && selectedProvince.equals("ON")) {
                                            billPopupLink = "/" + OscarProperties.getKaiemrDeployedContext() + "/#/billing/?demographicNo=" + demographicID;
                                        }
                                    %>
                                    <input type="button" value="Bill" onClick="popupPage(700, 1000, '<%=billPopupLink%>');return false;"/>
                                    <% } %>
				    <input type="button" value="Req# <%=reqTableID%>" title="Link to Requisition" onclick="linkreq('<%=segmentID%>','<%=reqID%>');" />


                                   	<% if (bShortcutForm) { %>
									<input type="button" value="<%=formNameShort%>" onClick="popupStart(700, 1024, '../../../form/forwardshortcutname.jsp?formname=<%=formName%>&demographic_no=<%=demographicID%>', '<%=formNameShort%>')" />
									<% } %>
									<% if (bShortcutForm2) { %>
									<input type="button" value="<%=formName2Short%>" onClick="popupStart(700, 1024, '../../../form/forwardshortcutname.jsp?formname=<%=formName2%>&demographic_no=<%=demographicID%>', '<%=formName2Short%>')" />
									<% } %>

                                    <% if(recall){%>
                                    <input type="button" value="Recall" onclick="handleLab('','<%=segmentID%>','msgLabRecall');">
                                    <%}%>
									
                                    <% if (!label.equals(null) && !label.equals("")) { %>
										<button type="button" id="createLabel_<%=segmentID%>" value="Label" onclick="submitLabel(this, '<%=segmentID%>');">Label</button>
										<%} else { %>
										<button type="button" id="createLabel_<%=segmentID%>" style="background-color:#6699FF" value="Label" onclick="submitLabel(this, '<%=segmentID%>');">Label</button>
										<%} %>
										<input type="hidden" id="labNum_<%=segmentID %>" name="lab_no" value="<%=lab_no%>">
						                <input type="text" id="acklabel_<%=segmentID %>" name="label" value=""/>

						                 <% String labelval="";
						                 if (label!="" && label!=null) {
						                 	labelval = label;
						                 }else {
						                	 labelval = "(not set)";

						                 } %>
					                 <span id="labelspan_<%=segmentID%>" class="Field2"><i>Label: <%=labelval %> </i></span>

                                    <span class="Field2"><i>Next Appointment: <oscar:nextAppt demographicNo="<%=demographicID%>"/></i></span>
                               	 	<%
									if(demographicPortalEnabled) {
									%>
                               		<input type="button" value="<bean:message key="yourcare.patientportal.pushToPortal"/>" title="Push To Portal" onclick="pushToPortal('acknowledgeForm_<%=segmentID%>');" />
                                	<span class="Field2"><i>Share Status: <%=syncStatus%></i></span>
                               		<%
									}
                               		%>
                                </td>
                            </tr>
                            <%
                            if (obgynShortcuts && isLinkedToDemographic) {
                            %>
                            <tr>
                                <td>
                                    <%
                                        ONPerinatal2017Dao onPerinatal2017Dao = SpringUtils.getBean(ONPerinatal2017Dao.class);
                                        String formId = onPerinatal2017Dao.getArFormIdIfNoPrForm(loggedInInfo, demographicID);
                                        if ("0".equals(formId)) {
                                    %>
                                        <input type="button" value="PR2-LAB"
                                               onClick="popupONAREnhanced(290, 775, '<%=request.getContextPath()%>/form/formONPerinatalForm.jsp?demographic_no=<%=demographicID%>&section='+this.value)"/>
                                        <input type="button" value="PR2-PGI"
                                               onClick="popupONAREnhanced(290, 775, '<%=request.getContextPath()%>/form/formONPerinatalForm.jsp?demographic_no=<%=demographicID%>&section='+this.value)"/>
                                        <input type="button" value="PR2-US"
                                               onClick="popupONAREnhanced(290, 775, '<%=request.getContextPath()%>/form/formONPerinatalForm.jsp?demographic_no=<%=demographicID%>&section='+this.value)"/>
                                        <input type="button" value="PR2"
                                               onClick="popupPage(700, 1024, '<%=request.getContextPath()%>/form/formONPerinatalRecord2.jsp?demographic_no=<%=demographicID%>&shortcut=true&update=true')"/>
                                        <input type="button" value="PR3"
                                               onClick="popupPage(700, 1024, '<%=request.getContextPath()%>/form/formONPerinatalRecord3.jsp?demographic_no=<%=demographicID%>&shortcut=true&update=true')"/>
                                    <%
                                        } else {
                                    %>
                                    <input type="button" value="AR1-ILI"
                                           onClick="popupONAREnhanced(290, 625, '<%=request.getContextPath()%>/form/formonarenhancedForm.jsp?demographic_no=<%=demographicID%>&formId=<%=formId%>&section='+this.value)"/>
                                    <input type="button" value="AR1-PGI"
                                           onClick="popupONAREnhanced(225, 590,'<%=request.getContextPath()%>/form/formonarenhancedForm.jsp?demographic_no=<%=demographicID%>&formId=<%=formId%>&section='+this.value)"/>
                                    <input type="button" value="AR2-US"
                                           onClick="popupONAREnhanced(395, 655, '<%=request.getContextPath()%>/form/formonarenhancedForm.jsp?demographic_no=<%=demographicID%>&formId=<%=formId%>&section='+this.value)"/>
                                    <input type="button" value="AR2-ALI"
                                           onClick="popupONAREnhanced(375, 430, '<%=request.getContextPath()%>/form/formonarenhancedForm.jsp?demographic_no=<%=demographicID%>&formId=<%=formId%>&section='+this.value)"/>
                                    <input type="button" value="AR2"
                                           onClick="popupPage(700, 1024, '<%=request.getContextPath()%>/form/formonarenhancedpg2.jsp?demographic_no=<%=demographicID%>&formId=<%=formId%>&update=true')"/>
                                    <%
                                    }
                                    %>
                                </td>
                            </tr>
                            <%
                            }
                            %>
                        </table>
                        <table width="100%" border="1" cellspacing="0" cellpadding="3" bgcolor="#9999CC" bordercolordark="#bfcbe3">
                            <%
                            if (multiLabId != null){
                                String[] multiID = multiLabId.split(",");
                                if (multiID.length > 1){
                                    %>
                                    <tr>
                                        <td class="Cell" colspan="2" align="middle">
                                            <div class="Field2">
                                                Version:&#160;&#160;
                                                <%
                                                for (int i=0; i < multiID.length; i++){
                                                    if (multiID[i].equals(segmentID)){
                                                        %>v<%= i+1 %>&#160;<%
                                                    }else{
                                                        if ( searchProviderNo != null ) { // null if we were called from e-chart
                                                            %><a href="labDisplay.jsp?segmentID=<%=multiID[i]%>&multiID=<%=multiLabId%>&providerNo=<%= providerNo %>&searchProviderNo=<%= searchProviderNo %>">v<%= i+1 %></a>&#160;<%
                                                        }else{
                                                            %><a href="labDisplay.jsp?segmentID=<%=multiID[i]%>&multiID=<%=multiLabId%>&providerNo=<%= providerNo %>">v<%= i+1 %></a>&#160;<%
                                                        }
                                                    }
                                                }
                                                if( multiID.length > 1 ) {
                                                    if ( searchProviderNo != null ) { // null if we were called from e-chart
                                                        %><a href="labDisplay.jsp?segmentID=<%=segmentID%>&multiID=<%=multiLabId%>&providerNo=<%= providerNo %>&searchProviderNo=<%= searchProviderNo %>&all=true">All</a>&#160;<%
                                                    }else{
                                                        %><a href="labDisplay.jsp?segmentID=<%=segmentID%>&multiID=<%=multiLabId%>&providerNo=<%= providerNo %>&all=true">All</a>&#160;<%
                                                    }
                                                }
                                                %>
                                            </div>
                                        </td>
                                    </tr>
                                    <%
                                }
                            }
                            %>
                            <tr>
                                <td width="66%" align="middle" class="Cell">
                                    <div class="Field2">
                                        <bean:message key="oscarMDS.segmentDisplay.formDetailResults"/>
                                    </div>
                                </td>
                                <td width="33%" align="middle" class="Cell">
                                    <div class="Field2">
                                        <bean:message key="oscarMDS.segmentDisplay.formResultsInfo"/>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="white" valign="top">
                                    <table valign="top" border="0" cellpadding="2" cellspacing="0" width="100%">
                                        <tr valign="top">
                                            <td valign="top" width="33%" align="left">
                                                <table width="100%" border="0" cellpadding="2" cellspacing="0" valign="top"  <% if ( !isLinkedToDemographic){ %> bgcolor="orange" <% } %> id="DemoTable<%=segmentID%>" >                                                    <tr>
                                                        <td valign="top" align="left">
                                                            <table valign="top" border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td nowrap>
                                                                        <div class="FieldData">
                                                                            <strong><bean:message key="oscarMDS.segmentDisplay.formPatientName"/>: </strong>
                                                                        </div>
                                                                    </td>
                                                                    <td nowrap>
                                                                        <div class="FieldData" nowrap="nowrap">
                                                                            <% if ( searchProviderNo == null ) { // we were called from e-chart%>
                                                                            <a href="javascript:window.close()">
                                                                            <% } else { // we were called from lab module%>
                                                                            <a href="javascript:void(0)" onclick="popupStart(360, 680,
                                                                                    '../../../oscarMDS/SearchPatient.do?providerNumber=<%= providerNo %>&labType=HL7&segmentID=<%=
                                                                                    segmentID %>&name=<%=java.net.URLEncoder.encode(handler.getLastName()+", "+handler.getFirstName())%>',
                                                                                    '<%= isLinkedToDemographic ? "encounter" : "searchPatientWindow" %>')">
                                                                                <% } %>
                                                                                <%= handler.getPatientName() %>
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                                    <td colspan="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap>
                                                                        <div class="FieldData">
                                                                            <strong><bean:message key="oscarMDS.segmentDisplay.formDateBirth"/>: </strong>
                                                                        </div>
                                                                    </td>
                                                                    <td nowrap>
                                                                        <div class="FieldData" nowrap="nowrap">
                                                                            <%=handler.getDOB()%>
                                                                        </div>
                                                                    </td>
                                                                    <td colspan="2"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap>
                                                                        <div class="FieldData">
                                                                            <strong><bean:message key="oscarMDS.segmentDisplay.formAge"/>: </strong>
                                                                        </div>
                                                                    </td>
                                                                    <td nowrap>
                                                                        <div class="FieldData">
                                                                            <%=handler.getAge()%>
                                                                        </div>
                                                                    </td>
                                                                    
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap>
                                                                        <div class="FieldData">
                                                                            <strong>
                                                                                <bean:message key="oscarMDS.segmentDisplay.formHealthNumber"/>
                                                                            </strong>
                                                                        </div>
                                                                    </td>
                                                                    <td nowrap>
                                                                        <div class="FieldData" nowrap="nowrap">
                                                                            <%=handler.getHealthNum()%>
                                                                        </div>
                                                                    </td>
                                                                    <td colspan="2"></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="33%" valign="top">
                                                            <table valign="top" border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td nowrap>
                                                                        <div align="left" class="FieldData">
                                                                            <strong><bean:message key="oscarMDS.segmentDisplay.formHomePhone"/>: </strong>
                                                                        </div>
                                                                    </td>
                                                                    <td nowrap>
                                                                        <div align="left" class="FieldData" nowrap="nowrap">
                                                                            <%=handler.getHomePhone()%>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap>
                                                                        <div align="left" class="FieldData">
                                                                            <strong><bean:message key="oscarMDS.segmentDisplay.formWorkPhone"/>: </strong>
                                                                        </div>
                                                                    </td>
                                                                    <td nowrap>
                                                                        <div align="left" class="FieldData" nowrap="nowrap">
                                                                            <%=handler.getWorkPhone()%>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                   <td nowrap>
                                                                        <div class="FieldData">
                                                                            <strong><bean:message key="oscarMDS.segmentDisplay.formSex"/>: </strong>
                                                                        </div>
                                                                    </td>
                                                                    <td align="left" nowrap>
                                                                        <div class="FieldData">
                                                                            <%=handler.getSex()%>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td nowrap>
                                                                        <div align="left" class="FieldData">
                                                                            <strong><bean:message key="oscarMDS.segmentDisplay.formPatientLocation"/>: </strong>
                                                                        </div>
                                                                    </td>
                                                                    <td nowrap>
                                                                        <div align="left" class="FieldData" nowrap="nowrap">
                                                                            <%=handler.getPatientLocation()%>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td bgcolor="white" valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="1">
                                        <tr>
                                            <td>
                                                <div class="FieldData">
                                                    <strong><bean:message key="oscarMDS.segmentDisplay.formDateService"/>:</strong>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="FieldData" nowrap="nowrap">
                                                    <%= handler.getServiceDate() %>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                        	<td>
                                        		<div class="FieldData">
                                                    <strong><bean:message key="oscarMDS.segmentDisplay.formDateReceived"/>:</strong>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="FieldData" nowrap="nowrap">
                                                    <%= dateLabReceived %>
                                                </div>
                                            </td>
                                        </tr>
                                      <% if (handler.getMsgType().equals("PATHL7")) { %>
                                      <tr>
                                        <td>
                                          <div class="FieldData">
                                            <strong><bean:message key="oscarMDS.segmentDisplay.formReportOn"/>:</strong>
                                          </div>
                                        </td>
                                        <td>
                                          <div class="FieldData" nowrap="nowrap">
                                            <%= handler.getReportedOnDate() %>
                                          </div>
                                        </td>
                                      </tr>
                                      <% } %>
                                        <tr>
                                            <td>
                                                <div class="FieldData">
                                                    <strong><bean:message key="oscarMDS.segmentDisplay.formReportStatus"/>:</strong>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="FieldData" nowrap="nowrap">
                                                    <%= LabUtils.getHl7OrderStatusString(handler.getMsgType(), handler.getOrderStatus()) %>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td nowrap>
                                                <div class="FieldData">
                                                    <strong><bean:message key="oscarMDS.segmentDisplay.formClientRefer"/>:</strong>
                                                </div>
                                            </td>
                                            <td nowrap>
                                                <div class="FieldData" nowrap="nowrap">
                                                    <%= handler.getClientRef()%>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="FieldData">
                                                    <strong><bean:message key="oscarMDS.segmentDisplay.formAccession"/>:</strong>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="FieldData" nowrap="nowrap">
                                                    <%= handler.getAccessionNum()%>
                                                </div>
                                            </td>
                                        </tr>
                                        <% if (handler.getMsgType().equals("ExcellerisON") && !((ExcellerisOntarioHandler)handler).getAlternativePatientIdentifier().isEmpty()) {  %>
                                          <tr>
                                            <td>
                                                <div class="FieldData">
                                                    <strong>Reference #:</strong>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="FieldData" nowrap="nowrap">
                                                    <%= ((ExcellerisOntarioHandler)handler).getAlternativePatientIdentifier()%>
                                                </div>
                                            </td>
                                        </tr>
                                        <% } %>  
                                        <% if (handler.getMsgType().equals("MEDVUE")) {  %>
                                        <tr>
                                        	<td>
                                                <div class="FieldData">
                                                    <strong>MEDVUE Encounter Id:</strong>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="FieldData" nowrap="nowrap">
                                                   <%= handler.getEncounterId() %>
                                                </div>
                                            </td>
                                        </tr>
                                        <% } 
                                        String comment = handler.getNteForPID();
                                        if (comment != null && !comment.equals("")) {%>
                                        <tr>
                                        	<td>
                                                <div class="FieldData">
                                                    <strong>Remarks:</strong>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="FieldData" nowrap="nowrap">
                                                   <%= comment %>
                                                </div>
                                            </td>
                                        </tr>
                                        <%} %>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="white" colspan="2">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
                                        <tr>
                                            <td bgcolor="white">
                                                <div class="FieldData">
                                                    <strong><bean:message key="oscarMDS.segmentDisplay.formRequestingClient"/>: </strong>
                                                    <%= handler.getDocName()%>
                                                </div>
                                            </td>
                                            <%-- <td bgcolor="white">
                                    <div class="FieldData">
                                        <strong><bean:message key="oscarMDS.segmentDisplay.formReportToClient"/>: </strong>
                                            <%= No admitting Doctor for CML messages%>
                                    </div>
                                </td> --%>
                                            <td bgcolor="white" align="right">
                                                <div class="FieldData">
                                                    <strong><bean:message key="oscarMDS.segmentDisplay.formCCClient"/>: </strong>
                                                    <%= handler.getCCDocs()%>

                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="white" colspan="2" style="padding:0px;" cellspacing="0">							    
<% if(demographicID!=null && !demographicID.equals("")){
							    TicklerManager ticklerManager = SpringUtils.getBean(TicklerManager.class);
							    List<Tickler> LabTicklers = null;
							    if(demographicID != null) {
							    	LabTicklers = ticklerManager.getTicklerByLabId(loggedInInfo, Integer.valueOf(segmentID), Integer.valueOf(demographicID));
							    }
							    
							    if(LabTicklers!=null && LabTicklers.size()>0){
							    %>
							    <div id="ticklerWrap" class="DoNotPrint">
							    <h3 style="color:#fff"><a href="javascript:void(0)" id="open-ticklers" onclick="showHideItem('ticklerDisplay')">View Ticklers</a> Linked to this Lab</h3><br>
							    
							           <div id="ticklerDisplay" style="display:none">
							   <%
							   String flag;
							   String ticklerClass;
							   String ticklerStatus;
							   for(Tickler tickler:LabTicklers){
							   
							   ticklerStatus = tickler.getStatus().toString();
							   if(!ticklerStatus.equals("C") && tickler.getPriority().toString().equals("High")){ 
							   	flag="<span style='color:red'>&#9873;</span>";
							   }else if(ticklerStatus.equals("C") && tickler.getPriority().toString().equals("High")){
							   	flag="<span>&#9873;</span>";
							   }else{	
							   	flag="";
							   }
							   
							   if(ticklerStatus.equals("C")){
							  	 ticklerClass = "completedTickler";
							   }else{
							  	 ticklerClass="";
							   }
							   %>	
							   <div style="text-align:left;background-color:#fff;padding:5px; width:600px;" class="<%=ticklerClass%>">
							   	<table width="100%">
							   	<tr>
							   	<td><b>Priority:</b> <%=flag%> <%=tickler.getPriority()%></td>
							   	<td><b>Service Date:</b> <%=tickler.getServiceDate()%></td>   	
							   	<td><b>Assigned To:</b> <%=tickler.getAssignee() != null ? Encode.forHtml(tickler.getAssignee().getLastName() + ", " + tickler.getAssignee().getFirstName()) : "N/A"%></td>
							   	<td width="90px"><b>Status:</b> <%=ticklerStatus.equals("C") ? "Completed" : "Active" %></td> 
							   	</tr>
							   	<tr>
							   	<td colspan="4"><%=Encode.forHtml(tickler.getMessage())%></td>
							   	</tr>
							   	</table>
							   </div>	
							   <br>
							   <%
							   }
							   %>
							   		</div><!-- end ticklerDisplay -->
							   </div>   
							   <%}//no ticklers to display 
}%>                     
                                
                                    <%String[] multiID = multiLabId.split(",");
                                    ReportStatus report;
                                    boolean startFlag = false;
                                    for (int j=multiID.length-1; j >=0; j--){
                                        ackList = AcknowledgementData.getAcknowledgements(multiID[j]);
                                        if (multiID[j].equals(segmentID))
                                            startFlag = true;
                                        if (startFlag) {
                                            //if (ackList.size() > 0){{%>
                                                <table width="100%" height="20" cellpadding="2" cellspacing="2">
                                                    <tr>
                                                        <% if (multiID.length > 1){ %>
                                                            <td align="center" bgcolor="white" width="20%" valign="top">
                                                                <div class="FieldData">
                                                                    <b>Version:</b> v<%= j+1 %>
                                                                </div>
                                                            </td>
                                                            <td align="left" bgcolor="white" width="80%" valign="top">
                                                        <% }else{ %>
                                                            <td align="center" bgcolor="white">
                                                        <% } %>
                                                            <div class="FieldData">
                                                                <!--center-->
                                                                    <% for (int i=0; i < ackList.size(); i++) {
                                                                        report = ackList.get(i); %>
                                                                        <%= Encode.forHtml(report.getProviderName()) %> :

                                                                        <% String ackStatus = report.getStatus();
                                                                            if(ackStatus.equals("A")){
                                                                                ackStatus = "Acknowledged";
                                                                            }else if(ackStatus.equals("F")){
                                                                                ackStatus = "Filed but not Acknowledged";
                                                                            }else{
                                                                                ackStatus = "Not Acknowledged";
                                                                            }
                                                                        %>
                                                                        <font color="red"><%= ackStatus %></font>
                                                                        <% if ( ackStatus.equals("Acknowledged") ) { %>
                                                                            <%= report.getTimestamp() %>,
                                                                        <% } %>
                                                                        <span id="<%=report.getOscarProviderNo() + "_" + segmentID%>commentLabel"><%=StringUtils.isEmpty(report.getRoutingComment()) ? "" : "comment : "%></span><span id="<%=report.getOscarProviderNo() + "_" + segmentID%>commentText"><%=report.getRoutingComment()==null ? "" : Encode.forHtml(report.getRoutingComment())%></span>
                                                                        <br>
                                                                    <% }
                                                                    if (ackList.size() == 0){
                                                                        %><font color="red">N/A</font><%
                                                                    }
                                                                    %>
                                                                <!--/center-->
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <% if (handler.isReportBlocked()) { %>
                                                    <tr>
                                                        <td bgcolor="white">
                                                            <div class="FieldData">
                                                                <span style="color:red; font-weight:bold">Blocked Test Result: Do Not Disclose Without Explicit Patient Consent</span>
                                                                <br/>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <% } %>
                                                </table>

                                            <%//}
                                        }
                                    }%>

                                </td>
                            </tr>
                        </table>

                        <% int i=0;
                        int j=0;
                        int k=0;
                        int l=0;
                        int linenum=0;
                        String highlight = "#E0E0FF";

                        ArrayList<String> headers = handler.getHeaders();
                        int OBRCount = handler.getOBRCount();
                        
                        if (handler.getMsgType().equals("MEDVUE")) { %>
<%-- MEDVUE Redirect. --%>
                        <table style="page-break-inside:avoid;" bgcolor="#003399" border="0" cellpadding="0" cellspacing="0" width="100%">
                           <tr>
                               <td colspan="4" height="7">&nbsp;</td>
                           </tr>
                           <tr>
                               <td bgcolor="#FFCC00" width="300" valign="bottom">
                                   <div class="Title2">
                                      <%=headers.get(0)%>
                                   </div>
                               </td>
                               <%--<td align="right" bgcolor="#FFCC00" width="100">&nbsp;</td>--%>
                               <td width="9">&nbsp;</td>
                               <td width="9">&nbsp;</td>
                               <td width="*" class="labReqDate">
                                   <% if (StringUtils.isNotEmpty(handler.getRequestDate(i))) { %>
                                   <b><bean:message key="oscarMDS.createLab.labReqDate" /></b><br/>
                                   <%=handler.getRequestDate(i)%>
                                   <% } %>
                               </td>
                           </tr>
                       </table>
                       <table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="#CCCCFF" bordercolor="#9966FF" bordercolordark="#bfcbe3" name="tblDiscs" id="tblDiscs">
                           <tr class="Field2">
                               <td width="25%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formTestName"/></td>
                               <td width="15%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formResult"/></td>
                               <td width="5%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formAbn"/></td>
                               <td width="15%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formReferenceRange"/></td>
                               <td width="10%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formUnits"/></td>
                               <td width="15%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formDateTimeCompleted"/></td>
                               <td width="6%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formNew"/></td>
                               <td width="6%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formAnnotate"/></td>
                               <%if(showCodes){%>
                               <td width="6%" align="middle" valign="bottom" class="Cell">Code</td>
                               <%}%>
                           </tr>
	                        <tr class="TDISRes">
		                      	<td valign="top" align="left" colspan="8" ><pre  style="margin:0px 0px 0px 100px;"><b>Radiologist: </b><b><%=handler.getRadiologistInfo()%></b></pre></td>
		                      	</td>
	                     	 </tr>
	                        <tr class="TDISRes">
		                       	<td valign="top" align="left" colspan="8"><pre  style="margin:0px 0px 0px 100px;"><b><%=handler.getOBXComment(1, 1, 1)%></b></pre></td>
		                       	</td>
		                       	<td align="center" valign="top">
                                    <a href="javascript:void(0);" title="Annotation" onclick="window.open('<%=request.getContextPath()%>/annotation/annotation.jsp?display=<%=annotation_display%>&amp;table_id=<%=segmentID%>&amp;demo=<%=demographicID%>&amp;other_id=<%=String.valueOf(1) + "-" + String.valueOf(1) %>','anwin','width=400,height=500');">
                                    	<img src="../../../images/notes.gif" alt="rxAnnotation" height="16" width="13" border="0"/>
                                    </a>
                                </td>
	                      	 </tr>
                     	 </table>
 <%-- ALL OTHERS Redirect. --%>                    	 
                     <% } else {

                      for(i=0;i<headers.size();i++){
                           linenum=0;
						boolean isUnstructuredDoc = false;
						boolean	isVIHARtf = false;
						boolean isSGorCDC = false;

						//Checks to see if the PATHL7 lab is an unstructured document, a VIHA RTF pathology report, or if the patient location is SG/CDC
						//labs that fall into any of these categories have certain requirements per Excelleris
						if(handler.getMsgType().equals("PATHL7")){
							isUnstructuredDoc = ((PATHL7Handler) handler).unstructuredDocCheck(headers.get(i));
							isVIHARtf = ((PATHL7Handler) handler).vihaRtfCheck(headers.get(i));
							if(handler.getPatientLocation().equals("SG") || handler.getPatientLocation().equals("CDC")){
								isSGorCDC = true;
							}
						}

						if( handler.getMsgType().equals("MEDITECH") ) {
							isUnstructuredDoc = ((MEDITECHHandler) handler).isUnstructured();
						}

						%>
		                       <table style="page-break-inside:avoid;" bgcolor="#003399" border="0" cellpadding="0" cellspacing="0" width="100%">
	                           <tr>
	                               <td colspan="4" height="7">&nbsp;</td>
	                           </tr>
	                           <tr>
	                               <td style="background: #FFCC00" width="300" valign="bottom">
	                                   <div class="Title2">
	                                       <%=headers.get(i)%>
	                                   </div>
	                               </td>
	                               <%--<td align="right" bgcolor="#FFCC00" width="100">&nbsp;</td>--%>
	                               <td width="9">&nbsp;</td>
	                               <td width="9">&nbsp;</td>
                                   <td width="*" class="labReqDate">
                                       <% if (StringUtils.isNotEmpty(handler.getRequestDate(i))) { %>
                                       <b><bean:message key="oscarMDS.createLab.labReqDate" /></b><br/>
                                       <%=handler.getRequestDate(i)%>
                                       <% } %>
                                   </td>
	                           </tr>
	                       </table>
	                   <% if ( ( handler.getMsgType().equals("MEDITECH") && isUnstructuredDoc) || 
	                		   ( handler.getMsgType().equals("MEDITECH") && ((MEDITECHHandler) handler).isReportData() ) ) { %>
	                       	<table style="width:100%;border-collapse:collapse;" bgcolor="#CCCCFF" bordercolor="#9966FF" bordercolordark="#bfcbe3" name="tblDiscs" id="tblDiscs" >	                  
	                       	<tr><td colspan="4" style="padding-left:10px;">

                       	<%} else if( isUnstructuredDoc){%>
	                       <table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="#CCCCFF" bordercolor="#9966FF" bordercolordark="#bfcbe3" name="tblDiscs" id="tblDiscs">

	                           <tr class="Field2">
	                               <td width="20%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formTestName"/></td>
	                               <td width="60%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formResult"/></td>
	                               <td width="20%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formDateTimeCompleted"/></td>
	                           </tr><%
						} else {%>
                       <table width="100%" border="0" cellspacing="0" cellpadding="2" bgcolor="#CCCCFF" bordercolor="#9966FF" bordercolordark="#bfcbe3" name="tblDiscs" id="tblDiscs">
                           
                           <% if(handler.getMsgType().equals("MEDITECH") && "MIC".equals( ((MEDITECHHandler) handler).getSendingApplication() ) ) { %>
	                          		<tr>
			                   			<td colspan="8" ></td>
		                   			</tr>
		                   			<tr>
			                   			<td valign="top" align="left" style="padding-left:20px;font-weight:bold;" >SPECIMEN SOURCE: </td>
			                   			<td valign="top" align="left" style="font-weight:bold;" colspan="7"><%= ((MEDITECHHandler) handler).getSpecimenSource(i) %></td>
		                   			</tr>
		                   			<tr>
			                   			<td valign="top" align="left" style="padding-left:20px;font-weight:bold;">SPECIMEN DESCRIPTION: </td>
			                   			<td valign="top" align="left" style="font-weight:bold;" colspan="7"><%= ((MEDITECHHandler) handler).getSpecimenDescription(i) %></td>
		                   			</tr>
		                   			<tr>
			                   			<td colspan="8" ></td>
		                   			</tr>
		                   		<% }%>
                           
                           <tr class="Field2">
                               <td width="25%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formTestName"/></td>
                               <td width="15%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formResult"/></td>
                               <td width="5%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formAbn"/></td>
                               <td width="15%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formReferenceRange"/></td>
                               <td width="10%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formUnits"/></td>
                               <td width="15%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formDateTimeCompleted"/></td>
                               <td width="6%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formNew"/></td>
                          	   <td width="6%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formAnnotate"/></td>
                               <%if(showCodes){%>
                               <td width="6%" align="middle" valign="bottom" class="Cell">Code</td>
                               <%}%>
                               <% if ("ExcellerisON".equals(handler.getMsgType())) { %>
                          	   	<td width="6%" align="middle" valign="bottom" class="Cell">License #</td>
                          	   </tr>
                          	   <% } %>
                           </tr>
                           
 							<%if("CLS".equals(handler.getMsgType())) { %>
 							<%for (k=0; k < handler.getOBRCommentCount(j); k++){
                                   %>
                               <tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="NormalRes">
                                   <td valign="top" align="left" colspan="8"><pre><%=handler.getOBRComment(j, k)%></pre></td>
                               </tr>
                              <%
                                }//end for k=0
 							}//end of CLS check
                           }
                           
                           for ( j=0; j < OBRCount; j++){

                               boolean obrFlag = false;
                               int obxCount = handler.getOBXCount(j);
                               for (k=0; k < obxCount; k++){

                               	String obxName = handler.getOBXName(j, k);
                               	String nameLong = handler.getOBXNameLong(j ,k);
                               	if (StringUtils.isNotEmpty(nameLong)) {
                               	    nameLong = ": " + nameLong;
                                }

								boolean isAllowedDuplicate = false;
								if(handler.getMsgType().equals("PATHL7")){
									//if the obxidentifier and result name are any of the following, they must be displayed (they are the Excepetion to Excelleris TX/FT duplicate result name display rules)
									if((handler.getOBXName(j, k).equals("Culture") && handler.getOBXIdentifier(j, k).equals("6463-4")) || 
									(handler.getOBXName(j, k).equals("Organism") && (handler.getOBXIdentifier(j, k).equals("X433") || handler.getOBXIdentifier(j, k).equals("X30011")))){
					   					isAllowedDuplicate = true;
					   				}
								}
                                   boolean b1=false, b2=false, b3=false;

                                   boolean fail = true;
                                   String header = "";
                                   try {
                                   b1 = !handler.getOBXResultStatus(j, k).equals("DNS");
                                 	b2 = !obxName.equals("");
                                   header = headers.get(i);
                                   String obsHeader = handler.getObservationHeader(j,k);
                                   b3 = handler.getObservationHeader(j, k).equals(header);
                                   fail = false;
                                 } catch (Exception e){
                                   	//logger.info("ERROR :"+e);
                                   }

                                   if ( handler.getMsgType().equals("MEDITECH") ) {
                                	   b2=true;
                                	   if (isUnstructuredDoc) {
                                           b3=true;
                                       }
                                   } if (handler.getMsgType().equals("EPSILON")) {
                                     b2 = true;
                                   	b3 = header.equals(handler.getOBXIdentifier(j, k));
                                   } else if(handler.getMsgType().equals("CML") || handler.getMsgType().equals("HHSEMR")) {
                                   	b2=true;
                                   }

                                    if (!fail && b1 && b2 && b3){ // <<--  DNS only needed for MDS messages
																		String lineClass = "NormalRes";
                                   	String obrName = handler.getOBRName(j);
																		String obrResult = handler.getOBRResultStatus(j);
                                   	if (!obrFlag && !obrName.equals("") && (!(obxName.contains(obrName) && obxCount < 2 && !isUnstructuredDoc)) && !obrName.equals(header)) {
                                       %>
                                           <tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="<%= lineClass %>" >
                                               <td valign="top" align="left"><span style="font-size:16px;font-weight: bold;"><%=obrName%></span></td>
																						 <% if (("PATHL7").equals(handler.getMsgType())) { %>
																							 <td align="right">
																								 <%=
																								 "I".equals(obrResult) ? "Results pending ..." : ""
																								 %>
																							 </td>
																						   <td colspan="5">&nbsp;</td>
																						 <% } else { %>
																						   <td colspan="6">&nbsp;</td>
																						 <% } %>
                                           </tr>
                                           <%obrFlag = true;
                                       }
                                       String abnormal = handler.getOBXAbnormalFlag(j, k);
                                       if ( abnormal != null && abnormal.startsWith("L")){
                                           lineClass = "HiLoRes";
                                       } else if ( abnormal != null && ( abnormal.equals("A") || abnormal.startsWith("H") || handler.isOBXAbnormal( j, k) ) ){
                                           lineClass = "AbnormalRes";
                                       }

                                       boolean isPrevAnnotation = false;
                                       CaseManagementNoteLink cml = caseManagementManager.getLatestLinkByTableId(CaseManagementNoteLink.LABTEST,Long.valueOf(segmentID),j+"-"+k);
                                       CaseManagementNote p_cmn = null;
                                       if (cml!=null) {p_cmn = caseManagementManager.getNote(cml.getNoteId().toString());}
                                       if (p_cmn!=null){isPrevAnnotation=true;}

                                       String loincCode = null;
                                       try{
                                       	List<MeasurementMap> mmapList =  measurementMapDao.getMapsByIdent(handler.getOBXIdentifier(j, k));
                                       	if (mmapList.size()>0) {
	                                    	MeasurementMap mmap =mmapList.get(0);
	                                       	loincCode = mmap.getLoincCode();
                                       	}
                                       }catch(Exception e){
                                        	MiscUtils.getLogger().error("loincProb",e);
                                       }

                                       if (handler.getMsgType().equals("EPSILON")) {
	                                    	   if (handler.getOBXIdentifier(j,k).equals(headers.get(i)) && !obxName.equals("")) { %>

	                                        	<tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="<%=lineClass%>">
		                                            <td valign="top" align="left"><%= obrFlag ? "&nbsp; &nbsp; &nbsp;" : "&nbsp;" %>
                                                        <% if(isLinkedToDemographic) {%>
                                                        <a href="javascript:popupStart('660','900','../ON/labValues.jsp?testName=<%=URLEncoder.encode(obxName.replaceAll("#", "%23"), "UTF-8")%>&demo=<%=demographicID%>&labType=HL7&identifier=<%= URLEncoder.encode(handler.getOBXIdentifier(j, k).replaceAll("&","%26").replaceAll("#", "%23"),"UTF-8") %>')"><%=obxName %></a>&nbsp;<%if(loincCode != null){ %><a href="javascript:popupStart('660','1000','http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.1&mainSearchCriteria.v.c=<%=loincCode%>&informationRecipient.languageCode.c=en')"> info</a><%} %>
                                                        <% } else { %>
                                                        <%= obxName %>
                                                        <% } %>
                                                	</td>
		                                            <td align="right"><%= handler.getOBXResult( j, k) %></td>

		                                            <td align="center">
		                                                    <%= handler.getOBXAbnormalFlag(j, k)%>
		                                            </td>
		                                            <td align="left"><%=handler.getOBXReferenceRange( j, k)%></td>
		                                            <td align="left"><%=handler.getOBXUnits( j, k) %></td>
		                                            <td align="center"><%= handler.getTimeStamp(j, k) %></td>
		                                            <td align="center"><%= handler.getOBXResultStatus( j, k) %></td>
		                                            <td align="center" valign="top">
	                                                <a href="javascript:void(0);" title="Annotation" onclick="window.open('<%=request.getContextPath()%>/annotation/annotation.jsp?display=<%=annotation_display%>&amp;table_id=<%=segmentID%>&amp;demo=<%=demographicID%>&amp;other_id=<%=String.valueOf(j) + "-" + String.valueOf(k) %>','anwin','width=400,height=500');">
	                                                	<%if(!isPrevAnnotation){ %><img src="../../../images/notes.gif" alt="rxAnnotation" height="16" width="13" border="0"/><%}else{ %><img src="../../../images/filledNotes.gif" alt="rxAnnotation" height="16" width="13" border="0"/> <%} %>
	                                                </a>
                                                </td>
                                                    <% if(showCodes){%>
                                                    <td align="center"><%= handler.getOBXIdentifier(j, k) %></td>
                                                    <%}%>
	                                       		</tr>
	                                       <% } else if (handler.getOBXIdentifier(j,k).equals(headers.get(i)) && obxName.equals("")) { %>
	                                       			<tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="NormalRes">
                                                        <td valign="top" align="left" colspan="8"><pre  style="margin:0px 0px 0px 100px;"><%=handler.getOBXResult( j, k)%></pre></td>

                                                    </tr>
	                                       	<% }

                                      } else if (handler.getMsgType().equals("PFHT") || handler.getMsgType().equals("HHSEMR") || handler.getMsgType().equals("CML")) {
                                   	   if (!obxName.equals("")) { %>
	                                    		<tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="<%=lineClass%>">
		                                            <td valign="top" align="left"><%= obrFlag ? "&nbsp; &nbsp; &nbsp;" : "&nbsp;" %><a href="javascript:popupStart('660','900','../ON/labValues.jsp?testName=<%=URLEncoder.encode(obxName.replaceAll("#", "%23"), "UTF-8")%>&demo=<%=demographicID%>&labType=HL7&identifier=<%= URLEncoder.encode(handler.getOBXIdentifier(j, k).replaceAll("&","%26").replaceAll("#", "%23"),"UTF-8") %>')"><%=obxName %></a>&nbsp;<%if(loincCode != null){ %><a href="javascript:popupStart('660','1000','http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.1&mainSearchCriteria.v.c=<%=loincCode%>&informationRecipient.languageCode.c=en')"> info</a><%} %>
                                                	</td>
		                                            <td align="right"><%= handler.getOBXResult( j, k) %></td>

		                                            <td align="center">
		                                                    <%= handler.getOBXAbnormalFlag(j, k)%>
		                                            </td>
		                                            <td align="left"><%=handler.getOBXReferenceRange( j, k)%></td>
		                                            <td align="left"><%=handler.getOBXUnits( j, k) %></td>
		                                            <td align="center"><%= handler.getTimeStamp(j, k) %></td>
		                                            <td align="center"><%= handler.getOBXResultStatus( j, k) %></td>
		                                            <td align="center" valign="top">
	                                                <a href="javascript:void(0);" title="Annotation" onclick="window.open('<%=request.getContextPath()%>/annotation/annotation.jsp?display=<%=annotation_display%>&amp;table_id=<%=segmentID%>&amp;demo=<%=demographicID%>&amp;other_id=<%=String.valueOf(j) + "-" + String.valueOf(k) %>','anwin','width=400,height=500');">
	                                                	<%if(!isPrevAnnotation){ %><img src="../../../images/notes.gif" alt="rxAnnotation" height="16" width="13" border="0"/><%}else{ %><img src="../../../images/filledNotes.gif" alt="rxAnnotation" height="16" width="13" border="0"/> <%} %>
	                                                </a>
                                                </td>
                                                    <% if(showCodes){%>
                                                    <td align="center"><%= handler.getOBXIdentifier(j, k) %></td>
                                                    <%}%>
	                                       		 </tr>

                                   	 	<%} else { %>
                                   		   <tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="NormalRes">
	      	                                     <td valign="top" align="left" colspan="8"><pre  style="margin:0px 0px 0px 100px;"><%=handler.getOBXResult( j, k)%></pre></td>

	      	                                   </tr>
                                   	 	<%}
	                                    	if (!handler.getNteForOBX(j,k).equals("") && handler.getNteForOBX(j,k)!=null) { %>
		                                       <tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="NormalRes">
		                                       		<td valign="top" align="left"colspan="8"><pre  style="margin:0px 0px 0px 100px;"><%=handler.getNteForOBX(j,k)%></pre></td>
		                                       </tr>
		                                    <% }
			                                for (l=0; l < handler.getOBXCommentCount(j, k); l++){%>
			                                     <tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="NormalRes">
			                                        <td valign="top" align="left" colspan="8"><pre  style="margin:0px 0px 0px 100px;"><%=handler.getOBXComment(j, k, l)%></pre></td>
			                                     </tr>
			                                <%}

                                   } else if ((!handler.getOBXResultStatus(j, k).equals("TDIS") && handler.getMsgType().equals("Spire")) )  { %>
											<tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="<%=lineClass%>">
                                           <td valign="top" align="left"><%= obrFlag ? "&nbsp; &nbsp; &nbsp;" : "&nbsp;" %><a href="javascript:popupStart('660','900','../ON/labValues.jsp?testName=<%=URLEncoder.encode(obxName.replaceAll("#", "%23"), "UTF-8")%>&demo=<%=demographicID%>&labType=HL7&identifier=<%= URLEncoder.encode(handler.getOBXIdentifier(j, k).replaceAll("&","%26").replaceAll("#", "%23"),"UTF-8") %>')"><%=obxName %></a>&nbsp;<%if(loincCode != null){ %> <a href="javascript:popupStart('660','1000','http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.1&mainSearchCriteria.v.c=<%=loincCode%>&informationRecipient.languageCode.c=en')"> info</a><%} %>
                                                	 </td>
                                           <% 	if (handler.getOBXResult( j, k).length() > 20) {
													%>

													<td align="left" colspan="4"><%= handler.getOBXResult( j, k) %></td>

													<% 	String abnormalFlag = handler.getOBXAbnormalFlag(j, k);
														if (abnormalFlag != null && abnormalFlag.length() > 0) {
													 %>
		                                           <td align="center">
		                                                   <%= abnormalFlag%>
		                                           </td>
		                                           <% } %>

		                                           <% 	String refRange = handler.getOBXReferenceRange(j, k);
														if (refRange != null && refRange.length() > 0) {
													 %>
		                                           <td align="left"><%=refRange%></td>
		                                           <% } %>

		                                           <% 	String units = handler.getOBXUnits(j, k);
														if (units != null && units.length() > 0) {
													 %>
		                                           <td align="left"><%=units %></td>
		                                           <% } %>
												<%
												} else {
												%>
												   <td align="right" colspan="1"><%= handler.getOBXResult( j, k) %></td>
		                                           <td align="center"> <%= handler.getOBXAbnormalFlag(j, k)%> </td>
		                                           <td align="left"> <%=handler.getOBXReferenceRange(j, k)%> </td>
		                                           <td align="left"> <%=handler.getOBXUnits(j, k) %> </td>
												<%
												}
												%>

                                           <td align="center"><%= handler.getTimeStamp(j, k) %></td>
                                           <td align="center"><%= handler.getOBXResultStatus(j, k) %></td>

                                      		<td align="center" valign="top">
	                                                <a href="javascript:void(0);" title="Annotation" onclick="window.open('<%=request.getContextPath()%>/annotation/annotation.jsp?display=<%=annotation_display%>&amp;table_id=<%=segmentID%>&amp;demo=<%=demographicID%>&amp;other_id=<%=String.valueOf(j) + "-" + String.valueOf(k) %>','anwin','width=400,height=500');">
	                                                	<%if(!isPrevAnnotation){ %><img src="../../../images/notes.gif" alt="rxAnnotation" height="16" width="13" border="0"/><%}else{ %><img src="../../../images/filledNotes.gif" alt="rxAnnotation" height="16" width="13" border="0"/> <%} %>
	                                                </a>
                                                </td>
                                                <% if(showCodes){%>
                                                <td align="center"><%= handler.getOBXIdentifier(j, k) %></td>
                                                <%}%>
                                       </tr>

                                       <%for (l=0; l < handler.getOBXCommentCount(j, k); l++){%>
                                            <tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="NormalRes">
                                               <td valign="top" align="left" colspan="8"><pre  style="margin:0px 0px 0px 100px;"><%=handler.getOBXComment(j, k, l)%></pre></td>
                                            </tr>
                                       <%}


                                    } else if (handler.getMsgType().equals("MEDITECH") && isUnstructuredDoc) { %>
                                       <tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="<%="NarrativeRes"%>">
                                           <td>
                                                <pre>
                                                    <%= handler.getOBXResult(j,k) %>
                                                </pre>
                                           </td>
                                       </tr>
                                    <%  
                                    } else if ((!handler.getOBXResultStatus(j, k).equals("TDIS") && !handler.getMsgType().equals("EPSILON")) )  {

                                    	if(isUnstructuredDoc){ %>

                                   			<tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="<%="NarrativeRes"%>"><% 
                                   			if(handler.getOBXIdentifier(j, k).equalsIgnoreCase(handler.getOBXIdentifier(j, k-1)) && (obxCount>1) && ! handler.getMsgType().equals("MEDITECH") ){%>
                                   				<td valign="top" align="left"><%= obrFlag ? "&nbsp; &nbsp; &nbsp;" : "&nbsp;" %><a href="javascript:popupStart('660','900','../ON/labValues.jsp?testName=<%=URLEncoder.encode(obxName.replaceAll("#", "%23"), "UTF-8")%>&demo=<%=demographicID%>&labType=HL7&identifier='<%= URLEncoder.encode(handler.getOBXIdentifier(j, k).replaceAll("&","%26").replaceAll("#", "%23"),"UTF-8")%>')"></a>
                                                        <%
                                   			} else if(! handler.getMsgType().equals("MEDITECH") ) { %>
                                                <td valign="top" align="left"><%= obrFlag ? "&nbsp; &nbsp; &nbsp;" : "&nbsp;" %><a href="javascript:popupStart('660','900','../ON/labValues.jsp?testName=<%=URLEncoder.encode(obxName.replaceAll("#", "%23"), "UTF-8")%>&demo=<%=demographicID%>&labType=HL7&identifier=<%= URLEncoder.encode(handler.getOBXIdentifier(j, k).replaceAll("&","%26").replaceAll("#", "%23"),"UTF-8") %>')"><%=obxName %></a>
                                            <%}%>
											<% if(isVIHARtf) {
												
											    //create bytes from the rtf string
										    	byte[] rtfBytes = handler.getOBXResult(j, k).getBytes();
										    	ByteArrayInputStream rtfStream = new ByteArrayInputStream(rtfBytes);
										    	
										    	//Use RTFEditor Kit to get plaintext from RTF
										    	RTFEditorKit rtfParser = new RTFEditorKit();
										    	javax.swing.text.Document doc = rtfParser.createDefaultDocument();
										    	rtfParser.read(rtfStream, doc, 0);
										    	String rtfText = doc.getText(0, doc.getLength()).replaceAll("\n", "<br>");
										    	String disclaimer = "<br>IMPORTANT DISCLAIMER: You are viewing a PREVIEW of the original report. The rich text formatting contained in the original report may convey critical information that must be considered for clinical decision making. Please refer to the ORIGINAL report, by clicking 'Print', prior to making any decision on diagnosis or treatment.";%>
										    	<td align="left"><%= rtfText + disclaimer %></td><%}
											else {%>
                                           		<td align="left">
	                                           		<span>
	                                           			<%= handler.getOBXResult( j, k) %>
	                                           		</span>
                                           		</td><%} %>
                                           	<%if(handler.getTimeStamp(j, k).equals(handler.getTimeStamp(j, k-1)) && (obxCount>1)){
                                        			%><td align="center"></td><%}
                                        		else{%> <td align="center"><%= handler.getTimeStamp(j, k) %></td><%}%>
<%--
                                           	<td align="center" valign="top">
                                           		<a href="javascript:void(0);" title="Annotation" onclick="window.open('<%=request.getContextPath()%>/annotation/annotation.jsp?display=<%=annotation_display%>&amp;table_id=<%=segmentID%>&amp;demo=<%=demographicID%>&amp;other_id=<%=String.valueOf(j) + "-" + String.valueOf(k) %>','anwin','width=400,height=500');">
		                                        	<%if(!isPrevAnnotation){ %><img src="../../../images/notes.gif" alt="rxAnnotation" height="16" width="13" border="0"/><%}else{ %><img src="../../../images/filledNotes.gif" alt="rxAnnotation" height="16" width="13" border="0"/> <%} %>
		                                        </a>
		                                    </td>
--%>
                                                    <% if(showCodes){%>
                                                    <td align="center"><%= handler.getOBXIdentifier(j, k) %></td>
                                                    <%}%>
                                   			<% }//end of isUnstructuredDoc

                                   			else{//if it isn't a PATHL7 doc%>
                               		<tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="<%=lineClass%>"><%
                               				if(handler.getMsgType().equals("PATHL7") && !isAllowedDuplicate && (obxCount>1) && handler.getOBXIdentifier(j, k).equalsIgnoreCase(handler.getOBXIdentifier(j, k-1)) && (handler.getOBXValueType(j, k).equals("TX") || handler.getOBXValueType(j, k).equals("FT"))){%>
                                   				<td valign="top" align="left"><%= obrFlag ? "&nbsp; &nbsp; &nbsp;" : "&nbsp;" %><a href="javascript:popupStart('660','900','../ON/labValues.jsp?testName=<%=URLEncoder.encode(obxName.replaceAll("#", "%23"), "UTF-8")%>&demo=<%=demographicID%>&labType=HL7&identifier=<%= URLEncoder.encode(handler.getOBXIdentifier(j, k).replaceAll("&","%26").replaceAll("#", "%23"),"UTF-8") %>')"></a>
                                                        <%=nameLong%> <br/>
                                           <% } else{%>
                                           <td valign="top" align="left"><%= obrFlag ? "&nbsp; &nbsp; &nbsp;" : "&nbsp;" %><a href="javascript:popupStart('660','900','../ON/labValues.jsp?testName=<%=URLEncoder.encode(obxName.replaceAll("#", "%23"), "UTF-8")%>&demo=<%=demographicID%>&labType=HL7&identifier=<%= URLEncoder.encode(handler.getOBXIdentifier(j, k).replaceAll("&","%26").replaceAll("#", "%23"),"UTF-8") %>')"><%=obxName %></a>&nbsp;<%if(loincCode != null){ %><a href="javascript:popupStart('660','1000','http://apps.nlm.nih.gov/medlineplus/services/mpconnect.cfm?mainSearchCriteria.v.cs=2.16.840.1.113883.6.1&mainSearchCriteria.v.c=<%=loincCode%>&informationRecipient.languageCode.c=en')"> info</a><%} %>
                                                </td><%}%>
                                           <% if(handler instanceof AlphaHandler && "FT".equals(handler.getOBXValueType(j, k))) { %>
                                                <td colspan="4"><pre style="font-family:Courier New, monospace;">       <%= handler.getOBXResult( j, k) %></pre></td>
                                           <%
                                           } else if(handler instanceof PATHL7Handler && "FT".equals(handler.getOBXValueType(j, k))){
                                        	  %> <td colspan="4"><%= handler.getOBXResult( j, k) %></td> <%
                                           } else {
                                           	String align = "right";
                                          	//for pathl7, if it is an SG/CDC result greater than 100 characters, left justify it
                                           	if((handler.getOBXResult(j, k) != null && handler.getOBXResult(j, k).length() > 100) && (isSGorCDC)){
                                           		align="left";
                                           	}%>

                                           	<%
                                           		//CLS textual results - use 4 columns.
                                           		if(handler instanceof CLSHandler && ((oscar.oscarLab.ca.all.parsers.CLSHandler)handler).isUnstructured()) {
                                           	%>
                                           		<td align="left" colspan="4"><%= handler.getOBXResult( j, k) %></td>
                                           	<%		
                                           		} 
                                           		
                                           		else if(handler.getMsgType().equals("MEDITECH")  && isUnstructuredDoc ) {  
                                            %>
                                                    <pre>
                                                    <%= handler.getOBXResult(j,k) %>
                                                    </pre>

                                            <%} else if(handler.getMsgType().equals("MEDITECH")  && ((MEDITECHHandler) handler).isReportData() ) { %>
                                                   <tr>
                                                       <td>
                                                           <%= handler.getOBXResult(j,k) %>
                                                       </td>
                                                   </tr>
                                            <%
                                           		}
                                           		// else {
                                           	%>
											
											<%
												if((handler.getMsgType().equals("ExcellerisON") || handler.getMsgType().equals("PATHL7")) && handler.getOBXValueType(j,k).equals("ED")) {
													String legacy = "";
													if(handler.getMsgType().equals("PATHL7") && ((PATHL7Handler)handler).isLegacy(j,k) ) {
														legacy ="&legacy=true";
													}
												
												%>	
													 <td align="<%=align%>">PDF Report (<a href="<%=request.getContextPath() %>/lab/DownloadEmbeddedDocumentFromLab.do?labNo=<%=segmentID%>&segment=<%=j%>&group=<%=k%><%=legacy%>">Download</a>)</td>
													 <%
												} else {
											%>
                                           <td align="<%=align%>"><%= handler.getOBXResult( j, k) %></td>
                                          
                                          	<% } %>
                                           <td align="center">
                                                   <%= handler.getOBXAbnormalFlag(j, k)%>
                                           </td>
                                           <td align="left"><%=handler.getOBXReferenceRange( j, k)%></td>
                                           <td align="left"><%=handler.getOBXUnits( j, k) %></td>
                                           
                                        <% } %>
                                           <td align="center"><%= handler.getTimeStamp(j, k) %></td>
                                           <td align="center"><%= handler.getOBXResultStatus( j, k) %></td>
                                      		<td align="center" valign="top">                                           <a href="javascript:void(0);" title="Annotation" onclick="window.open('<%=request.getContextPath()%>/annotation/annotation.jsp?display=<%=annotation_display%>&amp;table_id=<%=segmentID%>&amp;demo=<%=demographicID%>&amp;other_id=<%=String.valueOf(j) + "-" + String.valueOf(k) %>','anwin','width=400,height=500');">
	                                                	<%if(!isPrevAnnotation){ %><img src="../../../images/notes.gif" alt="rxAnnotation" height="16" width="13" border="0"/><%}else{ %><img src="../../../images/filledNotes.gif" alt="rxAnnotation" height="16" width="13" border="0"/> <%} %>
	                                                </a>
                                                </td>
                                           <% if(showCodes){%>
                                            <td align="center"><%= handler.getOBXIdentifier(j, k) %></td>
                                           <%}%>

                                            <% if ("ExcellerisON".equals(handler.getMsgType())) { 
                                            	lastLicenseNo = currentLicenseNo;
                        						currentLicenseNo = ((ExcellerisOntarioHandler)handler).getLabLicenseNo(j, k);
                        						String licenseName = ((ExcellerisOntarioHandler)handler).getLabLicenseName(j, k);
                        						if(!allLicenseNames.contains(licenseName)) {
                        							allLicenseNames.add(licenseName);
                        						}
                                            %>
                                            	<td><%= !currentLicenseNo.equals(lastLicenseNo)?currentLicenseNo:""%></td>
                                            <% } %>
                                       </tr>
                                        <% 
                                            if(embedPdf){
                                                if((handler.getMsgType().equals("ExcellerisON") || handler.getMsgType().equals("PATHL7")) && handler.getOBXValueType(j,k).equals("ED")){
                                                String legacy = "";
                                                if(handler.getMsgType().equals("PATHL7") && ((PATHL7Handler)handler).isLegacy(j,k) ) {
                                                    legacy ="&legacy=true";
                                                }
                                        %>
                                            <tr>
                                                <td colspan="8">
                                                    <object data="<%=request.getContextPath() %>/lab/DisplayEmbeddedDocumentFromLab.do?labNo=<%=segmentID%>&segment=<%=j%>&group=<%=k%><%=legacy%>" width="100%" height="600 px" type="application/pdf" style="text-align: center"><i>OSCAR Message: </i>Could not display preview (the file is too large or has errors)<br>Please use the download link above</object> 
                                                </td>
                                            </tr>
                                        <% 
                                                }
                                            }
                                   		}

                                        for (l=0; l < handler.getOBXCommentCount(j, k); l++){%>
                                        <tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="NormalRes">
                                           <td valign="top" align="left" colspan="8"><pre  style="margin:0px 0px 0px 100px;"><%=handler.getOBXComment(j, k, l)%></pre></td>
                                        </tr>
                                   <%}


                                    } else { %>
                                       	<%for (l=0; l < handler.getOBXCommentCount(j, k); l++){
                                       			if (!handler.getOBXComment(j, k, l).equals("")) {
                                       		%>
                                            <tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="TDISRes">
                                               <td valign="top" align="left" colspan="8"><pre  style="margin:0px 0px 0px 100px;"><%=handler.getOBXComment(j, k, l)%></pre></td>
                                            	<td align="center" valign="top">
	                                                <a href="javascript:void(0);" title="Annotation" onclick="window.open('<%=request.getContextPath()%>/annotation/annotation.jsp?display=<%=annotation_display%>&amp;table_id=<%=segmentID%>&amp;demo=<%=demographicID%>&amp;other_id=<%=String.valueOf(1) + "-" + String.valueOf(1) %>','anwin','width=400,height=500');">
	                                                	<%if(!isPrevAnnotation){ %><img src="../../../images/notes.gif" alt="rxAnnotation" height="16" width="13" border="0"/><%}else{ %><img src="../../../images/filledNotes.gif" alt="rxAnnotation" height="16" width="13" border="0"/> <%} %>
	                                                </a>
                                             </td>
                                            </tr>
                                       			<%}
                                       	} %>


                                 <%  }

                                    }

                                    
                               }
                           //}


                           //for ( j=0; j< OBRCount; j++){
                           if (!handler.getMsgType().equals("PFHT") && !handler.getMsgType().equals("CLS")) {
                               if (headers.get(i).equals(handler.getObservationHeader(j, 0))) {
                               	 %>
                               <%for (k=0; k < handler.getOBRCommentCount(j); k++){
                                   // the obrName should only be set if it has not been
                                   // set already which will only have occured if the
                                   // obx name is "" or if it is the same as the obr name
                                   if(!obrFlag && handler.getOBXName(j, 0).equals("")){  %>
                                       <tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" >
                                           <td valign="top" align="left"><%=handler.getOBRName(j)%> </td>
                                           <td colspan="6">&nbsp;</td>
                                       </tr>
                                       <%obrFlag = true;
                                   }%>
                               <tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" class="NormalRes">
                                   <td valign="top" align="left" colspan="8"><pre  style="margin:0px 0px 0px 100px;"><%=handler.getOBRComment(j, k)%></pre></td>
                               </tr>
                               <% if  (!handler.getMsgType().equals("HHSEMR") || !handler.getMsgType().equals("TRUENORTH")) {
                               		if(handler.getOBXName(j,k).equals("")){
	                                        String result = handler.getOBXResult(j, k);
                               %>
	                                         <tr bgcolor="<%=(linenum % 2 == 1 ? highlight : "")%>" >
	                                                 <td colspan="7" valign="top"  align="left"><%=result%></td>
	                                         </tr>
	                              		<%}
                               	}
                                }//end for k=0


                             }//end if handler.getObservation..
                          } // end for if (PFHT)

                              } //end for j=0; j<obrCount;
                          } // // end for headersfor i=0... (headers) line 625
                         
							if (handler.getMsgType().equals("Spire")) {

								int numZDS = ((SpireHandler)handler).getNumZDSSegments();
								String lineClass = "NormalRes";
								int lineNumber = 0;
								MiscUtils.getLogger().info("HERE: " + numZDS);

								if (numZDS > 0) { %>
									<tr class="Field2">
		                               <td width="25%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formTestName"/></td>
		                               <td width="15%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formResult"/></td>
		                               <td width="15%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formProvider"/></td>
		                               <td width="15%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formDateTimeCompleted"/></td>
		                               <td width="6%" align="middle" valign="bottom" class="Cell"><bean:message key="oscarMDS.segmentDisplay.formNew"/></td>
                                        <%if(showCodes){%>
                                        <td width="6%" align="middle" valign="bottom" class="Cell">Code</td>
                                        <%}%>
		                            </tr>
								<%
								}

								for (int m=0; m < numZDS; m++) {
									%>
									<tr bgcolor="<%=(lineNumber % 2 == 1 ? highlight : "")%>" class="<%=lineClass%>">
										<td valign="top" align="left"> <%=((SpireHandler)handler).getZDSName(m)%> </td>
										<td align="right"><%= ((SpireHandler)handler).getZDSResult(m) %></td>
										<td align="center"><%= ((SpireHandler)handler).getZDSProvider(m) %></td>
										<td align="center"><%= ((SpireHandler)handler).getZDSTimeStamp(m) %></td>
										<td align="center"><%= ((SpireHandler)handler).getZDSResultStatus(m) %></td>
									</tr>
									<%
									lineNumber++;
								}
							}

                           %>
                       </table>
                       <%

                       } // end for handler.getMsgType().equals("MEDVUE")

                       %>
<%-- FOOTER --%>
                        <table width="100%" border="0" cellspacing="0" cellpadding="3" class="MainTableBottomRowRightColumn" bgcolor="#003399">
                            <tr>
                                <td align="left" width="50%">
                                    <% if (!hideClassicButtons && !ackFlag) { %>
                                    <input type="button" value="<bean:message key="oscarMDS.segmentDisplay.btnAcknowledge" />" onclick="<%=ackLabFunc%>" >
                                    <input type="button" value="<bean:message key="oscarMDS.segmentDisplay.btnComment"/>" onclick="return getComment('addComment',<%=segmentID%>);">
                                    <% } %>
                                    <input type="button" class="smallButton" value="<bean:message key="oscarMDS.index.btnForward"/>" onClick="popupStart(397, 700, '../../../oscarMDS/SelectProvider.jsp?docId=<%=segmentID%>&labDisplay=true', 'providerselect')">
                                    <% if (!hideClassicButtons) { %>
                                    <input type="button" value=" <bean:message key="global.btnClose"/> " onClick="window.close();window.parent.openedWindowClosing();">
                                    <% } %>
                                    <input type="button" value=" <bean:message key="global.btnPrint"/> " onClick="printPDF('<%=segmentID%>')">
                                        <indivo:indivoRegistered demographic="<%=demographicID%>" provider="<%=providerNo%>">
                                        <input type="button" value="<bean:message key="global.btnSendToPHR"/>" onClick="sendToPHR('<%=segmentID%>', '<%=demographicID%>')">
                                        </indivo:indivoRegistered>
                                    <%
                                    if ( searchProviderNo != null ) { // we were called from e-chart
                                      if (!hideEncounterLink) {
                                    %>
                                    <input type="button" value=" <bean:message key="oscarMDS.segmentDisplay.btnEChart"/> " onClick="popupStart(360, 680, '../../../oscarMDS/SearchPatient.do?providerNumber=<%= providerNo %>&labType=HL7&segmentID=<%= segmentID %>&name=<%=java.net.URLEncoder.encode(handler.getLastName()+", "+handler.getFirstName())%>', 'encounter')">

                                    <% } %>
                                    <%

									if(demographicPortalEnabled) {
									%>
										<input type="button" value="<bean:message key="yourcare.patientportal.pushToPortal"/>" title="Push To Portal" onclick="pushToPortal('acknowledgeForm_<%=segmentID%>');" />
                                		<span class="Field2"><i>Share Status: <%=syncStatus%></i></span>
                            	   <% } %>
                                    <%
                                    }
                                    %>
                                </td>
                                <td width="50%" valign="center" align="left">
                                    <span class="Field2"><i><bean:message key="oscarMDS.segmentDisplay.msgReportEnd"/></i></span>
                                </td>
                            </tr>
                        </table>
                        
                        <br/>
                        <table>
                        	<%
                        		for(String lName : allLicenseNames) {
                        	%>
                        	<tr>
                        		<td><%=lName %></td>
                        	</tr>
                        	
                        	<% } %>
                        </table>
                    </td>
                </tr>
                   <%
                   if (obgynShortcuts && isLinkedToDemographic) {
                   %>
               <tr>
                   <td>
                           <%
                               ONPerinatal2017Dao onPerinatal2017Dao = SpringUtils.getBean(ONPerinatal2017Dao.class);
                               String formId = onPerinatal2017Dao.getArFormIdIfNoPrForm(loggedInInfo, demographicID);
                               if ("0".equals(formId)) {
                           %>
                               <input type="button" value="PR2-LAB"
                                      onClick="popupONAREnhanced(290, 775, '<%=request.getContextPath()%>/form/formONPerinatalForm.jsp?demographic_no=<%=demographicID%>&section='+this.value)"/>
                               <input type="button" value="PR2-PGI"
                                      onClick="popupONAREnhanced(290, 775, '<%=request.getContextPath()%>/form/formONPerinatalForm.jsp?demographic_no=<%=demographicID%>&section='+this.value)"/>
                               <input type="button" value="PR2-US"
                                      onClick="popupONAREnhanced(290, 775, '<%=request.getContextPath()%>/form/formONPerinatalForm.jsp?demographic_no=<%=demographicID%>&section='+this.value)"/>
                               <input type="button" value="PR2"
                                          onClick="popupPage(700, 1024, '<%=request.getContextPath()%>/form/formONPerinatalRecord2.jsp?demographic_no=<%=demographicID%>&shortcut=true&update=true')"/>
                               <input type="button" value="PR3"
                                      onClick="popupPage(700, 1024, '<%=request.getContextPath()%>/form/formONPerinatalRecord3.jsp?demographic_no=<%=demographicID%>&shortcut=true&update=true')"/>
                           <%
                               } else {
                           %>
                           <input type="button" value="AR1-ILI"
                                  onClick="popupONAREnhanced(290, 625, '<%=request.getContextPath()%>/form/formonarenhancedForm.jsp?demographic_no=<%=demographicID%>&formId=<%=formId%>&section='+this.value)"/>
                           <input type="button" value="AR1-PGI"
                                  onClick="popupONAREnhanced(225, 590,'<%=request.getContextPath()%>/form/formonarenhancedForm.jsp?demographic_no=<%=demographicID%>&formId=<%=formId%>&section='+this.value)"/>
                           <input type="button" value="AR2-US"
                                  onClick="popupONAREnhanced(395, 655, '<%=request.getContextPath()%>/form/formonarenhancedForm.jsp?demographic_no=<%=demographicID%>&formId=<%=formId%>&section='+this.value)"/>
                           <input type="button" value="AR2-ALI"
                                  onClick="popupONAREnhanced(375, 430, '<%=request.getContextPath()%>/form/formonarenhancedForm.jsp?demographic_no=<%=demographicID%>&formId=<%=formId%>&section='+this.value)"/>
                           <input type="button" value="AR2"
                                  onClick="popupPage(700, 1024, '<%=request.getContextPath()%>/form/formonarenhancedpg2.jsp?demographic_no=<%=demographicID%>&formId=<%=formId%>&update=true')"/>
                           <%
                           }
                           %>
                   </td>
               </tr>
                   <%
                   }
                   %>
            </table>
        </form>      
        
        <%String s = ""+System.currentTimeMillis();%>
        <a style="color:white;" href="javascript: void(0);" onclick="showHideItem('rawhl7<%=s%>');" >show</a>
        <pre id="rawhl7<%=s%>" style="display:none;"><%=hl7%></pre>
        </div>
        <%} %>
        
    </body>
<%
	// Jsp successfully loaded, mark lab read
	ReadLabDao readLabDao = SpringUtils.getBean(ReadLabDao.class);
	readLabDao.markAsRead(loggedInInfo.getLoggedInProviderNo(), "HL7", Integer.valueOf(segmentID));
%>
</html>
<%!
    public String[] divideStringAtFirstNewline(String s){
        int i = s.indexOf("<br />");
        String[] ret  = new String[2];
        if(i == -1){
               ret[0] = new String(s);
               ret[1] = null;
            }else{
               ret[0] = s.substring(0,i);
               ret[1] = s.substring(i+6);
            }
        return ret;
    }
%>
 <%--
    AD Address
    CE Coded Entry
    CF Coded Element With Formatted Values
    CK Composite ID With Check Digit
    CN Composite ID And Name
    CP Composite Price
    CX Extended Composite ID With Check Digit
    DT Date
    ED Encapsulated Data
    FT Formatted Text (Display)
    MO Money
    NM Numeric
    PN Person Name
    RP Reference Pointer
    SN Structured Numeric
    ST String Data.
    TM Time
    TN Telephone Number
    TS Time Stamp (Date & Time)
    TX Text Data (Display)
    XAD Extended Address
    XCN Extended Composite Name And Number For Persons
    XON Extended Composite Name And Number For Organizations
    XPN Extended Person Number
    XTN Extended Telecommunications Number
 --%>
