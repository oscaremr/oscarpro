/**
 * verticalTabTable.js
 * 
 * 
 * Implements vertical tabbing functionality in table elements
 * 
 * Set class="vertical-tab" on <table> OR <tbody>
 * 
 * Additional <td> class selectors functionalities:
 * .skip
 *  - skips setting a tabIndex to cell child elements
 *
 * .ti-match-next
 *  - sets the tabIndex to the same value as next td child elements,
 *      for temporary horizontal tabbing
 *
 * @requires jQuery
 */

if (typeof jQuery === 'undefined') { 
    throw new Error("Requires jQuery");
}


$(function () {
    verticalTab();
});

function verticalTab() {
    // table.vertical-tab
    $('table.vertical-tab').each(function () {
        $(this).find('tbody > tr').each(function () {
            updateTabIndexes($(this))
        });
    });

    // tbody.vertical-tab
    $('tbody.vertical-tab').each(function () {
        $(this).find('tr').each(function () {
            updateTabIndexes($(this))
        });
    });
}

function updateTabIndexes($this) {
    $this.find('td:not(.skip)').each(function (i) {
        let inputs = Array.from($(this)[0].children);
        inputs.forEach(function (ele) {
            // tab index same as row to force vertical tabbing
            ele.tabIndex = i + 1;
        });
    });
    
    
    $this.find('td[class="ti-match-next"]').each(function (i) {
        let inputs = Array.from($(this)[0].children);

        let nextChildren = $(this).next().children();
        if (inputs.length > 0 && nextChildren.length > 0) {
            inputs.forEach(function (ele) {
                ele.tabIndex = nextChildren[0].tabIndex;
            });
        }
    });
}