function setupDemoAutoCompletion(selectedDemos, docId, flagToMrp) {
    if(jQuery("#autocompletedemo" + docId) ){

        var url;
        if( jQuery("#activeOnly" + docId).is(":checked") ) {
            url = window.contextpath + "/demographic/SearchDemographic.do?jqueryJSON=true&activeOnly=" + jQuery("#activeOnly" + docId).val();
        }
        else {
            url = window.contextpath + "/demographic/SearchDemographic.do?jqueryJSON=true";
        }

        jQuery( "#autocompletedemo" + docId ).autocomplete({
            source: url,
            minLength: 2,

            focus: function( event, ui ) {
                jQuery( "#autocompletedemo" + docId ).val( ui.item.label );
                return false;
            },
            select: function(event, ui) {
                jQuery( "#autocompletedemo" + docId ).val(ui.item.label);
                jQuery( "#demofind" + docId).val(ui.item.value);
                jQuery( "#demofindName" + docId ).val(ui.item.formattedName);
                selectedDemos.push(ui.item.label);
                console.log(ui.item.providerNo);
                if(flagToMrp &&  ui.item.providerNo != undefined && ui.item.providerNo != null && ui.item.providerNo != "" && ui.item.providerNo != "null" ) {
                    addDocToList(ui.item.providerNo, ui.item.provider + " (MRP)", "" + docId);
                    forwardDocumentToForwardedProviders(ui.item.providerNo, docId);
                }
                if( ui.item.nurse != undefined && ui.item.nurse != null &&ui.item.nurse != "" && ui.item.nurse != "null" ) {
                    addDocToList(ui.item.nurse, ui.item.nurseName + " (Alt. Provider 1)", "" + docId);
                }
                if( ui.item.resident != undefined && ui.item.resident != null &&ui.item.resident != "" && ui.item.resident != "null" ) {
                    addDocToList(ui.item.resident, ui.item.residentName + " (Alt. Provider 2)", "" + docId);
                }
                if( ui.item.midwife != undefined && ui.item.midwife != null &&ui.item.midwife != "" && ui.item.midwife != "null" ) {
                    addDocToList(ui.item.midwife, ui.item.midwifeName + " (Alt. Provider 3)", "" + docId);
                }

                //enable Save button whenever a selection is made
                jQuery('#save' + docId).removeAttr('disabled');
                jQuery('#saveNext' + docId).removeAttr('disabled');

                jQuery('#msgBtn_' + docId).removeAttr('disabled');
                jQuery('#mainTickler_' + docId).removeAttr('disabled');
                jQuery('#mainEchart_' + docId).removeAttr('disabled');
                jQuery('#mainMaster_' + docId).removeAttr('disabled');
                jQuery('#mainApptHistory_' + docId).removeAttr('disabled');
                return false;
            }
        });
    }
}

