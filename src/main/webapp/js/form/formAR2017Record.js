function reset() {
    document.forms[0].target = "";
    document.forms[0].action = '/'+projectHome+'/form/formname.do' ;
}

function refreshOpener() {
    if (window.opener && window.opener.name=="inboxDocDetails") {
        window.opener.location.reload(true);
    }
}

function popupPage(varpage,ar) {
    windowprops = "height=960,width=1280"+
        ",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=no,screenX=50,screenY=50,top=20,left=20";
    var popup = window.open(varpage, ar, windowprops);
    if (popup.opener == null) {
        popup.opener = self;
    }
}

function onSave() {
    document.forms[0].submit.value="save";
    var ret = true;
    if(ret==true) {
        reset();
        ret = confirm("Are you sure you want to save this form?");
    }
    return ret;
}

function onExit() {
    var ret = true;
    if(!bView) ret = confirm("Are you sure you wish to exit without saving your changes?");
    if(ret==true)
    {
        refreshOpener();
        window.close();
    }
    return(false);
}

function onSaveExit() {
    document.forms[0].submit.value="exit";
    var ret = true;
    if(ret == true) {
        reset();
        ret = confirm("Are you sure you wish to save and close this window?");
    }
    return ret;
}

function onPrint(pageNumber) {
    document.forms[0].submit.value="print";
    var ret = checkAllDates();
    ret = true;
    setLock(false);
    if(ret==true) {
        if( document.forms[0].c_fedb.value == "") {
            alert('Please set Final EDB before printing');
            ret = false;
        }
        else {
            document.forms[0].action = "../form/createpdf?__title=Antenatal+Record+Part+3+Appendix&__cfgfile=ar2017PrintCfgPg4&__template="+pageNumber;
            document.forms[0].target="_blank";
        }
    }
    setTimeout('setLock(wasLocked)', 500);
    return ret;
}

function onPrintAll() {
    document.forms[0].submit.value="printAll";
    var ret = checkAllDates();
    setLock(false);
    if(ret==true) {
        if( document.forms[0].c_fedb.value == "") {
            alert('Please set Final EDB before printing');
            ret = false;
        } else {
            document.forms[0].action = "../form/createpdf?__title=Ontario+Perinatal+Record+1&__cfgfile=ar2017PrintCfgPg1&__template=ar2017pg1&multiple=4&__title1=Antenatal+Record+Part+2&__cfgfile1=ar2017PrintCfgPg2&__template1=ar2017pg2&__title2=Antenatal+Record+Part+3&__cfgfile2=ar2017PrintCfgPg3&__template2=ar2017pg3&__title3=Antenatal+Record+Part+3+Appendix&__cfgfile3=ar2017PrintCfgPg4&__template3=ar2017pg4";
            document.forms[0].target="_blank";
        }
    } else {
        alert('Please set Final EDB before printing');
    }
    setTimeout('setLock(wasLocked)', 500);
    return ret;
}

function calToday(field) {
    var calDate=new Date();
    varMonth = calDate.getMonth()+1;
    varMonth = varMonth>9? varMonth : ("0"+varMonth);
    varDate = calDate.getDate()>9? calDate.getDate(): ("0"+calDate.getDate());
    field.value = calDate.getFullYear() + '/' + (varMonth) + '/' + varDate;
}

function setLock (checked) {
    formElems = document.forms[0].elements;
    for (var i=0; i<formElems.length; i++) {
        if (formElems[i].type == "text" || formElems[i].type == "textarea") {
            formElems[i].readOnly = checked;
        } else if ((formElems[i].type == "checkbox") && (formElems[i].id != "pg1_lockPage")) {
            formElems[i].disabled = checked;
        }
    }
}

function calcBMIMetric(obj) {
    if(isNumber(document.forms[0].c_ppwt) && isNumber(document.forms[0].c_ppht)) {
        weight = document.forms[0].c_ppwt.value / 1;
        height = document.forms[0].c_ppht.value / 100;
        if(weight!="" && weight!="0" && height!="" && height!="0") {
            obj.value = Math.round(weight * 10 / height / height) / 10;
        } else obj.value = '0.0';
    }
}

function isNumber(ss){
    var s = ss.value;
    var i;
    for (i = 0; i < s.length; i++){
        var c = s.charAt(i);
        if (c == '.') {
            continue;
        } else if (((c < "0") || (c > "9"))) {
            alert('Invalid '+s+' in field ' + ss.name);
            ss.focus();
            return false;
        }
    }
    return true;
}


function calcWeek(id) {
    source = $("#"+id);
    var delta = 0;
    var str_date = getDateField(source.attr('name'));
    if (str_date.length < 8) return;
    var yyyy = str_date.substring(0, str_date.indexOf("/"));
    var mm = eval(str_date.substring(eval(str_date.indexOf("/")+1), str_date.lastIndexOf("/")) - 1);
    var dd = str_date.substring(eval(str_date.lastIndexOf("/")+1));
    var check_date=new Date(yyyy,mm,dd);
    var start=new Date(sDate);
    if (check_date.getUTCHours() != start.getUTCHours()) {
        if (check_date.getUTCHours() > start.getUTCHours()) {
            delta = -1 * 60 * 60 * 1000;
        } else {
            delta = 1 * 60 * 60 * 1000;
        }
    }
    var day = eval((check_date.getTime() - start.getTime() + delta) / (24*60*60*1000));
    if(isNaN(day)) return;
    var week = Math.floor(day/7);
    var weekday = day%7;
    source.val(week + "w+" + weekday);
}

function getDateField(name) {
    var n1 = name.split("_")[0];
    var n2 = name.split("_")[1];
    var newName = n1 + '_' + n2.replace('g','d');
    for (var i =0; i <document.forms[0].elements.length; i++) {
        if (document.forms[0].elements[i].name == newName) {
            return document.forms[0].elements[i].value;
        }
    }
    return newName;
}


function valDate(dateBox) {
    try {
        var dateString = dateBox.value;
        if(dateString == "") {
            return true;
        }
        var dt = dateString.split('/');
        var y = dt[0];
        var m = dt[1];
        var d = dt[2];
        var orderString = m + '/' + d + '/' + y;
        var pass = isDate(orderString);
        if(pass!=true) {
            alert('Invalid '+pass+' in field ' + dateBox.name);
            dateBox.focus();
            return false;
        }
    }
    catch (ex) {
        alert('Catch Invalid Date in field ' + dateBox.name);
        dateBox.focus();
        return false;
    }
    return true;
}

function isDate(dtStr){
    var daysInMonth = DaysArray(12)
    var pos1=dtStr.indexOf(dtCh)
    var pos2=dtStr.indexOf(dtCh,pos1+1)
    var strMonth=dtStr.substring(0,pos1)
    var strDay=dtStr.substring(pos1+1,pos2)
    var strYear=dtStr.substring(pos2+1)
    strYr=strYear
    if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
    if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
    for (var i = 1; i <= 3; i++) {
        if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
    }
    month=parseInt(strMonth)
    day=parseInt(strDay)
    year=parseInt(strYr)
    if (pos1==-1 || pos2==-1){
        return "format"
    }
    if (month<1 || month>12){
        return "month"
    }
    if (day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
        return "day"
    }
    if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
        return "year"
    }
    if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
        return "date"
    }
    return true
}

function daysInFebruary (year) {
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}

function DaysArray(n) {
    for (var i = 1; i <= n; i++) {
        this[i] = 31
        if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
        if (i==2) {this[i] = 29}
    }
    return this
}

var dtCh= "/";
var minYear=1900;
var maxYear=2040;

function isInteger(s){
    var i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    return true;
}

function stripCharsInBag(s, bag){
    var i;
    var returnString = "";
    for (i = 0; i < s.length; i++){
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}


//in this form, kg is hardcoded as measure unit: so dont need it?
function wtEnglish2Metric(obj) {
    //if(isNumber(document.forms[0].c_ppWt) ) {
    //	weight = document.forms[0].c_ppWt.value;
    if(isNumber(obj) ) {
        weight = obj.value;
        weightM = Math.round(weight * 10 * 0.4536) / 10 ;
        if(confirm("Are you sure you want to change " + weight + " pounds to " + weightM +"kg?") ) {
            //document.forms[0].c_ppWt.value = weightM;
            obj.value = weightM;
        }
    }
}
//in this form, sm is hardcoded as measure unit: so dont need it?
function htEnglish2Metric(obj) {
    height = obj.value;
    if(height.length > 1 && height.indexOf("'") > 0 ) {
        feet = height.substring(0, height.indexOf("'"));
        inch = height.substring(height.indexOf("'"));
        if(inch.length == 1) {
            inch = 0;
        } else {
            inch = inch.charAt(inch.length-1)=='"' ? inch.substring(0, inch.length-1) : inch;
            inch = inch.substring(1);
        }

        if(isNumber(feet) && isNumber(inch) ) {
            height = Math.round((feet * 30.48 + inch * 2.54) * 10) / 10 ;
            if(confirm("Are you sure you want to change " + feet + " feet " + inch + " inch(es) to " + height +"cm?") ) {
                obj.value = height;
            }
        }
    }
}

