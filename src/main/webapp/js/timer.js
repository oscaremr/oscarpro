/* Code taken from https://jsfiddle.net/Daniel_Hug/pvk6p/ */
var timer;
var startPause;
var clear;
var paused = false;
var startTime;
var timeoutId;

var chartTimer = {
  "milliseconds": 0,
  "seconds": 0,
  "minutes": 0,
  "hours": 0,
  "periods": [
    {
      "startTime": new Date(),
      "endTime": null
    }
  ]
};
var pauseTimer = {
    "milliseconds": 0,
    "seconds": 0,
    "minutes": 0,
    "hours": 0
};

function initTimer() {
    timer = document.getElementById("timer__display");
    startPause = document.getElementById("timer__start-pause");
    clear = document.getElementById("timer__restart");


    timer.onclick = pasteTimer;
    startPause.onclick = startPauseTimer;
    clear.onclick = resetTimer;
    
    startTime = new Date().toLocaleTimeString()
    createTimer();
}

function runIntervalFunction(intervalFunction) {
    intervalFunction();
    
    // clear timeout and set timeout for next call
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function(){
        runIntervalFunction(intervalFunction)
    }, 500);
}

function incrementActiveTimer() {
    let timerToIncrement = chartTimer;
    if (paused) {
        timerToIncrement = pauseTimer;
        if (timerToIncrement.periods.length === 0) {
            timerToIncrement.periods.push({ "startTime": new Date(), "endTime": null });
        }
    }
    
    let totalMillisecondsDifference = calculateSumOfTimerPeriods(timerToIncrement);
    setTimerValuesFromMilliseconds(totalMillisecondsDifference, timerToIncrement);
    

    if (!paused) {
        timer.textContent = formatTime(chartTimer.hours, chartTimer.minutes, chartTimer.seconds);
    }
}

function createTimer() {
    runIntervalFunction(incrementActiveTimer);
}

/* Start button */
function pasteTimer() {
    let displayText = timer.textContent + "\n"
        + "Start Time: " + startTime + "\n"
        + "End Time: " + new Date().toLocaleTimeString();

    if (pauseTimer.seconds > 0 || pauseTimer.minutes > 0 || pauseTimer.hours > 0) {
        displayText = displayText + "\n"
            + "Pause Duration: " + formatTime(pauseTimer.hours, pauseTimer.minutes, pauseTimer.seconds);
    }

    pasteToEncounterNote(displayText);
}

/* Pause/Start button */
function startPauseTimer() {
    clearTimeout(timeoutId);
    paused = !paused;
    
    
    if (paused) {
        // if now paused set end date for current timer interval and start the interval for the pausedTimer
        chartTimer.periods[chartTimer.periods.length - 1].endTime = new Date();
        if (pauseTimer.periods.length === 0) {
            pauseTimer.periods.push({ "startTime": new Date(), "endTime": null });
        }
    } else {
        // if unpaused add new interval to chartTimer and end the interval for the pausedTimer
        chartTimer.periods.push({ "startTime": new Date(), "endTime": null });
        chartTimer.periods[chartTimer.periods.length - 1].endTime = new Date();
    }
    
    createTimer();
    startPause.title = paused ? "Play":"Pause";

    startPause.classList.toggle("glyphicon-pause");
    startPause.classList.toggle("glyphicon-play");
}

/* Clear button */
function resetTimer() {
    timer.textContent = "00:00:00";
    chartTimer = {"milliseconds": 0, "seconds": 0, "minutes": 0, "hours": 0, "periods": [{ "startTime": new Date(), "endTime": null }]};
    pauseTimer = {"milliseconds": 0, "seconds": 0, "minutes": 0, "hours": 0, "periods": []};
    startTime = new Date().toLocaleTimeString();
}

function formatTime(hours, minutes, seconds) {
    return (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);
}

function calculateSumOfTimerPeriods(timer) {
    let totalMillisecondsDifference = 0;
    if (timer.periods.length > 1) {
        // get sum of existing periods difference
        for (let i = 0; i < timer.periods.length - 1; i++) {
            let interval = timer.periods[i];
            totalMillisecondsDifference += (interval.endTime.getTime() - interval.startTime.getTime())
        }
    }

    let latestInterval = timer.periods[timer.periods.length - 1];
    let thisStartTime = latestInterval.startTime;
    let now = new Date();

    totalMillisecondsDifference += (now.getTime() - thisStartTime.getTime());
    return totalMillisecondsDifference;
}

function setTimerValuesFromMilliseconds(ms, timer) {
    let seconds = Math.ceil(ms / 1000);
    let hours = Math.floor( seconds / 3600); // 3,600 seconds in 1 hour
    seconds = seconds % 3600; // seconds remaining after extracting
    let minutes = Math.floor( seconds / 60 ); // 60 seconds in 1 minute
    seconds = seconds % 60;

    timer.seconds = seconds;
    timer.minutes = minutes;
    timer.hours = hours;
}