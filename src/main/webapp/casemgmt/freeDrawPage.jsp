<%--
/*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
--%>

<%@page import="java.util.List" %>
<%@page import="org.oscarehr.common.dao.DocumentDao" %>
<%@page import="org.oscarehr.common.dao.FreeDrawDataDao" %>
<%@page import="org.oscarehr.common.model.Document" %>
<%@page import="org.oscarehr.common.model.FreeDrawData" %>
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="oscar.OscarProperties" %>

<%
String ctx = request.getContextPath();
DocumentDao docDao = (DocumentDao)SpringUtils.getBean("documentDao");
OscarProperties prop = OscarProperties.getInstance();
List<Document> pics = docDao.findByDoctype(prop.getProperty("drawing_tool.document_type"));
String demoNo = request.getParameter("demographic_no");
String apptNo = request.getParameter("appointment");
String providerNo = request.getParameter("provNo");
String freeDrawId = request.getParameter("freeDrawId");

if (freeDrawId == null) {	
	freeDrawId = "";
}

String defaultImageId = prop.getProperty("drawing_tool.default_image_documment_id", "-1");
	
%>
<!DOCTYPE html>
<html>
<head>
<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
<meta content="yes" name="apple-mobile-web-app-capable">
<meta content="width = 1040" name="viewport">
<title>Free Drawing</title>
<style type="text/css" media="print">
 .DoNotPrint {
	 display: none;
 }
 </style>
 <link rel="stylesheet" type="text/css" href="<%=ctx %>/share/zwibbler/zwibbler-style.css">
 <style type="text/css">

 #leftSection {
 	width: 100%;
 	margin: 10px 1px;
 	padding: 1px 1px;
 }
 #leftSection inupt[type='button'] {
 	margin: 5px 0px;
 	line-height: 2em;
 }
 </style>

<script>
var defaultImageId = <%=defaultImageId%>;
</script>
</head>
<body onload="loadImage(defaultImageId)">
	<div id="leftSection">
		<b>Upload background image file from eDoc. The document type is "free draw"</b><br><br>
		<select id="zwibblerBgImg">
			<option value="-1">-->>select background image<<--</option>
			<%if (pics != null) {
				for (Document doc : pics) {
			%>
				<option value="<%=doc.getId() %>"><%=doc.getDocdesc()%>-<%=doc.getDocfilename() %></option>
			<%}} %>
		</select>
		
		<input type="button" name="loadImage" value="Load Image" onclick="loadImage(0);">
		<input type="button" name="submit" style="display: none;" value="Submit & Save" id="submit" onclick="save();">
		<input type="button" name="submit" style="display: none;" value="Submit & Save & Document" id="saveAndDocument" onclick="saveAndDocument();">
		<input type="button" name="cancel" value="Cancel" id="cancel" onclick="window.close();">
		<form action="" id="zwiFrm" name="zwiFrm">
			<input type="hidden" name="demographicNo" value="<%=demoNo%>">
			<input type="hidden" name="thumbnailImage" id="thumbnailImage" value="">
			<input type="hidden" name="signature" value="">
			<input type="hidden" name="providerNo" value="<%=providerNo%>">
		</form>
		<form action="" id="zwiFrm2" name="zwiFrm2">
			<input type="hidden" name="freeDrawId" value="<%=freeDrawId%>">
			<input type="hidden" name="demographicNo" value="<%=demoNo%>">
			<input type="hidden" name="drawingData" id="drawingData" value="">
			<input type="hidden" name="apptNo" value="<%=apptNo%>">
			<input type="hidden" name="providerNo" value="<%=providerNo%>">
			<input type="hidden" name="SubmitData" value="">
			<input type="hidden" name="type" value="zwibbler">
			<input type="hidden" name="thumbnailId" id="thumbnailId" value="">
			<input type="hidden" name="forward" value="json">
		</form>
	</div>
	<script type="text/javascript">
	var myCtx;
	</script>
	<script type="text/javascript" src="<%=ctx %>/js/jquery-1.12.3.js"></script>
	<script type="text/javascript" src="<%=ctx %>/share/zwibbler/zwibbler2.js"></script>
	<script type="text/javascript" src="<%=ctx %>/share/zwibbler/zwibbler-script.js"></script>
	
	<div class="container">		
		    <div id="zwibbler-div">
            <div class="zwibbler-toolbar"></div>
            <div class="zwibbler-canvas"></div>
            <div class="toolbar-right">
                <h1 class="layer-title">Shape options</h1>
                <div zwibbler-show="AnyNode-selected">
                    <button zwibbler-click="deleteNodes">Remove</button>
                    <button zwibbler-click="bringToFront">Bring to front</button>
                    <button zwibbler-click="sendToBack">Send to back</button>
                </div>
                <h2 zwibbler-show="fillStyle strokeStyle">Colours</h2>
                <div class="layer-option" zwibbler-show="fillStyle">
                    <div class="swatch" zwibbler-property="fillStyle"></div>
                    Fill colour
                </div>
                <div class="layer-option" zwibbler-show="strokeStyle">
                    <div class="swatch" zwibbler-property="strokeStyle"></div>
                    Outline colour
                </div>
                <div zwibbler-show="PathNode-open-selected">
                    <h2>Arrow style</h2>
                    <div class="layer-option" zwibbler-property="arrowSize" zwibbler-value="0">
                        No arrow
                    </div>
                    <div class="layer-option" zwibbler-property="arrowSize" zwibbler-value="20">
                        Arrow
                    </div>
                </div>
                <div zwibbler-show="dashes">
                    <h2>Line style</h2>
                    <div class="layer-option" zwibbler-property="dashes" zwibbler-value="">Solid</div>
                    <div class="layer-option" zwibbler-property="dashes" zwibbler-value="5,2">Dashes</div>
                </div>
                <div zwibbler-show="lineWidth">
                    <h2>Outline thickness</h2>
                    <select zwibbler-property="lineWidth">
                        <option value="0">None</option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>5</option>
                        <option>10</option>
                    </select>
                </div>
                <div zwibbler-show="fontName">
                    <h2>Font</h2>
                    <select zwibbler-property="fontName">
                        <option value="Times New Roman">Times New Roman</option>
                        <option value="Arial">Arial</option>
                    </select>
                </div>
                <div zwibbler-show="fontSize">
                    <h2>Font size</h2>
                    <select zwibbler-property="fontSize">
                        <option value="10">10</option>
                        <option value="12">12</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="50">50</option>
                    </select>
                </div>
            </div>
            <div class="zwibbler-palette zwibbler-dialog"></div>
        </div>
	</div>
		
	<script type="text/javascript"> 	
	function loadImage(imageId) {
		var docNo;
		if (imageId>0) { //Have default image id defined in the properties file.
			docNo = imageId;
			$("#zwibblerBgImg").val(docNo);
			myCtx.setConfig("backgroundImage", "<%=ctx%>/oscarEncounter/freeDrawing.do?method=displayFreebgImg&docNo="+docNo);
		} else if(imageId == 0){ //Select image from oscar free drawing page
			docNo=$("#zwibblerBgImg option:selected").val();
			if (docNo == -1) {
				alert("Please select backgroud image first!");
				return;
			}
			myCtx.setConfig("backgroundImage", "<%=ctx%>/oscarEncounter/freeDrawing.do?method=displayFreebgImg&docNo="+docNo);
		} else {
			//default image not defined, id < 0
			return;
		} 
		
	}
	
	function save() {
		// 1. save png picture
		$("#thumbnailImage").val(myCtx.save("png"));
		$.ajax({
			url:"<%=ctx%>/signature_pad/uploadThumbnail.jsp",			
			type:"post",
			data: $("#zwiFrm").serialize(),
			success: function (data) {
				var thumbId = $(data).val();
				if (thumbId == null || thumbId.trim().length==0) {
					alert("Failed to save free draw data (thumbnail)!");
					return;
				}
				$("#thumbnailId").val(thumbId);
				$("#drawingData").val(encodeURIComponent(myCtx.save("zwibbler3")));
				// 2. save data & pic id to db
				$.ajax({
					url:"<%=ctx%>/oscarEncounter/freeDrawing.do?method=save",
					type: "post",
					data: $("#zwiFrm2").serialize() + "&imgnum=" + $("#zwibblerBgImg option:selected").val(),
					dataType:"json",
					success: function(ret) {
						if (ret != null && ret.retCode != null && ret.retCode == "OK") {
							alert("Save successfully!");
							window.opener.location.reload();
							window.opener.focus();
							self.close();
						}
					},
					error: function() {
						alert("Failed to save free draw data!");
					}
				});
			},
			error: function() {
				alert("Failed to save free draw data!");
			}
		});
	}
	
	function saveAndDocument() {
		// 1. save png picture
		$("#thumbnailImage").val(myCtx.save("png"));
		$.ajax({
			url:"<%=ctx%>/signature_pad/uploadThumbnail.jsp",
			type:"post",
			data: $("#zwiFrm").serialize(),
			success: function (data) {
				var thumbId = $(data).val();
				if (thumbId == null || thumbId.trim().length==0) {
					alert("Failed to save free draw data (thumbnail)!");
					return;
				}
				$("#thumbnailId").val(thumbId);
				$("#drawingData").val(encodeURIComponent(myCtx.save("zwibbler3")));
				// 2. save data & pic id to db
				$.ajax({
					url:"<%=ctx%>/oscarEncounter/freeDrawing.do?method=saveAndDocument",
					type: "post",
					data: $("#zwiFrm2").serialize() + "&imgnum=" + $("#zwibblerBgImg option:selected").val(),
					dataType:"json",
					success: function(ret) {
						if (ret != null && ret.retCode != null && ret.retCode == "OK") {
							alert("Save successfully!");
							window.opener.location.reload();
							window.opener.focus();
							self.close();
						}
					},
					error: function() {
						alert("Failed to save free draw data!");
					}
				});
			},
			error: function() {
				alert("Failed to save free draw data!");
			}
		});
	}
	
	<%if (!freeDrawId.trim().isEmpty()) {%>
		$(function() {
			// restore data
			$.ajax({
				url: "<%=ctx%>/oscarEncounter/freeDrawing.do?method=getDrawingData&freeDrawId=<%=freeDrawId%>",
				type:"GET",
				dataType:"json",
				success: function(ret) {
					if (ret == null || ret.retCode != "OK") {
						alert("Failed to load free draw data!");
						return;
					}
					if(ret.imgnum != null && ret.imgnum != ""){
						$("#zwibblerBgImg").val(ret.imgnum);
					}
					myCtx.load(decodeURIComponent(ret.data));
				},
				error: function() {
					alert("Failed to load free draw data!");
				}
			})
		});
		<%}%>
	
	</script>
</body>
</html>
