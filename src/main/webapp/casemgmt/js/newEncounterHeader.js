jQuery.noConflict();
jQuery(function() {
    var linkObj = jQuery('#encounterHeader #headerXappHref');
    linkObj.attr('href', window.encodeURI(linkObj.attr('href')));
    if (!linkObj || linkObj.length === 0) {
        return;
    }
    var integrationName = jQuery('#divXappIntegrationName').text().trim()
    var providerNo = linkObj.data('provider');
    var url = globalContextPath + `/ws/rs/integration/connection/` + integrationName + `?1=1`;
    url += `&providerNo=${providerNo}`;
    url += `&healthCheck=true`;
    jQuery.get(url).done(function(data, textStatus, jqXHR) {
        if (jqXHR.status == '200') {
            linkObj.show()
        } else {
            linkObj.hide()
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        linkObj.hide()
    })
});