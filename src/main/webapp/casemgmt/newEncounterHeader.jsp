
<%--


    Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for
    Centre for Research on Inner City Health, St. Michael's Hospital,
    Toronto, Ontario, Canada

--%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="oscar.oscarEncounter.data.*, oscar.oscarProvider.data.*, oscar.util.UtilDateUtilities" %>
<%@ page import="org.oscarehr.util.MiscUtils"%>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="org.oscarehr.PMmodule.caisi_integrator.CaisiIntegratorManager, org.oscarehr.util.LoggedInInfo, org.oscarehr.common.model.Facility" %>
<%@ page import="org.oscarehr.common.dao.DemographicExtDao" %>
<%@ page import="org.oscarehr.common.model.DemographicExt" %>
<%@ page import="org.oscarehr.common.dao.DemographicDao"%>
<%@ page import="org.oscarehr.common.dao.DemographicPronounDao" %>
<%@ page import="org.oscarehr.common.model.Demographic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.apache.commons.lang.StringUtils"%>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@page import="org.oscarehr.common.dao.DemographicDao" %>
<%@ page import="org.oscarehr.util.SessionConstants" %>
<%@ page import="org.oscarehr.util.CareConnectUtils" %>
<%@ page import="org.oscarehr.common.model.DemographicPronoun" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Objects" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%
	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
  OscarProperties oscarProperties = OscarProperties.getInstance();

    oscar.oscarEncounter.pageUtil.EctSessionBean bean = null;
    if((bean=(oscar.oscarEncounter.pageUtil.EctSessionBean)request.getSession().getAttribute("EctSessionBean"))==null) {
        response.sendRedirect("error.jsp");
        return;
    }
    
    Facility facility = loggedInInfo.getCurrentFacility();

    String demoNo = bean.demographicNo;
    String providerNo = bean.providerNo;
    
    EctPatientData.Patient pd = new EctPatientData().getPatient(loggedInInfo, demoNo);
    DemographicPronounDao demographicPronounDao = SpringUtils.getBean(DemographicPronounDao.class);
    String pronouns = "";
    if (pd.getPronounId() > 0) {
      List<DemographicPronoun> pronounsList = demographicPronounDao.findbyId(pd.getPronounId());
      pronouns = pronounsList != null && pronounsList.get(0) != null
              ? " (" + pronounsList.get(0).getValue() + ")"
              : "";
    }
    String famDocName, famDocSurname, famDocColour, inverseUserColour, userColour;
    String user = (String) session.getAttribute("user");
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    ProviderColourUpdater colourUpdater = new ProviderColourUpdater(user);
    userColour = colourUpdater.getColour();
    
	DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
    DemographicExt infoExt = demographicExtDao.getDemographicExt(Integer.parseInt(demoNo), "informedConsent");
    boolean showPopup = infoExt == null || StringUtils.isBlank(infoExt.getValue());
    boolean enhancedEnabled = "E".equals(request.getSession().getAttribute(SessionConstants.LOGIN_TYPE));

    //we calculate inverse of provider colour for text
    int base = 16;
    if( userColour == null || userColour.length() == 0 )
        userColour = "#CCCCFF";   //default blue if no preference set

    int num = Integer.parseInt(userColour.substring(1), base);      //strip leading # sign and convert
    int inv = ~num;                                                 //get inverse
    inverseUserColour = Integer.toHexString(inv).substring(2);    //strip 2 leading digits as html colour codes are 24bits

    if(bean.familyDoctorNo == null || bean.familyDoctorNo.equals("")) {
        famDocName = "";
        famDocSurname = "";
        famDocColour = "";
    }
    else {
        EctProviderData.Provider prov = new EctProviderData().getProvider(bean.familyDoctorNo);
        famDocName =  prov == null || prov.getFirstName() == null ? "" : prov.getFirstName();
        famDocSurname = prov == null || prov.getSurname() == null ? "" : prov.getSurname();
        colourUpdater = new ProviderColourUpdater(bean.familyDoctorNo);
        famDocColour = colourUpdater.getColour();
        if( famDocColour.length() == 0 )
            famDocColour = "#CCCCFF";
    }

    String patientAge = pd.getAge();
    String patientSex = pd.getSex();
    String pAge = Integer.toString(UtilDateUtilities.calcAge(bean.yearOfBirth,bean.monthOfBirth,bean.dateOfBirth));

    java.util.Locale vLocale =(java.util.Locale)session.getAttribute(org.apache.struts.Globals.LOCALE_KEY);

    Map<String, Boolean> echartPreferences = SystemPreferencesUtils.findByKeysAsMap(SystemPreferences.ECHART_PREFERENCE_KEYS);
    Map<String, Boolean> generalSettingsMap = SystemPreferencesUtils.findByKeysAsMap(SystemPreferences.GENERAL_SETTINGS_KEYS);
    boolean mohFileManagementEnabled = SystemPreferencesUtils
            .isReadBooleanPreferenceWithDefault("moh_file_management_enabled", false);
    boolean replaceNameWithPreferred = generalSettingsMap.getOrDefault("replace_demographic_name_with_preferred", false);
    boolean showEmailIndicator = echartPreferences.getOrDefault("echart_email_indicator", false) && StringUtils.isNotEmpty(bean.email);
    boolean showRosterStatus = echartPreferences.getOrDefault("echartShowRosterStatus", false);
    boolean eChartDisplayDemographicNo = echartPreferences.getOrDefault("echart_display_demographic_no", false);

    StringBuilder patientName = new StringBuilder();
    patientName.append(bean.getPatientLastName())
               .append(", ");
    if (replaceNameWithPreferred && !bean.patientPreferredName.isEmpty()) {
        patientName.append(bean.patientPreferredName);
    } else {
        patientName.append(bean.getPatientFirstName());
        if (!bean.patientPreferredName.isEmpty()) {
            patientName.append(" (").append(bean.patientPreferredName).append(")");
        }
    }

    DemographicDao demographicDao = (DemographicDao)SpringUtils.getBean("demographicDao");
    Demographic demographic = (Demographic)demographicDao.getDemographic(demoNo);
    String chartNo = demographic!=null?demographic.getChartNo():null;
    %>

    <c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
    
<%
if ("yes".equalsIgnoreCase(oscar.OscarProperties.getInstance().getProperty("XAPP_LINK_ENABLE", "no"))) {
%>
<div id="divXappIntegrationName" style="display: none;"><%=OscarProperties.getInstance().getProperty("XAPP_INTEGRATION_NAME")%></div>
<%
}
%>

<div style="float:left; width: 100%; padding-left:2px; text-align:left; font-size: 12px; color:<%=inverseUserColour%>; background-color:<%=userColour%>" id="encounterHeader">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
    <security:oscarSec roleName="<%=roleName$%>" objectName="_newCasemgmt.doctorName" rights="r">
    <span style="border-bottom: medium solid <%=famDocColour%>"><bean:message key="oscarEncounter.Index.msgMRP"/>&nbsp;&nbsp;
    <%=Encode.forHtml(famDocName.toUpperCase()+" "+famDocSurname.toUpperCase())%>  </span>
	</security:oscarSec>
    <span class="Header" style="color:<%=inverseUserColour%>; background-color:<%=userColour%>">
        <%
        
            String appointmentNo = request.getParameter("appointmentNo");
            String winName = "Master" + bean.demographicNo;
            String url = "/demographic/demographiccontrol.jsp?demographic_no=" + bean.demographicNo + "&amp;displaymode=edit&amp;dboperation=search_detail&appointment="+appointmentNo;
            String demoNumber = "";
            if (eChartDisplayDemographicNo) {
              demoNumber = "Demo#" + bean.demographicNo;
            }

            SystemPreferences displayChartNo = SystemPreferencesUtils.findPreferenceByName("displayChartNo");
            if (displayChartNo == null) {
              displayChartNo = new SystemPreferences("displayChartNo", "false");
            }
            Boolean enableDisplayChartNo = "true".equals(displayChartNo.getValue());
        %>
        <a href = "#" onClick = "popupPage(700,1000,'<%=winName%>',
        '<c:out value = "${ctx}"/><%=url%>'); return false;"
        title = "<bean:message key = " provider.appointmentProviderAdminDay.msgMasterFile"/>">
        <%= Encode.forHtmlContent(patientName.toString()) + pronouns %>
        </a>
        <%=bean.patientSex%> <%=bean.yearOfBirth%>-<%=bean.monthOfBirth%>-<%=bean.dateOfBirth%>
        <%=bean.patientAge%> <span title = "Demographic No"><%=demoNumber%></span>
        &nbsp;<oscar:phrverification demographicNo="<%=demoNo%>"><bean:message key="phr.verification.link"/></oscar:phrverification> &nbsp;<%=bean.phone%> &nbsp; <%=showEmailIndicator ? "<i title='"+ bean.email +"'>@</i>" : ""%>
		<span id="encounterHeaderExt"></span>

		<% if (enableDisplayChartNo && !StringUtils.isBlank(chartNo)) { %>
		  Chart# <%=chartNo%>
		<% } %>

		<% if (showRosterStatus) { %>
		    <span style="margin-left:10px;">
		        <%= bean.roster %>
		    </span>
		<% } %>
		<input type='button'
			value="<bean:message key="provider.appointmentProviderAdminDay.searchLetter"/>"
			name='searchview' onClick=goSearchView('<%= bean.curProviderNo %>')
			title="<bean:message key="provider.appointmentProviderAdminDay.searchView"/>"
			style="font-size: 10px; padding: 0px; height: 15px; width: 15px"/>
		<security:oscarSec roleName="<%=roleName$%>" objectName="_newCasemgmt.apptHistory" rights="r">
		<a href="javascript:popupPage(400,850,'ApptHist','<c:out value="${ctx}"/>/demographic/demographiccontrol.jsp?demographic_no=<%=bean.demographicNo%>&amp;last_name=<%=bean.patientLastName.replaceAll("'", "\\\\'")%>&amp;first_name=<%=bean.patientFirstName.replaceAll("'", "\\\\'")%>&amp;orderby=appointment_date&amp;displaymode=appt_history&amp;dboperation=appt_history&amp;limit1=0&amp;limit2=25')" style="font-size: 11px;text-decoration:none;" title="<bean:message key="oscarEncounter.Header.nextApptMsg"/>"><span style="margin-left:1px;"><bean:message key="oscarEncounter.Header.nextAppt"/>: <oscar:nextAppt demographicNo="<%=bean.demographicNo%>"/></span></a>
		</security:oscarSec>
        &nbsp;&nbsp;        

        
        <%  
         boolean enableKaiEmr = Boolean.parseBoolean(OscarProperties.getInstance().getProperty("enable_kai_emr", "false"));
         boolean enableTiaHealthPatientPortal = Boolean.parseBoolean(OscarProperties.getInstance().getProperty("enable_tia_health_patient_portal", "false"));
         boolean displayHealthInsuranceNumber = Boolean.parseBoolean(SystemPreferencesUtils.getPreferenceValueByName("display_health_insurance_number", "false"));
         
         if(enableKaiEmr && enableTiaHealthPatientPortal) {
          String portalId = demographicDao.getDemographicById(Integer.parseInt(bean.demographicNo)).getPortalUserId();
         if (portalId == null || portalId.isEmpty()) {%> 
                           
		 <a href="javascript:void(0)"  
               onclick="popupPage(400,400, 'patient_portal', '/<%=OscarProperties.getInstance().getKaiemrDeployedContext()%>/app/components/tiahealth/?demographicNo=<%=bean.demographicNo%>');"><bean:message key="demographic.demographiceditdemographic.registerPatientPortalAccount"/></a>
                                 <% } else {%>
                                 	<a href="javascript:void(0)"  
                                    onclick="popupPage(400,400,'patient_portal', '/<%=OscarProperties.getInstance().getKaiemrDeployedContext()%>/app/components/tiahealth/?remove=true&demographicNo=<%=bean.demographicNo%>');"><bean:message key="demographic.demographiceditdemographic.unRegisterPatientPortalAccount"/></a>
                               
                                 <% } %> 
                                  &nbsp;&nbsp;
                                  <% } %>

        <% if(oscar.OscarProperties.getInstance().hasProperty("ONTARIO_MD_INCOMINGREQUESTOR")){%>
           <a href="javascript:void(0)" onClick="popupPage(600,175,'Calculators','<c:out value="${ctx}"/>/common/omdDiseaseList.jsp?sex=<%=bean.patientSex%>&age=<%=pAge%>'); return false;" ><bean:message key="oscarEncounter.Header.OntMD"/></a>
        <%}
		if (oscar.OscarProperties.getInstance().hasProperty("kaiemr_lab_queue_url")) {%>
            <a href="javascript:void(0)" id="work_lab_button" title='Lab Queue' onclick="popupPage(700, 1215,'work_queue', '<%=OscarProperties.getInstance().getProperty("kaiemr_lab_queue_url")%>?demographicNo=<%=bean.demographicNo%>')">Lab Queue</a>
		<% }
        if (displayHealthInsuranceNumber && demographic != null) {
            String healthInsuranceNumberTerm = "ON".equalsIgnoreCase(oscarProperties.getProperty("billregion")) ? "HIN" : "PHN";
            String demographicHIN =
                    demographic.getHin() != null && !Objects.equals(demographic.getHin(), "")
                            ? demographic.getHin()
                            : "No " + healthInsuranceNumberTerm + " provided";
            %>
            <span id="health_insurance_number" title='Health Insurance Number'><%= healthInsuranceNumberTerm %>: <%= demographicHIN %></span>
		<% }
        %>
        <%=getEChartLinks() %>
        &nbsp;&nbsp;
    <%
      if ("BC".equalsIgnoreCase(oscarProperties.getProperty("billregion"))) {
        String[] effectiveCareConnectURL = CareConnectUtils.getEffectiveCareConnectURL();
        String careConnectUrl = "";
        if (effectiveCareConnectURL != null && effectiveCareConnectURL.length == 2 && !StringUtils.isBlank(effectiveCareConnectURL[1])) {
          careConnectUrl = effectiveCareConnectURL[1];
        }
        if (!careConnectUrl.isEmpty()) {
          String[] orgs = CareConnectUtils.getEffectiveCareConnectOrgs().split(",");
    %>
      <script src="../careconnect/careconnect.js"></script>
      <script>
        function callCareConnect() {
          if(!eHealth) {
            return false;
          }
          var url = '<%= Encode.forHtmlAttribute(careConnectUrl) %>';
          var personalHealthNumber = '<%= Encode.forHtmlAttribute(demographic.getHin()) %>';
          var firstName = '<%= Encode.forHtmlAttribute(demographic.getFirstName()) %>';
          var lastName = '<%= Encode.forHtmlAttribute(demographic.getLastName()) %>';
          var dateOfBirth = '<%= Encode.forHtmlAttribute(demographic.getFormattedDob().replace("-", "")) %>';
          var gender = '<%= Encode.forHtmlAttribute(demographic.getSex()) %>'.toUpperCase();
          if (gender !== 'M' && gender !== 'F' && gender !== 'U' && gender !== 'UN' ) {
            gender = 'U';
          }
          var organization = '';
          if (<%= orgs.length != 0 %>) {
            organization = '<%= Encode.forHtmlAttribute(orgs[0]) %>';
          }
          eHealth.postwith.theFollowingPatientInfo(url, personalHealthNumber, firstName, lastName, dateOfBirth, gender, organization);
        }
      </script>
        &nbsp;&nbsp;
      <a href="javascript:callCareConnect()">CareConnect</a>
    <%
        }
      } else {
    %>
      <a href="javascript:popupPage(800,1000, 'olis_search', '<%=request.getContextPath()%>/olis/Search.jsp?demographicNo=<%=demoNo%>')">OLIS Search</a>
    <% } %>
        &nbsp;&nbsp;
        <% if (mohFileManagementEnabled) { %>
        <a href="javascript:popupPage(900,1100, 'outside_use_report', '<%=request.getContextPath()%>/billing/CA/ON/outsideUse.jsp?demographic_no=<%=demoNo%>')">OU</a>
        &nbsp;&nbsp;
        <% } %>
        <% if ("true".equals(SystemPreferencesUtils.getPreferenceValueByName("echartDisplayInboxLink", ""))) { %>
            <% if (enhancedEnabled) { %>
                <a href="javascript:popupPage(900, 1100, 'inbox', '/<%= OscarProperties.getKaiemrDeployedContext() %>/#/inbox/?providerNo=-1&demoNo=<%= demographic.getDemographicNo() %>&displayInListMode=false');" >Inbox</a>
                &nbsp;&nbsp;
            <% } else { %>
                <a href="javascript:popupPage(900, 1100, 'inbox', '../dms/inboxManage.do?method=prepareForIndexPage&searchProviderAll=-1&isListView=false&demoNo=<%= demographic.getDemographicNo() %>', 'Lab')">Inbox</a>
                &nbsp;&nbsp;
            <% } %>
        <% }
        String cyclesLink = oscar.OscarProperties.getInstance().getProperty("cyclesLink");
        String cyclesLocalLink = oscar.OscarProperties.getInstance().getProperty("cyclesLocalLink");
        String cyclesClientName = StringUtils.trimToNull(oscar.OscarProperties.getInstance().getProperty("cyclesClientName"));
        if (cyclesLink != null && !cyclesLink.isEmpty()) {
          String params = "providerNo=" + bean.providerNo + "&patientId=" + bean.demographicNo;
          if (cyclesClientName == null || request.getServerName().startsWith(cyclesClientName)) {
            cyclesLink = cyclesLink + "?" + params;
          } else {
            cyclesLink = cyclesLocalLink + "?" + params;
          }
        %>
        <a href='<%=cyclesLink%>'
            target = "_blank"
            onclick = "window.open('<%=cyclesLink%>','_blank'); return false;">
            <bean:message key = "global.cycles" />
        </a>
        &nbsp;&nbsp;
        <% } %>
        
        <%
	        String secondCyclesLink = oscar.OscarProperties.getInstance().getProperty("secondCyclesLink");
	        if (StringUtils.trimToNull(secondCyclesLink) != null) {
	       		secondCyclesLink = secondCyclesLink.replace("${demographicId}", String.valueOf(bean.demographicNo));
        %>
		        <a href ='<%= secondCyclesLink %>'
		            target = "_blank"
		            onclick = "window.open('<%= secondCyclesLink %>', '_blank'); return false;">
		            <bean:message key = "global.cl" />
		        </a>
		        &nbsp;&nbsp;
        <% } %>
        
        <% if("yes".equalsIgnoreCase(oscar.OscarProperties.getInstance().getProperty("XAPP_LINK_ENABLE", "no"))) {%>
          &nbsp; &nbsp;<span style="">
            <a href="javascript:void(0)" id="headerXappHref" data-provider="<%=providerNo%>">Avidity</a>
        </span>
        <%} %>
        
		<%
		if (facility.isIntegratorEnabled()){
			int secondsTillConsideredStale = -1;
			try{
				secondsTillConsideredStale = Integer.parseInt(oscar.OscarProperties.getInstance().getProperty("seconds_till_considered_stale"));
			}catch(Exception e){
				MiscUtils.getLogger().error("OSCAR Property: seconds_till_considered_stale did not parse to an int",e);
				secondsTillConsideredStale = -1;
			}
			
			boolean allSynced = true;
			
			try{
				allSynced  = CaisiIntegratorManager.haveAllRemoteFacilitiesSyncedIn(loggedInInfo, loggedInInfo.getCurrentFacility(), secondsTillConsideredStale,false); 
				CaisiIntegratorManager.setIntegratorOffline(session, false);	
			}catch(Exception remoteFacilityException){
				MiscUtils.getLogger().error("Error checking Remote Facilities Sync status",remoteFacilityException);
				CaisiIntegratorManager.checkForConnectionError(session, remoteFacilityException);
			}
			if(secondsTillConsideredStale == -1){  
				allSynced = true; 
			}
		%>
			<%if (CaisiIntegratorManager.isIntegratorOffline(session)) {%>
    			<div style="background: none repeat scroll 0% 0% red; color: white; font-weight: bold; padding-left: 10px; margin-bottom: 2px;"><bean:message key="oscarEncounter.integrator.NA"/></div>
    		<%}else if(!allSynced) {%>
    			<div style="background: none repeat scroll 0% 0% orange; color: white; font-weight: bold; padding-left: 10px; margin-bottom: 2px;"><bean:message key="oscarEncounter.integrator.outOfSync"/>
    			&nbsp;&nbsp;
				<a href="javascript:void(0)" onClick="popupPage(233,600,'ViewICommun','<c:out value="${ctx}"/>/admin/viewIntegratedCommunity.jsp'); return false;" >Integrator</a>
    			</div>
	    	<%}else{%>
	    		<a href="javascript:void(0)" onClick="popupPage(233,600,'ViewICommun','<c:out value="${ctx}"/>/admin/viewIntegratedCommunity.jsp'); return false;" >I</a>
	    	<%}%>
	  <%}%>    
   </span>
</td>
<td align=right>
	<span class="HelpAboutLogout">
	<oscar:help keywords="&Title=Chart+Interface&portal_type%3Alist=Document" key="app.top1" style="font-size:10px;font-style:normal;"/>&nbsp;|
	<a style="font-size:10px;font-style:normal;" href="<%=request.getContextPath()%>/oscarEncounter/About.jsp" target="_new"><bean:message key="global.about" /></a>
	</span>
</td>
</tr>
</table>
</div>

<script>
    window.globalContextPath = '<%=request.getContextPath() %>';

    function goSearchView(s) {
        popupSearchPage(600, 650, "../appointment/appointmentsearch.jsp?provider_no=" + s);
    }

    function popupSearchPage(vheight, vwidth, varpage) {
        if (vheight === "auto") {
            vheight = document.body.clientHeight;
        }

        if (vwidth === "auto") {
            vwidth = document.body.clientWidth;
        }
        var page = "" + varpage;
        windowprops = "height=" + vheight + ",width=" + vwidth + ",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=50,screenY=50,top=0,left=0";
        var popup = window.open(page, "<bean:message key="provider.appointmentProviderAdminDay.apptProvider"/>", windowprops);
        if (popup != null) {
            if (popup.opener == null) {
                popup.opener = self;
            }
            popup.focus();
        }
    }
    function xappPost() {
        new Ajax.Request("<%=request.getContextPath()%>/XappAction.do",
            {   method: 'post',
                parameters: 'providerNo=' + <%=providerNo%> + '&demoNo=' + <%=demoNo%>,
                onSuccess:function(transport) {
                    const res = transport.responseText;
                    console.log("Success Response: " + res);
                    var win = window.open("", "Title",
                        "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1510, height=700"
                    );
                    if (res.includes("redirect:")) {
                        win.location.href = res.substr("redirect:".length);
                    } else {
                        win.document.body.innerHTML = transport.responseText;
                    }
                },
                onFailure:function() {
                    alert("Something went wrong! Please contact Support.");
                }
            });
    }

    jQuery(document).ready(function(){
        const anchor = document.getElementById("headerXappHref");
        if (anchor) {
            anchor.addEventListener('click', xappPost);
        }
    })
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/casemgmt/js/newEncounterHeader.js"></script>
<%!

String getEChartLinks(){
	String str = oscar.OscarProperties.getInstance().getProperty("ECHART_LINK");
		if (str == null){
			return "";
		}
		try{
			String[] httpLink = str.split("\\|"); 
 			return "<a target=\"_blank\" href=\""+httpLink[1]+"\">"+httpLink[0]+"</a>";
		}catch(Exception e){
			MiscUtils.getLogger().error("ECHART_LINK is not in the correct format. title|url :"+str, e);
		}
		return "";
}
%>
