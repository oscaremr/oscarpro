<%--
/*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
--%>

<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="org.oscarehr.util.SpringUtils"%>
<%@page import="org.oscarehr.common.dao.FreeDrawDataDao"%>
<%@page import="oscar.oscarEncounter.data.FreeDrawMetaData"%>
<%@page import="oscar.OscarProperties"%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<html:html locale="true">
<head>
<title>Free Drawing History List</title>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/share/css/OscarStandardLayout.css">
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/share/css/eformStyle.css">
<script type="text/javascript"
	src="<%=request.getContextPath()%>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" language="JavaScript"
	src="<%=request.getContextPath()%>/share/javascript/Oscar.js"></script>
<%
String demographicNo = request.getParameter("demographic_no");
String providerNo = request.getParameter("provNo");
String appointmentNo = request.getParameter("appointment");
%>
</head>
<body class="BodyStyle" vlink="#0000FF">
	<table class="MainTable" id="scrollNumber1" name="freeDrawList">
		<tr class="MainTableTopRow">
			<td class="MainTableTopRowLeftColumn" width="175">My Free
				Drawing List</td>
			<td class="MainTableTopRowRightColumn">
				<table class="TopStatusBar">
					<tr>
						<td>Free Drawing Library</td>
						<td>&nbsp;</td>
						<td style="text-align: right"><oscar:help keywords="eform"
								key="app.top1" /> | <a
							href="javascript:popupStart(300,400,'About.jsp')"><bean:message
									key="global.about" /></a> | <a
							href="javascript:popupStart(300,400,'License.jsp')"><bean:message
									key="global.license" /></a></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="MainTableLeftColumn" valign="top"><a target="_blank"
				href="<%=request.getContextPath()%>/casemgmt/freeDrawPage.jsp?demographic_no=<%=demographicNo%>&provNo=<%=providerNo%>&appointment=<%=appointmentNo%>&parentAjaxId=freeDrawing">Add
					New</a></td>
			<td class="MainTableRightColumn" valign="top">
				<form action="">
					<table class="elements" width="100%">
						<tr>
							<th></th>
							<th>Free Drawing Name</th>
							<th>Free Drawing Type</th>
							<th>Modified Date</th>
						</tr>
						<%
						FreeDrawDataDao freeDao = (FreeDrawDataDao) SpringUtils.getBean(FreeDrawDataDao.class);
						List<FreeDrawMetaData> dataList = freeDao.getAllDrawingListByDemo(Integer.parseInt(demographicNo));

						DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
						for (int i = 0; i < dataList.size(); i++) {
						%>
						<tr bgcolor="<%=((i % 2) == 1) ? "#F2F2F2" : "white"%>">
							<td align="center"><a
								href="<%=request.getContextPath()%>/casemgmt/showDrawPage.jsp?id=<%=dataList.get(i).getId()%>">
									<img style="height: 100%; width: 100px;"
									name="thumbnailFREEDRAW"
									src="<%=request.getContextPath()%>/imageRenderingServlet?source=freedraw_thumbnail_stored&thumbnailId=<%=dataList.get(i).getThumbnailId()%>&demoNo=<%=demographicNo%>">
							</a></td>
							<td align="center"><%=dataList.get(i).getName() == null ? "NA" : dataList.get(i).getName()%></td>
							<td align="center"><%=dataList.get(i).getType()%></td>
							<td align="center"><%=formatter.format(dataList.get(i).getUpdateTime())%></td>
						</tr>
						<%}%>
					</table>
				</form>
			</td>
		</tr>
		<tr>
			<td class="MainTableBottomRowLeftColumn"></td>
			<td class="MainTableBottomRowRightColumn"></td>
		</tr>
	</table>
</body>
</html:html>