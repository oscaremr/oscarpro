/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

/**
 * Supported health record data types.
 * @enum {string}
 * @type {{CONDITION: string, PROCEDURE: string}}
 */
const DataType = {
  CONDITION: '/condition/',
  PREVENTION: '/prevention/',
  PROCEDURE: '/procedure/'
};

const API_URL_COMPONENT = "/api/v1/data-sharing/portal";

const DATA_SHARING_OFF_ALERT =
    "Data sharing feature has not been turned on. "
    + "Contact the admin user to turn it on.";

function pushDataToPatientPortal(oscarBase, type, demographicNumber) {
  jQuery.post(
      oscarBase + API_URL_COMPONENT + type + "demographic/" + demographicNumber,
      "",
      function (response) {
        if (response === false) {
          alert(DATA_SHARING_OFF_ALERT);
        } else {
          location.reload();
        }
      });
  }