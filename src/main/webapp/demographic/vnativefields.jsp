<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ page import="org.oscarehr.common.dao.DemographicFirstNationDao" %>
<%@ page import="org.oscarehr.common.model.DemographicFirstNation" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_demographic" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect(request.getContextPath() + "/securityError.jsp?type=_demographic");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
%>

<%@page import="java.util.*"%>
<%@page import="org.oscarehr.common.dao.DemographicExtDao" %>
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.common.model.DemographicExtKey" %>

<%
String demographic_no = request.getParameter("demo");
DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
Map<String,String> demoExt = Collections.emptyMap();
if(demographic_no != null){
	demoExt = demographicExtDao.getAllValuesForDemo(Integer.parseInt(demographic_no) );
}

DemographicFirstNationDao DemographicFirstNationDao = SpringUtils.getBean(DemographicFirstNationDao.class);
List<DemographicFirstNation> demographicFirstNationList = DemographicFirstNationDao.findAllDemographicFirstNation(); 
Integer id = oscar.util.StringUtils.parseInt(demoExt.get(DemographicExtKey.DEMOGRAPHIC_FIRST_NATION_COMMUNITY.getKey()));
List<DemographicFirstNation> demographicFirstNation = DemographicFirstNationDao.findFirstNationById(id);
if(demoExt == null) demoExt = new HashMap<String,String>();

%>
<style>
.custom-combobox {
position: relative;
display: inline-block;
}
.custom-combobox-toggle {
position: absolute;
top: 0;
bottom: 0;
margin-left: -1px;
padding: 0;
}
.custom-combobox-input {
padding-bottom: 3px;
}
.ui-button-icon-only {
height: 24px;
}
.ui-menu {
overflow: scroll;
max-height: 50%;
}
#firstNationButton{
margin-left: 20px;
}
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
<script type="text/javascript" >
jQuery(document).ready(function() {

	$('#demographicFirstNationCommunity > option').each(function() {
    	    var textValue = $.trim($(this).text());
    	    $(this).text(textValue);
	});
	
});	

function checkDemographicFirstNationCommunity() {
    let btnValue;
    const selectFirstNation = document.getElementById("demographicFirstNationCommunity");
    if (selectFirstNation[selectFirstNation.selectedIndex].dataset["isEditable"] == "true") {
        btnValue = "<bean:message key="demographic.demographiceditdemographic.btnEdit"/>";
        deleteFirstNationButton.style.display = "inline";
    } else {
        btnValue = "<bean:message key="demographic.demographiceditdemographic.btnAddNew"/>";
        deleteFirstNationButton.style.display = "none";
    }
    const firstNationButton = document.getElementById("firstNationButton");
    firstNationButton.value = btnValue;
}
async function newDemographicFirstNationCommunity() {
    const firstNationButton = document.getElementById("firstNationButton");
    let newOpt = "";
    const addNew = firstNationButton.value
            == "<bean:message key="demographic.demographiceditdemographic.btnAddNew"/>";
    if (addNew) {
        newOpt = prompt(
                "<bean:message key="demographic.demographiceditdemographic.msgPromptFirstNationCom"/> : ","");
    } else {
        const optionValue
                = document.getElementById("demographicFirstNationCommunity").options[document.getElementById("demographicFirstNationCommunity").selectedIndex]
                .innerHTML.trim();
        newOpt = prompt(
                "<bean:message key="demographic.demographiceditdemographic.msgPromptFirstNationCom"/> : ", optionValue);
    }
    if (newOpt == null) {
        return;
    } else if (newOpt == "") {
        alert("<bean:message key="demographic.demographiceditdemographic.msgInvalidEntry"/>");
        return;
    }
    if (addNew) {
        const value = await saveDemographicFirstNationCommunity(newOpt);
        const newOption = new Option(newOpt, value);
        newOption.selected = true;
        newOption.dataset["isEditable"] = true;
        document.getElementById("demographicFirstNationCommunity").options[document.getElementById("demographicFirstNationCommunity").length] = newOption;
        firstNationButton.value = "<bean:message key="demographic.demographiceditdemographic.btnEdit"/>";
        deleteFirstNationButton.style.display = "inline";
    } else {
        const currentId = document.getElementById("demographicFirstNationCommunity").value;
        const id = saveDemographicFirstNationCommunity(newOpt, currentId);
        const newOption = new Option(newOpt, currentId);
        newOption.dataset["isEditable"] = true;
        newOption.selected = true;
        document.getElementById("demographicFirstNationCommunity").options[document.getElementById("demographicFirstNationCommunity").selectedIndex]
                = newOption;
    }
    $("#demographicFirstNationCommunity").trigger("change");
}

async function saveDemographicFirstNationCommunity(newOpt, currentId) {
    let id = '0';
    if (typeof currentId == 'undefined') {
        let response = await fetch ("<%= request.getContextPath() %>/saveDemographicFirstNationCommunity.do?method=addDemographicFirstNationCommunity&demographicFirstNationCommunity=" + newOpt, {
            method: 'GET',
        });
        id = await response.text();
    } else {
        fetch ("<%= request.getContextPath() %>/saveDemographicFirstNationCommunity.do?method=editDemographicFirstNationCommunity&demographicFirstNationCommunity="
                + newOpt + "&id=" + currentId, {
	    method: 'GET'
	});
    }
    return id.trim();
}

async function deleteDemographicFirstNationCommunity() {
    const deleteFirstNationButton = document.getElementById("deleteFirstNationButton");
    const firstNationButton = document.getElementById("firstNationButton");
    const firstNationCommunity = document.getElementById("demographicFirstNationCommunity");
    const currentId = document.getElementById("demographicFirstNationCommunity").value;
    const optionValue
            = document.getElementById("demographicFirstNationCommunity").options[document.getElementById("demographicFirstNationCommunity").selectedIndex].innerHTML.trim();
    const delOpt
            = confirm("Are you sure want to delete " + optionValue + "? This will remove this option from all demographics");
    if (!delOpt) {
        return;
    }
    fetch ("<%= request.getContextPath() %>/saveDemographicFirstNationCommunity.do?method=deleteDemographicFirstNationCommunityById&id=" + currentId, {
        method: 'GET'
    });
    firstNationCommunity.remove(firstNationCommunity.selectedIndex);
    firstNationButton.value = "<bean:message key="demographic.demographiceditdemographic.btnAddNew"/>";
    deleteFirstNationButton.style.display = "none";
    $("#demographicFirstNationCommunity").trigger("change");
}

$(function() {
    $.widget(
      "custom.combobox", {
        _create: function() {
          this.wrapper = $("<span>").addClass(
            "custom-combobox").insertAfter(
            this.element);
          this.element.hide();
          this._createAutocomplete();
          this._createShowAllButton();
        },
        _createAutocomplete: function() {
          var selected = this.element
            .children(":selected"),
            value = selected.val() ? selected.text() : "";
          this.input = $("<input>")
            .appendTo(this.wrapper)
            .val(value)
            .attr("title", "")
            .addClass(
              "custom-combobox-input")
            .autocomplete({
              delay: 0,
              minLength: 0,
              source: this._source.bind(this)
            })
            .tooltip({
              classes: {
                "ui-tooltip": "ui-state-highlight"
              }
            });

          this._on(this.input, {
            autocompleteselect: function(event, ui) {
              ui.item.option.selected = true;
              this._trigger("select", event, {
                item: ui.item.option
              });
              ui.item.option.parentElement.onchange();
            },
            autocompletechange: "_removeIfInvalid"
          });
        },
        _createShowAllButton: function() {
          var input = this.input,  wasOpen = false;
          $("<a>")
            .attr("tabIndex", -1)
            .tooltip()
            .appendTo(this.wrapper)
            .button({
              icons: {
                primary: "ui-icon-triangle-1-s"
              },
              text: false
            })
            .removeClass("ui-corner-all")
            .addClass(
              "custom-combobox-toggle ui-corner-right")
            .on(
              "mousedown",
              function() {
                wasOpen = input
                  .autocomplete(
                    "widget")
                  .is(":visible");
              }).on("click", function() {
              input.trigger("focus");

              if (wasOpen) {
                return;
              }
              // Pass empty string as value to search for, displaying all results
              input.autocomplete("search", "");
            });
        },
        _source: function(request, response) {
          var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
          response(this.element.children("option").map(function() {
            var text = $(this).text().trim();
            if (this.value && (!request.term || matcher.test(text)))
              return {
                label: text,
                value: text,
                option: this
              };
          }));
        },

        _removeIfInvalid: function(event, ui) {
          // Selected an item, nothing to do
          if (ui.item) {
            return;
          }

          // Search for a match (case-insensitive)
          var value = this.input.val(),
            valueLowerCase = value.toLowerCase(),
            valid = false;
          this.element
            .children("option")
            .each(
              function() {
                if ($(this).text().toLowerCase().trim() === valueLowerCase.trim()) {
                  this.selected = valid = true;
                  return false;
                }
              });

          // Found a match, nothing to do
          if (valid) {
            return;
          }
          // Remove invalid value
          this.input.val("").attr("title",
              value + " didn't match any item")
            .tooltip("open");
          this.element.val("");
          
          this._delay(function() {
            this.input.tooltip("close").attr("title",
              "");
          }, 2500);
          this.input.autocomplete("instance").term = "";
        },

        _destroy: function() {
          this.wrapper.remove();
          this.element.show();
        }
      });
      $("#demographicFirstNationCommunity").on("change", function() {
          $("#demographicFirstNationCommunity").combobox("destroy");
          $("#demographicFirstNationCommunity").combobox();
      });
      $("#demographicFirstNationCommunity").combobox();
      $("#toggle").on("click", function() {
          $("#demographicFirstNationCommunity").toggle();
      });
});

</script>
<tr>
	<td align="right"><b>Status #:</b></td>
	<td align="left" >
            <input type="text" name="statusNum" value="<%=StringUtils.trimToEmpty(demoExt.get("statusNum"))%>">
	    <input type="hidden" name="statusNumOrig" value="<%=StringUtils.trimToEmpty(demoExt.get("statusNum"))%>">
	</td>
        <td align="right"><b>First Nation Community:</b></td>
        <td align="left">
        <%
            Integer bandName = 0;
            if (!demographicFirstNation.isEmpty() && demographicFirstNation.get(0).getId() != null) {
                bandName = demographicFirstNation.get(0).getId();
            } 
        %>
        <select name="demographicFirstNationCommunity" id="demographicFirstNationCommunity" class="demographicFirstNationCommunity" onchange="checkDemographicFirstNationCommunity()" >
            <option value="" <%= !bandName.equals(0) ? "selected" : "" %>></option>
            <%
                for (DemographicFirstNation thisDemographicFirstNation : demographicFirstNationList) {
                    String selected = bandName.equals(thisDemographicFirstNation.getId()) ? "selected" : "";
            %>
                    <option value="<%= thisDemographicFirstNation.getId() %>"
                                    data-is-editable="<%= thisDemographicFirstNation.isEditable() %>"
                        <%= selected %>>
                        <%= thisDemographicFirstNation.getFirstNationCommunity() %>
                    </option>
            <% } %>
        </select>
        <input type="button" id="firstNationButton" onClick="newDemographicFirstNationCommunity()" 
                value="<bean:message key="demographic.demographiceditdemographic.btnAddNew"/>">
        <input type="button" id="deleteFirstNationButton" onClick="deleteDemographicFirstNationCommunity()" style="display: none;"
                value="<bean:message key="demographic.demographiceditdemographic.btnDelete"/>">
        <input type="hidden" name="demographicFirstNationCommunityOrig" value="<%= StringUtils.trimToEmpty(demoExt.get(DemographicExtKey.DEMOGRAPHIC_FIRST_NATION_COMMUNITY.getKey())) %>">
        <script>checkDemographicFirstNationCommunity();</script>
        </td>
</tr>

<tr>
	<td align="right"><b>Ethnicity:</b></td>
	<td align="left">
	<% String ethnicity = StringUtils.trimToEmpty(demoExt.get("ethnicity")); %>
	<select name="ethnicity">
		<option value="-1" <%=getSel(ethnicity,"-1")%>>Not Set</option>
		<option value="1" <%=getSel(ethnicity,"1")%>>Status
		On-reserve</option>
		<option value="2" <%=getSel(ethnicity,"2")%>>Status
		Off-reserve</option>
		<option value="3" <%=getSel(ethnicity,"3")%>>Non-status
		on-reserve</option>
		<option value="4" <%=getSel(ethnicity,"4")%>>Non-status
		off-reserve</option>
		<option value="5" <%=getSel(ethnicity,"5")%>>Metis</option>
		<option value="6" <%=getSel(ethnicity,"6")%>>Inuit</option>
		<option value="7" <%=getSel(ethnicity,"7")%>>Asian</option>
		<option value="8" <%=getSel(ethnicity,"8")%>>Caucasian</option>
		<option value="9" <%=getSel(ethnicity,"9")%>>Hispanic</option>
		<option value="10" <%=getSel(ethnicity,"10")%>>Black</option>
		<option value="11" <%=getSel(ethnicity,"11")%>>Other</option>
	</select> <input type="hidden" name="ethnicityOrig"
		value="<%=StringUtils.trimToEmpty(demoExt.get("ethnicity"))%>">
	</td>
	<td align="right"><b>Area:</b></td>
	<td align="left">
	<% String area =   StringUtils.trimToEmpty(demoExt.get("area")); %> <select
		name="area">
		<option value="-1" <%=getSel(area,"-1")%>>Not Set</option>
		<option value="1" <%=getSel(area,"1")%>>CHA1</option>
		<option value="2" <%=getSel(area,"2")%>>CHA2</option>
		<option value="3" <%=getSel(area,"3")%>>CHA3</option>
		<option value="4" <%=getSel(area,"4")%>>CHA4</option>
		<option value="5" <%=getSel(area,"5")%>>CHA5</option>
		<option value="6" <%=getSel(area,"6")%>>CHA6</option>
		<option value="7" <%=getSel(area,"7")%>>Richmond</option>
		<option value="8" <%=getSel(area,"8")%>>North or West
		Vancouver</option>
		<option value="9" <%=getSel(area,"9")%>>Surrey</option>
		<option value="10" <%=getSel(area,"10")%>>On-Reserve</option>
		<option value="14" <%=getSel(area,"14")%>>Off-Reserve</option>
		<option value="11" <%=getSel(area,"11")%>>Homeless</option>
		<option value="12" <%=getSel(area,"12")%>>Out of Country
		Residents</option>
		<option value="13" <%=getSel(area,"13")%>>Other</option>
	</select> <input type="hidden" name="areaOrig"
		value="<%=StringUtils.trimToEmpty(demoExt.get("area"))%>"></td>

</tr>
<%!
    String getSel(String s, String s2){
        if (s != null && s2 != null && s.equals(s2)){
            return "selected";
        }
        return "";
    }

%>
