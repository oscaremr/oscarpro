<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ page import="java.util.*"%>

<%@ page import="apps.health.pillway.PillwayManager" %>
<%@ page import="ca.kai.util.MapUtils" %>
<%@ page import="java.net.URLEncoder"%>
<%@ page import="org.oscarehr.common.OtherIdManager"%>
<%@ page import="org.apache.commons.lang.StringUtils"%>
<%@ page import="org.oscarehr.common.dao.AlertDao" %>
<%@ page import="org.oscarehr.common.dao.DemographicDao" %>
<%@ page import="org.oscarehr.common.dao.DemographicGroupLinkDao" %>
<%@ page import="org.oscarehr.common.dao.DemographicSiteDao" %>
<%@ page import="org.oscarehr.common.dao.WaitingListDao" %>
<%@ page import="org.oscarehr.common.model.Alert" %>
<%@ page import="org.oscarehr.common.model.ConsentType" %>
<%@ page import="org.oscarehr.common.model.Demographic" %>
<%@ page import="org.oscarehr.common.model.DemographicSite" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.oscarehr.managers.PatientConsentManager" %>
<%@ page import="org.oscarehr.managers.DemographicManager" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProgramDao" %>
<%@ page import="org.oscarehr.PMmodule.service.ProgramManager" %>
<%@ page import="org.oscarehr.PMmodule.service.AdmissionManager" %>
<%@ page import="org.oscarehr.PMmodule.web.GenericIntakeEditAction" %>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="org.oscarehr.util.MiscUtils"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.MyDateFormat"%>
<%@ page import="oscar.oscarWaitingList.WaitingList"%>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<%@ taglib uri="/WEB-INF/caisi-tag.tld" prefix="caisi"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_demographic" rights="w" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect(request.getContextPath() + "/securityError.jsp?type=_demographic");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
	AdmissionManager am = SpringUtils.getBean(AdmissionManager.class);
	DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
	DemographicGroupLinkDao demographicGroupLinkDao = SpringUtils.getBean(DemographicGroupLinkDao.class);
	ProgramDao programDao = SpringUtils.getBean(ProgramDao.class);
	ProgramManager pm = SpringUtils.getBean(ProgramManager.class);
	WaitingListDao waitingListDao = SpringUtils.getBean(WaitingListDao.class);

	Map<String, Boolean> masterFilePreferences = SystemPreferencesUtils.findByKeysAsMap(SystemPreferences.MASTER_FILE_PREFERENCE_KEYS);
	LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
	String loggedInProviderNumber = loggedInInfo.getLoggedInProviderNo();
%>
<html:html locale="true">
<head>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<link rel="stylesheet" href="../web.css" />
<script LANGUAGE="JavaScript">
    <!--
    function start(){
      this.focus();
      this.resizeTo(1000,700);
    }
    function closeit() {
      //parent.refresh();
      close();
    }
    //-->
</script>
</head>
<body onload="start()" bgproperties="fixed" topmargin="0" leftmargin="0" rightmargin="0">
<center>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr bgcolor="#486ebd">
		<th align="CENTER"><font face="Helvetica" color="#FFFFFF">
		<bean:message key="demographic.demographicaddarecord.title" /></font></th>
	</tr>
</table>
<form method="post" name="addappt">
<%
        //If this is from adding appointment screen, then back to there
        String provider_no2 = request.getParameter("provider_no");
        String bFirstDisp2 = request.getParameter("bFirstDisp");
        String year2 = request.getParameter("year");
        String month2 = request.getParameter("month");
        String day2 = request.getParameter("day");
        String start_time2 = request.getParameter("start_time");
        String end_time2 = request.getParameter("end_time");
        String duration2 = request.getParameter("duration");
        boolean addFamily = request.getParameter("submit") != null
				&& request.getParameter("submit").equalsIgnoreCase("Save & Add Family Member");

	String curUser_no = (String)session.getAttribute("user");
	Demographic demographic = new Demographic(request, curUser_no);

    // add checking hin duplicated record, if there is a HIN number
    // added check to see if patient has a bc health card and has a version code of 66, in this case you are aloud to have dup hin
    boolean hinDupCheckException = false;
     String hcType = request.getParameter("hc_type");
     String ver  = request.getParameter("ver");
     if (hcType != null && ver != null && hcType.equals("BC") && ver.equals("66")){
        hinDupCheckException = true;
     }

    if(request.getParameter("hin")!=null && request.getParameter("hin").length()>5 && !hinDupCheckException) {
  		//oscar.oscarBilling.ca.on.data.BillingONDataHelp dbObj = new oscar.oscarBilling.ca.on.data.BillingONDataHelp();
		//String sql = "select demographic_no from demographic where hin=? and year_of_birth=? and month_of_birth=? and date_of_birth=?";
		String paramNameHin = request.getParameter("hin").trim();
		List<Demographic> demographics = demographicDao.searchByHealthCard(paramNameHin);
		if(demographics.size()>0){
%>
		***<font color='red'><bean:message key="demographic.demographicaddarecord.msgDuplicatedHIN" /></font>***<br><br>
		<a href=# onClick="history.go(-1);return false;"><b>&lt;-<bean:message key="global.btnBack" /></b></a>
<%
		return;
		}
	}
    if(demographic.getMyOscarUserName() != null && !demographic.getMyOscarUserName().trim().isEmpty()){
     	Demographic myoscarDemographic = demographicDao.getDemographicByMyOscarUserName(demographic.getMyOscarUserName());
     	if(myoscarDemographic != null){

%>
			***<font color='red'><bean:message key="demographic.demographicaddarecord.msgDuplicatedPHR" /></font>
			***<br><br><a href=# onClick="history.go(-1);return false;"><b>&lt;-<bean:message key="global.btnBack" /></b></a>
<%
			return;
     	}
    }
	StringBuilder bufName = new StringBuilder(request.getParameter("last_name")+ ","+ request.getParameter("first_name") );
	StringBuilder  bufChart = new StringBuilder(StringUtils.trimToEmpty("chart_no"));
	StringBuilder bufDoctorNo = new StringBuilder( StringUtils.trimToEmpty("provider_no") );

	String providerNo = (String) session.getValue("user");
	DemographicManager.createDemographic(demographic, true, true);
	Integer demographicNo = demographic.getDemographicNo();
    String demographicNoString = demographicNo.toString();

          GenericIntakeEditAction gieat = new GenericIntakeEditAction();
          gieat.setAdmissionManager(am);
          gieat.setProgramManager(pm);
          String bedP = request.getParameter("rps");
          if (bedP == null) {
        	  int bedPID=programDao.getProgramByName("OSCAR").getId();
        	  gieat.admitBedCommunityProgram(demographicNo,loggedInInfo.getLoggedInProviderNo(),bedPID,"","",null);
          } else {
          	  gieat.admitBedCommunityProgram(demographicNo,loggedInInfo.getLoggedInProviderNo(),Integer.parseInt(bedP),"","",null);
          }

          String[] servP = request.getParameterValues("sp");
          if(servP!=null&&servP.length>0){
	  Set<Integer> s = new HashSet<Integer>();
            for(String _s:servP) s.add(Integer.parseInt(_s));
            gieat.admitServicePrograms(demographicNo,loggedInInfo.getLoggedInProviderNo(),s,"",null);
          }

		if( OscarProperties.getInstance().getBooleanProperty("USE_NEW_PATIENT_CONSENT_MODULE", "true") ) {
			// Retrieve and set patient consents.
			PatientConsentManager patientConsentManager = SpringUtils.getBean( PatientConsentManager.class );
			List<ConsentType> consentTypes = patientConsentManager.getActiveConsentTypes();
			boolean explicitConsent = Boolean.TRUE;

			for( ConsentType consentType : consentTypes )
			{
				String type = consentType.getType();
				String consentRecord = request.getParameter(type);

				if( consentRecord != null )
				{
					//either opt-in or opt-out is selected
					boolean optOut = Integer.parseInt(consentRecord) == 1;
					patientConsentManager.addEditConsentRecord(loggedInInfo, demographicNo, consentType.getId(), explicitConsent, optOut);
				}

			}
		}

		String chartAlertText = request.getParameter("chartAlertText");
		if (!StringUtils.isBlank(chartAlertText)) {
			Alert chartAlert = new Alert(demographicNo, Alert.AlertType.CHART, chartAlertText);
			AlertDao alertDao = SpringUtils.getBean(AlertDao.class);
			alertDao.saveEntity(chartAlert);
		}

		// Demographic Groups
		String[] groups = request.getParameterValues("demographicGroups");
		if (groups != null) {
			for (String group : groups) {
				if (group != null) {
					// An empty group number indicates the 'None' group
					if (group.length() > 0) {
						try {
							int groupId = Integer.parseInt(group);
							demographicGroupLinkDao.add(demographicNo, groupId);
						} catch (Exception e) {
							MiscUtils.getLogger()
									.error("Error parsing demographic group number", e);
						}
					}
				} else {
					MiscUtils.getLogger().warn("Null demographic group number passed in.");
				}
			}
		}
       //for the IBD clinic
		OtherIdManager.saveIdDemographic(demographicNoString, "meditech_id", request.getParameter("meditech_id"));

	// Update demographic, add demographicExt values and save
	DemographicManager.updateDemographicAndSave(request, demographic, loggedInProviderNumber, true);
	if (MapUtils.getOrDefault(masterFilePreferences, "populateChartNoWithDemographicNo", false)
			&& StringUtils.isEmpty(request.getParameter("chart_no"))) {
		demographic.setChartNo(demographicNo.toString());
		DemographicManager.saveDemographic(demographic, true);
	}

	//multiple sites
       if (org.oscarehr.common.IsPropertiesOn.isMultisitesEnable()) {
         DemographicSiteDao demographicSiteDao = (DemographicSiteDao) SpringUtils.getBean("demographicSiteDao");
         String[] sites = request.getParameterValues("sites");
         if (sites != null) {
			 for (String site : sites) {
				 DemographicSite demographicSite = new DemographicSite();
				 demographicSite.setDemographicId(Integer.valueOf(demographicNo.toString()));
				 demographicSite.setSiteId(Integer.valueOf(site));
				 demographicSiteDao.persist(demographicSite);
			 }
         }
       }

        //add to waiting list if the waiting_list parameter in the property file is set to true
        WaitingList wL = WaitingList.getInstance();
        if(wL.getFound()){

            String[] paramWLPosition = new String[1];
            paramWLPosition[0] = request.getParameter("list_id");
            if(paramWLPosition[0].compareTo("")!=0){

                List<Long> positionList = new ArrayList<Long>();
                List<org.oscarehr.common.model.WaitingList> waitingListList = waitingListDao.findByWaitingListId(new Integer(1));

                if(waitingListList != null) {

	                for(org.oscarehr.common.model.WaitingList waitingList : waitingListList) {
	                	positionList.add(waitingList.getPosition());
	                }
	                Long maxPosition = 0L;
	                if(positionList.size()> 0) {
	                	maxPosition = Collections.max(positionList);
	                }

                    String listId = request.getParameter("list_id");
                    if(listId != null && !listId.equals("") && !listId.equals("0")) {
	                    org.oscarehr.common.model.WaitingList waitingList = new org.oscarehr.common.model.WaitingList();
	                    waitingList.setListId(Integer.parseInt(request.getParameter("list_id")));
	                    waitingList.setDemographicNo(demographicNo);
	                    waitingList.setNote(request.getParameter("waiting_list_note"));
	                    waitingList.setPosition(maxPosition +1);
	                    waitingList.setOnListSince(MyDateFormat.getSysDate(request.getParameter("waiting_list_referral_date")));
	                    waitingList.setIsHistory("N");
	                    waitingList.setOnListSince(new java.util.Date());
	                    waitingListDao.persist(waitingList);
                    }
                }
            }


        } //end of waiting list

        // Add Pillway to patient
        PillwayManager pillwayManager = SpringUtils.getBean(PillwayManager.class);
        pillwayManager.addToDemographic(demographicNo);

        //if(request.getParameter("fromAppt")!=null && request.getParameter("provider_no").equals("1")) {
        if(start_time2!=null && !start_time2.equals("null")) {
	%>
	<script language="JavaScript">
	<!--
	document.addappt.action="../appointment/addappointment.jsp?user_id=<%=Encode.forJavaScript(request.getParameter("creator"))%>&provider_no=<%=provider_no2%>&bFirstDisp=<%=bFirstDisp2%>&appointment_date=<%=request.getParameter("appointment_date")%>&year=<%=year2%>&month=<%=month2%>&day=<%=day2%>&start_time=<%=start_time2%>&end_time=<%=end_time2%>&duration=<%=duration2%>&name=<%=URLEncoder.encode(bufName.toString())%>&chart_no=<%=URLEncoder.encode(bufChart.toString())%>&bFirstDisp=false&demographic_no=<%=demographicNoString%>&messageID=<%=request.getParameter("messageId")%>&doctor_no=<%=Encode.forJavaScript(bufDoctorNo.toString())%>&notes=<%=Encode.forJavaScript(request.getParameter("appointment-notes"))%>&reason=<%=Encode.forJavaScript(request.getParameter("reason"))%>&location=<%=Encode.forJavaScript(request.getParameter("location"))%>&resources=<%=request.getParameter("resources")%>&type=<%=request.getParameter("type")%>&style=<%=request.getParameter("style")%>&billing=<%=request.getParameter("billing")%>&status=<%=Encode.forJavaScript(request.getParameter("status"))%>&createdatetime=<%=request.getParameter("createdatetime")%>&creator=<%=Encode.forJavaScript(request.getParameter("creator"))%>&remarks=<%=Encode.forJavaScript(request.getParameter("remarks"))%>";
	document.addappt.submit();
	//-->
	</SCRIPT>
	<% }

		if(addFamily) {
            session.setAttribute("address", demographic.getAddress());
			session.setAttribute("city", demographic.getCity());
			session.setAttribute("province", demographic.getProvince());
			session.setAttribute("postal", demographic.getPostal());
			session.setAttribute("phone", demographic.getPhone());
			response.sendRedirect("demographicaddarecordhtm.jsp");
		} %>
</form>



<p>
<h2><bean:message key="demographic.demographicaddarecord.msgSuccessful" /></h2>
<a href="demographiccontrol.jsp?demographic_no=<%=demographicNo%>&displaymode=edit&dboperation=search_detail"><bean:message key="demographic.demographicaddarecord.goToRecord"/></a>

<caisi:isModuleLoad moduleName="caisi">
<br/>
<a href="../PMmodule/ClientManager.do?id=<%=demographicNo%>"><bean:message key="demographic.demographicaddarecord.goToCaisiRecord"/> (<a href="#"  onclick="popup(700,1027,'demographiccontrol.jsp?demographic_no=<%=demographicNo%>&displaymode=edit&dboperation=search_detail')">New Window</a>)</a>
</caisi:isModuleLoad>


<caisi:isModuleLoad moduleName="caisi">
<br/>
<a href="../PMmodule/ClientManager.do?id=<%=demographicNo%>"><bean:message key="demographic.demographicaddarecord.goToCaisiRecord"/></a>
</caisi:isModuleLoad>


<p></p>
<%@ include file="footer.jsp"%></center>
</body>
</html:html>
