<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ page import="java.util.*"%>
<%@ page import="oscar.log.*"%>
<%@ page import="org.oscarehr.common.model.*" %>
<%@ page import="org.oscarehr.common.dao.*" %>

<%@ page import="java.util.Date" %>
<%@ page import="org.apache.commons.lang.StringUtils"%>
<%@ page import="org.oscarehr.common.IsPropertiesOn" %>
<%@ page import="org.oscarehr.common.OtherIdManager"%>
<%@ page import="org.oscarehr.common.model.Alert.AlertType" %>
<%@ page import="org.oscarehr.managers.DemographicManager" %>
<%@ page import="org.oscarehr.managers.PatientConsentManager" %>
<%@ page import="org.oscarehr.PMmodule.model.Program" %>
<%@ page import="org.oscarehr.PMmodule.web.GenericIntakeEditAction" %>
<%@ page import="org.oscarehr.PMmodule.service.ProgramManager" %>
<%@ page import="org.oscarehr.PMmodule.service.AdmissionManager" %>
<%@ page import="org.oscarehr.provider.model.PreventionManager"%>
<%@ page import="org.oscarehr.PMmodule.exception.AdmissionException" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.util.MiscUtils"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.oscarDemographic.data.DemographicNameAgeString" %>
<%@ page import="oscar.oscarWaitingList.util.WLWaitingListUtil"%>
<%@ page import="oscar.util.ChangedField" %>

<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/wellAiVoice.tld" prefix="well-ai-voice"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_demographic" rights="w" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect(request.getContextPath() + "/securityError.jsp?type=_demographic");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
	AlertDao alertDao = SpringUtils.getBean(AlertDao.class);
	AppointmentReminderDao appointmentReminderDao = SpringUtils.getBean(AppointmentReminderDao.class);
	DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
	DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
	DemographicGroupLinkDao demographicGroupLinkDao = SpringUtils.getBean(DemographicGroupLinkDao.class);
	OscarAppointmentDao appointmentDao = SpringUtils.getBean(OscarAppointmentDao.class);
	UserPropertyDAO propertyDao = SpringUtils.getBean(UserPropertyDAO.class);
	WaitingListDao waitingListDao = SpringUtils.getBean(WaitingListDao.class);
	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
	String loggedInProviderNumber = loggedInInfo.getLoggedInProviderNo();
%>
<html:html locale="true">
<head>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script></head>
<body>
	<well-ai-voice:script/>
<center>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr bgcolor="#486ebd">
		<th align="CENTER"><font face="Helvetica" color="#FFFFFF">
		UPDATE demographic RECORD</font></th>
	</tr>
</table>
<%
	Demographic demographic = demographicDao.getDemographic(request.getParameter("demographic_no"));
	Demographic oldDemographic = new Demographic(demographic);

	boolean updateFamily = request.getParameter("submit") != null
			&& request.getParameter("submit").equalsIgnoreCase("Save & Update Family Members");

	List<Demographic> family = null;
	if (updateFamily){
		 family = demographicDao.getDemographicFamilyMembers(
				 String.valueOf(oldDemographic.getDemographicNo())
		 );
	}

	// Update Freshbooks section
	String demoNo = request.getParameter("demographic_no");
	int demographicNo = Integer.parseInt(demoNo);
	List<DemographicExt> demoProviders = demographicExtDao.getMultipleDemographicExt(demographicNo, "freshbooksId");
	List<Appointment> demoAppointments = appointmentDao.getAllByDemographicNo(demographicNo);
	List<AppointmentReminder> demoReminders = new ArrayList<AppointmentReminder>();

	for(Appointment appointment : demoAppointments) {
	    AppointmentReminder appointmentReminder = appointmentReminderDao.getByAppointmentNo(appointment.getId());
	    if (appointmentReminder != null) {
	        demoReminders.add(appointmentReminder);
		}
	}

	String updateEmail = demographic.getEmail();
	String updateFirstName = demographic.getFirstName();
	String updateLastName = demographic.getLastName();
	String updateHomePhone = demographic.getPhone();
	String updateStreet = demographic.getAddress();
	String updatePostal = demographic.getPostal();
	String updateProvince = demographic.getProvince();
	String updateCity = demographic.getCity();

	if (!request.getParameter("email").equalsIgnoreCase(demographic.getEmail()))
	{
		updateEmail = request.getParameter("email");
		for (AppointmentReminder appointmentReminder : demoReminders) {
				appointmentReminder.setReminderEmail(updateEmail);
				appointmentReminderDao.merge(appointmentReminder);
			}
	}

	if (!request.getParameter("first_name").equalsIgnoreCase(demographic.getFirstName()))
	{
		updateFirstName = request.getParameter("first_name");
	}

	if (!request.getParameter("last_name").equalsIgnoreCase(demographic.getLastName()))
	{
		updateLastName = request.getParameter("last_name");
	}

	if (!request.getParameter("phone").equalsIgnoreCase(demographic.getPhone()))
	{
		updateHomePhone = request.getParameter("phone");
		updateHomePhone = updateHomePhone.replaceAll("[^0-9]", "");
		if (!updateHomePhone.equals("")) {
            if (updateHomePhone.charAt(0) == '1') {
                updateHomePhone = "+" + updateHomePhone;
            } else {
                updateHomePhone = "+1" + updateHomePhone;
            }
        }
		for (AppointmentReminder appointmentReminder : demoReminders) {
			appointmentReminder.setReminderPhone(updateHomePhone);
			appointmentReminderDao.merge(appointmentReminder);
		}
	}

	Map<String, String> demoExt = demographicExtDao.getAllValuesForDemo(demographicNo);

	if (!StringUtils.trimToEmpty(request.getParameter("demo_cell")).equalsIgnoreCase(StringUtils.trimToEmpty(demoExt.get("demo_cell"))))
	{
        String updateCell = request.getParameter("demo_cell");
        updateCell = updateCell.replaceAll("[^0-9]", "");
        if (!updateCell.equals("")) {
            if (updateCell.charAt(0) == '1') {
                updateCell = "+" + updateCell;
            } else {
                updateCell = "+1" + updateCell;
            }
        }
        for (AppointmentReminder appointmentReminder : demoReminders) {
            appointmentReminder.setReminderCell(updateCell);
            appointmentReminderDao.merge(appointmentReminder);
        }
	}

	if (!request.getParameter("address").equalsIgnoreCase(demographic.getAddress()))
	{
		updateStreet = request.getParameter("address");
	}

	if (!request.getParameter("city").equalsIgnoreCase(demographic.getCity()))
	{
		updateCity = request.getParameter("city");
	}

	if (!request.getParameter("province").equalsIgnoreCase(demographic.getProvince()))
	{
		updateProvince = request.getParameter("province");
	}

	if (!request.getParameter("postal").equalsIgnoreCase(demographic.getPostal()))
	{
		updatePostal = request.getParameter("postal");
	}

	if (!demoProviders.isEmpty())
	{
		for (DemographicExt demo : demoProviders)
		{
		    UserProperty prop = propertyDao.getProp(demo.getProviderNo(), UserProperty.PROVIDER_FRESHBOOKS_ID);
			if (prop!=null)
			{
				String provFreshbooksId = prop.getValue();
				String demoFreshbooksId = demo.getValue();
				FreshbooksService fs = new FreshbooksService();
				if(provFreshbooksId!=null && demoFreshbooksId != null && !provFreshbooksId.equals("") && !demoFreshbooksId.equals(""))
				{
					fs.updateClient(demoFreshbooksId, provFreshbooksId, updateEmail, updateFirstName, updateLastName, updateHomePhone, updateStreet, updateCity, updateProvince, updatePostal, false);
				}
			}
		}
	}

	if( OscarProperties.getInstance().getBooleanProperty("USE_NEW_PATIENT_CONSENT_MODULE", "true") ) {
		// Retrieve and set patient consents.
		PatientConsentManager patientConsentManager = SpringUtils.getBean( PatientConsentManager.class );
		List<ConsentType> consentTypes = patientConsentManager.getActiveConsentTypes();			
		for( ConsentType consentType : consentTypes )
		{
			String type = consentType.getType();
			String consentRecord = request.getParameter(type);
			int deleteme = 0;
			if(!StringUtils.isEmpty(request.getParameter("deleteConsent_" + type))) {
				deleteme = Integer.parseInt(request.getParameter("deleteConsent_" + type));
			}
			
			if( consentRecord != null )
			{
				//either opt-in or opt-out is selected
				boolean optOut = Integer.parseInt(consentRecord) == 1;
				patientConsentManager.addEditConsentRecord(loggedInInfo, demographicNo, consentType.getId(), true, optOut);
			} 
			else if(deleteme == 1)
			{
				patientConsentManager.deleteConsent(loggedInInfo, demographicNo, consentType.getId());
			}
		}
	}

	// Update demographic, add demographicExt values and save
	DemographicManager.updateDemographicAndSave(request, demographic, loggedInProviderNumber, false);

	// Demographic Groups
	String[] groupsOrig = request.getParameterValues("demographicGroupsOrig");
	String[] groups = request.getParameterValues("demographicGroups");

	if (groupsOrig != null) {
		for (int i=0; i < groupsOrig.length; i++) {
			try {
				int groupId = Integer.parseInt( groupsOrig[i] );
				demographicGroupLinkDao.remove(demographicNo, groupId);
			} catch (Exception e) {
				MiscUtils.getLogger().error("Error parsing demographic group number", e);
			}
		}
	}

	if (groups != null) {
		for (int i=0; i < groups.length; i++) {
			if (groups[i] != null) {
				// An empty group number indicates the 'None' group
				if (groups[i].length() > 0) {
					try {
						int groupId = Integer.parseInt( groups[i] );
				        demographicGroupLinkDao.add(demographicNo, groupId);
					} catch (Exception e) {
						MiscUtils.getLogger().error("Error parsing demographic group number", e);
					}
				}
			} else {
				MiscUtils.getLogger().warn("Null demographic group number passed in.");
			}
		}
	}

	// for the IBD clinic
	OtherIdManager.saveIdDemographic(demographicNo, "meditech_id", request.getParameter("meditech_id"));
	
     // added check to see if patient has a bc health card and has a version code of 66, in this case you are aloud to have dup hin
     boolean hinDupCheckException = false;
     String hcType = request.getParameter("hc_type");
     String ver  = request.getParameter("ver");
     if (hcType != null && ver != null && hcType.equals("BC") && ver.equals("66")){
        hinDupCheckException = true;
     }

     if(request.getParameter("hin")!=null && request.getParameter("hin").length()>5 && !hinDupCheckException) {
		String paramNameHin = request.getParameter("hin").trim();
		
		boolean outOfDomain = true;
		
		List<Demographic> hinDemoList = demographicDao.searchDemographicByHIN(paramNameHin, 100, 0, loggedInInfo.getLoggedInProviderNo(),outOfDomain);
		for(Demographic hinDemo : hinDemoList) {
        
            if (!(hinDemo.getDemographicNo().toString().equals(request.getParameter("demographic_no")))) {
				if (hinDemo.getVer() != null && !hinDemo.getVer().equals("66")) {

%>
	***<font color='red'><bean:message
		key="demographic.demographicaddarecord.msgDuplicatedHIN"/></font>***
	<br><br><a href=# onClick="history.go(-1);return false;"><b>&lt;-<bean:message
		key="global.btnBack"/></b></a>
	<%
						return;
					}
	        }
	    }
	}
     
    if(demographic.getMyOscarUserName() != null && !demographic.getMyOscarUserName().trim().isEmpty()){ 
     	Demographic myoscarDemographic = demographicDao.getDemographicByMyOscarUserName(demographic.getMyOscarUserName());
     	if (myoscarDemographic != null
				&& !myoscarDemographic.getDemographicNo().equals(demographicNo)) {
%>
			***<font color='red'><bean:message key="demographic.demographicaddarecord.msgDuplicatedPHR" /></font>
			***<br><br><a href=# onClick="history.go(-1);return false;"><b>&lt;-<bean:message key="global.btnBack" /></b></a> 
<% 
			return;
     	}
    }

	List<ChangedField> changedFields = new ArrayList<ChangedField>(ChangedField.getChangedFieldsAndValues(oldDemographic, demographic));
	String keyword = "demographicNo=" + demographicNo;
	if (request.getParameter("keyword") != null) {
		keyword += "\n" + request.getParameter("keyword");
	}
	LogAction.addLog(
			LoggedInInfo.getLoggedInInfoFromSession(request),
			LogConst.UPDATE,
			"demographic",
			keyword,
			Integer.toString(demographicNo),
			changedFields
	);

	if(family!=null && !family.isEmpty()){
	    List<String> members = new ArrayList<String>();
		for (Demographic member : family){
		    member.setAddress(demographic.getAddress());
		    member.setCity(demographic.getCity());
		    member.setProvince(demographic.getProvince());
		    member.setPostal(demographic.getPostal());
		    member.setPhone(demographic.getPhone());
			DemographicManager.saveDemographic(member);
			members.add(member.getFormattedName());
		}
		session.setAttribute("updatedFamily", members);
	}
    
    //multiple site, update site
    if (IsPropertiesOn.isMultisitesEnable()) {
	   DemographicSiteDao demographicSiteDao = SpringUtils.getBean(DemographicSiteDao.class);
	   String[] sites = request.getParameterValues("sites");
	   demographicSiteDao.removeSitesByDemographicId(Integer.valueOf(demoNo));

	   if (sites != null) {
	     for (String siteId : sites) {	       
	       DemographicSite demographicSite = new DemographicSite();
	       demographicSite.setDemographicId(Integer.valueOf(demoNo));
	       demographicSite.setSiteId(Integer.valueOf(siteId));
	       demographicSiteDao.persist(demographicSite);
	     }
	   }
    }
	   
    try{
    	DemographicNameAgeString.resetDemographic(request.getParameter("demographic_no"));
    }catch(Exception nameAgeEx){
    	MiscUtils.getLogger().error("ERROR RESETTING NAME AGE", nameAgeEx);
    }

	String newChartAlertText = request.getParameter("chartAlertText");
	Alert oldChartAlert = alertDao.findLatestEnabledByDemographicNoAndType(demographicNo, AlertType.CHART);
	
	if ((oldChartAlert == null || !newChartAlertText.equals(Encode.forHtmlContent(oldChartAlert.getMessage())))) {
	    if (oldChartAlert != null) {
	        oldChartAlert.setEnabled(false);
	        alertDao.saveEntity(oldChartAlert);
		}
	    
		if (!StringUtils.isBlank(newChartAlertText) && newChartAlertText.length() <= 200) {
			Alert newChartAlert = new Alert(demographicNo, AlertType.CHART, newChartAlertText);
			alertDao.saveEntity(newChartAlert);
		}
	}

    //update admission information
    GenericIntakeEditAction gieat = new GenericIntakeEditAction();
    ProgramManager pm = SpringUtils.getBean(ProgramManager.class);
	AdmissionManager am = SpringUtils.getBean(AdmissionManager.class);
    gieat.setAdmissionManager(am);
    gieat.setProgramManager(pm);
    
	String bedP = request.getParameter("rps");
    if(bedP != null && bedP.length()>0) {
	    try {
	   	 gieat.admitBedCommunityProgram(demographicNo, loggedInProviderNumber, Integer.parseInt(bedP), "", "(Master record change)", new Date());
	    }catch(Exception e) {
	    	
	    }
    }
    
    String[] servP = request.getParameterValues("sp");
    if(servP!=null&&servP.length>0){
    	Set<Integer> s = new HashSet<Integer>();
        for(String _s:servP) s.add(Integer.parseInt(_s));
   		try {
   	   	 gieat.admitServicePrograms(demographicNo, loggedInProviderNumber, s, "(Master record change)", new Date());
   	    }catch(Exception e) {
   	 }
    }
    
    String _pvid = loggedInInfo.getLoggedInProviderNo();
    Set<Program> pset = gieat.getActiveProviderProgramsInFacility(loggedInInfo,_pvid,loggedInInfo.getCurrentFacility().getId());
    List<Program> allServiceProgramsShown = gieat.getServicePrograms(pset,_pvid);
    for(Program p:allServiceProgramsShown) {
    	if(!isFound(servP,p.getId().toString())) {
    		try {
    			am.processDischarge(p.getId(), demographicNo, "(Master record change)", "0");
    		} catch (AdmissionException e) {
				// legacy no-op
			}
    	}
    }
    
try {   
    //add to waiting list if the waiting_list parameter in the property file is set to true
    oscar.oscarWaitingList.WaitingList wL = oscar.oscarWaitingList.WaitingList.getInstance();
    if(wL.getFound()){
 	  WLWaitingListUtil.updateWaitingListRecord(
 	  request.getParameter("list_id"), request.getParameter("waiting_list_note"),
 	  request.getParameter("demographic_no"), request.getParameter("waiting_list_referral_date"));

%>

		<form name="add2WLFrm" action="../oscarWaitingList/Add2WaitingList.jsp">
		<input type="hidden" name="listId" value="<%=request.getParameter("list_id")%>" /> 
		<input type="hidden" name="demographicNo" value="<%=request.getParameter("demographic_no")%>" /> 
		<input type="hidden" name="demographic_no" value="<%=request.getParameter("demographic_no")%>" /> 
		<input type="hidden" name="waitingListNote" value="<%=request.getParameter("waiting_list_note")%>" /> 
		<input type="hidden" name="onListSince" value="<%=request.getParameter("waiting_list_referral_date")%>" /> 
		<input type="hidden" name="displaymode" value="edit" /> 
		<input type="hidden" name="dboperation" value="search_detail" /> 

<%
	if(!request.getParameter("list_id").equalsIgnoreCase("0")){
		String wlDemoId = request.getParameter("demographic_no");
		String wlId = request.getParameter("list_id");
		Integer waitlistId = Integer.parseInt(wlId);
		Integer waitlistDemographicId = Integer.parseInt(wlDemoId);
        List<WaitingList> waitingListList = waitingListDao.findByWaitingListIdAndDemographicId(waitlistId, waitlistDemographicId);

		//check if patient has already added to the waiting list and check if the patient already has an appointment in the future
		if(waitingListList.isEmpty()){
			
			List<Appointment> apptList = appointmentDao.findNonCancelledFutureAppointments(new Integer(wlDemoId));
			if(!apptList.isEmpty()){
%>
			<script language="JavaScript">
				var add2List = confirm("The patient already has an appointment, do you still want to add him/her to the waiting list?");
				if(add2List){
					document.add2WLFrm.action = "../oscarWaitingList/Add2WaitingList.jsp?demographicNo=<%=request.getParameter("demographic_no")%>&listId=<%=request.getParameter("list_id")%>&waitingListNote=<%=request.getParameter("waiting_list_note")==null?"":request.getParameter("waiting_list_note")%>&onListSince=<%=request.getParameter("waiting_list_referral_date")==null?"":request.getParameter("waiting_list_referral_date")%>";
				}
				else{
					document.add2WLFrm.action ="demographiccontrol.jsp?demographic_no=<%=request.getParameter("demographic_no")%>&displaymode=edit&dboperation=search_detail";
				}
				document.add2WLFrm.submit();
		</script> 
<%
			}
			else{
%> 
			<script language="JavaScript">
				document.add2WLFrm.action = "../oscarWaitingList/Add2WaitingList.jsp?demographicNo=<%=request.getParameter("demographic_no")%>&listId=<%=request.getParameter("list_id")%>&waitingListNote=<%=request.getParameter("waiting_list_note")==null?"":request.getParameter("waiting_list_note")%>&onListSince=<%=request.getParameter("waiting_list_referral_date")==null?"":request.getParameter("waiting_list_referral_date")%>";
				document.add2WLFrm.submit();
			</script> 
<%
			}
		}
		else{
			response.sendRedirect("demographiccontrol.jsp?demographic_no=" + request.getParameter("demographic_no") + "&displaymode=edit&dboperation=search_detail");
		}
	}
	else{
		response.sendRedirect("demographiccontrol.jsp?demographic_no=" + request.getParameter("demographic_no") + "&displaymode=edit&dboperation=search_detail");
	}
%>
		</form>
<%
	}
	else{
		response.sendRedirect("demographiccontrol.jsp?demographic_no=" + request.getParameter("demographic_no") + "&displaymode=edit&dboperation=search_detail");
	}
%>

<h2>Update a Provider Record Successfully !
	<p><a href="demographiccontrol.jsp?demographic_no=<%=request.getParameter("demographic_no")%>&displaymode=edit&dboperation=search_detail"><%= request.getParameter("demographic_no") %></a></p>
</h2>

<%
	PreventionManager prevMgr = (PreventionManager) SpringUtils.getBean("preventionMgr");
	prevMgr.removePrevention(request.getParameter("demographic_no"));

}
catch (NumberFormatException nfe) { 
MiscUtils.getLogger().error("Either waitListId or demographicId is not a valid integer for the demographic");
%>
<h1 style="color:red">The waitlist could not be updated while saving the demographic.</h1>
<% } %>

</center>
</body>
</html:html>

<%!
	public boolean isFound(String[] vals, String val) {
		if(vals != null) {
			for(String t:vals) {
				if(t.equals(val)) {
					return true;
				}
			}
		}
		return false;
}
%>
