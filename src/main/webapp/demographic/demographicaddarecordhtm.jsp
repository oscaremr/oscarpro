<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ page import="java.util.*" %>
<%@ page import="oscar.*" %>
<%@ page import="org.oscarehr.common.dao.*" %>
<%@ page import="org.oscarehr.common.model.*" %>
<%@ page import="ca.kai.util.MapUtils" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@ page import="org.oscarehr.common.dao.DemographicDao" %>
<%@ page import="org.oscarehr.common.dao.DemographicGenderDao" %>
<%@ page import="org.oscarehr.common.dao.DemographicGroupDao" %>
<%@ page import="org.oscarehr.common.dao.EFormDao" %>
<%@ page import="org.oscarehr.common.dao.PatientTypeDao" %>
<%@ page import="org.oscarehr.common.dao.ProfessionalSpecialistDao" %>
<%@ page import="org.oscarehr.common.dao.WaitingListNameDao" %>
<%@ page import="org.oscarehr.common.model.DemographicGroup" %>
<%@ page import="org.oscarehr.common.model.Facility" %>
<%@ page import="org.oscarehr.common.model.ProfessionalSpecialist" %>
<%@ page import="org.oscarehr.common.model.Provider" %>
<%@ page import="org.oscarehr.common.model.UserProperty"%>
<%@ page import="org.oscarehr.common.model.WaitingListName" %>
<%@ page import="org.oscarehr.managers.LookupListManager"%>
<%@ page import="org.oscarehr.managers.PatientConsentManager" %>
<%@ page import="org.oscarehr.managers.ProgramManager2" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="org.oscarehr.PMmodule.model.Program" %>
<%@ page import="org.oscarehr.PMmodule.model.ProgramProvider" %>
<%@ page import="org.oscarehr.PMmodule.service.ProgramManager" %>
<%@ page import="org.oscarehr.PMmodule.web.GenericIntakeEditAction" %>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="org.oscarehr.util.SessionConstants"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.oscarDemographic.data.ProvinceNames" %>
<%@ page import="oscar.oscarDemographic.pageUtil.Util" %>
<%@ page import="oscar.oscarWaitingList.WaitingList"%>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/caisi-tag.tld" prefix="caisi" %>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/wellAiVoice.tld" prefix="well-ai-voice"%>

<jsp:useBean id="apptMainBean" class="oscar.AppointmentMainBean" scope="session" />
<jsp:useBean id="providerBean" class="java.util.Properties" scope="session" />
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_demographic" rights="w" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect(request.getContextPath() + "/securityError.jsp?type=_demographic");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
	OscarProperties oscarProperties = OscarProperties.getInstance();
	java.util.Locale vLocale =
			(java.util.Locale) session.getAttribute(org.apache.struts.Globals.LOCALE_KEY);
	CountryCodeDao ccDAO = SpringUtils.getBean(CountryCodeDao.class);
	DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
	DemographicGenderDao demographicGenderDao = (DemographicGenderDao) SpringUtils.getBean("demographicGenderDao");
	DemographicPronounDao demographicPronounDao = (DemographicPronounDao) SpringUtils.getBean("demographicPronounDao");
	List<DemographicGender> demographicGender = demographicGenderDao.findAllDemographicGender();
	List<DemographicPronoun> demographicPronoun = demographicPronounDao.findAllDemographicPronoun();
	DemographicGroupDao demographicGroupDao = SpringUtils.getBean(DemographicGroupDao.class);
	EFormDao eformDao = SpringUtils.getBean(EFormDao.class);
	ProfessionalSpecialistDao professionalSpecialistDao
			= SpringUtils.getBean(ProfessionalSpecialistDao.class);
	ProgramManager pm = SpringUtils.getBean(ProgramManager.class);
	ProgramManager2 programManager2 = SpringUtils.getBean(ProgramManager2.class);
	PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
	ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
	UserPropertyDAO userPropertyDAO = SpringUtils.getBean(UserPropertyDAO.class);
	WaitingListNameDao waitingListNameDao = SpringUtils.getBean(WaitingListNameDao.class);

	boolean enableExternalNameOnDemographic = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("enable_external_name_on_demographic", false);
	boolean requireCpsidAndDoctorForMrp = SystemPreferencesUtils.isReadBooleanPreferenceWithDefault("require_cpsid_and_doctor_for_mrp", false);
	boolean privateConsentEnabled = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("private_informed_consent_enabled", false);
	boolean enableMeditechId = SystemPreferencesUtils.isReadBooleanPreferenceWithDefault("enable_meditech_id", false);
	LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
	String curUser_no = (String) session.getAttribute("user");
	GregorianCalendar now = new GregorianCalendar();
	SimpleDateFormat standardDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	String curMonth = Integer.toString(now.get(Calendar.MONTH) + 1);
	if (curMonth.length() < 2) {
		curMonth = "0" + curMonth;
	}
	String curDay = Integer.toString(now.get(Calendar.DAY_OF_MONTH));
	if (curDay.length() < 2) {
		curDay = "0" + curDay;
	}
	int nStrShowLen = 20;
	String address = session.getAttribute("address") != null
			? (String) session.getAttribute("address")
			: "";
	ProvinceNames pNames = ProvinceNames.getInstance();
	String prov = (oscarProperties.getProperty("billregion", "")).trim().toUpperCase();
	String billingCentre = (oscarProperties.getProperty("billcenter", "")).trim().toUpperCase();
	String defaultCity = session.getAttribute("city") != null
			? (String) session.getAttribute("city")
			: oscarProperties.getProperty("default_city") != null
					? oscarProperties.getProperty("default_city")
					: prov.equals("ON")
							? (billingCentre.equals("N") ? "Toronto" : "")
							: "";
	String postal = session.getAttribute("postal") != null
			? (String) session.getAttribute("postal")
			: "";
	String phone = session.getAttribute("phone") != null
			? (String) session.getAttribute("phone")
			: oscarProperties.getProperty("phoneprefix", "905-");
	String phonePreference = session.getAttribute("phone_preference") != null
			? (String) session.getAttribute("phone_preference")
			: "";

  List<CountryCode> countryList = ccDAO.getAllCountryCodes();
  List<DemographicGroup> demographicGroups = demographicGroupDao.getAll();
  String HCType = "";
  // Determine if curUser has selected a default HC Type
  UserProperty HCTypeProp = userPropertyDAO.getProp(curUser_no,  UserProperty.HC_TYPE);
  if (HCTypeProp != null) {
     HCType = HCTypeProp.getValue();
  } else {
     // If there is no user defined property, then determine if the hctype system property is activated
     HCType = oscarProperties.getProperty("hctype", "");
     if (HCType == null || HCType.equals("")) {
           // The system property is not activated, so use the billregion
		 HCType = oscarProperties.getProperty("billregion", "");
     }
  }
  // Use this value as the default value for province, as well
  String defaultProvince = session.getAttribute("province")!=null?(String)session.getAttribute("province"):HCType;
	session.removeAttribute("address");
	session.removeAttribute("city");
	session.removeAttribute("province");
	session.removeAttribute("postal");
	session.removeAttribute("phone");
	//get a list of programs the patient has consented to.
	if (oscarProperties.getBooleanProperty("USE_NEW_PATIENT_CONSENT_MODULE", "true") ) {
	    PatientConsentManager patientConsentManager = SpringUtils.getBean( PatientConsentManager.class );
		pageContext.setAttribute( "consentTypes", patientConsentManager.getConsentTypes() );
	}
	boolean showSin = !"hidden".equalsIgnoreCase(propertyDao.getValueByNameAndDefault("demographic.field.sin", ""));
	Map<String, Boolean> masterFilePreferences = SystemPreferencesUtils.findByKeysAsMap(SystemPreferences.MASTER_FILE_PREFERENCE_KEYS);
%>
<html:html locale="true">
<head>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.js"></script> 
   <script>
     jQuery.noConflict();     
   </script>	
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-3.1.0.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.maskedinput.js"></script>
	<script type="application/javascript">
		var jQuery_3_1_0 = jQuery.noConflict(true);
	</script>
   <script type="text/javascript">
        function aSubmit(){
            if(document.getElementById("eform_iframe")!=null)document.getElementById("eform_iframe").contentWindow.document.forms[0].submit();
            if(!checkFormTypeIn()) return false;
            if( !ignoreDuplicates() ) return false;
            return true;
        }
   </script>
<oscar:customInterface section="masterCreate"/>
<title><bean:message key="demographic.demographicaddrecordhtm.title" /></title>
<% if (oscarProperties.getBooleanProperty("indivica_hc_read_enabled", "true")) { %>
	<script language="javascript" src="<%=request.getContextPath() %>/hcHandler/hcHandler.js"></script>
	<script language="javascript" src="<%=request.getContextPath() %>/hcHandler/hcHandlerNewDemographic.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/hcHandler/hcHandler.css" type="text/css" />
<% } %>
	
<!-- calendar stylesheet -->
<link rel="stylesheet" type="text/css" media="all"
	href="../share/calendar/calendar.css" title="win2k-cold-1" />

<!-- main calendar program -->
<script type="text/javascript" src="../share/calendar/calendar.js"></script>

<!-- language for the calendar -->
<script type="text/javascript"
	src="../share/calendar/lang/<bean:message key="global.javascript.calendar"/>"></script>

<!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
<script type="text/javascript" src="../share/calendar/calendar-setup.js"></script>

<script type="text/javascript" src="<%=request.getContextPath() %>/js/check_hin.js"></script>

<!-- Stylesheet for zdemographicfulltitlesearch.jsp -->
<link rel="stylesheet" type="text/css" href="../share/css/searchBox.css" />
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/css/Demographic.css" />

<!--link rel="stylesheet" type="text/css" media="all" href="../share/css/extractedFromPages.css"  /-->
<script language="JavaScript">
function upCaseCtrl(ctrl) {
	ctrl.value = ctrl.value.toUpperCase();
}

function checkTypeIn() {
  var dob = document.titlesearch.keyword; typeInOK = false;

  if (dob.value.indexOf('%b610054') == 0 && dob.value.length > 18){
     document.titlesearch.keyword.value = dob.value.substring(8,18);
     document.titlesearch.search_mode[4].checked = true;
  }

  if(document.titlesearch.search_mode[2].checked) {
    if(dob.value.length==8) {
      dob.value = dob.value.substring(0, 4)+"-"+dob.value.substring(4, 6)+"-"+dob.value.substring(6, 8);
      typeInOK = true;
    }
    if(dob.value.length != 10) {
      alert("<bean:message key="demographic.search.msgWrongDOB"/>");
      typeInOK = false;
    }

    return typeInOK ;
  } else {
    return true;
  }
}
function checkGender() {
    let btnValue;
    const selectGender = document.getElementById("gender");
    const deleteGenBtn = document.getElementById("deleteGenBtn");
    if (selectGender[selectGender.selectedIndex].dataset["isEditable"] == "true") {
        btnValue = "<bean:message key="demographic.demographiceditdemographic.btnEdit"/>";
        deleteGenBtn.style.display = "inline";
    } else {
        btnValue = "<bean:message key="demographic.demographiceditdemographic.btnAddNew"/>";
        deleteGenBtn.style.display = "none";
    }
    const genderBtn = document.getElementById("genderBtn");
    genderBtn.value = btnValue;
}

async function saveGender(newOpt, currentId) {
    let id = '0';
    if (typeof currentId == 'undefined') {
        let response = await fetch("<%= request.getContextPath() %>/saveGender.do?method=addGender&gender=" + newOpt, {
            method: 'GET',
        });
        id = await response.text();
    } else {
        fetch("<%= request.getContextPath() %>/saveGender.do?method=editGender&gender=" + newOpt + "&id=" + currentId, {
            method: 'GET'
        });
    }
    return id.trim();
}

async function newGender() {
    const genderBtn = document.getElementById("genderBtn");
    let newOpt = "";
    const addNew = genderBtn.value == "<bean:message key="demographic.demographiceditdemographic.btnAddNew"/>"; 
    if (addNew) {
        newOpt = prompt("<bean:message key="demographic.demographiceditdemographic.msgPromptGender"/> : ", "");
    } else {
        const optionValue = document.adddemographic.gender.options[document.adddemographic.gender.selectedIndex].innerHTML.trim();
        newOpt = prompt("<bean:message key="demographic.demographiceditdemographic.msgPromptGender"/> : ",
                optionValue);
    }
    if (newOpt == null) {
        return;
    } else if (newOpt != "") {
        if (addNew) {
            const id = await saveGender(newOpt);
            const newOption = new Option(newOpt, id);
            newOption.selected = true;
            newOption.dataset["isEditable"] = true;
            document.adddemographic.gender.options[document.adddemographic.gender.length] = newOption;
            genderBtn.value = "<bean:message key="demographic.demographiceditdemographic.btnEdit"/>";
            deleteGenBtn.style.display = "inline";
        } else {
            const currentId = document.adddemographic.gender.value;
            const id = saveGender(newOpt, currentId);
            const newOption = new Option(newOpt, currentId);
            newOption.dataset["isEditable"] = true;
            newOption.selected = true;
            document.adddemographic.gender.options[document.adddemographic.gender.selectedIndex] = newOption;
        }
    } else {
        alert("<bean:message key="demographic.demographiceditdemographic.msgInvalidEntry"/>");
    }
}

function checkPronoun() {
    let btnValue;
    const selectPronoun = document.getElementById("pronoun");
    const deleteProBtn = document.getElementById("deleteProBtn");
    if (selectPronoun[selectPronoun.selectedIndex].dataset["isEditable"] == "true") {
        btnValue = "<bean:message key="demographic.demographiceditdemographic.btnEdit"/>";
        deleteProBtn.style.display = "inline";
    } else {
        btnValue = "<bean:message key="demographic.demographiceditdemographic.btnAddNew"/>";
        deleteProBtn.style.display = "none";
    }
    const pronounBtn = document.getElementById("pronounBtn");
    pronounBtn.value = btnValue;
}
async function savePronoun(newOpt, currentId) {
    let id = '0';
    if (typeof currentId == 'undefined') {
        let response = await fetch("<%= request.getContextPath() %>/savePronoun.do?method=addPronoun&pronoun=" + newOpt, {
            method: 'GET',
        });
        id = await response.text();
    } else {
        fetch("<%= request.getContextPath() %>/savePronoun.do?method=editPronoun&pronoun=" + newOpt + "&id=" + currentId, {
                method: 'GET'
            }
        );
    }
    return id.trim();
}

async function newPronoun() {
    let newOpt = "";
    const deleteProBtn = document.getElementById("deleteProBtn");
    const pronounBtn = document.getElementById("pronounBtn");
    const addNew = pronounBtn.value == "<bean:message key="demographic.demographiceditdemographic.btnAddNew"/>";
    if (addNew) {
        newOpt = prompt("<bean:message key="demographic.demographiceditdemographic.msgPromptPronoun"/> : ", "");
    } else {
        const optionValue = document.adddemographic.pronoun.options[document.adddemographic.pronoun.selectedIndex].innerHTML.trim();
        newOpt = prompt("<bean:message key="demographic.demographiceditdemographic.msgPromptPronoun"/>:",
                optionValue);
    }
    if (newOpt == null) {
        return;
    } else if (newOpt != "") {
        if (addNew) {
            const id = await savePronoun(newOpt);
            const newOption = new Option(newOpt, id);
            newOption.selected = true;
            newOption.dataset["isEditable"] = true;
            document.adddemographic.pronoun.options[document.adddemographic.pronoun.length] = newOption;
            pronounBtn.value = ("<bean:message key="demographic.demographiceditdemographic.btnEdit"/>");
            deleteProBtn.style.display = "inline";
    	} else {
            const currentId = document.adddemographic.pronoun.value;
            const id = savePronoun(newOpt, currentId);
            const newOption = new Option(newOpt, currentId);
            newOption.dataset["isEditable"] = true;
            newOption.selected = true;
            document.adddemographic.pronoun.options[document.adddemographic.pronoun.selectedIndex] = newOption;
        }
    } else {
        alert("<bean:message key="demographic.demographiceditdemographic.msgInvalidEntry"/>");
    }
}
async function deleteGender(currentId) {
	fetch("<%= request.getContextPath() %>/saveGender.do?method=deleteGenderById&id=" + currentId, {
        method: 'GET'
    });
}
async function delGender() {
	const deleteGenBtn = document.getElementById("deleteGenBtn");
	const genderBtn = document.getElementById("genderBtn");
	const gender = document.getElementById("gender");
	const currentId = document.adddemographic.gender.value;
	const optionValue = document.adddemographic.gender.options[document.adddemographic.gender.selectedIndex].innerHTML.trim();
	const delOpt = confirm("Are you sure want to Delete " + optionValue + "? This will remove this option from all demographics");
	if (delOpt == false) {
		return;
	} else {
	    deleteGender(currentId);
  	    gender.remove(gender.selectedIndex);
	    genderBtn.value = "<bean:message key="demographic.demographiceditdemographic.btnAddNew"/>";
	    deleteGenBtn.style.display = "none";
	}
}

async function deletePronoun(currentId) {
    fetch("<%= request.getContextPath() %>/savePronoun.do?method=deletePronounById&id=" + currentId, {
        method: 'GET'
    });
}

async function delPronoun() {
	const deleteProBtn = document.getElementById("deleteProBtn");
	const pronounBtn = document.getElementById("pronounBtn");
	const pronoun = document.getElementById("pronoun");
	const currentId = document.adddemographic.pronoun.value;
	const optionValue = document.adddemographic.pronoun.options[document.adddemographic.pronoun.selectedIndex].innerHTML.trim();
	const delOpt = confirm("Are you sure want to Delete " + optionValue + "? This will remove this option from all demographics");
	if (delOpt == false) {
		return;
	} else {
	    deletePronoun(currentId);
	    pronoun.remove(pronoun.selectedIndex);
	    pronounBtn.value = "<bean:message key="demographic.demographiceditdemographic.btnAddNew"/>";
	    deleteProBtn.style.display = "none";
	}
}


function checkTypeInAdd() {
	var typeInOK = false;
	if(document.adddemographic.last_name.value!="" && document.adddemographic.first_name.value!="" && document.adddemographic.sex.value!="") {
      if(checkTypeNum(document.adddemographic.year_of_birth.value) && checkTypeNum(document.adddemographic.month_of_birth.value) && checkTypeNum(document.adddemographic.date_of_birth.value) ){
	    typeInOK = true;
	  }
	}
	if(!typeInOK) alert ("<bean:message key="demographic.demographicaddrecordhtm.msgMissingFields"/>");
	return typeInOK;
}

function toggleMailing () {
	var checkBox = document.getElementById ("addMailing");
	var enableMailing = document.getElementById ("enableMailing");
	var mailingSection = document.getElementById("mailingBody");

	if (checkBox.checked) {
		mailingSection.style.visibility = "visible";
		enableMailing.value = "true";
	} else {
		mailingSection.style.visibility = "hidden";
		enableMailing.value = "false";
	}
}

function newStatus() {
    newOpt = prompt("Please enter the new status:", "");
    if (newOpt != "") {
        document.adddemographic.patient_status.options[document.adddemographic.patient_status.length] = new Option(newOpt, newOpt);
        document.adddemographic.patient_status.options[document.adddemographic.patient_status.length-1].selected = true;
    } else {
        alert("Invalid entry");
    }
}
function newStatus1() {
    newOpt = prompt("Please enter the new status:", "");
    if (newOpt != "") {
        document.adddemographic.roster_status.options[document.adddemographic.roster_status.length] = new Option(newOpt, newOpt);
        document.adddemographic.roster_status.options[document.adddemographic.roster_status.length-1].selected = true;
    } else {
        alert("Invalid entry");
    }
}

function formatPhoneNum() {
    if (document.adddemographic.phone.value.length == 10) {
        document.adddemographic.phone.value = document.adddemographic.phone.value.substring(0,3) + "-" + document.adddemographic.phone.value.substring(3,6) + "-" + document.adddemographic.phone.value.substring(6);
        }
    if (document.adddemographic.phone.value.length == 11 && document.adddemographic.phone.value.charAt(3) == '-') {
        document.adddemographic.phone.value = document.adddemographic.phone.value.substring(0,3) + "-" + document.adddemographic.phone.value.substring(4,7) + "-" + document.adddemographic.phone.value.substring(7);
    }

    if (document.adddemographic.phone2.value.length == 10) {
        document.adddemographic.phone2.value = document.adddemographic.phone2.value.substring(0,3) + "-" + document.adddemographic.phone2.value.substring(3,6) + "-" + document.adddemographic.phone2.value.substring(6);
        }
    if (document.adddemographic.phone2.value.length == 11 && document.adddemographic.phone2.value.charAt(3) == '-') {
        document.adddemographic.phone2.value = document.adddemographic.phone2.value.substring(0,3) + "-" + document.adddemographic.phone2.value.substring(4,7) + "-" + document.adddemographic.phone2.value.substring(7);
    }

    if (document.adddemographic.demo_cell.value.length == 10) {
        document.adddemographic.demo_cell.value = document.adddemographic.demo_cell.value.substring(0,3) + "-" + document.adddemographic.demo_cell.value.substring(3,6) + "-" + document.adddemographic.demo_cell.value.substring(6);
    }
    if (document.adddemographic.demo_cell.value.length == 11 && document.adddemographic.demo_cell.value.charAt(3) == '-') {
        document.adddemographic.demo_cell.value = document.adddemographic.demo_cell.value.substring(0,3) + "-" + document.adddemographic.demo_cell.value.substring(4,7) + "-" + document.adddemographic.demo_cell.value.substring(7);
    }
}
function rs(n,u,w,h,x) {
  args="width="+w+",height="+h+",resizable=yes,scrollbars=yes,status=0,top=60,left=30";
  remote=window.open(u,n,args);
}

function referralScriptAttach2(refDoctorNoElement, refDoctorNameElement, refDoctorIdElement, searchType) {
    refDoctorNo = escape(document.forms[1].elements[refDoctorNoElement].value);
    refDoctorName = escape(document.forms[1].elements[refDoctorNameElement].value);
    t0 = escape("document.forms[1].elements[\'"+refDoctorNoElement+"\'].value");
    t1 = escape("document.forms[1].elements[\'"+refDoctorNameElement+"\'].value");
    t2 = escape("document.forms[1].elements[\'"+refDoctorIdElement+"\'].value");
    var referralDateParam = escape("document.forms[1].elements['referral-date'].value");
    
    rs('att',('../billing/CA/ON/searchRefDoc.jsp?refDoctorNo='+refDoctorNo+'&refDoctorName='+refDoctorName + '&param=' + t0 + '&param2=' + t1 + '&paramId=' + t2 + '&searchType=' + searchType + '&referralDateParam=' + referralDateParam),600,600,1);
	 }
    <%
    SystemPreferences systemPreferences =
        SystemPreferencesUtils.findPreferenceByName("referring_physician_mandatory");
    boolean refPhysicianMandatory =
        systemPreferences != null && Boolean.parseBoolean(systemPreferences.getValue());
    systemPreferences = SystemPreferencesUtils.findPreferenceByName("email_mandatory");
    boolean emailMandatory =
        systemPreferences != null && Boolean.parseBoolean(systemPreferences.getValue());
    %>

function checkRefPhysician() {
    var valid = false;
    if (document.adddemographic.r_doctor.value.trim() !== "") {
        valid = true;
	} else {
        alert ("You must type in the following fields: Referral Doctor")
	}
	return valid;
}

function checkEmail() {	
    let emailRegex = /^[^\s]+@[^\s@]+\.[^\s@]+$/; // simple email regex checking for a string with at least one '@' character followed by *domain*.*extension*.
	if(document.adddemographic.email.value.trim().length == 0){		
		alert("Email is mandatory.");
		document.adddemographic.email.focus();
		return false;
	} else {		
		if(!emailRegex.test(document.adddemographic.email.value.trim()) ){
			alert("Invalid email.");
			document.adddemographic.email.focus();
			return false;
		}
		return true;	
	}	
}

function checkName() {
	var typeInOK = false;
	if(document.adddemographic.last_name.value.trim() !== "" && document.adddemographic.last_name.value.trim() !== "") {
	    typeInOK = true;
	} else {
		alert ("You must type in the following fields: Last Name, First Name.");
    }
	return typeInOK;
}

function checkDob() {
	var typeInOK = false;
	var yyyy = document.adddemographic.year_of_birth.value;
	var mm = document.adddemographic.month_of_birth.value;
	var dd = document.adddemographic.date_of_birth.value

	if(checkTypeNum(yyyy) && checkTypeNum(mm) && checkTypeNum(dd) ){
        var check_date = new Date(yyyy,(mm-1),dd);
		var now = new Date();
		var year=now.getFullYear();
		var month=now.getMonth()+1;
		var date=now.getDate();
		var young = new Date(year,month,date);
		var old = new Date(1800,01,01);
		if (check_date.getTime() <= young.getTime() && check_date.getTime() >= old.getTime() && yyyy.length==4) {
		    typeInOK = true;
		}
		if ( yyyy == "0000"){
        typeInOK = false;
      }
	}
	if (!typeInOK){
      alert ("You must type in the right DOB.");
   } else if (!isValidDate(dd,mm,yyyy)){
      alert ("DOB Date is an incorrect date");
      typeInOK = false;
   }
	return typeInOK;
}

function isValidDate(day,month,year){
   month = ( month - 1 );
   dteDate=new Date(year,month,day);
   return ((day==dteDate.getDate()) && (month==dteDate.getMonth()) && (year==dteDate.getFullYear()));
}

function checkOOPHin() {
	var hin = document.adddemographic.hin.value;
	var province = document.adddemographic.hc_type.value;
	if (!isValidOOPHinLength(hin, province)) {
		alert("Please enter a valid HIN for HC Type: " + province);
		return(false);
	}
	return(true);
}

function checkHin() {
	var hin = document.adddemographic.hin.value;
	var province = document.adddemographic.hc_type.value;
	if (!isValidHin(hin, province))
	{
		alert ("You must type in the right HIN.");
		return(false);
	}
	return(true);
}

function checkSex() {
	var sex = document.adddemographic.sex.value;
	if (sex.length == 0)
	{
		alert ("You must select a Sex.");
		return false;
	}
	return true;
}

function checkEnrollmentFields() {
    var provider = jQuery_3_1_0('#enrollmentProvider')[0].value;
    var status = jQuery_3_1_0('#roster_status')[0].value;
    var date = jQuery_3_1_0('#roster_date')[0].value;
    if (provider === "" && status !== "EN" && date === "") {
        return true;
    }
    if (date !== "" && status !== "EN") {
        return true;
	}
    if ((provider !== "" && status === "") || (status !== "EN" && provider === "") || (provider === "" && status === "")) {
        var message = "You must enter both an enrollment provider and an enrollment status";
        if (date !== "") {
            message += " when entering an enrollment date";
        }
        alert (message);
        return false;
    }
    if (provider !== "" && status !== "") {
        if (date !== "") {
            return validEnrollmentDateCheck(date);
        }
        return true;
    }
    return false;
}

// From checkDate.js - Line 421
function validEnrollmentDateCheck(dateStr)
{
    //default is yyyy-mm-dd
    var datePat = /^(\d{4})(\/|-)(\d{1,2})(\/|-)(\d{1,2})$/;
    var matchArray = dateStr.match(datePat); // is the format ok?
    if (matchArray == null) {
        alert("Please enter the date as yyyy-mm-dd. Your current selection reads: " + datePat);
        return false;
    }
    var dateArr = dateStr.split('-');
    var year = dateArr[0];
    var month = dateArr[1]; // parse date into variables
    var day = dateArr[2];
    if (month < 1 || month > 12) { // check month range
        alert("Enrollment Month must be between 1 and 12.");
        return false;
    }
    if (day < 1 || day > 31) {
        alert("Enrollment Day must be between 1 and 31.");
        return false;
    }
    if ((month==4 || month==6 || month==9 || month==11) && day==31) {
        alert("Enrollment Month "+month+" doesn't have 31 days!")
        return false
    }
    if (month == 2) { // check for february 29th
        var isleap = (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
        if (day>29 || (day==29 && !isleap)) {
            alert("February " + year + " doesn't have " + day + " days!");
            return false;
        }
    }
    return true;  // date is valid
}

function checkResidentStatus(){
    var rs = document.adddemographic.rsid.value;
    if(rs!="")return true;
    else{
        alert("you must choose a Residential Status");
     return false;}
}

function referralSourceIsOther (value) {
	var element = document.getElementById ("referralSourceCust");
	if (value=="Other") {
		element.style.visibility='visible';
	} else {
		element.style.visibility='hidden';
	}
}

function checkAllDate() {
	var typeInOK = false;
	typeInOK = checkDateYMD( document.adddemographic.date_joined.value, "Date Joined" );
	if (!typeInOK) { return false; }

	return typeInOK;
}
	function checkDateYMD(yy, mm, dd, fieldName) {
		var typeInOK = false;
		if((yy.length==0) && (mm.length==0) && (dd.length==0) ){
			typeInOK = true;
		} else if(checkTypeNum(yy) && checkTypeNum(mm) && checkTypeNum(dd) ){
			if (checkDateYear(yy) && checkDateMonth(mm) && checkDateDate(dd)) {
				typeInOK = true;
			}
		}
		if (!typeInOK) { alert ("You must type in the right '" + fieldName + "'."); return false; }
		return typeInOK;
	}

	function checkDateYMD(dateString, fieldName) {
		var date = new Date(dateString);
		if (isNaN(date)) {
			alert ("You must type in the right '" + fieldName + "'.");
			return false;
		} else {
			return true;
		}
	}

	function checkDateYear(y) {
		if (y>1900 && y<2045) return true;
		return false;
	}
	function checkDateMonth(y) {
		if (y>=1 && y<=12) return true;
		return false;
	}
	function checkDateDate(y) {
		if (y>=1 && y<=31) return true;
		return false;
	}

function checkFormTypeIn() {
	if(document.getElementById("eform_iframe")!=null)document.getElementById("eform_iframe").contentWindow.document.forms[0].submit();
	<% if (refPhysicianMandatory) { %>
		if ( !checkRefPhysician() ) return false;
	<% } %>
	<% if (emailMandatory) { %>
	if ( !checkEmail() ) return false;
	<% } %>
	if ( !checkName() ) return false;
	if ( !checkDob() ) return false;
	if ( !checkHin() ) return false;
	if ( document.adddemographic.hc_type.value !== "ON" ) {
		if ( !checkOOPHin() ) return false;
	}
	if ( !checkEnrollmentFields() ) return false;
	if ( !checkSex() ) return false;
	if ( !checkResidentStatus() ) return false;
	if ( !checkAllDate() ) return false;
	return true;
}

function checkTitleSex(ttl) {
   if (ttl=="MS" || ttl=="MISS" || ttl=="MRS" || ttl=="SR") document.adddemographic.sex.selectedIndex=2;
	else if (ttl=="MR" || ttl=="MSSR") document.adddemographic.sex.selectedIndex=1;
}

function removeAccents(s){
        var r=s.toLowerCase();
        r = r.replace(new RegExp("\\s", 'g'),"");
        r = r.replace(new RegExp("[������]", 'g'),"a");
        r = r.replace(new RegExp("�", 'g'),"c");
        r = r.replace(new RegExp("[����]", 'g'),"e");
        r = r.replace(new RegExp("[����]", 'g'),"i");
        r = r.replace(new RegExp("�", 'g'),"n");
        r = r.replace(new RegExp("[�����]", 'g'),"o");
        r = r.replace(new RegExp("[����]", 'g'),"u");
        r = r.replace(new RegExp("[��]", 'g'),"y");
        r = r.replace(new RegExp("\\W", 'g'),"");
        return r;
}

function autoFillHin(){
   var hcType = document.getElementById('hc_type').value;
   var hin = document.getElementById('hin').value;
   if(	hcType == 'QC' && hin == ''){
   	  var last = document.getElementById('last_name').value;
   	  var first = document.getElementById('first_name').value;
      var yob = document.getElementById('year_of_birth').value;
      var mob = document.getElementById('month_of_birth').value;
      var dob = document.getElementById('date_of_birth').value;
   	  last = removeAccents(last.substring(0,3)).toUpperCase();
   	  first = removeAccents(first.substring(0,1)).toUpperCase();
   	  yob = yob.substring(2,4);
   	  var sex = document.getElementById('sex').value;
   	  if(sex == 'F'){
   		  mob = parseInt(mob) + 50; 
   	  }
      document.getElementById('hin').value = last + first + yob + mob + dob;
      hin.focus();
      hin.value = hin.value;
   }
}

function ignoreDuplicates() {
		//do the check
		var lastName = jQuery("#last_name").val();
		var firstName = jQuery("#first_name").val();
		var yearOfBirth = jQuery("#year_of_birth").val();
		var monthOfBirth = jQuery("#month_of_birth").val();
		var dayOfBirth = jQuery("#date_of_birth").val();
		var ret = false;
	jQuery.ajax({
			url:"../demographicSupport.do?method=checkForDuplicates&lastName="+lastName+"&firstName="+firstName+"&yearOfBirth="+yearOfBirth+"&monthOfBirth="+monthOfBirth+"&dayOfBirth="+dayOfBirth,
			success:function(data){
				if(data.hasDuplicates != null) {
					if(data.hasDuplicates) {
						if(confirm('There are other patients in this system with the same name and date of birth. Are you sure you want to create this new patient record?')) {
							//submit the form
							ret = true;
						}
					} else {
						//submit the form
						ret = true;
					}
				} else {
					//submit the form
					ret = true;
				}
			},
			dataType:'json',
			async: false
	});
	return ret;
}

function consentClearBtn(radioBtnName)
{
	if( confirm("Proceed to clear all record of this consent?") )
	{
	    //clear out opt-in/opt-out radio buttons
	    var ele = document.getElementsByName(radioBtnName);
	    for(var i=0;i<ele.length;i++)
	    {
	    	ele[i].checked = false;
	    }
	    //hide consent date field from displaying
	    var consentDate = document.getElementById("consentDate_" + radioBtnName);
	    if (consentDate)
	    {
	        consentDate.style.display = "none";
	    }
	}
}
</script>
</head>
	<%!
			public String getDisabled(String fieldName) {
				String val = OscarProperties.getInstance().getProperty("demographic.edit." + fieldName, "");
				if (val != null && val.equals("disabled")) {
					return " disabled=\"disabled\" ";
				}
				return "";
			}
	%>
<body bgproperties="fixed" topmargin="0"
	leftmargin="0" rightmargin="0">
	<well-ai-voice:script/>
<table border="0" cellspacing="0" cellpadding="0" width="100%">
	<tr bgcolor="#CCCCFF">
		<th class="subject"><bean:message
			key="demographic.demographicaddrecordhtm.msgMainLabel" /></th>
	</tr>
</table>

<%@ include file="zdemographicfulltitlesearch.jsp"%>
<table width="100%" bgcolor="#CCCCFF">
<tr><td class="RowTop">
    <b><bean:message key="demographic.record"/></b>
    <% if (oscarProperties.getBooleanProperty("indivica_hc_read_enabled", "true")) { %>
		<span style="position: relative; float: right; font-style: italic; background: black; color: white; padding: 4px; font-size: 12px; border-radius: 3px;">
			<span class="_hc_status_icon _hc_status_success"></span>Ready for Card Swipe
		</span>
	<% } %>
</td></tr>
<tr><td>
<form method="post" id="adddemographic" name="adddemographic" action="demographicaddarecord.jsp" onsubmit="return aSubmit()">
<input type="hidden" name="fromAppt" value="<%=Encode.forHtmlAttribute(request.getParameter("fromAppt"))%>">
<input type="hidden" name="originalPage" value="<%=Encode.forHtmlAttribute(request.getParameter("originalPage"))%>">
<input type="hidden" name="bFirstDisp" value="<%=Encode.forHtmlAttribute(request.getParameter("bFirstDisp"))%>">
<input type="hidden" name="provider_no" value="<%=Encode.forHtmlAttribute(request.getParameter("provider_no"))%>">
<input type="hidden" name="start_time" value="<%=Encode.forHtmlAttribute(request.getParameter("start_time"))%>">
<input type="hidden" name="end_time" value="<%=Encode.forHtmlAttribute(request.getParameter("end_time"))%>">
<input type="hidden" name="duration" value="<%=Encode.forHtmlAttribute(request.getParameter("duration"))%>">
<input type="hidden" name="year" value="<%=Encode.forHtmlAttribute(request.getParameter("year"))%>">
<input type="hidden" name="month" value="<%=Encode.forHtmlAttribute(request.getParameter("month"))%>">
<input type="hidden" name="day" value="<%=Encode.forHtmlAttribute(request.getParameter("day"))%>">
<input type="hidden" name="appointment_date" value="<%=Encode.forHtmlAttribute(request.getParameter("appointment_date"))%>">
<input type="hidden" name="appointment-notes" value="<%=Encode.forHtmlAttribute(request.getParameter("notes"))%>">
<input type="hidden" name="reason" value="<%=Encode.forHtmlAttribute(request.getParameter("reason"))%>">
<input type="hidden" name="location" value="<%=Encode.forHtmlAttribute(request.getParameter("location"))%>">
<input type="hidden" name="resources" value="<%=Encode.forHtmlAttribute(request.getParameter("resources"))%>">
<input type="hidden" name="type" value="<%=Encode.forHtmlAttribute(request.getParameter("type"))%>">
<input type="hidden" name="style" value="<%=Encode.forHtmlAttribute(request.getParameter("style"))%>">
<input type="hidden" name="billing" value="<%=Encode.forHtmlAttribute(request.getParameter("billing"))%>">
<input type="hidden" name="status" value="<%=Encode.forHtmlAttribute(request.getParameter("status"))%>">
<input type="hidden" name="createdatetime" value="<%=Encode.forHtmlAttribute(request.getParameter("createdatetime"))%>">
<input type="hidden" name="creator" value="<%=Encode.forHtmlAttribute(request.getParameter("creator"))%>">
<input type="hidden" name="remarks" value="<%=Encode.forHtmlAttribute(request.getParameter("remarks"))%>">
<table id="addDemographicTbl" border="0" cellpadding="1" cellspacing="0" width="100%" bgcolor="#EEEEFF">
    <% if (oscarProperties.getProperty("workflow_enhance") != null
			&& oscarProperties.getProperty("workflow_enhance").equals("true")) { %>
   		 <tr bgcolor="#CCCCFF">
				<td colspan="4">
				<div align="center"><input type="hidden" name="dboperation"
					value="add_record">
          <input type="hidden" name="displaymode" value="Add Record">
				<input type="submit" name="submit"
					value="<bean:message key="demographic.demographicaddrecordhtm.btnAddRecord"/>">
				<input type="submit" name="submit" value="Save & Add Family Member">
				<input type="button" name="Button"
					value="<bean:message key="demographic.demographicaddrecordhtm.btnSwipeCard"/>"
					onclick="window.open('zadddemographicswipe.htm','', 'scrollbars=yes,resizable=yes,width=600,height=300')";>
				&nbsp; <input type="button" name="Button"
					value="<bean:message key="demographic.demographicaddrecordhtm.btnCancel"/>"
					onclick=self.close();></div>
				</td>
			</tr>
    <% }
   String lastNameVal = "";
   String firstNameVal = "";
   String chartNoVal = "";
   if (searchMode != null) {
      if (searchMode.equals("search_name")) {
        int commaIdx = keyWord.indexOf(",");
        if (commaIdx == -1) 
	   lastNameVal = keyWord.trim();
        else if (commaIdx == (keyWord.length()-1))
           lastNameVal = keyWord.substring(0,keyWord.length()-1).trim();
        else {
           lastNameVal = keyWord.substring(0,commaIdx).trim();
  	   firstNameVal = keyWord.substring(commaIdx+1).trim();
        }
        lastNameVal = lastNameVal.toUpperCase();
        firstNameVal = firstNameVal.toUpperCase();
   } else if (searchMode.equals("search_chart_no")) {
	chartNoVal = keyWord;
   }
  }
%>
    <tr id="rowWithLastName" >
      <td align="right"> <b><bean:message key="demographic.demographicaddrecordhtm.formLastName"/><font color="red">:</font> </b></td>
      <td id="lastName" align="left">
        <input type="text" name="last_name" id="last_name" onBlur="upCaseCtrl(this)" size=30 value="<%=Encode.forHtmlAttribute(lastNameVal)%>">

      </td>
      <td align="right" id="firstNameLbl"><b><bean:message key="demographic.demographicaddrecordhtm.formFirstName"/><font color="red">:</font> </b> </td>
      <td id="firstName" align="left">
        <input type="text" name="first_name" id="first_name" onBlur="upCaseCtrl(this)"  value="<%=Encode.forHtmlAttribute(firstNameVal)%>" size=30>
      </td>
    </tr>
	<tr>
		<td align="right"> <b><bean:message key="demographic.demographicaddrecordhtm.formPrefName"/><font color="red">:</font> </b></td>
		<td id="prefName" align="left">
			<input type="text" name="pref_name" id="pref_name" onBlur="upCaseCtrl(this)" size=30 value="">
		</td>
		<% if (MapUtils.getOrDefault(masterFilePreferences, "display_former_name", false)) { %>
		<td align="right">
			<b>
				<bean:message key="demographic.demographicaddrecordhtm.formFormerName"/>
				<font color="red">:</font>
			</b>
		</td>
		<td id="formerName" align="left">
			<input type="text" name="former_name" id="former_name" onBlur="upCaseCtrl(this)" size=30 value="">
		</td>
		<% } %>
	</tr>
    <tr>
	<td id="languageLbl" align="right"><b><bean:message key="demographic.demographicaddrecordhtm.msgDemoLanguage"/><font color="red">:</font></b></td>
	<td id="languageCell" align="left">
	    <select id="official_lang" name="official_lang">
		<option value="English" <%= vLocale.getLanguage().equals("en") ? " selected":"" %>><bean:message key="demographic.demographiceaddrecordhtm.msgEnglish"/></option>
		<option value="French"  <%= vLocale.getLanguage().equals("fr") ? " selected":"" %>><bean:message key="demographic.demographiceaddrecordhtm.msgFrench"/></option>
	    </select>
	</td>
	<td id="titleLbl" align="right"><b><bean:message key="demographic.demographicaddrecordhtm.msgDemoTitle"/><font color="red">:</font></b></td>
	<td id="titleCell" align="left">
	    <select id="title" name="title" onchange="checkTitleSex(value);">
                <option value=""><bean:message key="demographic.demographicaddrecordhtm.msgNotSet"/></option>
                <option value="DR"><bean:message key="demographic.demographicaddrecordhtm.msgDr"/></option>
                <option value="MS"><bean:message key="demographic.demographicaddrecordhtm.msgMs"/></option>
                <option value="MISS"><bean:message key="demographic.demographicaddrecordhtm.msgMiss"/></option>
                <option value="MRS"><bean:message key="demographic.demographicaddrecordhtm.msgMrs"/></option>
                <option value="MR"><bean:message key="demographic.demographicaddrecordhtm.msgMr"/></option>
                <option value="MSSR"><bean:message key="demographic.demographicaddrecordhtm.msgMssr"/></option>
                <option value="PROF"><bean:message key="demographic.demographicaddrecordhtm.msgProf"/></option>
                <option value="REEVE"><bean:message key="demographic.demographicaddrecordhtm.msgReeve"/></option>
                <option value="REV"><bean:message key="demographic.demographicaddrecordhtm.msgRev"/></option>
                <option value="RT_HON"><bean:message key="demographic.demographicaddrecordhtm.msgRtHon"/></option>
                <option value="SEN"><bean:message key="demographic.demographicaddrecordhtm.msgSen"/></option>
                <option value="SGT"><bean:message key="demographic.demographicaddrecordhtm.msgSgt"/></option>
                <option value="SR"><bean:message key="demographic.demographicaddrecordhtm.msgSr"/></option>
	    </select>
	</td>
    </tr>
    <tr>
        <td id="spokenLbl" align="right"><b><bean:message key="demographic.demographicaddrecordhtm.msgSpoken"/>:</b></td>
        <td id="spokenCell">
			<select name="spoken_lang">
				<% for (String spokenLanguage : Util.spokenLangProperties.getLangSorted()) { %>
					<option value="<%= spokenLanguage %>"><%= spokenLanguage %></option>
				<% } %>
            </select>
        </td>
        <td align="right"><b><bean:message
					key="demographic.demographiceditdemographic.aboriginal" />: </b></td>
		<td align="left">
		<select name="aboriginal">
			<option value="">Unknown</option>
			<option value="No">No</option>
			<option value="Yes" >Yes</option>
		</select>
		</td>
    </tr>
	<td align="left" colspan="2"><b>Residential</b></td>
			<tr valign="top">
				<td id="addrLbl" align="right">
					<span style="float: left;"><input type="checkbox" name="addMailing" id="addMailing" onclick="toggleMailing()"><b>Add Mailing </b><input type="hidden" name="enableMailing" id="enableMailing"></span>
					<span style="float: right;"><b><bean:message
					key="demographic.demographicaddrecordhtm.formAddress" />: </b></span></td>
				<td id="addressCell" align="left"><input id="address" type="text" name="address" value="<%=Encode.forHtmlAttribute(address)%>" size=40 />
				</td>
				<td id="cityLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formCity" />: </b></td>
				<td id="cityCell" align="left"><input type="text" id="city" name="city"
					value="<%=defaultCity %>" /></td>
			</tr>
			<tr valign="top">
				<td id="provLbl" align="right"><b> 
				<% if (oscarProperties.getProperty("demographicLabelProvince") == null) { %>
				<bean:message key="demographic.demographicaddrecordhtm.formprovince" />
				<% } else {
          			out.print(oscarProperties.getProperty("demographicLabelProvince"));
      	 		} %> : </b></td>
				<td id="provCell" align="left">
				<select id="province" name="province">
					<option value="OT"
						<%=defaultProvince.equals("")||defaultProvince.equals("OT")?" selected":""%>>Other</option>
					<%-- <option value="">None Selected</option> --%>
					<% if (pNames.isDefined()) {
                   for (ListIterator li = pNames.listIterator(); li.hasNext(); ) {
                       String province = (String) li.next(); %>
					<option value="<%=province%>"
						<%=province.equals(defaultProvince)?" selected":""%>><%=li.next()%></option>
					<% } %>
					<% } else { %>
					<option value="AB" <%=defaultProvince.equals("AB")?" selected":""%>>AB-Alberta</option>
					<option value="BC" <%=defaultProvince.equals("BC")?" selected":""%>>BC-British Columbia</option>
					<option value="MB" <%=defaultProvince.equals("MB")?" selected":""%>>MB-Manitoba</option>
					<option value="NB" <%=defaultProvince.equals("NB")?" selected":""%>>NB-New Brunswick</option>
					<option value="NL" <%=defaultProvince.equals("NL")?" selected":""%>>NL-Newfoundland & Labrador</option>
					<option value="NT" <%=defaultProvince.equals("NT")?" selected":""%>>NT-Northwest Territory</option>
					<option value="NS" <%=defaultProvince.equals("NS")?" selected":""%>>NS-Nova Scotia</option>
					<option value="NU" <%=defaultProvince.equals("NU")?" selected":""%>>NU-Nunavut</option>
					<option value="ON" <%=defaultProvince.equals("ON")?" selected":""%>>ON-Ontario</option>
					<option value="PE" <%=defaultProvince.equals("PE")?" selected":""%>>PE-Prince Edward Island</option>
					<option value="QC" <%=defaultProvince.equals("QC")?" selected":""%>>QC-Quebec</option>
					<option value="SK" <%=defaultProvince.equals("SK")?" selected":""%>>SK-Saskatchewan</option>
					<option value="YT" <%=defaultProvince.equals("YT")?" selected":""%>>YT-Yukon</option>
					<option value="US" <%=defaultProvince.equals("US")?" selected":""%>>US resident</option>
					<option value="US-AK" <%=defaultProvince.equals("US-AK")?" selected":""%>>US-AK-Alaska</option>
					<option value="US-AL" <%=defaultProvince.equals("US-AL")?" selected":""%>>US-AL-Alabama</option>
					<option value="US-AR" <%=defaultProvince.equals("US-AR")?" selected":""%>>US-AR-Arkansas</option>
					<option value="US-AZ" <%=defaultProvince.equals("US-AZ")?" selected":""%>>US-AZ-Arizona</option>
					<option value="US-CA" <%=defaultProvince.equals("US-CA")?" selected":""%>>US-CA-California</option>
					<option value="US-CO" <%=defaultProvince.equals("US-CO")?" selected":""%>>US-CO-Colorado</option>
					<option value="US-CT" <%=defaultProvince.equals("US-CT")?" selected":""%>>US-CT-Connecticut</option>
					<option value="US-CZ" <%=defaultProvince.equals("US-CZ")?" selected":""%>>US-CZ-Canal Zone</option>
					<option value="US-DC" <%=defaultProvince.equals("US-DC")?" selected":""%>>US-DC-District Of Columbia</option>
					<option value="US-DE" <%=defaultProvince.equals("US-DE")?" selected":""%>>US-DE-Delaware</option>
					<option value="US-FL" <%=defaultProvince.equals("US-FL")?" selected":""%>>US-FL-Florida</option>
					<option value="US-GA" <%=defaultProvince.equals("US-GA")?" selected":""%>>US-GA-Georgia</option>
					<option value="US-GU" <%=defaultProvince.equals("US-GU")?" selected":""%>>US-GU-Guam</option>
					<option value="US-HI" <%=defaultProvince.equals("US-HI")?" selected":""%>>US-HI-Hawaii</option>
					<option value="US-IA" <%=defaultProvince.equals("US-IA")?" selected":""%>>US-IA-Iowa</option>
					<option value="US-ID" <%=defaultProvince.equals("US-ID")?" selected":""%>>US-ID-Idaho</option>
					<option value="US-IL" <%=defaultProvince.equals("US-IL")?" selected":""%>>US-IL-Illinois</option>
					<option value="US-IN" <%=defaultProvince.equals("US-IN")?" selected":""%>>US-IN-Indiana</option>
					<option value="US-KS" <%=defaultProvince.equals("US-KS")?" selected":""%>>US-KS-Kansas</option>
					<option value="US-KY" <%=defaultProvince.equals("US-KY")?" selected":""%>>US-KY-Kentucky</option>
					<option value="US-LA" <%=defaultProvince.equals("US-LA")?" selected":""%>>US-LA-Louisiana</option>
					<option value="US-MA" <%=defaultProvince.equals("US-MA")?" selected":""%>>US-MA-Massachusetts</option>
					<option value="US-MD" <%=defaultProvince.equals("US-MD")?" selected":""%>>US-MD-Maryland</option>
					<option value="US-ME" <%=defaultProvince.equals("US-ME")?" selected":""%>>US-ME-Maine</option>
					<option value="US-MI" <%=defaultProvince.equals("US-MI")?" selected":""%>>US-MI-Michigan</option>
					<option value="US-MN" <%=defaultProvince.equals("US-MN")?" selected":""%>>US-MN-Minnesota</option>
					<option value="US-MO" <%=defaultProvince.equals("US-MO")?" selected":""%>>US-MO-Missouri</option>
					<option value="US-MS" <%=defaultProvince.equals("US-MS")?" selected":""%>>US-MS-Mississippi</option>
					<option value="US-MT" <%=defaultProvince.equals("US-MT")?" selected":""%>>US-MT-Montana</option>
					<option value="US-NC" <%=defaultProvince.equals("US-NC")?" selected":""%>>US-NC-North Carolina</option>
					<option value="US-ND" <%=defaultProvince.equals("US-ND")?" selected":""%>>US-ND-North Dakota</option>
					<option value="US-NE" <%=defaultProvince.equals("US-NE")?" selected":""%>>US-NE-Nebraska</option>
					<option value="US-NH" <%=defaultProvince.equals("US-NH")?" selected":""%>>US-NH-New Hampshire</option>
					<option value="US-NJ" <%=defaultProvince.equals("US-NJ")?" selected":""%>>US-NJ-New Jersey</option>
					<option value="US-NM" <%=defaultProvince.equals("US-NM")?" selected":""%>>US-NM-New Mexico</option>
					<option value="US-NU" <%=defaultProvince.equals("US-NU")?" selected":""%>>US-NU-Nunavut</option>
					<option value="US-NV" <%=defaultProvince.equals("US-NV")?" selected":""%>>US-NV-Nevada</option>
					<option value="US-NY" <%=defaultProvince.equals("US-NY")?" selected":""%>>US-NY-New York</option>
					<option value="US-OH" <%=defaultProvince.equals("US-OH")?" selected":""%>>US-OH-Ohio</option>
					<option value="US-OK" <%=defaultProvince.equals("US-OK")?" selected":""%>>US-OK-Oklahoma</option>
					<option value="US-OR" <%=defaultProvince.equals("US-OR")?" selected":""%>>US-OR-Oregon</option>
					<option value="US-PA" <%=defaultProvince.equals("US-PA")?" selected":""%>>US-PA-Pennsylvania</option>
					<option value="US-PR" <%=defaultProvince.equals("US-PR")?" selected":""%>>US-PR-Puerto Rico</option>
					<option value="US-RI" <%=defaultProvince.equals("US-RI")?" selected":""%>>US-RI-Rhode Island</option>
					<option value="US-SC" <%=defaultProvince.equals("US-SC")?" selected":""%>>US-SC-South Carolina</option>
					<option value="US-SD" <%=defaultProvince.equals("US-SD")?" selected":""%>>US-SD-South Dakota</option>
					<option value="US-TN" <%=defaultProvince.equals("US-TN")?" selected":""%>>US-TN-Tennessee</option>
					<option value="US-TX" <%=defaultProvince.equals("US-TX")?" selected":""%>>US-TX-Texas</option>
					<option value="US-UT" <%=defaultProvince.equals("US-UT")?" selected":""%>>US-UT-Utah</option>
					<option value="US-VA" <%=defaultProvince.equals("US-VA")?" selected":""%>>US-VA-Virginia</option>
					<option value="US-VI" <%=defaultProvince.equals("US-VI")?" selected":""%>>US-VI-Virgin Islands</option>
					<option value="US-VT" <%=defaultProvince.equals("US-VT")?" selected":""%>>US-VT-Vermont</option>
					<option value="US-WA" <%=defaultProvince.equals("US-WA")?" selected":""%>>US-WA-Washington</option>
					<option value="US-WI" <%=defaultProvince.equals("US-WI")?" selected":""%>>US-WI-Wisconsin</option>
					<option value="US-WV" <%=defaultProvince.equals("US-WV")?" selected":""%>>US-WV-West Virginia</option>
					<option value="US-WY" <%=defaultProvince.equals("US-WY")?" selected":""%>>US-WY-Wyoming</option>
					<% } %>
				</select>
				</td>
				<td id="postalLbl" align="right"><b>
					<% if (oscarProperties.getProperty("demographicLabelPostal") == null) { %>
				<bean:message key="demographic.demographicaddrecordhtm.formPostal" />
					<% } else {
          			out.print(oscarProperties.getProperty("demographicLabelPostal"));
					} %> :
				</b></td>
				<td id="postalCell" align="left"><input type="text" id="postal" name="postal" value="<%=postal%>"
					onBlur="upCaseCtrl(this)"></td>
			</tr>
	<tbody id="mailingBody" style="visibility: hidden">
	<td align="left" colspan="2"><b>Mailing</b></td>
	<tr valign="top">
		<td id="addrMailingLbl" align="right"><b><bean:message key="demographic.demographicaddrecordhtm.formAddress" />: </b></td>
		<td id="addressMailingCell" align="left"><input id="address_mailing" type="text" name="address_mailing" value="" size=40 />
		</td>
		<td id="cityMailingLbl" align="right"><b><bean:message key="demographic.demographicaddrecordhtm.formCity" />: </b></td>
		<td id="cityMailingCell" align="left">
			<input type="text" id="city_mailing" name="city_mailing" value="" />
		</td>
	</tr>
	<tr valign="top">
		<td id="provMailingLbl" align="right"><b>
			<% if (oscarProperties.getProperty("demographicLabelProvince") == null) { %>
			<bean:message key="demographic.demographicaddrecordhtm.formprovince" />
			<% } else {
				out.print(oscarProperties.getProperty("demographicLabelProvince"));
			} %> :
		</b></td>
		<td id="provMailingCell" align="left">
			<select id="province_mailing" name="province_mailing">
				<option value="OT"
						<%=defaultProvince.equals("")||defaultProvince.equals("OT")?" selected":""%>>Other</option>
				<% if (pNames.isDefined()) {
					for (ListIterator li = pNames.listIterator(); li.hasNext(); ) {
						String province = (String) li.next(); %>
				<option value="<%=province%>"
						<%=province.equals(defaultProvince)?" selected":""%>><%=li.next()%></option>
				<% } %>
				<% } else { %>
				<option value="AB" <%=defaultProvince.equals("AB")?" selected":""%>>AB-Alberta</option>
				<option value="BC" <%=defaultProvince.equals("BC")?" selected":""%>>BC-British Columbia</option>
				<option value="MB" <%=defaultProvince.equals("MB")?" selected":""%>>MB-Manitoba</option>
				<option value="NB" <%=defaultProvince.equals("NB")?" selected":""%>>NB-New Brunswick</option>
				<option value="NL" <%=defaultProvince.equals("NL")?" selected":""%>>NL-Newfoundland & Labrador</option>
				<option value="NT" <%=defaultProvince.equals("NT")?" selected":""%>>NT-Northwest Territory</option>
				<option value="NS" <%=defaultProvince.equals("NS")?" selected":""%>>NS-Nova Scotia</option>
				<option value="NU" <%=defaultProvince.equals("NU")?" selected":""%>>NU-Nunavut</option>
				<option value="ON" <%=defaultProvince.equals("ON")?" selected":""%>>ON-Ontario</option>
				<option value="PE" <%=defaultProvince.equals("PE")?" selected":""%>>PE-Prince Edward Island</option>
				<option value="QC" <%=defaultProvince.equals("QC")?" selected":""%>>QC-Quebec</option>
				<option value="SK" <%=defaultProvince.equals("SK")?" selected":""%>>SK-Saskatchewan</option>
				<option value="YT" <%=defaultProvince.equals("YT")?" selected":""%>>YT-Yukon</option>
				<option value="US" <%=defaultProvince.equals("US")?" selected":""%>>US resident</option>
				<option value="US-AK" <%=defaultProvince.equals("US-AK")?" selected":""%>>US-AK-Alaska</option>
				<option value="US-AL" <%=defaultProvince.equals("US-AL")?" selected":""%>>US-AL-Alabama</option>
				<option value="US-AR" <%=defaultProvince.equals("US-AR")?" selected":""%>>US-AR-Arkansas</option>
				<option value="US-AZ" <%=defaultProvince.equals("US-AZ")?" selected":""%>>US-AZ-Arizona</option>
				<option value="US-CA" <%=defaultProvince.equals("US-CA")?" selected":""%>>US-CA-California</option>
				<option value="US-CO" <%=defaultProvince.equals("US-CO")?" selected":""%>>US-CO-Colorado</option>
				<option value="US-CT" <%=defaultProvince.equals("US-CT")?" selected":""%>>US-CT-Connecticut</option>
				<option value="US-CZ" <%=defaultProvince.equals("US-CZ")?" selected":""%>>US-CZ-Canal Zone</option>
				<option value="US-DC" <%=defaultProvince.equals("US-DC")?" selected":""%>>US-DC-District Of Columbia</option>
				<option value="US-DE" <%=defaultProvince.equals("US-DE")?" selected":""%>>US-DE-Delaware</option>
				<option value="US-FL" <%=defaultProvince.equals("US-FL")?" selected":""%>>US-FL-Florida</option>
				<option value="US-GA" <%=defaultProvince.equals("US-GA")?" selected":""%>>US-GA-Georgia</option>
				<option value="US-GU" <%=defaultProvince.equals("US-GU")?" selected":""%>>US-GU-Guam</option>
				<option value="US-HI" <%=defaultProvince.equals("US-HI")?" selected":""%>>US-HI-Hawaii</option>
				<option value="US-IA" <%=defaultProvince.equals("US-IA")?" selected":""%>>US-IA-Iowa</option>
				<option value="US-ID" <%=defaultProvince.equals("US-ID")?" selected":""%>>US-ID-Idaho</option>
				<option value="US-IL" <%=defaultProvince.equals("US-IL")?" selected":""%>>US-IL-Illinois</option>
				<option value="US-IN" <%=defaultProvince.equals("US-IN")?" selected":""%>>US-IN-Indiana</option>
				<option value="US-KS" <%=defaultProvince.equals("US-KS")?" selected":""%>>US-KS-Kansas</option>
				<option value="US-KY" <%=defaultProvince.equals("US-KY")?" selected":""%>>US-KY-Kentucky</option>
				<option value="US-LA" <%=defaultProvince.equals("US-LA")?" selected":""%>>US-LA-Louisiana</option>
				<option value="US-MA" <%=defaultProvince.equals("US-MA")?" selected":""%>>US-MA-Massachusetts</option>
				<option value="US-MD" <%=defaultProvince.equals("US-MD")?" selected":""%>>US-MD-Maryland</option>
				<option value="US-ME" <%=defaultProvince.equals("US-ME")?" selected":""%>>US-ME-Maine</option>
				<option value="US-MI" <%=defaultProvince.equals("US-MI")?" selected":""%>>US-MI-Michigan</option>
				<option value="US-MN" <%=defaultProvince.equals("US-MN")?" selected":""%>>US-MN-Minnesota</option>
				<option value="US-MO" <%=defaultProvince.equals("US-MO")?" selected":""%>>US-MO-Missouri</option>
				<option value="US-MS" <%=defaultProvince.equals("US-MS")?" selected":""%>>US-MS-Mississippi</option>
				<option value="US-MT" <%=defaultProvince.equals("US-MT")?" selected":""%>>US-MT-Montana</option>
				<option value="US-NC" <%=defaultProvince.equals("US-NC")?" selected":""%>>US-NC-North Carolina</option>
				<option value="US-ND" <%=defaultProvince.equals("US-ND")?" selected":""%>>US-ND-North Dakota</option>
				<option value="US-NE" <%=defaultProvince.equals("US-NE")?" selected":""%>>US-NE-Nebraska</option>
				<option value="US-NH" <%=defaultProvince.equals("US-NH")?" selected":""%>>US-NH-New Hampshire</option>
				<option value="US-NJ" <%=defaultProvince.equals("US-NJ")?" selected":""%>>US-NJ-New Jersey</option>
				<option value="US-NM" <%=defaultProvince.equals("US-NM")?" selected":""%>>US-NM-New Mexico</option>
				<option value="US-NU" <%=defaultProvince.equals("US-NU")?" selected":""%>>US-NU-Nunavut</option>
				<option value="US-NV" <%=defaultProvince.equals("US-NV")?" selected":""%>>US-NV-Nevada</option>
				<option value="US-NY" <%=defaultProvince.equals("US-NY")?" selected":""%>>US-NY-New York</option>
				<option value="US-OH" <%=defaultProvince.equals("US-OH")?" selected":""%>>US-OH-Ohio</option>
				<option value="US-OK" <%=defaultProvince.equals("US-OK")?" selected":""%>>US-OK-Oklahoma</option>
				<option value="US-OR" <%=defaultProvince.equals("US-OR")?" selected":""%>>US-OR-Oregon</option>
				<option value="US-PA" <%=defaultProvince.equals("US-PA")?" selected":""%>>US-PA-Pennsylvania</option>
				<option value="US-PR" <%=defaultProvince.equals("US-PR")?" selected":""%>>US-PR-Puerto Rico</option>
				<option value="US-RI" <%=defaultProvince.equals("US-RI")?" selected":""%>>US-RI-Rhode Island</option>
				<option value="US-SC" <%=defaultProvince.equals("US-SC")?" selected":""%>>US-SC-South Carolina</option>
				<option value="US-SD" <%=defaultProvince.equals("US-SD")?" selected":""%>>US-SD-South Dakota</option>
				<option value="US-TN" <%=defaultProvince.equals("US-TN")?" selected":""%>>US-TN-Tennessee</option>
				<option value="US-TX" <%=defaultProvince.equals("US-TX")?" selected":""%>>US-TX-Texas</option>
				<option value="US-UT" <%=defaultProvince.equals("US-UT")?" selected":""%>>US-UT-Utah</option>
				<option value="US-VA" <%=defaultProvince.equals("US-VA")?" selected":""%>>US-VA-Virginia</option>
				<option value="US-VI" <%=defaultProvince.equals("US-VI")?" selected":""%>>US-VI-Virgin Islands</option>
				<option value="US-VT" <%=defaultProvince.equals("US-VT")?" selected":""%>>US-VT-Vermont</option>
				<option value="US-WA" <%=defaultProvince.equals("US-WA")?" selected":""%>>US-WA-Washington</option>
				<option value="US-WI" <%=defaultProvince.equals("US-WI")?" selected":""%>>US-WI-Wisconsin</option>
				<option value="US-WV" <%=defaultProvince.equals("US-WV")?" selected":""%>>US-WV-West Virginia</option>
				<option value="US-WY" <%=defaultProvince.equals("US-WY")?" selected":""%>>US-WY-Wyoming</option>
				<% } %>
			</select>
		</td>
		<td id="postalMailingLbl" align="right">
			<b> <% if (oscarProperties.getProperty("demographicLabelPostal") == null) { %>
			<bean:message key="demographic.demographicaddrecordhtm.formPostal" />
			<% } else {
				out.print(oscarProperties.getProperty("demographicLabelPostal"));
			} %> :
			</b>
		</td>
		<td id="postalMailingCell" align="left"><input type="text" id="postal_mailing" name="postal_mailing" value="<%=postal%>" onBlur="upCaseCtrl(this)"></td>
	</tr></tbody>
		<tbody>
			<% if (SystemPreferencesUtils.isAppointmentRemindersEnabled()) { %>
			<tr valign="top">
				<td align="right" nowrap>
					<b><bean:message key="demographic.demographiceditdemographic.AllowAppointmentReminders" />:</b>
				</td>
				<td align="left">
					<select name="allow_appointment_reminders" id="allow_appointment_reminders" onchange="checkApptReminderSelect();">
						<option value="true" selected>Yes</option>
						<option value="false">No</option>
					</select>
				</td>
			</tr>
			<tr valign="top" class="reminderContactMethods">
				<td align="right" nowrap></td>
				<td align="left">
					<label>Phone:
						<input type="checkbox" id="reminder-phone" name="reminder_phone" onclick="checkReminderContactMethod('reminder_phone')" checked/>
					</label>
					<label>Cell:
						<input type="checkbox" id="reminder-cell" name="reminder_cell" onclick="checkReminderContactMethod('reminder_cell')" checked/>
					</label>
					<label>Email:
						<input type="checkbox" id="reminder-email" name="reminder_email" onclick="checkReminderContactMethod('reminder_email')" checked/>
					</label>
				</td>
			</tr>
			<% } %>
		</tbody>
			<tr valign="top">
				<td id="phoneLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formPhoneHome" />: </b></td>
				<td id="phoneCell" align="left"><input type="text" id="phone" name="phone"
					onBlur="formatPhoneNum()"
					value="<%=phone%>"> <bean:message
					key="demographic.demographicaddrecordhtm.Ext" />:<input
					type="text" id="hPhoneExt" name="hPhoneExt" value="" size="4" /></td>
				<td id="phoneWorkLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formPhoneWork" />:</b></td>
				<td id="phoneWorkCell" align="left"><input type="text" name="phone2"
					onBlur="formatPhoneNum()" value=""> <bean:message
					key="demographic.demographicaddrecordhtm.Ext" />:<input type="text"
					name="wPhoneExt" value="" style="display: inline" size="4" /></td>
			</tr>
			<tr valign="top">
				<td id="phoneCellLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formPhoneCell" />: </b></td>
				<td id="phoneCellCell" align="left"><input type="text" name="demo_cell"
					onBlur="formatPhoneNum()"></td>
				<td align="right"><b><bean:message
						key="demographic.demographicaddrecordhtm.formPhoneComment" />: </b></td>
				<td align="left" colspan="3">
						<textarea rows="2" cols="30" name="phoneComment"></textarea>
				</td>
			</tr>
            <tr>
                <td td align="right" nowrap>
                    <b><bean:message key="demographic.demographiceditdemographic.PhonePreference" />:</b>
                </td>
                <td align="left">
                    <select name="phone_preference" id="phone_preference">
                        <option value="H" <%="H".equals(phonePreference) ? "selected=\"selected\"" : ""%>>Home</option>
                        <option value="W" <%="W".equals(phonePreference) ? "selected=\"selected\"" : ""%>>Work</option>
                        <option value="C" <%="C".equals(phonePreference) ? "selected=\"selected\"" : ""%>>Cell</option>
                    </select>
                </td>
                <td align="right"><b><bean:message key="demographic.demographiceditdemographic.formGender" />: </b></td>
				<td align="left">
					<% 
						String gender = "";
					 %>  
					 <select name="gender" id="gender" onchange="checkGender()"<%= getDisabled("gender") %>>
					 	<option value="" <%= gender.equals("") ? "selected" : "" %>></option>
						<%
							for (DemographicGender thisDemographicGender : demographicGender) {
						%>
								<option value="<%= thisDemographicGender.getId() %>"
									data-is-editable="<%= thisDemographicGender.isEditable() %>">
									<%= thisDemographicGender.getValue() %>
								</option>
						<%
							}
						%>
					</select> 
					<input type="button" id="genderBtn" onClick="newGender()"
							value="<bean:message key="demographic.demographiceditdemographic.btnAddNew"/>">
					<input type="button" id="deleteGenBtn" onClick="delGender()" style="display: none;"
							value="<bean:message key="demographic.demographiceditdemographic.btnDelete"/>">
				</td>
            </tr>
			<tr valign="top">
				<td id="newsletterLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formNewsLetter" />: </b></td>
				<td id="newsletterCell" align="left"><select name="newsletter">
					<option value="Unknown" selected><bean:message
						key="demographic.demographicaddrecordhtm.formNewsLetter.optUnknown" /></option>
					<option value="No"><bean:message
						key="demographic.demographicaddrecordhtm.formNewsLetter.optNo" /></option>
					<option value="Paper"><bean:message
						key="demographic.demographicaddrecordhtm.formNewsLetter.optPaper" /></option>
					<option value="Electronic"><bean:message
						key="demographic.demographicaddrecordhtm.formNewsLetter.optElectronic" /></option>
				</select></td>
							
				<td align="right">
					<b><bean:message key="demographic.demographiceditdemographic.formPronoun" />:</b>
				</td>
				<td align="left">
					<%
						String pronoun = "";
					%> 
					<select name="pronoun" id="pronoun" onchange="checkPronoun()" <%= getDisabled("pronoun") %>>
						<option value="" <%= pronoun.equals("") ? "selected" : "" %>></option>
						<%
							for (DemographicPronoun thisDemographicPronoun : demographicPronoun) {
						%>
								<option value="<%= thisDemographicPronoun.getId() %>"
									data-is-editable="<%= thisDemographicPronoun.isEditable() %>">
									<%= thisDemographicPronoun.getValue() %>
								</option>
						<%
							}
						%>
					</select> 
					<input type="button" onClick="newPronoun()" id="pronounBtn"
						value="<bean:message key="demographic.demographiceditdemographic.btnAddNew"/>">
					<input type="button" id="deleteProBtn" onClick="delPronoun()" style="display: none;"
						value="<bean:message key="demographic.demographiceditdemographic.btnDelete"/>">
				</td>
			</tr>
			<tr valign="top">
				<td id="emailLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formEMail" />: </b></td>
				<td id="emailCell" align="left"><input type="text" id="email" name="email" value="">
					<input type="checkbox" name="includeEmailOnConsults" value="true"/> Include on Consults
				</td>
				<td id="myOscarLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formPHRUserName" />:</b></td>
				<td id="myOscarCell"  align="left"><input type="text" name="myOscarUserName" value="">
				</td>
			</tr>
			<tr valign="top">
				<td id="dobLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formDOB" /></b><b><font
					color="red">:</font></b></td>
				<td id="dobTbl" align="left" nowrap>
					<input type="text" name="full_birth_date" id="full_birth_date"/>
					<img src="../images/cal.gif" id="full_birth_date_cal">
					<input type="hidden" name="year_of_birth" id="year_of_birth"/>
					<input type="hidden" name="month_of_birth" id="month_of_birth"/>
					<input type="hidden" name="date_of_birth" id="date_of_birth"/>
					<script type="application/javascript">
						createStandardDatepicker(jQuery_3_1_0('#full_birth_date'), "full_birth_date_cal");
						jQuery_3_1_0('#full_birth_date').change(function(){
							var birthDate = new Date(jQuery_3_1_0('#full_birth_date').val());
							if (!isNaN(birthDate)) {
								document.getElementById('year_of_birth').value = birthDate.toISOString().substring(0, 4);
								document.getElementById('month_of_birth').value = birthDate.toISOString().substring(5, 7)
								document.getElementById('date_of_birth').value = birthDate.toISOString().substring(8, 10)
							} else if (jQuery_3_1_0('#full_birth_date').val() == '') {
								document.getElementById('year_of_birth').value = '';
								document.getElementById('month_of_birth').value = '';
								document.getElementById('date_of_birth').value = '';
							}
						});
					</script>
				</td>
				<td align="right" id="genderLbl"><b><bean:message
					key="demographic.demographicaddrecordhtm.formSex" /><font
					color="red">:</font></b></td>
				<% // Determine if curUser has selected a default sex in preferences
                                   UserProperty sexProp = userPropertyDAO.getProp(curUser_no,  UserProperty.DEFAULT_SEX);
                                   String sex = "";
                                   if (sexProp != null) {
                                       sex = sexProp.getValue();
                                   } else {
                                       // Access defaultsex system property
                                       sex = oscarProperties.getProperty("defaultsex","");
                                   }
                                %>
                                <td id="gender" align="left">
                                <select  name="sex" id="sex">
			                        <option value=""></option>
			                		<% for(org.oscarehr.common.Gender gn : org.oscarehr.common.Gender.values()){ %>
			                        <option value=<%=gn.name()%> <%=((sex.toUpperCase().equals(gn.name())) ? "selected" : "") %>><%=gn.getText()%></option>
			                        <% } %>
			                        </select>
                                </select>
                                </td>
			</tr>
			<tr valign="top">
				<td align="right" id="hinLbl"><b><bean:message
					key="demographic.demographicaddrecordhtm.formHIN" />: </b></td>
				<td align="left" id="hinVer" nowrap><input type="text" name="hin" id="hin"
                                                               size="15" onfocus="autoFillHin()" > <b><bean:message
					key="demographic.demographicaddrecordhtm.formVer" />: <input
					type="text" id="ver" name="ver" value="" size="3" onBlur="upCaseCtrl(this)">
				</b></td>
				<td id="effDateLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formEFFDate" /></b><b>: </b></td>
				<td id="effDate" align="left">
					<input type="text" name="eff_date" id="eff_date">
					<img src="../images/cal.gif" id="eff_date_cal">
					<script type="application/javascript">createStandardDatepicker(jQuery_3_1_0('#eff_date'), "eff_date_cal");</script>
				</td>
			</tr>                       
			<tr>
				<td id="hcTypeLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formHCType" />: </b></td>
				<td id="hcType">
				<select name="hc_type" id="hc_type">
					<option value="OT"
						<%=HCType.equals("")||HCType.equals("OT")?" selected":""%>>Other</option>
					<% if (pNames.isDefined()) {
                   for (ListIterator li = pNames.listIterator(); li.hasNext(); ) {
                       String province = (String) li.next(); %>
                       <option value="<%=province%>"<%=province.equals(HCType)?" selected":""%>><%=li.next()%></option>
                   <% } %>
            <% } else { %>
		<option value="AB"<%=HCType.equals("AB")?" selected":""%>>AB-Alberta</option>
		<option value="BC"<%=HCType.equals("BC")?" selected":""%>>BC-British Columbia</option>
		<option value="MB"<%=HCType.equals("MB")?" selected":""%>>MB-Manitoba</option>
		<option value="NB"<%=HCType.equals("NB")?" selected":""%>>NB-New Brunswick</option>
		<option value="NL"<%=HCType.equals("NL")?" selected":""%>>NL-Newfoundland & Labrador</option>
		<option value="NT"<%=HCType.equals("NT")?" selected":""%>>NT-Northwest Territory</option>
		<option value="NS"<%=HCType.equals("NS")?" selected":""%>>NS-Nova Scotia</option>
		<option value="NU"<%=HCType.equals("NU")?" selected":""%>>NU-Nunavut</option>
		<option value="ON"<%=HCType.equals("ON")?" selected":""%>>ON-Ontario</option>
		<option value="PE"<%=HCType.equals("PE")?" selected":""%>>PE-Prince Edward Island</option>
		<option value="QC"<%=HCType.equals("QC")?" selected":""%>>QC-Quebec</option>
		<option value="SK"<%=HCType.equals("SK")?" selected":""%>>SK-Saskatchewan</option>
		<option value="YT"<%=HCType.equals("YT")?" selected":""%>>YT-Yukon</option>
		<option value="US"<%=HCType.equals("US")?" selected":""%>>US resident</option>
		<option value="US-AK" <%=HCType.equals("US-AK")?" selected":""%>>US-AK-Alaska</option>
		<option value="US-AL" <%=HCType.equals("US-AL")?" selected":""%>>US-AL-Alabama</option>
		<option value="US-AR" <%=HCType.equals("US-AR")?" selected":""%>>US-AR-Arkansas</option>
		<option value="US-AZ" <%=HCType.equals("US-AZ")?" selected":""%>>US-AZ-Arizona</option>
		<option value="US-CA" <%=HCType.equals("US-CA")?" selected":""%>>US-CA-California</option>
		<option value="US-CO" <%=HCType.equals("US-CO")?" selected":""%>>US-CO-Colorado</option>
		<option value="US-CT" <%=HCType.equals("US-CT")?" selected":""%>>US-CT-Connecticut</option>
		<option value="US-CZ" <%=HCType.equals("US-CZ")?" selected":""%>>US-CZ-Canal Zone</option>
		<option value="US-DC" <%=HCType.equals("US-DC")?" selected":""%>>US-DC-District Of Columbia</option>
		<option value="US-DE" <%=HCType.equals("US-DE")?" selected":""%>>US-DE-Delaware</option>
		<option value="US-FL" <%=HCType.equals("US-FL")?" selected":""%>>US-FL-Florida</option>
		<option value="US-GA" <%=HCType.equals("US-GA")?" selected":""%>>US-GA-Georgia</option>
		<option value="US-GU" <%=HCType.equals("US-GU")?" selected":""%>>US-GU-Guam</option>
		<option value="US-HI" <%=HCType.equals("US-HI")?" selected":""%>>US-HI-Hawaii</option>
		<option value="US-IA" <%=HCType.equals("US-IA")?" selected":""%>>US-IA-Iowa</option>
		<option value="US-ID" <%=HCType.equals("US-ID")?" selected":""%>>US-ID-Idaho</option>
		<option value="US-IL" <%=HCType.equals("US-IL")?" selected":""%>>US-IL-Illinois</option>
		<option value="US-IN" <%=HCType.equals("US-IN")?" selected":""%>>US-IN-Indiana</option>
		<option value="US-KS" <%=HCType.equals("US-KS")?" selected":""%>>US-KS-Kansas</option>
		<option value="US-KY" <%=HCType.equals("US-KY")?" selected":""%>>US-KY-Kentucky</option>
		<option value="US-LA" <%=HCType.equals("US-LA")?" selected":""%>>US-LA-Louisiana</option>
		<option value="US-MA" <%=HCType.equals("US-MA")?" selected":""%>>US-MA-Massachusetts</option>
		<option value="US-MD" <%=HCType.equals("US-MD")?" selected":""%>>US-MD-Maryland</option>
		<option value="US-ME" <%=HCType.equals("US-ME")?" selected":""%>>US-ME-Maine</option>
		<option value="US-MI" <%=HCType.equals("US-MI")?" selected":""%>>US-MI-Michigan</option>
		<option value="US-MN" <%=HCType.equals("US-MN")?" selected":""%>>US-MN-Minnesota</option>
		<option value="US-MO" <%=HCType.equals("US-MO")?" selected":""%>>US-MO-Missouri</option>
		<option value="US-MS" <%=HCType.equals("US-MS")?" selected":""%>>US-MS-Mississippi</option>
		<option value="US-MT" <%=HCType.equals("US-MT")?" selected":""%>>US-MT-Montana</option>
		<option value="US-NC" <%=HCType.equals("US-NC")?" selected":""%>>US-NC-North Carolina</option>
		<option value="US-ND" <%=HCType.equals("US-ND")?" selected":""%>>US-ND-North Dakota</option>
		<option value="US-NE" <%=HCType.equals("US-NE")?" selected":""%>>US-NE-Nebraska</option>
		<option value="US-NH" <%=HCType.equals("US-NH")?" selected":""%>>US-NH-New Hampshire</option>
		<option value="US-NJ" <%=HCType.equals("US-NJ")?" selected":""%>>US-NJ-New Jersey</option>
		<option value="US-NM" <%=HCType.equals("US-NM")?" selected":""%>>US-NM-New Mexico</option>
		<option value="US-NU" <%=HCType.equals("US-NU")?" selected":""%>>US-NU-Nunavut</option>
		<option value="US-NV" <%=HCType.equals("US-NV")?" selected":""%>>US-NV-Nevada</option>
		<option value="US-NY" <%=HCType.equals("US-NY")?" selected":""%>>US-NY-New York</option>
		<option value="US-OH" <%=HCType.equals("US-OH")?" selected":""%>>US-OH-Ohio</option>
		<option value="US-OK" <%=HCType.equals("US-OK")?" selected":""%>>US-OK-Oklahoma</option>
		<option value="US-OR" <%=HCType.equals("US-OR")?" selected":""%>>US-OR-Oregon</option>
		<option value="US-PA" <%=HCType.equals("US-PA")?" selected":""%>>US-PA-Pennsylvania</option>
		<option value="US-PR" <%=HCType.equals("US-PR")?" selected":""%>>US-PR-Puerto Rico</option>
		<option value="US-RI" <%=HCType.equals("US-RI")?" selected":""%>>US-RI-Rhode Island</option>
		<option value="US-SC" <%=HCType.equals("US-SC")?" selected":""%>>US-SC-South Carolina</option>
		<option value="US-SD" <%=HCType.equals("US-SD")?" selected":""%>>US-SD-South Dakota</option>
		<option value="US-TN" <%=HCType.equals("US-TN")?" selected":""%>>US-TN-Tennessee</option>
		<option value="US-TX" <%=HCType.equals("US-TX")?" selected":""%>>US-TX-Texas</option>
		<option value="US-UT" <%=HCType.equals("US-UT")?" selected":""%>>US-UT-Utah</option>
		<option value="US-VA" <%=HCType.equals("US-VA")?" selected":""%>>US-VA-Virginia</option>
		<option value="US-VI" <%=HCType.equals("US-VI")?" selected":""%>>US-VI-Virgin Islands</option>
		<option value="US-VT" <%=HCType.equals("US-VT")?" selected":""%>>US-VT-Vermont</option>
		<option value="US-WA" <%=HCType.equals("US-WA")?" selected":""%>>US-WA-Washington</option>
		<option value="US-WI" <%=HCType.equals("US-WI")?" selected":""%>>US-WI-Wisconsin</option>
		<option value="US-WV" <%=HCType.equals("US-WV")?" selected":""%>>US-WV-West Virginia</option>
		<option value="US-WY" <%=HCType.equals("US-WY")?" selected":""%>>US-WY-Wyoming</option>
          <% } %>
          </select>
      </td>
      <td id="renewDateLbl" align="right"><b>*<bean:message key="demographic.demographiceditdemographic.formHCRenewDate" />:</b></td>
      <td id="renewDate" align="left">
		  <input type="text" name="hc_renew_date" id="hc_renew_date">
		  <img src="../images/cal.gif" id="hc_renew_date_cal">
		  <script type="application/javascript">createStandardDatepicker(jQuery_3_1_0('#hc_renew_date'), "hc_renew_date_cal");</script>
      </td>
     </tr>
     <tr>
      <td id="countryLbl" align="right">
         <b><bean:message key="demographic.demographicaddrecordhtm.msgCountryOfOrigin"/>:</b>
      </td>
      <td id="countryCell">
          <select id="countryOfOrigin" name="countryOfOrigin">
              <option value="-1"><bean:message key="demographic.demographicaddrecordhtm.msgNotSet"/></option>
              <% for (CountryCode cc : countryList) { %>
              	<option value="<%= cc.getCountryId() %>"><%= cc.getCountryName() %></option>
              <% } %>
          </select>
      </td>
		<% if (privateConsentEnabled) { %>
		<%
			String[] privateConsentPrograms = oscarProperties.getProperty("privateConsentPrograms","").split(",");
			ProgramProvider pp2 = programManager2.getCurrentProgramInDomain(loggedInInfo,loggedInInfo.getLoggedInProviderNo());
			boolean showConsentsThisTime=false;
			if(pp2 != null) {
				for (String privateConsentProgram : privateConsentPrograms) {
					if (privateConsentProgram.equals(pp2.getProgramId().toString())) {
						showConsentsThisTime = true;
						break;
					}
				}
			}
			if(showConsentsThisTime) {
		%>
		<td colspan="2">
			<div id="usSigned">
				<input type="radio" name="usSigned" value="signed">U.S. Resident Consent Form Signed
				<br/>
			    <input type="radio" name="usSigned" value="unsigned">U.S. Resident Consent Form NOT Signed
		    </div>
		</td>
		<% } %>
		<% } %>
    </tr>
    <tr valign="top">
     		<%-- TOGGLE FIRST NATIONS MODULE --%>							    
			<oscar:oscarPropertiesCheck value="true" defaultVal="false" property="FIRST_NATIONS_MODULE">
					<jsp:include page="manageFirstNationsModule.jsp" flush="true">
						<jsp:param name="demo" value="0" />
					</jsp:include>
			</oscar:oscarPropertiesCheck>
			<%-- END TOGGLE FIRST NATIONS MODULE --%>
				<% if (showSin) {%>
				<td align="right"><b>SIN:</b></td>
				<td align="left"><input type="text" name="sin" <%=getDisabled("sin")%> size="30" value=""></td>
				<%}%>
	<td  id="cytologyLbl" align="right"><b> <bean:message key="demographic.demographicaddrecordhtm.cytolNum"/>:</b> </td>
	<td id="cytologyCell" align="left"  >
	    <input type="text" name="cytolNum">
	</td>
    </tr>
    <tr valign="top">
      <td id="demoDoctorLbl" align="right"><b>
		  <% if (oscarProperties.getProperty("demographicLabelDoctor") != null) {
			  out.print(oscarProperties.getProperty("demographicLabelDoctor",""));
		  } else { %>
		  	<bean:message key="demographic.demographicaddrecordhtm.formDoctor"/>
		  <% } %> :
	  </b></td>
      <td id="demoDoctorCell" align="left" >
        <select name="staff" id="provider_no">
					<option value=""></option>
					<%
					    String defaultDoctor = ((ProviderPreference)session.getAttribute(SessionConstants.LOGGED_IN_PROVIDER_PREFERENCE)).getDefaultDoctor(); 
					    if (requireCpsidAndDoctorForMrp) {
					        for (Provider p : providerDao.getActiveProvidersByRole("doctor")) {
					            String docProviderNo = p.getProviderNo();
					            if (p.getPractitionerNo() != null
					                    && !p.getPractitionerNo().isEmpty()
					                    && p.getProviderType() != null
					                    && !p.getProviderType().isEmpty()
					                    && p.getProviderType().equalsIgnoreCase("doctor")) { %>
					                <option id="doc<%= docProviderNo %>"
					                    value="<%= docProviderNo %>" <%= docProviderNo.equals(defaultDoctor) ? "selected='selected'" : "" %>>
					                    <%= p.getFormattedName(enableExternalNameOnDemographic) %>
					                </option>
					            <% } %>
					        <% } %> 
					    <% } else {
					        for (Provider p : providerDao.getActiveProvidersByRole("doctor")) {
					            String docProviderNo = p.getProviderNo();
					    %>
					            <option id="doc<%= docProviderNo %>" 
					                value="<%= docProviderNo %>" <%= docProviderNo.equals(defaultDoctor) ? "selected='selected'" : "" %>>
					                <%= p.getFormattedName(enableExternalNameOnDemographic) %>
					            </option>
					        <% } %>
					    <% } %>
				</select></td>
				<td id="nurseLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formNurse" />: </b></td>
				<td id="nurseCell" ><select name="nurse">
					<option value=""></option>
					<% for (Provider p : providerDao.getActiveProvidersByRole("nurse")) { %>
						<option value="<%= p.getProviderNo() %>">
							<%=Encode.forHtmlContent(Misc.getShortStr( (p.getFormattedName()),"",12)) %>
						</option>
					<% } %>
				</select></td>
			</tr>
			<tr valign="top">
				<td id="ptStatusLbl" align="right"><b><bean:message
						key="demographic.demographicaddrecordhtm.formPatientStatus" />:</b></td>
				<td id="ptStatusCell" align="left">
					<select id="patient_status" name="patient_status" style="width: 160">
						<option value="AC"><bean:message key="demographic.demographicaddrecordhtm.AC-Active" /></option>
						<option value="IN"><bean:message key="demographic.demographicaddrecordhtm.IN-InActive" /></option>
						<option value="DE"><bean:message key="demographic.demographicaddrecordhtm.DE-Deceased" /></option>
						<option value="MO"><bean:message key="demographic.demographicaddrecordhtm.MO-Moved" /></option>
						<option value="FI"><bean:message key="demographic.demographicaddrecordhtm.FI-Fired" /></option>
						<% for (String status : demographicDao.search_ptstatus()) { %>
							<option value="<%= status %>"><%= status %></option>
						<% } %>
					</select> <input type="button" onClick="newStatus();" value="<bean:message
					key="demographic.demographicaddrecordhtm.AddNewPatient"/> ">
				</td>
			</tr>
			<tr valign="top">
				<td id="midwifeLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formMidwife" />: </b></td>
				<td id="midwifeCell">
					<select name="midwife">
						<option value=""></option>
						<% for (Provider p : providerDao.getActiveProvidersByRole("midwife")) { %>
							<option value="<%= p.getProviderNo() %>">
								<%= Encode.forHtmlContent(Misc.getShortStr( (p.getFormattedName()),"",12)) %>
							</option>
						<% } %>
					</select>
				</td>
				<td id="residentLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formResident" />: </b></td>
				<td id="residentCell" align="left"><select name="resident">
					<option value=""></option>
					<% for (Provider p : providerDao.getActiveProvidersByRole("doctor")) { %>
					<option value="<%= p.getProviderNo() %>">
					<%= Encode.forHtmlContent(Misc.getShortStr((p.getFormattedName()), "", 12)) %></option>
					<% } %>
				</select></td>
			</tr>
			<tr id="rowWithReferralDoc" valign="top">
				<td id="referralDocLbl" align="right" height="10"><b><bean:message
					key="demographic.demographicaddrecordhtm.formReferalDoctor" />:</b></td>
				<td id="referralDocCell" align="left" height="10">
				<% if (oscarProperties.getProperty("isMRefDocSelectList", "").equals("true")) {
                                  		// drop down list
									  Properties prop = null;
									  Vector vecRef = new Vector();
                                      List<ProfessionalSpecialist> specialists = professionalSpecialistDao.findAll();
                                      for(ProfessionalSpecialist specialist : specialists) {
                                    	  if (specialist != null && specialist.getReferralNo() != null
												  && ! specialist.getReferralNo().equals("")) {
                                    		  prop = new Properties();
                                    		  prop.setProperty("referral_no", specialist.getReferralNo());
                                          	  prop.setProperty("last_name", specialist.getLastName());
                                          	  prop.setProperty("first_name", specialist.getFirstName());
                                          	  vecRef.add(prop);
                                    	  }
                                      }
                                  %> <select name="r_doctor" id="r_doctor"
					onChange="changeRefDoc()" style="width: 200px">
					<option value=""></option>
					<%
						for (Object o : vecRef) {
						prop = (Properties) o;
					%>
					<option value="<%= prop.getProperty("last_name") + "," + prop.getProperty("first_name") %>">
						<%= Misc.getShortStr((prop.getProperty("last_name") + "," + prop.getProperty("first_name")), "", nStrShowLen) %>
					</option>
					<% } %>
				</select>
<script language="Javascript">
<!--
function changeRefDoc() {
var refName = document.forms[1].r_doctor.options[document.forms[1].r_doctor.selectedIndex].value;
var refNo = "";
  	<%
  	for (Object o: vecRef) {
  		prop = (Properties) o;
  	%>
	if (refName.indexOf("<%= prop.getProperty("last_name") + "," + prop.getProperty("first_name")%>") >= 0) {
		refNo = <%=prop.getProperty("referral_no", "")%>;
	}
	<% } %>
document.forms[1].r_doctor_ohip.value = refNo;
}
//-->
</script>
					<% } else { %>
						<input type="hidden" name="r_doctor_id" size="17" maxlength="40" value="">  
						<input type="text" name="r_doctor" size="17" maxlength="40" value="">
						<a href="javascript:referralScriptAttach2('r_doctor_ohip','r_doctor', 'r_doctor_id', 'name')"><bean:message key="demographic.demographiceditdemographic.btnSearch"/> Name</a>
				<% } %>
				</td>
				<td id="referralDocNoLbl" align="right" nowrap height="10"><b><bean:message
					key="demographic.demographicaddrecordhtm.formReferalDoctorN" />:</b></td>
				<td id="referralDocNoCell" align="left" height="10"><input type="text"
					name="r_doctor_ohip" maxlength="6"> <% if("ON".equals(prov)) { %>
								<a
									href="javascript:referralScriptAttach2('r_doctor_ohip','r_doctor', 'r_doctor_id', 'number')"><bean:message key="demographic.demographiceditdemographic.btnSearch"/>
								#</a> <% } %>
				</td>
			</tr>
	<% if (oscarProperties.isPropertyActive("show_referral_date")) { %>
			<tr>
				<td align="right">
					<b>Referral Date:</b>
				</td>
				<td>
					<input type="text" name="referral-date" id="referral-date" value=""/>
					<img src="../images/cal.gif" id="referral-date-calendar">
					<script type="application/javascript">
                        createStandardDatepicker(jQuery_3_1_0('#referral-date'), "referral-date-calendar");
					</script>
				</td>
			</tr>
	<% } %>
            <tr valign="top">
                <td align="right" height="10"><b><bean:message
                        key="demographic.demographicaddrecordhtm.formFamilyDoctor" />:</b></td>
                <td align="left" height="10">
                    <% if (oscarProperties.getProperty("isMRefDocSelectList", "").equals("true")) {
                        // drop down list
                        Properties prop = null;
                        Vector vecRef = new Vector();

                        List<ProfessionalSpecialist> specialists = professionalSpecialistDao.findAll();
                        for(ProfessionalSpecialist specialist : specialists) {
                            prop = new Properties();
                            prop.setProperty("referral_no", specialist.getReferralNo());
                            prop.setProperty("last_name", specialist.getLastName());
                            prop.setProperty("first_name", specialist.getFirstName());
                            vecRef.add(prop);
                        }
                    %> <select name="f_doctor"
                               onChange="changeRefDoc()" style="width: 200px">
                    <option value=""></option>
                    <%
						for (Object o : vecRef) {
						prop = (Properties) o;
					%>
					<option value="<%= prop.getProperty("last_name") + "," + prop.getProperty("first_name") %>">
						<%= Misc.getShortStr((prop.getProperty("last_name") + "," + prop.getProperty("first_name")), "", nStrShowLen) %>
					</option>
					<% } %>
                </select> <script language="Javascript">
                    <!--
                    function changeFamDoc() {
                        var famName = document.forms[1].r_doctor.options[document.forms[1].f_doctor.selectedIndex].value;
                        var famNo = "";
                        <% for(int k=0; k<vecRef.size(); k++) {
                            prop= (Properties) vecRef.get(k);
                        %>
                        if(famName.indexOf("<%=prop.getProperty("last_name")+","+prop.getProperty("first_name")%>")>=0) {
                            famNo = <%=prop.getProperty("referral_no", "")%>;
                        }
                        <% } %>
                        document.forms[1].f_doctor_ohip.value = famNo;
                    }
                    //-->
                </script>
					<% } else { %>
						<input type="hidden" name="f_doctor_id" size="17" maxlength="40" value="">
						<input type="text" name="f_doctor" size="17" maxlength="40" value="">
						<a href="javascript:referralScriptAttach2('f_doctor_ohip','f_doctor', 'f_doctor_id', 'name')">
							<bean:message key="demographic.demographiceditdemographic.btnSearch"/> Name
						</a>
					<% } %>
                </td>
                <td align="right" nowrap height="10"><b><bean:message
                        key="demographic.demographicaddrecordhtm.formFamilyDoctorN" />:</b></td>
                <td align="left" height="10">
					<input type="text" name="f_doctor_ohip" maxlength="6">
					<% if ("ON".equals(prov)) { %>
						<a href="javascript:referralScriptAttach2('f_doctor_ohip','f_doctor','f_doctor_id')">
							<bean:message key="demographic.demographiceditdemographic.btnSearch"/> #
						</a>
					<% } %>
                </td>
            </tr>
            <script>
                function toggleSameAsMRP() {
                    jQuery_3_1_0('#enrollmentProvider').val(jQuery_3_1_0('#provider_no').val());
                }
            </script>
            <tr valign="top">
                <td align="right">
                    <b><bean:message key="demographic.demographiceditdemographic.formEnrollmentDoctor" />: </b>
                </td>
                <td align="left">
                    <select name="enrollmentProvider" id="enrollmentProvider" style="width: 200px">
                        <option value=""></option>
                        <%
                            for(Provider p : providerDao.getActiveProvidersByRole("doctor")) {
                        %>
                        <option value="<%=p.getProviderNo()%>">
                            <%=Misc.getShortStr(Encode.forHtmlContent(p.getLastName() + "," + p.getFirstName()), "", nStrShowLen)%>
                        </option>
                        <% } %>
                    </select>
                    <input name="enrollmentProvider" id="hiddenEnrollmentProvider" type="hidden" value=""/>
                    <input type="button" value="Same as MRP" onclick="toggleSameAsMRP(this)" />
                </td>
            </tr>
			<tr valign="top">
				<td align="right" id="rosterStatusLbl" nowrap><b><bean:message
					key="demographic.demographicaddrecordhtm.formPCNRosterStatus" />: </b></td>
				<td id="rosterStatus" align="left"><!--input type="text" name="roster_status" onBlur="upCaseCtrl(this)"-->
				<select id="roster_status"  name="roster_status" style="width: 160">
					<option value=""></option>
					<option value="RO"><bean:message key="demographic.demographicaddrecordhtm.EN-Enrolled" /></option>
					<option value="NR"><bean:message key="demographic.demographicaddrecordhtm.NE-NotEnrolled" /></option>
					<option value="TE"><bean:message key="demographic.demographicaddrecordhtm.TE-terminated" /></option>
					<option value="FS"><bean:message key="demographic.demographicaddrecordhtm.FS-feeforservice" /></option>
					<option value="UHIP"><bean:message key="demographic.demographicaddrecordhtm.UHIP"/></option>
					<% 
					for (String status : demographicDao.getRosterStatuses()) {%>
					<option value="<%=status%>"><%=status%></option>
					<% } %>
				</select> <input type="button" onClick="newStatus1();" value="<bean:message
					key="demographic.demographicaddrecordhtm.AddNewRosterStatus"/> " /></td>
				<td id="rosterDateLbl" align="right" nowrap><b><bean:message
					key="demographic.demographicaddrecordhtm.formPCNDateJoined" />: </b></td>
				<td id="rosterDateCell" align="left">					
					<input type="text" name="roster_date" id="roster_date">
					<img src="../images/cal.gif" id="roster_date_cal">
					<script type="application/javascript">createStandardDatepicker(jQuery_3_1_0('#roster_date'), "roster_date_cal");</script>
				</td>
			</tr>
			<tr valign="top">
				<td id="chartNoLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formChartNo" />:</b></td>
				<td id="chartNo" align="left"><input type="text" id="chart_no" name="chart_no" value="<%=StringEscapeUtils.escapeHtml(chartNoVal)%>">
				</td>
			</tr>
			<%
				ReferralSourceDao referralSourceDao = SpringUtils.getBean (ReferralSourceDao.class);
				List<ReferralSource> referralSourceList = referralSourceDao.getReferralSourceList();
				for (int i = referralSourceList.size()-1; i >= 0; i--) {
					if (referralSourceList.get(i).getArchiveStatus()) {
						referralSourceList.remove(i);
					}
				}
				boolean enableRefSources = false;
				List<SystemPreferences> preferences = SystemPreferencesUtils.findPreferencesByNames(SystemPreferences.REFERRAL_SOURCE_PREFERENCE_KEYS);
				for(SystemPreferences preference : preferences) {
					if (preference.getValue() != null) {
						if (preference.getName().equals("enable_referral_source")) {
							enableRefSources = Boolean.parseBoolean(preference.getValue());
						}
					}
				}//Copying code from masterEdit to masterAdd p'much. Shit looks weird atm, other categorey is weird
			%>
			<tr valign="top" <%=enableRefSources ? "" : "style=\"display: none;\""%>>
				<td align="right"><b>Patient Discovered Clinic VIA:</b></td>
				<td align="left">
			<span style="float: left;"><select name="referralSource" onChange='referralSourceIsOther(this.value);' style="width: 200px">
			<option value="" selected disabled hidden>Choose here</option>
				<%
				for (ReferralSource refSourceName : referralSourceList) {
			%>
			<option value="<%=refSourceName.getReferralSource()%>"><%=refSourceName.getReferralSource()%></option>
			<% } %>
			<option value="Other">Other</option>
			</select></span>
			<span style="float: left;"><input type="text" name="referralSourceCust" id="referralSourceCust" style="visibility: visible;" maxlength="200"></span>
			</td>
			</tr>
    <tr valign="top">
        <td id="phuLbl" align="right"><b><bean:message
                key="demographic.demographicaddrecordhtm.formPHU" />:</b></td>
        <td id="phuLblCell" align="left">
            <select id="PHU" name="PHU" >
                <option value="">Select Below</option>
                <%
                    String defaultPhu = oscarProperties.getProperty("default_phu");
                    LookupListManager lookupListManager = SpringUtils.getBean(LookupListManager.class);
                    LookupList ll = lookupListManager.findLookupListByName(LoggedInInfo.getLoggedInInfoFromSession(request), "phu");
if(ll != null) {
                    for(LookupListItem llItem : ll.getItems()) {
                        String selected = "";
                        if(llItem.getValue().equals(defaultPhu)) {
                            selected = " selected=\"selected\" ";
                        }
                %>
                <option value="<%=llItem.getValue()%>" <%=selected%>><%=llItem.getLabel()%></option><%
							}
						} else {
							%>
							<option value="">None Available</option>
                <% } %>
            </select>
        </td>
        <td align="right">&nbsp;
        </td>
    </tr>
	<%
		if (oscarProperties.getProperty("EXTRA_DEMO_FIELDS") != null) {
      		String fieldJSP = oscarProperties.getProperty("EXTRA_DEMO_FIELDS") + ".jsp";
    %>
			<jsp:include page="<%=fieldJSP%>" />
	<%
		}
		if (enableMeditechId) { %>
                         <tr valign="top">
                             <td align="right"><b>Meditech ID:</b></td>
                             <td align="left"><input type="text" name="meditech_id" value=""></td>
                             <td align="right"><b>&nbsp;</b></td>
                             <td align="left">&nbsp;</td>
                         </tr>
	<%
		}
        String wLReadonly = "";
        WaitingList wL = WaitingList.getInstance();
        if (!wL.getFound()) {
            wLReadonly = "readonly";
		}
    %>
			<tr>
				<td id="waitListTbl" colspan="4">
				<table border="1" width="100%">
					<tr valign="top">
                                            <td align="right" width="15%" nowrap><b> <bean:message key="demographic.demographicaddarecordhtm.msgWaitList"/>: </b></td>
						<td align="left" width="38%"><select id="name_list_id" name="list_id">
							<% if (wLReadonly.equals("")) { %>
							<option value="0">--Select Waiting List--</option>
							<% } else { %>
							<option value="0"><bean:message key="demographic.demographicaddarecordhtm.optCreateWaitList"/>
							</option>
							<% } %>
							<%
							List<WaitingListName> waitLists;
							if (oscarProperties.getBooleanProperty("show_all_wait_lists", "true")) {
								waitLists = waitingListNameDao.getAllActiveWaitLists();
							} else {
								waitLists = waitingListNameDao.findCurrentByGroup(((ProviderPreference)session.getAttribute(SessionConstants.LOGGED_IN_PROVIDER_PREFERENCE)).getMyGroupNo());
							}
							for (WaitingListName wln : waitLists) { %>
								<option value="<%= wln.getId() %>"><%= wln.getName() %></option>
							<% } %>
						</select></td>
						<td align="right" nowrap><b><bean:message key="demographic.demographicaddarecordhtm.msgWaitListNote"/>: </b></td>
						<td align="left"><input type="text" id="waiting_list_note" name="waiting_list_note"
							size="36" <%=wLReadonly%>></td>
					</tr>
					<tr>
						<td colspan="2" align="right">&nbsp;</td>
						<td align="right" nowrap><b><bean:message key="demographic.demographicaddarecordhtm.msgDateOfReq"/>:</b></td>
						<td align="left">
							<input type="text" name="waiting_list_referral_date" id="waiting_list_referral_date"
								value="" size="12" <%=wLReadonly%>>
							<img src="../images/cal.gif" id="referral_date_cal">
							<script type="application/javascript">createStandardDatepicker(jQuery_3_1_0('#waiting_list_referral_date'), "referral_date_cal");</script>
						</td>
					</tr>
				</table>
				</td>
			</tr>
<%-- TOGGLE PRIVACY CONSENT MODULE --%>
<% if (privateConsentEnabled) { %>
			<%
				String[] privateConsentPrograms2 =
						oscarProperties.getProperty("privateConsentPrograms","").split(",");
				ProgramProvider pp3 = programManager2.getCurrentProgramInDomain(loggedInInfo,loggedInInfo.getLoggedInProviderNo());
				boolean showConsents = false;
				if(pp3 != null) {
					for (String s : privateConsentPrograms2) {
						if (s.equals(pp3.getProgramId().toString())) {
							showConsents = true;
							break;
						}
					}
				}
			if (showConsents) { %>
			<!-- consents -->
			<tr valign="top">
				<td colspan="4">
					<input type="checkbox" name="privacyConsent" value="yes"><b>Privacy Consent (verbal) Obtained</b> 
					<br/>
					<input type="checkbox" name="informedConsent" value="yes"><b>Informed Consent (verbal) Obtained</b>
					<br/>
				</td>
		  	</tr>
			<% } %>
		<oscar:oscarPropertiesCheck property="USE_NEW_PATIENT_CONSENT_MODULE" value="true" >
				<c:forEach items="${ consentTypes }" var="consentType" varStatus="count">
					<c:set var="patientConsent" value="" />
					<c:forEach items="${ patientConsents }" var="consent" >
						<c:if test="${ consent.consentType.id eq consentType.id }">
							<c:set var="patientConsent" value="${ consent }" />
						</c:if>													
					</c:forEach>
					<tr class="privacyConsentRow" id="${ count.index }" >
						<td class="alignRight" style="width:16%;vertical-align:top;">
							<div style="font-weight:bold;white-space:nowrap;" >
								<c:out value="${ consentType.name }" />
							</div>
						</td>
						<td colspan="2" style="padding-left:10px;vertical-align:top;">
							<c:out value="${ consentType.description }" />
						</td>
						<td id="consentStatusDate" style="width:31%;vertical-align:top;">
                            <input type="radio"
                                   name="${ consentType.type }"
                                   id="optin_${ consentType.type }"
                                   value="0"
                            />
                            <label for="optin_${ consentType.type }" >Opt-In</label>
                            <input type="radio"
                                   name="${ consentType.type }"
                                   id="optout_${ consentType.type }"
                                   value="1"                         
                            />
                            <label for="optout_${ consentType.type }" >Opt-Out</label>
                            <input type="button"
                                   name="clearRadio_${consentType.type}_btn"
                                   onclick="consentClearBtn('${consentType.type}')" value="Clear" />
						</td>
					</tr>
				</c:forEach>
		</oscar:oscarPropertiesCheck>
<% } %>
			<tr valign="top">
				<td id="joinDateLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formDateJoined" /></b><b>:
				</b></td>
				<td id="joinDateCell" align="left">
					<input type="text" name="date_joined" id="date_joined" value="<%=standardDateFormat.format(now.getTime())%>">
					<img src="../images/cal.gif" id="date_joined_cal">
					<script type="application/javascript">createStandardDatepicker(jQuery_3_1_0('#date_joined'), "date_joined_cal");</script>
				</td>
				<td id="endDateLbl" align="right"><b><bean:message
					key="demographic.demographicaddrecordhtm.formEndDate" /></b><b>: </b></td>
				<td id="endDateCell" align="left">
					<input type="text" name="end_date" id="end_date">
					<img src="../images/cal.gif" id="end_date_cal">
					<script type="application/javascript">createStandardDatepicker(jQuery_3_1_0('#end_date'), "end_date_cal");</script>
			</tr>
		<oscar:oscarPropertiesCheck property="DEMOGRAPHIC_PROGRAM_ADMISSIONS" value="true">
			<tr valign="top">
			    <td colspan="4">
			        <table border="1" width="100%">
			            <tr bgcolor="#CCCCFF">
			                <td colspan="2" >Program Admissions</td>
			            </tr>
			            <tr>
			                <td>Residential Status<font color="red">:</font></td>
			                <td>Service Programs</td>
			            </tr>
			            <tr>
			                <td>
                                <select id="rsid" name="rps">
                                    <%
                                        GenericIntakeEditAction gieat = new GenericIntakeEditAction();
                                        gieat.setProgramManager(pm);
                                        String _pvid = loggedInInfo.getLoggedInProviderNo();
                                        Set<Program> pset = gieat.getActiveProviderProgramsInFacility(loggedInInfo,_pvid,loggedInInfo.getCurrentFacility().getId());
                                        List<Program> bedP = gieat.getBedPrograms(pset,_pvid);
                                        List<Program> commP = gieat.getCommunityPrograms();
                                        for(Program _p:bedP){
                                    %>
                                        <option value="<%=_p.getId()%>" <%=("OSCAR".equals(_p.getName())?" selected=\"selected\" ":"")%> ><%=_p.getName()%></option>
                                    <% } %>
                                </select>
			                </td>
			                <td>
			                    <%
			                        List<Program> servP = gieat.getServicePrograms(pset,_pvid);
			                        for(Program _p:servP){
			                    %>
			                        <input type="checkbox" name="sp" value="<%=_p.getId()%>"/><%=_p.getName()%><br/>
			                    <%}%>
			                </td>
			            </tr>
			        </table>
			    </td>
			</tr>
		</oscar:oscarPropertiesCheck>
	<%
		//"Has Primary Care Physician" & "Employment Status" fields
		final String hasPrimary = "Has Primary Care Physician";
		final String empStatus = "Employment Status";
		boolean hasHasPrimary = oscarProperties.isPropertyActive("showPrimaryCarePhysicianCheck");
		boolean hasEmpStatus = oscarProperties.isPropertyActive("showEmploymentStatus");
		String hasPrimaryCarePhysician = "N/A";
		String employmentStatus = "N/A";
		if (hasHasPrimary || hasEmpStatus) {
		%>
		<tr valign="top" bgcolor="#CCCCFF">
		<% if (hasHasPrimary) { %>
			<td><b><%= hasPrimary.replace(" ", "&nbsp;") %>:</b></td>
								<td>
									<select name="<%= hasPrimary.replace(" ", "") %>">
										<option value="N/A" <%="N/A".equals(hasPrimaryCarePhysician)?"selected":""%>>N/A</option>
										<option value="Yes" <%="Yes".equals(hasPrimaryCarePhysician)?"selected":""%>>Yes</option>
										<option value="No" <%="No".equals(hasPrimaryCarePhysician)?"selected":""%>>No</option>
									</select> 
								</td>
		<%
			}
			if (hasEmpStatus) {
		%>
			<td><b><%= empStatus.replace(" ", "&nbsp;") %>: </b></td>
								<td>
									<select name="<%= empStatus.replace(" ", "") %>">
										<option value="N/A" <%="N/A".equals(employmentStatus)?"selected":""%>>N/A</option>
										<option value="FULL TIME" <%="FULL TIME".equals(employmentStatus)?"selected":""%>>FULL TIME</option>
										<option value="ODSP" <%="ODSP".equals(employmentStatus)?"selected":""%>>ODSP</option>
										<option value="OW" <%="OW".equals(employmentStatus)?"selected":""%>>OW</option>
										<option value="PART TIME" <%="PART TIME".equals(employmentStatus)?"selected":""%>>PART TIME</option>
										<option value="UNEMPLOYED" <%="UNEMPLOYED".equals(employmentStatus)?"selected":""%>>UNEMPLOYED</option>
									</select> 
								</td>
							</tr>
		<%
			}
		}
		//customized key
		if (oscarProperties.getProperty("demographicExt") != null) {
		boolean bExtForm = oscarProperties.getProperty("demographicExtForm") != null;
		String [] propDemoExtForm = bExtForm
				? (oscarProperties.getProperty("demographicExtForm","").split("\\|") )
				: null;
		String [] propDemoExt = oscarProperties.getProperty("demographicExt","").split("\\|");
		for (int k = 0; k < propDemoExt.length; k = k + 2) { %>
			<tr valign="top" bgcolor="#CCCCFF">
				<td align="right"><b><%= propDemoExt[k] %></b><b>: </b></td>
				<td align="left">
				<%
					if (bExtForm) {
						out.println(propDemoExtForm[k] );
					} else {
				%>
					<input type="text" name="<%= propDemoExt[k].replace(' ', '_') %>" value="">
				<% } %>
				</td>
				<td align="right">
					<%= (k + 1) < propDemoExt.length ? ("<b>"+propDemoExt[k+1]+": </b>") : "&nbsp;" %>
				</td>
				<td align="left">
				<%
					if (bExtForm && (k + 1) < propDemoExt.length) {
						out.println(propDemoExtForm[k + 1]);
					} else {
				%>
					<%= (k + 1)<propDemoExt.length ? "<input type=\"text\" name=\"" + propDemoExt[k + 1].replace(' ', '_')+"\"  value=''>" : "&nbsp;" %>
				<% }  %>
				</td>
			</tr>
			<%
			}
		}
		if (oscarProperties.getProperty("demographicExtJScript") != null) {
			out.println(oscarProperties.getProperty("demographicExtJScript"));
		}
	%>
			<tr>
				<td colspan="4">
				<table width="100%" bgcolor="#EEEEFF">
          <tr>
            <td width="7%" align="right">
							<label for="patientType">
								<bean:message key="demographic.demographiceditdemographic.formPatientType"/>:
							</label>
						</td>
  						<td>
								<input type="hidden" name="patientTypeOrig"/>
								<select id="patientType" name="patientType" onchange="this.form.patientTypeOrig.value=this.value">
									<option value="NotSet">Not Specified</option>
							<%	    PatientTypeDao patientTypeDao=(PatientTypeDao)SpringUtils.getBean("patientTypeDao");
                                		List<PatientType> patientTypes = patientTypeDao.findAllPatientTypes();
                                		for (PatientType thisPatientType : patientTypes) { %>				
                                			<option value="<%=thisPatientType.getType() %>"><%=thisPatientType.getDescription()%></option>		
                               <% } %>
                	</select>
              </td>
  						<td align="right">
  							<label for="patient-id">
  								<bean:message key="demographic.demographiceditdemographic.formDemographicMiscId"/>:
  							</label>
  						</td>
  						<td>
  							<input type="text" id="patient-id" name="patientId" value=""/>
              </td>
          </tr>
          <tr>
						<td width="7%" align="right">
							<label for="demographicGroups"> <bean:message key="demographic.demographiceditdemographic.formDemographicGroups"/>: </label>
						</td>
						<td>
							<select multiple size="4" id="demographicGroups" name="demographicGroups" style="vertical-align:top;">
							<option value="" selected> None </option>
							<% for (DemographicGroup dg : demographicGroups) { %>
								<option value="<%= dg.getId() %>"> <%= dg.getName() %> </option>
							<% } %>
							</select>
 						</td>
          </tr>
					<tr>
						<td id="alertLbl" width="10%" align="right"><font color="#FF0000"><b><bean:message
							key="demographic.demographicaddrecordhtm.formAlert" />: </b></font></td>
						<td id="alertCell"><textarea id="booking_alert" name="bookingAlert" style="width: 100%" rows="2"></textarea>
						</td>
					</tr>
					<tr>
						<td width="7%" align="right"><font color="#FF0000"><b> Chart Alert: </b></font></td>
						<td>
							<textarea name="chartAlertText" style="width: 100%" cols="80" rows="2" maxlength="200"></textarea>
						</td>
					</tr>
					<tr>
						<td id="notesLbl" align="right"><b><bean:message
							key="demographic.demographicaddrecordhtm.formNotes" /> : </b></td>
						<td id="notesCell"><textarea id="content" name="notes" style="width: 100%" rows="2"></textarea>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<% if (org.oscarehr.common.IsPropertiesOn.isMultisitesEnable()) { %>
			<tr>
				<td>
				<div align="left"><b><bean:message key="admin.provider.sitesAssigned" />:</b></div>
				</td>		
			</tr>
			<tr>
				<td>
				<% 
				SiteDao siteDao = (SiteDao) SpringUtils.getBean("siteDao");	   
				List<Site> sites = siteDao.getActiveSitesByProviderNo(curUser_no);
				for (Site site : sites) {
				%>
					<input type="checkbox" name="sites" value="<%= site.getSiteId() %>"><%=
					site.getName() %><br/>
					<% } %>
				</td>
			</tr>
			<% } %>
			<tr>
			    <td colspan="4">
			        <div>
<%
    Facility facility = loggedInInfo.getCurrentFacility();
    Integer fid = null;
    if(facility!=null) fid = facility.getRegistrationIntake();
    if(fid==null||fid<0){
        List<EForm> eforms = eformDao.getEfromInGroupByGroupName("Registration Intake");
        if(eforms!=null&&eforms.size()==1) fid=eforms.get(0).getId();
    }
    if(fid!=null&&fid>=0){
%>
<iframe scrolling="no" id="eform_iframe" name="eform_iframe" frameborder="0" src="../eform/efmshowform_data.jsp?fid=<%=fid%>" onload="this.height=0;var fdh=(this.Document?this.Document.body.scrollHeight:this.contentDocument.body.offsetHeight);this.height=(fdh>800?fdh:800)" width="100%"></iframe>
<% } %>
			        </div>
			    </td>
			</tr>
			<tr bgcolor="#CCCCFF">
				<td colspan="4">
				<div align="center"><input type="hidden" name="dboperation"
					value="add_record"> <input type="hidden" name="displaymode" value="Add Record">
				<input type="submit" id="btnAddRecord" name="btnAddRecord" 
					value="<bean:message key="demographic.demographicaddrecordhtm.btnAddRecord"/>">
				<input type="submit" name="submit" value="Save & Add Family Member">
				<input type="button" id="btnSwipeCard" name="Button"
					value="<bean:message key="demographic.demographicaddrecordhtm.btnSwipeCard"/>"
					onclick="window.open('zadddemographicswipe.htm','', 'scrollbars=yes,resizable=yes,width=600,height=300')";>
				&nbsp; 
				<caisi:isModuleLoad moduleName="caisi" reverse="true">
				<input type="button" name="closeButton"
					value="<bean:message key="demographic.demographicaddrecordhtm.btnCancel"/>"
					onclick="self.close();">
				</caisi:isModuleLoad>				
				</div>
				&nbsp; 				
				<caisi:isModuleLoad moduleName="caisi" reverse="true">
				<input type="button" name="closeButton"
					value="<bean:message key="demographic.demographicaddrecordhtm.btnCancel"/>"
					onclick="self.close();">
				</caisi:isModuleLoad>
				<caisi:isModuleLoad moduleName="caisi">
				<input type="button" name="closeButton"
					value="<bean:message key="demographic.demographicaddrecordhtm.btnReturnToSchedule"/>"
					onclick="window.location.href='../provider/providercontrol.jsp';">
				</caisi:isModuleLoad>
				</td>
			</tr>
		</table>
		</form>
		</td>
	</tr>
</table>
*
<font face="Courier New, Courier, mono" size="-1"><bean:message
	key="demographic.demographicaddrecordhtm.formDateFormat" /> </font>
<script type="text/javascript">

<% if (privateConsentEnabled) { %>
jQuery(document).ready(function(){
	var countryOfOrigin = jQuery("#countryOfOrigin").val();
	if("US" != countryOfOrigin) {
		jQuery("#usSigned").hide();
	} else {
		jQuery("#usSigned").show();
	}
	
	jQuery("#countryOfOrigin").change(function () {
		var countryOfOrigin = jQuery("#countryOfOrigin").val();
		if("US" == countryOfOrigin){
		   	jQuery("#usSigned").show();
		} else {
			jQuery("#usSigned").hide();
		}
	});
});
<% } %>
</script>
</body>
</html:html>
