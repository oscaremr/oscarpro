<%@page import="org.oscarehr.common.service.TeleplanAutomateService"%>
<%@page import="java.util.Properties"%>
<%@page import="oscar.util.OscarRoleObjectPrivilege"%>
<%@page import="org.apache.commons.lang3.time.DateFormatUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="oscar.OscarProperties"%>
<%@page import="org.oscarehr.billing.CA.BC.model.TeleplanAlert"%>
<%@page import="java.util.List"%>

<link rel="stylesheet" href="resource/tasks-status.css?v=2" type="text/css">
<script type="text/javascript" src="resource/tasks-status.js?v=2"></script>
<script type="text/javascript">
  window.systemAPIContext = '<%=request.getContextPath()%>';
</script>

<%
TeleplanAutomateService service = new TeleplanAutomateService();
Map resultMap = service.getStatusInfo();
Map taskNameMap = service.getTaskNameMap();
List allAlerts = (List) resultMap.get("alerts");
%>

<%
String roleName$ = (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
%>

<div id="globalTASKSStatus" style="display: none;">
  <span class="tasks-status <%=resultMap.get("statusClass")%>">TASKS - <%=resultMap.get("statusLabel")%></span>
</div>

<div id="dlgTASKSMessage-backdrop"></div>
<div tabindex="-1" role="dialog" id="dlgTASKSMessage">
  <div role="document">
    <div class="tasks-content">
      <div class="tasks-header">
        <h4>TASKS Status Detail</h4>
      </div>
      <div class="tasks-body" style="padding: 20px 0;">
        <%if (resultMap.get("statusCode").equals("run_has_error")) {%>
        <table border style="border-collapse: collapse;">
          <thead>
            <tr>
              <th width="170px">Automate Task</th>
              <th width="120px;">Execute Time</th>
              <th width="64px">Success</th>
              <th>Result</th>
              <th>Message</th>
            </tr>
          </thead>
          <tbody>
            <%
            for (int i = 0; i < allAlerts.size(); i++) {
            	TeleplanAlert alert = (TeleplanAlert) allAlerts.get(i);
            %>
                <tr class="<%=(alert.isSuccess() ? "success" : "error")%>">
                  <td><%=taskNameMap.get(alert.getJobName())%></td>
                  <td><%=DateFormatUtils.format(alert.getCreateTime(), "dd/MM/yyyy HH:mm:ss")%></td>
                  <td><%=alert.isSuccess() ? "Yes" : "No"%></td>
                  <td><%=alert.getResult()%></td>
                  <td><%=alert.getMsg()%></td>
                </tr>
            <%}%>
          </tbody>
        </table>
        <%} else {%>

          <%if (resultMap.get("statusCode").equals("all_task_active")) {%>
          <p class="tasks-text-success">All automated tasks are running.</p>
          <%}%>
          
          <%if (resultMap.get("statusCode").equals("all_conf_disabled")) {%>
          <p>Teleplan Automate and Excelleris Download features are disabled.</p>
          <%}%>
          
          <%if (resultMap.get("statusCode").equals("teleplan_conf_disabled")) {%>
          <p>Teleplan Automate feature is disabled.</p>
          <%}%>
          
          <%if (resultMap.get("statusCode").equals("excelleris_conf_disabled")) {%>
          <p>Excelleris Download feature is disabled.</p>
          <%}%>
  
          <%if (resultMap.get("statusCode").equals("all_task_deactive")) {%>
          <p>All automated tasks are deactivated.</p>
          <%}%>
          
          <%if (resultMap.get("statusCode").equals("some_task_active")) {%>
          <p>Following automated tasks are deactivated: <br><%=service.generateTaskNames((List)resultMap.get("deactivaedTasks")) %></p>
          <%}%>
        
        <%}%>
      </div>
      <div class="tasks-footer">
        <%if (resultMap.get("statusCode").equals("run_has_error")) {%>
          <%
          String roleName = (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
          ArrayList v = new ArrayList(OscarRoleObjectPrivilege.getPrivilegeProp("_admin.clear_teleplan_alert"));
          %>
          <%if (OscarRoleObjectPrivilege.checkPrivilege(roleName, (Properties) v.get(0), (List<String>) v.get(1),
            		(List<String>) v.get(2), "r")) {%>
          <button type="button" class="btn btn-primary" style="margin-right: 20px;">Clear All Error(s)</button>
          <%}%>
        <%}%>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>