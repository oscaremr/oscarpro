<%--
Copyright (c) 2022 WELL EMR Group Inc. This software is made available under the terms of the GNU
General Public License, Version 2, 1991 (GPLv2). License details are available via
"gnu.org/licenses/gpl-2.0.html".
--%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html:html locale="true">
  <head>
    <title>Success</title>
    <script src="<%= request.getContextPath() %>/js/global.js"></script>
  </head>
  <body>
  <div style="text-align: center">
    <table style="margin-right: auto; margin-left: auto" border="0" cellspacing="0" cellpadding="0" width="90%">
      <tr bgcolor="#486ebd">
        <th align="CENTER" style="font-family: Helvetica;color: #FFF;">
          Successfully Saved Default Inbox Start Date Range
        </th>
      </tr>
    </table>
    <p></p>
    <hr width="90%"/>
    <form>
      <input type="button" value=<bean:message key="global.btnClose"/> onClick="self.close()">
    </form>
  </div>
  </body>
</html:html>