<%--
Copyright (c) 2022 WELL EMR Group Inc. This software is made available under the terms of the GNU
General Public License, Version 2, 1991 (GPLv2). License details are available via
"gnu.org/licenses/gpl-2.0.html".
--%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ page import="org.oscarehr.common.dao.UserPropertyDAO" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    String userNumber = (String) session.getAttribute("user");
    boolean isFirstLoad = request.getAttribute("status") == null;
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html:html>
  <head>
    <html:base/>
    <title>Set Default Encounter Type</title>
    <link rel="stylesheet" href="../oscarEncounter/encounterStyles.css">
    <script src="<%= request.getContextPath() %>/js/global.js"></script>
    <script src="<%=request.getContextPath()%>/JavaScriptServlet"></script>
  </head>
    <%
        UserPropertyDAO propDao = SpringUtils.getBean(UserPropertyDAO.class);
        String defaultEncounterType =
                propDao.getStringValue(userNumber, "default_encounter_type") != null
                        ? propDao.getStringValue(userNumber, "default_encounter_type")
                        : "";
        List<String> options = new ArrayList<String>();
        options.add("");
        options.add("face to face encounter with client");
        options.add("telephone encounter with client");
        options.add("email encounter with client");
        options.add("video encounter with client");
        options.add("encounter without client");
    %>
  <body class="BodyStyle" <%=isFirstLoad ? "onload='update()'" : ""%>>
    <table class="MainTable" name="encounterTable">
      <tr class="MainTableTopRow">
        <td class="MainTableTopRowLeftColumn">Preference</td>
        <td style="color: white" class="MainTableTopRowRightColumn">Encounter Type</td>
      </tr>
      <tr>
        <td class="MainTableLeftColumn">&nbsp;</td>
        <td class="MainTableRightColumn">
          Please select a default encounter type:
          <html:form action="/SetEncounterType.do">
            <html:select property="encounterType">
              <% for (String option : options) { %>
                <option value="<%= option %>" <%= defaultEncounterType.equals(option)
                      ? "selected" : "" %>>
                    <%= option %>
                </option>
              <% } %>
              <input type="submit" value="Save"/>
            </html:select>
          </html:form>
        </td>
      </tr>
      <tr>
        <td class="MainTableBottomRowLeftColumn"></td>
        <td class="MainTableBottomRowRightColumn"></td>
      </tr>
    </table>
  </body>
</html:html>
