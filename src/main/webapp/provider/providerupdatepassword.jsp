<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%
	if(session.getValue("user") == null)
    response.sendRedirect("../logout.jsp");
  String curUser_no = (String) session.getAttribute("user");
%>

<%@ page
	import="java.lang.*, java.util.*, java.text.*,java.security.*, oscar.*"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.Security" %>
<%@ page import="org.oscarehr.common.dao.SecurityDao" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.myoscar.utils.MyOscarLoggedInInfo" %>
<%@ page import="oscar.oscarProvider.data.ProviderMyOscarIdData" %>
<%@ page import="org.oscarehr.phr.util.MyOscarUtils" %>

<%
	LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);

%>
<%@ page import="oscar.log.LogAction" %>
<%@ page import="oscar.log.LogConst" %>
<%@ page import="org.oscarehr.admin.web.OktaUserHelper" %>
<%@ page import="org.oscarehr.util.rest.StatusResponse" %>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="org.oscarehr.managers.OktaManager" %>
<%@ page import="org.oscarehr.util.MiscUtils" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="ca.oscarpro.security.OscarPasswordService" %>
<%@ page import="ca.oscarpro.security.PasswordValidationException" %>

<%
	Logger logger = MiscUtils.getLogger();
	OktaUserHelper oktaUserHelper = new OktaUserHelper();
	SecurityDao securityDao = SpringUtils.getBean(SecurityDao.class);
	OktaManager oktaManager = SpringUtils.getBean(OktaManager.class);

	List<Security> ss = securityDao.findByProviderNo(curUser_no);
	for(Security s:ss) {

		boolean pinUpdateRequired = false;
		String errorMsg = "";
		//check if the user will change the  PIN
		if (request.getParameter("pin") != null
				&& request.getParameter("pin").length() > 0
				&& request.getParameter("newpin") != null
				&& request.getParameter("newpin").length() > 0
				&& request.getParameter("confirmpin") != null
				&& request.getParameter("confirmpin").length() > 0) {

			String pin = request.getParameter("pin");
			String newPin = request.getParameter("newpin");
			String confPin = request.getParameter("confirmpin");

			if (!pin.equals(s.getPin())) {
				errorMsg = "PIN Update Error: PIN doesn't match the existing one in the system. ";
			} else if (!newPin.equals(confPin)) {
				errorMsg = "PIN Update Error: New PIN doesn't match the Confirm PIN. ";
			} else if (newPin.equals(s.getPin())) {
				errorMsg = "PIN Update Error: New PIN must be different from the existing PIN. ";
			} else {
				pinUpdateRequired = true;
				s.setPin(newPin);
			}
		}

		boolean passwordUpdateRequired = false;
		//check if the user will change the  Password
		String oldPassword = request.getParameter("oldpassword");
		String newPassword = request.getParameter("mypassword");
		String confirmPassword = request.getParameter("confirmpassword");

		if (oldPassword != null && oldPassword.length() > 0
				&& newPassword != null && newPassword.length() > 0
				&& confirmPassword != null && confirmPassword.length() > 0) {

			String strDBpasswd = s.getPassword();
			if (strDBpasswd == null) {
				strDBpasswd = "";  // prevent null pointer
			}


			if (strDBpasswd.length() < 20) {
				strDBpasswd = OscarPasswordService.encodePassword(strDBpasswd);
			}

			if (newPassword.equals(confirmPassword)) {
				if (oktaManager.isOktaEnabled()) {
					// mark that an update is needed, but don't update the Oscar security record
					passwordUpdateRequired = true;
				} else {
					if (OscarPasswordService.isMatch(oldPassword, s)) {
						if (OscarPasswordService.isMatch(newPassword, s)) {
							errorMsg = errorMsg
									+ " Password Update Error: New Password must be different from the existing Password. ";
						} else {
							try {
								OscarPasswordService.validatePassword(newPassword);
								s.setPassword(OscarPasswordService.encodePassword(newPassword));   // change
								OscarPasswordService.setPasswordVersion(s);
								passwordUpdateRequired = true;
							} catch (PasswordValidationException e) {
								errorMsg = errorMsg + " Password Update Error: " + e.getMessage();
							}
						}
					} else {
						errorMsg = errorMsg
								+ " Password Update Error: Password doesn't match the existing password. ";
					}
				}
			}
		} else {
			errorMsg = errorMsg	+ " Password Update Error: New Password Cannot be blank";
		}

		// Attempt to change Okta password, if it fails do not change local password either
		String ip = LoggedInInfo.obtainClientIpAddress(request);

		if (oktaManager.isOktaEnabled()) {
			if (passwordUpdateRequired) {
				logger.debug("###### providerupdatepassword:  changeOktaUserPassword" );
				StatusResponse oktaPasswordStatus = oktaUserHelper.changeOktaUserPassword(s, oldPassword, newPassword, request);
				if (!oktaPasswordStatus.isOk()) {
					passwordUpdateRequired = false;
					errorMsg = StringUtils.trimToEmpty(oktaPasswordStatus.toUserMessage())
							.replaceAll("oldPassword.value", "Current Password")
							.replaceAll("newPassword.value", "New Password");
				} else {
					LogAction.addLog(curUser_no, LogConst.UPDATE, "Password updated.", "", ip);
				}
			}
		} else {

			//Persist it if one of them has gone thru.
			if (passwordUpdateRequired || pinUpdateRequired) {
				securityDao.saveEntity(s);
				logger.debug("###### providerupdatepassword:  oktaEnabled=false, update security with passworde" );
				//Log the action
				LogAction.addLog(curUser_no, LogConst.UPDATE, "Password/PIN update.", "", ip);
			}
		}

		if (ProviderMyOscarIdData.idIsSet(curUser_no)) {
			MyOscarLoggedInInfo.setLoggedInInfo(request.getSession(), null);
			MyOscarUtils.attemptMyOscarAutoLoginIfNotAlreadyLoggedInAsynchronously(loggedInInfo, true);
		}

		//In case of the error for any reason go back.
		if (!errorMsg.isEmpty()) {
			if (passwordUpdateRequired) {
				errorMsg = "Password Update Successfully However, " + errorMsg;
			}
			if (pinUpdateRequired) {
				errorMsg = "PIN Update Successfully However, " + errorMsg;
			}
			response.sendRedirect("providerchangepassword.jsp?errormsg=" + errorMsg);
		}

		out.println("<script language='javascript'>self.close();</script>");
	     
	}
%>