<%--

   Copyright (c) 2021 WELL EMR Group Inc.
   This software is made available under the terms of the
   GNU General Public License, Version 2, 1991 (GPLv2).
   License details are available via "gnu.org/licenses/gpl-2.0.html".

--%>

<%@ taglib uri = "/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri = "/WEB-INF/struts-html.tld" prefix="html" %>
<%@ page import = "org.oscarehr.common.model.UserProperty" %>
<%@ page import = "oscar.util.StringUtils" %>
<%@ page import = "org.oscarehr.util.SpringUtils" %>
<%@ page import = "org.oscarehr.common.dao.UserPropertyDAO" %>

<%
    if (session.getValue("user") == null)
        response.sendRedirect("../logout.htm");
    String curUser_no = (String) session.getAttribute("user");
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html:html>
    <head>
        <script type = "text/javascript" src = "<%= request.getContextPath() %>/js/global.js"></script>
        <html:base/>

        <title>
            <bean:message key = "provider.setHarmonyID"/>
        </title>

        <script src = "<%= request.getContextPath()%>/JavaScriptServlet" type = "text/javascript"></script>

        <link rel = "stylesheet" type = "text/css" href = "../oscarEncounter/encounterStyles.css">

        <script type = "text/javascript">
            function focus() {
                document.forms["setHarmonyIdForm"].harmonyId.focus();
                return true;
            }
        </script>
    </head>

    <body class = "BodyStyle" vlink = "#0000FF">
        <table class = "MainTable" id = "scrollNumber1">
            <tr class = "MainTableTopRow">
                <td class = "MainTableTopRowLeftColumn"><bean:message key = "provider.setColour.msgPrefs"/></td>
                <td style = "color: white" class = "MainTableTopRowRightColumn"><bean:message key = "provider.setHarmonyID"/></td>
            </tr>
            <tr>
                <td class = "MainTableLeftColumn">&nbsp;</td>
                <td class = "MainTableRightColumn"><html:errors/>
                <%
                    UserPropertyDAO userPropertyDAO = (UserPropertyDAO)SpringUtils.getBean("UserPropertyDAO");
                    String harmonyId = StringUtils.noNull(userPropertyDAO.getStringValue(curUser_no, UserProperty.HARMONY_ID));
                    if (request.getAttribute("status") == null) {
                %>
                    <html:form action = "/setHarmonyId.do">
                        <bean:message key = "provider.setHarmonyID.msgEdit"/>&nbsp;&nbsp;
                        <html:text property = "harmonyId" value = "<%=harmonyId%>" size = "20"/>
                        <br>
                        <input type = "submit" onclick = "return focus();" value = "<bean:message key = "global.btnSubmit"/>"/>
                    </html:form>
                <%
                    } else if (((String) request.getAttribute("status")).equals("complete")) {
                %>
                    <bean:message key = "provider.setHarmonyID.msgSuccess"/>&nbsp;'<%=harmonyId%>'
                <%
                    }
                %>
                </td>
            </tr>
            <tr>
                <td class = "MainTableBottomRowLeftColumn"></td>
                <td class = "MainTableBottomRowRightColumn"></td>
            </tr>
        </table>
        <script type = "text/javascript">
            if (document.forms.length > 0)
                document.forms["setHarmonyIdForm"].harmonyId.focus();
        </script>
    </body>
</html:html>
