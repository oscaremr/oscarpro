<!--
* Copyright (c) 2021 WELL EMR Group Inc.
* This software is made available under the terms of the
* GNU General Public License, Version 2, 1991 (GPLv2).
* License details are available via "gnu.org/licenses/gpl-2.0.html".
-->
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="org.oscarehr.util.SessionConstants" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    boolean close = false;
    String logoutUrl = "/" + OscarProperties.getKaiemrDeployedContext() + "/#/one-id/login?logout=true";
    if (loggedInInfo.getOneIdGatewayData() == null) {
      close = true;
    } else {
      request.getSession().setAttribute(SessionConstants.OH_GATEWAY_DATA, null);
    }
%>
<html>
<head>
    <title>Logging out of ONE ID</title>
</head>
<body>
<script>
    <% if (close) { %>
    window.close();
    <% } else { %>
    window.location = '<%=logoutUrl%>';
    <% } %>
</script>
</body>
</html>
