<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ page import="java.net.*" %>
<%@ page import="java.text.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.concurrent.TimeUnit" %>
<%@ page import="org.apache.commons.lang.*" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.billing.CA.BC.dao.TeleplanEligibilityDao" %>
<%@ page import="org.oscarehr.common.dao.DemographicStudyDao" %>
<%@ page import="org.oscarehr.common.dao.DemographicDao" %>
<%@ page import="org.oscarehr.common.dao.DemographicExtDao" %>
<%@ page import="org.oscarehr.common.dao.MyGroupAccessRestrictionDao" %>
<%@ page import="org.oscarehr.common.dao.MyGroupDao" %>
<%@ page import="org.oscarehr.common.dao.OscarAppointmentDao" %>
<%@ page import="org.oscarehr.common.dao.PropertyDao" %>
<%@ page import="org.oscarehr.common.dao.ProviderSiteDao" %>
<%@ page import="org.oscarehr.common.dao.ProviderScheduleNoteDao" %>
<%@ page import="org.oscarehr.common.dao.ScheduleDateDao" %>
<%@ page import="org.oscarehr.common.dao.ScheduleTemplateCodeDao" %>
<%@ page import="org.oscarehr.common.dao.SiteDao" %>
<%@ page import="org.oscarehr.common.dao.StudyDao" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ page import="org.oscarehr.common.dao.ThirdPartyApplicationDao" %>
<%@ page import="org.oscarehr.common.dao.UserAcceptanceDao" %>
<%@ page import="org.oscarehr.common.dao.UserPropertyDAO" %>
<%@ page import="org.oscarehr.common.model.Appointment.BookingSource" %>
<%@ page import="org.oscarehr.common.model.Appointment" %>
<%@ page import="org.oscarehr.common.model.Dashboard" %>
<%@ page import="org.oscarehr.common.model.Demographic" %>
<%@ page import="org.oscarehr.common.model.DemographicExtKey" %>
<%@ page import="org.oscarehr.common.model.DemographicStudy" %>
<%@ page import="org.oscarehr.common.model.EmrContextEnum" %>
<%@ page import="org.oscarehr.common.model.LookupList" %>
<%@ page import="org.oscarehr.common.model.LookupListItem" %>
<%@ page import="org.oscarehr.common.model.MyGroup" %>
<%@ page import="org.oscarehr.common.model.MyGroupAccessRestriction" %>
<%@ page import="org.oscarehr.common.model.Provider" %>
<%@ page import="org.oscarehr.common.model.ProviderPreference" %>
<%@ page import="org.oscarehr.common.model.ProviderScheduleNote" %>
<%@ page import="org.oscarehr.common.model.ScheduleDate" %>
<%@ page import="org.oscarehr.common.model.ScheduleTemplate" %>
<%@ page import="org.oscarehr.common.model.ScheduleTemplateCode" %>
<%@ page import="org.oscarehr.common.model.Site" %>
<%@ page import="org.oscarehr.common.model.Study" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.oscarehr.common.model.ThirdPartyApplication" %>
<%@ page import="org.oscarehr.common.model.UserAcceptance" %>
<%@ page import="org.oscarehr.common.model.UserProperty" %>
<%@ page import="org.oscarehr.managers.AppManager" %>
<%@ page import="org.oscarehr.managers.DashboardManager" %>
<%@ page import="org.oscarehr.managers.LookupListManager" %>
<%@ page import="org.oscarehr.managers.ProgramManager2" %>
<%@ page import="org.oscarehr.managers.SecurityInfoManager" %>
<%@ page import="org.oscarehr.managers.TicklerManager" %>
<%@ page import="org.oscarehr.phr.util.MyOscarUtils" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="org.oscarehr.PMmodule.model.ProgramProvider" %>
<%@ page import="org.oscarehr.PMmodule.web.utils.UserRoleUtils" %>
<%@ page import="org.oscarehr.provider.model.PreventionManager" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.util.MiscUtils" %>
<%@ page import="org.oscarehr.util.MiscUtilsOld" %>
<%@ page import="org.oscarehr.util.SessionConstants" %>
<%@ page import="org.oscarehr.web.admin.ProviderPreferencesUIBean" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.appt.JdbcApptImpl" %>
<%@ page import="oscar.oscarMessenger.data.MsgMessageData" %>
<%@ page import="oscar.*" %>
<%@ page import="oscar.util.*" %>
<%@ page import="org.oscarehr.common.model.*" %>
<%@ page import="org.oscarehr.common.dao.*" %>
<%@ page import="org.oscarehr.integration.OneIdGatewayData" %>
<!-- add by caisi -->
<%@ taglib uri="http://www.caisi.ca/plugin-tag" prefix="plugin" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/caisi-tag.tld" prefix="caisi" %>
<%@ taglib uri="/WEB-INF/indivo-tag.tld" prefix="myoscar" %>
<%@ taglib uri="/WEB-INF/phr-tag.tld" prefix="phr" %>
<%@ taglib uri="/WEB-INF/wellAiVoice.tld" prefix="well-ai-voice"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security" %>
<%@ taglib uri="/WEB-INF/special_tag.tld" prefix="special" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%
	BillingONOUReportDao billingOnOUReportDao = SpringUtils.getBean(BillingONOUReportDao.class);
	AppManager appManager = SpringUtils.getBean(AppManager.class);
	DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
	DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
	DemographicStudyDao demographicStudyDao = SpringUtils.getBean(DemographicStudyDao.class);
	LoggedInInfo loggedInInfo1=LoggedInInfo.getLoggedInInfoFromSession(request);
	LookupListManager lookupListManager = SpringUtils.getBean(LookupListManager.class);
	MyGroupDao myGroupDao = SpringUtils.getBean(MyGroupDao.class);
	PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
	ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
	ProviderScheduleNoteDao providerScheduleNoteDao = SpringUtils.getBean(ProviderScheduleNoteDao.class);
	ProviderSiteDao providerSiteDao = SpringUtils.getBean(ProviderSiteDao.class);
	OscarAppointmentDao appointmentDao = SpringUtils.getBean(OscarAppointmentDao.class);
	SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
	ScheduleDateDao scheduleDateDao = SpringUtils.getBean(ScheduleDateDao.class);
	ScheduleTemplateCodeDao scheduleTemplateCodeDao = SpringUtils.getBean(ScheduleTemplateCodeDao.class);
	SiteDao siteDao = SpringUtils.getBean(SiteDao.class);
	StudyDao studyDao = SpringUtils.getBean(StudyDao.class);
	TicklerManager ticklerManager= SpringUtils.getBean(TicklerManager.class);
	UserAcceptanceDao userAcceptanceDao = SpringUtils.getBean(UserAcceptanceDao.class);
	UserPropertyDAO userPropertyDao = SpringUtils.getBean(UserPropertyDAO.class);
	LookupList reasonCodes = lookupListManager.findLookupListByName(loggedInInfo1, "reasonCode");
	Map<Integer,LookupListItem> reasonCodesMap = new  HashMap<Integer,LookupListItem>();
	for(LookupListItem lli:reasonCodes.getItems()) {
		reasonCodesMap.put(lli.getId(),lli);	
	}
	Boolean enableCustomTemporaryGroups = propertyDao.isActiveBooleanProperty("enable_custom_temporary_groups");
	String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean isSiteAccessPrivacy=false;
    boolean isTeamAccessPrivacy=false;
    MyGroupAccessRestrictionDao myGroupAccessRestrictionDao = SpringUtils.getBean(MyGroupAccessRestrictionDao.class);
    boolean authed=true;
	// Load all permissions once, and then reference them in the schedule loop below to avoid having to check on each appointment
	boolean eChartR = false;
	boolean ticklerR = false;
	boolean appointmentDoctorLinkR = false;
	boolean billingR = false;
	boolean masterLinkR = false;
	// <security:oscarSec roleName="< %=roleName$% >" objectName="_eChart" rights="r">
	if(securityInfoManager.hasPrivilege(loggedInInfo1, "_echart", "r", null)) {
		eChartR = true;
	}
	// <security:oscarSec roleName="< %=roleName$% >" objectName="_tickler" rights="r">
	if(securityInfoManager.hasPrivilege(loggedInInfo1, "_tickler", "r", null)) {
		ticklerR = true;
	}
	// <security:oscarSec roleName="< %=roleName$% >" objectName="_appointment.doctorLink" rights="r">
	if(securityInfoManager.hasPrivilege(loggedInInfo1, "_appointment.doctorLink", "r", null)) {
		appointmentDoctorLinkR = true;
	}
	// <security:oscarSec roleName="< %=roleName$% >" objectName="_billing" rights="r">
	if(securityInfoManager.hasPrivilege(loggedInInfo1, "_billing", "r", null)) {
		billingR = true;
	}
	// <security:oscarSec roleName="< %=roleName$% >" objectName="_masterLink" rights="r">
	if(securityInfoManager.hasPrivilege(loggedInInfo1, "_masterLink", "r", null)) {
		masterLinkR = true;
	}
	boolean enableKaiEmr = Boolean.parseBoolean(OscarProperties.getInstance().getProperty("enable_kai_emr", "false"));
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_appointment,_day" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect(request.getContextPath() + "/securityError.jsp?type=_appointment");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
%>
<security:oscarSec objectName="_site_access_privacy" roleName="<%=roleName$%>" rights="r" reverse="false">
	<%
		isSiteAccessPrivacy=true;
	%>
</security:oscarSec>
<security:oscarSec objectName="_team_access_privacy" roleName="<%=roleName$%>" rights="r" reverse="false">
	<%
		isTeamAccessPrivacy=true;
	%>
</security:oscarSec>
<% //multisite starts =====================
	boolean bMultisites = org.oscarehr.common.IsPropertiesOn.isMultisitesEnable();
	List<Site> sites = new ArrayList<>();
	List<Site> curUserSites = new ArrayList<>();
	List<String> siteProviderNos = new ArrayList<>();
	List<String> siteGroups = new ArrayList<>();
	HashMap<String,String> siteBgColor = new HashMap<>();
	HashMap<String,String> CurrentSiteMap = new HashMap<>();
	String selectedSite = null;
	if (bMultisites) {
	sites = siteDao.getAllActiveSites();
	selectedSite = (String)session.getAttribute("site_selected");
	if (selectedSite != null) {
		//get site provider list
		siteProviderNos = siteDao.getProviderNoBySiteLocation(selectedSite);
		siteGroups = siteDao.getGroupBySiteLocation(selectedSite);
	}
	if (isSiteAccessPrivacy || isTeamAccessPrivacy) {
		String siteManagerProviderNo = (String) session.getAttribute("user");
		curUserSites = siteDao.getActiveSitesByProviderNo(siteManagerProviderNo);
		if (selectedSite==null) {
	siteProviderNos = siteDao.getProviderNoBySiteManagerProviderNo(siteManagerProviderNo);
	siteGroups = siteDao.getGroupBySiteManagerProviderNo(siteManagerProviderNo);
		}
	}
	else {
		curUserSites = sites;
	}
	for (Site s : curUserSites) {
		CurrentSiteMap.put(s.getName(),"Y");
	}
	//get all sites bgColors
	for (Site st : sites) {
		siteBgColor.put(st.getName(),st.getBgColor());
	}
}
//multisite ends =======================
%>
<%
	long loadPage = System.currentTimeMillis();
    if(session.getAttribute("userrole") == null )  response.sendRedirect("../logout.jsp");
    //String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_appointment" rights="r" reverse="<%=true%>" >
<%
	response.sendRedirect("../logout.jsp");
%>
</security:oscarSec>
<!-- caisi infirmary view extension add -->
<caisi:isModuleLoad moduleName="caisi">
<%
	if (request.getParameter("year")!=null && request.getParameter("month")!=null && request.getParameter("day")!=null)
	{
		java.util.Date infirm_date=new java.util.GregorianCalendar(Integer.valueOf(request.getParameter("year")).intValue(), Integer.valueOf(request.getParameter("month")).intValue()-1, Integer.valueOf(request.getParameter("day")).intValue()).getTime();
		session.setAttribute("infirmaryView_date",infirm_date);
	}else
	{
		session.setAttribute("infirmaryView_date",null);
	}
	String reqstr =request.getQueryString();
	if (reqstr == null)
	{
		//Hack:: an unknown bug of struts or JSP causing the queryString to be null
		String year_q = request.getParameter("year");
	    String month_q =request.getParameter("month");
	    String day_q = request.getParameter("day");
	    String view_q = request.getParameter("view");
	    String displayMode_q = request.getParameter("displaymode");
	    reqstr = "year=" + year_q + "&month=" + month_q
           + "&day="+ day_q + "&view=" + view_q + "&displaymode=" + displayMode_q;
	}
	session.setAttribute("infirmaryView_OscarQue",reqstr);
%>
<c:import url="/infirm.do?action=showProgram" />
</caisi:isModuleLoad>
<!-- caisi infirmary view extension add end -->
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<jsp:useBean id="providerBean" class="java.util.Properties" scope="session" />
<jsp:useBean id="as" class="oscar.appt.ApptStatusData" scope="page" />
<jsp:useBean id="dateTimeCodeBean" class="java.util.Hashtable" scope="page" />
<%
	Properties oscarVariables = OscarProperties.getInstance();
	OscarProperties oscarProperties = OscarProperties.getInstance();
	String econsultUrl = oscarVariables.getProperty("backendEconsultUrl");
	String cyclesLink = oscar.OscarProperties.getInstance().getProperty("cyclesLink");
	String cyclesLocalLink = oscar.OscarProperties.getInstance().getProperty("cyclesLocalLink");
	String secondCyclesLink = oscar.OscarProperties.getInstance().getProperty("secondCyclesLink");
	String scheduleSecondCyclesLink = oscar.OscarProperties.getInstance().getProperty("scheduleSecondCyclesLink");
	String cyclesClientName = StringUtils.trimToNull(oscar.OscarProperties.getInstance().getProperty("cyclesClientName"));
	//Gets the request URL
	StringBuffer oscarUrl = request.getRequestURL();
	//Sets the length of the URL, found by subtracting the length of the servlet path from the length of the full URL, that way it only gets up to the context path
	oscarUrl.setLength(oscarUrl.length() - request.getServletPath().length());
	boolean enableUrgentMessages = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("enable_urgent_messages", false);
	boolean enableExternalNameOnSchedule = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("enable_external_name_on_schedule", false);
	boolean enableProviderScheduleNote = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("enable_provider_schedule_note", false);
%>
<!-- Struts for i18n -->
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%
	PreventionManager prevMgr = (PreventionManager)SpringUtils.getBean("preventionMgr");
%>
<%!/**
Checks if the schedule day is patients birthday
**/
public boolean isBirthday(String schedDate,String demBday){
	return schedDate.equals(demBday);
}
public boolean patientHasOutstandingPrivateBills(String demographicNo){
	oscar.oscarBilling.ca.bc.MSP.MSPReconcile msp = new oscar.oscarBilling.ca.bc.MSP.MSPReconcile();
	return msp.patientHasOutstandingPrivateBill(demographicNo);
}%>
<%
	if(session.getAttribute("user") == null )
        response.sendRedirect("../logout.jsp");
	String curUser_no = (String) session.getAttribute("user");
    ProviderPreference providerPreference2=(ProviderPreference)session.getAttribute(SessionConstants.LOGGED_IN_PROVIDER_PREFERENCE);
    String mygroupno = (request.getParameter("mygroup_no") == null ? providerPreference2.getMyGroupNo() : request.getParameter("mygroup_no"));
    if(mygroupno == null){
    	mygroupno = ".default";
    }
    if (!mygroupno.equals("tmp-" + loggedInInfo1.getLoggedInProviderNo())) {
        myGroupDao.deleteTemporaryGroup("tmp-" + loggedInInfo1.getLoggedInProviderNo());
    }
    String caisiView = null;
    caisiView = request.getParameter("GoToCaisiViewFromOscarView");
    boolean notOscarView = "false".equals(session.getAttribute("infirmaryView_isOscar"));
    if((caisiView!=null && "true".equals(caisiView)) || notOscarView) {
    	mygroupno = ".default";
    }
    String userfirstname = (String) session.getAttribute("userfirstname");
    String userlastname = (String) session.getAttribute("userlastname");
    String prov= (oscarVariables.getProperty("billregion","")).trim().toUpperCase();

    int startHour=providerPreference2.getStartHour();
    int endHour=providerPreference2.getEndHour();
    int everyMin=providerPreference2.getEveryMin();
    boolean twelveHourFormat = providerPreference2.isTwelveHourFormat();
    boolean labelShortcutEnabled = providerPreference2.isLabelShortcutEnabled();
    String defaultServiceType = (String) session.getAttribute("default_servicetype");
    ProviderPreference providerPreference = ProviderPreferencesUIBean
            .getProviderPreference(loggedInInfo1.getLoggedInProviderNo());
    if( defaultServiceType == null && providerPreference!=null) {
    	defaultServiceType = providerPreference.getDefaultServiceType();
    }
    if( defaultServiceType == null ) {
        defaultServiceType = "";
    }
    Collection<Integer> eforms = providerPreference2.getAppointmentScreenEForms();
    StringBuilder eformIds = new StringBuilder();
    for( Integer eform : eforms ) {
    	eformIds = eformIds.append("&eformId=" + eform);
    }
    Collection<String> forms = providerPreference2.getAppointmentScreenForms();
    StringBuilder ectFormNames = new StringBuilder();
    for( String formName : forms ) {
    	ectFormNames = ectFormNames.append("&encounterFormName=" + formName);
    }
	boolean prescriptionQrCodes = providerPreference2.isPrintQrCodeOnPrescriptions();
	boolean erx_enable = providerPreference2.isERxEnabled();
	boolean erx_training_mode = providerPreference2.isERxTrainingMode();
	boolean bShortcutIntakeForm = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("enable_appointment_intake_form", false);
	boolean displayAppointmentDaysheetButton = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("display_appointment_daysheet_button", true);
	boolean showAppointmentReason = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("show_appointment_reason", true);
    String newticklerwarningwindow=null;
    String default_pmm=null;
    String programId_oscarView=null;
	String ocanWarningWindow=null;
	String cbiReminderWindow=null;
	String caisiBillingPreferenceNotDelete = null;
	String tklerProviderNo = null;
	UserProperty userProperty
			= userPropertyDao.getProp(curUser_no, UserProperty.PROVIDER_FOR_TICKLER_WARNING);
	if (userProperty != null) {
		tklerProviderNo = userProperty.getValue();
	} else {
		tklerProviderNo = curUser_no;
	}
	if (org.oscarehr.common.IsPropertiesOn.isCaisiEnable() && org.oscarehr.common.IsPropertiesOn.propertiesOn("OCAN_warning_window") ) {
        ocanWarningWindow = (String)session.getAttribute("ocanWarningWindow");
	}
	if (org.oscarehr.common.IsPropertiesOn.isCaisiEnable() && org.oscarehr.common.IsPropertiesOn.propertiesOn("CBI_REMINDER_WINDOW") ) {
        cbiReminderWindow = (String)session.getAttribute("cbiReminderWindow");
	}
	//Hide old echart link
	boolean showOldEchartLink = true;
	UserProperty oldEchartLink = userPropertyDao.getProp(curUser_no, UserProperty.HIDE_OLD_ECHART_LINK_IN_APPT);
	if (oldEchartLink!=null && "Y".equals(oldEchartLink.getValue())) showOldEchartLink = false;
	SimpleDateFormat appointmentDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
if (org.oscarehr.common.IsPropertiesOn.isCaisiEnable() && org.oscarehr.common.IsPropertiesOn.isTicklerPlusEnable()){
	newticklerwarningwindow = (String) session.getAttribute("newticklerwarningwindow");
	default_pmm = (String)session.getAttribute("default_pmm");
	caisiBillingPreferenceNotDelete = String.valueOf(session.getAttribute("caisiBillingPreferenceNotDelete"));
    if(caisiBillingPreferenceNotDelete==null) {
    	ProviderPreference pp = ProviderPreferencesUIBean.getProviderPreferenceByProviderNo(curUser_no);
    	if(pp!=null) {
    		caisiBillingPreferenceNotDelete = String.valueOf(pp.getDefaultDoNotDeleteBilling());
    	}
    }
	//Disable schedule view associated with the program
	//Made the default program id "0";
	//programId_oscarView= (String)session.getAttribute("programId_oscarView");
	programId_oscarView = "0";
} else {
	programId_oscarView="0";
	session.setAttribute("programId_oscarView",programId_oscarView);
}
    int lenLimitedL=11; //L - long
    if(OscarProperties.getInstance().getProperty("APPT_SHOW_FULL_NAME","").equalsIgnoreCase("true")) {
    	lenLimitedL = 25;
    }
    int lenLimitedS=3; //S - short
    int len = lenLimitedL;
    int view = request.getParameter("view")!=null ? Integer.parseInt(request.getParameter("view")) : 0; //0-multiple views, 1-single view
    //// THIS IS THE VALUE I HAVE BEEN LOOKING FOR!!!!!
	boolean bDispTemplatePeriod = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("enable_receptionist_alt_view", false); // true - display as schedule template period, false - display as preference
%>
<%
  String tickler_no="", textColor="", tickler_note="";
  String ver = "", roster="";
  String yob = "";
  String mob = "";
  String dob = "";
  String demBday = "";
  StringBuffer study_no=null, study_link=null,studyDescription=null;
  String studySymbol = "\u03A3", studyColor = "red";

  // List of statuses that are excluded from the schedule appointment count for each provider
  final List<String> noAppointmentCountStatus = Arrays.asList("C", "CS", "CV", "N", "NS", "NV");

  String resourcebaseurl =  oscarVariables.getProperty("resource_base_url");
    UserProperty rbu = userPropertyDao.getProp("resource_baseurl");
    if(rbu != null) {
    	resourcebaseurl = rbu.getValue();
    }
    String resourcehelpHtml = "";
    UserProperty rbuHtml = userPropertyDao.getProp("resource_helpHtml");
    if(rbuHtml != null) {
    	resourcehelpHtml = rbuHtml.getValue();
    }
    boolean isWeekView = false;
    String provNum = request.getParameter("provider_no");
    if (provNum != null) {
        isWeekView = true;
    }
    if(caisiView!=null && "true".equals(caisiView)) {
    	isWeekView = false;
    }
int nProvider;
boolean caseload = "1".equals(request.getParameter("caseload"));
GregorianCalendar cal = new GregorianCalendar();
int curYear = cal.get(Calendar.YEAR);
int curMonth = (cal.get(Calendar.MONTH)+1);
int curDay = cal.get(Calendar.DAY_OF_MONTH);
int year = Integer.parseInt(request.getParameter("year"));
int month = Integer.parseInt(request.getParameter("month"));
int day = Integer.parseInt(request.getParameter("day"));
//verify the input date is really existed
cal = new GregorianCalendar(year,(month-1),day);
boolean weekendsEnabled = true;
int weekViewDays = 7;
if (isWeekView) {
UserProperty weekViewWeekendProp = userPropertyDao.getProp(curUser_no, UserProperty.SCHEDULE_WEEK_VIEW_WEEKENDS);
if (weekViewWeekendProp!= null && StringUtils.trimToNull(weekViewWeekendProp.getValue()) != null) {
	weekendsEnabled = Boolean.parseBoolean(weekViewWeekendProp.getValue());
}
weekViewDays = weekendsEnabled ? 7 : 5;
cal.add(Calendar.DATE, -(cal.get(Calendar.DAY_OF_WEEK)- (weekendsEnabled ? 1 : 2))); // change the day to the current weeks initial sunday or monday
}
int week = cal.get(Calendar.WEEK_OF_YEAR);
year = cal.get(Calendar.YEAR);
month = (cal.get(Calendar.MONTH)+1);
day = cal.get(Calendar.DAY_OF_MONTH);
String strDate = year + "-" + (month>9?(""+month):("0"+month))+ "-" + (day>9?(""+day):("0"+day));
String monthDay = String.format("%02d", month) + "-" + String.format("%02d", day);
SimpleDateFormat inform = new SimpleDateFormat ("yyyy-MM-dd", request.getLocale());
String formatDate;
try {
java.util.ResourceBundle prop = ResourceBundle.getBundle("oscarResources", request.getLocale());
formatDate = UtilDateUtilities.DateToString(inform.parse(strDate), prop.getString("date.EEEyyyyMMdd"),request.getLocale());
} catch (Exception e) {
	MiscUtils.getLogger().error("Error", e);
formatDate = UtilDateUtilities.DateToString(inform.parse(strDate), "EEE, yyyy-MM-dd");
}
String strYear=""+year;
String strMonth=month>9?(""+month):("0"+month);
String strDay=day>9?(""+day):("0"+day);
Calendar apptDate = Calendar.getInstance();
apptDate.set(year, month-1 , day);
Calendar minDate = Calendar.getInstance();
minDate.set( minDate.get(Calendar.YEAR), minDate.get(Calendar.MONTH), minDate.get(Calendar.DATE) );
String allowDay = "";
if (apptDate.equals(minDate)) {
    allowDay = "Yes";
    } else {
    allowDay = "No";
}
minDate.add(Calendar.DATE, 7);
String allowWeek = "";
if (apptDate.before(minDate)) {
    allowWeek = "Yes";
    } else {
    allowWeek = "No";
}
String termsOfAgreement = "<br/><h3>Example Terms</h3><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p><br/><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p><br/><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p><br/><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p><br/><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p><br/><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p><br/><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p><br/><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p><br/><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>";
UserAcceptance userAcceptance = userAcceptanceDao.getByProviderNo(curUser_no);
boolean showAgreement = false;
	Boolean enhancedEnabled = "E".equals(session.getAttribute(SessionConstants.LOGIN_TYPE));
HashMap<String, Boolean> schedulePreferencesMap = new HashMap<String, Boolean>();

String clinicalConnectEndpoint = SystemPreferencesUtils.findPreferenceByName("oneid.clinical_connect.endpoint")
		== null ? "" : SystemPreferencesUtils.findPreferenceByName("oneid.clinical_connect.endpoint").getValue();
boolean enableAlertsOnScheduleScreenSystemPreference = SystemPreferencesUtils
		.isReadBooleanPreferenceWithDefault("enable_alerts_on_schedule_screen", false);
boolean enableNotesOnScheduleScreenSystemPreference = SystemPreferencesUtils
		.isReadBooleanPreferenceWithDefault("enable_notes_on_schedule_screen", false);

List<SystemPreferences> schedulePreferences = SystemPreferencesUtils.findPreferencesByNames(SystemPreferences.SCHEDULE_PREFERENCE_KEYS);
for (SystemPreferences preference : schedulePreferences) {
    schedulePreferencesMap.put(preference.getName(), Boolean.parseBoolean(preference.getValue()));
}
boolean enableEFormInAppointment = SystemPreferencesUtils
    .isReadBooleanPreferenceWithDefault("enable_eform_in_appointment", false);

UserProperty customRosterStatusProperty = userPropertyDao.getProp(curUser_no, "schedule_display_custom_roster_status");
if (customRosterStatusProperty != null) {
    schedulePreferencesMap.put("schedule_display_custom_roster_status", Boolean.parseBoolean(customRosterStatusProperty.getValue()));
}
    Map<String, Boolean> generalSettingsMap = SystemPreferencesUtils.findByKeysAsMap(SystemPreferences.GENERAL_SETTINGS_KEYS);
    boolean replaceNameWithPreferred = generalSettingsMap.getOrDefault("replace_demographic_name_with_preferred", false);
    UserProperty prescribeItEnabledProperty = userPropertyDao.getProp("erx.enabled");
    Boolean prescribeItEnabled = (prescribeItEnabledProperty != null && "true".equals(prescribeItEnabledProperty.getValue()));
	OneIdGatewayData oneIdGatewayData = loggedInInfo1.getOneIdGatewayData();

	boolean showReferralMenu = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("show_referral_menu", false);
    boolean forceLogoutWhenInactive = SystemPreferencesUtils
            .getPreferenceValueByName("force_logout_when_inactive", "false").equals("true");
    SystemPreferences forceLogoutWhenInactiveTime = SystemPreferencesUtils
            .findPreferenceByName("force_logout_when_inactive_time");
    int pageRefreshTimeout = 0;
    int forceLogoutWhenInactiveTimeValue = 0;
    int dailyAppointmentsPageRefreshTimeout = 0;
    
    try {
        dailyAppointmentsPageRefreshTimeout = Integer.parseInt(SystemPreferencesUtils
                .getPreferenceValueByName("daily_appointments_page_refresh_timeout", "180"));
    } catch (Exception ex) {}
    
    if (forceLogoutWhenInactiveTime != null && forceLogoutWhenInactive) {
        try {
            forceLogoutWhenInactiveTimeValue =
                    Integer.parseInt(forceLogoutWhenInactiveTime.getValue()) * 60;
        } catch (Exception ex) {}
        
        pageRefreshTimeout = Math.min(
                forceLogoutWhenInactiveTimeValue, dailyAppointmentsPageRefreshTimeout);
    } else {
        pageRefreshTimeout = dailyAppointmentsPageRefreshTimeout;
    }
%>
<html:html locale="true">
<head>
    <script>let ctx = "${pageContext.request.contextPath}"</script>
<title><%=WordUtils.capitalize(userlastname + ", " +  org.apache.commons.lang.StringUtils.substring(userfirstname, 0, 1)) + "-"%><bean:message key="provider.appointmentProviderAdminDay.title"/></title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
	<link rel="stylesheet" href="../css/alertify.core.css" type="text/css">
	<link rel="stylesheet" href="../css/alertify.default.css" type="text/css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css">



<%
	if (!caseload) {
%>
<c:if test="${empty sessionScope.archiveView or sessionScope.archiveView != true}">
	<%
		String thisRequestUrl = request.getRequestURL().toString() + (StringUtils.isEmpty(request.getQueryString()) ? "" : "?" + request.getQueryString());
		if (!thisRequestUrl.contains("&autoRefresh=true")) {
			thisRequestUrl += "&autoRefresh=true";
		}
		if (oneIdGatewayData != null) {
			thisRequestUrl += "&omdSessionCheck=true";
		}
	%>
<%= (pageRefreshTimeout <= 0)?"":"<meta http-equiv=\"refresh\" content=\""+pageRefreshTimeout+"; url="+thisRequestUrl+"\">" %>
</c:if>
<%
	}
%>
<script type="text/javascript" src="../share/javascript/oneid.js" ></script>
<script type="text/javascript" src="../share/javascript/Oscar.js" ></script>
<script type="text/javascript" src="../share/javascript/prototype.js"></script>
<script type="text/javascript" src="../share/javascript/popupmenu.js"></script>
<script type="text/javascript" src="../share/javascript/menutility.js"></script>
<script type="text/javascript" src="../phr/phr.js"></script>
<script src="<c:out value="../js/jquery.js"/>"></script>
<script>
	jQuery.noConflict();
</script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-3.1.0.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.floatThead.js"></script>
<script type="application/javascript">
	var jQuery_3_1_0 = jQuery.noConflict(true);
</script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/alertify.js"></script>
<script type="text/javascript">
	function addTableHeaderFloat() {
		if (jQuery('div#caseloadDiv').length) { //if on caseload screen
			var table = jQuery('div#caseloadDiv');
			var topPadding = jQuery('div.header-div').height();
			table.css('padding-top', (topPadding + 2) + 'px');
		} else if (typeof jQuery_3_1_0 != 'undefined' && jQuery_3_1_0().floatThead && jQuery('table#scheduleTable').length) { //if on schedule and floatThead enabled
			var table = jQuery_3_1_0('table#scheduleTable');
			var topPadding = jQuery_3_1_0('div.header-div').height();
			table.css('padding-top', (topPadding) + 'px');
			table.floatThead('destroy');
			table.floatThead({top: topPadding});
		}
	}
</script>
<link rel="stylesheet" href="../js/custom/kai/kai_bar.css" type="text/css" onload="addTableHeaderFloat()"/>
<!-- Determine which stylesheet to use: mobile-optimized or regular -->
<%
	boolean isMobileOptimized = session.getAttribute("mobileOptimized") != null;
if (isMobileOptimized) {
%>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, width=device-width"/>
    <link rel="stylesheet" href="../mobile/receptionistapptstyle.css" type="text/css">
<%
	} else {
%>
<link rel="stylesheet" href="../css/receptionistapptstyle.css" type="text/css" onload="addTableHeaderFloat()">
<link rel="stylesheet" href="../css/helpdetails.css" type="text/css">
<%
	}
%>
<script type="text/javascript" src="../share/javascript/schedule.js" ></script>
<script type="text/javascript">
function getElementsByClass(searchClass,node,tag) {
        var classElements = new Array();
        if ( node == null )
                node = document;
        if ( tag == null )
                tag = '*';
        var els = document.getElementsByTagName(tag);
        var elsLen = els.length;
        var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
        for (i = 0, j = 0; i < elsLen; i++) {
                if ( pattern.test(els[i].className) ) {
                        classElements[j] = els[i];
                        j++;
                }
        }
        return classElements;
}
jQuery("document").ready(function(){
	jQuery(".hideReason").hide();
	for( var i = 0; i < localStorage.length; i++ ) {
		var key = localStorage.key(i);
		if( localStorage.getItem(key) == "true" ) {
			jQuery(key).show();
		}
	}
});
function toggleReason( providerNo ) { 
	var id = ".reason_" + providerNo;
    jQuery( id ).toggle();
    localStorage.setItem( id, jQuery( id ).is( ":visible" ) );
	if (typeof jQuery_3_1_0 != 'undefined' && jQuery_3_1_0().floatThead && jQuery('table#scheduleTable').length) { //if on schedule and floatThead enabled
		jQuery_3_1_0('table#scheduleTable').floatThead('reflow');
	}
}
function bulkHCV() {
	var searchStr = window.location.search;
	if(searchStr.length == 0){
		if(jQuery(".dateAppointment").length > 0){
			var dateStr = jQuery(jQuery(".dateAppointment")[0]).text();
			if(dateStr.indexOf(",") > 0){
				dateStr = dateStr.substring(dateStr.indexOf(",") + 1).trim();
				if(dateStr.indexOf("-") > -1){
					dateStr = dateStr.split("-");
					
					if(dateStr.length == 3){
						searchStr = "?year=" + dateStr[0] + "&month=" + dateStr[1] + "&day=" + dateStr[2];
					}
				}
			}
		}
	}
	var urlStr = "<%=request.getContextPath()%>/well/hcv/bulk.jsp" + searchStr + "#!/bulk";
	popupPage2(urlStr, "Validate HIN", "500", "1024");
}
function confirmPopupPage(height, width, queryString, doConfirm, allowDay, allowWeek){
    if (doConfirm == "Yes") {
        if (confirm("<bean:message key="provider.appointmentProviderAdminDay.confirmBooking"/>")){
            popupPage(height, width, queryString);
        }
    }
    else if (doConfirm == "Day"){
        if (allowDay == "No") {
            alert("<bean:message key="provider.appointmentProviderAdminDay.sameDay"/>");
        }
        else {
            popupPage(height, width, queryString);
        }
    }
    else if (doConfirm == "Wk"){
        if (allowWeek == "No") {
            alert("<bean:message key="provider.appointmentProviderAdminDay.sameWeek"/>");
        }
        else {
            popupPage2(queryString, 'appointment', height, width);
        }
    }
    else {
        popupPage2(queryString, 'appointment', height, width);
    }
}
function popupPage(vheight,vwidth,varpage) {
    if (vheight === "auto") {
        vheight = document.body.clientHeight;
    }

    if (vwidth === "auto") {
        vwidth = document.body.clientWidth;
    }
    var page = "" + varpage;
windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=50,screenY=50,top=0,left=0";
var popup=window.open(page, "<bean:message key="provider.appointmentProviderAdminDay.apptProvider"/>", windowprops);
if (popup != null) {
if (popup.opener == null) {
popup.opener = self;
}
popup.focus();
}
}
function popupPageOfChangePassword(){
<%
	String expired_days="";
	if(session.getAttribute("expired_days")!=null){
		expired_days = (String)session.getAttribute("expired_days");
	}
	if(!(expired_days.equals(" ")||expired_days.equals("")||expired_days==null)) {
		//javascript%>
window.open("changePassword.jsp","changePassword","resizable=yes,scrollbars=yes,width=400,height=300");
changePassword.moveTo(0,0);
<%}%>
}
function popupInboxManager(varpage){
    var page = "" + varpage;
    let widthVal = '1215';
    if (!page.includes('/dms/inboxManage.do')) { // is new ui inbox
        // set to 90% of screen width, otherwise use 1600
        widthVal = (window.screen.width < 1600) ? (window.screen.width * 0.9) : '1600';
    }
    var windowname="apptProviderInbox";
    windowprops = "height=900,width="+widthVal+",location=no,"
    + "scrollbars=yes,menubars=no,toolbars=no,resizable=yes,top=10,left=0";
    var popup = window.open(page, windowname, windowprops);
    if (popup != null) {
        if (popup.opener == null) {
            popup.opener = self;
        }
        popup.focus();
    }
}
<!--oscarMessenger code block-->
function popupMsg(vheight,vwidth,varpage) {
	var page = varpage;
	windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=0,screenY=0,top=0,left=0";
	var popup=window.open(varpage, "msg", windowprops);
	if (popup != null) {
		if (popup.opener == null) {
			popup.opener = self;
		}
		popup.focus();
	}
}
function popupOscarRx(vheight,vwidth,varpage) {
var page = varpage;
windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=0,screenY=0,top=0,left=0";
var popup=window.open(varpage, "<bean:message key="global.oscarRx"/>_appt", windowprops);
if (popup != null) {
if (popup.opener == null) {
popup.opener = self;
}
popup.focus();
}
}
function onUnbilled(url) {
if(confirm("<bean:message key="provider.appointmentProviderAdminDay.onUnbilled"/>")) {
popupPage(700,720, url);
}
}
function review(key) {
    if(self.location.href.lastIndexOf("?") > 0) {
        if(self.location.href.lastIndexOf("&viewall=") > 0 ) a = self.location.href.substring(0,self.location.href.lastIndexOf("&viewall="));
        else a = self.location.href;
    } else {
        a="providercontrol.jsp?year="+document.jumptodate.year.value+"&month="+document.jumptodate.month.value+"&day="+document.jumptodate.day.value+"&view=0&displaymode=day&dboperation=searchappointmentday&site=" + "<%=(selectedSite==null? "none" : Encode.forJavaScriptBlock(selectedSite))%>";
    }
    self.location.href = a + "&viewall="+key ;
}
function changeGroup(s) {
var newGroupNo = s.options[s.selectedIndex].value;
if(newGroupNo.indexOf("_grp_") != -1) {
  newGroupNo = s.options[s.selectedIndex].value.substring(5);
}else{
  newGroupNo = s.options[s.selectedIndex].value;
}
<%if (org.oscarehr.common.IsPropertiesOn.isCaisiEnable() && org.oscarehr.common.IsPropertiesOn.isTicklerPlusEnable()){%>
	//Disable schedule view associated with the program
	//Made the default program id "0";
	//var programId = document.getElementById("bedprogram_no").value;
	var programId = 0;
	var programId_forCME = document.getElementById("bedprogram_no").value;
	popupPage(10,10, "providercontrol.jsp?provider_no=<%=curUser_no%>&start_hour=<%=startHour%>&end_hour=<%=endHour%>&every_min=<%=everyMin%>&caisiBillingPreferenceNotDelete=<%=Encode.forJavaScriptBlock(caisiBillingPreferenceNotDelete)%>&new_tickler_warning_window=<%=Encode.forJavaScriptBlock(newticklerwarningwindow)%>&default_pmm=<%=Encode.forJavaScriptBlock(default_pmm)%>&color_template=deepblue&dboperation=updatepreference&displaymode=updatepreference&default_servicetype=<%=Encode.forJavaScriptBlock(defaultServiceType)%>&prescriptionQrCodes=<%=prescriptionQrCodes%>&erx_enable=<%=erx_enable%>&erx_training_mode=<%=erx_training_mode%>&mygroup_no="+newGroupNo+"&programId_oscarView="+programId+"&case_program_id="+programId_forCME + "<%=Encode.forJavaScriptBlock(eformIds.toString())%><%=Encode.forJavaScriptBlock(ectFormNames.toString())%>");
<%}else {%>
  var programId=0;
	popupPage(10,10, "providercontrol.jsp?provider_no=<%=curUser_no%>&start_hour=<%=startHour%>&end_hour=<%=endHour%>&every_min=<%=everyMin%>&color_template=deepblue&dboperation=updatepreference&displaymode=updatepreference&default_servicetype=<%=Encode.forJavaScriptBlock(defaultServiceType)%>&prescriptionQrCodes=<%=prescriptionQrCodes%>&erx_enable=<%=erx_enable%>&erx_training_mode=<%=erx_training_mode%>&mygroup_no="+newGroupNo+"&programId_oscarView="+programId + "<%=Encode.forJavaScriptBlock(eformIds.toString())%><%=Encode.forJavaScriptBlock(ectFormNames.toString())%>");
<%}%>
}
function changeGroupExternal(newGroupNo) {
	<% if (org.oscarehr.common.IsPropertiesOn.isCaisiEnable() && org.oscarehr.common.IsPropertiesOn.isTicklerPlusEnable()) { %>
	var programId_forCME = document.getElementById("bedprogram_no").value;
	popupPage(10, 10, "providercontrol.jsp?provider_no=<%=curUser_no%>&start_hour=<%=startHour%>&end_hour=<%=endHour%>&every_min=<%=everyMin%>&caisiBillingPreferenceNotDelete=<%=Encode.forJavaScriptBlock(caisiBillingPreferenceNotDelete)%>&new_tickler_warning_window=<%=Encode.forJavaScriptBlock(newticklerwarningwindow)%>&default_pmm=<%=Encode.forJavaScriptBlock(default_pmm)%>&color_template=deepblue&dboperation=updatepreference&displaymode=updatepreference&default_servicetype=<%=Encode.forJavaScriptBlock(defaultServiceType)%>&prescriptionQrCodes=<%=prescriptionQrCodes%>&erx_enable=<%=erx_enable%>&erx_training_mode=<%=erx_training_mode%>&mygroup_no=" + newGroupNo + "&programId_oscarView=0&case_program_id=" + programId_forCME + "<%=Encode.forJavaScriptBlock(eformIds.toString())%><%=Encode.forJavaScriptBlock(ectFormNames.toString())%>");
	<% } else { %>
	popupPage(10, 10, "providercontrol.jsp?provider_no=<%=curUser_no%>&start_hour=<%=startHour%>&end_hour=<%=endHour%>&every_min=<%=everyMin%>&color_template=deepblue&dboperation=updatepreference&displaymode=updatepreference&default_servicetype=<%=Encode.forJavaScriptBlock(defaultServiceType)%>&prescriptionQrCodes=<%=prescriptionQrCodes%>&erx_enable=<%=erx_enable%>&erx_training_mode=<%=erx_training_mode%>&mygroup_no=" + newGroupNo + "&programId_oscarView=0<%=Encode.forJavaScriptBlock(eformIds.toString())%><%=Encode.forJavaScriptBlock(ectFormNames.toString())%>");
	<% } %>
}
<%
	String viewall = request.getParameter("viewall");
	if( viewall == null )
	{
    	viewall = "0";
	}
%>
function ts1(s) {
popupPage(360,780,('../appointment/addappointment.jsp?'+s));
}
function tsr(s) {
popupPage(360,780,('../appointment/appointmentcontrol.jsp?displaymode=edit&dboperation=search&'+s));
}
function goFilpView(s) {
self.location.href = "../schedule/scheduleflipview.jsp?originalpage=schedule&startDate=<%=year+"-"+month+"-"+day%>" + "&provider_no="+s ;
}
function goDaySheet(s) {
var date="<%=year%>-<%=month%>-<%=day%>";
popupPage2("../report/displayDaysheet.do?dsmode=all&provider_no="+s+"&sdate="+date+"&edate="+date+"&sTime=<%=startHour%>&eTime=<%=endHour%>","reportPage");
}
function goWeekView(s) {
self.location.href = "providercontrol.jsp?year=<%=year%>&month=<%=month%>&day=<%=day%>&view=0&displaymode=day&dboperation=searchappointmentday&viewall=<%=Encode.forUriComponent(viewall)%>&provider_no="+s;
}
function goZoomView(s, n) {
self.location.href = "providercontrol.jsp?year=<%=strYear%>&month=<%=strMonth%>&day=<%=strDay%>&view=1&curProvider="+s+"&curProviderName="+encodeURIComponent(n)+"&displaymode=day&dboperation=searchappointmentday" ;
}
function findProvider(p,m,d) {
popupPage(300,400, "receptionistfindprovider.jsp?pyear=" +p+ "&pmonth=" +m+ "&pday=" +d+ "&providername="+ document.findprovider.providername.value );
}
function goSearchView(s) {
	popupPage(600,650,"../appointment/appointmentsearch.jsp?provider_no="+s);
}
function displayKaiMessage()
{
    <%
    	boolean hideOscarClassic = Boolean.parseBoolean(oscarProperties.getProperty("hide_oscar_classic"));
    %>
    var hideOscarClassic = <%=hideOscarClassic%>
    alertify.set({buttons: {ok:'I Understand', cancel:'Remind Me Later'} });
    alertify.confirm("<h2>Kai Enhanced Terms and Conditions</h2><br/><div id=\"termsAndConditionsDiv\" style=\"overflow-y: auto; height: 250px; border-style: solid;\"><%=termsOfAgreement%></div><br/><input type=\"checkbox\" id=\"termsCheck\"/>I have read and agree with all Terms and Conditions<br/><br/><a href=\"#\" onclick='popupPage(800,900,\"https://www.google.ca\")' style='color: blue;'>Click here to learn more</a>", function (accepted)
	{
            var url = "<%=request.getContextPath()%>/KaiMessageAction.do";
            var data = "providerNo=<%=curUser_no%>&accepted=" + accepted;
            new Ajax.Request(url,{method:'post', parameters:data, onSuccess:function(transport)
            {
               var response = transport.responseText;
               if (response === "enhanced") {
                   alertify.alert("You successfully agreed to our Terms and Conditions! You will now enter Kai Enhanced.", function () {
                       refresh();
				   });
			   } else if (response === "classic") {
				   if (hideOscarClassic) {
                       alertify.alert("You have chosen not to accept the Terms and Conditions. You will be logged out, but may try again.", function () {
                           window.location.href = '/oscar/logout.jsp';
                       });
                   } else {
                       alertify.alert("You have been reverted to OSCAR Classic. You will be re-prompted next time you sign in with Kai Enhanced.", function () {
                           refresh();
                       });
				   }
			   } else {
                   alertify.error("There was an error saving your response, please try again or contact Kai Support.");
			   }
            }});
	}).set('modal', false);
    document.getElementById('alertify-ok').disabled = true;
    document.getElementById('alertify-ok').style.background="#B0B0B0";
    jQuery('#termsCheck').change(function () {
        if (this.checked) {
            document.getElementById('alertify-ok').disabled = false;
            document.getElementById('alertify-ok').style.background="#5CB811";
		} else {
            document.getElementById('alertify-ok').disabled = true;
            document.getElementById('alertify-ok').style.background="#B0B0B0";
		}
	});
}
function pollMessageCallback(response, status, xhr) {
    // if response includes logout error message, change window location to redirct url
    if (xhr.responseURL.includes('errorMessage=logged%20out%20due%20to%20inactivity')) {
        window.location = xhr.responseURL;
    }
}
//popup a new tickler warning window
function load() {
	var ocan = "<%=Encode.forJavaScriptBlock(ocanWarningWindow)%>";
	if(ocan!="null" && cbi!="") {
		alert(ocan);
	}
	var cbi = "<%=Encode.forJavaScriptBlock(cbiReminderWindow)%>";
	if(cbi!="null" && cbi!="") {
		alert(cbi);
		<%request.getSession().setAttribute("cbiReminderWindow", "null");%>
	}
	if ("<%=Encode.forJavaScriptBlock(newticklerwarningwindow)%>"=="enabled") {
		if (IsPopupBlocker()) {
		    alert("You have a popup blocker, so you can not see the new tickler warning window. Please disable the pop blocker in your google bar, yahoo bar or IE ...");
		} else{
				var pu=window.open("../UnreadTickler.do",'viewUnreadTickler',"height=120,width=250,location=no,scrollbars=no,menubars=no,toolbars=no,resizable=yes,top=500,left=700");
				if(window.focus)
					pu.focus();
			}
	}
	popupPageOfChangePassword();
	refreshAllTabAlerts();
    <% if (enhancedEnabled && prescribeItEnabled) { %>
		refreshRenewalAlert();
	<%}%>
}
function callEligibilityWebService(url,id,demographic_no){
	document.getElementById('search_spinner').innerHTML='<bean:message key="demographic.demographiceditdemographic.msgLoading"/>';
	document.getElementById(id).innerHTML="";
	var ran_number=Math.round(Math.random()*1000000);
	var params = "demographic="+demographic_no+"&provider=<%=curUser_no%>&method=checkElig&rand="+ran_number;  //hack to get around ie caching the page
	var response;
	new Ajax.Request(url+'?'+params, {
		onSuccess: function(response) {
			document.getElementById(id).innerHTML=response.responseText ;
			document.getElementById('search_spinner').innerHTML="";
		}
	} );
}
function showMenu(menuNumber, eventObj) {
	var menuId = 'menu' + menuNumber;
	return showPopup(menuId, eventObj);
}
function toggleLoginSwitch() {
  var loginMenu = document.getElementById("loginSwitchNav");
  if (loginMenu.style.display === "block") {
    loginMenu.style.display = "none";
  } else {
    loginMenu.style.display = "block";
  }
}
function acknowledgeLoginSwitchNotification() {
  var url = "<%=request.getContextPath()%>/provider/acknowledgeLoginSwitchNotification.do";
  var data = "providerNo=<%=curUser_no%>"
  new Ajax.Request(url,{method:'post', parameters:data, onSuccess:function() {
    jQuery('.loginSwitchNotification').hide();
  }});
}

function refreshOAuthToken() {
	let isSso = <%=oneIdGatewayData != null && oneIdGatewayData.isSso()%>;
	let baseUrl = '<%= OscarProperties.getOscarProBaseUrl() %>';
	new Ajax.Request(baseUrl + '/api/one-id/refreshSession', {
		method: 'GET',
		onSuccess: function(response) {
			if (response.responseText === 'false') {
				alert ('There was an issue automatically refreshing your ONE ID token. Please try manually re-authenticating.');
				if (isSso) {
					popupPage(600, 650, '<%=OscarProperties.getOscarProBaseUrl()%>/api/one-id/login?forward=<%=OscarProperties.getOscarProBaseUrl()%>/#/one-id/login?reauthenticated=true');
				} else {
					// If the attempt to refresh the token fails and it is not SSO, clear the oneIDSession
					new Ajax.Request(baseUrl + '/api/one-id/clearSession', {
						method: 'DELETE',
						onFailure: function() {
							alert ('Your ONE ID session has expired. Please try manually re-authenticating.')
						}
					})
				}
			}
		},
		onFailure: function() {
			alert ('There was an issue automatically refreshing your ONE ID token. Please try manually re-authenticating.');
		}
	})
}
</script>
<%
	if (OscarProperties.getInstance().getBooleanProperty("indivica_hc_read_enabled", "true")) {
%>
<script src="<%=request.getContextPath()%>/hcHandler/hcHandler.js"></script>
<script src="<%=request.getContextPath()%>/hcHandler/hcHandlerAppointment.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/hcHandler/hcHandler.css" type="text/css" />
<%
	}
%>
<style>
#loginSwitchNav {
  display: none;
  position: absolute;
  top: 38px;
  left: 10px;
  background: #FFFFFF;
  border: 1px solid rgba(44, 72, 110, 0.1);
  border-radius: 4px;
  width: 220px;
}
#loginSwitchNav ul {
  width: 100%;
  margin: 0px;
  list-style: none;
  padding: 0px;
}
#loginSwitchNav ul li {
  padding: 10px;
}
#loginSwitchNav ul li:not(:last-child) {
  border-bottom: 1px solid #E9ECF0;
}
#loginSwitchNav ul li a {
  font-family: nimbus-sans, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 18px;
  color: #2C486E;
  line-height: 21px;
  vertical-align: middle;
  display: inline-flex;
  width: 100%
}
#loginSwitchNav ul li a img {
  height: 20px;
  padding-right: 10px;
}
#loginSwitchNav ul li a .selectedView {
  color: #2C486E;
  font-size: 12px;
  padding-top: 5px;
}
#loginSwitchNav ul li a .uiView {
  width: 160px;
}
</style>
</head>
<%
	String onLoadString = "";
	if (org.oscarehr.common.IsPropertiesOn.isCaisiEnable()) {
	    onLoadString += "load();";
	} else {
	 	onLoadString += "refreshAllTabAlerts();scrollOnLoad();";
	 	if (enhancedEnabled && prescribeItEnabled) {
	 	    onLoadString += "refreshRenewalAlert();";
	 	}
	}
	if (userAcceptance != null && !userAcceptance.isAccepted() && showAgreement) {
	    onLoadString += "displayKaiMessage();";
	}
	else if (userAcceptance == null && showAgreement) {
		onLoadString += "displayKaiMessage();";
	}

	if (Boolean.parseBoolean(request.getParameter("omdSessionCheck"))) {
		if (oneIdGatewayData != null && OneIDUtil.isSessionNearExpiry(oneIdGatewayData.getLastKeptActive())
				&& oneIdGatewayData.isSso()) {
			onLoadString += "popupPage(600, 650, '" + OscarProperties.getOscarProBaseUrl() +
					"/api/one-id/login?forward=" + OscarProperties.getOscarProBaseUrl() +
					"/#/one-id/login?reauthenticated=true')";
		}
	}

	// Will check if the access token will expire in less than 3 minutes
	if (oneIdGatewayData != null && oneIdGatewayData.howLongUntilAccessTokenIsExpired() < 180) {
		// If the refresh token is still valid, use that, otherwise reauthenticate entirely
		if (!oneIdGatewayData.isRefreshTokenExpired()) {
			onLoadString += "refreshOAuthToken();";
		} else {
			if (oneIdGatewayData.isSso()) {
				onLoadString += "popupPage(600, 650, '" + OscarProperties.getOscarProBaseUrl() +
						"/api/one-id/login?forward=" + OscarProperties.getOscarProBaseUrl() +
						"/#/one-id/login?reauthenticated=true')";
			}
		}
	}
%>
<body bgcolor="#116754" onLoad="<%=onLoadString%>" topmargin="0" leftmargin="0" rightmargin="0" ng-app="portal.app">
<!-- well Ai Voice Tag -->
<well-ai-voice:script/>
<%
	boolean isTeamScheduleOnly = false;
%>
<security:oscarSec roleName="<%=roleName$%>"
	objectName="_team_schedule_only" rights="r" reverse="false">
<%
	isTeamScheduleOnly = true;
%>
</security:oscarSec>
<%
	int numProvider=0, numAvailProvider=0;
String [] curProvider_no;
String [] curProviderName;
//initial provider bean for all the application
if(providerBean.isEmpty()) {
	for(Provider p : providerDao.getActiveProviders()) {
		 providerBean.setProperty(p.getProviderNo(),p.getFormattedName());
	}
 }
List<Map<String,Object>> resultList = null;
if(mygroupno != null && providerBean.get(mygroupno) != null) { //single appointed provider view
     numProvider=1;
     curProvider_no = new String [numProvider];
     curProviderName = new String [numProvider];
     curProvider_no[0]=mygroupno;
     curProviderName[0]=providerDao.getProvider(mygroupno).getFullName();
} else {
	if(view==0) { //multiple views
	   if (selectedSite!=null) {
		   numProvider = siteDao.site_searchmygroupcount(mygroupno, selectedSite).intValue();
	   }
	   else {
		   numProvider = myGroupDao.getGroupByGroupNo(mygroupno).size();
	   }
       String [] param3 = new String [2];
       param3[0] = mygroupno;
       param3[1] = strDate; //strYear +"-"+ strMonth +"-"+ strDay ;
       numAvailProvider = 0;
       if (selectedSite!=null) {
    	    List<String> siteProviders = providerSiteDao.findByProviderNoBySiteName(selectedSite);
    	  	List<ScheduleDate> results = scheduleDateDao.search_numgrpscheduledate(mygroupno, ConversionUtils.fromDateString(strDate));
    	  	
    	  	for(ScheduleDate result:results) {
    	  		if(siteProviders.contains(result.getProviderNo())) {
    	  			numAvailProvider++;
    	  		}
    	  	}
       }
       else {
    	   	numAvailProvider = scheduleDateDao.search_numgrpscheduledate(mygroupno, ConversionUtils.fromDateString(strDate)).size();
       }
     // _team_schedule_only does not support groups
     // As well, the mobile version only shows the schedule of the login provider.
     if(numProvider==0 || isTeamScheduleOnly || isMobileOptimized) {
       numProvider=1; //the login user
       curProvider_no = new String []{curUser_no};  //[numProvider];
       curProviderName = new String []{(userlastname+", "+userfirstname)}; //[numProvider];
     } else {
       if(request.getParameter("viewall")!=null && request.getParameter("viewall").equals("1") ) {
         if(numProvider >= 5) {lenLimitedL = 2; lenLimitedS = 3; }
       } else {
         if(numAvailProvider >= 5) {lenLimitedL = 2; lenLimitedS = 3; }
         if(numAvailProvider == 2) {lenLimitedL = 20; lenLimitedS = 10; len = 20;}
         if(numAvailProvider == 1) {lenLimitedL = 30; lenLimitedS = 30; len = 30; }
       }
     curProvider_no = new String [numProvider];
     curProviderName = new String [numProvider];
     int iTemp = 0;
     if (selectedSite!=null) {
    	 List<String> siteProviders = providerSiteDao.findByProviderNoBySiteName(selectedSite);
    	 List<MyGroup> results = myGroupDao.getGroupByGroupNo(mygroupno);
		 Collections.sort(results,MyGroup.LastNameComparator);
    	 for(MyGroup result:results) {
    		 if(siteProviders.contains(result.getId().getProviderNo())) {
    			 curProvider_no[iTemp] = String.valueOf(result.getId().getProviderNo());

    			 Provider p = providerDao.getProvider(curProvider_no[iTemp]);
    			 if (p!=null) {
    				 curProviderName[iTemp] = p.getFullName();
    			 }
        	     iTemp++;
    		 }
    	 }
     }
     else {
    	 List<MyGroup> results = myGroupDao.getGroupByGroupNo(mygroupno);
    	 //Collections.sort(results,MyGroup.LastNameComparator);

    	 for(MyGroup result:results) {
    		 curProvider_no[iTemp] = String.valueOf(result.getId().getProviderNo());

    		 Provider p = providerDao.getProvider(curProvider_no[iTemp]);
    		 if (p!=null) {
        		 curProviderName[iTemp] = p.getFullName();
    		 }
    	     iTemp++;
    	 }
     }
    }
   } else { //single view
     numProvider=1;
     curProvider_no = new String [numProvider];
     curProviderName = new String [numProvider];
     curProvider_no[0]=request.getParameter("curProvider");
     curProviderName[0]=request.getParameter("curProviderName");
   }
}
    List<Integer> demographicNumbers = appointmentDao.getDemographicNumbersForCurrentScheduleByProviderNumbers(Arrays.asList(curProvider_no), ConversionUtils.fromDateString(strDate));
	HashMap<Integer, String> ticklers;
	if (demographicNumbers.size() > 0) {
        //Gets all ticklers for the provider numbers found in the curProvider_no array and the current day
	    ticklers = ticklerManager.getTicklersByDemographicsAndDay(demographicNumbers, ConversionUtils.fromDateString(strDate));    
    }
    else {
	    ticklers = new HashMap<Integer, String>();
    }
      UserProperty uppatientNameLength = userPropertyDao.getProp(curUser_no, UserProperty.PATIENT_NAME_LENGTH);
      int NameLength=0;
      if ( uppatientNameLength != null && uppatientNameLength.getValue() != null) {
          try {
             NameLength=Integer.parseInt(uppatientNameLength.getValue());
          } catch (NumberFormatException e) {
             NameLength=0;
          }
          if(NameLength>0) {
             len=lenLimitedS= lenLimitedL = NameLength;
          }
       }
//set timecode bean
String bgcolordef = "#486ebd" ;
String [] param3 = new String[2];
param3[0] = strDate;
for(nProvider=0;nProvider<numProvider;nProvider++) {
     param3[1] = curProvider_no[nProvider];
     List<Object[]> results = scheduleDateDao.search_appttimecode(ConversionUtils.fromDateString(strDate), curProvider_no[nProvider], selectedSite);
     for(Object[] result:results) {
    	 ScheduleTemplate st = (ScheduleTemplate)result[0];
    	 ScheduleDate sd = (ScheduleDate)result[1];
    	 dateTimeCodeBean.put(sd.getProviderNo(), st.getTimecode());
     }
}
	for(ScheduleTemplateCode stc : scheduleTemplateCodeDao.findAll()) {
     dateTimeCodeBean.put("description"+stc.getCode(), stc.getDescription());
     dateTimeCodeBean.put("duration"+stc.getCode(), stc.getDuration());
     dateTimeCodeBean.put("color"+stc.getCode(), (stc.getColor()==null || "".equals(stc.getColor()))?bgcolordef:stc.getColor());
     dateTimeCodeBean.put("confirm" + stc.getCode(), stc.getConfirm());
   }
java.util.Locale vLocale =(java.util.Locale)session.getAttribute(org.apache.struts.Globals.LOCALE_KEY);
    // Third Party Link on main scheduler populates link directly from notes section (Configured from Administration - Schedule Management - Display Settings)
	String tpLinkDisplayString = "";
	String tpLinkTypeString = "";
	Boolean tpLinkEnabled = schedulePreferencesMap.getOrDefault("schedule_tp_link_enabled", false);
	SystemPreferences tpLinkDisplay = SystemPreferencesUtils.findPreferenceByName("schedule_tp_link_display");
	SystemPreferences tpLinkType = SystemPreferencesUtils.findPreferenceByName("schedule_tp_link_type");
	if (tpLinkDisplay != null) {
		tpLinkDisplayString = tpLinkDisplay.getValue();
	}
	if (tpLinkType != null) {
		tpLinkTypeString = tpLinkType.getValue();
	}
	// Eligibility Link on main schedule - toggled from Administration - Schedule Management - Display Settings)
	Boolean eligibilityEnabled = schedulePreferencesMap.getOrDefault("schedule_eligibility_enabled", false);
	TeleplanEligibilityDao teleplanEligibilityDao = SpringUtils.getBean(TeleplanEligibilityDao.class);
%>
<div style="display: flex;">
		<%
		ThirdPartyApplicationDao thirdPartyApplicationDao = SpringUtils.getBean(ThirdPartyApplicationDao.class);
		List<ThirdPartyApplication> apps = thirdPartyApplicationDao.findByEmrContext(EmrContextEnum.SCHEDULE);
		if (apps != null && !apps.isEmpty()) {
     	%>
			<jsp:include page="../common/thirdPartyAppSideBar.jsp">
				<jsp:param name="context" value="<%= EmrContextEnum.SCHEDULE %>"/>
				<jsp:param name="providerNo" value="<%= loggedInInfo1.getLoggedInProvider().getProviderNo() %>"/>
			</jsp:include>
	 	</td>
	<%
     	}
	%>
	<div style="flex: auto;" id="contentContainer">
<div id="helpHtml">
	<div class="help-title">Help
		<a href="javascript:void(0)" class="help-close" onclick="document.getElementById('helpHtml').style.right='-280px';document.getElementById('helpHtml').style.display='none'">(X)</a>
	</div>
	<div class="help-body">
		<%=resourcehelpHtml%>
	</div>
</div>
<div class="header-div"style="width: 100%; position: fixed; background-color: #116754;">
<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%" id="firstTable" style="padding:10px 7px; color: #fff">
<tr>
<td align="center" >
<%
if(enableKaiEmr) {
%>
<a style="cursor: pointer;" onClick="toggleLoginSwitch()" title="Switch between Pro and Classic Views"><img src="../images/Oscar_Pro-white.svg" border="0"></a>
<% } else { %>
<a href="../web/" title="OSCAR EMR"><img src="../images/Oscar_Pro-white.svg" border="0"></a><!-- <img src="<%=request.getContextPath()%>/images/oscar_small.png" border="0"> -->
<% } %>
</td>
<td id="firstMenu" style="padding-left: 5px;">
<ul id="navlist">
<logic:notEqual name="infirmaryView_isOscar" value="false">
<% if(request.getParameter("viewall")!=null && request.getParameter("viewall").equals("1") ) { %>
         <li>
         <a href=# onClick = "view('0')" title="<bean:message key="provider.appointmentProviderAdminDay.viewProvAval"/>"><bean:message key="provider.appointmentProviderAdminDay.schedView"/></a>
         </li>
 <% } else {  %>
 <li>
 <a href='providercontrol.jsp?year=<%=curYear%>&month=<%=curMonth%>&day=<%=curDay%>&view=0&displaymode=day&dboperation=searchappointmentday&viewall=1'><bean:message key="provider.appointmentProviderAdminDay.schedView"/></a>
 </li>
         
<% } %>
</logic:notEqual>
 <li>
 <a href='providercontrol.jsp?year=<%=curYear%>&month=<%=curMonth%>&day=<%=curDay%>&view=0&displaymode=day&dboperation=searchappointmentday&caseload=1&clProv=<%=curUser_no%>'><bean:message key="global.caseload"/></a>
 </li>
 <%
 	if (isMobileOptimized) {
 %>
        <!-- Add a menu button for mobile version, which opens menu contents when clicked on -->
        <li id="menu"><a class="leftButton top" onClick="showHideItem('navlistcontents');">
                <bean:message key="global.menu" /></a>
            <ul id="navlistcontents" style="display:none;">
<% } %>
<security:oscarSec roleName="<%=roleName$%>" objectName="_search" rights="r">
 <li id="search">
    <caisi:isModuleLoad moduleName="caisi">
    	<%
    		String caisiSearch = oscarVariables.getProperty("caisi.search.workflow", "true");
    		if("true".equalsIgnoreCase(caisiSearch)) {
    	%>
    	<a HREF="../PMmodule/ClientSearch2.do" TITLE='<bean:message key="global.searchPatientRecords"/>' OnMouseOver="window.status='<bean:message key="global.searchPatientRecords"/>' ; return true"><bean:message key="provider.appointmentProviderAdminDay.search"/></a>
    	<%
    		} else {
    	%>
       	 <a HREF="#" ONCLICK ="popupPage2('../demographic/search.jsp');return false;"  TITLE='<bean:message key="global.searchPatientRecords"/>' OnMouseOver="window.status='<bean:message key="global.searchPatientRecords"/>' ; return true"><bean:message key="provider.appointmentProviderAdminDay.search"/></a>
   	<% } %>
    </caisi:isModuleLoad>
    <caisi:isModuleLoad moduleName="caisi" reverse="true">
       <a HREF="#" ONCLICK ="popupPage2('../demographic/search.jsp');return false;"  TITLE='<bean:message key="global.searchPatientRecords"/>' OnMouseOver="window.status='<bean:message key="global.searchPatientRecords"/>' ; return true"><bean:message key="provider.appointmentProviderAdminDay.search"/></a>
    </caisi:isModuleLoad>
</li>
</security:oscarSec>
<caisi:isModuleLoad moduleName="TORONTO_RFQ" reverse="true">
<security:oscarSec roleName="<%=roleName$%>" objectName="_report" rights="r">
<li>
    <a HREF="#" ONCLICK ="popupPage2('../report/reportindex.jsp','reportPage');return false;"   TITLE='<bean:message key="global.genReport"/>' OnMouseOver="window.status='<bean:message key="global.genReport"/>' ; return true"><bean:message key="global.report"/></a>
</li>
</security:oscarSec>
<oscar:oscarPropertiesCheck property="NOT_FOR_CAISI" value="no" defaultVal="true">
<% if (billingR) { %>
<li>
	<a HREF="#" ONCLICK ="popupPage2('../billing/CA/<%=prov%>/billingReportCenter.jsp?displaymode=billreport&providerview=<%=curUser_no%>');return false;" TITLE='<bean:message key="global.genBillReport"/>' onMouseOver="window.status='<bean:message key="global.genBillReport"/>';return true"><bean:message key="global.billing"/></a>
</li>
<% } %>
<% if (appointmentDoctorLinkR) { %>
   <li>
	   <% if (enhancedEnabled) { %>
       <a href="javascript:popupInboxManager('/<%=OscarProperties.getKaiemrDeployedContext()%>/#/inbox/?providerNo=<%=curUser_no%>');" TITLE='<bean:message key="provider.appointmentProviderAdminDay.viewLabReports"/>'>
       
	   <% } else { %>
           <a HREF="#" ONCLICK ="popupInboxManager('../dms/inboxManage.do?method=prepareForIndexPage&providerNo=<%=curUser_no%>', 'Lab');return false;" TITLE='<bean:message key="provider.appointmentProviderAdminDay.viewLabReports"/>'>
	   <% } %>
	   <span id="oscar_new_lab"><bean:message key="global.lab"/></span>
       </a>
       <oscar:newUnclaimedLab>
				   <% if (enhancedEnabled) { %>
           <a class="tabalert" href="javascript:popupInboxManager('/<%=OscarProperties.getKaiemrDeployedContext()%>/#/inbox/?providerNo=0');" TITLE='<bean:message key="provider.appointmentProviderAdminDay.viewLabReports"/>'>*</a>
				   <% } else { %>
           <a class="tabalert" HREF="#" ONCLICK ="popupInboxManager('../dms/inboxManage.do?method=prepareForIndexPage&providerNo=0&searchProviderNo=0&status=N&lname=&fname=&hnum=&pageNum=1&startIndex=0', 'Lab');return false;" TITLE='<bean:message key="provider.appointmentProviderAdminDay.viewLabReports"/>'>*</a>
				   <% } %>
       </oscar:newUnclaimedLab>
   </li>
<% } %>
</oscar:oscarPropertiesCheck>
 </caisi:isModuleLoad>
  	<%
        	String pNo1 = (String) session.getAttribute("user");
        	String valus = "";
        	if (enableUrgentMessages) {
				valus = MsgMessageData.getUrgentList(pNo1);
			}
     %>
 <caisi:isModuleLoad moduleName="TORONTO_RFQ" reverse="true">
 	<security:oscarSec roleName="<%=roleName$%>" objectName="_msg" rights="r">
     <li><input type="hidden" id="urgentVls"  value="<%=valus%>"/>
	 <a HREF="#" ONCLICK ="popupOscarRx(600,1024,'../oscarMessenger/DisplayMessages.do?providerNo=<%=curUser_no%>&userName=<%=URLEncoder.encode(userfirstname+" "+userlastname)%>')" title="<bean:message key="global.messenger"/>">
	 <span id="oscar_new_msg"><bean:message key="global.msg"/></span></a>
     </li>
   	</security:oscarSec>
	 <% if (enableUrgentMessages) { %>
		 <script>
			 const urgentVls = document.getElementById("urgentVls").value.replace(/(^\s*)|(\s*$)/g, "");
			 const urgentGrams = urgentVls.split(",");
			 const reg = new RegExp("^[0-9]*$");
			 const XX = (window.pageXOffset ? window.pageXOffset : window.document.body.scrollLeft);
			 const YY = (window.pageYOffset ? window.pageYOffset : window.document.body.scrollTop);
			 for (let i = 0; i < urgentGrams.length; i++) {
				 if (reg.test(urgentGrams[i]) && urgentGrams[i].replace(/(^\s*)|(\s*$)/g, "") !== '' && urgentGrams[i].replace(/(^\s*)|(\s*$)/g, "") != null){
					 window.open("../oscarMessenger/ViewMessage.do?messageID="+urgentGrams[i].replace(/(^\s*)|(\s*$)/g, "")+"&boxType=0", "newwin", "height="+ XX + "width=" + YY + ",toolbar=no,menubar=no");
				 }
			 }
		 </script>
	 <% } %>
 </caisi:isModuleLoad>
	<security:oscarSec roleName="<%=roleName$%>" objectName="_msg" rights="r">
		<% if (enhancedEnabled && prescribeItEnabled) { %>
		<li>
			<a HREF="#" ONCLICK ="popupMsg(900,1200,'/kaiemr/app/components/messages/')" title="<bean:message key="global.renewal"/>">
				<span id="oscar_new_renewal"><bean:message key="global.renewal"/></span></a>
		</li>
		<% } %>
	</security:oscarSec>
<caisi:isModuleLoad moduleName="TORONTO_RFQ" reverse="true">
<security:oscarSec roleName="<%=roleName$%>" objectName="_con" rights="r">
<li id="con">
 <a HREF="#" ONCLICK ="popupOscarRx(625,1024,'../oscarEncounter/IncomingConsultation.do?providerNo=<%=curUser_no%>&userName=<%=URLEncoder.encode(userfirstname+" "+userlastname)%>')" title="<bean:message key="provider.appointmentProviderAdminDay.viewConReq"/>">
 <span id="oscar_aged_consults"><bean:message key="global.con"/></span></a>
</li>
</security:oscarSec>
</caisi:isModuleLoad>
 <%
 boolean hide_eConsult = SystemPreferencesUtils
		 .isReadBooleanPreferenceWithDefault("hide_econsult_link", false);
 if("on".equalsIgnoreCase(prov) && !hide_eConsult){
 %>
<oscar:oscarPropertiesCheck property="enable_econsult" value="true" defaultVal="false">
 <li id="econ">
	<a href="#" onclick ="popupOscarRx(625, 1024, '../oscarEncounter/econsult.do')" title="eConsult">
 	<span>eConsult</span></a>
</li>
</oscar:oscarPropertiesCheck>
<% } %>
<security:oscarSec roleName="<%=roleName$%>" objectName="_ehr" rights="r">
<% if(!StringUtils.isEmpty(clinicalConnectEndpoint) && SystemPreferencesUtils.isOneIdEnabled()
		&& SystemPreferencesUtils.isClinicalConnectEnabled()) { %>
<li id="clinical_connect">
	<a href="#" onclick ="popupEHRService('/<%=OscarProperties.getKaiemrDeployedContext()%>/api/v1/clinical-connect/launch', null, '<%= OscarProperties.getOscarProBaseUrl()%>')" title="Clinical Connect Viewer">
 	<span>Clinical Connect</span></a>
</li>
<% } %>
</security:oscarSec>
<security:oscarSec roleName="<%=roleName$%>" objectName="_pref" rights="r">
<li>    <!-- remove this and let providerpreference check -->
    <caisi:isModuleLoad moduleName="ticklerplus">
	<a href=# onClick ="popupPage(715,680,'providerpreference.jsp?provider_no=<%=curUser_no%>&start_hour=<%=startHour%>&end_hour=<%=endHour%>&every_min=<%=everyMin%>&mygroup_no=<%=mygroupno%>&new_tickler_warning_window=<%=Encode.forJavaScriptBlock(newticklerwarningwindow)%>&default_pmm=<%=Encode.forJavaScriptBlock(default_pmm)%>&caisiBillingPreferenceNotDelete=<%=Encode.forJavaScriptBlock(caisiBillingPreferenceNotDelete)%>&tklerproviderno=<%=Encode.forJavaScriptBlock(tklerProviderNo)%>');return false;" TITLE='<bean:message key="provider.appointmentProviderAdminDay.msgSettings"/>' OnMouseOver="window.status='<bean:message key="provider.appointmentProviderAdminDay.msgSettings"/>' ; return true"><bean:message key="global.pref"/></a>
    </caisi:isModuleLoad>
    <caisi:isModuleLoad moduleName="ticklerplus" reverse="true">
	<a href=# onClick ="popupPage(715,680,'providerpreference.jsp?provider_no=<%=curUser_no%>&start_hour=<%=startHour%>&end_hour=<%=endHour%>&every_min=<%=everyMin%>&mygroup_no=<%=mygroupno%>');return false;" TITLE='<bean:message key="provider.appointmentProviderAdminDay.msgSettings"/>' OnMouseOver="window.status='<bean:message key="provider.appointmentProviderAdminDay.msgSettings"/>' ; return true"><bean:message key="global.pref"/></a>
    </caisi:isModuleLoad>
</li>
</security:oscarSec>
 <caisi:isModuleLoad moduleName="TORONTO_RFQ" reverse="true">
<security:oscarSec roleName="<%=roleName$%>" objectName="_edoc" rights="r">
<li>
   <a HREF="#" onclick="popup('700', '1024', '../dms/documentReport.jsp?function=provider&functionid=<%=curUser_no%>&curUser=<%=curUser_no%>', 'edocView');" TITLE='<bean:message key="provider.appointmentProviderAdminDay.viewEdoc"/>'><bean:message key="global.edoc"/></a>
</li>
</security:oscarSec>
 </caisi:isModuleLoad>
<% if (ticklerR) { %>
<li>
   <caisi:isModuleLoad moduleName="ticklerplus" reverse="true">
    <a HREF="#" ONCLICK ="popupPage2('../tickler/ticklerMain.jsp','<bean:message key="global.tickler"/>');return false;" TITLE='<bean:message key="global.tickler"/>'>
	<span id="oscar_new_tickler"><bean:message key="global.btntickler"/></span></a>
   </caisi:isModuleLoad>
   <caisi:isModuleLoad moduleName="ticklerplus">
    <a HREF="#" ONCLICK ="popupPage2('../Tickler.do?filter.assignee=<%=curUser_no%>&filter.demographic_no=&filter.demographic_webName=','<bean:message key="global.tickler"/>');return false;" TITLE='<bean:message key="global.tickler"/>'+'+'>
	<span id="oscar_new_tickler"><bean:message key="global.btntickler"/></span></a>
   </caisi:isModuleLoad>
</li>
<% } %>
<oscar:oscarPropertiesCheck property="OSCAR_LEARNING" value="yes">
<li>
    <a HREF="#" ONCLICK ="popupPage2('../oscarLearning/CourseView.jsp','<bean:message key="global.courseview"/>');return false;" TITLE='<bean:message key="global.courseview"/>'>
	<span id="oscar_courseview"><bean:message key="global.btncourseview"/></span></a>
</li>
</oscar:oscarPropertiesCheck>

<% if (showReferralMenu) { %>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.misc" rights="r">
<li id="ref">
 <a href="#" onclick="popupPage(550,800,'../admin/ManageBillingReferral.do');return false;"><bean:message key="global.manageReferrals"/></a>
</li>
</security:oscarSec>
<% } %>
<oscar:oscarPropertiesCheck property="WORKFLOW" value="yes">
   <li><a href="javascript: function myFunction() {return false; }" onClick="popup(700,1024,'../oscarWorkflow/WorkFlowList.jsp','<bean:message key="global.workflow"/>')"><bean:message key="global.btnworkflow"/></a></li>
</oscar:oscarPropertiesCheck>
    <myoscar:indivoRegistered provider="<%=curUser_no%>">
		<%
			MyOscarUtils.attemptMyOscarAutoLoginIfNotAlreadyLoggedInAsynchronously(loggedInInfo1, false);
		%>
	    <li>
			<a HREF="#" ONCLICK ="popup('600', '1024','../phr/PhrMessage.do?method=viewMessages','INDIVOMESSENGER2<%=curUser_no%>')" title='<bean:message key="global.phr"/>'>
				<bean:message key="global.btnphr"/>
				<div id="unreadMessagesMenuMarker" style="display:inline-block;vertical-align:top"><!-- place holder for unread message count --></div>
			</a>
			<script type="text/javascript">
				function pollMessageCount()
				{
					jQuery('#unreadMessagesMenuMarker').load('<%=request.getContextPath()%>/phr/msg/unread_message_count.jsp?autoRefresh=true', pollMessageCallback)
				}
				window.setInterval(pollMessageCount, 60000);
				window.setTimeout(pollMessageCount, 2000);
			</script>
	    </li>
	</myoscar:indivoRegistered>
    <phr:phrNotRegistered provider="<%=curUser_no%>">
    		<li>
			<a HREF="#" ONCLICK ="popup('600', '1024','../phr/PHRSignup.jsp','INDIVOMESSENGER2<%=curUser_no%>')" title='<bean:message key="global.phr"/>'>
				<bean:message key="global.btnphr"/>
			</a>
	    </li>
    </phr:phrNotRegistered>
<%if(appManager.isK2AEnabled()){ %>
<li>
	<a href="javascript:void(0);" id="K2ALink">K2A<span><sup id="k2a_new_notifications"></sup></span></a>
	<script type="text/javascript">
		function getK2AStatus(){
			jQuery.get( "../ws/rs/resources/notifications/number", function( data ) {
				  if(data === "-"){ //If user is not logged in
					  jQuery("#K2ALink").click(function() {
						var win = window.open('../apps/oauth1.jsp?id=K2A','appAuth','width=700,height=450,scrollbars=1');
						win.focus();
					  });
				   }else{
					  jQuery("#k2a_new_notifications").text(data); 
					  jQuery("#K2ALink").click(function() {
						var win = window.open('../apps/notifications.jsp','appAuth','width=450,height=700,scrollbars=1');
						win.focus();
					  });
				   }
			});
		}
		getK2AStatus();
	</script>
</li>
<%}%>
<caisi:isModuleLoad moduleName="TORONTO_RFQ" reverse="true">
	<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.userAdmin,_admin.schedule,_admin.billing,_admin.resource,_admin.reporting,_admin.backup,_admin.messenger,_admin.eform,_admin.encounter,_admin.misc,_admin.fax" rights="r">
	<%
	String url = oscar.OscarProperties.getInstance().getProperty("cyclesLink");
	if (cyclesLink != null && !cyclesLink.isEmpty()) {
		String params = "providerNo=" + curUser_no;
		if (cyclesClientName == null || request.getServerName().startsWith(cyclesClientName)) {
			url = cyclesLink + "?" + params;
		} else {
			url = cyclesLocalLink + "?" + params;
		}
	%>
	<li id="cycles">
		<a href = '<%=url%>'
		    target = "_blank"
		    onclick = "window.open('<%=url%>','_blank'); return false;">
		    <bean:message key = "global.cycles" />
		</a>
	</li>
	<% } %>

	<%
		if (StringUtils.trimToNull(scheduleSecondCyclesLink) != null) {
	%>
			<li>
				<a href = '<%= scheduleSecondCyclesLink %>'
				    target = "_blank"
				    onclick = "window.open('<%= scheduleSecondCyclesLink %>', '_blank'); return false;">
				    <bean:message key = "global.cl" />
				</a>
			</li>
	<% } %>

<li id="admin2">
 <a href="javascript:void(0)" id="admin-panel" TITLE='Administration Panel' onclick="newWindow('<%=request.getContextPath()%>/administration/','admin')">Administration</a>
</li>
</security:oscarSec>
	</caisi:isModuleLoad>
<security:oscarSec roleName="<%=roleName$%>" objectName="_dashboardDisplay" rights="r">
	<% 
		DashboardManager dashboardManager = SpringUtils.getBean(DashboardManager.class);
		List<Dashboard> dashboards = dashboardManager.getActiveDashboards(loggedInInfo1);
		pageContext.setAttribute("dashboards", dashboards);
	%>
	<li id="dashboardList">
		 <div class="dropdown">
			<a href="#" class="dashboardBtn">Dashboard</a>
			<div class="dashboardDropdown">
				<c:forEach items="${ dashboards }" var="dashboard" >			
					<a href="javascript:void(0)" onclick="newWindow('<%=request.getContextPath()%>/web/dashboard/display/DashboardDisplay.do?method=getDashboard&dashboardId=${ dashboard.id }','dashboard')"> 
						<c:out value="${ dashboard.name }" />
					</a>
				</c:forEach>
				<security:oscarSec roleName="<%=roleName$%>" objectName="_dashboardCommonLink" rights="r">
					<a href="javascript:void(0)" onclick="newWindow('<%=request.getContextPath()%>/web/dashboard/display/sharedOutcomesDashboard.jsp','shared_dashboard')"> 
						Common Provider Dashboard
					</a>
				</security:oscarSec>
			</div>
		</div>
	</li>
</security:oscarSec>
<% if (OscarProperties.getInstance().hasProperty("kaiemr_lab_queue_url")) { %>
	<li>
		<a href="javascript:void(0)" id="work_lab_button" title='Lab Queue' onclick="popupPage2('/<%=OscarProperties.getKaiemrDeployedContext()%>/app/components/labqueue/','work_queue', 700, 1215)">Lab Queue</a>
	</li>
<% }
	boolean allowOnlineBookingSystemPreferenceEnabled = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("allow_online_booking", true);
	UserProperty onlineBook = userPropertyDao.getProp(loggedInInfo1.getLoggedInProviderNo(), "allow_online_booking");
	Boolean providerAllowOnlineBooking = false;
	if (onlineBook != null && onlineBook.getValue() != null && !onlineBook.getValue().isEmpty())
	{
	    if (Boolean.parseBoolean(onlineBook.getValue()))
		{
			providerAllowOnlineBooking = true;
		}
	}
	if (allowOnlineBookingSystemPreferenceEnabled && providerAllowOnlineBooking) { %>
	<li>
		<a href="javascript:void(0)" id="online_booking_button" title='Online Booking' onclick="popupPage2('/<%=OscarProperties.getKaiemrDeployedContext()%>/app/components/onlinebooking/#!/patientModule','work_queue', 700, 1215)">Online Booking</a>
	</li>
<% } %>
  <!-- Added logout link for mobile version -->
  <li id="logoutMobile">
      <a href="../logout.jsp"><bean:message key="global.btnLogout"/></a>
  </li>
<!-- plugins menu extension point add -->
<%
	int pluginMenuTagNumber=0;
%>
<plugin:pageContextExtension serviceName="oscarMenuExtension" stemFromPrefix="Oscar"/>
<logic:iterate name="oscarMenuExtension.points" id="pt" scope="page" type="oscar.caisi.OscarMenuExtension">
<%
	if (oscar.util.plugin.IsPropertiesOn.propertiesOn(pt.getName().toLowerCase())) {
	pluginMenuTagNumber++;
%>
       <li><a href='<html:rewrite page="<%=pt.getLink()%>"/>'>
       <%=pt.getName()%></a></li>
<%
	}
%>
</logic:iterate>
<!-- plugin menu extension point add end-->
<%
	int menuTagNumber=0;
%>
<caisi:isModuleLoad moduleName="caisi">
   <li>
     <a href='<html:rewrite page="/PMmodule/ProviderInfo.do"/>'>Program</a>
     <%
     	menuTagNumber++ ;
     %>
   </li>
</caisi:isModuleLoad>
<% if (isMobileOptimized) { %>
    </ul></li> <!-- end menu list for mobile-->
<% } %>
</ul>  <!--- old TABLE -->
</td>
<td align="right" style="white-space: nowrap;">
	<a href="javascript: function myFunction() {return false; }" onClick="popup(700,1024,'../scratch/index.jsp','scratch')"><span id="oscar_scratch"></span></a>&nbsp;
	<%if(resourcehelpHtml==""){ %>
		<a href="javascript:void(0)" onClick ="popupPage(600,750,'<%=resourcebaseurl%>')"><bean:message key="global.help"/></a>
	<%}else{%>
<div id="help-link">
	    <a href="javascript:void(0)" onclick="document.getElementById('helpHtml').style.display='block';document.getElementById('helpHtml').style.right='0px';"><bean:message key="global.help"/></a>
</div>
	<%}%>

		<%  	if(loggedInInfo1.getOneIdGatewayData() != null) { %>
				| <a href="#" onclick="confirmNGo('<%=request.getContextPath()%>/logout.jsp', 'Please close all additional windows potentially containing Patient Health Information.')"><bean:message key="global.btnLogout"/>&nbsp;</a>
				<% } else { %>
				| <a href="../logout.jsp"><bean:message key="global.btnLogout"/>&nbsp;</a>
				<% } %>
</td>
</tr>
</table>
<%
if (enableKaiEmr) {
	UserProperty hasUserSeenNotification = userPropertyDao.getProp(curUser_no, UserProperty.ACKNOWLEDGED_LOGIN_SWITCH_NOTIFICATION);
 	boolean hasAcknowledgedNotification = false;
 	if (hasUserSeenNotification == null || !Boolean.parseBoolean(hasUserSeenNotification.getValue())) {
 %>
<div class="loginSwitchNotification" id="loginSwitchNotification" style="background: rgb(53, 54, 58); color: white; position: absolute; left:42px; top: 5px;  border: 1px solid rgb(53, 54, 58);
  border-radius: 4px; width: 345px; padding: 15px; font-family: nimbus-sans, sans-serif; font-style: normal; font-weight: normal;
  font-size: 15px; line-height: 18px;">
	<div><h3 style="margin: 0px;">New</h3></div>
	<div style="font-size: 13px;">Click this icon to toggle between the available Classic and PRO UI screens. OSCAR will automatically save your last preference to hold that view.</div>
	<button onClick ="acknowledgeLoginSwitchNotification()" style="float: right; background: #00C39C; border-radius: 4px; border: none; padding: 8px 15px; font-family: nimbus-sans, sans-serif; font-style: normal; font-weight: normal; font-size: 15px; line-height: 18px; color: white;">Got it!</button>
</div>
<div class="loginSwitchNotification" style="position: absolute; top: 12px; left:33px;">
	<span style='font-size:24px; color: rgb(53, 54, 58); transform: rotate(-45deg); display: inline-block'>&#9700; </span>
</div>
<% } %>
<div id="loginSwitchNav">
	<ul>
		<li>
			<a href='<%=request.getContextPath()%>/provider/switchLoginType.do?loginType=C'>
				<img src="../images/oscar_logo_small.png" border="0">
				<span class="uiView">Classic view</span>
			<% if (!enhancedEnabled) { %>
				<span class="selectedView"><i class="icon-ok"></i></span>
			<% } %>
			</a>
		</li>
		<li>
			<a href='<%=request.getContextPath()%>/provider/switchLoginType.do?loginType=E'>
				<img src="../images/OSCARPro-icon.png" border="0">
				<span class="uiView">PRO view</span>
			<% if (enhancedEnabled) { %>
				<span class="selectedView"><i class="icon-ok"></i></span>
			<% } %>
			</a>
		</li>
	</ul>
</div>
<% } %>
<script>
		jQuery.get("<%=request.getContextPath()%>/SystemMessage.do","method=view&autoRefresh=true",function(data,textStatus){
			jQuery("#system_message").html(data);
		});
		jQuery.get("<%=request.getContextPath()%>/FacilityMessage.do","method=view&autoRefresh=true",function(data,textStatus){
			jQuery("#facility_message").html(data);
		});
		jQuery.get("<%=request.getContextPath()%>/servlet/OscarProviderMessage","autoRefresh=true",function(data,textStatus){
			jQuery("#oscar_provider_message").html(data);
		});
		
</script>
<div id="system_message"></div>
<div id="facility_message"></div>
<div class="alert-bar">
    <strong id="oscar_provider_message"></strong>
</div>
<div class="alert-bar">
	<strong id="oscar_provider_message"></strong>
</div>
<%
	if (!caseload) {
%>
<div style="width: 100%;">
<table BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%" BGCOLOR="#fff">
<tr id="ivoryBar" style="color:#fff;">
<td id="dateAndCalendar" width="33%" style="padding:7px">
 <a class="redArrow" href="providercontrol.jsp?year=<%=year%>&month=<%=month%>&day=<%=isWeekView?(day-7):(day-1)%>&view=<%=view==0?"0":("1&curProvider="+request.getParameter("curProvider")+"&curProviderName="+URLEncoder.encode(request.getParameter("curProviderName"),"UTF-8") )%>&displaymode=day&dboperation=searchappointmentday<%=isWeekView?"&provider_no="+provNum:""%>&viewall=<%=viewall%>">
 &nbsp;&nbsp;<img src="../images/previous.png" style="margin-bottom: -3px" BORDER="0" ALT="<bean:message key="provider.appointmentProviderAdminDay.viewPrevDay"/>"></a>
 <b><span class="dateAppointment"><%
 	if (isWeekView) {
 %><bean:message key="provider.appointmentProviderAdminDay.week"/> <%=week%><%
 	} else {
 %><%=formatDate%><%
 	}
 %></span></b>
 <a class="redArrow" href="providercontrol.jsp?year=<%=year%>&month=<%=month%>&day=<%=isWeekView?(day+7):(day+1)%>&view=<%=view==0?"0":("1&curProvider="+request.getParameter("curProvider")+"&curProviderName="+URLEncoder.encode(request.getParameter("curProviderName"),"UTF-8") )%>&displaymode=day&dboperation=searchappointmentday<%=isWeekView?"&provider_no="+provNum:""%>&viewall=<%=Encode.forUriComponent(viewall)%>">
 <img src="../images/next.png" style="margin-bottom: -3px" BORDER="0" class="noprint" ALT="<bean:message key="provider.appointmentProviderAdminDay.viewNextDay"/>">&nbsp;&nbsp;</a>
<a id="calendarLink" href=# onClick ="popupPage(425,430,'../share/CalendarPopup.jsp?urlfrom=<%=request.getContextPath()%>/provider/providercontrol.jsp&year=<%=strYear%>&month=<%=strMonth%>&param=<%=URLEncoder.encode("&view=0&displaymode=day&dboperation=searchappointmentday&viewall="+Encode.forUriComponent(viewall),"UTF-8")%><%=isWeekView?URLEncoder.encode("&provider_no="+provNum, "UTF-8"):""%>')"><bean:message key="global.calendar"/></a>
<logic:notEqual name="infirmaryView_isOscar" value="false">
| <% if(request.getParameter("viewall")!=null && request.getParameter("viewall").equals("1") ) { %>
 <!-- <span style="color:#333"><bean:message key="provider.appointmentProviderAdminDay.viewAll"/></span> -->
 <u><a href=# onClick = "review('0')" title="<bean:message key="provider.appointmentProviderAdminDay.viewAllProv"/>"><bean:message key="provider.appointmentProviderAdminDay.schedView"/></a></u>
 
<%}else{%>
	<u><a href=# onClick = "review('1')" title="<bean:message key="provider.appointmentProviderAdminDay.viewAllProv"/>"><bean:message key="provider.appointmentProviderAdminDay.viewAll"/></a></u>
<%}%>
</logic:notEqual>
<caisi:isModuleLoad moduleName="TORONTO_RFQ" reverse="true">
<security:oscarSec roleName="<%=roleName$%>" objectName="_day" rights="r">
 | <a class="rightButton top" href="providercontrol.jsp?year=<%=curYear%>&month=<%=curMonth%>&day=<%=curDay%>&view=<%=view==0?"0":("1&curProvider="+request.getParameter("curProvider")+"&curProviderName="+URLEncoder.encode(request.getParameter("curProviderName"),"UTF-8") )%>&displaymode=day&dboperation=searchappointmentday&viewall=<%=Encode.forUriComponent(viewall)%>" TITLE='<bean:message key="provider.appointmentProviderAdminDay.viewDaySched"/>' OnMouseOver="window.status='<bean:message key="provider.appointmentProviderAdminDay.viewDaySched"/>' ; return true"><bean:message key="global.today"/></a>
</security:oscarSec>
<security:oscarSec roleName="<%=roleName$%>" objectName="_month" rights="r">
   | <a href="providercontrol.jsp?year=<%=year%>&month=<%=month%>&day=1&view=<%=view==0?"0":("1&curProvider="+request.getParameter("curProvider")+"&curProviderName="+URLEncoder.encode(request.getParameter("curProviderName"),"UTF-8") )%>&displaymode=month&dboperation=searchappointmentmonth" TITLE='<bean:message key="provider.appointmentProviderAdminDay.viewMonthSched"/>' OnMouseOver="window.status='<bean:message key="provider.appointmentProviderAdminDay.viewMonthSched"/>' ; return true"><bean:message key="global.month"/></a>
 </security:oscarSec>
</caisi:isModuleLoad>
<%
	boolean anonymousEnabled = false;
	if (loggedInInfo1.getCurrentFacility() != null) {
		anonymousEnabled = loggedInInfo1.getCurrentFacility().isEnableAnonymous();
	}
	if(anonymousEnabled) {
%>
&nbsp;&nbsp;(<a href="#" onclick="popupPage(710, 1024,'<html:rewrite page="/PMmodule/createAnonymousClient.jsp"/>?programId=<%=(String)session.getAttribute(SessionConstants.CURRENT_PROGRAM_ID)%>');return false;">New Anon Client</a>)
<%
	}
%>
<%
	boolean epe = false;
	if (loggedInInfo1.getCurrentFacility() != null) {
		epe = loggedInInfo1.getCurrentFacility().isEnablePhoneEncounter();
	}
	if(epe) {
%>
&nbsp;&nbsp;(<a href="#" onclick="popupPage(710, 1024,'<html:rewrite page="/PMmodule/createPEClient.jsp"/>?programId=<%=(String)session.getAttribute(SessionConstants.CURRENT_PROGRAM_ID)%>');return false;">Phone Encounter</a>)
<%
	}
%>
<% if (oscarProperties.getProperty("billregion", "ON").equals("ON")) { %>
<input type='button' value="<bean:message key="provider.appointmentProviderAdminDay.hcv"/>" name='hcvView' onClick=bulkHCV() title="<bean:message key="provider.appointmentProviderAdminDay.hcv"/>" style="color:black" class="noprint">
<% } %>
</td>
<td class="title noprint" ALIGN="center" width="33%" style="padding:7px">
<%
	if (isWeekView) {
for(int provIndex=0;provIndex<numProvider;provIndex++) {
if (curProvider_no[provIndex].equals(provNum)) {
%>
<bean:message key="provider.appointmentProviderAdminDay.weekView"/>: <%=Encode.forHtml(curProviderName[provIndex])%>
<%
	} } } else { if (view==1) {
%>
<a href='providercontrol.jsp?year=<%=strYear%>&month=<%=strMonth%>&day=<%=strDay%>&view=0&displaymode=day&dboperation=searchappointmentday'><bean:message key="provider.appointmentProviderAdminDay.grpView"/></a>
<% } else { %>
<% if (!isMobileOptimized) { %> <bean:message key="global.hello"/> <% } %>
<% out.println( Encode.forHtmlContent(userfirstname+" "+userlastname)); %>
</td>
<% } } %>
<td id="group" ALIGN="RIGHT" style="padding:7px"><!-- BGCOLOR="ivory" -->
<caisi:isModuleLoad moduleName="TORONTO_RFQ" reverse="true">
<form method="post" name="findprovider" onSubmit="findProvider(<%=year%>,<%=month%>,<%=day%>);return false;" target="apptReception" action="receptionistfindprovider.jsp" style="display:inline;margin:0px;padding:0px;padding-right:10px">
<INPUT TYPE="text" NAME="providername" VALUE="" WIDTH="2" HEIGHT="10" border="0" size="10" maxlength="10" class="noprint" title="Find a Provider" placeholder="Enter Lastname">
<INPUT TYPE="SUBMIT" NAME="Go" VALUE='<bean:message key="provider.appointmentprovideradminmonth.btnGo"/>' class="noprint" onClick="findProvider(<%=year%>,<%=month%>,<%=day%>);return false;">
</form>
</caisi:isModuleLoad>
<form name="appointmentForm" style="display:inline;margin:0px;padding:0px;">
<% if (isWeekView) { %>
<bean:message key="provider.appointmentProviderAdminDay.provider"/>:
<select name="provider_select" onChange="goWeekView(this.options[this.selectedIndex].value)">
<%
	for (nProvider=0;nProvider<numProvider;nProvider++) {
%>
<option value="<%=Encode.forHtmlAttribute(curProvider_no[nProvider])%>"<%=curProvider_no[nProvider].equals(provNum)?" selected":""%>><%=Encode.forHtmlContent(curProviderName[nProvider])%></option>
<%
	}
%>
</select>
<%
	} else {
%>
<caisi:isModuleLoad moduleName="caisi">
<table><tr><td align="right">
    <caisi:ProgramExclusiveView providerNo="<%=curUser_no%>" value="appointment">
	<%
		session.setAttribute("infirmaryView_isOscar", "true");
	%>
    </caisi:ProgramExclusiveView>
    <caisi:ProgramExclusiveView providerNo="<%=curUser_no%>" value="case-management">
	<%
		session.setAttribute("infirmaryView_isOscar", "false");
	%>
    </caisi:ProgramExclusiveView>
</caisi:isModuleLoad>
<caisi:isModuleLoad moduleName="TORONTO_RFQ">
	<%
		session.setAttribute("infirmaryView_isOscar", "false");
	%>
</caisi:isModuleLoad>
<caisi:isModuleLoad moduleName="oscarClinic">
	<%
		session.setAttribute("infirmaryView_isOscar", "true");
	%>
</caisi:isModuleLoad>
<logic:notEqual name="infirmaryView_isOscar" value="false">
	<!--  multi-site , add site dropdown list -->
 <%
 	if (bMultisites) {
 %>
	   <script>
			function changeSite(sel) {
				sel.style.backgroundColor=sel.options[sel.selectedIndex].style.backgroundColor;
				var siteName = sel.options[sel.selectedIndex].value;
				var newGroupNo = "<%=(mygroupno == null ? ".default" : Encode.forJavaScriptBlock(mygroupno))%>";
			        <%if (org.oscarehr.common.IsPropertiesOn.isCaisiEnable() && org.oscarehr.common.IsPropertiesOn.isTicklerPlusEnable()){%>
				  popupPage(10,10, "providercontrol.jsp?provider_no=<%=Encode.forJavaScriptBlock(curUser_no)%>&start_hour=<%=startHour%>&end_hour=<%=endHour%>&every_min=<%=everyMin%>&new_tickler_warning_window=<%=Encode.forJavaScriptBlock(newticklerwarningwindow)%>&default_pmm=<%=Encode.forJavaScriptBlock(default_pmm)%>&color_template=deepblue&dboperation=updatepreference&displaymode=updatepreference&mygroup_no="+newGroupNo+"&site="+siteName);
			        <%}else {%>
			          popupPage(10,10, "providercontrol.jsp?provider_no=<%=Encode.forJavaScriptBlock(curUser_no)%>&start_hour=<%=startHour%>&end_hour=<%=endHour%>&every_min=<%=everyMin%>&color_template=deepblue&dboperation=updatepreference&displaymode=updatepreference&mygroup_no="+newGroupNo+"&site="+siteName);
			        <%}%>
			}
      </script>
    	<select id="site" name="site" onchange="changeSite(this)" style="background-color: <%=( selectedSite == null || siteBgColor.get(selectedSite) == null ? "#FFFFFF" : siteBgColor.get(selectedSite))%>">
    		<option value="none" style="background-color:white">---all clinic---</option>
    	<%
    		for (int i=0; i<curUserSites.size(); i++) {
    	%>
    		<option value="<%=Encode.forHtmlAttribute(curUserSites.get(i).getName())%>" style="background-color:<%=curUserSites.get(i).getBgColor()%>"
    				<%=(curUserSites.get(i).getName().equals(selectedSite)) ? " selected " : ""%> >
    			<%=Encode.forHtmlContent(curUserSites.get(i).getName())%>
    		</option>
    	<%
    		}
    	%>
    	</select>
<%
	}
	if (enableCustomTemporaryGroups) {
%>
	<a href="#" onclick="popupPage(715,680,'customSchedule.jsp');return false;" title="Create a temporary custom schedule">C</a>
<%  } %>
  <span><bean:message key="global.group"/>:</span>
<%
	List<MyGroupAccessRestriction> restrictions = myGroupAccessRestrictionDao.findByProviderNo(curUser_no);
	List<Provider> myGroupProviders = providerDao.getActiveProvidersWithSchedule();
%>
  <select id="mygroup_no" name="mygroup_no" onChange="changeGroup(this)">
  <option value=".<bean:message key="global.default"/>">.<bean:message key="global.default"/></option>
<security:oscarSec roleName="<%=roleName$%>" objectName="_team_schedule_only" rights="r" reverse="false">
<%
	for(Provider p : myGroupProviders) {
		boolean skip = checkRestriction(restrictions,p.getProviderNo());
		if(!skip) {
%>
<option value="<%=p.getProviderNo()%>" <%=mygroupno.equals(p.getProviderNo())?"selected":""%>>
	<% if (enableExternalNameOnSchedule) { %>
		<%=Encode.forHtmlContent(p.getFormattedName(true))%>
	<% } else { %>
		<%=Encode.forHtmlContent(p.getFormattedName())%>
	<% } %>
	<% if (oscarProperties.isPropertyActive("queens_message_search")) { %>
		<%=(p.getSpecialty() != null && !p.getSpecialty().isEmpty()) ? " (" + Encode.forHtmlContent(p.getSpecialty()) + ")" : ""%>
	<% } %>
</option>
<%
	} }
%>
</security:oscarSec>
<security:oscarSec roleName="<%=roleName$%>" objectName="_team_schedule_only" rights="r" reverse="true">
<%
	request.getSession().setAttribute("archiveView","false");
	for(MyGroup g : myGroupDao.searchmygroupno()) {
	
		boolean skip = checkRestriction(restrictions,g.getId().getMyGroupNo());

		if (!skip && (!bMultisites || siteGroups == null || siteGroups.size() == 0 || siteGroups.contains(g.getId().getMyGroupNo()))) {
		    String groupName = g.getId().getMyGroupNo();
		    if (g.getId().getMyGroupNo().equals("tmp-" + loggedInInfo1.getLoggedInProviderNo())) {
				groupName = "Custom Group";
			} else if (g.getId().getMyGroupNo().startsWith("tmp-")) { // else if the temp group does not match provider's
		        continue;
			}
%>
  <option value="<%="_grp_"+g.getId().getMyGroupNo()%>" <%=mygroupno.equals(g.getId().getMyGroupNo())?"selected":""%>>
	  <%=Encode.forHtmlContent(groupName)%>
  </option>
<%
		}
	}
	for(Provider p : myGroupProviders) {
		boolean skip = checkRestriction(restrictions,p.getProviderNo());
		if (!skip && (!bMultisites || siteProviderNos  == null || siteProviderNos.size() == 0 || siteProviderNos.contains(p.getProviderNo()))) {
%>
  <option value="<%=p.getProviderNo()%>" <%=mygroupno.equals(p.getProviderNo())?"selected":""%>>
		<%=Encode.forHtmlContent(p.getFormattedName())%>
	  <% if (oscarProperties.isPropertyActive("queens_message_search")) { %>
	  	<%=(p.getSpecialty() != null && !p.getSpecialty().isEmpty()) ? " (" + Encode.forHtmlContent(p.getSpecialty()) + ")" : ""%>
	  <% } %>
  </option>
<%
	}
	}
%>
</security:oscarSec>
</select>
</logic:notEqual>
<logic:equal name="infirmaryView_isOscar" value="false">
</logic:equal>
<%
	}
%>
<!-- caisi infirmary view extension add fffffffffffff-->
<caisi:isModuleLoad moduleName="caisi">

	<jsp:include page="infirmaryviewprogramlist.jspf">
		<jsp:param value="<%=curUser_no%>" name="curUser_no"/>
	</jsp:include>
      </td>
      </tr>
	</table>
</caisi:isModuleLoad>
<!-- caisi infirmary view extension add end fffffffffffff-->
					</form>
				</td>
			</tr>
		</table>
	</div>
	<% } %>
	<oscar:customInterface section="main"/>
</div>
<%
	if (caseload) {
%>
	<jsp:include page="caseload.jspf" />
<%
	} else {
%>
        <table id="scheduleTable" border="0" cellpadding="0" bgcolor="#dbdbdb" cellspacing="0" width="100%">
		<thead>
        <tr>
<%
boolean userAvail = true;
int me = -1;
for(nProvider=0;nProvider<numProvider;nProvider++) {
	if(curUser_no.equals(curProvider_no[nProvider]) ) {
       //userInGroup = true;
		me = nProvider; break;
	}
}
   // set up the iterator appropriately (today - for each doctor; this week - for each day)
   int iterMax;
   if (isWeekView) {
      iterMax= weekViewDays;
      // find the nProvider value that corresponds to provNum
      if(numProvider == 1) {
    	  nProvider = 0;
      }
      else {
	      for(int provIndex=0;provIndex<numProvider;provIndex++) {
	         if (curProvider_no[provIndex].equals(provNum)) {
	            nProvider=provIndex;
	         }
	      }
      }
   } else {
      iterMax=numProvider;
   }
					java.util.ResourceBundle wdProp = ResourceBundle.getBundle("oscarResources", request.getLocale());
   					//Clones the calendar for this first iteration of the week so that the second iteration of the week later on still starts on
					// the first day of the week instead of where this iteration left off
					Calendar firstWeekCalendar = (Calendar)cal.clone();
					for (int iterNum = 0; iterNum < iterMax; iterNum++) {
						if (isWeekView) {
							// get the appropriate datetime objects for the current day in this week
							year = firstWeekCalendar.get(Calendar.YEAR);
							month = (firstWeekCalendar.get(Calendar.MONTH) + 1);
							day = firstWeekCalendar.get(Calendar.DAY_OF_MONTH);

							strDate = year + "-" + (month>9?(""+month):("0"+month))+ "-" + (day>9?(""+day):("0"+day));

							inform = new SimpleDateFormat("yyyy-MM-dd", request.getLocale());
							try {
								formatDate = UtilDateUtilities.DateToString(inform.parse(strDate), wdProp.getString("date.EEEyyyyMMdd"), request.getLocale());
							} catch (Exception e) {
								MiscUtils.getLogger().error("Error", e);
								formatDate = UtilDateUtilities.DateToString(inform.parse(strDate), "EEE, yyyy-MM-dd");
							}
							// move the calendar forward one day
							firstWeekCalendar.add(Calendar.DATE, 1);
						} else {
							nProvider = iterNum;
						}
						userAvail = true;

            //Sets the appointmentList
            List<Appointment> appointmentsToCount = appointmentDao.searchappointmentday(curProvider_no[nProvider],
                    ConversionUtils.fromDateString(year + "-" + month + "-" + day),
                    ConversionUtils.fromIntString(programId_oscarView));
            Integer appointmentCount = 0;
            for (Appointment appointment : appointmentsToCount) {
              if (!noAppointmentCountStatus.contains(appointment.getStatus())
				  && appointment.getDemographicNo() != 0
                  && (!bMultisites
                    || selectedSite == null
                    || "none".equals(selectedSite)
                    || (bMultisites && selectedSite.equals(appointment.getLocation())))
              ) {
                appointmentCount++;
              }
            }

						ScheduleDate sd = scheduleDateDao.findByProviderNoAndDate(curProvider_no[nProvider], ConversionUtils.fromDateString(strDate));
						//viewall function
						if (request.getParameter("viewall") == null || request.getParameter("viewall").equals("0")) {
							if (sd == null || "0".equals(String.valueOf(sd.getAvailable()))) {
								if (nProvider != me) continue;
								else userAvail = false;
							}
						} %>
				<td valign="top" style="width: <%=isWeekView?100/7:100/numProvider%>%">
					<table border="0" cellpadding="0" bgcolor="#fff" cellspacing="0" width="100%">
						<tr>
							<td class="infirmaryView" NOWRAP ALIGN="center">
								<logic:notEqual name="infirmaryView_isOscar" value="false">
									<%
										if (isWeekView) {
									%>
									<b><a href="providercontrol.jsp?year=<%=year%>&month=<%=month%>&day=<%=day%>&view=0&displaymode=day&dboperation=searchappointmentday"
										  class="weekView"><%=formatDate%>
									</a></b>
									<%
									} else {
									%>
									<b>
										<input type='button'
											   value="<bean:message key="provider.appointmentProviderAdminDay.weekLetter"/>"
											   name='weekview' onClick=goWeekView('<%=curProvider_no[nProvider]%>')
											   title="<bean:message key="provider.appointmentProviderAdminDay.weekView"/>"
											   style="color:black" class="noprint">
										<% if (displayAppointmentDaysheetButton) { %>
											<input type='button' value="<bean:message key="provider.appointmentProviderAdminDay.daySheetLetter"/>" name='daysheetview' onClick=goDaySheet('<%=curProvider_no[nProvider]%>') title="<bean:message key="provider.appointmentProviderAdminDay.daySheet"/>" style="color:black">
										<% } %>
										<input type='button'
											   value="<bean:message key="provider.appointmentProviderAdminDay.searchLetter"/>"
											   name='searchview' onClick=goSearchView('<%=curProvider_no[nProvider]%>')
											   title="<bean:message key="provider.appointmentProviderAdminDay.searchView"/>"
											   style="color:black" class="noprint">
										<input type='radio' name='flipview' class="noprint"
											   onClick="goFilpView('<%=curProvider_no[nProvider]%>')" title="Flip view">
										<a style="color:#333" href=#
										   onClick="goZoomView('<%=curProvider_no[nProvider]%>','<%=Encode.forJavaScript(curProviderName[nProvider])%>')"
										   onDblClick="goFilpView('<%=curProvider_no[nProvider]%>')"
										   title="<bean:message key="provider.appointmentProviderAdminDay.zoomView"/>">
											<%=Encode.forHtmlContent(curProviderName[nProvider] + "(" + appointmentCount + ")")%>
										</a>
									</b>
									<% } %>
									<oscar:oscarPropertiesCheck value="yes" property="TOGGLE_REASON_BY_PROVIDER"
																defaultVal="true">
										<% if (!providerPreference.isShowAppointmentReason()) { %>
										<a id="expandReason" href="#"
										   onclick="return toggleReason('<%=isWeekView?strDate:curProvider_no[nProvider]%>');"
										   title="<bean:message key="provider.appointmentProviderAdminDay.expandreason"/>">*</a>
										<%-- Default is to hide inline reasons. --%>
										<c:set value="true" var="hideReason"/>
										<% } %>
									</oscar:oscarPropertiesCheck>
									<%
										if (!userAvail) {
									%>
									[<bean:message key="provider.appointmentProviderAdminDay.msgNotOnSched"/>]
									<%
										}
									%>
								</logic:notEqual>
								<logic:equal name="infirmaryView_isOscar" value="false">
									<%
										String prID = "1";
									%>
									<logic:present name="infirmaryView_programId">
										<%
											prID = (String) session.getAttribute(SessionConstants.CURRENT_PROGRAM_ID);
										%>
									</logic:present>
									<logic:iterate id="pb" name="infirmaryView_programBeans"
												   type="org.apache.struts.util.LabelValueBean">
										<%
											if (pb.getValue().equals(prID)) {
										%>
										<b><label><%=Encode.forHtml(pb.getLabel())%></label></b>
										<%
											}
										%>
									</logic:iterate>
								</logic:equal>
								<% if (enableProviderScheduleNote) { %>
								<div id="dayNote_<%=curProvider_no[nProvider]%>" style="padding-top: 5px; height: 16px;">
									<%
										ProviderScheduleNote note = null;
										try { 
										    note = providerScheduleNoteDao.findByProviderNoAndDate(curProvider_no[nProvider], strDate);
										} catch (ParseException e) {
											MiscUtils.getLogger().error("ProviderScheduleNote date note parsable (" + strYear + "-" + strMonth + "-" + strDay + ")", e);
										}
										String noteText = "Click to add note";
										if (note != null) {
											noteText = StringEscapeUtils.escapeHtml(note.getNote());
										}
									%>
									<a href="#" id="dayNoteLink_<%=curProvider_no[nProvider]%>" class="note-link-text" onclick="enableEdit('<%=curProvider_no[nProvider]%>')">
										<%=noteText%>
									</a>
									<div id="dayNoteInputDiv_<%=curProvider_no[nProvider]%>" style="display: none">
										<input id="dayNoteProviderNo_<%=curProvider_no[nProvider]%>" 
											   type="hidden" value="<%=curProvider_no[nProvider]%>"/>
										<input id="dayNoteDate_<%=curProvider_no[nProvider]%>"
											   type="hidden" value="<%=strYear + "-" + strMonth + "-" + strDay%>"/>
										<input id="dayNoteInput_<%=curProvider_no[nProvider]%>" type="text" value="<%=noteText.equals("Click to add note")?"":noteText%>"/>
										<input type="button" id="dayNoteInputOkDiv_<%=curProvider_no[nProvider]%>" value="OK"/>
									</div>
								</div>
								<% } %>
							</td>
						</tr>
					</table>
				</td>
				<%
					} //end of display team a, etc. 
				%>
			</tr>
		</thead>
		<tbody>
		<tr>
			<%
				boolean bShowDocLink = false;
				boolean bShowEncounterLink = false;
				if (oscar.OscarProperties.getInstance().isPropertyActive("queens_privilege_check_with_priority")) {
					bShowEncounterLink = true;
				}
				if (appointmentDoctorLinkR) {
					bShowDocLink = true;
				}
				if (eChartR) {
					bShowEncounterLink = true;
				}
				int hourCursor = 0, minuteCursor = 0, depth = everyMin; //depth is the period, e.g. 10,15,30,60min.
				String am_pm = null;
				boolean bColor = true, bColorHour = true; //to change color

				int iCols = 0, iRows = 0, iS = 0, iE = 0, iSm = 0, iEm = 0; //for each S/E starting/Ending hour, how many events
				int ih = 0, im = 0, iSn = 0, iEn = 0; //hour, minute, nthStartTime, nthEndTime, rowspan
				boolean bFirstTimeRs = true;
				boolean bFirstFirstR = true;
				Object[] paramTickler = new Object[2];
				String[] param = new String[2];
				String strsearchappointmentday = request.getParameter("dboperation");
				boolean disableStopSigns = PreventionManager.isDisabled();
				boolean propertyExists = PreventionManager.isCreated();
				userAvail = true;
   StringBuffer hourmin = null;
   String [] param1 = new String[2];
   for(int iterNum=0;iterNum<iterMax;iterNum++) {
     if (isWeekView) {
        // get the appropriate datetime objects for the current day in this week
        year = cal.get(Calendar.YEAR);
        month = (cal.get(Calendar.MONTH)+1);
        day = cal.get(Calendar.DAY_OF_MONTH);
        strDate = year + "-" + (month>9?(""+month):("0"+month))+ "-" + (day>9?(""+day):("0"+day));
        monthDay = String.format("%02d", month) + "-" + String.format("%02d", day);
        inform = new SimpleDateFormat ("yyyy-MM-dd", request.getLocale());
        try {
           formatDate = UtilDateUtilities.DateToString(inform.parse(strDate), wdProp.getString("date.EEEyyyyMMdd"),request.getLocale());
        } catch (Exception e) {
           MiscUtils.getLogger().error("Error", e);
           formatDate = UtilDateUtilities.DateToString(inform.parse(strDate), "EEE, yyyy-MM-dd");
        }
        strYear=""+year;
        strMonth=month>9?(""+month):("0"+month);
        strDay=day>9?(""+day):("0"+day);
        // Reset timecode bean for this day
        param3[0] = strDate; //strYear+"-"+strMonth+"-"+strDay;
        param3[1] = curProvider_no[nProvider];
      dateTimeCodeBean.put(String.valueOf(provNum), "");
      List<Object[]> results = scheduleDateDao.search_appttimecode(ConversionUtils.fromDateString(strDate),
              curProvider_no[nProvider],
              selectedSite);
      for(Object[] result : results) {
         ScheduleTemplate st = (ScheduleTemplate)result[0];
         ScheduleDate sd = (ScheduleDate)result[1];
         dateTimeCodeBean.put(sd.getProviderNo(), st.getTimecode());
      }
      for(ScheduleTemplateCode stc : scheduleTemplateCodeDao.findAll()) {
        dateTimeCodeBean.put("description"+stc.getCode(), stc.getDescription());
        dateTimeCodeBean.put("duration"+stc.getCode(), stc.getDuration());
        dateTimeCodeBean.put("color"+stc.getCode(), (stc.getColor()==null || "".equals(stc.getColor()))?bgcolordef:stc.getColor());
        dateTimeCodeBean.put("confirm" + stc.getCode(), stc.getConfirm());
      }
        // move the calendar forward one day
        cal.add(Calendar.DATE, 1);
     } else {
        nProvider = iterNum;
     }
     boolean userTemplateAvailable = true;
     int timecodeLength = dateTimeCodeBean.get(curProvider_no[nProvider])!=null?((String) dateTimeCodeBean.get(curProvider_no[nProvider]) ).length() : 4*24;
     if (timecodeLength == 0){
        timecodeLength = 4*24;
     }
     depth = bDispTemplatePeriod ? (24*60 / timecodeLength) : everyMin; // add function to display different time slot
     param1[0] = strDate; //strYear+"-"+strMonth+"-"+strDay;
     param1[1] = curProvider_no[nProvider];

    //Sets the appointmentList
    List<Appointment> appointmentsToCount = appointmentDao.searchappointmentday(curProvider_no[nProvider], ConversionUtils.fromDateString(year+"-"+month+"-"+day),ConversionUtils.fromIntString(programId_oscarView));
    Integer appointmentCount = 0;
    for (Appointment appointment : appointmentsToCount) {
      if (!noAppointmentCountStatus.contains(appointment.getStatus())
			  && appointment.getDemographicNo() != 0) {
          appointmentCount++;
      }
    }

    ScheduleDate sd = scheduleDateDao.findByProviderNoAndDate(curProvider_no[nProvider],ConversionUtils.fromDateString(strDate));
    //viewall function
    if (request.getParameter("viewall") == null
        || request.getParameter("viewall").equals("0")) {
      if (sd == null || "0".equals(String.valueOf(sd.getAvailable()))) {
        if (nProvider != me) {
          continue;
        } else {
          userAvail = false;
        }
      }
    } else if (OscarProperties.getInstance().isPropertyActive("not_availiable_lock_appointemnts")) {
       if (sd == null|| "0".equals(String.valueOf(sd.getAvailable()))) {
         if (nProvider != me) {
           userTemplateAvailable = false;
         }
      }
    }
    bColor=bColor?false:true;
%>
            <td valign="top" width="<%=isWeekView?100/7:100/numProvider%>%">

        <table border="0" cellpadding="0" bgcolor="#fff" cellspacing="0" width="100%"><!-- for the first provider's name -->
          <tr><td valign="top">

<!-- caisi infirmary view exteion add -->
<caisi:isModuleLoad moduleName="caisi">
<jsp:include page="infirmarydemographiclist.jspf">
	<jsp:param value="<%=userAvail%>" name="userAvail" />
	<jsp:param value="<%=tickler_no%>" name="tickler_no" />
	<jsp:param value="<%=tickler_note%>" name="tickler_note" />
	<jsp:param value="<%=strDate%>" name="strDate" />
	<jsp:param value="<%=bShowDocLink%>" name="bShowDocLink" />
	<jsp:param value="<%=studyColor%>" name="studyColor" />
	<jsp:param value="<%=bShowEncounterLink%>" name="bShowEncounterLink" />
	<jsp:param value="<%=curUser_no%>" name="curUser_no" />
	<jsp:param value="<%=Arrays.toString(curProvider_no)%>" name="curProvider_no" />
	<jsp:param value="<%=nProvider%>" name="nProvider" />
	<jsp:param value="<%=userlastname%>" name="userlastname" />
	<jsp:param value="<%=userfirstname%>" name="userfirstname" />
	<jsp:param value="<%=curYear%>" name="curYear" />
	<jsp:param value="<%=curMonth%>" name="curMonth" />
	<jsp:param value="<%=curDay%>" name="curDay" />
	<jsp:param value="<%=year%>" name="year" />
	<jsp:param value="<%=month%>" name="month" />
	<jsp:param value="<%=day%>" name="day" />
	<jsp:param value="<%=roleName$%>" name="roleName$" />
	<jsp:param value="<%=studySymbol%>" name="studySymbol" />
	<jsp:param value="<%=monthDay%>" name="monthDay" />
	<jsp:param value="<%=demBday%>" name="demBday" />
</jsp:include>
</caisi:isModuleLoad>
<logic:notEqual name="infirmaryView_isOscar" value="false">
<!-- caisi infirmary view exteion add end -->
<!-- =============== following block is the original oscar code. -->
        <!-- table for hours of day start -->
        <table id="providerSchedule" border="0" cellpadding="0" bgcolor="#fff" cellspacing="0" width="100%"><%-- bgcolor="<%=userAvail?"#486ebd":"silver"%>" --%>
<%
		bFirstTimeRs=true;
        bFirstFirstR=true;
        boolean useProgramLocation = SystemPreferencesUtils
                .isReadBooleanPreferenceWithDefault("use_program_location_enabled", false);
    	String moduleNames = OscarProperties.getInstance().getProperty("ModuleNames");
    	boolean caisiEnabled = moduleNames != null && org.apache.commons.lang.StringUtils.containsIgnoreCase(moduleNames, "Caisi");
    	boolean locationEnabled = caisiEnabled && useProgramLocation;
    	int length = locationEnabled ? 4 : 3;
        String [] param0 = new String[length];
        param0[0]=curProvider_no[nProvider];
        param0[1]=year+"-"+month+"-"+day;//e.g."2001-02-02";
		param0[2]=programId_oscarView;
		if (locationEnabled) {
			ProgramManager2 programManager2 = SpringUtils.getBean(ProgramManager2.class);
			ProgramProvider programProvider = programManager2.getCurrentProgramInDomain(loggedInInfo1,loggedInInfo1.getLoggedInProviderNo());
            if(programProvider!=null && programProvider.getProgram() != null) {
            	programProvider.getProgram().getName();
            }
		    param0[3]=request.getParameter("programIdForLocation");
		    strsearchappointmentday = "searchappointmentdaywithlocation";
		}
		List<Appointment> appointments = appointmentDao.searchappointmentday(curProvider_no[nProvider], ConversionUtils.fromDateString(year+"-"+month+"-"+day),ConversionUtils.fromIntString(programId_oscarView));
               	Iterator<Appointment> it = appointments.iterator();
                Appointment appointment = null;
            	String router = "";
            	String record = "";
            	String module = "";
            	String newUxUrl = "";
            	String inContextStyle = "";
            	if(request.getParameter("record")!=null){
            		record=request.getParameter("record");
            	}
            	if(request.getParameter("module")!=null){
            		module=request.getParameter("module");
            	}
        List<Object[]> confirmTimeCode = scheduleDateDao.search_appttimecode(ConversionUtils.fromDateString(strDate), curProvider_no[nProvider], selectedSite);
        String hourDisplay = "";
		String minuteDisplay = "";
		String timeDisplay = "";
	    for(ih=startHour*60; ih<=(endHour*60+(60/depth-1)*depth); ih+=depth) { // use minutes as base
            hourCursor = ih/60;
            minuteCursor = ih%60;
            minuteDisplay = (minuteCursor < 10 ? "0" : "") + minuteCursor;
            if (twelveHourFormat) {
	           	if (hourCursor > 12)
	           		hourDisplay = Integer.toString(hourCursor - 12);
	           	else
	           		hourDisplay = Integer.toString(hourCursor);
	            	timeDisplay = hourDisplay + ":" + minuteDisplay;
            }
           	else {
            	timeDisplay = (hourCursor<10?"0":"") + hourCursor + ":" + (minuteCursor<10?"0":"") + minuteCursor;
            }
            bColorHour=minuteCursor==0?true:false; //every 00 minute, change color
            //templatecode
            if((dateTimeCodeBean.get(curProvider_no[nProvider]) != null)&&(dateTimeCodeBean.get(curProvider_no[nProvider]) != "") && confirmTimeCode.size()!=0) {
	          int nLen = 24*60 / ((String) dateTimeCodeBean.get(curProvider_no[nProvider]) ).length();
	          int ratio = (hourCursor*60+minuteCursor)/nLen;
              hourmin = new StringBuffer(dateTimeCodeBean.get(curProvider_no[nProvider])!=null?((String) dateTimeCodeBean.get(curProvider_no[nProvider])).substring(ratio,ratio+1):" " );
            } else { hourmin = new StringBuffer(); }
%>
          <tr>
            <td align="RIGHT" class="<%=bColorHour?"scheduleTime00":"scheduleTimeNot00"%>" NOWRAP>
				<% if (OscarProperties.getInstance().isPropertyActive("not_availiable_lock_appointemnts")) { 
					if (userTemplateAvailable && (hourmin.toString().equals("") || hourmin.toString().equals("_"))) { %>
            			 <a href=# onClick="confirmPopupPage(400,780,'../appointment/addappointment.jsp?provider_no=<%=curProvider_no[nProvider]%>&bFirstDisp=<%=true%>&year=<%=strYear%>&month=<%=strMonth%>&day=<%=strDay%>&start_time=<%=(hourCursor>9?(""+hourCursor):("0"+hourCursor))+":"+ (minuteCursor<10?"0":"") +minuteCursor%>&end_time=<%=(hourCursor>9?(""+hourCursor):("0"+hourCursor))+":"+(minuteCursor+depth-1)%>&duration=<%=dateTimeCodeBean.get("duration"+hourmin.toString())%>','<%=dateTimeCodeBean.get("confirm"+hourmin.toString())%>','<%=allowDay%>','<%=allowWeek%>');return false;"
  							title='<%=MyDateFormat.getTimeXX_XXampm(hourCursor +":"+ (minuteCursor<10?"0":"")+minuteCursor)%> - <%=MyDateFormat.getTimeXX_XXampm(hourCursor +":"+((minuteCursor+depth-1)<10?"0":"")+(minuteCursor+depth-1))%>' class="adhour">
						<%=timeDisplay%>&nbsp;</a>
					<% } else { %>
							<span title="Provider not available at this time" class="adhour" style="color: grey;"><%=timeDisplay%>&nbsp;</span>
					<% }
				} else { %>
				<a href=# onClick="confirmPopupPage(400,780,'../appointment/addappointment.jsp?provider_no=<%=curProvider_no[nProvider]%>&bFirstDisp=<%=true%>&year=<%=strYear%>&month=<%=strMonth%>&day=<%=strDay%>&start_time=<%=(hourCursor>9?(""+hourCursor):("0"+hourCursor))+":"+ (minuteCursor<10?"0":"") +minuteCursor%>&end_time=<%=(hourCursor>9?(""+hourCursor):("0"+hourCursor))+":"+(minuteCursor+depth-1)%>&duration=<%=dateTimeCodeBean.get("duration"+hourmin.toString())%>','<%=dateTimeCodeBean.get("confirm"+hourmin.toString())%>','<%=allowDay%>','<%=allowWeek%>');return false;"
				   title='<%=MyDateFormat.getTimeXX_XXampm(hourCursor +":"+ (minuteCursor<10?"0":"")+minuteCursor)%> - <%=MyDateFormat.getTimeXX_XXampm(hourCursor +":"+((minuteCursor+depth-1)<10?"0":"")+(minuteCursor+depth-1))%>' class="adhour">
					<%=timeDisplay%>&nbsp;</a>
				<% } %>
			</td>
            <td class="hourmin" width='1%' <%=dateTimeCodeBean.get("color"+hourmin.toString())!=null?("bgcolor=\""+Encode.forHtmlAttribute((String)dateTimeCodeBean.get("color"+hourmin.toString())) + "\""):""%> title='<%=dateTimeCodeBean.get("description"+hourmin.toString()) != null ? Encode.forHtmlAttribute(dateTimeCodeBean.get("description"+hourmin.toString()).toString()) : ""%>'><font color='<%=(dateTimeCodeBean.get("color"+hourmin.toString())!=null && !dateTimeCodeBean.get("color"+hourmin.toString()).equals(bgcolordef) )?"black":"white"%>'><%=hourmin.toString()%></font></td>
<%
	while (bFirstTimeRs?it.hasNext():true) { //if it's not the first time to parse the standard time, should pass it by
                  appointment = bFirstTimeRs?it.next():appointment;
                  len = lenLimitedL;
                  String strStartTime = ConversionUtils.toTimeString(appointment.getStartTime());
                  String strEndTime = ConversionUtils.toTimeString(appointment.getEndTime());
                  iS=Integer.parseInt(String.valueOf(strStartTime).substring(0,2));
                  iSm=Integer.parseInt(String.valueOf(strStartTime).substring(3,5));
                  iE=Integer.parseInt(String.valueOf(strEndTime).substring(0,2));
              	  iEm=Integer.parseInt(String.valueOf(strEndTime).substring(3,5));
          	  if( (ih < iS*60+iSm) && (ih+depth-1)<iS*60+iSm ) { //iS not in this period (both start&end), get to the next period
          	  	//out.println("<td width='10'>&nbsp;</td>"); //should be comment
          	  	bFirstTimeRs=false;
          	  	break;
          	  }
          	  if( (ih > iE*60+iEm) ) { //appt before this time slot (both start&end), get to the next period
          	  	//out.println("<td width='10'>&nbsp;</td>"); //should be comment
          	  	bFirstTimeRs=true;
          	  	continue;
          	  }
         	    iRows=((iE*60+iEm)-ih)/depth+1; //to see if the period across an hour period
         	    //iRows=(iE-iS)*60/depth+iEm/depth-iSm/depth+1; //to see if the period across an hour period
                    int demographic_no = appointment.getDemographicNo();
                  //Pull the appointment name from the demographic information if the appointment is attached to a specific demographic.
                  //Otherwise get the name associated with the appointment from the appointment information
                  StringBuilder nameSb = new StringBuilder();
                  Demographic demographic = demographicDao.getDemographicById(demographic_no);
                  String prefName = "";                 
                  String provide_name = "";                 
		  if ((demographic_no != 0) && (demographic != null)) {
				String enrollmentProviderNo = demographicExtDao.getValueForDemoKey(demographic_no, "enrollmentProvider");
				if(!StringUtils.isBlank(enrollmentProviderNo)) {
					Provider enrollP = providerDao.getProvider(enrollmentProviderNo);
					provide_name ="Dr. " + enrollP.getLastName() + ", " + enrollP.getFirstName();
				}
                        nameSb.append(demographic.getLastName())
                              .append(",");
                        if (replaceNameWithPreferred && StringUtils.isNotEmpty(demographic.getPreferredName())) {
                            nameSb.append(demographic.getPreferredName());
                        } else {
                              nameSb.append(demographic.getFirstName());
                              if (demographic.getPreferredName().length()>0) { prefName = " (" + demographic.getPreferredName() + ")"; }
						}
                  }
                  else {
                        nameSb.append(String.valueOf(appointment.getName()));
                  }
                  String name = UtilMisc.toUpperLowerCase(nameSb.toString());
                  paramTickler[0]=String.valueOf(demographic_no);
                  paramTickler[1]=MyDateFormat.getSysDate(strDate); //year+"-"+month+"-"+day;//e.g."2001-02-02";
                  tickler_no = "";
                  tickler_note="";
                  if (ticklers.containsKey(demographic_no)) {
                   	tickler_no = "1";
                   	tickler_note = ticklers.get(demographic_no);
                  }
                  ver = "";
                  roster = "";
                  if(demographic != null) {
                    ver = demographic.getVer();
                    roster = demographic.getRosterStatus();
                    mob = String.valueOf(demographic.getMonthOfBirth());
                    dob = String.valueOf(demographic.getDateOfBirth());
                    demBday = mob + "-" + dob;
                    if (roster == null ) {
                        roster = "";
                    }
                  }
                  study_no = new StringBuffer("");
                  study_link = new StringBuffer("");
		  studyDescription = new StringBuffer("");
		  int numStudy = 0;
		  for(DemographicStudy ds:demographicStudyDao.findByDemographicNo(demographic_no)) {
			  Study study = studyDao.find(ds.getId().getStudyNo());
			  if(study != null && study.getCurrent1() == 1) {
				  numStudy++;
				  if(numStudy == 1) {
					  study_no = new StringBuffer(String.valueOf(study.getId()));
	                          study_link = new StringBuffer(String.valueOf(study.getStudyLink()));
	                          studyDescription = new StringBuffer(String.valueOf(study.getDescription()));
				  } else {
					  study_no = new StringBuffer("0");
		                      study_link = new StringBuffer("formstudy.jsp");
				      studyDescription = new StringBuffer("Form Studies");
				  }
			  }
		  }
                  String reason = String.valueOf(appointment.getReason()).trim();
                  String notes = String.valueOf(appointment.getNotes()).trim();
                  String status = String.valueOf(appointment.getStatus()).trim();
          	      String sitename = String.valueOf(appointment.getLocation()).trim();
          	      String arrivalTime = ConversionUtils.toTimestampString(appointment.getArrivalTime());
          	      String type = appointment.getType();
          	      String urgency = appointment.getUrgency();
          	      String reasonCodeName = null;
          	      if(appointment.getReasonCode() != null)    {  	   
          	    	LookupListItem lli  = reasonCodesMap.get(appointment.getReasonCode()); 
          	    	if(lli != null) {
          	    		reasonCodeName = lli.getLabel();
          	    	}
          	      }
				if ( "yes".equalsIgnoreCase(OscarProperties.getInstance().getProperty("SHOW_APPT_TYPE_WITH_REASON")) ) {
					reasonCodeName = ( type + (reasonCodeName == null ? "" : " : " + reasonCodeName));
				}

				// Eligibility Check and get display code for this demographic
				String eligibilityDisplayCode = "#";
				String eligibilityDisplayTooltip = "No eligibility check on record";
				if (prov.equals("BC") && eligibilityEnabled && demographic_no > 0) {
					eligibilityDisplayCode = teleplanEligibilityDao.getEligibilityDisplayCode(demographic_no, apptDate.getTime());
					if (eligibilityDisplayCode == "+") {
						eligibilityDisplayTooltip = "Eligibile for appointment calendar month";
					} else if (eligibilityDisplayCode == "-") {
						eligibilityDisplayTooltip = "Ineligible for appointment calendar month";
					} else if (eligibilityDisplayCode == "#+") {
						eligibilityDisplayTooltip = "Eligibile previously before appointment calendar month";
					} else if (eligibilityDisplayCode == "#-") {
						eligibilityDisplayTooltip = "Ineligible previously before appointment calendar month";
					} else if (eligibilityDisplayCode == "?") {
						eligibilityDisplayTooltip = "PHN missing or is Invalid";
					} else if (eligibilityDisplayCode == "^") {
						eligibilityDisplayTooltip = "Demographic HC Type is Out of Province";
					}
				}
          	  bFirstTimeRs=true;
	    as.setApptStatus(status);
		UserProperty hideNoShowsAndCancellationsProperty = userPropertyDao.getProp(curUser_no, UserProperty.HIDE_NO_SHOWS_AND_CANCELLATIONS);
		String hideAppointmentStatus = "";
		String hideAppointmentHtml = "";
		if (hideNoShowsAndCancellationsProperty != null)
			hideAppointmentStatus = hideNoShowsAndCancellationsProperty.getValue();
		if (status.matches(UserProperty.HIDE_NO_SHOWS_AND_CANCELLATIONS_REGEX)) {
			if (hideAppointmentStatus.equals(status.replaceAll("S", "")) || hideAppointmentStatus.equals(UserProperty.HIDE_NO_SHOWS_AND_CANCELLATIONS_BOTH)){
				hideAppointmentHtml = "style=\"display:none\"";
			}
		}
	 //multi-site. if a site have been selected, only display appointment in that site, if not site is selected, display all sites
	 if (!bMultisites || (selectedSite == null && (CurrentSiteMap.get(sitename) != null || sitename.equals("null") || sitename.equals(""))) || sitename.equals(selectedSite) || ((sitename.equals("null") || sitename.equals("")) && selectedSite == null)){
%>
            <td <%=hideAppointmentHtml%> class="appt" bgcolor='<%=as.getBgColor()%>' rowspan="<%=iRows%>" nowrap>
			<%
			   if (BookingSource.MYOSCAR_SELF_BOOKING == appointment.getBookingSource())
				{
					%>
						<bean:message key="provider.appointmentProviderAdminDay.SelfBookedMarker"/>
					<%
				}
			%>
			 <!-- multisites : add colour-coded to the "location" value of that appointment. -->
			 <%if (bMultisites) {%>
			 	<span title="<%= Encode.forHtmlAttribute(sitename) %>" style="background-color:<%=siteBgColor.get(sitename)%>;">&nbsp;</span>|
			 <%} %>
            <%
                String nextStatus = as.getNextStatus();
			    if (nextStatus != null && !nextStatus.equals("")) {
            %>
			<!-- Short letters -->
            <a class="apptStatus" href=# onclick="refreshSameLoc('providercontrol.jsp?appointment_no=<%=appointment.getId()%>&provider_no=<%=curProvider_no[nProvider]%>&arrivalTime=<%=arrivalTime %>&status=&statusch=<%=nextStatus%>&year=<%=year%>&month=<%=month%>&day=<%=day%>&view=<%=view==0?"0":("1&curProvider="+request.getParameter("curProvider")+"&curProviderName="+URLEncoder.encode(request.getParameter("curProviderName"),"UTF-8") )%>&displaymode=addstatus&dboperation=updateapptstatus&viewall=<%=request.getParameter("viewall")==null?"0":Encode.forUriComponent(request.getParameter("viewall"))%><%=isWeekView?"&viewWeek=1":""%>');" title="<%=Encode.forHtmlAttribute(as.getTitleString(request.getLocale()))%> " >
            <%
						}
						if (nextStatus != null) {
							if(OscarProperties.getInstance().getProperty("APPT_SHOW_SHORT_LETTERS", "false") != null 
								&& OscarProperties.getInstance().getProperty("APPT_SHOW_SHORT_LETTERS", "false").equals("true")){
								String colour = as.getShortLetterColour();
								if(colour == null){
									colour = "#FFFFFF";
								}
					%>
								<span 
									class='short_letters' 
									style='color:<%= colour%>;border:0;height:10'>
											[<%=UtilMisc.htmlEscape(as.getShortLetters())%>]
									</span>
					<%	
							}else{
				    %>
				    			<img src="../images/<%=as.getImageName()%>" border="0" height="10" title="<%=Encode.forHtmlAttribute(as.getTitleString(request.getLocale()))%>">
            <%
							}
                } else {
	                out.print("&nbsp;");
                }
			%>
			</a>
			<%
            if(urgency != null && urgency.equals("critical")) {
            %>
            	<img src="../images/warning-icon.png" border="0" width="14" height="14" title="Critical Appointment"/>
            <% } %>
        <%
        			if(demographic_no==0) {
        %>
        	<!--  caisi  -->
			<% if (ticklerR) { %>
	        	<% if (tickler_no.compareTo("") != 0) {%>
		        	<caisi:isModuleLoad moduleName="ticklerplus" reverse="true">
	        			<a href="#" onClick="popupPage(700,1024, '../tickler/ticklerDemoMain.jsp?demoview=0');return false;" title="<bean:message key="provider.appointmentProviderAdminDay.ticklerMsg"/>: <%=UtilMisc.htmlEscape(tickler_note)%>"><font color="red">!</font></a>
	    			</caisi:isModuleLoad>
	    			<caisi:isModuleLoad moduleName="ticklerplus">
	    				<a href="../ticklerPlus/index.jsp" title="<bean:message key="provider.appointmentProviderAdminDay.ticklerMsg"/>: <%=UtilMisc.htmlEscape(tickler_note)%>"><font color="red">!</font></a>
	    			</caisi:isModuleLoad>
	    		<%} %>
			<% } %>
    		<!--  alerts -->
    		<%
				String bookingAlert
					= demographicExtDao.getValueForDemoKey(demographic_no, DemographicExtKey.ALERT_BOOKING.getKey(), true);
				if (enableAlertsOnScheduleScreenSystemPreference) {
					if (StringUtils.isNotEmpty(bookingAlert)) {
			%>
				<a href="#" onClick="return false;" title="<%= StringEscapeUtils.escapeHtml(bookingAlert) %>">A</a>
			<%
					}
				}
			%>
    		<!--  notes -->
    		<%
				if (enableNotesOnScheduleScreenSystemPreference) {
					String demographicExtNotes
						= demographicExtDao.getValueForDemoKey(demographic_no, DemographicExtKey.NOTES.getKey(), true);
					String notesXmlContent = StringUtils.isNotEmpty(demographicExtNotes)
							? SxmlMisc.getXmlContent(demographicExtNotes, "<unotes>", "</unotes>")
							: "";
					if(StringUtils.isNotEmpty(notesXmlContent)) {
			%>
				<a href="#" onClick="return false;" title="<%= StringEscapeUtils.escapeHtml(notesXmlContent) %>">N</a>
			<%
					}
				}
			%>
<a href=# onClick ="popupPage(535,1000,'../appointment/appointmentcontrol.jsp?appointment_no=<%=appointment.getId()%>&provider_no=<%=curProvider_no[nProvider]%>&year=<%=year%>&month=<%=month%>&day=<%=day%>&start_time=<%=iS+":"+iSm%>&demographic_no=0&displaymode=edit&dboperation=search');return false;" title="<%=iS+":"+(iSm>10?"":"0")+iSm%>-<%=iE+":"+iEm%>
<%=Encode.forHtmlAttribute(name)%>
	<%=type != null ? "type: " + Encode.forHtmlAttribute(type) : "" %>
	reason: <%=reasonCodeName!=null?Encode.forHtmlAttribute(reasonCodeName):"" %> <%if(reason!=null && !reason.isEmpty()){%>- <%=UtilMisc.htmlEscape(reason)%>
<%}%>	<bean:message key="provider.appointmentProviderAdminDay.notes"/>: <%=UtilMisc.htmlEscape(notes)%>" >
            .<%=Encode.forHtml((view==0&&numAvailProvider!=1)?(name.length()>len?name.substring(0,len).toUpperCase():name.toUpperCase()):name.toUpperCase())%>
            </font></a><!--Inline display of reason -->
				<% if (!schedulePreferencesMap.getOrDefault("schedule_display_type", false)) { %>
      <% if (showAppointmentReason) { %>
      <span class="reason reason_<%=isWeekView?strDate:curProvider_no[nProvider]%> ${ hideReason ? "hideReason" : "" }"><bean:message key="provider.appointmentProviderAdminDay.Reason"/>:<%=UtilMisc.htmlEscape(reason)%></span>
      <% } %></td>
			<%
				}
					  } else {
						if (tickler_no.compareTo("") != 0) {
			%>
			        	<caisi:isModuleLoad moduleName="ticklerplus" reverse="true">
                                        <a href="#" onClick="popupPage(700,1024, '../tickler/ticklerDemoMain.jsp?demoview=<%=demographic_no%>');return false;" title="<bean:message key="provider.appointmentProviderAdminDay.ticklerMsg"/>: <%=UtilMisc.htmlEscape(tickler_note)%>"><font color="red">!</font></a>
    					</caisi:isModuleLoad>
    					<caisi:isModuleLoad moduleName="ticklerplus">
    						<a href="#" onClick="popupPage(700,102.4, '../Tickler.do?method=filter&filter.client=<%=demographic_no %>');return false;" title="<bean:message key="provider.appointmentProviderAdminDay.ticklerMsg"/>: <%=UtilMisc.htmlEscape(tickler_note)%>"><font color="red">!</font></a>
    					</caisi:isModuleLoad>
			<%
				}
			%>
			  <!--  alerts -->
			<%
				String bookingAlert
					= demographicExtDao.getValueForDemoKey(demographic_no, DemographicExtKey.ALERT_BOOKING.getKey(), true);
				if (enableAlertsOnScheduleScreenSystemPreference) {
					if (StringUtils.isNotEmpty(bookingAlert)) {
			%>
			  <a href="#" onClick="return false;" title="<%= StringEscapeUtils.escapeHtml(bookingAlert) %>">A</a>
			<%
					}
				}
			%>
			  <!--  notes -->
		  <%
				if (enableNotesOnScheduleScreenSystemPreference) {
					String demographicExtNotes
						= demographicExtDao.getValueForDemoKey(demographic_no, DemographicExtKey.NOTES.getKey(), true);
					String notesXmlContent = StringUtils.isNotEmpty(demographicExtNotes)
							? SxmlMisc.getXmlContent(demographicExtNotes, "<unotes>", "</unotes>")
							: "";
					if(StringUtils.isNotEmpty(notesXmlContent)) {
			%>
			  <a href="#" onClick="return false;"
				 title="<%= StringEscapeUtils.escapeHtml(notesXmlContent) %>">N</a>
		  <%
					}
				}
			%>
<!-- doctor code block 1 -->
<% if(bShowDocLink) { %>
<% if ("".compareTo(study_no.toString()) != 0) {%>	<a href="#" onClick="popupPage(700,1024, '../form/study/forwardstudyname.jsp?study_link=<%=study_link.toString()%>&demographic_no=<%=demographic_no%>&study_no=<%=study_no%>');return false;" title="<bean:message key="provider.appointmentProviderAdminDay.study"/>: <%=UtilMisc.htmlEscape(studyDescription.toString())%>"><%="<font color='"+studyColor+"'>"+studySymbol+"</font>"%></a><%} %>
<% if (ver!=null && ver!="" && "##".compareTo(ver.toString()) == 0){%><a href="#" title="<bean:message key="provider.appointmentProviderAdminDay.versionMsg"/> <%=UtilMisc.htmlEscape(ver)%>"> <font color="red">*</font></a><%}%>
<% if ("RO".equalsIgnoreCase(roster)) { %>
	<a href="#" title="<%=UtilMisc.htmlEscape(roster)%> - Rostered"><font color="green">&#10003;</font></a>
<% } else if ("TE".equalsIgnoreCase(roster)) { %>
	<a href="#" title="<%=UtilMisc.htmlEscape(roster)%> - Terminated"><font color="red">&#10007;</font></a>
<% } else if ("UHIP".equalsIgnoreCase(roster)){%>
	<a href="#" title="<%=UtilMisc.htmlEscape(roster)%> - University Health Insurance Plan"><font color="green">$</font></a>
<% } else if ("FS".equalsIgnoreCase(roster)) { %>
	<a href="#" title="<bean:message key="provider.appointmentProviderAdminDay.rosterMsg"/> <%=UtilMisc.htmlEscape(roster)%>"><font color="red">$</font></a>
<% } else if ("NR".equalsIgnoreCase(roster) || "PL".equalsIgnoreCase(roster)) { %>
	<a href="#" title="<bean:message key="provider.appointmentProviderAdminDay.rosterMsg"/> <%=UtilMisc.htmlEscape(roster)%>"><font color="red">#</font></a>
<% } else if (schedulePreferencesMap.getOrDefault("schedule_display_custom_roster_status", false)) {%>
	<a href="#" title="<bean:message key="provider.appointmentProviderAdminDay.rosterMsg"/> <%=UtilMisc.htmlEscape(roster)%>"><font color="red"><%=Encode.forHtml(roster)%></font></a>
<% } %>
<!-- /security:oscarSec -->
<% } %>
<!-- doctor code block 2 -->
<%
if(disableStopSigns!=true){
if( OscarProperties.getInstance().getProperty("SHOW_PREVENTION_STOP_SIGNS","false").equals("true") || propertyExists==true) {
		String warning = prevMgr.getWarnings(loggedInInfo1, String.valueOf(demographic_no));
		warning = PreventionManager.checkNames(warning);
		String htmlWarning = "";
		if( !warning.equals("")) {
			  htmlWarning = "<img src=\"../images/stop_sign.png\" height=\"11\" width=\"11\" title=\"" + warning +"\">&nbsp;";
		}
		out.print(htmlWarning);
}
}
String start_time = "";
if( iS < 10 ) {
	 	start_time = "0"; 
}
start_time +=  iS + ":";
if( iSm < 10 ) {
	 	start_time += "0";
}
start_time += iSm + ":00";
Boolean displayAppointmentType = (appointment.getType() != null && !appointment.getType().equals("") && !appointment.getType().equals("null"));
Boolean displayAppointmentReason = appointment.getReason() != null && appointment.getReason().length() > 0;
%>
<!-- Eligibility Link on main schedule - toggled from Administration - Schedule Management - Display Settings) -->
<% if (prov.equals("BC") && eligibilityEnabled) { %>
	<a  href="javascript: void();" onclick="callEligibilityWebService('../billing/CA/BC/ManageTeleplan.do','returnTeleplanMsg','<%= demographic_no %>');return !showMenu('2', event);" <%= eligibilityDisplayCode == "-" ? "style='color:#cc0000'" : ""%> title="<%=eligibilityDisplayTooltip%>"><%=eligibilityDisplayCode%></a>
<% } %>
<% boolean outsideUseIcon = schedulePreferencesMap.getOrDefault("schedule_display_outside_use_enabled", false);
	if (outsideUseIcon) {
		String hin = StringUtils.trimToEmpty(demographic.getHin());
		List<BillingONOUReport> ouReports = billingOnOUReportDao.findByHin(hin);
		Date today = new Date();
		for (BillingONOUReport ouReport : ouReports) {
			long diffInMillieseconds = Math.abs(today.getTime() - ouReport.getServiceDate().getTime());
			long diffInDays = TimeUnit.DAYS.convert(diffInMillieseconds, TimeUnit.MILLISECONDS);
			if (diffInDays >= 180) {
				continue;
			} %>
			  <a class="outsideUse" href="#" onclick="popupPage(535,1000, '../billing/CA/ON/outsideUse.jsp?demographic_no=<%=demographic_no%>')">
				  <img src="../images/outsideuse.png" width="15px" height="15px" alt="Outside Use" title="Outside Use">
			  </a>
	<%		break;
		}
%>
<%  } %>
<a class="apptLink" href=# onClick ="popupPage(535,860,'../appointment/appointmentcontrol.jsp?appointment_no=<%=appointment.getId()%>&provider_no=<%=curProvider_no[nProvider]%>&year=<%=year%>&month=<%=month%>&day=<%=day%>&start_time=<%=iS+":"+iSm%>&demographic_no=<%=demographic_no%>&displaymode=edit&dboperation=search');return false;"
<oscar:oscarPropertiesCheck property="SHOW_APPT_REASON_TOOLTIP" value="yes" defaultVal="true"> 
	title="<%=Encode.forHtmlAttribute(name + prefName)%>
	type: <%=type != null ? Encode.forHtmlAttribute(type) : "" %>
	reason: <%=reasonCodeName!=null? Encode.forHtml(reasonCodeName):""%> <%if(reason!=null && !reason.isEmpty()){%>- <%=Encode.forHtmlAttribute(reason)%><%}%>
	notes: <%=Encode.forHtmlAttribute(notes)%>"
</oscar:oscarPropertiesCheck> ><%=Encode.forHtml((view==0) ? (name.length()>len?name.substring(0,len) : name + prefName) :name + prefName)%></a>
<%	Boolean display_enrollment_dr = schedulePreferencesMap.getOrDefault("schedule_display_enrollment_dr_enabled", false);
	if(display_enrollment_dr){
		if(roster !=null && roster.contains("RO")){
%>
			(<font color="green"><%=roster%>:</font> <%=provide_name%>)
<%
		}
	}
%>

<% if (schedulePreferencesMap.getOrDefault("schedule_display_type", false) && (displayAppointmentType || displayAppointmentReason)) { %>
  <br>
  <% if (displayAppointmentType) { %>
    <%= Encode.forHtml(appointment.getType()) %>

  <% }
  if (displayAppointmentReason && !providerPreference.isShowAppointmentReason()) { %>
    <%= " | " + Encode.forHtml(appointment.getReason()) %>
  <% }%>
  <br>
<% } %>
<% if (len==lenLimitedL || view!=0 || numAvailProvider==1 ) {%>
	<% if (eChartR && enableEFormInAppointment) { %>
		&#124;<b><a href="#" onclick="popupPage(500,1024,'../eform/efmformslistadd.jsp?parentAjaxId=eforms&demographic_no=<%=demographic_no%>&appointment=<%=appointment.getId()%>'); return false;"
			  title="eForms">e</a></b>
	<% } %>
	<!-- doctor code block 3 -->
	<% if (bShowEncounterLink && !isWeekView) { %>
	<%
	  boolean showSinglePageChart = SystemPreferencesUtils
		  .isReadBooleanPreferenceWithDefault("show_single_page_chart", false);
	  if (showSinglePageChart) {

		newUxUrl = "../web/#/record/" + demographic_no + "/";

		if (String.valueOf(demographic_no).equals(record) && !module.equals("summary")) {
			newUxUrl =  newUxUrl + module;
			inContextStyle = "style='color: blue;'";
		} else{
			newUxUrl =  newUxUrl + "summary?appointmentNo=" + appointment.getId() + "&encType=face%20to%20face%20encounter%20with%20client";
			inContextStyle = "";
		}
	%>
	&#124; <a href="<%=newUxUrl%>" <%=inContextStyle %>><bean:message key="provider.appointmentProviderAdminDay.btnE"/>2</a>
	<%}%>
	<% String  eURL = "../oscarEncounter/IncomingEncounter.do?providerNo="
		+curUser_no+"&appointmentNo="
		+appointment.getId()
		+"&demographicNo="
		+demographic_no
		+"&curProviderNo="
		+curProvider_no[nProvider]
		+"&reason="
		+Encode.forUriComponent(reason)
		+"&encType="
		+URLEncoder.encode("face to face encounter with client","UTF-8")
		+"&userName="
		+URLEncoder.encode( userfirstname+" "+userlastname)
		+"&curDate="+curYear+"-"+curMonth+"-"
		+curDay+"&appointmentDate="+year+"-"
		+month+"-"+day+"&startTime="
		+ start_time + "&status="+Encode.forUriComponent(status)
		+ "&apptProvider_no="
		+ curProvider_no[nProvider]
				+ "&providerview="
		+ curProvider_no[nProvider];%>

	<!-- Third Party Link on main scheduler populates link directly from notes section (Configured from Administration - Schedule Management - Display Settings) -->
	<%= (tpLinkEnabled && type.equalsIgnoreCase(tpLinkTypeString)) ? "| <a href='#' onClick=\"popupPage(700, 1024, '" + notes + "')\">" + Encode.forHtmlContent(tpLinkDisplayString) + "</a>" : "" %>
	<% if (showOldEchartLink) { %>
	&#124; <a href=# class="encounterBtn" onClick="popupWithApptNo(710, 1024,'<%=eURL%>','encounter',<%=appointment.getId()%>);return false;" title="<bean:message key="global.encounter"/>">
	<bean:message key="provider.appointmentProviderAdminDay.btnE"/></a>
	<% }} %>
	<%= (bShortcutIntakeForm) ? "| <a href='#' onClick='popupPage(700, 1024, \"formIntake.jsp?demographic_no="+demographic_no+"&appointmentNo="+appointment.getId()+"&fromSchedule=true\")' title='Intake Form'>In</a>" : "" %>
	<!--  eyeform open link -->
	<% if ((oscar.OscarProperties.getInstance().isPropertyActive("new_eyeform_enabled") || UserRoleUtils.hasRole(request, UserRoleUtils.Roles.Ophthalmologist)) && !isWeekView) { %>
	&#124; <a href="#" onClick='popupPage(800, 1280, "../eyeform/eyeform.jsp?demographic_no=<%=demographic_no %>&appointment_no=<%=appointment.getId()%>");return false;' title="EyeForm">EF</a>
	<% } %>
	<!-- billing code block -->
	<% if (!isWeekView && billingR) {
		if (status.indexOf('B') == -1)
		{
			String signedUrlParam = status.contains("S") ? "&sign=true" : "";
			if (enhancedEnabled && prov.equals("ON")) {
			%>
				&#124; <a href=# onClick='popupPage(755,1200, "/<%= OscarProperties.getKaiemrDeployedContext() %>/#/billing/?demographicNo=<%= demographic_no %>&appointmentNo=<%= appointment.getId() %><%= signedUrlParam %>");return false;' title="<bean:message key="global.billingtag"/>">B</a>
			<% } else { %>
				&#124; <a href=# onClick='popupPage(755,1200, "../billing.do?billRegion=<%=URLEncoder.encode(prov)%>&billForm=<%=URLEncoder.encode(oscarVariables.getProperty("default_view"))%>&hotclick=<%=URLEncoder.encode("")%>&appointment_no=<%=appointment.getId()%>&demographic_name=<%=URLEncoder.encode(name)%>&status=<%=Encode.forUriComponent(status)%>&demographic_no=<%=demographic_no%>&providerview=<%=curProvider_no[nProvider]%>&user_no=<%=curUser_no%>&apptProvider_no=<%=curProvider_no[nProvider]%>&appointment_date=<%=year+"-"+month+"-"+day%>&start_time=<%=start_time%>&bNewForm=1");return false;' title="<bean:message key="global.billingtag"/>"><bean:message key="provider.appointmentProviderAdminDay.btnB"/></a>
			<% } %>
		<%
		}
		else
		{
			if (caisiBillingPreferenceNotDelete != null && caisiBillingPreferenceNotDelete.equals("1"))
			{
		%>
				&#124; <a href=# onClick='onUpdatebill("../billing/CA/ON/billingEditWithApptNo.jsp?billRegion=<%=URLEncoder.encode(prov)%>&billForm=<%=URLEncoder.encode(oscarVariables.getProperty("default_view"))%>&hotclick=<%=URLEncoder.encode("")%>&appointment_no=<%=appointment.getId()%>&demographic_name=<%=URLEncoder.encode(name)%>&status=<%=Encode.forUriComponent(status)%>&demographic_no=<%=demographic_no%>&providerview=<%=curProvider_no[nProvider]%>&user_no=<%=curUser_no%>&apptProvider_no=<%=curProvider_no[nProvider]%>&appointment_date=<%=year+"-"+month+"-"+day%>&start_time=<%=iS+":"+iSm%>&bNewForm=1");return false;' title="<bean:message key="global.billingtag"/>">=<bean:message key="provider.appointmentProviderAdminDay.btnB"/></a>
		<%
			}
			else
			{
		%>
			&#124; <a href=# onClick='onUnbilled("../billing/CA/<%=prov%>/billingDeleteWithoutNo.jsp?status=<%=Encode.forUriComponent(status)%>&appointment_no=<%=appointment.getId()%>");return false;' title="<bean:message key="global.billingtag"/>">-<bean:message key="provider.appointmentProviderAdminDay.btnB"/></a>
		<%
			}
		}
	} %>
	<!-- billing code block -->
	<% if (masterLinkR) { %>
		&#124; <a class="masterBtn" href="javascript: function myFunction() {return false; }" onClick="popupWithApptNo(700,1024,'../demographic/demographiccontrol.jsp?demographic_no=<%=demographic_no%>&apptProvider=<%=curProvider_no[nProvider]%>&appointment=<%=appointment.getId()%>&displaymode=edit&dboperation=search_detail<%= bMultisites ? "&site=" + Encode.forUriComponent(sitename) : "" %>','master',<%=appointment.getId()%>)"
		title="<bean:message key="provider.appointmentProviderAdminDay.msgMasterFile"/>"><bean:message key="provider.appointmentProviderAdminDay.btnM"/></a>
	<% } %>
	<% if (!isWeekView) { %>
	<!-- doctor code block 4 -->
		<% if (appointmentDoctorLinkR) { %>
			&#124;
			<% if (enhancedEnabled && OscarProperties.getInstance().isPropertyActive("use_fdb")) { %>
				<a href=#
					onClick='popupPage(755,1200, "/kaiemr/app/components/rx/?demographicNo=<%= demographic_no %>&appointmentNo=<%= appointment.getId() %>");return false;'
					title="<bean:message key="global.prescriptions"/>">
						<bean:message key="global.rx"/>
				</a>
			<% } else { %>
				<a href=# onClick="popupWithApptNo(755,1200,'../oscarRx/choosePatient.do?providerNo=<%=curUser_no%>&demographicNo=<%=demographic_no%>','rx',<%=appointment.getId()%>)" title="<bean:message key="global.prescriptions"/>"><bean:message key="global.rx"/></a>
			<% } %>
			<!-- Display Label button for fast access to printing labels -->
			<% if (labelShortcutEnabled) { %>
				  <a href=# onClick="popupPage(700,1027,'../demographic/printDemoLabelAction.do?demographic_no=<%=demographic_no%>')"
					title="<bean:message key="provider.appointmentProviderAdminDay.label"/>">|<bean:message key="provider.appointmentProviderAdminDay.btnL"/>
				  </a>
			<% } %>
			<!-- AsthmaLife link -->
			<% if (OscarProperties.getInstance().isPropertyActive("indivicare_asthma_life_enabled")) { %>
				<b><a href="#" onclick="popupPage(500,1024, '../indivicare/asthmaLife.do?demographicNo=<%=demographic_no%>');return false;"
					title="Indivicare Asthma Life">|As </a></b>
			<% } %>
			<!-- doctor color -->
			<oscar:oscarPropertiesCheck property="ENABLE_APPT_DOC_COLOR" value="yes">
				<%
					String providerColor = null;
					if (view == 1 && demographicDao != null && userPropertyDao != null) {
						String providerNo = (demographicDao.getDemographic(String.valueOf(demographic_no)) == null ? null : demographicDao.getDemographic(String.valueOf(demographic_no)).getProviderNo());
						UserProperty property = userPropertyDao.getProp(providerNo, UserPropertyDAO.COLOR_PROPERTY);
						if (property != null) {
							providerColor = property.getValue();
						}
					}
				%>
				<%= (providerColor != null ? "<span style=\"background-color:"+providerColor+";width:5px\">&nbsp;</span>" : "") %>
			</oscar:oscarPropertiesCheck>
			<%
				if ("bc".equalsIgnoreCase(prov)) {
					if (patientHasOutstandingPrivateBills(String.valueOf(demographic_no))) {
			%>
			  			&#124;<b style="color:#FF0000">$</b>
					<% }
				} %>
		<% } %>
			<%
				UserProperty insigPortalEnabled = userPropertyDao.getProp(loggedInInfo1.getLoggedInProviderNo(), UserProperty.INSIG_PORTAL_ENABLED);
				boolean isInsigPortalEnabled = insigPortalEnabled != null ? Boolean.parseBoolean(insigPortalEnabled.getValue()) : false;
				if (isInsigPortalEnabled) {
			%>
					<span class="portalButton" ng-controller="PortalButtonController" portal-button></span>
				<% } %>
		<%
			String url = oscar.OscarProperties.getInstance().getProperty("cyclesLink");
			if (cyclesLink != null && !cyclesLink.isEmpty()) {
				String params = "providerNo=" + curUser_no + "&patientId=" + demographic_no;
				if (cyclesClientName == null || request.getServerName().startsWith(cyclesClientName)) {
					url = cyclesLink + "?" + params;
				} else {
					url = cyclesLocalLink + "?" + params;
				}
		%>
		&#124;
			<a href='<%=url%>'
					target = "_blank"
					title = "<bean:message key = "global.cycles"/>"
					onclick = "window.open('<%=url%>','_blank'); return false;">
					<bean:message key = "global.c" />
			</a>
		<% } %>
		<%
			String secondCyclesUrl = oscar.OscarProperties.getInstance().getProperty("secondCyclesLink");
			if (StringUtils.trimToNull(secondCyclesUrl) != null) {
				secondCyclesUrl = secondCyclesUrl.replace("${demographicId}", String.valueOf(demographic_no));
		%>
		&#124;
			<a href ='<%= secondCyclesUrl %>' id="cycles-link"
				target = "_blank"
				title = "<bean:message key = "global.cycles"/>"
				onclick = "window.open('<%= secondCyclesUrl %>', '_blank'); return false;">
				<bean:message key = "global.cl" />
			</a>
		<% } %>
		  <!-- add one link to caisi Program Management Module -->
		<caisi:isModuleLoad moduleName="caisi">
			<a href='../PMmodule/ClientManager.do?id=<%=demographic_no%>' title="Program Management">| PM</a>
		</caisi:isModuleLoad>
		<%
			if (isBirthday(monthDay,demBday)){ %>
			&#124; <img src="../images/cake.gif" height="20" alt="Happy Birthday"/>
			<% } %>
		<%
			String appointment_no=appointment.getId().toString();
			request.setAttribute("providerPreference", providerPreference);
			Date appointmentDate = appointmentDateTimeFormat.parse(strYear + "-" + strMonth + "-" + strDay + " " + start_time);
		%>
		<c:set var="demographic_no" value="<%=demographic_no %>" />
		<c:set var="appointment_no" value="<%=appointment_no %>" />
		<c:set var="appointment_date" value="<%=appointmentDate.getTime()%>" />
		<jsp:include page="appointmentFormsLinks.jspf">
			<jsp:param value="${demographic_no}" name="demographic_no"/>
			<jsp:param value="${appointment_no}" name="appointment_no"/>
			<jsp:param value="${appointment_date}" name="appointment_date"/>
		</jsp:include>
		<oscar:oscarPropertiesCheck property="appt_pregnancy" value="true" defaultVal="false">
			<c:set var="demographicNo" value="<%=demographic_no %>" />
		<jsp:include page="appointmentPregnancy.jspf" >
			<jsp:param value="${demographicNo}" name="demographicNo"/>
		</jsp:include>
		</oscar:oscarPropertiesCheck>
	<% } %>
	<% if (showAppointmentReason) { %>
		<span class="reason reason_<%=isWeekView?strDate:curProvider_no[nProvider]%> ${ hideReason ? "hideReason" : "" }">
			<strong>&#124;<%=reasonCodeName==null?"":"&nbsp;" + Encode.forHtml(reasonCodeName) + " -"%><%=reason==null?"":"&nbsp;" + Encode.forHtmlContent(reason)%></strong>
		</span>
	<% } %>
<% } %>
			<%
			if (StringUtils.isNotBlank(arrivalTime)) {
				String arrivalTimeStamp = arrivalTime.substring(11, 16);
			%>
       		&nbsp;&nbsp;&nbsp;&nbsp;<span><%=arrivalTimeStamp%></span>
			<% }
			%>
        	</td>	
        <%
        			}
        		}
        			bFirstFirstR = false;
          	}
            //out.println("<td width='1'>&nbsp;</td></tr>"); give a grid display
            out.println("<td class='noGrid' width='1'></td></tr>"); //no grid display
          }
				%>
          </table> <!-- end table for each provider schedule display -->
</logic:notEqual>
<!-- caisi infirmary view extension add end fffffffffffffff-->
         </td></tr>
       </table><!-- end table for each provider name -->
            </td>
 <%
   } //end of display team a, etc.
 %>
			</tr>
		</tbody>
	</table>        <!-- end table for the whole schedule row display -->
<% } // end caseload view %>
  <% if (enableAlertsOnScheduleScreenSystemPreference) { %>
  <jsp:include page="./tasks-status.jsp"/>
  <% } %>
<script type="text/javascript" src="<%=request.getContextPath() %>/library/angular-1.8.2/angular.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/library/angular-1.8.2/angular-sanitize.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/integration/portal/portal.controller.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/common/integration.service.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/common/system-preferences.service.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/common/security.service.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/common/global.factory.jsp"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/integration/portal/app.module.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/integration/portal/app.core.js"></script>
</div>
</div>
</body>
<!-- key shortcut hotkey block added by phc -->
<script language="JavaScript">
// popup blocking for the site must be off!
// developed on Windows FF 2, 3 IE 6 Linux FF 1.5
// FF on Mac and Opera on Windows work but will require shift or control with alt and Alpha
// to fire the altKey + Alpha combination - strange
// Modification Notes:
//     event propagation has not been blocked beyond returning false for onkeydown (onkeypress may or may not fire depending)
//     keyevents have not been even remotely standardized so test mods across agents/systems or something will break!
//     use popupOscarRx so that this codeblock can be cut and pasted to appointmentprovideradminmonth.jsp
// Internationalization Notes:
//     underlines should be added to the labels to prompt/remind the user and should correspond to
//     the actual key whose keydown fires, which is also stored in the oscarResources.properties files
//     if you are using the keydown/up event the value stored is the actual key code
//     which, at least with a US keyboard, also is the uppercase utf-8 code, ie A keyCode=65
document.onkeydown=function(e){
	evt = e || window.event;  // window.event is the IE equivalent
	if (evt.altKey) {
		//use (evt.altKey || evt.metaKey) for Mac if you want Apple+A, you will probably want a seperate onkeypress handler in that case to return false to prevent propagation
		switch(evt.keyCode) {
			case <bean:message key="global.adminShortcut"/> : newWindow("../administration/","admin");  return false;  //run code for 'A'dmin
			case <bean:message key="global.billingShortcut"/> : popupOscarRx(600,1024,'../billing/CA/<%=prov%>/billingReportCenter.jsp?displaymode=billreport&providerview=<%=curUser_no%>');return false;  //code for 'B'illing
			case <bean:message key="global.calendarShortcut"/> : popupOscarRx(425,430,'../share/CalendarPopup.jsp?urlfrom=<%=request.getContextPath()%>/provider/providercontrol.jsp&year=<%=strYear%>&month=<%=strMonth%>&param=<%=URLEncoder.encode("&view=0&displaymode=day&dboperation=searchappointmentday","UTF-8")%>');  return false;  //run code for 'C'alendar
			case <bean:message key="global.edocShortcut"/> : popupOscarRx('700', '1024', '../dms/documentReport.jsp?function=provider&functionid=<%=curUser_no%>&curUser=<%=curUser_no%>', 'edocView');  return false;  //run code for e'D'oc
			case <bean:message key="global.resourcesShortcut"/> : popupOscarRx(550,687,'<%=resourcebaseurl%>'); return false; // code for R'e'sources
 			case <bean:message key="global.helpShortcut"/> : popupOscarRx(600,750,'<%=resourcebaseurl%>');  return false;  //run code for 'H'elp
			case <bean:message key="global.ticklerShortcut"/> : {
				<caisi:isModuleLoad moduleName="ticklerplus" reverse="true">
					popupOscarRx(700,1024,'../tickler/ticklerMain.jsp','<bean:message key="global.tickler"/>') //run code for t'I'ckler
				</caisi:isModuleLoad>
				<caisi:isModuleLoad moduleName="ticklerplus">
					popupOscarRx(700,1024,'../Tickler.do','<bean:message key="global.tickler"/>'); //run code for t'I'ckler+
				</caisi:isModuleLoad>
				return false;
			}
			case <bean:message key="global.labShortcut"/> : popupOscarRx(600,1024,'../dms/inboxManage.do?method=prepareForIndexPage&providerNo=<%=curUser_no%>', '<bean:message key="global.lab"/>');  return false;  //run code for 'L'ab
			case <bean:message key="global.msgShortcut"/> : popupOscarRx(600,1024,'../oscarMessenger/DisplayMessages.do?providerNo=<%=curUser_no%>&userName=<%=URLEncoder.encode(userfirstname+" "+userlastname)%>'); return false;  //run code for 'M'essage
			case <bean:message key="global.monthShortcut"/> : window.open("providercontrol.jsp?year=<%=year%>&month=<%=month%>&day=1&view=<%=view==0?"0":("1&curProvider="+request.getParameter("curProvider")+"&curProviderName="+URLEncoder.encode(request.getParameter("curProviderName"),"UTF-8") )%>&displaymode=month&dboperation=searchappointmentmonth","_self"); return false ;  //run code for Mo'n'th
			case <bean:message key="global.conShortcut"/> : popupOscarRx(625,1024,'../oscarEncounter/IncomingConsultation.do?providerNo=<%=curUser_no%>&userName=<%=URLEncoder.encode(userfirstname+" "+userlastname)%>');  return false;  //run code for c'O'nsultation
			case <bean:message key="global.reportShortcut"/> : popupOscarRx(650,1024,'../report/reportindex.jsp','reportPage');  return false;  //run code for 'R'eports
			case <bean:message key="global.prefShortcut"/> : {
				    <caisi:isModuleLoad moduleName="ticklerplus">
					popupOscarRx(715,680,'providerpreference.jsp?provider_no=<%=curUser_no%>&start_hour=<%=startHour%>&end_hour=<%=endHour%>&every_min=<%=everyMin%>&mygroup_no=<%=mygroupno%>&caisiBillingPreferenceNotDelete=<%=caisiBillingPreferenceNotDelete%>&new_tickler_warning_window=<%=newticklerwarningwindow%>&default_pmm=<%=default_pmm%>'); //run code for tickler+ 'P'references
					return false;
				    </caisi:isModuleLoad>
			            <caisi:isModuleLoad moduleName="ticklerplus" reverse="true">
					popupOscarRx(715,680,'providerpreference.jsp?provider_no=<%=curUser_no%>&start_hour=<%=startHour%>&end_hour=<%=endHour%>&every_min=<%=everyMin%>&mygroup_no=<%=mygroupno%>'); //run code for 'P'references
					return false;
			            </caisi:isModuleLoad>
			}
			case <bean:message key="global.searchShortcut"/> : popupOscarRx(550,687,'../demographic/search.jsp');  return false;  //run code for 'S'earch
			case <bean:message key="global.dayShortcut"/> : window.open("providercontrol.jsp?year=<%=curYear%>&month=<%=curMonth%>&day=<%=curDay%>&view=<%=view==0?"0":("1&curProvider="+request.getParameter("curProvider")+"&curProviderName="+URLEncoder.encode(request.getParameter("curProviderName"),"UTF-8") )%>&displaymode=day&dboperation=searchappointmentday","_self");  return false;  //run code for 'T'oday
			case <bean:message key="global.viewShortcut"/> : {
				<% if(request.getParameter("viewall")!=null && request.getParameter("viewall").equals("1") ) { %>
				         review('0');  return false; //scheduled providers 'V'iew
				<% } else {  %>
				         review('1');  return false; //all providers 'V'iew
				<% } %>
			}
			case <bean:message key="global.workflowShortcut"/> : popupOscarRx(700,1024,'../oscarWorkflow/WorkFlowList.jsp','<bean:message key="global.workflow"/>'); return false ; //code for 'W'orkflow
			case <bean:message key="global.phrShortcut"/> : popupOscarRx('600', '1024','../phr/PhrMessage.do?method=viewMessages','INDIVOMESSENGER2<%=curUser_no%>')
			default : return;
               }
	}
	if (evt.ctrlKey) {
               switch(evt.keyCode || evt.charCode) {
			case <bean:message key="global.btnLogoutShortcut"/> : window.open('../logout.jsp','_self');  return false;  // 'Q'uit/log out
			default : return;
               }
        }
}
</script>
<!-- end of keycode block -->
	<div id='menu2' class='menu' onclick='event.cancelBubble = true;' style="width:350px;">
		<span id="search_spinner" ><bean:message key="demographic.demographiceditdemographic.msgLoading"/></span>
		<span id="returnTeleplanMsg"></span>
	</div>
<% if (OscarProperties.getInstance().getBooleanProperty("indivica_hc_read_enabled", "true")) { %>
<jsp:include page="/hcHandler/hcHandler.html"/>
<% } %>
</html:html>
<%!public boolean checkRestriction(List<MyGroupAccessRestriction> restrictions, String name) {
                for(MyGroupAccessRestriction restriction:restrictions) {
                        if(restriction.getMyGroupNo().equals(name))
                                return true;
                }
                return false;
        }%>
