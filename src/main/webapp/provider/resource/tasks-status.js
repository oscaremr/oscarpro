jQuery_3_1_0(document).ready(function () {
    var $ = jQuery_3_1_0;
    var $kaiBar = $('.KaiBar');
    var dlg = {
        $el: $("#dlgTASKSMessage"),
        init: function () {
            var that = this;
            $('.btn-default', that.$el).on('click', function () {
                that.close();
            });
            $('.btn-primary', that.$el).on('click', function () {
                jQuery.ajax({
                    url: systemAPIContext + '/billing/CA/BC/ManageTeleplan.do?method=clearErrorAlerts',
                    method: 'post',
                    dataType: 'json',
                    success: function (data) {
                        if (data.code === 'success') {
                            alert('Submit successfully.')
                            window.location.reload()
                        } else {
                            alert('Submit failed. Please try again');
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert('Submit failed. Please try again');
                    }
                });
            });
        },
        close: function () {
            this.$el.hide()
            $('#dlgTASKSMessage-backdrop').hide()
        },
        show: function () {
            this.$el.show();
            $('#dlgTASKSMessage-backdrop').show()
        }
    }
    $kaiBar.find('.block.right').prepend($('#globalTASKSStatus').html())
    dlg.init();
    $('.tasks-status', $kaiBar).on('click', function () {
        dlg.show();
    });

});