<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName2$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed2=true;
%>
<security:oscarSec roleName="<%=roleName2$%>" objectName="_appointment" rights="r" reverse="<%=true%>">
	<%authed2=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_appointment");%>
</security:oscarSec>
<%
	if(!authed2) {
		return;
	}
%>

<%@ page import="java.sql.*, java.util.*, oscar.MyDateFormat, org.oscarehr.common.OtherIdManager"%>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="org.oscarehr.event.EventService, org.oscarehr.util.SpringUtils"%>
<%@ page import="org.oscarehr.common.dao.OscarAppointmentDao"%>
<%@ page import="org.oscarehr.common.dao.DemographicDao"%>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="org.oscarehr.common.model.Appointment"%>
<%@ page import="org.oscarehr.common.model.AppointmentArchive"%>
<%@ page import="org.oscarehr.common.model.Demographic"%>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao"%>
<%@ page import="org.oscarehr.common.model.Provider"%>
<%@ page import="org.oscarehr.common.dao.AppointmentArchiveDao"%>
<%@ page import="org.oscarehr.common.dao.SiteDao"%>
<%@ page import="org.oscarehr.common.model.Site"%>
<%@ page import="org.oscarehr.common.dao.ClinicDAO"%>
<%@ page import="org.oscarehr.common.model.Clinic"%>
<%@ page import="org.oscarehr.common.model.ProviderPreference"%>
<%@ page import="org.oscarehr.common.dao.UserPropertyDAO"%>
<%@ page import="org.oscarehr.common.model.UserProperty"%>
<%@ page import="org.oscarehr.util.SessionConstants" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ page import="oscar.OscarProperties" %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/wellAiVoice.tld" prefix="well-ai-voice"%>
<%@ include file="/common/webAppContextAndSuperMgr.jsp"%>

<%
	ProviderPreference providerPreference=(ProviderPreference)session.getAttribute(SessionConstants.LOGGED_IN_PROVIDER_PREFERENCE);
	boolean twelveHourFormat = providerPreference.isTwelveHourFormat();
%>

<html:html locale="true">
<head>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<style type="text/css" media="print">
    .DoNotPrint {
        display: none;
    }
</style>
</head>
<body background="../images/gray_bg.jpg"
	bgproperties="fixed" onload="window.print()">
	<well-ai-voice:script/>
<center>
<div class="DoNotPrint">
<table border="0" cellspacing="0" cellpadding="0" width="90%">
	<tr bgcolor="#486ebd">
		<th align="CENTER"><font face="Helvetica" color="#FFFFFF">
		Appointment Card</font></th>
	</tr>
</table>
<%
	Properties oscarVariables = OscarProperties.getInstance(); 
	UserPropertyDAO userPropertyDao = SpringUtils.getBean(UserPropertyDAO.class);

	String strAppointmentNo = request.getParameter("appointment_no");
	String demoNo = request.getParameter("demographic_no");
	String providerNo = request.getParameter("provider_no");
	SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat timeFormatter = new SimpleDateFormat("hh:mm aaa");

	
	SimpleDateFormat dateFormatter2 = new SimpleDateFormat("EEE, d MMM yyyy");
	//SimpleDateFormat timeFormatter2 = new SimpleDateFormat("h:mm aaa");
	
	OscarAppointmentDao appointmentDao = SpringUtils.getBean(OscarAppointmentDao.class);
	SiteDao siteDao =  SpringUtils.getBean(SiteDao.class);
	AppointmentArchiveDao appointmentArchiveDao = SpringUtils.getBean(AppointmentArchiveDao.class);
	
	String[] appt_no_list = strAppointmentNo.split(",");
	List<Appointment> apptList = new ArrayList<Appointment>();
	
	List<Site> siteList = new ArrayList<Site>();
	for(int i = appt_no_list.length-1;i >= 0; i--){  //list the oldest one on the top
		if(appt_no_list[i].length() > 0){
			Appointment appoint = appointmentDao.find(Integer.parseInt(appt_no_list[i]));
			if (appoint == null) {
				AppointmentArchive appointmentArchive = appointmentArchiveDao.getMostRecent(Integer.parseInt(appt_no_list[i]));
				appoint = new Appointment();
				appoint.setAppointmentDate(appointmentArchive.getAppointmentDate());
				appoint.setLocation(appointmentArchive.getLocation());
				appoint.setProviderNo(appointmentArchive.getProviderNo());
				appoint.setDemographicNo(appointmentArchive.getDemographicNo());
				appoint.setStartTime(appointmentArchive.getStartTime());
			}
			apptList.add(appoint);			
			
			if(oscarVariables.getProperty("multisites","off").equalsIgnoreCase("on") && !StringUtils.isBlank(appoint.getLocation())) {
				//Site site = siteDao.getById(Integer.valueOf(appoint.getLocation()));
				Site site = siteDao.getByLocation(appoint.getLocation());
				siteList.add(site);
			} else {
				siteList.add(new Site());
			}
			
			if(demoNo == null){
				demoNo = String.valueOf(appoint.getDemographicNo());
			}
			if(providerNo == null){
				providerNo = appoint.getProviderNo();
			}
		}
	}
	//Appointment appt = appointmentDao.find(Integer.parseInt(strAppointmentNo));
	
	ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
	
	//get clinic info for address
	ClinicDAO clinicDao = SpringUtils.getBean(ClinicDAO.class);
	Clinic clinic = clinicDao.getClinic();
			
	String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	
	DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
	Demographic demo = demographicDao.getDemographic(demoNo);
	
%>
<p>

    </div>
    
    <!-- hiding this old version -->
    <%--
    <div class="DoNotPrint">
<form>
    <table border="1" bgcolor="white" >
        <tr><td>
	
        <table style="font-size: 14pt;"  align="left" valign="top">

            <tr style="font-family: arial, sans-serif; font-size: 12pt;" >
                <th colspan="3"><%=appt.getName()%></th>
            </tr>
             <tr style="font-family: arial, sans-serif; font-size: 12pt;" >
		<th style="padding-right: 10px"><bean:message key="Appointment.formDate" /></th>
 		<th width="60" style="padding-right: 10px"><bean:message key="Appointment.formStartTime" /></th>
		<th width="120" style="padding-right: 10px"><bean:message key="appointment.addappointment.msgProvider" /></th>

            </tr>
        <%
       
        int iRow=0;
        int iPageSize=5;
        String pname="";
        // if the booking is not matched to a demographic demoNo=="0" as a default
        if( appt.getDemographicNo() == 0 ) {

        %>
            <tr bgcolor="#eeeeff">
		<td style="padding-right: 10px"><%=dateFormatter.format(appt.getAppointmentDate())%></td>
		<td style="padding-right: 10px"><%=timeFormatter.format(appt.getStartTime())%></td>
		<td style="padding-right: 10px">&nbsp;</td>
            </tr>
	<%
        } else {
        	
        	for(Appointment a: appointmentDao.findDemoAppointmentsOnDate(appt.getDemographicNo(),appt.getAppointmentDate())) {   	
        		Provider provider = providerDao.getProvider(a.getProviderNo());
     
    %>
            <tr bgcolor="#eeeeff">
		<td style="padding-right: 10px"><%=dateFormatter.format(a.getAppointmentDate())%></td>
		<td style="padding-right: 10px"><%=timeFormatter.format(a.getStartTime())%></td>
		<td style="padding-right: 10px"><%=provider.getLastName() + "," + provider.getFirstName().substring(0,1)%></td>
            </tr>
	<%
            }
        }
    %>
    
            <tr class="DoNotPrint">
		<td style="padding-left: 10px"><input type="button" value="<bean:message key="global.btnPrint"/>" onClick="window.print();"></td>
                <td>&nbsp;</td>
		<td>&nbsp;</td>
            </tr>
       </table>
       </td></tr>
</table>
<p>

<p></p>
<hr width="90%"/>


</div>

--%>

<table border="0" bgcolor="white" >
	<tr><td style="padding: 10px 10px 10px 10px" width="80%">
	<table border="1" bgcolor="white">
	<tr> <!-- first row is logo | prov info -->		
		<td style="padding-right: 1px">
			<img src="../imageRenderingServlet?source=clinic_logo" width="200px"/>
		</td>		
		<td>
		<%
			Provider provider = providerDao.getProvider(providerNo);
		
			String salutation="";
			
			if(roleName$.indexOf("doctor") != -1) {
				salutation="Dr.";
			}
			
			String firstLine = salutation + " " + provider.getFirstName() + " " + provider.getLastName();
			String phone = clinic.getClinicPhone();
			String fax = clinic.getClinicFax();
			
	
			UserProperty up = userPropertyDao.getProp(providerNo,"APPT_CARD_NAME");
			if(up != null && !up.getValue().isEmpty()) {
				firstLine = up.getValue();
			}
			
			up = userPropertyDao.getProp(providerNo,"APPT_CARD_PHONE");
			if(up != null && !up.getValue().isEmpty()) {
				phone = up.getValue();
			}
			
			up = userPropertyDao.getProp(providerNo,"APPT_CARD_FAX");
			if(up != null && !up.getValue().isEmpty()) {
				fax = up.getValue();
			}
			
		%>
			<b style="font-size:14pt"><%=Encode.forHtml(firstLine) %></b><br/>
			<%=StringEscapeUtils.escapeHtml(provider.getSpecialty()) %><br/>
			<br/>
			<%if(!oscarVariables.getProperty("multisites","off").equalsIgnoreCase("on")) { %>
				<%=StringEscapeUtils.escapeHtml(clinic.getClinicAddress()) %><br/>
				<%=StringEscapeUtils.escapeHtml(clinic.getClinicCity()) %>, <%=StringEscapeUtils.escapeHtml(clinic.getClinicProvince()) %>  <%=StringEscapeUtils.escapeHtml(clinic.getClinicPostal()) %><br/>
				Phone <%=StringEscapeUtils.escapeHtml(phone) %>  &nbsp;&nbsp;<br/>
				Fax <%=StringEscapeUtils.escapeHtml(fax) %>   <br/>
			<%} else { //multiple sites and assume the patient only visits one site %>
				<%=StringEscapeUtils.escapeHtml(siteList.get(0).getAddress()) %><br/>
				<%=StringEscapeUtils.escapeHtml(siteList.get(0).getCity()) %>, <%=StringEscapeUtils.escapeHtml(siteList.get(0).getProvince()) %>  <%=StringEscapeUtils.escapeHtml(siteList.get(0).getPostal()) %><br/>
				Phone <%=StringEscapeUtils.escapeHtml(siteList.get(0).getPhone()) %>  &nbsp;&nbsp;<br/>
				Fax <%=StringEscapeUtils.escapeHtml(siteList.get(0).getFax()) %>   <br/>
			<%} %>
		</td>
	</tr>

	<tr> <!-- patient name -->
		<td colspan="2">
			<b>Name</b>: <span style="text-decoration: underline;"><%=StringEscapeUtils.escapeHtml(demo.getFullName()) %></span>
		</td>
	</tr>
	
	<tr> <!-- appt date and time-->
		<td colspan="2">
			<table>
			<%for(int i = 0;i < apptList.size();i ++){%>
			<tr>
				<%if(i == 0){ %>
				<td><b>Appointment Date</b>: </td>
				<%}else{ %>
				<td></td>
				<%} %>
				<td><span style="text-decoration: underline;"><%=dateFormatter2.format(apptList.get(i).getAppointmentDate()) %> at <%=twelveHourFormat?MyDateFormat.getTimeXX_XXampm(apptList.get(i).getStartTime().toString()):apptList.get(i).getStartTime() %><span></td>
			</tr>
			<%}%>
			</table>
		</td>
	</tr>
	
	</table>
	</td></tr>
</table>

<div class="DoNotPrint">
<input type="button" value="<bean:message key="global.btnClose"/>" onClick="window.close();">
&nbsp;
<input type="button" value="<bean:message key="global.btnPrint"/>" onClick="window.print();">
</div>
</form>
</center>
</body>
</html:html>
