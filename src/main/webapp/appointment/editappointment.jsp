<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.math.BigDecimal" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.GregorianCalendar" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="org.apache.commons.beanutils.BeanUtils" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@ page import="org.apache.commons.lang3.math.NumberUtils" %>
<%@ page import="org.oscarehr.common.dao.AppointmentArchiveDao" %>
<%@ page import="org.oscarehr.common.dao.AppointmentTypeDao" %>
<%@ page import="org.oscarehr.common.dao.BillingONCHeader1Dao"%>
<%@ page import="org.oscarehr.common.dao.BillingONExtDao" %>
<%@ page import="org.oscarehr.common.dao.DemographicExtDao" %>
<%@ page import="org.oscarehr.common.dao.EncounterFormDao" %>
<%@ page import="org.oscarehr.common.dao.OscarAppointmentDao" %>
<%@ page import="org.oscarehr.common.dao.ProviderDataDao"%>
<%@ page import="org.oscarehr.common.dao.SiteDao"%>
<%@ page import="org.oscarehr.common.dao.ThirdPartyApplicationDao" %>
<%@ page import="org.oscarehr.common.model.Appointment" %>
<%@ page import="org.oscarehr.common.model.AppointmentArchive" %>
<%@ page import="org.oscarehr.common.model.AppointmentStatus"%>
<%@ page import="org.oscarehr.common.model.AppointmentType" %>
<%@ page import="org.oscarehr.common.model.BillingONCHeader1"%>
<%@ page import="org.oscarehr.common.model.Demographic" %>
<%@ page import="org.oscarehr.common.model.DemographicExtKey" %>
<%@ page import="org.oscarehr.common.model.EmrContextEnum" %>
<%@ page import="org.oscarehr.common.model.EncounterForm" %>
<%@ page import="org.oscarehr.common.model.Facility" %>
<%@ page import="org.oscarehr.common.model.LookupList"%>
<%@ page import="org.oscarehr.common.model.LookupListItem"%>
<%@ page import="org.oscarehr.common.model.Site"%>
<%@ page import="org.oscarehr.common.model.ThirdPartyApplication" %>
<%@ page import="org.oscarehr.common.model.Provider" %>
<%@ page import="org.oscarehr.common.model.ProviderData"%>
<%@ page import="org.oscarehr.common.model.ProviderPreference"%>
<%@ page import="org.oscarehr.common.OtherIdManager" %>
<%@ page import="org.oscarehr.managers.DemographicManager"%>
<%@ page import="org.oscarehr.managers.LookupListManager"%>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="org.oscarehr.PMmodule.model.Program" %>
<%@ page import="org.oscarehr.PMmodule.service.ProviderManager" %>
<%@ page import="org.oscarehr.PMmodule.service.ProgramManager" %>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="org.oscarehr.util.SessionConstants"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.appt.ApptData" %>
<%@ page import="oscar.appt.ApptUtil" %>
<%@ page import="oscar.appt.status.service.AppointmentStatusMgr"%>
<%@ page import="oscar.appt.status.service.impl.AppointmentStatusMgrImpl"%>
<%@ page import="oscar.MyDateFormat" %>
<%@ page import="oscar.oscarBilling.ca.on.data.BillingDataHlp" %>
<%@ page import="oscar.oscarEncounter.data.EctFormData"%>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="oscar.util.ConversionUtils" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>
<%@ taglib uri="/WEB-INF/wellAiVoice.tld" prefix="well-ai-voice"%>

<jsp:useBean id="providerBean" class="java.util.Properties" scope="session" />
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_appointment" rights="u" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_appointment");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
  if (session.getAttribute("user") == null)    response.sendRedirect("../logout.jsp");
  String curProvider_no = request.getParameter("provider_no");
  String appointment_no = request.getParameter("appointment_no");
  String curUser_no = (String) session.getAttribute("user");
  String creatorName =
          (String) session.getAttribute("userlastname") + ", "
          + (String) session.getAttribute("userfirstname");
  String deepcolor = "#CCCCFF", weakcolor = "#EEEEFF";
  String origDate = null;
  boolean bFirstDisp = true; //this is the first time to display the window
  if (request.getParameter("bFirstDisp")!=null) bFirstDisp = (request.getParameter("bFirstDisp")).equals("true");
    String mrpName = "";
	DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
	EncounterFormDao encounterFormDao = SpringUtils.getBean(EncounterFormDao.class);
    ProviderPreference providerPreference=(ProviderPreference)session.getAttribute(SessionConstants.LOGGED_IN_PROVIDER_PREFERENCE);
    DemographicManager demographicManager = SpringUtils.getBean(DemographicManager.class);
    OscarAppointmentDao appointmentDao = SpringUtils.getBean(OscarAppointmentDao.class);
    AppointmentArchiveDao appointmentArchiveDao = SpringUtils.getBean(AppointmentArchiveDao.class);
    ProviderDataDao providerDao = SpringUtils.getBean(ProviderDataDao.class);
	AppointmentTypeDao appointmentTypeDao = SpringUtils.getBean(AppointmentTypeDao.class);
	SiteDao siteDao = SpringUtils.getBean(SiteDao.class);
	ProviderDao pDao = SpringUtils.getBean(ProviderDao.class);
	BillingONCHeader1Dao cheader1Dao = SpringUtils.getBean(BillingONCHeader1Dao.class);
    boolean twelveHourFormat = providerPreference.isTwelveHourFormat();
    ProviderManager providerManager = SpringUtils.getBean(ProviderManager.class);
	ProgramManager programManager = SpringUtils.getBean(ProgramManager.class);
	String demographic_nox = request.getParameter("demographic_no");
	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
    Demographic demographicTmp=demographicManager.getDemographic(loggedInInfo,demographic_nox);
    String proNoTmp = demographicTmp==null?null:demographicTmp.getProviderNo();
    if (demographicTmp!=null&&proNoTmp!=null&&proNoTmp.length()>0) {
            Provider providerTmp=pDao.getProvider(demographicTmp.getProviderNo());
        if (providerTmp != null) {
            mrpName = providerTmp.getFormattedName();
        }
    }
	String providerNo = loggedInInfo.getLoggedInProviderNo();
	Facility facility = loggedInInfo.getCurrentFacility();
    List<Program> programs = programManager.getActiveProgramByFacility(providerNo, facility.getId());
	List<AppointmentType> appointmentTypes;
	if (OscarProperties.getInstance().isPropertyActive("provider_appointment_select")) {
		appointmentTypes = appointmentTypeDao.findAllEnabledForProvider(curProvider_no);
	} else {
		appointmentTypes = appointmentTypeDao.listAllTemplates();
	}
    Collections.sort(appointmentTypes, AppointmentType.ORDER_BY_NAME);
    LookupListManager lookupListManager = SpringUtils.getBean(LookupListManager.class);
    LookupList reasonCodes = lookupListManager.findLookupListByName(loggedInInfo, "reasonCode");
    ApptData apptObj = ApptUtil.getAppointmentFromSession(request);
 List<BillingONCHeader1> cheader1s = null;
 if("ON".equals(OscarProperties.getInstance().getProperty("billregion", "ON"))) {
 	cheader1s = cheader1Dao.getBillCheader1ByDemographicNo(Integer.parseInt(demographic_nox));
 }
 BillingONExtDao billingOnExtDao = SpringUtils.getBean(BillingONExtDao.class);
    oscar.OscarProperties pros = oscar.OscarProperties.getInstance();
    String strEditable = pros.getProperty("ENABLE_EDIT_APPT_STATUS");
  String apptStatusHere = pros.getProperty("appt_status_here");
    AppointmentStatusMgr apptStatusMgr =  new AppointmentStatusMgrImpl();
    List<AppointmentStatus> allStatus = apptStatusMgr.getAllActiveStatus();
    Boolean isMobileOptimized = session.getAttribute("mobileOptimized") != null;
    boolean useProgramLocation = SystemPreferencesUtils
            .isReadBooleanPreferenceWithDefault("use_program_location_enabled", false);
    String moduleNames = OscarProperties.getInstance().getProperty("ModuleNames");
    boolean caisiEnabled = moduleNames != null && org.apache.commons.lang.StringUtils.containsIgnoreCase(moduleNames, "Caisi");
    boolean locationEnabled = caisiEnabled && useProgramLocation;
    boolean enableAppointmentMcNumber = SystemPreferencesUtils
            .isReadBooleanPreferenceWithDefault("enable_appointment_mc_number", false);
    boolean allowMultipleSameDayGroupAppts = SystemPreferencesUtils
            .isReadBooleanPreferenceWithDefault("allow_multiple_same_day_group_appts", true);
%>
<html:html locale="true">
<head>
<% if (isMobileOptimized) { %>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, width=device-width" />
    <link rel="stylesheet" href="../mobile/appointmentstyle.css" type="text/css">
<% } else { %>
    <link rel="stylesheet" href="appointmentstyle.css" type="text/css">
    <style type="text/css">
        .deep { background-color: <%= deepcolor %>; }
        .weak { background-color: <%= weakcolor %>; }
    </style>
<% } %>
<%-- Calendar.js --%>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/calendar/calendar.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/calendar/lang/<bean:message key="global.javascript.calendar"/>"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/calendar/calendar-setup.js"></script>
<link rel="stylesheet" type="text/css" media="all" href="<%= request.getContextPath() %>/share/calendar/calendar.css" title="win2k-cold-1" />
	
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<title><bean:message key="appointment.editappointment.title" /></title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
   <script>
     jQuery.noConflict();
   </script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-3.1.0.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.maskedinput.js"></script>
	<script type="application/javascript">
		var jQuery_3_1_0 = jQuery.noConflict(true);
	</script>
<script language="javascript">
<!-- // start javascript
function toggleView() {
    showHideItem('editAppointment');
    showHideItem('viewAppointment');
}
function demographicdetail(vheight,vwidth) {
  if(document.forms['EDITAPPT'].demographic_no.value=="") return;
  self.close();
  var page = "../demographic/demographiccontrol.jsp?demographic_no=" + document.forms['EDITAPPT'].demographic_no.value+"&displaymode=edit&dboperation=search_detail";
  windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=600,screenY=200,top=0,left=0";
  var popup=window.open(page, "demographic", windowprops);
}
function onButRepeat() {
	if(calculateEndTime()) {
		document.forms[0].action = "appointmenteditrepeatbooking.jsp" ;
		document.forms[0].submit();
	}
}

var saveTemp=0;

function setfocus() {
  this.focus();
  document.EDITAPPT.keyword.focus();
  document.EDITAPPT.keyword.select();
}

function onBlockFieldFocus(obj) {
  obj.blur();
  document.EDITAPPT.keyword.focus();
  document.EDITAPPT.keyword.select();
  window.alert("<bean:message key="Appointment.msgFillNameField"/>");
}
function labelprint(vheight,vwidth,varpage) {
  var page = "" + varpage;
  windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=600,screenY=200,top=0,left=0";
  var popup=window.open(page, "encounterhist", windowprops);
}
function onButDelete() {
  saveTemp=1;
}
function onButUpdate() {
  saveTemp=2;
}

function onButCancel(){
   var aptStat = document.EDITAPPT.status.value;
   if (aptStat.indexOf('B') == 0){
       var agree = confirm("<bean:message key="appointment.editappointment.msgCanceledBilledConfirmation"/>") ;
       if (agree){
          window.location='appointmentcontrol.jsp?buttoncancel=Cancel Appt&displaymode=Update Appt&appointment_no=<%=appointment_no%>';
       }
   }else{
      window.location='appointmentcontrol.jsp?buttoncancel=Cancel Appt&displaymode=Update Appt&appointment_no=<%=appointment_no%>';
   }
}
function upCaseCtrl(ctrl) {
	ctrl.value = ctrl.value.toUpperCase();
}
function onSub() {
  if( saveTemp==1 ) {
    var aptStat = document.EDITAPPT.status.value;
    if (aptStat.indexOf('B') == 0){
       return (confirm("<bean:message key="appointment.editappointment.msgDeleteBilledConfirmation"/>")) ;
    }else{
       return (confirm("<bean:message key="appointment.editappointment.msgDeleteConfirmation"/>")) ;
    }
  }
  if( saveTemp==2 ) {
    if (document.EDITAPPT.isMultisitesLocationActiveFlag !== undefined
        && document.EDITAPPT.location.value === '') {
      window.alert("<bean:message key="appointment.editappointment.appointmentLocationRequired"/>");
      return false;
    }

    if (document.EDITAPPT.notes.value.length > 255) {
      window.alert("<bean:message key="appointment.editappointment.msgNotesTooBig"/>");
      return false;
    }
    return calculateEndTime() ;
  } else
      return true;
}



function calculateEndTime() {
  var stime = document.EDITAPPT.start_time.value;
  var vlen = stime.indexOf(':')==-1?1:2;
  var amPmStr = "";
  if ("<%=twelveHourFormat%>" == "true") {
	  if (stime.substring(5).trim().toUpperCase() == "AM" || stime.substring(5).trim().toUpperCase() == "PM") {
		  amPmStr = stime.substring(5).trim();
	  }
  }
  stime = stime.substring(0, 5);
  if(vlen==1 && stime.length==4 ) {
	  document.EDITAPPT.start_time.value = stime.substring(0,2) +":"+ stime.substring(2);
    stime = document.EDITAPPT.start_time.value;
  }
  if(stime.length!=5) {
    alert("<bean:message key="Appointment.msgInvalidDateFormat"/>");
    return false;
  }
  document.EDITAPPT.start_time.value = stime + amPmStr;
	
  var shour = stime.substring(0,2) ;
  if (shour == "12" && amPmStr.toUpperCase() == "AM") { shour = parseInt(shour) + 12; }
  else if (shour != "12" && amPmStr.toUpperCase() == "PM") { shour = parseInt(shour) + 12; }
  var smin = stime.substring(stime.length-vlen) ;
  var duration = document.EDITAPPT.duration.value ;
  
  if(isNaN(duration)) {
	  alert("<bean:message key="Appointment.msgFillTimeField"/>");
	  return false;
  }
  
  if(eval(duration) == 0) { duration =1; }
  if(eval(duration) < 0) { duration = Math.abs(duration) ; }

  var lmin = eval(smin)+eval(duration)-1 ;
  var lhour = parseInt(lmin/60);

  if((lmin) > 59) {
    shour = eval(shour) + eval(lhour);
    shour = shour<10?("0"+shour):shour;
    smin = lmin - 60*lhour;
  } else {
    smin = lmin;
  }
  smin = smin<10?("0"+ smin):smin;
  document.EDITAPPT.end_time.value = shour +":"+ smin;
  if(shour > 23) {
    alert("<bean:message key="Appointment.msgCheckDuration"/>");
    return false;
  }
  return true;
}

function checkTypeNum(typeIn) {
	var typeInOK = true;
	var i = 0;
	var length = typeIn.length;
	var ch;

	// walk through a string and find a number
	if (length>=1) {
	  while (i <  length) {
		  ch = typeIn.substring(i, i+1);
		  if (ch == ":") { i++; continue; }
		  if ((ch < "0") || (ch > "9") ) {
			  typeInOK = false;
			  break;
		  }
	    i++;
      }
	} else typeInOK = false;
	return typeInOK;
}

function checkTimeTypeIn(obj) {
  var colonIdx;
  var timeVal = obj.value;
  var amPmStr = "";
  if ("<%=twelveHourFormat%>" == "true") {
	  if (timeVal.substring(5).trim().toUpperCase() == "AM" || timeVal.substring(5).trim().toUpperCase() == "PM") {
		  amPmStr = timeVal.substring(5).trim();
	  }
  }
  timeVal = timeVal.substring(0, 5);
  if(!checkTypeNum(timeVal) ) {
	  alert ("<bean:message key="Appointment.msgFillTimeField"/>");
  } else {
  	colonIdx = timeVal.indexOf(':');
  	if(colonIdx==-1) {
  		if(timeVal.length < 3) alert("<bean:message key="Appointment.msgFillValidTimeField"/>");
  		timeVal = timeVal.substring(0, timeVal.length-2 )+":"+timeVal.substring( timeVal.length-2 );
  	}
  }
          
  var hours = "";
  var minutes = "";  

  colonIdx = timeVal.indexOf(':');  
  if (colonIdx < 1)
      hours = "00";     
  else if (colonIdx == 1)
      hours = "0" + timeVal.substring(0,1);
  else
      hours = timeVal.substring(0,2);
  
  minutes = timeVal.substring(colonIdx+1,colonIdx+3);
  if (minutes.length == 0)
	    minutes = "00";
	else if (minutes.length == 1)
		minutes = "0" + minutes;
	else if (minutes > 59)
		minutes = "00";

  obj.value = hours + ":" + minutes + amPmStr;
}

<% if (apptObj!=null) { %>
function pasteAppt(multipleSameDayGroupAppt) {

        var warnMsgId = document.getElementById("tooManySameDayGroupApptWarning");

        if (multipleSameDayGroupAppt) {
           warnMsgId.style.display = "block";
           if (document.EDITAPPT.updateButton) {
              document.EDITAPPT.updateButton.style.display = "none";
           }
           if (document.EDITAPPT.groupButton) {
              document.EDITAPPT.groupButton.style.display = "none";
           }
           if (document.EDITAPPT.deleteButton){
              document.EDITAPPT.deleteButton.style.display = "none";
           }
           if (document.EDITAPPT.cancelButton){
              document.EDITAPPT.cancelButton.style.display = "none";
           }
           if (document.EDITAPPT.noShowButton){
              document.EDITAPPT.noShowButton.style.display = "none";
           }
           if (document.EDITAPPT.labelButton) {
                document.EDITAPPT.labelButton.style.display = "none";
           }
           if (document.EDITAPPT.repeatButton) {
                document.EDITAPPT.repeatButton.style.display = "none";
           }
        }
        //else {
        //   warnMsgId.style.display = "none";
        //}
	document.EDITAPPT.duration.value = "<%= Encode.forJavaScriptBlock(apptObj.getDuration()) %>";
	document.EDITAPPT.chart_no.value = "<%= Encode.forHtmlAttribute(Encode.forJavaScriptBlock(apptObj.getChart_no())) %>";
	document.EDITAPPT.keyword.value = "<%= Encode.forJavaScriptBlock(apptObj.getName()) %>";
	document.EDITAPPT.demographic_no.value = "<%= Encode.forJavaScriptBlock(apptObj.getDemographic_no()) %>";
	document.EDITAPPT.reason.value = "<%= Encode.forJavaScriptBlock(apptObj.getReason()) %>";
	document.EDITAPPT.notes.value = "<%= Encode.forJavaScriptBlock(apptObj.getNotes()) %>";
	document.EDITAPPT.location.value = "<%= Encode.forJavaScriptBlock(apptObj.getLocation()) %>";
	document.EDITAPPT.resources.value = "<%= Encode.forJavaScriptBlock(apptObj.getResources()) %>";
	document.EDITAPPT.type.value = "<%= Encode.forJavaScriptBlock(apptObj.getType()) %>";
	document.EDITAPPT.typeSel.value = "<%= Encode.forJavaScriptBlock(apptObj.getAppointmentType()) %>";
    <% if (apptObj.isMoved()) {
        creatorName = Encode.forJavaScriptBlock(apptObj.getCreator());
    %>
        document.EDITAPPT.createdatetime.value = "<%= Encode.forJavaScriptBlock(apptObj.getCreatedatetime()) %>";
        document.EDITAPPT.creator.value = "<%= Encode.forJavaScriptBlock(apptObj.getCreator()) %>";
    <% } %>
	if('<%=apptObj.getUrgency()%>' == 'critical') {
		document.EDITAPPT.urgency.checked = "checked";
	}
}
<% } %>
function onCut() {
  document.EDITAPPT.submit();
}

function openTypePopup () {
    windowprops = "height=340,width=720,location=no,scrollbars=no,menubars=no,toolbars=no,resizable=yes,screenX=0,screenY=0,top=100,left=100";
    var popup=window.open("appointmentType.jsp?type="+document.forms['EDITAPPT'].type.value, "Appointment Type", windowprops);
    if (popup != null) {
      if (popup.opener == null) {
        popup.opener = self;
      }
      popup.focus();
    }
}
<%
Appointment appt = null;
Boolean deleted = false;
String selectedApptType;
 appt = appointmentDao.find(Integer.parseInt(appointment_no));
 
 if (appt == null) {
     AppointmentArchive appointmentArchive = appointmentArchiveDao.getMostRecent(Integer.parseInt(appointment_no));
     if (appointmentArchive != null) {
         appt = new Appointment();
         BeanUtils.copyProperties(appt, appointmentArchive);
         appt.setId(appointmentArchive.getAppointmentNo());
         deleted=true;
     }
 }
if (bFirstDisp) {
    selectedApptType = appt.getType();
} else {
    selectedApptType = request.getParameter("type");
}
%>

var previousTypeId = 0;
var appointmentTypeData = {};
<%	for (AppointmentType appointmentType : appointmentTypes) {
    if (selectedApptType != null && selectedApptType.equals(appointmentType.getName())) { %>
    previousTypeId = '<%= appointmentType.getId()%>';
 <% } %>
        
	appointmentTypeData['<%=appointmentType.getId()%>'] = {};
	appointmentTypeData['<%=appointmentType.getId()%>'].name = '<%=StringEscapeUtils.escapeJavaScript(appointmentType.getName())%>';
	appointmentTypeData['<%=appointmentType.getId()%>'].duration = '<%=appointmentType.getDuration()%>';
	appointmentTypeData['<%=appointmentType.getId()%>'].reasonCode = '<%=appointmentType.getReasonCode()%>';
	appointmentTypeData['<%=appointmentType.getId()%>'].reason = '<%=StringEscapeUtils.escapeJavaScript(appointmentType.getReason())%>';
	appointmentTypeData['<%=appointmentType.getId()%>'].location = '<%=StringEscapeUtils.escapeJavaScript(appointmentType.getLocation())%>';
	appointmentTypeData['<%=appointmentType.getId()%>'].notes = '<%=StringEscapeUtils.escapeJavaScript(appointmentType.getNotes())%>';
	appointmentTypeData['<%=appointmentType.getId()%>'].resources = '<%=StringEscapeUtils.escapeJavaScript(appointmentType.getResources())%>';
<%	} %>

	function setAppointmentType(appointmentTypeDataId) {
		var typeData = appointmentTypeData[appointmentTypeDataId];
		if (typeof typeData !== 'undefined') {
			<%	if (OscarProperties.getInstance().isPropertyActive("provider_appointment_select")) { %>
			document.forms['EDITAPPT'].typeSel.value = appointmentTypeDataId;
			<% } %>
			document.forms['EDITAPPT'].type.value = typeData.name;
			document.forms['EDITAPPT'].duration.value = typeData.duration;
			document.forms['EDITAPPT'].notes.value = typeData.notes;
			document.forms['EDITAPPT'].resources.value = typeData.resources;
			var locations = document.forms['EDITAPPT'].location;
			if (locations.nodeName === 'SELECT') {
				for (var i = 0; i < locations.length; i++) {
					if (locations.options[i].innerHTML === typeData.location) {
						locations.selectedIndex = i;
						locations.style.backgroundColor = locations.options[locations.selectedIndex].style.backgroundColor;
						break;
					}
				}
			} else if (locations.nodeName === "INPUT") {
				document.forms['EDITAPPT'].location.value = typeData.location;
			}
			
			var existingReason = document.forms['EDITAPPT'].reason.value;
			if (previousTypeId > 0) {
			    var previousType = appointmentTypeData[previousTypeId];
			    existingReason = existingReason.replace(previousType.reason, "");
                document.forms['EDITAPPT'].reason.value = existingReason;
            }
			
			if (typeData.reason.length > 0) {
                var reasonOption = $("select[name='reasonCode'] option[value='" + typeData.reason + "']");
                if (reasonOption.length > 0) {
                    reasonOption[0].selected = true;
                } else {
                    document.forms['EDITAPPT'].reason.value = existingReason + (existingReason.length > 0 && !existingReason.endsWith("\n") ? "\n" : "") + typeData.reason;
                }
            }
			
			previousTypeId = appointmentTypeDataId;
    } else {
      <%	if (OscarProperties.getInstance().isPropertyActive("provider_appointment_select")) { %>
        document.forms['EDITAPPT'].typeSel.value = appointmentTypeDataId;
      <% } %>
        document.forms['EDITAPPT'].type.value = '';
        document.forms['EDITAPPT'].notes.value = '';
        document.forms['EDITAPPT'].resources.value = '';
        document.forms['EDITAPPT'].duration.value = 15;
        document.forms['EDITAPPT'].reason.value = '';
      }
	}

</script>
</head>
<body onload="setfocus()" bgproperties="fixed"
      topmargin="0" leftmargin="0" rightmargin="0" bottommargin="0">
  <well-ai-voice:script/>
<!-- The mobile optimized page is split into two sections: viewing and editing an appointment
     In the mobile version, we only display the edit section first if we are returning from a search -->
<div style="display: flex;">
		<% 	 
		ThirdPartyApplicationDao thirdPartyApplicationDao = SpringUtils.getBean(ThirdPartyApplicationDao.class);
		
		List<ThirdPartyApplication> apps = thirdPartyApplicationDao.findByEmrContext(EmrContextEnum.APPOINTMENT);
		if (apps != null && !apps.isEmpty()) {
     	%>
		
			<jsp:include page="../common/thirdPartyAppSideBar.jsp">
				<jsp:param name="context" value="<%= EmrContextEnum.APPOINTMENT %>"/>
				<jsp:param name="demographicNo" value="<%= demographic_nox %>"/>
				<jsp:param name="providerNo" value="<%= curProvider_no %>"/>
				<jsp:param name="appointmentId" value="<%= appointment_no %>"/>
			</jsp:include>
	<%
     	}
	%>
<div style="flex: auto;">
<div id="editAppointment" style="display:<%= (isMobileOptimized && bFirstDisp) ? "none":"block"%>;">
<FORM NAME="EDITAPPT" METHOD="post" ACTION="appointmentcontrol.jsp"
	onSubmit="return(onSub())"><INPUT TYPE="hidden"
	NAME="displaymode" value="">
    <div class="header deep">
        <div class="title">
            <!-- We display a shortened title for the mobile version -->
            <% if (deleted) { %> VIEW APPOINTMENT
            <% } else if (isMobileOptimized) { %><bean:message key="appointment.editappointment.msgMainLabelMobile" />
            <% } else { %><bean:message key="appointment.editappointment.msgMainLabel" />
            <% } %>
        </div>
        <a href="javascript:toggleView();" id="viewButton" class="leftButton top">
            <bean:message key="appointment.editappointment.btnView" />
        </a>
    </div>

<%
	
	String demoNo="", chartno="", cellPhone="", rosterstatus="", alert="", doctorNo="";
	String strApptDate = bFirstDisp?"":request.getParameter("appointment_date") ;

	if (bFirstDisp) {
		if (appt == null) {
%>
<bean:message key="appointment.editappointment.msgNoSuchAppointment" />
<%
			return;
		}
	}


	if (bFirstDisp) {
		demoNo = String.valueOf(appt.getDemographicNo());
	} else if (request.getParameter("demographic_no")!=null && !request.getParameter("demographic_no").equals("")) {
		demoNo = request.getParameter("demographic_no");
	}

	Demographic demographic = null;
	//get chart_no from demographic table if it exists
	if (!demoNo.equals("0") && !demoNo.equals("") && NumberUtils.isParsable(demoNo)) {
		demographic = demographicManager.getDemographic(loggedInInfo, demoNo);
		if(demographic != null) {
			chartno = demographic.getChartNo();
			rosterstatus = demographic.getRosterStatus();
		}

        String bookingAlert = demographicExtDao.getValueForDemoKey(
                Integer.parseInt(request.getParameter("demographic_no")),
                DemographicExtKey.ALERT_BOOKING
        );
        if (StringUtils.isNotBlank(bookingAlert)) {
			alert = bookingAlert;
		}
		
		cellPhone = demographicExtDao.getValueForDemoKey(Integer.parseInt(demoNo), "demo_cell");

	}

        OscarProperties props = OscarProperties.getInstance();
        String displayStyle="display:none";
        String myGroupNo = providerPreference.getMyGroupNo();
        boolean bMultipleSameDayGroupAppt = false;
        if (!allowMultipleSameDayGroupAppts) {

            if (!bFirstDisp && !demoNo.equals("0") && !demoNo.equals("")) {
                String [] sqlParam = new String[3] ;
                sqlParam[0] = myGroupNo; //schedule group
                sqlParam[1] = demoNo;
                sqlParam[2] = strApptDate;

               List<Appointment> aa = appointmentDao.search_group_day_appt(myGroupNo,Integer.parseInt(demoNo),ConversionUtils.fromDateString(strApptDate));
                
                long numSameDayGroupAppts = aa.size() > 0 ? new Long(aa.size()) : 0;
                bMultipleSameDayGroupAppt = (numSameDayGroupAppts > 0);
            }

            if (bMultipleSameDayGroupAppt){
                displayStyle="display:block";
            }
%>
<div id="tooManySameDayGroupApptWarning" style="<%=displayStyle%>">
    <table width="98%" BGCOLOR="red" border=1 align='center'>
        <tr>
            <th>
                <font color='white'>
                    <bean:message key='appointment.addappointment.titleMultipleGroupDayBooking'/><br/>
                    <bean:message key='appointment.addappointment.MultipleGroupDayBooking'/>
                </font>
            </th>
        </tr>
    </table>
</div>
 <%
        }
        if (StringUtils.isNotEmpty(alert)) { 
 %>
    <table width="98%" bgcolor="yellow" border=1 align='center'>
        <tr>
            <td><font color='red'><bean:message key="Appointment.formAlert" />: <b><%=Encode.forHtml(alert)%></b></font></td>
        </tr>
    </table>
<% }

    //RJ 07/12/2006
    //If page is loaded first time hit db for patient's family doctor
    //Else if we are coming back from search this has been done for us
    //Else how did we get here?
    if( bFirstDisp ) {
    		oscar.oscarDemographic.data.DemographicData dd = new oscar.oscarDemographic.data.DemographicData();
        org.oscarehr.common.model.Demographic demo = dd.getDemographic(loggedInInfo, String.valueOf(appt.getDemographicNo()));
        doctorNo = demo!=null ? (demo.getProviderNo()) : "";
    } else if (request.getParameter("doctor_no") != null && !request.getParameter("doctor_no").equals("")) {
        doctorNo = request.getParameter("doctor_no");
    }
%>
<div class="panel">
    <ul>
        <li class="row weak">
            <div class="label">
                <bean:message key="Appointment.formDate" />:
            </div>
            <div class="input">
				<input type="text" name="appointment_date" id="appointment_date" WIDTH="25" HEIGHT="20" border="0"
					   value="<%=bFirstDisp?ConversionUtils.toDateString(appt.getAppointmentDate()):strApptDate%>">
				<img src="../images/cal.gif" id="appointment_date_cal">
				<script type="application/javascript">createStandardDatepicker(jQuery_3_1_0('#appointment_date'), "appointment_date_cal");</script>
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formStatus" />:</div>
            <div class="input">
<%
              String statusCode = request.getParameter("status");
			  String importedStatus = null;
              if (bFirstDisp){
                  statusCode =appt.getStatus();
                  importedStatus = appt.getImportedStatus();
              }
              
			  String arrivalTime = ConversionUtils.toTimestampString(appt.getArrivalTime());
			  
%>              
			  <input type='hidden' name='arrivalTime' value=<%=arrivalTime%>>
              <input type='hidden' name='oldStatus' value=<%=statusCode%> >

<%            String signOrVerify = "";
              if (statusCode.length() >= 2){
                  signOrVerify = statusCode.substring(1,2);
                  statusCode = statusCode.substring(0,1);
              }
              if (strEditable!=null && strEditable.equalsIgnoreCase("yes")) { %>
                <select name="status" STYLE="width: 154px">
					<% for (int i = 0; i < allStatus.size(); i++) { %>
					<option
						value="<%=Encode.forHtmlAttribute(((AppointmentStatus)allStatus.get(i)).getStatus())+signOrVerify%>"
						<%=((AppointmentStatus)allStatus.get(i)).getStatus().equals(statusCode)?"SELECTED":""%>><%=Encode.forHtmlContent(((AppointmentStatus)allStatus.get(i)).getDescription())%></option>
					<% } %>
				</select> <%
              } else {
              	if (importedStatus==null || importedStatus.trim().equals("")) { %>
              	<INPUT TYPE="TEXT" NAME="status" VALUE="<%=Encode.forHtmlAttribute(statusCode)%>" WIDTH="25"> <%
              	} else { %>
                <INPUT TYPE="TEXT" NAME="status" VALUE="<%=Encode.forHtmlAttribute(statusCode)%>" WIDTH="25">
                <INPUT TYPE="TEXT" TITLE="Imported Status" VALUE="<%=Encode.forHtmlAttribute(importedStatus)%>" WIDTH="25" readonly> <%
              	}
              }
%>
            </div>
        </li>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formStartTime" />:</div>
            <div class="input">
				<%
					String startTimeFormatted;
					if (bFirstDisp) {
						startTimeFormatted = (twelveHourFormat ? MyDateFormat.getTimeXX_XXampm(ConversionUtils.toTimeStringNoSeconds(appt.getStartTime())) : ConversionUtils.toTimeStringNoSeconds(appt.getStartTime()));
					} else {
						startTimeFormatted = request.getParameter("start_time");
					}
				%>
                <INPUT TYPE="TEXT"
					NAME="start_time"
					VALUE="<%=startTimeFormatted%>"
                    WIDTH="25"
                    HEIGHT="20" border="0" onChange="checkTimeTypeIn(this)" >
            </div>
            <div class="space">&nbsp;</div>

            <%
                        // multisites start
                boolean bMultisites = org.oscarehr.common.IsPropertiesOn.isMultisitesEnable();

            
            String siteProviderNo = curProvider_no != null ? curProvider_no : (String) session.getAttribute("user");
            List<Site> sites = siteDao.getActiveSitesByProviderNo(siteProviderNo);
            // multisites end

            boolean bMoreAddr = bMultisites? true : props.getProperty("scheduleSiteID", "").equals("") ? false : true;

            String loc = bFirstDisp?(appt.getLocation()):request.getParameter("location");
            String colo = bMultisites
                                        ? ApptUtil.getColorFromLocation(sites, loc)
                                        : bMoreAddr? ApptUtil.getColorFromLocation(props.getProperty("scheduleSiteID", ""), props.getProperty("scheduleSiteColor", ""),loc) : "white";
            %>
			<%	if (OscarProperties.getInstance().isPropertyActive("provider_appointment_select")) { %>
			<div class="label"><bean:message key="Appointment.formType"/>:</div>
			<div class="input">
				<select style="width: 154px" name="typeSel" id="typeSel" onchange="setAppointmentType(this.value)">
					<option value="">Select type</option>
					<%	for (AppointmentType apptType : appointmentTypes) { %>
					<option value="<%=apptType.getId()%>" <%=(apptType.getName().equals(selectedApptType)?"selected=\"selected\"":"")%>><%=Encode.forHtmlContent(apptType.getName())%></option>
						<% } %>
				</select>
				<input type="hidden" name="type" value='<%=Encode.forHtmlAttribute(selectedApptType)%>'>
			</div>
			<%	} else { %>
			<div class="label">
				<INPUT TYPE="button" NAME="typeButton" VALUE="<bean:message key="Appointment.formType"/>" onClick="openTypePopup()">
            </div>
            <div class="input">
                <INPUT TYPE="TEXT" NAME="type"
					VALUE="<%=Encode.forHtmlAttribute(bFirstDisp?appt.getType():request.getParameter("type"))%>"
                    WIDTH="25">
            </div>
			<%	} %>
        </li>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formDuration" />:</div>
            <div class="input">
				<%
  int everyMin = 1;
  StringBuilder nameSb = new StringBuilder();
  if(bFirstDisp) {
	  Calendar startCal = Calendar.getInstance();
	  startCal.setTime(appt.getStartTime());
	  Calendar endCal = Calendar.getInstance();
	  endCal.setTime(appt.getEndTime());
	  
    int endtime = (endCal.get(Calendar.HOUR_OF_DAY) )*60 + (endCal.get(Calendar.MINUTE)) ;
    int starttime = (startCal.get(Calendar.HOUR_OF_DAY) )*60 + (startCal.get(Calendar.MINUTE)) ;
    everyMin = endtime - starttime +1;

    if (!demoNo.equals("0") && !demoNo.equals("") && (demographicManager != null)) {
        Demographic demo = demographicManager.getDemographic(loggedInInfo, demoNo);
        nameSb.append(demo.getLastName())
              .append(",")
              .append(demo.getFirstName());
    }
    else {
        nameSb.append(appt.getName());
    }
  }
%> <INPUT TYPE="hidden" NAME="end_time"
					VALUE="<%=bFirstDisp?ConversionUtils.toTimeStringNoSeconds(appt.getEndTime()):request.getParameter("end_time")%>"
					WIDTH="25" HEIGHT="20" border="0" onChange="checkTimeTypeIn(this)">
				<%--              <INPUT TYPE="hidden" NAME="end_time" VALUE="<%=request.getParameter("end_time")%>" WIDTH="25" HEIGHT="20" border="0" onChange="checkTimeTypeIn(this)">--%>
				<INPUT TYPE="TEXT" NAME="duration"
					VALUE="<%=request.getParameter("duration")!=null?(request.getParameter("duration").equals(" ")||request.getParameter("duration").equals("")||request.getParameter("duration").equals("null")?(""+everyMin) :request.getParameter("duration")):(""+everyMin)%>"
                    WIDTH="25">
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formChartNo" />:</div>
            <div class="input">
              <input type="TEXT" name="chart_no" readonly
                     value="<%= bFirstDisp?StringUtils.trimToEmpty(chartno):Encode.forHtmlAttribute(request.getParameter("chart_no")) %>"
                     width="25">
            </div>
        </li>
			<% if(providerBean != null && doctorNo != null && providerBean.getProperty(doctorNo)!=null) {%>
        <li class="row weak">
            <div class="label"></div>
            <div class="input"></div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formDoctor" />:</div>
            <div class="input">
                <INPUT type="TEXT" name="doctorNo" readonly
                   value="<%=providerBean.getProperty(doctorNo)%>"
                   width="25">
            </div>
        </li>
			<% } %>
        <li class="row weak">
            <div class="label"><a href="#"onclick="demographicdetail(550,700)">
                    <bean:message key="Appointment.formName" /></a>:
            </div>
          <%
            boolean increaseNameFieldLength = SystemPreferencesUtils.isReadBooleanPreferenceWithDefault(
                "increase_last_name_field_width_enabled", false);
          %>
            <div class="input">
                <input type="text" name="keyword" id="<%=increaseNameFieldLength ? "name_increasedWidth" : ""%>"
					tabindex="1"
					VALUE="<%=Encode.forHtmlAttribute(bFirstDisp?nameSb.toString():request.getParameter("name"))%>"
                    width="25">
            </div>
            <div class="space">&nbsp;</div>
            <div class="label">
		<INPUT TYPE="hidden" NAME="orderby" VALUE="last_name, first_name">
<%
    String searchMode = Encode.forHtmlAttribute(request.getParameter("search_mode"));
    if (searchMode == null || searchMode.isEmpty() || "null".equals(searchMode)) {
        searchMode = OscarProperties.getInstance().getProperty("default_search_mode","search_name");
    }
%>
              <INPUT TYPE="hidden" NAME="search_mode"
                     VALUE="<%= Encode.forHtmlAttribute(searchMode) %>">
                <INPUT TYPE="hidden" NAME="originalpage" VALUE="<%=request.getContextPath()%>/appointment/editappointment.jsp">
                <INPUT TYPE="hidden" NAME="limit1" VALUE="0">
                <INPUT TYPE="hidden" NAME="limit2" VALUE="5">
                <INPUT TYPE="hidden" NAME="ptstatus" VALUE="active">
                <!--input type="hidden" name="displaymode" value="Search " -->
                <input type="submit" style="width:auto;"
					onclick="document.forms['EDITAPPT'].displaymode.value='Search '"
                    value="<bean:message key="appointment.editappointment.btnSearch"/>">
            </div>
            <div class="input">
                <input type="TEXT"
					name="demographic_no" onFocus="onBlockFieldFocus(this)" readonly
					value="<%=bFirstDisp?( (appt.getDemographicNo())==0?"":(""+appt.getDemographicNo()) ):request.getParameter("demographic_no")%>"
                    width="25">
            </div>
        </li>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formReason" />:</div>
            <div class="input">
				<select name="reasonCode">
                    <option value="0" selected></option>
					<%
					Integer apptReasonCode = bFirstDisp ? (appt.getReasonCode() == null ? 0 : appt.getReasonCode()) : Integer.parseInt(request.getParameter("reasonCode"));
					if(reasonCodes != null) {
						for(LookupListItem reasonCode : reasonCodes.getItems()) {
							if(reasonCode.isActive() || (apptReasonCode.equals(reasonCode.getId()) && !reasonCode.isActive())) {
							    String reason = StringEscapeUtils.escapeHtml(reasonCode.getLabel());
					%>
						<option value="<%=reasonCode.getId()%>" <%=apptReasonCode.equals(reasonCode.getId()) ? "selected=\"selected\"" : "" %>><%=reason==null?"":reason%></option>
					<%
							} } //end of for loop
					} else {
					%>
						<option value="-1">Other</option>
					<%
					}
					%>
				</select>
 				</br>
				<textarea id="reason" name="reason" tabindex="2" rows="2" wrap="virtual"
					cols="18"><%=Encode.forHtmlContent(bFirstDisp?appt.getReason():request.getParameter("reason"))%></textarea>
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formNotes" />:</div>
            <div class="input">
				<textarea name="notes" maxlength="255" tabindex="3" rows="2" wrap="virtual"
					cols="18"><%=Encode.forHtmlContent(bFirstDisp?appt.getNotes():request.getParameter("notes"))%></textarea>
            </div>
        </li>
			<% if (enableAppointmentMcNumber) {
		String mcNumber = OtherIdManager.getApptOtherId(appointment_no, "appt_mc_number");
%>
        <li class="row weak">
            <div class="label">M/C number :</div>
            <div class="input">
                <input type="text" name="appt_mc_number" tabindex="4"
                    value="<%=bFirstDisp?mcNumber:request.getParameter("appt_mc_number")%>" />
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"></div>
            <div class="input"></div>
        </li>
<% } %>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formLocation" />:</div>
            <div class="input">

<% // multisites start
boolean isSiteSelected = false;
if (bMultisites) { %>
				<input type="hidden" name="isMultisitesLocationActiveFlag" value="">
				<select tabindex="4" name="location" style="background-color: <%=colo%>" onchange='this.style.backgroundColor=this.options[this.selectedIndex].style.backgroundColor'>
				<%
					StringBuilder sb = new StringBuilder();
					// add initial empty value option for cases where `multisites` is enabled but
					// there is no appointment site selected
					if (loc == null || loc.isEmpty()) {
						sb.append("<option value=\"\" selected>(none selected)</option>");
					}

					for (Site s:sites) {
						if (s.getName().equals(loc)) isSiteSelected = true; // added by vic
						sb.append("<option value=\"").append(Encode.forHtmlAttribute(s.getName())).append("\" style=\"background-color: ").append(s.getBgColor()).append("\" ").append(s.getName().equals(loc)?"selected":"").append(">").append(Encode.forHtml(s.getName())).append("</option>");
					}

					out.println(sb.toString());
				%>

				</select>
<% } else {
	isSiteSelected = true;
	// multisites end
	if (locationEnabled) {
%>           					
		<select name="location" >
               <%
               String location = bFirstDisp?(appt.getLocation()):request.getParameter("location");
               if (programs != null && !programs.isEmpty()) {
		       	for (Program program : programs) {
		       	    String description = StringUtils.isBlank(program.getLocation()) ? program.getName() : program.getLocation();
		   	%>
		        <option value="<%=program.getId()%>" <%=(program.getId().toString().equals(location) ? "selected='selected'" : "") %>><%=StringEscapeUtils.escapeHtml(description)%></option>
		    <%	}
               }
		  	%>
           </select>
	<% } else { %>
		<INPUT TYPE="TEXT" NAME="location" tabindex="4" VALUE="<%=Encode.forHtmlAttribute(bFirstDisp?appt.getLocation():request.getParameter("location"))%>" WIDTH="25">
	<% } %>           
<% } %>
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formResources" />:</div>
            <div class="input">
                <input type="TEXT"
					name="resources" tabindex="5"
					value="<%=Encode.forHtmlAttribute(bFirstDisp?appt.getResources():request.getParameter("resources"))%>"
                    width="25">
            </div>
        </li>
        <li class="weak row">
            <div class="label">Creator:</div>
            <div class="input">
            <% String lastCreatorNo = bFirstDisp?(appt.getCreator()):request.getParameter("user_id"); %>
                <INPUT TYPE="TEXT" NAME="user_id" VALUE="<%=Encode.forHtmlAttribute(lastCreatorNo)%>" readonly WIDTH="25">
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formLastTime" />:</div>
            <div class="input">
				<%
                 origDate =  bFirstDisp ? ConversionUtils.toTimestampString(appt.getCreateDateTime()) : request.getParameter("createDate");
                 String lastDateTime = bFirstDisp?ConversionUtils.toTimestampString(appt.getUpdateDateTime()):request.getParameter("updatedatetime");
                 if (lastDateTime == null){ lastDateTime = bFirstDisp?ConversionUtils.toTimestampString(appt.getCreateDateTime()):request.getParameter("createdatetime"); }

				GregorianCalendar now=new GregorianCalendar();
				String strDateTime=now.get(Calendar.YEAR)+"-"+(now.get(Calendar.MONTH)+1)+"-"+now.get(Calendar.DAY_OF_MONTH)+" "
					+	now.get(Calendar.HOUR_OF_DAY)+":"+now.get(Calendar.MINUTE)+":"+now.get(Calendar.SECOND);

                 String remarks = "";
                 if (bFirstDisp && appt.getRemarks()!=null) {
                     remarks = appt.getRemarks();
                 }

%>
                <INPUT TYPE="TEXT" NAME="lastcreatedatetime" readonly
                    VALUE="<%=bFirstDisp?lastDateTime:request.getParameter("lastcreatedatetime")%>"
                    WIDTH="25">
                <INPUT TYPE="hidden" NAME="createdatetime" VALUE="<%=strDateTime%>">
				<INPUT TYPE="hidden" NAME="provider_no" VALUE="<%=curProvider_no%>">
				<INPUT TYPE="hidden" NAME="dboperation" VALUE="">
                <INPUT TYPE="hidden" NAME="creator" VALUE="<%=Encode.forHtmlAttribute(creatorName)%>">
                <INPUT TYPE="hidden" NAME="remarks" VALUE="<%=Encode.forHtmlAttribute(remarks)%>">
                <INPUT TYPE="hidden" NAME="appointment_no" VALUE="<%=appointment_no%>">
            </div>
        </li>
        <%  lastCreatorNo = request.getParameter("user_id");
	if( bFirstDisp ) {
		if( appt.getLastUpdateUser() != null ) {
	    
	    	ProviderData provider = providerDao.findByProviderNo(appt.getLastUpdateUser());
	    	if( provider != null ) {
			lastCreatorNo = provider.getLastName() + ", " + provider.getFirstName();
	    	}
		}
		else {
		    lastCreatorNo = appt.getCreator();
		}
	}
%>  
      <li class="row weak">
      		<div class="label">&nbsp;</div>
            <div class="input">&nbsp;</div>
            <div class="space">&nbsp;</div>
            <div class="label">Last Editor:</div>
            <div class="input">
                <INPUT TYPE="TEXT" readonly
					VALUE="<%=Encode.forHtmlAttribute(lastCreatorNo)%>" WIDTH="25">
            </div>           
        </li>
          
        <li class="row weak">
            <div class="label">Create Date:</div>
            <div class="input">
                <INPUT TYPE="TEXT" NAME="createDate" readonly
					VALUE="<%=origDate%>" WIDTH="25">
            </div>
            <div class="space">&nbsp;</div>
            <div class="label"><bean:message key="Appointment.formCritical" />:</div>
            <div class="input">
            	<%
           			String urgencyChecked=new String();
            		if(bFirstDisp) {
            			if(appt.getUrgency() != null && appt.getUrgency().equals("critical")) {
            				urgencyChecked=" checked=\"checked\" ";
            			}
            		} else {
            			if(request.getParameter("urgency") != null) {
            				if(request.getParameter("urgency").equals("critical")) {
            					urgencyChecked=" checked=\"checked\" ";
            				}
            			}
            		}
            	%>
            	<input type="checkbox" name="urgency" value="critical" <%=urgencyChecked%>/>
            </div>
        </li>
        <%
            if (SystemPreferencesUtils.isKioskEnabled()) {
        %>
        <li class="row weak">
            <div class="label"><bean:message key="Appointment.formSeeReceptionist" />:</div>
            <div class="input">
                <%
                    String seeReceptionist = "";
                    if(bFirstDisp) {
                        if(appt.isSeeReceptionistAtCheckin()) {
                            seeReceptionist=" checked=\"checked\" ";
                        }
                    } else {
                        if(request.getParameter("seeReceptionist") != null) {
                            if(request.getParameter("seeReceptionist").equals("true")) {
                                seeReceptionist=" checked=\"checked\" ";
                            }
                        }
                    }
                %>
                <input type="checkbox" name="seeReceptionist" value="true" <%=seeReceptionist%>/>
            </div>
            <div class="space">&nbsp;</div>
            <div class="space">&nbsp;</div>
            <div class="space">&nbsp;</div>
        </li>
        <% } %>
        <li class="row weak">
			<div class="label"></div>
            <div class="input"></div>
            <div class="space">&nbsp;</div>
			<div class="label"></div>
            <div class="input"></div>
        </li>
    </ul>

<% if (deleted) { %>
    <div class="buttonBar" style="background: #ff0000;color:#ffffff">
        This appointment has been deleted.
    </div>
<% } else { %>
<table class="buttonBar deep">
	<tr>
            <% if (!bMultipleSameDayGroupAppt) { %>
        <td align="left"><input type="submit" class="rightButton blueButton top" id="updateButton"
			onclick="document.forms['EDITAPPT'].displaymode.value='Update Appt'; onButUpdate();"
			value="<bean:message key="appointment.editappointment.btnUpdateAppointment"/>">
             <% if (allowMultipleSameDayGroupAppts) { %>
		<input type="submit" id="groupButton"
			onclick="document.forms['EDITAPPT'].displaymode.value='Group Action'; onButUpdate();"
			value="<bean:message key="appointment.editappointment.btnGroupAction"/>">
             <% }%>
        <input TYPE="submit" id="printReceiptButton"
            onclick="document.forms['EDITAPPT'].displaymode.value='Update Appt';document.forms['EDITAPPT'].printReceipt.value='1';"
            VALUE="<bean:message key='appointment.editappointment.btnPrintReceipt'/>">
        <input type="hidden" name="printReceipt" value="">             
		<input type="submit" class="redButton button" id="deleteButton"
			onclick="document.forms['EDITAPPT'].displaymode.value='Delete Appt'; onButDelete();"
			value="<bean:message key="appointment.editappointment.btnDeleteAppointment"/>">
		<input type="button" name="buttoncancel" id="cancelButton"
			value="<bean:message key="appointment.editappointment.btnCancelAppointment"/>"
			onClick="onButCancel();"> <input type="button"
			name="buttoncancel" id="noShowButton"
			value="<bean:message key="appointment.editappointment.btnNoShow"/>"
			onClick="window.location='appointmentcontrol.jsp?buttoncancel=No Show&displaymode=Update Appt&appointment_no=<%=appointment_no%>'">
			<input type="button"
			name="buttonprintcard" id="printCardButton"
			value="Print Card"
			onClick="window.location='appointmentcontrol.jsp?displaymode=PrintCard&appointment_no=<%=appointment_no%>'">
			
		</td>
		<td align="right" nowrap><input type="button" name="labelprint" id="labelButton"
			value="<bean:message key="appointment.editappointment.btnLabelPrint"/>"
			onClick="window.open('../demographic/demographiclabelprintsetting.jsp?demographic_no='+document.EDITAPPT.demographic_no.value, 'labelprint','height=550,width=700,location=no,scrollbars=yes,menubars=no,toolbars=no' )">
		<!--input type="button" name="Button" value="<bean:message key="global.btnExit"/>" onClick="self.close()"-->
		 <% if (allowMultipleSameDayGroupAppts) { %>
                    <input type="button" id="repeatButton"
			value="<bean:message key="appointment.addappointment.btnRepeat"/>"
			onclick="onButRepeat()"></td>
                 <% }
                }%>
	</tr>
</table>
<% } %>

</div>
<div id="bottomInfo">
	<%
		String formTblProp = "";
		String[] formTblNames = new String[0];
		int numForms;
		int pageSize = 5;
	%>
	<table align="center">
		<tr>
			<td valign="top">
				<% if (demographic != null) { 
				    String dobString = "("+demographic.getYearOfBirth()+"-"+demographic.getMonthOfBirth()+"-"+demographic.getDateOfBirth()+")";
				    String hin = StringUtils.trimToEmpty(demographic.getHin());
                    String ver = StringUtils.trimToEmpty(demographic.getVer());
                    hin = (hin +" "+ ver).replace("null", "");
				%>
				<table style="font-size: 9pt;" bgcolor="#c0c0c0" align="center" valign="top" cellpadding="3px">
					<tr bgcolor="#ccccff">
						<th colspan="2">
							<bean:message key="appointment.addappointment.msgDemgraphics"/>
							<a title="Master File" onclick="popup(700,1000,'../demographic/demographiccontrol.jsp?demographic_no=<%=demoNo%>&amp;displaymode=edit&amp;dboperation=search_detail','master')" href="javascript: function myFunction() {return false; }"><bean:message key="appointment.addappointment.btnEdit"/></a>
							<bean:message key="appointment.addappointment.msgSex"/>: <%=demographic.getSex()%> &nbsp; <bean:message key="appointment.addappointment.msgDOB"/>: <%=dobString%>
						</th>
					</tr>
					<tr bgcolor="#ccccff">
						<th style="padding-right: 20px" align="left">
							<bean:message key="appointment.addappointment.msgHin"/>:<br/>
                            <% if(!props.getBooleanProperty("cardswipe","false")) { %>
							    <a href="#" onclick="popup(500, 500, '/CardSwipe/?hc=<%=hin%>&providerNo=<%=StringUtils.trimToEmpty(curProvider_no)%>', 'Card Swipe'); return false;" style="float:right; padding-right: 5px;">Validate HC</a>
                            <% } %>
						</th>
						<td><%=hin%> </td>
					</tr>
					<tr bgcolor="#ccccff">
						<th style="padding-right: 20px"align="left"><bean:message key="appointment.addappointment.msgAddress"/>:</th>
						<td><%=Encode.forHtml(StringUtils.trimToEmpty(demographic.getAddress()))%>, <%=Encode.forHtml(StringUtils.trimToEmpty(demographic.getCity()))%>, <%=StringUtils.trimToEmpty(demographic.getProvince())%>, <%=StringUtils.trimToEmpty(demographic.getPostal())%></td>
					</tr>
					<tr bgcolor="#ccccff">
						<th style="padding-right: 20px" align="left"><bean:message key="appointment.addappointment.msgPhone"/>:</th>
						<td><b><bean:message key="appointment.addappointment.msgH"/></b>:<%=StringUtils.trimToEmpty(demographic.getPhone())%> <b><bean:message key="appointment.addappointment.msgW"/></b>:<%=StringUtils.trimToEmpty(demographic.getPhone2())%> <b><bean:message key="appointment.addappointment.msgC"/></b>:<%=Encode.forHtmlContent(StringUtils.trimToEmpty(cellPhone))%></td>
					</tr>
					<tr bgcolor="#ccccff" align="left">
						<th style="padding-right: 20px"><bean:message key="appointment.addappointment.msgEmail"/>:</th>
						<td><%=StringUtils.trimToEmpty(demographic.getEmail())%></td>
					</tr>
					<tr bgcolor="#ccccff" align="left">
						<th style="padding-right: 20px"><bean:message key="appointment.addappointment.msgRefDoc"/>:</th>
						<td><%= demographic.getReferralPhysicianName()
                                + (!demographic.getReferralPhysicianOhip().isEmpty()
                                ? " (" + demographic.getReferralPhysicianOhip() + ")"
                                : "") %>
                        </td>
					</tr>
					<tr bgcolor="#ccccff" align="left">
						<th style="padding-right: 20px"><bean:message key="appointment.addappointment.msgFamDoc"/>:</th>
						<td><%= demographic.getFamilyPhysicianName()
                                + (!demographic.getFamilyPhysicianOhip().isEmpty()
                                ? " (" + demographic.getFamilyPhysicianOhip() + ")"
                                : "") %>
                        </td>
					</tr>
				</table>
				<%}%>
			</td>
			<td valign="top">
				<%
					formTblProp = props.getProperty("appt_formTbl","");
					formTblNames = formTblProp.split(";");

					numForms = 0;
					for (String formTblName : formTblNames){
						if ((formTblName != null) && !formTblName.equals("")) {
							//form table name defined
							for(EncounterForm ef:encounterFormDao.findByFormTable(formTblName)) {
								String formName = ef.getFormName();
								pageContext.setAttribute("formName", formName);
								boolean formComplete = false;
								EctFormData.PatientForm[] ptForms = EctFormData.getPatientFormsFromLocalAndRemote(loggedInInfo, demoNo, formTblName);

								if (ptForms.length > 0) {
									formComplete = true;
								}
								numForms++;
								if (numForms == 1) {

				%>
				<table style="font-size: 9pt;" bgcolor="#c0c0c0" align="center" valign="top" cellpadding="3px">
					<tr bgcolor="#ccccff">
						<th colspan="2">
							<bean:message key="appointment.addappointment.msgFormsSaved"/>
						</th>
					</tr>
					<%              }%>

					<tr bgcolor="#c0c0c0" align="left">
						<th style="padding-right: 20px"><c:out value="${formName}:"/></th>
						<%              if (formComplete){  %>
						<td><bean:message key="appointment.addappointment.msgFormCompleted"/></td>
						<%              } else {            %>
						<td><bean:message key="appointment.addappointment.msgFormNotCompleted"/></td>
						<%              } %>
					</tr>
					<%
								}
							}
						}

						if (numForms > 0) {
					%>
				</table>
				<%  }   %>
			</td>
			<td valign="top">
				<table style="font-size: 8pt;" bgcolor="#c0c0c0" align="center" valign="top">
					<tr bgcolor="#ccccff">
						<th colspan="5"><bean:message key="appointment.addappointment.msgOverview" /></th>
					</tr>
					<tr bgcolor="#ccccff">
						<th style="padding-right: 25px"><bean:message key="Appointment.formDate" /></th>
						<th style="padding-right: 25px"><bean:message key="Appointment.formStartTime" /></th>
						<th style="padding-right: 25px"><bean:message key="appointment.addappointment.msgProvider" /></th>
						<th style="padding-right: 25px"><bean:message key="appointment.addappointment.msgStatus" /></th>
						<th style="padding-right: 25px"><bean:message key="appointment.addappointment.msgNotes" /></th>
					</tr>
					<%

						int iRow=0;
						if(demographic != null) {

							Calendar cal2 = Calendar.getInstance();
							java.util.Date start = cal2.getTime();
							cal2.add(Calendar.YEAR, 1);
							java.util.Date end = cal2.getTime();

							String status;
							String notes;
							String shortenedNotes;
							String statusDescription;

							for(Object[] result : appointmentDao.search_appt_future(Integer.parseInt(demoNo), start, end)) {
								Appointment a = (Appointment)result[0];
								Provider currentProvider = (Provider)result[1];
								iRow ++;
								if (iRow > pageSize) break;

								status = a.getStatus();
								notes = a.getNotes();

								if (notes.length() > 15) {
									shortenedNotes = notes.substring(0, 13).trim() + "<b>...</b>";
								}
								else {
									shortenedNotes = notes;
								}
								statusDescription = "";
								for (AppointmentStatus appointmentStatus : allStatus) {
									if (appointmentStatus.getStatus().equals(status)) {
										statusDescription = appointmentStatus.getDescription();
									}
								}
					%>
					<tr bgcolor="#eeeeff" style="<%=curUser_no.equals(currentProvider.getProviderNo())?"font-weight:bold":""%>">
						<td style="background-color: #CCFFCC; padding-right: 25px"><%=ConversionUtils.toDateString(a.getAppointmentDate())%></td>
						<td style="background-color: #CCFFCC; padding-right: 25px"><%=ConversionUtils.toTimeString(a.getStartTime())%></td>
						<td style="background-color: #CCFFCC; padding-right: 25px"><%=Encode.forHtmlContent(((Provider) result[1]).getFormattedName())%></td>
						<td style="background-color: #CCFFCC;"><%=Encode.forHtml(statusDescription) %></td>
						<td style="background-color: #CCFFCC;" title="<%= Encode.forHtmlAttribute(notes) %>"><%= Encode.forHtmlContent(shortenedNotes) %></td>
					</tr>
					<%
						}

						iRow=0;
						cal2 = Calendar.getInstance();
						cal2.add(Calendar.YEAR, -1);

						for(Object[] result : appointmentDao.search_appt_past(Integer.parseInt(demoNo), start, cal2.getTime())) {
							Appointment a = (Appointment)result[0];
							Provider currentProvider = (Provider)result[1];
							iRow ++;
							if (iRow > pageSize) break;

							status = a.getStatus();
							notes = a.getNotes();

							if (notes.length() > 15) {
								shortenedNotes = notes.substring(0, 13).trim() + "<b>...</b>";
							}
							else {
								shortenedNotes = notes;
							}
							statusDescription = "";
							for (AppointmentStatus appointmentStatus : allStatus) {
								if (appointmentStatus.getStatus().equals(status)) {
									statusDescription = appointmentStatus.getDescription();
								}
							}
					%>
					<tr bgcolor="#eeeeff" style="<%=curUser_no.equals(currentProvider.getProviderNo())?"font-weight:bold":""%>">
						<td style="padding-right: 25px"><%=ConversionUtils.toDateString(a.getAppointmentDate())%></td>
						<td style="padding-right: 25px"><%=ConversionUtils.toTimeString(a.getStartTime())%></td>
						<td style="padding-right: 25px"><%=Encode.forHtmlContent(currentProvider.getFormattedName())%></td>
						<td style="padding-right: 25px"><%= Encode.forHtmlContent(statusDescription) %></td>
						<td style="padding-right: 25px" title="<%= Encode.forHtmlAttribute(notes) %>"><%= Encode.forHtmlContent(shortenedNotes) %></td>
					</tr>
					<%
							}
						}
					%>
				</table>
			</td>
		</tr>
	</table>
	
<table width="95%" align="center">
	<tr>
		<td>
		<bean:message key="Appointment.msgRosterStatus" />: <%=StringUtils.trimToEmpty(rosterstatus)%>
		</td>
	</tr>
</table>
<hr />

<% if (isSiteSelected) { %>
<table width="95%" align="center" id="belowTbl">
	<tr>
		<td>
            <% if (!deleted) { %>
            <input type="submit"
			onclick="document.forms['EDITAPPT'].displaymode.value='Cut';"
			value="Cut" /> |
            <% } %>
            <input type="submit"
			onclick="document.forms['EDITAPPT'].displaymode.value='Copy';"
			value="Copy" /> |
            <% if (!deleted) { %>
            <input type="submit"
                   onclick="document.forms['EDITAPPT'].displaymode.value='Move';"
                   value="Move"/>
            <% } %>
                     <%
                     if(bFirstDisp && apptObj!=null && !deleted) {

                            long numSameDayGroupApptsPaste = 0;

                            if (!allowMultipleSameDayGroupAppts) {
                                
                                List<Appointment> aa = appointmentDao.search_group_day_appt(myGroupNo,Integer.parseInt(demoNo),appt.getAppointmentDate());
                                
                                numSameDayGroupApptsPaste = aa.size() > 0 ? new Long(aa.size()): 0;
                            }
                  %><a href=#
			onclick="pasteAppt(<%=(numSameDayGroupApptsPaste > 0)%>);">Paste</a>
		<% } %>
		</td>
	</tr>
	<%if(cheader1s != null && cheader1s.size()>0){%>
		<tr>
		<th width="40%"><font color="red">Outstanding 3rd Invoices</font></th>
		<th width="20%"><font color="red">Invoice Date</font></th>
		<th><font color="red">Amount</font></th>
		<th><font color="red">Balance</font></th>
		</tr>
		<%
		java.text.SimpleDateFormat fm = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for(int i=0;i<cheader1s.size();i++) {
			if(cheader1s.get(i).getPayProgram().matches(BillingDataHlp.BILLINGMATCHSTRING_3RDPARTY)){ 
				BigDecimal payment = billingOnExtDao.getAccountVal(cheader1s.get(i).getId(), billingOnExtDao.KEY_PAYMENT);
				BigDecimal discount = billingOnExtDao.getAccountVal(cheader1s.get(i).getId(), billingOnExtDao.KEY_DISCOUNT);
				BigDecimal credit =  billingOnExtDao.getAccountVal(cheader1s.get(i).getId(), billingOnExtDao.KEY_CREDIT);
				BigDecimal total = cheader1s.get(i).getTotal();
				BigDecimal balance = total.subtract(payment).subtract(discount).add(credit);
				
            	if(balance.compareTo(BigDecimal.ZERO) != 0) { %>
					<tr>
						<td align="center"><a href="javascript:void(0)" onclick="popupPage(600,800, '<%=request.getContextPath() %>/billing/CA/ON/billingONCorrection.jsp?billing_no=<%=cheader1s.get(i).getId()%>')"><font color="red">Inv #<%=cheader1s.get(i).getId() %></font></a></td>
						<td align="center"><font color="red"><%=fm.format(cheader1s.get(i).getTimestamp()) %></font></td>
						<td align="center"><font color="red">$<%=cheader1s.get(i).getTotal() %></font></td>
						<td align="center"><font color="red">$<%=balance %></font></td>
					</tr>
				<%}
			}
		}
	 } %>
</table>
<% } %>
</div>
</FORM>
</div> <!-- end of edit appointment screen -->
<%
    formTblProp = props.getProperty("appt_formTbl");
    formTblNames = formTblProp.split(";");
               
    numForms = 0;
    for (String formTblName : formTblNames){
        if ((formTblName != null) && !formTblName.equals("")) {
            //form table name defined
            for(EncounterForm ef:encounterFormDao.findByFormTable(formTblName)) {
            	String formName = ef.getFormName();
                pageContext.setAttribute("formName", formName);
                boolean formComplete = false;
                EctFormData.PatientForm[] ptForms = EctFormData.getPatientFormsFromLocalAndRemote(loggedInInfo,demoNo, formTblName);

                if (ptForms.length > 0) {
                    formComplete = true;
                }
                numForms++;
                if (numForms == 1) {
%>
         <table style="font-size: 9pt;" bgcolor="#c0c0c0" align="center" valign="top" cellpadding="3px">
            <tr bgcolor="#ccccff">
                <th colspan="2">
                    <bean:message key="appointment.addappointment.msgFormsSaved"/>
                </th>
            </tr>              
<%              } %>
             
            <tr bgcolor="#c0c0c0" align="left">
                <th style="padding-right: 20px"><c:out value="${formName}:"/></th>
<%                 if (formComplete){  %>
                <td><bean:message key="appointment.addappointment.msgFormCompleted"/></td>
<%                 } else {            %>
                <td><bean:message key="appointment.addappointment.msgFormNotCompleted"/></td>
<%                 } %>               
            </tr>
<%                         
            }
        }
    }
    if (numForms > 0) {        %>
         </table>
<%  }   %>
         
         
<!-- View Appointment: Screen that summarizes appointment data.
Currently this is only used in the mobile version -->
<div id="viewAppointment" style="display:<%=(bFirstDisp && isMobileOptimized) ? "block":"none"%>;">
    <%
        // Format date to be more readable
        java.text.SimpleDateFormat inform = new java.text.SimpleDateFormat ("yyyy-MM-dd");
        String strDate = bFirstDisp ? ConversionUtils.toDateString(appt.getAppointmentDate()) : request.getParameter("appointment_date");
        java.util.Date d = inform.parse(strDate);
        String formatDate = "";
        try { // attempt to change string format
        java.util.ResourceBundle prop = ResourceBundle.getBundle("oscarResources", request.getLocale());
        formatDate = oscar.util.UtilDateUtilities.DateToString(d, prop.getString("date.EEEyyyyMMdd"));
        } catch (Exception e) {
            org.oscarehr.util.MiscUtils.getLogger().error("Error", e);
            formatDate = oscar.util.UtilDateUtilities.DateToString(inform.parse(strDate), "EEE, yyyy-MM-dd");
        }
    %>
    <div class="header">
        <div class="title" id="appointmentTitle">
            <bean:message key="appointment.editappointment.btnView" />
        </div>
        <a href=# onclick="window.close();" id="backButton" class="leftButton top"><%= strDate%></a>
        <a href="javascript:toggleView();" id="editButton" class="rightButton top">Edit</a>
    </div>
    <div id="info" class="panel">
        <ul>
            <li class="mainInfo"><a href="#" onclick="demographicdetail(550,700)">
                <%
                    String apptName = (bFirstDisp ? appt.getName() : request.getParameter("name")).toString();
                    //If a comma exists, need to split name into first and last to prevent overflow
                    int comma = apptName.indexOf(",");
                    if (comma != -1)
                        apptName = apptName.substring(0, comma) + ", " + apptName.substring(comma+1);
                %>
                <%=Encode.forHtmlContent(apptName)%>
            </a></li>
            <li><div class="label"><bean:message key="Appointment.formDate" />: </div>
                <div class="info"><%=formatDate%></div>
            </li>
            <% // Determine appointment status from code so we can access
   // the description, colour, image, etc.
      AppointmentStatus apptStatus = (AppointmentStatus)allStatus.get(0);
      for (int i = 0; i < allStatus.size(); i++) {
            AppointmentStatus st = (AppointmentStatus) allStatus.get(i);
            if (st.getStatus().equals(statusCode)) {
                apptStatus = st;
                break;
            }
      }
%>
            <li><div class="label"><bean:message key="Appointment.formStatus" />: </div>
                <div class="info">
                <font style="background-color:<%=apptStatus.getColor()%>; font-weight:bold;">
                    <img src="../images/<%=apptStatus.getIcon()%>" />
                    <%=Encode.forHtml(apptStatus.getDescription())%>
                </font>
                </div>
            </li>
            <li><div class="label"><bean:message key="appointment.editappointment.msgTime" />: </div>
                <div class="info">From <%=bFirstDisp ? ConversionUtils.toTimeStringNoSeconds(appt.getStartTime()) : MyDateFormat.getTimeXX_XX_XX(request.getParameter("start_time"))%>
                to <%=bFirstDisp ? ConversionUtils.toTimeStringNoSeconds(appt.getEndTime()) : request.getParameter("end_time")%></div>
            </li>
            <li><div class="label"><bean:message key="Appointment.formType" />: </div>
                <div class="info"><%=Encode.forHtml(bFirstDisp ? appt.getType() : request.getParameter("type"))%></div>
            </li>
            <li><div class="label"><bean:message key="Appointment.formReason" />: </div>
                <div class="info"><%=Encode.forHtml(bFirstDisp ? appt.getReason() : request.getParameter("reason"))%></div>
            </li>
            <li><div class="label"><bean:message key="Appointment.formLocation" />: </div>
                <div class="info"><%=Encode.forHtml(bFirstDisp ? appt.getLocation() : request.getParameter("location"))%></div>
            </li>
            <li><div class="label"><bean:message key="Appointment.formResources" />: </div>
                <div class="info"><%=Encode.forHtml(bFirstDisp ? appt.getResources() : request.getParameter("resources"))%></div>
            </li>
            <li>&nbsp;</li>
            <li class="notes">
                <div class="label"><bean:message key="Appointment.formNotes" />: </div>
                <div class="info"><%=Encode.forHtml(bFirstDisp ? appt.getNotes() : request.getParameter("notes"))%></div>
            </li>
        </ul>
    </div>
</div> <!-- end of screen to view appointment -->
</div>
</div>
</body>
<script type="text/javascript">
var loc = document.forms['EDITAPPT'].location;
if(loc.nodeName.toUpperCase() == 'SELECT') loc.style.backgroundColor=loc.options[loc.selectedIndex].style.backgroundColor;

jQuery(document).ready(function(){
	var belowTbl = jQuery("#belowTbl");
	if (belowTbl != null && belowTbl.length > 0 && belowTbl.find("tr").length == 2) {
		jQuery(belowTbl.find("tr")[1]).remove();
	} 
	
	<% if (deleted) { %>
    $("input[type='button']").attr("disabled", "disabled");
    $("input[type='submit'][value!='Copy']").attr("disabled", "disabled");
    $("select").attr("disabled", "disabled");
    
    $("input[type='text']").attr("readonly", "readonly");
    $("textarea").attr("readonly", "readonly");
    
    $("#appointment_date_cal").attr("hidden", "hidden");
    <% } %>
});

</script>

</html:html>
