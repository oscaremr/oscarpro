<%--

    Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

--%>
<%@ page import="java.util.*, java.sql.*, oscar.*, java.text.*, java.lang.*,java.net.*, oscar.appt.*, org.oscarehr.common.dao.AppointmentTypeDao, org.oscarehr.common.model.AppointmentType, org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.dao.LookupListItemDao" %>
<%@ page import="org.owasp.encoder.Encode" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%
	AppointmentTypeDao appDao = (AppointmentTypeDao) SpringUtils.getBean("appointmentTypeDao");
	List<AppointmentType> types = appDao.listAllTemplates();
	LookupListItemDao lookupListItemDao = SpringUtils.getBean(LookupListItemDao.class);
%>
<html>
<head>
<title>Appointment Type</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<script type="text/javascript">
var typeIds = [];
var durations = [];
var reasonCodes = [];
var reasons = [];
var locations = [];
var notes = [];
var resources = [];
var names = [];
var autoBill = [];
<%   for(int j = 0;j < types.size(); j++) { %>
		typeIds.push('<%=types.get(j).getId()%>');
		durations.push('<%= types.get(j).getDuration() %>');
		reasonCodes.push('<%= types.get(j).getReasonCode() %>_<%=lookupListItemDao.find(types.get(j).getReasonCode())!=null?lookupListItemDao.find(types.get(j).getReasonCode()).getLabel():""%>');
		reasons.push('<%= Encode.forJavaScriptBlock(types.get(j).getReason().replaceAll("\\s", " ")) %>');
		locations.push('<%= Encode.forJavaScriptBlock(types.get(j).getLocation()) %>');
		notes.push('<%= Encode.forJavaScriptBlock(types.get(j).getNotes().replaceAll("\\s", " ")) %>');
		resources.push('<%= Encode.forJavaScriptBlock(types.get(j).getResources()) %>');
		names.push('<%= Encode.forJavaScriptBlock(types.get(j).getName()) %>');
		autoBill.push('<%= types.get(j).getAutoBill() %>');
<%   } %>
	var typeId = '';
	var typeSel = '';
	var reasonCodeSel = '';
	var reasonCodeSelLabel = '';
	var reasonSel = '';
	var locSel = '';
	var durSel = 15;
	var notesSel = '';
	var resSel = '';
	var autoBillSel = '';

function getFields(idx) {
	if(idx>0) {
		typeId = typeIds[idx-1];
		typeSel = document.getElementById('durId').innerHTML = names[idx-1];
		durSel = document.getElementById('durId').innerHTML = durations[idx-1];
        	reasonCodeSelLabel = document.getElementById('reasonCodeId').innerHTML = reasonCodes[idx-1].split('_')[1];
        	reasonCodeSel = reasonCodes[idx-1].split('_')[0];
		reasonSel = document.getElementById('reasonId').innerHTML = reasons[idx-1];
		locSel = document.getElementById('locId').innerHTML = locations[idx-1];
		notesSel = document.getElementById('notesId').innerHTML = notes[idx-1];
		resSel = document.getElementById('resId').innerHTML = resources[idx-1];
		autoBillSel = autoBill[idx-1];

		showAutoBill(autoBillSel, typeId);
	} else {
		typeId = 0;
		document.getElementById('durId').innerHTML = '15';
		document.getElementById('reasonCodeId').innerHTML = '';
		document.getElementById('reasonId').innerHTML = '';
		document.getElementById('locId').innerHTML = '';
		document.getElementById('notesId').innerHTML = '';
		document.getElementById('resId').innerHTML = '';
	}
}
</script>


<style>
#auto-bill-table{display:none;margin:20px;font-size:12px;border:thin solid #333;padding-bottom:12px;}
#auto-bill-rules{font-size:12px;width:100%;background-color:#ffffff}
#auto-bill-rules th, #auto-bill-rules td{text-align:left;border:thin solid #C0C0C0}
#auto-bill-rules tbody tr:hover{background-color:#ffff66;}
</style>
</head>
<body bgcolor="#EEEEFF" bgproperties="fixed" topmargin="0" leftmargin="0" rightmargin="0">
<table width="100%">
	<tr>
		<td width="100">Type</td>
		<td width="200">
			<select id="typesId" width="25" maxsize="50" onchange="getFields(this.selectedIndex)">
				<option value="">Select type</option>
				<% for(AppointmentType type : types) { %>
				<option value="<%=type.getId()%>" <%=(type.getName().equals(request.getParameter("type"))?" selected":"") %>>
					<%=Encode.forHtmlContent(type.getName())%>
				</option>
				<% } %>
			</select>
		</td>
		<td><input type="button" name="Select" value="Select" onclick="window.opener.setAppointmentType(typeId); window.close()">
	</tr>
	<tr>
		<td>Duration</td>
		<td colspan="2"><div id="durId"></div></td>
	</tr>
	<tr>
		<td>Reason Code</td>
		<td colspan="2"><span id="reasonCodeId"/></td>
	</tr>
	<tr>
		<td>Reason</td>
		<td colspan="2"><span id="reasonId"/></td>
	</tr>
	<tr>
		<td>Location</td>
		<td colspan="2"><span id="locId"/></td>
	</tr>
	<tr>
		<td>Notes</td>
		<td colspan="2"><span id="notesId"/></td>
	</tr>
	<tr>
		<td>Resources</td>
		<td colspan="2"><span id="resId"/></td>
	</tr>
</table>

<table id="auto-bill-table" cellpadding="4">
<tr>
	<th style="text-align:left"><img src="../images/check.png"> Auto Bill</th>
</tr>

<tr>
	<td>
		<table id="auto-bill-rules" cellspacing="0" cellpadding="4" width="100%">
		<thead><tr><th>Name</th> <th>Service Code</th> <th>DX</th> <th>SLI</th> <th>VL</th> <th>Billing Physician</th> <th>Referral Physician</th></tr></thead>
		<tbody><tr id="no-rules"><td colspan="7">No rules mapped</td></tr></tbody>
		</table>
	</td>
<tr>
</table>

<script src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>

<script type="text/javascript">
getFields(document.getElementById('typesId').selectedIndex);

function showAutoBill(auto_bill, rule_id){
	auto_bill_table = document.getElementById('auto-bill-table');
	if(auto_bill==1){
		getBillingAutoRule(rule_id);
		auto_bill_table.style.display='inline-block';
	}else{
		auto_bill_table.style.display='none';
	}
}

function getBillingAutoRule(rule_id){
	$.getJSON("../ws/rs/billing/billingAutoRulesMap/"+rule_id, {}, function(data) {
		var ruleJson = data.content;
		body = $('#auto-bill-rules tbody');

		console.log("data length: " + data.content.length);
		//remove no-class if exists
		if($('#no-rules').length){
			$('#no-rules').remove();
		}

		var sequence = 1;
		html = '';

		if(data.content.length>0){
			$.each(ruleJson, function(key, val) {
			    html += "<tr data-id=\""+key+"\"><td>"+val.ruleName+"</td></td> <td>"+val.serviceCode+"</td></td> <td>"+val.dx+"</td></td> <td>"+val.sliCode+"</td></td> <td>"+val.visitLocation+"</td></td> <td>"+val.billingDoctor+"</td></td> <td>"+val.referralDoctor+"</td></tr>";
			});
		}else{
			html= '<tr id="no-rules"><td colspan="7">No rules mapped</td></tr>';
		}

		body.html(html);
	});
}
</script>
</body>
</html>
