<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_appointment" rights="u" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../securityError.jsp?type=_appointment");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
%>

<%@ page import="java.sql.*, java.util.*, oscar.*, oscar.util.*, org.oscarehr.common.OtherIdManager"%>
<%@ page import="org.oscarehr.event.EventService"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@page import="org.oscarehr.common.model.Appointment" %>
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="oscar.util.ConversionUtils" %>
<%@ page import="oscar.log.LogAction" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="oscar.log.LogConst" %>
<%@ page import="org.oscarehr.common.model.AppointmentReminder" %>
<%@ page import="org.oscarehr.common.model.Appointment" %>
<%@ page import="org.oscarehr.common.dao.AppointmentReminderDao" %>
<%@ page import="org.oscarehr.common.dao.AppointmentReminderStatusDao" %>
<%@ page import="org.oscarehr.common.model.AppointmentReminderStatus" %>
<%@ page import="org.oscarehr.common.model.OnlineBookingInfo" %>
<%@ page import="org.oscarehr.common.dao.AppointmentArchiveDao" %>
<%@ page import="org.oscarehr.common.dao.OscarAppointmentDao" %>
<%@ page import="org.oscarehr.common.dao.DemographicExtDao" %>
<%@ page import="org.oscarehr.common.dao.DemographicDao" %>
<%@ page import="org.oscarehr.common.model.Demographic" %>
<%@ page import="org.oscarehr.common.model.DemographicExt" %>
<%@ page import="org.oscarehr.common.dao.OnlineBookingInfoDao" %>

<%@ page import="java.util.Date,java.text.SimpleDateFormat" %>
<%@ page import="org.oscarehr.common.dao.BillingONCHeader1Dao"%>
<%@ page import="org.oscarehr.common.dao.AppointmentTypeDao" %>
<%@ page import="org.oscarehr.common.model.AppointmentType" %>
<%@ page import="org.oscarehr.common.dao.BillingDao" %>
<%@ page import="org.oscarehr.common.model.BillingAutoRules" %>
<%@ page import="org.oscarehr.common.model.BillingAutoRulesExt" %>
<%@ page import="org.oscarehr.common.dao.BillingAutoRulesExtDao" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%
	AppointmentArchiveDao appointmentArchiveDao = (AppointmentArchiveDao)SpringUtils.getBean("appointmentArchiveDao");
	OscarAppointmentDao appointmentDao = (OscarAppointmentDao)SpringUtils.getBean("oscarAppointmentDao");
	DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
	DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
    	String changedStatus = null;
    	
    String apptStatusHere = OscarProperties.getInstance().getProperty("appt_status_here");    	
%>
<html:html locale="true">
<head>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<script src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
</head>

<body>
<center>
<table border="0" cellspacing="0" cellpadding="0" width="90%">
	<tr bgcolor="#486ebd">
		<th align="CENTER"><font face="Helvetica" color="#FFFFFF">
		<bean:message key="appointment.appointmentupdatearecord.msgMainLabel" /></font></th>
	</tr>
</table>
<%
  String updateuser = (String) session.getAttribute("user");

  int rowsAffected = 0;
  boolean appointmentRemindersEnabled = SystemPreferencesUtils.isAppointmentRemindersEnabled();
  int appt_no = Integer.parseInt(request.getParameter("appointment_no"));
  String appt_type = request.getParameter("type");
  Appointment appt = appointmentDao.find(appt_no);
  appointmentArchiveDao.archiveAppointment(appt);

  Appointment oldAppointment = new Appointment(appt);
  AppointmentReminderDao appointmentReminderDao = SpringUtils.getBean(AppointmentReminderDao.class);
  AppointmentReminderStatusDao appointmentReminderStatusDao = SpringUtils.getBean(AppointmentReminderStatusDao.class);
  AppointmentReminder appointmentReminder = appointmentReminderDao.getByAppointmentNo(appt.getId());
  OnlineBookingInfoDao onlineBookingInfoDao = SpringUtils.getBean(OnlineBookingInfoDao.class);
  OnlineBookingInfo confirmedStatus = onlineBookingInfoDao.getOnlineBookingPreference("reminder_confirmed_status", "CLINIC");
    OnlineBookingInfo cancelledStatus = onlineBookingInfoDao.getOnlineBookingPreference("reminder_cancelled_status", "CLINIC");

    String changedAppointmentStatus = "";

  //Did the appt status change ?
  if(!appt.getStatus().equals(request.getParameter("status")) && request.getParameter("status") != null){
	  changedStatus = request.getParameter("status");

	  if(changedStatus.length() >= 2) {
	    changedAppointmentStatus = changedStatus.substring(0, 1);
	  } else {
	    changedAppointmentStatus = changedStatus;
	  }

	  if (appointmentRemindersEnabled && confirmedStatus != null && confirmedStatus.getValue() != null && appointmentReminder != null) {
	    AppointmentReminderStatus appointmentReminderStatus = appointmentReminderStatusDao.getByAppointmentReminderNo(appointmentReminder.getId());
	    if( changedAppointmentStatus.equals(confirmedStatus.getValue())) {
	      appointmentReminder.setConfirmed(true);
	      if(appointmentReminderStatus != null) {
	        appointmentReminderStatus.setAllDelivered(true);
	      }
	    } else {
	      if(appointmentReminderStatus != null && appointmentReminderStatus.getDeliveryTime() == null) {
	        appointmentReminder.setConfirmed(false);
	        String oldAppointmentStatus = "";
	        oldAppointmentStatus = appt.getStatus();
	        if (oldAppointmentStatus.length() >= 2) {
	          oldAppointmentStatus = oldAppointmentStatus.substring(0, 1);
	        }
	        if(cancelledStatus != null && cancelledStatus.getValue() != null && cancelledStatus.getValue().equals(oldAppointmentStatus)) {
	          appointmentReminder.setCancelled(false);
	        }
	        appointmentReminderStatus.setAllDelivered(false);
	      }
	    }

	    appointmentReminderDao.merge(appointmentReminder);
	    if (appointmentReminderStatus != null) {
	      appointmentReminderStatusDao.merge(appointmentReminderStatus);
	    }
	  }
  }

	if (request.getParameter("buttoncancel")!=null && (request.getParameter("buttoncancel").equals("Cancel Appt") || request.getParameter("buttoncancel").equals("No Show"))) {
		changedStatus = request.getParameter("buttoncancel").equals("Cancel Appt") ? "C" : "N";
		changedAppointmentStatus = changedStatus;
	}

	if (appointmentRemindersEnabled && "C".equals(changedAppointmentStatus) || (cancelledStatus != null && cancelledStatus.getValue() != null && cancelledStatus.getValue().equals(changedAppointmentStatus))) {
		if (appointmentReminder != null) {
			appointmentReminder.setCancelled(true);
			appointmentReminderDao.merge(appointmentReminder);
			AppointmentReminderStatus appointmentReminderStatus = appointmentReminderStatusDao.getByAppointmentReminderNo(appointmentReminder.getId());
			if (appointmentReminderStatus != null) {
				appointmentReminderStatus.setAllDelivered(true);
				appointmentReminderStatusDao.merge(appointmentReminderStatus);
			}
		}
	}

   if (request.getParameter("buttoncancel")!=null && (request.getParameter("buttoncancel").equals("Cancel Appt") || request.getParameter("buttoncancel").equals("No Show"))) {
	  changedStatus = request.getParameter("buttoncancel").equals("Cancel Appt")?"C":"N";
	  if(appt != null) {
		  appt.setStatus(request.getParameter("buttoncancel").equals("Cancel Appt") ? "C" : "N");
		  appt.setLastUpdateUser(updateuser);
		  appointmentDao.merge(appt);
		  rowsAffected = 1;
	  }
  } else {

	  if(appt != null) {
		  	if (request.getParameter("demographic_no")!=null && !(request.getParameter("demographic_no").equals(""))) {
		  		appt.setDemographicNo(Integer.parseInt(request.getParameter("demographic_no")));

		  		Demographic demographic = demographicDao.getDemographic(request.getParameter("demographic_no"));
				DemographicExt demographicExt = demographicExtDao.getDemographicExt(Integer.parseInt(request.getParameter("demographic_no")), "demo_cell");

				appointmentReminder = appointmentReminderDao.getByAppointmentNo(appt.getId());
				if (appointmentReminder != null && demographic != null) {
					String reminderCell = demographicExt==null?"":demographicExt.getValue().isEmpty()?"":demographicExt.getValue().substring(0, 1).equals("1")?"+" + demographicExt.getValue().replaceAll("[^0-9]", ""):"+1" + demographicExt.getValue().replaceAll("[^0-9]", "");
					String reminderPhone = demographic.getPhone()==null?"":demographic.getPhone().isEmpty()?"":demographic.getPhone().substring(0, 1).equals("1") ? "+" + demographic.getPhone().replaceAll("[^0-9]", "") : "+1" + demographic.getPhone().replaceAll("[^0-9]", "");

				    appointmentReminder.setReminderCell(reminderCell);
				    appointmentReminder.setReminderPhone(reminderPhone);
				    appointmentReminder.setReminderEmail(demographic.getEmail()==null?"":demographic.getEmail());
				    appointmentReminderDao.merge(appointmentReminder);
				}
		 	} else {
			 	appt.setDemographicNo(0);
		 	}
			appt.setAppointmentDate(ConversionUtils.fromDateString(request.getParameter("appointment_date")));
			appt.setStartTime(new java.sql.Time(ConversionUtils.fromTimeString(MyDateFormat.getTimeXX_XX_XX(request.getParameter("start_time"))).getTime()));
			appt.setEndTime(new java.sql.Time(ConversionUtils.fromTimeString(MyDateFormat.getTimeXX_XX_XX(request.getParameter("end_time"))).getTime()));
			appt.setName(request.getParameter("keyword"));
			appt.setNotes(request.getParameter("notes"));
			appt.setReason(request.getParameter("reason"));
			appt.setLocation(request.getParameter("location"));
			appt.setResources(request.getParameter("resources"));
			appt.setType(appt_type);
			appt.setStyle(request.getParameter("style"));
			appt.setBilling(request.getParameter("billing"));
			appt.setStatus(request.getParameter("status"));
			appt.setLastUpdateUser(updateuser);
			appt.setRemarks(request.getParameter("remarks"));
			appt.setUpdateDateTime(new java.util.Date());
			appt.setUrgency((request.getParameter("urgency")!=null)?request.getParameter("urgency"):"");
			appt.setReasonCode(Integer.valueOf(request.getParameter("reasonCode")));
			appt.setSeeReceptionistAtCheckin(Boolean.parseBoolean(request.getParameter("seeReceptionist")));

			String arrivalTime = request.getParameter("arrivalTime");
			String oldStatus = request.getParameter("oldStatus");
			if(!appt.getStatus().equals(oldStatus) && apptStatusHere != null){
				if (appt.getStatus().equals(apptStatusHere) && StringUtils.isNullOrEmpty(arrivalTime)) { 
					appt.setArrivalTime(new java.util.Date());
				}
			}
			
			appointmentDao.merge(appt);
			rowsAffected=1;
	  }

  }

  if (rowsAffected == 1) {
	  List<ChangedField> changedFields = new ArrayList<ChangedField>(ChangedField.getChangedFieldsAndValues(oldAppointment, appt));
	  LogAction.addLog(LoggedInInfo.getLoggedInInfoFromSession(request), LogConst.UPDATE, LogConst.CON_APPT,
			  "appointment_no=" + appt.getId(), String.valueOf(appt.getDemographicNo()), changedFields);
%>
<p>
<h1><bean:message key="appointment.appointmentupdatearecord.msgUpdateSuccess" /></h1>

<%
boolean autoGeneratedBilling = SystemPreferencesUtils
		.isReadBooleanPreferenceWithDefault("auto_generated_billing_enabled", false);

if (autoGeneratedBilling) {
 if (changedStatus!=null && changedStatus.startsWith("H")) {
	if (request.getParameter("demographic_no")!=null && !(request.getParameter("demographic_no").equals(""))) {

		// CHECK IF APPT TYPE IS SET TO USE AUTOBILL
		AppointmentTypeDao apptTypeDao = (AppointmentTypeDao) SpringUtils.getBean("appointmentTypeDao");
		int appointmentTypeId = apptTypeDao.getApptTypeIdIfAutoBillSet(appt_type);

		// IF appt_type_id returned get all Mapped Rules
		if(appointmentTypeId>0){
			BillingDao billingDao = (BillingDao)SpringUtils.getBean("billingDao");
			List<BillingAutoRules> mappings = billingDao.findAutoRulesMap(String.valueOf(appointmentTypeId));


			BillingONCHeader1Dao cheader1Dao = (BillingONCHeader1Dao) SpringUtils.getBean("billingONCHeader1Dao");
			Integer demographic = Integer.parseInt(request.getParameter("demographic_no"));

			for(BillingAutoRules map:mappings){
			//status has changed and patient marked as here so generate billing

      BillingAutoRulesExtDao billingAutoRulesExtDao = (BillingAutoRulesExtDao)SpringUtils.getBean("billingAutoRulesExtDao");
      List<BillingAutoRulesExt> codes = new ArrayList();
      int ruleId = map.getId();
      if(map.getServiceCode()){
        codes = billingAutoRulesExtDao.findActiveValuesByExtKey("service_code", ruleId);
      }

      List<BillingAutoRulesExt> dxCodes = new ArrayList();
      if(map.getDx()){
        dxCodes = billingAutoRulesExtDao.findActiveValuesByExtKey("dx_code", ruleId);
      }

      Demographic patient = demographicDao.getDemographic(request.getParameter("demographic_no"));
      String patientsMrp = patient.getProviderNo();

      String site = request.getParameter("location");
      
			String billingDoctor = map.getBillingDoctor();
      String billingDoctorType = map.getBillingDoctorType();
      if(billingDoctorType.equals("mrp"))
      billingDoctor = patientsMrp;

      String referralDoctor = map.getReferralDoctor();
      String referralDoctorType = map.getReferralDoctorType();
      if(referralDoctorType.equals("mrp"))
      referralDoctor = patientsMrp;


			String clinicRefCode = map.getVisitLocation();

			String visitType = map.getVisitType();


			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date serviceDate = new Date();
			String date_time = formatter.format(serviceDate);
			serviceDate = formatter.parse(date_time);

			String curUser = billingDoctor;

			cheader1Dao.createAutoGeneratedBill(billingDoctor, demographic, appt_no, codes, dxCodes, clinicRefCode, serviceDate, curUser, "0", referralDoctor, visitType, site);
			}
		} // mappings not empty

	} //demographic not empty
 }
} // auto_billing_enabled
%>

<script LANGUAGE="JavaScript">
    <%
        if(!(request.getParameter("printReceipt")==null) && request.getParameter("printReceipt").equals("1")) {
    %>
            popupPage(350,750,'printappointment.jsp?appointment_no=<%=request.getParameter("appointment_no")%>') ;
    <%}%>
	self.opener.refresh();
	self.close();
</script>
<%
	String apptNo = request.getParameter("appointment_no");
	String mcNumber = request.getParameter("appt_mc_number");
	OtherIdManager.saveIdAppointment(apptNo, "appt_mc_number", mcNumber);

	if(changedStatus != null){
		EventService eventService = SpringUtils.getBean(EventService.class); //updating an appt from the appt update screen delete doesn't work
		eventService.appointmentStatusChanged(this,apptNo.toString(), appt.getProviderNo(), changedStatus);
	}
	// End External Prescriber
  } else {
%>
<p>
<h1><bean:message key="appointment.appointmentupdatearecord.msgUpdateFailure" /></h1>

<%
  }
%>
<p></p>
<hr width="90%"/>
<form>
<input type="button" value="<bean:message key="global.btnClose"/>" onClick="closeit()">
</form>
</center>
</body>
</html:html>
