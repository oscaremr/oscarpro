<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%
  //Make sure user has logged in first and username is in the session

  if(session.getAttribute("userName") == null )
    response.sendRedirect("../logout.jsp");
    		
   String errormsg = "";
   if (request.getParameter("errormsg") != null) {
	   errormsg = request.getParameter("errormsg") ;
   } else if (request.getAttribute("errormsg") != null) {
	   errormsg = (String) request.getAttribute("errormsg");
   }
%>


<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<%@ page import="org.springframework.web.util.JavaScriptUtils"%>
<%@ page
	import="java.lang.*, java.util.*, java.text.*,java.sql.*, oscar.*"%>
<%@ page import="org.oscarehr.util.SpringUtils"%>
<%@ page import="org.oscarehr.managers.OktaManager"%>


<%!
	OscarProperties op = OscarProperties.getInstance();
	OktaManager oktaManager = SpringUtils.getBean(OktaManager.class);
%>

<html:html locale="true">
<head>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/checkPassword.js.jsp"></script>
<title><bean:message key="provider.providerchangepassword.title" /></title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<script type="text/javascript">
function setfocus(el) {
	this.focus();
	document.forms[0].elements[el].focus();
	document.forms[0].elements[el].select();
}
function checkPwdPolicy() {
		
	if (document.forms[0].oldPassword.value == "") {
		alert ('<bean:message key="provider.providerchangepassword.msgOldPasswordError"/>');
		setfocus('oldPassword');
		return false;
	}
	var pwd1=document.forms[0].newPassword.value;
	var pwd2=document.forms[0].confirmPassword.value;
		
	if (!validatePasswordWithoutOkta(pwd1)) {
		setfocus('newPassword');
		return false;
	}
	if (pwd1 != pwd2) {
		alert ('<bean:message key="provider.providerchangepassword.msgPasswordConfirmError"/>');
		setfocus('confirmPassword');
		return false;
	}
	return true;
}
</script>
</head>

<body onLoad="setfocus('oldPassword')" topmargin="0" leftmargin="0" rightmargin="0">
<html:form method="post" action="login" onsubmit="return checkPwdPolicy();">
<table border=0 cellspacing=0 cellpadding=0 width="100%">
	<tr bgcolor="#486ebd">
		<th align=CENTER NOWRAP><font face="Helvetica" color="#FFFFFF"><bean:message
			key="provider.providerchangepassword.description" /></font></th>
	</tr>
</table>

<table width="100%" border="0" cellpadding="2" bgcolor="#eeeeee">
	<tr>
		<td><font face="arial" size="2"><bean:message
			key="provider.providerchangepassword.msgInstructions" /> &nbsp; <b><bean:message
			key="provider.providerchangepassword.msgUpdate" /></b> <bean:message
			key="provider.providerchangepassword.msgClickButton" /></font></td>
	</tr>

</table>
<center>

<p><b><font color='red'><%=errormsg%></font></b>

<table border="0" width="100%" cellpadding="4" cellspacing="0">
	<tr>
		<td align="right" width="50%"><font face="arial"><bean:message
			key="provider.providerchangepassword.msgEnterOld" /> &nbsp; <b><bean:message
			key="provider.providerchangepassword.formOldPassword" />:</b></font></td>
		<td><input type=password name="oldPassword" value="" size=20 autocomplete="off"
			maxlength=32></td>
	</tr>
	<tr>
		<td width="50%" align="right"><font face="arial"><bean:message
			key="provider.providerchangepassword.msgChooseNew" /> &nbsp; <b><bean:message
			key="provider.providerchangepassword.formNewPassword" />:</b></font></td>
		<td><input type=password name="newPassword" value="" size=20 autocomplete="off"
			maxlength=32> <font size="-2">(<bean:message
			key="provider.providerchangepassword.msgAtLeast" />
			<%=op.getProperty("password_min_length")%> <bean:message
			key="provider.providerchangepassword.msgSymbols" />)</font></td>
	</tr>
	<tr>
		<td width="50%" align="right"><font face="arial"><bean:message
			key="provider.providerchangepassword.msgConfirm" /> &nbsp; <b><bean:message
			key="provider.providerchangepassword.formNewPassword" />:</b></font></td>
		<td><input type=password name="confirmPassword" value="" size=20 autocomplete="off"
			maxlength=32> <font size="-2">(<bean:message
			key="provider.providerchangepassword.msgAtLeast" />
			<%=op.getProperty("password_min_length")%> <bean:message
			key="provider.providerchangepassword.msgSymbols" />)</font></td>
	</tr>
</table>
</center>
<table width="100%" border="0" cellpadding="4" cellspacing="0"
	bgcolor="#486ebd">
	<tr>
		<TD align="center" width="50%"><INPUT TYPE="submit"
			VALUE='<bean:message key="provider.providerchangepassword.btnSubmit"/>'
			SIZE="7" onSubmit="return(checkPwdPolicy())"> </TD>			
	</tr>
</table>

    <input type=hidden name='forcedpasswordchange' value='true' />

</html:form>
</body>
</html:html>
