/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
// Wrapped in function to preserve group
function appointmentTypeServicesSpec() {
    const name = 'appointmentTypeService';
    const apiPath = '/ws/rs/appointmentType/';
    describe(name, () => {
        let service;
        let globals;
        let $httpBackend;
        const getPath = function (path) {
            return `${globals.contextPath}${apiPath}${path}`
        }

        beforeEach(() => {
            module('common.appointmentTypeService');

            inject(function($injector,_globals_) {
                service = $injector.get(name)
                globals = _globals_;
                $httpBackend = $injector.get('$httpBackend');
            });
        });

        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it('should make http get request to REST endpoint for all appointment types', () => {
            const response = { data: '1234'};
            $httpBackend
                .expectGET(getPath('all'))
                .respond(200, response);
            service.getAll().then(r => { expect(r).toEqual(response); });
            $httpBackend.flush();
        });

        it('should return the appropriate error message', () => {
            const message = 'Unable to retrieve appointment types.';
            const response = { data: '2345'};
            $httpBackend
                .expectGET(getPath('all'))
                .respond(500, response)
            service.getAll().catch((e) => { expect(e.message).toEqual(message) });
            $httpBackend.flush();
        });
    });
}