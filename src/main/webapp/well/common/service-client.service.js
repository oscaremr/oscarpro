/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function () {
    const module = angular.module("common.serviceClient", [])
        .service("serviceClientService", ServiceClientService);
    module.requires = [...module.requires, 'common.globals'];

    ServiceClientService.$inject = ['$http', 'globals'];
    function ServiceClientService($http, globals) {
        const apiPath = `${globals.contextPath}/admin/api/clientManage.json`;
        this.getClients = () => {
            return $http(angular.extend({ url: `${apiPath}?method=list` }, globals.httpConfigs.jsonConfig))
                .then(response => response.data)
                .catch(error => { throw new Error('An error occurred while retrieving rest clients.') });
        };
    }
})();