/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function () {
    const module = angular.module('common.systemPreferences', [])
        .service("systemPreferencesService", SystemPreferencesService);
    module.requires = [...module.requires, 'common.globals'];

    SystemPreferencesService.$inject = ['$http', 'globals'];
    function SystemPreferencesService ($http, globals) {
        const apiPath = `${globals.contextPath}/ws/rs/systemPreferences`;
        this.getThirdPartyLinkPreferences = () => {
            return $http(angular.extend({ url: `${apiPath}/thirdPartyLinkPreferences` }, globals.httpConfigs.jsonConfig))
                .then(response => response.data)
                .catch(error => { throw new Error('Unable to retrieve third party appointment link preferences.') });
        };
        
        this.getPatientPortalUrl = () => {
            return $http(angular.extend({ url: `${apiPath}/getPatientPortalUrl` }, globals.httpConfigs.jsonConfig))
                .then(response => response.data)
                .catch(error => { throw new Error(error + ' \nUnable to retrieve the URL for Patient Portal.') });
        };
    }
})();