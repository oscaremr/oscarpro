/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function () {

    const jsonConfig = {
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        }
    }
    const jsonConfigWithCache = angular.extend({ cache: true }, jsonConfig);

    /**
     * Module meant to be imported prior to initializing Angular 1.8.4 components
     * facilitating injection of global variables for tomcat context path and debug
     * messages in the log.
     *
     *    angular
     *        .module('YourModule', ['common.globals'])
     *        .service('YourService', function(globals) {
     *            // globals.contextPath == /oscar
     *            // globals.debug == false
     *        });
     *
     */
    angular.module('common.globals', [])
        .factory('globals', Globals);

    function Globals() {
        return {
            // Tomcat web app context path
            contextPath,

            // Cardswipe web app context path
            cardSwipeContextPath,

            // Indicates whether debug messages should appear in the browser console.
            debug,

            // Commonly used HTTP request headers for service ajax requests.
            httpConfigs: {
                jsonConfig,
                jsonConfigWithCache,
            },
        }
    }
})();
