/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
// Wrapped in function to preserve group
function globalFactorySpec() {
    describe('common.globals', () => {
        let $httpBackend;
        let globals;
        beforeEach(() => {
            module('common.globals');

            inject(function(_globals_) {
                globals = _globals_;
            });
        });
        it('should provide context path', () => {
            expect(globals.contextPath).toEqual('/oscar')
        });
        it('should provide CardSwipep context', () => {
            expect(globals.cardSwipeContextPath).toEqual('/CardSwipe')
        });
        it('should have debug set to false', () => {
            expect(globals.debug).toEqual(false);
        });
    });
}
