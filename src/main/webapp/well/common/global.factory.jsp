<%--
/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
--%>
<%@ page import="oscar.OscarProperties" %>
<%@ page language="java" contentType="application/javascript" %>
(function () {
    var debug = <%=OscarProperties.getInstance().getProperty("angular.debug", "false")%>;
    var contextPath = '<%=request.getContextPath()%>';
    var cardSwipeContextPath = '<%=OscarProperties.getInstance().getCardSwipeDeployedContext()%>';
<%-- This is reasonable, it happens during page translation to servlet and not during request. --%>
<%@ include file="global.factory.js"%>
})();
