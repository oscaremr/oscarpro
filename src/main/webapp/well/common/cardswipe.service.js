/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function() {
    const module = angular.module("common.cardSwipService", [])
        .service("cardSwipeService", CardSwipeService);
    module.requires = [...module.requires, 'common.globals'];

    CardSwipeService.$inject = ['$http', '$log', 'globals'];
    function CardSwipeService($http, $log, globals) {
        const apiPath = `${globals.cardSwipeContextPath}/ws`;
        this.validate = (hin, ver, providerNo) => {
            $log.log('CardSwipeService:validate', hin, ver, providerNo);
            return $http(
                angular.extend({
                    url: `${apiPath}?hc=${hin} ${ver}&providerNo=${providerNo}`,
                    method: 'GET',
                }, globals.httpConfigs.jsonConfig))
                .then(response => response.data)
                .catch(error => { throw new Error('Unable to validate hin(s).') });
        };
        this.bulk = (demographics) => {
            $log.log('CardSwipeService:bulk', demographics);
            return $http(
                angular.extend({
                    url: `${apiPath}?hc=placeholder&providerNo=placeholder`,
                    method: 'POST',
                    data: demographics
                }, globals.httpConfigs.jsonConfig))
                .then(response => response.data)
                .catch(error => { throw new Error('Unable to validate hin(s).') });
        };
    }
})();