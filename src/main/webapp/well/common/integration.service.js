/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function () {
    const CONNECTION_OPTION = {
        REST: 'REST',
        SOAP: 'SOAP',
        TPLINK: 'TPLINK'
    }

    const CONNECTION_STATUS = {
        NO_API_KEY: 'NO_API_KEY',         // Missing API key
        LOADING: 'LOADING',              // Retrieving config or connection status
        UNAVAILABLE: 'UNAVAILABLE',     // Service is down
        UNAUTHORIZED: 'UNAUTHORIZED',  // Invalid API key (S0)
        DISCONNECTED: 'DISCONNECTED', // Not configured or provisioned (S1)
        PROVISIONED: 'PROVISIONED',  // Not configured (S2)
        CONFIGURED: 'CONFIGURED',   // Configured but credentials don't work (S3)
        CONNECTED: 'CONNECTED',    // Configured and provisioned (S4)
    }

    const CONNECTION_INSTRUCTIONS = {}
    CONNECTION_INSTRUCTIONS[CONNECTION_STATUS.DISCONNECTED] = `
        <p>Establishing this connection will allow this integration to read and write data to the EMR via the REST and Soap web services.</p>
    `;
    CONNECTION_INSTRUCTIONS[CONNECTION_STATUS.PROVISIONED] = `
        <p>There is already an existing connection to  this integration.</p> 
        <p>Please check that the correct REST client and SOAP provider are selected before proceeding with system configuration.</p>
    `;
    CONNECTION_INSTRUCTIONS[CONNECTION_STATUS.CONFIGURED] = `
        <p>Updating this connection may correct issues when the integration is configured and unavailable.</p>
    `;
    CONNECTION_INSTRUCTIONS[CONNECTION_STATUS.CONNECTED] = `
        <p>A connection with this integration is <span class="text-success text-bold">active</span> and requires no configuration at this time.</p>
    `;

    const CONNECTION_STATUS_TEXT = {}
    CONNECTION_STATUS_TEXT[CONNECTION_STATUS.LOADING] = 'Loading ...';
    CONNECTION_STATUS_TEXT[CONNECTION_STATUS.NO_API_KEY] = 'System not configured';
    CONNECTION_STATUS_TEXT[CONNECTION_STATUS.UNAUTHORIZED] = 'Not authorized';
    CONNECTION_STATUS_TEXT[CONNECTION_STATUS.DISCONNECTED] = 'Disconnected';
    CONNECTION_STATUS_TEXT[CONNECTION_STATUS.PROVISIONED] = 'Connected but not configured locally';
    CONNECTION_STATUS_TEXT[CONNECTION_STATUS.CONFIGURED] = 'Configured and unavailable';
    CONNECTION_STATUS_TEXT[CONNECTION_STATUS.CONNECTED] = 'Connected';
    CONNECTION_STATUS_TEXT[CONNECTION_STATUS.UNAVAILABLE] = 'Unavailable';

    const CONNECTION_ERROR_TEXT = {};
    CONNECTION_ERROR_TEXT[CONNECTION_STATUS.UNAUTHORIZED] = `
        <p>Authentication rejected.</p>
        <p>Please contact your service provider.</p>
    `;
    CONNECTION_ERROR_TEXT[CONNECTION_STATUS.UNAVAILABLE] = `
        <p>This service is unavailable.</p>
        <p>Please try again later.</p>
    `;
    CONNECTION_ERROR_TEXT[CONNECTION_STATUS.NO_API_KEY] = `
        <p>This system has not been configured to access this integration.</p>
        <p>Please contact your service provider to have it configured.</p>
    `;

    const integrationEnums = {
        CONNECTION: {
            ERROR_TEXT: CONNECTION_ERROR_TEXT,
            INSTRUCTIONS: CONNECTION_INSTRUCTIONS,
            OPTIONS: CONNECTION_OPTION,
            STATUS: CONNECTION_STATUS,
            STATUS_TEXT: CONNECTION_STATUS_TEXT,
        }
    }

    const module = angular.module("common.integration", [])
        .constant('integrationEnums', integrationEnums)
        .filter('integrationService', IntegrationFilter)
        .service('integrationService', IntegrationService);
    module.requires = [...module.requires, 'common.globals']

    IntegrationFilter.$inject = ['integrationService'];
    function IntegrationFilter(service) {
        return (status, func) => service.filters[func].call(service, status);
    }

    IntegrationService.$inject = ['$http', '$q', '$rootScope', 'globals'];
    function IntegrationService($http, $q, $rootScope, globals) {
        return new (function () {
            const service = this;
            IntegrationServiceCalls.call(service, $http, globals);
            IntegrationConnection.call(service);

            const initConnection = function () {
                return service.getConnectionStatus()
                    .then(r => $rootScope.$broadcast('integration:ready'))
                    .catch(err => $rootScope.$broadcast('integration:error'));
            }

            const initConfig = function () {
                return service.generateConfig()
                    .then(generated => {
                        service.config = angular.extend(service.config, generated);
                        return initConnection();
                    })
                    .catch(err => $rootScope.$broadcast('integration:error'));
            }

            this.initialize = function () {
                return service.getConfig()
                    .then(config => { return config.configured ? initConnection() : initConfig(); })
                    .catch(err => {
                        $rootScope.$broadcast('integration:error');
                        service.loading = false;
                    })
                    .finally(() => service.loading = false);
            }
        });
    }

    function IntegrationConnection() {
        this.configured = false;
        this.hasAPIKey = false;
        this.authorized = false;
        this.provisioned = false;
        this.available = false;
        this.enabled = false;
        this.loading = true;
        this.config = {};

        this.handleStatusResponse = function (status) {
            this.authorized = status !== 401;
            this.provisioned = status > 199 && status < 300;
            this.available = status !== 408;
            this.status = status;
        }

        this.getStatus = function () {
            return this.loading ? CONNECTION_STATUS.LOADING
                : !this.enabled || !this.hasAPIKey ? CONNECTION_STATUS.NO_API_KEY
                : !this.available ? CONNECTION_STATUS.UNAVAILABLE
                : !this.authorized ? CONNECTION_STATUS.UNAUTHORIZED
                : !this.configured && !this.provisioned ? CONNECTION_STATUS.DISCONNECTED
                : this.configured && this.provisioned ? CONNECTION_STATUS.CONNECTED
                : this.provisioned ? CONNECTION_STATUS.PROVISIONED
                // else isConfigured() == true
                : CONNECTION_STATUS.CONFIGURED;
        };

        this.getStatusText = function (status) {
            return CONNECTION_STATUS_TEXT[status || this.getStatus()];
        };

        this.getStatusCSS = function (status) {
            return `connection-status-${status ? status.toLowerCase() : this.getStatus().toLowerCase()}`;
        };

        this.getSaveOperation = function (status) {
            status = status || this.getStatus();
            return status === CONNECTION_STATUS.DISCONNECTED || status === CONNECTION_STATUS.PROVISIONED
                ? 'Connect' : 'Update';
        };

        this.hasError = function () {
            return !this.loading && (!this.enabled || !this.authorized || !this.available || !this.hasAPIKey);
        }

        this.getErrorText = function (status) {
            return this.hasError() ? CONNECTION_ERROR_TEXT[status || this.getStatus()] : undefined;
        }

        this.filters = {
            text: this.getStatusText,
            css: this.getStatusCSS,
            saveOperation: this.getSaveOperation,
            hasError: this.hasError,
            errorText: this.getErrorText,
        }
    }

    function IntegrationServiceCalls($http, globals) {
        const apiPath = `${globals.contextPath}/ws/rs/integration`;
        this.getConfig = () => {
            const httpParams = angular.extend({
                url: `${apiPath}/config/${this.integration}`,
                method: 'GET',
            }, globals.httpConfigs.jsonConfig);
            return $http(httpParams).then(response => {
                this.config = angular.extend(this.config, response.data);
                this.hasAPIKey = this.config.hasApiKey;
                this.enabled = this.config.enabled;
                this.configured = this.config.configured;
                return response;
            }).catch(err => {
                throw new Error('An error occurred while retrieving configuration.');
            });
        };
        this.saveConfig = config => {
            const httpParams = angular.extend({
                url: `${apiPath}/config/${this.integration}`,
                method: 'POST',
                data: config,
            }, globals.httpConfigs.jsonConfig);
            return $http(httpParams)
                .then(response => {
                    this.config = response.data;
                    return this.config;
                })
                .catch(error => { throw new Error('An error occurred while submitting configuration.') });
        };
        this.generateConfig = () => {
            const httpParams = angular.extend(
                { url: `${apiPath}/config/${this.integration}/generate`},
                globals.httpConfigs.jsonConfig
            );
            return $http(httpParams)
                .then(response => response.data)
                .catch(error => { throw new Error('An error occurred while generating configuration.'); });
        }
        this.getConnectionStatus = (healthCheck = null, providerNo = null) => {
            let url = `${apiPath}/connection/${this.integration}?1=1`; 
            if (providerNo) {
                url += `&providerNo=${providerNo}`;
            }
            if (healthCheck) {
                url += `&healthCheck=${healthCheck}`;
            }
            const httpParams = angular.extend({
                url: url,
                method: 'GET',
            }, globals.httpConfigs.jsonConfig);
            return $http(httpParams).then(response => {
                this.handleStatusResponse(response.status);
                return this.status;
            }).catch(err => {
                throw new Error('An error occurred while retrieving connection status.');
            })
            .finally(() => this.loading = false);
        };
        this.getIntegrationAdminUrl = () => {
            const httpParams = angular.extend({
                url: `${apiPath}/adminUrl/${this.integration}`,
                method: 'GET',
            }, globals.httpConfigs.jsonConfig);
            return $http(httpParams)
                .then(response => response.data)
                .catch(error => {
                throw new Error(`An error occurred while retrieving the admin URL for integration: ${this.integration}`);
            })
                .finally(() => this.loading = false);
        };
    }
})();