/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
// Wrapped in function to preserve group
function cardSwipeServiceSpec() {
    const name = 'cardSwipeService';
    const apiPath = '/ws';
    describe(name, () => {
        let service;
        let globals;
        let $httpBackend;
        const getPath = function (path) {
            return `${globals.cardSwipeContextPath}${apiPath}${path}`
        }

        beforeEach(() => {
            module('common.cardSwipService');

            inject(function($injector,_globals_) {
                service = $injector.get(name)
                globals = _globals_;
                $httpBackend = $injector.get('$httpBackend');
            });
        });

        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it('should make http get request to REST endpoint to validate hin', () => {
            const response = { data: '1234'};
            const hin = 'placeholder';
            const providerNo = 'placeholder';
            $httpBackend
                .expectPOST(getPath(`?hc=${hin}&providerNo=${providerNo}`))
                .respond(200, response);
            service.validate(hin, providerNo).then(r => { expect(r).toEqual(response); });
            $httpBackend.flush();
        });

        it('should return the appropriate error message', () => {
            const hin = 'placeholder';
            const providerNo = 'placeholder';
            const message = 'Unable to validate hin(s).';
            const response = { data: '2345'};
            $httpBackend
                .expectPOST(getPath(`?hc=${hin}&providerNo=${providerNo}`))
                .respond(500, response)
            service.validate(hin, providerNo).catch((e) => { expect(e.message).toEqual(message) });
            $httpBackend.flush();
        });
    });
}