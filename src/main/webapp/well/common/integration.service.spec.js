/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
// Wrapped in function to preserve group
function integrationServiceSpec() {
    const name = 'integrationService';
    const apiPath = '/ws/rs/integration/';
    describe(name, () => {
        let enums;
        let service;
        let filter;
        let globals;
        let $httpBackend;
        let $rootScope;
        const getPath = function (path, data) {
            return `${globals.contextPath}${apiPath}${path}/insig${data ? '/' + data : ''}`
        }
        const getQueryPath = function (path, data) {
            return `${globals.contextPath}${apiPath}${path}/insig${data ? '?' + data : ''}`
        }
        beforeEach(() => {
            module('common.integration');
            inject(function(_$rootScope_, $injector, _globals_) {
                $rootScope = _$rootScope_;
                enums = $injector.get('integrationEnums');
                filter = $injector.get('integrationServiceFilter')
                service = $injector.get(name);
                service.integration = 'insig';
                globals = _globals_;
                $httpBackend = $injector.get('$httpBackend');
            });
        });
        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });
        describe('IntegrationFilter', () => {
            let filterTest;
            beforeEach(() => {
                filterTest = (filterFn, serviceFn) => {
                    Object.keys(enums.CONNECTION.STATUS).forEach(status => {
                        expect(filter(status, filterFn)).toEqual(service[serviceFn](status));
                    });
                }
            })
            it('should provide status text filter', () => {
                expect(Object.keys(service.filters)).toContain('text');
                filterTest('text', 'getStatusText');
            });
            it('should provide status css filter', () => {
                expect(Object.keys(service.filters)).toContain('css');
                filterTest('css', 'getStatusCSS');
            });
            it('should provide status save operation filter', () => {
                expect(Object.keys(service.filters)).toContain('saveOperation');
                filterTest('saveOperation', 'getSaveOperation');
            });
            it('should provider has error filter', () => {
                expect(Object.keys(service.filters)).toContain('hasError');
                filterTest('hasError', 'hasError');
            });
            it('should provide error text filter', () => {
                expect(Object.keys(service.filters)).toContain('errorText');
                filterTest('errorText', 'getErrorText');
            });
            it('should not leak internal members of IntegrationService', () => {
                expect(service.available).toBeFalse();
                expect(() => filter(200, 'handleStatusResponse')).toThrow();
                expect(service.available).toBeFalse();
            })
        });
        describe('IntegrationServiceCalls', () => {
            describe('getConfig', () => {
                it('should make http GET request to REST endpoint for configuration', done => {
                    const response = { hasApiKey: true, configured: true };
                    $httpBackend
                        .expectGET(getPath('config'))
                        .respond(200, response);
                    service.getConfig().then(r => {
                        expect(service.config).toEqual(response);
                        expect(service.hasAPIKey).toEqual(response.hasApiKey);
                        expect(service.configured).toEqual(response.configured);
                        expect(r.data).toEqual(response);
                        done();
                    });
                    $httpBackend.flush();
                });
                it('should preserve additional information stored locally in config', done => {
                    const extra = { extra: true };
                    service.config = extra;
                    const config = { hasApiKey: true, configured: true };
                    $httpBackend
                        .expectGET(getPath('config'))
                        .respond(200, config);
                    service.getConfig().then(r => {
                        expect(service.configured).toBeTrue()
                        expect(service.hasAPIKey).toBeTrue();
                        expect(service.config.hasApiKey).toBeTrue()
                        expect(service.config.configured).toBeTrue()
                        expect(service.config.extra).toBeTrue();
                        expect(service.config).toEqual({...r.data, ...extra});
                        done();
                    });
                    $httpBackend.flush();
                });
                it('should return the appropriate error message', done => {
                    const message = 'An error occurred while retrieving configuration.';
                    const response = { data: '2345'};
                    $httpBackend
                        .expectGET(getPath('config'))
                        .respond(500, response)
                    service.getConfig().catch((e) => {
                        expect(e.message).toEqual(message);
                        done();
                    });
                    $httpBackend.flush();
                });
            });
            describe('saveConfig', () => {
                it('should make http POST request to REST endpoint for configuration', done => {
                    const data = { test: 'data' };
                    const response = { hasApiKey: true, configured: true };
                    $httpBackend
                        .expectPOST(getPath('config'), d => {
                            expect(d).toEqual(JSON.stringify(data));
                            return true;
                        })
                        .respond(200, response);
                    service.saveConfig(data).then(r => {
                        expect(r).toEqual(response);
                        expect(service.config).toEqual(response);
                        done();
                    });
                    $httpBackend.flush();
                });
                it('should return the appropriate error message', done => {
                    const message = 'An error occurred while submitting configuration.';
                    const response = { data: '2345'};
                    $httpBackend
                        .expectPOST(getPath('config'))
                        .respond(500, response)
                    service.saveConfig().catch((e) => {
                        expect(e.message).toEqual(message);
                        done();
                    });
                    $httpBackend.flush();
                });
            });
            describe('generateConfig', () => {
                it('should make http get request to REST endpoint for connection status', done => {
                    $httpBackend
                        .expectGET(getPath('config', 'generate'))
                        .respond(211, 'test');
                    service.generateConfig().then(r => {
                        expect(r).toEqual('test');
                        done();
                    });
                    $httpBackend.flush();
                });
                it('should return the appropriate error message', () => {
                    const message = 'An error occurred while generating configuration.';
                    $httpBackend
                        .expectGET(getPath('config', 'generate'))
                        .respond(500)
                    service.generateConfig().catch((e) => { expect(e.message).toEqual(message) });
                    $httpBackend.flush();
                });
            });
            describe('getConnectionStatus', () => {
                it('should make http get request to REST endpoint for connection status', done => {
                    $httpBackend
                        .expectGET(getQueryPath('connection', '1=1'))
                        .respond(211);
                    service.getConnectionStatus().then(r => {
                        expect(r).toEqual(211);
                        done();
                    });
                    $httpBackend.flush();
                });
                it('should return the appropriate error message', done => {
                    const message = 'An error occurred while retrieving connection status.';
                    $httpBackend
                        .expectGET(getQueryPath('connection', '1=1'))
                        .respond(500)
                    service.getConnectionStatus().catch((e) => {
                        expect(e.message).toEqual(message);
                        done();
                    });
                    $httpBackend.flush();
                });
            });
        });
        describe('IntegrationConnection', () => {
            let STATUS;
            let TEXT;
            beforeEach(() => {
                STATUS = enums.CONNECTION.STATUS;
                TEXT = enums.CONNECTION.STATUS_TEXT;
                service.enabled = true;
                service.authorized = true;
                service.available = true;
                service.hasAPIKey = true;
                service.loading = false;
            });
            describe('getStatus', () => {
                it('should correctly determine no api key status', () => {
                    service.hasAPIKey = false;
                    expect(service.getStatus()).toEqual(STATUS.NO_API_KEY);
                });
                it('should correctly determine loading status', () => {
                    service.loading = true;
                    expect(service.getStatus()).toEqual(STATUS.LOADING);
                });
                it('should correctly determine unauthorized status', () => {
                    service.authorized = false;
                    expect(service.getStatus()).toEqual(STATUS.UNAUTHORIZED);
                });
                it('should correctly determine disconnected status', () => {
                    service.provisioned = false;
                    service.configured = false;
                    expect(service.getStatus()).toEqual(STATUS.DISCONNECTED);
                });
                it('should correctly determine provisioned status', () => {
                    service.provisioned = true;
                    service.configured = false;
                    expect(service.getStatus()).toEqual(STATUS.PROVISIONED);
                });
                it('should correctly determine configured status', () => {
                    service.provisioned = false;
                    service.configured = true;
                    expect(service.getStatus()).toEqual(STATUS.CONFIGURED);
                });
                it('should correctly determine connected status', () => {
                    service.provisioned = true;
                    service.configured = true;
                    expect(service.getStatus()).toEqual(STATUS.CONNECTED);
                });
                it('should correctly determine unavailable status', () => {
                    service.available = false;
                    expect(service.getStatus()).toEqual(STATUS.UNAVAILABLE);
                });
            });
            describe('getStatusText', () => {
                it('should use parameter when present', () => {
                    service.hasAPIKey = false;
                    expect(service.getStatusText()).toEqual("System not configured");
                    expect(service.getStatusText(STATUS.LOADING)).toEqual("Loading ...");

                });
                it('should indicate when api key is missing', () => {
                    service.hasAPIKey = false;
                    expect(service.getStatusText()).toEqual("System not configured");
                });
                it('should indicate when loading', () => {
                    service.loading = true;
                    expect(service.getStatusText()).toEqual("Loading ...");
                });
                it('should indicate when unauthorized', () => {
                    service.authorized = false;
                    expect(service.getStatusText()).toEqual("Not authorized");
                });
                it('should return correct text for disconnected status', () => {
                    service.provisioned = false;
                    service.configured = false;
                    expect(service.getStatusText()).toEqual(TEXT.DISCONNECTED);
                });
                it('should return correct text for provisioned status', () => {
                    service.provisioned = true;
                    service.configured = false;
                    expect(service.getStatusText()).toEqual(TEXT.PROVISIONED);
                });
                it('should return correct text for configured status', () => {
                    service.provisioned = false;
                    service.configured = true;
                    expect(service.getStatusText()).toEqual(TEXT.CONFIGURED);
                });
                it('should return correct text for connected status', () => {
                    service.provisioned = true;
                    service.configured = true;
                    expect(service.getStatusText()).toEqual(TEXT.CONNECTED);
                });
            });
            describe('getStatusCSS', () => {
                it('should use parameter when present', () => {
                    service.hasAPIKey = false;
                    expect(service.getStatusCSS())
                        .toEqual('connection-status-no_api_key');
                    expect(service.getStatusCSS(STATUS.CONNECTED))
                        .toEqual('connection-status-connected');
                });
                it('should generate css class for no api key status', () => {
                    service.hasAPIKey = false;
                    expect(service.getStatusCSS())
                        .toEqual('connection-status-no_api_key');
                });
                it('should generate css class for loading status', () => {
                    service.loading = true;
                    expect(service.getStatusCSS())
                        .toEqual('connection-status-loading');
                });
                it('should generate css class for unavailable status', () => {
                    service.available = false;
                    expect(service.getStatusCSS())
                        .toEqual('connection-status-unavailable');
                });
                it('should generate css class for disconnected status', () => {
                    service.provisioned = false;
                    service.configured = false;
                    expect(service.getStatusCSS())
                        .toEqual('connection-status-disconnected');
                });
                it('should generate css class for provisioned status', () => {
                    service.provisioned = true;
                    service.configured = false;
                    expect(service.getStatusCSS())
                        .toEqual('connection-status-provisioned');
                });
                it('should generate css class for configured status', () => {
                    service.provisioned = false;
                    service.configured = true;
                    expect(service.getStatusCSS())
                        .toEqual('connection-status-configured');
                });
                it('should generate css class for connected status', () => {
                    service.provisioned = true;
                    service.configured = true;
                    expect(service.getStatusCSS())
                        .toEqual('connection-status-connected');
                });
            });
            describe('getSaveOperation', () => {
                it('should use parameter when present', () => {
                    service.provisioned = false;
                    service.configured = false;
                    expect(service.getSaveOperation())
                        .toEqual('Connect');
                    expect(service.getSaveOperation(STATUS.CONFIGURED))
                        .toEqual('Update');
                });
                it('should return Connect when disconnected', () => {
                    service.provisioned = false;
                    service.configured = false;
                    expect(service.getSaveOperation())
                        .toEqual('Connect');
                });
                it('should return Connect when provisioned', () => {
                    service.provisioned = true;
                    service.configured = false;
                    expect(service.getSaveOperation())
                        .toEqual('Connect');
                });
                it('should return Update when configured', () => {
                    service.provisioned = false;
                    service.configured = true;
                    expect(service.getSaveOperation())
                        .toEqual('Update');
                });
                it('should return Update when connected', () => {
                    service.provisioned = true;
                    service.configured = true;
                    expect(service.getSaveOperation())
                        .toEqual('Update');
                });
            });
            describe('hasError', () => {
                it('should have no error while loading', () => {
                    service.loading = true;
                    service.authorized = false;
                    service.available = false;
                    service.hasAPIKey = false;
                    expect(service.hasError()).toBeFalse();
                });
                it('should have no error when loaded and authorized', () => {
                    // authorized implies service is available and API key is present.
                    service.loading = false;
                    service.authorized = true;
                    service.available = true;
                    service.hasAPIKey = true;
                    expect(service.hasError()).toBeFalse();
                });
                it('should have an error when unauthorized', () => {
                    service.loading = false;
                    service.authorized = false;
                    service.available = true;
                    service.hasAPIKey = true;
                    expect(service.hasError()).toBeTrue();
                });
                it('should have an error when service unavailable', () => {
                    service.loading = false;
                    service.authorized = true;
                    service.available = false;
                    service.hasAPIKey = true;
                    expect(service.hasError()).toBeTrue();
                });
                it('should have an error when missing API key', () => {
                    service.loading = false;
                    service.authorized = true;
                    service.available = true;
                    service.hasAPIKey = false;
                    expect(service.hasError()).toBeTrue();
                });
            });
            describe('getErrorText', () => {
                let STATUS;
                let TEXT;
                beforeEach(() => {
                    STATUS = enums.CONNECTION.STATUS;
                    TEXT = enums.CONNECTION.ERROR_TEXT;
                })
                it('should use parameter if specified', () => {
                    service.loading = false;
                    service.authorized = false;
                    service.available = true;
                    service.hasAPIKey = true;
                    expect(service.getErrorText()).toEqual(TEXT[STATUS.UNAUTHORIZED]);
                    expect(service.getErrorText(STATUS.CONNECTED)).toBeUndefined()
                });
                it('should show proper error when unauthorized', () => {
                    service.loading = false;
                    service.authorized = false;
                    service.available = true;
                    service.hasAPIKey = true;
                    expect(service.getErrorText()).toEqual(TEXT[STATUS.UNAUTHORIZED]);
                });
                it('should show proper error when service unavailable', () => {
                    service.loading = false;
                    service.authorized = true;
                    service.available = false;
                    service.hasAPIKey = true;
                    expect(service.getErrorText()).toEqual(TEXT[STATUS.UNAVAILABLE]);
                });
                it('should show proper error when missing API key', () => {
                    service.loading = false;
                    service.authorized = true;
                    service.available = true;
                    service.hasAPIKey = false;
                    expect(service.getErrorText()).toEqual(TEXT[STATUS.NO_API_KEY]);
                });
                it('should show proper error when not enabled', () => {
                    service.enabled = false;
                    service.loading = false;
                    service.authorized = true;
                    service.available = true;
                    service.hasAPIKey = true;
                    expect(service.getErrorText()).toEqual(TEXT[STATUS.NO_API_KEY]);
                });
                it('should return undefined when no error', () => {
                    service.loading = true;
                    expect(service.hasError()).toBeFalse();
                    expect(service.getErrorText()).toBeUndefined();
                });
            });
        });
        describe('initialize', () => {
            let config;
            beforeEach(() => {
                config = {
                    hasApiKey: true,
                    configured: true,
                    enabled: true,
                    available: true,
                    authorized: true,
                };
                spyOn(service, 'getConfig').and.callThrough();
                spyOn(service, 'generateConfig').and.callThrough();
                spyOn(service, 'getConnectionStatus').and.callThrough();
            })
            describe('initialize', () => {
                it('should retrieve configuration', (done) => {
                    $httpBackend
                        .expectGET(getPath('config'))
                        .respond(200, config)
                    $httpBackend
                        .expectGET(getPath('config', 'generate'))
                        .respond(200, config)
                    $httpBackend
                        .expectGET(getQueryPath('connection', '1=1'))
                        .respond(200)
                    expect(service.loading).toBeTrue();
                    service.initialize().then(() => {
                        expect(service.getConfig).toHaveBeenCalled();
                        expect(service.generateConfig).toHaveBeenCalled();
                        expect(service.getConnectionStatus).toHaveBeenCalled();
                        expect(service.config).toEqual(config);
                        expect(service.loading).toBeFalse();
                        expect(service.getStatus()).toEqual(enums.CONNECTION.STATUS.CONNECTED);
                        done();
                    });
                    $httpBackend.flush();
                    $rootScope.$digest()
                });
            });
        })
    });
}