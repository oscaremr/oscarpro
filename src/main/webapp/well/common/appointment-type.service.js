/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function() {
    const module = angular.module("common.appointmentTypeService", [])
        .service("appointmentTypeService", AppointmentTypeService);
    module.requires = [...module.requires, 'common.globals'];

    AppointmentTypeService.$inject = ['$http', 'globals'];
    function AppointmentTypeService($http, globals) {
        const apiPath = `${globals.contextPath}/ws/rs/appointmentType`;
        this.getAll = () => {
            return $http(angular.extend({ url: `${apiPath}/all` }, globals.httpConfigs.jsonConfig))
                .then(response => response.data)
                .catch(error => { throw new Error('Unable to retrieve appointment types.') });
        };
    }
})();