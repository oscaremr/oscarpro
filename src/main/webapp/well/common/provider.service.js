/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function () {
    const module = angular.module("common.provider", [])
        .service("providerService", ProviderService);
    module.requires = [...module.requires, 'common.globals'];

    ProviderService.$inject = ['$http', 'globals'];
    function ProviderService ($http, globals) {
        const apiPath = `${globals.contextPath}/ws/rs/providerService`;
        this.getAllActiveProviders = () => {
            return $http(angular.extend({ url: `${apiPath}/providers_json` }, globals.httpConfigs.jsonConfig))
                .then(response => response.data.content)
                .catch(error => { throw new Error('Error fetching active providers.') });
        };
    }
})();