/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
// Wrapped in function to preserve group
function providerServicesSpec() {
    const name = 'providerService';
    const apiPath = '/ws/rs/providerService/';

    describe(name, () => {
        let service;
        let globals;
        let $httpBackend;
        const getPath = function (path) {
            return `${globals.contextPath}${apiPath}${path}`
        }
        beforeEach(() => {
            module('common.provider');

            inject(function($injector,_globals_) {
                service = $injector.get(name)
                globals = _globals_;
                $httpBackend = $injector.get('$httpBackend');
            });
        });

        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });

        it('should make http get request to REST endpoint for all active providers', () => {
            const response = { content: { data: '1234'} };
            $httpBackend
                .expectGET(getPath('providers_json'))
                .respond(200, response);
            service.getAllActiveProviders().then(r => { expect(r).toEqual(response.content); });
            $httpBackend.flush();
        });

        it('should return the appropriate error message', () => {
            const message = 'Error fetching active providers.';
            const response = { data: '2345'};
            $httpBackend
                .expectGET(getPath('providers_json'))
                .respond(500, response)
            service.getAllActiveProviders().catch((e) => { expect(e.message).toEqual(message) });
            $httpBackend.flush();
        });
    });
}