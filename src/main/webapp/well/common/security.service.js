/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function () {
    const module = angular.module('common.securityCheck', [])
        .service('securityCheckService', SecurityCheckService);
    module.requires = [...module.requires, 'common.globals'];
    
    SecurityCheckService.$inject = ['$http', 'globals'];
    function SecurityCheckService($http, globals) {
        const apiPath = `${globals.contextPath}/ws/rs/security`;
        
        this.hasRight = (objectName, privilege, demographicNo) => {
            const httpParams = {
                url: `${apiPath}/hasRight?objectName=${objectName}` 
                    + `&privilege=${privilege}&demographicNo=${demographicNo}`
            };
            return $http(angular.extend(httpParams, globals.httpConfigs.jsonConfig))
                .then(response => response.data)
                .catch(error => {
                    throw new Error(error + "\nUnable to retrieve status of " + objectName + " right.")
                });
        }
    }
})();