/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
// Wrapped in function to preserve group
function scheduleControllerSpec() {
    describe('ScheduleController ', () => {
        let $q;
        let $controller;
        let controller;
        let $rootScope;
        let $scope;
        let integrationConfig;
        let integrationServiceMock;
        let appointmentTypeServiceMock;
        let providerServiceMock;
        let serviceClientServiceMock;
        let systemPreferencesServiceMock;
        let globals;
        let promiseStub;
        let connectionModel;

        beforeEach(() => {
            module('integration.app');
            integrationConfig = { integrationConfig: 'integrationConfig' };
            appointmentTypeServiceMock = jasmine.createSpyObj(
                'appointmentTypeService', ['getAll']);
            integrationServiceMock = jasmine.createSpyObj('integrationService', [
                'getAppointmentTypes',
                'getConfig',
                'saveConfig',
                'getProviders',
                'getRESTClients',
                'getThirdPartyLinkPreferences',
            ]);
            providerServiceMock = jasmine.createSpyObj(
                'providerService', ['getAllActiveProviders']);
            serviceClientServiceMock = jasmine.createSpyObj(
                'serviceClientService', ['getClients']);
            systemPreferencesServiceMock = jasmine.createSpyObj(
                'systemPreferencesService', ['getThirdPartyLinkPreferences']);
            inject(function($injector,
                            _$q_,
                            _$controller_,
                            _$rootScope_,
                            _integrationEnums_,
                            integrationOptionFilter,
                            _integrationService_,
                            _connectionModel_,
                            _providerService_,
                            _globals_) {
                $q = _$q_;
                $controller = _$controller_;
                $rootScope = _$rootScope_;
                $scope = $rootScope.$new();
                connectionModel = _connectionModel_;
                globals = _globals_;
                // Setup promise for getConfig
                integrationServiceMock.getConfig.and.callFake(() => {
                    const deferred = $q.defer();
                    deferred.resolve(integrationConfig);
                    return deferred.promise;
                });
                systemPreferencesServiceMock.getThirdPartyLinkPreferences.and.callFake(() => {
                    const deferred = $q.defer();
                    deferred.resolve({})
                    return deferred.promise;
                });
                // Populating inherited members from parent $scope(s).
                $controller('AdminController', {
                    $scope: $scope,
                    appointmentTypeService: appointmentTypeServiceMock,
                    integrationService: integrationServiceMock,
                    providerService: providerServiceMock,
                    serviceClientService: serviceClientServiceMock,
                    systemPreferencesService: systemPreferencesServiceMock,
                });
                $controller('ConnectionController', {
                    $q: $q,
                    $scope: $scope,
                    appointmentTypeService: appointmentTypeServiceMock,
                    integrationService: integrationServiceMock,
                    providerService: providerServiceMock,
                    serviceClientService: serviceClientServiceMock,
                    systemPreferencesService: systemPreferencesServiceMock,
                });
                // Creating test controller
                controller = $controller('ScheduleController', {
                    $q: $q,
                    $scope: $scope,
                    appointmentTypeService: appointmentTypeServiceMock,
                    systemPreferencesService: systemPreferencesServiceMock,
                });
                promiseStub = () => {
                    const deferred = $q.defer();
                    deferred.resolve([]);
                    $rootScope.$digest();
                    return deferred.promise;
                }
            });
        });
        describe('Event handlers', () => {
            describe('onAppointmentTypeChange', () => {
                let customType;
                beforeEach(() => {
                    customType = {id: 'Custom', name: 'Custom'};
                });
                it('should provide Custom option', done => {
                    appointmentTypeServiceMock.getAll.and.callFake(promiseStub);
                    controller.getAppointmentTypes().then(() => {
                        controller.getThirdPartyLinkPreferences().then(() => {
                            expect($scope.appointmentTypes.find(t => t.id === 'Custom')).toBeDefined();
                            done();
                        });
                    });
                    $rootScope.$apply();
                });
                it('should populate display name when a link type is selected', () => {
                    const type = {id: 'test', name: 'test'};
                    $scope.model.linkPrefs = {type};
                    $scope.onAppointmentTypeChange();
                    expect($scope.model.linkPrefs.customType).toEqual(type.name);
                    expect($scope.model.linkPrefs.display).toEqual(type.name[0]);
                });
                it('should should use current options when Custom link type is selected ', () => {
                    const type = {id: 'test', name: 'test'};
                    const custom = {id: 'Custom', name: 'Custom'};
                    controller.tpLinkType = type.name;
                    controller.tpLinkDisplay = type.name;
                    $scope.model.linkPrefs = {type: custom};
                    $scope.onAppointmentTypeChange();
                    expect($scope.model.linkPrefs.customType).toEqual(type.name);
                    expect($scope.model.linkPrefs.display).toEqual(type.name);
                });
            });
        });
        describe('Service calls', () => {
            const genFuncMock = function (mock, fn, val) {
                mock[fn].and.callFake(() => {
                    const d = $q.defer();
                    d.resolve(val);
                    return d.promise;
                })
            }
            describe('getAppointmentTypes', () => {
                it('should populate list with defaults', done => {
                    genFuncMock(appointmentTypeServiceMock, 'getAll', []);
                    controller.getAppointmentTypes().then(() => {
                        expect($scope.appointmentTypes).toEqual(controller.defaultAppointmentTypes);
                        done();
                    });
                    $rootScope.$apply();
                });
                it('should populate list with service response', done => {
                    const type = { test: 'type' }
                    genFuncMock(appointmentTypeServiceMock, 'getAll', [type]);
                    controller.getAppointmentTypes().then(() => {
                        expect($scope.appointmentTypes).toEqual([...controller.defaultAppointmentTypes, type]);
                        done();
                    });
                    $rootScope.$apply();
                });
            });
            describe('getThirdPartyLinkPreferences', () => {
                let tpLink;
                beforeEach(() => {
                    integrationServiceMock.config = {};
                    tpLink = {
                        enabled: false,
                        tpLinkType: 'Test1',
                        tpLinkDisplay: 'Test2'
                    }
                })
                it('should always set enabled to true', done => {
                    genFuncMock(systemPreferencesServiceMock, 'getThirdPartyLinkPreferences', tpLink)
                    controller.getThirdPartyLinkPreferences().then(() => {
                        expect($scope.model.linkPrefs.enabled).toBeTrue();
                        done();
                    });
                    $rootScope.$apply();
                });
                it('should default to Custom/Virtual/VC+', done => {
                    genFuncMock(systemPreferencesServiceMock, 'getThirdPartyLinkPreferences', {})
                    controller.getThirdPartyLinkPreferences().then(() => {
                        expect($scope.model.linkPrefs.type).toEqual('Custom');
                        expect($scope.model.linkPrefs.customType).toEqual('Virtual');
                        expect($scope.model.linkPrefs.display).toEqual('VC+');
                        done();
                    });
                    $rootScope.$apply();
                });
                it('should store values returned in scope', done => {
                    genFuncMock(systemPreferencesServiceMock, 'getThirdPartyLinkPreferences', tpLink)
                    controller.getThirdPartyLinkPreferences().then(() => {
                        expect($scope.model.linkPrefs.type).toEqual(tpLink.tpLinkType);
                        expect($scope.model.linkPrefs.customType).toEqual(tpLink.tpLinkType);
                        expect($scope.model.linkPrefs.display).toEqual(tpLink.tpLinkDisplay);
                        done();
                    });
                    $rootScope.$apply();

                })
                it('should cache current values in configuration', done => {
                    genFuncMock(systemPreferencesServiceMock, 'getThirdPartyLinkPreferences', tpLink)
                    controller.getThirdPartyLinkPreferences().then(() => {
                        expect(controller.tpLinkType).toEqual(tpLink.tpLinkType);
                        expect(controller.tpLinkDisplay).toEqual(tpLink.tpLinkDisplay);
                        done();
                    });
                    $rootScope.$apply();
                });
            });
            describe('Selectors', () => {
                describe('selectPreferredAppointmentType', () => {
                    it('should select the current link type if available', () => {
                        const type = {id: 'test', name: 'test'};
                        $scope.model.linkPrefs = {type: type.name};
                        $scope.appointmentTypes = [...controller.defaultAppointmentTypes, type];
                        controller.selectPreferredAppointmentType();
                        expect($scope.model.linkPrefs.type).toEqual(type);
                    });
                    it('should fall back to Custom when type not found', () => {
                        const type = {id: 'test', name: 'test'};
                        $scope.model.linkPrefs = {type: type.name};
                        $scope.appointmentTypes = controller.defaultAppointmentTypes;
                        controller.selectPreferredAppointmentType();
                        expect($scope.model.linkPrefs.type.name).toEqual('Custom');
                    });
                });
            });

            describe('initialize', () => {
                let initCallTest;
                beforeEach(() => {
                    integrationServiceMock.config = {};
                    appointmentTypeServiceMock.getAll.and.callFake(promiseStub);
                    systemPreferencesServiceMock.getThirdPartyLinkPreferences.and.callFake(promiseStub);
                    initCallTest = (fn, cb) => {
                        spyOn(controller, fn).and.callThrough();
                        expect(controller[fn]).toHaveBeenCalledTimes(0);
                        controller.initialize().then(() => {
                            expect(controller[fn]).toHaveBeenCalled();
                            cb();
                        });
                        $rootScope.$digest();
                    }
                });
                it('should retrieve appointment types', done => {
                    initCallTest('getAppointmentTypes', done);
                });
                it('should retrieve third party link preferences', done => {
                    initCallTest('getThirdPartyLinkPreferences', done);
                });
                it('should select preferred appointment types', done => {
                    initCallTest('selectPreferredAppointmentType', done);
                });
                it('should set loaded to true when done', done => {
                    expect($scope.loaded).toBeFalse();
                    controller.initialize().then(() => {
                        expect($scope.loaded).toBeTrue();
                        done();
                    });
                    $rootScope.$digest();
                });
            });

        });
    });
}