/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function() {
    angular.module("integration.app", [
        // Common code
        'integration.core',

        // Components
        'integration.admin',
        'integration.connection',
        'integration.schedule',
    ]);
})();