<%--
/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
 --%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%
	String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	boolean authed = true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.misc" rights="r" reverse="true">
<%
	authed = false;
	response.sendRedirect("../securityError.jsp?type=_admin&type=_admin.misc");
%>
</security:oscarSec>
<% if (!authed) { return; } %>
<jsp:useBean id="props" class="java.util.Properties"/>
<html ng-app="integration.app">
<head ng-cloak>
	<title>Loading integration ... </title>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/library/bootstrap/3.0.0/css/bootstrap.css" />
	<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/well/admin/integration/app.css" />
	<style>
		[ng-cloak] {
			display: none;
		}
	</style>
</head>
<body>
<div class="container">
	<ui-view>
		<h2>Integration</h2>
		<hr />
		<h3>Loading ...</h3>
	</ui-view>
</div>

<!-- Third party -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/angular-1.8.2/angular.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/angular-1.8.2/angular-sanitize.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/angular-ui-router-1.0.29/angular-ui-router.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/ui-bootstrap-tpls-0.11.0.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/showdown.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/markdown.js"></script>

<!-- START COMMON  -->
<script type="text/javascript" src="<%=request.getContextPath()%>/well/common/global.factory.jsp"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/common/appointment-type.service.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/common/integration.service.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/common/provider.service.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/common/service-client.service.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/common/system-preferences.service.js"></script>
<!-- END COMMON  -->

<!-- START INTEGRATION  -->
<script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/app.module.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/app.core.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/app.config.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/admin.component.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/connection.controller.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/schedule.controller.js"></script>
<!-- END INTEGRATION  -->

</body>
</html>
