/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function () {
    let $logProvider;

    angular.module('integration.app')
        .config(configure)
        .run(run);

    configure.$inject = ['$logProvider', '$stateProvider'];
    function configure(_$logProvider_, $stateProvider) {
        $logProvider = _$logProvider_;
        $stateProvider.state('Integration',{
            name: 'integration',
            url: '/{integration}?schedule',
            controller: 'AdminController',
            templateUrl: 'admin.component.html',
        });
    }

    run.$inject = ['globals', '$trace']
    function run(globals, $trace) {
        $logProvider.debugEnabled(globals.debug);
        if (globals.debug) {
            $trace.enable();
        }
    }
})();