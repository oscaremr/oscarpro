/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function() {
	const module = angular.module('integration.admin', [])
		.controller('AdminController', AdminController)
		.component('integrationAdmin', AdminDirective());
	module.requires = [...module.requires, 'integration.core', 'integration.connection'];

	AdminController.$inject = [
		'$window',
		'$rootScope',
		'$scope',
		'$location',
		'$log',
		'$q',
		'integrationService',
		'integrationEnums',
		'$stateParams'
	];
	function AdminController($window,
							 $rootScope,
							 $scope,
							 $location,
							 $log,
							 $q,
							 integrationService,
							 integrationEnums,
							 $stateParams) {

		angular.extend($scope, {
			$log: $log, // Facilitate template log statements throughout app
			status: integrationEnums.CONNECTION.STATUS.LOADING,
			integration: $stateParams.integration,
			integrationName: 'Loading',

			isIntegrationConfigured: function() { return integrationService.configured; },

			hasAPIKey: function () { return integrationService.hasAPIKey; },

			hasError: function () { return integrationService.hasError(); },

			getErrorText: function () { return integrationService.getErrorText(); },
		});

		this.$onInit = function() {
			integrationService.integration = $scope.integration;

			$scope.$on('integration:ready', () => {
				$scope.integrationName = integrationService.config.name;
				$window.document.title = `${integrationService.config.name} integration`;
			});

			// Cache connection status in scope when it changes to prevent recalculating it
			$scope.$watch(
				function (scope) { return integrationService.getStatus(); },
				function (status, _, scope) {
					$log.debug('ConnectionController:$watch IntegrationService:getStatus',status, _);
					scope.status = status;
				}
			);
			// Get integration config (and generate one if necessary)
			integrationService.initialize();
		}
	}

	function AdminDirective () {
		return {
			templateUrl: 'admin.component.html',
		};
	}
})();