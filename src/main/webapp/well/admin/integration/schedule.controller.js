/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function() {
    const schedule = angular.module("integration.schedule", [])
        .controller("ScheduleController", ScheduleController)
        .directive('integrationSchedule', ScheduleDirective)
    schedule.requires = [...schedule.requires, 'integration.core'];


    ScheduleController.$inject = [
        '$scope',
        '$log',
        '$q',
        'appointmentTypeService',
        'systemPreferencesService',
        'connectionModel', // model
    ];
    function ScheduleController($scope, $log, $q, appointmentTypeService, systemPreferencesService, model) {

        const controller = this;
        controller.tpLinkType = undefined;
        controller.tpLinkDisplay = undefined;

        this.defaultAppointmentTypes =[{id: 'Custom', name: 'Custom', customType: 'Custom'}];

        angular.extend($scope, {
            appointmentTypes: [],

            // START EVENT HANDLERS
            // Handles updating the custom type when an appointment type is changed.
            onAppointmentTypeChange: function () {
                if (model.linkPrefs.type.name === 'Custom') {
                    model.linkPrefs.customType = controller.tpLinkType;
                    model.linkPrefs.display = controller.tpLinkDisplay;
                } else {
                    model.linkPrefs.customType = model.linkPrefs.type.name;
                    model.linkPrefs.display = model.linkPrefs.type.name[0];
                }
            },
            // END EVENT HANDLERS
        });

        // START SERVICE CALLS
        this.getAppointmentTypes = function () {
            return appointmentTypeService.getAll()
                .then(function(types) {
                    $log.debug('ConnectionController.getAppointmentTypes', types);
                    $scope.appointmentTypes = [...controller.defaultAppointmentTypes, ...types];
                    return types;
                })
                .catch(function (response) {
                    $log.error('ConnectionController.getAppointmentTypes:error', response);
                    return response;
                });
        };

        this.getThirdPartyLinkPreferences = function () {
            return systemPreferencesService.getThirdPartyLinkPreferences()
                .then(function (prefs) {
                    // Models for appointment third party link settings
                    model.linkPrefs = {
                        enabled: true,
                        type: prefs.tpLinkType || 'Custom',
                        customType: prefs.tpLinkType || 'Virtual',
                        display: prefs.tpLinkDisplay || 'VC+',
                    };

                    // Cache initial values to allow them to be repopulated
                    // when custom is selected.
                    controller.tpLinkType = model.linkPrefs.customType;
                    controller.tpLinkDisplay = model.linkPrefs.display;
                    $log.debug("ConnectionController.getThirdPartyLinkPreferences", prefs);
                    return prefs;
                })
                .catch(function(response) {
                    $log.error("ConnectionController.getThirdPartyLinkPreferences:error", response);
                    return response;
                });
        };
        // END SERVICE CALLS

        // START SELECTORS
        this.selectPreferredAppointmentType = function () {
            // Default to currently selected link type
            model.linkPrefs.type = $scope.appointmentTypes.find(function (t) {
                return t.name === model.linkPrefs.type;
            });
            // Otherwise fall back to Custom
            if (model.linkPrefs.type === undefined) {
                model.linkPrefs.type = $scope.appointmentTypes[0];
            }
        }
        // END SELECTORS

        this.initialize = function () {
            const deferred = $q.defer();
            $q.all(
                [
                    this.getAppointmentTypes(),
                    this.getThirdPartyLinkPreferences(),
                ])
                .then(() => {
                    this.selectPreferredAppointmentType();
                    $scope.loaded = true;
                    deferred.resolve();
                })
                .catch(function(err) {
                    $log.error('Unable to initialize ConnectionController', err);
                    deferred.reject();
                });
            return deferred.promise;
        }
        this.$onInit = function () {
            // Wait for parent to finish loading.
            $scope.$on('integration:ready', () => {
                return this.initialize().catch(e => $log.error('initialization error', e))
            });
        }
    }

    // ScheduleController.$inject = [];
    function ScheduleDirective() {
        return {
            templateUrl: "schedule.controller.html"
        }
    }
})();