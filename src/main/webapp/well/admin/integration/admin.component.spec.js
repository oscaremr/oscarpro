/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
function adminControllerSpec() {
    describe('AdminController ', AdminControllerSpec);

    function AdminControllerSpec() {
        let $q;
        let $controller;
        let controller;
        let $rootScope;
        let $scope;
        let integrationEnums;
        let integrationService;
        let integrationServiceMock;
        let providerService;

        const integrationConfig = {configured: false};
        beforeEach(() => {
            module('integration.admin');

            integrationServiceMock = jasmine.createSpyObj('integrationService', [
                'initialize',
                'hasError',
                'getErrorText',
                'getConfig',
                'getStatus',
                'getConnectionStatus',
                'handleStatusResponse'
            ]);

            inject((_$q_, _$controller_, _$rootScope_, _integrationEnums_, _integrationService_, _providerService_) => {
                $q = _$q_;
                $controller = _$controller_;
                $rootScope = _$rootScope_;
                $scope = $rootScope.$new();
                integrationEnums = _integrationEnums_;
                integrationService = _integrationService_;
                providerService = _providerService_;

                // Setup promise for getConfig
                integrationServiceMock.getConfig.and.callFake(() => {
                    const deferred = $q.defer();
                    deferred.resolve(integrationConfig);
                    return deferred.promise;
                });
                // Setup promise for getConnectionStatus
                integrationServiceMock.getConnectionStatus.and.callFake(() => {
                    const deferred = $q.defer();
                    deferred.resolve({status: 206});
                    return deferred.promise;
                });


                controller = $controller('AdminController', {
                    $scope: $scope,
                    integrationService: integrationServiceMock,
                });
            });
        });
        describe('$scope', () => {
            describe('isIntegrationConfigured', () => {
                it('should return true when service is configured', () => {
                    expect($scope.isIntegrationConfigured()).toBeUndefined();
                    integrationServiceMock.configured = true;
                    expect($scope.isIntegrationConfigured()).toBeTrue();
                });
                it('should return false when service is not configured', () => {
                    expect($scope.isIntegrationConfigured()).toBeUndefined();
                    integrationServiceMock.configured = false;
                    expect($scope.isIntegrationConfigured()).toBeFalse();
                });
            });
            describe('hasAPIKey', () => {
                it('should return true when service has API key', () => {
                    expect($scope.hasAPIKey()).toBeUndefined();
                    integrationServiceMock.hasAPIKey = true;
                    expect($scope.hasAPIKey()).toBeTrue();
                });
                it('should return false when service is missing API key', () => {
                    expect($scope.hasAPIKey()).toBeUndefined();
                    integrationServiceMock.hasAPIKey = false;
                    expect($scope.hasAPIKey()).toBeFalse();
                });
            });
            describe('hasError', () => {
                it('should proxy call to service', () => {
                    integrationServiceMock.hasError.and.returnValues(false, true);
                    expect($scope.hasError()).toBeFalse();
                    expect($scope.hasError()).toBeTrue();
                    expect(integrationServiceMock.hasError).toHaveBeenCalledTimes(2);
                });
            });
            describe('getErrorText', () => {
                it('should proxy call to service', () => {
                    integrationServiceMock.getErrorText.and.returnValues(undefined, 'Some error');
                    expect($scope.getErrorText()).toBeUndefined();
                    expect($scope.getErrorText()).toEqual('Some error');
                    expect(integrationServiceMock.getErrorText).toHaveBeenCalledTimes(2);
                })
            });
        });
        describe('$onInit', () => {
            let STATUS;
            beforeEach(() => {
                STATUS = integrationEnums.CONNECTION.STATUS;
            });
            it('should watch status on integration service and update $scope.status', () => {
                expect(integrationServiceMock.getStatus).toHaveBeenCalledTimes(0);
                controller.$onInit();
                expect($scope.status).toEqual(STATUS.LOADING);
                expect(integrationServiceMock.initialize).toHaveBeenCalledTimes(1);
                integrationServiceMock.getStatus.and.returnValue(STATUS.CONNECTED);
                $scope.$digest();
                expect(integrationServiceMock.getStatus).toHaveBeenCalledTimes(2);
                expect($scope.status).toEqual(STATUS.CONNECTED);
            });
        });
    }
}
