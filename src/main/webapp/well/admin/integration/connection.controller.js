/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function() {
	const connection = angular.module("integration.connection", [])
		.controller("ConnectionController", ConnectionController)
		.directive('integrationConnection', ConnectionDirective)
		.filter('integrationOption', OptionFilter)
		.service('connectionModel', ConnectionModel);
	connection.requires = [...connection.requires, 'integration.core', 'integration.schedule'];

	OptionFilter.$inject = ['integrationEnums', 'integrationService'];
	function OptionFilter (enums, service) {
		return (model, option) => getOptionText(enums.CONNECTION.OPTIONS, option, model, service.config);
	}

	function getOptionText (OPTIONS, option, model, config = {}) {
		if (option == null) { return; }
		if (option === OPTIONS.REST) { return getRestOptionText(model, config); }
		if (option === OPTIONS.SOAP) {  return getSoapOptionText(model, config); }
		if (option === OPTIONS.TPLINK) { return getThirdPartyOptionText(model, config); }
		return `Error encountered while attempting to determine ${option} please check configuration.`;
	}

	function getRestOptionText(model, config) {
		return model.restClient.id === 'create'
			? `A <strong>${config.restClientName}</strong> REST client configuration will be created.`
			: `Existing REST client configuration <strong>${model.restClient.name}</strong> will be used.`;
	}

	function getSoapOptionText(model, config) {
		const randomPassword = model.providerPassword.length === 0 ? 'random': 'user generated';
		const result =  model.provider.providerNo === 'create'
			? `A <strong>new</strong> provider '${config.soapProviderLastName}, ${config.soapProviderFirstName}' will be created`
			: `Existing provider account <strong>${model.provider.displayName}</strong>`;
		return `${result} and a <strong>${randomPassword}</strong> password will be used.`
	}

	function getThirdPartyOptionText(model, config) {
		return model.linkPrefs.enabled
			? `Third party links will be set to appointment type 
						<strong>${model.linkPrefs.customType}</strong>
						with display name <strong>${model.linkPrefs.display}</strong>`
			: `Third party links will be <strong>disabled</strong>.`;
	}

	// Service instead of factory so we can inject it into the filter.
	function ConnectionModel() {
		this.provider = { providerNo: 'create', };
		this.providerPassword = '';
		this.restClient = { id: 'create' };
		this.linkPrefs = {};
	}

	ConnectionController.$inject = [
		'$scope',
		'$log',
		'$q',
		'$stateParams',
		'$location',
		'appointmentTypeService',
		'integrationService',
		'integrationEnums',
		'providerService',
		'serviceClientService',
		'systemPreferencesService',
		'connectionModel', // model
	];
	function ConnectionController(
			$scope,
			$log,
			$q,
			$stateParams,
			$location,
			appointmentTypeService,
			integrationService,
			integrationEnums,
			providerService,
			serviceClientService,
			systemPreferencesService,
			model) {

		const controller = this;

		// Convenience variables
		const STATUS = integrationEnums.CONNECTION.STATUS;
		const OPTIONS = integrationEnums.CONNECTION.OPTIONS;
		const INSTRUCTIONS = integrationEnums.CONNECTION.INSTRUCTIONS;

		this.PROVISION_MATCHING_ERROR = `
			<p><strong>Warning</strong> an established connection already exists; 
			however, new credentials are being generated.</p>
			<p>Please double check the selected credentials before proceeding.</p>					
		`;

		this.defaultProviders = [{ providerNo: 'create', name: 'Create new', displayName: 'Create new'}];
		this.defaultRestClients = [{ id: 'create', name: 'Create new'}];

		angular.extend($scope, {
			// Models for user input.
			OPTIONS,
			model,

			loaded: false,
			revealPassword: false,
			showOptions: false,
			schedule: $location.search().schedule,
			error: undefined,

			// Data sets for select menus.
			providers: [],
			restClients: [],

			// START DASHBOARD
			// Used to display configuration options in human readable format
			// when inputs for configuration are not visible.
			getErrorText: function() {
				return ($scope.loaded && $scope.status === STATUS.PROVISIONED &&
					(model.restClient.id === 'create' || model.provider.providerNo === 'create'))
					? controller.PROVISION_MATCHING_ERROR : undefined;
			},

			getInstructionsText: function () {
				return INSTRUCTIONS[$scope.status];
			},

			showDashboard: function (status) {
				return $scope.loaded
					&& (status === STATUS.DISCONNECTED
					|| status === STATUS.PROVISIONED
					|| status === STATUS.CONFIGURED
					|| status === STATUS.CONNECTED);
			},

			showDashboardDetails: function () {
				return !$scope.showOptions && $scope.status !== STATUS.CONNECTED;
			},

			showPasswordWarning: function () {
				return model.provider.providerNo !== 'create';
			},

			showSubmitButton: function () {
				return $scope.showOptions || $scope.status !== STATUS.CONNECTED;
			},

			toggleRevealPassword: function () {
				$scope.revealPassword = !$scope.revealPassword
			},
			// END DASHBOARD

			// START EVENT HANDLERS
			onSubmitClick: function () {
				$scope.error = undefined;
				$scope.showOptions = false;
				$log.log('getConnectionOptions', controller.getConnectionOptions());
				return integrationService.saveConfig(controller.getConnectionOptions())
					.then(() => {
						integrationService.getConfig()
							.then(controller.initialize)
							.then(initFields);
					})
					.catch(err => {
						$scope.showOptions = true;
						$scope.error = err;
					})

			},
			onToggleOptionsClick: function () {
				$scope.showOptions = !$scope.showOptions;
			}
			// END EVENT HANDLERS
		});

		// START SERVICE CALLS
		// Retrieve and populate the REST clients configured in the system
		this.getRESTClients = function () {
			return serviceClientService.getClients()
				.then(function (clients) {
					$scope.restClients = [...controller.defaultRestClients, ...clients];
					$log.debug('ConnectionController.getRESTClients', $scope.restClients);
					return clients;
				})
				.catch(function (response) {
					$log.error('ConnectionController.getRESTClients:error', response);
					return response;
				});
		};

		this.getConnectionOptions = function () {
			const options = {
				// need to pass int values of restClientId as string
				restClientId: model.restClient.id ? `${model.restClient.id}` : undefined,
				soapProviderNo: model.provider.providerNo,
				soapProviderPassword: model.providerPassword,
			};
			if ($scope.schedule) {
				angular.extend(options, {
					tpLinkEnabled: model.linkPrefs.enabled,
					tpLinkType: !model.linkPrefs.enabled ? ''
						: model.linkPrefs.type.name === 'Custom'
							? model.linkPrefs.customType : model.linkPrefs.type.name,
					tpLinkDisplay: !model.linkPrefs.enabled ? '' : model.linkPrefs.display,
				});
			}
			return options;
		};

		// Retrieve and populate the active providers in the system
		this.getProviders = function () {
			return providerService.getAllActiveProviders()
				.then(function (providers) {
					angular.forEach(providers, function (p) {
						p.displayName = p.name + ' ' + '(' + p.providerNo + ')';
					});
					$scope.providers = [...controller.defaultProviders, ...providers];
					$log.debug("ConnectionController.getProviders", providers);
					return providers;
				})
				.catch(function(response) {
					$log.error("ConnectionController.getProviders:error", response);
					return response;
				});
		};
		// END SERVICE CALLS

		// START SELECTORS
		this.selectProvider = function (providerNo) {
			const virtualProvider = $scope.providers.find(p => p.providerNo === providerNo);
			model.provider = virtualProvider !== undefined ? virtualProvider : $scope.providers[0];
			$log.debug('ConnectionController.virtualProvider', virtualProvider, model.provider);
		};

		this.selectRESTClient = function (id) {
			const virtualClient = $scope.restClients.find(c => c.id == id); // want type coercion here
			model.restClient = virtualClient !== undefined ? virtualClient : $scope.restClients[0];
			$log.debug('ConnectionController.virtualClient', virtualClient);
		};
		// END SELECTORS

		const initFields = function() {
			controller.selectProvider(integrationService.config.soapProviderNo)
			controller.selectRESTClient(integrationService.config.restClientId);
			$scope.loaded = true;
		};

		this.initialize = function () {
			return $q.all([
					controller.getProviders(),
					controller.getRESTClients(),
				])
				.then(initFields)
				.catch(err => {
					$log.error('Unable to initialize ConnectionController', err);
				});
		};

		this.$onInit = function () {
			// Wait for parent to finish loading.
			$scope.$on('integration:ready', () => this.initialize().catch(e => $log.error('initialization error', e)));
		}
	}

	function ConnectionDirective() {
		return {
			templateUrl: 'connection.controller.html',
		};
	}
})();