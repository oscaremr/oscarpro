/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function () {
    // Shared dependencies
    angular.module("integration.core", [
        // Factories
        'common.globals',

        // Services
        'common.appointmentTypeService',
        'common.integration',
        'common.provider',
        'common.serviceClient',
        'common.systemPreferences',

        // Third party
        'btford.markdown',
        'ui.bootstrap',
        'ui.router',
    ]);
})();