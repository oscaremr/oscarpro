/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
// Wrapped in function to preserve group
function connectionControllerSpec() {
	describe('ConnectionController ', () => {
		let $q;
		let $controller;
		let controller;
		let $rootScope;
		let $scope;
		let integrationConfig;
		let integrationEnums;
		let integrationOption;
		let integrationService;
		let integrationServiceMock;
		let appointmentTypeServiceMock;
		let providerService;
		let providerServiceMock;
		let serviceClientServiceMock;
		let systemPreferencesServiceMock;
		let globals;
		let promiseStub;
		let connectionModel;

		beforeEach(() => {
			module('integration.app');
			integrationConfig = { integrationConfig: 'integrationConfig' };
			appointmentTypeServiceMock = jasmine.createSpyObj(
				'appointmentTypeService', ['getAll']);
			integrationServiceMock = jasmine.createSpyObj('integrationService', [
				'getAppointmentTypes',
				'getConfig',
				'generateConfig',
				'saveConfig',
				'getProviders',
				'getRESTClients',
				'getThirdPartyLinkPreferences',
			]);
			providerServiceMock = jasmine.createSpyObj(
				'providerService', ['getAllActiveProviders']);
			serviceClientServiceMock = jasmine.createSpyObj(
				'serviceClientService', ['getClients']);
			systemPreferencesServiceMock = jasmine.createSpyObj(
				'systemPreferencesService', ['getThirdPartyLinkPreferences']);
			inject(function($injector,
							_$q_,
							_$controller_,
							_$rootScope_,
							_integrationEnums_,
							integrationOptionFilter,
							_integrationService_,
							_connectionModel_,
							_providerService_,
							_globals_) {
				$q = _$q_;
				$controller = _$controller_;
				$rootScope = _$rootScope_;
				$scope = $rootScope.$new();
				integrationEnums = _integrationEnums_;
				integrationOption = integrationOptionFilter;
				integrationService = _integrationService_;
				connectionModel = _connectionModel_;
				providerService = _providerService_;
				globals = _globals_;
				// Setup promise for getConfig
				integrationServiceMock.getConfig.and.callFake(() => {
					const deferred = $q.defer();
					deferred.resolve(integrationConfig);
					return deferred.promise;
				});
				systemPreferencesServiceMock.getThirdPartyLinkPreferences.and.callFake(() => {
					const deferred = $q.defer();
					deferred.resolve({})
					return deferred.promise;
				});
				// Populating inherited members from parent $scope.
				$controller('AdminController', {
					$scope: $scope,
					appointmentTypeService: appointmentTypeServiceMock,
					integrationService: integrationServiceMock,
					providerService: providerServiceMock,
					serviceClientService: serviceClientServiceMock,
					systemPreferencesService: systemPreferencesServiceMock,
				});
				controller = $controller('ConnectionController', {
					$q: $q,
					$scope: $scope,
					appointmentTypeService: appointmentTypeServiceMock,
					integrationService: integrationServiceMock,
					providerService: providerServiceMock,
					serviceClientService: serviceClientServiceMock,
					systemPreferencesService: systemPreferencesServiceMock,
				});
				promiseStub = () => {
					const deferred = $q.defer();
					deferred.resolve([]);
					$rootScope.$digest();
					return deferred.promise;
				}
			});
		});
		describe('$scope', () => {
			describe('Dashboard', () => {
				let OPTIONS;
				beforeEach(() => {
					OPTIONS = integrationEnums.CONNECTION.OPTIONS;
					$scope.loaded = true;
				});
				describe('getErrorText', () => {
					beforeEach(() => {
						$scope.model.provider = { providerNo: '999998' };
						$scope.model.restClient = { id: 1 }
						$scope.status = integrationEnums.CONNECTION.STATUS.PROVISIONED;
					});
					it('should not return an error when a provisioned system matches provider and rest client', () => {
						expect($scope.getErrorText()).toEqual(undefined);
					});
					it('should return an error when a provisioned system cannot match provider', () => {
						$scope.loaded = true;
						$scope.model.restClient = { id: 'create' }
						expect($scope.getErrorText()).toEqual(controller.PROVISION_MATCHING_ERROR);
					});
					it('should return an error when a provisioned system cannot match rest client', () => {
						$scope.loaded = true;
						$scope.model.provider = { providerNo: 'create' }
						expect($scope.getErrorText()).toEqual(controller.PROVISION_MATCHING_ERROR);
					});
				});
				describe('getInstructionsText', () => {
					let INSTRUCTIONS;
					let STATUS;
					beforeEach(() => {
						INSTRUCTIONS = integrationEnums.CONNECTION.INSTRUCTIONS;
						STATUS = integrationEnums.CONNECTION.STATUS;
					})
					function testInstructions(status, instructions) {
						$scope.status = status;
						expect($scope.getInstructionsText()).toEqual(instructions);
					}
					it('should return correct instructions for disconnected status', () => {
						testInstructions(STATUS.DISCONNECTED, INSTRUCTIONS.DISCONNECTED);
					});
					it('should return correct instructions for provisioned status', () => {
						testInstructions(STATUS.PROVISIONED, INSTRUCTIONS.PROVISIONED);
					});
					it('should return correct instructions for configured status', () => {
						testInstructions(STATUS.CONFIGURED, INSTRUCTIONS.CONFIGURED);
					});
					it('should return correct instructions for connected status', () => {
						testInstructions(STATUS.CONNECTED, INSTRUCTIONS.CONNECTED);
					});
				});
				describe('showDashboard', () => {
					let STATUS;
					beforeEach(() => {
						STATUS = integrationEnums.CONNECTION.STATUS;
					})
					it('should be hidden for status no api key', () => {
						expect($scope.showDashboard(STATUS.NO_API_KEY)).toBeFalse();
					});
					it('should be hidden while controller is loading', () => {
						$scope.loaded = false;
						expect($scope.showDashboard(STATUS.DISCONNECTED)).toBeFalse();
					});
					it('should be hidden for status loading', () => {
						expect($scope.showDashboard(STATUS.LOADING)).toBeFalse();
					});
					it('should be hidden for status unavailable', () => {
						expect($scope.showDashboard(STATUS.UNAVAILABLE)).toBeFalse();
					});
					it('should be hidden for status unauthorized', () => {
						expect($scope.showDashboard(STATUS.UNAUTHORIZED)).toBeFalse();
					});
					it('should be visible for status disconnected', () => {
						expect($scope.showDashboard(STATUS.DISCONNECTED)).toBeTrue();
					});
					it('should be visible for status provisioned', () => {
						expect($scope.showDashboard(STATUS.PROVISIONED)).toBeTrue();
					});
					it('should be visible for status configured', () => {
						expect($scope.showDashboard(STATUS.CONFIGURED)).toBeTrue();
					});
					it('should be visible for status connected', () => {
						expect($scope.showDashboard(STATUS.CONNECTED)).toBeTrue();
					});
				});
				describe('showDashboardDetails', () => {
					it('should be hidden when options are visible', () => {
						$scope.showOptions = false;
						expect($scope.showDashboardDetails()).toBeTrue();
						$scope.showOptions = true;
						expect($scope.showDashboardDetails()).toBeFalse();
					});
					it('should be hidden when status is connected', () => {
						$scope.showOptions = false;
						// Dashboard only visible for S1-S4
						$scope.status = integrationEnums.CONNECTION.STATUS.DISCONNECTED;
						expect($scope.showDashboardDetails()).toBeTrue();
						$scope.status = integrationEnums.CONNECTION.STATUS.PROVISIONED;
						expect($scope.showDashboardDetails()).toBeTrue();
						$scope.status = integrationEnums.CONNECTION.STATUS.CONFIGURED;
						expect($scope.showDashboardDetails()).toBeTrue();
						$scope.status = integrationEnums.CONNECTION.STATUS.CONNECTED;
						expect($scope.showDashboardDetails()).toBeFalse();
					});
				});
				describe('showPasswordWarning', () => {
					it('should be visible when using existing provider', () => {
						$scope.model.provider.providerNo = '999998';
						expect($scope.showPasswordWarning()).toBeTrue();
					});
					it('should be hidden when creating a new provider', () => {
						$scope.model.provider.providerNo = controller.defaultProviders[0].providerNo;
						expect($scope.showPasswordWarning()).toBeFalse();
					});
				});
				describe('showSubmitButton', () => {
					it('should be visible when options are visible', () => {
						$scope.status = integrationEnums.CONNECTION.STATUS.CONNECTED;
						$scope.showOptions = true;
						expect($scope.showSubmitButton()).toBeTrue();
						$scope.showOptions = false;
						expect($scope.showSubmitButton()).toBeFalse();
					});
					it('should be visible when status is not connected', () => {
						$scope.showOptions = false;
						// Dashboard only visible for S1-S4
						$scope.status = integrationEnums.CONNECTION.STATUS.DISCONNECTED;
						expect($scope.showSubmitButton()).toBeTrue();
						$scope.status = integrationEnums.CONNECTION.STATUS.PROVISIONED;
						expect($scope.showSubmitButton()).toBeTrue();
						$scope.status = integrationEnums.CONNECTION.STATUS.CONFIGURED;
						expect($scope.showSubmitButton()).toBeTrue();
						$scope.status = integrationEnums.CONNECTION.STATUS.CONNECTED;
						expect($scope.showSubmitButton()).toBeFalse();
					});
				});
				describe('toggleRevealPassword', () => {
					it('should set revealPassword to true when already false', () => {
						$scope.revealPassword = false;
						$scope.toggleRevealPassword();
						expect($scope.revealPassword).toBeTrue();
					});
					it('should set revealPassword to false when already true', () => {
						$scope.revealPassword = true;
						$scope.toggleRevealPassword();
						expect($scope.revealPassword).toBeFalse();
					});
				});
				describe('OPTIONS enum', () => {
					it('should be included in $scope', () => {
						expect($scope.OPTIONS).toEqual(OPTIONS);
					});
				})
			});
			describe('Event handlers', () => {
				describe('onSubmitClick', () => {
					it('should submit configuration option to EMR', () => {
						const config = { some: 'configuration' }
						integrationServiceMock.saveConfig.and.callFake(promiseStub);
						spyOn(controller, 'getConnectionOptions').and.returnValue(config);
						$scope.onSubmitClick();
						expect(integrationServiceMock.saveConfig).toHaveBeenCalledWith(config);
					});
				});
				describe('onToggleOptionsClick', () => {
					it('should set show options to true when value is currently false', () => {
						$scope.showOptions = false;
						$scope.onToggleOptionsClick();
						expect($scope.showOptions).toBeTrue();
					});
					it('should set show options to false when value is currently true', () => {
						$scope.showOptions = true;
						$scope.onToggleOptionsClick();
						expect($scope.showOptions).toBeFalse();
					});
				});
			});
		});
		describe('Service calls', () => {
			const genFuncMock = function(mock, fn, val) {
				mock[fn].and.callFake(() => {
					const d = $q.defer();
					d.resolve(val);
					return d.promise;
				})
			}

			describe('getRESTClients', () => {
				it('should populate list with defaults', done => {
					genFuncMock(serviceClientServiceMock, 'getClients', []);
					controller.getRESTClients().then(() => {
						expect($scope.restClients).toEqual(controller.defaultRestClients);
						done();
					});
					$rootScope.$apply();
				});
				it('should populate list with service response', done => {
					const client = { test: 'client' }
					genFuncMock(serviceClientServiceMock, 'getClients', [client]);
					controller.getRESTClients().then(() => {
						expect($scope.restClients).toEqual([...controller.defaultRestClients, client]);
						done();
					});
					$rootScope.$apply();
				});
			});
			describe('getProviders', () => {
				it('should populate list with defaults', done => {
					genFuncMock(providerServiceMock, 'getAllActiveProviders', []);
					controller.getProviders().then(() => {
						expect($scope.providers).toEqual(controller.defaultProviders);
						done();
					});
					$rootScope.$apply();
				});
				it('should populate list with service response', done => {
					const provider = { test: 'provider' }
					genFuncMock(providerServiceMock, 'getAllActiveProviders', [provider]);
					controller.getProviders().then(() => {
						expect($scope.providers).toEqual([...controller.defaultProviders, provider]);
						done();
					});
					$rootScope.$apply();
				});
			});
		});
		describe('Selectors', () => {
			describe('selectProvider', () => {
				let createProvider;
				const testProvider = function (providers, provider) {
					$scope.providers = [...controller.defaultProviders, ...providers];
					controller.selectProvider(provider.providerNo);
					expect($scope.model.provider).toEqual(provider);
				}
				it('should offer option to create a new provider', () => {
					const createProvider = { providerNo: 'create', name: 'Create new', displayName: 'Create new'}
					testProvider([], createProvider);
				});
				it('should select provider with matching providerNo', () => {
					const provider = { providerNo: '123456', name: 'ViRTual' };
					testProvider([provider], provider);
				});
				it('should select create when no match found.', () => {
					const createProvider = { providerNo: 'create', name: 'Create new', displayName: 'Create new'}
					const provider1 = { providerNo: '123456', name: 'ViRTual' };
					const provider2 = { providerNo: '123456', name: 'ViRTual' };
					testProvider([provider1, provider2], createProvider);
				});
			});
			describe('selectRESTClient', () => {
				const testClient = function (clients, client) {
					$scope.restClients = [...controller.defaultRestClients, ...clients]
					controller.selectRESTClient(client.id);
					expect($scope.model.restClient).toEqual(client);
				}
				it ('should include option to create a new REST client', () => {
					const client = { id: 'create', name: 'Create new'};
					testClient([], client);
				});
				it('should select REST client with matching id', () => {
					const client = { id: 23, name: 'ViRTual' };
					testClient([client], client);
				});
				it('should select create REST when no match found.', () => {
					const create = { id: 'create', name: 'Create new'};
					const client1 = { id: 23, name: 'ViRTual' };
					const client2 = { id: 24, name: 'vC' };
					testClient([client1, client2], create);
				});
			});
		});
		describe('OptionFilter', () => {
			let OPTIONS;
			beforeEach(() =>{
				OPTIONS = integrationEnums.CONNECTION.OPTIONS;
				integrationService.config = {
					soapProviderFirstName: 'FIRST',
					soapProviderLastName: 'LAST',
					restClientName: 'REST_CLIENT',
				};
			});
			describe('getOptionText', () => {
				it('should show when new rest client is being created', () => {
					const text = integrationOption($scope.model, OPTIONS.REST);
					expect(text).toContain('<strong>REST_CLIENT</strong>');
				});

				it('should show what client is selected', () => {
					$scope.model.restClient = { id: 1 };
					const text = integrationOption($scope.model, OPTIONS.REST);
					expect(text).toContain('Existing REST client');
				});

				it('should show when a new provider is being created', () => {
					const text = integrationOption($scope.model, OPTIONS.SOAP);
					expect(text).toContain('<strong>new</strong>');
				});

				it('should show what provider is selected', () => {
					$scope.model.provider = { providerNo: '999998' };
					const text = integrationOption($scope.model, OPTIONS.SOAP);
					expect(text).toContain('Existing provider account');
				});

				it('should indicate if the provider password is randomly generated', () => {
					const text = integrationOption($scope.model, OPTIONS.SOAP);
					expect(text).toContain('<strong>random</strong>');
				});

				it('should indicate if the provider password is supplied by user', () => {
					$scope.model.providerPassword = '1234';
					const text = integrationOption($scope.model, OPTIONS.SOAP);
					expect(text).toContain('<strong>user generated</strong>');
				});

				it('should indicate whether third party links will be enabled', () => {
					$scope.model.linkPrefs = { enabled: true };
					const text = integrationOption($scope.model, OPTIONS.TPLINK);
					expect(text).toContain('Third party links will be set to appointment type');
				});
				it('should indicate whether third party links will be enabled', () => {
					$scope.model.linkPrefs = { enabled: false };
					const text = integrationOption($scope.model, OPTIONS.TPLINK);
					expect(text).toContain('<strong>disabled</strong>');
				})
			});
		});
		describe('getConnectionOptions', () => {
			beforeEach(() => {
				angular.extend(connectionModel, {
					restClient: {},
					provider: {},
					password: '',
					linkPrefs: { type: {} },
				});
				$scope.schedule = true;
			});
			it ('should populate rest client id', () => {
				expect(controller.getConnectionOptions().restClientId).toBeUndefined();
				$scope.model.restClient = { id: '1234' };
				expect(controller.getConnectionOptions().restClientId).toEqual('1234');
			});
			it ('should populate soap provider no', () => {
				expect(controller.getConnectionOptions().soapProviderNo).toBeUndefined();
				$scope.model.provider = { providerNo: '1234' };
				expect(controller.getConnectionOptions().soapProviderNo).toEqual('1234');
			});
			it ('should populate soap provider password', () => {
				expect(controller.getConnectionOptions().soapProviderPassword).toEqual('');
				$scope.model.providerPassword = 'some password';
				expect(controller.getConnectionOptions().soapProviderPassword).toEqual('some password');
			});
			it ('should populate third party link enabled', () => {
				expect(controller.getConnectionOptions().tpLinkEnabled).toBeUndefined();
				$scope.model.linkPrefs.enabled = true;
				expect(controller.getConnectionOptions().tpLinkEnabled).toBeTrue();
				$scope.model.linkPrefs.enabled = false;
				expect(controller.getConnectionOptions().tpLinkEnabled).toBeFalse();
			});
			it('should not populate third party link information when schedule disabled', () => {
				$scope.schedule = undefined;
				const options = controller.getConnectionOptions();
				expect(options.tpLinkEnabled).toBeUndefined();
				expect(options.tpLinkType).toBeUndefined();
				expect(options.tpLinkDisplay).toBeUndefined();
			})
			it ('should populate third party link type when enabled', () => {
				expect(controller.getConnectionOptions().tpLinkType).toEqual('');
				$scope.model.linkPrefs = { enabled: true, type: { name: 'Type' } };
				expect(controller.getConnectionOptions().tpLinkType).toEqual('Type');
			});
			it ('should not populate third party link type when disabled', () => {
				expect(controller.getConnectionOptions().tpLinkType).toEqual('');
				$scope.model.linkPrefs = { enabled: false, type: { name: 'Type' } };
				expect(controller.getConnectionOptions().tpLinkType).toEqual('');
			});
			it ('should populate third party link custom value', () => {
				expect(controller.getConnectionOptions().tpLinkType).toEqual('');
				$scope.model.linkPrefs = { enabled: true, customType: 'Type', type: { name: 'Custom', customType: 'Custom' } };
				expect(controller.getConnectionOptions().tpLinkType).toEqual('Type');
			});
			it ('should populate third party link display name when enabled', () => {
				expect(controller.getConnectionOptions().tpLinkDisplay).toEqual('');
				$scope.model.linkPrefs = { enabled: true, type: {}, display: 'Display' };
				expect(controller.getConnectionOptions().tpLinkDisplay).toEqual('Display');
			});
			it ('should not populate third party link display name when enabled', () => {
				expect(controller.getConnectionOptions().tpLinkDisplay).toEqual('');
				$scope.model.linkPrefs = { enabled: false, display: 'Display' };
				expect(controller.getConnectionOptions().tpLinkDisplay).toEqual('');
			});
		});
		describe('initialize', () => {
			let initCallTest;
			beforeEach(() => {
				integrationServiceMock.config = {};
				integrationServiceMock.getAppointmentTypes.and.callFake(promiseStub);
				integrationServiceMock.getProviders.and.callFake(promiseStub);
				integrationServiceMock.generateConfig.and.callThrough()
				integrationServiceMock.getRESTClients.and.callFake(promiseStub);
				integrationServiceMock.getThirdPartyLinkPreferences.and.callFake(promiseStub);
				appointmentTypeServiceMock.getAll.and.callFake(promiseStub);
				providerServiceMock.getAllActiveProviders.and.callFake(promiseStub);
				serviceClientServiceMock.getClients.and.callFake(promiseStub);
				systemPreferencesServiceMock.getThirdPartyLinkPreferences.and.callFake(promiseStub);
				initCallTest = (fn, cb) => {
					spyOn(controller, fn).and.callThrough();
					expect(controller[fn]).toHaveBeenCalledTimes(0);
					controller.initialize().then(() => {
						expect(controller[fn]).toHaveBeenCalled();
						cb();
					});
					$rootScope.$digest();
				}
			});
			it('should retrieve providers', done => {
				initCallTest('getProviders', done);
			});
			it('should retrieve REST clients', done => {
				initCallTest('getRESTClients', done);
			});
			it('should select provider', done => {
				initCallTest('selectProvider', done);
			});
			it('should select rest client', done => {
				initCallTest('selectRESTClient', done);
			});
			it('should set loaded to true when done', done => {
				expect($scope.loaded).toBeFalse();
				controller.initialize().then(() => {
					expect($scope.loaded).toBeTrue();
					done();
				});
				$rootScope.$digest();
			});
		});
	});
}