/**
 * This control allows users to attach content to a
 * 'attachment-control-printables' element that can be sent as an attachment.
 *
 * To include this control in an eForm make sure that it contains the
 * following HTML (if jQuery is already present the first line may be omitted.)

 <script language="javascript" type="text/javascript" src="${oscar_javascript_path}eforms/jquery-2.0.3.min.js"></script>
 <script language="javascript" type="text/javascript" src="${oscar_javascript_path}../../attachments/AttachmentManagerWidget.js"></script>
 <div id='attachment-control'></div><input id='attachment-control-printables' name='attachment-control-printables' type='hidden'>
 <input type="hidden" id="proContextPath" value="${proContextPath}">
 <input type="hidden" id="demographicNo" value="${demographicNo}">
 <input type="hidden" id="providerNo" value="${providerNo}">

 */
(function ($, window) {
  if (typeof jQuery == "undefined") {
    alert(
      "The attachmentManagerWidget library requires jQuery. Please ensure that it is loaded first",
    );
    return;
  }

  const oscarContext = $("#attachment-control-oscar-context").val();
  var SIGNATURE_CONTROL_STYLES = `
        @import url('${oscarContext}/css/bootstrap-wrapper.min.css');
        @import url('${oscarContext}/css/attachment-manager-widget.css');
        
    `;

  function attachmentTemplate(context) {
    return `
            <div class="printable">
                <div class="col-xs-5 text-overflow-ellipse">${context.name}</div>
                <div class="col-xs-3">${context.type}</div>
                <div class="col-xs-3" style="${context.date ? "" : "opacity: 0.6"}">
                    ${context.date ? convertDateToIsoString(context.date) : "N/A"}
                </div>
                <div class="col-xs-1"><button class="btn btn-remove-selected btn-xs pull-right">Remove</button></div>
            </div>
        `;
  }

  function convertDateToIsoString(unixTimeInMilliseconds) {
    return new Date(unixTimeInMilliseconds).toISOString().split("T")[0];
  }

  function AttachmentManagerWidget() {
    this.initialize();
  }

  AttachmentManagerWidget.prototype = {
    initialize: function () {
      this.attachmentUrl = `/${this.getOscarProContext()}/#/attachment-manager`;
      this.$el = $("#attachment-control");
      if (this.$el == null || this.$el.size() === 0) {
        if ($(".DoNotPrint").size() > 0) {
          this.$el = jQuery("<div id='attachment-control'>&nbsp;</div>");
          $(".DoNotPrint").append(this.$el);
        } else {
          alert(
            "Missing placeholder please ensure a div with the id attachment-control or a div with class DoNotPrint exists on the page .",
          );
          return;
        }
      }
      this.printables = [];
      this.$printables = $("#attachment-control-printables");
      this.$selectedCount = this.$el.find(".selected-count");
      this.$body = this.$el.find("#selected-list");
      this.$addButton = this.$el.find("#attachment-add-button");
      this.$editButton = this.$el.find("#attachment-edit-button");
      this.$clearButton = this.$el.find("#attachment-clear-button");
      this.$addButton.click(this.addAttachments.bind(this));
      this.$editButton.click(this.editAttachments.bind(this));
      this.$clearButton.click(this.clearAttachments.bind(this));
      this.setStyles();
      this.updateButtons();
      window.onAttachmentsSaved = this.onAttachmentsSaved.bind(this);
      if (this.$printables.val().trim().length > 0) {
        this.loadPrintables();
      }
    },
    setStyles: function () {
      this.$el.addClass("bootstrap-wrapper");
      var $style = $("<style>");
      $style.html(SIGNATURE_CONTROL_STYLES);
      this.$el.append($style);
    },
    getOscarProContext: function () {
      return $("#proContextPath").val();
    },
    getDemographicNo: function () {
      return $("#demographicNo").val();
    },
    getProviderNumber: function () {
      return $("#logged-in-provider-number").val();
    },
    openAttachmentManager: function () {
      window
        .open(
          this.attachmentUrl
            + "?demographicNumber="
            + this.getDemographicNo()
            + "&providerNumber="
            + this.getProviderNumber(),
          "attachmentManager",
          "width=1024,height=660",
        )
        .focus();
    },
    addAttachments: function () {
      this.openAttachmentManager();
      return false;
    },
    editAttachments: function () {
      this.openAttachmentManager();
      return false;
    },
    clearAttachments: function () {
      this.$body.slideUp();
      this.printables = [];
      this.$body.empty();
      this.updateButtons();
      this.updatePrintables();
      return false;
    },
    onAttachmentsSaved: function (printables) {
      this.printables = printables;
      this.updateButtons();
      this.updatePrintables();
      this.$body.empty();
      $.each(
        printables,
        function (index, element) {
          var $el = $(attachmentTemplate(element));
          $el
            .find("button")
            .click(this.removePrintable.bind({ $el: $el, context: this }));
          $el.data("printable", element);
          this.$body.append($el);
        }.bind(this),
      );
      this.$body.slideDown();
    },
    removePrintable: function () {
      var printable = this.$el.data("printable");
      this.context.printables.splice(
        $.inArray(printable, this.context.printables),
        1,
      );
      this.$el.remove();
      this.context.updateButtons();
      this.context.updatePrintables();
      if (this.context.printables.length === 0) {
        this.context.$body.slideUp();
      }
    },
    updateButtons: function () {
      var hasPrintables = this.printables.length > 0;
      this.$addButton.toggle(!hasPrintables);
      this.$editButton.toggle(hasPrintables);
      this.$clearButton.toggle(hasPrintables);
    },
    updatePrintables: function () {
      this.$printables.val(JSON.stringify(this.printables));
      this.$selectedCount.html(this.printables.length);
      this.$selectedCount.toggle(this.printables.length > 0);
    },
    loadPrintables: function () {
      var parse = JSON.parse(this.$printables.val());
      if (typeof parse == "object") {
        onAttachmentsSaved(parse);
      }
    },
  };
  $(function () {
    window.AttachmentManagerWidget = new AttachmentManagerWidget();
  });
})(jQuery, window, void 0);
