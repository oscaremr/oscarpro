<%--
/**
* Copyright (c) 2021 WELL EMR Group Inc.
* This software is made available under the terms of the
* GNU General Public License, Version 2, 1991 (GPLv2).
* License details are available via "gnu.org/licenses/gpl-2.0.html".
*/
--%>
<%@ page import="com.well.hcv.HCVBulkFactory" %>
<%@ page import="com.google.gson.GsonBuilder" %>
<%@ page import="com.google.gson.Gson" %>
<%@ page import="oscar.OscarProperties" %>
<jsp:useBean id="providerBean" class="java.util.Properties" scope="session"/>
<html ng-app="hcv.app">
<%
    HCVBulkFactory hcvBulkFactory = new HCVBulkFactory(session, request, providerBean).invoke();
    Gson gson = new GsonBuilder().create();
%>

<head>
    <link rel="stylesheet" href="<%=request.getContextPath() %>/library/bootstrap/3.0.0/css/bootstrap.css" />
    <link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<%=request.getContextPath() %>/well/hcv/app.css" />

    <title>HCV</title>
</head>
<body>
<div class="container">
    <ui-view>
        <h3>Bulk HCV</h3>

        Loading ....
    </ui-view>
</div>
<!-- Third party -->
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/angular-1.8.2/angular.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/angular-1.8.2/angular-sanitize.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/angular-ui-router-1.0.29/angular-ui-router.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/ui-bootstrap-tpls-0.11.0.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/showdown.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/library/markdown.js"></script>

<!-- START COMMON  -->
<script type="text/javascript">
    var debug = <%=OscarProperties.getInstance().getProperty("angular.debug", "false")%>;
    var contextPath = '<%=request.getContextPath()%>';
    var cardSwipeContextPath = '<%=OscarProperties.getInstance().getCardSwipeDeployedContext()%>';
</script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/common/global.factory.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/check_hin.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/common/cardswipe.service.js"></script>
<!-- END COMMON  -->

<script type="text/javascript">
    angular.module('hcv.constants', []).constant('hcvBulkParams', {
    	demographics: <%= gson.toJson(hcvBulkFactory.getDemographicsWithValidHin())%>,
        demographicsInvalid: <%= gson.toJson(hcvBulkFactory.getDemographicsWithInvalidHin())%>,
        demographicsOutOfOn: <%= gson.toJson(hcvBulkFactory.getDemographicsWithOutOfOnHin())%>
    });
</script>

<!-- START APP -->
<script type="text/javascript" src="<%=request.getContextPath()%>/well/hcv/app.module.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/hcv/app.core.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/hcv/app.config.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/well/hcv/bulk.component.js"></script>
<!-- END APP -->

</body>
</html>
