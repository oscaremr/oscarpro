/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function() {
	const module = angular.module('hcv.bulk', [])
		.controller('BulkController', BulkController)
		.component('hcvBulk', BulkDirective());
	module.requires = [...module.requires, 'hcv.core', 'hcv.constants'];


	BulkController.$inject = [
		'$window',
		'$rootScope',
		'$scope',
		'$log',
		'$q',
		'cardSwipeService',
		'hcvBulkParams',
		'globals',
		'$stateParams'
	];
	function BulkController($window,
							 $rootScope,
							 $scope,
							 $log,
							 $q,
							 cardSwipeService,
							 hcvBulkParams,
							 globals,
							 $stateParams) {

		angular.extend($scope, {
			loaded: false,
			globals: globals,
			hcvError: false,
			demographics: [],
			demographicsInvalid: [],
			demographicsOutOfOn: []
		});

		this.$onInit = function() {
			$log.debug('params', hcvBulkParams);
			cardSwipeService.bulk(hcvBulkParams.demographics)
				.then(data => {
					$log.debug('response', data);
					$scope.demographics = data;
					$scope.demographicsInvalid = hcvBulkParams.demographicsInvalid;
					$scope.demographicsOutOfOn = hcvBulkParams.demographicsOutOfOn;
					$scope.loaded = true;
				})
				.catch(error => {
					$scope.hcvError = true;
					$log.error('Unable to validate hin(s)', error);
				});
		}
	}

	function BulkDirective () {
		return {
			templateUrl: 'bulk.component.html',
		};
	}
})();