/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
(function() {
    describe('Integration Admin UI', () => {
        describe('Module', moduleSpec);
        adminControllerSpec();
        connectionControllerSpec();
        scheduleControllerSpec();
        subscriptionControllerSpec();
    });

    function moduleSpec() {
        let $$logProvider;

        beforeEach(module('integration.app', ($logProvider) => {
            $$logProvider = $logProvider;
        }));

        // Trigger module injection
        beforeEach(inject(() => { }));

        it('should disable debug logging', () => {
            expect($$logProvider.debugEnabled()).toEqual(false);
        });
    }
})();