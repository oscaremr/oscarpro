<%--
/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
--%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Test Runner</title>
    <link rel="stylesheet" href="<%=request.getContextPath()%>/library/jasmine-3.6.0/jasmine.css">

    <script>
        window.FAIL_UNIMPLEMENTED = true;
    </script>
    <!-- App dependencies -->
    <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/library/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/library/angular-1.8.2/angular.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/library/angular-1.8.2/angular-sanitize.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/library/angular-ui-router-1.0.29/angular-ui-router.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/library/ui-bootstrap-tpls-0.11.0.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/library/showdown.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/library/markdown.js"></script>

    <!-- Testing libraries -->
    <script type="text/javascript" src="<%=request.getContextPath()%>/library/jasmine-3.6.0/jasmine.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/library/jasmine-3.6.0/jasmine-html.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/library/jasmine-3.6.0/boot.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/library/angular-1.8.2/angular-mocks.js"></script>

    <!-- START COMMON -->
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/global.factory.jsp"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/appointment-type.service.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/integration.service.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/provider.service.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/service-client.service.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/system-preferences.service.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/subscription.service.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/cardswipe.service.js"></script>

    <!-- Unit tests -->
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/appointment-type.service.spec.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/global.factory.spec.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/integration.service.spec.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/provider.services.spec.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/service-client.service.spec.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/system-preferences.service.spec.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/subscription.service.spec.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/common/cardswipe.service.spec.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/test/common.spec.js"></script>
    <!-- END COMMON -->

    <!-- START INTEGRATION  -->
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/app.module.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/app.core.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/app.config.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/admin.component.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/connection.controller.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/schedule.controller.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/subscription.controller.js"></script>

    <!-- Unit tests -->
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/admin.component.spec.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/connection.controller.spec.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/schedule.controller.spec.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/admin/integration/subscription.controller.spec.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/well/test/integration.spec.js"></script>
    <!-- END INTEGRATION  -->
</head>

<body>
</body>
</html>
