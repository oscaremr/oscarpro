(function () {
    const portal = angular.module('portal.button', [])
        .controller('PortalButtonController', PortalButtonController)
        .directive('portalButton', PortalButtonDirective)
        .service('PortalButtonModel', PortalButtonModel);
    portal.requires = [...portal.requires, 'portal.core'];
    
    PortalButtonModel.$inject = ['securityCheckService', 'systemPreferencesService', 'integrationService', '$q', '$log', 'globals'];
    function PortalButtonModel(securityCheckService, systemPreferencesService, integrationService, $q, $log, globals) {
        const model = this;
        
        const CONNECTED = 'Access patient portal, send messages and documents.';
        const NOT_CONNECTED_WITH_ADMIN = 'Patient portal is either unavailable or not configured. Click here to configure.';
        const NOT_CONNECTED_NO_ADMIN = 'Patient portal is either unavailable or not configured. Please contact an ' +
            'administrator or support as appropriate.';
        const LINK_NOT_CONFIGURED = 'Link to the patient portal has not been properly configured in your database. Please contact support.'
        
        this.portalUrl = '#';
        this.connectionStatus = '';
        this.adminPrivilege = false;
        this.loading = true;
        this.disconnected = true;
        this.integration = 'insig';
        this.adminUrl = '#';
        this.title = 'Patient Portal';
        this.link = '';
        
        this.getUrl = function() {
            return systemPreferencesService.getPatientPortalUrl()
                .then(function(response) {
                    model.portalUrl = response.url;
                }).catch(function(error) {
                    $log.error('PortalButtonModel.getUrl:error', error);
                });
        }

        this.getConnectionStatus = function() {
            integrationService.integration = model.integration;
            return integrationService.getConnectionStatus(true)
                .then(function (response) {
                    model.connectionStatus = response;
                }).catch(function(error) {
                    $log.error('PortalButtonModel.getUrl:error', error);
                });
        }

        this.hasRight = function() {
            return securityCheckService.hasRight('_admin', 'w')
                .then(function(response) {
                    model.adminPrivilege = response.success;
                }).catch(function(error) {
                    $log.error('PortalButtonModel.getRight:error', error);
                });
        }
        
        this.getAdminUrl = function() {
            return integrationService.getIntegrationAdminUrl()
                .then(function(response) {
                    model.adminUrl = response.url;
                }).catch(function(error) {
                    $log.error('PortalButtonModel.getAdminUrl:error', error);
                });
        }
        
        this.buildPortalButton = function() {
            if (model.connectionStatus === 200) {
                model.disconnected = false;
                model.link = model.portalUrl;
                model.title = CONNECTED;
            } else if (model.adminPrivilege) {
                model.link = globals.contextPath + model.adminUrl;
                model.title = NOT_CONNECTED_WITH_ADMIN;
            } else {
                model.title = NOT_CONNECTED_NO_ADMIN;
            }
            
            if (!model.link) {
                model.title = LINK_NOT_CONFIGURED;
            }
        }
       
        this.initialize = function() {
            $q.all([
                this.getUrl(),
                this.getConnectionStatus(),
                this.hasRight(),
                this.getAdminUrl()
            ]).then(function() {
                model.loading = false;
                model.buildPortalButton();
            }).catch(function(error) {
                $log.error('PortalButtonModel.initialize:error', error); 
            });
        }
        this.initialize();
    }
    
    PortalButtonController.$inject = [
        '$scope',
        '$window',
        'PortalButtonModel'
    ];
    function PortalButtonController(
            $scope,
            $window,
            model) {
        
        angular.extend($scope, {
            error: undefined,
            model,
            openWindow() {
                if (model.link && (!model.disconnected || model.adminPrivilege)) {
                    $window.open(model.link, 'Patient Portal', 'height=700, width=1027');
                } else {
                    alert(model.title);
                }
            }
        });
    }
    
    PortalButtonDirective.$inject = [
        'globals'
    ]
    function PortalButtonDirective(globals) {
        return {
            templateUrl: `${globals.contextPath}/well/integration/portal/portal.controller.html`
        };
    }
})();