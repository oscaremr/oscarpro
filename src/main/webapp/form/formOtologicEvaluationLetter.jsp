<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ page import="oscar.util.*, oscar.form.*, oscar.form.data.*"%>
<%@ page import="org.apache.commons.lang.StringUtils"%>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>

    
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.*"%>
<%@ page import="oscar.eform.data.DatabaseAP"%>
<%@ page import="oscar.eform.EFormLoader"%>
<%@ page import="oscar.eform.EFormUtil" %>
<%@ page import="org.oscarehr.common.model.UserProperty" %>
<%@ page import="org.oscarehr.common.dao.UserPropertyDAO" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">

<title>Otology Evaluation Letter</title>

<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('select').change(function() {
		var name = $(this).attr('name');
		$('input[name="other_' + name + '"]').hide();
		changeSelectStyle(this);
	});
	$('select').each(function() {
		var name = $(this).attr('name');
		$('input[name="other_' + name + '"]').hide();
		changeSelectStyle(this);
	});
	function changeSelectStyle(obj) {
		var name = $(obj).attr('name');
		var width = $(obj).find("option:selected").attr('width');
		if (width) {
			$(obj).width(width);
		} else {
			$(obj).removeAttr('style');
		}
		if ($(obj).find("option:selected").text().trim().toLowerCase().indexOf('other') > -1) {
			$('input[name="other_' + name + '"]').show();
		}
	}
});
$(window).load(function() {
	if (window.location.href.indexOf("efmshowform_data.jsp") > -1) {
		return;
	}
	$.ajax( {
		type : "post",
		url : "<%=request.getContextPath() %>/form/formname.do",
		data : {
			submit : 'saveFormLetter',
			demographic_no : '<%=request.getParameter("demographic_no") %>',
			letterHtml : '<!DOCTYPE html><html xmlns="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">'
				+ $('html').html() + '</html>',
			letterEformName : 'Otology Evaluation Letter',
			form_class : 'OtologicEvaluation',
			formId : '<%=request.getParameter("formId") %>',
		},
		async : true,
		dataType : "text",
		success : function(ret) {
			if (ret.trim() == 'ok') {
				
			}
		},
		error : function() {
		}
	});
});
</script>

<style>
body{
font-family:"Arial", verdana;
font-size:14px;
line-height: 24px;
}

input{border:none; border-bottom: 1px #ccc solid;}

#content{

}

#content-inner{
width:700px;
text-align:left;
padding:10px;
border:thin solid #ccc;
}


   .container-controls {
        position:fixed;
	width:100%;
	bottom:0px;
	left:0px;
	padding:4px;
	padding-bottom:8px;
   }

   .controls {
       position:relative;
       color:White;
       z-index:5;

   }

   .background {
       position:absolute;
       top:0px;
       left:0px;
       width:100%;
       height:100%;
       background-color:Black;
       z-index:1;
       /* These three lines are for transparency in all browsers. */
       -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
       filter: alpha(opacity=50);
       opacity:.5;
   }



.button-slim:hover {
cursor: hand; cursor: pointer;
}



.button-save{
	background:#5CCD00;
	background:-moz-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-webkit-gradient(linear,left top,left bottom,color-stop(0%,#5CCD00),color-stop(100%,#4AA400));
	background:-webkit-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-o-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-ms-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#5CCD00',endColorstr='#4AA400',GradientType=0);
	padding:6px 10px;
	color:#fff;
	font-family:'Helvetica Neue',sans-serif;
	font-size:12px;
	border-radius:5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border:1px solid #347400;
}

.button-save:hover{
cursor: hand; cursor: pointer;
}

.top-link{
color:#fff;
text-decoration:none;
padding-left:10px;
}

.top-link:hover{
text-decoration:underline;
cursor: hand; cursor: pointer;
}
u00E2u20ACu2039
</style>
<style> .left {
  float: left;
  width: 125px;
  text-align: right;
  margin: 2px 10px;
  display: inline;
}

.right {
  float: left;
  text-align: left;
  margin: 2px 10px;
  display: inline;
}
.nav_button {
    display: inline;
}</style>

<style type="text/css" media="print">
.DoNotPrint {display: none;}
input {font-size:14px;border: none;}
select {border: none;}

/*dont print placeholders*/
::-webkit-input-placeholder { /* WebKit browsers */
      color: transparent;
}
:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
color: transparent;
}
::-moz-placeholder { /* Mozilla Firefox 19+ */
color: transparent;
}
:-ms-input-placeholder { /* Internet Explorer 10+ */
color: transparent;
}
/*dont print placeholders*/

#content-inner{border:0px}
</style>
</head>
<%!
	private String replaceAllFields(HttpServletRequest request, String sql) {
		sql = DatabaseAP.parserReplace("demographic", request.getParameter("demographic_no"), sql);
		return sql;
	}
%>
<%!
	private String getDatabaseAPValue(HttpServletRequest request,String apName) {
		DatabaseAP ap = EFormLoader.getInstance().getAP(apName);
		if (ap == null) {
			return "";
		}
		String sql = ap.getApSQL();
		String output = ap.getApOutput();
		if (!StringUtils.isBlank(sql)) {
			sql = replaceAllFields(request,sql);
			ArrayList<String> names = DatabaseAP.parserGetNames(output); // a list of ${apName} --> apName
			sql = DatabaseAP.parserClean(sql); // replaces all other ${apName} expressions with 'apName'
			ArrayList<String> values = EFormUtil.getValues(names, sql);
			if (values.size() != names.size()) {
				output = "";
			} else {
				for (int i = 0; i < names.size(); i++) {
					output = DatabaseAP.parserReplace( names.get(i), values.get(i), output);
				}
			}
		}
		return output;
	}
%>
<%
    String formClass = "OtologicEvaluation";
    String formLink = "formOtologicEvaluation.jsp";

    UserPropertyDAO userPropertyDAO = SpringUtils.getBean(UserPropertyDAO.class);
    int demoNo = Integer.parseInt(request.getParameter("demographic_no"));
    int formId = Integer.parseInt(request.getParameter("formId"));
    FrmRecord rec = (new FrmRecordFactory()).factory(formClass);
    java.util.Properties props = rec.getFormRecord(LoggedInInfo.getLoggedInInfoFromSession(request),demoNo, formId);
    UserProperty prop = userPropertyDAO.getProp((String) session.getAttribute("user"), UserProperty.PROVIDER_CONSULT_SIGNATURE);
%>
<%!
	private String joinMultipleValueForSymptoms(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty("symNone").indexOf("checked") > -1) {
			sb.append("[None]");
		}
		
		if (props.getProperty("symHearLoss").indexOf("checked") > -1) {
			if (props.getProperty("symHearLossLeft").indexOf("checked") > -1) {
				sb.append("[Hearing loss,Left]");
				sb.append("&nbsp;&nbsp;");
			}
			if (props.getProperty("symHearLossRight").indexOf("checked") > -1) {
				sb.append("[Hearing loss,Right]");
			}
			sb.append(",");
		}
		if (props.getProperty("symOtorrhea").indexOf("checked") > -1) {
			if (props.getProperty("symOtorrheaLeft").indexOf("checked") > -1) {
				sb.append("[Otorrhea,left]");
				sb.append("&nbsp;&nbsp;");
			}
			if (props.getProperty("symOtorrheaRight").indexOf("checked") > -1) {
				sb.append("[Otorrhea,right]");
			}
			sb.append(",");
		}
		if (props.getProperty("symOtalgia").indexOf("checked") > -1) {
			if (props.getProperty("symOtalgiaLeft").indexOf("checked") > -1) {
				sb.append("[Otalgia,left]");
				sb.append("&nbsp;&nbsp;");
			}
			if (props.getProperty("symOtalgiaRight").indexOf("checked") > -1) {
				sb.append("[Otalgia,right]");
			}
			sb.append(",");
		}		
		if (props.getProperty("symTinnitus").indexOf("checked") > -1) {
			if (props.getProperty("symTinnitusLeft").indexOf("checked") > -1) {
				sb.append("[Tinnitus,left]");
				sb.append("&nbsp;&nbsp;");
			}
			if (props.getProperty("symTinnitusRight").indexOf("checked") > -1) {
				sb.append("[Tinnitus,right]");
			}
			sb.append(",");
		}
		if (props.getProperty("symPulsatile").indexOf("checked") > -1) {
			sb.append("[Pulsatile]");
		}
		if (props.getProperty("symVertigo").indexOf("checked") > -1) {
			sb.append("[Vertigo]");
		}		
		return sb.toString();		
	}
%>
<%!
	private String joinMultipleValueForSeptalDeviation(java.util.Properties props) {
		final String namePrefix = "noseSeptalDeviation";
		StringBuffer sb = new StringBuffer();
		List<String> values = new ArrayList<String>();
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.startsWith(namePrefix) && value.indexOf("checked") > -1) {
				values.add(key.substring(namePrefix.length()));
			}
		}
		return StringUtils.join(values,",");
	}
%>
<%!
	private String joinMultipleValueForRFR(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty("rfrHearLoss").indexOf("checked") > -1) {
			sb.append("[Hear Loss]");
		}
		if (props.getProperty("rfrVertigo").indexOf("checked") > -1) {
			sb.append("[Vertigo]");
		}
		if (props.getProperty("rfrTinnitus").indexOf("checked") > -1) {
			sb.append("[Tinnitus]");
		}
		if (props.getProperty("rfrOtitis").indexOf("checked") > -1) {
			sb.append("[Otitis]");
		}
		if (props.getProperty("rfrOther").indexOf("checked") > -1) {
			sb.append("["+props.getProperty("inferiorTurbinatesText")+"]");
		}
		
		return sb.toString();
	}
%>
<%!
	private String joinMultipleValueForAudioLeft(java.util.Properties props) {
		final String namePrefix = "audioLeft";
		StringBuffer sb = new StringBuffer();
		List<String> values = new ArrayList<String>();
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.startsWith(namePrefix) && value.indexOf("checked") > -1) {
				values.add(key.substring(namePrefix.length()));
			}
		}
		return StringUtils.join(values,",");
	}
%>
<%!
	private String joinMultipleValueForAudioRight(java.util.Properties props) {
		final String namePrefix = "audioRight";
		StringBuffer sb = new StringBuffer();
		List<String> values = new ArrayList<String>();
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.startsWith(namePrefix) && value.indexOf("checked") > -1) {
				values.add(key.substring(namePrefix.length()));
			}
		}
		return StringUtils.join(values,",");
	}
%>
<%!
	private String joinMultipleValueForABR(java.util.Properties props) {
		final String namePrefix = "abr";
		StringBuffer sb = new StringBuffer();
		List<String> values = new ArrayList<String>();
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.startsWith(namePrefix) && value.indexOf("checked") > -1) {
				String postFix = key.substring(namePrefix.length());
				if (postFix.indexOf("WNL")>-1) {
					values.add("[An ABR was within normal limits]");
				} else if (postFix.indexOf("NA")>-1) {
					values.add("[An ABR is N/A]");
				} else if (postFix.indexOf("NYD")>-1) {
					values.add("[An ABR is not yet done]");
				} else if (postFix.indexOf("Other")>-1) {
					values.add("["+props.getProperty("abrOtherText")+"]");
				}
			}
		}
		return StringUtils.join(values,",");
	}
%>
<%!
	private String joinMultipleValueForMRI(java.util.Properties props) {
		final String namePrefix = "mri";
		StringBuffer sb = new StringBuffer();
		List<String> values = new ArrayList<String>();
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.startsWith(namePrefix) && value.indexOf("checked") > -1) {
				String postFix = key.substring(namePrefix.length());
				if (postFix.indexOf("WNL")>-1) {
					values.add("[was within normal limits]");
				} else if (postFix.indexOf("NA")>-1) {
					values.add("[is N/A]");
				} else if (postFix.indexOf("NYD")>-1) {
					values.add("[is not yet done]");
				} else if (postFix.indexOf("Other")>-1) {
					values.add("["+props.getProperty("abrOtherText")+"]");
				}
			}
		}
		return StringUtils.join(values,",");
	}
%>
<%!
	private String joinMultipleValueForInferiorTurbinates(java.util.Properties props) {
		List<String> values = new ArrayList<String>();
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals("inferiorTurbinatesNormal") && value.indexOf("checked") > -1) {
				values.add("Normal");
			}
			if (key.equals("inferiorTurbinatesHypertrophy") && value.indexOf("checked") > -1) {
				values.add("Hypertrophy");
			}
			if (key.equals("inferiorTurbinatesPallor") && value.indexOf("checked") > -1) {
				values.add("mucosal pallor");
			}
			if (key.equals("inferiorTurbinatesOther") && value.indexOf("checked") > -1) {
				values.add(props.getProperty("inferiorTurbinatesText"));
			}
		}
		return StringUtils.join(values,",");
	}
%>
<%!
	private void outputCheckedValue(java.util.Properties props,List<String> list,String name,String defValue) {
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals(name) && value.indexOf("checked") > -1) {
				list.add(defValue);
			}
		}
	}
%>
<%!
	private String joinMultipleValueForEndoscopy(java.util.Properties props) {
		List<String> values = new ArrayList<String>();
		outputCheckedValue(props,values,"endoscopyPolyps","Polyps");
		values.add(" Grade " + props.getProperty("endoscopyGradeText") + "/4");
		outputCheckedValue(props,values,"endoscopyPus","Pus");
		outputCheckedValue(props,values,"endoscopyNasopharynx","Nasopharynx");
		values.add(" " + props.getProperty("endoscopyNasopharynxText"));
		String text = StringUtils.join(values,",");
		text = text.substring(1);
		return text;
	}
%>
<%!
	private String joinMultipleValueForLarynx(java.util.Properties props) {
		List<String> values = new ArrayList<String>();
		outputCheckedValue(props,values,"larynxWNL","WNL");
		outputCheckedValue(props,values,"larynxRVCP","RVCP");
		outputCheckedValue(props,values,"larynxLVCP","LVCP");
		outputCheckedValue(props,values,"larynxOther","Other");
		values.add(" " + props.getProperty("larynxText"));
		String text = StringUtils.join(values,",");
		text = text.substring(1);
		return text;
	}
%>

<%!
	private String outputText(java.util.Properties props,String name) {
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals(name) && value.trim().length() > 0) {
				return value;
			}
		}
		return "";
	}
%>
<%!
	private String joinMultipleValueForRecentTreatment(java.util.Properties props) {
		List<String> values = new ArrayList<String>();
		outputCheckedValue(props,values,"recentTreatmentSerc","[Serc]");
		outputCheckedValue(props,values,"recentTreatmentAbx","[Abx]");
		values.add(outputText(props,"recentTreatmentAbxText"));
		outputCheckedValue(props,values,"recentTreatmentOther","[Other]");
		values.add(outputText(props,"recentTreatmentOtherText"));
		String text = StringUtils.join(values,",");
		text = text.substring(1);
		return text;
	}
%>

<%!
	private String joinMultipleValueForImpression(java.util.Properties props) {
		List<String> values = new ArrayList<String>();
		outputCheckedValue(props,values,"impressionPresbycusis","Presbycusis:");
		outputCheckedValue(props,values,"impressionOtosclerosis","[Otosclerosis]");
		outputCheckedValue(props,values,"impressionCEOE","[CEOE]");
		outputCheckedValue(props,values,"impressionOtitisExterna",";Otitis Externa:");
		outputCheckedValue(props,values,"impressionOME","[OME]");
		outputCheckedValue(props,values,"impressioinOther","[Other]");
		values.add(outputText(props,"impressionOtherText"));
		String text = StringUtils.join(values,",");
		return text;
	}
%>

<%!
	private String joinMultipleValueForOtologicSurgery(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		sb.append("is");
		if (props.getProperty("priorOtologicSurgeryYes").indexOf("checked") > -1) {
			
			sb.append(" ["+props.getProperty("priorOtologicSurgeryText")+"] ");
		}
		if (props.getProperty("priorOtologicSurgeryNo").indexOf("checked") > -1) {
			sb.append(" [no]");
		}
		
		return sb.toString();
	}
%>

<%!
	private String joinMultipleValueForFamilyHistory(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty("familyHistoryHYes").indexOf("checked") > -1) {
			sb.append("[is]");
		}
		if (props.getProperty("familyHistoryHlNo").indexOf("checked") > -1) {
			sb.append("is [no]");
		}
		
		return sb.toString();
	}
%>

<%!
	private String joinMultipleValueForHistoryOfNoise(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty("historyOfNoiseYes").indexOf("checked") > -1) {
			sb.append("is");
			sb.append(" ["+props.getProperty("historyOfNoiseText")+"] ");
		}
		if (props.getProperty("historyOfNoiseNo").indexOf("checked") > -1) {
			sb.append("is [no]");
		}
		
		return sb.toString();
	}
%>

<%!
	private String joinMultipleValueForHistoryOfOtotoxic(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty("historyOfOtotoxicYes").indexOf("checked") > -1) {
			sb.append("is");
			sb.append(" ["+props.getProperty("historyOfOtotoxicText")+"] ");
		}
		if (props.getProperty("historyOfOtotoxicNo").indexOf("checked") > -1) {
			sb.append("is [no]");
		}
		
		return sb.toString();
	}
%>

<%!
	private String joinMultipleValueForHearingAids(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty("usesHearingYes").indexOf("checked") > -1) {
			sb.append("[is]");
		}
		if (props.getProperty("usesHearingNo").indexOf("checked") > -1) {
			sb.append(" is [no]");
		}
		
		return sb.toString();
	}
%>

<%!
/*	private String getSingleValue(Properties props,String radioName,String otherTextName) {
		String value = props.getProperty(radioName,"");
		if (value.indexOf("text") > -1) {
			return props.getProperty(otherTextName,"");
		} else {
			return value;
		}
	}
*/
%>


<%!
	private String getSingleValue(Properties props, String radioName, String value) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty(radioName).indexOf("checked") > -1) {
			if (radioName.indexOf("WNL")>-1) {
			 	sb.append("[within normal limits]");
			} else {
				sb.append("["+value+"]");
			}
		}
		
		return sb.toString();
	}
%>

<%!
	private String getOtherText(Properties props,String otherRadioName,String otherText) {
		StringBuffer sb = new StringBuffer();
		
		if (props.getProperty(otherRadioName).indexOf("checked") > -1) {
			sb.append("["+props.getProperty(otherText,"")+"]");
		}
		
		return sb.toString();
	}
%>
<body>
<div id="content">    
<center id="Top">
<div id="content-inner">

<p align="center">
<strong><span style="font-size:22px">Dr. Shaun Kilty</span><br>
MD, FRCSC<br>
Otolaryngology, Head &amp; Neck Surgery<br></strong>
<span style="font-size:14px">459 - 737 Parkdale Ave, Ottawa, Ontario K1Y 1J8<br>
Tel: (613)798-5555 xt. 18514<br>
Fax: (613)729-2412</span>
<br/>
<span><a href="http://www.entotologycare.com" target="_blank">www.entotologycare.com</a></span>
</p>

<form method="post" action="#" name="form-EMR Letters" id="form-consult1">
	<input type="text" name="today" id="today" placeholder="YYYY-MM-DD" style="border:0px;margin-top:40px;" value="<%=getDatabaseAPValue(request,"today") %>" oscardb=today>

	<div id="referring-doctor" style="margin-top:30px">
	<input type="text" name="referring-doctor" id="referring-doctor" value="<%=getDatabaseAPValue(request,"referral_name") %>" placeholder="Referring Doctor" style="border:0px;" name="referral-name" oscardb=referral_name><br>
	<input type="text" name="referring-doctor-address" id="referring-doctor-address" value="<%=getDatabaseAPValue(request,"referral_address") %>" placeholder="Referring Doctor Address" style="border:0px;width:400px" name="referral-address" oscardb=referral_address><br>
	<input type="text" name="referring-doctor-phone" id="referring-doctor-phone" value="<%=getDatabaseAPValue(request,"referral_phone") %>" placeholder="Phone" style="border:0px;width:200px" name="referral-phone" oscardb=referral_phone><br>
	<input type="text" name="referring-doctor-fax" id="referring-doctor-fax" value="<%=getDatabaseAPValue(request,"referral_fax") %>" placeholder="Fax" style="border:0px;width:200px" name="referral-fax" oscardb=referral_fax><br>
	</div>
        

	<p>Dear Dr. <input type="text" placeholder="Doctor Name" name="referring-doctor-dear" style="border:0px;" value="<%=getDatabaseAPValue(request,"referral_name") %>" oscardb=referral_name><br></p>
	<p>RE: <input type="text" placeholder="Patient Name" style="border:0px;width:160px" name="patient-name-re" value="<%=getDatabaseAPValue(request,"first_last_name") %>" oscardb=first_last_name> <span style="padding-left:20px">
	D.O.B: <input type="text" name="dob" id="dob" placeholder="YYYY-MM-DD" style="border:0px;margin-top:5px;" value="<%=getDatabaseAPValue(request,"dobc2") %>" oscardb=dobc2></span> </p>


 


<p style="margin: 0px;">Thank you for this referral. This patient was seen for <%=joinMultipleValueForRFR(props) %>.<br>
The current otologic symptoms are: <p style="margin:0px;margin-left: 30px;"><%=joinMultipleValueForSymptoms(props) %>.
There <%=joinMultipleValueForOtologicSurgery(props)%> prior otologic surgery. 
There <%=joinMultipleValueForFamilyHistory(props)%> family history of early onset hearing loss. 
There <%=joinMultipleValueForHistoryOfNoise(props)%> history of significant noise exposure.
There <%=joinMultipleValueForHistoryOfOtotoxic(props)%> history of ototoxic medication use. 
<%=joinMultipleValueForHearingAids(props)%> aids are currently used.<br>
<strong>Recent Treatment:</strong><br> 
Symptoms improved (+) or No Symptom Improvement (-) is <input type="text" style="width: 280px;" value="<%=joinMultipleValueForRecentTreatment(props) %>"> 

</p>

<p><strong>Examination:</strong></p>

<!-- <p style="margin: 0px;">Examination by otoscopy revealed the left tympanic membrance  -->
<%-- <input type="text" value="<%=getSingleValue(props,"ears-as","ears-as-text1") %>" />. --%>
<!-- The right tympanic membrane  -->
<%-- <input type="text" value="<%=getSingleValue(props,"ears-ad","ears-ad-text1") %>" />.The facial nerve function was  --%>
<%-- <input type="text" value="<%=getSingleValue(props,"facialNerveDeficit","facialNerveDeficit-text1") %>" />. --%>
<%-- The Dix Hallpike showed<input type="text" value="<%=getSingleValue(props,"dixHallpike","dixHallpike-text1") %>" />. --%>
<!-- The audiogram showed right  -->
<%-- <input type="text" value="<%=getSingleValue(props,"audioRight","") %>" />. --%>
<%-- MRI was <input type="text" value="<%=getSingleValue(props,"mri","mri-other-text") %>" />. --%>
<%-- An ABR was <input type="text" value="<%=getSingleValue(props,"abr","abr-other-text") %>" />. --%>
<!-- </p> -->


<span class="nav_button"><strong>Ears AS: </strong></span>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"earsAsWNL","With in Normal Limits") %>" /></span><br>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"earsAsOME","OME") %>" /></span><br>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"earsAsPerforation","Perforation") %>" /></span><br>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"earsAsTIPP","TIPP") %>" /></span><br>
<span class="nav_button"><input type="text" value="<%=getOtherText(props,"earsAsOther","earsAsOtherText")%>" /></span><br>

<span class="nav_button"><strong>Ears AD: </strong></span>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"earsAdWNL","[With in Normal Limits]") %>" /></span><br>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"earsAdOME","OME") %>" /></span><br>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"earsAdPerforation","Perforation") %>" /></span><br>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"earsAdTIPP","TIPP") %>" /></span><br>
<span class="nav_button"><input type="text" style="width: 300px;" value="<%=getOtherText(props,"earsAdOther","earsAdOtherText")%>" /></span><br>

<span class="nav_button"><strong>Facial Nerve Deficit:</strong> </span>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"facialNerveDeficitNone","The facial nerve function was ") %>" /></span><br>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"facialNerveDeficitL","Left") %>" /></span><br>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"facialNerveDeficitR","Right") %>" /></span><br>
<span class="nav_button"><input type="text" style="width: 300px;" value="[House-Brackmann Grade:<%=props.getProperty("facialNerveDeficitText","") %>]" /></span><br>

<span class="nav_button"><strong>Cranial nerve screen:</strong> </span>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"cranialNerveScreenWNL","[With in Normal Limits]") %>" /></span><br>
<span class="nav_button"><input type="text" style="width: 300px;" value="<%=getOtherText(props,"cranialNerveScreenOther","cranialNerveScreenOtherText")%>" /></span><br>
<span class="nav_button"><strong>Dix-Hallpike:</strong> </span>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"dixHallpikeNotDone","Not Done") %>" /></span><br>
<span class="nav_button"><input type="text" style="width: 320px;" value="<%=getSingleValue(props,"dixHallpikeNoNorV","The The Dix Hallpike showed no nystagmus or vertigo") %>" /></span><br>
<span class="nav_button"><input type="text" style="width: 300px;" value="<%=getSingleValue(props,"dixHallpikeLeftCCW","The Dix Hallpike showed left CCW nystagmus ") %>" /></span><br>
<span class="nav_button"><input type="text" style="width: 300px;" value="<%=getSingleValue(props,"dixHallpikeRightCCW","The Dix Hallpike showed right CCW nystagmus") %>" /></span><br>
<span class="nav_button"><input type="text" style="width: 300px;" value="<%=getOtherText(props,"dixHallpikeOther","dixHallpikeOtherText")%>" /></span><br>
<span class="nav_button"><strong>Oral: </strong></span>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"oralWNL","[With in Normal Limits]") %>" /></span><br>
<span class="nav_button"><input type="text" style="width: 300px;" value="<%=getOtherText(props,"oralOther","oralOtherText")%>" /></span><br>
<span class="nav_button"><strong>Neck:</strong></span>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"neckNoAdopathy","No Adopathy") %>" /></span><br>
<span class="nav_button"><input type="text" style="width: 300px;" value="<%=getOtherText(props,"neckOther","neckOtherText")%>" /></span><br>
<strong>Nose: Septal Deviation: </strong><input type="text" style="width: 300px;" value="<%= joinMultipleValueForSeptalDeviation(props) %>" /><br>
<strong>Inferior Turbinates: </strong><input type="text" style="width: 300px;" value="<%= joinMultipleValueForInferiorTurbinates(props) %>" />
<br>
<strong>Endoscopy:</strong><input type="text" style="width: 500px;" value="<%= joinMultipleValueForEndoscopy(props) %>">
<br>
<strong>Larynx:</strong><input type="text" style="width: 500px;" value="<%= joinMultipleValueForLarynx(props) %>"><br>
<strong>Audio Left:</strong> The audiogram showed Left <input type="text" style="width: 400px;"x value="<%=joinMultipleValueForAudioLeft(props) %>">
<br>
<strong>Audio Right:</strong> The audiogram showed Right <input type="text" style="width: 400px;" value="<%=joinMultipleValueForAudioRight(props) %>">
<br>
<strong>MRI:</strong><input type="text" style="width: 500px;" value="<%=joinMultipleValueForMRI(props) %>"><br>
<strong>ABR:</strong><input type="text" style="width: 500px;" value="<%=joinMultipleValueForABR(props) %>"><br>


<p><strong>Impression: <br></strong>
This patient has <input type="text" style="width: 500px;" value="<%=joinMultipleValueForImpression(props) %>"> 
</p>
<div id="planArea">
<p>
	<strong>Plan<br></strong>	
	
	<p class="MsoNormal" id="hearing">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="hearing" <%= props.getProperty("hearing", "") %>/>Hearing Aid Evaluation</i>:&nbsp; </span>
	</p>
	
	<p class="MsoNormal" id="myringotomy">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="myringotomy" <%= props.getProperty("myringotomy", "") %>/>Myringotomy and Tube</i>:&nbsp; </span>
	</p>
	
	<p class="MsoNormal" id="myringotomyLeft">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="myringotomyLeft" <%= props.getProperty("myringotomyLeft", "") %>/>Myringotomy and Tube Left</i>:&nbsp; </span>
	</p>
	
	<p class="MsoNormal" id="myringotomyRight">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="myringotomyRight" <%= props.getProperty("myringotomyRight", "") %>/>Myringotomy and Tube Right</i>:&nbsp; </span>
	</p>
	
	<p class="MsoNormal" id="myringotomyBilateral">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="myringotomyBilateral" <%= props.getProperty("myringotomyBilateral", "") %>/>Myringotomy and Tube Bilateral</i>:&nbsp; </span>
	</p>
	
	<p class="MsoNormal" id="abrPlan">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="abrPlan" <%= props.getProperty("abrPlan", "") %>/>ABR</i>:&nbsp; </span>
	</p>
	
	<p class="MsoNormal" id="mriPlan">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="mriPlan" <%= props.getProperty("mriPlan", "") %>/>MRI</i>:&nbsp; </span>
	</p>
	
	<p class="MsoNormal" id="ctTemporal">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="ctTemporal" <%= props.getProperty("ctTemporal", "") %>/>CT Temporal Bones and follow-up after the scan</i>:&nbsp; </span>
	</p>
	
	<p class="MsoNormal" id="repeatAduio">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="repeatAduio" <%= props.getProperty("repeatAduio", "") %>/>Repeat Audio in</i>:&nbsp; 
	</span>
	</p>
	
	<p class="MsoNormal" id="repeatAduioText">
	<span style="font-family: Arial">
	<i><input class="underline" style="width: 30px;" type="text" name="repeatAduioText" value="<%= props.getProperty("repeatAduioText", "") %>" /> months &nbsp; </i>
	</span>
	</p>
	
	<p class="MsoNormal" id="requiringRepeat">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="requiringRepeat" <%= props.getProperty("requiringRepeat", "") %>/>Requiring repeat consultation request</i>:&nbsp; 
	</span>
	</p>
	
	<p class="MsoNormal" id="surgery">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="surgery" <%= props.getProperty("surgery", "") %>/>Surgery</i>:&nbsp; 
	</span>
	</p>
	
	<p class="MsoNormal" id="surgeryText">
	<span style="font-family: Arial">
	<i><input class="underline" style="width: 30px;" type="text" name="surgeryText" value="<%= props.getProperty("surgeryText", "") %>" /> </i>
	</span>
	</p>
	
	<p class="MsoNormal" id="consult">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="consult" <%= props.getProperty("consult", "") %>/>Consult</i>:&nbsp; 
	</span>
	</p>
	
	<p class="MsoNormal" id="ciprodex">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="ciprodex" <%= props.getProperty("ciprodex", "") %>/>Ciprodex otic drops: 3 drops in affected ear(s) BID for</i>:&nbsp; 
	</span>
	</p>
	
	<p class="MsoNormal" id="ciprodexText">
	<span style="font-family: Arial">
	<i><input class="underline" style="width: 30px;" type="text" name="ciprodexText" value="<%= props.getProperty("ciprodexText", "") %>" /> days</i>
	</span>
	</p>
	
	<p class="MsoNormal" id="engVertigoEvaluation">
	<span style="font-family: Arial">
	<i><input type="checkbox" name="engVertigoEvaluation" <%= props.getProperty("engVertigoEvaluation", "") %>/>ENG for vertigo evaluation</i>
	</span>
	</p>


<span class="MsoNormal" id="planOther">
		<span style="font-family: Arial"> 
			<input type="checkbox" name="planOther" <%= props.getProperty("planOther", "") %>/>
			<textarea name="planOtherText" style="border: 1px solid #000;width: 90%"><%= props.getProperty("planOtherText", "") %></textarea>
		</span>
	</span>

</div>

</div>
	
<p><span style="font-size: 11.0pt; line-height: 115%; font-family: Calibri">&nbsp;</span>Sincerely,</p>	
<p>
	<% if (prop != null && StringUtils.isNotEmpty(prop.getValue())) { %>
	<img src="<%= request.getContextPath() %>/eform/displayImage.do?imagefile=<%= prop.getValue() %>"/>
	<% } %>
</p>
<p><span style="font-size: 11.0pt; line-height: 115%; font-family: Calibri">&nbsp;</span>Dr. Shaun Kilty MD, FRCSC</p>
<!--<p><%= props.getProperty("signatures", "") %></p>-->

</center>
</div><!--content-->

<br><br><br>


<div class="container-controls DoNotPrint">
   <div class="controls">
<input value="Print" class="button-slim" name="PrintButton" type="button" onclick="window.print();" title="Print - HTML print out">


<a href="#Top" class="top-link"><strong>Top</strong></a>
   </div>

   <div class="background"></div>
</div>

</form>
<script type="text/javascript">
jQuery(window).load(function() {
	var plan_array = [
    	{ 
    		module:'plan',
    		options:['hearing','myringotomy','myringotomyLeft','myringotomyRight','myringotomyBilateral',
    		         'abrPlan','mriPlan','ctTemporal','repeatAduio','repeatAduioText','requiringRepeat','surgery',
    		         'surgeryText','consult','ciprodex','ciprodexText','planOther','planOtherText'
    		         ]
    	},
    	
    ];
    var totalCount = 0;
    for(var i=0;i<plan_array.length;i++) {
        var mod = plan_array[i];
        var modCount = 0 ;
        var opts = mod.options;
        for(var j=0;j<opts.length;j++) {
            var obj = $('#planArea input[name="' + opts[j] + '"]');
            if (obj.is(":checked")) {
                modCount += 1;
                totalCount += 1;
            } else {
                $('#' + opts[j]).hide();
            }
        }
        if (modCount == 0) {
            $('#' + mod.module).hide();
        }
    }
    if (totalCount == 0) {
        $('#planArea').hide();
    }
});
</script>
</body>
</html>