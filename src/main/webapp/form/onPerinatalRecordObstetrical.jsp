<%@ page import="oscar.form.FormOnPerinatalUtils" %>
<%
    int ohNum = request.getParameter("ohNum") != null ? Integer.parseInt(request.getParameter("ohNum")) : 0;

    java.util.Properties props = (java.util.Properties) request.getAttribute("props");
    
%>

<tr align="center" id="oh_<%=ohNum%>">
    <td>
        <a class="delete_link form-link" href="javascript:void(0)" onclick="deleteObstetricalHistory('<%=ohNum%>'); return false;">[x]</a>&nbsp;<%=ohNum%>
    </td>
    <td>
        <input type="text" name="oh_yearMonth<%=ohNum%>" size="6" maxlength="7" style="width: 90%" 
               placeholder="YYYY/MM" 
               value="<%= FormOnPerinatalUtils.getFormAttribute(props, "oh_yearMonth" + ohNum)%>" />


    </td>
    <td>
        <input type="text" name="oh_place<%=ohNum%>" size="8" maxlength="20" style="width: 80%" 
        value="<%= FormOnPerinatalUtils.getFormAttribute(props, "oh_place" + ohNum) %>" />
    </td>
    <td>
        <input type="text" name="oh_gest<%=ohNum%>" size="3" maxlength="5" style="width: 80%"
        value="<%= FormOnPerinatalUtils.getFormAttribute(props, "oh_gest" + ohNum) %>" />
    </td>
    <td>
        <input type="text" name="oh_length<%=ohNum%>" size="5" maxlength="6" style="width: 80%" 
        value="<%= FormOnPerinatalUtils.getFormAttribute(props, "oh_length" + ohNum) %>"/>
    </td>
    <td>
        <input type="radio" name="oh_birth_type<%=ohNum%>" <%= FormOnPerinatalUtils.setIfChecked(props, "oh_birth_type" + ohNum, "SVB") %> value="SVB" />
        <input type="radio" name="oh_birth_type<%=ohNum%>" <%= FormOnPerinatalUtils.setIfChecked(props, "oh_birth_type" + ohNum, "CS") %> value="CS" />
        <input type="radio" name="oh_birth_type<%=ohNum%>" <%= FormOnPerinatalUtils.setIfChecked(props, "oh_birth_type" + ohNum, "Assisted") %> value="Assisted" />
    </td>
    <td align="left">
        <input type="text" name="oh_comments<%=ohNum%>" size="20" maxlength="80" style="width: 100%"  
        value="<%= FormOnPerinatalUtils.getFormAttribute(props, "oh_comments" + ohNum) %>"/>
    </td>
    <td>
        <input type="text" name="oh_sex<%=ohNum%>" size="2" maxlength="1" style="width: 50%" 
        value="<%= FormOnPerinatalUtils.getFormAttribute(props, "oh_sex" + ohNum) %>"/>
    </td>
    <td>
        <input type="text" name="oh_weight<%=ohNum%>" size="5" maxlength="6" style="width: 80%" 
        value="<%= FormOnPerinatalUtils.getFormAttribute(props, "oh_weight" + ohNum) %>"/>
    </td>
    <td>
        <input type="text" name="oh_breastfed<%=ohNum%>" size="5" maxlength="10" style="width: 80%" 
        value="<%= FormOnPerinatalUtils.getFormAttribute(props, "oh_breastfed" + ohNum) %>"/>
    </td>
    <td>
        <input type="text" name="oh_health<%=ohNum%>" size="5" maxlength="10" style="width: 80%" 
        value="<%= FormOnPerinatalUtils.getFormAttribute(props, "oh_health" + ohNum) %>"/>
    </td>
</tr>
