<%@ page import="oscar.form.FormOnPerinatalUtils" %>
<%
    int usNum = request.getParameter("usNum") != null ? Integer.parseInt(request.getParameter("usNum")) : 0;
    java.util.Properties props = (java.util.Properties) request.getAttribute("props");
%>

<tr id="us_<%=usNum%>" class="us">

    <td style="min-width: 155px;">
        <input type="text" id="us_date<%=usNum%>" name="us_date<%=usNum%>" class="spe" ondblclick="calToday(this)" size="10" maxlength="10" placeholder="YYYY/MM/DD" value="<%= FormOnPerinatalUtils.getFormAttribute(props, "us_date" + usNum) %>" />

        <img src="../images/cal.gif" id="us_date<%=usNum%>_cal" />
    </td>

    <td>
        <input type="text" id="us_ga<%=usNum%>" name="us_ga<%=usNum%>" class="spe" ondblclick="getGestationalAge(this)" size="5" maxlength="10" 
               value="<%= FormOnPerinatalUtils.getFormAttribute(props, "us_ga" + usNum) %>"/>
    </td>
    <% if (usNum == 3) { %>
    <td class="ultrasound-3">
        <input type="text" id="us_result<%=usNum%>_as" name="us_result<%=usNum%>_as" size="32" maxlength="50" placeholder="Anatomy scan (between 18-22 wks)" value="<%= FormOnPerinatalUtils.getFormAttribute(props, "us_result" + usNum+"_as") %>" />
        <input type="text" id="us_result<%=usNum%>_pl" name="us_result<%=usNum%>_pl" size="32" maxlength="50" placeholder="Placental Location" 
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "us_result" + usNum + "_pl") %>"/>
        <input type="text" id="us_result<%=usNum%>_sm" name="us_result<%=usNum%>_sm" size="32" maxlength="50" placeholder="Soft Markers"
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "us_result" + usNum + "_sm") %>" />
    </td>
    <% } else { %>
    <td>
        <input type="text" id="us_result<%=usNum%>" name="us_result<%=usNum%>" style="width: 100%;" maxlength="99" <%=(usNum == 2) ? "placeholder='NT Ultrasound (between 11-13+6 weeks)'" : ""%> value="<%= FormOnPerinatalUtils.getFormAttribute(props, "us_result" + usNum) %>" />
    </td>
    <% }%>
</tr>