<%@ page import="oscar.util.*, oscar.form.*, oscar.form.data.*"%>
<%@ page import="org.apache.commons.lang.StringUtils"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>


<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.*"%>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="oscar.eform.data.DatabaseAP"%>
<%@ page import="oscar.eform.EFormLoader"%>
<%@ page import="oscar.eform.EFormUtil" %>
<%@ page import="org.oscarehr.common.model.UserProperty" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.dao.UserPropertyDAO" %>
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">

<title>Sinus Evaluation Letter.jsp Letter</title>

<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('select').change(function() {
		var name = $(this).attr('name');
		$('input[name="other_' + name + '"]').hide();
		changeSelectStyle(this);
	});
	$('select').each(function() {
		var name = $(this).attr('name');
		$('input[name="other_' + name + '"]').hide();
		changeSelectStyle(this);
	});
	function changeSelectStyle(obj) {
		var name = $(obj).attr('name');
		var width = $(obj).find("option:selected").attr('width');
		if (width) {
			$(obj).width(width);
		} else {
			$(obj).removeAttr('style');
		}
		if ($(obj).find("option:selected").text().trim().toLowerCase().indexOf('other') > -1) {
			$('input[name="other_' + name + '"]').show();
		}
	}
});
$(window).load(function() {
	if (window.location.href.indexOf("efmshowform_data.jsp") > -1) {
		return;
	}
	$.ajax( {
		type : "post",
		url : "<%=request.getContextPath() %>/form/formname.do",
		data : {
			submit : 'saveFormLetter',
			demographic_no : '<%=request.getParameter("demographic_no") %>',
			letterHtml : '<!DOCTYPE html><html xmlns="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">'
				+ $('html').html() + '</html>',
			letterEformName : 'Sinus Evaluation Letter',
			form_class : 'SinusEvaluation',
			formId : '<%=request.getParameter("formId") %>',
		},
		async : true,
		dataType : "text",
		success : function(ret) {
			if (ret.trim() == 'ok') {
				
			}
		},
		error : function() {
		}
	});
});
</script>

<style>
.plan-other span{
	border: 1px solid #ccc;
	display: inline-block;
	width: 85%;
}
.impression-desc{
	padding-left:30px;
	display:block;
	border-bottom: 1px #ccc solid;
	width: 600px;
	font-size: 14px;
}
.plan-other{
	margin-top:3px;
	display:block;
}
.plan-other *{
	vertical-align: bottom;
}
.plan-other input{
	/*margin-bottom: 0px;*/
}
body{
font-family:"Arial", verdana;
font-size:14px;
line-height: 24px;
}

input{border:none; border-bottom: 1px #ccc solid;}

#content{

}

#content-inner{
width:700px;
text-align:left;
padding:10px;
border:thin solid #ccc;
}


   .container-controls {
        position:fixed;
	width:100%;
	bottom:0px;
	left:0px;
	padding:4px;
	padding-bottom:8px;
   }

   .controls {
       position:relative;
       color:White;
       z-index:5;

   }

   .background {
       position:absolute;
       top:0px;
       left:0px;
       width:100%;
       height:100%;
       background-color:Black;
       z-index:1;
       /* These three lines are for transparency in all browsers. */
       -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
       filter: alpha(opacity=50);
       opacity:.5;
   }



.button-slim:hover {
cursor: hand; cursor: pointer;
}



.button-save{
	background:#5CCD00;
	background:-moz-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-webkit-gradient(linear,left top,left bottom,color-stop(0%,#5CCD00),color-stop(100%,#4AA400));
	background:-webkit-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-o-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-ms-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#5CCD00',endColorstr='#4AA400',GradientType=0);
	padding:6px 10px;
	color:#fff;
	font-family:'Helvetica Neue',sans-serif;
	font-size:12px;
	border-radius:5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border:1px solid #347400;
}

.button-save:hover{
cursor: hand; cursor: pointer;
}

.top-link{
color:#fff;
text-decoration:none;
padding-left:10px;
}

.top-link:hover{
text-decoration:underline;
cursor: hand; cursor: pointer;
}
u00E2u20ACu2039
</style>
<style> .left {
  float: left;
  width: 125px;
  text-align: right;
  margin: 2px 10px;
  display: inline;
}

.right {
  float: left;
  text-align: left;
  margin: 2px 10px;
  display: inline;
}
.nav_button {
    display: inline;
}</style>

<style type="text/css" media="print">
.DoNotPrint {display: none;}
input {font-size:14px;border: none;}
select {border: none;}
.plan-other span{
	border: none;
}
.impression-desc{
	border: none;
}
/*dont print placeholders*/
::-webkit-input-placeholder { /* WebKit browsers */
      color: transparent;
}
:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
color: transparent;
}
::-moz-placeholder { /* Mozilla Firefox 19+ */
color: transparent;
}
:-ms-input-placeholder { /* Internet Explorer 10+ */
color: transparent;
}
/*dont print placeholders*/

#content-inner{border:0px}
</style>
</head>
<%!
	private String replaceAllFields(HttpServletRequest request, String sql) {
		sql = DatabaseAP.parserReplace("demographic", request.getParameter("demographic_no"), sql);
		return sql;
	}
%>
<%!
	private String getRfr(java.util.Properties props) {
		String value = props.getProperty("rfr");
		if (value == null) {
			value = "";
		}
		if (value.equals("Other")) {
			value = props.getProperty("rfr-other-text");
		}
		return value;
	}
%>
<%!
	private String getDatabaseAPValue(HttpServletRequest request,String apName) {
		DatabaseAP ap = EFormLoader.getInstance().getAP(apName);
		if (ap == null) {
			return "";
		}
		String sql = ap.getApSQL();
		String output = ap.getApOutput();
		if (!StringUtils.isBlank(sql)) {
			sql = replaceAllFields(request,sql);
			ArrayList<String> names = DatabaseAP.parserGetNames(output); // a list of ${apName} --> apName
			sql = DatabaseAP.parserClean(sql); // replaces all other ${apName} expressions with 'apName'
			ArrayList<String> values = EFormUtil.getValues(names, sql);
			if (values.size() != names.size()) {
				output = "";
			} else {
				for (int i = 0; i < names.size(); i++) {
					output = DatabaseAP.parserReplace( names.get(i), values.get(i), output);
				}
			}
		}
		return output;
	}
%>
<%
    String formClass = "SinusEvaluation";
    String formLink = "formSinusEvaluation.jsp";

    UserPropertyDAO userPropertyDAO = SpringUtils.getBean(UserPropertyDAO.class);
    int demoNo = Integer.parseInt(request.getParameter("demographic_no"));
    int formId = Integer.parseInt(request.getParameter("formId"));
    FrmRecord rec = (new FrmRecordFactory()).factory(formClass);
    java.util.Properties props = rec.getFormRecord(LoggedInInfo.getLoggedInInfoFromSession(request),demoNo, formId);
    UserProperty prop = userPropertyDAO.getProp((String) session.getAttribute("user"), UserProperty.PROVIDER_CONSULT_SIGNATURE);
%>
<%!
	private String joinMultipleValueForSymptoms(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty("sym-None").indexOf("checked") > -1) {
			sb.append("None<br/>");
		}
		if (props.getProperty("sym-Congestion").indexOf("checked") > -1) {
			sb.append("Congestion<br/>");
		}
		if (props.getProperty("sym-Pain").indexOf("checked") > -1) {
			sb.append("Pain:");
			if (props.getProperty("sym-Midfacial").indexOf("checked") > -1) {
				sb.append("midfacial");
				sb.append("&nbsp;&nbsp;");
			}
			if (props.getProperty("sym-Frontal").indexOf("checked") > -1) {
				sb.append("frontal");
			}
			sb.append("<br/>");
		}
		if (props.getProperty("sym-Obstruction").indexOf("checked") > -1) {
			sb.append("Obstruction:");
			if (props.getProperty("sym-Left").indexOf("checked") > -1) {
				sb.append("left");
				sb.append("&nbsp;&nbsp;");
			}
			if (props.getProperty("sym-Right").indexOf("checked") > -1) {
				sb.append("right");
			}
			sb.append("<br/>");
		}
		if (props.getProperty("sym-Rhinorrhea").indexOf("checked") > -1) {
			sb.append("Rhinorrhea<br/>");
		}
		if (props.getProperty("sym-Smell").indexOf("checked") > -1) {
			sb.append("Smell:");
			if (props.getProperty("sym-Hyposmia").indexOf("checked") > -1) {
				sb.append("hyposmia");
				sb.append("&nbsp;&nbsp;");
			}
			if (props.getProperty("sym-Anosmia").indexOf("checked") > -1) {
				sb.append("anosmia");
			}
			sb.append("<br/>");
		}
		return sb.toString();
		/*
		final String namePrefix = "sym-";
		StringBuffer sb = new StringBuffer();
		List<String> values = new ArrayList<String>();
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.startsWith(namePrefix) && value.indexOf("checked") > -1) {
				values.add(key.split("-")[1]);
			}
		}
		return StringUtils.join(values,",") + ".";
		*/
	}
%>
<%!
	private String joinMultipleValueForSeptalDeviation(java.util.Properties props) {
		final String namePrefix = "septalDeviation";
		StringBuffer sb = new StringBuffer();
		List<String> values = new ArrayList<String>();
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.startsWith(namePrefix) && value.indexOf("checked") > -1) {
				values.add(key.substring(namePrefix.length()));
			}
		}
		return StringUtils.join(values,",");
	}
%>
<%!
	private String joinMultipleValueForInferiorTurbinates(java.util.Properties props) {
		List<String> values = new ArrayList<String>();
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals("inferiorNormal") && value.indexOf("checked") > -1) {
				values.add("Normal");
			}
			if (key.equals("inferiorHypertrophy") && value.indexOf("checked") > -1) {
				values.add("Hypertrophy");
			}
			if (key.equals("inferiorPallor") && value.indexOf("checked") > -1) {
				values.add("mucosal pallor");
			}
			if (key.equals("inferiorOther") && value.indexOf("checked") > -1) {
				values.add(props.getProperty("inferiorOther-text1"));
			}
		}
		return StringUtils.join(values,",");
	}
%>
<%!
	private void outputCheckedValue(java.util.Properties props,List<String> list,String name,String defValue) {
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals(name) && value.indexOf("checked") > -1) {
				list.add(defValue);
			}
		}
	}
%>
<%!
	private String joinMultipleValueForEndoscopy(java.util.Properties props) {
		List<String> values = new ArrayList<String>();
		Iterator keyI = props.keySet().iterator();
		outputCheckedValue(props,values,"endoscopy-polyps","Polyps");
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals("endoscopy-grade-left") && value.indexOf("checked") > -1) {
				values.add("Left grade " + props.getProperty("endoscopy-grade-left-text1") + "/4");
			}
		}
		keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals("endoscopy-grade-right") && value.indexOf("checked") > -1) {
				values.add("Right grade " + props.getProperty("endoscopy-grade-right-text1") + "/4");
			}
		}
		outputCheckedValue(props,values,"endoscopy-inflammation","Inflammation");
		keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals("endoscopy-inflammation-left") && value.indexOf("checked") > -1) {
				values.add("Left grade " + props.getProperty("endoscopy-inflammation-left-text1") + "/4");
			}
		}
		keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals("endoscopy-inflammation-right") && value.indexOf("checked") > -1) {
				values.add("Right grade " + props.getProperty("endoscopy-inflammation-right-text1") + "/4");
			}
		}
		outputCheckedValue(props,values,"endoscopy-pus","Pus");
		outputCheckedValue(props,values,"endoscopy-left","Left");
		outputCheckedValue(props,values,"endoscopy-right","Right");
		outputCheckedValue(props,values,"endoscopy-normal","Normal");
		keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals("endoscopy-other-text") && value.trim().length() > 0) {
				values.add(value);
			}
		}
		return StringUtils.join(values,",");
	}
%>
<%!
	private String outputText(java.util.Properties props,String name) {
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals(name) && value.trim().length() > 0) {
				return value;
			}
		}
		return "";
	}
%>
<%!
	private String joinMultipleValueForImpression(java.util.Properties props) {
		List<String> values = new ArrayList<String>();
		outputCheckedValue(props,values,"impression-Sinusitis","Sinusitis:");
		outputCheckedValue(props,values,"impression-Sinusitis-CRSwNP","CRSwNP");
		outputCheckedValue(props,values,"impression-Sinusitis-CRSsNP","CRSsNP");
		outputCheckedValue(props,values,"impression-Rhinitis",";Rhinitis:");
		outputCheckedValue(props,values,"impression-Rhinitis-Allergic","Allergic");
		if (StringUtils.isNotBlank(outputText(props,"impression-Rhinitis-other-text"))) {
			values.add(outputText(props,"impression-Rhinitis-other-text"));
		}
		outputCheckedValue(props,values,"impression-Nonallergic",";Nonallergic:");
		outputCheckedValue(props,values,"impression-Nonallergic-Vasomotor","Vasomotor");
		if (StringUtils.isNotBlank(outputText(props,"impression-Nonallergic-other-text"))) {
			values.add(outputText(props,"impression-Nonallergic-other-text"));			
		}
		outputCheckedValue(props,values,"impression-SinonasalsTumor",";Sinonasal Tumor:");
		if (StringUtils.isNotBlank(outputText(props,"impression-SinonasalsTumor-text"))) {
			values.add(outputText(props,"impression-SinonasalsTumor-text"));
		}
		outputCheckedValue(props,values,"impression-Headache",";Headache");
		outputCheckedValue(props,values,"impression-SeptalDeviation",";Septal Deviation:");
		outputCheckedValue(props,values,"impression-SeptalDeviation-L","L");
		outputCheckedValue(props,values,"impression-SeptalDeviation-R","R");
		outputCheckedValue(props,values,"impression-NasoseptalDeviation",";Nasoseptal Deviation:");
		outputCheckedValue(props,values,"impression-NasoseptalDeviation-L","L");
		outputCheckedValue(props,values,"impression-NasoseptalDeviation-R","R");
		outputCheckedValue(props,values,"impression-other",";Other:");
		if (StringUtils.isNotBlank(outputText(props,"impression-other-text"))) {
			values.add(outputText(props,"impression-other-text"));
		}
		String text = StringUtils.join(values,",");
		text = text.replaceAll(":,",":");
		text = text.replaceAll(",;",";");
		if (text.startsWith(";")) {
			text = text.substring(1);
		}
		if (text.endsWith(";")) {
			text = text.substring(0,text.length() - 1);
		}
		return text;
	}
%>
<%!
	private String getSingleValue(Properties props,String radioName,String otherTextName) {
		String value = props.getProperty(radioName,"");
		if (value.indexOf("text") > -1) {
			return props.getProperty(otherTextName,"");
		} else {
			return value;
		}
	}
%>
<body>
<div id="content">    
<center id="Top">
<div id="content-inner">

<p align="center">
<strong><span style="font-size:22px">Dr. Shaun Kilty MD, FRCSC</span><br>
Otolaryngology - Head &amp; Neck Surgery<br></strong>
<span style="font-size:14px">459 - 737 Parkdale Ave, Ottawa, Ontario K1Y 1J8<br>
Tel: (613)798-5555 xt. 18514<br>
Fax: (613)729-2412</span>
<br/>
<span><a href="http://www.entsinuscare.com" target="_blank">www.entsinuscare.com</a></span>
</p>

<form method="post" action="#" name="form-EMR Letters" id="form-consult1">
	<input type="text" name="today" id="today" placeholder="YYYY-MM-DD" style="border:0px;margin-top:40px;" value="<%=getDatabaseAPValue(request,"today") %>" oscardb=today>

	<div id="referring-doctor" style="margin-top:30px">
	<input type="text" name="referring-doctor" id="referring-doctor" value="<%=getDatabaseAPValue(request,"referral_name") %>" placeholder="Referring Doctor" style="border:0px;" name="referral-name" oscardb=referral_name><br>
	<input type="text" name="referring-doctor-address" id="referring-doctor-address" value="<%=getDatabaseAPValue(request,"referral_address") %>" placeholder="Referring Doctor Address" style="border:0px;width:400px" name="referral-address" oscardb=referral_address><br>
	<input type="text" name="referring-doctor-phone" id="referring-doctor-phone" value="<%=getDatabaseAPValue(request,"referral_phone") %>" placeholder="Phone" style="border:0px;width:200px" name="referral-phone" oscardb=referral_phone><br>
	<input type="text" name="referring-doctor-fax" id="referring-doctor-fax" value="<%=getDatabaseAPValue(request,"referral_fax") %>" placeholder="Fax" style="border:0px;width:200px" name="referral-fax" oscardb=referral_fax><br>
	</div>
        

	<p>Dear Dr. <input type="text" placeholder="Doctor Name" name="referring-doctor-dear" style="border:0px;" value="<%=getDatabaseAPValue(request,"referral_name") %>" oscardb=referral_name><br></p>
	<p>RE: <input type="text" placeholder="Patient Name" style="border:0px;width:160px" name="patient-name-re" value="<%=getDatabaseAPValue(request,"first_last_name") %>" oscardb=first_last_name> <span style="padding-left:20px">
	D.O.B: <input type="text" name="dob" id="dob" placeholder="YYYY-MM-DD" style="border:0px;margin-top:5px;" value="<%=getDatabaseAPValue(request,"dobc2") %>" oscardb=dobc2></span> </p>


 

<span class="MsoNormal" id="userChkbx">
		<span style="font-family: Arial" class="plan-other"> 
			<input type="checkbox" name="userChkbx" <%= props.getProperty("userChkbx", "") %>/>
			<span>
				<%= props.getProperty("userText", "") %>				
			</span>			
		</span>
</span>

<p style="margin: 0px;">Thank you for this referral. This patient was seen for possible  <%= getRfr(props) %>.
This patient has symptom(s) of <p style="margin:0px;margin-left: 30px;"><%=joinMultipleValueForSymptoms(props) %></p>
They
<%
	String value = props.getProperty("priorSinusSurgery","");
	if (value.equalsIgnoreCase("yes")) {
		out.print("have");
	} else {
		out.print("have not");
	}
%> 
undergone sinus surgery in the past. They do 
<%
	value = props.getProperty("historyFacialFracture","");
	if (value.equalsIgnoreCase("yes")) {
		out.print("have");
	} else {
		out.print("have not");
	}
%> 
a history of facial and/or nasal trauma. There
<%
	value = props.getProperty("environmental","");
	if (value.equalsIgnoreCase("yes")) {
		out.print("is");
	} else {
		out.print("is no");
	}
%>  
history of environmental allergy testing.
<%
	value = props.getProperty("environmental","");
	if (value.equalsIgnoreCase("yes")) {
		out.print("They have multiple environmental allergies.");
	} else {
		out.print("");
	}
%>  
</p>

<div id="examination_area">
	<strong>Examination:<br></strong>
	<div>
		Otoscopy:
		<input type="text" value="<%=getSingleValue(props,"otoscopy","otoscopy-text1") %>" />
	</div>
	<div>
		Oral cavity and oropharyx:
		<input type="text" value="<%=getSingleValue(props,"oralCavity","oralCavity-text1") %>" />		
	</div>
	<div>
		Neck:
		<input type="text" value="<%=getSingleValue(props,"neck","neck-text1") %>" />
	</div>
	<div>
		Cranial nerve screen:
		<input type="text" value="<%=getSingleValue(props,"cranial","cranial-text1") %>" />
	</div>
	<div>
		Anterior rhinoscopy:
		<input type="text" value="<%= props.getProperty("anterior-text1", "") %>" />
	</div>
	<div>
		Septal Deviation: <input type="text" style="width: 300px;" value="<%= joinMultipleValueForSeptalDeviation(props) %>" />
	</div>
	<div>
		Inferior Turbinates: <input type="text" style="width: 300px;" value="<%= joinMultipleValueForInferiorTurbinates(props) %>" />
	</div>
	<div>
		Endoscopy:<input type="text" style="width: 500px;" value="<%= joinMultipleValueForEndoscopy(props) %>">
	</div>
	<div>
		CT scan of the sinuses: <input type="text" value="<%=getSingleValue(props,"ctscan","ctscan-other-text") %>">
	</div>
</div>


<p><strong>Impression: <br></strong>
This patient has 
<span class="impression-desc">
	<%=joinMultipleValueForImpression(props) %>
</span>
</p>
<div id="planArea">
<p>
	<strong>Plan<br/></strong>
	
	<span id="saline">
		<input type="checkbox" name="saline" <%= props.getProperty("saline", "") %>/>Saline Irrigation 
		<label style="font-size: 10.0pt; font-family: Arial">OD-BID&nbsp; </label>
		<br/>
	</span>
	
	<span id="singulair">
		<input type="checkbox" name="singulair" <%= props.getProperty("singulair", "") %>/>
		<span style="font-family: Arial">Singulair</span>
		<span style="font-size: 10.0pt; font-family: Arial">10 mg OD</span>
		<br/>
	</span>
	
	<span class="MsoNormal" id="antibiotic">
		<span style="font-family: Arial">
		<i><input type="checkbox" name="antibiotic" <%= props.getProperty("antibiotic", "") %>/>Antibiotic</i>:&nbsp; </span><span style="font-family: Arial">
		Duration:&nbsp; <input class="underline" style="width: 50px;" type="text" name="duration" value="<%= props.getProperty("duration", "") %>" /> days</span>
		<br/>
	</span>
	
	<span id="cefuroxime">
		<span style="font-family: Arial"> 
		<input type="checkbox" name="cefuroxime" <%= props.getProperty("cefuroxime", "") %>/></span><span style="font-family: Arial">Cefuroxime </span><span style="font-size: 10.0pt; font-family: Arial">(500 
		mg BID)&nbsp;&nbsp;</span>
	</span>
	
	<span id="clavulin">
		<span style="font-family: Arial"><input type="checkbox" name="clavulin" <%= props.getProperty("clavulin", "") %>/>Clavulin </span><span style="font-size: 10.0pt; font-family: Arial">(875 mg 
		BID)</span><span style="font-family: Arial">&nbsp;&nbsp;
	</span>
	
	<span id="clarithromycin">
		<input type="checkbox" name="clarithromycin" <%= props.getProperty("clarithromycin", "") %>/>Clarithromycin </span><span style="font-size: 10.0pt; font-family: Arial">
		(500 mg BID)</span>
		<br/>
	</span>
	
	<span id="medical-other">
		<span style="font-family: Arial">
		<input type="checkbox" name="medical-other" <%= props.getProperty("medical-other", "") %>/>
		<input class="underline" style="width: 300px;" type="text" name="medical-other-text" value="<%= props.getProperty("medical-other-text", "") %>" />
		<br/>
	</span>
	
	
	<span id="low-dose">
		<input type="checkbox" name="low-dose" <%= props.getProperty("low-dose", "") %>/></span><span style="font-family: Arial">Low Dose Macrolide (Clarithromycin 250 mg po OD) for 3 months</span>
		<br/>
	</span>
	
	
<!--	<i id="steroids"><b>Steroids:</b></i>-->
	
	<span id="incs">
		<input type="checkbox" name="incs" <%= props.getProperty("incs", "") %>/><span style="font-family: Arial">INCS </span><span style="font-size: 10.0pt; font-family: Arial">(2 
		sprays/nostril <input class="underline" style="width: 30px;" type="text" name="incs-days" value="<%= props.getProperty("incs-days", "") %>" />D)</span>
		<br/>
	</span>
	
	<span id="prednisone">
		<input type="checkbox" name="prednisone" <%= props.getProperty("prednisone", "") %>/><span style="font-family: Arial">Prednisone: </span>
		<input class="underline" style="width: 50px;" type="text" name="prednisone-mg" value="<%= props.getProperty("prednisone-mg", "") %>" />
		<span style="font-size: 10.0pt; font-family: Arial">mg po OD for</span>
		<input class="underline" style="width: 30px;" type="text" name="prednisone-days" value="<%= props.getProperty("prednisone-days", "") %>" />
		<span style="font-size: 10.0pt; font-family: Arial">days.(risks: GI, Psych, AVN hip)</span>
		<input type="checkbox" name="medical-other" <%= props.getProperty("prednisone-discussed", "") %>/>discussed
		<br/>
	</span>
	
	<span id="pulmicort">	
		<span style="font-family: Arial">
		<input type="checkbox" name="pulmicort" <%= props.getProperty("pulmicort", "") %>/></span>
		<span style="font-family: Arial">
			Pulmicort Irrigations: 
			<input class="underline" style="width: 50px;" type="text" name="pulmicort-mg" value="<%= props.getProperty("pulmicort-mg", "") %>" /> 
			mg/mL
		</span>
		<span style="font-size: 8.0pt; font-family: Arial">
			,2 mL respule.1 respule in 240 mL saline, irrigate with 120 mL BID.)
		</span>
		<br/>
	</span>
	
<!--	<i id="allergy"><span style="font-family: Arial;font-weight: bold;">Allergy:</span></i>-->
	
	<span id="allergist-1">
		<input type="checkbox" name="allergist-1" <%= props.getProperty("allergist-1", "") %>/>Allergist Referral:&nbsp;&nbsp; 
	</span>
	
	<span id="allergist-2">
		<input type="checkbox" name="allergist-2" <%= props.getProperty("allergist-2", "") %>/>Requested Today&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</span>
	
	<span id="allergist-3">
		<input type="checkbox" name="allergist-3" <%= props.getProperty("allergist-3", "") %>/>Please Refer this Patient
		<br/>
	</span>
	 
	<span id="allergist-4">
	<input type="checkbox" name="allergist-4" <%= props.getProperty("allergist-4", "") %>/>Immunotherapy evaluation with current allergist<br/>
	</span> 
	
	<span id="allergist-5">
		<input type="checkbox" name="allergist-5" <%= props.getProperty("allergist-5", "") %>/>ASA desensitization
		<br/>
	</span>
	
	<span id="allergist-6">
		<input type="checkbox" name="allergist-6" <%= props.getProperty("allergist-6", "") %>/>omalizumab evaluation
		<br/>
	</span>
	
	<span id="allergist-7">
		<input type="checkbox" name="allergist-7" <%= props.getProperty("allergist-7", "") %>/>
		<input class="underline" style="width: 600px;" type="text" name="allergist-8-text" value="<%= props.getProperty("allergist-8-text", "") %>" />
		<br/>
	</span>
	
	
	
	<span class="MsoNormal" id="surgery-1">
		<input type="checkbox" name="surgery-1" <%= props.getProperty("surgery-1", "") %>/>
		<input class="underline" style="width: 60px;" type="text" name="surgery-1-ess" value="<%= props.getProperty("surgery-1-ess", "") %>" />
		ESS (risks: orbital injury, CSF leak, bleeding, pain, infection, recurrence) 
		<input type="checkbox" name="surgery-1-discussed" <%= props.getProperty("surgery-1-discussed", "") %>/>
		discussed
		<br/>
	</span>
	
	<span class="MsoNormal" id="surgery-2">
		<input type="checkbox" name="surgery-2" <%= props.getProperty("surgery-2", "") %>/>
		Endonasal Endoscopic Resection of
		<input class="underline" style="width: 60px;" type="text" name="surgery-2-text" value="<%= props.getProperty("surgery-2-text", "") %>" />
		Sinonasal Tumor(risks: orbital injury, CSF leak, bleeding, pain, infection, recurrence, post-op care) 
		<input type="checkbox" name="surgery-2-discussed" <%= props.getProperty("surgery-2-discussed", "") %>/>
		discussed
		<br/>
	</span>
	
	<span class="MsoNormal" id="surgery-3">
		<input type="checkbox" name="surgery-3" <%= props.getProperty("surgery-3", "") %>/>
		Septoplasty and BITR (risks: pain, bleeding, infection, residual obstruction) 
		<input type="checkbox" name="surgery-3-discussed" <%= props.getProperty("surgery-3-discussed", "") %>/>
		discussed
		<br/>
	</span>
	
	<span class="MsoNormal" id="surgery-4">
		<input type="checkbox" name="surgery-4" <%= props.getProperty("surgery-4", "") %>/>
		<input class="underline" style="width: 60px;" type="text" name="surgery-4-text" value="<%= props.getProperty("surgery-4-text", "") %>" />
		Septorhinoplasty (risks: pain, bleeding, infection, scar, residual deviation or obstruction) 
		<input type="checkbox" name="surgery-4-discussed" <%= props.getProperty("surgery-4-discussed", "") %>/>
		discussed
		<br/>
	</span>
	
	<span class="MsoNormal" id="surgery-5">
		<span style="font-family: Arial">
		<i><input type="checkbox" name="surgery-5" <%= props.getProperty("surgery-5", "") %>/>CT scan</i> of the paranasal sinuses </span>
		<br/>
	</span>
	
	
	<span class="MsoNormal" id="surgery-6">
		<span style="font-family: Arial"> 
			<input type="checkbox" name="surgery-6" <%= props.getProperty("surgery-6", "") %>/>
			Given the absence of sinus disease, I suggest you refer this patient to a neurologist for a headache evaluation.
		</span>
		<br/>
	</span>
	
	
	<span class="MsoNormal" id="surgery-7">
		<span style="font-family: Arial"> 
			<input type="checkbox" name="surgery-7" <%= props.getProperty("surgery-7", "") %>/>Return to clinic: 
			<input class="underline" style="width: 60px;" type="text" name="surgery-7-text" value="<%= props.getProperty("surgery-7-text", "") %>" /> 
			months&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
		</span>
		<br/>
	</span>
	
	<span class="MsoNormal" id="plan-other">
		<span style="font-family: Arial" class="plan-other"> 
			<input type="checkbox" name="plan-other" <%= props.getProperty("plan-other", "") %>/>
			<span>
				<%= props.getProperty("plan-other-text", "") %>				
			</span>			
		</span>
	</span>
</div>
	
<p><span style="font-size: 11.0pt; line-height: 115%; font-family: Calibri">&nbsp;</span>Sincerely,</p>	
<p>
	<% if (prop != null && StringUtils.isNotEmpty(prop.getValue())) { %>
		<img src="<%= request.getContextPath() %>/eform/displayImage.do?imagefile=<%= prop.getValue() %>"/>
	<% } %>
</p>
<p><span style="font-size: 11.0pt; line-height: 115%; font-family: Calibri">&nbsp;</span>Dr. Shaun Kilty MD, FRCSC</p>
<!--<p><%= props.getProperty("signatures", "") %></p>-->

</center>
</div><!--content-->

<br><br><br>


<div class="container-controls DoNotPrint">
   <div class="controls">
<input value="Print" class="button-slim" name="PrintButton" type="button" onclick="window.print();" title="Print - HTML print out">


<a href="#Top" class="top-link"><strong>Top</strong></a>
   </div>

   <div class="background"></div>
</div>

</form>
<script type="text/javascript">
jQuery(window).load(function() {
	var plan_array = [
    	{ 
    		module:'medical',
    		options:['saline','singulair','antibiotic','cefuroxime','clavulin','clarithromycin','medical-other','low-dose']
    	},
    	{ 
    		module:'steroids',
    		options:['incs','prednisone','pulmicort']
    	},
    	{ 
    		module:'allergy',
    		options:['allergist-1','allergist-2','allergist-3','allergist-4','allergist-5','allergist-6','allergist-7',
    		         'surgery-1','surgery-2','surgery-3','surgery-4','surgery-5','surgery-6','surgery-7','plan-other']
    	},
    ];
    var totalCount = 0;
    for(var i=0;i<plan_array.length;i++) {
        var mod = plan_array[i];
        var modCount = 0 ;
        var opts = mod.options;
        for(var j=0;j<opts.length;j++) {
            var obj = $('#planArea input[name="' + opts[j] + '"]');
            if (obj.is(":checked")) {
                modCount += 1;
                totalCount += 1;
            } else {
                $('#' + opts[j]).hide();
            }
        }
        if (modCount == 0) {
            $('#' + mod.module).hide();
        }
    }
    if (totalCount == 0) {
        $('#planArea').hide();
    }
    handleExaminationArea();

    function handleExaminationArea() {
        var inputs = $('#examination_area input');
        var len = inputs.length;
        var count = 0;
        inputs.each(function(i,obj) {
            if ($(obj).val().trim() === '') {
                $(obj).parent().hide();
                count++;
            }
        });
        if (count === len) {
        	$('#examination_area').hide();
        }
    }
});
</script>
</body>
</html>