<%@ page import="oscar.util.*, oscar.form.*, oscar.form.data.*"%>
<%@ page import="org.apache.commons.lang.StringUtils"%>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>


<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.*"%>
<%@ page import="oscar.eform.data.DatabaseAP"%>
<%@ page import="oscar.eform.EFormLoader"%>
<%@ page import="oscar.eform.EFormUtil" %>
<%@ page import="org.oscarehr.common.dao.UserPropertyDAO" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.UserProperty" %>
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">

<title>Sinus Follow Up Letter</title>
<style>
body {
	font-family:"Arial", verdana;
	font-size:14px;
	line-height: 24px;
}

input {
	border:none; border-bottom: 1px #ccc solid;
}

#content-inner {
	width:750px;
	text-align:left;
	padding:10px;
	border:thin solid #ccc;
}
.container-controls {
    position:fixed;
	width:100%;
	bottom:0px;
	left:0px;
	padding:4px;
	padding-bottom:8px;
}
.controls {
    position:relative;
    color:White;
    z-index:5;
}
.background {
	position:absolute;
	top:0px;
	left:0px;
	width:100%;
	height:100%;
	background-color:Black;
	z-index:1;
	/* These three lines are for transparency in all browsers. */
	-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
	filter: alpha(opacity=50);
	opacity:.5;
}
.button-slim:hover {
	cursor: hand; cursor: pointer;
}
.button-save{
	background:#5CCD00;
	background:-moz-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-webkit-gradient(linear,left top,left bottom,color-stop(0%,#5CCD00),color-stop(100%,#4AA400));
	background:-webkit-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-o-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-ms-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#5CCD00',endColorstr='#4AA400',GradientType=0);
	padding:6px 10px;
	color:#fff;
	font-family:'Helvetica Neue',sans-serif;
	font-size:12px;
	border-radius:5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border:1px solid #347400;
}
.button-save:hover {
	cursor: hand; cursor: pointer;
}
.top-link {
	color:#fff;
	text-decoration:none;
	padding-left:10px;
}
.top-link:hover {
	text-decoration:underline;
	cursor: hand; cursor: pointer;
}
.left {
	float: left;
	width: 125px;
	text-align: right;
	margin: 2px 10px;
	display: inline;
}
.right {
	float: left;
	text-align: left;
	margin: 2px 10px;
	display: inline;
}
.nav_button {
	display: inline;
}
</style>

<style type="text/css" media="print">
.DoNotPrint {display: none;}
textarea,input {font-size:14px;border: none;}
select {border: none;}

/*dont print placeholders*/
::-webkit-input-placeholder { /* WebKit browsers */
      color: transparent;
}
:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
color: transparent;
}
::-moz-placeholder { /* Mozilla Firefox 19+ */
color: transparent;
}
:-ms-input-placeholder { /* Internet Explorer 10+ */
color: transparent;
}
/*dont print placeholders*/

#content-inner{border:0px}
</style>
<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
$(window).load(function() {
	if (window.location.href.indexOf("efmshowform_data.jsp") > -1) {
		return;
	}
	$.ajax( {
		type : "post",
		url : "<%=request.getContextPath() %>/form/formname.do",
		data : {
			submit : 'saveFormLetter',
			demographic_no : '<%=request.getParameter("demographic_no") %>',
			letterHtml : '<!DOCTYPE html><html xmlns="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">'
				+ $('html').html() + '</html>',
			letterEformName : 'Sinus Follow Up',
			form_class : 'SinusFollowUp',
			formId : '<%=request.getParameter("formId") %>',

		},
		async : true,
		dataType : "text",
		success : function(ret) {
			if (ret.trim() == 'ok') {
				
			}
		},
		error : function() {
		}
	});
});
</script>
</head>
<%!
	private String replaceAllFields(HttpServletRequest request, String sql) {
		sql = DatabaseAP.parserReplace("demographic", request.getParameter("demographic_no"), sql);
		return sql;
	}
%>
<%!
	private String getDatabaseAPValue(HttpServletRequest request,String apName) {
		DatabaseAP ap = EFormLoader.getInstance().getAP(apName);
		if (ap == null) {
			return "";
		}
		String sql = ap.getApSQL();
		String output = ap.getApOutput();
		if (!StringUtils.isBlank(sql)) {
			sql = replaceAllFields(request,sql);
			ArrayList<String> names = DatabaseAP.parserGetNames(output); // a list of ${apName} --> apName
			sql = DatabaseAP.parserClean(sql); // replaces all other ${apName} expressions with 'apName'
			ArrayList<String> values = EFormUtil.getValues(names, sql);
			if (values.size() != names.size()) {
				output = "";
			} else {
				for (int i = 0; i < names.size(); i++) {
					output = DatabaseAP.parserReplace( names.get(i), values.get(i), output);
				}
			}
		}
		return output;
	}
%>
<%
    String formClass = "SinusFollowUp";

    UserPropertyDAO userPropertyDAO = SpringUtils.getBean(UserPropertyDAO.class);
    int demoNo = Integer.parseInt(request.getParameter("demographic_no"));
    int formId = Integer.parseInt(request.getParameter("formId"));
    FrmRecord rec = (new FrmRecordFactory()).factory(formClass);
    java.util.Properties props = rec.getFormRecord(LoggedInInfo.getLoggedInInfoFromSession(request),demoNo, formId);
    UserProperty prop = userPropertyDAO.getProp((String) session.getAttribute("user"), UserProperty.PROVIDER_CONSULT_SIGNATURE);
%>
<%!
	private String joinMultipleValueForSymptoms(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty("Symptoms_None").indexOf("checked") > -1) {
			sb.append("None<br/>");
		}
		if (props.getProperty("Symptoms_Congestion").indexOf("checked") > -1) {
			sb.append("Congestion<br/>");
		}
		if (props.getProperty("Symptoms_Pain").indexOf("checked") > -1) {
			sb.append("Pain:");
			if (props.getProperty("Symptoms_Pain_M").indexOf("checked") > -1) {
				sb.append("Midfacial");
				sb.append("&nbsp;&nbsp;");
			}
			if (props.getProperty("Symptoms_Pain_F").indexOf("checked") > -1) {
				sb.append("Frontal");
			}
			sb.append("<br/>");
		}
		if (props.getProperty("Symptoms_Obstruction").indexOf("checked") > -1) {
			sb.append("Obstruction:");
			if (props.getProperty("Symptoms_Obstruction_L").indexOf("checked") > -1) {
				sb.append("Left");
				sb.append("&nbsp;&nbsp;");
			}
			if (props.getProperty("Symptoms_Obstruction_R").indexOf("checked") > -1) {
				sb.append("Right");
			}
			sb.append("<br/>");
		}
		if (props.getProperty("Symptoms_Rhinorrhea").indexOf("checked") > -1) {
			sb.append("Rhinorrhea<br/>");
		}
		if (props.getProperty("Symptoms_Smell").indexOf("checked") > -1) {
			sb.append("Smell:");
			if (props.getProperty("Symptoms_Smell_Hyposmia").indexOf("checked") > -1) {
				sb.append("Hyposmia");
				sb.append("&nbsp;&nbsp;");
			}
			if (props.getProperty("Symptoms_Smell_Anosmia").indexOf("checked") > -1) {
				sb.append("Anosmia");
			}
			sb.append("<br/>");
		}
		return sb.toString();
	}
%>
<%!
	private String joinForTreatment(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty("Steroid_Spray").indexOf("checked") > -1) {
			sb.append("<li>Steroid Spray");
			if (StringUtils.isNotBlank(props.getProperty("Steroid_Spray_D_text"))) {
				sb.append(":").append(props.getProperty("Steroid_Spray_D_text")).append("D</li>");
			} else {
				sb.append("</li>");
			}
		}
		if (props.getProperty("Saline").indexOf("checked") > -1) {
			sb.append("<li>Saline</li>");
		}
		if (props.getProperty("Prednisone").indexOf("checked") > -1) {
			sb.append("<li>Prednisone</li>");
		}
		if (props.getProperty("Antihistamine").indexOf("checked") > -1) {
			sb.append("<li>Antihistamine</li>");
		}
		if (props.getProperty("Singulair").indexOf("checked") > -1) {
			sb.append("<li>Singulair</li>");
		}
		if (props.getProperty("Pulmicort_Irrigations").indexOf("checked") > -1) {
			sb.append("<li>Pulmicort Irrigations");
			if (StringUtils.isNotBlank(props.getProperty("Pulmicort_Irrigations_text"))) {
				sb.append("(").append(props.getProperty("Pulmicort_Irrigations_text")).append("mg/mL)</li>");
			} else {
				sb.append("</li>");
			}
		}
		if (props.getProperty("Immunotherapy").indexOf("checked") > -1) {
			sb.append("<li>Immunotherapy</li>");
		}
		if (props.getProperty("Abx").indexOf("checked") > -1) {
			sb.append("<li>Abx");
			if (StringUtils.isNotBlank(props.getProperty("Abx_text"))) {
				sb.append(":").append(props.getProperty("Abx_text")).append("</li>");
			} else {
				sb.append("</li>");
			}
		}
		if (props.getProperty("Low_dose_Macrolide").indexOf("checked") > -1) {
			sb.append("<li>Low dose Macrolide</li>");
		}
		if (props.getProperty("Current_Treatment_other").indexOf("checked") > -1 && StringUtils.isNotBlank(props.getProperty("Current_Treatment_other_text"))) {
			sb.append("<li>");
			sb.append(props.getProperty("Current_Treatment_other_text")).append("</li>");
		}
		return sb.toString();
	}
%>
<%!
	private String joinMultipleValueForSeptalDeviation(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		List<String> values = new ArrayList<String>();
		outputCheckedValue(props,values,"Nose_Septal_Deviation_None","None");
		outputCheckedValue(props,values,"Nose_Septal_Deviation_R","Right");
		outputCheckedValue(props,values,"Nose_Septal_Deviation_L","Left");
		outputCheckedValue(props,values,"Nose_Septal_Deviation_Mild","Mild");
		outputCheckedValue(props,values,"Nose_Septal_Deviation_Moderate","Moderate");
		outputCheckedValue(props,values,"Nose_Septal_Deviation_S","Severe");
		if (values.size() > 0) {
			return StringUtils.join(values,",");
		} else {
			return "";
		}
	}
%>
<%!
	private String joinMultipleValueForInferiorTurbinates(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		List<String> values = new ArrayList<String>();
		outputCheckedValue(props,values,"Turbinates_Normal","Normal");
		outputCheckedValue(props,values,"Turbinates_Hypertrophy","Hypertrophy");
		outputCheckedValue(props,values,"Turbinates_Pallor","Pallor");
		if (values.size() > 0) {
			String tmp = StringUtils.join(values,",");
			if (StringUtils.isNotBlank(props.getProperty("Turbinates_other_text"))) {
				tmp += "," + props.getProperty("Turbinates_other_text");
			}
			return tmp;
		} else {
			if (StringUtils.isNotBlank(props.getProperty("Turbinates_other_text"))) {
				return props.getProperty("Turbinates_other_text");
			} else {
				return "";
			}
		}		
	}
%>
<%!
	private void outputCheckedValue(java.util.Properties props,List<String> list,String name,String defValue) {
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals(name) && value.indexOf("checked") > -1) {
				list.add(defValue);
			}
		}
	}
%>
<%!
	private String joinMultipleValueForEndoscopy(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty("endoscopy_polyps").indexOf("checked") > -1) {
			sb.append("Polyps:&nbsp;&nbsp;");
			if (props.getProperty("endoscopy_grade_left").indexOf("checked") > -1) {
				sb.append("Left:Grade&nbsp;&nbsp;").append(props.getProperty("endoscopy_grade_left_text")).append("/4");
				if (props.getProperty("endoscopy_grade_right").indexOf("checked") > -1) {
					sb.append(",");
				}
			}
			if (props.getProperty("endoscopy_grade_right").indexOf("checked") > -1) {
				sb.append("Right:Grade&nbsp;&nbsp;").append(props.getProperty("endoscopy_grade_right_text")).append("/4");
			}
			sb.append("<br/>");
		}
		if (props.getProperty("endoscopy_pus").indexOf("checked") > -1) {
			sb.append("Pus:&nbsp;&nbsp;");
			List<String> items = new ArrayList<String>();
			outputCheckedValue(props,items,"endoscopy_pus_left","Left");
			outputCheckedValue(props,items,"endoscopy_pus_right","Right");
			sb.append(StringUtils.join(items,","));
			sb.append("<br/>");
		}
		if (props.getProperty("endoscopy_inflammation").indexOf("checked") > -1) {
			sb.append("Inflammation:&nbsp;&nbsp;");
			if (props.getProperty("endoscopy_inflammation_left").indexOf("checked") > -1) {
				sb.append("Left:Grade&nbsp;&nbsp;").append(props.getProperty("endoscopy_infl_grade_left_text")).append("/4");
				if (props.getProperty("endoscopy_inflammation_right").indexOf("checked") > -1) {
					sb.append(",");
				}
			}
			if (props.getProperty("endoscopy_inflammation_right").indexOf("checked") > -1) {
				sb.append("Right:Grade&nbsp;&nbsp;").append(props.getProperty("endoscopy_infl_grade_right_text")).append("/4");
			}
			sb.append("<br/>");
		}
		if (props.getProperty("endoscopy_Irrigated").indexOf("checked") > -1) {
			sb.append("Irrigated and debrided<br/>");
		}
		if (props.getProperty("endoscopy_MTS").indexOf("checked") > -1) {
			sb.append("MTS removed<br/>");
		}
		if (props.getProperty("endoscopy_other").indexOf("checked") > -1) {
			sb.append("Other:").append(props.getProperty("endoscopy_other_text"));
		}
		return sb.toString();
	}
%>
<%!
	private String joinMultipleValueForCTScan(java.util.Properties props) {
		List<String> values = new ArrayList<String>();
		outputCheckedValue(props,values,"CT_Scan_Normal","Normal");
		outputCheckedValue(props,values,"CT_Scan_Chronic","Chronic Sinusitis");
		if (props.getProperty("CT_Scan_Tumor").indexOf("checked") > -1) {
			StringBuffer tumor = new StringBuffer();
			tumor.append("Tumor:");
			List<String> items = new ArrayList<String>();
			outputCheckedValue(props,items,"CT_Scan_Tumor_L","Left");
			outputCheckedValue(props,items,"CT_Scan_Tumor_R","Right");
			tumor.append(StringUtils.join(items,","));
			values.add(tumor.toString());
		}
		outputCheckedValue(props,values,"CT_Scan_Not_Yet","Not Yet Done");
		if (props.getProperty("CT_Scan_other").indexOf("checked") > -1) {
			values.add("Other:" + props.getProperty("CT_Scan_other_text"));
		}
		return StringUtils.join(values,",");
	}
%>
<%!
	private String joinMultipleValueForImpression(java.util.Properties props) {
		List<String> values = new ArrayList<String>();
		if (props.getProperty("impression_Sinusitis").indexOf("checked") > -1) {
			String temp = "Sinusitis:";
			List<String> items = new ArrayList<String>();
			outputCheckedValue(props,items,"impression_Sinusitis_with_Polyps","CRS with Polyps");
			outputCheckedValue(props,items,"impression_Sinusitis_without_Polyps","CRS without Polyps");
			outputCheckedValue(props,items,"impression_CRS_controlled","CRS controlled");
			outputCheckedValue(props,items,"impression_CRS_not_controlled","CRS not controlled");
			temp += StringUtils.join(items,",");
			values.add(temp);
		}
		if (props.getProperty("impression_Rhinitis").indexOf("checked") > -1) {
			String temp = "Rhinitis:";
			List<String> items = new ArrayList<String>();
			outputCheckedValue(props,items,"impression_Rhinitis_Allergic","Allergic");
			outputCheckedValue(props,items,"impression_Rhinitis_Nonallergic","Nonallergic");
			outputCheckedValue(props,items,"impression_Rhinitis_Vasomotor","Vasomotor");
			if (StringUtils.isNotBlank(props.getProperty("impression_Rhinitis_other_text"))) {
				items.add(props.getProperty("impression_Rhinitis_other_text"));
			}
			temp += StringUtils.join(items,",");
			values.add(temp);
		}
		if (props.getProperty("impression_SinonasalsTumor").indexOf("checked") > -1) {
			values.add("Sinonasal Tumor:" + props.getProperty("impression_SinonasalsTumor_text"));
		}
		if (props.getProperty("impression_SeptalDeviation").indexOf("checked") > -1) {
			String temp = "Septal Deviation:";
			List<String> items = new ArrayList<String>();
			outputCheckedValue(props,items,"impression_SeptalDeviation_L","Left");
			outputCheckedValue(props,items,"impression_SeptalDeviation_R","Right");
			temp += StringUtils.join(items,",");
			values.add(temp);
		}
		if (props.getProperty("impression_NasoseptalDeviation").indexOf("checked") > -1) {
			String temp = "Nasoseptal Deviation:";
			List<String> items = new ArrayList<String>();
			outputCheckedValue(props,items,"impression_NasoseptalDeviation_L","Left");
			outputCheckedValue(props,items,"impression_NasoseptalDeviation_R","Right");
			temp += StringUtils.join(items,",");
			values.add(temp);
		}
		outputCheckedValue(props,values,"impression_well","Healed well following surgery. No concerns.");
		outputCheckedValue(props,values,"impression_Headache","Headache");
		if (props.getProperty("impression_other").indexOf("checked") > -1) {
			String temp = "Other:" + props.getProperty("impression_other_text");
			values.add(temp);
		}
		return StringUtils.join(values,"&nbsp;;&nbsp;");
	}
%>
<body>
<div id="content">    
<center id="Top">
<div id="content-inner">
<p>
	Sinus Follow-up Letter
</p>
<p align="center">
	RE: <input type="text" placeholder="Patient Name" style="border:0px;width:160px" name="patient-name-re" value="<%=getDatabaseAPValue(request,"first_last_name") %>" oscardb=first_last_name>
	<br/> 
	<span style="padding-left:20px">
	D.O.B: <input type="text" name="dob" id="dob" placeholder="YYYY-MM-DD" style="border:0px;margin-top:5px;" value="<%=getDatabaseAPValue(request,"dobc2") %>" oscardb=dobc2>
	</span>
</p>

<form method="post" action="#" name="form-EMR Letters" id="form-consult1">
	<p>
		Dear Dr. <input type="text" placeholder="Doctor Name" name="referring-doctor-dear" style="border:0px;" value="<%=getDatabaseAPValue(request,"referral_name") %>" oscardb=referral_name>
		<br/>
		<br/>
	</p>
	<p>
		This patient was seen for review.
	</p>
	<p>
		Diagnosis:<%= props.getProperty("Diagnosis", "") %>
	</p>
	<p>
		Current symptoms:
		<br/>
		<span style="margin:0px;margin-left: 60px;display: inline-block;font-size:14px;">
			<%=joinMultipleValueForSymptoms(props) %>
		</span>
	</p>
	<p style="margin-bottom: 0px;">
		Current treatment:
	</p>
	<div>
		<ol style="margin: 0px;padding-top: 0px;padding-left: 80px;">
			<%=joinForTreatment(props) %>
		</ol>
	</div>
	<p>
		<strong>Examination:<br></strong>
	</p>
	<div>
		<input type="checkbox" name="Examination_other" <%= props.getProperty("Examination_other", "") %>/>
		<textarea name="Examination_other_text" style="border: 1px solid #ccc;width: 90%;font-size: 14px;" rows="3"><%= props.getProperty("Examination_other_text", "") %></textarea>
	</div>
	<div>
		Septal Deviation:<input type="text" style="width: 400px;" value="<%= joinMultipleValueForSeptalDeviation(props) %>" />
	</div>
	<div>
		<input type="checkbox" name="Examination_nose_other" <%= props.getProperty("Examination_nose_other", "") %>/>
		<textarea name="Examination_nose_other_text" style="border: 1px solid #ccc;width: 90%;font-size: 14px;" rows="3"><%= props.getProperty("Examination_nose_other_text", "") %></textarea>
	</div>
	<div>
		Inferior Turbinates: <input type="text" style="width: 400px;" value="<%= joinMultipleValueForInferiorTurbinates(props) %>" /><br/>
	</div>
	<div>
		Endoscopy:
		<br/>
		<span style="margin:0px;margin-left: 60px;display: inline-block;font-size:14px;">
			<%=joinMultipleValueForEndoscopy(props) %>
		</span>
	</div>
	<div>
		CT scan of the sinuses:<input type="text" style="width: 530px;" value="<%= joinMultipleValueForCTScan(props) %>" />
	</div>
	<div>
		<input type="checkbox" name="lab_other" <%= props.getProperty("lab_other", "") %>/>CQS:
		<textarea name="lab_other_text" style="border: 1px solid #ccc;width: 90%;font-size: 14px;" rows="3"><%= props.getProperty("lab_other_text", "") %></textarea>
	</div>
	<div>
		Impression: <br>
		<textarea style="border:none; border-bottom: 1px #ccc solid;width: 600px;font-size: 14px;" rows="4"><%=joinMultipleValueForImpression(props) %></textarea>
	</div>

	<div id="planArea">
	<p>
		<strong>Plan<br/></strong>
		
		<span id="Medical_saline">
			<input type="checkbox" name="Medical_saline" <%= props.getProperty("Medical_saline", "") %>/>Saline Irrigation OD-BID
			<br/>
		</span>
		
		<span id="Medical_singulair">
			<input type="checkbox" name="Medical_singulair" <%= props.getProperty("Medical_singulair", "") %>/>
			Singulair 10 mg OD
			<br/>
		</span>
		
		<span id="Medical_antibiotic">
			<i><input type="checkbox" name="Medical_antibiotic" <%= props.getProperty("Medical_antibiotic", "") %>/>Antibiotic</i>:&nbsp; 
			Duration:&nbsp; <input class="underline" style="width: 50px;" type="text" name="Medical_antibiotic_days" value="<%= props.getProperty("Medical_antibiotic_days", "") %>" /> days
			<br/>
		</span>
		
		<span id="Medical_cefuroxime">
			<input type="checkbox" name="Medical_cefuroxime" <%= props.getProperty("Medical_cefuroxime", "") %>/>Cefuroxime (500 mg BID)
			<br/>
		</span>
		
		<span id="Medical_clavulin">
			<input type="checkbox" name="Medical_clavulin" <%= props.getProperty("Medical_clavulin", "") %>/>Clavulin(875 mg BID)
			<br/>
		</span>
		
		<span id="Medical_clarithromycin">
			<input type="checkbox" name="Medical_clarithromycin" <%= props.getProperty("Medical_clarithromycin", "") %>/>Clarithromycin (500 mg BID)
			<br/>
		</span>
		
		<span id="Medical_other">
			<input type="checkbox" name="Medical_other" <%= props.getProperty("Medical_other", "") %>/>
			<input class="underline" style="width: 300px;" type="text" name="Medical_other_text" value="<%= props.getProperty("Medical_other_text", "") %>" />
			<br/>
		</span>
		
		
		<span id="Medical_low_dose">
			<input type="checkbox" name="Medical_low_dose" <%= props.getProperty("Medical_low_dose", "") %>/>Low Dose Macrolide (Clarithromycin 250 mg po OD) for 3 months
			<br/>
		</span>
		
		<span id="Steroids_incs">
			<input type="checkbox" name="Steroids_incs" <%= props.getProperty("Steroids_incs", "") %>/>Intranasal Steroid Spray(2 sprays/nostril 
			<input class="underline" style="width: 30px;" type="text" name="Steroids_incs_days" value="<%= props.getProperty("Steroids_incs_days", "") %>" />D)
			<br/>
		</span>
		
		<span id="Steroids_prednisone">
			<input type="checkbox" name="Steroids_prednisone" <%= props.getProperty("Steroids_prednisone", "") %>/>Prednisone: 
			<input class="underline" style="width: 50px;" type="text" name="Steroids_prednisone_mg" value="<%= props.getProperty("Steroids_prednisone_mg", "") %>" />
			mg po OD for
			<input class="underline" style="width: 30px;" type="text" name="Steroids_prednisone_days" value="<%= props.getProperty("Steroids_prednisone_days", "") %>" />
			days.(risks: GI, Psych, AVN hip)
			<input type="checkbox" name="Steroids_prednisone_discussed" <%= props.getProperty("Steroids_prednisone_discussed", "") %>/>discussed
			<br/>
		</span>
		
		<span id="Steroids_pulmicort">	
			
			<input type="checkbox" name="Steroids_pulmicort" <%= props.getProperty("Steroids_pulmicort", "") %>/>Pulmicort Irrigations: 
			<input class="underline" style="width: 50px;" type="text" name="Steroids_pulmicort_mg" value="<%= props.getProperty("Steroids_pulmicort_mg", "") %>" /> 
			mg/mL,2 mL respule.1 respule in 240 mL saline, irrigate with 120 mL BID.)
			<br/>
		</span>
		
		<span id="Allergy_Referral">
			<input type="checkbox" name="Allergy_Referral" <%= props.getProperty("Allergy_Referral", "") %>/>Allergist Referral:
			&nbsp;&nbsp; 
			<input type="checkbox" name="Allergy_Referral_today" <%= props.getProperty("Allergy_Referral_today", "") %>/>Requested Today
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="checkbox" name="Allergy_Referral_pls" <%= props.getProperty("Allergy_Referral_pls", "") %>/>Please Refer this Patient
			<br/>
		</span>
		
		<span id="Allergy_Immunotherapy">
		<input type="checkbox" name="Allergy_Immunotherapy" <%= props.getProperty("Allergy_Immunotherapy", "") %>/>Immunotherapy evaluation with current allergist<br/>
		</span> 
		
		<span id="Allergy_ASA">
			<input type="checkbox" name="Allergy_ASA" <%= props.getProperty("Allergy_ASA", "") %>/>ASA desensitization
			<br/>
		</span>
		
		<span id="Allergy_other">
			<input type="checkbox" name="Allergy_other" <%= props.getProperty("Allergy_other", "") %>/>
			<textarea name="Allergy_other_text" style="border: 1px solid #000;width: 90%" rows="4"><%= props.getProperty("Allergy_other_text", "") %></textarea>
			<br/>
		</span>
		
		<span id="surgery_1">
			<input type="checkbox" name="surgery_1" <%= props.getProperty("surgery_1", "") %>/>
			<input class="underline" style="width: 40px;" type="text" name="surgery_1_text" value="<%= props.getProperty("surgery_1_text", "") %>" />
			Endoscopic Sinus Surgery (risks: orbital injury, CSF leak, bleeding, pain, infection, recurrence) 
			<input type="checkbox" name="surgery_1_discussed" <%= props.getProperty("surgery_1_discussed", "") %>/>discussed
			<br/>
		</span>
		
		<span id="surgery_2">
			<input type="checkbox" name="surgery_2" <%= props.getProperty("surgery_2", "") %>/>
			Endonasal Endoscopic Resection of
			<input class="underline" style="width: 60px;" type="text" name="surgery_2_text" value="<%= props.getProperty("surgery_2_text", "") %>" />
			Sinonasal Tumor(risks: orbital injury, CSF leak, bleeding, pain, infection, recurrence, post-op care) 
			<input type="checkbox" name="surgery_2_discussed" <%= props.getProperty("surgery_2_discussed", "") %>/>discussed
			<br/>
		</span>
		
		<span id="surgery_3">
			<input type="checkbox" name="surgery_3" <%= props.getProperty("surgery_3", "") %>/>
			Septoplasty and Bilateral Inferior Turbinate Reduction (risks: pain, bleeding, infection, residual obstruction) 
			<input type="checkbox" name="surgery_3_discussed" <%= props.getProperty("surgery_3_discussed", "") %>/>discussed
			<br/>
		</span>
		
		<span id="surgery_4">
			<input type="checkbox" name="surgery_4" <%= props.getProperty("surgery_4", "") %>/>
			<input class="underline" style="width: 60px;" type="text" name="surgery_4_text" value="<%= props.getProperty("surgery_4_text", "") %>" />
			Septorhinoplasty (risks: pain, bleeding, infection, scar, residual deviation or obstruction) 
			<input type="checkbox" name="surgery_4_discussed" <%= props.getProperty("surgery_4_discussed", "") %>/>discussed
			<br/>
		</span>
		
		<span id="surgery_5">
			<i><input type="checkbox" name="surgery_5" <%= props.getProperty("surgery_5", "") %>/>CT scan</i> of the paranasal sinuses
			<br/>
		</span>
		
		
		<span id="surgery_6">
			<input type="checkbox" name="surgery_6" <%= props.getProperty("surgery_6", "") %>/>
			Given the absence of sinus disease, I suggest you refer this patient to a neurologist for a headache evaluation.
			<br/>
		</span>
		
		<span id="surgery_7">
			<input type="checkbox" name="surgery_7" <%= props.getProperty("surgery_7", "") %>/>
			<textarea name="surgery_7_text" style="border: 1px solid #000;width: 90%" rows="4"><%= props.getProperty("surgery_7_text", "") %></textarea>
			<br/>
		</span>
		
		<span id="surgery_8">
			<input type="checkbox" name="surgery_8" <%= props.getProperty("surgery_8", "") %>/>Return to clinic: 
			<input class="underline" style="width: 60px;" type="text" name="surgery_8_text" value="<%= props.getProperty("surgery_8_text", "") %>" />months 
			<br/>
		</span>
	</div>
	
	<p>
		Sincerely,
	</p>	
	<p>
		<% if (prop != null && StringUtils.isNotEmpty(prop.getValue())) { %>
		<img src="<%= request.getContextPath() %>/eform/displayImage.do?imagefile=<%= prop.getValue() %>"/>
		<% } %>
	</p>
	<p>
		<span style="font-size: 11.0pt; line-height: 115%; font-family: Calibri">&nbsp;</span>Dr. Shaun Kilty MD, FRCSC
	</p>
</form>
</div>
</center>
</div>
<!--content-->
<br><br><br>
<div class="container-controls DoNotPrint">
	<div class="controls">
		<input value="Print" class="button-slim" name="PrintButton" type="button" onclick="window.print();" title="Print - HTML print out">
		<a href="#Top" class="top-link"><strong>Top</strong></a>
   </div>
   <div class="background"></div>
</div>
<script type="text/javascript">
jQuery(window).load(function() {
	var totalCount = 0;
	var plan_array = ['Medical_saline','Medical_singulair','Medical_antibiotic','Medical_cefuroxime','Medical_clavulin',
      	'Medical_clarithromycin','Medical_other','Medical_low_dose','Steroids_incs','Steroids_prednisone','Steroids_pulmicort',
      	'Allergy_Referral','Allergy_Immunotherapy','Allergy_ASA','surgery_1','surgery_2','surgery_3','surgery_4',
      	'surgery_5','surgery_6','surgery_7','surgery_8'];
    for(var i=0;i<plan_array.length;i++) {
        var obj = $('#planArea input[name="' + plan_array[i] + '"]');
        if (obj.is(":checked")) {
            totalCount += 1;
        } else {
            $('#' + plan_array[i]).hide();
        }
    }
    if (totalCount == 0) {
        $('#planArea').hide();
    }
});
</script>
</body>
</html>