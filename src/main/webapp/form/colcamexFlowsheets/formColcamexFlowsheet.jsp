<%--

 Copyright (c) 2021 WELL EMR Group Inc.
 This software is made available under the terms of the
 GNU General Public License, Version 2, 1991 (GPLv2).
 License details are available via "gnu.org/licenses/gpl-2.0.html".

--%>

<%@ page import="java.util.*, oscar.oscarReport.pageUtil.*" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/colcamex-tags.tld" prefix="colca" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html:html>

  <head>
    <title><bean:message key="form.colcamexFlowSheets.formColcamexFlowSheet.title"/></title>
    <link rel="stylesheet" type="text/css" media="screen"
          href="${ pageContext.request.contextPath }/form/colcamexFlowsheets/colcamexFlowsheet.css"/>
    <link rel="stylesheet" type="text/css" media="print"
          href="${ pageContext.request.contextPath }/form/colcamexFlowsheets/printColcamexFlowsheet.css"/>
  </head>
  <body id="colcamexFlowSheet">
  <div id="heading">
    <logic:notEmpty name="colcamexFlowSheet" property="imageLink">
      <img src="<c:out value="${ colcamexFlowSheet.imageLink }" />" alt="heading"/>
    </logic:notEmpty>

    <logic:empty name="colcamexFlowSheet" property="imageLink">
      <h1><bean:write name="colcamexFlowSheet" property="clinicNfo.clinicName"/></h1>
      <p><bean:write name="colcamexFlowSheet" property="clinicNfo.clinicAddress"/><br/>
        <bean:write name="colcamexFlowSheet" property="clinicNfo.clinicCity"/>,
        <bean:write name="colcamexFlowSheet" property="clinicNfo.clinicProvince"/><br/>
        Phone: <bean:write name="colcamexFlowSheet" property="clinicNfo.clinicDelimPhone"/>
        Fax: <bean:write name="colcamexFlowSheet" property="clinicNfo.clinicDelimFax"/></p>
    </logic:empty>
  </div>
  <logic:lessThan property="formId" name="colcamexFlowSheet" value="0">
    <div id="newFlowsheetDialog">
      <h2>New Measurement Flowsheet by Colcamex</h2>
      <p>
        Choose an available measurement group from the drop-down menu (below) to begin a new
        flowsheet. If the
        desired measurement group is not available in the menu then create a new group at: Admin -
        oscarEncounter - customizeOscarMeasurements.
      </p>
      <p>
        To replace the default clinic address heading information with a logo or image: upload an
        image named
        <strong>tfsHeadingImg.png</strong> to the eForm image directory at: Admin - eForms - Upload
        an Image.
      </p>
      <html:form styleId="newFlowsheetForm" action="/colcamexNewFlowSheet.do">
        <label for="group_names">Measurement group:</label>
        <html:select property="measurementGroupName" name="colcamexFlowSheet">
          <logic:iterate id="groupName" property="measurementGroupNames" name="colcamexFlowSheet">
            <html:option value="${ groupName }"/>
          </logic:iterate>
        </html:select>
        <html:submit value="Create Flowsheet" property="groupNamesSelect"/>
      </html:form>
    </div>
  </logic:lessThan>
  <logic:greaterThan property="formId" name="colcamexFlowSheet" value="0">
    <div>
      <h2><bean:write name="colcamexFlowSheet" property="title"/> Flowsheet</h2>
      <fmt:formatDate value="${ colcamexFlowSheet.date }" type="date" dateStyle="long"/>
    </div>
    <html:form action="/colcamexFlowSheetSave.do" styleId="colcamexFlowsheetForm">
      <div id="content">
        <table id="coreData">
          <tr>
            <th align="right"><bean:message key="demographic.demographicaddrecordhtm.formName"/>:
            </th>
            <th>
              <bean:write name="colcamexFlowSheet" property="demographic.firstName"/>
              <bean:write name="colcamexFlowSheet" property="demographic.lastName"/>
            </th>
            <th align="right"><bean:message key="demographic.demographicaddrecordhtm.formDOB"/>:
            </th>
            <th>
              <bean:write name="colcamexFlowSheet" property="demographic.formattedDob"/>
            </th>
          </tr>
          <tr>
            <td rowspan="2" align="right" id="problems">
              <strong><bean:message
                  key="form.colcamexFlowSheets.formColcamexFlowSheet.problems"/>:
              </strong>
            </td>
            <td rowspan="2">
              <span class="inputField">
                  <html:text name="colcamexFlowSheet"
                             property="problem.comments"
                             styleId="problem.type"
                             maxlength="100"
                             size="30"/>
              </span>
              <logic:notEmpty name="colcamexFlowSheet" property="problems">
                <ul>
                  <logic:iterate name="colcamexFlowSheet" property="problems" id="prob">
                    <li>
                      <bean:write name="prob" property="comments"/>
                    </li>
                  </logic:iterate>
                </ul>
              </logic:notEmpty>
            </td>

            <td align="right" id="allergies"><strong><bean:message
                key="global.allergies"/>:</strong>
            </td>

            <td>
              <logic:empty name="colcamexFlowSheet" property="allergies">
                <bean:message key="form.colcamexFlowSheets.formColcamexFlowSheet.nk"/>
              </logic:empty>
              <logic:notEmpty name="colcamexFlowSheet" property="allergies">
                <ul>
                  <logic:iterate name="colcamexFlowSheet" property="allergies" id="allergy">
                    <li>
                      <bean:write name="allergy" property="description" scope="page"/>
                      <logic:notEmpty name="allergy" property="reaction">
                        R: <bean:write name="allergy" property="reaction"/>
                      </logic:notEmpty>
                    </li>
                  </logic:iterate>
                </ul>
              </logic:notEmpty>
            </td>
          </tr>

          <tr>
            <td align="right" id="height">
              <strong><bean:message key="oscarEncounter.formAnnualPrint.msgHeight"/>:</strong>
            </td>

            <logic:notEmpty name="colcamexFlowSheet" property="patientHeight.dataField">
              <td>
                <bean:write name="colcamexFlowSheet" property="patientHeight.dataField"/> cm
              </td>
            </logic:notEmpty>

            <logic:empty name="colcamexFlowSheet" property="patientHeight.dataField">
              <td class="inputField noDisplay">
                <html:text name="colcamexFlowSheet"
                           property="patientHeight.dataField"
                           maxlength="4"
                           size="4"/> cm
              </td>
            </logic:empty>
          </tr>

          <tr>
            <td colspan="4" id="others">
              <strong><bean:message key="oscarEncounter.formAlpha.formOther"/>:</strong>
            </td>
          </tr>

          <logic:notEmpty name="colcamexFlowSheet" property="others">
            <logic:iterate name="colcamexFlowSheet" property="others" id="othr">
              <logic:notEmpty name="othr" property="comments">
                <tr>
                  <td class="other_date">
                    <fmt:formatDate value="${ othr.dateObserved }" type="date" dateStyle="long"/>
                  </td>
                  <td colspan="3" class="other_comment">
                    <bean:write name="othr" property="comments"/>
                  </td>
                </tr>
              </logic:notEmpty>
            </logic:iterate>
          </logic:notEmpty>

          <tr>
            <td colspan="4" class="inputField noDisplay">
              <html:textarea name="colcamexFlowSheet" property="other.comments"
                             styleId="other.type"/>
            </td>
          </tr>

        </table>

          <%-- Dear stranger: don't mess with this table; unless you fully understand it... --%>

        <div id="inputErrors" class="noDisplay">
          <html:errors header="form.colcamexFlowSheets.formColcamexFlowSheet.validation.header"
                       footer="form.colcamexFlowSheets.formColcamexFlowSheet.validation.footer"
                       prefix="form.colcamexFlowSheets.formColcamexFlowSheet.validation.prefix"
                       suffix="form.colcamexFlowSheets.formColcamexFlowSheet.validation.suffix"/>
        </div>

        <colca:resultTable name="colcamexFlowSheet" property="flowsheetMeasurements"
                           entryRow="measurementObjects"
                           addRow="SITE"
                           id="resultTable"
                           addColumn="DATE(time)">

          <%-- render the input fields at the beginning of the table listing --%>

          <logic:notEmpty name="colcamexFlowSheet" property="measurementObjects">

            <tr id="input_row" class="noDisplay">
              <td>&nbsp;</td>
              <logic:iterate name="colcamexFlowSheet" property="measurementObjects"
                             id="measurement" indexId="i"
                             length="size">
                <c:if test='${measurement.type != "SITE"}'>
                  <td class="inputField">
                    <html:text name="measurement" property="dataField" maxlength="10"
                               size="5"
                               indexed="true"
                               styleClass="measurementEntry"/>
                  </td>
                </c:if>
              </logic:iterate>
            </tr>

            <tr class="site_input noDisplay">
              <td align="right"><strong>SITE:</strong></td>
              <td colspan="<bean:write name="colcamexFlowSheet" property="listSize" />">
                <html:textarea name="colcamexFlowSheet" property="site.comments"
                               styleClass="siteEntry"/>
              </td>

            </tr>
          </logic:notEmpty>
        </colca:resultTable>

      </div>

      <div id="footer" class="noDisplay">
        <html:reset value="Reset"/>
        <html:submit value="Save" property="save"/>
        <input type="button" value="Print" onclick="window.print();"/>
      </div>

    </html:form>
  </logic:greaterThan>

  </body>
</html:html>