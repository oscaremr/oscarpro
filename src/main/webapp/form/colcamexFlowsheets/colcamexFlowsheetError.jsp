<%--

 Copyright (c) 2021 WELL EMR Group Inc.
 This software is made available under the terms of the
 GNU General Public License, Version 2, 1991 (GPLv2).
 License details are available via "gnu.org/licenses/gpl-2.0.html".

--%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>
    <bean:message key="form.colcamexFlowSheets.formColcamexFlowSheet.errors.errorpage.title"/>
  </title>
</head>
<body>
  <h1>Error</h1>
  <p><c:out value="${error}"/></p>
</body>
</html>