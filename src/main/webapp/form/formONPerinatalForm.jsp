<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String user = (String) session.getAttribute("user");
    if(session.getAttribute("userrole") == null )  response.sendRedirect("../logout.jsp");
    String roleName2$ = (String)session.getAttribute("userrole") + "," + user;
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName2$%>" objectName="_form" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_form");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ page import="oscar.util.*, oscar.form.*, oscar.form.data.*"%>
<%@ page import="org.oscarehr.common.web.PregnancyAction"%>
<%@ page import="java.util.List"%>
<%@ page import="org.apache.struts.util.LabelValueBean"%>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="java.util.HashMap" %>
<%@ page import="oscar.eform.EFormUtil" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="org.oscarehr.common.model.UserProperty" %>
<%@ page import="org.oscarehr.common.dao.UserPropertyDAO" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="oscar.form.graphic.FrmGraphicAR" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="org.oscarehr.common.model.Measurement" %>
<%@ page import="oscar.oscarEncounter.data.EctFormData" %>
<%@ page import="java.util.Arrays" %>


<%
    String formClass = "ONPerinatal";
    String formLink = "formONPerinatalForm.jsp";
    String formTable = "form_on_perinatal_2017";

    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    int demoNo = Integer.parseInt(request.getParameter("demographic_no"));
    int formId = 0;
    int provNo = Integer.parseInt((String) session.getAttribute("user"));
    String section = StringUtils.noNull(request.getParameter("section"));
    String pageNo = section.replaceAll("-[A-Z]*", "").replaceAll("PR", "");
    String providerNo = request.getParameter("provider_no") != null ? request.getParameter("provider_no") : loggedInInfo.getLoggedInProviderNo();
    String appointment = request.getParameter("appointmentNo") != null ? request.getParameter("appointmentNo") : "";

    // get form ID
    List<EctFormData.PatientForm> formsONPerinatal = Arrays.asList(EctFormData.getPatientFormsFromLocalAndRemote(loggedInInfo, String.valueOf(demoNo), formTable, true));
    if (formsONPerinatal!=null && !formsONPerinatal.isEmpty()){
        formId = Integer.parseInt(formsONPerinatal.get(0).getFormId());
    }
    
    FrmONPerinatalRecord rec = (FrmONPerinatalRecord)(new FrmRecordFactory()).factory(formClass);
    java.util.Properties props = rec.getFormRecord(loggedInInfo, demoNo, formId, Integer.parseInt(pageNo));

    FrmData fd = new FrmData();
    String resource = fd.getResource();
    resource = resource + "../ob/riskinfo/";

    //get project_home
    String project_home = request.getContextPath().substring(1);

    //load eform groups
    List<LabelValueBean> cytologyForms = PregnancyAction.getEformsByGroup("Cytology");
    List<LabelValueBean> ultrasoundForms = PregnancyAction.getEformsByGroup("Ultrasound");
    List<LabelValueBean> ipsForms = PregnancyAction.getEformsByGroup("IPS");

    String labReqVer = oscar.OscarProperties.getInstance().getProperty("onare_labreqver", "10");
    if(labReqVer.equals("")) {
        labReqVer = "10";
    }

    String orderByRequest = request.getParameter("orderby");
    String orderBy = "";
    if (orderByRequest == null) orderBy = EFormUtil.NAME;
    else if (orderByRequest.equals("form_subject")) orderBy = EFormUtil.SUBJECT;
    else if (orderByRequest.equals("form_date")) orderBy = EFormUtil.DATE;

    String groupView = request.getParameter("group_view");
    if (groupView == null) {
        UserPropertyDAO userPropDAO = SpringUtils.getBean(UserPropertyDAO.class);
        UserProperty usrProp = userPropDAO.getProp(user, UserProperty.EFORM_FAVOURITE_GROUP);
        if( usrProp != null ) {
            groupView = usrProp.getValue();
        }
        else {
            groupView = "";
        }
    }

    boolean bView = "1".equals(request.getParameter("view")) || formId <= 0;
    int usNum = Integer.parseInt(props.getProperty("us_num", "0"));
    boolean autoLockPerinatal = OscarProperties.getInstance().isPropertyActive("auto_lock_antenatal");

%>

<html:html locale="true">
    <head>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <title>Perinatal Record <%=section%></title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="<%=bView?"arStyleView.css" : "arStyle.css"%>">
        <link rel="stylesheet" type="text/css" media="all" href="../share/calendar/calendar.css" title="win2k-cold-1" />
        <script type="text/javascript" src="../share/calendar/calendar.js"></script>
        <script type="text/javascript" src="../share/calendar/lang/<bean:message key="global.javascript.calendar"/>"></script>
        <script type="text/javascript" src="../share/calendar/calendar-setup.js"></script>

        <script src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js" type="text/javascript"></script>
        <script src="<%=request.getContextPath()%>/js/jquery-ui-1.8.18.custom.min.js"></script>
        <script src="<%=request.getContextPath()%>/js/fg.menu.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/formONPerinatalRecord.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/verticalTabTable.js"></script>

        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/cupertino/jquery-ui-1.8.18.custom.css">
        <link rel="stylesheet" href="<%=request.getContextPath() %>/library/bootstrap/3.0.0/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/fg.menu.css">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/formONPerinatalRecord.css">

        <script type="text/javascript">
            $(document).ready(function() {
                //dialogs(<%=pageNo%>, <%=bView%>);
                <% if (section.startsWith("PR2-")) { %>

                $("#formContent").load("formONPerinatalRecord<%=pageNo%>.jsp?demographic_no=<%=demoNo%>&formId=<%=formId%>&sectionView=true #<%=section%>", function(){
                    init(<%=pageNo%>, <%=bView%>, <%=autoLockPerinatal%>, '<%=section%>');
                    <% if(section.equalsIgnoreCase("PR2-LAB")){ %>
                    $("select[name='lab_ABO']").val('<%= Encode.forJavaScriptBlock(props.getProperty("lab_ABO", "NDONE")) %>');
                    $("select[name='lab_rh']").val('<%= Encode.forJavaScriptBlock(props.getProperty("lab_rh", "NDONE")) %>');
                    $("select[name='lab_rubella']").val('<%= Encode.forJavaScriptBlock(props.getProperty("lab_rubella", "NDONE")) %>');
                    $("select[name='lab_Hbsag']").val('<%= Encode.forJavaScriptBlock(props.getProperty("lab_Hbsag", "NDONE")) %>');
                    $("select[name='lab_syphilis']").val('<%= Encode.forJavaScriptBlock(props.getProperty("lab_syphilis", "NDONE")) %>');
                    $("select[name='lab_hiv']").val('<%= Encode.forJavaScriptBlock(props.getProperty("lab_hiv", "NDONE")) %>');
                    $("select[name='lab_gc']").val('<%= Encode.forJavaScriptBlock(props.getProperty("lab_gc", "NDONE")) %>');
                    $("select[name='lab_chlamydia']").val('<%= Encode.forJavaScriptBlock(props.getProperty("lab_chlamydia", "NDONE")) %>');
                    $("select[name='lab_ABO2']").val('<%= Encode.forJavaScriptBlock(props.getProperty("lab_ABO2", "NDONE")) %>');
                    $("select[name='lab_rh2']").val('<%= Encode.forJavaScriptBlock(props.getProperty("lab_rh2", "NDONE")) %>');
                    <% } else if(section.equalsIgnoreCase("PR2-US")){ %>
                    $.when($.ajax(initUltrasounds())).then(function () {
                        loadUltrasoundValues();
                    });
                    <% } %>
                    verticalTab();
                } );
                <% } %>
            });

            function loadUltrasoundValues() {
                <%  
                for(int i = 1; i <= usNum ; i++) {
                %>
                $("input[name='us_date<%=i%>']").val("<%= Encode.forJavaScriptBlock(props.getProperty("us_date"+i, "")) %>");
                $("input[name='us_ga<%=i%>']").val("<%= Encode.forJavaScriptBlock(props.getProperty("us_ga"+i, "")) %>");
                <%     
                    if (i == 3) {
                %>
                $("input[name='us_result<%=i%>_as']").val("<%= Encode.forJavaScriptBlock(props.getProperty("us_result"+i+"_as", "")) %>");
                $("input[name='us_result<%=i%>_pl']").val("<%= Encode.forJavaScriptBlock(props.getProperty("us_result"+i+"_pl", "")) %>");
                $("input[name='us_result<%=i%>_sm']").val("<%= Encode.forJavaScriptBlock(props.getProperty("us_result"+i+"_sm", "")) %>");
                <% 
                    } else {
                %>
                $("input[name='us_result<%=i%>']").val("<%= Encode.forJavaScriptBlock(props.getProperty("us_result"+i, "")) %>");
                <%
                    }
                }
                %>

                <% if(bView)  { %>
                $("input[name^='us_'").prop('disabled', true);

                $("img[id$='_cal']").each(function(){
                    $(this).hide();
                });
                <% } %>
            }


        </script>
        <html:base />
    </head>

    <body bgproperties="fixed" topmargin="0" leftmargin="1" rightmargin="1">
    <div id="maincontent" style="left: 0px;">
        <input type="hidden" name="ps_edb_final" id="ps_edb_final" value="<%= UtilMisc.htmlEscape(props.getProperty("ps_edb_final", "")) %>" />
        <div id="content_bar" class="innertube" style="background-color: #c4e9f6">
            <html:form action="/form/formname">
                <% if (formId <= 0) { %>
                <div class='alert-danger'>
                    No forms exist to edit. Please create a new form via patient's eChart to update.
                </div>
                <% } %>
                <div id="formContent">

                </div>
                <input type="hidden" id="demographicNo" name="demographicNo" value="<%=demoNo%>" />
                <input type="hidden" id="formId" name="formId" value="<%=formId%>" />
                <input type="hidden" id="user" name="provNo" value=<%=provNo%> />
                <input type="hidden" name="update" value="true" />
                <input type="hidden" name="method" value="exit" />

                <input type="hidden" name="forwardTo" value="<%=pageNo%>" />
                <input type="hidden" name="pageNo" value="<%=pageNo%>" />
                <input type="hidden" name="formCreated" value="<%= Encode.forHtmlAttribute(props.getProperty("formCreated", "")) %>" />
                <input type="hidden" id="episodeId" name="episodeId" value="<%= Encode.forHtmlAttribute(props.getProperty("episodeId", "")) %>" />
                <input type="hidden" id="checkForChanges" name="checkForChanges" value="" />

                <input type="hidden" id="section" name="section" value="<%=section.replaceAll("PR[1-3]-", "").toLowerCase()%>" />

                <%if ("PR2-US".equalsIgnoreCase(section)){%>
                <input type="hidden" id="us_num" name="us_num" value="<%= props.getProperty("us_num", "0") %>"/>
                <%}%>
                
                <%if (!bView) { %>
                <input type="submit" value="Save and Exit" onclick="javascript:return onSaveExit();" />
                <%} %>
                <input type="submit" value="Exit" onclick="javascript:return onExit();" />
                <span style="font-size:x-small;float: right;">id=<%=formId%> &nbsp;|&nbsp; demographic_no=<%=demoNo%></span>

            </html:form>

        </div>


    </div>
    </body>



</html:html>

<%!
    String getSelected(String a, String b) {
        if(a.trim().equalsIgnoreCase(b.trim())) {
            return " selected=\"selected\" ";
        }
        return "";
    }
%>
