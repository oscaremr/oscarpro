<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ page import="oscar.util.*, oscar.form.*, oscar.form.data.*"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>


<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@page import="java.util.*"%>
<%@page import="oscar.eform.data.DatabaseAP"%>
<%@page import="oscar.eform.EFormLoader"%>
<%@ page import="oscar.eform.EFormUtil" %>
<%@ page import="org.oscarehr.common.model.UserProperty" %>
<%@ page import="org.oscarehr.common.dao.UserPropertyDAO" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns="http://www.w3.org/TR/REC-html40">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta http-equiv="Content-Style-Type" content="text/css">

<title>Epistaxis Evaluation Letter</title>

<script type="text/javascript" src="../js/jquery-1.7.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('select').change(function() {
		var name = $(this).attr('name');
		$('input[name="other_' + name + '"]').hide();
		changeSelectStyle(this);
	});
	$('select').each(function() {
		var name = $(this).attr('name');
		$('input[name="other_' + name + '"]').hide();
		changeSelectStyle(this);
	});
	function changeSelectStyle(obj) {
		var name = $(obj).attr('name');
		var width = $(obj).find("option:selected").attr('width');
		if (width) {
			$(obj).width(width);
		} else {
			$(obj).removeAttr('style');
		}
		if ($(obj).find("option:selected").text().trim().toLowerCase().indexOf('other') > -1) {
			$('input[name="other_' + name + '"]').show();
		}
	}
});
$(window).load(function() {
	if (window.location.href.indexOf("efmshowform_data.jsp") > -1) {
		return;
	}
	$.ajax( {
		type : "post",
		url : "<%=request.getContextPath() %>/form/formname.do",
		data : {
			submit : 'saveFormLetter',
			demographic_no : '<%=request.getParameter("demographic_no") %>',
			letterHtml : '<!DOCTYPE html><html xmlns="http://www.w3.org/TR/REC-html40" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">'
				+ $('html').html() + '</html>',
			letterEformName : 'Epistaxis Evaluation Letter',
			form_class : 'EpistaxisEvaluation',
			formId : '<%=request.getParameter("formId") %>',

		},
		async : true,
		dataType : "text",
		success : function(ret) {
			if (ret.trim() == 'ok') {
				
			}
		},
		error : function() {
		}
	});
});
</script>

<style>
body{
font-family:"Arial", verdana;
font-size:14px;
line-height: 24px;
}

input{border:none; border-bottom: 1px #ccc solid;}

#content{

}

#content-inner{
width:700px;
text-align:left;
padding:10px;
border:thin solid #ccc;
}


   .container-controls {
        position:fixed;
	width:100%;
	bottom:0px;
	left:0px;
	padding:4px;
	padding-bottom:8px;
   }

   .controls {
       position:relative;
       color:White;
       z-index:5;

   }

   .background {
       position:absolute;
       top:0px;
       left:0px;
       width:100%;
       height:100%;
       background-color:Black;
       z-index:1;
       /* These three lines are for transparency in all browsers. */
       -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
       filter: alpha(opacity=50);
       opacity:.5;
   }



.button-slim:hover {
cursor: hand; cursor: pointer;
}



.button-save{
	background:#5CCD00;
	background:-moz-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-webkit-gradient(linear,left top,left bottom,color-stop(0%,#5CCD00),color-stop(100%,#4AA400));
	background:-webkit-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-o-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-ms-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#5CCD00',endColorstr='#4AA400',GradientType=0);
	padding:6px 10px;
	color:#fff;
	font-family:'Helvetica Neue',sans-serif;
	font-size:12px;
	border-radius:5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border:1px solid #347400;
}

.button-save:hover{
cursor: hand; cursor: pointer;
}

.top-link{
color:#fff;
text-decoration:none;
padding-left:10px;
}

.top-link:hover{
text-decoration:underline;
cursor: hand; cursor: pointer;
}
u00E2u20ACu2039
</style>
<style> .left {
  float: left;
  width: 125px;
  text-align: right;
  margin: 2px 10px;
  display: inline;
}

.right {
  float: left;
  text-align: left;
  margin: 2px 10px;
  display: inline;
}
.nav_button {
    display: inline;
}</style>

<style type="text/css" media="print">
.DoNotPrint {display: none;}
input {font-size:14px;border: none;}
select {border: none;}

/*dont print placeholders*/
::-webkit-input-placeholder { /* WebKit browsers */
      color: transparent;
}
:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
color: transparent;
}
::-moz-placeholder { /* Mozilla Firefox 19+ */
color: transparent;
}
:-ms-input-placeholder { /* Internet Explorer 10+ */
color: transparent;
}
/*dont print placeholders*/

#content-inner{border:0px}
</style>
</head>
<%!
	private String replaceAllFields(HttpServletRequest request, String sql) {
		sql = DatabaseAP.parserReplace("demographic", request.getParameter("demographic_no"), sql);
		return sql;
	}
%>
<%!
	private String getDatabaseAPValue(HttpServletRequest request,String apName) {
		DatabaseAP ap = EFormLoader.getInstance().getAP(apName);
		if (ap == null) {
			return "";
		}
		String sql = ap.getApSQL();
		String output = ap.getApOutput();
		if (!StringUtils.isBlank(sql)) {
			sql = replaceAllFields(request,sql);
			ArrayList<String> names = DatabaseAP.parserGetNames(output); // a list of ${apName} --> apName
			sql = DatabaseAP.parserClean(sql); // replaces all other ${apName} expressions with 'apName'
			ArrayList<String> values = EFormUtil.getValues(names, sql);
			if (values.size() != names.size()) {
				output = "";
			} else {
				for (int i = 0; i < names.size(); i++) {
					output = DatabaseAP.parserReplace( names.get(i), values.get(i), output);
				}
			}
		}
		return output;
	}
%>
<%
	String formClass = "EpistaxisEvaluation";
	String formLink = "formEpistaxisEvaluation.jsp";

    UserPropertyDAO userPropertyDAO = SpringUtils.getBean(UserPropertyDAO.class);
    int demoNo = Integer.parseInt(request.getParameter("demographic_no"));
    int formId = Integer.parseInt(request.getParameter("formId"));
    FrmRecord rec = (new FrmRecordFactory()).factory(formClass);
    java.util.Properties props = rec.getFormRecord(LoggedInInfo.getLoggedInInfoFromSession(request),demoNo, formId);
    UserProperty prop = userPropertyDAO.getProp((String) session.getAttribute("user"), UserProperty.PROVIDER_CONSULT_SIGNATURE);
%>
<%!
	private String joinMultipleValueForOccurance(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty("nasalSideBleedingLeft","").indexOf("checked") > -1) {
			sb.append("[Left side]");
		}
		if (props.getProperty("nasalSideBleedingRight","").indexOf("checked") > -1) {
			sb.append("[Right side]");
		}
		if (props.getProperty("nasalSideBleedingUnsure","").indexOf("checked") > -1) {
			sb.append("[the patient is Unsure  as to which side it occurs]");
		}
		
		return sb.toString();
	}
%>
<%!
	private String joinMultipleValueForRecurrent(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty("historyEpistaxisYes","").indexOf("checked") > -1) {
			sb.append("[is]");
		}
		if (props.getProperty("historyEpistaxisNo","").indexOf("checked") > -1) {
			sb.append("[is not]");
		}
		
		return sb.toString();
	}
%>
<%!
	private String joinMultipleValueForRiskFactors(java.util.Properties props) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty("familyHistoryBleedingYes","").indexOf("checked") > -1) {
			sb.append("[family history of bleeding disorder]");
		}
		if (props.getProperty("personalHistoryBleedingYes","").indexOf("checked") > -1) {
			sb.append("[personal history of bleeding disorder]");
		}
		if (props.getProperty("anticoagulantUseYes","").indexOf("checked") > -1) {
			sb.append("[anticoagulant use]");
		}
		if (props.getProperty("historyHypertentionYes","").indexOf("checked") > -1) {
			sb.append("[hypertension]");
		}
		if (props.getProperty("historyHypertentionYes","").indexOf("checked") > -1) {
			sb.append("[hypertension]");
		}
		if (props.getProperty("historyLiverDiseaseFailureYes","").indexOf("checked") > -1) {
			sb.append("[hepatic disease or failure]");
		}
		if (props.getProperty("historyRenalFailureInsufficiencyYes","").indexOf("checked") > -1) {
			sb.append("[renal insufficiency or failure]");
		}
		if (props.getProperty("historyEnvironmentalAllergiesYes","").indexOf("checked") > -1) {
			sb.append("[environmental allergies]");
		}
		if (props.getProperty("anticoagulantUseYes","").indexOf("checked") > -1) {
			sb.append("[use of anticoagulants]");
		}
		return sb.toString();
	}
%>
<%!
	private String joinMultipleValueForSeptalDeviation(java.util.Properties props) {
		final String namePrefix = "noseSeptalDeviation";
		StringBuffer sb = new StringBuffer();
		List<String> values = new ArrayList<String>();
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.startsWith(namePrefix) && value.indexOf("checked") > -1) {
				values.add(key.substring(namePrefix.length()));
			}
		}
		return StringUtils.join(values,",");
	}
%>

<%!
	private String joinMultipleValueForInferiorTurbinates(java.util.Properties props) {
		List<String> values = new ArrayList<String>();
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals("inferiorTurbinatesNormal") && value.indexOf("checked") > -1) {
				values.add("Normal");
			}
			if (key.equals("inferiorTurbinatesHypertrophy") && value.indexOf("checked") > -1) {
				values.add("Hypertrophy");
			}
			if (key.equals("inferiorTurbinatesPallor") && value.indexOf("checked") > -1) {
				values.add("mucosal pallor");
			}
			if (key.equals("inferiorTurbinatesOther") && value.indexOf("checked") > -1) {
				values.add(props.getProperty("inferiorTurbinatesText"));
			}
		}
		return StringUtils.join(values,",");
	}
%>
<%!
	private void outputCheckedValue(java.util.Properties props,List<String> list,String name,String defValue) {
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals(name) && value.indexOf("checked") > -1) {
				list.add(defValue);
			}
		}
	}
%>
<%!
	private String joinMultipleValueForEndoscopy(java.util.Properties props) {
		List<String> values = new ArrayList<String>();
		Iterator keyI = props.keySet().iterator();
		outputCheckedValue(props,values,"endoscopyNasalCavityLeftNormal","[Left: Normal]");
		outputCheckedValue(props,values,"endoscopyNasalCavityLeftTumor","[Left:Tumor]");
		outputCheckedValue(props,values,"endoscopyNasalCavityLeftPNBS","[Left:Posterior Nasal Bleeding Site]");
		outputCheckedValue(props,values,"endoscopyNasalCavityRightNormal","[Right:Normal]");
		outputCheckedValue(props,values,"endoscopyNasalCavityRightTumor","[Right:Tumor]");
		outputCheckedValue(props,values,"endoscopyNasalCavityRightPNBS","[Right:Posterior Nasal Bleeding Site]");
		
		return StringUtils.join(values," ");
	}
%>
<%!
	private String outputText(java.util.Properties props,String name) {
		Iterator keyI = props.keySet().iterator();
		while(keyI.hasNext()) {
			String key = keyI.next().toString();
			String value = props.getProperty(key);
			if (key.equals(name) && value.trim().length() > 0) {
				return value;
			}
		}
		return "";
	}
%>
<%!
	private String joinMultipleValueForImpression(java.util.Properties props) {
		List<String> values = new ArrayList<String>();
		outputCheckedValue(props,values,"impressioinEpistaxis","[Epistaxis]");
		outputCheckedValue(props,values,"impressioinEpistaxisLeft","[Left]");
		outputCheckedValue(props,values,"impressioinEpistaxisRight","[Right]");
		outputCheckedValue(props,values,"impressioinEpistaxisAnterior","[Anterior]");
		outputCheckedValue(props,values,"impressioinEpistaxisPosterior","[Posterior]");
		//values.add(outputText(props,"impression-Rhinitis-other-text"));
		outputCheckedValue(props,values,"impressioinEpistaxisActiveIssue","[Active]");
		outputCheckedValue(props,values,"impressioinEpistaxisRight","[Inactive]");
		//values.add(outputText(props,"impression-Nonallergic-other-text"));
		outputCheckedValue(props,values,"impressioinEpistaxisInactiveIssue","[Inactive]");
		outputCheckedValue(props,values,"impressioinEpistaxisBilateral","[Inactive]");
		//values.add(outputText(props,"impression-SinonasalsTumor-text"));
		outputCheckedValue(props,values,"impressioinNasalTumor","[Nasal tumor]");
		outputCheckedValue(props,values,"impressioinNasalTumorLeft","[Nasal tumor,Left]");
		outputCheckedValue(props,values,"impressioinNasalTumorRight","[Nasal tumor,Right]");
		outputCheckedValue(props,values,"impressioinNasopharyngealTumor","[NasopharyngealTumor]");
		outputCheckedValue(props,values,"impressioinNasopharyngealTumorLeft","[Nasopharyngeal Tumor,Left]");
		outputCheckedValue(props,values,"impressioinNasopharyngealTumorRight","[Nasopharyngeal Tumor,Right]");
		outputCheckedValue(props,values,"impressioinOther","[Other]");
		values.add(outputText(props,"impressioinOtherText"));
		String text = StringUtils.join(values," ");
		return text;
	}
%>
<%!
	private String getSingleValue(Properties props, String radioName) {
		StringBuffer sb = new StringBuffer();
		if (props.getProperty(radioName,"").indexOf("checked") > -1) {
			if (radioName.indexOf("WNL")>-1) {
			 	sb.append("[within normal limits]");
			}
			if (radioName.indexOf("neckNoAdenopathy")> -1) {
				sb.append("[No Adenopathy]");
			} else {
				sb.append("["+radioName+"]");
			}
		}
		
		return sb.toString();
	}
%>
<%!
	private String getOtherText(Properties props,String otherRadioName,String otherText) {
		StringBuffer sb = new StringBuffer();
		
		if (props.getProperty(otherRadioName,"").indexOf("checked") > -1) {
			sb.append("["+props.getProperty(otherText,"")+"]");
		}
		
		return sb.toString();
	}
%>
<body>
<div id="content">    
<center id="Top">
<div id="content-inner">

<p align="center">
<strong><span style="font-size:22px">Dr. Shaun Kilty</span><br>
MD, FRCSC<br>
Otolaryngology, Head &amp; Neck Surgery<br></strong>
<span style="font-size:14px">459 - 737 Parkdale Ave, Ottawa, Ontario K1Y 1J8<br>
Tel: (613)798-5555 xt. 18514<br>
Fax: (613)729-2412</span>
<br/>
<span><a href="http://www.entsinuscare.com" target="_blank">www.entsinuscare.com</a></span>
</p>

<form method="post" action="#" name="form-EMR Letters" id="form-consult1">
	<input type="text" name="today" id="today" placeholder="YYYY-MM-DD" style="border:0px;margin-top:40px;" value="<%=getDatabaseAPValue(request,"today") %>" oscardb=today>
	<input type="hidden" name="demographic_no"
		   value="<%= props.getProperty("demographic_no", "0") %>" />
	<input type="hidden" name="formCreated"
		   value="<%= props.getProperty("formCreated", "") %>" />
	<input type="hidden" name="form_class" value="<%=formClass%>" />
	<input type="hidden" name="form_link" value="<%=formLink%>" />
	<input type="hidden" name="formId" value="<%=formId%>" />

	<div id="referring-doctor" style="margin-top:30px">
	<input type="text" name="referring-doctor" id="referring-doctor" value="<%=getDatabaseAPValue(request,"referral_name") %>" placeholder="Referring Doctor" style="border:0px;" name="referral-name" oscardb=referral_name><br>
	<input type="text" name="referring-doctor-address" id="referring-doctor-address" value="<%=getDatabaseAPValue(request,"referral_address") %>" placeholder="Referring Doctor Address" style="border:0px;width:400px" name="referral-address" oscardb=referral_address><br>
	<input type="text" name="referring-doctor-phone" id="referring-doctor-phone" value="<%=getDatabaseAPValue(request,"referral_phone") %>" placeholder="Phone" style="border:0px;width:200px" name="referral-phone" oscardb=referral_phone><br>
	<input type="text" name="referring-doctor-fax" id="referring-doctor-fax" value="<%=getDatabaseAPValue(request,"referral_fax") %>" placeholder="Fax" style="border:0px;width:200px" name="referral-fax" oscardb=referral_fax><br>
	</div>
        

	<p>Dear Dr. <input type="text" placeholder="Doctor Name" name="referring-doctor-dear" style="border:0px;" value="<%=getDatabaseAPValue(request,"referral_name") %>" oscardb=referral_name><br></p>
	<p>RE: <input type="text" placeholder="Patient Name" style="border:0px;width:160px" name="patient-name-re" value="<%=getDatabaseAPValue(request,"first_last_name") %>" oscardb=first_last_name> <span style="padding-left:20px">
	D.O.B: <input type="text" name="dob" id="dob" placeholder="YYYY-MM-DD" style="border:0px;margin-top:5px;" value="<%=getDatabaseAPValue(request,"dobc2") %>" oscardb=dobc2></span> </p>


 


<p style="margin: 0px;">Thank you for this referral. This patient was seen for epistaxis. It has bean occuring on the <p style="margin:0px;margin-left: 30px;"><%=joinMultipleValueForOccurance(props) %>.</p>
The epistaxis <p style="margin:0px;margin-left: 30px;"><%=joinMultipleValueForRecurrent(props) %> recurrent. </p> This This patient has the following risk factors:
<p style="margin:0px;margin-left: 30px;"><%=joinMultipleValueForRiskFactors(props) %>.</p><br>
<p><strong>Examination:<br></strong>
<span class="nav_button">Otoscopy: </span>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"earAD-WNL") %><%=getOtherText(props,"earAD-Other","earAD-Text")%><%=getSingleValue(props,"earAS-WNL")%><%=getOtherText(props,"earAS-Other","earAS-Text")%>" /></span> bilaterally.<br>
<span class="nav_button">Oral cavity and oropharyx: </span>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"oralWNL")%><%=getSingleValue(props,"telangiectasia")%><%=getOtherText(props,"oralOther","oralText") %>" /></span><br>
<span class="nav_button">Neck: </span>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"neckNoAdenopathy") %><%=getOtherText(props,"neckNoAdenopathyOther","neckNoAdenopathyText")%>" /></span><br>
<span class="nav_button">Cranial nerve screen: </span>
<span class="nav_button"><input type="text" value="<%=getSingleValue(props,"cranialWNL") %><%=getOtherText(props,"cranialOther","cranialText")%>" /></span><br>
<span class="nav_button">Anterior rhinoscopy: </span>
<%-- <span class="nav_button"><input type="text" value="<%= props.getProperty("anterior-text1", "") %>" /></span> --%><br>
Septal Deviation: <%= joinMultipleValueForSeptalDeviation(props) %><br>
Inferior Turbinates: <input type="text" style="width: 300px;" value="<%= joinMultipleValueForInferiorTurbinates(props) %>" />
<br>
Endoscopy:<input type="text" style="width: 500px;" value="<%= joinMultipleValueForEndoscopy(props) %>">
<br>
CT scan of the sinuses: <input type="text" value="<%=joinMultipleValueForSeptalDeviation(props)%>">
</p>
<p><strong>Impression: <br></strong>
This patient has <input type="text" style="width: 500px;" value="<%=joinMultipleValueForImpression(props) %>"> 
</p>
<div id="planArea">
<p>
	<strong>Plan<br></strong>
	
	<span id="silverNiterateCASBL"><input type="checkbox" name="silverNiterateCASBL" <%= props.getProperty("silverNiterateCASBL", "") %>/>Silver nitrate cautery of anterior septal bleeding location</span><br>
	<span id="surgicalAnteriorNasalPack"><input type="checkbox" name="surgicalAnteriorNasalPack" <%= props.getProperty("surgicalAnteriorNasalPack", "") %>/><span style="font-family: Arial">Surgicel anterior nasal pack</span><br>
	<span id="bactrobanOintmentBid"><input type="checkbox" name="bactrobanOintmentBid" <%= props.getProperty("bactrobanOintmentBid", "") %>/>Antibiotic</span>Bactroban ointment bid in affected naris for 1/52, then OD for 1/52<br>
	<span id="flosealTreatmentBleedingSite"><input type="checkbox" name="flosealTreatmentBleedingSite" <%= props.getProperty("flosealTreatmentBleedingSite", "") %>/></span><span style="font-family: Arial">Floseal treatment of bleeding site </span><br>
	<span id="merocelPack"><input type="checkbox" name="merocelPack" <%= props.getProperty("merocelPack", "") %>/>Merocel pack: </span>
	<span id="merocelPackLeft"><input type="checkbox" name="merocelPackLeft" <%= props.getProperty("merocelPackLeft", "") %>/>Left </span>
	<span id="merocelPackRight"><input type="checkbox" name="merocelPackRight" <%= props.getProperty("merocelPackRight", "") %>/>Right</span><br>
	<span id="posteriorNasalPacking"><input type="checkbox" name="posteriorNasalPacking" <%= props.getProperty("posteriorNasalPacking", "") %>/>Posterior Nasal Packing:</span>
	
	<span id="posteriorNasalPackingLeft"><input type="checkbox" name="posteriorNasalPackingLeft" <%= props.getProperty("posteriorNasalPackingLeft", "") %>/>Left</span>
	<span id="posteriorNasalPackingRight"><input type="checkbox" name="posteriorNasalPackingRight" <%= props.getProperty("posteriorNasalPackingRight", "") %>/>Right</span><br>
	<span id="surgicalTreatment"><input type="checkbox" name="surgicalTreatment" <%= props.getProperty("surgicalTreatment", "") %>/>Surgical treatment:</span>
	<span id="surgicalTreatmentSuctionCautery"><input type="checkbox" name="surgicalTreatmentSuctionCautery" <%= props.getProperty("surgicalTreatmentSuctionCautery", "") %>/>Suction cautery</span>
	<span id="surgicalTreatmentExcision"><input type="checkbox" name="surgicalTreatmentExcision" <%= props.getProperty("surgicalTreatmentExcision", "") %>/>Excision</span>
	<span id="surgicalTreatmentSPALigation"><input type="checkbox" name="surgicalTreatmentSPALigation" <%= props.getProperty("surgicalTreatmentSPALigation", "") %>/>SpA Ligation</span><br>
	<span id="septodermoplasty"><input type="checkbox" name="septodermoplasty" <%= props.getProperty("septodermoplasty", "") %>/>Septodermoplasty:</span>
	<span id="septodermoplastyLeft"><input type="checkbox" name="septodermoplastyLeft" <%= props.getProperty("septodermoplastyLeft", "") %>/>Left</span>
	<span id="septodermoplastyRight"><input type="checkbox" name="septodermoplastyRight" <%= props.getProperty("septodermoplastyRight", "") %>/>Right</span><br>
	<span id="surgicalRisks"><input type="checkbox" name="surgicalRisks" <%= props.getProperty("surgicalRisks", "") %>/>Surgical risks:</span>
	<span id="surgicalRisksText"><input class="underline" style="width: 300px;" type="text" name="surgicalRisksText" value="<%= props.getProperty("surgicalRisksText", "") %>" /></span><br>
	<span id="angiography"><input type="checkbox" name="angiography" <%= props.getProperty("angiography", "") %>/>Angiography</span><br>
	<span id="tumorBiopsy"><input type="checkbox" name="tumorBiopsy" <%= props.getProperty("tumorBiopsy", "") %>/>Tumor biopsy</span><br>
	<span id="ctScanParanasalSinuses"><input type="checkbox" name="ctScanParanasalSinuses" <%= props.getProperty("ctScanParanasalSinuses", "") %>/><strong>CT scan</strong> of the paranasal sinuses</span><br>
	<span id="planOther"><input type="checkbox" name="planOther" <%= props.getProperty("planOther", "") %>/>Other</span>
	<span id="planOtherText"><input class="underline" style="width: 300px;" type="text" name="planOtherText" value="<%= props.getProperty("planOtherText", "") %>" /></span><br>
	<span id="returnToClinic">
	<input type="checkbox" name="returnToClinic" <%= props.getProperty("returnToClinic", "") %>/><span style="font-family: Arial">Return to clinic: </span><span style="font-size: 10.0pt; font-family: Arial">
	<input class="underline" style="width: 30px;" type="text" name="returnToClinicNumberOfMonths" value="<%= props.getProperty("returnToClinicNumberOfMonths", "") %>" /> months</span><br>
	</span>
	<span id="noFollowUp"><input type="checkbox" name="noFollowUp" <%= props.getProperty("noFollowUp", "") %>/>No follow-up</span><br>
	<span id="userInput3"><input type="checkbox" name="userInput3" <%= props.getProperty("userInput3", "") %>/>Other</span>
	<span id="userInput3Text"><input class="underline" style="width: 640px;" type="text" name="userInput3Text" value="<%= props.getProperty("userInput3Text", "") %>" /></span><br>
	</p>
</div>
	
<p><span style="font-size: 11.0pt; line-height: 115%; font-family: Calibri">&nbsp;</span>Sincerely,</p>	
<p>
	<% if (prop != null && StringUtils.isNotEmpty(prop.getValue())) { %>
	<img src="<%= request.getContextPath() %>/eform/displayImage.do?imagefile=<%= prop.getValue() %>"/>
	<% } %>
</p>
<p><span style="font-size: 11.0pt; line-height: 115%; font-family: Calibri">&nbsp;</span>Dr. Shaun Kilty MD, FRCSC</p>
<!--<p><%= props.getProperty("signatures", "") %></p>-->

</center>
</div><!--content-->

<br><br><br>


<div class="container-controls DoNotPrint">
   <div class="controls">
<input value="Print" class="button-slim" name="PrintButton" type="button" onclick="window.print();" title="Print - HTML print out">


<a href="#Top" class="top-link"><strong>Top</strong></a>
   </div>

   <div class="background"></div>
</div>

</form>
<script type="text/javascript">
jQuery(window).load(function() {
	var plan_array = [
    	{ 
    		module:'medical',
    		options:['silverNiterateCASBL','surgicalAnteriorNasalPack','bactrobanOintmentBid','flosealTreatmentBleedingSite','merocelPack','merocelPackLeft','merocelPackRight','posteriorNasalPacking'
    		         ,'posteriorNasalPackingLeft'.'posteriorNasalPackingRight'.'surgicalTreatment'.'surgicalTreatmentSuctionCautery'
    		         ,'surgicalTreatmentExcision','surgicalTreatmentSPALigation','septodermoplasty','septodermoplastyLeft','septodermoplastyRight'
    		         ,'surgicalRisks','surgicalRisksText','angiography','tumorBiopsy','ctScanParanasalSinuses','planOther','planOtherText','returnToClinicNumberOfMonths','noFollowUp','noFollowUpText']
    	},
    ];
    var totalCount = 0;
    for(var i=0;i<plan_array.length;i++) {
        var mod = plan_array[i];
        var modCount = 0 ;
        var opts = mod.options;
        for(var j=0;j<opts.length;j++) {
            var obj = $('#planArea input[name="' + opts[j] + '"]');
            if (obj.is(":checked")) {
                modCount += 1;
                totalCount += 1;
            } else {
                $('#' + opts[j]).hide();
            }
        }
        if (modCount == 0) {
            $('#' + mod.module).hide();
        }
    }
    if (totalCount == 0) {
        $('#planArea').hide();
    }
});
</script>
</body>
</html>