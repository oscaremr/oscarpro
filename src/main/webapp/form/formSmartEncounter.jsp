<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName2$ = (String) session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed = true;
%>
<security:oscarSec roleName="<%=roleName2$%>" objectName="_form" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_form");%>
</security:oscarSec>
<% if (!authed) { return; } %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib prefix="csrf" uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld" %>
<%@ page import="oscar.form.*"%>
<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="java.util.Properties" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="oscar.form.dao.SmartEncounterTemplateDao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="oscar.form.model.SmartEncounterTemplate" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="oscar.form.model.SmartEncounterFooter" %>
<%@ page import="oscar.form.model.SmartEncounterHeader" %>
<%@ page import="oscar.form.model.SmartEncounterShortCode" %>
<%@ page import="oscar.form.dao.SmartEncounterShortCodeDao" %>
<%@ page import="org.oscarehr.common.dao.ProfessionalSpecialistDao" %>
<%@ page import="org.oscarehr.common.model.ProfessionalSpecialist" %>
<%@ page import="org.oscarehr.common.dao.DemographicExtDao" %>
<%@ page import="org.oscarehr.common.model.DemographicExt" %>
<%@ page import="org.oscarehr.util.MiscUtils" %>
<%@ page import="org.apache.log4j.Logger" %>
<%@ page import="oscar.form.dao.SmartEncounterFooterDao" %>
<%@ page import="oscar.form.dao.SmartEncounterHeaderDao" %>
<%@ page import="oscar.form.dao.SmartEncounterProviderPreferenceDao" %>
<%@ page import="oscar.form.model.SmartEncounterProviderPreference" %>
<%@ page import="oscar.form.model.SmartEncounterTemplateImage" %>
<%@ page import="org.oscarehr.common.dao.PropertyDao" %>
<%@ page import="org.oscarehr.common.model.Property" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="oscar.form.pageUtil.SmartEncounterUtil" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ page import="oscar.OscarProperties" %>
<%
    Logger logger = MiscUtils.getLogger();
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    int formId = Integer.parseInt(request.getParameter("formId"));
    int demographicNo = Integer.parseInt(request.getParameter("demographic_no"));
    String appointmentNo = StringUtils.trimToEmpty(request.getParameter("appointmentNo"));
    String errorMessage = request.getParameter("error_message");
    String faxRecipients = request.getParameter("faxRecipients");
    Boolean faxSent = Boolean.valueOf(request.getParameter("faxSuccess"));
    FrmRecord rec = (new FrmRecordFactory()).factory("SmartEncounter");
    Properties formProperties = rec.getFormRecord(LoggedInInfo.getLoggedInInfoFromSession(request), demographicNo, formId);
    String deltaText = formProperties.getProperty("deltaText", "");
    // replace image placeholders with proper urls
	for (Object o : formProperties.keySet()) {
		String key = (String) o;
		if (key.startsWith("image_")) {
			deltaText = deltaText.replaceAll("\\$\\{" + key + "}", request.getContextPath() + "/eform/displayImage.do?smartEncounterFormImage=true&imagefile=" + formProperties.getProperty(key));
		}
	}

    SmartEncounterTemplateDao smartEncounterTemplateDao = SpringUtils.getBean(SmartEncounterTemplateDao.class);
    List<SmartEncounterTemplate> templateList = smartEncounterTemplateDao.findAllActiveOrderByName();
    SmartEncounterShortCodeDao smartEncounterShortCodeDao = SpringUtils.getBean(SmartEncounterShortCodeDao.class);
    List<SmartEncounterShortCode> shortCodeList = smartEncounterShortCodeDao.findAll(null, null);
    SmartEncounterHeaderDao smartEncounterHeaderDao = SpringUtils.getBean(SmartEncounterHeaderDao.class);
    List<SmartEncounterHeader> headerList = smartEncounterHeaderDao.findAllOrderByName();
    SmartEncounterFooterDao smartEncounterFooterDao = SpringUtils.getBean(SmartEncounterFooterDao.class);
    List<SmartEncounterFooter> footerList = smartEncounterFooterDao.findAllOrderByName();

    PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
    Property defaultSmartEncounterTemplate = propertyDao.checkByName("default_smart_encounter_template");
    Property defaultSmartEncounterHeader = propertyDao.checkByName("default_smart_encounter_header");
    String headerIdInDB = formProperties.getProperty("headerId", "");
	String defaultTemplateValue = formId == 0 && defaultSmartEncounterTemplate != null
			&& defaultSmartEncounterTemplate.getValue() != null
			? Encode.forJavaScriptBlock(defaultSmartEncounterTemplate.getValue())
			: "";
    if (defaultSmartEncounterHeader == null || defaultSmartEncounterHeader.getValue().equalsIgnoreCase("None")) {
        defaultSmartEncounterHeader = new Property();
        defaultSmartEncounterHeader.setValue(null);
    }

    if (headerIdInDB != null && headerIdInDB.trim().length() > 0 && !headerIdInDB.equalsIgnoreCase("0")) {
        defaultSmartEncounterHeader = new Property();
        defaultSmartEncounterHeader.setValue(headerIdInDB);
    }

    Property defaultSmartEncounterFooter = propertyDao.checkByName("default_smart_encounter_footer");
    String footerIdInDB = formProperties.getProperty("footerId", "");
    if (defaultSmartEncounterFooter == null || defaultSmartEncounterFooter.getValue().equalsIgnoreCase("None"))	{
        defaultSmartEncounterFooter = new Property();
        defaultSmartEncounterFooter.setValue(null);
    }

    if (footerIdInDB != null && footerIdInDB.trim().length() > 0 && !footerIdInDB.equalsIgnoreCase("0")) {
        defaultSmartEncounterFooter = new Property();
        defaultSmartEncounterFooter.setValue(footerIdInDB);
    }
    
    // get Family Doctor and Referral Doctor
    ProfessionalSpecialist familyDoctor = null;
    ProfessionalSpecialist referralDoctor = null;
    ProfessionalSpecialistDao professionalSpecialistDao = SpringUtils.getBean(ProfessionalSpecialistDao.class);
    DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
    DemographicExt familyDoctorExt = demographicExtDao.getDemographicExt(demographicNo, ProfessionalSpecialist.DemographicRelationship.FAMILY_DOCTOR.getDemographicExtKeyVal());
    DemographicExt referralDoctorExt = demographicExtDao.getDemographicExt(demographicNo, ProfessionalSpecialist.DemographicRelationship.REFERRAL_DOCTOR.getDemographicExtKeyVal());
    if (familyDoctorExt != null) {
        try {
            familyDoctor = professionalSpecialistDao.find(Integer.valueOf(familyDoctorExt.getValue()));
        } catch (NumberFormatException e) { logger.warn("Invalid professional specialist id: " + familyDoctorExt.getValue()); }
    }
    if (referralDoctorExt != null) {
        try {
            referralDoctor = professionalSpecialistDao.find(Integer.valueOf(referralDoctorExt.getValue()));
        } catch (NumberFormatException e) { logger.warn("Invalid professional specialist id: " + referralDoctorExt.getValue()); }
    }
    
    // get providers header and footer
	SmartEncounterProviderPreference smartEncounterProviderPreference = null;
	if (SystemPreferencesUtils.isPreferenceValueEquals("smart_template_provider_header", "true")) {
		SmartEncounterProviderPreferenceDao smartEncounterProviderPreferenceDao = SpringUtils.getBean(SmartEncounterProviderPreferenceDao.class);
		smartEncounterProviderPreference = smartEncounterProviderPreferenceDao.findByProviderNo(loggedInInfo.getLoggedInProviderNo());
	}
	
	boolean pasteFaxNote = false;
	if (SystemPreferencesUtils.isPreferenceValueEquals("echart_paste_fax_note", "true")) {
		pasteFaxNote = true;
	}
	
	String timeStamp = new SimpleDateFormat("dd-MMM-yyyy hh:mm a").format(Calendar.getInstance().getTime());

	String fileUploadErrorMessage = SmartEncounterUtil.createErrorMessage(request);
	if (StringUtils.isNotBlank(fileUploadErrorMessage)) {
		out.println("<script>alert('" + fileUploadErrorMessage + "');</script>");
	}
	boolean attachmentManagerSmartFormEnabled = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("attachment_manager.smart_form.enabled", false);
%>
<html>
<head>
<meta charset="utf-8">
<title>Smart Encounter Form</title>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/quill/quill.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-editor.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-placeholder.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-block-placeholder.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-image-placeholder.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-register-inline-styles.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-ui-1.10.2.custom.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/share/javascript/eforms/APCache.js"></script>
    
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/quill/quill.snow.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/javascript/forms/forms-quill-template-editor.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/js/jquery_css/smoothness/jquery-ui-1.7.3.custom.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/yui/css/fonts-min.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/yui/css/autocomplete.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/css/demographicProviderAutocomplete.css"/>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/bulma/bulma-trimmed.css"/>
    <script type="text/javascript">
        function init(defaultTemplate) {
            // resize window
            window.resizeTo(1150, 800);
            if (defaultTemplate && defaultTemplate !== 'None') {
                setTemplate(defaultTemplate);
            }
        }
        
        <%
        List<String> jsTemplates = new ArrayList<String>();
        for (SmartEncounterTemplate template : templateList) {
            List<String> imageJson = new ArrayList<String>();
            for (SmartEncounterTemplateImage smartEncounterTemplateImage : template.getPlaceholderImageList()) {
                imageJson.add("'" + smartEncounterTemplateImage.getName() + "' : '" + smartEncounterTemplateImage.getValue() + "'");
            }
            String templateDeltaText = template.getTemplate();
			// replace image placeholders with proper urls
			for (SmartEncounterTemplateImage placeHolderImage : template.getPlaceholderImageList()) {
			    templateDeltaText = templateDeltaText.replaceAll("\\$\\{" + placeHolderImage.getName() + "}", request.getContextPath() + "/eform/displayImage.do?smartEncounterFormImage=true&imagefile=" + placeHolderImage.getValue());
			}
            jsTemplates.add("['" + template.getId() + "', { 'name':'" + Encode.forJavaScript(template.getName()) + "', 'template': " + templateDeltaText + ", 'headerId': '" + template.getHeaderId() + "', 'footerId': '" + template.getFooterId()+ "'}]");
        }
        %>
        let templatesMap = new Map([<%=StringUtils.join(jsTemplates, ",")%>]);
        <%
        List<String> jsShortCodes = new ArrayList<String>();
        for (SmartEncounterShortCode shortCode : shortCodeList) {
            jsShortCodes.add("['" + Encode.forJavaScriptBlock(shortCode.getName().toLowerCase()) + "', " + shortCode.getText() + "]");
        }
        %>
        let shortCodesMap = new Map([<%=StringUtils.join(jsShortCodes, ",")%>]);

        <%
        List<String> jsHeaders = new ArrayList();
        Map<Integer, SmartEncounterHeader> smartEncounterHeaderMap = new HashMap();

        for (SmartEncounterHeader header : headerList) {
            jsHeaders.add("['" + header.getId() + "', { 'name':'" + Encode.forJavaScriptBlock(header.getName()) +
                    "', 'text':" + header.getText() + ", 'createDate': " + header.getCreateDate().getTime() + "}]");
            smartEncounterHeaderMap.put(header.getId(), header);
        }
        %>
        let headersMap = new Map([<%= StringUtils.join(jsHeaders, ",") %>]);


        <%
        String headerHtml = formProperties.getProperty("headerText", "");
        if (headerHtml != null && headerHtml.length() > 0) {
        	headerHtml = headerHtml.replaceAll("\r\n","\\\\r\\\\n");
        }
        if ((formProperties.getProperty("headerText", "") == null
                || formProperties.getProperty("headerText", "").trim().length() == 0 )
                && (defaultSmartEncounterTemplate == null
                || defaultSmartEncounterTemplate.getValue() == null
                || defaultSmartEncounterTemplate.getValue().equalsIgnoreCase("None"))
                && defaultSmartEncounterHeader != null
                && defaultSmartEncounterHeader.getValue() != null && smartEncounterHeaderMap.containsKey(Integer.parseInt(defaultSmartEncounterHeader.getValue()))) {
            headerHtml= smartEncounterHeaderMap.get(Integer.parseInt(defaultSmartEncounterHeader.getValue())).getText();
        }

        String selectedHeaderId = "0";
        if (defaultSmartEncounterHeader != null
                && defaultSmartEncounterHeader.getValue() != null
                && smartEncounterHeaderMap.containsKey(Integer.parseInt(defaultSmartEncounterHeader.getValue()))) {
        	selectedHeaderId = defaultSmartEncounterHeader.getValue();
        }
		
        %>
            let printoutHeaderHtml = '<%= headerHtml %>';
            let selectedHeaderId = '<%= selectedHeaderId %>';
        <%
        List<String> jsFooters = new ArrayList();
        Map<Integer, SmartEncounterFooter> smartEncounterFooterMap = new HashMap();
        for (SmartEncounterFooter footer : footerList) {
            jsFooters.add("['" + footer.getId() + "', { 'name':'" + Encode.forJavaScriptBlock(footer.getName()) +
                    "', 'text':" + footer.getText() + ", 'createDate': " + footer.getCreateDate().getTime() + "}]");
            smartEncounterFooterMap.put(footer.getId(), footer);
        }
        %>
        let footersMap = new Map([<%= StringUtils.join(jsFooters, ",") %>]);

        <%
        String footerHtml = formProperties.getProperty("footerText", "");
        if (footerHtml != null && footerHtml.length() > 0) {
        	footerHtml = footerHtml.replaceAll("\r\n","\\\\r\\\\n");
        }
        if ((formProperties.getProperty("footerText", "") == null
                || formProperties.getProperty("footerText", "").trim().length() == 0 )
                && (defaultSmartEncounterTemplate == null
                || defaultSmartEncounterTemplate.getValue() == null
                || defaultSmartEncounterTemplate.getValue().equalsIgnoreCase("None"))
                && defaultSmartEncounterFooter != null
                && defaultSmartEncounterFooter.getValue() != null && smartEncounterFooterMap.containsKey(Integer.parseInt(defaultSmartEncounterFooter.getValue()))) {
            footerHtml = smartEncounterFooterMap.get(Integer.parseInt(defaultSmartEncounterFooter.getValue())).getText();
        }

        String selectedFooterId = "0";
        if (defaultSmartEncounterFooter != null
               && defaultSmartEncounterFooter.getValue() != null
               && smartEncounterFooterMap.containsKey(Integer.parseInt(defaultSmartEncounterFooter.getValue()))) {
            selectedFooterId = defaultSmartEncounterFooter.getValue();
        }

        %>
        let printoutFooterHtml = '<%= footerHtml %>';
        let selectedFooterId = '<%= selectedFooterId %>';
        let demographicPlaceholderValues = <%=formProperties.getProperty("resolvedPlaceholders", "[]")%>
    
    </script>
</head>
<body class="smart-template-layout-main">
    <html:form styleId="formSmartEncounter" action="/form/FormSmartEncounterAction" styleClass="container is-fluid">
		<div class="main">
			<div class="body-inner-section smart-encounter-body">
				<div class="columns is-side-gap is-multiline flex-must">
					<div class="column">
						<input type="hidden" id="method" name="method" value="">
						<input type="hidden" name="<csrf:tokenname/>" value="<csrf:tokenvalue/>">
						<input type="hidden" name="formId" value="<%=formId%>">
						<input type="hidden" id="demographicNo" name="demographicNo" value="<%=demographicNo%>">
						<input type="hidden" name="appointmentNo" value="<%=appointmentNo%>">
						<input type="hidden" id="templateUsed" name="templateUsed" value="<%=Encode.forHtmlAttribute(formProperties.getProperty("templateUsed", ""))%>">
						<input type="hidden" id="deltaText" name="deltaText" value="<%=Encode.forHtmlAttribute(deltaText)%>">
						<input type="hidden" id="htmlText" name="htmlText" value="<%=Encode.forHtmlAttribute(formProperties.getProperty("htmlText", ""))%>">
						<input type="hidden" id="plainText" name="plainText" value="<%=Encode.forHtmlAttribute(formProperties.getProperty("plainText", ""))%>">
						<input type="hidden" id="headerText" name="headerText" value="<%=Encode.forHtmlAttribute(formProperties.getProperty("headerText", ""))%>">
						<input type="hidden" id="footerText" name="footerText" value="<%=Encode.forHtmlAttribute(formProperties.getProperty("footerText", ""))%>">
						<input type="hidden" id="headerId" name="headerId" value="<%=Encode.forHtmlAttribute(formProperties.getProperty("headerId", ""))%>">
						<input type="hidden" id="footerId" name="footerId" value="<%=Encode.forHtmlAttribute(formProperties.getProperty("footerId", ""))%>">
						<input type="hidden" id="popUpfooter" name="popUpfooter" value="">
						<input type="hidden" id="popUpheader" name="popUpheader" value="">
						<input type="hidden" id="fieldsToPullListString" name="fieldsToPullListString" value="">
						<input type="hidden" id="proContextPath"
						       value="<%=OscarProperties.getKaiemrDeployedContext()%>">
						<input type="hidden" id="providerNo" name="providerNo"
						       value="<%=loggedInInfo.getLoggedInProviderNo()%>">
						<span class="title">Smart Encounter</span>
					</div>
					<div class="column is10-md-2">
						<div class="field is-horizontal">
							<div class="control">
								<div class="select is-primary">
									<select id="template-select" name="templateSelect" onchange="setTemplate(this.value)">
										<option value="">Select Template:</option>
										<% for (SmartEncounterTemplate template : templateList) { %>
										<option value="<%=template.getId()%>"><%=template.getName()%></option>
										<% } %>
									</select>
								</div>
							</div>
							<div class="control">
								<div class="select is-primary">
									<select id="header-select" name="headerSelect" onchange="setHeader(this.value)">
										<option value="">Select Header:</option>
										<% for (SmartEncounterHeader header : headerList) {
											boolean isSmartEncounterHeaderSelected =
													defaultSmartEncounterHeader.getValue() != null
													&& header.getId().equals(Integer.valueOf(defaultSmartEncounterHeader.getValue()));
											String smartEncounterHeaderSelectedText = isSmartEncounterHeaderSelected ? "selected=\"selected\"" : "";
										%>

											<option value="<%= header.getId() %>" <%= smartEncounterHeaderSelectedText %>>
											<%= header.getName() %></option>
										<% } %>
									</select>
								</div>
							</div>
							<div class="control">
								<div class="select is-primary">
									<select id="footer-select" name="footerselect" onchange="setFooter(this.value)">
										<option value="">Select Footer:</option>
										<% for (SmartEncounterFooter footer : footerList) {
											boolean isSmartEncounterFooterSelected =
													defaultSmartEncounterFooter.getValue() != null
													&& footer.getId().equals(Integer.valueOf(defaultSmartEncounterFooter.getValue()));
											String smartEncounterFooterSelectedText = isSmartEncounterFooterSelected ? "selected=\"selected\"" : "";
										%>
											<option value="<%= footer.getId() %>" <%= smartEncounterFooterSelectedText %>>
											<%= footer.getName() %>
											</option>
										<% } %>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="column is-pulled-right" style="color: green;">
						<%=faxSent ? "Fax sent successfully" : ""%>
					</div>
					<div class="column is-pulled-right">
						Referring: <%=referralDoctor != null ? Encode.forHtml(referralDoctor.getFormattedName()) : "(none)"%><br/>
						Family: <%=familyDoctor != null ? Encode.forHtml(familyDoctor.getFormattedName()) : "(none)"%>
					</div>
				</div>
				<div class="columns is-side-gap is-multiline smart-template-layout mt-2">
					<div class="column is12-xs-8 is12-sm-10 is10-md-8">
						<div class="panel-1 page-content smart-template-inner-panel">
							<div class="panel-body">
								<div class="columns">
									<div class="column is10-md-10">
										<div class="field">
											<label class="label">Document Name:</label>
											<div class="control">
												<input type="text" class="input smart-template-page-width" id="documentName" name="documentName" value="<%=Encode.forHtmlAttribute(formProperties.getProperty("documentName", ""))%>"/>
											</div>
										</div>
									</div>
								</div>
								<div class="columns">
									<div class="column is10-md-10 smart-template-page-width" style="color: black">
										<div id="quillEditor" class="ql-white-background"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="column is12-xs-4 is12-sm-2 is10-md-2">
						<div class="panel-1 smart-encounter-panel1">
							<div class="panel-body">
								<% if (errorMessage != null) { %>
								<div id="error-messages" class="alert alert-danger">
									<%=errorMessage%>
								</div>
								<% } %>
								<input class="outter outter-3 smart-encounter-input-panel1" role="group">
									<input type="button" class="button is-green is-small is-block" onclick="printCommonMacro('label');" value="Patient Block"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('social_family_historyc');" value="Social History"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('medical_history');" value="Medical History"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('ongoingconcerns');" value="Ongoing Concerns"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('reminders');" value="Reminders"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('allergies_des');" value="Allergies"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('druglist_trade');" value="Prescriptions"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('other_medications_history'); " value="Other Medications"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('family_history_json'); " value="Family History"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('first_last_name');" value="Patient Name"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('ageComplex');" value="Patient Age"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('label');" value="Patient Label"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('sex');" value="Patient Gender"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('current_user');" value="Current User"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('doctor');" value="Doctor (MRP)"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="printCommonMacro('_recent_note')" value="RecentNote"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="popupPage(700,1200,'../admin/smartReportHeaderFooterEdit.jsp?mode=footer');return false;" value="Custom Footer"/><br/>
									<input type="button" class="button is-green is-small is-block mt-1" onclick="popupPage(700,1200,'../admin/smartReportHeaderFooterEdit.jsp?mode=header');return false;" value="Custom Header"/><br/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<% if (attachmentManagerSmartFormEnabled) {
          String attachmentWidgetJson = formProperties.getProperty("attachments", "");
          %>
				<div id="attachmentManagerWidgetOnSmartEncounter" style="padding: 0px 100px 10px;">
					<%@ include file="/well/feature/attachment-manager/attachmentManagerWidget.jspf" %>
				</div>
				<% } %>
				<div class="columns is-side-gap is-multiline smart-template-layout mt-2">
					<div class="column is10-10 is10-md-10">
						<div class="panel-1 page-content" style="padding: 20px;">
							<div class="panel-body">
								<div class="columns is-multiline flex-must">
									<div class="column is10-md-6">
										<label class="label">Fax Recipients:</label>
										<div class="field is-grouped">
											<div class="control">
												<input type="text" class="input is-small" style="width: 180px" id="faxRecipientsSearch" name="faxRecipientsSearch"/>
											</div>
											<div class="control">
												<input type="button" class="button is-green is-small" value="Add Fax Number" onclick="onAddFaxNumberRecipient()"/>
											</div>
										</div>
										<ul id="faxRecipientsList">
											<% if (!StringUtils.isEmpty(faxRecipients)) {
												String[] recipients = StringUtils.split(
														StringUtils.substring(faxRecipients, 1,
																faxRecipients.length() - 1),
														",");
												for (String recipient : recipients) {
													String faxRecipient = StringUtils.trimToEmpty(
															recipient); %>
											<li><%= faxRecipient %> (<%= faxRecipient %>)
												<a onclick="removeFaxRecipient(this)">remove</a>
												<input type="hidden" class="fax-recipient"
													   name="faxRecipient"
													   value="<%= faxRecipient %>"/>
												<input type="hidden" name="faxRecipientName"
													   value="<%= faxRecipient %>"/>
											</li>
											<% } %>
											<% } %>
										</ul>
									</div>
									<div class="column is10-md-4">
										<div class="buttons">
											<input type="button" class="button is-green is-small" value="Save" onclick="onClickSave()"/>
											<input type="button" class="button is-green is-small" value="Print" onclick="onClickPrint()" <%=formId == 0 ? "disabled" : ""%>/>
											<input type="button" class="button is-green is-small" value="Save and Print" onclick="onClickSaveAndPrint()" <%=formId == 0 ? "disabled" : ""%>/>
											<input type="button" class="button is-green is-small" value="Fax" onclick="onClickFax()" <%=formId == 0 ? "disabled" : ""%>/>
											<input type="button" class="button is-green is-small" value="Fax and Print" onclick="onClickFaxAndPrint()" <%=formId == 0 ? "disabled" : ""%>/>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </html:form>
    <script type="text/javascript">
        let csrfToken = { name: '<csrf:tokenname/>', value: '<csrf:tokenvalue/>'};
        let methodElement = document.getElementById('method');
        let existingDelta = <%=deltaText.isEmpty() ? "{ops: [] }" : "JSON.parse('" + Encode.forJavaScriptBlock(deltaText) + "')"%>;
        let quillOptions = { demographicNo: '<%=demographicNo%>', appointmentNo: '<%=appointmentNo%>', oscarContext: '<%=request.getContextPath()%>', csrfToken: csrfToken, formId: '<%=formId%>' };
        let quillEditor = new TemplatePlaceholderQuill('quillEditor', shortCodesMap, demographicPlaceholderValues, quillOptions);
        if (printoutFooterHtml != '') {
            document.getElementById('footerText').value = printoutFooterHtml;
        }
        if (printoutHeaderHtml != '') {
            document.getElementById('headerText').value = printoutHeaderHtml;
        }
        if (selectedHeaderId != '0') {
            setHeader(selectedHeaderId);
        }
        if (selectedFooterId != '0') {
            setFooter(selectedFooterId);
        }
        if (existingDelta.ops.length > 0) {
            quillEditor.setContents(existingDelta);
            quillEditor.updateDomWithResolvedPlaceHolderValues(false);
            addReferringPhysicianFromTemplateToFaxRecipients(demographicPlaceholderValues);
        }
        
        function addReferringPhysicianFromTemplateToFaxRecipients(resolvedPlaceholders) {
            let referringFaxData = {};
            if (resolvedPlaceholders.length > 0) {
                for (let i = 0; i < resolvedPlaceholders.length; i++) {
					let placeholder = resolvedPlaceholders[i];
					if (placeholder.marker === 'demographic.referringPhysicianFullName') {
                        referringFaxData['fullName'] = placeholder.value;
					} else if (placeholder.marker === 'demographic.referringPhysicianFaxNo') {
                        referringFaxData['faxNo'] = placeholder.value;
					} else  if (placeholder.marker === 'demographic.referringPhysicianSpecId') {
                        referringFaxData['specId'] = placeholder.value;
					}
                }
			}
            
			// add to list
			if (referringFaxData['fullName'] && referringFaxData['faxNo']) {
                if (checkPhone(referringFaxData['faxNo'])) {
                    let faxRecipientListElement = jQuery('#faxRecipientsList');
                    let trimmedNumber = referringFaxData['faxNo'].replace(/(\D|-)+/g, '');
                    faxRecipientListElement.append('<li>' + referringFaxData['fullName'] + ' (' + trimmedNumber + ') <a onclick="removeFaxRecipient(this)">remove</a><input type="hidden" class="fax-recipient" name="faxRecipient" value="' + trimmedNumber + '"/><input type="hidden" name="faxRecipientName" value="' + referringFaxData['fullName'] + '"/></li>');
                }
			}
		}

        function setHeader(headerId) {
        	if (headerId === '') {
                printoutHeaderHtml = '';
                document.getElementById('headerText').value = '';
                document.getElementById('popUpheader').value = '';
                document.getElementById('headerId').value = '';
            } else {
                const headerSelect = headersMap.get(headerId);
                printoutHeaderHtml =  headerSelect['text'];
                document.getElementById('headerText').value = quillEditor.getFormattedHtml(printoutHeaderHtml);
                document.getElementById('popUpheader').value = '';
                document.getElementById('headerId').value = headerId;
            }

        }

        function setFooter(footerId) {
        	if (footerId === '') {
                printoutFooterHtml = '';
                document.getElementById('footerText').value = '';
                document.getElementById('popUpfooter').value = '';
                document.getElementById('footerId').value = '';
            } else {
                const footerSelect = footersMap.get(footerId);
                printoutFooterHtml =  footerSelect['text'];
                document.getElementById('footerText').value = quillEditor.getFormattedHtml(printoutFooterHtml);
                document.getElementById('popUpfooter').value = '';
                document.getElementById('footerId').value = footerId;
            }

        }

        function setTemplate(templateId) {
            let template = templatesMap.get(templateId);
            if (quillEditor.getText()) {
                if (confirm('Warning: Selecting a new template will overwrite the existing note content. Continue with new template?')) {
                    quillEditor.setContents(template['template']);
                    document.getElementById('documentName').value = template['name'];
                    document.getElementById('templateUsed').value = templateId;
                    quillEditor.fetchAndApplyTemplatePlaceHoldersWithFilledValues([], addReferringPhysicianFromTemplateToFaxRecipients);
                    jQuery('#faxRecipientsList').empty();
                    if (template['headerId'] != 'None' && template['headerId'] != '') {
                        document.getElementById('header-select').value = template['headerId'];
                        document.getElementById('header-select').onchange(template['headerId']);
                    }
                    if (template['footerId'] != 'None' && template['footerId'] != '') {
                        document.getElementById('footer-select').value = template['footerId'];
                        document.getElementById('footer-select').onchange(template['footerId']);
                    }
                }
            } else if (template) { 
                quillEditor.setContents(template['template']);
                document.getElementById('documentName').value = template['name'];
                document.getElementById('templateUsed').value = templateId;
                quillEditor.fetchAndApplyTemplatePlaceHoldersWithFilledValues([], addReferringPhysicianFromTemplateToFaxRecipients);
                jQuery('#faxRecipientsList').empty();
                if (template['headerId'] != 'None' && template['headerId'] != '') {
                    document.getElementById('header-select').value = template['headerId'];
                    document.getElementById('header-select').onchange(template['headerId']);
                }
                if (template['footerId'] != 'None' && template['footerId'] != '') {
                    document.getElementById('footer-select').value = template['footerId'];
                    document.getElementById('footer-select').onchange(template['footerId']);
                }
            }
            document.getElementById('template-select').value = '';
        }
        
        function setInputElementsToQuillValues() {
			createPlaceholderValueElements();
			let fieldsToPullArray = quillEditor.getTemplatePlaceholderMarkers();
            let imagesToSaveArray = quillEditor.getDocumentImages();
            let formElement = document.getElementById('formSmartEncounter');
            for (let i = 0; i < imagesToSaveArray.length; i++) {
                let imageDataToSave = imagesToSaveArray[i].insert.ImagePlaceholder;
                let fileInput = document.createElement('input');
                fileInput.setAttribute('type', 'hidden');
                fileInput.setAttribute('name', 'images');
                fileInput.setAttribute('value', imageDataToSave.identifier + ';' + imageDataToSave.data);
                formElement.appendChild(fileInput);
            }
            document.getElementById('deltaText').value = JSON.stringify(quillEditor.getContentsWithImagePlaceholders());
            document.getElementById('fieldsToPullListString').value += ';' + fieldsToPullArray.join(';');
            let formattedData = quillEditor.getFormattedHtmlAndText();
            document.getElementById('htmlText').value = formattedData.html;
            document.getElementById('plainText').value = formattedData.text;
            if (document.getElementById('popUpheader').value != '') {
                printoutHeaderHtml = JSON.parse(document.getElementById('popUpheader').value);
                document.getElementById('headerText').value = quillEditor.getFormattedHtml(printoutHeaderHtml);
            }

            if (document.getElementById('popUpfooter').value != '') {
                printoutFooterHtml = JSON.parse(document.getElementById('popUpfooter').value);
                document.getElementById('footerText').value = quillEditor.getFormattedHtml(printoutFooterHtml);
            }
        }

		function createPlaceholderValueElements() {
			quillEditor.getPlaceholdersWithValues().forEach(function (item) {
				let valueName = 'placeholderValueMap[' + item.marker + ']';
				if (document.getElementsByName(valueName).length > 0) {
					return;
				}
				let input = document.createElement('input');
				input.setAttribute('type', 'hidden');
				input.setAttribute('name', valueName);
				input.setAttribute('value', item.value);
				document.getElementById('formSmartEncounter').appendChild(input);
			});
		}

        function onClickSave() {
            disableAllSaveButtons();
            window.onbeforeunload = null;
            methodElement.value = 'save';
            setInputElementsToQuillValues();
            document.getElementById('formSmartEncounter').submit();
        }
        function onClickPrint() {
	        window.onbeforeunload = null;
            methodElement.value = 'print';
            document.getElementById('formSmartEncounter').setAttribute('target', '_blank');
            document.getElementById('formSmartEncounter').submit();
            document.getElementById('formSmartEncounter').setAttribute('target', '_self');
		}
        function onClickSaveAndPrint() {
            disableAllSaveButtons();
            window.onbeforeunload = null;
            methodElement.value = 'saveAndPrint';
            setInputElementsToQuillValues();
            document.getElementById('formSmartEncounter').submit();
        }
        function onClickFax() {
	        window.onbeforeunload = null;
            if (document.getElementsByName('faxRecipient').length === 0) {
                window.alert('Cannot Fax: No recipients selected')
            } else {
                methodElement.value = 'fax';
                setInputElementsToQuillValues();
                <% if(pasteFaxNote) { %>
                	pasteToEchartNote();
                <% } %>
                document.getElementById('formSmartEncounter').submit();
            }
        }
        function onClickFaxAndPrint() {
	        window.onbeforeunload = null;
            if (document.getElementsByName('faxRecipient').length === 0) {
                window.alert('Cannot Fax: No recipients selected')
            } else {
                methodElement.value = 'faxAndPrint';
                setInputElementsToQuillValues();
                <% if(pasteFaxNote) { %>
                	pasteToEchartNote();
                <% } %>
                document.getElementById('formSmartEncounter').submit();
            }
        }
        
        function onAddFaxNumberRecipient() {
            let number = jQuery("#faxRecipientsSearch").val();
            if (checkPhone(number)) {
                jQuery('#faxRecipientsSearch').val('');
                let faxRecipientListElement = jQuery('#faxRecipientsList');
                let trimmedNumber = number.replace(/(\D|-)+/g, '');
                faxRecipientListElement.append('<li>' + number + ' (' + trimmedNumber + ') <a onclick="removeFaxRecipient(this)">remove</a><input type="hidden" class="fax-recipient" name="faxRecipient" value="' + trimmedNumber + '"/><input type="hidden" name="faxRecipientName" value="' + number + '"/></li>');
            }
            else {
                alert("The fax number you entered is invalid.");
            }
        }

        // Fax Recipient autocomplete
        function setupReferralDoctorAutoCompletion() {
            jQuery('#faxRecipientsSearch').autocomplete({
                source: '<%=request.getContextPath()%>/professionalSpecialist/Search.do',
                minLength: 2,
                focus: function(event, data) {
                    jQuery('#faxRecipientsSearch').val('');
                    return false;
                },
                select: function(event, data) {
                    jQuery('#faxRecipientsSearch').val('');
                    let faxRecipientListElement = jQuery('#faxRecipientsList');
                    faxRecipientListElement.append('<li><span>' + data.item.label + '</span> (' + data.item.value + ') <a onclick="removeFaxRecipient(this)">remove</a><input type="hidden" class="fax-recipient" name="faxRecipient" value="' + data.item.value + '"/><input type="hidden" name="faxRecipientName" value="' + data.item.label + '"/></li>');
                    return false;
                }
            });
        }
        jQuery(setupReferralDoctorAutoCompletion());

        function removeFaxRecipient(thisElement) {
            $(thisElement).parent().remove()
        }
        
        // Common Macros, using the APCache.js
        let lastUsedCommonMacroKey = null;
        function printCommonMacro(key) {
            if (key) {
                cache.lookup(key);
            }
        }

        let cache = createCache({
            defaultCacheResponseHandler: function(type) {
                if (checkKeyResponse(type)) {
                    quillEditor.insertTextAtCaret(cache.get(type).replace(/<br>/g, '\n') + '\n');
                }

            },
            cacheResponseErrorHandler: function(xhr, error) {
                alert("Please contact an administrator, an error has occurred.");
            },
            serviceUrlContext: '<%=request.getContextPath()%>/eform/'
        });

        cache.addMapping({
            name: "_recent_note",
            values: ["recent_note"],
            storeInCacheHandler: function (key, value) {
                let text = (!cache.isEmpty("recent_note") ? cache.get("recent_note") : "");
                text = text.replaceAll(/\[(\d\d-[A-z]{3}.*?|\d{4}-\d\d-\d\d.*?)](<br>|\r|\n)?/g, ''); // remove "[dd-mmm-yyyy .:]" lines
                text = text.replaceAll(/\[(Signed on |Verified and Signed on )(\d\d-[A-z]{3}.*?)(.|\n|\r)*?]/g, ''); // remove "[Signed on dd-mmm-yyyy ]" lines
                text = text.replaceAll(/\[Faxed .*?(Fax#: )(\d{3}-\d{3}-\d{4})(.*?)]/g, ''); // remove  "[Faxed ...]" lines
                cache.put(this.name, text);
            },
            cacheResponseHandler: function () {
                if (checkKeyResponse(this.name)) {
                    quillEditor.insertTextAtCaret(cache.get(this.name).replace(/<br>/g, '\n'));
                }
            }
        });
        
        function checkKeyResponse(response) {
            if (cache.isEmpty(response)) {
                alert("The requested value has no content.");
                return false;
            }
            return true;
        }


        function checkPhone(str) {
            var phone =  /(([0-9]-{0,1}){0,1}(\([0-9]{3}\)|[0-9]{3}-{0,1})[0-9]{3}-{0,1}[0-9]{4})/;
            var numbersOnly = str.replace(/(\D|-)+/g, '');
            if (str.match(phone) && numbersOnly.length >= 10 && numbersOnly.length <= 11)  {
                return true;
            } else {
                return false;
            }
        }

        function pasteToEchartNote() {
			let faxedNameAndNoList = [];
			let documentName = document.getElementById('documentName').value;

            let faxRecipientNodes = document.getElementsByName('faxRecipient');
			for (let i = 0; i < faxRecipientNodes.length; i++) {
			    let faxNo = faxRecipientNodes[i].value;
			    let faxName = '';
                for (let j = 0; j < faxRecipientNodes[i].parentElement.children.length; j++) {
                    if ('SPAN' === faxRecipientNodes[i].parentElement.children[j].tagName) {
                        faxName = faxRecipientNodes[i].parentElement.children[j].innerText;
					}
                }

                faxedNameAndNoList.push((faxName !== '' && faxName.trim() !== faxNo.trim() ? faxName.trim() : '') + ' Fax#: ' + faxNo);
			}
			
			let text = '[Faxed smart template form ' + documentName + ' to ' + faxedNameAndNoList.join(', ') + ' by <%=loggedInInfo.getLoggedInProvider().getFormattedName()%> <%= timeStamp %>]\n';
			
            // check if opener has casemanagement form and pasteToEncounterNote function
            if( opener.document.forms["caseManagementEntryForm"] !== undefined && opener.pasteToEncounterNote != null) {
                opener.pasteToEncounterNote(text);
            }
        }

        function popupPage(vheight, vwidth, varpage) { //open a new popup window
            const page = "" + varpage;
            windowprops = "height="+vheight+", width="+vwidth+", location=no, scrollbars=yes, menubars=no, toolbars=no, resizable=yes, top=5, left=5";
            const popup = window.open(page, "EditFooter", windowprops);
            if (popup != null && popup.opener == null) {
				popup.opener = self;
            }
        }

        function disableAllSaveButtons() {
            document.querySelectorAll('.buttons input').forEach(function (button) {
                if (button.value.startsWith("Save")) {
                    button.disabled = true;
                }
            });
        }

        window.onbeforeunload = function() {
          return "Changes you made may not be saved.";
        };

		window.onload = function() {
			init('<%=defaultTemplateValue%>')
			const urlParams = new URLSearchParams(window.location.search);
			if (urlParams.get("print") === "true") {
				urlParams.delete("print");
				const newUrl = window.location.origin + window.location.pathname + "?" + urlParams.toString();
				window.history.replaceState({}, document.title, newUrl);
				onClickPrint();
			}
		}
    </script>
</body>
</html>
