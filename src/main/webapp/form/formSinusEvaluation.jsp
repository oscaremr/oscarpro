<%@ page import="oscar.util.*, oscar.form.*, oscar.form.data.*"%>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>


<%
    String formClass = "SinusEvaluation";
    String formLink = "formSinusEvaluation.jsp";

    int demoNo = Integer.parseInt(request.getParameter("demographic_no"));
    int formId = Integer.parseInt(request.getParameter("formId"));
    int provNo = Integer.parseInt((String) session.getAttribute("user"));
    FrmRecord rec = (new FrmRecordFactory()).factory(formClass);
    java.util.Properties props = rec.getFormRecord(LoggedInInfo.getLoggedInInfoFromSession(request),demoNo, formId);
    //FrmData fd = new FrmData();    String resource = fd.getResource(); resource = resource + "ob/riskinfo/";

    //get project_home
    String project_home = request.getContextPath().substring(1);	
%>
<%
  boolean bView = false;
  if (request.getParameter("view") != null && request.getParameter("view").equals("1")) bView = true; 
%>
<%!
	private String outputChecked(java.util.Properties props,String attr,String value) {
		Object obj = props.get(attr);
		if (obj != null && obj.toString().equals(value)) {
			return " checked='checked' ";
		} else {
			return "";
		}
	}
%>
<html:html locale="true">
<head>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<title>Sinus Evaluation</title>
<html:base />
<link rel="stylesheet" type="text/css" media="all" href="../share/css/extractedFromPages.css"  />
</head>


<script type="text/javascript" language="Javascript">
    
    //var choiceFormat  = new Array(7,11,16,20);        
    var choiceFormat  = null;        
    var allNumericField = null;
    var allMatch = null;
    var action = "/<%=project_home%>/form/formname.do";
    
    function checkBeforeSave() {
        /*
        var distance = document.forms[0].elements[6].value;
        var re1 = new RegExp('^[0-9][0-9][0-9][0-9][\.][0-9]$');
        var re2 = new RegExp('^[0-9][0-9][0-9][\.][0-9]$');
        var re3 = new RegExp('^[0-9][0-9][\.][0-9]$');
        var re4 = new RegExp('^[0-9][\.][0-9]$');
        var match1 = document.forms[0].elements[6].value.match(re1);
        var match2 = document.forms[0].elements[6].value.match(re2);
        var match3 = document.forms[0].elements[6].value.match(re3);
        var match4 = document.forms[0].elements[6].value.match(re4);
        if (match1 || match2 || match3 || match4) {            
            if (isFormCompleted(6,21,2,3)==true) {
                return true;
            }    
        } else {
            alert("The input distance must be in ####.# format");
            return false;
        }
             
        return false;
        */
        return true;
    }

</script>
<style>
td{
	height: 27px;
}
.underline{
	border: none;
	border-bottom: 2px solid #000;
}
</style>
<script type="text/javascript" src="formScripts.js">
    
</script>


<body bgproperties="fixed" topmargin="0" leftmargin="0" rightmargin="0"
	onload="window.resizeTo(1024,800)">
<html:form action="/form/formname" method="post">
	<input type="hidden" name="demographic_no"
		value="<%= props.getProperty("demographic_no", "0") %>" />
	<input type="hidden" name="formCreated"
		value="<%= props.getProperty("formCreated", "") %>" />
	<input type="hidden" name="form_class" value="<%=formClass%>" />
	<input type="hidden" name="form_link" value="<%=formLink%>" />
	<input type="hidden" name="formId" value="<%=formId%>" />
	<input type="hidden" name="submit" value="formSinusLetter" />

	<table border="0" cellspacing="0" cellpadding="0" width="740px"
		height="95%">
		<tr>
			<td>
			<table border="0" cellspacing="0" cellpadding="0" width="740px"
				height="50px">
				<tr>
					<th class="subject">Sinus Evaluation</th>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td valign="top">
			<table border="0" cellspacing="0" cellpadding="0" height="85%"
				width="740px" id="page1">
				<tr>
					<td colspan="2">
					<table width="740px" height="620px" border="0" cellspacing="0"
						cellpadding="0">
						<tr>
							<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="4">
								Referring MD: 
								<input class="underline" style="width: 300px;" type="text" maxlength="100" name="referringMD" value="<%= props.getProperty("referringMD", "") %>" />
								<br/>
								<br/>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" colspan="4">
								<table>
									<tr>
										<td>
											RFR:
										</td>
										<td><input type="radio" name="rfr" value="Chronic Sinusitis" <%=outputChecked(props,"rfr","Chronic Sinusitis") %>/>Chronic Sinusitis</td>
										<td><input type="radio" name="rfr" value="Recurrent Acute Sinusitis" <%=outputChecked(props,"rfr","Recurrent Acute Sinusitis") %>/>Recurrent Acute Sinusitis</td>
										<td><input type="radio" name="rfr" value="Rhinitis" <%=outputChecked(props,"rfr","Rhinitis") %>/>Rhinitis</td>
									</tr>
									<tr>
										<td>
										</td>
										<td><input type="radio" name="rfr" value="Nasal-Septal Deviation" <%=outputChecked(props,"rfr","Nasal-Septal Deviation") %>/>Nasal-Septal Deviation</td>
										<td><input type="radio" name="rfr" value="Sinonasal tumor" <%=outputChecked(props,"rfr","Sinonasal tumor") %>/>Sinonasal tumor</td>
									</tr>
									<tr>
										<td>
										</td>
										<td valign="top" >
											<input type="radio" name="rfr" value="Other" <%=outputChecked(props,"rfr","Other") %>/>
											<textarea name="rfr-other-text" style="border: 1px solid #000;width: 95%;margin-left:20px;margin-top:-20px"><%= props.getProperty("rfr-other-text", "") %></textarea>
										</td>
									</tr>
									<tr>
										<td></td>
										<td colspan="4">
											<table>
												<tr>
													<td>Symptoms:</td>
													<td><input type="checkbox" name="sym-None" <%= props.getProperty("sym-None", "") %>/>None</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="sym-Congestion" <%= props.getProperty("sym-Congestion", "") %>/>Congestion</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="sym-Pain" <%= props.getProperty("sym-Pain", "") %>/>Pain:</td>
													<td><input type="checkbox" name="sym-Midfacial" <%= props.getProperty("sym-Midfacial", "") %>/>Midfacial</td>
													<td><input type="checkbox" name="sym-Frontal" <%= props.getProperty("sym-Frontal", "") %>/>Frontal</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="sym-Obstruction" <%= props.getProperty("sym-Obstruction", "") %>/>Obstruction:</td>
													<td><input type="checkbox" name="sym-Left" <%= props.getProperty("sym-Left", "") %>/>Left</td>
													<td><input type="checkbox" name="sym-Right" <%= props.getProperty("sym-Right", "") %>/>Right</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="sym-Rhinorrhea" <%= props.getProperty("sym-Rhinorrhea", "") %>/>Rhinorrhea</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="sym-Smell" <%= props.getProperty("sym-Smell", "") %>/>Smell:</td>
													<td><input type="checkbox" name="sym-Hyposmia" <%= props.getProperty("sym-Hyposmia", "") %>/>Hyposmia</td>
													<td><input type="checkbox" name="sym-Anosmia" <%= props.getProperty("sym-Anosmia", "") %>/>Anosmia</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="userChkbx" <%=props.getProperty("userChkbx","")%>/>
								<textarea name="userText" style="border: 1px solid #000;width: 95%"><%= props.getProperty("userText", "") %></textarea>
								</br>
								Prior Sinus Surgery:
								<input type="radio" name="priorSinusSurgery" value="No" <%=outputChecked(props,"priorSinusSurgery","No") %>/>No
								<input type="radio" name="priorSinusSurgery" value="Yes" <%=outputChecked(props,"priorSinusSurgery","Yes") %>/>Yes
								&nbsp;&nbsp;&nbsp;History facial fracture: 
								<input type="radio" name="historyFacialFracture" value="No" <%=outputChecked(props,"historyFacialFracture","No") %>/>No
								<input type="radio" name="historyFacialFracture" value="Yes" <%=outputChecked(props,"historyFacialFracture","Yes") %>/>Yes
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Environmental Allergies:
								<input type="radio" name="environmental" value="No" <%=outputChecked(props,"environmental","No") %>/>No
								<input type="radio" name="environmental" value="Yes" <%=outputChecked(props,"environmental","Yes") %>/>Yes
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="radio" name="environmental" value="Not tested" <%=outputChecked(props,"environmental","Not tested") %>/>Not tested
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Recent Treatment: Symptoms improved (+)   No Symptom Improvement (-)
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="recentTreatment-incs" <%= props.getProperty("recentTreatment-incs", "") %>/>INCS
								<input class="underline" style="width: 25px;" type="text" name="recentTreatment-incs-text" value="<%= props.getProperty("recentTreatment-incs-text", "") %>" />
								<input type="checkbox" name="recentTreatment-saline" <%= props.getProperty("recentTreatment-saline", "") %>/>Saline
								<input class="underline" style="width: 25px;" type="text" name="recentTreatment-saline-text" value="<%= props.getProperty("recentTreatment-saline-text", "") %>" />
								<input type="checkbox" name="recentTreatment-prednisone" <%= props.getProperty("recentTreatment-prednisone", "") %>/>Prednisone
								<input class="underline" style="width: 25px;" type="text" name="recentTreatment-prednisone-text" value="<%= props.getProperty("recentTreatment-prednisone-text", "") %>" />
								<input type="checkbox" name="recentTreatment-antihistamine" <%= props.getProperty("recentTreatment-antihistamine", "") %>/>Antihistamine
								<input class="underline" style="width: 25px;" type="text" name="recentTreatment-antihistamine-text" value="<%= props.getProperty("recentTreatment-antihistamine-text", "") %>" />
								<input type="checkbox" name="recentTreatment-singulair" <%= props.getProperty("recentTreatment-singulair", "") %>/>Singulair
								<input class="underline" style="width: 25px;" type="text" name="recentTreatment-singulair-text" value="<%= props.getProperty("recentTreatment-singulair-text", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="recentTreatment-immunotherapy" <%= props.getProperty("recentTreatment-immunotherapy", "") %>/>Immunotherapy
								<input class="underline" style="width: 25px;" type="text" name="recentTreatment-immunotherapy-text" value="<%= props.getProperty("recentTreatment-immunotherapy-text", "") %>" />
								<input type="checkbox" name="recentTreatment-abx" <%= props.getProperty("recentTreatment-abx", "") %>/>Abx
								<input class="underline" style="width: 25px;" type="text" name="recentTreatment-abx-text" value="<%= props.getProperty("recentTreatment-abx-text", "") %>" />
<!--								<input class="underline" style="width: 120px;" type="text" name="recentTreatment-text1" value="<%= props.getProperty("recentTreatment-text1", "") %>" />&nbsp;&nbsp;&nbsp;&nbsp;-->
								<input type="checkbox" name="recentTreatment-other" <%= props.getProperty("recentTreatment-other", "") %>/>
								<input class="underline" style="width: 120px;" type="text" name="recentTreatment-other-text" value="<%= props.getProperty("recentTreatment-other-text", "") %>" />
<!--								<input class="underline" style="width: 120px;" type="text" name="recentTreatment-text2" value="<%= props.getProperty("recentTreatment-text2", "") %>" />&nbsp;&nbsp;&nbsp;&nbsp;-->
							</td>
						</tr>
						
						<tr class="title">
							<th colspan="5" height="50px">Examination</th>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Otoscopy:
								<input type="radio" name="otoscopy" value="within normal limits" <%=outputChecked(props,"otoscopy","within normal limits") %>/>WNL
								<input type="radio" name="otoscopy" value="otoscopy-text1" <%=outputChecked(props,"otoscopy","otoscopy-text1") %>/>
								<input class="underline" style="width: 120px;" type="text" name="otoscopy-text1" value="<%= props.getProperty("otoscopy-text1", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Oral:
								<input type="radio" name="oralCavity" value="within normal limits" <%=outputChecked(props,"oralCavity","within normal limits") %>/>WNL
								<input type="radio" name="oralCavity" value="oralCavity-text1" <%=outputChecked(props,"oralCavity","oralCavity-text1") %>/>
								<textarea name="oralCavity-text1" style="border: 1px solid #000;width: 75%"><%= props.getProperty("oralCavity-text1", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Neck:
								<input type="radio" name="neck" value="No adenopathy" <%=outputChecked(props,"neck","No adenopathy") %>/>No adenopathy
								<input type="radio" name="neck" value="nect-text1" <%=outputChecked(props,"neck","nect-text1") %> />
								<input class="underline" style="width: 120px;" type="text" name="neck-text1" value="<%= props.getProperty("neck-text1", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Cranial Nerve Screen:
								<input type="radio" name="cranial" value="within normal limits" <%=outputChecked(props,"cranial","within normal limits") %>/>WNL
								<input type="radio" name="cranial" value="cranial-text1" <%=outputChecked(props,"cranial","cranial-text1") %> />
								<input class="underline" style="width: 435px;" type="text" name="cranial-text1" value="<%= props.getProperty("cranial-text1", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Anterior rhinoscopy:
								<textarea name="anterior-text1" style="border: 1px solid #000;width: 75%"><%= props.getProperty("anterior-text1", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Septal Deviation:
								<input type="checkbox" name="septalDeviationNone" <%= props.getProperty("septalDeviationNone", "") %> />None
								<input type="checkbox" name="septalDeviationRight" <%= props.getProperty("septalDeviationRight", "") %> />Right
								<input type="checkbox" name="septalDeviationLeft" <%= props.getProperty("septalDeviationLeft", "") %> />Left
								<input type="checkbox" name="septalDeviationMild" <%= props.getProperty("septalDeviationMild", "") %> />Mild
								<input type="checkbox" name="septalDeviationModerate" <%= props.getProperty("septalDeviationModerate", "") %> />Moderate
								<input type="checkbox" name="septalDeviationSevere" <%= props.getProperty("septalDeviationSevere", "") %> />Severe
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Inferior Turbinates:
								<input type="checkbox" name="inferiorNormal" <%= props.getProperty("inferiorNormal", "") %>/>Normal
								<input type="checkbox" name="inferiorHypertrophy" <%= props.getProperty("inferiorHypertrophy", "") %>/>Hypertrophy
								<input type="checkbox" name="inferiorPallor" <%= props.getProperty("inferiorPallor", "") %>/>mucosal pallor
								<input type="checkbox" name="inferiorOther" <%= props.getProperty("inferiorOther", "") %>/>
								<input class="underline" style="width: 120px;" type="text" name="inferiorOther-text1" value="<%= props.getProperty("inferiorOther-text1", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<table>
									<tr>
										<td>Endoscopy: </td>
										<td><input type="checkbox" name="endoscopy-polyps" <%= props.getProperty("endoscopy-polyps", "") %>/>Polyps</td>
										<td>
											<input type="checkbox" name="endoscopy-grade-left" <%= props.getProperty("endoscopy-grade-left", "") %>/>Left:
											Grade&nbsp;&nbsp;<input class="underline" style="width: 40px;" type="text" name="endoscopy-grade-left-text1" value="<%= props.getProperty("endoscopy-grade-left-text1", "") %>" />/4
										</td>
										<td>
											<input type="checkbox" name="endoscopy-grade-right" <%= props.getProperty("endoscopy-grade-right", "") %>/>Right:
											Grade&nbsp;&nbsp;<input class="underline" style="width: 40px;" type="text" name="endoscopy-grade-right-text1" value="<%= props.getProperty("endoscopy-grade-right-text1", "") %>" />/4
										</td>
									</tr>
									<tr>
										<td></td>
										<td><input type="checkbox" name="endoscopy-pus" <%= props.getProperty("endoscopy-pus", "") %>/>Pus</td>
										<td>
											<input type="checkbox" name="endoscopy-left" <%= props.getProperty("endoscopy-left", "") %>/>Left
										</td>
										<td>
											<input type="checkbox" name="endoscopy-right" <%= props.getProperty("endoscopy-right", "") %>/>Right
										</td>
									</tr>
									<tr>
										<td></td>
										<td><input type="checkbox" name="endoscopy-inflammation" <%= props.getProperty("endoscopy-inflammation", "") %>/>Inflammation&nbsp;</td>
										<td>
											<input type="checkbox" name="endoscopy-inflammation-left" <%= props.getProperty("endoscopy-inflammation-left", "") %>/>Left: Grade&nbsp;&nbsp;
											<input class="underline" style="width: 40px;" type="text" name="endoscopy-inflammation-left-text1" value="<%= props.getProperty("endoscopy-inflammation-left-text1", "") %>" />/4
										</td>
										<td>
											<input type="checkbox" name="endoscopy-inflammation-right" <%= props.getProperty("endoscopy-inflammation-right", "") %>/>Right: Grade&nbsp;&nbsp;
											<input class="underline" style="width: 40px;" type="text" name="endoscopy-inflammation-right-text1" value="<%= props.getProperty("endoscopy-inflammation-right-text1", "") %>" />/4
										</td>
									</tr>
									<tr>
										<td></td>
										<td><input type="checkbox" name="endoscopy-normal" <%= props.getProperty("endoscopy-normal", "") %>/>Normal</td>
									</tr>
									<tr>
										<td></td>
										<td colspan="3">
											Other:
											<textarea name="endoscopy-other-text" style="border: 1px solid #000;width: 88%"><%= props.getProperty("endoscopy-other-text", "") %></textarea>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<table>
									<tr>
										<td>CT Scan:</td>
										<td><input type="radio" name="ctscan" value="CRS" <%=outputChecked(props,"ctscan","CRS") %>/>CRS</td>
										<td>
											Tumor:
											<input type="radio" name="ctscan" value="tumor left" <%=outputChecked(props,"ctscan","tumor left") %>/>Left
											<input type="radio" name="ctscan" value="tumor right" <%=outputChecked(props,"ctscan","tumor right") %>/>Right
											<input type="radio" name="ctscan" value="NYD" <%=outputChecked(props,"ctscan","NYD") %>/>NYD
										</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td><input type="radio" name="ctscan" value="normal" <%=outputChecked(props,"ctscan","normal") %>/>Normal</td>
										<td colspan="1">
										</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td colspan="7">
											<input type="radio" name="ctscan" value="other-text" <%=outputChecked(props,"ctscan","other") %>/>Other:
											<textarea name="ctscan-other-text" style="border: 1px solid #000;width: 77%"><%= props.getProperty("ctscan-other-text", "") %></textarea>
										</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<table>
									<tr>
										<td><span style="font-weight: bold;">Impression:</span></td>
										<td>
											<input type="checkbox" name="impression-Sinusitis" <%= props.getProperty("impression-Sinusitis", "") %> />Sinusitis: 
											<input type="checkbox" name="impression-Sinusitis-CRSwNP" <%= props.getProperty("impression-Sinusitis-CRSwNP", "") %> />CRS with polyps 
											<input type="checkbox" name="impression-Sinusitis-CRSsNP" <%= props.getProperty("impression-Sinusitis-CRSwNP", "") %> />CRS without polyps 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression-Rhinitis" <%= props.getProperty("impression-Rhinitis", "") %>/>Rhinitis:
											<input type="checkbox" name="impression-Rhinitis-Allergic" <%= props.getProperty("impression-Rhinitis-Allergic", "") %> />Allergic
											<input type="checkbox" name="impression-Rhinitis-other" <%= props.getProperty("impression-Rhinitis-other", "") %> />
											<input class="underline" style="width: 160px;" type="text" name="impression-Rhinitis-other-text" value="<%= props.getProperty("impression-Rhinitis-other-text", "") %>" /> 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression-Nonallergic" <%= props.getProperty("impression-Nonallergic", "") %>/>Nonallergic: 
											<input type="checkbox" name="impression-Nonallergic-Vasomotor" <%= props.getProperty("impression-Nonallergic-Vasomotor", "") %> />Vasomotor 
											
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression-Nonallergic-other" <%= props.getProperty("impression-Nonallergic-other", "") %> />
											<textarea name="impression-Nonallergic-other-text" style="border: 1px solid #000;width: 75%"><%= props.getProperty("impression-Nonallergic-other-text", "") %></textarea> 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression-SinonasalsTumor" <%= props.getProperty("impression-SinonasalsTumor", "") %> />Sinonasal Tumor:
											<input style="width: 300px;" type="text" name="impression-SinonasalsTumor-text" value="<%= props.getProperty("impression-SinonasalsTumor-text", "") %>" /> 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression-Headache" <%= props.getProperty("impression-Headache", "") %> />Headache 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression-SeptalDeviation" <%= props.getProperty("impression-SeptalDeviation", "") %>/>Septal Deviation: 
											<input type="checkbox" name="impression-SeptalDeviation-L" <%= props.getProperty("impression-SeptalDeviation-L", "") %> />Left
											<input type="checkbox" name="impression-SeptalDeviation-R" <%= props.getProperty("impression-SeptalDeviation-R", "") %> />Right 											 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression-NasoseptalDeviation" <%= props.getProperty("impression-NasoseptalDeviation", "") %> />Nasoseptal Deviation:
											<input type="checkbox" name="impression-NasoseptalDeviation-L" <%= props.getProperty("impression-NasoseptalDeviation-L", "") %> />Left
											<input type="checkbox" name="impression-NasoseptalDeviation-R" <%= props.getProperty("impression-NasoseptalDeviation-R", "") %> />Right
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression-other" <%= props.getProperty("impression-other", "") %> />Other:
											<textarea name="impression-other-text" style="border: 1px solid #000;width: 75%"><%= props.getProperty("impression-other-text", "") %></textarea>
										</td>
									</tr>
								</table>
<!--								<table>-->
<!--									<tr>-->
<!--										<td><span style="font-weight: bold;">Impression:</span></td>-->
<!--										<td>-->
<!--											<input type="checkbox"/>Sinusitis: -->
<!--											<input type="checkbox" name="impression" value="sinusitis CRSwNP" <%=outputChecked(props,"impression","sinusitis CRSwNP") %>/>CRSwNP -->
<!--											<input type="checkbox" name="impression" value="sinusitis CRSsNP" <%=outputChecked(props,"impression","sinusitis CRSsNP") %>/>CRSsNP -->
<!--										</td>-->
<!--									</tr>-->
<!--									<tr>-->
<!--										<td></td>-->
<!--										<td>-->
<!--											Rhinitis:-->
<!--											<input type="radio" name="impression" value="Rhinitis Allergic" <%=outputChecked(props,"impression","Rhinitis Allergic") %>/>Allergic-->
<!--											<input type="radio" name="impression" value="Rhinitis Other" <%=outputChecked(props,"impression","Rhinitis Other") %>/>-->
<!--											<input class="underline" style="width: 160px;" type="text" name="Nonallergic-other-text" value="<%= props.getProperty("Rhinitis-other-text", "") %>" /> -->
<!--										</td>-->
<!--									</tr>-->
<!--									<tr>-->
<!--										<td></td>-->
<!--										<td>-->
<!--											Nonallergic: -->
<!--											<input type="radio" name="impression" value="Nonallergic Vasomotor" <%=outputChecked(props,"impression","Nonallergic Vasomotor") %>/>Vasomotor -->
<!--											<input type="radio" name="impression" value="Nonallergic Other" <%=outputChecked(props,"impression","Nonallergic Other") %>/>-->
<!--											<input class="underline" style="width: 160px;" type="text" name="Nonallergic-other-text" value="<%= props.getProperty("Nonallergic-other-text", "") %>" /> -->
<!--										</td>-->
<!--									</tr>-->
<!--									<tr>-->
<!--										<td></td>-->
<!--										<td>-->
<!--											<input type="radio" name="impression" value="Sinonasal Tumor" <%=outputChecked(props,"impression","Sinonasal Tumor") %>/>Sinonasal Tumor:-->
<!--											<input class="underline" style="width: 300px;" type="text" name="Sinonasal-Tumor-text" value="<%= props.getProperty("Sinonasal-Tumor-text", "") %>" /> -->
<!--										</td>-->
<!--									</tr>-->
<!--									<tr>-->
<!--										<td></td>-->
<!--										<td>-->
<!--											<input type="radio" name="impression" value="Headache" <%=outputChecked(props,"impression","Headache") %>/>Headache -->
<!--										</td>-->
<!--									</tr>-->
<!--									<tr>-->
<!--										<td></td>-->
<!--										<td>-->
<!--											Septal Deviation: -->
<!--											<input type="radio" name="impression" value="Septal Deviation Left" <%=outputChecked(props,"impression","Septal Deviation Left") %>/>L-->
<!--											<input type="radio" name="impression" value="Septal Deviation Right" <%=outputChecked(props,"impression","Septal Deviation Right") %>/>R -->
<!--											Nasoseptal Deviation:-->
<!--											<input type="radio" name="impression" value="Nasoseptal Deviation Left" <%=outputChecked(props,"impression","Nasoseptal Deviation Left") %>/>L-->
<!--											<input type="radio" name="impression" value="Nasoseptal Deviation Right" <%=outputChecked(props,"impression","Nasoseptal Deviation Right") %>/>R -->
<!--										</td>-->
<!--									</tr>-->
<!--									<tr>-->
<!--										<td></td>-->
<!--										<td>-->
<!--											<input type="radio" name="impression" value="Other-text" <%=outputChecked(props,"impression","Other") %>/>Other:-->
<!--											<input class="underline" style="width: 300px;" type="text" name="impression-other-text" value="<%= props.getProperty("impression-other-text", "") %>" /> -->
<!--										</td>-->
<!--									</tr>-->
<!--								</table>-->
							</td>
						</tr>
						
						
						<!-----invalid start
						<tr>
							<td height="80px;"></td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Ears: AD:
								<input type="radio" name="recurrentAcute"/>WNL
								<input type="radio" name="recurrentAcute"/>
								<input class="underline" style="width: 120px;" type="text" name="referringMD" value="<%= props.getProperty("referringMD", "") %>" />
								AS:
								<input type="radio" name="recurrentAcute"/>WNL
								<input type="radio" name="recurrentAcute"/>
								<input class="underline" style="width: 120px;" type="text" name="referringMD" value="<%= props.getProperty("referringMD", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Oral:
								<input type="radio" name="recurrentAcute"/>WNL
								<input type="radio" name="recurrentAcute"/>
								<input class="underline" style="width: 120px;" type="text" name="referringMD" value="<%= props.getProperty("referringMD", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<table>
									<tr>
										<td></td>
										<td>Cottle Maneuver:</td>
										<td colspan="2">
											Right:<input type="radio" name="recurrentAcute"/>Pos
											<input type="radio" name="recurrentAcute"/>Neg
										</td>
										<td colspan="2">
											Left:<input type="radio" name="recurrentAcute"/>Pos
											<input type="radio" name="recurrentAcute"/>Neg
										</td>
									</tr>
									<tr>
										<td></td>
										<td>Dorsum Deformity:</td>
										<td colspan="2">
											<input type="radio" name="recurrentAcute"/>C-shaped
										</td>
										<td colspan="2">
											<input type="radio" name="recurrentAcute"/>Reverse C
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<table>
									<tr>
										<td>Larynx:</td>
										<td><input type="radio" name="recurrentAcute"/>WNL</td>
										<td>
											<input type="radio" name="recurrentAcute"/>RVCP
										</td>
										<td>
											<input type="radio" name="recurrentAcute"/>LVCP
										</td>
										<td>
											<input type="radio" name="recurrentAcute"/>
											<input class="underline" style="width: 150px;" type="text" name="referringMD" value="<%= props.getProperty("referringMD", "") %>" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Laboratory Data:
								lgE<input class="underline" style="width: 60px;" type="text" name="referringMD" value="<%= props.getProperty("referringMD", "") %>" />
								Ig GAM<input class="underline" style="width: 60px;" type="text" name="referringMD" value="<%= props.getProperty("referringMD", "") %>" />
							</td>
						</tr>
						
						<tr>
							<td height="80px">&nbsp;</td>
						</tr>
						invalid end----->
						
						
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<span style="font-weight: bold;">Plan:</span>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<span style="font-weight: bold;">Medical:</span>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="saline" <%= props.getProperty("saline", "") %>/>Saline Irrigation OD-BID
								<input type="checkbox" name="singulair" <%= props.getProperty("singulair", "") %>s/>Singulair 10 mg OD
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="antibiotic" <%= props.getProperty("antibiotic", "") %>/>Antibiotic:
								&nbsp;&nbsp;&nbsp;
								Duration:
								<input class="underline" style="width: 50px;" type="text" name="duration" value="<%= props.getProperty("duration", "") %>" />								
								days
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="cefuroxime" <%= props.getProperty("cefuroxime", "") %>/>Cefuroxime (500 mg BID)
								<input type="checkbox" name="clavulin" <%= props.getProperty("clavulin", "") %>/>Clavulin (875 mg BID)
								<input type="checkbox" name="clarithromycin" <%= props.getProperty("clarithromycin", "") %>/>Clarithromycin (500 mg BID)
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="medical-other" <%= props.getProperty("medical-other", "") %>/>
								<input class="underline" style="width: 300px;" type="text" name="medical-other-text" value="<%= props.getProperty("medical-other-text", "") %>" />								
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="low-dose" <%= props.getProperty("low-dose", "") %>/>
								Low Dose Macrolide (Clarithromycin 250 mg po OD) for 3 months								
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="steroids" <%= props.getProperty("steroids", "") %>/>
								<span style="font-weight: bold;">Steroids:</span>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="incs" <%= props.getProperty("incs", "") %>/>INCS (2 sprays/nostril 
								<input class="underline" style="width: 50px;" type="text" name="incs-days" value="<%= props.getProperty("incs-days", "") %>" />								
								D)
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="prednisone" <%= props.getProperty("prednisone", "") %>/>Prednisone: 
								<input class="underline" style="width: 50px;" type="text" name="prednisone-mg" value="<%= props.getProperty("prednisone-mg", "") %>" />								
								mg po OD for
								<input class="underline" style="width: 50px;" type="text" name="prednisone-days" value="<%= props.getProperty("prednisone-days", "") %>" />								
								days.(risks: GI, Psych, AVN hip)
								<input type="checkbox" name="prednisone-discussed" <%= props.getProperty("prednisone-discussed", "") %>/>
								<span style="color: red;">discussed</span>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="pulmicort" <%= props.getProperty("pulmicort", "") %>/>Pulmicort Irrigations:
								<input class="underline" style="width: 50px;" type="text" name="pulmicort-mg" value="<%= props.getProperty("pulmicort-mg", "") %>" />								
								mg/mL, 2 mL respule.1 respule in 240 mL saline, irrigate with 120 mL BID.)
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<span style="font-weight: bold;">Allergy:</span>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="allergist-1" <%= props.getProperty("allergist-1", "") %>/>Allergist Referral:
								<input type="checkbox" name="allergist-2" <%= props.getProperty("allergist-2", "") %>/>Requested Today
								<input type="checkbox" name="allergist-3" <%= props.getProperty("allergist-3", "") %>/>Please Refer this Patient
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="allergist-4" <%= props.getProperty("allergist-4", "") %>/>Immunotherapy evaluation with current allergist
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="allergist-5" <%= props.getProperty("allergist-5", "") %>/>ASA desensitization
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="allergist-6" <%= props.getProperty("allergist-6", "") %>/>Anti-IgE evaluation
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="allergist-7" <%= props.getProperty("allergist-7", "") %>/>
								<textarea name="allergist-8-text" style="border: 1px solid #000;width: 95%"><%= props.getProperty("allergist-8-text", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<span style="font-weight: bold;">Surgery:</span>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="surgery-1" <%= props.getProperty("surgery-1", "") %>/>
								<input class="underline" style="width: 60px;" type="text" name="surgery-1-ess" value="<%= props.getProperty("surgery-1-ess", "") %>" />
								Endoscopic Sinus Surgery (risks: orbital injury, CSF leak, bleeding, pain, infection, recurrence) 
								<input type="checkbox" name="surgery-1-discussed" <%= props.getProperty("surgery-1-discussed", "") %>/>
								<span style="color: red;">discussed</span>								
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="surgery-2" <%= props.getProperty("surgery-2", "") %>/>
								Endonasal Endoscopic Resection of
								<input class="underline" style="width: 60px;" type="text" name="surgery-2-text" value="<%= props.getProperty("surgery-2-text", "") %>" />
								Sinonasal Tumor(risks: orbital injury, CSF leak, bleeding, pain, infection, recurrence, post-op care) 
								<input type="checkbox" name="surgery-2-discussed" <%= props.getProperty("surgery-2-discussed", "") %>/>
								<span style="color: red;">discussed</span>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="surgery-3" <%= props.getProperty("surgery-3", "") %>/>
								Septoplasty and BITR (risks: pain, bleeding, infection, residual obstruction) 
								<input type="checkbox" name="surgery-3-discussed" <%= props.getProperty("surgery-3-discussed", "") %>/>
								<span style="color: red;">discussed</span>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="surgery-4" <%= props.getProperty("surgery-4", "") %>/>
								<input class="underline" style="width: 60px;" type="text" name="surgery-4-text" value="<%= props.getProperty("surgery-4-text", "") %>" />
								Septorhinoplasty (risks: pain, bleeding, infection, scar, residual deviation or obstruction) 
								<input type="checkbox" name="surgery-4-discussed" <%= props.getProperty("surgery-4-discussed", "") %>/>
								<span style="color: red;">discussed</span>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="surgery-5" <%= props.getProperty("surgery-5", "") %>/>
								CT scan of the paranasal sinuses 
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="surgery-6" <%= props.getProperty("surgery-6", "") %>/>
								Given the abscence of sinus disease, I suggest you refer this patient to a neurologist for a headache evaluation.
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="surgery-7" <%= props.getProperty("surgery-7", "") %>/>
								Return to clinic: <input class="underline" style="width: 60px;" type="text" name="surgery-7-text" value="<%= props.getProperty("surgery-7-text", "") %>" />
								months
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" colspan="7">
								<input type="checkbox" name="plan-other" <%= props.getProperty("plan-other", "") %>>
								<textarea name="plan-other-text" style="border: 1px solid #000;width: 95%"><%= props.getProperty("plan-other-text", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="right" colspan="7">
								<br/>
								<input class="underline" style="width: 300px;" type="text" name="signatures" value="<%= props.getProperty("signatures", "") %>" />
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>

		<tr>
			<td valign="top">
			<table class="Head" class="hidePrint" height="5%">
				<tr>
					<td align="left">
					<%
  if (!bView) {
%> 
					<input type="submit" value="Save"
						onclick="javascript:if (checkBeforeSave()==true) return justSave(); else return false;" />
						
					<input type="submit" value="Save & Letter"
						onclick="javascript:if (checkBeforeSave()==true) return onSubmitAndOpenLetter(); else return false;" />
					<%
  }
%> <input type="button" value="Exit"
						onclick="javascript:return onExit();" /> <input type="button"
						value="Print" onclick="javascript:window.print();" /></td>
					<td align="right">Study ID: <%= props.getProperty("studyID", "N/A") %>
					<input type="hidden" name="studyID"
						value="<%= props.getProperty("studyID", "N/A") %>" /></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</html:form>
<script type="text/javascript">
function onSubmitAndOpenLetter() {
    document.forms[0].submit.value="formSinusLetter";
    
    var ret = is1CheckboxChecked(0, choiceFormat) && allAreNumeric(0, allNumericField) && areInRange(0, allMatch);                       

    if (ret==true) {                        
        ret = confirm("Are you sure you want to save this form?");            
    }                
    return ret;
}
function justSave() {
    document.forms[0].submit.value="exit";   
    var ret = is1CheckboxChecked(0, choiceFormat) && allAreNumeric(0, allNumericField) && areInRange(0, allMatch);                       
    if (ret==true) {                        
        ret = confirm("Are you sure you want to save this form?");            
    }                
    return ret;     
}
</script>
</body>
</html:html>
