<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ page import="oscar.util.*, oscar.form.*, oscar.form.data.*"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>


<%
    String formClass = "EpistaxisEvaluation";
    String formLink = "formEpistaxisEvaluation.jsp";

    int demoNo = Integer.parseInt(request.getParameter("demographic_no"));
    int formId = Integer.parseInt(request.getParameter("formId"));
    int provNo = Integer.parseInt((String) session.getAttribute("user"));
    FrmRecord rec = (new FrmRecordFactory()).factory(formClass);
    java.util.Properties props = rec.getFormRecord(LoggedInInfo.getLoggedInInfoFromSession(request),demoNo, formId);

    String project_home = request.getContextPath().substring(1);	
%>
<%
  boolean bView = false;
  if (request.getParameter("view") != null && request.getParameter("view").equals("1")) bView = true; 
%>
<%!
	private String outputChecked(java.util.Properties props,String attr,String value) {
		Object obj = props.get(attr);
		if (obj != null && obj.toString().equals(value)) {
			return " checked='checked' ";
		} else {
			return "";
		}
	}
%>
<html:html locale="true">
<head>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<title>Epistaxis Evaluation</title>
<html:base />
<link rel="stylesheet" type="text/css" media="all" href="../share/css/extractedFromPages.css"  />
</head>


<script type="text/javascript" language="Javascript">
    
    //var choiceFormat  = new Array(7,11,16,20);        
    var choiceFormat  = null;        
    var allNumericField = null;
    var allMatch = null;
    var action = "/<%=project_home%>/form/formname.do";
    
    function checkBeforeSave() {
        /*
        var distance = document.forms[0].elements[6].value;
        var re1 = new RegExp('^[0-9][0-9][0-9][0-9][\.][0-9]$');
        var re2 = new RegExp('^[0-9][0-9][0-9][\.][0-9]$');
        var re3 = new RegExp('^[0-9][0-9][\.][0-9]$');
        var re4 = new RegExp('^[0-9][\.][0-9]$');
        var match1 = document.forms[0].elements[6].value.match(re1);
        var match2 = document.forms[0].elements[6].value.match(re2);
        var match3 = document.forms[0].elements[6].value.match(re3);
        var match4 = document.forms[0].elements[6].value.match(re4);
        if (match1 || match2 || match3 || match4) {            
            if (isFormCompleted(6,21,2,3)==true) {
                return true;
            }    
        } else {
            alert("The input distance must be in ####.# format");
            return false;
        }
             
        return false;
        */

		var returnToClinicNumberOfMonths = document.getElementById("returnToClinicNumberOfMonths").value;
		const reg = new RegExp('^[0-9]+$');
		if (reg.test(returnToClinicNumberOfMonths) === false) {
			alert("Return to clinic time should be integer only ");
			return false;
		}
		return true;

    }

</script>
<style>
td{
	height: 27px;
}
.underline{
	border: none;
	border-bottom: 2px solid #000;
}
</style>
<script type="text/javascript" src="formScripts.js">
    
</script>


<body bgproperties="fixed" topmargin="0" leftmargin="0" rightmargin="0"
	onload="window.resizeTo(1024,800)">
<html:form action="/form/formname" method="post">
	<input type="hidden" name="demographic_no"
		value="<%= props.getProperty("demographic_no", "0") %>" />
	<input type="hidden" name="formCreated"
		value="<%= props.getProperty("formCreated", "") %>" />
	<input type="hidden" name="form_class" value="<%=formClass%>" />
	<input type="hidden" name="form_link" value="<%=formLink%>" />
	<input type="hidden" name="formId" value="<%=formId%>" />
	<input type="hidden" name="submit" value="formEpistaxisLetter" />

	<table border="0" cellspacing="0" cellpadding="0" width="740px"
		height="95%">
		<tr>
			<td>
			<table border="0" cellspacing="0" cellpadding="0" width="740px"
				height="50px">
				<tr>
					<th class="subject">Epistaxis Evaluation</th>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td valign="top">
			<table border="0" cellspacing="0" cellpadding="0" height="85%"
				width="740px" id="page1">
				<tr>
					<td colspan="2">
					<table width="740px" height="620px" border="0" cellspacing="0"
						cellpadding="0">
						<tr>
							<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="7">
								Referrin MD: 
								<input class="underline" style="width: 300px;" type="text" maxlength="100" name="fpOrGP" value="<%= props.getProperty("fpOrGP", "") %>" />
								<br/>
								<br/>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="7">
								Last Epistaxis Episode: 
								<input class="underline" style="width: 90px;" type="text" maxlength="65" name="lastEpistaxisEpisode" value="<%= props.getProperty("lastEpistaxisEpisode", "") %>" />
								<input type="checkbox" name="days" <%= props.getProperty("days", "") %>>days</input>
								<input type="checkbox" name="weeks" <%= props.getProperty("weeks", "") %>>weeks</input>
								<input type="checkbox" name="months" <%= props.getProperty("months", "") %>>months&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ago</input>
							</td>
						</tr>
						<tr>
						<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="7">  
								Nasal Side that bleeding occurred: 
								<input type="checkbox" name="nasalSideBleedingLeft"  <%= props.getProperty("nasalSideBleedingLeft", "") %>>Left</input>
								<input type="checkbox" name="nasalSideBleedingRight" <%= props.getProperty("nasalSideBleedingRight", "") %>>Right</input>
								<input type="checkbox" name="nasalSideBleedingUnsure" <%= props.getProperty("nasalSideBleedingUnsure", "") %>>Unsure</input>
							</td>
						</tr>
						<tr>
						<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="7">
								Blood immediately drained into the throat with little from the nose: 
								<input type="checkbox" name="bloodIntoThroatAndNoseNO" <%= props.getProperty("bloodIntoThroatAndNoseNO", "") %>>No</input> 
								<input type="checkbox" name="bloodIntoThroatAndNoseYes" <%= props.getProperty("bloodIntoThroatAndNoseYes", "") %>>Yes</input>
							</td>
						</tr>
						<tr>
						<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="7">
								History of Recurrent Epistaxis:  
								<input type="checkbox" name="historyEpistaxisNo"  <%= props.getProperty("historyEpistaxisNo", "") %>>No</input>
								<input type="checkbox" name="historyEpistaxisYes"  <%= props.getProperty("historyEpistaxisYes", "") %>>Yes</input>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="7">
								Prior treatment for epistaxis:  
								<input type="checkbox" name="priorTreatmentNo" <%= props.getProperty("priorTreatmentNo", "") %>>No</input>
								<input type="checkbox" name="priorTreatmentYes" <%=props.getProperty("priorTreatmentYes", "") %>>Yes</input>
								<input class="underline" name="priorTreatmentText" style="width: 350px;" type="text" maxlength="30" value="<%= props.getProperty("priorTreatmentText", "") %>"></input>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="7">
								Family History of a bleeding disorder: 
								<input type="checkbox" name="familyHistoryBleedingNo"  <%= props.getProperty("familyHistoryBleedingNo", "") %>>No</input> 
								<input type="checkbox" name="familyHistoryBleedingYes" <%= props.getProperty("familyHistoryBleedingYes", "") %>>Yes</input>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="7">
								Personal History of a bleeding disorder:  
								<input type="checkbox" name="personalHistoryBleedingNo" <%= props.getProperty("personalHistoryBleedingNo", "") %>>No</input>
								<input type="checkbox" name="personalHistoryBleedingYes" <%= props.getProperty("personalHistoryBleedingYes", "") %>>Yes</input>
								<input class="underline" name="personalHistoryBleedingText" style="width: 265px;" type="text" maxlength="30" value="<%= props.getProperty("personalHistoryBleedingText", "") %>"></input>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="7">
								History of Hypertension:  
								<input type="checkbox" name="historyHypertentionNo"  <%= props.getProperty("historyHypertentionNo", "") %>>No</input>
								<input type="checkbox" name="historyHypertentionYes"  <%= props.getProperty("historyHypertentionYes", "") %>>Yes</input>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="7">
								History of liver disease or failure:  
								<input type="checkbox" name="historyLiverDiseaseFailureNo" <%= props.getProperty("historyLiverDiseaseFailureNo", "") %>>No</input>
								<input type="checkbox" name="historyLiverDiseaseFailureYes" <%= props.getProperty("historyLiverDiseaseFailureYes", "") %>>Yes</input>
								<input class="underline" name="historyLiverDiseaseFailureText" style="width: 315px;" type="text" maxlength="30" value="<%= props.getProperty("historyLiverDiseaseFailureText", "") %>"></input>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="7">
								History of Renal Failure or insufficiency:  
								<input type="checkbox" name="historyRenalFailureInsufficiencyNo" <%= props.getProperty("historyRenalFailureInsufficiencyNo", "") %>>No</input>
								<input type="checkbox" name="historyRenalFailureInsufficiencyYes"  <%= props.getProperty("historyRenalFailureInsufficiencyYes", "") %>">Yes</input>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="7">
								History of environmental allergies: 
								<input type="checkbox" name="historyEnvironmentalAllergiesNo"  <%= props.getProperty("historyEnvironmentalAllergiesNo", "") %>>No</input> 
								<input type="checkbox" name="historyEnvironmentalAllergiesYes" <%= props.getProperty("historyEnvironmentalAllergiesYes", "") %>>Yes</input>
								<input class="underline" name="historyEnvironmentalAllergiesText" style="width: 306px;" type="text" maxlength="30"  value="<%= props.getProperty("historyEnvironmentalAllergiesText", "") %>"></input>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="7">
								Anticoagulant use: 
								<input type="checkbox" name="anticoagulantUseNo" <%= props.getProperty("anticoagulantUseNo", "") %>>No</input>
								<input type="checkbox" name="anticoagulantUseYes" <%= props.getProperty("anticoagulantUseYes", "") %>>Yes</input>
								<input class="underline" name="anticoagulantUseText" value="<%= props.getProperty("anticoagulantUseText", "") %>" style="width: 434px;" type="text" maxlength="100"></input>
							</td>
						</tr>
						<tr>
							<td colspan="7">
								<table border="0" cellspacing="0" cellpadding="0" width="740px" height="50px">
									<tr>
										<th class="subject">Examination</th>
									</tr>
								</table>
							</td>
						</tr>
						
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Ears:
								<i><u>AD:</u></i>
								<input type="checkbox" name="earAD-WNL" <%= props.getProperty("earAD-WNL", "") %>/>WNL
								<input type="checkbox" name="earAD-Other" <%= props.getProperty("earAD-Other", "") %>/>
								<input class="underline" name="earAD-Text" value="<%= props.getProperty("earAD-Text", "") %>" style="width: 175px;" type="text" maxlength="50"></input>
								<i><u>AS:</u></i>
								<input type="checkbox" name="earAS-WNL"  <%= props.getProperty("earAS-WNL", "") %>>WNL</input>
								<input type="checkbox" name="earAS-Ohter" <%= props.getProperty("earAS-Ohter", "") %>></input>
								<input class="underline" name="earAS-Text" value="<%= props.getProperty("earAS-Text", "") %>" style="width: 215px;" type="text" maxlength="50"></input>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" colspan="7">
								Oral:
								<input type="checkbox" name="oralWNL" <%= props.getProperty("oralWNL", "") %>>WNL</input>
								<input type="checkbox" name="telangiectasia"  <%= props.getProperty("telangiectasia", "") %>>Telangiectasia</input>
								<input type="checkbox" name="oralOther" <%= props.getProperty("oralOther", "") %>></input>
								<input class="underline" name="oralText" value="<%= props.getProperty("oralText", "") %>" style="width: 418px;" type="text" maxlength="50"></input>  
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Neck:
								<input type="checkbox" name="neckNoAdenopathy"  <%=props.getProperty("neckNoAdenopathy","") %>/>No adenopathy
								<input type="checkbox" name="neckNoAdenopathyOther"  <%=props.getProperty("neckNoAdenopathyOther","") %> />
								<input class="underline" style="width: 464px;" type="text" name="neckNoAdenopathyText" value="<%= props.getProperty("neckNoAdenopathyText", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Cranial Nerve Screen:
								<input type="checkbox" name="cranialWNL"  <%=props.getProperty("cranialWNL","") %>/>WNL
								<input type="checkbox" name="cranialOther" <%=props.getProperty("cranialOther","") %> />Other:
								<input class="underline" style="width: 365px;" type="text" name="cranialText" value="<%= props.getProperty("cranialText", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<table>
									<tr>
										<td>Nose: Septal Deviation </td>
										<td><input type="checkbox" name="noseSeptalDeviationNone" <%= props.getProperty("noseSeptalDeviationNone", "") %>/>None</td>
										<td>
											<input type="checkbox" name="noseSeptalDeviationRight" <%= props.getProperty("noseSeptalDeviationRight", "") %>/>Right
										</td>
										<td>
											<input type="checkbox" name="noseSeptalDeviationLeft" <%= props.getProperty("noseSeptalDeviationLeft", "") %>/>Left
										</td>
										<td>
											<input type="checkbox" name="noseSeptalDeviationPerforation"  <%= props.getProperty("noseSeptalDeviationPerforation", "") %>/>Perforation
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="noseSeptalDeviationMild" <%= props.getProperty("noseSeptalDeviationMild", "") %>/>Mild
										</td>
										<td>
											<input type="checkbox" name="noseSeptalDeviationModerate" <%= props.getProperty("noseSeptalDeviationModerate", "") %>/>Moderate
										</td>
										<td>
											<input type="checkbox" name="noseSeptalDeviationNoneSever" <%= props.getProperty("noseSeptalDeviationNoneSever", "") %>/>Severe
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" align="left">
							</td>
							<td valign="top" align="left">
								Little's Area: 
							</td>
							<td colspan="6">
								<u><b>Right:</b></u>
								<input type="checkbox" name="littleAreaRightNormal"  <%= props.getProperty("littleAreaRightNormal", "littleAreaRightNormal") %>/>Normal
								<input type="checkbox" name="littleAreaRightBleedingSite"  <%= props.getProperty("littleAreaRightBleedingSite", "littleAreaRightBleedingSite") %>/>Bleeding Site
								<input type="checkbox" name="littleAreaRightTelangiectasia"  <%= props.getProperty("littleAreaRightTelangiectasia", "littleAreaRightTelangiectasia") %>/>Telangiectasia
							</td>
						</tr>
						<tr>
							<td valign="top" align="left">
							</td>
							<td valign="top" align="left">
								
							</td>
							<td colspan="6">
								<u><b>Left:</b></u>
								<input type="checkbox" name="littleAreaLeftNormal" <%= props.getProperty("littleAreaLeftNormal", "") %>/>Normal
								<input type="checkbox" name="littleAreaLeftBleedingSite"  <%= props.getProperty("littleAreaLeftBleedingSite", "") %>/>Bleeding Site
								<input type="checkbox" name="littleAreaLeftTelangiectasia" <%= props.getProperty("littleAreaLeftTelangiectasia", "") %>/>Telangiectasia
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="userInput1"  <%=props.getProperty("userInput1","") %>/>
								<textarea name="userInput1Text" style="border: 1px solid #000;width: 95%"><%= props.getProperty("userInput1Text", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Inferior Turbinates:
								<input type="checkbox" name="inferiorTurbinatesNormal" <%= props.getProperty("inferiorTurbinatesNormal", "") %>/>Normal
								<input type="checkbox" name="inferiorTurbinatesHypertrophy" <%= props.getProperty("inferiorTurbinatesHypertrophy", "") %>/>Hypertrophy
								<input type="checkbox" name="inferiorTurbinatesPallor"  <%= props.getProperty("inferiorTurbinatesPallor", "") %>/>Pallor
								<input type="checkbox" name="inferiorTurbinatesOther" <%= props.getProperty("inferiorTurbinatesOther", "") %>/>
								<input class="underline" style="width: 220px;" type="text" name="inferiorTurbinatesText" value="<%= props.getProperty("inferiorTurbinatesText", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<table>
									<tr>
										<td colspan="7">Endoscopy: Nasal Cavity</td>
									</tr>
									<tr>
										<td align="right" colspan="2"><b>Left:</b></td>
										<td><input type="checkbox" name="endoscopyNasalCavityLeftNormal" <%= props.getProperty("endoscopyNasalCavityLeftNormal", "") %>/>Normal</td>
										<td>
											<input type="checkbox" name="endoscopyNasalCavityLeftTumor" <%= props.getProperty("endoscopyNasalCavityLeftTumor", "") %>/>Tumor
										</td>
										<td>
											<input type="checkbox" name="endoscopyNasalCavityLeftPNBS"  <%= props.getProperty("endoscopyNasalCavityLeftPNBS", "") %>/>Posterior Nasal Bleeding Site
										</td>
									</tr>
									<tr>
										<td align="right" colspan="2"><b>Right:</b></td>
										<td><input type="checkbox" name="endoscopyNasalCavityRightNormal"  <%= props.getProperty("endoscopyNasalCavityRightNormal", "") %>/>Normal</td>
										<td>
											<input type="checkbox" name="endoscopyNasalCavityRightTumor" <%= props.getProperty("endoscopyNasalCavityRightTumor", "") %>/>Tumor
										</td>
										<td>
											<input type="checkbox" name="endoscopyNasalCavityRightPNBS" <%= props.getProperty("endoscopyNasalCavityRightPNBS", "") %>/>Posterior Nasal Bleeding Site
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<table>
									<tr>
										<td colspan="7">Nasopharynx</td>
									</tr>
									<tr>
										<td align="right" colspan="2"><b>Normal:</b></td>
										<td><input type="checkbox" name="nasopharynxNormal"  <%= props.getProperty("nasopharynxNormal", "") %>/></td>
										
									</tr>
									<tr>
										<td align="right" colspan="2"><b>Left:</b></td>
										
										<td>
											<input type="checkbox" name="nasopharynxLeftTumor"  <%= props.getProperty("nasopharynxLeftTumor", "") %>/>Tumor
										</td>
									</tr>
									<tr>
										<td align="right" colspan="2"><b>Right:</b></td>
										<td><input type="checkbox" name="nasopharynxRightNormal"  <%= props.getProperty("nasopharynxRightNormal", "") %>/>Normal</td>
										<td>
											<input type="checkbox" name="nasopharynxRightTumor"  <%= props.getProperty("nasopharynxRightTumor", "") %>/>Tumor
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Larynx:
								<input type="checkbox" name="larynxWNL" <%= props.getProperty("larynxWNL", "") %>/>WNL
								<input type="checkbox" name="larynxRVCP" <%= props.getProperty("larynxRVCP", "") %>/>RVCP
								<input type="checkbox" name="larynxLVCP"  <%= props.getProperty("larynxLVCP", "") %>/>LVCP
								<input type="checkbox" name="larynxOther"  <%= props.getProperty("larynxOther", "") %>/>
								<input class="underline" style="width: 400px;" type="text" name="larynxText" value="<%= props.getProperty("larynxText", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<table>
									<tr>
										<td>CT Scan:</td>
										<td>
											<input type="checkbox" name="ctScanCRS" <%=props.getProperty("ctScanCRS","") %>/>CRS
											<input type="checkbox" name="ctScanNYD" <%=props.getProperty("ctScanNYD","") %>/>NYD
										</td>
										
										<td>
											Tumor:
											<input type="checkbox" name="ctScanTumor" <%=props.getProperty("ctScanTumor","") %>/>Left
											<input type="checkbox" name="ctScanTumorLeft" <%=props.getProperty("ctScanTumorLeft","") %>/>Right
										</td>
										
									</tr>
									<tr>
										<td></td>
										<td colspan="1">
											<input type="checkbox" name="ctScanOther" <%=props.getProperty("ctScanOther","") %>/>Other:
											<input class="underline" style="width: 150px;" type="text" name="ctScanOtherText" value="<%= props.getProperty("ctScanOtherText", "") %>" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td>
								Laboratory Data:
							</td>
							<td>Hb:</td>
							<td>
								<input class="underline" style="width: 150px;" type="text" name="laboratoryDataHbText" value="<%= props.getProperty("laboratoryDataHbText", "") %>" />
							</td>
							<td>
								INR:
							</td>
							<td>
								<input class="underline" style="width: 150px;" type="text" name="laboratoryDataINRText" value="<%= props.getProperty("laboratoryDataINRText", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="userInput2"  <%=props.getProperty("userInput2","") %>/>
								<textarea name="userInput2Text" style="border: 1px solid #000;width: 95%"><%= props.getProperty("userInput2Text", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="6">
								<table border="0" valign="top">
									<tr>
										<td valign="top"><span style="font-weight: bold;">Impression:</span></td>
										<td valign="top" colspan="">
											<input type="checkbox" name="impressioinEpistaxis" <%=props.getProperty("impressioinEpistaxis","") %> />Epistaxis: 
										</td>
										<td align="left">
											<table>
												<tr>
													<td>
														<input type="checkbox" name="impressioinEpistaxisLeft"  <%=props.getProperty("impressioinEpistaxisLeft","") %> />Left 
													</td>
													<td>
														<input type="checkbox" name="impressioinEpistaxisRight" <%=props.getProperty("impressioinEpistaxisRight","") %> />Right
													</td>
													<td>
														<input type="checkbox" name="impressioinEpistaxisBilateral" <%=props.getProperty("impressioinEpistaxisBilateral","") %> />Bilateral													</td>
												</tr>
												<tr>
													<td>
														<input type="checkbox" name="impressioinEpistaxisAnterior"  <%=props.getProperty("impressioinEpistaxisAnterior","") %> />Anterior 
													</td>
													<td>
														<input type="checkbox" name="impressioinEpistaxisPosterior" <%=props.getProperty("impressioinEpistaxisPosterior","") %> />Posterior
													</td>
												</tr>
												<tr>
													<td>
														<input type="checkbox" name="impressioinEpistaxisActiveIssue" <%=props.getProperty("impressioinEpistaxisActiveIssue","") %> />Active Issue 
													</td>
													<td>
														<input type="checkbox" name="impressioinEpistaxisInactiveIssue" <%=props.getProperty("impressioinEpistaxisInactiveIssue","") %> />Inactive Issue
													</td>
												</tr>
											</table> 
										</td>
										
									</tr>
									<tr>
										<td valign="top"></td>
										<td valign="top" colspan="2">
											<input type="checkbox" name="impressioinNasalTumor"  <%=props.getProperty("impressioinNasalTumor","") %> />Nasal tumor 
										</td>
										<td>
											<input type="checkbox" name="impressioinNasalTumorLeft"  <%=props.getProperty("impressioinNasalTumorLeft","") %> />Left
										</td>
										<td>
											<input type="checkbox" name="impressioinNasalTumorRight"  <%=props.getProperty("impressioinNasalTumorRight","") %> />Right
										</td>
									</tr>
									<tr>
										<td valign="top"></td>
										<td valign="top" colspan="2">
											<input type="checkbox" name="impressioinNasopharyngealTumor"  <%=props.getProperty("impressioinNasopharyngealTumor","") %>/>Nasopharyngeal tumor 
										</td>
										
										<td>
											<input type="checkbox" name="impressioinNasopharyngealTumorLeft"  <%=props.getProperty("impressioinNasopharyngealTumorLeft","") %> />Left
										</td>
										<td>
											<input type="checkbox" name="impressioinNasopharyngealTumorRight"  <%=props.getProperty("impressioinNasopharyngealTumorRight","") %> />Right
										</td>
									</tr>
									<tr>
										<td valign="top"></td>
										<td valign="top" colspan="7">
											<input type="checkbox" name="impressioinOther" <%= props.getProperty("impressioinOther", "") %> />Other:
											<textarea name="impressioinOtherText" style="border: 1px solid #000;width: 86%"><%= props.getProperty("impressioinOtherText", "") %></textarea>
										</td>
									</tr>
								</table>
							</td>
							
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<span style="font-weight: bold;">Plan:</span>
							</td>
						</tr>
						
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="silverNiterateCASBL" <%= props.getProperty("silverNiterateCASBL", "") %>/>Silver nitrate cautery of anterior septal bleeding location
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="surgicalAnteriorNasalPack" <%= props.getProperty("surgicalAnteriorNasalPack", "") %>/>Surgicel anterior nasal pack
							</td>
						</tr>
						
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="bactrobanOintmentBid" <%= props.getProperty("bactrobanOintmentBid", "") %>/>Bactroban ointment bid in affected naris for 1/52, then OD for 1/52
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="flosealTreatmentBleedingSite" <%= props.getProperty("flosealTreatmentBleedingSite", "") %>/>Floseal treatment of bleeding site							
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="merocelPack" <%= props.getProperty("merocelPack", "") %>/>Merocel pack:
								<input type="checkbox" name="merocelPackLeft" <%= props.getProperty("merocelPackLeft", "") %>/>Left
								<input type="checkbox" name="merocelPackRight" <%= props.getProperty("merocelPackRight", "") %>/>Right
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="posteriorNasalPacking" <%= props.getProperty("posteriorNasalPacking", "") %>/>Posterior Nasal Packing:
								<input type="checkbox" name="posteriorNasalPackingLeft" <%= props.getProperty("posteriorNasalPackingLeft", "") %>/>Left
								<input type="checkbox" name="posteriorNasalPackingRight" <%= props.getProperty("posteriorNasalPackingRight", "") %>/>Right
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="surgicalTreatment" <%= props.getProperty("surgicalTreatment", "") %>/>Surgical treatment:
								<input type="checkbox" name="surgicalTreatmentSuctionCautery" <%= props.getProperty("surgicalTreatmentSuctionCautery", "") %>/>Suction cautery
								<input type="checkbox" name="surgicalTreatmentExcision" <%= props.getProperty("surgicalTreatmentExcision", "") %>/>Excision
								<input type="checkbox" name="surgicalTreatmentSPALigation" <%= props.getProperty("surgicalTreatmentSPALigation", "") %>/>SpA Ligation
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="septodermoplasty" <%= props.getProperty("septodermoplasty", "") %>/>Septodermoplasty:
								<input type="checkbox" name="septodermoplastyLeft" <%= props.getProperty("septodermoplastyLeft", "") %>/>Left
								<input type="checkbox" name="septodermoplastyRight" <%= props.getProperty("septodermoplastyRight", "") %>/>Right
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
							<input type="checkbox" name="surgicalRisks" <%= props.getProperty("surgicalRisks", "") %>/>
								Surgical risks:
							<input class="underline" style="width: 350px;" type="text" name="surgicalRisksText" value="<%= props.getProperty("surgicalRisksText", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="angiography" <%= props.getProperty("angiography", "") %>/>Angiography
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="tumorBiopsy" <%= props.getProperty("tumorBiopsy", "") %>/>Tumor biopsy
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="ctScanParanasalSinuses" <%= props.getProperty("ctScanParanasalSinuses", "") %>/>CT scan of the paranasal sinuses
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="planOther" <%= props.getProperty("planOther", "") %>/>
								Other:
								<input class="underline" style="width: 350px;" type="text" name="planOtherText" value="<%= props.getProperty("planOtherText", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" id="returnToClinic" name="returnToClinic" <%= props.getProperty("returnToClinic", "") %>/>
								Return to clinic:
								<input class="underline" style="width: 150px;" type="text" id="returnToClinicNumberOfMonths" name="returnToClinicNumberOfMonths" value="<%= props.getProperty("returnToClinicNumberOfMonths", "") %>" />
								months
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="noFollowUp" <%= props.getProperty("noFollowUp", "") %>/>No follow-up 
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="userInput3"  <%=props.getProperty("userInput3","") %>/>
								<textarea name="userInput3Text" style="border: 1px solid #000;width: 95%"><%= props.getProperty("userInput3Text", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="right" colspan="7">
								<br/>
								<input class="underline" style="width: 300px;" type="text" name="signatures" value="<%= props.getProperty("signatures", "") %>" />
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>

		<tr>
			<td valign="top">
			<table class="Head" class="hidePrint" height="5%">
				<tr>
					<td align="left">
					<%
  if (!bView) {
%> 
					<input type="submit" value="Save"
						onclick="javascript:if (checkBeforeSave()==true) return justSave(); else return false;" />
						
					<input type="submit" value="Save & Letter"
						onclick="javascript:if (checkBeforeSave()==true) return onSubmitAndOpenLetter(); else return false;" />
					<%
  }
%> <input type="button" value="Exit"
						onclick="javascript:return onExit();" /> <input type="button"
						value="Print" onclick="javascript:window.print();" /></td>
					<td align="right">Study ID: <%= props.getProperty("studyID", "N/A") %>
					<input type="hidden" name="studyID"
						value="<%= props.getProperty("studyID", "N/A") %>" /></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</html:form>
<script type="text/javascript">
function onSubmitAndOpenLetter() {
    document.forms[0].submit.value="formEpistaxisLetter";                
    
    var ret = is1CheckboxChecked(0, choiceFormat) && allAreNumeric(0, allNumericField) && areInRange(0, allMatch);                       

    if (ret==true) {                        
        ret = confirm("Are you sure you want to save this form?");            
    }                
    return ret;
}
function justSave() {
    document.forms[0].submit.value="exit";   
    var ret = is1CheckboxChecked(0, choiceFormat) && allAreNumeric(0, allNumericField) && areInRange(0, allMatch);                       
    if (ret==true) {                        
        ret = confirm("Are you sure you want to save this form?");            
    }                
    return ret;     
}
</script>
</body>
</html:html>
