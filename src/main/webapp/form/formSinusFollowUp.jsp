<%@ page import="oscar.util.*, oscar.form.*, oscar.form.data.*"%>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>


<%
    String formClass = "SinusFollowUp";
    String formLink = "formSinusFollowUp.jsp";

    int demoNo = Integer.parseInt(request.getParameter("demographic_no"));
    int formId = Integer.parseInt(request.getParameter("formId"));
    int provNo = Integer.parseInt((String) session.getAttribute("user"));
    FrmRecord rec = (new FrmRecordFactory()).factory(formClass);
    java.util.Properties props = rec.getFormRecord(LoggedInInfo.getLoggedInInfoFromSession(request),demoNo, formId);
    String project_home = request.getContextPath().substring(1);	
%>
<%
  boolean bView = false;
  if (request.getParameter("view") != null && request.getParameter("view").equals("1")) bView = true; 
%>
<html:html locale="true">
<head>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<title>Sinus Follow Up</title>
<html:base />
<link rel="stylesheet" type="text/css" media="all" href="../share/css/extractedFromPages.css"  />
</head>


<script type="text/javascript" language="Javascript">
    
    //var choiceFormat  = new Array(7,11,16,20);        
    var choiceFormat  = null;        
    var allNumericField = null;
    var allMatch = null;
    var action = "/<%=project_home%>/form/formname.do";
    
    function checkBeforeSave() {
        /*
        var distance = document.forms[0].elements[6].value;
        var re1 = new RegExp('^[0-9][0-9][0-9][0-9][\.][0-9]$');
        var re2 = new RegExp('^[0-9][0-9][0-9][\.][0-9]$');
        var re3 = new RegExp('^[0-9][0-9][\.][0-9]$');
        var re4 = new RegExp('^[0-9][\.][0-9]$');
        var match1 = document.forms[0].elements[6].value.match(re1);
        var match2 = document.forms[0].elements[6].value.match(re2);
        var match3 = document.forms[0].elements[6].value.match(re3);
        var match4 = document.forms[0].elements[6].value.match(re4);
        if (match1 || match2 || match3 || match4) {            
            if (isFormCompleted(6,21,2,3)==true) {
                return true;
            }    
        } else {
            alert("The input distance must be in ####.# format");
            return false;
        }
             
        return false;
        */
        return true;
    }

</script>
<style>
td{
	height: 33px;
}
.underline{
	border: none;
	border-bottom: 2px solid #000;
}
.higher-line{
	height: 50px;
	vertical-align: middle;
}
.head-title{
	font-weight: bold;
}
.x-space{
	display: inline-block;
	width:30px;
}
.text-border{
	border: 1px solid #000;
}
</style>
<script type="text/javascript" src="formScripts.js">
</script>
<body topmargin="0" leftmargin="0" rightmargin="0" onload="window.resizeTo(1024,800)">
<html:form action="/form/formname" method="post">
	<input type="hidden" name="demographic_no" value="<%= props.getProperty("demographic_no", "0") %>" />
	<input type="hidden" name="formCreated" value="<%= props.getProperty("formCreated", "") %>" />
	<input type="hidden" name="form_class" value="<%=formClass%>" />
	<input type="hidden" name="form_link" value="<%=formLink%>" />
	<input type="hidden" name="formId" value="<%=formId%>" />
	<input type="hidden" name="submit" value="followUpLetter" />

	<table border="0" cellspacing="0" cellpadding="0" width="740px" height="95%">
		<tr>
			<td>
			<table border="0" cellspacing="0" cellpadding="0" width="740px"
				height="50px">
				<tr>
					<th class="subject">Sinus Follow Up</th>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>
			<table border="0" cellspacing="0" cellpadding="0" height="85%" id="page1">
				<tr>
					<td>
					<table width="840px" height="620px" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="45px">
							</td>
							<td class="higher-line">
								FP/GP: 
								<input class="underline" style="width: 387px;" type="text" name="fp_gp" value="<%= props.getProperty("fp_gp", "") %>" />
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td class="higher-line">
								Diagnosis: 
								<input class="underline" style="width: 356px;" type="text" name="Diagnosis" value="<%= props.getProperty("Diagnosis", "") %>" />
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td class="higher-line">
								<input type="checkbox" name="Surgery_Date" <%= props.getProperty("Surgery_Date", "") %>/>Surgery Date: 
								<input class="underline" style="width: 300px;" type="text" name="Surgery_Date_text" value="<%= props.getProperty("Surgery_Date_text", "") %>" />
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td colspan="4">
								<table>
									<tr>
										<td colspan="4">
											<table>
												<tr>
													<td>Current Symptoms:</td>
													<td><input type="checkbox" name="Symptoms_None" <%= props.getProperty("Symptoms_None", "") %>/>None</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="Symptoms_Congestion" <%= props.getProperty("Symptoms_Congestion", "") %>/>Congestion</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="Symptoms_Pain" <%= props.getProperty("Symptoms_Pain", "") %>/>Pain:</td>
													<td><input type="checkbox" name="Symptoms_Pain_M" <%= props.getProperty("Symptoms_Pain_M", "") %>/>Midfacial</td>
													<td><input type="checkbox" name="Symptoms_Pain_F" <%= props.getProperty("Symptoms_Pain_F", "") %>/>Frontal</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="Symptoms_Obstruction" <%= props.getProperty("Symptoms_Obstruction", "") %>/>Obstruction:</td>
													<td><input type="checkbox" name="Symptoms_Obstruction_L" <%= props.getProperty("Symptoms_Obstruction_L", "") %>/>Left</td>
													<td><input type="checkbox" name="Symptoms_Obstruction_R" <%= props.getProperty("Symptoms_Obstruction_R", "") %>/>Right</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="Symptoms_Rhinorrhea" <%= props.getProperty("Symptoms_Rhinorrhea", "") %>/>Rhinorrhea</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="Symptoms_Smell" <%= props.getProperty("Symptoms_Smell", "") %>/>Smell:</td>
													<td><input type="checkbox" name="Symptoms_Smell_Hyposmia" <%= props.getProperty("Symptoms_Smell_Hyposmia", "") %>/>Hyposmia</td>
													<td><input type="checkbox" name="Symptoms_Smell_Anosmia" <%= props.getProperty("Symptoms_Smell_Anosmia", "") %>/>Anosmia</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="form_header_other" <%= props.getProperty("form_header_other", "") %>/>
								<textarea name="form_header_other_text" class="text-border" rows="2" cols="70"><%= props.getProperty("form_header_other_text", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td class="higher-line">
								<span class="head-title">Current Treatment:</span>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Steroid_Spray" <%= props.getProperty("Steroid_Spray", "") %>/>Steroid Spray
								<input class="underline" style="width: 30px;" type="text" name="Steroid_Spray_D_text" value="<%= props.getProperty("Steroid_Spray_D_text", "") %>" />&nbsp;D
								<span class="x-space"></span>
								<input type="checkbox" name="Saline" <%= props.getProperty("Saline", "") %>/>Saline
								<span class="x-space"></span>
								<input type="checkbox" name="Prednisone" <%= props.getProperty("Prednisone", "") %>/>Prednisone
								<span class="x-space"></span>
								<input type="checkbox" name="Antihistamine" <%= props.getProperty("Antihistamine", "") %>/>Antihistamine
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Singulair" <%= props.getProperty("Singulair", "") %>/>Singulair
								<span class="x-space"></span>
								<input type="checkbox" name="Pulmicort_Irrigations" <%= props.getProperty("Pulmicort_Irrigations", "") %>/>Pulmicort Irrigations(
								<input class="underline" style="width: 30px;" type="text" name="Pulmicort_Irrigations_text" value="<%= props.getProperty("Pulmicort_Irrigations_text", "") %>" />mg/mL)
								<span class="x-space"></span>
								<input type="checkbox" name="Immunotherapy" <%= props.getProperty("Immunotherapy", "") %>/>Immunotherapy
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Abx" <%= props.getProperty("Abx", "") %>/>Abx
								<input class="underline" style="width: 375px;" type="text" name="Abx_text" value="<%= props.getProperty("Abx_text", "") %>" />
								<span class="x-space"></span>
								<input type="checkbox" name="Low_dose_Macrolide" <%= props.getProperty("Low_dose_Macrolide", "") %>/>Low dose Macrolide
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Current_Treatment_other" <%= props.getProperty("Current_Treatment_other", "") %>/>
								<textarea name="Current_Treatment_other_text" class="text-border" rows="3" cols="70"><%= props.getProperty("Current_Treatment_other_text", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td class="higher-line">
								<span class="head-title">Examination:</span>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								Ears:<span class="x-space"></span>AD:
								<input type="checkbox" name="Ears_AD_WNL" <%= props.getProperty("Ears_AD_WNL", "") %>/>WNL
								<input type="checkbox" name="Ears_AD_other" <%= props.getProperty("Ears_AD_other", "") %>/>
								<input class="underline" style="width: 150px;" type="text" name="Ears_AD_other_text" value="<%= props.getProperty("Ears_AD_other_text", "") %>" />
								<span class="x-space"></span>AS:
								<input type="checkbox" name="Ears_AS_WNL" <%= props.getProperty("Ears_AS_WNL", "") %>/>WNL
								<input type="checkbox" name="Ears_AS_other" <%= props.getProperty("Ears_AS_other", "") %>/>
								<input class="underline" style="width: 150px;" type="text" name="Ears_AS_other_text" value="<%= props.getProperty("Ears_AS_other_text", "") %>" />
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								Oral:
								<input type="checkbox" name="Oral_WNL" <%= props.getProperty("Oral_WNL", "") %>/>WNL
								<span class="x-space"></span>
								<input type="checkbox" name="Oral_other" <%= props.getProperty("Oral_other", "") %>/>
								<input class="underline" style="width: 150px;" type="text" name="Oral_other_text" value="<%= props.getProperty("Oral_other_text", "") %>" />
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Examination_other" <%= props.getProperty("Examination_other", "") %>/>
								<textarea name="Examination_other_text" class="text-border" rows="2" cols="50"><%= props.getProperty("Examination_other_text", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								Neck:
								<input type="checkbox" name="Neck_No_adenopathy" <%= props.getProperty("Neck_No_adenopathy", "") %>/>No adenopathy
								<span class="x-space"></span>
								<input type="checkbox" name="Neck_other" <%= props.getProperty("Neck_other", "") %>/>
								<input class="underline" style="width: 150px;" type="text" name="Neck_other_text" value="<%= props.getProperty("Neck_other_text", "") %>" />
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								Cranial Nerve Screen:
								<input type="checkbox" name="CNS_WNL" <%= props.getProperty("CNS_WNL", "") %>/>WNL
								<span class="x-space"></span>
								<input type="checkbox" name="CNS_WNL_other" <%= props.getProperty("CNS_WNL_other", "") %>/>
								Other:
								<input class="underline" style="width: 250px;" type="text" name="CNS_WNL_other_text" value="<%= props.getProperty("CNS_WNL_other_text", "") %>" />
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								Nose : Septal Deviation:
								<input type="checkbox" name="Nose_Septal_Deviation_None" <%= props.getProperty("Nose_Septal_Deviation_None", "") %> />None
								<input type="checkbox" name="Nose_Septal_Deviation_R" <%= props.getProperty("Nose_Septal_Deviation_R", "") %> />Right
								<input type="checkbox" name="Nose_Septal_Deviation_L" <%= props.getProperty("Nose_Septal_Deviation_L", "") %> />Left
								<input type="checkbox" name="Nose_Septal_Deviation_Mild" <%= props.getProperty("Nose_Septal_Deviation_Mild", "") %> />Mild
								<input type="checkbox" name="Nose_Septal_Deviation_Moderate" <%= props.getProperty("Nose_Septal_Deviation_Moderate", "") %> />Moderate
								<input type="checkbox" name="Nose_Septal_Deviation_S" <%= props.getProperty("Nose_Septal_Deviation_S", "") %> />Severe
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Examination_nose_other" <%= props.getProperty("Examination_nose_other", "") %>/>
								<textarea name="Examination_nose_other_text" class="text-border" rows="2" cols="70"><%= props.getProperty("Examination_nose_other_text", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cottle Maneuver:
								<span class="x-space"></span>
								Right:<input type="checkbox" name="Maneuver_R_Pos" <%= props.getProperty("Maneuver_R_Pos", "") %>/>Pos
								<input type="checkbox" name="Maneuver_R_Neg" <%= props.getProperty("Maneuver_R_Neg", "") %>/>Neg
								<span class="x-space"></span>
								Left:<input type="checkbox" name="Maneuver_L_Pos" <%= props.getProperty("Maneuver_L_Pos", "") %>/>Pos
								<input type="checkbox" name="Maneuver_L_Neg" <%= props.getProperty("Maneuver_L_Neg", "") %>/>Neg
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dorsum Deformity:
								<input type="checkbox" name="Deformity_shaped" <%= props.getProperty("Deformity_shaped", "") %>/>C-shaped
								<span class="x-space"></span>
								<input type="checkbox" name="Deformity_Reverse" <%= props.getProperty("Deformity_Reverse", "") %>/>Reverse C
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Inferior Turbinates:
								<input type="checkbox" name="Turbinates_Normal" <%= props.getProperty("Turbinates_Normal", "") %>/>Normal
								<input type="checkbox" name="Turbinates_Hypertrophy" <%= props.getProperty("Turbinates_Hypertrophy", "") %>/>Hypertrophy
								<input type="checkbox" name="Turbinates_Pallor" <%= props.getProperty("Turbinates_Pallor", "") %>/>Pallor
								<input type="checkbox" name="Turbinates_other" <%= props.getProperty("Turbinates_other", "") %>/>
								<input class="underline" style="width: 150px;" type="text" name="Turbinates_other_text" value="<%= props.getProperty("Turbinates_other_text", "") %>" />
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="splints_out" <%= props.getProperty("splints_out", "") %>/>splints out
								<span class="x-space"></span>
								<input type="checkbox" name="columellar_sutures_removed" <%= props.getProperty("columellar_sutures_removed", "") %>/>columellar sutures removed
								<span class="x-space"></span>
								<input type="checkbox" name="dorsum_dress_removed" <%= props.getProperty("dorsum_dress_removed", "") %>/>dorsum dress. removed
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Healing" <%= props.getProperty("Healing", "") %>/>Healing:
								<textarea name="Healing_text" class="text-border" rows="2" cols="60"><%= props.getProperty("Healing_text", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td colspan="7">
								<table>
									<tr>
										<td>Endoscopy: </td>
										<td><input type="checkbox" name="endoscopy_polyps" <%= props.getProperty("endoscopy_polyps", "") %>/>Polyps:</td>
										<td>
											<input type="checkbox" name="endoscopy_grade_left" <%= props.getProperty("endoscopy_grade_left", "") %>/>Left:
											Grade&nbsp;&nbsp;<input class="underline" style="width: 40px;" type="text" name="endoscopy_grade_left_text" value="<%= props.getProperty("endoscopy_grade_left_text", "") %>" />/4
										</td>
										<td>
											<input type="checkbox" name="endoscopy_grade_right" <%= props.getProperty("endoscopy_grade_right", "") %>/>Right:
											Grade&nbsp;&nbsp;<input class="underline" style="width: 40px;" type="text" name="endoscopy_grade_right_text" value="<%= props.getProperty("endoscopy_grade_right_text", "") %>" />/4
										</td>
									</tr>
									<tr>
										<td></td>
										<td><input type="checkbox" name="endoscopy_pus" <%= props.getProperty("endoscopy_pus", "") %>/>Pus:</td>
										<td>
											<input type="checkbox" name="endoscopy_pus_left" <%= props.getProperty("endoscopy_pus_left", "") %>/>Left
										</td>
										<td>
											<input type="checkbox" name="endoscopy_pus_right" <%= props.getProperty("endoscopy_pus_right", "") %>/>Right
										</td>
									</tr>
									
									<tr>
										<td></td>
										<td><input type="checkbox" name="endoscopy_inflammation" <%= props.getProperty("endoscopy_inflammation", "") %>/>Inflammation:</td>
										<td>
											<input type="checkbox" name="endoscopy_inflammation_left" <%= props.getProperty("endoscopy_inflammation_left", "") %>/>Left:
											Grade&nbsp;&nbsp;<input class="underline" style="width: 40px;" type="text" name="endoscopy_infl_grade_left_text" value="<%= props.getProperty("endoscopy_infl_grade_left_text", "") %>" />/4											
										</td>
										<td>
											<input type="checkbox" name="endoscopy_inflammation_right" <%= props.getProperty("endoscopy_inflammation_right", "") %>/>Right:
											Grade&nbsp;&nbsp;<input class="underline" style="width: 40px;" type="text" name="endoscopy_infl_grade_right_text" value="<%= props.getProperty("endoscopy_infl_grade_right_text", "") %>" />/4
										</td>
									</tr>
									
									<tr>
										<td></td>
										<td colspan="2">
											<input type="checkbox" name="endoscopy_Irrigated" <%= props.getProperty("endoscopy_Irrigated", "") %>/>Irrigated and debrided
										</td>
										<td>
											<input type="checkbox" name="endoscopy_MTS" <%= props.getProperty("endoscopy_MTS","") %>/>MTS removed
										</td>
									</tr>
									<tr>
										<td></td>
										<td colspan="3">
											<input type="checkbox" name="endoscopy_other" <%= props.getProperty("endoscopy_other", "") %>/>Other:
											<input class="underline" style="width: 450px;" type="text" name="endoscopy_other_text" value="<%= props.getProperty("endoscopy_other_text", "") %>" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								Larynx:
								<span class="x-space"></span>
								<input type="checkbox" name="Larynx_WNL" <%= props.getProperty("Larynx_WNL", "") %>/>WNL
								<span class="x-space"></span>
								<input type="checkbox" name="Larynx_RVCP" <%= props.getProperty("Larynx_RVCP", "") %>/>RVCP
								<span class="x-space"></span>
								<input type="checkbox" name="Larynx_LVCP" <%= props.getProperty("Larynx_LVCP", "") %>/>LVCP
								<span class="x-space"></span>
<%-- 								<input type="checkbox" name="Larynx_other" <%= props.getProperty("Larynx_other", "") %>/> --%>
<%-- 								<input class="underline" style="width: 200px;" type="text" name="Larynx_other_text" value="<%= props.getProperty("Larynx_other_text", "") %>" /> --%>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Larynx_other" <%= props.getProperty("Larynx_other", "") %>/>
								<textarea name="Larynx_other_text" class="text-border" rows="2" cols="70"><%= props.getProperty("Larynx_other_text", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<table>
									<tr>
										<td>CT Scan:</td>
										<td>
											<input type="checkbox" name="CT_Scan_Normal" <%= props.getProperty("CT_Scan_Normal", "") %>/>Normal
											<span class="x-space"></span>
											<input type="checkbox" name="CT_Scan_Chronic" <%= props.getProperty("CT_Scan_Chronic", "") %>/>Chronic Sinusitis
										</td>
									</tr>
									<tr>
										<td>
										</td>
										<td>
											<input type="checkbox" name="CT_Scan_Tumor" <%= props.getProperty("CT_Scan_Tumor", "") %>/>Tumor:
											<input type="checkbox" name="CT_Scan_Tumor_L" <%= props.getProperty("CT_Scan_Tumor_L", "") %>/>Left
											<span class="x-space"></span>
											<input type="checkbox" name="CT_Scan_Tumor_R" <%= props.getProperty("CT_Scan_Tumor_R", "") %>/>Right
										</td>
									</tr>
									<tr>
										<td>
										</td>
										<td>
											<input type="checkbox" name="CT_Scan_Not_Yet" <%= props.getProperty("CT_Scan_Not_Yet", "") %>/>Not Yet Done
										</td>
									</tr>
									<tr>
										<td>
										</td>
										<td>
											<input type="checkbox" name="CT_Scan_other" <%= props.getProperty("CT_Scan_other", "") %>/>Other:
											<textarea name="CT_Scan_other_text" class="text-border" rows="2" cols="50"><%= props.getProperty("CT_Scan_other_text", "") %></textarea>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								Laboratory Data:
								IgE
								<input class="underline" style="width: 80px;" type="text" name="Lab_IgE" value="<%= props.getProperty("Lab_IgE", "") %>" />								
								<span class="x-space"></span>
								Ig GAM
								<input class="underline" style="width: 80px;" type="text" name="Lab_Ig_GAM" value="<%= props.getProperty("Lab_Ig_GAM", "") %>" />								
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="lab_other" <%= props.getProperty("lab_other", "") %>/>CQS:
								<textarea name="lab_other_text" class="text-border" rows="2" cols="50"><%= props.getProperty("lab_other_text", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td colspan="7">
								<table>
									<tr>
										<td>Impression:</td>
										<td>
											<input type="checkbox" name="impression_Sinusitis" <%= props.getProperty("impression_Sinusitis", "") %> />Sinusitis: 
											<input type="checkbox" name="impression_Sinusitis_with_Polyps" <%= props.getProperty("impression_Sinusitis_with_Polyps", "") %> />CRS with Polyps 
											<input type="checkbox" name="impression_Sinusitis_without_Polyps" <%= props.getProperty("impression_Sinusitis_without_Polyps", "") %> />CRS without Polyps 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<span style="display: inline-block;width: 96px;"></span>
											<input type="checkbox" name="impression_CRS_controlled" <%= props.getProperty("impression_CRS_controlled", "") %> />CRS controlled
											<input type="checkbox" name="impression_CRS_not_controlled" <%= props.getProperty("impression_CRS_not_controlled", "") %> />CRS not controlled
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression_Rhinitis" <%= props.getProperty("impression_Rhinitis", "") %> />Rhinitis: 
											<input type="checkbox" name="impression_Rhinitis_Allergic" <%= props.getProperty("impression_Rhinitis_Allergic", "") %> />Allergic 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<span style="display: inline-block;width: 88px;"></span> 
											<input type="checkbox" name="impression_Rhinitis_Nonallergic" <%= props.getProperty("impression_Rhinitis_Nonallergic", "") %> />Nonallergic 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<span style="display: inline-block;width: 88px;"></span> 
											<input type="checkbox" name="impression_Rhinitis_Vasomotor" <%= props.getProperty("impression_Rhinitis_Vasomotor", "") %> />Vasomotor 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<span style="display: inline-block;width: 88px;"></span> 
											<input type="checkbox" name="impression_Rhinitis_other" <%= props.getProperty("impression_Rhinitis_other", "") %> />
											<textarea name="impression_Rhinitis_other_text" class="text-border" rows="3" cols="42"><%= props.getProperty("impression_Rhinitis_other_text", "") %></textarea> 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression_SinonasalsTumor" <%= props.getProperty("impression_SinonasalsTumor", "") %> />Sinonasal Tumor:
											<textarea name="impression_SinonasalsTumor_text" class="text-border" rows="5" cols="36"><%= props.getProperty("impression_SinonasalsTumor_text", "") %></textarea> 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression_SeptalDeviation" <%= props.getProperty("impression_SeptalDeviation", "") %>/>Septal Deviation: 
											<input type="checkbox" name="impression_SeptalDeviation_L" <%= props.getProperty("impression_SeptalDeviation_L", "") %> />Left
											<input type="checkbox" name="impression_SeptalDeviation_R" <%= props.getProperty("impression_SeptalDeviation_R", "") %> />Right										 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression_NasoseptalDeviation" <%= props.getProperty("impression_NasoseptalDeviation", "") %> />Nasoseptal Deviation:
											<input type="checkbox" name="impression_NasoseptalDeviation_L" <%= props.getProperty("impression_NasoseptalDeviation_L", "") %> />Left
											<input type="checkbox" name="impression_NasoseptalDeviation_R" <%= props.getProperty("impression_NasoseptalDeviation_R", "") %> />Right
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression_well" <%= props.getProperty("impression_well", "") %> />Healed well following surgery. No concerns.
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression_Headache" <%= props.getProperty("impression_Headache", "") %> />Headache 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impression_other" <%= props.getProperty("impression_other", "") %> />Other:
											<textarea name="impression_other_text" class="text-border" rows="6" cols="47"><%= props.getProperty("impression_other_text", "") %></textarea>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<span style="font-weight: bold;">Plan:</span>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<span style="font-weight: bold;">Medical:</span>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Medical_saline" <%= props.getProperty("Medical_saline", "") %>/>Saline Irrigation OD-BID
								<input type="checkbox" name="Medical_singulair" <%= props.getProperty("Medical_singulair", "") %>s/>Singulair 10 mg OD
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Medical_antibiotic" <%= props.getProperty("Medical_antibiotic", "") %>/>Antibiotic:
								&nbsp;&nbsp;&nbsp;
								Duration:
								<input class="underline" style="width: 50px;" type="text" name="Medical_antibiotic_days" value="<%= props.getProperty("Medical_antibiotic_days", "") %>" />								
								days
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Medical_cefuroxime" <%= props.getProperty("Medical_cefuroxime", "") %>/>Cefuroxime (500 mg BID)
								<input type="checkbox" name="Medical_clavulin" <%= props.getProperty("Medical_clavulin", "") %>/>Clavulin (875 mg BID)
								<input type="checkbox" name="Medical_clarithromycin" <%= props.getProperty("Medical_clarithromycin", "") %>/>Clarithromycin (500 mg BID)
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Medical_other" <%= props.getProperty("Medical_other", "") %>/>
								<input class="underline" style="width: 600px;" type="text" name="Medical_other_text" value="<%= props.getProperty("Medical_other_text", "") %>" />								
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Medical_low_dose" <%= props.getProperty("Medical_low_dose", "") %>/>
								Low Dose Macrolide (Clarithromycin 250 mg po OD) for 3 months								
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<span style="font-weight: bold;">Steroids:</span>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Steroids_incs" <%= props.getProperty("Steroids_incs", "") %>/>Intranasal Steroid Spray (2 sprays/nostril 
								<input class="underline" style="width: 50px;" type="text" name="Steroids_incs_days" value="<%= props.getProperty("Steroids_incs_days", "") %>" />								
								D)
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Steroids_prednisone" <%= props.getProperty("Steroids_prednisone", "") %>/>Prednisone: 
								<input class="underline" style="width: 50px;" type="text" name="Steroids_prednisone_mg" value="<%= props.getProperty("Steroids_prednisone_mg", "") %>" />								
								mg po OD for
								<input class="underline" style="width: 50px;" type="text" name="Steroids_prednisone_days" value="<%= props.getProperty("Steroids_prednisone_days", "") %>" />								
								days.(risks: GI, Psych, AVN hip)
								<input type="checkbox" name="Steroids_prednisone_discussed" <%= props.getProperty("Steroids_prednisone_discussed", "") %>/>
								<span style="color: red;">discussed</span>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Steroids_pulmicort" <%= props.getProperty("Steroids_pulmicort", "") %>/>Pulmicort Irrigations:
								<input class="underline" style="width: 50px;" type="text" name="Steroids_pulmicort_mg" value="<%= props.getProperty("Steroids_pulmicort_mg", "") %>" />								
								mg/mL, 2 mL respule.1 respule in 240 mL saline, irrigate with 120 mL BID.)
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<span style="font-weight: bold;">Allergy:</span>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Allergy_Referral" <%= props.getProperty("Allergy_Referral", "") %>/>Allergist Referral:
								<input type="checkbox" name="Allergy_Referral_today" <%= props.getProperty("Allergy_Referral_today", "") %>/>Requested Today
								<input type="checkbox" name="Allergy_Referral_pls" <%= props.getProperty("Allergy_Referral_pls", "") %>/>Please Refer this Patient
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Allergy_Immunotherapy" <%= props.getProperty("Allergy_Immunotherapy", "") %>/>Immunotherapy evaluation with current allergist
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Allergy_ASA" <%= props.getProperty("Allergy_ASA", "") %>/>ASA desensitization
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Anti_ige" <%= props.getProperty("Anti_ige", "") %>/>Anti-IgE therapy evaluation
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="Allergy_other" <%= props.getProperty("Allergy_other", "") %> />
								<textarea name="Allergy_other_text" class="text-border" rows="2" cols="50"><%= props.getProperty("Allergy_other_text", "") %></textarea> 
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<span style="font-weight: bold;">Surgery:</span>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="surgery_1" <%= props.getProperty("surgery_1", "") %>/>
								<input class="underline" style="width: 60px;" type="text" name="surgery_1_text" value="<%= props.getProperty("surgery_1_text", "") %>" />
								Endoscopic Sinus Surgery (risks: orbital injury, CSF leak, bleeding, pain, infection, recurrence) 
								<input type="checkbox" name="surgery_1_discussed" <%= props.getProperty("surgery_1_discussed", "") %>/>
								<span style="color: red;">discussed</span>								
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="surgery_2" <%= props.getProperty("surgery_2", "") %>/>
								Endonasal Endoscopic Resection of
								<input class="underline" style="width: 60px;" type="text" name="surgery_2_text" value="<%= props.getProperty("surgery_2_text", "") %>" />
								Sinonasal Tumor(risks: orbital injury, CSF leak, bleeding, pain, infection, recurrence, post-op care) 
								<input type="checkbox" name="surgery_2_discussed" <%= props.getProperty("surgery_2_discussed", "") %>/>
								<span style="color: red;">discussed</span>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="surgery_3" <%= props.getProperty("surgery_3", "") %>/>
								Septoplasty and Bilateral Inferior Turbinate Reduction (risks: pain, bleeding, infection, residual obstruction) 
								<input type="checkbox" name="surgery_3_discussed" <%= props.getProperty("surgery_3_discussed", "") %>/>
								<span style="color: red;">discussed</span>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="surgery_4" <%= props.getProperty("surgery_4", "") %>/>
								<input class="underline" style="width: 60px;" type="text" name="surgery_4_text" value="<%= props.getProperty("surgery_4_text", "") %>" />
								Septorhinoplasty (risks: pain, bleeding, infection, scar, residual deviation or obstruction) 
								<input type="checkbox" name="surgery_4_discussed" <%= props.getProperty("surgery_4_discussed", "") %>/>
								<span style="color: red;">discussed</span>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="surgery_5" <%= props.getProperty("surgery_5", "") %>/>
								CT scan of the paranasal sinuses 
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="surgery_6" <%= props.getProperty("surgery_6", "") %>/>
								Given the abscence of sinus disease, I suggest you refer this patient to a neurologist for a headache evaluation.
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="surgery_7" <%= props.getProperty("surgery_7", "") %>/>
								<textarea name="surgery_7_text" class="text-border" rows="8" cols="80"><%= props.getProperty("surgery_7_text", "") %></textarea>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
								<input type="checkbox" name="surgery_8" <%= props.getProperty("surgery_8", "") %>/>
								Return to clinic: <input class="underline" style="width: 60px;" type="text" name="surgery_8_text" value="<%= props.getProperty("surgery_8_text", "") %>" />
								months
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td align="right">
								<br/>
								<br/>
								<br/>
								<input class="underline" style="width: 300px;" type="text" name="signatures" value="<%= props.getProperty("signatures", "") %>" />
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td valign="top">
			<table class="Head" class="hidePrint" height="5%">
				<tr>
					<td align="left">
					<%
					  if (!bView) {
					%> 
						<input type="submit" value="Save"
							onclick="javascript:if (checkBeforeSave()==true) return justSave(); else return false;" />
						
						<input type="submit" value="Save & Letter"
							onclick="javascript:if (checkBeforeSave()==true) return onSubmitAndOpenLetter(); else return false;" />
					<%
				  		}
					%> 						
						<input type="button" value="Exit" onclick="javascript:return onExit();" />
						<input type="button" value="Print" onclick="javascript:window.print();" />
					</td>					
				</tr>
			</table>
			</td>
		</tr>
	</table>
	<div style="height: 100px;"></div>
</html:form>
<script type="text/javascript">
function onSubmitAndOpenLetter() {
    document.forms[0].submit.value="followUpLetter";                
    
    var ret = is1CheckboxChecked(0, choiceFormat) && allAreNumeric(0, allNumericField) && areInRange(0, allMatch);                       

    if (ret==true) {                        
        ret = confirm("Are you sure you want to save this form?");            
    }                
    return ret;
}
function justSave() {
    document.forms[0].submit.value="exit";   
    var ret = is1CheckboxChecked(0, choiceFormat) && allAreNumeric(0, allNumericField) && areInRange(0, allMatch);                       
    if (ret==true) {                        
        ret = confirm("Are you sure you want to save this form?");            
    }                
    return ret;     
}
</script>
</body>
</html:html>
