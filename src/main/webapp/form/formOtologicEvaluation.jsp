<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<%@ page import="oscar.util.*, oscar.form.*, oscar.form.data.*"%>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

    
<%
    String formClass = "OtologicEvaluation";
    String formLink = "formOtologicEvaluation.jsp";

    int demoNo = Integer.parseInt(request.getParameter("demographic_no"));
    int formId = Integer.parseInt(request.getParameter("formId"));
    int provNo = Integer.parseInt((String) session.getAttribute("user"));
    FrmRecord rec = (new FrmRecordFactory()).factory(formClass);
    java.util.Properties props = rec.getFormRecord(LoggedInInfo.getLoggedInInfoFromSession(request),demoNo, formId);

    //FrmData fd = new FrmData();    String resource = fd.getResource(); resource = resource + "ob/riskinfo/";

    //get project_home
    String project_home = request.getContextPath().substring(1);	
%>
<%
  boolean bView = false;
  if (request.getParameter("view") != null && request.getParameter("view").equals("1")) bView = true; 
%>
<%!
	private String outputChecked(java.util.Properties props,String attr,String value) {
		Object obj = props.get(attr);
		if (obj != null && obj.toString().equals(value)) {
			return " checked='checked' ";
		} else {
			return "";
		}
	}
%>
<html:html locale="true">
<head>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<title>Otologic Evaluation</title>
<html:base />
<link rel="stylesheet" type="text/css" media="all" href="../share/css/extractedFromPages.css"  />
</head>


<script type="text/javascript" language="Javascript">
    
    //var choiceFormat  = new Array(7,11,16,20);        
    var choiceFormat  = null;        
    var allNumericField = null;
    var allMatch = null;
    var action = "/<%=project_home%>/form/formname.do";
    
    function checkBeforeSave() {
        /*
        var distance = document.forms[0].elements[6].value;
        var re1 = new RegExp('^[0-9][0-9][0-9][0-9][\.][0-9]$');
        var re2 = new RegExp('^[0-9][0-9][0-9][\.][0-9]$');
        var re3 = new RegExp('^[0-9][0-9][\.][0-9]$');
        var re4 = new RegExp('^[0-9][\.][0-9]$');
        var match1 = document.forms[0].elements[6].value.match(re1);
        var match2 = document.forms[0].elements[6].value.match(re2);
        var match3 = document.forms[0].elements[6].value.match(re3);
        var match4 = document.forms[0].elements[6].value.match(re4);
        if (match1 || match2 || match3 || match4) {            
            if (isFormCompleted(6,21,2,3)==true) {
                return true;
            }    
        } else {
            alert("The input distance must be in ####.# format");
            return false;
        }
             
        return false;
        */
        return true;
    }

</script>
<style>
td{
	height: 27px;
}
.underline{
	border: none;
	border-bottom: 2px solid #000;
}
</style>
<script type="text/javascript" src="formScripts.js">
    
</script>


<body bgproperties="fixed" topmargin="0" leftmargin="0" rightmargin="0"
	onload="window.resizeTo(1024,800)">
<html:form action="/form/formname" method="post">
	<input type="hidden" name="demographic_no"
		value="<%= props.getProperty("demographic_no", "0") %>" />
	<input type="hidden" name="formCreated"
		value="<%= props.getProperty("formCreated", "") %>" />
	<input type="hidden" name="form_class" value="<%=formClass%>" />
	<input type="hidden" name="form_link" value="<%=formLink%>" />
	<input type="hidden" name="formId" value="<%=formId%>" />
	<input type="hidden" name="submit" value="formOtologicLetter" />

	<table border="0" cellspacing="0" cellpadding="0" width="740px"
		height="95%">
		<tr>
			<td>
			<table border="0" cellspacing="0" cellpadding="0" width="740px"
				height="50px">
				<tr>
					<th class="subject">Otologic Evaluation</th>
				</tr>				
				<br>
			</table>
			</td>
		</tr>
		<tr>
			<td valign="top">
			<table border="0" cellspacing="0" cellpadding="0" height="85%"
				width="740px" id="page1">
				<tr>
					<td colspan="2">
					<table width="740px" height="620px" border="0" cellspacing="0"
						cellpadding="0">
						<tr>
							<td valign="top" align="right">
								&nbsp;&nbsp;&nbsp;&nbsp;
							</td>
							<td valign="top" colspan="4">
								Referring MD: 
								<input class="underline" style="width: 300px;" type="text" maxlength="100" name="referringMD" value="<%= props.getProperty("referringMD", "") %>" />
								<br/>
								<br/>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" colspan="4">
								<table>
									<tr>
										<td>
											RFR:
										</td>
										<td><input type="checkbox" name="rfrHearLoss" <%= props.getProperty("rfrHearLoss", "") %>>Hearing loss</input></td>
										<td><input type="checkbox" name="rfrVertigo" <%= props.getProperty("rfrVertigo", "") %>>Vertigo</input></td>
										<td><input type="checkbox" name="rfrTinnitus" <%= props.getProperty("rfrTinnitus", "") %>>Tinnitus</input></td>
									</tr>
									<tr>
										<td>
										</td>
										<td><input type="checkbox" name="rfrOtitis" <%= props.getProperty("rfrOtitis", "") %>>Otitis</input></td>
										<td colspan="2">
											<input type="checkbox" name="rfrOther" <%= props.getProperty("rfrOther", "") %>/>
											<input class="underline" style="width: 150px;" type="text" name="rfrOtherText" value="<%= props.getProperty("rfrOtherText", "") %>" />
										</td>
									</tr>
									<tr>
										<td></td>
										<td colspan="4">
											<table>
												<tr>
													<td>Otologic Symptoms:</td>
													<td><input type="checkbox" name="symNone" <%= props.getProperty("symNone", "") %>/>None</td>
												</tr>												
												<tr>
													<td></td>
													<td><input type="checkbox" name="symHearLoss" <%= props.getProperty("symHearLoss", "") %>/>Hearing loss:</td>
													<td><input type="checkbox" name="symHearLossLeft" <%= props.getProperty("symHearLossLeft", "") %>/>LEFT</td>
													<td><input type="checkbox" name="symHearLossRight" <%= props.getProperty("symHearLossRight", "") %>/>RIGHT</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="symOtorrhea" <%= props.getProperty("symOtorrhea", "") %>/>Otorrhea:</td>
													<td><input type="checkbox" name="symOtorrheaLeft" <%= props.getProperty("symOtorrheaLeft", "") %>/>LEFT</td>
													<td><input type="checkbox" name="symOtorrheaRight" <%= props.getProperty("symOtorrheaRight", "") %>/>RIGHT</td>
												</tr>
												
												<tr>
													<td></td>
													<td><input type="checkbox" name="symOtalgia" <%= props.getProperty("symOtalgia", "") %>/>Otalgia:</td>
													<td><input type="checkbox" name="symOtalgiaLeft" <%= props.getProperty("symOtalgiaLeft", "") %>/>LEFT</td>
													<td><input type="checkbox" name="symOtalgiaRight" <%= props.getProperty("symOtalgiaRight", "") %>/>RIGHT</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="symTinnitus" <%= props.getProperty("symTinnitus", "") %>/>Tinnitus:</td>
													<td><input type="checkbox" name="symTinnitusLeft" <%= props.getProperty("symTinnitusLeft", "") %>/>LEFT</td>
													<td><input type="checkbox" name="symTinnitusRight" <%= props.getProperty("symTinnitusRight", "") %>/>RIGHT</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="symPulsatile" <%= props.getProperty("symPulsatile", "") %>/>Pulsatile</td>
												</tr>
												<tr>
													<td></td>
													<td><input type="checkbox" name="symVertigo" <%= props.getProperty("symVertigo", "") %>/>Vertigo</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Prior Otologic Surgery:
								<input type="checkbox" name="priorOtologicSurgeryNo" <%=props.getProperty("priorOtologicSurgeryNo","") %>>No</input>
								<input type="checkbox" name="priorOtologicSurgeryYes" <%=props.getProperty("priorOtologicSurgeryYes","") %>>Yes</input>
								<input class="underline" style="width: 120px;" type="text" name="priorOtologicSurgeryText" value="<%= props.getProperty("priorOtologicSurgeryText", "") %>" />
								
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Family History Early Onset Hearing Loss: 
								<input type="checkbox" name="familyHistoryHlNo"  <%=props.getProperty("familyHistoryHlNo","") %>>No</input>
								<input type="checkbox" name="familyHistoryHYes" <%=props.getProperty("familyHistoryHYes","") %>>Yes</input>						
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								History of Noise Exposure: 
								<input type="checkbox" name="historyOfNoiseNo"   <%=props.getProperty("historyOfNoiseNo","No") %> >No</input>
								<input type="checkbox" name="historyOfNoiseYes"   <%=props.getProperty("historyOfNoiseYes","Yes") %> >Yes</input>
								<input class="underline" style="width: 120px;" type="text" name="historyOfNoiseText" value="<%= props.getProperty("historyOfNoiseText", "") %>" />								
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								History of Ototoxic Medication Use:  
								<input type="checkbox" name="historyOfOtotoxicNo"   <%=props.getProperty("historyOfOtotoxicNo","No") %> >No</input>
								<input type="checkbox" name="historyOfOtotoxicYes"   <%=props.getProperty("historyOfOtotoxicYes","Yes") %> >Yes</input>
								<input class="underline" style="width: 120px;" type="text" name="historyOfOtotoxicText" value="<%= props.getProperty("historyOfOtotoxicText", "") %>" />								
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Uses Hearing Aids:   
								<input type="checkbox" name="usesHearingNo"  <%=props.getProperty("usesHearingNo","No") %> >No</input>
								<input type="checkbox" name="usesHearingYes"  <%=props.getProperty("usesHearingYes","Yes") %> >Yes</input>							
							</td>
						</tr>
						</br>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Recent Treatment: Symptoms improved (+)   No Symptom Improvement (-)
							</td>
						</tr>							
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="recentTreatmentSerc" <%= props.getProperty("recentTreatmentSerc", "") %>/>Serc
								<input type="checkbox" name="recentTreatmentAbx" <%= props.getProperty("recentTreatmentAbx", "") %>/>Abx
								<input class="underline" style="width: 25px;" type="text" name="recentTreatmentAbxText" value="<%= props.getProperty("recentTreatmentAbxText", "") %>" />
								<input type="checkbox" name="recentTreatmentOther" <%= props.getProperty("recentTreatmentOther", "") %>/>
								<input class="underline" style="width: 120px;" type="text" name="recentTreatmentOtherText" value="<%= props.getProperty("recentTreatmentOtherText", "") %>" />
							</td>
						</tr>
						
						<tr class="title">
							<th colspan="5" height="50px">Examination</th>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Ears: AS:
								<input type="checkbox" name="earsAsWNL" <%= props.getProperty("earsAsWNL", "") %>/>WNL
								<input type="checkbox" name="earsAsOME" <%= props.getProperty("earsAsOME", "") %>/>OME
								<input type="checkbox" name="earsAsPerforation" <%= props.getProperty("earsAsPerforation", "") %>/>Perforation
								<input type="checkbox" name="earsAsTIPP" <%= props.getProperty("earsAsTIPP", "") %>/>TIPP
								<input type="checkbox" name="earsAsOther" <%= props.getProperty("earsAsOther", "") %>" />
								<input class="underline" style="width: 120px;" type="text" name="earsAsOtherText" value="<%= props.getProperty("earsAsOtherText", "") %>" />
								
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; AD:
								<input type="checkbox" name="earsAdWNL" <%= props.getProperty("earsAdWNL", "") %>/>WNL
								<input type="checkbox" name="earsAdOME" <%= props.getProperty("earsAdOME", "") %>/>OME
								<input type="checkbox" name="earsAdPerforation" <%= props.getProperty("earsAdPerforation", "") %>/>Perforation
								<input type="checkbox" name="earsAdTIPP" <%= props.getProperty("earsAdTIPP", "") %>/>TIPP
								<input type="checkbox" name="earsAdOther" <%= props.getProperty("earsAdOther", "") %>" />
								<input class="underline" style="width: 120px;" type="text" name="earsAdOtherText" value="<%= props.getProperty("earsAdOtherText", "") %>" />
								
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Facial Nerve Deficit:
								<input type="checkbox" name="facialNerveDeficitNone" <%= props.getProperty("facialNerveDeficitNone","") %>/>None
								<input type="checkbox" name="facialNerveDeficitL" <%= props.getProperty("facialNerveDeficitL","") %>/>LEFT
								<input type="checkbox" name="facialNerveDeficitR" <%= props.getProperty("facialNerveDeficitR","") %>/>RIGHT
								House-Brackmann Grade: <input class="underline" style="width: 120px;" type="text" name="facialNerveDeficitText" value="<%= props.getProperty("facialNerveDeficitText", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Cranial Nerve Screen:
								<input type="checkbox" name="cranialNerveScreenWNL" <%= props.getProperty("cranialNerveScreenWNL","") %>/>WNL
								<input type="checkbox" name="cranialNerveScreenOther" <%= props.getProperty("cranialNerveScreenOther","") %>/>
								<input class="underline" style="width: 120px;" type="text" name="cranialNerveScreenOtherText" value="<%= props.getProperty("cranialNerveScreenOtherText", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Dix-Hallpike: 
								<input type="checkbox" name="dixHallpikeNotDone" <%= props.getProperty("dixHallpikeNotDone","") %>/>Not done
								<input type="checkbox" name="dixHallpikeNoNorV" <%= props.getProperty("dixHallpikeNoNorV","") %>/>No nystagmus or vertigo
								<input type="checkbox" name="dixHallpikeLeftCCW" <%= props.getProperty("dixHallpikeLeftCCW","") %>/>Left CCW Nystagmus
								<input type="checkbox" name="dixHallpikeRightCCW" <%= props.getProperty("dixHallpikeRightCCW","") %>/>Right CCW Nystagmus
								<input type="checkbox" name="dixHallpikeOther" <%= props.getProperty("dixHallpikeOther","") %> />
								<input class="underline" style="width: 120px;" type="text" name="dixHallpikeOtherText" value="<%= props.getProperty("dixHallpikeOtherText", "") %>" />
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Oral:
								<input type="checkbox" name="oralWNL" <%= props.getProperty("oralWNL","") %>/>WNL
								<input type="checkbox" name="oralOther" <%= props.getProperty("oralOther","") %>/>
								<input class="underline" style="width: 120px;" type="text" name="oralOtherText" value="<%= props.getProperty("oralOtherText", "") %>" />
							</td>
						</tr>
						<tr>						
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Neck:
								<input type="checkbox" name="neckNoAdopathy"  <%= props.getProperty("neckNoAdopathy","") %>/>No adenopathy
								<input type="checkbox" name="neckOther" <%= props.getProperty("neckOther","") %> />
								<input class="underline" style="width: 120px;" type="text" name="neckOtherText" value="<%= props.getProperty("neckOtherText", "") %>" />
							</td>
						</tr>
						
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								Nose: Septal Deviation:
								<input type="checkbox" name="noseSeptalDeviationNone" <%= props.getProperty("noseSeptalDeviationNone", "") %> />None
								<input type="checkbox" name="noseSeptalDeviationRight" <%= props.getProperty("noseSeptalDeviationRight", "") %> />Right
								<input type="checkbox" name="noseSeptalDeviationLeft" <%= props.getProperty("noseSeptalDeviationLeft", "") %> />Left
								
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
							<table>
							<tr>
								Inferior Turbinates:
								<td><input type="checkbox" name="inferiorTurbinatesNormal" <%= props.getProperty("inferiorTurbinatesNormal", "") %>/>Normal</td>
								<td><input type="checkbox" name="inferiorTurbinatesHypertrophy" <%= props.getProperty("inferiorTurbinatesHypertrophy", "") %>/>Hypertrophy</td>
								<td><input type="checkbox" name="inferiorTurbinatesPallor" <%= props.getProperty("inferiorTurbinatesPallor", "") %>/>mucosal pallor</td>
								<td><input type="checkbox" name="inferiorTurbinatesOther" <%= props.getProperty("inferiorTurbinatesOther", "") %>/></td>
								<td><input class="underline" style="width: 75px;" type="text" name="inferiorTurbinatesText" value="<%= props.getProperty("inferiorTurbinatesText", "") %>" /></td>
							</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<table>
									<tr>
										<td>Scope: </td>
										<td><input type="checkbox" name="endoscopyPolyps" <%= props.getProperty("endoscopyPolyps", "") %>/>Polyps</td>
										<td>
											Grade&nbsp;&nbsp;<input class="underline" style="width: 40px;" type="text" name="endoscopyGradeText" value="<%= props.getProperty("endoscopyGradeText", "") %>" />/4
										</td>										
									</tr>
									<tr>
										<td></td>
										<td><input type="checkbox" name="endoscopyPus" <%= props.getProperty("endoscopyPus", "") %>/>Pus</td>										
									</tr>
									<tr>
										<td>
											<td><input type="checkbox" name="endoscopyNasopharynx" <%= props.getProperty("endoscopyNasopharynx", "") %>/>Nasopharynx&nbsp;</td>
											<td><input class="underline" style="width: 40px;" type="text" name="endoscopyNasopharynxText" value="<%= props.getProperty("endoscopyNasopharynxText", "") %>" /></td>
										</td>
										
									</tr>	
									<tr>
										<td>Larynx:</td>
										<td><input type="checkbox" name="larynxWNL" <%= props.getProperty("larynxWNL", "") %>/>WNL</td>
										<td>
											<input type="checkbox" name="larynxRVCP" <%= props.getProperty("larynxRVCP", "") %>/>RVCP
										</td>
										<td>
											<input type="checkbox" name="larynxLVCP" <%= props.getProperty("larynxLVCP", "") %>/>LVCP
										</td>
										<td>
											<input type="checkbox" name="larynxOther" <%= props.getProperty("larynxOther", "") %>/>
											<input class="underline" style="width: 150px;" type="text" name="larynxText" value="<%= props.getProperty("larynxText", "") %>" />
										</td>
									</tr>
									</table>
									<table>
									<tr>
										<td>Audio:</td>
										<td>Left:</td>
										<td>
											<input type="checkbox" name="audioLeftNormal" <%= props.getProperty("audioLeftNormal", "") %>/>Normal
										</td>
										<td>
											<input type="checkbox" name="audioLeftSNHL" <%= props.getProperty("audioLeftSNHL", "") %>/>SNHL
										</td>
										<td>
											<input type="checkbox" name="audioLeftCHL" <%= props.getProperty("audioLeftCHL", "") %>/>CHL
										</td>
										<td>
											<input type="checkbox" name="audioLeftMHL" <%= props.getProperty("audioLeftMHL", "") %>/>MHL
										</td>
									</tr>
									<tr>
									<td colspan="2"> </td>
										<td>
											<input type="checkbox" name="audioLeftMild" <%= props.getProperty("audioLeftMild", "") %>/>Mild
										</td>
										<td>
											<input type="checkbox" name="audioLeftModerate" <%= props.getProperty("audioLeftModerate", "") %>/>Moderate
										</td>
										<td>
											<input type="checkbox" name="audioLeftModSev" <%= props.getProperty("audioLeftModSev", "") %>/>Mod-Sev
										</td>
										<td>
											<input type="checkbox" name="audioLeftSevere" <%= props.getProperty("audioLeftSevere", "") %>/>Severe
										</td>
										</tr>
										<tr>
										<td colspan="2"> </td>
										<td>
											<input type="checkbox" name="audioLeftProfound" <%= props.getProperty("audioLeftProfound", "") %>/>Profound
										</td>
									</tr>	
									<tr>
									<td colspan="2"> </td>
									<td>Tymp Type: </td>
									<td>
											<input type="checkbox" name="leftTympTypeA" <%= props.getProperty("leftTympTypeA", "") %>/>A
									</td>
									<td>
											<input type="checkbox" name="leftTympTypeB" <%= props.getProperty("leftTympTypeB", "") %>/>B
									</td>
									<td>
											<input type="checkbox" name="leftTympTypeC" <%= props.getProperty("leftTympTypeC", "") %>/>C
									</td>
									</tr>	
									<tr>
										<td></td>
										<td>Right:</td>
										<td>
											<input type="checkbox" name="audioRightNormal" <%= props.getProperty("audioRightNormal", "") %>/>Normal
										</td>
										<td>
											<input type="checkbox" name="audioRightSNHL" <%= props.getProperty("audioRightSNHL", "") %>/>SNHL
										</td>
										<td>
											<input type="checkbox" name="audioRightCHL" <%= props.getProperty("audioRightCHL", "") %>/>CHL
										</td>
										<td>
											<input type="checkbox" name="audioRightMHL" <%= props.getProperty("audioRightMHL", "") %>/>MHL
										</td>
									</tr>
									<tr>
									<td colspan="2"> </td>
										<td>
											<input type="checkbox" name="audioRightMild" <%= props.getProperty("audioRightMild", "") %>/>Mild
										</td>
										<td>
											<input type="checkbox" name="audioRightModerate" <%= props.getProperty("audioRightModerate", "") %>/>Moderate
										</td>
										<td>
											<input type="checkbox" name="audioRightModSev" <%= props.getProperty("audioRightModSev", "") %>/>Mod-Sev
										</td>
										<td>
											<input type="checkbox" name="audioRightSevere" <%= props.getProperty("audioRightSevere", "") %>/>Severe
										</td>
										</tr>
										<tr>
										<td colspan="2"> </td>
										<td>
											<input type="checkbox" name="audioRightProfound" <%= props.getProperty("audioRightProfound", "") %>/>Profound
										</td>
									</tr>	
									<tr>
									<td colspan="2"> </td>
									<td>Tymp Type: </td>
									<td>
											<input type="checkbox" name="rightTympTypeA" <%= props.getProperty("rightTympTypeA", "") %>/>A
									</td>
									<td>
											<input type="checkbox" name="rightTympTypeB" <%= props.getProperty("rightTympTypeB", "") %>/>B
									</td>
									<td>
											<input type="checkbox" name="rightTympTypeC" <%= props.getProperty("rightTympTypeC", "") %>/>C
									</td>
									</tr>							
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<table>
									<tr>
										<td>ABR:</td>
										<td><input type="checkbox" name="abrWNL" <%= props.getProperty("abrWNL", "") %>/>WNL</td>
										<td><input type="checkbox" name="abrNA" <%= props.getProperty("abrNA", "") %>/>N/A</td>
										<td><input type="checkbox" name="abrNYD" <%= props.getProperty("abrNYD", "") %>/>NYD</td>
										<td><input type="checkbox" name="abrOther" <%= props.getProperty("abrOther", "") %>/></td>
										<td><input class="underline" style="width: 150px;" type="text" name="abrOtherText" value="<%= props.getProperty("abrOtherText", "") %>" /></td>
										
									</tr>
									<tr>
										<td>MRI:</td>
										<td><input type="checkbox" name="mriWNL" <%= props.getProperty("mriWNL", "") %>/>WNL</td>
										<td><input type="checkbox" name="mriNA" <%= props.getProperty("mriNA", "") %>/>N/A</td>
										<td><input type="checkbox" name="mriNYD" <%= props.getProperty("mriNYD", "") %>/>NYD</td>
										<td><input type="checkbox" name="mriOther" <%= props.getProperty("mriOther", "") %>/></td>
										<td><input class="underline" style="width: 150px;" type="text" name="mriOtherText" value="<%= props.getProperty("mriOtherText", "") %>" /></td>
										
									</tr>
									
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<table>
									<tr>
										<td><span style="font-weight: bold;">Impression:</span></td>
										<td>
											<input type="checkbox" name="impressionPresbycusis" <%= props.getProperty("impressionPresbycusis", "") %> />Presbycusis 
											<input type="checkbox" name="impressionOtosclerosis" <%= props.getProperty("impressionOtosclerosis", "") %> />Otosclerosis 
											<input type="checkbox" name="impressionCEOE" <%= props.getProperty("impressionCEOE", "") %> />CEOE 
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impressionOtitisExterna" <%= props.getProperty("impressionOtitisExterna", "") %>/>Otitis Externa
											<input type="checkbox" name="impressionOME" <%= props.getProperty("impressionOME", "") %> />OME
											<input type="checkbox" name="impressionCOM" <%= props.getProperty("impressionCOM", "") %> />COM
											
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<input type="checkbox" name="impressionOther" <%= props.getProperty("impressionOther", "") %> />
											<input class="underline" style="width: 300px;" type="text" name="impressionOtherText" value="<%= props.getProperty("impressionOtherText", "") %>" /> 
										</td>
									</tr>
								</table>						
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<span style="font-weight: bold;">Plan:</span>
							</td>
						</tr>
						
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="hearing" <%= props.getProperty("hearing", "") %>/>Hearing Aid Evaluation								
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="myringotomy" <%= props.getProperty("myringotomy", "") %>/>Myringotomy and Tube:
								<input type="checkbox" name="myringotomyLeft" <%= props.getProperty("myringotomyLeft", "") %>/>Left
								<input type="checkbox" name="myringotomyRight" <%= props.getProperty("myringotomyRight", "") %>/>Right
								<input type="checkbox" name="myringotomyBilateral" <%= props.getProperty("myringotomyBilateral", "") %>/>Bilateral
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="abrPlan" <%= props.getProperty("abrPlan", "") %>/>ABR								
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="mriPlan" <%= props.getProperty("mriPlan", "") %>/>MRI								
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="ctTemporal" <%= props.getProperty("ctTemporal", "") %>/>CT Temporal Bones and follow-up after the scan								
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="repeatAduio" <%= props.getProperty("repeatAduio", "") %>/>Repeat Audio in  
								<input class="underline" style="width: 50px;" type="text" name="repeatAduioText" value="<%= props.getProperty("repeatAduioText", "") %>" />								
								months
								<input type="checkbox" name="requiringRepeat" <%= props.getProperty("requiringRepeat", "") %>/>requiring repeat consultation request
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="surgery" <%= props.getProperty("surgery", "") %>/>Surgery	
								<input class="underline" style="width: 150px;" type="text" name="surgeryText" value="<%= props.getProperty("surgeryText", "") %>" />								
							</td>
							
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="consult" <%= props.getProperty("consult", "") %>/>Consult								
							</td>
							
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="ciprodex" <%= props.getProperty("ciprodex", "") %>/>Ciprodex otic drops: 3 drops in affected ear(s) BID for  
								<input class="underline" style="width: 50px;" type="text" name="ciprodexText" value="<%= props.getProperty("ciprodexText", "") %>" />								
								days 
							</td>
						</tr>
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="left" colspan="7">
								<input type="checkbox" name="engVertigoEvaluation" <%= props.getProperty("engVertigoEvaluation", "") %>/>ENG for vertigo evaluation								
							</td>
							
						</tr>
						
						
						
						
					
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" colspan="7">
								<input type="checkbox" name="planOther" <%= props.getProperty("planOther", "") %>>
								<textarea name="planOtherText" style="border: 1px solid #000;width: 95%"><%= props.getProperty("planOtherText", "") %></textarea>
							</td>
						</tr>
						
						<tr>
							<td valign="top" align="right">
							</td>
							<td valign="top" align="right" colspan="7">
								<br/>
								<input class="underline" style="width: 300px;" type="text" name="signatures" value="<%= props.getProperty("signatures", "") %>" />
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>

		<tr>
			<td valign="top">
			<table class="Head" class="hidePrint" height="5%">
				<tr>
					<td align="left">
					<%
  if (!bView) {
%> 
					<input type="submit" value="Save"
						onclick="javascript:if (checkBeforeSave()==true) return justSave(); else return false;" />
						
					<input type="submit" value="Save & Letter"
						onclick="javascript:if (checkBeforeSave()==true) return onSubmitAndOpenLetter(); else return false;" />
					<%
  }
%> <input type="button" value="Exit"
						onclick="javascript:return onExit();" /> <input type="button"
						value="Print" onclick="javascript:window.print();" /></td>
					<td align="right">Study ID: <%= props.getProperty("studyID", "N/A") %>
					<input type="hidden" name="studyID"
						value="<%= props.getProperty("studyID", "N/A") %>" /></td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</html:form>
<script type="text/javascript">
function onSubmitAndOpenLetter() {
    document.forms[0].submit.value="formOtologicLetter";                
    
    var ret = is1CheckboxChecked(0, choiceFormat) && allAreNumeric(0, allNumericField) && areInRange(0, allMatch);                       

    if (ret==true) {                        
        ret = confirm("Are you sure you want to save this form?");            
    }                
    return ret;
}
function justSave() {
    document.forms[0].submit.value="exit";   
    var ret = is1CheckboxChecked(0, choiceFormat) && allAreNumeric(0, allNumericField) && areInRange(0, allMatch);                       
    if (ret==true) {                        
        ret = confirm("Are you sure you want to save this form?");            
    }                
    return ret;     
}
</script>
</body>
</html:html>