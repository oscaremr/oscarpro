<%@ page import="oscar.form.FormOnPerinatalUtils" %>
<%
    int svNum = request.getParameter("svNum") != null ? Integer.parseInt(request.getParameter("svNum")) : 0;
    java.util.Properties props = (java.util.Properties) request.getAttribute("props");
%>

<tr align="center" id="sv_<%=svNum%>" style="width:100%; position:relative;">
    <td style="width:2%;">
        <a class="delete_link form-link" href="javascript:void(0)" onclick="deleteSubsequentVisit('<%=svNum%>'); return false;">[x]</a>&nbsp;<%=svNum%>
    </td>
    <td style="width:10%;">
        <input class="spe" style="width:100%;" type="text" name="sv_date<%=svNum%>" ondblclick="calToday(this)" maxlength="10" placeholder="YYYY/MM/DD" 
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "sv_date" + svNum) %>" />
    </td>
    <td style="width:8%;">
        <input class="spe" style="width:100%;" type="text" name="sv_ga<%=svNum%>" ondblclick="getGestationalAge(this)" maxlength="6" 
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "sv_ga" + svNum) %>" />
    </td>
    <td style="width:5%;">
        <input class="spe" style="width:100%;" type="text" name="sv_wt<%=svNum%>" ondblclick="weightImperialToMetric(this)" maxlength="5" 
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "sv_wt" + svNum) %>" />
    </td>
    <td style="width:8%;">
        <input style="width:100%;" type="text" name="sv_bp<%=svNum%>" maxlength="7" 
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "sv_bp" + svNum) %>" />
    </td>
    <td style="width:5%;">
        <input style="width:100%;" type="text" name="sv_urine<%=svNum%>" maxlength="5"
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "sv_urine" + svNum) %>"  />
    </td>
    <td style="width:5%;">
        <input style="width:100%;" type="text" name="sv_sfh<%=svNum%>" maxlength="5"
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "sv_sfh" + svNum) %>"  />
    </td>
    <td style="width:5%;">
        <input style="width:100%;" type="text" name="sv_pres<%=svNum%>" maxlength="5" 
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "sv_pres" + svNum) %>" />
    </td>
    <td style="width:5%;">
        <input style="width:100%;" type="text" name="sv_fhr<%=svNum%>" maxlength="5"
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "sv_fhr" + svNum) %>"  />
    </td>
    <td style="width:5%;">
        <input style="width:100%;" type="text" name="sv_fm<%=svNum%>" maxlength="5" 
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "sv_fm" + svNum) %>" />
    </td>
    <td style="width:30%;">
        <input style="width:100%;" type="text" name="sv_comments<%=svNum%>" maxlength="44" 
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "sv_comments" + svNum) %>" />
    </td>
    <td style="width:10%;">
        <input style="width:100%;" type="text" name="sv_next<%=svNum%>" maxlength="6" 
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "sv_next" + svNum) %>" />
    </td>
    <td style="width:2%;">
        <input style="width:100%;" type="text" name="sv_initial<%=svNum%>" maxlength="5" 
            value="<%= FormOnPerinatalUtils.getFormAttribute(props, "sv_initial" + svNum) %>" />
    </td>
</tr>
