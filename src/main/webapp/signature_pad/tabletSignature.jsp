<%--

    Copyright (c) 2008-2012 Indivica Inc.

    This software is made available under the terms of the
    GNU General Public License, Version 2, 1991 (GPLv2).
    License details are available via "indivica.ca/gplv2"
    and "gnu.org/licenses/gpl-2.0.html".

--%>
<%--
When including the "inWindow" parameter as "true" it is assumed that tabletSignature.jsp 
is hosted in an IFrame and that the IFrame's parent window implements signatureHandler(e)
--%>
<%@ page import="org.oscarehr.util.DigitalSignatureUtils" %>
<%@ page import="org.oscarehr.util.MiscUtils" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.ui.servlet.ImageRenderingServlet"%>
<%@ page import="org.apache.commons.lang3.math.NumberUtils" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="org.oscarehr.common.model.DigitalSignature" %>
<%@ page import="org.oscarehr.common.dao.DigitalSignatureDao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Arrays" %>

<!DOCTYPE html> 
<html lang="en"> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Signature Pad</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" />
<link rel="stylesheet" href="<%= request.getContextPath() %>/share/css/TabletSignature.css" media="screen"/>
<link href="<%=request.getContextPath() %>/css/font-awesome.min.css" rel="stylesheet"/>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/TabletSignature.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/jquery/jquery-1.4.2.js"></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/jquery/jquery.form.js"></script>

</head>
<%
final boolean FALSE_TO_FILTER_BY_SIGNATURE_FAVORITE = false;
LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
String requestIdKey = request.getParameter(DigitalSignatureUtils.SIGNATURE_REQUEST_ID_KEY);
String providerNumber = loggedInInfo.getLoggedInProviderNo();
if (requestIdKey == null) {
	requestIdKey = DigitalSignatureUtils.generateSignatureRequestId(providerNumber);
}
String imageUrl=request.getContextPath()+"/imageRenderingServlet?source="+ImageRenderingServlet.Source.signature_preview.name()+"&"+DigitalSignatureUtils.SIGNATURE_REQUEST_ID_KEY+"="+requestIdKey;
String storedImageUrl=request.getContextPath()+"/imageRenderingServlet?source="+ImageRenderingServlet.Source.signature_stored.name()+"&digitalSignatureId=";
boolean saveToDB = "true".equals(request.getParameter("saveToDB"));
String scriptNo = request.getParameter("scriptNo");
DigitalSignatureDao digitalSignatureDao = (DigitalSignatureDao) SpringUtils.getBean("digitalSignatureDao");
List<DigitalSignature> favouriteSignatures = digitalSignatureDao.getProviderSignatures(providerNumber, FALSE_TO_FILTER_BY_SIGNATURE_FAVORITE);
%>
<script type="text/javascript">
var _in_window = <%= "true".equals(request.getParameter("inWindow")) %>;

var enableEformDigitalSig = <%= "true".equals(request.getParameter("enableEformDigitalSig")) %>;
var provNum = <%= request.getParameter("provNum") %>;

var requestIdKey = "<%= requestIdKey %>";

var previewImageUrl = "<%= imageUrl %>";

var storedImageUrl = "<%= storedImageUrl %>";

var contextPath = "<%=request.getContextPath() %>";

var favouriteSignatures = new Map();

var isManageSignatures = <%= "true".equals(request.getParameter("isManageSignatures")) %>;

// Add java favourite signatures to javascript map
<% for (DigitalSignature favouriteSignature : favouriteSignatures) { %>
favouriteSignatures.set("<%=favouriteSignature.getId()%>", "<%=Arrays.toString(
			favouriteSignature.getSignatureImage())%>");
<% } %>

</script>
<% if (OscarProperties.getInstance().getBooleanProperty("topaz_enabled", "true")) { %>
<script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/SigWebTablet.js"></script>	
<% } %>
<body style="background-color: #555">

<div class="verticalCenterDiv">
	<div class="centerDiv">
		<canvas id='canvas'></canvas>
		<div id="canvasMenu">
			<div>
				<span id="signMessage" style="color:#FFFFFF;">Please sign in the box above this message.</span>
				<button id="clear" style="display: none">Clear</button>
				<button id="save" style="display: none">Save</button>
				<button id="displaySaveSignatureMenu" style="display: none">Add to Favourites</button>
				<select id="favouriteSignatures" style="display: inline; width: 185px">
					<option value="">Select a favourite signature</option>
					<% for (DigitalSignature favouriteSignature : favouriteSignatures) { %>
					<option value="<%=favouriteSignature.getId()%>"><%=favouriteSignature.getLabel()%></option>
					<% } %>
				</select>
				<div id="saveSignatureMenu" style="display: none">
					<input type="text" id="saveFavouriteSignatureLabel" maxlength="15" placeholder="Enter favourite label here...">
					<button id="saveFavouriteSignature">Save</button>
					<button id="cancelFavouriteSignature">Cancel</button>
				</div>
				<button id="sign" style="display:none;">Sign</button>
				<button id="signAndSave" style="display:none;">Sign and set as default</button>
			</div>
			<button id="manageSignaturesCog" onclick="manageSignatures()"><i class="icon-cog"></i></button>
		</div>
	</div>
</div>

<form onsubmit="return submitSignature();" action="<%=request.getContextPath() %>/signature_pad/uploadSignature.jsp" id="signatureForm" method="POST">
	<input type="hidden" id="signatureImage" name="signatureImage" value="" />
	<input type="hidden" name="source" value="IPAD" />
	<input type="hidden" name="<%=DigitalSignatureUtils.SIGNATURE_REQUEST_ID_KEY %>" value="<%= requestIdKey %>" />
	<input type="hidden" name="demographicNo" value="<%= request.getParameter("demographicNo") %>" />
	<input type="hidden" id="saveToDb" name="saveToDB" value="<%=saveToDB%>" />
	<input type="hidden" id="signatureLabel" name="signatureLabel" value="" />
	<% if (scriptNo != null) { %>
	<input type="hidden" name="scriptNo" value="<%=scriptNo%>" />
	<% } %>
</form>

</body>
</html>
