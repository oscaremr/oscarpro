<%--
/*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
--%>
 
<%@page import="org.oscarehr.util.DigitalSignatureUtils"%>
<%@page import="org.oscarehr.util.DigitalSignatureUtils"%><%@page import="org.oscarehr.util.MiscUtils"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="org.apache.commons.codec.binary.Base64" %>
<%@page import="oscar.dms.*" %>
<%@page import="java.io.*" %>

<%
	String signatureId = DigitalSignatureUtils.generateSignatureRequestId(request.getParameter("providerNo"));
    boolean uploadOk = DigitalSignatureUtils.uploadThumbnail(signatureId, request.getParameter("demographicNo"), request.getParameter("thumbnailImage"));
	if (uploadOk) {
	  response.setStatus(HttpServletResponse.SC_OK);
	} else {
	  response.sendError(
          HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
          "Failed to upload thumbnail image.");
	}
	
%>
<input type="hidden" name="signatureId" value="<%=signatureId%>" />
