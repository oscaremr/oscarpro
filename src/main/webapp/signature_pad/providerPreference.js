/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

const FILTER_ARCHIVED = "archived";
const FILTER_FAVOURITES = "favourites";

const SIGNATURE_LABEL_INPUT_CLASS = "signature-label-input";
const SIGNATURE_CONTAINER_CLASS = "signature-container";
const SIGNATURE_LABEL_CLASS = "signature-label";

const FALSE_TO_FILTER_BY_SIGNATURE_FAVORITE = false;
const TRUE_TO_FILTER_BY_SIGNATURE_ARCHIVED = true;

const LABEL_EDIT_CLASS = "label-edit";
const LABEL_EDIT_ERROR_CLASS = "label-edit-error";
const MISSING_LABEL_ERROR_CLASS = "missing-label-error";

const SELECTED_FILTER_BUTTON_CLASS = "filter-selected";

const ARCHIVED_CLASS = "archived";

const MANAGE_SIGNATURES_PATH = CONTEXT_PATH + "/ManageSignature.do";

const filterArchivedButton = document.getElementById("filterArchivedButton");
const filterFavouritesButton = document.getElementById(
    "filterFavouritesButton");
const signaturesContainer = document.getElementById("signaturesContainer");
const addSignatureButtonIcon = document.getElementById(
    "addSignatureButtonIcon");
const addSignatureContainer = document.getElementById("addSignatureContainer");
const signatureFrame = document.getElementById("signatureFrame");

const signatureMap = new Map();

// The default filter when the page loads
let currentFilter = FILTER_FAVOURITES;

populateSignaturesMaps();

async function populateSignaturesMaps() {
  let favouriteSignatures = await getSignaturesFromDatabase(
      "getProviderSignatures", FALSE_TO_FILTER_BY_SIGNATURE_FAVORITE);
  favouriteSignatures.map(signature => {
    return {
      ...signature,
      archived: FALSE_TO_FILTER_BY_SIGNATURE_FAVORITE
    };
  }).forEach(signature => {
    signatureMap.set(signature.id, signature);
  });
  let archivedSignatures = await getSignaturesFromDatabase(
      "getProviderSignatures", TRUE_TO_FILTER_BY_SIGNATURE_ARCHIVED);
  archivedSignatures.map(signature => {
    return {
      ...signature,
      archived: TRUE_TO_FILTER_BY_SIGNATURE_ARCHIVED
    };
  }).forEach(signature => {
    signatureMap.set(signature.id, signature);
  });
  updateSignaturesContainer(currentFilter);
}

function getSignaturesFromDatabase(method, archived) {
  return $.ajax({
    method: 'GET',
    url: MANAGE_SIGNATURES_PATH,
    data: `method=${method}&archived=${archived}`,
    dataType: 'json',
    success: function (data) {
      return data
    },
    error: function (error) {
      console.log(error);
      return [];
    }
  });
}

function populateSignaturesContainer() {
  for (const [key, signature] of signatureMap.entries()) {
    if (currentFilter === FILTER_FAVOURITES && signature.archived) {
      continue;
    }
    if (currentFilter === FILTER_ARCHIVED && !signature.archived) {
      continue;
    }

    // container for signature and label edit form
    const signatureContainer = document.createElement("div");
    signatureContainer.setAttribute("id", key);
    signatureContainer.setAttribute("class", "signature-container");

    // signature row components
    const signatureElement = document.createElement("div");
    signatureElement.setAttribute("class", "signature");

    // signature image
    createSignatureImage(signatureElement, signature.signatureImage)

    // signature favourite and archive buttons
    createSignatureButtons(signatureElement, signature.archived);

    // signature label and date signed
    createSignatureInfo(signatureElement, signature.label,
        new Date(signature.dateSigned).toLocaleString())

    // display label edit form when signature is clicked
    signatureElement.addEventListener("click", displaySignatureEdit);
    signatureContainer.appendChild(signatureElement);

    // label edit row
    const labelEdit = document.createElement("div");
    labelEdit.setAttribute("class", `${LABEL_EDIT_CLASS}`);
    labelEdit.style.display = "none";

    // create label edit row
    createEditLabelForm(labelEdit, signature);
    signatureContainer.appendChild(labelEdit);
    signaturesContainer.appendChild(signatureContainer);
  }
}

function createEditLabelForm(element, signature) {
  const labelEditRow = document.createElement("div");
  labelEditRow.setAttribute("class", "input-group");
  // create input for new signature label
  const signatureLabelInput = document.createElement("input");
  signatureLabelInput.setAttribute("type", "text");
  signatureLabelInput.setAttribute("class",
      `form-control ${SIGNATURE_LABEL_INPUT_CLASS}`);
  signatureLabelInput.setAttribute("placeholder", "New Label");
  labelEditRow.appendChild(signatureLabelInput);
  createEditLabelFormButtons(labelEditRow);
  element.appendChild(labelEditRow);
}

function createEditLabelFormButtons(element) {
  const inputGroupAppend = document.createElement("div");
  inputGroupAppend.setAttribute("class", "input-group-append");
  // create button for saving signature label
  const saveSignatureLabelButton = document.createElement("button");
  saveSignatureLabelButton.setAttribute("class", "btn btn-xs btn-default");
  saveSignatureLabelButton.setAttribute("type", "button");
  saveSignatureLabelButton.innerHTML = "Save";
  saveSignatureLabelButton.addEventListener("click", saveLabel);
  inputGroupAppend.appendChild(saveSignatureLabelButton);
  // create button to cancel editing signature label
  const cancelSignatureLabelButton = document.createElement("button");
  cancelSignatureLabelButton.setAttribute("class", "btn btn-xs btn-default");
  cancelSignatureLabelButton.setAttribute("type", "button");
  cancelSignatureLabelButton.innerHTML = "Cancel";
  cancelSignatureLabelButton.addEventListener("click", hideEditSignatureForm);
  inputGroupAppend.appendChild(cancelSignatureLabelButton);
  element.appendChild(inputGroupAppend);
}

async function saveLabel(e) {
  const signatureContainer = e.target.closest(`.${SIGNATURE_CONTAINER_CLASS}`);
  if (!validateLabel(signatureContainer)) {
    return;
  }
  // get id from signature-container parent id
  const id = signatureContainer.id;
  // get signature from signatures map
  const signature = signatureMap.get(parseInt(id));
  // get label value
  const label = signatureContainer.querySelector(
      `.${SIGNATURE_LABEL_INPUT_CLASS}`).value.trim();
  const updatedSignature = await updateSignatureInDatabase(id, label,
      signature.archived);
  if (updatedSignature) {
    updatedSignature.archived = signature.archived;
    updateSignature(e, updatedSignature);
    hideEditSignatureForm(e);
  }
}

function validateLabel(signatureContainer) {
  const labelEdit = signatureContainer.querySelector(`.${LABEL_EDIT_CLASS}`);
  // if label is missing, display error and return false
  if (!signatureContainer.querySelector(
      `.${SIGNATURE_LABEL_INPUT_CLASS}`).value.trim()) {
    createMissingLabelError(labelEdit);
    return false;
  }
  return true;
}

function updateSignature(e, newSignature) {
  updateSignatureInMap(newSignature);
  updateSignaturesContainer(currentFilter);
}

function updateSignatureInMap(newSignature) {
  signatureMap.set(newSignature.id, newSignature);
}

function updateSignatureInDatabase(id, label, isArchived) {
  // encode label to be sent in url
  const encodedLabel = label ? encodeURIComponent(label) : null;
  const data = `method=updateSignature&id=${id}`
      + `${encodedLabel ? `&label=${encodedLabel}` : ''}`
      + `${isArchived ? `&isArchived=${isArchived}` : ''}`;
  return $.ajax({
    method: 'POST',
    url: MANAGE_SIGNATURES_PATH,
    data: data,
    dataType: 'json',
    success: function (data) {
      return data;
    },
    error: function () {
      console.log("error");
      return null;
    }
  });
}

function hideEditSignatureForm(e) {
  const labelEdit = e.target.closest(
      `.${SIGNATURE_CONTAINER_CLASS}`).querySelector(`.${LABEL_EDIT_CLASS}`);
  labelEdit.style.display = "none";
  removeAllErrorMessages(labelEdit);
}

function removeAllErrorMessages(labelEdit) {
  const errorElements = labelEdit.querySelectorAll(
      `.${LABEL_EDIT_ERROR_CLASS}`);
  errorElements.forEach(errorElement => errorElement.remove());
}

function displaySignatureEdit(e) {
  const labelEdit = e.target.closest(
      `.${SIGNATURE_CONTAINER_CLASS}`).querySelector(`.${LABEL_EDIT_CLASS}`);
  labelEdit.style.display = "block";
}

function createSignatureImage(element, base64String) {
  const signatureImageElement = document.createElement("img");
  signatureImageElement.setAttribute("class", "signature-image");
  signatureImageElement.setAttribute("src",
      "data:image/png;base64," + base64String);
  element.appendChild(signatureImageElement);
}

function createSignatureButtons(element, isArchived) {
  const signatureButtons = document.createElement("div");
  // prevent click propagation to signature container
  signatureButtons.addEventListener("click", (e) => {
    e.stopPropagation();
  })
  signatureButtons.setAttribute("class", "signature-buttons");
  // archive button
  const archiveButton = document.createElement("i");
  archiveButton.setAttribute("class", "icon-2x icon-trash");
  archiveButton.addEventListener("click", toggleArchived);
  if (isArchived) {
    archiveButton.classList.add(ARCHIVED_CLASS);
  }
  signatureButtons.appendChild(archiveButton);
  element.appendChild(signatureButtons);
}

async function toggleArchived(e) {
  const id = e.target.closest(`.${SIGNATURE_CONTAINER_CLASS}`).id;
  const signature = signatureMap.get(parseInt(id));
  const updatedSignature = await updateSignatureInDatabase(id, signature.label, !signature.archived);
  if (updatedSignature) {
    updatedSignature.archived = !signature.archived;
    updateSignature(e, updatedSignature);
  }
}

function createMissingLabelError(labelEdit) {
  // return if message already exists
  if (labelEdit.querySelector(`.${MISSING_LABEL_ERROR_CLASS}`)) {
    return;
  }
  const errorElement = document.createElement("p");
  errorElement.setAttribute("class",
      `${LABEL_EDIT_ERROR_CLASS} ${MISSING_LABEL_ERROR_CLASS}`);
  errorElement.innerHTML = "Please add a label to signature.";
  labelEdit.appendChild(errorElement);
}

function createSignatureInfo(element, label, dateCreated) {
  const signatureInfoElement = document.createElement("div");
  signatureInfoElement.setAttribute("class", "signature-info");
  // label p
  const signatureLabelElement = document.createElement("p");
  signatureLabelElement.setAttribute("class",
      `${SIGNATURE_LABEL_CLASS} font-weight-bold`);
  signatureLabelElement.innerText = label ? label : "";
  signatureInfoElement.appendChild(signatureLabelElement);
  // date created p
  const signatureDateCreatedElement = document.createElement("p");
  signatureDateCreatedElement.setAttribute("class", "signature-date-created");
  signatureDateCreatedElement.innerText = dateCreated;
  signatureInfoElement.appendChild(signatureDateCreatedElement);
  element.appendChild(signatureInfoElement);
}

function setFilterButtonStyle() {
  filterArchivedButton.classList.remove(SELECTED_FILTER_BUTTON_CLASS);
  filterFavouritesButton.classList.remove(SELECTED_FILTER_BUTTON_CLASS);
  switch (currentFilter) {
    case FILTER_ARCHIVED:
      filterArchivedButton.classList.add(SELECTED_FILTER_BUTTON_CLASS);
      break;
    case FILTER_FAVOURITES:
      filterFavouritesButton.classList.add(SELECTED_FILTER_BUTTON_CLASS);
      break;
    default:
      break;
  }
}

function clearSignaturesContainer() {
  signaturesContainer.innerHTML = "";
}

function updateSignaturesContainer(filter) {
  currentFilter = filter ? filter : currentFilter;
  setFilterButtonStyle();
  clearSignaturesContainer();
  populateSignaturesContainer();
}

function signatureHandler(e) {
  // create a new map to show new signatures at the top
  signatureMap.clear();
  populateSignaturesMaps();
}

function toggleAddSignatureDisplay() {
  // set iframe src if not set
  if (!signatureFrame.getAttribute("src")) {
    setSignatureFrameIFrameSrc();
  }
  if (addSignatureContainer.style.display === "none") {
    addSignatureContainer.style.display = "block";
    addSignatureButtonIcon.classList.replace("icon-plus", "icon-minus");
    return;
  }
  addSignatureContainer.style.display = "none";
  addSignatureButtonIcon.classList.replace("icon-minus", "icon-plus");
}

function setSignatureFrameIFrameSrc() {
  signatureFrame.setAttribute("src",
      CONTEXT_PATH + "/signature_pad/tabletSignature.jsp?inWindow=true&isManageSignatures=true&"
      + SIGNATURE_REQUEST_ID_KEY + "=" + SIGNATURE_REQUEST_ID);
}