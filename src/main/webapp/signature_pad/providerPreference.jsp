<%--Copyright (c) 2023 WELL EMR Group Inc.--%>
<%--This software is made available under the terms of the--%>
<%--GNU General Public License, Version 2, 1991 (GPLv2).--%>
<%--License details are available via "gnu.org/licenses/gpl-2.0.html".--%>

<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.common.dao.DigitalSignatureDao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.DigitalSignature" %>
<%@ page import="java.util.List" %>
<%@ page import="org.oscarehr.util.DigitalSignatureUtils" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="org.apache.commons.codec.binary.Base64" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<html>
<head>
    <title><bean:message key="provider.btnManageSignatures"/></title>
    <script type="text/javascript"
            src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
    <link href="<%=request.getContextPath() %>/css/bootstrap4.1.1.min.css" rel="stylesheet"/>
    <link href="<%=request.getContextPath() %>/css/panel.css" rel="stylesheet"/>
    <link href="<%=request.getContextPath() %>/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="./providerPreference.css" rel="stylesheet"/>
</head>
<%
    if (session.getAttribute("user") == null) {
        response.sendRedirect("../../logout.jsp");
    }

    String providerNumber = LoggedInInfo.getLoggedInInfoFromSession(request)
            .getLoggedInProviderNo();
    String signatureRequestId =
            DigitalSignatureUtils.generateSignatureRequestId(providerNumber);
%>
<script>
  const CONTEXT_PATH = "<%=request.getContextPath()%>";
  const SIGNATURE_REQUEST_ID_KEY = "<%=DigitalSignatureUtils.SIGNATURE_REQUEST_ID_KEY%>";
  const SIGNATURE_REQUEST_ID = "<%=signatureRequestId%>";
</script>
<body>
<div class="container-fluid">
    <div class='panel panel-primary'>
        <div class='panel-heading d-flex align-items-center justify-content-between'>
            <bean:message key="provider.btnManageSignatures"/>
            <button class="btn btn-xs btn-default" type="button"
                    onclick="window.close(); return false;"><bean:message
                    key="global.btnClose"/></button>
        </div>
        <div class='panel-body'>
            <div id="menuContainer">
                <button onclick="toggleAddSignatureDisplay()" class="btn btn-xs btn-default add-signature-button">
                    <span><i id="addSignatureButtonIcon" class="icon-plus"></i> Add Signature</span>
                </button>
                <button onclick="updateSignaturesContainer(FILTER_ARCHIVED)" id="filterArchivedButton"
                        class="btn btn-xs btn-default filter-button">Archived
                </button>
                <button onclick="updateSignaturesContainer(FILTER_FAVOURITES)" id="filterFavouritesButton"
                        class="btn btn-xs btn-default filter-button">Favourites
                </button>
            </div>
            <div id="addSignatureContainer" style="display: none">
                <iframe style="width:500px; height:132px;" id="signatureFrame"></iframe>
            </div>
            <div id="signaturesContainer"></div>
        </div>
    </div>
</div>
<!-- import jquery -->
<script type='text/javascript'
        src='<%=request.getContextPath() %>/js/jquery-3.1.0.min.js'></script>
<script type='text/javascript' src='providerPreference.js'></script>
</body>
</html>
