<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@page import="oscar.log.*" errorPage="errorpage.jsp"%>
<%@ page import="org.oscarehr.util.SessionConstants" %>
<%@ page import="org.springframework.web.util.WebUtils" %>
<%@ page import="oscar.OscarProperties" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.managers.OktaManager" %>
<%@ page import="org.oscarehr.integration.OneIdSession" %>
<%@ page import="org.oscarehr.integration.OneIdSessionDao" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<%
  OktaManager oktaManager = SpringUtils.getBean(OktaManager.class);
  String param = "";
	if(request.getParameter("login")!=null ) {
		param = "?login="+request.getParameter("login") ;
		if (request.getParameter("nameId") != null) {
			param = param + "&nameId=" + request.getParameter("nameId");
		}
	}
	else if (request.getParameter("errorMessage") != null) {
		param = "?errorMessage=" + request.getParameter("errorMessage");
	}
	String redirect = "index.jsp" + param;
  if(oscar.oscarSecurity.CRHelper.isCRFrameworkEnabled()) net.sf.cookierevolver.CRFactory.getManager().recordLogout(request);
  if(session != null) {
    Object user = session.getAttribute("user");
    if (user != null) {
        OneIdSessionDao oneIdSessionDao = SpringUtils.getBean(OneIdSessionDao.class);
        if (oneIdSessionDao.find(user) != null) {
            response.sendRedirect("/" + OscarProperties.getKaiemrDeployedContext() + "/#/one-id/login?logout=true&redirect=true");
            return;
        }
        //HashMap hash=(HashMap)application.getAttribute("monitor");
        Boolean enhancedEnabled = "E".equals(session.getAttribute(SessionConstants.LOGIN_TYPE));
        session.invalidate();
        Cookie ssoCookie = WebUtils.getCookie(request, "kai-sso");
        if (ssoCookie != null) {
            ssoCookie.setMaxAge(0);
            ssoCookie.setPath("/");
            response.addCookie(ssoCookie);
        }

        boolean kaiemrEnabled = OscarProperties.isActive("enable_kai_emr");
        boolean oktaAuthEnabled = oktaManager.isOktaEnabled();
        boolean allowOnlineBookingSystemPreferenceEnabled = SystemPreferencesUtils
                .isReadBooleanPreferenceWithDefault("allow_online_booking", true);


        if (kaiemrEnabled && oktaAuthEnabled) {
          String logoutRedirect = OscarProperties.getOscarProBaseUrl() + "/api/logout";
          String oktaOrgUrl = SystemPreferencesUtils.getPreferenceValueByName("well_okta_organization_base_url" , "https://wellhealth.oktapreview.com") ;
          redirect = oktaOrgUrl + "/login/signout?fromURI=" + logoutRedirect;

        } else if (kaiemrEnabled ||
                allowOnlineBookingSystemPreferenceEnabled ||
                OscarProperties.getInstance().isPropertyActive("kaiemr_lab_queue_url")) {
            // redirect to kaiemr logout with redirect parameter in matrix url notation
            redirect = "/" + OscarProperties.getKaiemrDeployedContext() + "/#/user/logout";
		    }

        String ip = LoggedInInfo.obtainClientIpAddress(request);
        String logMessage = "";
        if ("true".equalsIgnoreCase(request.getParameter("autoLogout"))) {
            logMessage = "test";
        }
        LogAction.addLog((String)user, LogConst.LOGOUT, LogConst.CON_LOGIN, logMessage, ip);
    }
  }
  
  response.sendRedirect(redirect);
%>
