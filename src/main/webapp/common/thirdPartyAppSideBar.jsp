<%--
    Copyright (c) 2021 WELL EMR Group Inc.
    This software is made available under the terms of the
    GNU General Public License, Version 2, 1991 (GPLv2).
    License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>

<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_demographic" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect(request.getContextPath() + "/securityError.jsp?type=_demographic");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- @ taglib uri="../WEB-INF/taglibs-log.tld" prefix="log" --%>
<%@page import="oscar.OscarProperties" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@page import="org.oscarehr.common.model.EmrContextEnum" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<%
    String context = request.getParameter("context");
	String demographicNo = request.getParameter("demographicNo") != null ? request.getParameter("demographicNo") : "" ;
	String providerNo = request.getParameter("providerNo") != null ? request.getParameter("providerNo") : "";
	String appointmentId = request.getParameter("appointmentId") != null ? request.getParameter("appointmentId") : "";
	String oscarBase  = OscarProperties.getOscarProBaseUrl();  // external, clinic.url
	SystemPreferences enableThirdPartyApps = SystemPreferencesUtils.findPreferenceByName("enable_third_party_apps");
	String clinicSalt = OscarProperties.getInstance().getProperty("clinic.salt_id", "");
	if (enableThirdPartyApps != null && enableThirdPartyApps.getValueAsBoolean()) {
%>
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css">
<div id="thirdPartyAppSideBar" style="width: 48px; height: 100%; transition: all 0.5s ease; z-index: 10000; position: sticky; left: 0px;">
	<div id="resizeBtn" onclick="resizeIframe()" class="resizeBtn">
		<object style="line-height: 24px; padding-top: 7px; pointer-events: none" class="image-flip" type="image/svg+xml" data="../images/icons/apps-health/chevron.svg"></object>
	</div>
	<iframe onload="initSidebar()" style="transition: all 0.5s ease; border: none;" id="appsListIframe" width=48
		height=900
		src="<%=oscarBase%>/#/third-party-app?context=<%=context%>&demographicNo=<%=demographicNo%>&providerNo=<%=providerNo%>&appointmentId=<%=appointmentId%>&clinicSalt=<%=clinicSalt%>"></iframe>

</div>
<% } %>
<script type="text/javascript">


function resizeIframeHeightForSchedulePage() {
    document.getElementById('appsListIframe').height = document.getElementById('scheduleTable').offsetHeight;
}


function initSidebar() {
	var style = document.createElement('style');
	style.textContent =
	  'body {' +
	  '  overflow: hidden;' +
	  '}' 
	;
	document.getElementById('appsListIframe').contentDocument.head.appendChild(style);
	document.getElementById('appsListIframe').height = document.getElementById('thirdPartyAppSideBar').parentElement.offsetHeight;
	var scheduleContext = <%= (context.equals(EmrContextEnum.SCHEDULE.toString())) %>
	//if on the schedule page move the table header over, so it does not appear on top of the sidebar
	if(scheduleContext){
		document.getElementsByClassName('header-div')[0].style.left = '48px';
		// unset 100% width to prevent first load width from extending outside page
		document.getElementsByClassName('header-div')[0].style.width = 'unset';
	    document.getElementsByClassName('header-div')[0].style.right = '0px';
	    window.addEventListener('resize', resizeIframeHeightForSchedulePage);
	}
}


function resizeIframe() {
	var left ='48px';
	if(document.getElementById("appsListIframe").width === "48") {
		document.getElementById("resizeBtn").classList.add("image-flip");
		document.getElementById("resizeBtn").style.left = "237px";
		document.getElementById("appsListIframe").width = "250";
		document.getElementById("thirdPartyAppSideBar").style.width = "250px";
		left = "250px";
		
	} else {
		document.getElementById("appsListIframe").width = "48";
		document.getElementById("thirdPartyAppSideBar").style.width = "48px";
		document.getElementById("resizeBtn").style.left = "35px";
		document.getElementById("resizeBtn").classList.remove("image-flip");
	}
	

	var scheduleContext = <%=(context.equals(EmrContextEnum.SCHEDULE.toString()))%>
	//if on the schedule page remove and add the table header again so it does not overlay ontop of the sidebar, after the sidebar has resized
	if (scheduleContext) {
		document.getElementsByClassName('header-div')[0].style.transition = 'all 0.5s ease';
		document.getElementsByClassName('header-div')[0].style.left = left;

		setTimeout(function() {
			addTableHeaderFloat();
			document.getElementsByClassName('header-div')[0].style.transition = '';
		}, 500);

	}
}
</script>
<style type="text/css">
#resizeBtn:hover {
	color: white;
	background-color: #2C486E !important;
	border: solid 1px #2C486E !important;
	cursor: pointer;
}
#resizeBtn:hover object {
	filter: brightness(255);
}
.resizeBtn {
	z-index: 10000;
	width: 24px;
	height: 24px;
	border-radius: 20px;
	border: solid lightgrey 1px;
	text-align: center;
	align-content: center;
	line-height: 23px;
	font-size: 8px;
	position: absolute;
	top: 33px;
	left: 35px;
	background: white;
	transition: all 0.5s ease;"
}
.image-flip {
	-webkit-transform: scaleX(-1);
	transform: scaleX(-1);
}
</style>
