<%--
    Copyright (c) 2021 WELL EMR Group Inc.
    This software is made available under the terms of the
    GNU General Public License, Version 2, 1991 (GPLv2).
    License details are available via "gnu.org/licenses/gpl-2.0.html".
--%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_demographic" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect(request.getContextPath() + "/securityError.jsp?type=_demographic");%>
</security:oscarSec>
<%
	if(!authed) {
		return;
	}
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%-- @ taglib uri="../WEB-INF/taglibs-log.tld" prefix="log" --%>
<%@page import="oscar.OscarProperties" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>

<%
    String demographicNumber = request.getParameter("demographic_no");
    if (demographicNumber == null) {
		demographicNumber = request.getParameter("demographicNo");
    }
    if (demographicNumber == null) {
    	demographicNumber = (String)session.getAttribute("casemgmt_DemoNo");
    }
    if (demographicNumber == null && !("provider".equals(request.getParameter("function")) || "provider".equals((String) request.getAttribute("function")))) {
    	demographicNumber = (String) request.getParameter("functionid");
    }
%>
<security:oscarSec roleName="<%=roleName$%>"
				   objectName='<%="_demographic$"+demographicNumber%>' rights="o"
				   reverse="<%=false%>">
<bean:message key="demographic.demographiceditdemographic.accessDenied"/>
<% response.sendRedirect("../acctLocked.html"); 
authed=false;
%>
</security:oscarSec>

<%
if(!authed) {
	return;
}
%>
<%@page import="java.util.Map" %>
<%@page import="oscar.OscarProperties" %>
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="org.oscarehr.common.model.DemographicExtKey" %>
<%@page import="org.oscarehr.common.dao.DemographicDao" %>
<%@page import="org.oscarehr.common.dao.DemographicExtDao" %>
<%
	OscarProperties oscarProps = OscarProperties.getInstance();
	DemographicDao demographicDao=(DemographicDao)SpringUtils.getBean("demographicDao");
	DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
	
	boolean enableKaiEmr = Boolean.parseBoolean(oscarProps.getProperty("enable_kai_emr", "false"));
	boolean enableTiaHealthPatientPortal = Boolean.parseBoolean(oscarProps.getProperty("enable_tia_health_patient_portal", "false"));
	if (enableKaiEmr && enableTiaHealthPatientPortal && demographicNumber != null) {
		boolean requestSent = false;
		String portalId = demographicDao.getDemographicById(Integer.parseInt(demographicNumber)).getPortalUserId();
		Map<String,String> demoExt =
				demographicExtDao.getAllValuesForDemo(Integer.parseInt(demographicNumber));
		String portalRequestId = demoExt.get(
				DemographicExtKey.PATIENT_PORTAL_REGISTRATION_REQUEST_ID.getKey()
		);
		if (portalRequestId != null && !portalRequestId.isEmpty()) {
			requestSent = true;
		}
%>
<div id="patientPortalBar">
	<div id="patientPortalBar" style="display: flex;flex-direction: row; height: 40px;border: 1px solid #CCCCFE; font-size: 12px;justify-content: flex-end;">
           <% if (portalId == null || portalId.isEmpty()) { %>
                <% if (requestSent) { %>
				 	<img id="portalRegistrationStatusImg" 
		            	 src="../images/pending.png" 
						 alt="" 
		            	 style="padding: 12px 0px 9px 6px;">
                <% } else { %>
		            <img id="portalRegistrationStatusImg" 
		            	 src="../images/x.png" 
						 alt="" 
		            	 style="padding: 12px 0px 9px 6px;">
	            <% } %>
            <% } else { %>
	            <img id="portalRegistrationStatusImg" 
	            	 src="../images/check-status.png" 
					 alt="" 
	            	 style="padding: 12px 0px 9px 6px;">            	
            <% } %>
            <div id="portalRegistrationStatus" style="padding: 12px 6px 0px 6px;">
     			<% if (portalId == null || portalId.isEmpty()) { %>
     			    <% if (requestSent) {%>
     			    	<bean:message key="yourcare.patientportal.registrationPending"/>
     			    <% } else  {%>
     			    	<bean:message key="yourcare.patientportal.notRegistered"/>
            		<% } %>
            	<% } else { %>
            		<bean:message key="yourcare.patientportal.registered"/>
            	<% } %>
            </div>
            <div style="padding: 12px 6px 0px 6px;">|</div>
            <!-- Tia Health Patient Portal button -->
            <div id="patientPortalButton"
                 onclick="window.open('/<%= OscarProperties.getKaiemrDeployedContext() %>/#/patient-portal/settings/patient/<%= demographicNumber %>', '', 'width=1250,height=900');"
                 style="font: Verdana;font-size: 14px;font-weight: bold;background-color: #1A1BFF;color: #FFFFFF;letter-spacing: -0.5px;border-radius: 2px;padding: 9px 12px 0px 12px;margin: 2px 2px 2px 12px; cursor: pointer;">
                 <bean:message key="yourcare.patientportal.patientPortal"/>
            </div>
      </div>
</div>
<% } %>

