<%--

  Copyright (c) 2021 WELL EMR Group Inc.
  This software is made available under the terms of the
  GNU General Public License, Version 2, 1991 (GPLv2).
  License details are available via "gnu.org/licenses/gpl-2.0.html".

--%>

<%@page import="org.apache.poi.util.SystemOutLogger"%>
<%@page import="java.util.*"%>
<%@page import="org.oscarehr.common.model.MonerisUpload"%>
<%@page import="org.oscarehr.common.dao.MonerisUploadDao"%>
<%@page import="org.oscarehr.util.SessionConstants"%>
<%@page import="org.oscarehr.util.SpringUtils"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Moneris File Data</title>
</head>
<body>
  <%
  MonerisUploadDao monerisUploadDao = (MonerisUploadDao) SpringUtils.getBean("monerisUploadDao");
  MonerisUpload monerisUpload = monerisUploadDao.find(Integer.parseInt(request.getParameter("id")));

  String fileText = monerisUpload.getFileText();
  List<String> lines = Arrays.asList(fileText.split(System.lineSeparator()));
  %>

  <script type="text/javascript"
    src="<%=request.getContextPath()%>/js/jquery-1.9.1.min.js"></script>
  <script src="<%=request.getContextPath()%>/js/bootstrap.min.js"></script>
  <script type="text/javascript"
    src="<%=request.getContextPath()%>/library/bootstrap2-datepicker/bootstrap-datepicker.js"></script>

  <link href="<%=request.getContextPath()%>/css/bootstrap.min.css" rel="stylesheet">
  <link href="<%=request.getContextPath()%>/css/datepicker.css"
    rel="stylesheet" type="text/css">
  <link rel="stylesheet"
    href="<%=request.getContextPath()%>/css/font-awesome.min.css">

</body>
<h3>
  Moneris File -
  <%=monerisUpload.getFileName()%>
  Data
</h3>
<table class="table table-hover table-bordered" >
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <%
      String tableHeads[] = lines.get(0).split(",");
      for (String tableHead : tableHeads) {
        tableHead = tableHead.replace("\"", "");
      %>
      <th scope="col"><%=tableHead%></th>
      <% } %>
    </tr>
  </thead>
  <tbody>
    <tr>
      <%
      int i = 0;
      for (String singleLine : lines.subList(1, lines.size())) {
        String datas[] = singleLine.split(",");
        i++;
      %>
      <th scope="row"><%=i%></th>
      <%
      for (String data : datas) {
        data = data.replace("\"", "");
      %>
      <td><%=data%></td>
      <% } %>
    </tr>
    <% } %>
  </tbody>
</table>
</html>