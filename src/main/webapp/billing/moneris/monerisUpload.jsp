<%--

  Copyright (c) 2021 WELL EMR Group Inc.
  This software is made available under the terms of the
  GNU General Public License, Version 2, 1991 (GPLv2).
  License details are available via "gnu.org/licenses/gpl-2.0.html".

--%>

<%@page import="org.apache.poi.util.SystemOutLogger"%>
<%@page import="java.util.*"%>
<%@page import="org.oscarehr.common.model.MonerisUpload"%>
<%@page import="org.oscarehr.common.dao.MonerisUploadDao"%>
<%@page import="org.oscarehr.util.SessionConstants"%>
<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%
if (session.getAttribute("user") == null) {
  response.sendRedirect("../../../logout.jsp");
}
OscarProperties props = OscarProperties.getInstance();
session.setAttribute("homepath", props.getProperty("project_home", ""));
MonerisUploadDao monerisUploadDao = (MonerisUploadDao)SpringUtils.getBean("monerisUploadDao");
List<MonerisUpload> monerisUploadList = monerisUploadDao.findAll(0, monerisUploadDao.getCountAll());

String uploadMessage = (String)request.getAttribute("uploadMessage");

%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib prefix="csrf"
  uri="http://www.owasp.org/index.php/Category:OWASP_CSRFGuard_Project/Owasp.CsrfGuard.tld"%>
<%@page import="oscar.*"%>

<html:html>
<head>
<title><bean:message key="admin.admin.monerisUpload" /></title>
<link href="<%=request.getContextPath()%>/css/bootstrap.min.css"
  rel="stylesheet">

<script type="text/javascript">
  function onSubmit() {
    var arrInputs = document.getElementById("addUploadFile");
    var validExt = ".txt";
    var sFileName = arrInputs.value;
    if (sFileName.length > 0) {
      var isValid = false;
      if (sFileName.substr(sFileName.length - validExt.length,
          validExt.length).toLowerCase() == validExt.toLowerCase()) {
        isValid = true;
      }

      if (!isValid) {
        alert("Sorry, this is an invalid file type extension. Allowed file extension is .txt only");
        return false;
      }
    }
    var method = jQuery("#method");
    method.val('monerisUpload');
    var form = jQuery("#monerisUploadForm");
    form.submit();
    return true;
  }
</script>

<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<html:base />
</head>
<meta charset="ISO-8859-1">

<body>
  <h3>
    <bean:message key="admin.admin.monerisUpload" />
  </h3>
  <% if (uploadMessage != null && !uploadMessage.isEmpty()) { %>
  <div class="alert alert-info">
    <p>
      <%=uploadMessage%>
    </p>
  </div>
  <% } %>

  <div class="container-fluid well">
    <form id="monerisUploadForm" name="monerisUploadForm"
      styleId="monerisUploadForm" action="../moneris/monerisUpload.do"
      enctype="multipart/form-data" method="post">
      <input type="hidden" name="<csrf:tokenname/>"
        value="<csrf:tokenvalue/>" /> <input type="hidden" name="method"
        id="method" value="monerisUpload" /> Select File<input
        style="margin-left: 40px;" type="file" name="addUploadFile"
        id="addUploadFile" value="" required /> <input
        class="btn btn-primary" type="submit" name="Submit" value="Upload"
        onclick="return onSubmit();" />
    </form>

  </div>
  
  <table class="table table-striped  table-condensed">
        <tr style="font-weight:bold;">
            <td align="center">Id</td>
            <td align="center">FileName</td>
            <td align="center">Upload Date</td>
        </tr>
        <% for (MonerisUpload mUlpad : monerisUploadList) { %>
        <tr>
            <td  align="center"><%=mUlpad.getId()%></td>
            <td  align="center"><a href="monerisFileData.jsp?id=<%=mUlpad.getId()%>"> <%=mUlpad.getFileName()%></a></td>
            <td  align="center"><%=mUlpad.getUploadDate()%></td>
        </tr>
        <% } %>

    </table>
</body>
</html:html>