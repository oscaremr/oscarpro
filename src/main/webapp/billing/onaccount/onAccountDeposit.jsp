<%@page import="org.oscarehr.util.SpringUtils"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/rewrite-tag.tld" prefix="rewrite"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>

<%@page import="oscar.oscarBilling.onAccount.formBean.OnAccountDepositFormBean"%><html:html locale="true">

<head>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/global.js"></script>
	<title><bean:message key="demographic.onAccount.onAccountBilling.addDeposit" /></title>
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/css/extractedFromPages.css"  />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/oscarEncounter/encounterStyles.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/css/OscarStandardLayout.css">
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/calendar/calendar.css" title="win2k-cold-1" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/lang/<bean:message key="global.javascript.calendar"/>"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar-setup.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/prototype.js"></script>
	<style>
		.tableBorderStyle{
			font-size: x-small;
			font-variant: small-caps;
			font-weight: bold;
		}
	</style>
	<script language="javascript">
		function addPayment(date,type,amount) {
			document.forms[0].paymentDate.value = date;
			document.forms[0].paymentType.value = type;
			document.forms[0].paymentAmount.value = amount;
			document.forms[0].methodid.name="method";
			document.forms[0].methodid.value="<bean:message key='demographic.onAccount.deposit.addPaymentDetails'/>";
			document.forms[0].submit();
		}

		function addCharge(serviceCode,amount) {
			document.forms[0].serviceCode.value = serviceCode;
			document.forms[0].amount.value = amount;
			document.forms[0].methodid.name="method";
			document.forms[0].methodid.value="<bean:message key='demographic.onAccount.onAccountBilling.addCharge'/>";
			document.forms[0].submit();
		}
		
		function popup(location) {
		    var DocPopup = window.open(location,"_blank","height=300,width=300");

		    if (DocPopup != null) {
		        if (DocPopup.opener == null) {
		            DocPopup.opener = self;
		        }
		    }
		}
		
		function populatePrimaryPhysician(){
			document.getElementById("primaryPhysicianId").innerHTML = document.forms[0].primaryPhysicianHidden.value;
			document.forms[0].primaryPhysician.value = document.forms[0].primaryPhysicianHidden.value;
		}

		function populateReferringPhysician(){
			document.getElementById("referringPhysicianId").innerHTML = document.forms[0].referringPhysicianHidden.value;
			document.forms[0].referringPhysician.value = document.forms[0].referringPhysicianHidden.value;
		}

		function deleteDepositService(serviceCode){
			document.forms[0].methodid.name="method";
			document.forms[0].methodid.value="<bean:message key='demographic.onAccount.deposit.deleteService'/>";
			document.forms[0].serviceCode.value = serviceCode;
			document.forms[0].submit();
		}
		
		function deletePayment(paymentID){
			document.forms[0].methodid.name="method";
			document.forms[0].methodid.value="<bean:message key='demographic.onAccount.deposit.deletePayment'/>";
			document.forms[0].paymentID.value = paymentID;
			document.forms[0].submit();
		}

		function printDeposit(){
			document.getElementById("buttonsIDHidden").style.display = "none";
			window.print();
		}
	</script>
</head>

<%
	OnAccountDepositFormBean form = (OnAccountDepositFormBean) request.getSession().getAttribute("onAccountDepositFormBean");
	int depositCount = form.getServiceList().size();
	int paymentCount = form.getPaymentList().size();
	
	String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	
	String curUser_no = (String) session.getAttribute("user");
%>
<body class="BodyStyle" vlink="#0000FF">
<html:form action="/onAccount/addDeposit.do">
	<table>
		<tr>
			<td id="buttonsID">
				<html:submit property="method" style="font-size: small;font-family: calibri"><bean:message key="demographic.onAccount.bill.save"/></html:submit>&nbsp;&nbsp;
				<html:submit property="method" style="font-size: small;font-family: calibri"><bean:message key="demographic.onAccount.bill.saveClose"/></html:submit>&nbsp;&nbsp;
				<html:submit property="method" style="font-size: small;font-family: calibri"><bean:message key="demographic.onAccount.bill.savePrintPreview"/></html:submit>&nbsp;&nbsp;
				<html:submit property="method" style="font-size: small;font-family: calibri"><bean:message key="demographic.onAccount.bill.cancel"/></html:submit>&nbsp;&nbsp;
				
				<security:oscarSec roleName="<%=roleName$%>" objectName="_billing.OnAccountBilling.BlockVoid"
								   rights="x" reverse="<%=true%>">
				<html:submit property="method" style="font-size: small;font-family: calibri"><bean:message key="demographic.onAccount.bill.void"/></html:submit>&nbsp;&nbsp;
				</security:oscarSec>
				
				<html:hidden property="demographic_No"/>
				<html:hidden property="paymentID"/>
				<html:hidden property="paymentType"/>
				<html:hidden property="paymentAmount"/>
				<html:hidden property="paymentDate"/>
				<html:hidden property="serviceCode"/>
				<html:hidden property="amount"/>
				<input type="hidden" name="buttonCount"/>
				<input type="hidden" name="methodid"/><br><br>
			</td>
		</tr>
		<tr>
			<td id="buttonsIDHidden" style="display: none;">
				<html:button property="method" style="font-size: small;font-family: calibri" onclick="printDeposit();">Print</html:button>&nbsp;&nbsp;
				<html:button property="method" style="font-size: small;font-family: calibri" onclick="self.close();">Close</html:button>&nbsp;&nbsp;
			</td>
		</tr>
		
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td width="680px">
							<h3><bean:write name="onAccountDepositFormBean" property="remitToName" /></h3><br>
							<bean:write name="onAccountDepositFormBean" property="remitToAddress" /><br>
							<bean:write name="onAccountDepositFormBean" property="remitToCity" />,&nbsp;
							<bean:write name="onAccountDepositFormBean" property="remitToProvince" />&nbsp;
							<bean:write name="onAccountDepositFormBean" property="remitToPostal" /><br>
							<bean:message key="demographic.onAccount.bill.tel"/>:&nbsp;<bean:write name="onAccountDepositFormBean" property="remitToPhone" /><br>
							<bean:message key="demographic.onAccount.bill.fax"/>:&nbsp;<bean:write name="onAccountDepositFormBean" property="remitToFax" />
						</td>
						<td>
							<table border="1" cellpadding="0" cellspacing="0" style="border-width: 0;border-color: black">
								<tr>
									<td style="border-left-style: none; border-top-style: none">
										<b>Deposit#</b> 
									</td>
									<td style="border-right-style: none;border-top-style: none">
										<logic:notEqual name="onAccountDepositFormBean" property="invoiceNo" value="0">
											<bean:write name="onAccountDepositFormBean" property="invoiceNo"/>&nbsp;&nbsp;
										</logic:notEqual>
										<html:hidden property="invoiceNo"/>
									</td>
								</tr>
								<tr>
									<td style="border-bottom-style: none; border-left-style: none">
										<bean:message key="demographic.onAccount.bill.date"/>
									</td>
									<td style="border-right-style: none; border-bottom-style: none">
										<input type="text" name="date" id="invoiceDate" readonly size="10">&nbsp;<img src="<%=request.getContextPath()%>/images/cal.gif" id="invoice_date_cal">&nbsp;&nbsp;&nbsp;&nbsp;
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td>
				&nbsp;<br><br>
			</td>
		</tr>
		<tr> 
			<td>
				<table cellpadding="0" cellspacing="0" border="1" style="border-width: 0;border-color: black">
					<tr>
						<td width="400px" style="border-left-style: none;border-top-style: none">
							<b><bean:message key="demographic.onAccount.bill.patientInfo"/></b>
						</td>
						<td width="400px" style="border-right-style: none; border-top-style: none">
							<b><bean:message key="demographic.onAccount.bill.remitTo"/></b>
						</td>
					</tr>
					<tr>
						<td style="border-bottom-style: none; border-left-style: none">
							<bean:write name="onAccountDepositFormBean" property="patientName" /><br>
							<bean:write name="onAccountDepositFormBean" property="patientAddress" /><br>
							<bean:write name="onAccountDepositFormBean" property="patientCity" />,&nbsp;
							<bean:write name="onAccountDepositFormBean" property="patientProvince" />&nbsp;
							<bean:write name="onAccountDepositFormBean" property="patientPostal" />
						</td>
						<td style="border-bottom-style: none; border-right-style: none">
							<bean:write name="onAccountDepositFormBean" property="remitToName" /><br>
							<bean:write name="onAccountDepositFormBean" property="remitToAddress" /><br>
							<bean:write name="onAccountDepositFormBean" property="remitToCity" />,&nbsp;
							<bean:write name="onAccountDepositFormBean" property="remitToProvince" />&nbsp;
							<bean:write name="onAccountDepositFormBean" property="remitToPostal" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td>
				&nbsp;<br>
			</td>
		</tr>
		
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="1">
					<tr>
						<td class="RowTop" width="120px">
							<bean:message key="demographic.onAccount.bill.dx"/>
						</td>
						<td class="RowTop" width="225px" id="primaryPhysicianID">
							<bean:message key="demographic.onAccount.bill.primaryPhysician"/>&nbsp;&nbsp;
							<a href="#" onclick="populatePrimaryPhysician();">Add</a>
							<html:hidden property="primaryPhysicianHidden"/>
							<html:hidden property="primaryPhysician"/>
						</td>
						<td class="RowTop" width="225px" style="display: none;" id="primaryPhysicianIDHidden">
							<bean:message key="demographic.onAccount.bill.primaryPhysician"/>&nbsp;&nbsp;
						</td>
						<td class="RowTop" width="225px">
							<bean:message key="demographic.onAccount.bill.billingPhysician"/>
						</td>
						<td class="RowTop" width="225px" id="referringPhysicianID">
							<bean:message key="demographic.onAccount.bill.referringPhysician"/>&nbsp;&nbsp;
							<a href="#" onclick="populateReferringPhysician();">Add</a>
							<html:hidden property="referringPhysicianHidden"/>
							<html:hidden property="referringPhysician"/>
						</td>
						<td class="RowTop" width="225px" style="display: none" id="referringPhysicianIDHidden">
							<bean:message key="demographic.onAccount.bill.referringPhysician"/>&nbsp;&nbsp;
						</td>
					</tr>
					<tr>
						<td id="dxCodeID">
							<html:text name="onAccountDepositFormBean" property="dxCode" size="4" maxlength="4"/>
						</td>
						<td style="display: none" id="dxCodeIDHidden" >
							<bean:write name="onAccountDepositFormBean" property="dxCode" />&nbsp;
						</td>
						<td id="primaryPhysicianId">
							<bean:write name="onAccountDepositFormBean" property="primaryPhysician" />&nbsp;
						</td>
						<td >
							<bean:write name="onAccountDepositFormBean" property="billingPhysician" />&nbsp;
						</td>
						<td id="referringPhysicianId">
							<bean:write name="onAccountDepositFormBean" property="referringPhysician" />&nbsp;
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td>
				&nbsp;<br>
			</td>
		</tr>
		
		<tr id="addDepositID">
			<td>
				<%
					int invoiceNo = form.getInvoiceNo();
				%>
				<br>
				<a href="#" onclick="popup('<rewrite:reWrite jspPage="../../onAccount/selectService.do"/>?p1=true&method=Load&fromPage=Deposit');return false;">
					Add Deposit
				</a>&nbsp;&nbsp;
				<br>
				<br>
			</td>
		</tr>
		
		<tr>
			<td>
				<h2>Deposits for Services:</h2>
			</td>
		</tr>
		
		<tr>
			<td>
				<table border="1" width="800px" cellpadding="0" cellspacing="0" style="border-color: black"> 
					<tr align="center" bgcolor="#CCCCFF">
						<td width="120px" id="deleteButtonHeaderID">
							&nbsp;
						</td>
						<td width="180px" class="tableBorderStyle">
							Service Code
						</td>
						<td width="350px" class="tableBorderStyle">
							Description
						</td>
						<td width="150px" class="tableBorderStyle">
							Amount Added
						</td>
					</tr>
					<logic:iterate id="services" indexId="indexId" property="serviceList" name="onAccountDepositFormBean" type="oscar.oscarBilling.onAccount.valueObject.OnAccountServiceValueObject">
					<%
						if((indexId%2) == 0) {
					%>
					<tr align="center">
					<%
						} else {
					%>
					<tr align="center" bgcolor="#EEEEFF">
					<%
						}
						String jsStart = "deleteDepositService('";
						String jsEnd = "')";
					%>
						<td width="120px" id="deleteButtonID<%=indexId%>">
							<html:button property="method" style="font-size: small;font-family: calibri;" onclick="<%=jsStart+services.getServiceCode()+jsEnd%>">
								<bean:message key="demographic.onAccount.bill.delete"/>
							</html:button>&nbsp;
						</td>
						<td width="180px">
							<%=services.getServiceCode()%>&nbsp;
						</td>
						<td width="350px">
							<%=services.getDescription()%>&nbsp;
						</td>
						<td width="150px" align="right">
							$<%if(services.getAmount()==0){%>
								0.00
							<%}else{%>
								<bean:write name="services" property="amount" format="#.00"/>
							<%}%>
							&nbsp;
						</td>
					</tr>
					</logic:iterate>
				</table>						
			</td>
		</tr>
		<tr>
			<td>
				<table border="1" width="800px" cellpadding="0" cellspacing="0" style="border-color: black;border-width: 0"> 
					<tr>
						<td align="right" width="648px" style="border-bottom-style: none;border-left-style: none;border-top-style: none" id="totalDepositID">
							<b>Total Deposit:</b>
						</td>
						<td style="border-bottom-style: none; border-top-style: none;border-right-style: none" align="right">
							$
							<%if(form.getDepositTotal()==0){%>
								0.00
							<%}else{%>
								<bean:write name="onAccountDepositFormBean" property="depositTotal" format="#.00"/>
							<%}%>
						</td>
					</tr>
				</table>						
			</td>
		</tr>
		
		<tr>
			<td>
				&nbsp;<br><br>
			</td>
		</tr>
		
		<tr id="addPaymentID">
			<td>
				<br>
				<a href="#" onclick="popup('<rewrite:reWrite jspPage="../onaccount/addCashPayment.jsp"/>');return false;">
					Add Cash
				</a>&nbsp;&nbsp;
				<a href="#" onclick="popup('<rewrite:reWrite jspPage="../onaccount/addCardPayment.jsp"/>');return false;">
					Add Credit Card
				</a>&nbsp;&nbsp;
				<a href="#" onclick="popup('<rewrite:reWrite jspPage="../onaccount/addChequePayment.jsp"/>');return false;">
					Add Cheque
				</a>&nbsp;&nbsp;
				<a href="#" onclick="popup('<rewrite:reWrite jspPage="../onaccount/addTransferBalance.jsp"/>');return false;">
					Add Transfer Balance
				</a>&nbsp;&nbsp;
				<br>
				<br>
			</td>
		</tr>
		
		<tr>
			<td>
				<h2>Payment Summary:</h2>
			</td>
		</tr>
		
		<tr>
			<td>
				<table border="1" width="800px" cellpadding="0" cellspacing="0" style="border-color: black"> 
					<tr align="center" bgcolor="#CCCCFF">
						<td width="120px" id="deletePaymentButtonHeaderID">
							&nbsp;
						</td>
						<td width="180px" class="tableBorderStyle">
							Payment Date
						</td>
						<td width="350px" class="tableBorderStyle">
							Payment Type
						</td>
						<td width="150px" class="tableBorderStyle">
							Amount
						</td>
					</tr>
					<logic:iterate id="payments" indexId="indexId" property="paymentList" name="onAccountDepositFormBean" type="oscar.oscarBilling.onAccount.valueObject.OnAccountPaymentValueObject">
					<%
						if((indexId%2) == 0) {
					%>
					<tr align="center">
					<%
						} else {
					%>
					<tr align="center" bgcolor="#EEEEFF">
					<%
						}
						String jsStart = "deletePayment('";
						String jsEnd = "')";
					%>
						<td width="120px" id="deletePaymentButtonID<%=indexId%>">
							<html:button property="method" style="font-size: small;font-family: calibri;" onclick="<%=jsStart+payments.getPaymentID()+jsEnd%>">
								<bean:message key="demographic.onAccount.bill.delete"/>
							</html:button>&nbsp;
						</td>
						<td width="180px">
							<%=payments.getPaymentDate()%>&nbsp;
						</td>
						<td width="350px">
							<%=payments.getPaymentDetails()%>&nbsp;
						</td>
						<td width="150px" align="right">
							$<%if(payments.getPaymentAmount()==0){%>
								0.00
							<%}else{%>
								<bean:write name="payments" property="paymentAmount" format="#.00"/>
							<%}%>
						</td>
					</tr>
					</logic:iterate>
				</table>						
			</td>
		</tr>
		<tr>
			<td>
				<table border="1" width="800px" cellpadding="0" cellspacing="0" style="border-color: black;border-width: 0"> 
					<tr>
						<td align="right" width="648px" style="border-bottom-style: none;border-left-style: none;border-top-style: none" id="totalPaymentsID">
							<b>Total Payments:</b>
						</td>
						<td style="border-bottom-style: none; border-top-style: none;border-right-style: none" align="right">
							$
							<%if(form.getPaymentTotal()==0){%>
								0.00
							<%}else{%>
								<bean:write name="onAccountDepositFormBean" property="paymentTotal" format="#.00"/>
							<%}%>
						</td>
					</tr>
					<tr>
						<td align="right" width="648px" style="border-bottom-style: none;border-left-style: none;border-top-style: none" id="totalBalanceOwingID">
							<b>Balance Owing:</b>
						</td>
						<td style="border-bottom-style: none; border-top-style: none;border-right-style: none" align="right">
							$
							<%if(form.getBalanceOwing()==0){%>
								0.00
							<%}else{%>
								<bean:write name="onAccountDepositFormBean" property="balanceOwing" format="#.00"/>
							<%}%>
						</td>
					</tr>
				</table>						
			</td>
		</tr>
		
		<tr id="notesID">
			<td><b>Notes:</b><br>
				<html:textarea property="notes" name="onAccountDepositFormBean" cols="100" rows="5"></html:textarea>
			</td>
		</tr>
	</table>
</html:form>
	<%
		if(form.getButtonStatus()!= null && !"".equalsIgnoreCase(form.getButtonStatus())) {
	%>
		<script language="javascript">
			window.opener.location.reload();
		</script>	
	<%
		} 
	%>
	<%
		if("Saved".equalsIgnoreCase(form.getButtonStatus())) {
	%>
		<script language="javascript">
			alert('Deposit Saved Successfully!');
		</script>	
	<%
		} else if("Close".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Deposit Saved Successfully!');
			self.close();		
		</script>	
	<%
		} else if("Void".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Deposit Voided Successfully!');
			self.close();		
		</script>
	<%
		} else if("VoidedAlready".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Deposit is already Voided!');
		</script>
	<%
		} else if("Cancel".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			self.close();		
		</script>
	<%
		} else if("Voided".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Void deposit cannot be saved!');
		</script>
	<%
		} else if("VoidedPayment".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Cannot add payment to void deposit!');
		</script>
	<%
		} else if("VoidedService".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Cannot add deposit to void deposit!');
		</script>
	<%
		} else if("VoidedDeleteService".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Cannot delete deposit from void deposit!');
		</script>
	<%
		} else if("VoidedDeletePayment".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Cannot delete payment from void deposit!');
		</script>
	<%
		} else if("Print".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			document.getElementById("buttonsID").style.display="none";
			document.getElementById("buttonsIDHidden").style.display="block";
			document.getElementById("primaryPhysicianID").style.display="none";
			document.getElementById("primaryPhysicianIDHidden").style.display="block";
			document.getElementById("referringPhysicianID").style.display="none";
			document.getElementById("referringPhysicianIDHidden").style.display="block";
			document.getElementById("dxCodeID").style.display="none";
			document.getElementById("dxCodeIDHidden").style.display="block";
			document.getElementById("addDepositID").style.display="none";
			document.getElementById("addPaymentID").style.display="none";
			document.getElementById("deleteButtonHeaderID").style.display="none";
			document.getElementById("deletePaymentButtonHeaderID").style.display="none";
			for(i=0;i<<%=depositCount%>;i++)
				if(document.getElementById("deleteButtonID"+i) != null)
					document.getElementById("deleteButtonID"+i).style.display="none";
			for(i=0;i<<%=paymentCount%>;i++)
				if(document.getElementById("deletePaymentButtonID"+i) != null)
					document.getElementById("deletePaymentButtonID"+i).style.display="none";
			document.getElementById("notesID").style.display="none";
			document.getElementById("totalDepositID").width="622px";
			document.getElementById("totalPaymentsID").width="622px";
			document.getElementById("totalBalanceOwingID").width="622px";
		</script>
	<%
		} 
	%>
	<script type="text/javascript">
		Calendar.setup({ inputField : "invoiceDate", ifFormat : "%Y-%m-%d", showsTime :false, button : "invoice_date_cal", singleClick : true, step : 1 });
		document.getElementById("invoiceDate").value = "<%=form.getDate()%>";
	</script>

</body>
</html:html>
