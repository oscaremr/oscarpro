<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/rewrite-tag.tld" prefix="rewrite"%>

<%@page import="oscar.oscarBilling.onAccount.formBean.OnAccountFormBean"%>
<%@page import="oscar.oscarBilling.onAccount.formBean.OnAccountBillFormBean"%><html:html locale="true">
<head>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/global.js"></script>
	<%
	if("Bill".equalsIgnoreCase(request.getParameter("fromPage"))) {
	%>
	<title><bean:message key="demographic.onAccount.onAccountBilling.addCharge" /></title>
	<%
	}else{
	%>
	<title><bean:message key="demographic.onAccount.onAccountBilling.addDeposit" /></title>
	<%
	}
	%>
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/css/extractedFromPages.css"  />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/css/OscarStandardLayout.css">
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/prototype.js"></script>
	<script language="javascript">
		function submitPage(){
			if(document.forms[0].serviceCode.value=="")
				alert("Please select Service Code");
			else if(document.forms[0].amount.value==0 && "<%="Bill".equalsIgnoreCase(request.getParameter("fromPage"))%>"!="true")
			{
				alert("Please enter Amount");
			}
			else if(document.forms[0].amount.value<0)
			{
				alert("Please enter Amount");
			}
			else {
				window.opener.addCharge(document.forms[0].serviceCode.value,document.forms[0].amount.value);
				self.close();
			}
		}

		function clearValues(){
			document.forms[0].serviceCode.value = "";
			document.forms[0].amount.value = "";
		}
	</script>
</head>

<body class="BodyStyle" vlink="#0000FF" onload="clearValues();">
<html:form action="/onAccount/selectService.do">
	<table>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.bill.serviceCode"/>:&nbsp;&nbsp;
				<input type="hidden" name="serviceCode" id="serviceCode">
				<html:select property="serviceCode1" style="font-family: Calibri" onchange="document.getElementsByName('amount').item(0).value = document.getElementsByName('serviceCode1').item(0).value.split('__')[0];document.getElementsByName('serviceCode').item(0).value = document.getElementsByName('serviceCode1').item(0).value.split('__')[1]" > 
					<html:option value="">Select One</html:option>
					<html:options collection="serviceList"
       property="value" labelProperty="label" />
				</html:select>
				<html:hidden property="fromPage"/>
			</td>
		</tr>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.bill.amount"/>:&nbsp;&nbsp;
				<html:text property="amount" style="font-family: Calibri" size="10"></html:text>
			</td>
		</tr>
		<tr>
			<td>
				<%
				if("Bill".equalsIgnoreCase(request.getParameter("fromPage"))) { 
				%>
				<html:button property="button" style="font-size: small;font-family: calibri" onclick="submitPage();"><bean:message key="demographic.onAccount.onAccountBilling.addCharge"/></html:button>&nbsp;&nbsp;
				<%
				}else{
				%>
				<html:button property="button" style="font-size: small;font-family: calibri" onclick="submitPage();"><bean:message key="demographic.onAccount.onAccountBilling.addDeposit"/></html:button>&nbsp;&nbsp;
				<%
				} 
				%>
			</td>
		</tr>
	</table>
</html:form>
</body>
</html:html>
