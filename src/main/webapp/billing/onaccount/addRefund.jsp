<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/rewrite-tag.tld" prefix="rewrite"%>

<html:html locale="true">
<head>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/global.js"></script>
	<title><bean:message key="demographic.onAccount.refund.addRefund" /></title>
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/css/extractedFromPages.css"  />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/css/OscarStandardLayout.css">
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/calendar/calendar.css" title="win2k-cold-1" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/prototype.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/lang/<bean:message key="global.javascript.calendar"/>"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar-setup.js"></script>
	<script language="javascript">
		function submitPage(){
			if(document.forms[0].refundDetails.value=="")
				alert("Please enter refund details");
			else if(document.forms[0].refundAmount.value<=0)
				alert("Please enter refund amount");
			else if(document.forms[0].refundDate.value<=0)
				alert("Please select refund date");
			else {
				window.opener.addRefund(document.forms[0].refundDetails.value,document.forms[0].refundAmount.value,document.forms[0].refundDate.value);
				self.close();
			}
		}

		function clearValues(){
			document.forms[0].refundDetails.value = "";
			document.forms[0].refundAmount.value = "";
			document.forms[0].refundDate.value = "";
		}
	</script>
</head>

<body class="BodyStyle" vlink="#0000FF" onload="clearValues();">
<html:form action="/onAccount/createRefund.do">
	<table>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.refund.refundDate"/>:&nbsp;&nbsp;
				<input type="text" name="refundDate" id="refundDate" readonly size="10">&nbsp;<img src="<%=request.getContextPath()%>/images/cal.gif" id="refund_date_cal">&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.refund.refundDetails"/>:&nbsp;&nbsp;
				<html:text property="refundDetails" style="font-family: Calibri" size="25"></html:text>
			</td>
		</tr>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.refund.refundAmount"/>:&nbsp;&nbsp;
				<html:text property="refundAmount" style="font-family: Calibri" size="10"></html:text>
			</td>
		</tr>
		<tr>
			<td>
				<html:button property="button" style="font-size: small;font-family: calibri" onclick="submitPage();"><bean:message key="demographic.onAccount.refund.addRefund"/></html:button>&nbsp;&nbsp;
			</td>
		</tr>
	</table>
</html:form>
<script type="text/javascript">
Calendar.setup({ inputField : "refundDate", ifFormat : "%Y-%m-%d", showsTime :false, button : "refund_date_cal", singleClick : true, step : 1 });
</script>
</body>
</html:html>