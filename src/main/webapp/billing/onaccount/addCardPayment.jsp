<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/rewrite-tag.tld" prefix="rewrite"%>

<html:html locale="true">
<head>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/global.js"></script>
	<title><bean:message key="demographic.onAccount.deposit.addCreditCardPayment" /></title>
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/css/extractedFromPages.css"  />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/css/OscarStandardLayout.css">
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/calendar/calendar.css" title="win2k-cold-1" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/prototype.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/lang/<bean:message key="global.javascript.calendar"/>"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar-setup.js"></script>
	<script language="javascript">
		function submitPage(){
			if(document.forms[0].paymentDate.value=="")
				alert("Please enter payment date");
			else if(document.forms[0].paymentType.value=="")
				alert("Please enter payment type");
			else if(document.forms[0].ccDigits.value<=0)
				alert("Please enter last four digits of credit card");
			else if(document.forms[0].paymentAmount.value<=0)
				alert("Please enter payment amount");
			else {
				window.opener.addPayment(document.forms[0].paymentDate.value,document.forms[0].paymentType.value+" xxxx-xxxx-xxxx-"+document.forms[0].ccDigits.value,document.forms[0].paymentAmount.value);
				self.close();
			}
		}

		function clearValues(){
			document.forms[0].paymentDate.value = "";
			document.forms[0].paymentType.value = "";
			document.forms[0].ccDigits.value = "";
			document.forms[0].paymentAmount.value = "";
		}
	</script>
</head>

<body class="BodyStyle" vlink="#0000FF" onload="clearValues();">
<html:form action="/onAccount/addDeposit.do">
	<table>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.deposit.paymentDate"/>:&nbsp;&nbsp;
				<input type="text" name="paymentDate" id="paymentDate" size="10" readonly>&nbsp;<img src="<%=request.getContextPath()%>/images/cal.gif" id="payment_date_cal">&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.deposit.creditCardType"/>:&nbsp;&nbsp;
				<html:select property="paymentType" style="font-family: Calibri">
					<html:option value="">Select One</html:option>
					<html:option value="Visa">Visa</html:option>
					<html:option value="Master Card">Master Card</html:option>
					<html:option value="American Express">American Express</html:option>
					<html:option value="Debit">Debit</html:option>
					<html:option value="Other">Other</html:option>
				</html:select>
			</td>
		</tr>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.deposit.creditCardDigits"/>:&nbsp;&nbsp;
				<html:text property="ccDigits" style="font-family: Calibri" size="10"></html:text>
			</td>
		</tr>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.deposit.paymentAmount"/>:&nbsp;&nbsp;
				<html:text property="paymentAmount" style="font-family: Calibri" size="10"></html:text>
			</td>
		</tr>
		<tr>
			<td>
				<html:button property="button" style="font-size: small;font-family: calibri" onclick="submitPage();"><bean:message key="demographic.onAccount.deposit.addPayment"/></html:button>&nbsp;&nbsp;
			</td>
		</tr>
	</table>
</html:form>
<script type="text/javascript">
Calendar.setup({ inputField : "paymentDate", ifFormat : "%Y-%m-%d", showsTime :false, button : "payment_date_cal", singleClick : true, step : 1 });
</script>
</body>
</html:html>