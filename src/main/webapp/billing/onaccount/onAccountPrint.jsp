<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/rewrite-tag.tld" prefix="rewrite"%>

<%@page import="oscar.oscarBilling.onAccount.formBean.OnAccountPrintFormBean"%>
<%@page import="org.apache.commons.lang.StringUtils"%><html:html locale="true">

<head>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/global.js"></script>
	<title><bean:message key="demographic.onAccount.onAccountBilling.printHistory" /></title>
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/css/extractedFromPages.css"  />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/oscarEncounter/encounterStyles.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/css/OscarStandardLayout.css">
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/calendar/calendar.css" title="win2k-cold-1" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/lang/<bean:message key="global.javascript.calendar"/>"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar-setup.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/prototype.js"></script>
	<style>
		.tableBorderStyle{
			font-size: x-small;
			font-variant: small-caps;
			font-weight: bold;
		}
	</style>
	<script language="javascript">		
		function populatePrimaryPhysician(){
			document.getElementById("primaryPhysicianId").innerHTML = document.forms[0].primaryPhysicianHidden.value;
			document.forms[0].primaryPhysician.value = document.forms[0].primaryPhysicianHidden.value;
		}

		function populateReferringPhysician(){
			document.getElementById("referringPhysicianId").innerHTML = document.forms[0].referringPhysicianHidden.value;
			document.forms[0].referringPhysician.value = document.forms[0].referringPhysicianHidden.value;
		}
		function printBill(){
			document.getElementById("primaryPhysicianID").style.display="none";
			document.getElementById("primaryPhysicianIDHidden").style.display="block";
			document.getElementById("referringPhysicianID").style.display="none";
			document.getElementById("referringPhysicianIDHidden").style.display="block";
			document.getElementById("dxCodeID").style.display="none";
			document.getElementById("dxCodeIDHidden").style.display="block";
			document.getElementById("buttonsIDHidden").style.display = "none";
			window.print();
		}
	</script>
</head>

<%
	OnAccountPrintFormBean form = (OnAccountPrintFormBean) request.getSession().getAttribute("onAccountPrintFormBean");
%>
<body class="BodyStyle" vlink="#0000FF">
<html:form action="/onAccount/printHistory.do">
	<table>
		<tr>
			<td id="buttonsIDHidden">
				<html:button property="method" style="font-size: small;font-family: calibri" onclick="printBill();">Print</html:button>&nbsp;&nbsp;
				<html:button property="method" style="font-size: small;font-family: calibri" onclick="self.close();">Close</html:button>&nbsp;&nbsp;
			</td>
		</tr>
		
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td width="680px">
							<h3><bean:write name="onAccountPrintFormBean" property="remitToName" /></h3><br>
							<bean:write name="onAccountPrintFormBean" property="remitToAddress" /><br>
							<bean:write name="onAccountPrintFormBean" property="remitToCity" />,&nbsp;
							<bean:write name="onAccountPrintFormBean" property="remitToProvince" />&nbsp;
							<bean:write name="onAccountPrintFormBean" property="remitToPostal" /><br>
							<bean:message key="demographic.onAccount.bill.tel"/>:&nbsp;<bean:write name="onAccountPrintFormBean" property="remitToPhone" /><br>
							<bean:message key="demographic.onAccount.bill.fax"/>:&nbsp;<bean:write name="onAccountPrintFormBean" property="remitToFax" />
						</td>
						<td>
							<table border="1" cellpadding="0" cellspacing="0" style="border-width: 0;border-color: black">
								<tr>
									<td colspan="2" align="center" style="border-left-style: none; border-top-style: none;border-right-style: none">
										<b>Account History</b>
									</td>
								</tr>
								<tr>
									<td style="border-bottom-style: none; border-left-style: none">
										<bean:message key="demographic.onAccount.bill.date"/>
									</td>
									<td style="border-right-style: none; border-bottom-style: none">
										<bean:write name="onAccountPrintFormBean" property="date"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td>
				&nbsp;<br><br>
			</td>
		</tr>
		<tr> 
			<td>
				<table cellpadding="0" cellspacing="0" border="1" style="border-width: 0;border-color: black">
					<tr>
						<td width="400px" style="border-left-style: none;border-top-style: none">
							<b><bean:message key="demographic.onAccount.bill.patientInfo"/></b>
						</td>
						<td width="400px" style="border-right-style: none; border-top-style: none">
							<b><bean:message key="demographic.onAccount.bill.remitTo"/></b>
						</td>
					</tr>
					<tr>
						<td style="border-bottom-style: none; border-left-style: none">
							<bean:write name="onAccountPrintFormBean" property="patientName" /><br>
							<bean:write name="onAccountPrintFormBean" property="patientAddress" /><br>
							<bean:write name="onAccountPrintFormBean" property="patientCity" />,&nbsp;
							<bean:write name="onAccountPrintFormBean" property="patientProvince" />&nbsp;
							<bean:write name="onAccountPrintFormBean" property="patientPostal" />
						</td>
						<td style="border-bottom-style: none; border-right-style: none">
							n/a
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td>
				&nbsp;<br>
			</td>
		</tr>
		
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="1">
					<tr>
						<td class="RowTop" width="120px">
							<bean:message key="demographic.onAccount.bill.dx"/>
						</td>
						<td class="RowTop" width="225px" id="primaryPhysicianID">
							<bean:message key="demographic.onAccount.bill.primaryPhysician"/>&nbsp;&nbsp;
							<a href="#" onclick="populatePrimaryPhysician();">Add</a>
							<html:hidden property="primaryPhysicianHidden"/>
							<html:hidden property="primaryPhysician"/>
						</td>
						<td class="RowTop" width="225px" style="display: none;" id="primaryPhysicianIDHidden">
							<bean:message key="demographic.onAccount.bill.primaryPhysician"/>&nbsp;&nbsp;
						</td>
						<td class="RowTop" width="225px">
							<bean:message key="demographic.onAccount.bill.billingPhysician"/>
						</td>
						<td class="RowTop" width="225px" id="referringPhysicianID">
							<bean:message key="demographic.onAccount.bill.referringPhysician"/>&nbsp;&nbsp;
							<a href="#" onclick="populateReferringPhysician();">Add</a>
							<html:hidden property="referringPhysicianHidden"/>
							<html:hidden property="referringPhysician"/>
						</td>
						<td class="RowTop" width="225px" style="display: none" id="referringPhysicianIDHidden">
							<bean:message key="demographic.onAccount.bill.referringPhysician"/>&nbsp;&nbsp;
						</td>
					</tr>
					<tr>
						<td id="dxCodeID">
							<html:text name="onAccountPrintFormBean" property="dxCode" size="4" maxlength="4"/>
						</td>
						<td style="display: none" id="dxCodeIDHidden" >
							<bean:write name="onAccountPrintFormBean" property="dxCode" />&nbsp;
						</td>
						<td id="primaryPhysicianId">
							<bean:write name="onAccountPrintFormBean" property="primaryPhysician" />&nbsp;
						</td>
						<td >
							<bean:write name="onAccountPrintFormBean" property="billingPhysician" />&nbsp;
						</td>
						<td id="referringPhysicianId">
							<bean:write name="onAccountPrintFormBean" property="referringPhysician" />&nbsp;
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td>
				&nbsp;<br>
			</td>
		</tr>
		
		<tr>
			<td>
				Account Balance: $
				<%if(form.getAccountBalance() == 0){%>
					0.00
				<%}else{%>
					<bean:write name="onAccountPrintFormBean" property="accountBalance" format="#.00"/>
				<%}%>
			</td>
		</tr>
		
		<tr>
			<td>
				Pending Refunds: $
				<%if(form.getPendingRefunds() == 0){%>
					0.00
				<%}else{%>
					<bean:write name="onAccountPrintFormBean" property="pendingRefunds" format="#.00"/>
				<%}%>
			</td>
		</tr>
		
		<tr>
			<td>
				&nbsp;<br>
			</td>
		</tr>
		
		<tr>
			<td>
				<h1>Transaction History:</h1>
			</td>
		</tr>
		
		<tr>
			<td>
				<table border="1" width="800px" cellpadding="0" cellspacing="0" style="border-color: black"> 
					<tr align="center" bgcolor="#CCCCFF">
						<td width="50px" class="tableBorderStyle">
							#
						</td>
						<td width="140px" class="tableBorderStyle">
							Date
						</td>
						<td width="140px" class="tableBorderStyle">
							Type
						</td>
						<td width="50px" class="tableBorderStyle">
							Pending?
						</td>
						<td width="140px" class="tableBorderStyle">
							Amount
						</td>
						<td width="140px" class="tableBorderStyle">
							Received/Applied
						</td>
						<td width="140px" class="tableBorderStyle">
							Amount Owing
						</td>
					</tr>
					<logic:iterate id="txn" indexId="indexId" property="transactions" name="onAccountPrintFormBean" type="oscar.oscarBilling.onAccount.valueObject.OnAccountValueObject">
					<%
						if((indexId%2) == 0) {
					%>
					<tr align="center">
					<%
						} else {
					%>
					<tr align="center" bgcolor="#EEEEFF">
					<%
						}
					%>
						<td width="50px" >
							<%=txn.getInvoiceNo()%>
						</td>
						<td width="140px" >
							<%=txn.getDate() %>
						</td>
						<td width="140px" >
							<%=txn.getTxnType()%>
						</td>
						<td width="50px" >
							<%if(StringUtils.isNotEmpty(txn.getPendingStatus())) {%>
								<img src="<%=request.getContextPath()%>/images/check.png">
							<%
								}
							%>&nbsp;
						</td>
						<td width="140px" >
							<%if(txn.getTotalAmount() == 0){%>
								0.00
							<%} else {%>
							<bean:write name="txn" property="totalAmount" format="#.00"/>
							<%}%>
						</td>
						<td width="140px" >
							<%if(txn.getAmountCharged() == 0){%>
								0.00
							<%} else {%>
							<bean:write name="txn" property="amountCharged" format="#.00"/>
							<%}%>
						</td>
						<td width="140px" >
							<%if(txn.getBalanceOwing() == 0){%>
								0.00
							<%} else {%>
							<bean:write name="txn" property="balanceOwing" format="#.00"/>
							<%}%>
						</td>
					</tr>
					</logic:iterate>
				</table>						
			</td>
		</tr>
	</table>
</html:form>
</body>
</html:html>
