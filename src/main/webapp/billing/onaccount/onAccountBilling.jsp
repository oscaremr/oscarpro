<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/rewrite-tag.tld" prefix="rewrite"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>

<%@page import="oscar.oscarBilling.onAccount.formBean.OnAccountFormBean"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="java.text.SimpleDateFormat"%><html:html locale="true">
<head>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/global.js"></script>
	<title><bean:message key="demographic.onAccount.onAccountBilling" /></title>
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/css/extractedFromPages.css"  />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/css/OscarStandardLayout.css">
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/calendar/calendar.css" title="win2k-cold-1" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/lang/<bean:message key="global.javascript.calendar"/>"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar-setup.js"></script>
	<style>
		.tableBorderStyle{
			font-size: x-small;
			font-variant: small-caps;
			font-weight: bold;
			border-bottom-style: hidden;
			border-top-style: hidden;	
		}
	</style>
	<script language="javascript">
		function popupPage(vheight,vwidth,varpage) { //open a new popup window
		  var page = "" + varpage;
		  windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=50,screenY=50,top=20,left=20";
		  var popup=window.open(page,"",windowprops);
		  if (popup != null) {
		    if (popup.opener == null) {
		      popup.opener = self;
		    }
		    popup.focus();
		  }
		}

		function checkAllStatus(){
			if(document.getElementsByName("status")[0].checked){
				document.getElementsByName("status")[1].checked = false;
				document.getElementsByName("status")[2].checked = false;
				document.getElementsByName("status")[3].checked = false;
			}
		}

		function checkOtherStatus(){
			if(document.getElementsByName("status")[1].checked
				|| document.getElementsByName("status")[2].checked
				|| document.getElementsByName("status")[3].checked) {
				document.getElementsByName("status")[0].checked = false;
			}
		}

		function checkAllType(){
			if(document.getElementsByName("type")[0].checked){
				document.getElementsByName("type")[1].checked = false;
				document.getElementsByName("type")[2].checked = false;
				document.getElementsByName("type")[3].checked = false;
			}
		}

		function checkOtherType(){
			if(document.getElementsByName("type")[1].checked
				|| document.getElementsByName("type")[2].checked
				|| document.getElementsByName("type")[3].checked) {
				document.getElementsByName("type")[0].checked = false;
			}
		}

		function checkDateAll(){
			if(document.getElementsByName("dateAll")[0].checked) {
				document.getElementById("fromDate").value="";
				document.getElementById("toDate").value="";
				document.getElementsByName("days")[0].checked = false;
				document.getElementsByName("days")[1].checked = false;
				document.getElementsByName("days")[2].checked = false;
			}
		}

		function check30Days() {
			if(document.getElementsByName("days")[0].checked) {
				document.getElementsByName("days")[1].checked = false;
				document.getElementsByName("days")[2].checked = false;
				document.getElementById("fromDate").value="";
				document.getElementById("toDate").value="";
				document.getElementsByName("dateAll")[0].checked = false;
			}
		}

		function check60Days() {
			if(document.getElementsByName("days")[1].checked) {
				document.getElementsByName("days")[0].checked = false;
				document.getElementsByName("days")[2].checked = false;
				document.getElementById("fromDate").value="";
				document.getElementById("toDate").value="";
				document.getElementsByName("dateAll")[0].checked = false;
			}
		}

		function check90Days() {
			if(document.getElementsByName("days")[2].checked) {
				document.getElementsByName("days")[0].checked = false;
				document.getElementsByName("days")[1].checked = false;
				document.getElementById("fromDate").value="";
				document.getElementById("toDate").value="";
				document.getElementsByName("dateAll")[0].checked = false;
			}
		}

		function checkFromToDate(){
			if(document.getElementById("fromDate").value != "" 
				|| document.getElementById("toDate").value != "") {
				document.getElementsByName("days")[0].checked = false;
				document.getElementsByName("days")[1].checked = false;
				document.getElementsByName("days")[2].checked = false;
				document.getElementsByName("dateAll")[0].checked = false;
			}
		}

		function applyFilter(serviceCode){
			if((document.getElementById("fromDate").value != ""
				&& document.getElementById("toDate").value == "")
				|| (document.getElementById("fromDate").value == ""
				&& document.getElementById("toDate").value != ""))
				alert("Please enter both from and to date");
			else {
				document.forms[0].methodid.name="method";
				document.forms[0].methodid.value="<bean:message key='demographic.onAccount.onAccountBilling.applyFilter'/>";
				document.forms[0].submit();
			}
		}
	</script>
</head>
<%
	String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	OnAccountFormBean form = (OnAccountFormBean) request.getSession().getAttribute("onAccountFormBean");
%>
<body class="BodyStyle" vlink="#0000FF">
<html:form action="/onAccount/onAccountBilling.do" method="get">
	<table>
		<tr>
			<td>
				<bean:write name="onAccountFormBean" property="demoName"/>
			</td>
		</tr>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.onAccountBilling.balance"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;$
				<%if(form.getBalance() == 0){%>
					0.00
				<%}else{%>
					<bean:write name="onAccountFormBean" property="balance" format="#.00"/>
				<%}%>
			</td>
		</tr>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.onAccountBilling.paymentowing"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;$
				<%if(form.getDepositPaymentOwing() == 0){%>
					0.00
				<%}else{%>
					<bean:write name="onAccountFormBean" property="depositPaymentOwing" format="#.00"/>
				<%}%>
			</td>
		</tr>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.onAccountBilling.balancebills"/>&nbsp;&nbsp;:&nbsp;&nbsp;$
				<%if(form.getBalanceBill() == 0){%>
					0.00
				<%}else{%>
					<bean:write name="onAccountFormBean" property="balanceBill" format="#.00"/>
				<%}%>
			</td>
		</tr>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.onAccountBilling.refund"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;$
				<%if(form.getPendingRefund() == 0){%>
					0.00
				<%}else{%>
					<bean:write name="onAccountFormBean" property="pendingRefund" format="#.00"/>
				<%}%>
			</td>
		</tr>
		<tr>
			<td>
			<security:oscarSec roleName="<%=roleName$%>" objectName="_billing.OnAccountBilling.BlockAddDepositButton" rights="x" reverse="<%=true%>">
				<a href="#" onclick="popupPage(700,900,'<rewrite:reWrite jspPage="../../onAccount/onAccountBilling.do"/>?method=Add Deposit');return false;" style="text-decoration: none">
					<input type="button" value="Add Deposit" style="font-family: calibri; font-size: small">
				</a>
				</security:oscarSec>
				<security:oscarSec roleName="<%=roleName$%>" objectName="_billing.OnAccountBilling.BlockCreateBillButton" rights="x" reverse="<%=true%>" >
				<a href="#" onclick="popupPage(700,900,'<rewrite:reWrite jspPage="../../onAccount/onAccountBilling.do"/>?method=Create Bill');return false;" style="text-decoration: none">
					<input type="button" value="Create Bill" style="font-family: calibri; font-size: small">
				</a>
				</security:oscarSec>
				<security:oscarSec roleName="<%=roleName$%>" objectName="_billing.OnAccountBilling.BlockCreateRefundButton" rights="x" reverse="<%=true%>">
				<a href="#" onclick="popupPage(700,900,'<rewrite:reWrite jspPage="../../onAccount/onAccountBilling.do"/>?method=Create Refund');return false;" style="text-decoration: none">
					<input type="button" value="Create Refund" style="font-family: calibri; font-size: small">
				</a>
				</security:oscarSec>
				<a href="#" onclick="popupPage(700,900,'<rewrite:reWrite jspPage="../../onAccount/onAccountBilling.do"/>?method=Print History');return false;" style="text-decoration: none">
					<input type="button" value="Print History" style="font-family: calibri; font-size: small">
				</a><br><br>
				<html:hidden property="demographic_No"/>
				<input type="hidden" name="methodid"/>
			</td>
		</tr>
		<tr>
			<td>
				<bean:message key="demographic.onAccount.onAccountBilling.filterCriteria"/>:&nbsp;&nbsp; 
				<input type="button" value="Apply Filter" style="font-size: small;font-family: calibri" onclick="applyFilter();">&nbsp;&nbsp;
				<html:submit property="method" style="font-size: small;font-family: calibri"><bean:message key="demographic.onAccount.onAccountBilling.resetFilter"/></html:submit>&nbsp;&nbsp;<br>
			</td>
		</tr>
		<tr>
			<td>
				<table border="1" cellpadding="0" cellspacing="0" width="680px"> 
					<tr>
						<td>
							<table border="0" cellpadding="0" cellspacing="0">
								<tr> 
									<td colspan="2">
										&nbsp;
									</td>
								</tr>
								<tr> 
									<td>
										<bean:message key="demographic.onAccount.onAccountBilling.type"/>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<br>
									</td>
									<td>
										<input type="checkbox" name="type" value="All" onchange="checkAllType();"><bean:message key="demographic.onAccount.onAccountBilling.type.all"/>&nbsp;&nbsp;
										<input type="checkbox" name="type" value="Deposit" onchange="checkOtherType();"><bean:message key="demographic.onAccount.onAccountBilling.type.deposit"/>&nbsp;&nbsp;
										<input type="checkbox" name="type" value="Bill" onchange="checkOtherType();"><bean:message key="demographic.onAccount.onAccountBilling.type.bill"/>&nbsp;&nbsp;
										<input type="checkbox" name="type" value="Refund" onchange="checkOtherType();"><bean:message key="demographic.onAccount.onAccountBilling.type.refund"/>&nbsp;&nbsp;
										<div style="display: none"><input type="checkbox" name="type" value="temp" checked="checked"></div>
										<%
											if(form.getType() != null){
											String[] type = form.getType();
											for(int i=0;i<type.length;i++){
											if(type[i].equalsIgnoreCase("All")){
										%>
										<script>
											document.getElementsByName("type")[0].checked = true;
										</script>
										<%
											}else if(type[i].equalsIgnoreCase("Deposit")){
										%>
										<script>
											document.getElementsByName("type")[1].checked = true;
										</script>
										<%
											}else if(type[i].equalsIgnoreCase("Bill")){
										%>
										<script>
											document.getElementsByName("type")[2].checked = true;
										</script>
										<%
											}else if(type[i].equalsIgnoreCase("Refund")){
										%>
										<script>
											document.getElementsByName("type")[3].checked = true;
										</script>
										<%
											}}}
										%>
									</td>
								</tr>
								<tr>
									<td>
										<bean:message key="demographic.onAccount.onAccountBilling.status"/>&nbsp;&nbsp;:&nbsp;&nbsp;<br>
									</td>
									<td>
										<input type="checkbox" name="status" value="All" onchange="checkAllStatus();"><bean:message key="demographic.onAccount.onAccountBilling.status.all"/>&nbsp;&nbsp;
										<input type="checkbox" name="status" value="Refund Pending" onchange="checkOtherStatus();"><bean:message key="demographic.onAccount.onAccountBilling.status.refundPending"/>&nbsp;&nbsp;
										<input type="checkbox" name="status" value="Balance Owing" onchange="checkOtherStatus();"><bean:message key="demographic.onAccount.onAccountBilling.status.balanceOwing"/>&nbsp;&nbsp;
										<input type="checkbox" name="status" value="Void" onchange="checkOtherStatus();"><bean:message key="demographic.onAccount.onAccountBilling.status.void"/>&nbsp;&nbsp;
										<div style="display: none"><input type="checkbox" name="status" value="temp" checked="checked"></div>
										<%
											if(form.getStatus() != null){
											String[] status = form.getStatus();
											for(int i=0;i<status.length;i++){
											if(status[i].equalsIgnoreCase("All")){
										%>
										<script>
											document.getElementsByName("status")[0].checked = true;
										</script>
										<%
											}else if(status[i].equalsIgnoreCase("Refund Pending")){
										%>
										<script>
											document.getElementsByName("status")[1].checked = true;
										</script>
										<%
											}else if(status[i].equalsIgnoreCase("Balance Owing")){
										%>
										<script>
											document.getElementsByName("status")[2].checked = true;
										</script>
										<%
											}else if(status[i].equalsIgnoreCase("Void")){
										%>
										<script>
											document.getElementsByName("status")[3].checked = true;
										</script>
										<%
											}}}
										%>
									</td>
								</tr>
								<tr>
									<td>
										<bean:message key="demographic.onAccount.onAccountBilling.date"/>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<br>
									</td>
									<td>
										<input type="checkbox" name="dateAll" value="dateAll" onchange="checkDateAll();"><bean:message key="demographic.onAccount.onAccountBilling.date.all"/>&nbsp;&nbsp;
										<div style="display: none"><input type="checkbox" name="dateAll" value="temp" checked="checked"></div>
										<%
											if(form.getDateAll() != null){
												String[] dateAll = form.getDateAll();
												for(int i=0;i<dateAll.length;i++){
											if(dateAll[i].equalsIgnoreCase("dateAll")){
										%>
										<script>
											document.getElementsByName("dateAll")[0].checked = true;
										</script>
										<%
											}}}
										%>
										
										<input type="checkbox" name="days" value="30" onchange="check30Days();"><bean:message key="demographic.onAccount.onAccountBilling.date.days.30"/>&nbsp;&nbsp;
										<input type="checkbox" name="days" value="60" onchange="check60Days();"><bean:message key="demographic.onAccount.onAccountBilling.date.days.60"/>&nbsp;&nbsp;
										<input type="checkbox" name="days" value="90" onchange="check90Days();"><bean:message key="demographic.onAccount.onAccountBilling.date.days.90"/>&nbsp;&nbsp;<br>
										<div style="display: none"><input type="checkbox" name="days" value="temp" checked="checked"></div>
										<%
											if(form.getDays() != null){
											String[] days = form.getDays();
											for(int i=0;i<days.length;i++){
											if(days[i].equalsIgnoreCase("30")){
										%>
										<script>
											document.getElementsByName("days")[0].checked = true;
										</script>
										<%
											}else if(days[i].equalsIgnoreCase("60")){
										%>
										<script>
											document.getElementsByName("days")[1].checked = true;
										</script>
										<%
											}else if(days[i].equalsIgnoreCase("90")){
										%>
										<script>
											document.getElementsByName("days")[2].checked = true;
										</script>
										<%
											}}}
										%>
									</td>
								</tr>
								<tr>
									<td>
										&nbsp;
									</td>
									<td>
										<bean:message key="demographic.onAccount.onAccountBilling.date.or"/>&nbsp;&nbsp;&nbsp;&nbsp;
										<bean:message key="demographic.onAccount.onAccountBilling.date.from"/>&nbsp;:&nbsp;&nbsp;
										<input type="text" name="fromDate" id="fromDate" readonly size="10" onchange="checkFromToDate();">&nbsp;<img src="<%=request.getContextPath()%>/images/cal.gif" id="from_date_cal">&nbsp;&nbsp;&nbsp;&nbsp;
										
										<bean:message key="demographic.onAccount.onAccountBilling.date.to"/>&nbsp;:&nbsp;&nbsp;
										<input type="text" name="toDate" id="toDate" readonly size="10" onchange="checkFromToDate();">&nbsp;<img src="<%=request.getContextPath()%>/images/cal.gif" id="to_date_cal">&nbsp;&nbsp;&nbsp;&nbsp;
										<%
											if(!StringUtils.isEmpty(form.getFromDate()) && !StringUtils.isEmpty(form.getToDate())){
										%>
										<script>
											document.getElementById("fromDate").value = '<%=form.getFromDate()%>';
											document.getElementById("toDate").value = '<%=form.getToDate()%>';
										</script>
										<%
											}
										%>
									</td>
								</tr>
								<tr> 
									<td colspan="2">
										&nbsp;
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table border="1" width="680px" cellpadding="0" cellspacing="0"> 
					<tr align="center" bgcolor="#CCCCFF">
						<td width="50px" class="tableBorderStyle">
							#
						</td>
						<td width="80px" class="tableBorderStyle">
							Invoice Date
						</td>
						<td width="80px" class="tableBorderStyle">
							Type
						</td>
						<td width="45px" class="tableBorderStyle">
							Void?
						</td>
						<td width="45px" class="tableBorderStyle">
							Pending?
						</td>
						<td width="100px" class="tableBorderStyle">
							Amount
						</td>
						<td width="100px" class="tableBorderStyle">
							Received/Applied
						</td>
						<td width="100px" class="tableBorderStyle">
							Amount Owing
						</td>
						<td width="80px" class="tableBorderStyle">
							Creation Date
						</td>
					</tr>
					<logic:iterate id="txn" indexId="indexId" property="transactions" name="onAccountFormBean" type="oscar.oscarBilling.onAccount.valueObject.OnAccountValueObject">
					<%
						if((indexId%2) == 0) {
					%>
					<tr align="center">
					<%
						} else {
					%>
					<tr align="center" bgcolor="#EEEEFF">
					<%
						}
					%>
						<td width="50px" >
						<%
							if("Bill".equalsIgnoreCase(txn.getTxnType())){
						%>
							<a href="#" onclick="popupPage(700,900,'<rewrite:reWrite jspPage="../../onAccount/createBill.do"/>?method=Open&demographic_no=<%=form.getDemographic_No()%>&invoiceNo=<%=txn.getInvoiceNo()%>');return false;"><%=txn.getInvoiceNo()%></a>
						<%
							}else if("Refund".equalsIgnoreCase(txn.getTxnType())) {
						%>
							<a href="#" onclick="popupPage(700,900,'<rewrite:reWrite jspPage="../../onAccount/createRefund.do"/>?method=Open&demographic_no=<%=form.getDemographic_No()%>&invoiceNo=<%=txn.getInvoiceNo()%>');return false;"><%=txn.getInvoiceNo()%></a>
						<%
							}else if("Deposit".equalsIgnoreCase(txn.getTxnType())) {
						%>
							<a href="#" onclick="popupPage(700,900,'<rewrite:reWrite jspPage="../../onAccount/addDeposit.do"/>?method=Open&demographic_no=<%=form.getDemographic_No()%>&invoiceNo=<%=txn.getInvoiceNo()%>');return false;"><%=txn.getInvoiceNo()%></a>
						<%
							}
						%>
						</td>
						<td width="80px" >
							<%=txn.getDate() %>
						</td>
						<td width="80px" >
							<%=txn.getTxnType()%>
						</td>
						<td width="45px" >
							<%if(StringUtils.isNotEmpty(txn.getVoidStatus())) {%>
								<img src="<%=request.getContextPath()%>/images/check.png">
							<%
								}
							%>&nbsp;
						</td>
						<td width="45px" >
							<%if(StringUtils.isNotEmpty(txn.getPendingStatus())) {%>
								<img src="<%=request.getContextPath()%>/images/check.png">
							<%
								}
							%>&nbsp;
						</td>
						<td width="100px" >
							<%if(txn.getTotalAmount() == 0){%>
								0.00
							<%} else {%>
							<bean:write name="txn" property="totalAmount" format="#.00"/>
							<%}%>
						</td>
						<td width="100px" >
							<%if(txn.getAmountCharged() == 0){%>
								0.00
							<%} else {%>
							<bean:write name="txn" property="amountCharged" format="#.00"/>
							<%}%>
						</td>
						<td width="100px" >
							<%if(txn.getBalanceOwing() == 0){%>
								0.00
							<%} else {%>
							<bean:write name="txn" property="balanceOwing" format="#.00"/>
							<%}%>
						</td>
						<td width="80px">
							<%
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							out.println(sdf.format(txn.getCreationDate()));
							%>
						</td>
					</tr>
					</logic:iterate>
				</table>
			</td>
		</tr>
	</table>
</html:form>
<script type="text/javascript">
Calendar.setup({ inputField : "fromDate", ifFormat : "%Y-%m-%d", showsTime :false, button : "from_date_cal", singleClick : true, step : 1 });
Calendar.setup({ inputField : "toDate", ifFormat : "%Y-%m-%d", showsTime :false, button : "to_date_cal", singleClick : true, step : 1 });
</script>
</body>
</html:html>
