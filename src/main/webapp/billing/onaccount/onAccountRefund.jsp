<%@page import="org.oscarehr.util.SpringUtils"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/rewrite-tag.tld" prefix="rewrite"%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>

<%@page import="oscar.oscarBilling.onAccount.formBean.OnAccountRefundFormBean"%><html:html locale="true">

<head>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/global.js"></script>
	<title><bean:message key="demographic.onAccount.onAccountBilling.createRefund" /></title>
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/css/extractedFromPages.css"  />
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/oscarEncounter/encounterStyles.css">
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/share/css/OscarStandardLayout.css">
	<link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/share/calendar/calendar.css" title="win2k-cold-1" />
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/lang/<bean:message key="global.javascript.calendar"/>"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar-setup.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/prototype.js"></script>
	<style>
		.tableBorderStyle{
			font-size: x-small;
			font-variant: small-caps;
			font-weight: bold;
		}
	</style>
	<script language="javascript">
		function addRefund(details,amount,date) {
			document.forms[0].refundDetails.value = details;
			document.forms[0].refundAmount.value = amount;
			document.forms[0].refundDate.value = date;
			document.forms[0].methodid.name="method";
			document.forms[0].methodid.value="<bean:message key='demographic.onAccount.refund.addRefund'/>";
			document.forms[0].submit();
		}
		
		function popup(location) {
		    var DocPopup = window.open(location,"_blank","height=300,width=300");

		    if (DocPopup != null) {
		        if (DocPopup.opener == null) {
		            DocPopup.opener = self;
		        }
		    }
		}
		
		function populatePrimaryPhysician(){
			document.getElementById("primaryPhysicianId").innerHTML = document.forms[0].primaryPhysicianHidden.value;
			document.forms[0].primaryPhysician.value = document.forms[0].primaryPhysicianHidden.value;
		}

		function populateReferringPhysician(){
			document.getElementById("referringPhysicianId").innerHTML = document.forms[0].referringPhysicianHidden.value;
			document.forms[0].referringPhysician.value = document.forms[0].referringPhysicianHidden.value;
		}

		function deleteRefund(refundID){
			document.forms[0].methodid.name="method";
			document.forms[0].methodid.value="<bean:message key='demographic.onAccount.bill.delete'/>";
			document.forms[0].refundID.value = refundID;
			document.forms[0].submit();
		}

		function printRefund(){
			document.getElementById("buttonsIDHidden").style.display = "none";
			window.print();
		}
	</script>
</head>

<%
	OnAccountRefundFormBean form = (OnAccountRefundFormBean) request.getSession().getAttribute("onAccountRefundFormBean");
	int refundCount = form.getRefundList().size();
	
	String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
	
	String curUser_no = (String) session.getAttribute("user");

%>
<body class="BodyStyle" vlink="#0000FF">
<html:form action="/onAccount/createRefund.do">
	<table>
		<tr>
			<td id="buttonsID">
				<html:submit property="method" style="font-size: small;font-family: calibri"><bean:message key="demographic.onAccount.bill.save"/></html:submit>&nbsp;&nbsp;
				<html:submit property="method" style="font-size: small;font-family: calibri"><bean:message key="demographic.onAccount.bill.saveClose"/></html:submit>&nbsp;&nbsp;
				<html:submit property="method" style="font-size: small;font-family: calibri"><bean:message key="demographic.onAccount.bill.savePrintPreview"/></html:submit>&nbsp;&nbsp;
				<html:submit property="method" style="font-size: small;font-family: calibri"><bean:message key="demographic.onAccount.bill.cancel"/></html:submit>&nbsp;&nbsp;
				
				<security:oscarSec roleName="<%=roleName$%>" objectName="_billing.OnAccountBilling.BlockVoid"
								   rights="x" reverse="<%=true%>">
				<html:submit property="method" style="font-size: small;font-family: calibri"><bean:message key="demographic.onAccount.bill.void"/></html:submit>&nbsp;&nbsp;
				</security:oscarSec>
				
				<html:hidden property="demographic_No"/>
				<html:hidden property="refundID"/>
				<html:hidden property="refundAmount"/>
				<html:hidden property="refundDetails"/>
				<html:hidden property="refundDate"/>
				<input type="hidden" name="methodid"/><br><br>
			</td>
		</tr>
		<tr>
			<td id="buttonsIDHidden" style="display: none;">
				<html:button property="method" style="font-size: small;font-family: calibri" onclick="printRefund();">Print</html:button>&nbsp;&nbsp;
				<html:button property="method" style="font-size: small;font-family: calibri" onclick="self.close();">Close</html:button>&nbsp;&nbsp;
			</td>
		</tr>
		
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td width="680px">
							<h3><bean:write name="onAccountRefundFormBean" property="remitToName" /></h3><br>
							<bean:write name="onAccountRefundFormBean" property="remitToAddress" /><br>
							<bean:write name="onAccountRefundFormBean" property="remitToCity" />,&nbsp;
							<bean:write name="onAccountRefundFormBean" property="remitToProvince" />&nbsp;
							<bean:write name="onAccountRefundFormBean" property="remitToPostal" /><br>
							<bean:message key="demographic.onAccount.bill.tel"/>:&nbsp;<bean:write name="onAccountRefundFormBean" property="remitToPhone" /><br>
							<bean:message key="demographic.onAccount.bill.fax"/>:&nbsp;<bean:write name="onAccountRefundFormBean" property="remitToFax" />
						</td>
						<td>
							<table border="1" cellpadding="0" cellspacing="0" style="border-width: 0;border-color: black">
								<tr>
									<td style="border-left-style: none; border-top-style: none">
										<b>Refund#</b> 
									</td>
									<td style="border-right-style: none;border-top-style: none">
										<bean:write name="onAccountRefundFormBean" property="invoiceNo"/>&nbsp;&nbsp;
										<html:hidden property="invoiceNo"/>
									</td>
								</tr>
								<tr>
									<td style="border-bottom-style: none; border-left-style: none">
										<bean:message key="demographic.onAccount.bill.date"/>
									</td>
									<td style="border-right-style: none; border-bottom-style: none">
										<input type="text" name="date" id="invoiceDate" readonly size="10">&nbsp;<img src="<%=request.getContextPath()%>/images/cal.gif" id="invoice_date_cal">&nbsp;&nbsp;&nbsp;&nbsp;
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td>
				&nbsp;<br><br>
			</td>
		</tr>
		<tr> 
			<td>
				<table cellpadding="0" cellspacing="0" border="1" style="border-width: 0;border-color: black">
					<tr>
						<td width="400px" style="border-left-style: none;border-top-style: none">
							<b><bean:message key="demographic.onAccount.bill.patientInfo"/></b>
						</td>
						<td width="400px" style="border-right-style: none; border-top-style: none">
							<b><bean:message key="demographic.onAccount.bill.remitTo"/></b>
						</td>
					</tr>
					<tr>
						<td style="border-bottom-style: none; border-left-style: none">
							<bean:write name="onAccountRefundFormBean" property="patientName" /><br>
							<bean:write name="onAccountRefundFormBean" property="patientAddress" /><br>
							<bean:write name="onAccountRefundFormBean" property="patientCity" />,&nbsp;
							<bean:write name="onAccountRefundFormBean" property="patientProvince" />&nbsp;
							<bean:write name="onAccountRefundFormBean" property="patientPostal" />
						</td>
						<td style="border-bottom-style: none; border-right-style: none">
							n/a
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td>
				&nbsp;<br>
			</td>
		</tr>
		
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="1">
					<tr>
						<td class="RowTop" width="120px">
							<bean:message key="demographic.onAccount.bill.dx"/>
						</td>
						<td class="RowTop" width="225px" id="primaryPhysicianID">
							<bean:message key="demographic.onAccount.bill.primaryPhysician"/>&nbsp;&nbsp;
							<a href="#" onclick="populatePrimaryPhysician();">Add</a>
							<html:hidden property="primaryPhysicianHidden"/>
							<html:hidden property="primaryPhysician"/>
						</td>
						<td class="RowTop" width="225px" style="display: none;" id="primaryPhysicianIDHidden">
							<bean:message key="demographic.onAccount.bill.primaryPhysician"/>&nbsp;&nbsp;
						</td>
						<td class="RowTop" width="225px">
							<bean:message key="demographic.onAccount.bill.billingPhysician"/>
						</td>
						<td class="RowTop" width="225px" id="referringPhysicianID">
							<bean:message key="demographic.onAccount.bill.referringPhysician"/>&nbsp;&nbsp;
							<a href="#" onclick="populateReferringPhysician();">Add</a>
							<html:hidden property="referringPhysicianHidden"/>
							<html:hidden property="referringPhysician"/>
						</td>
						<td class="RowTop" width="225px" style="display: none" id="referringPhysicianIDHidden">
							<bean:message key="demographic.onAccount.bill.referringPhysician"/>&nbsp;&nbsp;
						</td>
					</tr>
					<tr>
						<td id="dxCodeID">
							<html:text name="onAccountRefundFormBean" property="dxCode" size="4" maxlength="4"/>
						</td>
						<td style="display: none" id="dxCodeIDHidden" >
							<bean:write name="onAccountRefundFormBean" property="dxCode" />&nbsp;
						</td>
						<td id="primaryPhysicianId">
							<bean:write name="onAccountRefundFormBean" property="primaryPhysician" />&nbsp;
						</td>
						<td >
							<bean:write name="onAccountRefundFormBean" property="billingPhysician" />&nbsp;
						</td>
						<td id="referringPhysicianId">
							<bean:write name="onAccountRefundFormBean" property="referringPhysician" />&nbsp;
						</td>
					</tr>
				</table>
			</td>
		</tr>
		
		<tr>
			<td>
				&nbsp;<br>
			</td>
		</tr>
		
		<tr id="addRefundID">
			<td>
				<%
					int invoiceNo = form.getInvoiceNo();
				%>
				<br>
				<a href="#" onclick="popup('<rewrite:reWrite jspPage="../onaccount/addRefund.jsp"/>');return false;">
					Add Refund
				</a>&nbsp;&nbsp;
				<input type="checkbox" name="refundPending">Mark Refund as Pending
				<script>
					<%
					if("on".equalsIgnoreCase(form.getRefundPending())){
					%>
					document.forms[0].refundPending.checked = true;
					<%
					}
					%>
				</script>
				<br>
				<br>
			</td>
		</tr>
		
		<tr>
			<td>
				<table border="1" width="800px" cellpadding="0" cellspacing="0" style="border-color: black"> 
					<tr align="center" bgcolor="#CCCCFF">
						<td width="120px" id="deleteButtonHeaderID">
							&nbsp;
						</td>
						<td width="180px" class="tableBorderStyle">
							Date
						</td>
						<td width="350px" class="tableBorderStyle">
							Refund Details
						</td>
						<td width="150px" class="tableBorderStyle">
							Amount
						</td>
					</tr>
					<logic:iterate id="refunds" indexId="indexId" property="refundList" name="onAccountRefundFormBean" type="oscar.oscarBilling.onAccount.valueObject.OnAccountRefundValueObject">
					<%
						if((indexId%2) == 0) {
					%>
					<tr align="center">
					<%
						} else {
					%>
					<tr align="center" bgcolor="#EEEEFF">
					<%
						}
						String jsStart = "deleteRefund('";
						String jsEnd = "')";
					%>
						<td width="120px" id="deleteButtonID<%=indexId%>">
							<html:button property="method" style="font-size: small;font-family: calibri;" onclick="<%=jsStart+refunds.getRefundID()+jsEnd%>">
								<bean:message key="demographic.onAccount.bill.delete"/>
							</html:button>&nbsp;
						</td>
						<td width="180px">
							<%=refunds.getDate()%>&nbsp;
						</td>
						<td width="350px">
							<%=refunds.getRefundDetails()%>&nbsp;
						</td>
						<td width="150px" align="right">
							$<%if(refunds.getRefundAmount()==0){%>
								0.00
							<%}else{%>
								<bean:write name="refunds" property="refundAmount" format="#.00"/>
							<%}%>
						</td>
					</tr>
					</logic:iterate>
				</table>						
			</td>
		</tr>
		<tr>
			<td>
				<table border="1" width="800px" cellpadding="0" cellspacing="0" style="border-color: black;border-width: 0"> 
					<tr>
						<td align="right" width="648px" style="border-bottom-style: none;border-left-style: none;border-top-style: none" id="totalAmountID">
							<b>Total Amount:</b>
						</td>
						<td style="border-bottom-style: none; border-top-style: none;border-right-style: none" align="right">
							$
							<%if(form.getTotalAmount()==0){%>
								0.00
							<%}else{%>
								<bean:write name="onAccountRefundFormBean" property="totalAmount" format="#.00"/>
							<%}%>
						</td>
					</tr>
				</table>						
			</td>
		</tr>
		
		<tr id="notesID">
			<td><b>Notes:</b><br>
				<html:textarea property="notes" name="onAccountRefundFormBean" cols="100" rows="5"></html:textarea>
			</td>
		</tr>
	</table>
</html:form>
	<%
		if(form.getButtonStatus()!= null && !"".equalsIgnoreCase(form.getButtonStatus())) {
	%>
		<script language="javascript">
			window.opener.location.reload();
		</script>	
	<%
		} 
	%>
	<%
		if("Saved".equalsIgnoreCase(form.getButtonStatus())) {
	%>
		<script language="javascript">
			alert('Refund Saved Successfully!');
		</script>	
	<%
		} else if("Close".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Refund Saved Successfully!');
			self.close();		
		</script>	
	<%
		} else if("Void".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Refund Voided Successfully!');
			self.close();		
		</script>
	<%
		} else if("VoidedAlready".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Refund is already Voided!');
		</script>
	<%
		} else if("Cancel".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			self.close();		
		</script>
	<%
		} else if("Voided".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Void refund cannot be saved!');
		</script>
	<%
		} else if("VoidedCharge".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Cannot add refund to void refund!');
		</script>
	<%
		} else if("VoidedDelete".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			alert('Cannot delete refund from void refund!');
		</script>
	<%
		} else if("Print".equalsIgnoreCase(form.getButtonStatus())) { 
	%>
		<script language="javascript">
			document.getElementById("buttonsID").style.display="none";
			document.getElementById("buttonsIDHidden").style.display="block";
			document.getElementById("primaryPhysicianID").style.display="none";
			document.getElementById("primaryPhysicianIDHidden").style.display="block";
			document.getElementById("referringPhysicianID").style.display="none";
			document.getElementById("referringPhysicianIDHidden").style.display="block";
			document.getElementById("dxCodeID").style.display="none";
			document.getElementById("dxCodeIDHidden").style.display="block";
			document.getElementById("addRefundID").style.display="none";
			document.getElementById("deleteButtonHeaderID").style.display="none";
			for(i=0;i<<%=refundCount%>;i++)
				if(document.getElementById("deleteButtonID"+i) != null)
					document.getElementById("deleteButtonID"+i).style.display="none";
						document.getElementById("notesID").style.display="none";
			document.getElementById("totalAmountID").width="622px";
		</script>
	<%
		} 
	%>
	<script type="text/javascript">
		Calendar.setup({ inputField : "invoiceDate", ifFormat : "%Y-%m-%d", showsTime :false, button : "invoice_date_cal", singleClick : true, step : 1 });
		document.getElementById("invoiceDate").value = "<%=form.getDate()%>";
	</script>

</body>
</html:html>
