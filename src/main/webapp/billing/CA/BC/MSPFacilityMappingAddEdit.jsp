<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.dao.MSPFacilityMappingDao" %>
<%@ page import="org.oscarehr.common.model.MSPFacilityMapping" %>
<%@ page import="java.util.List" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="org.oscarehr.common.model.Provider" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    String clinic = request.getParameter("clinic") != null ? request.getParameter("clinic") : "";
    String clinicId = request.getParameter("clinicId") != null ? request.getParameter("clinicId") : "";
    String facilityNo = request.getParameter("facilityNo") != null ? request.getParameter("facilityNo") : "";
    String subNo = request.getParameter("subNo") != null ? request.getParameter("subNo") : "";
%>

<html:html locale="true">
    <head>
        <title><bean:message key="admin.admin.MSPFacilityMapping"/></title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-ui-1.10.2.custom.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
    <h4><bean:message key="admin.admin.MSPFacilityMapping"/></h4>
    <form name="MSPFacilityAddEditForm" method="post" action="MSPFacilityMappingSettings.jsp">
        <input type="hidden" name="dboperation" value="">
        <input type="hidden" name="clinicId" value="<%=clinicId%>">
        <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <td>
                    Clinic
                </td>
                <td>
                    <input type="text" id="clinic" name="clinic" value="<%=Encode.forHtmlAttribute(clinic)%>" maxlength="255">
                </td>
            </tr>
            <tr>
                <td>
                    Facility Number
                </td>
                <td>
                    <input type="text" id="facilityNo" name="facilityNo" value="<%=Encode.forHtmlAttribute(facilityNo)%>" maxlength="5">
                </td>
            </tr>
            <tr>
                <td>
                    Sub Number
                </td>
                <td>
                    <input type="text" id="subNo" name="subNo" value="<%=Encode.forHtmlAttribute(subNo)%>" maxlength="5">
                </td>
            </tr>
            </tbody>
        </table>

        <a href="MSPFacilityMappingSettings.jsp" class="btn btn-primary">Back</a>
        <input id="addBtn" type="button" class="btn btn-primary" onclick="document.forms['MSPFacilityAddEditForm'].dboperation.value='Save'; document.forms['MSPFacilityAddEditForm'].submit();" name="saveMSPFacilitySettings" value="Save"/>
    </form>
    <script type="text/javascript">
        jQuery(setupBillingServiceAutocomplete("../../../billing/SearchBillingService.do"));
    </script>
    </body>
</html:html>