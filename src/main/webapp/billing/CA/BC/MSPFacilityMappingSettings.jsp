<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.dao.MSPFacilityMappingDao" %>
<%@ page import="org.oscarehr.common.model.MSPFacilityMapping" %>
<%@ page import="java.util.List" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.OscarLogDao" %>
<%@ page import="org.oscarehr.common.model.OscarLog" %>
<%@ page import="org.apache.commons.lang3.math.NumberUtils" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    MSPFacilityMappingDao mspFacilityMappingDao = SpringUtils.getBean(MSPFacilityMappingDao.class);
    SystemPreferences filterPreference = SystemPreferencesUtils.findPreferenceByName("msp_facility_mapping_black_white");
    OscarLogDao oscarLogDao = SpringUtils.getBean(OscarLogDao.class);
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(session);
    String dboperation = StringUtils.trimToEmpty(request.getParameter("dboperation"));

    if (dboperation.equals("Save")) {
        String facilityNo = request.getParameter("facilityNo");
        String subNo = request.getParameter("subNo");
        String clinic = request.getParameter("clinic");
        Integer clinicId = NumberUtils.isParsable(request.getParameter("clinicId")) ? Integer.parseInt(request.getParameter("clinicId")) : 0;
        
        MSPFacilityMapping mspFacilityMapping = mspFacilityMappingDao.find(clinicId);
        if (mspFacilityMapping == null) {
            mspFacilityMapping = new MSPFacilityMapping();
            mspFacilityMapping.setClinic(clinic);
            mspFacilityMapping.setFacilityNumber(facilityNo);
            mspFacilityMapping.setSubNumber(subNo);
            mspFacilityMappingDao.persist(mspFacilityMapping);
        } else {
            OscarLog oscarLog = new OscarLog();
            oscarLog.setAction("edit");
            oscarLog.setContent("MSPFacilityMapping");
            oscarLog.setContentId("clinicId=" + clinicId);
            oscarLog.setIp(loggedInInfo.getIp());
            oscarLog.setProviderNo(loggedInInfo.getLoggedInProviderNo());
            oscarLog.setSecurityId(loggedInInfo.getLoggedInSecurity().getSecurityNo());
            oscarLog.setData("facilityNumber=" + mspFacilityMapping.getFacilityNumber() + " -> " + facilityNo + "; "
                    + "subNumber=" + mspFacilityMapping.getSubNumber() + " -> " + subNo + ";"
                    + "clinicName=" + mspFacilityMapping.getClinic() + " -> " + clinic + ";");
            oscarLogDao.persist(oscarLog);
            
            mspFacilityMapping.setFacilityNumber(facilityNo);
            mspFacilityMapping.setSubNumber(subNo);
            mspFacilityMapping.setClinic(clinic);
            mspFacilityMappingDao.merge(mspFacilityMapping);
        }
    } else if (dboperation.equals("Remove")) {
        Integer clinicId = Integer.parseInt(request.getParameter("clinicId"));

        mspFacilityMappingDao.removeClinic(clinicId, loggedInInfo);
    }

    List<MSPFacilityMapping> mspFacilityMappingList = mspFacilityMappingDao.findAll();
    boolean multipleSites = !mspFacilityMappingList.isEmpty() && !(filterPreference != null && filterPreference.getValue() != null && filterPreference.getValue().equals("white"));
%>

<html:html locale="true">
    <head>
        <title><bean:message key="admin.admin.MSPFacilityMapping"/></title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
        <script>
            function addNew () {
                if (<%=multipleSites%>) {
                    alert("Error: You cannot have multiple clinics with a blacklist. Please change to a whitelist before adding new clinics.")
                } else {
                    self.location.href = "MSPFacilityMappingAddEdit.jsp";
                }
            }
        </script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
        <h4><bean:message key="admin.admin.MSPFacilityMapping"/></h4>
        <div style="padding-bottom: 10px">
            <input type="button" onclick="addNew()" class="btn btn-primary" value="New"/>
            <a href="MSPFacilityMappingProviderList.jsp" class="btn btn-primary">Provider List</a>
            <a href="MSPFacilityMappingCodeAddEdit.jsp" class="btn btn-primary">Billing Codes</a>
        </div>
        <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <th>Clinic</th>
            <th>Facility Number</th>
            <th>Sub Number</th>
            <th></th>
                <%
                    for (MSPFacilityMapping mspFacilityMapping : mspFacilityMappingList) {
                %>
                    <tr>
                        <td>
                            <%=Encode.forHtml(mspFacilityMapping.getClinic())%>
                        </td>
                        <td>
                            <%=Encode.forHtml(mspFacilityMapping.getFacilityNumber())%>
                        </td>
                        <td>
                            <%=Encode.forHtml(mspFacilityMapping.getSubNumber())%>
                        </td>
                        <td>
                            <a href="MSPFacilityMappingAddEdit.jsp?dboperation=Edit&clinicId=<%=mspFacilityMapping.getId()%>&clinic=<%=mspFacilityMapping.getClinic()%>&facilityNo=<%=Encode.forUriComponent(mspFacilityMapping.getFacilityNumber())%>&subNo=<%=Encode.forUriComponent(mspFacilityMapping.getSubNumber())%>" class="btn btn-primary">Edit</a>
                            <a href="MSPFacilityMappingSettings.jsp?dboperation=Remove&clinicId=<%=mspFacilityMapping.getId()%>" class="btn btn-primary">Remove</a>
                        </td>
                    </tr>
                <%
                    }
                %>
            </tbody>
        </table>
    </body>
</html:html>