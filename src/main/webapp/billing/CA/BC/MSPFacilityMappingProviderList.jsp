<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="java.util.List" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.common.model.Provider" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.OscarLogDao" %>
<%@ page import="org.oscarehr.common.model.OscarLog" %>
<%@ page import="org.oscarehr.common.dao.MSPFacilityMappingBlackWhiteDao" %>
<%@ page import="org.oscarehr.common.model.MSPFacilityMappingBlackWhite" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="java.util.Date" %>
<%@ page import="org.oscarehr.common.model.MSPFacilityMapping" %>
<%@ page import="org.oscarehr.common.dao.MSPFacilityMappingDao" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="org.apache.commons.lang3.math.NumberUtils" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    MSPFacilityMappingBlackWhiteDao mspFacilityMappingBlackWhiteDao = SpringUtils.getBean(MSPFacilityMappingBlackWhiteDao.class);
    MSPFacilityMappingDao mspFacilityMappingDao = SpringUtils.getBean(MSPFacilityMappingDao.class);
    ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
    OscarLogDao oscarLogDao = SpringUtils.getBean(OscarLogDao.class);
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(session);
    boolean alreadyExistsError = false;

    if (StringUtils.isNotEmpty(request.getParameter("dboperation")) && request.getParameter("dboperation").equals("Save")) {
        String providerNo = request.getParameter("providerNoAdd");
        Integer clinic = NumberUtils.isParsable(request.getParameter("clinic")) ? Integer.parseInt(request.getParameter("clinic")) : 0;
        
        if (clinic > 0 && providerNo != null && mspFacilityMappingBlackWhiteDao.find(providerNo) == null) {

            OscarLog oscarLog = new OscarLog();
            oscarLog.setAction("Save");
            oscarLog.setContent("MSPFacilityMapping");
            oscarLog.setContentId(providerNo);
            oscarLog.setIp(loggedInInfo.getIp());
            oscarLog.setProviderNo(loggedInInfo.getLoggedInProviderNo());
            oscarLog.setSecurityId(loggedInInfo.getLoggedInSecurity().getSecurityNo());
            oscarLogDao.persist(oscarLog);

            MSPFacilityMappingBlackWhite mspFacilityMappingBlackWhite = new MSPFacilityMappingBlackWhite();
            mspFacilityMappingBlackWhite.setProviderNo(providerNo);
            mspFacilityMappingBlackWhite.setClinicId(clinic);
            mspFacilityMappingBlackWhiteDao.persist(mspFacilityMappingBlackWhite);
        } else {
            alreadyExistsError = true;
        }
    } else {
        if (StringUtils.isNotEmpty(request.getParameter("dboperation")) && request.getParameter("dboperation").equals("Remove")) {
            String providerNo = request.getParameter("providerNo");

            if (providerNo != null && mspFacilityMappingBlackWhiteDao.find(providerNo) != null) {

                OscarLog oscarLog = new OscarLog();
                oscarLog.setAction("Remove");
                oscarLog.setContent("MSPFacilityMapping");
                oscarLog.setContentId(providerNo);
                oscarLog.setIp(loggedInInfo.getIp());
                oscarLog.setProviderNo(loggedInInfo.getLoggedInProviderNo());
                oscarLog.setSecurityId(loggedInInfo.getLoggedInSecurity().getSecurityNo());
                oscarLogDao.persist(oscarLog);

                mspFacilityMappingBlackWhiteDao.removeByProviderNo(providerNo);
            }
        }
    }

    SystemPreferences filterPreference = SystemPreferencesUtils.findPreferenceByName("msp_facility_mapping_black_white");
    
    if (request.getParameter("selectedFilter") != null) {
        if (filterPreference != null) {
            filterPreference.setValue(request.getParameter("selectedFilter"));
            filterPreference.setUpdateDate(new Date());
            SystemPreferencesUtils.merge(filterPreference);
        } else {
            filterPreference = new SystemPreferences();
            filterPreference.setName("msp_facility_mapping_black_white");
            filterPreference.setValue(request.getParameter("selectedFilter"));
            filterPreference.setUpdateDate(new Date());
            SystemPreferencesUtils.persist(filterPreference);
        }
    }
    
    List<Provider> providerList = providerDao.getActiveProviders();
    List<MSPFacilityMappingBlackWhite> mspFacilityMappingBlackWhiteList = mspFacilityMappingBlackWhiteDao.findAll();
    List<MSPFacilityMapping> mspFacilityMappingList = mspFacilityMappingDao.findAll();
    Map<Integer, String> clinicNameMap = new HashMap<Integer, String>();
    
    for (MSPFacilityMapping mspFacilityMapping : mspFacilityMappingList) {
        clinicNameMap.put(mspFacilityMapping.getId(), mspFacilityMapping.getClinic());
    }

    for (MSPFacilityMappingBlackWhite mspFacilityMappingBlackWhite : mspFacilityMappingBlackWhiteList) {
        if (!clinicNameMap.containsKey(mspFacilityMappingBlackWhite.getClinicId())) {
            clinicNameMap.put(mspFacilityMappingBlackWhite.getClinicId(), "Error: Clinic name not Found!");
        }
    }

    Map<String, String> providerNameMap = new HashMap<String, String>();

    for (Provider provider : providerList) {
        providerNameMap.put(provider.getProviderNo(), provider.getFormattedName());
    }

    for (MSPFacilityMappingBlackWhite mspFacilityMappingBlackWhite : mspFacilityMappingBlackWhiteList) {
        if (!providerNameMap.containsKey(mspFacilityMappingBlackWhite.getProviderNo())) {
            providerNameMap.put(mspFacilityMappingBlackWhite.getProviderNo(), "Error: Provider not found or inactive!");
        }
    }
    
    boolean multipleSites = mspFacilityMappingList.size() > 1 && filterPreference != null && filterPreference.getValue() != null && filterPreference.getValue().equals("white");
%>

<html:html locale="true">
    <head>
        <title><bean:message key="admin.admin.MSPFacilityMappingBlackWhite"/></title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<%=request.getContextPath()%>/css/bootstrap2-toggle.min.css" rel="stylesheet">

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap2-toggle.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
        <script>
            var confirmed = false;
            function changeFilter () {
                if (!confirmed) {
                    return;
                }
                confirmed = false;
                var filterType = document.getElementById("blackwhite").checked ? 'white' : 'black';
                self.location.href = "MSPFacilityMappingProviderList.jsp?selectedFilter=" + filterType;
            }
            function confirmation () {
                if (<%=multipleSites%>) {
                    alert("Error: You cannot change to a blacklist with multiple clinics enabled.");
                } else if (confirm("Are you sure you want to change the list type?")) {
                    confirmed = true;
                    return true;
                }
                var blackwhite = document.getElementById("blackwhite");
                if (<%=(filterPreference != null && filterPreference.getValue() != null && filterPreference.getValue().equals("white"))%>) {
                    blackwhite.checked = blackwhite.checked ? false : blackwhite.checked;
                } else {
                    blackwhite.checked = blackwhite.checked ? blackwhite.checked : true;
                }
                return false;
            }
        </script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
        <h4><bean:message key="admin.admin.MSPFacilityMappingBlackWhite"/></h4>
        <% if (alreadyExistsError) { %>
            <div class="warning">Error: Entry already exists</div>
        <% } %>
        <form name="MSPFacilityMappingProviderListForm" method="post" action="MSPFacilityMappingProviderList.jsp">
            <input type="hidden" name="dboperation" value="">
            Clinic: <select id="clinic" name="clinic">
            <option value="" disabled selected hidden>Select a Clinic...</option>
            <%
                for (MSPFacilityMapping mspFacilityMapping : mspFacilityMappingList) {
            %>
            <option value="<%=mspFacilityMapping.getId()%>"><%=Encode.forHtmlContent(mspFacilityMapping.getClinic())%></option>
            <%
                }
            %>
            </select>
            Provider: <select id="providerNoAdd" name="providerNoAdd">
                <option value="" disabled selected hidden>Select a Provider...</option>
                <%
                    for (Provider provider : providerList) {
                %>
                <option value="<%=Encode.forHtmlAttribute(provider.getProviderNo())%>"><%=Encode.forHtmlContent(provider.getFormattedName())%></option>
                <%
                    }
                %>
            </select>
            <input id="addBtn" type="button" class="btn btn-primary" onclick="document.forms['MSPFacilityMappingProviderListForm'].dboperation.value='Save'; document.forms['MSPFacilityMappingProviderListForm'].submit();" name="MSPFacilityMappingProviderListForm" value="Save"/>
        </form>
        <a href="MSPFacilityMappingSettings.jsp" class="btn btn-primary">Back</a>
        <div class="pull-right" for="blackwhite" onclick="return confirmation();">
            List Type:
            <input onchange="changeFilter();" id="blackwhite" type="checkbox" data-on="White" data-off="Black" name="blackwhite" class="categoryToggle" data-toggle="toggle" data-offstyle="primary" data-onstyle="default" <%=(filterPreference != null && filterPreference.getValue() != null && filterPreference.getValue().equals("white")) ? "checked" : ""%>/>
        </div>
        <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <th>Clinic</th>
            <th>Provider</th>
            <th></th>
        <%
            for (MSPFacilityMappingBlackWhite mspFacilityMappingBlackWhite : mspFacilityMappingBlackWhiteList) {
                for (Provider provider : providerList) {
                    if (provider.getProviderNo() != null && mspFacilityMappingBlackWhite.getProviderNo() != null && mspFacilityMappingBlackWhite.getProviderNo().equals(provider.getProviderNo())) {
            %>
                        <tr>
                            <td>
                                <%
                                    String clinicName = clinicNameMap.get(mspFacilityMappingBlackWhite.getClinicId()) != null ? clinicNameMap.get(mspFacilityMappingBlackWhite.getClinicId()) : "Error: Clinic not found";
                                %>
                                <%=Encode.forHtmlContent(clinicName)%>
                            </td>
                            <td>
                                <%=Encode.forHtmlContent(provider.getFormattedName())%>
                            </td>
                            <td>
                                <a href="MSPFacilityMappingProviderList.jsp?dboperation=Remove&providerNo=<%=Encode.forUriComponent(provider.getProviderNo())%>" class="btn btn-primary">Remove</a>
                            </td>
                        </tr>
            <%
                    }
                }
            }
        %>
            </tbody>
        </table>
    </body>
</html:html>