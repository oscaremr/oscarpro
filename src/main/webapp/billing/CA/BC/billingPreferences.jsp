<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib prefix="oscar" uri="/oscarPropertiestag" %>
<%@page import="java.util.*,oscar.util.*"%>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="org.oscarehr.common.dao.PropertyDao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.Property" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.common.model.Provider" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ page import="oscar.oscarClinic.ClinicData" %>
<%@ page import="oscar.oscarBilling.ca.bc.data.BillingPreference" %>
<%@ page import="oscar.oscarBilling.ca.bc.data.BillingPreferencesDAO" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="oscar.oscarBilling.ca.bc.data.BillingFormData" %>
<%@ page import="oscar.OscarProperties" %>
<%
oscar.entities.Provider prov = new oscar.entities.Provider();
oscar.entities.Provider customPayee = new oscar.entities.Provider();
customPayee.setFirstName("Custom");
customPayee.setProviderNo("CUSTOM");
prov.setProviderNo("NONE");
List lst = (List)request.getAttribute("providerList");

lst.add(customPayee);
lst.add(prov);

oscar.entities.Provider noPreferenceProvider = new oscar.entities.Provider();
noPreferenceProvider.setFirstName("Clinic Default");
noPreferenceProvider.setProviderNo("clinicdefault");
List billingProviders = (List)request.getAttribute("billingProviderList");
billingProviders.add(0,noPreferenceProvider);
%>
<html:html>
<head>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<html:base />
<link rel="stylesheet" type="text/css"
	href="../../../oscarEncounter/encounterStyles.css">
<title></title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
	<style type="text/css">
		.payTo{
			border:solid black 1px;
		}
		.secHead {
			font-family: Verdana, Arial, Helvetica, sans-serif;
			font-size: 12px;
			font-weight: bold;
			color: #000000;
			background-color: #FFFFFF;
			border-top: thin none #000000;
			border-right: thin none #000000;
			border-bottom: thin solid #000000;
			border-left: thin none #000000;
		}
		.payeeInfo{
			white-space: pre-line;
			line-height: 13pt;
		}
		
		
		.invoiceFont{
			font-family: tahoma,sans-serif;
			font-size: 12px;
		}
		.tableHeader{
			font-size: x-small;
		}
		.rowSpacing{
			margin-bottom: .5rem;
		}

	</style>
</head>
<body class="BodyStyle" vlink="#0000FF">
<!--  -->
<table class="MainTable" id="scrollNumber1">
	<html:form action="/billing/CA/BC/saveBillingPreferencesAction.do"
		method="POST">
		<html:hidden property="providerNo" />
		<tr class="MainTableTopRow">
			<td class="MainTableTopRowLeftColumn">Billing Preferences</td>
			<td class="MainTableTopRowRightColumn">
			<table class="TopStatusBar">
				<tr>
					<td style="text-align: right"><a
						href="javascript:popupStart(300,400,'Help.jsp')"> <bean:message
						key="global.help" /> </a> | <a
						href="javascript:popupStart(300,400,'About.jsp')"> <bean:message
						key="global.about" /> </a> | <a
						href="javascript:popupStart(300,400,'License.jsp')"> <bean:message
						key="global.license" /> </a></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="MainTableLeftColumn">&nbsp;</td>
			<td class="MainTableRightColumn"><br>
			<label for="referral"> Select Default Referral Type: <html:select
				styleId="referral" property="referral">
				<html:option value="1">Refer To</html:option>
				<html:option value="2">Refer By</html:option>
				<html:option value="3">Neither</html:option>
			</html:select> </label>
		</tr>
		<tr>
			<td class="MainTableLeftColumn">&nbsp;</td>
			<%
				// Check for a global setting that overrides any per-provider setting
				PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
				boolean globalAutoPopulateRefer = false;
				List<Property> propertyList = propertyDao.findGlobalByName("auto_populate_refer");
				if (!propertyList.isEmpty()) {
					for (Property p : propertyList)
					{
						if (p.getValue().equalsIgnoreCase("true")) {
							globalAutoPopulateRefer = true;
						}
					}
				}
			%>
			<td class="MainTableRightColumn"><label>Auto-populate Referring Physician on Billing Form?<html:checkbox property="autoPopulateRefer" disabled="<%=globalAutoPopulateRefer%>"/>
				<% if (globalAutoPopulateRefer) { %>
				<p class="text-warning">Note: The Auto-Populate option above cannot be changed per provider, since it is already <b>enabled</b> for all providers. <br />
					To change this setting, go to <b>Administration - Billing - Billing Settings.</b></p>
				<% } %>
			</label></td>
		</tr>
		<tr>
			<td class="MainTableLeftColumn">&nbsp;</td>
			<td class="MainTableRightColumn"><label for="defaultBillingForm">Select
				Default Billing Form: <html:select property="defaultBillingForm" styleId="defaultBillingForm">
					<html:options collection="billingFormList" property="formCode"
								  labelProperty="description" />
				</html:select> </label></td>
		</tr>
		<tr>
			<td class="MainTableLeftColumn">&nbsp;</td>
			<td class="MainTableRightColumn"><label for="defaultBillingProvider">Select
				Default Billing Provider: <html:select property="defaultBillingProvider" styleId="defaultBillingProvider">
					<html:options collection="billingProviderList" property="providerNo"
								  labelProperty="fullName" />
				</html:select> </label></td>
		</tr>
		<tr>
			<td class="MainTableLeftColumn">&nbsp;</td>
			<td class="MainTableRightColumn"><label for="defaultServiceLocation">Select
				Default Service Location: <html:select property="defaultServiceLocation" styleId="defaultServiceLocation" style="max-width: 300px">
					<html:options collection="serviceLocationList" property="visitType"
								  labelProperty="displayName" />
				</html:select> </label></td>
		</tr>
		<tr>
			<td class="MainTableLeftColumn">&nbsp;</td>
			<td class="MainTableRightColumn"><label for="payeeProviderNo">Select
			Default Payee: <html:select property="payeeProviderNo" styleId="payeeProviderNo" onchange="defaultPayeeSelect()">
				<html:options collection="providerList" property="providerNo"
					labelProperty="fullName" />
			</html:select> </label></td>
		</tr>
		<tr>
			<td class="MainTableLeftColumn">&nbsp;</td>
			<td class="MainTableRightColumn">
				<label for="defaultBillingProvider">Select Display DX2/3 on the Create Invoice page by default: 
					<html:select property="displayDx23" styleId="displayDx23">
						<html:options collection="defaultYesNoList" property="key" labelProperty="value" />
					</html:select>
				</label>
			</td>
		</tr>
		<tr>
			<td class="MainTableLeftColumn">&nbsp;</td>
			<td class="MainTableRightColumn"><label>Use Clinic GST Number?<html:checkbox property="useClinicGstNo" onchange="checkUseClinicGst()"/></label></td>
		</tr>
		<tr>
			<td class="MainTableLeftColumn">&nbsp;</td>
			<td class="MainTableRightColumn"><label>GST Number:<html:text property="gstNo"/></label></td>
		</tr>
		<tr>
			<td class="MainTableLeftColumn">&nbsp;</td>
			<td class="MainTableRightColumn" valign="top"><div><label for="invoicePayeeInfo">Set Your Payee Information:</label></div> <html:textarea rows="4" cols="35" styleId="invoicePayeeInfo" property="invoicePayeeInfo"></html:textarea></td>
		</tr>
		<tr>
			<td class="MainTableLeftColumn">&nbsp;</td>
			<td class="MainTableRightColumn">Display Clinic Information Under Payee: <html:checkbox property="invoicePayeeDisplayClinicInfo"/></td>
		</tr>
		<tr>
			<td class="MainTableLeftColumn">&nbsp;</td>
			<td class="MainTableRightColumn">Example Payee Information:
				<div class="tableHeader rowSpacing">(This is what will display on invoices created for your provider)</div>
				<table class="payTo invoiceFont rowSpacing" width="486 px" border="0">
					<%

						BillingPreferencesDAO billingPreferencesDAO = SpringUtils.getBean(BillingPreferencesDAO.class);
						ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
						LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
						Provider p = loggedInInfo.getLoggedInProvider();
						
						BillingPreference billingPreference = billingPreferencesDAO.getUserBillingPreference(p.getProviderNo());
						
						Provider payeeProvider = providerDao.getProvider(billingPreference != null? "" + billingPreference.getDefaultPayeeNo() : null);

						String payeeInfo;
						if(billingPreference == null || "NONE".equals(billingPreference.getDefaultPayeeNo())){
							payeeInfo = "";
						} else {
							if("CUSTOM".equals(billingPreference.getDefaultPayeeNo())){
								List<Property> propList = propertyDao.findByNameAndProvider("invoice_payee_info", p.getProviderNo());
								payeeInfo = !propList.isEmpty() ? propList.get(0).getValue() : "";
							} else {
								payeeInfo = (payeeProvider != null ? ("Dr. " + payeeProvider.getFirstName() + " " + payeeProvider.getLastName()) : "");
							}
						}
						
						ClinicData clinic = new ClinicData();

						String strPhones = clinic.getClinicDelimPhone();
						if (strPhones == null) {
							strPhones = "";
						}
						String strFaxes = clinic.getClinicDelimFax();
						if (strFaxes == null) {
							strFaxes = "";
						}
						Vector vecPhones = new Vector();
						Vector vecFaxes = new Vector();
						StringTokenizer st = new StringTokenizer(strPhones, "|");
						while (st.hasMoreTokens()) {
							vecPhones.add(st.nextToken());
						}
						st = new StringTokenizer(strFaxes, "|");
						while (st.hasMoreTokens()) {
							vecFaxes.add(st.nextToken());
						}

					%>
					<tr>
						<td align="right" colspan="2"></td>
					</tr>
					<tr class="secHead">
						<td height="14" colspan="2">Please Make Cheque Payable To: </td>
					</tr>
					<% if(!StringUtils.isNullOrEmpty(payeeInfo)) { %>
					<tr>
						<td class="title4 payeeInfo"><%=Encode.forHtml(payeeInfo)%></td>
					</tr>
					<% }
						//Default to true when not found
						if(propertyDao.findByNameAndProvider("invoice_payee_display_clinic", p.getProviderNo()).isEmpty() || propertyDao.isActiveBooleanProperty("invoice_payee_display_clinic", p.getProviderNo())) {

					%>

					<tr>
						<%  SystemPreferences invoiceClinicInfo = SystemPreferencesUtils.findPreferenceByName("invoice_custom_clinic_info");
							if(invoiceClinicInfo == null || StringUtils.isNullOrEmpty(invoiceClinicInfo.getValue())) { %>
						<td class="title4">
							<%=Encode.forHtml(clinic.getClinicName())%>
						</td>
					</tr>
					<tr>
						<td class="address"><%=Encode.forHtml(clinic.getClinicAddress()+", "+clinic.getClinicCity()+", "+clinic.getClinicProvince()+" "+clinic.getClinicPostal())%> </td>
					</tr>
					<tr>
						<td class="address" id="clinicPhone"> Telephone: <%=vecPhones.size() >= 1 ? vecPhones.elementAt(0) : clinic.getClinicPhone()%> </td>
					</tr>
					<tr>
						<td class="address" id="clinicFax"> Fax: <%=vecFaxes.size() >= 1 ? vecFaxes.elementAt(0) : clinic.getClinicFax()%> </td>
						<% } else { %>

						<td class="payeeInfo"><%= Encode.forHtml(invoiceClinicInfo.getValue())%></td>

						<% } %>
					</tr>
					<% } %>
				</table>
			</td>
		</tr>
		<tr>
			<td class="MainTableBottomRowLeftColumn"></td>
			<td class="MainTableBottomRowRightColumn"><html:submit
				property="submit" value="Save" /></td>
		</tr>
	</html:form>
</table>
</body>
<footer>
	<%
		PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
		LoggedInInfo loggedInInfo= LoggedInInfo.getLoggedInInfoFromSession(request);
		Provider p = loggedInInfo.getLoggedInProvider();
		SystemPreferences clinicGstPref = SystemPreferencesUtils.findPreferenceByName("clinic_gst_number");
		String clinicGst = clinicGstPref != null && clinicGstPref.getValue() != null? clinicGstPref.getValue() : "";
		
		boolean useClinicGst = propertyDao.findByNameAndProvider("gst_number", p.getProviderNo()).isEmpty();
		boolean autoPopulateRefer = propertyDao.isActiveBooleanProperty("auto_populate_refer", p.getProviderNo());
		boolean displayDx23 = propertyDao.isActiveBooleanProperty("display_dx23", p.getProviderNo());

	%>
	<script type="text/javascript">
		var clinicGst = '<%= Encode.forJavaScript(clinicGst)%>';
		document.getElementsByName('useClinicGstNo')[0].checked = <%= useClinicGst %>;
		document.getElementsByName('autoPopulateRefer')[0].checked = <%= autoPopulateRefer %>;
		document.getElementsByName('displayDx23')[0].checked = <%= displayDx23 %>;
		
		checkUseClinicGst();
		
		function checkUseClinicGst(){
			if(document.getElementsByName('useClinicGstNo')[0].checked){
				document.getElementsByName('gstNo')[0].value = clinicGst;
				document.getElementsByName('gstNo')[0].disabled = true;
			} else {
				document.getElementsByName('gstNo')[0].disabled = false;
			}
		}

        function defaultPayeeSelect() {
            var invoicePayeeInfoTA = document.getElementById("invoicePayeeInfo");
            var payeeProviderNoSelect = document.getElementById("payeeProviderNo");
            if(payeeProviderNoSelect.value === 'CUSTOM'){
                invoicePayeeInfoTA.disabled = false;
            } else if(payeeProviderNoSelect.value === 'NONE'){
                invoicePayeeInfoTA.disabled = true;
                invoicePayeeInfoTA.value = '';
            } else {
                invoicePayeeInfoTA.disabled = true;
                Array.prototype.forEach.call(payeeProviderNoSelect.options, function (option) {
                    if(option.value === payeeProviderNoSelect.value)
                        invoicePayeeInfoTA.value = 'Dr. ' + option.text;
                });
            }
        }

        defaultPayeeSelect();
    </script>
</footer>
</html:html>
