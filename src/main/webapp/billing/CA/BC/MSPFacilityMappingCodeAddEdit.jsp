<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.dao.MSPFacilityMappingDao" %>
<%@ page import="org.oscarehr.common.model.MSPFacilityMapping" %>
<%@ page import="java.util.List" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="org.oscarehr.common.model.Provider" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.MSPFacilityMappingCodesDao" %>
<%@ page import="org.oscarehr.common.model.MSPFacilityMappingCode" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.oscarehr.common.model.MSPFacilityMappingCodePK" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<jsp:useBean id="dataBean" class="java.util.Properties"/>
<%
    String billingCode = StringUtils.trimToEmpty(request.getParameter("service_code_search"));
    Integer clinic = Integer.parseInt(request.getParameter("clinic") != null ? request.getParameter("clinic") : "0");
    MSPFacilityMappingDao mspFacilityMappingDao = SpringUtils.getBean(MSPFacilityMappingDao.class);
    MSPFacilityMappingCodesDao mspFacilityMappingCodesDao = SpringUtils.getBean(MSPFacilityMappingCodesDao.class);
    boolean alreadyExistsError = false;

    if (StringUtils.isNotEmpty(request.getParameter("dboperation")) && request.getParameter("dboperation").equals("Save") && StringUtils.isNotEmpty(billingCode) && clinic > 0) {
        SystemPreferences systemPreferences = SystemPreferencesUtils.findPreferenceByName("msp_facility_mapping_black_white");
        
        MSPFacilityMappingCode mspFacilityMappingCode = mspFacilityMappingCodesDao.find(clinic, billingCode);
        if (mspFacilityMappingCode == null) {
            mspFacilityMappingCode = new MSPFacilityMappingCode();
            mspFacilityMappingCode.setId (new MSPFacilityMappingCodePK());
            mspFacilityMappingCode.setClinicId(clinic);
            mspFacilityMappingCode.setBillingCode(billingCode);
            mspFacilityMappingCodesDao.persist(mspFacilityMappingCode);
        } else {
            alreadyExistsError = true;
        }
    } else if (StringUtils.isNotEmpty(request.getParameter("dboperation")) && request.getParameter("dboperation").equals("Remove") && StringUtils.isNotEmpty(billingCode) && clinic != 0) {
        mspFacilityMappingCodesDao.removeCode(clinic, billingCode, LoggedInInfo.getLoggedInInfoFromSession(session));
    }
    
    List<MSPFacilityMapping> mspFacilityMappingList = mspFacilityMappingDao.findAll();
    List<MSPFacilityMappingCode> mspFacilityMappingCodeList = mspFacilityMappingCodesDao.findAll();
    boolean editMode = "Edit".equals(request.getParameter("dboperation"));
%>

<html:html locale="true">
    <head>
        <title><bean:message key="admin.admin.MSPFacilityMapping"/></title>
        <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        <link href="<%=request.getContextPath() %>/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="../../../share/yui/css/autocomplete.css"/>
        <link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/js/jquery_css/smoothness/jquery-ui-1.10.2.custom.min.css"  />

        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-ui-1.10.2.custom.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath() %>/js/bootstrap.js"></script>
        <script type="text/javascript" language="JavaScript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/js/billingServiceAutocomplete.js"></script>
    </head>

    <body vlink="#0000FF" class="BodyStyle">
    <h4><bean:message key="admin.admin.MSPFacilityMapping"/></h4>
    <% if (alreadyExistsError) { %>
        <div class="warning">Error: Entry already exists</div>
    <% } %>
    <form name="MSPFacilityCodeAddEditForm" method="post" action="MSPFacilityMappingCodeAddEdit.jsp">
        <input type="hidden" name="dboperation" value="">
        <table id="displaySettingsTable" class="table table-bordered table-striped table-hover table-condensed">
            <tbody>
            <tr>
                <td>
                    Billing Code
                </td>
                <td>
                    <input id="searchService" class="typeahead" style="margin-bottom: 0;" type="text" name="service_code_search" placeholder="Search service codes" maxlength="20" onchange="checkSave(true)" value="<%=Encode.forHtmlAttribute(billingCode)%>" <%=editMode ? "readonly" : ""%>/>
                    <div id="autocomplete_choices" class="autocomplete"></div>
                </td>
            </tr>

            <tr>
                <td>
                    Clinic
                </td>
                <td>
                    <select id="clinic" name="clinic" <%=editMode ? "disabled" : ""%>>
                        <%
                            for (MSPFacilityMapping mspFacilityMapping : mspFacilityMappingList) {
                        %>
                            <option value="<%=mspFacilityMapping.getId()%>" <%=clinic.equals(mspFacilityMapping.getId()) ? "selected" : ""%>><%=Encode.forHtmlContent(mspFacilityMapping.getClinic())%></option>
                        <%
                            }
                        %>
                    </select>
                    <input type="hidden" id="clinicHidden" name="clinicHidden" value="<%=clinic%>">
                </td>
            </tr>
            </tbody>
        </table>

        <a href="MSPFacilityMappingSettings.jsp" class="btn btn-primary">Back</a>
        <input <%=StringUtils.isNotEmpty(billingCode) ? "" : "disabled"%> id="addBtn" type="button" class="btn btn-primary" onclick="document.forms['MSPFacilityCodeAddEditForm'].dboperation.value='Save'; document.forms['MSPFacilityCodeAddEditForm'].submit();" name="saveMSPFacilityMappingCode" value="Save"/>
    </form>
    <table class="table table-bordered table-striped table-hover table-condensed">
        <tbody>
            <th>Clinic</th>
            <th>Billing Code</th>
            <th></th>
            <%
                for (MSPFacilityMappingCode mspFacilityMappingCode : mspFacilityMappingCodeList) {
                    String clinicName = "Error: Clinic not found";
                    for (MSPFacilityMapping mspFacilityMapping : mspFacilityMappingList) {
                        if (mspFacilityMappingCode.getClinicId().equals(mspFacilityMapping.getId())) {
                            clinicName = mspFacilityMapping.getClinic();
                        }
                    }
            %>
            <tr>
                <td>
                    <%=Encode.forHtmlContent(clinicName)%>
                </td>
                <td>
                    <%=mspFacilityMappingCode.getBillingCode()%>
                </td>
                <td>
                    <a href="MSPFacilityMappingCodeAddEdit.jsp?dboperation=Remove&clinic=<%=mspFacilityMappingCode.getClinicId()%>&service_code_search=<%=Encode.forUriComponent(mspFacilityMappingCode.getBillingCode())%>" class="btn btn-primary">Remove</a>
                </td>
            </tr>
            <%
                }
            %>
        </tbody>
    </table>
    <script type="text/javascript">
        jQuery(setupBillingServiceAutocomplete("../../../billing/SearchBillingService.do"));
    </script>
    </body>
</html:html>