<%
    int billingCount = request.getParameter("billingCount") != null ? Integer.parseInt(request.getParameter("billingCount")) : 1;
    String billingFee = request.getParameter("billingFee") != null ? request.getParameter("billingFee") : "";
    String billingUnit = request.getParameter("billingUnit") != null ? request.getParameter("billingUnit") : "";
    String billingDx1 = request.getParameter("billingDx1") != null ? request.getParameter("billingDx1") : "";
    String billingDx2 = request.getParameter("billingDx2") != null ? request.getParameter("billingDx2") : "";
    String billingDx3 = request.getParameter("billingDx3") != null ? request.getParameter("billingDx3") : "";
    Boolean dx23Visible = request.getParameter("dx23Visible") != null ? Boolean.parseBoolean(request.getParameter("dx23Visible")) : false;
%>

<tr id="billing_row_<%=billingCount%>">
    <td class="<%=dx23Visible ? "billingRowFeeAux" : "billingRowFeeCode"%>">
        <div class="billingRowWrapper">
            <input type="text" name="billing_<%=billingCount%>_fee" id="billing_<%=billingCount%>_fee" value="<%=billingFee%>" onkeypress="return grabEnter(event,'searchFeeCode(\'billing_<%=billingCount%>_fee\')');" onblur="checkSelectedCodes()" />
            <a href="javascript:void(0)" onclick="searchFeeCode('billing_<%=billingCount%>_fee');"><img src="../../../oscarEncounter/graphics/edit-find.png" width="15px" height="15px" /></a>
        </div>
    </td>
    <td class="<%=dx23Visible ? "billingRowUnitAux" : "billingRowUnit"%>">
        <input type="text" name="billing_<%=billingCount%>_fee_unit" id="billing_<%=billingCount%>_fee_unit" value="<%=billingUnit%>" style="width: 55%;" />
        <input type="button" value=".5" onclick="jQuery('#billing_<%=billingCount%>_fee_unit').val('0.5');">
    </td>
    <td class="<%=dx23Visible ? "billingRowDx1Aux" : "billingRowDx1"%>">
        <div class="billingRowWrapper">
            <input type="text" name="billing_<%=billingCount%>_fee_dx1" id="billing_<%=billingCount%>_fee_dx1" value="<%=billingDx1%>" onkeypress="return grabEnter(event,'searchDiagnostic(\'billing_<%=billingCount%>_fee_dx1\',<%=billingCount%>);');" />
            <a href="javascript:void(0)" onclick="searchDiagnostic('billing_<%=billingCount%>_fee_dx1',<%=billingCount%>);"><img src="../../../oscarEncounter/graphics/edit-find.png" width="15px" height="15px" /></a>
        </div>
    </td>
    <td class="<%=dx23Visible ? "billingRowDx2Aux" : "billingRowDx2"%>">
        <div class="billingRowWrapper">
            <input type="text" name="billing_<%=billingCount%>_fee_dx2" id="billing_<%=billingCount%>_fee_dx2" value="<%=billingDx2%>" onkeypress="return grabEnter(event,'searchDiagnostic(\'billing_<%=billingCount%>_fee_dx2\',<%=billingCount%>);');" />
            <a href="javascript:void(0)" onclick="searchDiagnostic('billing_<%=billingCount%>_fee_dx2',<%=billingCount%>);"><img src="../../../oscarEncounter/graphics/edit-find.png" width="15px" height="15px" /></a>
        </div>
    </td>
    <td class="<%=dx23Visible ? "billingRowDx3Aux" : "billingRowDx3"%>">
        <div class="billingRowWrapper">
            <input type="text" name="billing_<%=billingCount%>_fee_dx3" id="billing_<%=billingCount%>_fee_dx3" value="<%=billingDx3%>" onkeypress="return grabEnter(event,'searchDiagnostic(\'billing_<%=billingCount%>_fee_dx3\',<%=billingCount%>);');" />
            <a href="javascript:void(0)" onclick="searchDiagnostic('billing_<%=billingCount%>_fee_dx3',<%=billingCount%>);"><img src="../../../oscarEncounter/graphics/edit-find.png" width="15px" height="15px" /></a>
        </div>
    </td>
</tr>
