<div class = "modal fade" tabindex = "-1" role = "dialog" id = "dlgSchedulePwd" style = "display: none;">
  <div class = "modal-dialog" role = "document">
    <div class = "modal-content">
      <div class = "modal-header">
        <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close">
          <span aria-hidden = "true">&times;</span>
        </button>
        <h4 class = "modal-title">Schedule Password Renewal</h4>
      </div>
      <div class = "modal-body" style = "padding: 40px 0;">
        <input type = "checkbox" name = "active"
          <%if (periodDefPwd.getActive()) {
              %> checked <%
          }
          %>>
          <span>Every
          </span>
          <input type = "text" name = "days" style = "width: 70px;" value="<%=periodDefPwd.getExpression()%>"> 
          <span>days to renew Teleplan password
          </span>
      </div>
      <div class = "modal-footer">
        <span class = "error-tips"></span>
        <button type = "button" class = "btn btn-primary">Save</button>
        <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class = "modal fade" tabindex = "-1" role = "dialog" id = "dlgScheduleRemit" style = "display: none;">
  <div class = "modal-dialog" role = "document">
    <div class = "modal-content">
      <div class = "modal-header">
        <button type = "button" class = "close" data-dismiss = "modal" aria-label = "Close">
          <span aria-hidden = "true">&times;</span>
        </button>
        <h4 class = "modal-title">Schedule Remittance Download</h4>
        <div id = "divExpression" style = "display: none;"><%=periodDefRemit.getExpression()%></div>
      </div>
      <div class = "modal-body" style = "padding: 40px 10px 40px 60px;">
        <div class = "clearfix row">
          <div class = "pull-left">Enabled:</div>
          <div class = "pull-left">
            <input type = "checkbox" name = "active"
              <%if (periodDefRemit.getActive()) {%> checked <%}%>>
          </div>
        </div>
        <div class = "clearfix row">
          <div class = "pull-left">Schedule Type:</div>
          <div class = "pull-left">
            <span style = "margin-right: 10px;"><input type = "radio" name = "schedule-type" value = "daily">Daily</span>
            <span style = "margin-right: 10px;"><input type = "radio" name = "schedule-type" value = "weekly">Weekly</span>
            <span><input type = "radio" name = "schedule-type" value = "monthly">Monthly</span>
          </div>
        </div>
        <div class = "clearfix row">
          <div class = "pull-left">Schedule Date:</div>
          <div class = "pull-left">
            <div id = "dailyDateSection" class = "date-section">Every Day</div>
            <div id = "weeklyDateSection" class = "date-section">
              <select style = "width: auto;" name = "dateofweek">
                <option value = "7">Sunday</option>
                <option value = "1">Monday</option>
                <option value = "2">Tuesday</option>
                <option value = "3">Wednesday</option>
                <option value = "4">Thursday</option>
                <option value = "5">Friday</option>
                <option value = "6">Saturday</option>
              </select>
            </div>
            <div id = "monthlyDateSection" class = "date-section">
              <select style = "width: auto;" name = "dateofmonth">
                <%
                for (int i = 1; i < 32; i++) {
                %>
                <option value = "<%=i%>"><%=i%></option>
                <%
                }
                %>
              </select>
              <div class = "tips">* if the selected date does not exist then it will 
                execute on the last day of that month.</div>
            </div>
          </div>
        </div>
        <div class = "clearfix row">
          <div class = "pull-left">Executing Start Time:</div>
          <div class = "pull-left">
            <select style = "width: auto;" name = "time">
              <%
              for (int i = 0; i < 24; i++) {
              %>
              <option value = "<%=i%>"><%=i%>:00</option>
              <%
              }
              %>
            </select>
            <div class = "tips">* The job will be executed within one hour after the start time.</div>
          </div>
        </div>
      </div>
      <div class = "modal-footer">
        <span class = "error-tips"></span>
        <button type = "button" class = "btn btn-primary">Save</button>
        <button type = "button" class = "btn btn-default" data-dismiss = "modal">Close</button>
      </div>
    </div>
  </div>
</div>