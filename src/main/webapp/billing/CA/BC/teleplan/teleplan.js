var WEEK_DEF = {
    2: 'Monday',
    3: 'Tuesday',
    4: 'Wednesday',
    5: 'Thursday',
    6: 'Friday',
    7: 'Saturday',
    1: 'Sunday'
};
$(window.document)
    .ready(
        function () {
            teleplanManager.initScheduleSection();
            $('#btnPwdRenew').add($('.renew-pwd-area a')).on('click', function () {
                $('#dlgSchedulePwd').modal('show');
            });
            $('#btnScheduleRemit').on('click', function () {
                var active = $('#dlgSchedulePwd input[name=active]').prop('checked');
                if (active) {
                    $('#dlgScheduleRemit').modal('show');
                } else {
                    alert('Please do "Schedule Password Renewal" firstly and then do "Schedule Remittance Download"')
                }
            });
            $('#dlgScheduleRemit input[name=schedule-type]').on('change', function () {
                var val = $(this).val()
                teleplanManager.handleScheduleDateArea(val);
            });
            $('#dlgSchedulePwd .btn-primary').on('click', function () {
                var active = $('#dlgSchedulePwd input[name=active]').prop('checked')
                teleplanManager.saveScheduleConfig(active ? 1 : 0, 'teleplan_password_renew');
            });
            $('#dlgScheduleRemit .btn-primary').on('click', function () {
                var active = $('#dlgScheduleRemit input[name=active]').prop('checked')
                teleplanManager.saveScheduleConfig(active ? 1 : 0, 'teleplan_remit_download');
            });
            teleplanManager.parseExpression();
        });
window.teleplanManager = {
    initScheduleSection: function () {
        var isPwdEnabled = ($('#btnPwdRenew').length === 0) && !$('.renew-pwd-area.schedule-detail').is('.disabled')
        if (isPwdEnabled) {
            $('#btnScheduleRemit').removeAttr('disabled');
            $('.schedule-detail.remit').removeClass('disabled');
        } else {
            $('#btnScheduleRemit').attr('disabled', 'true');
            $('.schedule-detail.remit').addClass('disabled');
        }
    },
    saveScheduleConfig: function (active, name) {
        if (name === 'teleplan_password_renew') {
            if (!this.checkPwdRenewForm()) {
                return;
            }
        } else {
            if (!this.checkRemitForm()) {
                return;
            }
        }
        jQuery.ajax({
            url: systemAPIContext + '/billing/CA/BC/ManageTeleplan.do?method=saveScheduleConfig',
            method: 'post',
            data: {
                name: name,
                active: active,
                expression: this.getExpression(name)
            },
            dataType: 'json',
            success: function (data) {
                if (data.code === 'success') {
                    alert('Save successfully.')
                    window.location.reload()
                } else {
                    alert('Save failed. Please try again');
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert('Save failed. Please try again');
            }
        });
    },
    getExpression(name) {
        if (name === 'teleplan_password_renew') {
            return $('#dlgSchedulePwd input[name=days]').val();
        } else {
            var text = '';
            var $dlg = $('#dlgScheduleRemit');
            var type = $('input[name="schedule-type"]:checked', $dlg).val();
            text += type;
            if (type === 'weekly') {
                text += '|' + $('select[name="dateofweek"]', $dlg).val()
            } else if (type === 'monthly') {
                text += '|' + $('select[name="dateofmonth"]', $dlg).val()
            }
            text += '|' + $('select[name="time"]', $dlg).val()
            return text + '|' + this.getRandomMinutes()
        }
    },
    getRandomMinutes() {
        var num = Math.floor((Math.random() * 60))
        while (num === 0) {
            num = Math.floor((Math.random() * 60))
        }
        return num
    },
    checkPwdRenewForm() {
        var $dlg = $('#dlgSchedulePwd');
        var days = $('input[name=days]', $dlg).val();
        $('.error-tips', $dlg).html('');
        if (!/^[0-9]+$/.test(days) || days == 0) {
            $('.error-tips', $dlg).html('Please input valid number');
            return false;
        }
        return true
    },
    checkRemitForm() {
        var $dlg = $('#dlgScheduleRemit');
        var type = $('input[name="schedule-type"]:checked', $dlg).val()
        $('.error-tips', $dlg).html('');
        if (!type) {
            $('.error-tips', $dlg).html('Please select schedule type');
            return false;
        }
        return true
    },
    handleScheduleDateArea(val) {
        $('#dailyDateSection').add('#weeklyDateSection').add('#monthlyDateSection').hide()
        if (val === 'daily') {
            $('#dailyDateSection').show()
        } else if (val === 'weekly') {
            $('#weeklyDateSection').show()
        } else {
            $('#monthlyDateSection').show()
        }
    },
    parseExpression() {
        var text = $('#divExpression').text().trim();
        var $dlg = $('#dlgScheduleRemit');
        var active = $('input[name=active]', $dlg).prop('checked')
        if (text.length > 0) {
            let arrays = text.split('|');
            var type = arrays[0];
            $('input[name="schedule-type"]', $dlg).filter('[value="' + type + '"]').prop('checked', true)
            var time = '';
            var obj = {type: type}
            if (type === 'weekly') {
                var date1 = arrays[1]
                time = arrays[2]
                $('select[name="dateofweek"]', $dlg).val(date1)
                obj.date = date1;
                obj.time = time;
            } else if (type === 'monthly') {
                var date2 = arrays[1]
                time = arrays[2]
                $('select[name="dateofmonth"]', $dlg).val(date2)
                obj.date = date2;
                obj.time = time;
            } else {
                time = arrays[1]
                obj.time = time;
            }
            $('select[name="time"]', $dlg).val(time)
            this.handleScheduleDateArea(type);
            this.showRemitScheduleDetail(obj);
            if (!$('.schedule-detail.remit').is('.disabled')) {
                $('.schedule-detail.remit').addClass(!active ? 'deactivated' : '')
            }
            $('#btnScheduleRemit').hide()
        } else {
            $('#btnScheduleRemit').show()
        }
    },
    showRemitScheduleDetail(obj) {
        var html = '<span>Schedule Type:{type}, </span>'
        html += '<span>Schedule Date:{date}, </span>'
        html += '<span>Executing Start Time:{time}:00</span>'
        html += '<a href="javascript:void(0)">Edit</a>'
        if (obj.type === 'daily') {
            html = html.replace('{date}', 'Every Day');
        } else if (obj.type === 'weekly') {
            html = html.replace('{date}', WEEK_DEF[obj.date]);
        } else {
            html = html.replace('{date}', obj.date);
        }
        html = html.replace('{type}', obj.type);
        html = html.replace('{time}', obj.time);
        $('.schedule-detail.remit').html(html)
        $('.schedule-detail.remit a').on('click', function () {
            $('#dlgScheduleRemit').modal('show');
        });
    }
};