<%--

    Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not , write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

--%>

<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@page import="oscar.util.DateUtils,org.oscarehr.util.SpringUtils, org.oscarehr.util.MiscUtils"%>
<%@page import="java.util.Properties,java.util.Date,java.util.List,java.util.ArrayList,java.math.BigDecimal"%>
<%@page import="org.oscarehr.common.dao.BillingONPaymentDao,org.oscarehr.common.model.BillingONPayment"%>
<%@page import="org.oscarehr.common.dao.BillingServiceDao,org.oscarehr.common.model.BillingService"%>
<%@page import="org.oscarehr.common.dao.ClinicDAO,org.oscarehr.common.model.Clinic"%>
<%@page import="org.oscarehr.PMmodule.dao.ProviderDao,org.oscarehr.common.model.Provider"%>
<%@page import="org.oscarehr.common.dao.DemographicDao,org.oscarehr.common.model.Demographic"%>
<%@page import="org.oscarehr.common.dao.BillingONExtDao,org.oscarehr.common.model.BillingONExt"%>
<%@page import="org.oscarehr.common.dao.BillingONCHeader1Dao,org.oscarehr.common.model.BillingONCHeader1"%>
<%@page import="org.oscarehr.common.model.BillingONItem, org.oscarehr.common.service.BillingONService"%> 
<%@page import="org.oscarehr.util.SpringUtils"%>
<%@page import="org.oscarehr.util.LocaleUtils"%>
<%@page import="org.oscarehr.common.model.Demographic"%>
<%@page import="org.oscarehr.common.dao.DemographicDao"%>
<%@page import="oscar.OscarProperties" %>
<%@page import="org.oscarehr.billing.CA.ON.util.DisplayInvoiceLogo" %>
<%@page import="org.oscarehr.common.dao.SiteDao" %>
<%@page import="org.oscarehr.common.model.Site" %>
<%@page import="oscar.oscarBilling.ca.on.pageUtil.Billing3rdPartPrep" %>
<%@page import="oscar.oscarBilling.ca.shared.administration.GstControlAction" %>
<%@ page import="java.math.MathContext" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.common.dao.PropertyDao" %>
<%@ page import="org.oscarehr.common.model.Property" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="org.oscarehr.managers.BillingManager" %>
<%@ page import="org.oscarehr.billing.CA.dao.GstControlDao" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<%
    PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
    
    String invoiceNoStr = request.getParameter("billingNo");
    Integer invoiceNo = null;
    try {
        invoiceNo = Integer.parseInt(invoiceNoStr);
    } catch(NumberFormatException e) {
        invoiceNoStr = "";
        MiscUtils.getLogger().warn("Invalid Invoice No.");
    }
    
Billing3rdPartPrep privateObj = new Billing3rdPartPrep();
Properties propClinic = privateObj.getLocalClinicAddr();
Properties prop3rdPart = privateObj.get3rdPartBillProp(invoiceNoStr);
Properties prop3rdPayMethod = privateObj.get3rdPayMethod();
Properties propGst = privateObj.getGst(invoiceNoStr);
OscarProperties oscarProp = OscarProperties.getInstance();
boolean isMulitSites = oscarProp.getBooleanProperty("multisites", "on");


    
    BillingONCHeader1Dao bCh1Dao = (BillingONCHeader1Dao) SpringUtils.getBean("billingONCHeader1Dao");
    BillingONCHeader1 bCh1 = null;
    
    if (invoiceNo != null) 
        bCh1 = bCh1Dao.find(invoiceNo);
    
    
    String billTo = ""; 
    String remitTo = "";
    BigDecimal totalOwed = new BigDecimal("0.00");
    BigDecimal paidTotal = new BigDecimal("0.00");
    BigDecimal refundTotal = new BigDecimal("0.00");
    BigDecimal balanceOwing = new BigDecimal("0.00");
    List<BillingONItem> billingItems = new ArrayList<BillingONItem>();
    Demographic demo = null; 
    String providerFormattedName = "";
    String invoiceComment = "";
    String invoiceRefNum = "";
    String billingDateStr ="";
    String dueDateStr = "";
    String paymentDescription = "";
    String payee = "";
    
    ClinicDAO clinicDao = (ClinicDAO) SpringUtils.getBean("clinicDAO");
    Clinic clinic = clinicDao.getClinic();              
    oscar.OscarProperties props = oscar.OscarProperties.getInstance();

	Properties gstProp = new Properties();
	GstControlAction db = new GstControlAction();
	gstProp = db.readDatabase();

	String percent = gstProp.getProperty("gstPercent", "");

    String paymentDate = "";

    boolean removePatientDetailInThirdPartySystemPreference = SystemPreferencesUtils
            .isReadBooleanPreferenceWithDefault("remove_patient_detail_in_third_party", false);
    Boolean removePatientDetailInThirdPartyInvoice = removePatientDetailInThirdPartySystemPreference
        && bCh1.getPayProgram().equalsIgnoreCase("PAT");
	
	String filePath = DisplayInvoiceLogo.getLogoImgAbsPath();
	boolean isLogoImgExisted = true;
	if (filePath.isEmpty()) {
		isLogoImgExisted = false;
	}

    if (bCh1 != null) {
        BillingONExtDao billExtDao = (BillingONExtDao) SpringUtils.getBean("billingONExtDao");
        BillingONPaymentDao billPaymentDao = (BillingONPaymentDao) SpringUtils.getBean("billingONPaymentDao");
        DemographicDao demoDAO = (DemographicDao)SpringUtils.getBean("demographicDao");
        ProviderDao providerDao = (ProviderDao) SpringUtils.getBean("providerDao");
        
        BillingONExt billExtPayee = billExtDao.getPayee(bCh1);
        if (billExtPayee != null) {
            payee = billExtPayee.getValue();
        }
        
        billingDateStr = DateUtils.formatDate(bCh1.getBillingDate(),request.getLocale());
        invoiceRefNum = bCh1.getRefNum();
        
        BillingONService billingONService = (BillingONService) SpringUtils.getBean("billingONService");
        billingItems = billingONService.getNonDeletedInvoices(bCh1.getId());

        invoiceComment = bCh1.getComment(); 
        
        totalOwed = bCh1.getTotal();

        List<BillingONPayment> paymentRecords = billPaymentDao.find3rdPartyPayRecordsByBill(bCh1);
        paidTotal = BillingONPaymentDao.calculatePaymentTotal(paymentRecords);
        refundTotal = BillingONPaymentDao.calculateRefundTotal(paymentRecords);
        balanceOwing = billingONService.calculateBalanceOwing(bCh1.getId());

        demo = demoDAO.getDemographic(bCh1.getDemographicNo().toString());
        
        Provider provider = providerDao.getProvider(bCh1.getProviderNo());
        providerFormattedName = provider.getFormattedName();
           
        String clinicBillingPhone = props.getProperty("clinic_billing_phone","");
        if (clinicBillingPhone.isEmpty()) {
            clinicBillingPhone = clinic.getClinicDelimPhone();
        }
                   
        BillingONExt billToBillExt = billExtDao.getBillTo(bCh1);

        String useDemoClinicInfoOnInvoice = props.getProperty("useDemoClinicInfoOnInvoice","");
        if (!useDemoClinicInfoOnInvoice.isEmpty() && useDemoClinicInfoOnInvoice.equals("true")) {

            BillingONExt useBillToExt = billExtDao.getUseBillTo(bCh1);

           //If we have stored 3rd Party "Bill To:" Information, then use it
            if (billToBillExt != null && billToBillExt.getValue() != null && !billToBillExt.getValue().isEmpty())
            {
                billTo = billToBillExt.getValue();                                    
            }
            //If someone actually wants to print the bill with the "Bill To:" section left blank, this allows them to do that.
            else if (useBillToExt != null && useBillToExt.getValue().equals("on"))
            {
                billTo = "";
            }
            //The purpose of property "useDemoClinicInfoOnInvoice" is so that if we don't have any 3rd Party info for this invoice, we'll default to using the demographic's contact information as the "Bill To:" content
            else 
            {
                StringBuilder buildBillTo = new StringBuilder();
                buildBillTo.append(demo.getFirstName()).append(" ").append(demo.getLastName()).append("\n")
                        .append(demo.getAddress()).append("\n")
                        .append(demo.getCity()).append(",").append(demo.getProvince()).append("\n")
                        .append(demo.getPostal()).append("\n\n")                       
                        .append("\n\n\n\n\n")
                        .append(LocaleUtils.getMessage(request.getLocale(),"billing.billing3rdInv.chartNo"))
                        .append(": ")
                        .append(demo.getChartNo());
                billTo = buildBillTo.toString();
            }
            
            StringBuilder buildRemitTo = new StringBuilder();
            if (StringUtils.trimToNull(clinic.getClinicName()) != null)  {
                buildRemitTo.append(clinic.getClinicName()).append("\n");
            }
            if (StringUtils.trimToNull(clinic.getClinicAddress()) != null) { 
                buildRemitTo.append(clinic.getClinicAddress()).append("\n");
            }
            if (StringUtils.trimToNull(clinic.getClinicCity()) != null
                    || StringUtils.trimToNull(clinic.getClinicProvince()) != null) {
                buildRemitTo.append(clinic.getClinicCity()).append(StringUtils.trimToNull(clinic.getClinicCity())
                        != null ? "," : "").append(clinic.getClinicProvince()).append("\n");
            }
            if (StringUtils.trimToNull(clinic.getClinicPostal()) != null) { 
                buildRemitTo.append(clinic.getClinicPostal()).append("\n");
            }
            if (StringUtils.trimToNull(clinicBillingPhone) != null) { 
                buildRemitTo.append("Ph:").append(clinicBillingPhone).append("\n");
            }
            remitTo = buildRemitTo.toString();
        } else {
            if (billToBillExt != null)
                billTo = billToBillExt.getValue();

            BillingONExt remitToBillExt = billExtDao.getRemitTo(bCh1);

            if (remitToBillExt != null)
                remitTo = remitToBillExt.getValue();
        }

        //if removePatientDetailInThirdPartyInvoice is true, remove the Tel value
        if(removePatientDetailInThirdPartyInvoice && billTo != null && billTo.contains("Tel:"))
        {
            billTo = billTo.substring(0,billTo.indexOf("Tel:"));
        }

        if (props.hasProperty("invoice_due_date")) {
            BillingONExt dueDateExt = billExtDao.getDueDate(bCh1);
            if (dueDateExt != null) {
                dueDateStr = dueDateExt.getValue();
            } else {
                Integer numDaysTilDue = Integer.parseInt(props.getProperty("invoice_due_date", "0"));
                Date serviceDate = bCh1.getBillingDate();
                dueDateStr = DateUtils.sumDate(serviceDate, numDaysTilDue, request.getLocale());
            }            
        }
        
        List<BillingONExt> payMethod = billExtDao.findByBillingNoAndKey(bCh1.getId(), "payMethod");
        if( !payMethod.isEmpty() && StringUtils.isNotEmpty(payMethod.get(0).getValue()) ) {
        	paymentDescription = billExtDao.getPayMethodDesc(payMethod.get(0));
        }

        
        
        if (balanceOwing.compareTo(BigDecimal.ZERO) == 0 && paymentRecords.size() > 0) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            paymentDate = simpleDateFormat.format(paymentRecords.get(paymentRecords.size() - 1).getPaymentDate());
        }
    }
   
    boolean showInvoiceComment = request.getParameter("print_invoice_comment") == null || Boolean.parseBoolean(request.getParameter("print_invoice_comment")); // print by default or by parameter
    Boolean showThirdPartyPaymentDate = Boolean.parseBoolean(props.getProperty("show_third_party_payment_date", "false"));
    boolean displayPatientDob = propertyDao.isActiveBooleanProperty("display_patient_dob_on_3rd_party_invoices", true);
    boolean displayPatientHin = propertyDao.isActiveBooleanProperty("display_patient_hin_on_3rd_party_invoices", true);
	
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <style type="text/css" media="print">
        .doNotPrint {
            display: none;
        }
    </style>
    <style type="text/css" media="">
        pre {
            font-family: inherit;
        }
        .titleBar {
            background-color: gray;  
            padding-top: .5em;
            padding-bottom: .5em;
            padding-left: .5em;
        }
    </style>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.js"></script>
    <script>
	    jQuery.noConflict();
    </script>
<script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
<script type="text/javascript">
	if (opener != null && (opener.location.href.indexOf("providercontrol.jsp") != -1 || opener.location.href.indexOf("appointmentprovideradminday.jsp") != -1)) {
		self.opener.refresh();
	}
	
    function submitForm(methodName) {
        if (methodName=="email"){
            document.forms[0].method.value="sendEmail";
        } else if (methodName=="print") {            
            document.forms[0].method.value="getPrintPDF";
        }
        document.forms[0].submit();
    }
</script>
<title>Billing Invoice</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.js"></script>
   <script>
     jQuery.noConflict();
   </script>
<oscar:customInterface section="invoice"/>
</head>
<body>
    <form action="<%=request.getContextPath()%>/BillingInvoice.do"> 
        <input type="hidden" name="method" value=""/>
        <input type="hidden" name="invoiceNo" id="invoiceNo" value="<%=invoiceNoStr%>"/>
        <div class="doNotPrint">
            <div class="titleBar">
                <input type="button" name="printInvoice" value="<bean:message key="billing.billing3rdInv.printPDF"/>" onClick="submitForm('print')"/>
                <input type="button" name="printHtml" value="Print" onclick="window.print();">
                <input type="button" name="emailInvoice" value="<bean:message key="billing.billing3rdInv.email"/>" onClick="submitForm('email')"/>
            </div>
        </div>
    </form>
	<table width="100%" border="0">
		<tr>
			<td>
			
			<%if (isMulitSites) {
				// get site info by siteName
				SiteDao siteDao = (SiteDao)SpringUtils.getBean(SiteDao.class);
				Site site = siteDao.findByName(bCh1.getClinic());
				if (site != null) {
					if (site.getSiteLogoId() != null && site.getSiteLogoId() > 0) {
					%>
						<img src="<%=request.getContextPath() %>/dms/ManageDocument.do?method=display&doc_no=<%=site.getSiteLogoId() %>" />
					<%
					} else {
					%>
						<% if (StringUtils.isNotBlank(site.getFullName()) && StringUtils.isNotBlank(site.getAddress())
								&& StringUtils.isNotBlank(site.getCity()) && StringUtils.isNotBlank(site.getPostal())
								&& StringUtils.isNotBlank(site.getPhone())) { %>
							<% if (StringUtils.isNotBlank(site.getFullName())) { %>
								<b><%= site.getFullName() %></b><br />
							<% } %>
							<% if (StringUtils.isNotBlank(site.getAddress())) { %>
								<%= site.getAddress() %><br />
							<% } %>
							<% if (StringUtils.isNotBlank(site.getCity())) { %>
								<%= site.getCity() %>,
							<% } %>
							<% if (StringUtils.isNotBlank(site.getProvince())) { %>
								<%= site.getProvince() %>
							<% } %>
							<% if (StringUtils.isNotBlank(site.getPostal())) { %>
								<%= site.getPostal() %><br />
							<% } %>
							<% if (StringUtils.isNotBlank(site.getPhone())) { %>
								Tel.: <%= site.getPhone() %><br />
							<% } %>
						<% } %>
				  <%} %>
			  <%} else { %>
			  	<b><%=propClinic.getProperty("clinic_name", "") %></b><br />
				<%=propClinic.getProperty("clinic_address", "") %><br />
				<%=propClinic.getProperty("clinic_city", "") %>, <%=propClinic.getProperty("clinic_province", "") %><br />
				<%=propClinic.getProperty("clinic_postal", "") %><br />
				Tel.: <%=propClinic.getProperty("clinic_phone", "") %><br />
			  <%} %>
			<%} else if (isLogoImgExisted) {%>
				<img src="<%=request.getContextPath() %>/billing/ca/on/DisplayInvoiceLogo.do" />
			<%} else { %>	
				<% if (StringUtils.trimToNull(propClinic.getProperty("clinic_name", "")) != null) { %>
					<b><%= propClinic.getProperty("clinic_name", "") %></b><br/>
				<% } %>
				<% if (StringUtils.trimToNull(propClinic.getProperty("clinic_address", "")) != null) { %>
					<%= propClinic.getProperty("clinic_address", "") %><br/>
				<% } %>
				<% if (StringUtils.trimToNull(propClinic.getProperty("clinic_city", "")) != null
						|| StringUtils.trimToNull(propClinic.getProperty("clinic_province", "")) != null) { %>
					<%= propClinic.getProperty("clinic_city", "") %>
					<%= StringUtils.trimToNull(propClinic.getProperty("clinic_city", "")) != null ? "," : "" %>
					<%= propClinic.getProperty("clinic_province", "") %><br/>
				<% } %>
				<% if (StringUtils.trimToNull(propClinic.getProperty("clinic_postal", "")) != null) { %>
					<%= propClinic.getProperty("clinic_postal", "") %><br/>
				<% } %>
				<% if (StringUtils.trimToNull(propClinic.getProperty("clinic_phone", "")) != null) { %>
					Tel.: <%= propClinic.getProperty("clinic_phone", "") %><br />
				<% } %>
			<%} %>
			</td>
			<td align="right" valign="top"><font size="+2"><b>Invoice
			- <%=invoiceNoStr %></b></font><br />
                <% if (!removePatientDetailInThirdPartyInvoice) { %>
			Print Date:<%=DateUtils.sumDate("yyyy-MM-dd HH:mm","0") %><br/>
                        <% if (props.hasProperty("invoice_due_date")) { %>
                          <b><bean:message key="oscar.billing.CA.ON.3rdpartyinvoice.dueDate"/>:</b><%=dueDateStr%>
                        <% }
                        } else { %>
                Pt ID Number: <%=demo.getDemographicNo()%>
                <% } %>
                        </td>
		</tr>
	</table>

<hr>
<table width="100%" border="0">
	<tr>
        <td width="50%" valign="top"><b>Bill To</b><br />
		<pre><%=Encode.forHtml(billTo)%>
</pre></td>
        <td valign="top"><b>Remit To</b><br />
		<pre><%=Encode.forHtml(remitTo)%>
</pre></td>
	</tr>
</table>

<oscar:customInterface section="billingInvoice"/>
    <% if (!removePatientDetailInThirdPartyInvoice) { %>
<table width="100%" border="0">
	<tr>
            <td id="ptName">Patient: <%=(bCh1 != null)? Encode.forHtml(bCh1.getDemographicName()):"N/A" %></td>
            <td id="ptDemoNo"> (<%=(bCh1 != null)?bCh1.getDemographicNo():"N/A" %>)</td>
            <td id="ptGender">
                <%
                    if (demo != null) {
                      if (StringUtils.equals(demo.getSex(), "M")) {
                        %>Male<%
                      } else if (StringUtils.equals(demo.getSex(), "F")) {
                        %>Female<%
                      } else if (StringUtils.equals(demo.getSex(), "T")) {
                        %>Transgender<%
                      } else if (StringUtils.equals(demo.getSex(), "O")) {
                        %>Other<%
                      } else if (StringUtils.equals(demo.getSex(), "U")) {
                        %>Undefined<%
                      } else {
                        %>N/A<%
                      }
                    }
                %>
            </td>
            <td id="ptDOB">
                <% if (displayPatientDob) { %>
                DOB: <%=(bCh1 != null)?bCh1.getDob():"N/A" %>
                <% } %>
            </td>
        </tr>
        <tr>    
		<td id="ptHin">
            <% if (displayPatientHin) { %>
                   Insurance No: <%=(demo!=null)?demo.getHin():"N/A"%>
            <% } %>
        </td>
    </tr>
</table>
    <% } %>

        <hr>

    <% if (showInvoiceComment) { %>
        <table width="100%" border="0">
            <tr>
                <td><%=Encode.forHtmlContent(invoiceComment)%></td>
            </tr>
        </table>
    <% } %>

        <table width="100%" border="0">
            <tr>
                <th>Service Date</th>
                <% if (showThirdPartyPaymentDate) { %>
                <th>Payment Date</th>
                <% } %>
                <th>Practitioner</th>
                <th>Payee</th>
                <th>Ref. Doctor</th>
            </tr>
            <tr align="center">
                <td><%=billingDateStr%></td>
                <% if (showThirdPartyPaymentDate) { %>
                <td><%=paymentDate%></td>
                <% } %>
                <td><%=Encode.forHtml(providerFormattedName)%></td>

                <% Properties prop = oscar.OscarProperties.getInstance();
                   if (StringUtils.isEmpty(payee)) {
                       payee = prop.getProperty("PAYEE", "");
                       payee = payee.trim();
                   }
                   if( payee.length() > 0 ) {
                %>
                <td><%=payee%></td>
                <% } else { %>
                <td><%=Encode.forHtml(providerFormattedName)%></td>
                <% } %>
                <td><%=invoiceRefNum%></td>
            </tr>
        </table>

        <hr />

        <table width="100%" border="0">
            <tr>
                <th>Item #:</th>
                <th>Description</th>
                <th>Service Code</th>
                <th>Qty</th>
                <th>Dx</th>
                <th>Individual Total</th>
                <th>Total HST</th>
                <th>Total</th>
            </tr>
            <%
            BillingServiceDao billingServiceDao = (BillingServiceDao) SpringUtils.getBean("billingServiceDao");
            GstControlDao gstControlDao = (GstControlDao) SpringUtils.getBean("gstControlDao");
            BigDecimal hst = gstControlDao.find(1).getGstPercent().divide(new BigDecimal(100)).add(new BigDecimal(1));
            
            for(BillingONItem billItem : billingItems) { 	
                BillingService bs = null;
                String serviceDesc = "N/A";
                BigDecimal fee = new BigDecimal(billItem.getFee());
                BigDecimal hstAmount = new BigDecimal("0.00").setScale(2, BigDecimal.ROUND_HALF_UP);
                BigDecimal serviceCount = new BigDecimal(billItem.getServiceCount());
                BigDecimal value = new BigDecimal(0);
                if (billItem.getServiceCode().startsWith("_"))
                    bs = billingServiceDao.searchPrivateBillingCode(billItem.getServiceCode(),billItem.getServiceDate());
                else
                    bs = billingServiceDao.searchBillingCode(billItem.getServiceCode(),"ON",billItem.getServiceDate());
                 
                if (bs != null) {
                    serviceDesc = bs.getDescription();
                    BigDecimal totalWithoutHst = fee.divide(hst, 2, BigDecimal.ROUND_HALF_UP);
                    if (bs.getGstFlag() && bs.getPercentage()!=null && fee != bCh1.getTotal()) {
                        // Calculate billing hst per item as (fee - (value*serviceCount)) to
                        // maintain old tax values charged.
                        value = new BigDecimal(bs.getValue());
                        // assign value to totalWithoutHst if service code amount was manually changed
                        if (value.compareTo(totalWithoutHst) != 0) {
                          value = totalWithoutHst;
                          hstAmount = fee.subtract(totalWithoutHst);
                        } else {
                          hstAmount = fee.subtract(value.multiply(serviceCount));
                        }
                    } else{
                        // Else, no hst on item, set value as (fee/serviceCount)
                        value = fee.divide(serviceCount, 2, BigDecimal.ROUND_HALF_UP);
                    }
                }    
             %>
            <tr align="center">
                <td><%=billItem.getId() %></td>
                <td><%=serviceDesc%></td>
                <td><%=billItem.getServiceCode()%></td>
                <td><%=billItem.getServiceCount()%></td>
                <td><%=billItem.getDx()%></td>
                <td><%=value.toString()%></td>
                <td><%=hstAmount.toString()%></td>
                <td align="right"><%=billItem.getFee()%></td>
            </tr>
            <% } %>
        </table>

        <hr />
<% 
BigDecimal bdBal = bCh1.getTotal().setScale(2, BigDecimal.ROUND_HALF_UP);
BigDecimal bdPay = new BigDecimal(prop3rdPart.getProperty("payment","0.00")).setScale(2, BigDecimal.ROUND_HALF_UP);
BigDecimal bdDis = new BigDecimal(prop3rdPart.getProperty("discount","0.00")).setScale(2, BigDecimal.ROUND_HALF_UP);
BigDecimal bdRef = new BigDecimal(prop3rdPart.getProperty("refund","0.00")).setScale(2, BigDecimal.ROUND_HALF_UP);
BigDecimal bdCre = new BigDecimal(prop3rdPart.getProperty("credit","0.00")).setScale(2, BigDecimal.ROUND_HALF_UP);
//bdBal = bdPay.subtract(bdBal);
bdBal = bdBal.subtract(bdPay).subtract(bdDis).add(bdCre);
//BigDecimal bdGst = new BigDecimal(propGst.getProperty("gst", "")).setScale(2, BigDecimal.ROUND_HALF_UP);
%>
<table width="100%" border="0">

	<tr align="right">
		<td width="86%">Total:</td>
		<td><%=bCh1.getTotal()%></td>
	</tr>
	<tr align="right">
		<td>Payments:</td>
		<td><%=prop3rdPart.getProperty("payment","0.00") %></td>
	</tr>
	<tr align="right">
		<td>Discounts:</td>
		<td><%=prop3rdPart.getProperty("discount","0.00") %></td>
	</tr>
	<tr align="right">
		<td>Overpayment:</td>
		<td><%=prop3rdPart.getProperty("credit","0.00") %></td>
	</tr>
	<tr align="right">
		<td>Refund / Write off:</td>
		<td><%=prop3rdPart.getProperty("refund","0.00") %></td>
	</tr>

	<tr align="right">
		<td><b>Balance:</b></td>
		<td><%=bdBal %></td>
	</tr>
	<tr align="right">
		<td><%=!paymentDescription.equals("") ? "(" + paymentDescription + ")" : "" %></td>
		<td></td>
	</tr>
</table>
    <%
        Property enableThirdPartyFooter = propertyDao.checkByName("enable_3rd_party_billing_footer");

        if (enableThirdPartyFooter != null && Boolean.parseBoolean(enableThirdPartyFooter.getValue())) {
            BillingManager billingManager = SpringUtils.getBean(BillingManager.class);
            String text = billingManager.getThirdPartyFooterText(demo != null ? demo.getDemographicNo() : -1);
    %>

    <div style="white-space: pre;"><%=text%></div>

    <% } %>

</body>
</html>
