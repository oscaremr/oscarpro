<%--

    Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

--%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ page import="java.util.*,java.sql.*,oscar.util.*,oscar.oscarBilling.ca.on.pageUtil.*,oscar.oscarBilling.ca.on.data.*,oscar.oscarProvider.data.*,java.math.* ,oscar.oscarBilling.ca.on.administration.*"%>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@ page import="org.springframework.web.context.WebApplicationContext"%>

<%@ page import="org.oscarehr.common.model.*,org.oscarehr.common.dao.*"%>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="oscar.log.LogAction" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="oscar.log.LogConst" %>
<%@ page import="org.oscarehr.casemgmt.model.CaseManagementNote" %>
<%@ page import="java.util.Date" %>
<%@ page import="org.oscarehr.managers.NoteManager" %>
<%@ page import="org.oscarehr.PMmodule.service.ProgramManager" %>
<%@ page import="org.oscarehr.casemgmt.service.CaseManagementManager" %>
<%@ page import="org.oscarehr.PMmodule.service.AdmissionManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%
	WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
    UserPropertyDAO userPropertyDAO = (UserPropertyDAO) ctx.getBean("UserPropertyDAO");
    BillingONCHeader1Dao cheader1Dao = (BillingONCHeader1Dao) SpringUtils.getBean("billingONCHeader1Dao");
    BillingONExtDao extDao = (BillingONExtDao) SpringUtils.getBean("billingONExtDao");
	boolean billNoteEnabled = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("bill_note_enabled", false);
    		
    		
	if (session.getAttribute("user") == null) {
		response.sendRedirect("../../../logout.jsp");
	}

	//String user_no = (String) session.getAttribute("user");
	String apptNo = "null".equals(request.getParameter("appointment_no")) || request.getParameter("appointment_no") == null ? "" : request.getParameter("appointment_no");

	if (request.getParameter("submit") != null && "Back to Edit".equals(request.getParameter("button"))) { %>
		<jsp:forward page="billingON.jsp" />
<%	}

	// save the billing if needed
	boolean ret = false;
	if (request.getParameter("submit") != null
			&& ("Settle & Print Invoice".equals(request.getParameter("submit"))
			    || "Save & Print Invoice".equals(request.getParameter("submit"))
			    || "Save".equals(request.getParameter("submit"))
			    || "Save and Back".equals(request.getParameter("submit"))
			    || "Save & Add Another Bill".equals(request.getParameter("submit")))) {
		BillingSavePrep bObj = new BillingSavePrep();
		Vector vecObj = bObj.getBillingClaimObj(request);
		ret = bObj.addABillingRecord(vecObj);
		if(request.getParameter("xml_billtype").substring(0,3).matches(BillingDataHlp.BILLINGMATCHSTRING_3RDPARTY)) {
			bObj.addPrivateBillExtRecord(request, vecObj);
		} else {
			// add transaction log here for ohip
			bObj.addOhipInvoiceTrans(vecObj);
		}
		int billingNo = bObj.getBillingId();
		String demographic_no=null;	
		String time="";
		String value="";
		value=request.getParameter("payeename");		
        if(value.trim()=="")
        {
        	value=request.getParameter("payeename1");
        }

		if (billNoteEnabled) {
			CaseManagementNote caseManagementNote = new CaseManagementNote();
			CaseManagementManager caseManagementManager = (CaseManagementManager) SpringUtils.getBean(
			    "caseManagementManager");
			BillingClaimHeader1Data billingClaimHeader1Data = (BillingClaimHeader1Data) vecObj.get(0);
			caseManagementNote.setDemographic_no(billingClaimHeader1Data.getDemographic_no());
			caseManagementNote.setProviderNo(billingClaimHeader1Data.getProviderNo());
			caseManagementNote.setSigned(true);
			Date date = new Date();
			caseManagementNote.setObservation_date(date);
			caseManagementNote.setUpdate_date(date);
			caseManagementNote.setSigning_provider_no(billingClaimHeader1Data.getProvider_no());
			NoteManager noteManager = (NoteManager) SpringUtils.getBean("noteManager");
			caseManagementNote.setProgram_no(noteManager.getProgram(LoggedInInfo.getLoggedInInfoFromSession(
			    request), billingClaimHeader1Data.getProviderNo()));
			//generate header
			SimpleDateFormat dt = new SimpleDateFormat("dd-MMM-yyyy", request.getLocale());
			String header = "[" + dt.format(new Date()) + " .: Billed]";
			String note = header;
			List<BillingItemData> billingItemDataList = (List<BillingItemData>) vecObj.get(1);
			JdbcBillingReviewImpl dbObj = new JdbcBillingReviewImpl();
			if (billingItemDataList != null && billingItemDataList.size() > 0) {
				for (BillingItemData billingItemData : billingItemDataList) {
					note += "\n" + billingItemData.getService_code() + " Diagnostic Code:" + billingItemData.getDx();
					String desc = dbObj.getCodeDescription( billingItemData.getService_code(),
					    billingClaimHeader1Data.getBilling_date());
					if (desc != null) {
						note += " Description:" + desc;
					}
				}
			}
			caseManagementNote.setNote(note);
			caseManagementNote.setHistory(note);
			Provider providerTemp = LoggedInInfo.getLoggedInInfoFromSession(request)
					.getLoggedInProvider();
			String userName = providerTemp != null ? providerTemp.getFullName() : "";
			caseManagementManager.saveCaseManagementNote(
					LoggedInInfo.getLoggedInInfoFromSession(request),
					caseManagementNote, new ArrayList(), null, null, false, request.getLocale(),
					new Date(), null,
					userName, (String) session.getAttribute("user"),
					LoggedInInfo.obtainClientIpAddress(request), null);

		}

        BillingONCHeader1 billing = cheader1Dao.find(billingNo);
        if(billing!=null) {
        	demographic_no = billing.getDemographicNo().toString();
        	//time = billing.getTimestamp();
        	
        	BillingONExt ext = new BillingONExt();
        	ext.setBillingNo(billingNo);
        	ext.setDemographicNo(billing.getDemographicNo());
        	ext.setKeyVal("payee");
        	ext.setValue(value);
        	ext.setDateTime(billing.getTimestamp());
        	extDao.persist(ext);
        	
        }
        
		boolean printInvoiceComment = Boolean.parseBoolean(request.getParameter("print_invoice_comment"));

		// update appt and close the page
		if (ret) {
            String logData = "";
			if (apptNo != null && apptNo.length() > 0 && !apptNo.equals("0")) {
				String apptCurStatus = bObj.getApptStatus(apptNo);
				oscar.appt.ApptStatusData as = new oscar.appt.ApptStatusData();
				String billStatus = as.billStatus(apptCurStatus);
				bObj.updateApptStatus(apptNo, billStatus, (String)session.getAttribute("user"));
				logData += "appointment_no=" + apptNo;
			}
			LogAction.addLog(LoggedInInfo.getLoggedInInfoFromSession(request), LogConst.ADD, LogConst.CON_BILL,
					"billingId=" + bObj.getBillingId(), demographic_no, logData);
				//if you are editing previous billing, the previous billing should be deleted(flag "D") after edit (insert a new billing)�      
		%>
			
			<jsp:include page="billingDeleteWithBillNo.jsp"/><!-- TODO: look at handling of actions from different submits -->
			<!-- let billingDeleteWithBillNo.jsp handle messages at this point to prevent duplicate or contradicting msgs-->  
			
		<% if (request.getParameter("submit") != null && "Save & Add Another Bill".equals(request.getParameter("submit"))) { %>
<script LANGUAGE="JavaScript">
					self.opener.refresh();  
					self.location.href="<%=request.getParameter("url_back")%>";
				    </script>
		<% } %>
		<% if ("Save".equals(request.getParameter("submit"))) { %>
		<script language="JavaScript">
			// refresh `opener` window only if it is browser regular page, not another popup window
			if (self.opener && self.opener.menubar && self.opener.menubar.visible) {
				self.opener.refresh();
			}
			self.close()
		</script>
		<% }
		if(!"Settle & Print Invoice".equals(request.getParameter("submit")) && !"Save & Print Invoice".equals(request.getParameter("submit"))) { %>
			<a href="billingON3rdInv.jsp?billingNo=<%=billingNo%>&print_invoice_comment=<%=printInvoiceComment%>"> Print invoice</a>
		<% } %>


		<% if(!"Settle & Print Invoice".equals(request.getParameter("submit")) && !"Save & Print Invoice".equals(request.getParameter("submit"))) { %>
<script LANGUAGE="JavaScript">
                                            
        <% 
            //GET WORKLOAD MANAGEMENT SCREEN IF THERE IS ANY
            String curBilf = request.getParameter("curBillForm");
            
            String provider = (String) request.getSession().getAttribute("user");
            UserProperty prop = userPropertyDAO.getProp(provider, UserProperty.WORKLOAD_MANAGEMENT);

            String wrkloadmanagement =  prop!= null ? prop.getValue() : null;
            if ( wrkloadmanagement != null && !wrkloadmanagement.equals("") && !wrkloadmanagement.equals(curBilf) ){ 
        	    ///NEED TO CHECK IF THIS IS THE CURRENT FORM IF SO LET IT CLOSE!!!
            	String urlBack = request.getParameter("url_back")+"&curBillForm="+wrkloadmanagement;

        %>
            self.opener.refresh(); 
            self.location.href="<%=urlBack%>";
        
       	 <% } else { %>       	 
       			if (opener != null && (opener.location.href.indexOf("providercontrol.jsp") != -1 || opener.location.href.indexOf("appointmentprovideradminday.jsp") != -1)) {
       	 			self.opener.refresh();
       			}
	       	 	self.close(); 	    
         <% } %>
</script>
	<% } else { 
		response.sendRedirect("billingON3rdInv.jsp?billingNo=" + billingNo + "&print_invoice_comment=" + printInvoiceComment);
	} %>

<%} else { %>

<h1>Sorry, billing has failed. Please do it again!</h1>

<%}
}
%>


