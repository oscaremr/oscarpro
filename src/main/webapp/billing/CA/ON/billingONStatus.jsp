<!DOCTYPE html>
<%--

    Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

--%>
<%@page import="org.oscarehr.util.MiscUtils"%>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@page import="oscar.OscarProperties" %>
<%@page import="java.text.NumberFormat" %>
<%@page import="java.text.DecimalFormat" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>

<%! boolean bMultisites = org.oscarehr.common.IsPropertiesOn.isMultisitesEnable(); %>
<%@ page import="java.math.*,java.util.*, java.sql.*, oscar.*, java.net.*,oscar.util.*,oscar.oscarBilling.ca.on.pageUtil.*,oscar.oscarBilling.ca.on.data.*,org.apache.struts.util.LabelValueBean" %>
<%@ page import="org.oscarehr.common.model.BillingPermission"%>
<%@ page import="org.oscarehr.common.dao.BillingPermissionDao"%>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    if(session.getAttribute("userrole") == null )  response.sendRedirect("../logout.jsp");
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean isTeamBillingOnly=false;
    boolean isSiteAccessPrivacy=false;
    boolean isTeamAccessPrivacy=false; 
    boolean hideName = SystemPreferencesUtils
            .isReadBooleanPreferenceWithDefault("invoice_reports_print_hide_name_enabled", false);
	BillingPermissionDao billingPermissionDao = SpringUtils.getBean(BillingPermissionDao.class);
	PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
	boolean isDisplayPaymentMethodColumnsInInvoiceReportEnabled =
			propertyDao.isActiveBooleanProperty("display_payment_method_column_in_invoice_report");
%>
<security:oscarSec objectName="_team_billing_only" roleName="<%=roleName$ %>" rights="r" reverse="false">
<% isTeamBillingOnly=true; %>
</security:oscarSec>
<security:oscarSec objectName="_site_access_privacy" roleName="<%=roleName$%>" rights="r" reverse="false">
	<%isSiteAccessPrivacy=true; %>
</security:oscarSec>
<security:oscarSec objectName="_team_access_privacy" roleName="<%=roleName$%>" rights="r" reverse="false">
	<%isTeamAccessPrivacy=true; %>
</security:oscarSec>

<%
	//multi-site office , save all bgcolor to Hashmap
	HashMap<String,String> siteBgColor = new HashMap<String,String>();
	HashMap<String,String> siteShortName = new HashMap<String,String>();
	int patientCount = 0;
	if (bMultisites) {
    	SiteDao siteDao = (SiteDao)WebApplicationContextUtils.getWebApplicationContext(application).getBean("siteDao");
    	
    	List<Site> sites = siteDao.getAllSites();
    	for (Site st : sites) {
    		siteBgColor.put(st.getName(),st.getBgColor());
    		siteShortName.put(st.getName(),st.getShortName());
    	}
	}
%>

<%//
response.setHeader("Pragma","no-cache"); //HTTP 1.0
response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
response.setDateHeader("Expires", 0); //prevents caching at the proxy server
response.setHeader("Cache-Control", "private"); // HTTP 1.1 
response.setHeader("Cache-Control", "no-store"); // HTTP 1.1 
response.setHeader("Cache-Control", "max-stale=0"); // HTTP 1.1 

LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
String curUser_providerno = loggedInInfo.getLoggedInProviderNo();

boolean bSearch = true;
String[] billType = request.getParameterValues("billType");
String[] strBillType = new String[] {""};
if (billType == null || billType.length == 0) { // no boxes checked
	bSearch = false;
	strBillType = new String[] {"HCP","WCB","RMB","NOT","PAT","OCF","ODS","CPP","STD","IFH","BON"};
} else { 
	// at least on box checked
	strBillType = billType;
}

String accountingNumber = StringUtils.noNull(request.getParameter("accountingNumber"));
String claimNumber = StringUtils.noNull(request.getParameter("claimNumber"));
String statusType = request.getParameter("statusType");
String providerNo = request.getParameter("providerview");
String startDate  = request.getParameter("xml_vdate"); 
String endDate    = request.getParameter("xml_appointment_date");
String demoHin = StringUtils.noNull(request.getParameter("demographicHin"));
String demoName = StringUtils.noNull(request.getParameter("demographicName"));
String demoNo     = request.getParameter("demographicNo");
String serviceCode     = request.getParameter("serviceCode");
String raCode     = request.getParameter("raCode");
String dx = request.getParameter("dx");
String visitType = request.getParameter("visitType");
String filename = request.getParameter("demographicNo");
Boolean hideProviderDateRange = Boolean.parseBoolean(request.getParameter("hideProviderDateRange"));

String selectedSite = request.getParameter("site");
String billingForm = request.getParameter("billing_form");

String visitLocation = request.getParameter("xml_location");

String sortName = request.getParameter("sortName");
String sortOrder = request.getParameter("sortOrder");

String paymentStartDate  = request.getParameter("paymentStartDate"); 
String paymentEndDate    = request.getParameter("paymentEndDate");

int pageNumber = NumberUtils.isParsable(request.getParameter("page")) ? Integer.parseInt(request.getParameter("page")) : 1;
int resultsPerPage = request.getParameter("results_per_page") == null ? 20 : (request.getParameter("results_per_page").equals("all") ? -1 : (NumberUtils.isParsable(request.getParameter("results_per_page")) ? Integer.parseInt(request.getParameter("results_per_page")) : 20));

if ( statusType == null ) { statusType = "O"; } 
if ( "_".equals(statusType) ) { demoNo = "";}
if ( startDate == null ) { startDate = ""; } 
if ( endDate == null ) { endDate = ""; } 
if ( demoNo == null ) { demoNo = ""; filename = "";} 
if ( providerNo == null ) { providerNo = "" ; } 
if ( raCode == null ) { raCode = "" ; } 
if ( dx == null ) { dx = "" ; } 
if ( visitType == null ) { visitType = "-" ; } 
if ( serviceCode == null || serviceCode.equals("")) serviceCode = "%";
if ( billingForm == null ) { billingForm = "-" ; }
if ( visitLocation == null) { visitLocation = "";}
if ( sortName == null) { sortName = "ServiceDate";}
if ( sortOrder == null) { sortOrder = "asc";}
if ( paymentStartDate == null ) { paymentStartDate = ""; } 
if ( paymentEndDate == null ) { paymentEndDate = ""; } 

ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
List<Provider> providerList = isTeamBillingOnly
			? providerDao.getCurrentTeamProviders((String) session.getAttribute("user"))
			: providerDao.getAllBillableProviders();

BillingStatusPrep sObj = new BillingStatusPrep();
sObj.setRequest(request);

Integer totalRecords = 0;

BigDecimal total = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
BigDecimal feeTotal = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
BigDecimal paidTotal = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
BigDecimal paidTotalPrivate = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
BigDecimal paidTotalOhip = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
BigDecimal adjTotal = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
BigDecimal delTotal = (BigDecimal.ZERO).setScale(2, BigDecimal.ROUND_HALF_UP);

List<BillingClaimHeader1Data> bList = null;
if((serviceCode == null || billingForm == null) && dx.length()<2 && visitType.length()<2) {
	bList = bSearch ? sObj.getBills(BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo, startDate, endDate, demoNo, visitLocation,paymentStartDate, paymentEndDate, pageNumber, resultsPerPage) : new ArrayList<BillingClaimHeader1Data>();
	//serviceCode = "-";
	serviceCode = "%";
} else {
	serviceCode = (serviceCode == null || serviceCode.length()<2)? "%" : serviceCode;
	if (bSearch) {
		totalRecords = sObj.getInvoiceSummary("count", BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo, startDate, endDate, demoNo, demoName, demoHin, serviceCode, accountingNumber, claimNumber, dx, raCode, visitType, billingForm, visitLocation, sortName, sortOrder, paymentStartDate, paymentEndDate, selectedSite).intValue();
		total = new BigDecimal(sObj.getInvoiceSummary("sumFee", BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo, startDate, endDate, demoNo, demoName, demoHin, serviceCode, accountingNumber, claimNumber, dx, raCode, visitType, billingForm, visitLocation, sortName, sortOrder, paymentStartDate, paymentEndDate, selectedSite)).setScale(2, BigDecimal.ROUND_HALF_UP);
		feeTotal = new BigDecimal(sObj.getInvoiceSummary("sumFee", BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo, startDate, endDate, demoNo, demoName, demoHin, serviceCode, accountingNumber, claimNumber, dx, raCode, visitType, billingForm, visitLocation, sortName, sortOrder, paymentStartDate, paymentEndDate, selectedSite)).setScale(2, BigDecimal.ROUND_HALF_UP);
		paidTotalPrivate = new BigDecimal(sObj.getInvoiceSummary("sumPrivatePay",
		    BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo,
		    startDate, endDate, demoNo, demoName, demoHin, serviceCode,
		    accountingNumber, claimNumber, dx, raCode, visitType, billingForm,
		    visitLocation, sortName, sortOrder, paymentStartDate, paymentEndDate,
		    selectedSite)).setScale(2, BigDecimal.ROUND_HALF_UP);
		paidTotalOhip = new BigDecimal(sObj.getInvoiceSummary("sumAmountPay",
		    BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo,
		    startDate, endDate, demoNo, demoName, demoHin, serviceCode,
		    accountingNumber, claimNumber, dx, raCode, visitType, billingForm,
		    visitLocation, sortName, sortOrder, paymentStartDate, paymentEndDate,
		    selectedSite)).setScale(2, BigDecimal.ROUND_HALF_UP);
		paidTotal = paidTotalPrivate.add(paidTotalOhip);
		delTotal = new BigDecimal(sObj.getInvoiceSummary("sumDeleted", BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo, startDate, endDate, demoNo, demoName, demoHin, serviceCode, accountingNumber, claimNumber, dx, raCode, visitType, billingForm, visitLocation, sortName, sortOrder, paymentStartDate, paymentEndDate, selectedSite)).setScale(2, BigDecimal.ROUND_HALF_UP);
		adjTotal = total.subtract(paidTotal);
		bList = sObj.getBillsWithSorting(BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo, startDate, endDate, demoNo, demoName, demoHin, serviceCode, accountingNumber, claimNumber, dx, raCode, visitType, billingForm, visitLocation, sortName, sortOrder, paymentStartDate, paymentEndDate, pageNumber, resultsPerPage, selectedSite);
	} else {
		bList = new ArrayList<BillingClaimHeader1Data>();
	}
}

RAData raData = new RAData();


NumberFormat formatter = new DecimalFormat("#0.00");


%>

<%
	String ohipNo= "";
	if(request.getParameter("provider_ohipNo")!=null)
		ohipNo = request.getParameter("provider_ohipNo");
%>

<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.oscarehr.common.model.Site"%>
<%@page import="org.oscarehr.common.model.Provider"%>
<%@ page import="org.oscarehr.common.model.BillingONCHeader1" %>
<%@ page import="org.oscarehr.common.dao.BillingONCHeader1Dao" %>
<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.common.model.*" %>
<%@ page import="org.oscarehr.common.dao.*" %>
<%@ page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.apache.commons.lang3.math.NumberUtils" %>
<html>
    <head>
        <title><bean:message key="admin.admin.invoiceRpts"/></title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
        
		<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.9.1.min.js"></script>
		<script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath() %>/library/bootstrap2-datepicker/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="<%=request.getContextPath() %>/js/excellentexport.min.js"></script>
		<link href="<%=request.getContextPath() %>/css/bootstrap.min.css" rel="stylesheet">
		<link href="<%=request.getContextPath() %>/css/datepicker.css" rel="stylesheet" type="text/css">
       
        <script type="text/javascript">

            function nav_colour_swap(navid, num) {               
                for(var i = 0; i < num; i++) {
                    var nav = document.getElementById("A" + i);
                    if(navid == nav.id) { //selected td
                        nav.style.color = "red";
                    }
                    else { //other td
                        nav.style.color = "#645FCD";
                    }
                }
            }  
            
            function submitForm(methodName) {
                if (methodName==="email"){
                    document.invoiceForm.method.value="sendListEmail";
                } else if (methodName==="print") {
					document.invoiceForm.method.value="getListPrintPDF";
				} else if (methodName==="excel") {
					document.invoiceForm.method.value="exportToExcel";
				} else if (methodName==="csv") {
					document.invoiceForm.method.value="exportToCsv";
				}
                document.invoiceForm.submit();
            }
            
            function printAllInvoiceReports() {
            	var url = "" + window.location;
            	var regex = /&results_per_page=\d*/i;
            	if (url.indexOf("&results_per_page=") > 0) {
            		url = url.replace (regex, "&results_per_page=all");
				}
				var printWindow = window.open(url, 'Print', 'left=200, top=200, width=950, height=500, toolbar=0, resizable=0');
				printWindow.addEventListener('load', function(){
					printWindow.print();
				}, true);
				printWindow.addEventListener('afterprint', function(){
				    printWindow.close();
                }, true);
			}

        function fillEndDate(d){
           document.serviceform.xml_appointment_date.value= d;  
        }
        function setDemographic(demoNo){
           //alert(demoNo);
           document.serviceform.demographicNo.value = demoNo;
        }
		function popupPage(vheight,vwidth,varpage) {
		  var page = "" + varpage;
		  windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=0,screenY=0,top=0,left=0";
		  var popup=window.open(page, "billcorrection", windowprops);
		    if (popup != null) {
		    if (popup.opener == null) {
		      popup.opener = self;
		    }
		    popup.focus();
		  }
		}
        function check(stat){
			for (var x = 0; x < 10; x++) {
				document.serviceform.billType[x].checked= stat;  
			}
        }
        function changeStatus(){
        	//alert(document.serviceform.billTypeAll.checked);
           if(document.serviceform.billTypeAll.checked) {
			check(true);
           } else {
			check(false);
           }  
        }	
        
        function changeProvider(shouldSubmit) {
        	var index = document.serviceform.providerview.selectedIndex;
        	var provider_no = document.serviceform.providerview[index].value;
			if (provider_no == "none") {
				document.serviceform.provider_ohipNo.value = "";
			}
        	<% for (Provider provider : providerList){
        	    if(billingPermissionDao.hasPermission(provider.getProviderNo(), curUser_providerno, BillingPermission.INVOICE_REPORT)){
        	%>
			if (provider_no == "<%=provider.getProviderNo()%>") {
				document.serviceform.provider_ohipNo.value = "<%=provider.getOhipNo()%>";
				if (shouldSubmit) {
					if (document.getElementById("xml_vdate").value.length > 0 && document.getElementById("xml_appointment_date").value.length > 0)
						document.serviceform.submit();
				} else { return };
			}<%
			    } 
        	} %>
        	if (shouldSubmit) { document.serviceform.submit(); }
        }	
        </script>
<script type="text/javascript">
var xmlHttp;
function createXMLHttpRequest() {
	if (window.ActiveXObject) {
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	else if (window.XMLHttpRequest) {
		xmlHttp = new XMLHttpRequest();
	}
}
var ajaxFieldId;
function startRequest(idNum) {
	ajaxFieldId = idNum;
	createXMLHttpRequest();
	xmlHttp.onreadystatechange = handleStateChange;
	var val = 'N';
	if(document.getElementById('status'+idNum).checked) {
	//alert(('status'+idNum) + document.getElementById('status'+idNum).checked);
		val = 'Y';
	}
	xmlHttp.open("GET", "billingONStatusERUpdateStatus.jsp?id="+idNum+"&val="+val , true);
	xmlHttp.send(null);
}

function handleStateChange() {
	if(xmlHttp.readyState == 4) {
//alert(xmlHttp.status + "0 :go 2" + xmlHttp.responseText);
			//document.getElementById(ajaxFieldId).innerHTML = xmlHttp.responseText;
		if(xmlHttp.status == 200) {
//alert("go 3" + xmlHttp.responseText);
			document.getElementById(ajaxFieldId).innerHTML = xmlHttp.responseText;
		}
	}
}

var isChecked = false;
function checkAll(group) {
    for (i = 0; i < group.length; i++) 
        group[i].checked = !isChecked;
    isChecked = !isChecked;
}


updateSort = function(name) {
	var sortName =$("#sortName").val();
	var sortOrder = $("#sortOrder").val();
	
	if(sortName != name) {
		sortName = name;
		sortOrder = 'asc';
	} else {
		if(sortOrder == 'asc') {
			sortOrder = 'desc';
		} else if(sortOrder == 'desc') {
			sortOrder = 'asc';
		} else {
			//this shouldn't happen..but just in case
			sortOrder='asc';
		}
	}
	
	$("#sortName").val(sortName);
	$("#sortOrder").val(sortOrder);
	
	document.serviceform.submit();
}

</script>

<style>

table td,th{font-size:12px;}

@media print {
  .hidden-print {
    display: none !important;
  }
  .show-print {
	  display: block !important;
  }
}
</style>
  
    </head>
    <body>
    <h3><bean:message key="admin.admin.invoiceRpts"/></h3>
  	<div class="container-fluid">
 <div class="row">
<%=DateUtils.sumDate("yyyy-MM-dd","0")%>

<div class="pull-right hidden-print">
<!--Hiding for now since this does not seem to manage the providers in the select 
<a href="javascript: function myFunction() {return false; }" onClick="popupPage(700,720,'../../../oscarReport/manageProvider.jsp?action=billingreport')">Manage Provider List</a>-->
<label for="hideProviderDateRange">
	<input type="checkbox" value="true" id="hideProviderDateRangeCheck" onchange="updateProviderDateRange()" <%=(hideProviderDateRange ? "checked='checked'" : "")%>/>
	Hide Provider & Date Range on Print
</label>
<button class="btn" type='button' name='print' value='Print' onClick='printAllInvoiceReports()'><i class="icon icon-print"></i> Print</button>
</div>
	 <%
		 String providerStr = "";
		 providerNo = StringUtils.noNull(providerNo);
		 startDate = StringUtils.noNull(startDate);
		 endDate = StringUtils.noNull(endDate);
		 if (providerNo.equals("all")) {
			 providerStr = "All Providers";
		 } else {
			 for (Provider p : providerList) {
				 if (providerNo.equals(p.getProviderNo())) {
					 providerStr = Encode.forJavaScript(p.getLastName()) + ", " + Encode.forJavaScript(p.getFirstName());
					 break;
				 }
			 }
		 }

	 %>
	 <div id="providerDateRange" class="pull-right <%=(hideProviderDateRange ? "hide-print" : "show-print")%>" style="display:none;">
		 <strong>Provider:&nbsp;&nbsp;&nbsp;</strong><%=providerStr%></br>
		 <strong>Start Date:&nbsp;</strong><%=startDate%></br>
		 <strong>End Date:&nbsp;&nbsp;&nbsp;</strong><%=endDate%></br>
	 </div>

</div>

<form name="serviceform" method="get" action="billingONStatus.jsp">
<input type="hidden" id="hideProviderDateRange" name="hideProviderDateRange" value="<%=hideProviderDateRange.toString()%>"/>
<input type="hidden" id="sortName" name="sortName" value="<%=sortName%>"/>
<input type="hidden" id="sortOrder" name="sortOrder" value="<%=sortOrder%>"/>

<div class="row well well-small hidden-print">    
  
    	<%
    		String tmpStrBillType = Arrays.toString(strBillType);
    	%>
<div class="row">
<div class="span10">
<small>    
<input type="checkbox" name="billTypeAll" value="ALL" checked onclick="changeStatus();"><span style="padding-right:4px">ALL</span></input> 
        <input type="checkbox" name="billType" value="HCP" <%=tmpStrBillType.indexOf("HCP")>=0?"checked":""%>><span style="padding-right:4px">Bill OHIP </span></input> 
        <input type="checkbox" name="billType" value="RMB" <%=tmpStrBillType.indexOf("RMB")>=0?"checked":""%>><span style="padding-right:4px">RMB </span></input> 
        <input type="checkbox" name="billType" value="WCB" <%=tmpStrBillType.indexOf("WCB")>=0?"checked":""%>><span style="padding-right:4px">WCB</span></input> 
        <input type="checkbox" name="billType" value="NOT" <%=tmpStrBillType.indexOf("NOT")>=0?"checked":""%>><span style="padding-right:4px">Not Bill</span></input> 
        <input type="checkbox" name="billType" value="PAT" <%=tmpStrBillType.indexOf("PAT")>=0?"checked":""%>><span style="padding-right:4px">Bill Patient</span></input> 
        <input type="checkbox" name="billType" value="OCF" <%=tmpStrBillType.indexOf("OCF")>=0?"checked":""%>><span style="padding-right:4px">OCF</span></input> 
        <input type="checkbox" name="billType" value="ODS" <%=tmpStrBillType.indexOf("ODS")>=0?"checked":""%>><span style="padding-right:4px">ODSP</span></input> 
        <input type="checkbox" name="billType" value="CPP" <%=tmpStrBillType.indexOf("CPP")>=0?"checked":""%>><span style="padding-right:4px">CPP</span></input> 
        <input type="checkbox" name="billType" value="STD" <%=tmpStrBillType.indexOf("STD")>=0?"checked":""%>><span style="padding-right:4px">STD/LTD</span></input> 
        <input type="checkbox" name="billType" value="IFH" <%=tmpStrBillType.indexOf("IFH")>=0?"checked":""%>><span style="padding-right:4px">IFH</span></input>
		<input type="checkbox" name="billType" value="BON" <%=tmpStrBillType.indexOf("BON")>=0?"checked":""%>><span style="padding-right:4px">BON</span></input>
</small>
</div>
</div><!-- row -->


<div class="row">
<div class="span3">
<br/>
<%
	String curSite = request.getParameter("site");
	// Create option list for all billable providers
	StringBuilder allProviderOptions = new StringBuilder();
	allProviderOptions.append("<option ");
	allProviderOptions.append("value=\"all\">");
	allProviderOptions.append("---All Providers---");
	allProviderOptions.append("</option>");
	
	HashMap <String, Boolean> permissionList = billingPermissionDao.hasPermissionList(providerList, curUser_providerno, BillingPermission.INVOICE_REPORT);
	
	for (Provider provider : providerList) {
	    if (permissionList.get(provider.getProviderNo()) == null || permissionList.get(provider.getProviderNo())) {
            allProviderOptions.append("<option ");
            if (providerNo.equals(provider.getProviderNo())) { allProviderOptions.append("selected "); }
            allProviderOptions.append("value=\"").append(provider.getProviderNo()).append("\">");
            allProviderOptions.append(Encode.forJavaScript(provider.getLastName())).append(", ").append(Encode.forJavaScript(provider.getFirstName()));
            allProviderOptions.append("</option>");
        }
	}
	if (bMultisites) {
		SiteDao siteDao = SpringUtils.getBean(SiteDao.class);
	  	List<Site> sites = siteDao.getActiveSitesByProviderNo((String) session.getAttribute("user"));
%>
<script>
	var _providers = [];
<%	// Create option lists for all site billable providers
	Map siteProvidersOptions = new HashMap<Site, String>();
	for (Site site : sites) {
		StringBuilder optionsString = new StringBuilder();
		Set<Provider> siteProvidersSet = site.getProvidersOrderByName();
		List<Provider> siteProvidersList = new ArrayList<Provider>();
		siteProvidersList.addAll(siteProvidersSet);
		Collections.sort(siteProvidersList, (new Provider()).ComparatorName());
		optionsString.append("<option ");
		optionsString.append("value=\"all\">");
		optionsString.append("---All Providers---");
		optionsString.append("</option>");
		for (Provider provider : siteProvidersList) {
			if (providerList.contains(provider) && (permissionList.get(provider.getProviderNo()) == null || permissionList.get(provider.getProviderNo()))) {
				optionsString.append("<option ");
				if (providerNo.equals(provider.getProviderNo())) { optionsString.append("selected "); }
				optionsString.append("value=\"").append(provider.getProviderNo()).append("\">");
				optionsString.append(Encode.forJavaScript(provider.getLastName())).append(", ").append(Encode.forJavaScript(provider.getFirstName()));
				optionsString.append("</option>");
			}
		}
		siteProvidersOptions.put(site, optionsString.toString());
	}
	// Output all provider arrays
	Iterator<Map.Entry<Site, String>> it = siteProvidersOptions.entrySet().iterator();
	while (it.hasNext()) {
		Map.Entry<Site, String> pair = it.next();%>
	<%="_providers[\'" + StringEscapeUtils.escapeJavaScript(pair.getKey().getName()) + "\']=\'" + pair.getValue() + "\';"%><% 
	} %>
	<%="_providers[\'all\']=\'" + allProviderOptions.toString() + "\';"%>
function changeSite(sel) {
	sel.form.providerview.innerHTML = _providers[sel.value];
	
	sel.style.backgroundColor=sel.options[sel.selectedIndex].style.backgroundColor;
	if (sel.value=='<%=StringEscapeUtils.escapeJavaScript(request.getParameter("site"))%>') {
		if (document.serviceform.provider_ohipNo.value!='')
			sel.form.providerview.value='<%=request.getParameter("providerview")%>';
	} else {
		document.serviceform.provider_ohipNo.value="";
	}
	changeProvider(false);
}
	</script>
	<select id="site" name="site" onchange="changeSite(this)">
		<option value="all" style="background-color:white">---All Clinics---</option>
		<% for (Site site : sites) { 
			String bgColor = StringUtils.isNullOrEmpty(site.getBgColor()) ? "#ffffff" : site.getBgColor();
		%>
			<option value="<%= Encode.forHtmlAttribute(site.getName()) %>" style="background-color:<%=bgColor%>" <%=site.getName().toString().equals(curSite) ? "selected" : "" %>>
				<%= Encode.forHtmlContent(site.getName()) %>
			</option>
		<% } %>
	</select>
</div>
<div class="span3">
<br/>
	<select id="providerview" name="providerview" onchange="changeProvider(true);">
		<%=allProviderOptions.toString()%>
	</select>
<% if (request.getParameter("providerview")!=null) { %>
	  	<script>
	 	window.onload=function(){changeSite(document.getElementById("site"));}
	  	</script>
<%  }
} else { %>
	<select name="providerview" onchange="changeProvider(false);">
		<% if(providerList.size() == 1) {
			Provider defaultProvider = providerList.get(0);
		%>
		<option value="<%=defaultProvider.getProviderNo()%>"> <%=Encode.forHtmlContent(defaultProvider.getLastName()+", "+defaultProvider.getFirstName())%></option>
		<script>document.serviceform.provider_ohipNo.value=<%=defaultProvider.getOhipNo()%>;</script>
		<% } else { %>
			<%=allProviderOptions%>
		<% } %>
	</select>
<% } %>
</div>

<div class="span2">      
OHIP No.: <br>
<input type="text" class="span2" name="provider_ohipNo" readonly value="<%=ohipNo%>">
</div>
 

	<div class="span2">		
	Start:
		<label class="input-append">
			<input type="text" name="xml_vdate" id="xml_vdate" style="width:90px" value="<%=startDate%>" pattern="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" autocomplete="off" />
			<span class="add-on"><i class="icon-calendar"></i></span>
		</label>
	</div>


	<div class="span2">		
	End: 
		<small>
		<a href="javascript: function myFunction() {return false; }" onClick="fillEndDate('<%=DateUtils.sumDate("yyyy-MM-dd","-30")%>')" >30</a>
		<a href="javascript: function myFunction() {return false; }" onClick="fillEndDate('<%=DateUtils.sumDate("yyyy-MM-dd","-60")%>')" >60</a>
		<a href="javascript: function myFunction() {return false; }" onClick="fillEndDate('<%=DateUtils.sumDate("yyyy-MM-dd","-90")%>')" >90</a>     
		days back                    
		</small>
	
		<label class="input-append">
			<input type="text" name="xml_appointment_date" style="width:90px" id="xml_appointment_date" value="<%=endDate%>" pattern="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" autocomplete="off" />
			<span class="add-on"><i class="icon-calendar"></i></span>
		</label>
	</div>
	<div class="span3">
		Results per Page:
		<input type="text" name="results_per_page" style="width:90px" id="results_per_page" value="<%=resultsPerPage%>"/>
	</div>
</div><!-- row -->    
    
<div class="row">		
<div class="span2">	        
Dx:<br>
<input type="text" name="dx" class="span2" value="<%=dx%>"/>
</div>

<div class="span2">	
Serv. Code:<br>
<input type="text" name="serviceCode" class="span2" value="<%=serviceCode%>"/>
</div>

<div class="span2">	
Invoice #:<br>
<input type="text" name="accountingNumber" class="span2" value="<%=accountingNumber%>"/>
</div>

<div class="span2">
	OHIP claim #:<br>
	<input type="text" name="claimNumber" class="span2" value="<%=claimNumber%>"/>
</div>
	
<div class="span2">	
RA Code:<br>
<input type="text" name="raCode" class="span2" value="<%=raCode%>"/>
</div>
</div> <!-- row -->

<div class="row">
	<div class="span2">
		Demographic No:<br>
		<input type="text" name="demographicNo" class="span2" value="<%=demoNo%>"/>
	</div>

	<div class="span3">
		Demographic Name:<br>
		<input type="text" name="demographicName" class="span3" value="<%=demoName%>"/>
	</div>

	<div class="span3">
		Demographic HIN:<br>
		<input type="text" name="demographicHin" class="span3" value="<%=demoHin%>"/>
	</div>
</div>
<div class="row">
<div class="span2">	
Visit Type:<br>
	<select class="span2" name="visitType" style="background-color:none;">
		<option value="-" <%=visitType.startsWith("-")?"selected":""%>> </option>
		<option value="00" <%=visitType.startsWith("00")?"selected":""%>>Clinic Visit</option>
		<option value="01" <%=visitType.startsWith("01")?"selected":""%>>Outpatient Visit</option>
		<option value="02" <%=visitType.startsWith("02")?"selected":""%>>Hospital Visit</option>
		<option value="03" <%=visitType.startsWith("03")?"selected":""%>>ER</option>
		<option value="04" <%=visitType.startsWith("04")?"selected":""%>>Nursing Home</option>
		<option value="05" <%=visitType.startsWith("05")?"selected":""%>>Home Visit</option>
	</select>

</div>

<div class="span5">		
Billing Form:<br>
<select name="billing_form" class="span5">
 <option value="---" selected="selected"> --- </option>
 <%
        List<LabelValueBean> forms = sObj.listBillingForms();
        String selected = "";
        for (LabelValueBean form : forms) {
            if (billingForm != null) {
                if (billingForm.equals(form.getValue())) {
                    selected = "selected";
                } else {
                    selected = "";
                }
            }
%>
<option value="<%= form.getValue()%>" <%= selected%> ><%= form.getLabel()%></option>
<%
        }
%>

</select>
</div>
</div><!-- row -->

<div class="row">
<div class="span5">		
Site Number:<br>
<select name="xml_location" class="span5">
 												<% //
		JdbcBillingPageUtil tdbObj = new JdbcBillingPageUtil();
 											    
	    String billLocationNo="", billLocation="";
	    List lLocation = tdbObj.getFacilty_num();
	    for (int i = 0; i < lLocation.size(); i = i + 2) {
		billLocationNo = (String) lLocation.get(i);
		billLocation = (String) lLocation.get(i + 1);
		String locationSelected = visitLocation.equals(billLocationNo)? " selected=\"selected\" ":"";
%>
	<option value="<%=billLocationNo%>" <%=locationSelected %>>
	<%=billLocationNo + " | " + billLocation%>
	</option>
	<%	    } %>
												

</select>
</div>
<div class="span7">
	<div class="span2">		
	Payment Start:
		<label class="input-append">
			<input type="text" name="paymentStartDate" id="paymentStartDate" style="width:90px" value="<%=paymentStartDate%>" pattern="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" autocomplete="off" />
			<span class="add-on"><i class="icon-calendar"></i></span>
		</label>
	</div>
	<div class="span2">		
	Payment End:
		<label class="input-append">
			<input type="text" name="paymentEndDate" id="paymentEndDate" style="width:90px" value="<%=paymentEndDate%>" pattern="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" autocomplete="off" />
			<span class="add-on"><i class="icon-calendar"></i></span>
		</label>
	</div>

</div>
</div>

<div class="row" >
<div class="span10">
<small>	
       <input type="radio" name="statusType" value="%" <%=statusType.equals("%")?"checked":""%>>All</input>
	   <input type="radio" name="statusType" value="_" <%=statusType.equals("_")?"checked":""%>>Rejected</input>
       <input type="radio" name="statusType" value="H" <%=statusType.equals("H")?"checked":""%>>Capitated</input>
       <input type="radio" name="statusType" value="O" <%=statusType.equals("O")?"checked":""%>>Invoiced</input>
       <input type="radio" name="statusType" value="P" <%=statusType.equals("P")?"checked":""%>>Bill Patient</input>
        <!--li><input type="radio" name="statusType" value="N" <%=statusType.equals("N")?"checked":""%>>Do Not Bill</input>
       <input type="radio" name="statusType" value="W" <%=statusType.equals("W")?"checked":""%>>WCB</input>-->
       <input type="radio" name="statusType" value="B" <%=statusType.equals("B")?"checked":""%>>Submitted OHIP</input>
       <input type="radio" name="statusType" value="S" <%=statusType.equals("S")?"checked":""%>>Settled/Paid</input>
       <input type="radio" name="statusType" value="X" <%=statusType.equals("X")?"checked":""%>>Bad Debt</input>
       <input type="radio" name="statusType" value="D" <%=statusType.equals("D")?"checked":""%>>Deleted Bill</input>
       <input type="radio" name="statusType" value="I" <%=statusType.equals("I")?"checked":""%>>Bonus Code</input>
</small>
</div>
</div>

<div class="row">
<div class="span2" style="padding-top:10px;">
<input class="btn btn-primary" type="submit" name="Submit" value="Create Report">
<br><br>
<input class="btn btn-primary" onClick="popupPage(600,800, 'https://my.freshbooks.com/#/reports/invoice-details')" value="Open Kai 3rd Party Reports">
</div>   
</div><!-- row -->

    </form>
</div>

<div class="row">
    <form name="invoiceForm" method="post" action="<%=request.getContextPath()%>/BillingInvoice.do?<%=request.getQueryString()%>">     
         <input type="hidden" name="method" value=""/>
<% //
if(statusType.equals("_")) { %>
    <!--  div class="rejected list"-->
		<p>(*) Invoice was created in another system.</p>
       <table class="table">
          <tr class="warning"> 
             <th>Health#</th>
             <th>D.O.B</th>
             <th>Invoice #</th>
             <!--th>Type</th-->
             <th>Ref #</th>
             <th>Hosp #</th>
			 <th>Status</th>
             <th title="admission date">Admitted</th>
             <th>Claim Error</th>
             <th>Code</th>
             <th>Fee</th>
             <th>Unit</th>
             <th>Date</th>
             <th>Dx</th>
             <th>Exp.</th>
             <th>Code Error</th>
             <th>Checked</th>
             <th>Report Name</th>
          </tr>
	<% //
		List<Provider> aLProviders = new ArrayList<Provider>();
		if ("all".equals(providerNo)) {
		    aLProviders.add(new Provider(providerNo));
		} else if (providerNo == null || providerNo.equals(""))  {
			aLProviders.addAll(providerList);
		} else {
			for (Provider provider : providerList) {
				if (providerNo.equals(provider.getProviderNo())) { aLProviders.add(provider); }
			}
		}
		for(Provider provider : aLProviders) {
			providerNo = provider.getProviderNo();
	List lPat = null;
	List<RaDetail> errors = new ArrayList<RaDetail>();
	BillingONCHeader1Dao cheader1Dao = (BillingONCHeader1Dao)SpringUtils.getBean(BillingONCHeader1Dao.class);
	RaDetailDao raDetailDao = SpringUtils.getBean(RaDetailDao.class);
	String[] errorCodes = {"AC1", "D8"};
        if(providerNo.equals("all")) {
            List<BillingProviderData> providerObj = (new JdbcBillingPageUtil()).getProviderObjList(providerNo);
            lPat = (new JdbcBillingErrorRepImpl()).getErrorRecords(BillingPermission.INVOICE_REPORT, loggedInInfo, providerObj, startDate, endDate, filename);
            errors = raDetailDao.getRaErrorsByProviderDate(Arrays.asList(errorCodes), startDate, endDate, ohipNo);
        } else {
            BillingProviderData providerObj = (new JdbcBillingPageUtil()).getProviderObj(providerNo);
            lPat = (new JdbcBillingErrorRepImpl()).getErrorRecords(BillingPermission.INVOICE_REPORT, loggedInInfo, providerObj, startDate, endDate, filename);
            errors = raDetailDao.getRaErrorsByProviderDate(Arrays.asList(errorCodes), startDate, endDate, ohipNo);
        }
    boolean nC = false;
	String invoiceNo = "";
	if (!errors.isEmpty()){
	    for (RaDetail detail : errors){
			BillingErrorRepData errorRep = new BillingErrorRepData();
			errorRep.setHin(detail.getHin());
			errorRep.setDob("");
			errorRep.setBilling_no(String.valueOf(detail.getBillingNo()));
			errorRep.setRef_no("");
			errorRep.setGroup_no("");
			errorRep.setStatus("");
			errorRep.setAdmitted_date("");
			errorRep.setClaim_error("");
			errorRep.setCode(detail.getServiceCode());
			errorRep.setFee(detail.getAmountClaim());
			errorRep.setUnit(detail.getServiceCount());
			errorRep.setCode_date(detail.getServiceDate());
			errorRep.setDx("");
			errorRep.setExp("");
			errorRep.setCode_error(detail.getErrorCode());
			errorRep.setReport_name(detail.getClaimNo());
			errorRep.setStatus("N");

			lPat.add(errorRep);
		}
	}
	
	List<Integer> billingNoList = new ArrayList<Integer>();
	for (int i = 0; i < lPat.size(); i++) {
		BillingErrorRepData bObj = (BillingErrorRepData) lPat.get(i);
		billingNoList.add(Integer.parseInt(bObj.getBilling_no()));
	}
	List<BillingONCHeader1> billCheader1List = cheader1Dao.getInvoicesByIds(billingNoList);

	for(int i=0; i<lPat.size(); i++) {

	    patientCount++;
		BillingErrorRepData bObj = (BillingErrorRepData) lPat.get(i);
		BillingONCHeader1 billCheader1 = null;
		for (BillingONCHeader1 billCHeader1Item : billCheader1List) {
			if (billCHeader1Item.getId() == Integer.parseInt(bObj.getBilling_no())) {
				billCheader1 = billCHeader1Item;
			}
		}
		String color = "";
		if(!invoiceNo.equals(bObj.getBilling_no())) {
			invoiceNo = bObj.getBilling_no();
			nC = nC ? false : true;
		} 
	    color = nC ? "class='success'" : "";
	%>
    		<tr <%=color %>>
    			<td><small><%=bObj.getHin() %> <%=(bObj.getVer()!=null)?bObj.getVer():""%></small></td>
    			<td><font size="-1"><%=bObj.getDob() %></font></td>
    			<td align="right">
					<% if (billCheader1!=null) {%>
						<a href=# onclick="popupPage(800,700,'billingONCorrection.jsp?billing_no=<%=bObj.getBilling_no()%>','BillCorrection<%=bObj.getBilling_no()%>');return false;"><%=bObj.getBilling_no() %></a>
					<% } else {%>
						* <%=bObj.getBilling_no() %>
					<%  } %>
				</td>
    			<td><%=bObj.getRef_no() %></td>
    			<td><%=bObj.getFacility() %></td>
				<td><%=billCheader1!=null?billCheader1.getStatus():"" %></td>
    			<td><%=bObj.getAdmitted_date() %></td>
    			<td><%=bObj.getClaim_error() %></td>
    			<td><%=bObj.getCode() %></td>
    			<%
    				String formattedFee = null;
    				try {
    				    formattedFee = String.valueOf(Integer.parseInt(bObj.getFee()));
    				}
    				catch( NumberFormatException e ) {
    				    formattedFee = "N/A";
    				}
    			%>
    			<td align="right"><%=ch2StdCurrFromNoDot(formattedFee)%></td>
    			<td align="right"><%=bObj.getUnit() %></td>
    			<td><font size="-1"><%=bObj.getCode_date() %></font></td>
    			<td><%=bObj.getDx() %></td>
    			<td><%=bObj.getExp() %></td>
    			<td><%=bObj.getCode_error() %></td>
    			<td align="center">
    			<input type="checkbox" id="status<%=bObj.getId() %>" name="status<%=bObj.getId() %>" 
    			value="Y" <%="N".equals(bObj.getStatus())? "":"checked" %> onclick="startRequest('<%=bObj.getId() %>');" />
    			</td>
    			<td id="<%=bObj.getId() %>"><%=bObj.getReport_name() %></td>
    		</tr>
<% }} %>
		   <tr class="warning">
			   <td>Count:</td>
			   <td align="center"><%=patientCount%></td>
			   <td align="center" class="<%=hideName?"hidden-print":""%>">&nbsp;</td>
			   <td>&nbsp;</td>
			   <td>&nbsp;</td>
			   <td>&nbsp;</td>
			   <td>&nbsp;</td>
			   <td>&nbsp;</td>
			   <td>Total:</td>
			   <td align="right"><%=feeTotal.toString()%></td>
			   <td>&nbsp;</td>
			   <td>&nbsp;</td>
			   <td>&nbsp;</td>
			   <td>&nbsp;</td>
			   <td>&nbsp;</td>
			   <td>&nbsp;</td>
			   <td>&nbsp;</td>
		   </tr>
        <%} else { %>
    <!--  div class="tableListing"-->
       <table class="table" id="bListTable">
          <thead>
		<tr> 
             <th><a href="javascript:void();" onClick="updateSort('ServiceDate');return false;">SERVICE DATE</a></th>
             <th> <a href="javascript:void();" onClick="updateSort('DemographicNo');return false;">PATIENT</a></th>
             <th class="<%=hideName?"hidden-print":""%>">PATIENT NAME</th>
             <th> <a href="javascript:void();" onClick="updateSort('VisitLocation');return false;">LOCATION</a></th>
             <th title="Billing Date">BILLING DATE</th>
             <th title="Status">STAT</th>
             <th>SETTLED/PAID</th>
             <th title="Code Billed">CODE</th>
             <th title="Amount Billed">BILLED</th>
             <th title="Amount Paid"  >PAID</th>
             <th title="Adjustments">ADJ</th>
             <th title="Deleted">DEL</th>
             <th>DX</th>
             <!--th>DX1</th-->
             <th>TYPE</th>
             <th>INVOICE #</th>
             <th>MESSAGES</th>
			<% if (!isDisplayPaymentMethodColumnsInInvoiceReportEnabled) {%>
			<th>METHOD</th>
			<% } else {%>
			<th>CASH</th>
			<th>DEBIT</th>
			<th>CHEQUE</th>
			<th>MASTER CARD</th>
			<th>VISA</th>
			<th>AMEX</th>
			<th>ELECTRONIC</th>
			<th>ALTERNATE</th>
			<% }%>
             <th>Quantity</th>
              <th>Provider</th>
		<% if (bMultisites) {%>
			 <th>SITE</th>             
        <% }%>  
            <th class="hidden-print"><a href="#" onClick="checkAll(document.invoiceForm.invoiceAction)"><bean:message key="billing.billingStatus.action"/></a></th>
          </tr>
       </thead>

<tbody>
          
       <% //
       String invoiceNo = ""; 
       boolean nC = false;
       boolean newInvoice = true;
       
       double totalCash=0;
       double totalDebit=0;

       ArrayList<Hashtable<String, String>> raMasterList = new ArrayList<Hashtable<String, String>>();
       
       if (!bList.isEmpty()) {
		   raMasterList = raData.getRADataInternListFromCh1ObjList(bList);
	   }
       
       for (int i = 0 ; i < bList.size(); i++) { 
    	   BillingClaimHeader1Data ch1Obj = bList.get(i);
    	   
    	   if (bMultisites && ch1Obj.getClinic()!=null && curSite!=null 
    			   && !ch1Obj.getClinic().equals(curSite) && isSiteAccessPrivacy) // only applies on user have siteAccessPrivacy (SiteManager)
				continue; // multisite: skip if the line doesn't belong to the selected clinic    		   
    		   
	       if (bMultisites && selectedSite != null && !selectedSite.equals("all") && (!selectedSite.equals(ch1Obj.getClinic())))
	    	   continue;
	       
	       patientCount ++;

		   ArrayList<Hashtable<String, String>> raList = new ArrayList<Hashtable<String, String>>();
    	   
    	   for (Hashtable<String, String> raItem : raMasterList) {
    	   		if (raItem.get("billing_no").equals(ch1Obj.getId())) {
					raList.add(raItem);
				}
		   }
    	   
	       boolean incorrectVal = false;
	       
	       BigDecimal valueToAdd = new BigDecimal("0.00");
	       try{
	          valueToAdd = new BigDecimal(ch1Obj.getTotal()).setScale(2, BigDecimal.ROUND_HALF_UP);  
	       }catch(Exception badValueException){ 
	          incorrectVal = true;
	       }
	       String amountPaid = "0.00";
	       String errorCode = "";
	       if(serviceCode.equals("-") && raList.size() > 0){
	    	   amountPaid = raData.getAmountPaid(raList);
	    	   errorCode = raData.getErrorCodes(raList);
	       } else if(raList.size() > 0) {
	    	   amountPaid = raData.getAmountPaid(raList,ch1Obj.getId(),ch1Obj.getTransc_id());
	    	   errorCode = raData.getErrorCodes(raList); 
	       }
	       // 3rd party billing
	       if(ch1Obj.getPay_program().matches("PAT|OCF|ODS|CPP|STD|IFH")) {
	    	   amountPaid = ch1Obj.getPaid();
	       }
	       
	       float qty = ch1Obj.getNumItems();
	       
    	   amountPaid = (amountPaid==null||amountPaid.equals("")||amountPaid.equals("null"))? "0.00" : amountPaid;
	       
	       BigDecimal bTemp;
	       BigDecimal adj;
		   BigDecimal del;
		  
	       try {
		   		bTemp = (new BigDecimal(amountPaid.trim())).setScale(2,BigDecimal.ROUND_HALF_UP);
			    adj = (new BigDecimal(ch1Obj.getTotal())).setScale(2,BigDecimal.ROUND_HALF_UP);
			    del = (new BigDecimal(ch1Obj.getTotalDeleted())).setScale(2,BigDecimal.ROUND_HALF_UP);
		   }
	       catch(NumberFormatException e ) {		   		
		   		MiscUtils.getLogger().error("Could not parse amount paid for invoice " + ch1Obj.getId(), e);
		   		throw e;
	       }
	       
           adj = adj.subtract(bTemp);
	       
           String color = "";
               
               if (invoiceNo.equals(ch1Obj.getId())){
                   newInvoice = false;
               }
               else {
                   newInvoice = true; 
               }
	       if(!invoiceNo.equals(ch1Obj.getId())) {
	    	   invoiceNo = ch1Obj.getId(); 
	    	   nC = nC ? false : true;
	       } 
	       color = nC ? "class='success'" : "";
               String settleDate = ch1Obj.getSettle_date();
               if( settleDate == null || !ch1Obj.getStatus().equals("S")) {
                   settleDate = "N/A";
               }
               else if (settleDate.indexOf(" ")>0){
                   settleDate = settleDate.substring(0, settleDate.indexOf(" "));
               }
               
               String payProgram = ch1Obj.getPay_program();
               boolean b3rdParty = false;
               if(payProgram.equals("PAT") || payProgram.equals("OCF") || payProgram.equals("ODS") || payProgram.equals("CPP") || payProgram.equals("STD")) {
                   b3rdParty = true;
               }

			   totalCash += ch1Obj.getCashTotal();
			   totalDebit += ch1Obj.getDebitTotal();
			   Map<Integer, BigDecimal> paymentTotals = new HashMap<Integer, BigDecimal>();
			   if (ch1Obj.getPaymentTotals()!=null){
			       paymentTotals = ch1Obj.getPaymentTotals();
			   }
			   BillingPaymentTypeDao paymentTypeDao = SpringUtils.getBean(BillingPaymentTypeDao.class);
			   
			
				
       %>       
          <tr <%=color %>> 
             <td align="center"><%=ch1Obj.getService_date()%>  <%--=ch1Obj.getBilling_time()--%></td>  <!--SERVICE DATE-->
             <td align="center"><%=ch1Obj.getDemographic_no()%></td> <!--PATIENT-->
             <td align="center" class="<%=hideName?"hidden-print":""%>"><a href=# onclick="popupPage(800,740,'../../../demographic/demographiccontrol.jsp?demographic_no=<%=ch1Obj.getDemographic_no()%>&displaymode=edit&dboperation=search_detail');return false;"><%= Encode.forHtmlContent(ch1Obj.getDemographic_name())%></a></td> 
             <td align="center"><%=ch1Obj.getFacilty_num()!=null?ch1Obj.getFacilty_num():"" %></td>
             <td align="center"><%=ch1Obj.getBilling_date()%></td><!--BILLING DATE-->
             <td align="center"><%=ch1Obj.getStatus()%></td> <!--STAT-->
             <td align="center"><%=settleDate%></td> <!--SETTLE DATE-->
             <td align="center"><%=getHtmlSpace(ch1Obj.getTransc_id())%></td><!--CODE-->
             <td align="right"><%=getStdCurr(ch1Obj.getTotal())%></td><!--BILLED-->
             <td align="right">
				 <%
					 if (b3rdParty && !isDisplayPaymentMethodColumnsInInvoiceReportEnabled) {
					     if (amountPaid.equals(ch1Obj.getTotal()) && !paymentTotals.entrySet().isEmpty()) {
					         for (Map.Entry<Integer, BigDecimal> payment : paymentTotals.entrySet()) {
					             if (payment.getValue()!=null){
				 %>
				 <a href="javascript: function myFunction() {return false; }"  onclick="javascript:popupPage(800,860,'billingON3rdPayments.do?method=listPayments&billingNo=<%=ch1Obj.getId()%>','RAView<%=ch1Obj.getId()%>');return false;">
					 <%=payment.getValue()%>
				 </a><br/>
				 <%
					             } 
					         }
						} else {
				 %>
				 <a href="javascript: function myFunction() {return false; }"  onclick="javascript:popupPage(800,860,'billingON3rdPayments.do?method=listPayments&billingNo=<%=ch1Obj.getId()%>','RAView<%=ch1Obj.getId()%>');return false;">
					 <%=amountPaid%> <br/>
				 </a>
				 <%
					     }
					 } else {
				 %>
				 <a href="javascript: function myFunction() {return false; }"  onclick="javascript:popupPage(800,860,'billingRAView.jsp?billing_no=<%=ch1Obj.getId()%>','RAView<%=ch1Obj.getId()%>');return false;">
					 <%=amountPaid%>
				 </a>
				 <% } %>
			 </td><!--PAID-->
             <td align="center"><%=adj.toString()%></td> <!--ADJ-->
			  <td align="center"><%=del.toString()%></td> <!--DEL-->
             <td align="center"><%=getHtmlSpace(ch1Obj.getRec_id())%></td><!--DX1-->
             <!--td>&nbsp;</td--><!--DX2-->
             <td align="center"><%=payProgram%></td>
             <td align="center"><a href=#  onclick="popupPage(800,700,'billingONCorrection.jsp?billing_no=<%=ch1Obj.getId()%>','BillCorrection<%=ch1Obj.getId()%>');nav_colour_swap(this.id, <%=bList.size()%>);return false;"><%=ch1Obj.getId()%></a></td><!--ACCOUNT-->
             <td class="highlightBox"><a id="A<%=i%>" href=#  onclick="popupPage(800,700,'billingONCorrection.jsp?billing_no=<%=ch1Obj.getId()%>','BillCorrection<%=ch1Obj.getId()%>');nav_colour_swap(this.id, <%=bList.size()%>);return false;">Edit</a> <%=errorCode%></td><!--MESSAGES-->
			  <% StringBuilder paymentMethod = new StringBuilder(payProgram);
				  if (payProgram.matches("PAT|OCF|ODS|CPP|STD|IFH")) {
					  paymentMethod = new StringBuilder();
					  for (Map.Entry<Integer, BigDecimal> payment : paymentTotals.entrySet()) {
						  if (isDisplayPaymentMethodColumnsInInvoiceReportEnabled) {
							  String paymentAmount = payment.getValue() == null ? "0.00"
									  : payment.getValue().toString();
			  %>
			  <td align="center"><%= paymentAmount %></td>
			  		   <% } else {
							  if (payment.getValue() != null) {
								  paymentMethod.append(
										  paymentTypeDao.find(payment.getKey()).getPaymentType()).append("<br/>");
							  } else if (amountPaid == null || amountPaid.equals("0.00")) {
								  paymentMethod = new StringBuilder("------");
							  }
						  }
					  }
					  if (!isDisplayPaymentMethodColumnsInInvoiceReportEnabled) { %>
			  			<td align="center"><%= paymentMethod.toString() %>
				   <% }
				  } else {
					  if (isDisplayPaymentMethodColumnsInInvoiceReportEnabled) {
						  for (int j = 0; j < paymentTotals.size() - 1; j++) { %>
			  					<td align="center">0.00</td>
			  		   <% }
					  } else {
						  paymentMethod.append("<br/>"); %>
			  			  <td align="center"><%= paymentMethod.toString() %></td>
			       <% } %>
			  <% } %>
             <td align="center"><%=qty %></td>
             <td align="center"><%=Encode.forHtml(ch1Obj.getProviderName())%></td>
			  	<% 
				 if (bMultisites) {
					String bgColor = "";
					if (!(StringUtils.isNullOrEmpty(ch1Obj.getClinic()) || ch1Obj.getClinic().equalsIgnoreCase("null")) 
							&& !StringUtils.isNullOrEmpty(siteBgColor.get(ch1Obj.getClinic()))) {
						bgColor = "style='background-color:" + siteBgColor.get(ch1Obj.getClinic()) + "'";
					 }
				%>
				 <td <%=bgColor%>>
				 	<%=(ch1Obj.getClinic()== null || ch1Obj.getClinic().equalsIgnoreCase("null") ? "" : siteShortName.get(ch1Obj.getClinic()))%>
				 </td>     <!--SITE-->          
        	<% }%>   
             <td align="center" class="hidden-print">
                 <% if (newInvoice && b3rdParty) { %>
                 <input type="checkbox" name="invoiceAction" id="invoiceAction<%=invoiceNo%>" value="<%=invoiceNo%>"/>
                 <% }%>
             </td><!--ACTION-->
          </tr>
       <% } %>  
       
          <tr class="warning"> 
             <td>Count:</td>  
             <td align="center"><%=patientCount%></td> 
             <td align="center" class="<%=hideName?"hidden-print":""%>">&nbsp;</td> 
             <td>&nbsp;</td> <!--LOCATION-->
             <td>&nbsp;</td> <!--STAT-->
             <td>&nbsp;</td>
             <td>&nbsp;</td>
             <td>Total:</td><!--CODE-->
             <td align="right"><%=total.toString()%></td><!--BILLED-->
             <td align="right"><%=paidTotal.toString()%></td><!--PAID-->
             <td align="right"><%=adjTotal.toString()%></td>
			  <td align="right"><%=delTotal.toString()%></td><!--ADJUSTMENTS-->
             <td>&nbsp;</td><!--DX-->
             <td>&nbsp;</td><!--TYPE-->
             <td>&nbsp;</td><!--ACCOUNT-->
             <td>&nbsp;</td><!--MESSAGES-->
             <td align="center">&nbsp;</td>
             <td align="center">&nbsp;</td>
             <td>&nbsp;</td><!--PROVIDER-->
             <% if (bMultisites) {%>
				 <td>&nbsp;</td><!--SITE-->          
        	<% }%>  
             <td align="center" class="hidden-print"><a href="#" onClick="submitForm('print')"><bean:message key="billing.billingStatus.print"/></a> 
                                <a href="#" onClick="submitForm('email')"><bean:message key="billing.billingStatus.email"/></a>
             </td>
          </tr>
</tbody>
       </table>
		   <% 
		   String path = (request.getRequestURL().append('?').append(request.getQueryString())).toString();
		   
		   if (path.contains("&page=")) {
		   		path = path.replaceAll("&page=\\d*", "");
		   }
		   
		   for (int invoicePage = 1; invoicePage <= Math.ceil(totalRecords / (double)resultsPerPage); invoicePage++) {
		   			if (invoicePage != pageNumber) {%>
		   <a href="<%=path + "&page=" + invoicePage%>"><%=invoicePage%></a>
		   <% 
		   			} else {%>
		   				<b><%=invoicePage%></b>
		   			<%} 
		   }%>
		Total Records: <%=totalRecords%>   
       <%if(bList != null && !bList.isEmpty()) {%> 
     	  <a href="#" onclick="submitForm('excel');">Export to Excel</a>
		  <a href="#" onclick="submitForm('csv');">Export to CSV</a>
     	  
     	  
       <%} %>
<% } %>
    </form>    
    </table>
    </form>
    </div>
</div>

<script language='javascript'>
    var startDate = $("#xml_vdate").datepicker({
		format : "yyyy-mm-dd",
		todayBtn: 'linked',
		autoclose: true,
	});
	var endDate = $("#xml_appointment_date").datepicker({
		format : "yyyy-mm-dd",
		todayBtn: 'linked',
		autoclose: true,
	});
	
	var paymentStartDate = $("#paymentStartDate").datepicker({
		format : "yyyy-mm-dd",
		todayBtn: 'linked',
		autoclose: true,
	});
	var paymentEndDate = $("#paymentEndDate").datepicker({
		format : "yyyy-mm-dd",
		todayBtn: 'linked',
		autoclose: true,
	});
	
    $( document ).ready(function() {
    	parent.parent.resizeIframe($('html').height());
    });
	
	$("input[name='serviceCode']").click(function(){
		console.log("test");
		if(this.value == "%"){
			this.value = "";
		}
	});

	function updateProviderDateRange() {
		let providerDateRangeInp = $('#providerDateRange');
		let hideProviderDateRangeInp = $('#hideProviderDateRange');
		let hideProviderDateRange = $('#hideProviderDateRangeCheck')[0].checked;

		hideProviderDateRangeInp.val(hideProviderDateRange);
		providerDateRangeInp.removeClass(hideProviderDateRange ? 'show-print' : 'hide-print');
		providerDateRangeInp.addClass(hideProviderDateRange ? 'hide-print' : 'show-print');
	}
</script>
    </body>
    <%! String getStdCurr(String s) {
    		if(s != null) {
    			if(s.indexOf(".") >= 0) {
    				s += "00".substring(0, 3-s.length()+s.indexOf("."));
    			} else {
    				s = s + ".00";
    			}
    		}
    		return s;
    }
    String getHtmlSpace(String s) {
    	String ret = s==null? "&nbsp;" : s;
    	return ret;
    }
    String ch2StdCurrFromNoDot(String s) {
		if(s != null) {
			if(s.indexOf(".") <= 0) {
				if(s.length()>2) {
					s = s.substring(0, (s.length()-2)) + "." + s.substring((s.length()-2)) ;
				} else if(s.length()==1) {
					s = "0.0" + s;
				} else {
					s = "0." + s;
				}
			}
		}
		return s;
	}
    %>
</html>
