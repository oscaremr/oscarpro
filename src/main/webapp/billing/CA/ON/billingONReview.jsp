<%--

    Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

--%>
<%@page import="org.oscarehr.common.dao.BillingServiceDao"%>
<%@page import="org.oscarehr.common.dao.DemographicDao"%>
<%@page import="org.oscarehr.common.model.Demographic"%>
<%@page import="org.oscarehr.common.model.Provider"%>
<%@page import="org.oscarehr.PMmodule.dao.ProviderDao"%>
<%! boolean bMultisites = org.oscarehr.common.IsPropertiesOn.isMultisitesEnable(); %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ page
	import="java.util.*,java.math.*,java.net.*,java.sql.*,oscar.util.*,oscar.*,oscar.appt.*"%>
<%@ page import="oscar.oscarBilling.ca.on.administration.*"%>
<%@ page import="oscar.oscarBilling.ca.on.data.*"%>
<%@ page import="oscar.oscarBilling.ca.on.pageUtil.*, java.util.Properties"%>
<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<% java.util.Properties oscarVariables = OscarProperties.getInstance(); %>
<jsp:useBean id="providerBean" class="java.util.Properties" scope="session" />
<%@ page import="org.oscarehr.util.SpringUtils"%>
<%@ page import="org.oscarehr.common.model.DiagnosticCode"%>
<%@ page import="org.oscarehr.common.dao.DiagnosticCodeDao"%>
<%@ page import="org.oscarehr.common.dao.BillingONCHeader1Dao, org.oscarehr.common.model.BillingONCHeader1"%>
<%@ page import="org.oscarehr.common.dao.UserPropertyDAO, org.oscarehr.common.model.UserProperty"%>
<%@ page import="org.oscarehr.common.dao.DxresearchDAO, org.oscarehr.common.model.Dxresearch"%>
<%@ page import="org.oscarehr.common.dao.Icd9Dao, org.oscarehr.common.model.Icd9" %>
<%@ page import="org.oscarehr.common.dao.DemographicExtDao, org.oscarehr.common.model.DemographicExt" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.js"></script>
<script>
    jQuery.noConflict();
</script>


<%
	DiagnosticCodeDao diagnosticCodeDao = SpringUtils.getBean(DiagnosticCodeDao.class);
	BillingONCHeader1Dao billingONCHeader1Dao = (BillingONCHeader1Dao) SpringUtils.getBean("billingONCHeader1Dao");
	UserPropertyDAO userPropertyDao = SpringUtils.getBean(UserPropertyDAO.class);
%>
<%//
	if (session.getAttribute("user") == null) {
		response.sendRedirect("../../../logout.jsp");
	}
	OscarProperties props = OscarProperties.getInstance();

	String loggedInProviderNo = (String) session.getAttribute("user");
			String providerview = request.getParameter("providerview") == null ? "" : request
					.getParameter("providerview");
			String asstProvider_no = "";
			String color = "";
			String premiumFlag = "";
			String service_form = "";
			
	boolean addDxOnSave = Boolean.parseBoolean(props.getProperty("add_dx_on_bill"));
	boolean removePatientDetailInThirdPartyInvoice = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("remove_patient_detail_in_third_party", false);
	boolean billNoteEnabled = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("billing_review_auto_payment_enabled", false);
%>

<%
Properties gstProp = new Properties();
GstControlAction db = new GstControlAction();
GstReport gstRep = new GstReport();
gstProp = db.readDatabase();
String gstFlag;
String flag = gstProp.getProperty("gstFlag", "");
String percent = gstProp.getProperty("gstPercent", "");
BigDecimal stotal = new BigDecimal(0);
BigDecimal gstTotal = new BigDecimal(0);
BigDecimal gstbilledtotal = new BigDecimal(0);
boolean dupServiceCode = false;
%>

<%//
	BillingReviewPrep prepObj = new BillingReviewPrep();
	@SuppressWarnings("unchecked")
	Vector<String>[] vecServiceParam = new Vector[3];
	if(oscarVariables.getProperty("onBillingSingleClick", "").equals("yes")) {
		vecServiceParam[0] = new Vector<String>();
		vecServiceParam[1] = new Vector<String>();
		vecServiceParam[2] = new Vector<String>();
	} else {
		vecServiceParam = prepObj.getRequestFormCodeVec(request, "xml_", "1", "1");
	}

	Vector<String>[] vecServiceParam0 = prepObj.getRequestCodeVec(request, "serviceCode", "serviceUnit", "serviceAt", BillingDataHlp.FIELD_SERVICE_NUM);
	vecServiceParam[0].addAll(vecServiceParam0[0]);
	vecServiceParam[1].addAll(vecServiceParam0[1]);
	vecServiceParam[2].addAll(vecServiceParam0[2]);

        //Check whether there are duplicated service code existing
        //User double click a service code, and then check off that
        //service code in billingON page will cause duplicated service
        //code in billing review page.
        TreeMap<String,Integer> mapServiceParam = new TreeMap<String,Integer>();
        for (int i = 0 ; i< vecServiceParam[0].size(); i++){
            mapServiceParam.put(vecServiceParam[0].get(i),i);
        }
        if (mapServiceParam.size()!=vecServiceParam[0].size())
            dupServiceCode = true;

        /////// hack used to order the billing codes
        /////// Would make sense to change getServiceCodeReviewVec method to accept the hashtable
        /////// But should cause that much of a performance hit. It's generally under 3 items
        String billReferalDate = request.getParameter("service_date");
        Vector v = new Vector();
        for (int ii = 0 ; ii< vecServiceParam[0].size(); ii++){
            Hashtable h = new Hashtable();
            h.put("serviceCode",vecServiceParam[0].get(ii));
            h.put("serviceUnit",vecServiceParam[1].get(ii));
            h.put("serviceAt",vecServiceParam[2].get(ii));
            h.put("billReferenceDate", billReferalDate);
            v.add(h);
        }

        Collections.sort(v,new BillingSortComparator());

        vecServiceParam[0] = new Vector();
        vecServiceParam[1] = new Vector();
        vecServiceParam[2] = new Vector();

        for (int ii = 0; ii < v.size(); ii++){
            Hashtable h = (Hashtable) v.get(ii);
            vecServiceParam[0].add((String) h.get("serviceCode") );
            vecServiceParam[1].add((String) h.get("serviceUnit"));
            vecServiceParam[2].add((String) h.get("serviceAt"));
        }
        ///////--------

    String warningMsg = "";
	String errorFlag = "";
	String errorMsg = "";
        
	Vector vecCodeItem = prepObj.getServiceCodeReviewVec(vecServiceParam[0], vecServiceParam[1],vecServiceParam[2],billReferalDate);
	Vector vecPercCodeItem = prepObj.getPercCodeReviewVec(vecServiceParam[0], vecServiceParam[1], vecCodeItem,billReferalDate);  //LINE CAUSING ERROR


        Properties propCodeDesc = (new JdbcBillingCodeImpl()).getCodeDescByNames(vecServiceParam[0]);
		String demo_no = request.getParameter("demographic_no");
		String dxCode = request.getParameter("dxCode");
		String icd9Code = "";
		String icd9Desc = "";
		if (addDxOnSave) {
			if (dxCode != null) dxCode = dxCode.trim();

			
			if (dxCode != null && !dxCode.isEmpty()) {
				DxresearchDAO dxresearchDao = SpringUtils.getBean(DxresearchDAO.class);
				Icd9Dao icd9Dao = SpringUtils.getBean(Icd9Dao.class);
				DemographicExtDao demoExtDao = SpringUtils.getBean(DemographicExtDao.class);

				//load codelists to add to patientDx
				UserProperty codeListProp = userPropertyDao.getProp(UserProperty.CODE_TO_ADD_PATIENTDX);
				if (codeListProp != null) {
					String codeList = codeListProp.getValue();
					if (codeList != null) {
						String[] codes = codeList.replace(" ", "").split(","); //remove spaces & split
						if (Arrays.asList(codes).contains(dxCode)) icd9Code = dxCode;
					}
				}
				if (icd9Code.isEmpty()) {
					codeListProp = userPropertyDao.getProp(UserProperty.CODE_TO_MATCH_PATIENTDX);
					if (codeListProp != null) {
						String codeList = codeListProp.getValue();
						if (codeList != null) {
							String[] codes = codeList.replace(" ", "").split(","); //remove spaces & split
							for (String codePair : codes) {
								String[] code = codePair.split(":");
								if (dxCode.equals(code[0])) {
									icd9Code = code[1];
									break;
								}
							}
						}
					}
				}
				
				if (!icd9Code.isEmpty()) {

					DemographicExt demoExt = demoExtDao.getDemographicExt(Integer.parseInt(demo_no), "code_to_avoid_patientDx");
					if (demoExt != null) {
						String codeList = demoExt.getValue();

						if (codeList != null) {
							String[] codes = codeList.replace(" ", "").split(","); //remove spaces & split
							if (Arrays.asList(codes).contains(icd9Code + "x3")) icd9Code = "";
						}
					}
				}

				//match patientDx
				if (!icd9Code.isEmpty()) {
					List<Dxresearch> dxList = dxresearchDao.getByDemographicNo(Integer.parseInt(demo_no));
					for (Dxresearch dx : dxList) {
						if ("icd9".equals(dx.getCodingSystem()) && icd9Code.equals(dx.getDxresearchCode())) {
							icd9Code = "";
							break;
						}
					}
				}

				//load icd9 description
				if (!icd9Code.isEmpty()) {
					Icd9 icd9 = icd9Dao.findByCode(icd9Code);
					if (icd9 != null) icd9Desc = icd9.getDescription();
				}
			}
		}
			String dxDesc = prepObj.getDxDescription(dxCode);
			String clinicview = oscarVariables.getProperty("clinic_view", "");
			String clinicNo = oscarVariables.getProperty("clinic_no", "");
			String visitType = oscarVariables.getProperty("visit_type", "");
			String appt_no = request.getParameter("appointment_no");
			String demoname = request.getParameter("demographic_name");
			String apptProvider_no = request.getParameter("apptProvider_no");
			String ctlBillForm = request.getParameter("billForm");
			String assgProvider_no = request.getParameter("assgProvider_no");
			String billType = request.getParameter("xml_billtype").substring(0,((String)request.getParameter("xml_billtype")).indexOf("|")).trim();
			String demoSex = request.getParameter("DemoSex");
			GregorianCalendar now = new GregorianCalendar();
			int curYear = now.get(Calendar.YEAR);
			int curMonth = (now.get(Calendar.MONTH) + 1);
			int curDay = now.get(Calendar.DAY_OF_MONTH);
			int dob_year = 0, dob_month = 0, dob_date = 0, age = 0;
			String content = "";
			String total = "";

			String msg = "<tr><td colspan='2'>Calculation</td></tr>";
			String action = "edit";
			Properties propHist = null;
			Vector vecHist = new Vector();
			// get provider's detail
			String proOHIPNO = "", proRMA = "";
			
			ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
			providerview = StringUtils.noNull(request.getParameter("xml_provider"));
			if (providerview.contains("|")) {
				providerview = providerview.substring(0, providerview.indexOf("|"));
			}
			Provider ppp = providerDao.getProvider(providerview);
			if (ppp != null) {
				proOHIPNO = ppp.getOhipNo(); 
				proRMA = ppp.getRmaNo();
			}
			// get patient's detail
			String r_doctor = "", r_doctor_ohip = "";
			String demoFirst = "", demoLast = "", demoHIN = "", demoVer = "", demoDOB = "", demoDOBYY = "", demoDOBMM = "", demoDOBDD = "", demoHCTYPE = "";
			String strPatientAddr = "";
			
			DemographicDao demoDao = SpringUtils.getBean(DemographicDao.class);
			Demographic demo = demoDao.getDemographic(demo_no);
			if (demo != null) {
				strPatientAddr = demo.getFirstName() + " " + demo.getLastName() + "\n"
				+ demo.getAddress() + "\n"
				+ demo.getCity() + ", " + demo.getProvince() + "\n"
				+ demo.getPostal() + "\n"
				+ "Tel: " + demo.getPreferredPhone();

				assgProvider_no = demo.getProviderNo();
				demoFirst = demo.getFirstName();
				demoLast = demo.getLastName();
				demoHIN = demo.getHin();
				demoVer = demo.getVer();
				demoSex = demo.getSex();
				if (demoSex.compareTo("M") == 0)
					demoSex = "1";
				if (demoSex.compareTo("F") == 0)
					demoSex = "2";

				demoHCTYPE = demo.getHcType() == null ? "" : demo.getHcType();
				if (demoHCTYPE.compareTo("") == 0 || demoHCTYPE == null || demoHCTYPE.length() < 2) {
					demoHCTYPE = "ON";
				} else {
					demoHCTYPE = demoHCTYPE.substring(0, 2).toUpperCase();
				}
				demoDOBYY = demo.getYearOfBirth();
				demoDOBMM = demo.getMonthOfBirth();
				demoDOBDD = demo.getDateOfBirth();
				r_doctor = demo.getReferralPhysicianName().isEmpty() ? "" : demo.getReferralPhysicianName();
				r_doctor_ohip = demo.getReferralPhysicianOhip().isEmpty() ? "" : demo.getReferralPhysicianOhip();

				demoDOBMM = demoDOBMM.length() == 1 ? ("0" + demoDOBMM) : demoDOBMM;
				demoDOBDD = demoDOBDD.length() == 1 ? ("0" + demoDOBDD) : demoDOBDD;
				demoDOB = demoDOBYY + demoDOBMM + demoDOBDD;

				if ("".equals(demoFirst)) {
					warningMsg += "<br/><div class='myError'>Warning: The patient does not have a first name. </div><br/>";
				}
				if (demo.getHin() == null) {
					errorFlag = "1";
					errorMsg = errorMsg
							+ "<br><div class='myError'>Error: The patient does not have a valid HIN. </div><br>";
				} else if (demo.getHin().equals("")) {
					warningMsg += "<br><div class='myError'>Warning: The patient does not have a valid HIN. </div><br>";
				}
				if (r_doctor_ohip != null && r_doctor_ohip.length() > 0 && r_doctor_ohip.length() != 6) {
					warningMsg += "<br><div class='myError'>Warning: the referral doctor's no is wrong. </div><br>";
				}
				if (demoDOB.length() != 8) {
					errorFlag = "1";
					errorMsg = errorMsg
							+ "<br><div class='myError'>Error: The patient does not have a valid DOB. </div><br>";
				}
			}


			// create msg
			String wrongMsg = errorMsg + warningMsg;
			
			boolean disableAddToRegistry = Boolean.parseBoolean(oscarVariables.getProperty("disable_add_to_registry"));
			%>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
<c:set var="demographicNo" value="${param.demographic_no}" scope="request"/>
<oscar:customInterface section="billingreview"/>

<%@page import="org.oscarehr.common.dao.SiteDao"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.oscarehr.common.model.Site"%>
<%@ page import="oscar.oscarProvider.data.HcTypeBillToRemitToPreference" %>
<%@ page import="oscar.oscarProvider.data.DefaultHcTypeBillToRemitToPreferenceService" %>
<%@ page import="oscar.oscarRx.data.RxProviderData" %>
<%@page import="org.oscarehr.common.model.Site"%>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="oscar.oscarBilling.ca.shared.administration.GstControlAction" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>OscarBilling</title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="billingON.css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.js"></script>
   <script>
     jQuery.noConflict();
   </script>
<oscar:customInterface section="billingReview"/>
<script language="JavaScript">
        ctx = "<c:out value="${ctx}"/>";
    	demographicNo = "<c:out value="${demographicNo}"/>";
    
		var bClick = false;
	    
		function onSave() {
		    <% if (addDxOnSave) { %>
			addToDiseaseRegistry();
			<% } %>
			var value=jQuery("#payee").val();
			jQuery("#payeename").val(value);
            var ret = checkTotal();
            
            if (ret) {
                jQuery("input[type='submit'][name='submit']").hide();
                jQuery("#isSaving").show();
			} 
            bClick = false;

            return ret;
        }
	    
	    function onClickSave() {
			bClick = true;
	    }
		
	    function popupPage(vheight,vwidth,varpage) {
		  var page = "" + varpage;
		  windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=0,screenY=0,top=0,left=0";
		  var popup=window.open(page, "billcorrection", windowprops);
		    if (popup != null) {
		    if (popup.opener == null) {
		      popup.opener = self;
		    }
		    popup.focus();
		  }
		}
		function settlePayment() {
		  document.forms[0].payment.value = document.forms[0].total.value;
		}

		function scriptAttach(elementName) {
		     var d = elementName;
		     //t0 = escape("document.forms[0].elements[\'"+d+"\'].value");
		     t0 = d;
		     popupPage('600', '700', 'onSearch3rdBillAddr.jsp?param='+t0);
		}
                function showtotal(){
                    var subtotal = document.getElementById("total").value;
                    //subtotal = subtotal * 1 + document.getElementById("gst").value * 1;
                    var element = document.getElementById("stotal");
                    if( element != null )
                        element.value = subtotal;
                }
                
                function validatePaymentNumberic(idx) {
                	var oldVal = document.getElementById("percCodeSubtotal_" + idx).value;
                	var val = document.getElementById("paid_" + idx).value;
                	/* if (val.length == 0) {
                		document.getElementById("paid_" + idx).value = "0.00";
                		oldVal = "0.00";
                		return;
                	} */
                	//var regexNumberic = /^([1-9]\d*|0)(\.\d{1,2})?$/;
                	var regexNumberic = /^([1-9]\d{0,9}|0)(\.\d{1,2})?$/;
                	if (!regexNumberic.test(val)) {
                		document.getElementById("paid_" + idx).value = oldVal;
                		alert("Please enter digital numbers !");
                		return;
                	}
                	oldVal = val;
		}


                function validateDiscountNumberic(idx) {
                	var oldVal = "0.00";
                	var val = document.getElementById("discount_" + idx).value;
                	if (val.length == 0) {
                		document.getElementById("discount_" + idx).value = "0.00";
                		oldVal = "0.00";
                		return;
                	}
                	//var regexNumberic = /^([1-9]\d*|0)(\.\d{1,2})?$/;
                	var regexNumberic = /^([1-9]\d{0,9}|0)(\.\d{1,2})?$/;
                	if (!regexNumberic.test(val)) {
                		document.getElementById("discount_" + idx).value = oldVal;
                		alert("Please enter digital numbers !");
                		return;
                	}
                	oldVal = val;
                }
                
                function validateFeeNumberic(idx) {
                	var oldVal = "0.00";
                	var val = document.getElementById("percCodeSubtotal_" + idx).value;
                	if (val.length == 0) {
                		document.getElementById("percCodeSubtotal_" + idx).value = "0.00";
                		oldVal = "0.00";
                		return;
                	}
                	//var regexNumberic = /^([1-9]\d*|0)(\.\d{1,2})?$/;
                	var regexNumberic = /^([1-9]\d{0,9}|0)(\.\d{1,2})?$/;
                	if (!regexNumberic.test(val)) {
                		document.getElementById("percCodeSubtotal_" + idx).value = oldVal;
                		alert("Please enter digital numbers !");
                		return;
                	}
                	else {
                        document.getElementById("percCodeSubtotal_" + idx).value = parseFloat(val).toFixed(2);
					}
                	oldVal = val;
                }


                
       function updateElement(eId, data) {
    	   jQuery("#"+eId).val(data);
       }
       
       function checkTotal() {
    	   var totValue = document.getElementById("total").value;
    	   if(isNaN(totValue)) {
    		   alert("Please enter a valid fee");
    		   return false;
    	   }
    	   return true;
       }
       
       function updateTotal(e) {
    	   var editedValue = e.value;
    	   if( isNaN(editedValue) ) {
    		   alert("Please enter a valid fee");
    		   e.focus();
    	   }
    	   else {
    			var codeFees;
    			var unit;
    			var total = 0.0;
    			var idx = 0;
    			var displayTotal = "0.00";
    			
    			while( (codeFees = document.getElementById("percCodeSubtotal_" + idx)) ) {    				    			
    				total += parseInt(codeFees.value);
    				++idx;
    			}
    		    			
    			updateElement("total", formatTotal(total));    			    			
    			total += new Number(jQuery("#gst").val());
    			updateElement("gstBilledTotal", formatTotal(total));
    			
    	   }
    	   
       }
       
       function formatTotal(total) {
    	   var displayTotal = "0.00";
    	   var decimal = total % 1;
    	   
    	   if( decimal == 0 ) {
				displayTotal = total + ".00";
			}
			else if( (decimal*10) % 1 < 0.1 ) {
				displayTotal = total + "0";
			}
			else {
				displayTotal = total;
			}
    	   
    	   return displayTotal;
       }
       
       function checkPaymentMethod(settle) {
    	   var payMethods = document.getElementsByName("payMethod");
    	   var checkedMethod = false;
    	   
    	   if( settle != "Settle" && document.forms[0].payment.value == 0 ) {
    		   return true;
    	   }
    	   
    	   
    	   for( var idx = 0; idx < payMethods.length; ++idx ) {
    		   if( payMethods[idx].checked ) {
    			   checkedMethod = true;
    			   break;
    		   }
    	   }
    	   
    	   if( !checkedMethod ) {
    		   alert("Please select a payment method");
    	   }
    	   else if( settle == "Settle") {
    		   document.forms['titlesearch'].btnPressed.value='Settle';
    		   document.forms['titlesearch'].submit();
    		   popupPage(700,720,'billingON3rdInv.jsp');
    	   }
    	   
    	   return checkedMethod;
    	   
       }

	//-->

</script>

<style type="text/css">
div.wrapper{
    background-color: #eeeeff;
    margin-top:0px;
    padding-top:0px;
    margin-bottom:0px;
    padding-bottom:0px;
}

div.wrapper br{
    clear: left;
}

div.wrapper ul{
    width: 80%;
    background-color: #eeeeff;
    list-style:none;
    list-style-type:none;
    list-style-position:outside;
    padding-left:1px;
    margin-left:1px;
    margin-top:0px;
    padding-top:1px;
    margin-bottom:0px;
    padding-bottom:0px;
}

div.wrapper ul li{
    background-color: #eeeeff;
}

div.dxBox{
    width:90%;
    background-color: #eeeeff;
    margin-top: 2px;
    margin-left:3px;
    margin-right:3px;
    margin-bottom:0px;
    padding-bottom:0px;
    float: left;
}


div.dxBox h3 {
    background-color: #ccccff;
  /*font-size: 1.25em;*/
    font-size: 10pt;
    font-variant:small-caps;
    font-weight: bold;
    margin-top:0px;
    padding-top:0px;
    margin-bottom:0px;
    padding-bottom:0px;
}


div.dxBox form {
    margin-top:0px;
    padding-top:0px;
    margin-bottom: 0px;
    padding-bottom: 0px;
}

div.dxBox input {
    margin-top:0px;
    padding-top:0px;
    margin-bottom: 0px;
    padding-bottom: 0px;
}

</style>
<script type="text/javascript" src="../../../share/javascript/prototype.js"></script>
<script type="text/javascript" src="../../../share/javascript/nifty.js"></script>
<link rel="stylesheet" type="text/css" href="../../../share/css/niftyCorners.css" />
<link rel="stylesheet" type="text/css" href="../../../share/css/niftyPrint.css" media="print"/>

<script type="text/javascript">
window.onload=function(){
          if(!NiftyCheck())
                      return;
          Rounded("div.dxBox","top","transparent","#CCCCFF","small border #CCCCFF");
          Rounded("div.dxBox","bottom","transparent","#EEEEFF","small border #CCCCFF");
}
</script>

</head>

<body topmargin="0" onload="showtotal();calculatePayment(); <%= addDxOnSave ? "confirmDialog()" : "" %>">

<form method="post" name="titlesearch" action="billingONSave.jsp" onsubmit="return onSave();">
    <input type="hidden" name="url_back" value="<%=request.getParameter("url_back")%>">
    <input type="hidden" name="billNo_old" id="billNo_old" value="<%=request.getParameter("billNo_old")%>" />
	<input type="hidden" name="billStatus_old" id="billStatus_old" value="<%=request.getParameter("billStatus_old")%>" />
	<input type="hidden" name="billForm" id="billForm" value="<%=request.getParameter("billForm")%>" />
    <input type="hidden" name="payeename" id="payeename" value="" />
    <input type="hidden" name="xml_location" id="xml_location" value="<%=request.getParameter("xml_location")%>" />
<table border="0" cellpadding="0" cellspacing="2" width="100%" class="myIvory">
	<tr>
		<td>
		<table border="0" cellspacing="0" cellpadding="0" width="100%" class="myDarkGreen">
			<tr>
				<td><b><font color="#FFFFFF">&nbsp;Confirmation </font></b></td>
				<td align="right"><input type="hidden" name="addition" value="Confirm" /></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
		<table border="0" cellspacing="0" cellpadding="0" width="100%" class="myYellow">
			<tr>
				<td nowrap width="10%" align="center"><%=demoname%> <%=demoSex.equals("1") ? "Male" : "Female"%>
				<%=" DOB: " + demoDOBYY + "/" + demoDOBMM + "/" + demoDOBDD + " HIN: " + demoHIN + "" + demoVer%>
				</td>
				<td align="center"><%=wrongMsg%></td>
			</tr>
		</table>

		<table border="1" cellspacing="0" cellpadding="0" width="100%" bordercolorlight="#99A005" bordercolordark="#FFFFFF"
			 class="myIvory">
			<tr>
				<td width="50%">

				<table border="1" cellspacing="2" cellpadding="0" width="100%" bordercolorlight="#99A005" bordercolordark="#FFFFFF"
					>
					<tr>
						<!--<input type="text" name="checkFlag" id="checkFlag" value="<%=request.getParameter("checkFlag") %>" />  -->
						<td nowrap width="30%" align="center" ><b>Service Date</b><br>
						<%=request.getParameter("service_date").replaceAll("\\n", "<br>")%></td>
						<td align="center" width="33%"><b>Diagnostic Code</b><br>
						<%=dxCode%></br>
						<%=dxDesc%>
						</td>
						<td valign="top"><b>Refer. Doctor</b><br>
						<%=request.getParameter("referralDocName")%><br>
						<b>Refer. Doctor #</b><br>
						<%=request.getParameter("referralCode")%></td>
					</tr>
				</table>

				</td>
				<td valign="top">

				<table border="1" cellspacing="2" cellpadding="0" width="100%" bordercolorlight="#99A005" bordercolordark="#FFFFFF"
					 class="myGreen">
					<tr>
						<td nowrap width="30%"><b>Billing Physician</b></td>
						<td width="20%"><%=Encode.forHtmlContent(providerBean.getProperty(!providerview.isEmpty() ? providerview : "", ""))%></td>
						<td nowrap width="30%"><b>Assig. Physician</b></td>
						<td width="20%"><%=assgProvider_no == null ? "N/A" : Encode.forHtmlContent(providerBean.getProperty(assgProvider_no, ""))%></td>
					</tr>
					<tr>

						<td width="30%"><b>Visit Type</b></td>
						<td width="20%"><%=request.getParameter("xml_visittype").substring(
							request.getParameter("xml_visittype").indexOf("|") + 1)%>
						</td>

						<td width="30%"><b>Billing Type</b></td>
						<td width="20%"><%=request.getParameter("xml_billtype").substring(
							request.getParameter("xml_billtype").indexOf("|") + 1)%>
						</td>
					</tr>
					<tr>
						<td><b>Site</b></td>
						<td><%=request.getParameter("xml_location").substring(
							request.getParameter("xml_location").indexOf("|") + 1)%> &nbsp;
							<% if(request.getParameter("m_review")!=null) { out.println("<b>Manual: Y</b>"); } %>
							</td>

					<% if (bMultisites) { %>
						<td width="30%"><b>Billing Clinic</b></td>
						<td width="20%" nowrap="nowrap"><%=request.getParameter("site")%>
						</td>
					<% } %>
					</tr>
					<tr>
                   		<td><b>SLI Code</b></td>
                        <td><%=request.getParameter("xml_slicode").substring(request.getParameter("xml_slicode").indexOf("|") + 1)%> &nbsp;</td>
                    </tr>
					<tr>
						<td><b>Admission Date</b></td>
						<td><%=request.getParameter("xml_vdate")%></td>
						<td colspan="2"></td>

					</tr>
				</table>


				</td>
			</tr>
		</table>

		</td>

	</tr>
	<tr>
		<td align="center">
		<table border="1" width="100%" bordercolorlight="#99A005" bordercolordark="#FFFFFF">
<%  boolean codeValid = true;

    //validation that user hasn't already had billed to OHIP an annual physical this year
    String serviceCodeValue = null;
    int srvCodeIdx = 0;

    //validation of user entered service codes    
    serviceCodeValue = null;
    for (int i = 0; i < BillingDataHlp.FIELD_SERVICE_NUM; i++) {
	serviceCodeValue = request.getParameter("serviceCode" + i);

	if (!serviceCodeValue.equals("")) {
		BillingServiceDao billingServiceDao = SpringUtils.getBean(BillingServiceDao.class); 
		
		List<Object> svcCodes = billingServiceDao.findBillingCodesByCodeAndTerminationDate(serviceCodeValue.trim().replaceAll("_","\\_"),
				ConversionUtils.fromDateString(billReferalDate));
	    
	    if (svcCodes.isEmpty()) {
			codeValid = false;
		%>
		<tr class="myErrorText"><td align=center>
		    &nbsp;<br>
		    Service code "<%=serviceCodeValue%>" is invalid. Please go back to correct it.
		</td></tr>
		<%
	    }
	}
    }

    //validation of diagnostic code (dxcode)
    String dxCodeValue = null;
    String headerDx = "";
    Map<String, String> dxCodes = new HashMap<String, String>();
    for (int i = 0; i < 3; i++) {
	if (i==0) dxCodeValue=dxCode;
	else dxCodeValue=request.getParameter("dxCode" + i);
	if (!dxCodeValue.equals("")) {
		List<DiagnosticCode> dcodes = diagnosticCodeDao.findByDiagnosticCode(dxCodeValue.trim());
		if(dcodes.size() == 0) {
		codeValid = false;
		%>
		<tr class="myErrorText"><td align=center>
		    &nbsp;<br>
		    Diagnostic code "<%=dxCodeValue%>" is invalid. Please go back to correct it.
		</td></tr>
		<%
	    } else {
		    if (i == 0) {
		        headerDx = dxCodeValue.trim();
			}
		    dxCodes.put(dxCodeValue.trim(), StringUtils.noNull(dcodes.get(0).getDescription()));
		}
	}
    }

    if (codeValid) {
%>
			<%--= msg --%>
			<tr class="myYellow">
				<td colspan='3'>Calculation</td>
				<td<%="PAT".equals(billType) ? " width=\"14%\"" : ""%>>Description</td>
				<td width="155px">Dx</td>
				
				<%if("PAT".equals(billType)){%>
				<td colspan='2'>
					Payment
					<span style="float: right"><input type="checkbox" id="calc_paid_all" name="calc_paid_all" onchange="updatePayment()"/></span>
				</td>
				<td colspan='2'>
					Discount
					<span style="float: right"><input type="checkbox" id="calc_discount_all" name="calc_discount_all" onchange="updateDiscount()"/></span>
				</td>
				<%}%>
			</tr>
<%  }
			//Vector[] vecServiceParam = prepObj.getRequestCodeVec(request, "serviceDate", "serviceUnit", "serviceAt", 8);
			//Vector vecCodeItem = prepObj.getServiceCodeReviewVec(vecServiceParam[0], vecServiceParam[1],
			//				vecServiceParam[2]);
			//Vector vecPercCodeItem = prepObj.getPercCodeReviewVec(vecServiceParam[0], vecCodeItem);
				boolean bPerc = false;
				int n = 0;
				int nCode = 0;
				int nPerc = 0;
				Vector vecPercNo = new Vector();
				Vector vecPercMin = new Vector();
				Vector vecPercMax = new Vector();
				for(int i=0; i<vecServiceParam[0].size(); i++) {
					String codeName = vecServiceParam[0].get(i);
					if(nCode<vecCodeItem.size() && codeName.equals( ((BillingReviewCodeItem)vecCodeItem.get(nCode)).getCodeName())) {
						n++;
						String codeDescription = ((BillingReviewCodeItem)vecCodeItem.get(nCode)).getCodeDescription();
						String codeUnit = ((BillingReviewCodeItem)vecCodeItem.get(nCode)).getCodeUnit();
						String codeFee = ((BillingReviewCodeItem)vecCodeItem.get(nCode)).getCodeFee();
						String codeTotal = ((BillingReviewCodeItem)vecCodeItem.get(nCode)).getCodeTotal();
                        String strWarning = ((BillingReviewCodeItem)vecCodeItem.get(nCode)).getMsg();
                        String codeAt = ((BillingReviewCodeItem)vecCodeItem.get(nCode)).getCodeAt();
                                                gstFlag = gstRep.getGstFlag(codeName,billReferalDate);  // Retrieve whether the code has gst involved
                                                BigDecimal cTotal = new BigDecimal(codeTotal);
                                                if ( gstFlag.equals("1") ){   // If it does, update the total with the gst calculated
                                                    BigDecimal perc = new BigDecimal(percent);
                                                    BigDecimal hund = new BigDecimal(100);
                                                    stotal = cTotal;
                                                    stotal = stotal.multiply(perc);
                                                    stotal = stotal.divide(hund);
                                                    gstTotal = gstTotal.add(stotal).setScale(2, BigDecimal.ROUND_HALF_UP);  // Total up GST Charged
                                                    stotal = stotal.add(cTotal).setScale(2, BigDecimal.ROUND_HALF_UP); // Finally update the new codeTotal
                                                    codeTotal = stotal + "";
                                                    BigDecimal temp = new BigDecimal(codeTotal);
                                                    gstbilledtotal = gstbilledtotal.add(temp).setScale(2, BigDecimal.ROUND_HALF_UP);
                                                }
                                                else {
                                                    gstbilledtotal = gstbilledtotal.add(cTotal).setScale(2, BigDecimal.ROUND_HALF_UP);
                                                }
                        if (codeValid) {
			%>
			<tr class="myGreen">
				<td align='center' width='3%'><%=""+n %></td>
				<td align='right' width='12%'><%=codeName %> (<%=codeUnit %>)</td>
				<td>
                    <% if( strWarning.length() > 0 ) { %>
                    <span style="color:red; float:left;"><%=strWarning%></span>
                    <%}%>
                    <span style="float:right;"> <%=codeFee %> x <%=codeUnit %><% if (gstFlag.equals("1")){%> + <%=percent%>% HST<%}%> =
				<input type="text" name="percCodeSubtotal_<%=i %>" size="5" value="<%=codeTotal %>" id="percCodeSubtotal_<%=i %>" onBlur="calculateTotal();" onchange="validateFeeNumberic(<%=i%>)"/>
				<input type="hidden" name="xserviceCode_<%=i %>" value="<%=codeName %>" />
				<input type="hidden" id="xserviceUnit_<%=i %>" name="xserviceUnit_<%=i %>" value="<%=codeUnit %>" />
                    </span>
				</td>
				<td width='<%="PAT".equals(billType) ? "14%" : "25%"%>'><%="PAT".equals(billType) ? propCodeDesc.getProperty(codeName, "") : codeDescription %></td>
				<td>
					<select name="itemDx_<%=i%>" style="width: 100%;max-width:155px;">
						<%
							for (Map.Entry<String, String> dx : dxCodes.entrySet()) {
						%>
						<option value="<%=dx.getKey()%>" <%=headerDx.equals(dx.getKey()) ? "selected=selected" : ""%>><%=dx.getKey() + " - " + dx.getValue()%></option>
						<%
							}
						%>
					</select>
				</td>
				<%
				if("PAT".equals(billType)){
				String paid_value = "0.00";
				%>
				<% if (billNoteEnabled) { %>
				<%paid_value = codeTotal; %>
				<% } %>
				<td nowrap width='3%'><input type="text" id="paid_<%=i%>" name="paid_<%=i %>" value="<%=paid_value%>" onBlur="calculatePayment();" ondblclick="this.value = '<%=codeTotal%>'" onchange="validatePaymentNumberic(<%=i %>)"/></td>
				<td><input type="checkbox" id="calc_paid_<%=i%>" name="calc_paid_<%=i %>" onchange="updatePayment(<%=i%>)" style="float: right"/></td>
				<td nowrap width='3%'><input type="text" id="discount_<%=i%>" name="discount_<%=i %>" value="0.00" onBlur="calculateDiscount();" onchange="validateDiscountNumberic(<%=i %>)"/></td>
				<td><input type="checkbox" id="calc_discount_<%=i%>" name="calc_discount_<%=i %>" onchange="updateDiscount(<%=i%>)" style="float: right"/></td>
				<%}%>
			</tr>
			<%
                        }
						nCode++;
					}
					else if(nPerc<vecPercCodeItem.size() && codeName.equals( ((BillingReviewPercItem)vecPercCodeItem.get(nPerc)).getCodeName())) {
						bPerc = true;
						BillingReviewPercItem percItem = (BillingReviewPercItem)vecPercCodeItem.get(nPerc);
						String percFee = percItem.getCodeFee();
						Vector vecPercFee = percItem.getVecCodeFee();
						Vector vecPercTotal = percItem.getVecCodeTotal();
						Vector vecPerUnit = percItem.getVecCodeUnit();
						String codeUnit = percItem.getCodeUnit();

						if (codeValid) {
			%>
			<tr class="myPink">
				<td align='center' ><%="&nbsp;" %></td>
				<td align='right' ><%=codeName %> (<%=codeName.equals("E400B") ? codeUnit : "1" %>)</td>
				<td align='right'>
						<% }
							
						for(int j=0; j<vecPercTotal.size(); j++) {
							String percTotal =  (Math.round(Float.parseFloat((String)vecPercTotal.get(j)) * 100f) / 100f) + "";
				if (codeValid) {
                                                        %>
						<input type="checkbox" name="percCode_<%=i %>" value="<%=percTotal %>" onclick="onCheckMaster();" /> <%=percTotal %><font size='-2'>(<%=vecPercFee.get(j) %>x<%=percFee %>x<%=vecPerUnit.get(j) %>)</font> |
				<%
                                }
                                                }
                                if (codeValid) {
				%> = <input type="text" name="percCodeSubtotal_<%=i %>" size="5" value="0.00" />
				<input type="hidden" name="xserviceCode_<%=i %>" value="<%=codeName %>" />
				<input type="hidden" name="xserviceUnit_<%=i %>" value="<%=codeUnit %>" />
				</td>
				<td width='25%'><%=propCodeDesc.getProperty(codeName, "") %>
				</td>
				<td>
					<select name="itemDx_<%=i%>" style="width: 100%;max-width:155px;">
						<%
							for (Map.Entry<String, String> dx : dxCodes.entrySet()) {
						%>
						<option value="<%=dx.getKey()%>" <%=headerDx.equals(dx.getKey()) ? "selected=selected" : ""%>><%=dx.getKey() + " - " + dx.getValue()%></option>
						<%
							}
						%>
					</select>
				</td>
			</tr>
			<%
                                }
						nPerc++;
						vecPercNo.add(""+i);
						String nMin = percItem.getCodeMinFee();
						String nMax = percItem.getCodeMaxFee();
						nMin = (nMin == null || "".equals(nMin))? "0" : nMin;
						nMax = (nMax == null || "".equals(nMax))? "9999" : nMax;
						vecPercMin.add(nMin);
						vecPercMax.add(nMax);
					}
				}
                        if (codeValid) {
			%>
			<tr>
				<td align='right' colspan='3' class="myGreen">Total: <input type="text" id="total" name="total" size="5" value="0.00" onchange="onTotalChanged();"/>
				<input type="hidden" name="totalItem" value="<%=vecServiceParam[0].size() %>" /></td>
<script Language="JavaScript">
<!--
function onCheckMaster() {
<%
	for(int i=0; i<vecPercNo.size(); i++) {
		String iCheckNo = (String)vecPercNo.get(i);
%>
	var nSubtotal = 0.00;
	var nMin = <%=vecPercMin.get(i)%>;
	var nMax = <%=vecPercMax.get(i)%>;
    	//alert(":" + document.forms[0].percCode_<%=iCheckNo%>.type);
    if(document.forms[0].percCode_<%=iCheckNo%>.length == undefined) {
		if (document.forms[0].percCode_<%=iCheckNo%>.checked){
			nSubtotal = nSubtotal + eval(document.forms[0].percCode_<%=iCheckNo%>.value*100);
		}
    }
	for (n = 0; n < document.forms[0].percCode_<%=iCheckNo%>.length; n++){
		// If a checkbox has been selected it will return true
		if (document.forms[0].percCode_<%=iCheckNo%>[n].checked){
			nSubtotal = nSubtotal + eval(document.forms[0].percCode_<%=iCheckNo%>[n].value*100);
		}
		//alert(nSubtotal+"here:" +document.forms[0].percCode_2.length);
	}
	nSubtotal = Math.round(nSubtotal);
	ssubtotal = nSubtotal/100 + "";
	if(ssubtotal.indexOf(".")<0) {
		ssubtotal = ssubtotal + ".00";
	} else if((ssubtotal.length - ssubtotal.indexOf('.')) <= 2) {
		//alert(ssubtotal.length + " : " + ssubtotal.indexOf("."));
		ssubtotal = ssubtotal + "00".substring(0, (ssubtotal.length - ssubtotal.indexOf('.') - 1));
	}
	document.forms[0].percCodeSubtotal_<%=iCheckNo%>.value = ssubtotal;
	if(nMin > document.forms[0].percCodeSubtotal_<%=iCheckNo%>.value) {
		document.forms[0].percCodeSubtotal_<%=iCheckNo%>.value = nMin;
	} else if (nMax < document.forms[0].percCodeSubtotal_<%=iCheckNo%>.value) {
		document.forms[0].percCodeSubtotal_<%=iCheckNo%>.value = nMax;
	}
<%	}
%>
	nSubtotal = 0.00;
    for (var i =0; i <document.forms[0].elements.length; i++) {
        if (document.forms[0].elements[i].name.indexOf("percCodeSubtotal") >=0 ) {
			nSubtotal = nSubtotal + document.forms[0].elements[i].value*10*10;
    	}
    	//alert(i + ":" + nSubtotal);
	}
	stotal = nSubtotal/100 + "";
	if(stotal.indexOf(".")<0) {
		stotal = stotal + ".00";
	} else if((stotal.length - stotal.indexOf('.')) <= 2) {
		//alert(stotal.length + " : " + stotal.indexOf("."));
		stotal = stotal + "00".substring(0, (stotal.length - stotal.indexOf('.') - 1));
	}
        var num = new Number(stotal);
	document.forms[0].total.value = num.toFixed(2);
}
	var ntotal = 0.00;
    for (var i =0; i <document.forms[0].elements.length; i++) {
        if (document.forms[0].elements[i].name.indexOf("percCodeSubtotal") >=0 ) {
			ntotal = ntotal + (document.forms[0].elements[i].value*10*10);
			//alert(":::" + document.forms[0].elements[i].value*10*10);
    	}
    	//alert(ntotal);
	}
	stotal = ntotal/100 + "";
	//alert(stotal);
	if(stotal.indexOf(".")<0) {
		stotal = stotal + ".00";
	} else if((stotal.length - stotal.indexOf('.')) <= 2) {
		//alert(stotal.length + " : " + stotal.indexOf("."));
		stotal = stotal + "00".substring(0, (stotal.length - stotal.indexOf('.') - 1));
	}
        var num = new Number(stotal);
	document.forms[0].total.value = num.toFixed(2);

-->
</script>

			</tr>
                        <% } %>
			<tr>

				<td colspan='3' align='center' bgcolor="silver">
				    <input type="submit" name="button" value="Back to Edit" style="width: 120px;" />
                                    <% if (codeValid && !dupServiceCode) { %>
                                    <input type="submit" name="submit" value="Save" style="width: 120px;" onClick="onClickSave();"/>
				    <input type="submit" name="submit" value="Save & Add Another Bill" onClick="onClickSave();"/>
                                    <% }else if (dupServiceCode){%>
                                    <td><div class='myError'>Warning: Duplicated service codes. </div></td>
                                    <% }
                                    %>
				<span id="isSaving" style="display: none">
					Saving ...
				</span>
                                    </td>
			</tr>
		</table>
		</td>
	</tr>
<% if (codeValid) {
        if(request.getParameter("xml_billtype")!=null && request.getParameter("xml_billtype").matches("ODP.*|WCB.*|NOT.*|BON.*")) { %>
	<tr>
			<td >
			Billing Notes:<br>
			<%
			String tempLoc = "";
		if (!bMultisites) {
            boolean bMoreAddr = props.getProperty("scheduleSiteID", "").equals("") ? false : true;
            if(bMoreAddr) {
            	tempLoc = request.getParameter("siteId").trim();
            } else {
            	tempLoc = props.getProperty("BILLING_NOTE","");
            }
		} else {
			tempLoc = request.getParameter("site");
		}
			%>
			<textarea name="comment" cols=60 rows=4><%=tempLoc %></textarea>
			</td>
	</tr>
<%      }
  }     %>
<%//
if(request.getParameter("xml_billtype")!=null && !request.getParameter("xml_billtype").matches("ODP.*|WCB.*|NOT.*|BON.*")) {
	JdbcBillingPageUtil pObj = new JdbcBillingPageUtil();
	List al = pObj.getPaymentType();

	Billing3rdPartPrep privateObj = new Billing3rdPartPrep();
	oscar.oscarRx.data.RxProviderData.Provider provider = new oscar.oscarRx.data.RxProviderData().getProvider((String) session.getAttribute("user"));

	String strClinicAddr = RxProviderData.createProviderContactString(provider);

if (codeValid) { %>

<%
HcTypeBillToRemitToPreference billToRemitToPreference = DefaultHcTypeBillToRemitToPreferenceService.getPreferenceForProvider(loggedInProviderNo, demo);
// for satellite clinics
String clinicAddress = null;
if (billToRemitToPreference.isBilledToSet()) {
	strPatientAddr = billToRemitToPreference.getBillToText();
}
if (billToRemitToPreference.isRemitToSet()) {
	clinicAddress = billToRemitToPreference.getRemitToText();
} else if (bMultisites) {
	String siteName = request.getParameter("site");
	SiteDao siteDao = (SiteDao)WebApplicationContextUtils.getWebApplicationContext(application).getBean("siteDao");
  	List<Site> sites = siteDao.getActiveSitesByProviderNo((String) session.getAttribute("user"));
  	Site s = ApptUtil.getSiteFromName(sites, siteName);

  	if (s==null)
  		clinicAddress = strClinicAddr;
  	else {
  		clinicAddress = s.getFullName()+"\n"+s.getAddress()+"\n"+s.getCity()+", "+s.getProvince()+" "+s.getPostal()+"\nTel: "+s.getPhone()+"\nFax: "+s.getFax();
  	}

} else {
	String siteID = request.getParameter("siteId");
	if(props.getProperty("clinicSatelliteCity") != null) {
	    //compare the site id with clinicSatelliteCity to get the current address index
	    //in properties file  clinicSatelliteCity and scheduleSiteID must have same value
	    String[] clinicCity = props.getProperty("clinicSatelliteCity", "").split("\\|");
	    //current address index
	    int siteFlag = 0;
	    for(int i = 0; i < clinicCity.length; i++){
	    	if (siteID.equals(clinicCity[i]))	siteFlag = i;
	    }
	    String[] temp0 = props.getProperty("clinicSatelliteName", "").split("\\|");
	    String[] temp1 = props.getProperty("clinicSatelliteAddress", "").split("\\|");
	    String[] temp3 = props.getProperty("clinicSatelliteProvince", "").split("\\|");
	    String[] temp4 = props.getProperty("clinicSatellitePostal", "").split("\\|");
	    String[] temp5 = props.getProperty("clinicSatellitePhone", "").split("\\|");
	    String[] temp6 = props.getProperty("clinicSatelliteFax", "").split("\\|");
	    clinicAddress = temp0[siteFlag]+"\n"+temp1[siteFlag] + "\n" + clinicCity[siteFlag] + ", " + temp3[siteFlag] + " " + temp4[siteFlag] + "\nTel: " + temp5[siteFlag] + "\nFax: " + temp6[siteFlag];
	}else{
		clinicAddress = strClinicAddr;
	}
}
	String tempLoc = props.getProperty("BILLING_NOTE","");
%>
<tr><td>
		<table border="1" width="100%" bordercolorlight="#99A005" bordercolordark="#FFFFFF">
			<tr class="myYellow">
				<td colspan='2'>Private Billing</td>
			</tr>
			<tr><td width="80%">

			<table id="privateBillInfo" border="0" width="100%" >
			<tr><td>Bill To [<a href=# onclick="scriptAttach('billTo'); return false;">Search</a>]<br>
			<textarea name="billto" id="billTo" cols=30 rows=6><%=Encode.forHtmlContent(strPatientAddr)%></textarea></td>
			<td>Remit To [<a href=# onclick="scriptAttach('remitTo'); return false;">Search</a>]<br>
			<textarea name="remitto" id="remitTo" value="" cols=30 rows=6><%=Encode.forHtmlContent(clinicAddress)%></textarea></td>
			<td>Payee<br>
             <% 
             String  providerNo= request.getParameter("xml_provider");
             int indexnumber=providerNo.indexOf("|");
             if(indexnumber!=-1)
             {
             providerNo=providerNo.substring(0,indexnumber);
             }
             
             String payeename="";
             String lname="";
             String fname="";
             Provider p = providerDao.getProvider(providerNo);
             lname = p.getLastName();
             fname = p.getFirstName();   
             payeename=fname+" "+lname;
             	  
   String payee = props.getProperty("PAYEE", "");
   payee = payee.trim();
   if( payee.length() > 0 ) {
%>
   <textarea id="payee" name="payee" value="" cols=20 rows=6><%=payee%></textarea></td>
<% } else { %>
   <textarea id="payee"  name="payee" value="" cols=20 rows=6><%=Encode.forHtmlContent(payeename)%></textarea></td>
   <input type="hidden" name="payeename1" id="payeename1" value="<%=Encode.forHtmlAttribute(payeename)%>" />
<% } %>
			</tr>
			</table>
			<table border="0" width="100%" >
			<tr>
			<td >
			Billing Notes: &nbsp; <%
			if (!removePatientDetailInThirdPartyInvoice) { 
			    %> <label> <input type="checkbox"
			    name="print_invoice_comment"
			    value="true" /> Print on Invoice? </label> 
			<% } %>
			<br>
			<textarea name="comment" cols=100 rows=6><%=tempLoc %></textarea>
			</td>
			<td align="right">
                        <input type="hidden" name="provider_no" value="<%=providerview%>"/>
                        HST Billed:<input type="text" id="gst" name="gst" value="<%=gstTotal%>" size="6"/><br>
                        <input type="hidden" id="gstBilledTotal" name="gstBilledTotal" value="<%=gstbilledtotal%>" size="6" />
                        Total:<input type="text" id="stotal" disabled name = "stotal" value="0.00" size="6" /><br>
			Payments:<input type="text"  disabled name="payment1" id="payment" value="0.00" size="6" onDblClick="settlePayment();" /><br/>
			Discount:<input type="text" disabled name="discount2" id="discount" value="0.00" size="6"/>
			</td>
			</tr>
			</table>

			<td class="myGreen">
			Payment Method:<br/>
			<% for(int i=0; i<al.size(); i=i+2) { %>
			<input type="radio" name="payMethod" value="<%=al.get(i) %>" id="payMethod_<%=i %>"/><%=al.get(i+1) %><br/>
			<% } %>
			</td></tr>
			<tr>
				<td colspan='2' align='center' bgcolor="silver"><input type="submit" name="submit" value="Save & Print Invoice"
					style="width: 150px;" /><input type="submit" name="submit" id="settlePrintBtn"
					value="Settle & Print Invoice" onClick="document.forms['titlesearch'].btnPressed.value='Settle'; document.forms['titlesearch'].submit();javascript:popupPage(700,720,'billingON3rdInv.jsp');" style="width: 160px;" />
				<input type="hidden"  name="btnPressed" value="">
				<input type="hidden" name="total_payment" id="total_payment" value="0.00"/>
				<input type="hidden" name="total_discount" id="total_discount" value="0.00"/>
				<input type="hidden" name="refund" id="refund" value="0.00"/>
				</td>
			</tr>
		</table>

</td></tr>
<% }} %>
	<%for (Enumeration e = request.getParameterNames(); e.hasMoreElements();) {
				String temp = e.nextElement().toString();
%>
	<input type="hidden" name="<%= temp %>" value="<%=StringEscapeUtils.escapeHtml(request.getParameter(temp))%>" />
	<%}

		%>


</table>
<% if(bPerc) { out.println("* Click the code you want the % code to apply to [1 or 2 ...]."); } %>
</form>


<script language="JavaScript">
    function updateDiscount(i) {
        var payment;
        var subtotal;
        if (isNaN(i)) {
            jQuery("input[id^='discount_']").each(function(j) {
                payment = "0.00";
                subtotal = jQuery('#percCodeSubtotal_' + j).val();

                if (jQuery('#calc_discount_all').attr("checked") && subtotal) {
                    jQuery('#calc_discount_' + j).attr('checked','checked');
                    payment = parseFloat(subtotal).toFixed(2);
                } else {
                    jQuery('#calc_discount_' + j).removeAttr('checked');
                }

                document.getElementById('discount_' + j).value = payment;
            });
        } else {
            payment = "0.00";
            subtotal = jQuery('#percCodeSubtotal_' + i).val();

            if (jQuery('#calc_discount_' + i).attr("checked") && subtotal) {
                payment = parseFloat(subtotal).toFixed(2);
            }

            document.getElementById('discount_' + i).value = payment;
        }

		calculateDiscount();
    }

    function updatePayment(i) {
        var payment;
        var subtotal;
        if (isNaN(i)) {
            jQuery("input[id^='paid_']").each(function(j) {
                payment = "0.00";
                subtotal = jQuery('#percCodeSubtotal_' + j).val();

                if (jQuery('#calc_paid_all').attr("checked") && subtotal) {
                    jQuery('#calc_paid_' + j).attr('checked','checked');
                    payment = parseFloat(subtotal).toFixed(2);
                } else {
                    jQuery('#calc_paid_' + j).removeAttr('checked');
                }

                document.getElementById('paid_' + j).value = payment;
            });
        } else {
            payment = "0.00";
            subtotal = jQuery('#percCodeSubtotal_' + i).val();

            if (jQuery('#calc_paid_' + i).attr("checked") && subtotal) {
                payment = parseFloat(subtotal).toFixed(2);
            }

            document.getElementById('paid_' + i).value = payment;
        }

        calculatePayment();
    }

function calculatePayment(){
    var payment = 0.00;
    jQuery("input[id^='paid_']").each(function(index) {
    	if (this != null && this.value.length > 0) {
    		payment = parseFloat(payment) + parseFloat(this.value);
    		payment = payment.toFixed(2);
    	}
    });
	if (document.getElementById("payment")!=null) document.getElementById("payment").value=payment;
	if (document.getElementById("total_payment")!=null) document.getElementById("total_payment").value=payment;
}

function calculateDiscount(){
	var discount = 0.00;
	jQuery("input[id^='discount_']").each(function(index) {
		if (this != null && this.value.length > 0) {
			discount = parseFloat(discount) + parseFloat(this.value);
			discount = discount.toFixed(2);
		}
	});
	
	document.getElementById("discount").value = discount;
	document.getElementById("total_discount").value = discount;
}

function calculateTotal() {
	var total = 0.00;
	jQuery("input[id^='percCodeSubtotal_']").each(function (index) {
		if (this != null && this.value.length > 0) {
			total = parseFloat(total) + parseFloat(this.value);
			total = total.toFixed(2);
		}
	});
	jQuery("#total").val(total);
	jQuery("#gstBilledTotal").val(total);
	jQuery("#stotal").val(total);
}

function onTotalChanged() {
	var val = document.getElementById("total").value;
	var regexNumberic = /^([1-9]\d{0,9}|0)(\.\d{1,2})?$/;
	if (!regexNumberic.test(val)) {
		calculateTotal();
		alert("Please enter digital numbers !");
		return;
	}
	
	var total = jQuery("#total").val();
	jQuery("#gstBilledTotal").val(total);
	jQuery("#stotal").val(total);
}

function addToDiseaseRegistry(){
	if ( validateItems() ) {
		var url = "../../../oscarResearch/oscarDxResearch/dxResearch.do";
		var data = Form.serialize(dxForm);
		
		new Ajax.Updater('dxListing',url, {method: 'post',postBody: data,asynchronous:true,onComplete: getNewCurrentDxCodeList});
	}
}

function validateItems(){
	var ret = false;
	
	dxChecks = document.getElementsByName("xml_research");
	for( idx = 0; idx < dxChecks.length; ++idx ) {
		if( dxChecks[idx].checked ) {
			ret = true;
			break;
		}
	}
	<% if (!disableAddToRegistry) { %>
	if (ret) ret = confirm("Are you sure to add to the patient's disease registry?");
	<% } %>
	return ret;
}

function getNewCurrentDxCodeList(origRequest){
	var url = "../../../oscarResearch/oscarDxResearch/currentCodeList.jsp";
	var ran_number=Math.round(Math.random()*1000000);
	var params = "demographicNo=<%=demo_no%>&rand="+ran_number;  //hack to get around ie caching the page
	new Ajax.Updater('dxFullListing',url, {method:'get',parameters:params,asynchronous:true});
}

function confirmDialog() {
	if ("<%=icd9Desc%>"=="") {
		jQuery("#confirmDialog").hide();
		return;
	}
	
	var html = jQuery("#confirmDialog").html().replace("[item]", "<%=icd9Desc%>");
	jQuery("#confirmDialog").html(html);
}

function addToPatientDx(){
	jQuery("#confirmDialog").hide();
	
	var html = jQuery("#dxForm").html();
	html += "<input name='xml_research' value='icd9,<%=icd9Code%>' type='checkbox' checked='checked' hidden='hidden'>";
	jQuery("#dxForm").html(html);
	
	window.open("billingONLogAddToPatientDx.jsp?demo=<%=demo_no%>&dxcode=<%=dxCode%>&icd9code=<%=icd9Code%>","_blank","height=100,width=100");
	addToDiseaseRegistry();
}

function noAddToPatientDx(){
	jQuery("#confirmDialog").hide();
	window.open("billingONXAddToPatientDx.jsp?demo=<%=demo_no%>&dxcode=<%=dxCode%>&icd9code=<%=icd9Code%>","_blank","height=100,width=100");
}
</script>


<oscar:oscarPropertiesCheck property="DX_QUICK_LIST_BILLING_REVIEW" value="yes">
	
<% if (addDxOnSave) { %>
<div id="confirmDialog" style="position:absolute; left:29%; top:30%; z-index:99; background-color:white; padding:50px; border:solid grey; border-width:1px 4px 4px 1px; font-size:medium">
	Do you want to add [item] to the Disease Registry?
	<br/><br/>
	<div style="text-align:center">
		<input type="button" value="Yes" onclick="addToPatientDx()"/>
		<input type="button" value="No" onclick="noAddToPatientDx()"/>
	</div>
</div>
<% } %>

<div class="dxBox">
	<h3>
		&nbsp;Current Patient Disease Registry&nbsp;
		<a href="#" onclick="Element.toggle('dxFullListing'); return false;" style="font-size:small;">show/hide</a>
	</h3>
	<br/>
	<div class="wrapper" id="dxFullListing">
		<jsp:include page="../../../oscarResearch/oscarDxResearch/currentCodeList.jsp">
			<jsp:param name="demographicNo" value="<%=demo_no%>"/>
		</jsp:include>
	</div>
	<br/>
</div>

<div class="dxBox">
	<h3>&nbsp;Select Disease Registry Terms from the options below to be added to Current Patient's Disease Registry List</h3>
	<form id="dxForm">
		<input type="hidden" name="demographicNo" value="<%=demo_no%>" />
		<input type="hidden" name="providerNo" value="<%=session.getAttribute("user")%>" />
		<input type="hidden" name="forward" value="" />
		<input type="hidden" name="forwardTo" value="codeList"/>
		<div class="wrapper" id="dxListing">
			<jsp:include page="../../../oscarResearch/oscarDxResearch/quickCodeList.jsp">
				<jsp:param name="demographicNo" value="<%=demo_no%>"/>
			</jsp:include>
		</div>
		<% if (!disableAddToRegistry) { %>
		<input type="button" value="Add To Disease Registry" onclick="addToDiseaseRegistry()"/>
		<% } %>
	</form>
</div>

</oscar:oscarPropertiesCheck>

</body>
</html>
