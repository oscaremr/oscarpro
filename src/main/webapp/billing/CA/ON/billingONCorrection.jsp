<!DOCTYPE html>
<%--

    Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

--%>
<%@page import="org.oscarehr.common.dao.BillingOnItemPaymentDao"%>
<%@page import="org.oscarehr.managers.SecurityInfoManager"%>
<%@page import="java.math.*,java.util.*,java.sql.*,oscar.*,java.net.*" %> <!-- -->
<%@page import="oscar.oscarBilling.ca.on.data.*"%>
<%@page import="oscar.oscarBilling.ca.on.pageUtil.*"%>
<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@page import="oscar.oscarDemographic.data.*"%>
<%@page import="oscar.util.UtilDateUtilities"%>
<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="org.oscarehr.util.SpringUtils"%>
<%@page import="oscar.util.DateUtils"%>

<%@page import="org.oscarehr.common.model.BillingPermission"%>
<%@page import="org.oscarehr.common.dao.BillingPermissionDao"%>
<%@page import="org.oscarehr.common.model.BillingONItem"%>
<%@page import="org.oscarehr.common.model.BillingONErrorCode, org.oscarehr.common.dao.BillingONErrorCodeDao"%>
<%@page import="org.oscarehr.common.dao.BillingONEAReportDao, org.oscarehr.common.model.BillingONEAReport"%>
<%@page import="org.oscarehr.common.model.RaDetail, org.oscarehr.common.dao.RaDetailDao"%>
<%@page import="org.oscarehr.common.model.ClinicLocation, org.oscarehr.common.dao.ClinicLocationDao"%>
<%@page import="org.oscarehr.common.dao.BillingONPaymentDao, org.oscarehr.common.model.BillingONPayment"%>
<%@page import="org.oscarehr.common.model.Provider,org.oscarehr.PMmodule.dao.ProviderDao"%>
<%@page import="org.oscarehr.common.dao.BillingONCHeader1Dao, org.oscarehr.common.model.BillingONCHeader1"%>
<%@page import="org.oscarehr.common.model.BillingONExt, org.oscarehr.common.dao.BillingONExtDao"%>
<%@page import="org.oscarehr.common.model.BillingService, org.oscarehr.common.dao.BillingServiceDao"%>
<%@page import="org.oscarehr.common.model.ClinicNbr, org.oscarehr.common.dao.ClinicNbrDao"%>
<%@page import="org.oscarehr.common.model.Site, org.oscarehr.common.dao.SiteDao"%>
<%@page import="org.oscarehr.common.model.ProviderSite, org.oscarehr.common.dao.ProviderSiteDao"%>
<%@page import="org.oscarehr.common.model.ProfessionalSpecialist" %>
<%@page import="org.oscarehr.common.dao.ProfessionalSpecialistDao" %>
<%@page import="org.oscarehr.common.service.BillingONService"%> 
<%@page import="java.text.NumberFormat" %>
<%@ page import="org.oscarehr.common.model.CustomHealthcardType" %>
<%@ page import="org.oscarehr.common.dao.CustomHealthcardTypeDao" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<%@taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar" %>

<%			if (session.getAttribute("user") == null)
				response.sendRedirect("../../../logout.htm");
			String userfirstname, userlastname;
			

    String userProviderNo = (String) session.getAttribute("user");
    ProviderDao providerDao = (ProviderDao) SpringUtils.getBean("providerDao");
    BillingONExtDao bExtDao = (BillingONExtDao) SpringUtils.getBean("billingONExtDao");
	BillingPermissionDao billingPermissionDao = SpringUtils.getBean(BillingPermissionDao.class);
    
    BillingONPaymentDao billingOnPaymentDao = SpringUtils.getBean(BillingONPaymentDao.class);
    Provider userProvider = providerDao.getProvider(userProviderNo);

    
    if (userProvider == null)
        response.sendRedirect("../logout.jsp");
    
    if (session.getAttribute("userrole") == null )
        response.sendRedirect("../logout.jsp");
    
    String roleName$ = (String)session.getAttribute("userrole") + "," + userProviderNo;

    boolean isSiteAccessPrivacy=false;
    boolean isTeamAccessPrivacy=false;
    
    SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    
    
%>
<security:oscarSec objectName="_site_access_privacy" roleName="<%=roleName$%>" rights="r" reverse="false">
    <%isSiteAccessPrivacy=true; %>
</security:oscarSec>

<security:oscarSec objectName="_team_access_privacy" roleName="<%=roleName$%>" rights="r" reverse="false">
    <%isTeamAccessPrivacy=true; %>
</security:oscarSec>
<%
	ProviderSiteDao providerSiteDao = (ProviderSiteDao) SpringUtils.getBean("providerSiteDao");
    Set<String> providerAccessList = new HashSet<String>();
    
    //multisites function
    if (isSiteAccessPrivacy) {
        List<ProviderSite> providerSite = providerSiteDao.findByProviderNo(userProviderNo);
        for (ProviderSite pSite : providerSite) {
            providerAccessList.add(pSite.getId().getProviderNo());
        }                       
    }
    
    if (isTeamAccessPrivacy) {
        List<Provider> providers = providerDao.getBillableProvidersOnTeam(userProvider);
        for (Provider p : providers) {
            providerAccessList.add(p.getProviderNo());
        }            
    }
    
    boolean isTeamBillingOnly=false;
    boolean isMultiSiteProvider = true;
%>
<security:oscarSec objectName="_team_billing_only" roleName="<%=roleName$%>" rights="r" reverse="false">
    <% isTeamBillingOnly=true; %>
</security:oscarSec>
<%    
    boolean bMultisites = org.oscarehr.common.IsPropertiesOn.isMultisitesEnable();
    List<String> mgrSites = new ArrayList<String>();

    if (bMultisites) {
        SiteDao siteDao = (SiteDao)WebApplicationContextUtils.getWebApplicationContext(application).getBean("siteDao");
        List<Site> sites = siteDao.getActiveSitesByProviderNo(userProviderNo);
        for (Site s : sites) {
                mgrSites.add(s.getName());
        }
    }

    String UpdateDate = "";
    String DemoNo = "";
    String DemoName = "";
    String DemoAddress = "";
    String DemoCity = "";
    String DemoProvince = "";
    String DemoPostal = "";
    String DemoDOB = "";
    String DemoRS = "";
    String DemoSex = "";
    String hin = "";
    String location = "";
    String BillLocation = "";
    String BillLocationNo = "";
    String BillDate = "";
    String Provider = "";
    String BillType = "";
    String payProgram = "";
    String BillTotal = "";
    String visitdate = "";
    String visittype = "";
    String sliCode = "";
    String BillDTNo = "";
    String HCTYPE = "";
    String HCSex = "";
    String r_doctor_ohip = "";
    String r_doctor = "";
    String r_doctor_ohip_s = "";
    String r_doctor_s = "";
    String m_review = "";
    String specialty = "";
    String r_status = "";
    String roster_status = "";
    String comment = "";
    String payer = "";
    String htmlPaid = "";
    int rowCount = 0;
    int rowReCount = 0;
    ResultSet rslocation = null;
    ResultSet rsPatient = null;

    boolean rmaBillingEnabled = SystemPreferencesUtils
            .isReadBooleanPreferenceWithDefault("rma_billing_enabled", false);
%>

<html:html locale="true">
<head>
<title><bean:message key="billing.billingCorrection.title" /></title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>

<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-1.9.1.min.js"></script>
<script src="<%=request.getContextPath() %>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/library/bootstrap2-datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/billing/CA/ON/js/billingCorrection.js"></script>

<link href="<%=request.getContextPath() %>/css/bootstrap.min.css" rel="stylesheet">
<link href="<%=request.getContextPath() %>/css/datepicker.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<%=request.getContextPath() %>/css/font-awesome.min.css">
	
<oscar:customInterface section="editInvoice"/>

<script type="text/javascript">
<!--

function setfocus() {	
	document.form1.billing_no.focus();
	document.form1.billing_no.select();
}
function rs(n,u,w,h,x) {
	args="width="+w+",height="+h+",resizable=yes,scrollbars=yes,status=0,top=60,left=30";
	remote=window.open(u,n,args);
	if (remote != null) {
		if (remote.opener == null)
			remote.opener = self;
	}
	if (x == 1) { return remote; }
}

function demoUpdate() {
	document.getElementsByName("method")[0].value="updateDemographic"
	if (confirm("Update Demographic information and set status to \"Bill OHIP\"?")) {
		return true;
	} else {
		return false;
	}
}

function unlinkReferralDoctor() {
    document.forms[1].elements['rd'].value = "";
    document.forms[1].elements['rdohip'].value = "";
}
var awnd=null;
function ScriptAttach(nameField) {
	f0 = escape(document.forms[1].elements[nameField].value);
	f1 = escape("document.forms[1].elements[\'"+nameField+ "\'].value")
	awnd=rs('att','billingDigSearch.jsp?name='+f0 + '&search=&name2=' + f1 + '&includeDescription=true',600,600,1);
	awnd.focus();
}
function referralScriptAttach2(elementName, name2) {
     var d = elementName;
     t0 = escape("document.forms[1].elements[\'"+d+"\'].value");
     t1 = escape("document.forms[1].elements[\'"+name2+"\'].value");
     awnd=rs('att',('searchRefDoc.jsp?param='+t0+'&param2='+t1),600,600,1);
     awnd.focus();
}
function scScriptAttach(nameF) {
	f0 = document.forms[1].elements[nameF].value;
    f1 = escape("document.forms[1].elements[\'"+nameF+ "\'].value");
	awnd=rs('att','billingCodeSearch.jsp?name='+f0 + '&search=&name1=&name2=&nameF='+f1,600,600,1);
	awnd.focus();
}

function scScriptAttachNew(elementName) {
    var row = 0;
    if ($('#billingServices').find("tr:last").length > 0) {
        row = parseInt($('#billingServices').find("tr:last")[0].id.replace('billingService', '')) + 1;
    }

    $('#billingServices').append('<tr id="billingService'+ row +'"> ' +
        '<td width="25%">' +
        '<input type="hidden" name="xml_service_code'+ row +'" value="">' +
        '<input type="hidden" name="row" value="'+ row +'">' +
        '<div class="input-append" style="max-width: 120px;"> <input type="text" name="servicecode'+ row +'" value="" style="max-width: 100px;" />' +
        '<a href="javascript:scScriptAttach(\'servicecode'+ row +')" class="btn"><i class="icon icon-search"></i></a></div></td>' +
        '<td id = "description'+ row +'"  name = "description'+ row +'"><font size="-1"></font></td>' +
        '<td width="20%">' +
        '   <input type="hidden" name="xml_diagnostic_code'+ row +'" value="" />' +
        '   <div class="input-append" style="max-width: 100px;">' +
        '       <input type="text"  name="dxCode'+ row +'" value="" maxlength="4" style="max-width: 90px;"/>' +
        '       <a href="javascript:ScriptAttach(\'dxCode'+ row +'\')" class="btn"><i class="icon icon-search"></i></a>' +
        '   </div>' +
        '</td>' +
        '<td class="control-group">' +
        '<input type="hidden" name="xml_billing_unit'+ row +'" value="">' +
        '<input type="text" style="width: 100%" id="billingunit'+ row +'" name="billingunit'+ row +'" value="1" size="5" maxlength="2" onchange="validateInt(this)" onblur="validateInt(this)" />' +
        '<label id="billingunit'+ row +'_error" class="control-label" for="billingunit'+ row +'" style="display:none;font-size:x-small">Must be a whole number</label>' +
        '</td>' +
        '<td align="right"><input type="hidden" name="xml_billing_amount'+ row +'" value="<">' +
        '<input type="text" style="width: 100%" size="5" maxlength="7" id="billingamount'+ row +'" name="billingamount'+ row +'" value="" onblur="parseTwoDecimalPlaces(this)" onchange="javascript:validateNum(this)"></td>' +
        '<td style="text-align: center"><input type="checkbox" name="itemStatus'+ row +'" id="itemStatus'+ row +'" value="S"></td>' +
        '<td> <a href="javascript:removeService('+ row +')" class="btn" title="Remove"><i class="icon icon-remove"></i></a></td></tr>'
    );

    var searchValue = document.forms[1].elements[elementName].value;
    var pasteInto = escape("\'servicecode" + row + "\'");
    awnd=rs('att','billingCodeSearch.jsp?name='+searchValue + '&search=&name1=&name2=&nameF='+pasteInto+'&selectOne=true',600,600,1);
    awnd.focus();

    var searchValue = document.forms[1].elements[elementName].value = "";
}

function removeService(idx) {
    document.getElementById("billingService" + idx).remove();
}

function search3rdParty(elementName) {
    var d = elementName;
    t0 = escape("document.forms[1].elements[\'"+d+"\'].value");
    popupPage('600', '700', 'onSearch3rdBillAddr.jsp?param='+t0);
}

function validateInt(el) {
    let thirdParty = ($("#payProgram").val() === "PAT");
    if (!thirdParty && el != null) {
        let elName = el.name;   // element name
        let error = $("#"+ elName + "_error"); // error field
        let parent = $(el).parent('td');  // parent field jQuery(this).parent().addClass('yourClass');
        let val = el.value;     // element vlaue
        
        if (val != null && val.trim() !== ''  && !Number.isInteger(Number(val))) {
            parent.addClass('error');
            error.show();
        } else {
            parent.removeClass('error');
            error.hide();
        }
    }
}

function validateNum(el) {
   var val = el.value;
   var tval = ""+val;

   if (isNaN(val)) {
      alert("Item value must be numeric.");
      el.select();
      el.focus();
      return false;
   }
   if ( val > 999999.99 ) {
     alert("Item value must be below $1000000");
     el.select();
     el.focus();
     return false;
   }
   decLen = tval.indexOf(".");
   if (decLen != -1  &&   ( tval.length - decLen ) > 3  ) {
      alert("Item value has a maximum of 2 decimal places");
      el.select();
      el.focus();
      return false;
   }

   parseTwoDecimalPlaces(el);
   
   return true;
}

function parseTwoDecimalPlaces(element) {
    if (element.value && element.value.length > 0 && !isNaN(element.value)) {
        element.value = parseFloat(element.value).toFixed(2);
    }
}

function validateAllItems() {

   var provider = document.getElementById("provider_no");
   if ( provider.options[provider.selectedIndex].value == "" ) {
      alert("Billing provider must be set");
      return false;
   }

    for (let idx = 0; idx < $("input[name^='billingamount']").length; idx++) {
        var billamt = document.getElementById("billingamount" + idx);
        if (billamt != undefined && !validateNum(billamt)) {
            return false;
        }
    }

   var statusOpts = document.getElementById("status");
   var status = statusOpts.options[statusOpts.selectedIndex].value;
   var payPrgrmOpts = document.getElementById("payProgram");
   var payPrgrm = payPrgrmOpts.options[payPrgrmOpts.selectedIndex].value;   
   var is3rdParty = true;
   if (payPrgrm == "HCP" || payPrgrm == "RMB" || payPrgrm == "WCB" ) {
      is3rdParty = false;
   }
   if ( status == "P" && !is3rdParty) {
	   alert("Pay Program does not match bill status.");
	   return false;
   }
    
    if (!is3rdParty) {
        for (let i = 0; i < $("input[name^='billingunit']").length; i++) {
            let serviceUnit = $("input[name^='billingunit']")[i];
            let val = serviceUnit != null ? serviceUnit.value : null;
            
            if (val != null && val.trim() !== '' && !Number.isInteger(Number(val))) {
                alert("All Billing Units must be whole numbers");
                return false;
            } 
        }
    }

   var outstandingAmt = document.getElementById("outstandingBalance").value;

   if ((outstandingAmt != "0.00") && (status == "S" && is3rdParty)) {
       if (!confirm('Warning: Settling this invoice will also settle the outstanding balance as paid. Continue?')) {
          return false;
       }
   }
                                     
   return true;
}
function popupPage(vheight,vwidth,varpage) {
  var page = "" + varpage;
  windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=0,screenY=0,top=0,left=0";
  var popup=window.open(page, "billcorrection", windowprops);
    if (popup != null) {
	    if (popup.opener == null) {
	      popup.opener = self;
	    }
    	popup.focus();
    }
}

function sanityCheck(id, hasPermission, isInvoiceNumberExist) {
    if (id == "") {
        alert("<bean:message key="billing.billingCorrection.msgSearchValidInvoiceNumber" />");
    } else if (!isInvoiceNumberExist) {
        alert("<bean:message key="billing.billingCorrection.msgInvoiceNumberNotExist" />");
    } else if (!hasPermission) {
        alert("<bean:message key="billing.billingCorrection.msgDoNotHavePermission" />");
    } else {
        location.href = "billingON3rdInv.jsp?billingNo=" + id;
    }
    return false;
}

function checkPayProgram(payProgram) {
    //enable 3rd party elements
    if (payProgram == 'PAT' || payProgram == 'OCF' || payProgram == 'ODS' || payProgram == 'CPP' || payProgram == 'STD' || payProgram == 'IFH' ) {
        document.getElementById("thirdParty").style.display = "inline";
        document.getElementById("thirdPartyPymnt").style.display = "inline";

        document.getElementById("billTo").disabled = false;
    } else {
        document.getElementById("thirdParty").style.display = "none";
        document.getElementById("thirdPartyPymnt").style.display = "none";
        
        document.getElementById("billTo").disabled = true;
    }
}

function checkSettle(status) {
    if ( status == 'S' ) {
        var payElem = document.getElementById("payment");
        if ( payElem != null ) {
            payElem.value = document.getElementById("billTotal").value;
        }
    } else if ( status == 'P') {
    	document.getElementById("thirdParty").style.display = "inline";
    	//document.getElementById("thirdPartyPymnt").style.display = "inline";
    	
    	document.getElementById("payment").disabled = false;
    	if (document.getElementById("oldPayment") != null) {
    		document.getElementById("oldPayment").disabled = false;    		
    	}
    	document.getElementById("payDate").disabled = false;
    	document.getElementById("refund").disabled = false;
    	document.getElementById("billTo").disabled = false;
    } else {
    	document.getElementById("thirdParty").style.display = "none";
    	document.getElementById("thirdPartyPymnt").style.display = "none";
    	
    	document.getElementById("payment").disabled = true;
    	if (document.getElementById("oldPayment") != null) {
    		document.getElementById("oldPayment").disabled = true;
    	}
    	document.getElementById("payDate").disabled = true;
    	document.getElementById("refund").disabled = true;
    	document.getElementById("billTo").disabled = true;
    }

}

function validateAmountNumberic(idx) {
	var oldVal = document.getElementById("billingamounttmp" + idx).value;
	var val = document.getElementById("billingamount" + idx).value;
	if (val.length == 0) {
		if (document.getElementsByName("servicecode" + idx)[0].value.trim().length > 0) {
			document.getElementById("billingamount" + idx).value = " ";
		}
		return;
	}
	//var regexNumberic = /^([1-9]\d*|0)(\.\d{1,2})?$/;
	var regexNumberic = /^([1-9]\d{0,9}|0)(\.\d{1,2})?$/;
	if (!regexNumberic.test(val)) {
		document.getElementById("billingamount" + idx).value = oldVal;
		alert("Please enter digital numbers !");
		return;
	}
	oldVal = val;
}

//-->
</script>

</head>

<body onload="$('#billing_no').focus()">
<%//
    RaDetailDao raDetailDao = (RaDetailDao) SpringUtils.getBean("raDetailDao");
    BillingONCHeader1Dao bCh1Dao = (BillingONCHeader1Dao) SpringUtils.getBean("billingONCHeader1Dao");
    BillingServiceDao bServiceDao = (BillingServiceDao) SpringUtils.getBean("billingServiceDao");
    
    // bFlag - fill in data?
    boolean bFlag = false;
	boolean hasPermission = true;
    boolean billNoErr = false;
    
    String billNo = request.getParameter("billing_no").trim();
    String claimNo = Encode.forHtmlAttribute(request.getParameter("claim_no"));
    if ( claimNo != null && claimNo.equals("null") ) {
		claimNo = null;
    }

    if ( billNo == null || billNo.length() == 0 ) {
        if ( claimNo != null && claimNo.length() > 0 ) {
            claimNo = claimNo.trim();
            List<RaDetail> raDetails = raDetailDao.getRaDetailByClaimNo(claimNo);
            if (!raDetails.isEmpty()) {
                billNo = String.valueOf(raDetails.get(0).getBillingNo());
            }
        }
    }
    if (billNo != null && billNo.length() > 0) {
            bFlag = true;
    }
    
    Locale locale = request.getLocale();
    BillingONCHeader1 bCh1 = null;
    Integer billingNo = null;    
    String createTimestamp = null;
    String creator = "";
    String createdDateTime = null;
    String clinicSite = "";

    if (bFlag) {

        billingNo = Integer.parseInt(billNo);
		bCh1 = bCh1Dao.find(billingNo);
		billNoErr = (bCh1 == null);

        if (bCh1 != null) {	
			clinicSite = bCh1.getClinic();

                //multisite. check provider no
            if (((isSiteAccessPrivacy || isTeamAccessPrivacy) && !providerAccessList.contains(bCh1.getProviderNo())) 
                 || (bMultisites && !mgrSites.contains(clinicSite))) { 
                
                isMultiSiteProvider = false;
	     }
	     if (!isMultiSiteProvider) {
                DemoNo = "";
                DemoName = "";
                DemoAddress = "";
                DemoCity = "";
                DemoProvince = "";
                DemoPostal = "";
                DemoDOB = "";
                DemoSex = "";
                r_doctor = "";
                r_doctor_ohip_s = "";
                r_doctor_s = "";
                m_review = bCh1.getManReview();
                specialty = "";
                r_status = "";
                roster_status = "";

                out.write("<script>window.alert('sorry, billing access denied.')</script>");               

            } else {
                 String curUser_providerno = loggedInInfo.getLoggedInProviderNo();
    
                 if (bCh1.getPayProgram().equals("HCP")) {
                     hasPermission = billingPermissionDao.hasPermission(bCh1.getProviderNo(), curUser_providerno, BillingPermission.OHIP_INVOICES);
                 } else {
                     hasPermission = billingPermissionDao.hasPermission(bCh1.getProviderNo(), curUser_providerno, BillingPermission.THIRD_PARTY_INVOICES);
                 }
    
                 if (hasPermission) {
                     createTimestamp = DateUtils.formatDateTime(bCh1.getTimestamp(), locale);
                     creator = StringUtils.trimToEmpty(providerDao.getProviderName(bCh1.getCreator()));
                     createdDateTime = DateUtils.formatDateTime(bCh1.getCreateDateTime(), locale);
                     DemoNo = bCh1.getDemographicNo().toString();
                     DemoName = bCh1.getDemographicName();
                     DemoAddress = "";
                     DemoCity = "";
                     DemoProvince = "";
                     DemoPostal = "";
                     DemoDOB = bCh1.getDob();
                     DemoSex = bCh1.getSex().equals("1") ? "M" : "F";
    
                     BigDecimal billTotal = bCh1.getTotal();
    
                     org.oscarehr.common.model.Demographic sdemo = (new DemographicData()).getDemographic(loggedInInfo, DemoNo);
                     hin = sdemo.getHin() + sdemo.getVer();
                     DemoDOB = sdemo.getYearOfBirth() + sdemo.getMonthOfBirth() + sdemo.getDateOfBirth();
                     DemoSex = sdemo.getSex();
                     DemoRS = sdemo.getRosterStatus();
                     //hin = ch1Obj.getHin() + ch1Obj.getVer();
                     location = bCh1.getFaciltyNum();
                     BillLocation = "";
                     BillLocationNo = location;
                     BillDate = DateUtils.formatDate(bCh1.getBillingDate(), locale);
                     Provider = bCh1.getProviderNo();
                     BillType = bCh1.getStatus();
                     payProgram = bCh1.getPayProgram();
                     BillTotal = billTotal.toPlainString();
                     try {
                         visitdate = DateUtils.formatDate(bCh1.getAdmissionDate(), locale);
                     } catch (java.text.ParseException e) {
                         visitdate = "";
                     }
                     visittype = bCh1.getVisitType();
                     sliCode = bCh1.getLocation();
                     BillDTNo = "";
                     HCTYPE = bCh1.getProvince();
                     HCSex = bCh1.getSex();
                     r_doctor_ohip = bCh1.getRefNum();
                     ProfessionalSpecialistDao professionalSpecialistDao = (ProfessionalSpecialistDao) SpringUtils.getBean("professionalSpecialistDao");
                     List<ProfessionalSpecialist> professionalSpecialists = professionalSpecialistDao.findByReferralNo(r_doctor_ohip);
                     if (professionalSpecialists != null)
                         r_doctor = professionalSpecialists.get(0).getLastName() + ", " + professionalSpecialists.get(0).getFirstName();
                     else
                         r_doctor = "";
                     r_doctor_ohip_s = "";
                     r_doctor_s = "";
                     m_review = bCh1.getManReview();
                     specialty = "";
                     r_status = "";
                     roster_status = "";
                     comment = bCh1.getComment();
    
                     // get ohip claim number
                     List<RaDetail> raDetails = raDetailDao.findByBillingNo(billingNo);
                     for (RaDetail ra : raDetails) {
                         if ((ra.getProviderOhipNo().equals(bCh1.getProviderOhipNo()))) {
                             if (ra.getHin() != null) {
                                 String raHin = (ra.getHin().length() >= 10 ? ra.getHin().substring(0, 10) : ra.getHin()).trim();
                                 if (raHin.equals(sdemo.getHin().trim())) {
                                     claimNo = Encode.forHtmlAttribute(ra.getClaimNo());
                                 }
                             }
                         }
                     }
                }
            }
        }
    }
    
				boolean thirdParty = false;
				Billing3rdPartPrep tObj = new Billing3rdPartPrep();
				if (hasPermission) {
                    if ("HCP".equals(payProgram) || "RMB".equals(payProgram) || "WCB".equals(
                            payProgram) || "BON".equals(payProgram) || "NOT".equals(payProgram)
                            || billNo.length() < 1) {
					
						Properties tProp = null;					
						if ( billNo.length() > 0 ) {
							tProp = tObj.get3rdPartBillPropInactive(billNo.trim());						
						}
						
						if ( tProp == null || tProp.size() == 0 ) {
							htmlPaid = "Paid<br><input type='text' id='payment' name='payment' size=5 value='0.00'/>" +
								"<input type='hidden' id='oldPayment' name='oldPayment' value='0.00'/> <input type='hidden' id='payDate' name='payDate' value='" +
								UtilDateUtilities.getToday("yyyy-MM-dd HH:mm:ss") + "'/><br> Refund<br><input type='text' id='refund' name='refund' size=5 value='0.00'/><br>";
								payer = "";
						} else {
							htmlPaid = "Paid<br><input type='text' id='payment' name='payment' size=5 value='"
								+ tProp.getProperty("payment","0.00") + "' /><input type='hidden' id='oldPayment' name='oldPayment' value='"
								+ tProp.getProperty("payment","0.00") + "' /><input type='hidden' id='payDate' name='payDate' value='"
								+ UtilDateUtilities.getToday("yyyy-MM-dd HH:mm:ss") + "'/><br>";
							htmlPaid += "Refund<br><input type='text' id='refund' name='refund' size=5 value='"
								+ tProp.getProperty("refund") + "' /><br>";
							payer = tProp.getProperty("billTo");
							if ( payer == null ) {
								payer = "";
							}
						}
					} else {
						thirdParty = true;
						Properties tProp = tObj.get3rdPartBillProp(billNo.trim());	
						NumberFormat currency = NumberFormat.getCurrencyInstance(Locale.US);

				if (isMultiSiteProvider) {
					BigDecimal payment = BigDecimal.ZERO;
					BigDecimal balance = BigDecimal.ZERO;
					BigDecimal total = BigDecimal.ZERO;
					BigDecimal refund = BigDecimal.ZERO;
					BigDecimal discount = BigDecimal.ZERO;
					BigDecimal credit = BigDecimal.ZERO;
					
					List<BillingONPayment> bops = billingOnPaymentDao.find3rdPartyPaymentsByBillingNo(Integer.parseInt(request.getParameter("billing_no").trim()));
					if (bCh1 != null && bops.isEmpty()) {
						payment = bCh1.getPaid();
					} else {for(BillingONPayment bop:bops) {
						credit = credit.add(bop.getTotal_credit());
						discount = discount.add(bop.getTotal_discount());
						payment = payment.add(bop.getTotal_payment());
						refund = refund.add(bop.getTotal_refund());				
					}
					}
					
					if (bCh1 != null) {
						total = bCh1.getTotal();
					}
					
					balance = total.subtract(payment).subtract(discount).subtract(refund).add(credit);
					payment = payment.subtract(credit);

                    htmlPaid = "<br/>&nbsp;&nbsp;<span style='font-size:large;font-weight:bold'>Billed:</span>&nbsp;&nbsp;&nbsp;<span id='billed' style='font-size:large;font-weight:bold'>"
							+ ((total.compareTo(BigDecimal.ZERO) == -1) ? "-" : "") + currency.format(total) + "</span>";
                    htmlPaid += "&nbsp;&nbsp;<spanstyle='font-size:large;font-weight:bold'>Paid:</span>&nbsp;&nbsp;&nbsp;<span id='payment' style='font-size:large;font-weight:bold'>"
                    	+ ((payment.compareTo(BigDecimal.ZERO) == -1) ? "-" : "") + currency.format(payment) + "</span>";
					htmlPaid += "&nbsp;&nbsp;&nbsp;&nbsp;<span style='font-size:large;font-weight:bold'>Balance:</span>&nbsp;&nbsp;&nbsp;<span id='balance' style='font-size:large;font-weight:bold'>"
						+ ((balance.compareTo(BigDecimal.ZERO) == -1 && refund.compareTo(BigDecimal.ZERO) == 0 ) ? "-" : "") + currency.format(balance) + "</span>";
					htmlPaid += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:display3rdPartyPayments()'>Payments List</a>";
				}	
                    		payer = tProp.getProperty("billTo");
                    		if ( payer == null ) {
                    		    payer = "";
                    		}}
			} else {
				bFlag = false;
			}

%>

<h3><bean:message key="admin.admin.btnBillingCorrection" /></h3>

<div class="container-fluid">
<% if (!hasPermission) { %>
	<div class="alert alert-error" id="alert_message">
    	<button type="button" class="close" data-dismiss="alert">&times;</button>
  		<strong>Error </strong> <bean:message key="billing.billingCorrection.msgDoNotHavePermission" />
    </div>
<% } %>
<% if (request.getParameter("adminSubmit")!=null) { %>
    <div class="alert alert-success" id="alert_message">
    	<button type="button" class="close" data-dismiss="alert">&times;</button>
  		<strong>Success! </strong> Your entry was saved!
    </div>
<% } %>

<% if (billNoErr) { %>
    <div class="alert alert-error" id="alert_message">
    	<button type="button" class="close" data-dismiss="alert">&times;</button>
  		<strong>Error! </strong> Invoice number does not exist!
    </div>
<% } %>

<div class="row well well-small">
    <% if (StringUtils.isNotBlank(billNo)) { %>
    Created by <%=creator%> <%=(StringUtils.isNotBlank(createdDateTime) ? " on " + createdDateTime : "")%>
    <a href="#" onclick="" style="float: right;font-weight: bold;" data-toggle="modal" data-target="#invoiceHistory">History</a>

    <br/>
    <% } %>
<% if (createTimestamp!=null) { %>
<bean:message key="billing.billingCorrection.msgLastUpdate" />: <%=nullToEmpty(createTimestamp)%>
<% } %>

<% 	String adminParam = "";
	if (request.getParameter("admin")!=null || request.getParameter("adminSubmit")!=null || request.getHeader("referer").contains("administration")) {
		adminParam = "?admin";
} %>
<form name="form1" method="post" action="billingONCorrection.jsp<%=adminParam%>">
<input type="hidden" id="billTotal" value="<%=BillTotal%>" />
    
<div class="span2">
<a href="#" onclick="return sanityCheck('<%= nullToEmpty(billNo) %>', <%= hasPermission %>, <%= bCh1 != null %>);"><bean:message key="billing.billingCorrection.formInvoiceNo" /></a><br>
<input type="text" id="billing_no" name="billing_no"  value="<%=nullToEmpty(billNo)%>" class="span2" required>
</div>


<div class="span2">
OHIP Claim No  <br>
  <input type="text" name="claim_no" value="<%=nullToEmpty(claimNo)%>"
         class="span2">
</div>

<div class="span2">
<br>
<input class="btn btn-primary" type="submit" name="submit" value="Search">
</div>

</form>
</div><!--well-->



<!-- RA error -->
<%
    if (bFlag && bCh1 != null) {
        BillingONEAReportDao billingONEAReportDao = (BillingONEAReportDao) SpringUtils.getBean("billingONEAReportDao");
	List<String> lReject = billingONEAReportDao.getBillingErrorList(billingNo);
	List<String> lError = raDetailDao.getBillingExplanatoryList(billingNo, bCh1.getHin(), bCh1.getProviderOhipNo());
	lError.addAll(lReject);	
        
        BillingONErrorCodeDao billingONErrorCodeDao = (BillingONErrorCodeDao) SpringUtils.getBean("billingONErrorCodeDao");        
%>
<table>
<% 
        for(int i=0; i<lError.size(); i++) {
            String codeNo = lError.get(i);
            if ("".equals(codeNo)) continue;

            BillingONErrorCode errorCode = billingONErrorCodeDao.find(codeNo);
            String codeDesc = null;
            if (errorCode != null) {
                codeDesc = errorCode.getDescription();
            }
            codeDesc = codeDesc == null ? "Unknown" : codeDesc;
%>
    <tr>
        <th width="10%"><b><%=codeNo %></b></th>
        <td align="left"><%=codeDesc %></td>
    </tr>
<%      } %>
</table>
<%  } 
    String curSite = request.getParameter("site")==null?clinicSite:request.getParameter("site");
%>


<html:form action="/billing/CA/ON/BillingONCorrection"> 
    
    <input type="hidden" name="method" value="updateInvoice"/>
    <input type="hidden" name="xml_billing_no" value="<%=billNo%>" /> 
    <input type="hidden" name="update_date" value="<%=nullToEmpty(createTimestamp)%>"/>
    <input type="hidden" name="payDate" value="<%=UtilDateUtilities.getToday("yyyy-MM-dd HH:mm:ss")%>" />
    <input type="hidden" name="demoNo" value="<%=DemoNo%>" />
    <input type="hidden" name="oldStatus" value="<%=thirdParty ? "thirdParty" : "" %>" />

<div class="row well well-small">  
<div class="span10">      
<table>
	<tr>
		<th align="left" colspan="2"><bean:message
			key="billing.billingCorrection.msgPatientInformation" /> </th>
	</tr>
	<tr>
		<td width="47%"><bean:message
			key="billing.billingCorrection.msgPatientName" />: <a href=#
			onclick="popupPage(720,860,'../../../demographic/demographiccontrol.jsp?demographic_no=<%=DemoNo %>&displaymode=edit&dboperation=search_detail');return false;">
		<%=Encode.forHtmlContent(DemoName)%></a> <input type="hidden" name="demo_name"
										value="<%=Encode.forHtmlAttribute(DemoName)%>"></td>
		<td width="41%"><bean:message
			key="billing.billingCorrection.formHealth" />: <%=hin%> <input
			type="hidden" name="xml_hin" value="<%=hin%>">
		RS: <%=DemoRS%></td>
		<td>
		<% if (bCh1 != null && !BillType.equals("S") && !BillType.equals("D") && !BillType.equals("N")) { %>
			<input class="btn" onclick="demoUpdate();" type="submit" value="Update and Bill">
		<% } %>
		</td>
	</tr>
	<tr>
		<td><bean:message key="billing.billingCorrection.msgSex" />:
			<%=DemoSex%> <input type="hidden" name="demo_sex" value="<%=DemoSex%>">
			<input type="hidden" name="hc_sex" value="<%=HCSex%>"></td>
		<td><bean:message key="billing.billingCorrection.formDOB" />:
			<input type="hidden" name="xml_dob" value="<%=DemoDOB%>"> <%=DemoDOB%>
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td><bean:message key="billing.billingCorrection.msgDoctor" />:<br>
			<input type="text" name="rd" value="<%=r_doctor%>" size=20 readonly>
		</td>
		<td>

			<bean:message
			key="billing.billingCorrection.msgDoctorNo" />:
			<div class="input-append">
			 <input type="text" name="rdohip" value="<%=r_doctor_ohip%>" class="span2" readonly />
			<a href="javascript:referralScriptAttach2('rdohip','rd')" class="btn"><i class="icon icon-search"></i></a>
			</div>
			</td>
	</tr>
<% if (r_doctor != null && !r_doctor.trim().equals("")) {  %>
    <tr>
        <td></td>
        <td>
            <input class="btn" onclick="unlinkReferralDoctor();" type="submit" name="submit" value="Unlink Referral Doctor"/>
        </td>
    </tr>
<% } %>

</table>
</div><!--span-->
</div>

<div class="row well well-small">
<div class="span10">

<strong><bean:message key="billing.billingCorrection.msgAditInfo" /></strong><br>

<bean:message key="billing.billingCorrection.formHCType" />: 
				<select name="hc_type" style="font-size: 80%;">
					<option value="OT" <%=HCTYPE.equals("OT")?" selected":""%>>OT-Other</option>
					<%	Map<String, String> provinces = ProvinceNames.getDefaultProvinces();
						for (Map.Entry<String, String> entry : provinces.entrySet()) {
							String shortName = entry.getKey();
							String longName = entry.getValue();
							Boolean selected = HCTYPE.equals(shortName);

					%>
					<option value="<%=shortName%>" <%=selected ? " selected" : ""%>><%=shortName%>-<%=longName%></option>
					<%	} %>
		</select>


<bean:message key="billing.billingCorrection.formManualReview" />: <input type="checkbox" name="m_review" value="Y" <%=m_review.equals("Y")?"checked":""%> >
</div><!--span-->
</div>

<div class="row well well-small">

<div class="span10">
<b><bean:message key="billing.billingCorrection.msgBillingInf" /></b><br>

<div class="span4"> 

<div class="span4" style="margin-left:0px;">		
<label><bean:message key="billing.billingCorrection.btnBillingDate" />:</label>
<label class="input-append">
	<input type="text" name="xml_appointment_date" id="xml_appointment_date" value="<%=BillDate%>" style="width:90px" pattern="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" autocomplete="off" />
	<span class="add-on"><i class="icon-calendar"></i></span>
</label>
</div><!--cal span2-->

<bean:message key="billing.billingCorrection.formBillingType" />:<br>
<input type="hidden" name="xml_status" value="<%=BillType%>"> 
<select style="font-size: 80%;" id="status" name="status" onchange="checkSettle(this.options[this.selectedIndex].value);">
<option value=""><bean:message
	key="billing.billingCorrection.formSelectBillType" /></option>
<option value="H" <%=BillType.equals("H")?"selected":""%>><bean:message
	key="billing.billingCorrection.formBillTypeH" /></option>
<option value="O" <%=BillType.equals("O")?"selected":""%>><bean:message
	key="billing.billingCorrection.formBillTypeO" /></option>
<option value="P" <%=BillType.equals("P")?"selected":""%>><bean:message
	key="billing.billingCorrection.formBillTypeP" /></option>
<option value="N" <%=BillType.equals("N")?"selected":""%>><bean:message
	key="billing.billingCorrection.formBillTypeN" /></option>
<option value="W" <%=BillType.equals("W")?"selected":""%>><bean:message
	key="billing.billingCorrection.formBillTypeW" /></option>
<option value="B" <%=BillType.equals("B")?"selected":""%>><bean:message
	key="billing.billingCorrection.formBillTypeB" /></option>
<option value="S" <%=BillType.equals("S")?"selected":""%>>S
| Settled</option>
<option value="X" <%=BillType.equals("X")?"selected":""%>><bean:message
	key="billing.billingCorrection.formBillTypeX" /></option>
<option value="D" <%=BillType.equals("D")?"selected":""%>><bean:message
	key="billing.billingCorrection.formBillTypeD" /></option>
<option value="I" <%=BillType.equals("I")?"selected":""%>><bean:message
	key="billing.billingCorrection.formBillTypeI" /></option>
</select><br>

Pay Program:<br>
<input type="hidden" name="xml_payProgram" value="<%=BillDate%>" />
<select style="font-size: 80%;" id="payProgram" name="payProgram" onchange="checkPayProgram(this.options[this.selectedIndex].value)">
<%for (int i = 0; i < BillingDataHlp.vecPaymentType.size(); i = i + 2) {

	%>
<option value="<%=BillingDataHlp.vecPaymentType.get(i) %>"
<%=payProgram.equals(BillingDataHlp.vecPaymentType.get(i))? "selected":"" %>><%=BillingDataHlp.vecPaymentType.get(i + 1)%></option>
<%}

%>
</select><br>
<bean:message key="billing.billingCorrection.formBillingPhysician" />: <br />

<% // multisite start ==========================================
    if (bMultisites) {
        SiteDao siteDao = (SiteDao)WebApplicationContextUtils.getWebApplicationContext(application).getBean("siteDao");
        List<Site> sites = siteDao.getActiveSitesByProviderNo(userProviderNo);
        // now get all providers eligible
        JdbcBillingPageUtil util = new JdbcBillingPageUtil();
        List<Provider> activeProviders = util.getActiveProviders(isTeamBillingOnly, isTeamAccessPrivacy, isSiteAccessPrivacy, userProviderNo);
        List<Provider> inactiveProviders = util.getInactiveProviders(isTeamBillingOnly, isTeamAccessPrivacy, isSiteAccessPrivacy, userProviderNo);

        HashSet<String> pros=new HashSet<String>();
        for (Provider provider : activeProviders) {
                pros.add(provider.getProviderNo());
        }
%>
      <script>
var _providers = [];
<%	for (int i=0; i<sites.size(); i++) {  
	Set<Provider> siteProviders = sites.get(i).getProvidersOrderByName();
	List<Provider>  siteProvidersList = new ArrayList<Provider> (siteProviders);
     Collections.sort(siteProvidersList,(new Provider()).ComparatorName());%>
	_providers["<%= sites.get(i).getName() %>"]="<% Iterator<Provider> iter = siteProvidersList.iterator();
	while (iter.hasNext()) {
		Provider p=iter.next();
		if (pros.contains(p.getProviderNo())) {
	%><option value='<%= p.getProviderNo() %>'><%=Encode.forHtmlContent(p.getLastName()+", "+p.getFirstName())%></option><% }} %>";
<% } %>
function changeSite(sel) {
	sel.form.provider_no.innerHTML=sel.value=="none"?"":_providers[sel.value];
	sel.style.backgroundColor=sel.options[sel.selectedIndex].style.backgroundColor;
}
      </script>
      	<select id="site" name="site" style="font-size: 80%;" onchange="changeSite(this)">
      		<option value="none" style="background-color:white">---select clinic---</option>
      	<%
      	for (int i=0; i<sites.size(); i++) {
      	%>
      		<option value="<%= Encode.forHtmlAttribute(sites.get(i).getName()) %>" style="background-color:<%= sites.get(i).getBgColor() %>"
      			 <%=sites.get(i).getName().toString().equals(curSite)?"selected":"" %>><%= Encode.forHtmlContent(sites.get(i).getName()) %></option>
      	<% } %>
      	</select>
        <select id="provider_no" name="provider_no" style="font-size: 80%;width:140px">
          <option value=""><bean:message key="billing.billingCorrection.msgSelectProvider"/></option>
          <optgroup label="Active Providers">
            <%
              for (Provider provider : activeProviders) {
            %>
            <option value="<%= provider.getId() %>"
                    <%= Provider.equals(provider.getId()) ? "selected" : "" %>><%= provider.getId() %>
                    | <%= Encode.forHtmlContent(provider.getLastName()) %>, <%= Encode.forHtmlContent(
                        provider.getFirstName()) %>
            </option>
            <% } %>
          </optgroup>
          <optgroup label="Inactive Providers">
            <%
              for (Provider provider : inactiveProviders) {
            %>
            <option value="<%= provider.getId() %>"
                    <%= Provider.equals(provider.getId()) ? "selected" : "" %>><%= provider.getId() %>
                    | <%= Encode.forHtmlContent(provider.getLastName()) %>, <%= Encode.forHtmlContent(
                      provider.getFirstName()) %>
            </option>
            <% } %>
          </optgroup>
        </select>
<%  // multisite end ==========================================
    } else {
%>
  <select id="provider_no" style="font-size: 80%;" name="provider_no">
    <option value=""><bean:message key="billing.billingCorrection.msgSelectProvider" /></option>
    <optgroup label="Active Providers">
      <%
        JdbcBillingPageUtil util = new JdbcBillingPageUtil();
        List<Provider> activeProviders = util.getActiveProviders(isTeamBillingOnly, isTeamAccessPrivacy, isSiteAccessPrivacy, userProviderNo);
        List<Provider> inactiveProviders = util.getInactiveProviders(isTeamBillingOnly, isTeamAccessPrivacy, isSiteAccessPrivacy, userProviderNo);;
        for (Provider provider : activeProviders) {
      %>
      <option value="<%= provider.getId() %>"
              <%= Provider.equals(provider.getId()) ? "selected" : "" %>><%= provider.getId() %> | <%= Encode.forHtmlContent(provider.getLastName()) %>, <%= Encode.forHtmlContent(provider.getFirstName()) %>
      </option>
      <% } %>
    </optgroup>
    <optgroup label="Inactive Providers">
      <%
        for (Provider provider : inactiveProviders) {
      %>
      <option value="<%= provider.getId() %>"
              <%= Provider.equals(provider.getId()) ? "selected" : "" %>><%= provider.getId() %> | <%= Encode.forHtmlContent(provider.getLastName()) %>, <%= Encode.forHtmlContent(provider.getFirstName()) %>
      </option>
      <% } %>
    </optgroup>
  </select>
<% } %>
<input type="hidden" name="xml_provider_no" value="<%=Provider%>">
</div><!--span4-->

<div class="span4"> 
<input type="hidden" name="xml_visitdate" value="<%=visitdate%>" /> 
<div class="span4" style="margin-left:0px;">		
<label><bean:message key="billing.billingCorrection.btnAdmissionDate" />:</label>
<label class="input-append">
	<input type="text" name="xml_vdate" id="xml_vdate" value="<%=visitdate%>" pattern="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" style="width:90px" autocomplete="off" />
	<span class="add-on"><i class="icon-calendar"></i></span>
</label>
</div><!--date span-->
<br>

<bean:message key="billing.billingCorrection.formVisit" />: <br>
<input type="hidden" name="xml_clinic_ref_code" value="<%=location%>"> 
<select name="clinic_ref_code">
<option value=""><bean:message key="billing.billingCorrection.msgSelectLocation" /></option>
<%//
ClinicLocationDao clinicLocationDao = (ClinicLocationDao) SpringUtils.getBean("clinicLocationDao");
List<ClinicLocation> clinicLocations = clinicLocationDao.findByClinicNo(1);				
	for (ClinicLocation clinicLoc : clinicLocations) {
		BillLocationNo = clinicLoc.getClinicLocationNo();
		BillLocation = clinicLoc.getClinicLocationName();
%>
<option value="<%=BillLocationNo%>"
	<%=location.equals(BillLocationNo)?"selected":""%>><%=BillLocationNo%>
| <%=BillLocation%></option>

<%}%>
</select><br>

<% if (rmaBillingEnabled) { %> Clinic Nbr <% } else { %> <bean:message key="billing.billingCorrection.formVisitType"/> <% } %>: <br>

<input type="hidden" name="xml_visittype" value="<%=visittype%>">
<select style="font-size: 80%;" name="visittype">
<option value=""><bean:message key="billing.billingCorrection.msgSelectVisitType" /></option>
<% if (rmaBillingEnabled) { %>
 <%
    ClinicNbrDao cnDao = (ClinicNbrDao) SpringUtils.getBean("clinicNbrDao");
	ArrayList<ClinicNbr> nbrs = cnDao.findAll();
for (ClinicNbr clinic : nbrs) {
		String valueString = String.format("%s | %s", clinic.getNbrValue(), clinic.getNbrString());
		%>
	<option value="<%=valueString%>" <%=visittype.startsWith(clinic.getNbrValue())?"selected":""%>><%=valueString%></option>
<%	}
} else { %>
<option value="00" <%=visittype.equals("00")?"selected":""%>><bean:message key="billing.billingCorrection.formClinicVisit" /></option>
<option value="01" <%=visittype.equals("01")?"selected":""%>><bean:message key="billing.billingCorrection.formOutpatientVisit" /></option>
<option value="02" <%=visittype.equals("02")?"selected":""%>><bean:message key="billing.billingCorrection.formHospitalVisit" /></option>
<option value="03" <%=visittype.equals("03")?"selected":""%>><bean:message key="billing.billingCorrection.formER" /></option>
<option value="04" <%=visittype.equals("04")?"selected":""%>><bean:message key="billing.billingCorrection.formNursingHome" /></option>
<option value="05" <%=visittype.equals("05")?"selected":""%>><bean:message key="billing.billingCorrection.formHomeVisit" /></option>
<% } %>
</select><br>


<%String clinicNo = OscarProperties.getInstance().getProperty("clinic_no", "").trim();%>
<bean:message key="oscar.billing.CA.ON.billingON.OB.SLIcode"/>: <br>
	<select name="xml_slicode">
		<option value="0000" ><bean:message key="oscar.billing.CA.ON.billingON.OB.SLIcode.NA" /></option>
        <% if (!"0000".equals(clinicNo)) { %>
        <option value="<%= clinicNo %>" <%= sliCode.equals(clinicNo) ? "selected" : "" %>><%= clinicNo + " | " %><bean:message key="oscar.billing.CA.ON.billingON.OB.SLIcode.Clinic" /></option>
        <% } %>
		<option value="HDS " <%=sliCode.startsWith("HDS")?"selected":""%>><bean:message key="oscar.billing.CA.ON.billingON.OB.SLIcode.HDS" /></option>
		<option value="HED " <%=sliCode.startsWith("HED")?"selected":""%>><bean:message key="oscar.billing.CA.ON.billingON.OB.SLIcode.HED" /></option>
		<option value="HIP " <%=sliCode.startsWith("HIP")?"selected":""%>><bean:message key="oscar.billing.CA.ON.billingON.OB.SLIcode.HIP" /></option>
		<option value="HOP " <%=sliCode.startsWith("HOP")?"selected":""%>><bean:message key="oscar.billing.CA.ON.billingON.OB.SLIcode.HOP" /></option>
		<option value="HRP " <%=sliCode.startsWith("HRP")?"selected":""%>><bean:message key="oscar.billing.CA.ON.billingON.OB.SLIcode.HRP" /></option>
		<option value="IHF " <%=sliCode.startsWith("IHF")?"selected":""%>><bean:message key="oscar.billing.CA.ON.billingON.OB.SLIcode.IHF" /></option>
		<option value="OFF " <%=sliCode.startsWith("OFF")?"selected":""%>><bean:message key="oscar.billing.CA.ON.billingON.OB.SLIcode.OFF" /></option>
		<option value="OTN " <%=sliCode.startsWith("OTN")?"selected":""%>><bean:message key="oscar.billing.CA.ON.billingON.OB.SLIcode.OTN" /></option>
		<option value="PDF " <%=sliCode.startsWith("PDF")?"selected":""%>><bean:message key="oscar.billing.CA.ON.billingON.OB.SLIcode.PDF" /></option>
		<option value="RTF " <%=sliCode.startsWith("RTF")?"selected":""%>><bean:message key="oscar.billing.CA.ON.billingON.OB.SLIcode.RTF" /></option>
	</select>
</div><!--span4-->
</div><!-- span10 -->
</div><!--well-->


<div class="row well well-small">

<table class="table table-striped table-hover">
<thead>
	<tr>
        <th><bean:message key="billing.billingCorrection.formServiceCode" /></th>
        <th><bean:message key="billing.billingCorrection.formDescription" /></th>
        <th><bean:message key="billing.billingCorrection.formDiagnosticCode" /></th>
        <th><bean:message key="billing.billingCorrection.formUnit" /></th>
        <th align="right"><bean:message key="billing.billingCorrection.formFee" /></th>
        <th style="text-align: center">Settle</th>
        <th>&nbsp;</th>
	</tr>
</thead>

<tbody id="billingServices">
<%//
        String serviceCode = "";
        String serviceDesc = "";
        String billAmount = "";
        String diagCode = "";
        String diagDesc = "";
        String billingunit = "";
        String itemStatus = "";

        if (bFlag && bCh1 != null) {
            BillingONService billingONService = (BillingONService) SpringUtils.getBean("billingONService"); 
            List<BillingONItem> bItems = billingONService.getNonDeletedInvoices(bCh1.getId());
            
            if (!bItems.isEmpty()) {

                for (int i = 0; i < bItems.size(); i++) {
                    //multisite. skip display if billing provider_no not in current access privacy
                    if (!isMultiSiteProvider) 
                        continue;

                    serviceCode = "";
                    serviceDesc = "";
                    billAmount = "";								
                    billingunit = "";								
                    itemStatus = "";

                    if (i < bItems.size()) {

                        BillingONItem bItem = bItems.get(i);
                   
                        BillingService bService = null;                
                        if (bItem.getServiceCode().startsWith("_"))                    
                            bService = bServiceDao.searchPrivateBillingCode(bItem.getServiceCode(),bItem.getServiceDate());                
                        else                    
                            bService = bServiceDao.searchBillingCode(bItem.getServiceCode(),"ON",bItem.getServiceDate());

                        serviceCode = bItem.getServiceCode();
                        serviceDesc = bService == null ? "N/A" : bService.getDescription();
                        billAmount = new BigDecimal(bItem.getFee()).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
                        diagCode = bItem.getDx();
                        billingunit = bItem.getServiceCount();								
                        itemStatus = bItem.getStatus().equals("S") ? "checked" : "";                      
                    }

                    rowCount = rowCount + 1;
%>

<tr id="billingService<%=rowCount-1%>">
    <td width="25%">
        <input type="hidden" name="xml_service_code<%=rowCount-1%>" value="<%=serviceCode%>">
        <input type="hidden" name="row" value="<%=rowCount-1%>">

        <div class="input-append" style="max-width: 120px;">
            <input type="text" name="servicecode<%=rowCount-1%>" value="<%=serviceCode%>" style="max-width: 100px;" />
            <a href="javascript:scScriptAttach('servicecode<%=rowCount-1%>')" class="btn"><i class="icon icon-search"></i></a>
        </div>
    </td>
    <td id = "description<%=rowCount - 1%>" name = "description<%=rowCount - 1%>"><font size="-1"><%=serviceDesc%></font></td>
    <td width="20%">
        <input type="hidden" name="xml_diagnostic_code<%=rowCount-1%>" value="<%=diagCode%>" />

        <div class="input-append" style="max-width: 100px;">
            <input type="text"  name="dxCode<%=rowCount-1%>" value="<%=diagCode%>" maxlength="4" style="max-width: 90px;"/>
            <a href="javascript:ScriptAttach('dxCode<%=rowCount-1%>')" class="btn"><i class="icon icon-search"></i></a>
        </div>
    </td>
    <td class="control-group">
        <input type="hidden" name="xml_billing_unit<%=rowCount-1%>" value="<%=billingunit%>">
        <input type="text" style="width: 100%" id="billingunit<%=rowCount-1%>" name="billingunit<%=rowCount-1%>" value="<%=billingunit%>" size="5" maxlength="2" onchange="validateInt(this)" onblur="validateInt(this)" />
        <label id="billingunit<%=rowCount-1%>_error" class="control-label" for="billingunit<%=rowCount-1%>" style="display:none;font-size:x-small">Must be a whole number</label>
    </td>
    <td align="right">
        <input type="hidden" name="xml_billing_amount<%=rowCount-1%>" value="<%=billAmount%>">
        <input type="text" style="width: 100%" size="5" maxlength="7" id="billingamount<%=rowCount-1%>" name="billingamount<%=rowCount-1%>"
               value="<%=billAmount%>" onblur="parseTwoDecimalPlaces(this)" onchange="javascript:validateNum(this)">
    </td>
    <td style="text-align: center">
        <input type="checkbox" name="itemStatus<%=rowCount-1%>" id="itemStatus<%=rowCount-1%>" value="S" <%=itemStatus %>>
    </td>
    <td>
        <a href="javascript:removeService(<%=rowCount-1%>);" class="btn" title="Remove"><i class="icon icon-remove"></i></a>
    </td>
</tr>
<%		
                }	
            }  
        }
%>
</tbody>
</table>
</div>


<div class="row well well-small">
    <div class="span12">
        <label style="font-weight: bold;">Add Service Code</label>
        <div class="input-append">
            <input type="text"  name="newServiceCode" value="" class="span8">
            <a href="javascript:scScriptAttachNew('newServiceCode')" class="btn"><i class="icon icon-search"></i></a>
        </div>
    </div>
</div>
	
<div class="row well well-small">
<div class="span10">

<%
	if (securityInfoManager.hasPrivilege(loggedInInfo, "_billing", "w", null)) {
%>
<% if (request.getParameter("admin")!=null || request.getParameter("adminSubmit")!=null || request.getHeader("referer").contains("administration") ) { %>
<input type="hidden" name="adminSubmit" value="adminSubmit">
<input class="btn btn-primary" type="submit" name="submit" onclick="return validateAllItems();" value="Save">
<% } else { %>
<input class="btn btn-primary" type="submit" name="submit" onclick="return validateAllItems();" value="Save">
<input class="btn" type="submit" name="submit" onclick="return validateAllItems();" value="Save&Correct Another">
<% } %>
<% } %>

<% if (billNo!=null) { %>

		<a id="reprintLink" name="reprintLink" href="billingON3rdInv.jsp?billingNo=<%=billNo%>" class="btn"><i class="icon icon-print"></i> Reprint</a>
<% } %>
	
<br><br>
               
                <div class="row">
                <div class="span5">
                    <bean:message key="billing.billingCorrection.msgNotes"/>:<br>
                    <textarea name="comment" cols="32" rows=4><%=comment %></textarea>
                    <%               
                        
                        if (thirdParty && bCh1 != null && OscarProperties.getInstance().hasProperty("invoice_due_date")) {
                            BillingONExt bExtDueDate = bExtDao.getDueDate(bCh1);
                            String dueDateStr;
	                                                               
                            if (bExtDueDate == null) {
                                Integer numDaysTilDue = Integer.parseInt(OscarProperties.getInstance().getProperty("invoice_due_date", "0"));
                                dueDateStr = DateUtils.sumDate(bCh1.getBillingDate(), numDaysTilDue, request.getLocale());
                            } else {
                                dueDateStr = bExtDueDate.getValue();
                            }
                    %>
                    <br/>
                    <!--  
                    <bean:message key="billing.billingCorrection.dueDate"/><img src="../../../images/cal.gif" id="invoiceDueDate_cal" />
                    :<input type="text" maxlength="10" id="invoiceDueDate" name="invoiceDueDate" value="<%=dueDateStr%>"/>
                    -->
					<div class="span2">		
					<label><bean:message key="billing.billingCorrection.dueDate"/>:</label>
					<label class="input-append">
						<input type="text" name="invoiceDueDate" id="invoiceDueDate" value="<%=dueDateStr%>" pattern="^\d{4}-((0\d)|(1[012]))-(([012]\d)|3[01])$" autocomplete="off" style="width:90px" />
						<span class="add-on"><i class="icon-calendar"></i></span>
					</label>
					</div>
                    <% } %>
                </div>
                
                    <div class="span5" id="thirdParty" style=" <%=thirdParty ? "" : "display:none"%>">
                        <a href="#" onclick="search3rdParty('billTo');return false;"><bean:message key="billing.billingCorrection.msgPayer"/></a><br>
                        <textarea id="billTo" name="billTo" cols="32" rows=4><%=payer%></textarea>
                          <% String useDemoClinicInfoOnInvoice = oscar.OscarProperties.getInstance().getProperty("useDemoClinicInfoOnInvoice","");
                             if (bCh1 != null && !useDemoClinicInfoOnInvoice.isEmpty() && useDemoClinicInfoOnInvoice.equals("true")) { 
                                BillingONExt bExtUseBillTo = bExtDao.getUseBillTo(bCh1);
                                String selectUseBillTo=""; 
                                                               
                                if ((bExtUseBillTo != null) && bExtUseBillTo.getValue().equalsIgnoreCase("on")) {
                                    selectUseBillTo = "checked";
                                } 
                           %>
                                <br><bean:message key="billing.billingCorrection.useDemoContactYesNo"/>:<input type="checkbox" name="overrideUseDemoContact" id="overrideUseDemoContact" <%=selectUseBillTo%> />
                          <% } %>
                    </div>
                    </div>
 
</div>
</div>

</div>
<div id="thirdPartyPymnt" style="<%= (thirdParty && bCh1 != null) ? "" : "display:none" %>">
<%=htmlPaid %>
</div>

</html:form>
        
<div >



<% if (thirdParty && bCh1 != null && OscarProperties.getInstance().hasProperty("invoice_due_date")) { %>
<script type="text/javascript">
    var startDate = $("#invoiceDueDate").datepicker({
    	format : "yyyy-mm-dd",
        todayBtn: 'linked',
        autoclose: true,
    });
</script>
<% } %>
<%!
    String nullToEmpty(String str) {
        return (str == null ? "" : str);
    }	
%>


</div>


<div class="modal fade" id="invoiceHistory" role="dialog" style="left: 35%;width: 850px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Invoice # <%=StringUtils.trimToEmpty(billNo)%> History</h4>
            </div>

            <div class="modal-body">
                <select class="form-control input-sm" onchange="updateHistory(this.value)">
                    <option value="">-- History Filters --</option>
                    <option value="billing_date">Billing Date</option>
                    <option value="status">Billing Status</option>
                    <option value="errors">Errors</option>
                    <optgroup label="Service Code History">
                        <option value="service_code">Codes Added/Removed</option>
                        <option value="service_code_dx">Dx Codes</option>
                        <% if (thirdParty) { %>
                        <option value="service_code_payment">3rd Party Payments</option>
                        <% } %>
                    </optgroup>
                </select>

                <div id="historyContainer">
                    <table id="history" class="table table-condensed table-hover">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>Date/Time Updated</th>
                            <th>Action</th>
                            <th>Data</th>
                            <th>Provider</th>
                            <th>File Name</th>
                        </tr>
                        </thead>

                        <tbody id="historyBody"></tbody>
                    </table>
                </div>

                <div id="loading" style="text-align:center;display: none;">
                    <i class="icon icon-lg icon-refresh icon-spin"></i>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Return to Invoice</button>
            </div>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    var startDate = $("#xml_appointment_date").datepicker({
        format : "yyyy-mm-dd",
        todayBtn: 'linked',
        autoclose: true,
    });
    var startDate = $("#xml_vdate").datepicker({
        format : "yyyy-mm-dd",
        todayBtn: 'linked',
        autoclose: true,
    });

    window.setTimeout(function() {
    	  $("#alert_message").fadeTo(500, 0).slideUp(500, function(){
    	    $(this).remove(); 
    	  });
    }, 5000);
	
    $( document ).ready(function() {
    	parent.parent.resizeIframe($('html').height());

    });

function display3rdPartyPayments() {
    popupPage('800', '860', 'billingON3rdPayments.do?method=listPayments&billingNo=<%= billNo %>');
}
</script>

</html:html>
