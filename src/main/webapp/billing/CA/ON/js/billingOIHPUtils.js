function inputSearchDemographicUpdateAutoComplete() {
	$("#inputSearchDemographic").autocomplete( {
		source: JSON.parse($('#availableDemographics').val()),
		minLength: 0,  
		focus: function(event, ui) {
			return false;
		}
	});
}


function displayDemographicFilterIfIsEnableInTheSettings(isToHideDemographicDiv) {			
	let divDemographicFilter = $('#divDemographicFilter');
	if (!divDemographicFilter) {
		return;
	}
	if (isToHideDemographicDiv) {
		divDemographicFilter.hide();
	} else {
		divDemographicFilter.show();
	}
}