<!DOCTYPE html>
<%--

    Copyright (c) 2008-2012 Indivica Inc.
    
    This software is made available under the terms of the
    GNU General Public License, Version 2, 1991 (GPLv2).
    License details are available via "indivica.ca/gplv2"
    and "gnu.org/licenses/gpl-2.0.html".
    
--%>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%
    if(session.getAttribute("userrole") == null )  response.sendRedirect("../logout.jsp");
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean bodd = false;
    EDTFolder folder = EDTFolder.getFolder(request.getParameter("folder"));
    String folderPath = folder.getPath();
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.backup,_admin.billing" rights="r" reverse="<%=true%>">
	<%response.sendRedirect("/oscar/logout.jsp");%>
</security:oscarSec>
<%@ page import="java.util.*,oscar.*,java.io.*,java.net.*,oscar.util.*,org.apache.commons.io.FileUtils,java.text.SimpleDateFormat,org.oscarehr.billing.CA.ON.util.EDTFolder,org.oscarehr.util.MiscUtils"%>
<%@page import="org.oscarehr.util.LoggedInInfo"%>
<%@page import="org.oscarehr.util.SpringUtils"%>
<%@page import="org.oscarehr.common.model.BillingPermission"%>
<%@page import="org.oscarehr.common.dao.BillingPermissionDao"%>
<%@ page import="java.util.regex.Pattern" %>
<%@ page import="java.util.regex.Matcher" %>
<%@ page import="org.oscarehr.util.WebUtilsOld" %>
<jsp:useBean id="oscarVariables" class="java.util.Properties" scope="session" />
<html>
<head>
<title><bean:message key="admin.admin.viewMOHFiles"/></title>
<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>

<script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery.js"></script>

<link href="<%=request.getContextPath() %>/css/bootstrap.min.css" rel="stylesheet">

<script LANGUAGE="JavaScript">
<!--
function viewMOHFile (filename) {
	var form = document.getElementById("form");
	document.getElementById("filename").value = filename;

    var selected = document.getElementsByClassName("info");
    if (selected && selected[0]) {
        selected[0].removeAttribute("class", "info");
    };
    var row = document.getElementById(filename);
    row.setAttribute("class", "info");

    var fileType = filename.substring(0,1).toUpperCase();
  if (filename.substring(filename.length - 4).toLowerCase() == ".zip") {
    var r = alert("Please unzip " + filename + " before processing.");
    location.href = "viewMOHFiles.jsp";
    return;
  } else if (filename.substring(filename.length - 4).toLowerCase() == ".pdf") {
    var page = "/oscar/dms/ManageDocument.do?method=display&filename=" + filename + "&moh=true";
    var windowprops = "location=no, scrollbars=yes, menubars=no, toolbars=no, resizable=yes, top=0, left=0";
    var popup = window.open(page, "_blank", windowprops);
    popup.focus();
  } else if (fileType == "P" || fileType == "S") {
    form.action = "/<%= OscarProperties.getInstance().getProperty("project_home") %>/servlet/oscar.DocumentUploadServlet";
  } else if (fileType == "L") {
    var page = "<%=request.getContextPath() %>/oscarBilling/DocumentErrorReportUpload.do?filename=" + filename;
    var windowprops = "location=no, scrollbars=yes, menubars=no, toolbars=no, resizable=yes, top=0, left=0";
    var popup = window.open(page, "_blank", windowprops);
    popup.focus();
  } else {
    var page = "<%=request.getContextPath() %>/oscarBilling/DocumentErrorReportUpload.do?filename=" + filename;
    var windowprops = "location=no, scrollbars=yes, menubars=no, toolbars=no, resizable=yes, top=0, left=0";
    var popup = window.open(page, "_blank", windowprops);
    popup.focus();
  }

    if (fileType == "P" || fileType == "S" || fileType == "L") {
	    form.submit()
    }
}

function toggleCheckboxes(el) {
	jQuery("input[name='mohFile']").attr("checked", jQuery(el).attr("checked"));
}

function checkForm() {
	if (jQuery("input[name='mohFile']:checked").size() > 0) { return true; }
	alert("Please select a file first.");
	return false;
}
//-->
</script>
<%=WebUtilsOld.popErrorMessagesAsAlert(session)%>
</head>

<body>
<h3><bean:message key="admin.admin.viewMOHFiles"/></h3>

<div class="container-fluid well">

<form id="form" method="POST">
	<input type="hidden" id="filename" name="filename" value="" >
</form>

		<%
		    if (folder == EDTFolder.INBOX) {
		%> <form method="POST" action="<%=request.getContextPath()%>/billing/CA/ON/moveMOHFiles.do" onsubmit="return checkForm();" class="form-inline">
<% } %>

<% if (folder  == EDTFolder.INBOX) {%>

		<input type="submit" value="Archive" class="btn">
<%}%>

		View: 
		<select name="folder" onchange="location.href='viewMOHFiles.jsp?folder='+this.options[selectedIndex].value">
			<option value="inbox" <% if (folder == EDTFolder.INBOX) {%>selected<%}%>>Inbox</option>
			<option value="outbox" <% if (folder == EDTFolder.OUTBOX) {%>selected<%}%>>Outbox</option>
			<option value="sent" <% if (folder == EDTFolder.SENT) {%>selected<%}%>>Sent</option>
			<option value="archive" <% if (folder == EDTFolder.ARCHIVE) {%>selected<%}%>>Archive</option>
		</select> 

		

<table class="table table-striped table-hover">
<thead>
	<tr>
		<% if (folder == EDTFolder.INBOX) {%><th><input type="checkbox" onclick="toggleCheckboxes(this)" title="select all"></th><% } %>
		<th>View File</th>
		<% if (folder.providesAccessToFiles()) {%><th>Download File</th><%}%>
		<th>Date</th>
	</tr>
</thead>

<tbody>
	<%
    if ( folderPath == null || folderPath.equals("") ) {
        Exception e = new Exception("Unable to find the key ONEDT_"+folder.name()+" in the properties file.  Please check the value of this key or add it if it is missing.");
        throw e;
    }
    session.setAttribute("backupfilepath", folderPath);

   // unzip any files indicated by <unzipfile>
   String zname = request.getParameter("unzipfile");
   String unzipMSG = "";
   try {
     if ( zname != null && !zname.equals("") ) {
    	 Boolean unzipDone = zip.unzipXML(folderPath,zname);
         if (!unzipDone) {
             unzipMSG="(Cannot unzip)";
         }
     }
   } catch(Exception e) {
       MiscUtils.getLogger().error("viewMOHFiles: unzip file Unhandled exception:", e);
       unzipMSG="(Cannot unzip)";
   }

    File f = new File(folderPath);
    File[] contents = null;
    if (f.exists()) { contents = f.listFiles(); }
    else { contents = new File[]{}; }

    Arrays.sort(contents,new FileSortByDate());
    if (contents == null) {
        Exception e = new Exception("Unable to find any files in the directory "+folderPath+".  (If this is the incorrect directory, please modify the value of ONEDT_"+folder.name()+" in your properties file to reflect the correct directory).");
        throw e;
    }
    // Creates a pattern for matching billing file names
    Pattern pattern = Pattern.compile("\\w{2,3}?-?(\\d+).*\\..+");
    
    for(int i=0; i<contents.length; i++) {
      bodd = bodd?false:true ;
      if (contents[i].isDirectory() || contents[i].getName().startsWith(".")) continue;
      if (contents[i].getName().endsWith(".sh")) continue;
      String archiveElement = "<td ><input type='checkbox' name='mohFile' value='"+URLEncoder.encode(contents[i].getName())+"' title='select to archive'/></td>";
        LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);  
        BillingPermissionDao billingPermissionDao = SpringUtils.getBean(BillingPermissionDao.class);

        boolean allowed = true;
        String filename = contents[i].getName();
        String fileProviderNumber = "";
        // Creates a new matcher for the filename
        Matcher matcher = pattern.matcher(filename);
        if (matcher.find()) {
            fileProviderNumber = matcher.group(1);
        }

        if (!fileProviderNumber.isEmpty()) {
            String curUser_providerno = loggedInInfo.getLoggedInProviderNo();
            List<String> ohipNos = billingPermissionDao.getOhipNosNotAllowed(curUser_providerno, BillingPermission.VIEW_MOH_FILES);
            if (ohipNos.contains(fileProviderNumber)) {
                continue;
            }
        }
      if (folder == EDTFolder.INBOX || folder == EDTFolder.ARCHIVE) {
          out.println("<tr id='" + URLEncoder.encode(contents[i].getName()) + "'>"+(folder == EDTFolder.INBOX ? archiveElement : "")+"<td><a HREF='javascript:void(0)' onclick='viewMOHFile(\""+URLEncoder.encode(contents[i].getName())+"\")'>"+contents[i].getName()+unzipMSG+"</a></td>") ;
          out.println("<td><a HREF='../../../servlet/BackupDownload?filename="+URLEncoder.encode(contents[i].getName())+"'>Download</a></td>") ;
      } else {
          out.println("<tr><td>"+contents[i].getName()+"</td>") ;
      }
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      Date d = new Date(contents[i].lastModified());
      out.println("<td align='right'>"+sdf.format(d)+"</td></tr>"); //+System.getProperty("file.separator")
    }
%>
</tbody>
</table>

<% if (contents.length > 20) { %>

<% if (folder  == EDTFolder.INBOX) {%>

		<input type="submit" value="Archive" class="btn">
<%}%>

		<select name="folder" onchange="location.href='viewMOHFiles.jsp?folder='+this.options[selectedIndex].value">
			<option value="inbox" <% if (folder == EDTFolder.INBOX) {%>selected<%}%>>Inbox</option>
			<option value="outbox" <% if (folder == EDTFolder.OUTBOX) {%>selected<%}%>>Outbox</option>
			<option value="sent" <% if (folder == EDTFolder.SENT) {%>selected<%}%>>Sent</option>
			<option value="archive" <% if (folder == EDTFolder.ARCHIVE) {%>selected<%}%>>Archive</option>
		</select> 



<% } %>
<% if (folder == EDTFolder.INBOX) {%> </form> <% } %>
</div><!--container-->
</body>
</html>