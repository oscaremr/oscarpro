<%--

    Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

--%>

<%@ page import="java.math.*, java.util.*, java.io.*, java.sql.*, java.net.*, oscar.*, oscar.util.*, oscar.MyDateFormat"%>
<%@ page import="oscar.oscarBilling.ca.on.pageUtil.*"%>

<%@page import="org.oscarehr.util.SpringUtils" %>
<%@page import="org.oscarehr.common.model.RaHeader" %>
<%@page import="org.oscarehr.common.dao.RaHeaderDao" %>
<%@page import="org.oscarehr.common.model.RaDetail" %>
<%@page import="org.oscarehr.common.dao.RaDetailDao" %>
<%@page import="org.oscarehr.common.model.Billing" %>
<%@page import="org.oscarehr.common.dao.BillingDao" %>
<%@ page import="org.oscarehr.common.model.BillingONCHeader1" %>
<%@ page import="org.oscarehr.common.dao.BillingONCHeader1Dao" %>
<%
	RaHeaderDao dao = SpringUtils.getBean(RaHeaderDao.class);
	BillingONCHeader1Dao billingONCHeader1Dao = SpringUtils.getBean(BillingONCHeader1Dao.class);
	RaDetailDao raDetailDao = SpringUtils.getBean(RaDetailDao.class);
%>


<%
String raNo = "", flag="", plast="", pfirst="", pohipno="", proNo="";
String filepath="", filename = "", header="", headerCount="", total="", paymentdate="", payable="", totalStatus="", deposit=""; //request.getParameter("filename");
String transactiontype="", providerno="", specialty="", account="", patient_last="", patient_first="", provincecode="", hin="", ver="", billtype="", location="";
String servicedate="", serviceno="", servicecode="", amountsubmit="", amountpay="", amountpaysign="", explain="", error="";
String proFirst="", proLast="", demoFirst="", demoLast="", apptDate="", apptTime="", checkAccount="";
String errorAccount ="", eFlag="", noErrorAccount="";

raNo = request.getParameter("rano") != null ? request.getParameter("rano") : "";
if (raNo.compareTo("") == 0)  return;

ArrayList noErrorBill = new ArrayList();
ArrayList errorBill = new ArrayList();
ArrayList errorBillNoQ = new ArrayList();


for(RaDetail rad:raDetailDao.search_raerror35(Integer.parseInt(raNo), "I2", "35", proNo+"%")) {
	account = String.valueOf(rad.getBillingNo());
	hin = rad.getHin() != null ? rad.getHin() : "";

	if (hin.length() > 10) {
		hin = hin.substring(0, 10);
	}

	BillingONCHeader1 invoice = billingONCHeader1Dao.getInvoice(rad.getBillingNo(), hin, rad.getProviderOhipNo());
	if (invoice != null) {
		errorBill.add(account);
		if (!rad.getServiceCode().matches("Q011A|Q020A|Q130A|Q131A|Q132A|Q133A|Q140A|Q141A|Q142A")) {
			errorBillNoQ.add(account);
		}
	}
}

account = "";
List<RaDetail> res = raDetailDao.search_raNonError35(Integer.parseInt(raNo),"I2","35",proNo+"%");

for (RaDetail ra : res) {
	account = String.valueOf(ra.getBillingNo());
	eFlag="1";
	hin = ra.getHin() != null ? ra.getHin() : "";

	if (hin.length() > 10) {
		hin = hin.substring(0, 10);
	}

	BillingONCHeader1 invoice = billingONCHeader1Dao.getInvoice(ra.getBillingNo(), hin, ra.getProviderOhipNo());
	if (invoice != null) {
		for (int i = 0; i < errorBill.size(); i++) {
			errorAccount = (String) errorBill.get(i);
			if (errorAccount.compareTo(account) == 0) {
				eFlag = "0";
				break;
			}
		}

		if (eFlag.compareTo("1") == 0 && !noErrorBill.contains(account)) noErrorBill.add(account);
	}
}

String[] param0 = new String[2];
param0[0] = raNo;
param0[1] = proNo+"%";


// settle Qcodes
account = "";
for(RaDetail r: raDetailDao.search_raNonErrorQ(Integer.parseInt(raNo), proNo+"%")) {
	account = String.valueOf(r.getBillingNo());
	eFlag="1";
	hin = r.getHin() != null ? r.getHin() : "";

	if (hin.length() > 10) {
		hin = hin.substring(0, 10);
	}

	BillingONCHeader1 invoice = billingONCHeader1Dao.getInvoice(r.getBillingNo(), hin, r.getProviderOhipNo());
	if (invoice != null) {
		for (int i = 0; i < errorBillNoQ.size(); i++) {
			errorAccount = (String) errorBillNoQ.get(i);
			if (errorAccount.compareTo(account) == 0) {
				eFlag = "0";
				break;
			}
		}

		if (eFlag.compareTo("1") == 0 && !noErrorBill.contains(account)) noErrorBill.add(account);
	}
}

BillingRAPrep obj = new BillingRAPrep();
for (int j=0; j< noErrorBill.size();j++){
	noErrorAccount=(String) noErrorBill.get(j);
	obj.updateBillingStatus(noErrorAccount, "S");

}

int recordAffected1 = 0;
	 
	 RaHeader raHeader = dao.find(Integer.parseInt(raNo));
	 if(raHeader != null) {
		 raHeader.setStatus("F");
		 dao.merge(raHeader);
		recordAffected1++;
	 }
%>

<script LANGUAGE="JavaScript">
self.close();
self.opener.refresh();
</script>
