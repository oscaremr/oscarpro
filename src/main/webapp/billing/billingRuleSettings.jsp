<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed = true;
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.misc" rights="r" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../securityError.jsp?type=_admin&type=_admin.misc");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.*"%>
<%@ page import="oscar.util.StringUtils" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="org.json.JSONObject" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%
    JSONObject rules = request.getAttribute("billingRules") != null ? new JSONObject((Map<String, String>) request.getAttribute("billingRules")) : new JSONObject();
	Boolean isBillingA233ReplaceA234AndG435A = request.getAttribute("billingA233ReplaceA234AndG435A") != null ? (Boolean) request.getAttribute("billingA233ReplaceA234AndG435A") : false;
	String billingA233Checked = "";
	if (isBillingA233ReplaceA234AndG435A != null && isBillingA233ReplaceA234AndG435A) {
		billingA233Checked = "checked";
	}
%>
<head>
    <title>Billing Rules</title>
    <script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>

    <link href="<%=request.getContextPath()%>/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="../share/yui/css/fonts-min.css"/>
    <link rel="stylesheet" type="text/css" href="../share/yui/css/autocomplete.css"/>
    <link rel="stylesheet" type="text/css" media="all" href="<%=request.getContextPath()%>/js/jquery_css/smoothness/jquery-ui-1.10.2.custom.min.css"  />
    <link rel="stylesheet" href="../css/alertify.core.css" type="text/css">
    <link rel="stylesheet" href="../css/alertify.default.css" type="text/css">
    <link href="<%=request.getContextPath()%>/css/bootstrap2-toggle.min.css" rel="stylesheet">
    

    <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="<%= request.getContextPath() %>/js/jquery-ui-1.10.2.custom.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath() %>/share/javascript/Oscar.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/alertify.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/js/bootstrap2-toggle.min.js"></script>
    <script type="text/javascript" src="<%=request.getContextPath()%>/billing/billingRuleSettings.js"></script>
</head>


<body vlink="#0000FF" class="BodyStyle">
<script>
    $(document).ready(function(){
        let rules = <%=rules%>;
        init(rules);
    });
</script>
<h4>Billing Rule Settings</h4>
<form id="billingRuleSettings" name="billingRuleSettings" method="post" action="<%=request.getContextPath() %>/billing/ManageBillingRules.do?method=save">
    <table id="manageBillingRules" name="manageBillingRules" class="table table-bordered table-condensed">
        <thead>
        <tr style="background-color:#f5f5f5;">
            <th>
                Pediatric Billing
                <label class="pull-right" for="pediatric">
                    <input type="checkbox" value="true" id="pediatric" name="pediatric" class="categoryToggle" data-toggle="toggle" data-onstyle="success" data-size="small" />
                </label>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <label>
                    K267A - patients 2-11 years, billed once per 365 days
                </label>

                <label class="pull-right codeToggle" for="K267A">
                    <input type="hidden" id="K267A" name="K267A" class="code" category="pediatric" value="false" />
                </label>
            </td>
        </tr>
        
        <tr>
            <td>
                <label>
                    K269A - patients 12-17 years, billed once per 365 days
                    <label class="pull-right codeToggle" for="K269A">
                        <input type="hidden" id="K269A" name="K269A" class="code" category="pediatric" value="false" />
                    </label>
                </label>
            </td>
        </tr>
        
        <tr>
            <td>
                <label>
                	Adjust invoice from A234/G435 to A233 automatically, based on the last 365 days (Ophthalmology clinics only)
	                <label class="pull-right" for="pediatric">
	                    <input type="checkbox" value="true" id="billingA233ReplaceA234AndG435A" <%= billingA233Checked %> name="billingA233ReplaceA234AndG435A" class="categoryToggle" data-toggle="toggle" data-onstyle="success" data-size="small" />
	                </label>
                </label>
            </td>
        </tr>

        </tbody>
    </table>
    <button type="button" class="btn btn-primary" onclick="save()">Save</button>
</form>
</body>
</html>

