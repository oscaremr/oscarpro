/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

const button = document.getElementById("btnCalculate");
const btnOpt = document.getElementById("btnAdditionalFields");
const btnClear = document.getElementById("btnClear");
let withOptionalFields = false;

function toggleOptFields() {
  const optFields = document.getElementById("optionalFields");
  if (optFields.style.display === 'none') {
    document.getElementById("albumin-input").value = '0';
    document.getElementById("phosphorous-input").value = '0';
    document.getElementById("bicarbonate-input").value = '0';
    document.getElementById("calcium-input").value = '0';
    optFields.style.display = 'block';
    document.getElementById('btnAdditionalFields').innerHTML = 'Hide Optional Fields'
    withOptionalFields = true;
  } else {
    optFields.style.display = 'none';
    document.getElementById('btnAdditionalFields').innerHTML = 'Show Optional Fields'
    withOptionalFields = false;
  }
}

function calculate() {
  let values = fetchValues();
  if (!areValuesValid(values)) {
    alert("Please, review the form!");
    return;
  }
  calculateTwoAndFiveYears(values)
}

function calculateOnload() {
  let values = fetchValues();
  if (!areValuesValid(values)){
    return;
  }
  calculateTwoAndFiveYears(values)
}

function calculateTwoAndFiveYears(values) {
  document.getElementById("twoYrsResult").textContent = calculateTwoYrs(values);
  document.getElementById("fiveYrsResult").textContent = calculateFiveYrs(values);
}

function fetchValues() {
  let values = {};

  values.age = parseFloat(document.getElementById("age-input").value);
  values.egfr = parseFloat(document.getElementById('egfr-input').value);
  values.acr = parseFloat(document.getElementById('urine-input').value);
  values.units = document.getElementById("urine-select").value;
  values.north_america = document.getElementById("region").value === 'na';
  values.male = document.getElementById("sex-select").value === 'M' ? 1 : 0;
  values.acr_x = Math.log(values.acr * (values.units === 'mgg' ? 1 : 8.84));

  if (withOptionalFields) {
    values.bicarbonate =  document.getElementById("bicarbonate-input").value;
    values.calcium = parseFloat(document.getElementById("calcium-input").value);
    values.albumin = parseFloat(document.getElementById("albumin-input").value);
    values.phosphorous = parseFloat(document.getElementById("phosphorous-input").value);

    if (document.getElementById("calcium-select").value === 'mmoll') {
      values.calcium = parseFloat((values.calcium / .25));
    }

    if (document.getElementById("albumin-select").value === 'gl') {
      values.albumin = parseFloat((values.albumin / 10));
    }

    if (document.getElementById("phos-select").value === 'mmoll') {
      values.phosphorous = parseFloat((values.phosphorous / .323));
    }
  }
  return values;
}

function areValuesValid(values) {
  let sex = document.getElementById("sex-select").value;
  let region = document.getElementById("region").value;
  if (values.age == ""
      || values.egfr == ""
      || values.acr == ""
      || values.units ==  ""
      || region == ""
      || sex == "") {
    return false;
  }

  if (withOptionalFields) {
    let calcium_sel = document.getElementById("calcium-select").value;
    let albumin_sel = document.getElementById("albumin-select").value;
    let phos_sel = document.getElementById("phos-select").value;
    let bicarb_select = document.getElementById("bicarbonate-select").value;

    if (calcium_sel == ""
        || albumin_sel == ""
        || phos_sel == ""
        || calcium_sel == ""
        || values.bicarbonate == ""
        || values.calcium == ""
        || values.albumin == ""
        || values.phosphorous == ""
        || bicarb_select == "") {
      return false;
    }
  }

  return true;
}

function calculateTwoYrs(values) {
  if (withOptionalFields) {
    values.baseline = values.north_america ? 0.978 : 0.9827;
  } else {
    values.baseline = values.north_america ? 0.975 : 0.9832;
  }
  const result = calculate_risk(values);
  return parseFloat(parseInt(result * 10000) / 100);
}

function calculateFiveYrs(values) {
  if (withOptionalFields) {
    values.baseline = values.north_america ? 0.9301 : 0.9245;
  } else {
    values.baseline = values.north_america ? 0.924 : 0.9365;
  }

  const result = calculate_risk(values);
  return parseFloat(parseInt(result * 10000) / 100);
}

function calculate_risk(values) {
  if (withOptionalFields) {
    return calculateWithOptFields(values);
  } else {
    return calculateWithoutOptFields(values);
  }
}

function calculateWithOptFields(values) {
  return 1.0 -
    Math.pow(
      values.baseline,
      Math.exp(
        -0.1992 * (values.age / 10 - 7.036) +
        0.1602 * (values.male - 0.5642) -
        0.4919 * (values.egfr / 5 - 7.222) +
        0.3364 * (values.acr_x - 5.137) -
        0.3441 * (values.albumin - 3.997) +
        0.2604 * (values.phosphorous - 3.916) -
        0.07354 * (values.bicarbonate - 25.57) -
        0.2228 * (values.calcium - 9.355)
      )
    );
}

function calculateWithoutOptFields(values) {
  return 1.0 -
    Math.pow(
      values.baseline,
      Math.exp(
        -0.2201 * (values.age / 10 - 7.036) +
        0.2467 * (values.male - 0.5642) -
        0.5567 * (values.egfr / 5 - 7.222) +
        0.451 * (values.acr_x - 5.137)
      )
    );
}

function clear() {
  document.getElementById("twoYrsResult").textContent = "0";
  document.getElementById("fiveYrsResult").textContent = "0";
  document.getElementById("age-input").value = "";
  document.getElementById('egfr-input').value = "";
  document.getElementById('urine-input').value = "";
  document.getElementById("sex-select").value = "";
  document.getElementById("urine-select").value = "";
  document.getElementById("region").value = "";
  if (withOptionalFields) {
    document.getElementById("bicarbonate-input").value = "";
    document.getElementById("calcium-input").value = "";
    document.getElementById("albumin-input").value = "";
    document.getElementById("phosphorous-input").value = "";
  }
}

btnOpt.addEventListener("click", toggleOptFields);
button.addEventListener("click", calculate);
btnClear.addEventListener("click", clear);

window.onload = calculateOnload;