<%--

   Copyright (c) 2021 WELL EMR Group Inc.
   This software is made available under the terms of the
   GNU General Public License, Version 2, 1991 (GPLv2).
   License details are available via "gnu.org/licenses/gpl-2.0.html".

--%>

<%@ page import="oscar.oscarEncounter.oscarMeasurements.util.KidneyFailureRiskCalculatorDataFetcher" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>

<html>

  <head>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <c:set var="sex" scope="session" value='<%=request.getParameter("sex")%>'/>
    <link rel="stylesheet" type="text/css" media="all"
        href="${pageContext.request.contextPath}/oscarEncounter/calculators/kidneyFailureRiskCalculator/kidneyFailureRiskCalculator.css"/>
    <title><bean:message key="oscarEncounter.Index.kidneyFailureRiskCalculator"/></title>
  </head>

<%
  KidneyFailureRiskCalculatorDataFetcher.MeasurementValue calcData = new KidneyFailureRiskCalculatorDataFetcher().fetch(request);
  String EGFR = KidneyFailureRiskCalculatorDataFetcher.KidneyFailureEnum.EGFR.toString();
  String ACR = KidneyFailureRiskCalculatorDataFetcher.KidneyFailureEnum.ACR.toString();

  String egfrVal = calcData.getValueOrDefault(EGFR);
  String acrVal = calcData.getValueOrDefault(ACR);
%>

  <body>
    <div class="calculator">
      <div class="calculator_header">
        <span class="kidney_failure">Kidney Failure</span> <br>
        <span class="risk_calculator">Risk Calculator</span>
      </div>
      <div class="row">
        <div class="col">
          <label> Age (Yrs) </label>
          <input id="age-input" type="number" value="<%= request.getParameter("age")%>">
        </div>
        <div class="col">
          <label> Sex </label>
          <select id="sex-select">
            <option value="">Select</option>
            <c:if test="${sex == 'M'}">
              <option value="M" selected="selected">Male</option>
            </c:if>
            <c:if test="${sex != 'M'}">
              <option value="M">Male</option>
            </c:if>
            <c:if test="${sex == 'F'}">
              <option value="F" selected="selected">Female</option>
            </c:if>
            <c:if test="${sex != 'F'}">
              <option value="F">Female</option>
            </c:if>
          </select>
        </div>
        <div class="col">
          <label> Region </label>
          <select id="region">
            <option value="">Select</option>
            <option value="na" selected="selected">North America</option>
            <option value="nna">Non-North America</option>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <label> eGFR (ml/min/1.73m<sup>2</sup>) </label>
          <input id="egfr-input" type="number" value="<%= egfrVal %>">
        </div>
        <div class="col">
          <label> Urine Albumin: Creatinine Ratio </label>
          <input id="urine-input" type="number" value="<%= acrVal %>">
        </div>
        <div class="col">
          <label> Units </label>
          <select id="urine-select">
            <option value="">Select</option>
            <option value="mgg">mg/g</option>
            <option value="mgmmol" selected="selected">mg/mmol</option>
          </select>
        </div>
      </div>
      <button id="btnAdditionalFields" type="button">Show Optional Fields</button>
      <div id="optionalFields" style="display: none">
        <div class="row">
          <div class="col">
            <label> Albumin </label>
            <input id="albumin-input" type="number">
          </div>
          <div class="col">
            <label> Units </label>
            <select id="albumin-select">
              <option value="">Select</option>
              <option value="gdl">g/dL</option>
              <option value="gl">g/L</option>
            </select>
          </div>
          <div class="col">
            <label> Phosphorous </label>
            <input id="phosphorous-input" type="number">
          </div>
          <div class="col">
            <label> Units </label>
            <select id="phos-select">
              <option value="">Select</option>
              <option value="mgdl">mg/dL</option>
              <option value="mmoll">mmol/L</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <label> Bicarbonate </label>
            <input id="bicarbonate-input" type="number">
          </div>
          <div class="col">
            <label> Units </label>
            <select id="bicarbonate-select">
              <option value="">Select</option>
              <option value="meql">mEq/L</option>
              <option value="mmoll">mmol/L</option>
            </select>
          </div>
          <div class="col">
            <label> Corrected Calcium </label>
            <input id="calcium-input" type="number">
          </div>
          <div class="col">
            <label> Units </label>
            <select id="calcium-select">
              <option value="">Select</option>
              <option value="mgdl">mg/dL</option>
              <option value="mmoll">mmol/L</option>
            </select>
          </div>
        </div>
      </div>
      <button id="btnCalculate" type="button">Calculate</button>
      <button id="btnClear" type="button">Clear</button>
      <div class="assessment">
        <span>Patient risk of progression to kidney failure requiring dialysis or transplant:</span>
        <div class="results row">
          <div class="at2yrs col">
            <span>AT 2 YEARS</span> <br>
            <span id="twoYrsResult">0</span>%
          </div>
          <div class="at5yrs col">
            <span>AT 5 YEARS</span> <br>
            <span id="fiveYrsResult">0</span>%
          </div>
        </div>
      </div>
      <div style="margin-top: 10px">
        <label>For more informations, visit:
          <a href="https://kidneyfailurerisk.com/">www.kidneyfailurerisk.com</a>
        </label>
      </div>
    </div>

    <script type="text/javascript"
        src="${pageContext.request.contextPath}/oscarEncounter/calculators/kidneyFailureRiskCalculator/kidneyFailureRiskCalculator.js">
    </script>
  </body>

</html>
