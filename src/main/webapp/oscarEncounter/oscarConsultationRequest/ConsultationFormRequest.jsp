<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%@ taglib uri="/WEB-INF/wellAiVoice.tld" prefix="well-ai-voice"%>
<%
      String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
      boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_con" rights="r" reverse="<%=true%>">
	<%authed=false; %>
	<%response.sendRedirect("../../securityError.jsp?type=_con");%>
</security:oscarSec>
<%
if(!authed) {
	return;
}
%>

<%@page import="org.oscarehr.util.WebUtilsOld"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/rewrite-tag.tld" prefix="rewrite"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!-- add for special encounter -->
<%@ taglib uri="http://www.caisi.ca/plugin-tag" prefix="plugin" %>
<%@ taglib uri="/WEB-INF/special_tag.tld" prefix="special" %>
<!-- end -->
<%@ taglib uri="/WEB-INF/oscar-tag.tld" prefix="oscar"%>


<%@page import="java.util.ArrayList, java.util.Collections, java.util.List, java.util.*, oscar.util.StringUtils, oscar.dms.*, oscar.oscarEncounter.pageUtil.*,oscar.oscarEncounter.data.*, oscar.OscarProperties, oscar.oscarLab.ca.on.*"%>
<%@page import="org.oscarehr.casemgmt.service.CaseManagementManager,org.oscarehr.casemgmt.model.CaseManagementNote,org.oscarehr.casemgmt.model.Issue,org.springframework.web.context.support.*,org.springframework.web.context.*"%>

<%@page import="org.springframework.web.context.support.WebApplicationContextUtils"%>
<%@page import="oscar.oscarEncounter.oscarConsultationRequest.pageUtil.EctConsultationFormRequestForm"%>
<%@page import="oscar.oscarEncounter.oscarConsultationRequest.pageUtil.EctConsultationFormRequestUtil"%>
<%@page import="oscar.oscarDemographic.data.DemographicData"%>
<%@page import="oscar.oscarEncounter.oscarConsultationRequest.pageUtil.EctViewRequestAction"%>
<%@page import="org.oscarehr.util.MiscUtils,oscar.oscarClinic.ClinicData"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils" %>
<%@ page import="org.oscarehr.util.LoggedInInfo"%>
<%@ page import="org.oscarehr.util.DigitalSignatureUtils"%>
<%@ page import="org.oscarehr.ui.servlet.ImageRenderingServlet"%>
<%@page import="org.oscarehr.util.SpringUtils"%>
<%@page import="org.oscarehr.util.MiscUtils, org.oscarehr.PMmodule.caisi_integrator.CaisiIntegratorManager, org.oscarehr.caisi_integrator.ws.CachedDemographicNote"%>
<%@page import="org.oscarehr.PMmodule.dao.ProgramDao, org.oscarehr.PMmodule.model.Program" %>
<%@page import="oscar.oscarDemographic.data.DemographicData, oscar.oscarRx.data.RxProviderData, oscar.oscarRx.data.RxProviderData.Provider, oscar.oscarClinic.ClinicData"%>
<%@ page import="java.net.URLEncoder" %>
<%@ page import="org.oscarehr.common.dao.*" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ page import="java.io.File" %>
<%@ page import="org.springframework.web.util.HtmlUtils" %>
<%@ page import="org.apache.cxf.javascript.JavascriptUtils" %>
<%@ page import="oscar.util.ConversionUtils" %>
<%@ page import="org.oscarehr.common.model.*" %>
<%@ page import="oscar.log.LogAction" %>
<%@ page import="oscar.log.LogConst" %>
<%@ page import="org.owasp.encoder.Encode" %>
<%@ page import="org.oscarehr.managers.ConsultationManager" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>
<%@ page import="static org.apache.commons.lang.StringUtils.isNotBlank" %>
<%@ page import="static org.apache.commons.lang.StringUtils.isBlank" %>

<jsp:useBean id="displayServiceUtil" scope="request" class="oscar.oscarEncounter.oscarConsultationRequest.config.pageUtil.EctConDisplayServiceUtil" />
<jsp:useBean id="providerBean" class="java.util.Properties"	scope="session" />

<html:html locale="true">

<%! boolean bMultisites=org.oscarehr.common.IsPropertiesOn.isMultisitesEnable(); %>

<%
	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);

	boolean enableConsultationDynamicLabellingSystemPreference = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("consultation_dynamic_labelling_enabled", false);
	boolean enableConsultationPatientWillBookSystemPreference = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("enable_consultation_patient_will_book", false);
	boolean isConsultationDateReadOnly = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("enable_consultation_lock_referral_date", true);
	boolean consultationSignatureEnabledSystemPreference = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("consultation_signature_enabled", false);
	boolean attachmentManagerConsultationEnabled = SystemPreferencesUtils
			.isReadBooleanPreferenceWithDefault("attachment_manager.consultation.enabled", false);

	displayServiceUtil.estSpecialist();	

	//multi-site support
	String appNo = request.getParameter("appNo");
	appNo = (appNo==null ? "" : appNo);

	String defaultSiteName = "";
	Integer defaultSiteId = 0;
	List<Site> sites = new ArrayList<Site>();
	
	if (bMultisites) {
		SiteDao siteDao = (SiteDao)WebApplicationContextUtils.getWebApplicationContext(application).getBean("siteDao");
		sites = siteDao.getActiveSitesByProviderNo((String) session.getAttribute("user"));
		if (appNo != "") {
			String apptSiteName = siteDao.getSiteNameByAppointmentNo(appNo);
			for (Site site : sites) {
				if (site.getName().equals(apptSiteName)) {
					defaultSiteName = site.getName();
				}
			}
		}
	}

	String demo = request.getParameter("de");
		String requestId = request.getParameter("requestId");
		
	Integer archiveId = null;
	String archiveIdStr = request.getParameter("archiveId");
	if (!isBlank(archiveIdStr)
			&& org.apache.commons.lang.StringUtils.isNumeric(archiveIdStr)) {
		archiveId = Integer.parseInt(archiveIdStr);
	}
		// segmentId is != null when viewing a remote consultation request from an hl7 source
		String segmentId = request.getParameter("segmentId");
		String team = request.getParameter("teamVar");
		String providerNo = (String)session.getAttribute("user");
		String providerName = loggedInInfo.getLoggedInProvider().getFullName();
		String providerNoFromChart = null;
		DemographicData demoData = null;
		org.oscarehr.common.model.Demographic demographic = null;

		RxProviderData rx = new RxProviderData();
		List<Provider> prList = rx.getAllProviders();
		Provider thisProvider = rx.getProvider(providerNo);
		ClinicData clinic = new ClinicData();
		
		EctConsultationFormRequestUtil consultUtil = new EctConsultationFormRequestUtil();
		
		if( team == null || team.trim().equals("")){
			team = consultUtil.getProviderTeam(providerNo);
		}
		
		if (archiveId != null) {
			consultUtil.estRequestFromArchiveId(loggedInInfo, archiveId.toString());
		}
		else if (requestId != null) {
			consultUtil.estRequestFromId(loggedInInfo, requestId);
		}
		if (demo == null) demo = consultUtil.demoNo;
		
		Boolean lockForm = archiveId != null || (consultUtil.locked && OscarProperties.getInstance().isPropertyActive("consultation_lock_on_print"));

		ArrayList<String> users = (ArrayList<String>)session.getServletContext().getAttribute("CaseMgmtUsers");
		boolean useNewCmgmt = false;
		WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
		CaseManagementManager cmgmtMgr = null;
		if (users != null && users.size() > 0 && (users.get(0).equalsIgnoreCase("all") || Collections.binarySearch(users, providerNo) >= 0))
		{
			useNewCmgmt = true;
			cmgmtMgr = (CaseManagementManager)ctx.getBean("caseManagementManager");
		}

		UserPropertyDAO userPropertyDAO = (UserPropertyDAO)ctx.getBean("UserPropertyDAO");
		UserProperty fmtProperty = userPropertyDAO.getProp(providerNo, UserProperty.CONSULTATION_REQ_PASTE_FMT);
		String pasteFmt = fmtProperty != null?fmtProperty.getValue():null;

		if(userPropertyDAO.getProp(providerNo,"rxAddress")!=null && userPropertyDAO.getProp(providerNo,"rxAddress").getValue() != null && userPropertyDAO.getProp(providerNo,"rxAddress").getValue().length()>0){
		    //default letterhead address to rxAddress if it is set it in preferences
			consultUtil.letterheadAddress =(userPropertyDAO.getProp(providerNo,"rxAddress").getValue()  + " " + userPropertyDAO.getProp(providerNo, "rxCity").getValue()  + " " + userPropertyDAO.getProp(providerNo, "rxProvince").getValue() + " " +userPropertyDAO.getProp(providerNo, "rxPostal").getValue());
		}

		if(userPropertyDAO.getProp(providerNo,"rxPhone")!=null && userPropertyDAO.getProp(providerNo,"rxPhone").getValue() != null && userPropertyDAO.getProp(providerNo,"rxPhone").getValue().length()>0){
			//default letterhead phone to rxPhone if it is set it in preferences
			consultUtil.letterheadPhone = userPropertyDAO.getProp(providerNo,"rxPhone").getValue();
		}

		if(userPropertyDAO.getProp(providerNo,"faxnumber")!=null && userPropertyDAO.getProp(providerNo,"faxnumber").getValue() != null  && userPropertyDAO.getProp(providerNo,"faxnumber").getValue().length()>0){
			//default letterhead fax to faxnumber if it is set it in preferences
			consultUtil.letterheadFax = userPropertyDAO.getProp(providerNo,"faxnumber").getValue();

		}

		if (demo != null)
		{
			demoData = new oscar.oscarDemographic.data.DemographicData();
			demographic = demoData.getDemographic(loggedInInfo, demo);
			
			providerNoFromChart = demographic.getProviderNo();
		}
		else if (requestId == null && segmentId == null)
		{
			MiscUtils.getLogger().debug("Missing both requestId and segmentId.");
		}

		if (demo != null) consultUtil.estPatient(loggedInInfo, demo);
		consultUtil.estActiveTeams();

		String lhndType = "provider"; //set default as provider
		String providerDefault = providerNo==null?"":providerNo;
		if (requestId != null) {
			// reading existing consult request
			String logData = "requestId=" + requestId;
			LogAction.addLog(loggedInInfo, LogConst.READ, "Consultation Request", requestId, demo, logData);
		}

		//MRP
		ProviderDao providerDao = (ProviderDao)SpringUtils.getBean("providerDao");
		org.oscarehr.common.model.Provider mrp = providerDao.getProvider(providerNoFromChart);
		org.oscarehr.common.model.Provider loggedIn = providerDao.getProvider(providerNo);
		String mrpCPSO = "";
		String userCPSO = "";
		if (mrp!=null) {mrpCPSO = mrp.getPractitionerNo();}
		if (loggedIn!=null) {userCPSO = loggedIn.getPractitionerNo();}

		if (consultUtil.letterheadName != null && !consultUtil.letterheadName.equalsIgnoreCase("")) {providerDefault = consultUtil.letterheadName;}
		else if (userCPSO != null && !userCPSO.equals("")) {providerDefault = providerNo;}
		else if (mrpCPSO != null && !mrpCPSO.equals("")) {providerDefault = providerNoFromChart;}

		if(consultUtil.letterheadName == null ){
			//nothing saved so find default
			UserProperty lhndProperty = userPropertyDAO.getProp(providerNo, UserProperty.CONSULTATION_LETTERHEADNAME_DEFAULT);
			String lhnd = lhndProperty != null?lhndProperty.getValue():null;
			//1 or null = provider, 2 = MRP and 3 = clinic

			if(lhnd!=null){
				if(lhnd.equals("2")){
					//mrp
					providerDefault = providerNoFromChart;
				}else if(lhnd.equals("3")){
					//clinic
					lhndType="clinic";
				}
			}

		}

		if (providerDefault==null||providerDefault.equals("")) {providerDefault = providerNo;}

		UserProperty defaultRxAddress = userPropertyDAO.getProp(providerDefault,"rxAddress");
		if(defaultRxAddress!=null && defaultRxAddress.getValue() != null && defaultRxAddress.getValue().length()>0){
		    //default letterhead address to rxAddress if it is set it in preferences
			consultUtil.letterheadAddress =(defaultRxAddress.getValue()  + " " + userPropertyDAO.getProp(providerDefault, "rxCity").getValue()  + " " + userPropertyDAO.getProp(providerDefault, "rxProvince").getValue() + " " +userPropertyDAO.getProp(providerDefault, "rxPostal").getValue());
		}
		UserProperty defaultRxPhone = userPropertyDAO.getProp(providerDefault,"rxPhone");
		if(defaultRxPhone!=null && defaultRxPhone.getValue() != null && defaultRxPhone.getValue().length()>0){
			//default letterhead phone to rxPhone if it is set it in preferences
			consultUtil.letterheadPhone = defaultRxPhone.getValue();
		}
		UserProperty defaultRxFaxNumber = userPropertyDAO.getProp(providerDefault,"faxnumber");
		if(defaultRxFaxNumber!=null && defaultRxFaxNumber.getValue() != null  && defaultRxFaxNumber.getValue().length()>0){
			//default letterhead fax to faxnumber if it is set it in preferences
			consultUtil.letterheadFax = defaultRxFaxNumber.getValue();
		}

		// Get Default Appointment Instructions
		ConsultationManager consultationManager = SpringUtils.getBean(ConsultationManager.class);
		String defaultAppointmentInstructions = consultationManager.getConsultDefaultAppointmentInstructions();

		boolean consultationFaxEnabled = SystemPreferencesUtils
				.isReadBooleanPreferenceWithDefault("consultation_fax_enabled", true);

		if (request.getParameter("error") != null){
%>
<SCRIPT LANGUAGE="JavaScript">
	var message = "The form could not be printed due to an error.";
	<% if (request.getAttribute("printError") != null) { %>
    	message +="\n <%=request.getAttribute("printError")%> \n";
	<%	request.removeAttribute("printError");
	}
	else if (request.getAttribute("faxError") != null) { %>
    	message +="\n <%=request.getAttribute("faxError")%> \n";
	<%
		request.removeAttribute("faxError");
	}
	%>
	message += "Please refer to the server logs for more details.";
	alert(message);
    </SCRIPT>
<%
	}

		java.util.Calendar calender = java.util.Calendar.getInstance();
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
		String formattedDate = sdf.format(calender.getTime());

		OscarProperties props = OscarProperties.getInstance();
		
		// Look up list 
		org.oscarehr.managers.LookupListManager lookupListManager = SpringUtils.getBean(org.oscarehr.managers.LookupListManager.class);
		pageContext.setAttribute("appointmentInstructionList", lookupListManager.findLookupListByName(loggedInInfo, "consultApptInst") ); 
		
		ConsultationServiceDao consultationServiceDao = SpringUtils.getBean(ConsultationServiceDao.class);
		
		
%><head>
<c:set var="ctx" value="${pageContext.request.contextPath}" scope="request"/>
	<script src="<%=request.getContextPath()%>/JavaScriptServlet" type="text/javascript"></script>
<script>
	var ctx = '<%=request.getContextPath()%>';
	var requestId = '<%=request.getParameter("requestId")%>';
	var demographicNo = '<%=demo%>';
	var demoNo = '<%=demo%>';
	var appointmentNo = '<%=appNo%>';
	var providerName = '<%=providerName%>';
</script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/global.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery_oscar_defaults.js"></script>

	<script type="application/javascript">
		var $jq = jQuery.noConflict(true);
	</script>
<script type="text/javascript" src="<%=request.getContextPath()%>/share/javascript/prototype.js"></script>
<link rel="stylesheet" type="text/css" media="all"
	href="<%=request.getContextPath()%>/share/calendar/calendar.css" title="win2k-cold-1" />
<!-- main calendar program -->
<script type="text/javascript" src="<%=request.getContextPath()%>/share/calendar/calendar.js"></script>
<!-- language for the calendar -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/share/calendar/lang/calendar-en.js"></script>
<!-- the following script defines the Calendar.setup helper function, which makes
       adding a calendar a matter of 1 or 2 lines of code. -->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/share/calendar/calendar-setup.js"></script>

	<script type="text/javascript" src="<%= request.getContextPath() %>/share/javascript/Oscar.js"></script>
   <script src="<c:out value="${ctx}/js/jquery.js"/>"></script>
   <script>
     jQuery.noConflict();
   </script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery-3.1.0.min.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath() %>/js/jquery.maskedinput.js"></script>
	<script type="application/javascript">
		var jQuery_3_1_0 = jQuery.noConflict(true);
	</script>


	<oscar:customInterface section="conreq"/>

<title><bean:message
	key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.title" />
</title>
<html:base />
<style type="text/css">

/* Used for "import from enctounter" button */
input.btn{
   color:black;
   font-family:'trebuchet ms',helvetica,sans-serif;
   font-size:84%;
   font-weight:bold;
   background-color:#B8B8FF;
   border:1px solid;
   border-top-color:#696;
   border-left-color:#696;
   border-right-color:#363;
   border-bottom-color:#363;
}

.doc {
    color:blue;
}

.lab {
    color: #CC0099;
}

.hrm {
	color: red;
}
.eForm {
	color: #008000;
}
.form {
    color: orange;
}
td.tite {

background-color: #bbbbFF;
color : black;
font-size: 12pt;

}

td.tite1 {

background-color: #ccccFF;
color : black;
font-size: 12pt;

}

th,td.tite2 {

background-color: #BFBFFF;
color : black;
font-size: 12pt;

}

td.tite3 {

background-color: #B8B8FF;
color : black;
font-size: 12pt;

}

td.tite4 {

background-color: #ddddff;
color : black;
font-size: 12pt;

}

td.stat{
font-size: 10pt;
}

input.righty{
text-align: right;
}

select#appointmentInstructions {
    width:90%;
}
	
span.oceanRefer	{
	display: flex;
	align-items: center;
}
	
span.oceanRefer a {
	margin-right: 5px;
}
</style>
</head>



<link type="text/javascript" src="../consult.js" />

<script language="JavaScript" type="text/javascript">

var servicesName = new Object();   		// used as a cross reference table for name and number
var services = new Array();				// the following are used as a 2D table for makes and models
var specialists = new Array();
var specialistFaxNumber = "";
<%oscar.oscarEncounter.oscarConsultationRequest.config.data.EctConConfigurationJavascriptData configScript;
				configScript = new oscar.oscarEncounter.oscarConsultationRequest.config.data.EctConConfigurationJavascriptData();
				out.println(configScript.getJavascript());%>

/////////////////////////////////////////////////////////////////////
function initMaster() {
	makeSpecialistslist(2);
}
//-------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////
// create car make objects and fill arrays
//==========
function K( serviceNumber, service ){

	//servicesName[service] = new ServicesName(serviceNumber);
        servicesName[service] = serviceNumber;
	services[serviceNumber] = new Service( );
}
//-------------------------------------------------------------------
	
	//-----------------disableDateFields() disables date fields if "Patient Will Book" selected
	var disableFields=false;
</script>

<% if (enableConsultationPatientWillBookSystemPreference) { %>
	<script type="text/javascript" >

	
	function disableDateFields(){
		if(document.forms[0].patientWillBook.checked){
			setDisabledDateFields(document.forms[0], true);
		}
		else{
			setDisabledDateFields(document.forms[0], false);
		}
	}
	</script>
<% } %>

<% if (!enableConsultationPatientWillBookSystemPreference) { %>
	<script type="text/javascript" >
	
	function disableDateFields(){

		setDisabledDateFields(document.forms[0], false);

	}
	</script>
<% } %>


<script type="text/javascript" >
// btnReminders
$jq(document).ready(function(){
	$jq(".clinicalData").click(function(){
		var data = new Object();
		var target = "#" + this.id.split("_")[1];		
		data.method = this.id.split("_")[0];
		data.demographicNo = <%= demo %>;
		getClinicalData( data, target )
	});
	document.getElementById("letterheadName").onchange();
})

function getClinicalData( data, target ) {
	$jq.ajax({
		method : "POST",
		url : "${ pageContext.request.contextPath }/oscarConsultationRequest/consultationClinicalData.do",
		data : data,
		dataType : 'JSON',
		success: function(data) {
			var json = JSON.parse(data);
			$jq(target).val( $jq(target).val() + "\n" + json.note );			
		}
	});
}

function setDisabledDateFields(form, disabled)
{
	//form.appointmentYear.disabled = disabled;
	//form.appointmentMonth.disabled = disabled;
	//form.appointmentDay.disabled = disabled;
	form.appointmentHour.disabled = disabled;
	form.appointmentMinute.disabled = disabled;
	form.appointmentPm.disabled = disabled;
}

function disableEditing() {
	if (disableFields) {
		form=document.forms[0];

		<% if (!lockForm) { %>
		form.status[0].disabled = disableFields;
		form.status[1].disabled = disableFields;
		form.status[2].disabled = disableFields;
		form.status[3].disabled = disableFields;
		<% } %>

		form.providerNo.disabled = disableFields;
		form.referalDate.disabled = disableFields;
		disableIfExists(form.specialist, disableFields);
		disableIfExists(form.service, disableFields);
		form.urgency.disabled = disableFields;
		form.phone.disabled = disableFields;
		form.fax.disabled = disableFields;
		form.address.disabled = disableFields;
		form.sendTo.disabled = disableFields;

		form.reasonForConsultation.disabled = disableFields;
		form.clinicalInformation.disabled = disableFields;
		form.concurrentProblems.disabled = disableFields;
		form.currentMedications.disabled = disableFields;
		form.allergies.disabled = disableFields;
		form.annotation.disabled = disableFields;

		form.letterheadName.disabled = disableFields;
		//form.ext_letterheadTitle.disabled = disableFields;
		disableIfExists(form.ext_letterheadTitle, disableFields);
		disableIfExists(form.ext_notes, disableFields);

		disableIfExists(form.patientWillBook, disableFields);
		disableIfExists(form.siteName, disableFields);
		disableIfExists(form.update, disableFields);
		disableIfExists(form.updateAndPrint, disableFields);
		disableIfExists(form.updateAndSendElectronically, disableFields);
		disableIfExists(form.updateAndFax, disableFields);

		disableIfExists(form.submitSaveOnly, disableFields);
		disableIfExists(form.submitAndPrint, disableFields);
		disableIfExists(form.submitAndSendElectronically, disableFields);
		disableIfExists(form.submitAndFax, disableFields);
		disableIfExists(form.letterheadFax, disableFields);

		hideElement('referalDate_cal');
		hideElement('appointmentDate_cal');
		hideElement("followUpDate_cal");	}
}

function disableIfExists(item, disabled)
{
	if (item!=null) item.disabled=disabled;
}

function hideElement(elementId) {
	let element = document.getElementById(elementId)
	if (element != null) {
		element.style.display = 'none';
	}
}

//------------------------------------------------------------------------------------------
/////////////////////////////////////////////////////////////////////
// create car model objects and fill arrays
//=======
function D( servNumber, specNum, phoneNum ,SpecName,SpecFax,SpecAddress){
    var specialistObj = new Specialist(servNumber,specNum,phoneNum, SpecName, SpecFax, SpecAddress);
    services[servNumber].specialists.push(specialistObj);
}
//-------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////
function Specialist(makeNumber,specNum,phoneNum,SpecName, SpecFax, SpecAddress){

	this.specId = makeNumber;
	this.specNbr = specNum;
	this.phoneNum = phoneNum;
	this.specName = SpecName;
	this.specFax = SpecFax;
	this.specAddress = SpecAddress;
}
//-------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////
// make name constructor
function ServicesName( makeNumber ){

	this.serviceNumber = makeNumber;
}
//-------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////
// make constructor
function Service(  ){

	this.specialists = new Array();
}
//-------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////
// construct model selection on page
function fillSpecialistSelect( aSelectedService ){


	var selectedIdx = aSelectedService.selectedIndex;
	var makeNbr = (aSelectedService.options[ selectedIdx ]).value;

	document.EctConsultationFormRequestForm.specialist.options.selectedIndex = 0;
	document.EctConsultationFormRequestForm.specialist.options.length = 1;

	document.EctConsultationFormRequestForm.phone.value = ("");
	document.EctConsultationFormRequestForm.fax.value = ("");
	document.EctConsultationFormRequestForm.address.value = ("");

	if ( selectedIdx == 0)
	{
		return;
	}

        var i = 1;
	var specs = (services[makeNbr].specialists);
	for ( var specIndex = 0; specIndex < specs.length; ++specIndex ){
		   aPit = specs[ specIndex ];	   	
           document.EctConsultationFormRequestForm.specialist.options[ i++ ] = new Option( aPit.specName , aPit.specNbr );
	}

}
//-------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////
function fillSpecialistSelect1( makeNbr )
{
	//document.EctConsultationFormRequestForm.specialist.options.length = 1;

	var specs = (services[makeNbr].specialists);
	var i=1;
    var match = false;
        
	for ( var specIndex = 0; specIndex < specs.length; ++specIndex )
	{
		aPit = specs[specIndex];

		if(aPit.specNbr=="<%=consultUtil.specialist%>"){
			//look for matching specialist on spec list and make option selected
			match=true;
			document.EctConsultationFormRequestForm.specialist.options[i] = new Option(aPit.specName, aPit.specNbr,false ,true );
		}else{
			//add specialist on list as normal
			document.EctConsultationFormRequestForm.specialist.options[i] = new Option(aPit.specName, aPit.specNbr );
		}

		i++;
	}

	<%if(requestId!=null){ %>
		if(!match){ 
			//if no match then most likely doctor has been removed from specialty list so just add specialist
          document.EctConsultationFormRequestForm.specialist.options[0] =
              new Option(
              "<%=Encode.forHtmlAttribute(consultUtil.getSpecialistsName(consultUtil.specialist))%>",
              "<%=Encode.forHtmlAttribute(consultUtil.specialist)%>",
                  false,
                  true
              );

			//don't display if no consultant was saved
			<%if(!consultUtil.specialist.equals("null")){%>
			document.getElementById("consult-disclaimer").style.display='inline';
			<%}else{%>
			//display so user knows why field is empty
			document.EctConsultationFormRequestForm.specialist.options[0] = new Option("No Consultant Saved", "-1");
			<%}%>
		}
	<%}%>

}
//-------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////
function setSpec(servNbr,specialNbr){
//    //window.alert("get Called");
    specs = (services[servNbr].specialists);
//    //window.alert("got specs");
    var i=1;
    var NotSelected = true;
 
    for ( var specIndex = 0; specIndex < specs.length; ++specIndex ){
//      //  window.alert("loop");
        aPit = specs[specIndex];
        if (aPit.specNbr == specialNbr){
//        //    window.alert("if");
            document.EctConsultationFormRequestForm.specialist.options[i].selected = true;
            NotSelected = false;
        }

        i++;
    }

    if( NotSelected )
        document.EctConsultationFormRequestForm.specialist.options[0].selected = true;
//    window.alert("exiting");

}
//=------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////
//insert first option title into specialist drop down list select box
function initSpec() {
	<%if(requestId==null){ %>
	var aSpecialist = services["-1"].specialists[0];
    document.EctConsultationFormRequestForm.specialist.options[0] = new Option(aSpecialist.specNbr, aSpecialist.specId);
    <%}%>
}

/////////////////////////////////////////////////////////////////////
function initService(ser,name,spec,specname,phone,fax,address){
	var i = 0;
	var isSel = 0;
	var strSer = new String(ser);
	var strNa = new String(name);
	var strSpec = new String(spec);
	var strSpecNa = new String(specname);
	var strPhone = new String(phone);
	var strFax = new String(fax);
	var strAddress = new String(address);

	var isSerDel=1;//flagging service if deleted: 1=deleted 0=active

	$H(servicesName).each(function(pair){
	if( pair.value == strSer ) {
	isSerDel = 0;
	}
	});

	if (isSerDel==1 && strSer != "null") {
	K(strSer,strNa);
	D(strSer,strSpec,strPhone,strSpecNa,strFax,strAddress);
    }

        $H(servicesName).each(function(pair){
              var opt = new Option( pair.key, pair.value );
              if( pair.value == strSer ) {
                opt.selected = true;
                fillSpecialistSelect1( pair.value );
              }
              $("service").options.add(opt);

        });

/*	for (aIdx in servicesName){
	   var serNBR = servicesName[aIdx].serviceNumber;
   	   document.EctConsultationFormRequestForm.service.options[ i ] = new Option( aIdx, serNBR );
	   if (serNBR == strSer){
	      document.EctConsultationFormRequestForm.service.options[ i ].selected = true;
	      isSel = 1;
          //window.alert("get here"+serNBR);
	      fillSpecialistSelect1( serNBR );
          //window.alert("and here");
	   }
	   if (isSel != 1){
	      document.EctConsultationFormRequestForm.service.options[ 0 ].selected = true;
	   }
	   i++;
	}*/
	}
//-------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////
function onSelectSpecialist(SelectedSpec)	{
	var selectedIdx = SelectedSpec.selectedIndex;
	var form=document.EctConsultationFormRequestForm;

	if (selectedIdx==null || selectedIdx==-1 || (SelectedSpec.options[ selectedIdx ]).value == "-1") {   		//if its the first item set everything to blank
		form.phone.value = ("");
		form.fax.value = ("");
		form.address.value = ("");

		enableDisableRemoteReferralButton(form, true);

		<%
		if (consultationFaxEnabled) {//
		%>
		specialistFaxNumber = "";
		updateFaxButton();
		<% } %>
		
		return;
	}
	var selectedService = document.EctConsultationFormRequestForm.service.value;  				// get the service that is selected now
	var specs = (services[selectedService].specialists); 			// get all the specs the offer this service
    
	// load the text fields with phone fax and address for past consult review even if spec has been removed from service list
	<%if(requestId!=null && !consultUtil.specialist.equals("null")){ %>
	form.phone.value = '<%=StringEscapeUtils.escapeJavaScript(consultUtil.specPhone)%>';
	form.fax.value = '<%=StringEscapeUtils.escapeJavaScript(consultUtil.specFax)%>';					
	form.address.value = '<%=StringEscapeUtils.escapeJavaScript(consultUtil.specAddr) %>'.replace(new RegExp("\\\\r\\\\n", 'g'), "\r\n");

	//make sure this dislaimer is displayed
	document.getElementById("consult-disclaimer").style.display='inline';
	<%}%>
	
								
        for( var idx = 0; idx < specs.length; ++idx ) {
            aSpeci = specs[idx];									// get the specialist Object for the currently selected spec
            if( aSpeci.specNbr == SelectedSpec.value ) {
            	form.phone.value = (aSpeci.phoneNum.replace(null,""));
            	form.fax.value = (aSpeci.specFax.replace(null,""));					// load the text fields with phone fax and address
            	form.address.value = (aSpeci.specAddress.replace(null,"").replace(new RegExp("\\\\r\\\\n", 'g'), "\r\n"));
            	
       			//since there is a match make sure the dislaimer is hidden
       			document.getElementById("consult-disclaimer").style.display='none';
        	
            	<%
        		if (consultationFaxEnabled) {//
				%>
				specialistFaxNumber = aSpeci.specFax.trim();
				
				updateFaxButton();
        		<% } %>
            	
				if (aSpeci.specNbr != null) {
					$jq.getJSON("getProfessionalSpecialist.jsp", {id: aSpeci.specNbr},
						function (xml) {
							var hasUrl = xml.eDataUrl != null && xml.eDataUrl != "";
							enableDisableRemoteReferralButton(form, !hasUrl);

							var annotation = document.getElementById("annotation");
							annotation.value = xml.annotation;
						}
					);
				}

            	break;
            }
        }//spec loop
	 
	}

//-----------------------------------------------------------------

/////////////////////////////////////////////////////////////////////
function FillThreeBoxes(serNbr)	{

	var selectedService = document.EctConsultationFormRequestForm.service.value;  				// get the service that is selected now
	var specs = (services[selectedService].specialists);					// get all the specs the offer this service

        for( var idx = 0; idx < specs.length; ++idx ) {
            aSpeci = specs[idx];									// get the specialist Object for the currently selected spec
            if( aSpeci.specNbr == serNbr ) {
                document.EctConsultationFormRequestForm.phone.value = (aSpeci.phoneNum);
                document.EctConsultationFormRequestForm.fax.value = (aSpeci.specFax);					// load the text fields with phone fax and address
                document.EctConsultationFormRequestForm.address.value = (aSpeci.specAddress).replace(new RegExp("\\\\r\\\\n", 'g'), "\r\n");
                <%
        		if (consultationFaxEnabled) {//
				%>
				specialistFaxNumber = aSpeci.specFax.trim();
				updateFaxButton();
				<% } %>
                break;
           }
        }
}
//-----------------------------------------------------------------

function enableDisableRemoteReferralButton(form, disabled)
{
	var button=form.updateAndSendElectronically;
	if (button!=null) button.disabled=disabled;
	button=form.submitAndSendElectronically;
	if (button!=null) button.disabled=disabled;

        var button=form.updateAndSendElectronicallyTop;
	if (button!=null) button.disabled=disabled;
	button=form.submitAndSendElectronicallyTop;
	if (button!=null) button.disabled=disabled;
}

//-->

function BackToOscar() {
       window.close();
}
function rs(n,u,w,h,x){
	args="width="+w+",height="+h+",resizalbe=yes,scrollbars=yes,status=0,top=60,left=30";
        remote=window.open(u,n,args);
        if(remote != null){
	   if (remote.opener == null)
		remote.opener = self;
	}
	if ( x == 1 ) { return remote; }
}

var DocPopup = null;
function popup(location) {
    DocPopup = window.open(location,"_blank","height=380,width=580");

    if (DocPopup != null) {
        if (DocPopup.opener == null) {
            DocPopup.opener = self;
        }
    }
}

function popupAttach( height, width, url, windowName){
  var page = url;
  windowprops = "height="+height+",width="+width+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=0,screenY=0,top=0,left=0";
  var popup=window.open(url, windowName, windowprops);
  if (popup != null){
    if (popup.opener == null){
      popup.opener = self;
    }
  }
  popup.focus();
  return false;
}

function popupOscarCal(vheight,vwidth,varpage) { //open a new popup window
  var page = varpage;
  windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=no,menubars=no,toolbars=no,resizable=no,screenX=0,screenY=0,top=20,left=20";
  var popup=window.open(varpage, "<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgCal"/>", windowprops);

  if (popup != null) {
    if (popup.opener == null) {
      popup.opener = self;
    }
  }
}


function paste2Note(){
	if( DocPopup != null && !DocPopup.closed ) {
        return false;
    }
	if (document.EctConsultationFormRequestForm.service.options.selectedIndex == 0){
     	return false;
  	}
	var apptDate = document.EctConsultationFormRequestForm.appointmentDate.value;
	var hasApptTime = document.EctConsultationFormRequestForm.appointmentHour.options.selectedIndex != 0 && 
	  	document.EctConsultationFormRequestForm.appointmentMinute.options.selectedIndex != 0;
	if(apptDate.length > 0 && !hasApptTime) {
		return false;
	}
	
	try{
		var faxName = "";
		var faxNo = "";
		for(var i = 0; i < $jq("#faxRecipients li").length; i ++){
			var num = 0;
			if($jq("#faxRecipients li")[i].innerHTML.indexOf("<b>,") > 0){
				num = $jq("#faxRecipients li")[i].innerHTML.indexOf("<b>,");
				var toName = $jq("#faxRecipients li")[i].innerHTML.substring(0, num);
				if(i == 0){
					faxName = toName;
				}else{
					faxName = faxName + "," + toName;
				}
			}
		}
		for(var i = 0; i < $jq("input[name='faxRecipients']").length; i ++){
			if(i == 0){
				faxNo = $jq("input[name='faxRecipients']")[i].value;
			}else{
				faxNo = faxNo + ", " + $jq("input[name='faxRecipients']")[i].value;
			}
		}
				
		var referralFax = $jq("#fax").val(); 		
		faxNo = faxNo + " " + referralFax;		
		<% 
		 String timeStamp = new SimpleDateFormat("dd-MMM-yyyy hh:mm a").format(Calendar.getInstance().getTime());
		%>
	 	var text ="[Faxed consultation to " + faxName + " Fax#: " + faxNo + " by " + providerName + " <%= timeStamp %>]" + "\n"; 
		var noteEditor = "noteEditor"+demoNo;
		
		if( window.parent.opener.document.forms["caseManagementEntryForm"] != undefined ) {
	        window.parent.opener.pasteToEncounterNote(text);
	      }else if( window.parent.opener.document.encForm != undefined ){
	        window.parent.opener.document.encForm.enTextarea.value = window.parent.opener.document.encForm.enTextarea.value + text;
	      }else if( window.parent.opener.document.getElementById(noteEditor) != undefined ){
	    	window.parent.opener.document.getElementById(noteEditor).value = window.parent.opener.document.getElementById(noteEditor).value + text; 
	      }
	}catch (e){
	      alert ("ERROR: could not paste to EMR");
	}
}

function checkForm(submissionVal,formName){
    //if document attach to consultation is still active user needs to close before submitting
    if( DocPopup != null && !DocPopup.closed ) {
        alert("Please close Consultation Documents window before proceeding");
        return false;
    }

   var msg = "<bean:message key="Errors.service.noServiceSelected"/>";
   msg  = msg.replace('<li>','');
   msg  = msg.replace('</li>','');
  if (document.EctConsultationFormRequestForm.service.options.selectedIndex == 0){
     alert(msg);
     document.EctConsultationFormRequestForm.service.focus();
     return false;
  }
  if (submissionVal.toLowerCase().includes('fax') && document.EctConsultationFormRequestForm.fax.value !== null && document.EctConsultationFormRequestForm.fax.value.trim() === '') {
      alert("Letterhead Fax number is empty, would you still like to save and submit? \nNo fax will be sent.");
	  document.EctConsultationFormRequestForm.fax.focus();
	  return false;
  }

  <% if (consultationFaxEnabled && enableConsultationDynamicLabellingSystemPreference) { %>
  let oldProviderNo = '<%= consultUtil.providerNo %>';
  let newProviderNo = document.EctConsultationFormRequestForm.providerNo.value;
  if (oldProviderNo && oldProviderNo !== 'null' && oldProviderNo !== newProviderNo) {
	  if (!confirm("Referring Practitioner has changed, would you like to make a copy of " +
			  "this consultation request with this new practitioner?")) {
		  document.EctConsultationFormRequestForm.providerNo.focus();
		  return false;
	  }
  }
  <% } %>

  $("saved").value = "true";
  document.forms[formName].submission.value=submissionVal;
  document.forms[formName].submit();
  return true;
}

function clearField(field){
	field.value = "";
}

function clearOnBackspaceDelete(e, field) {
	if(e.keyCode === 8 || e.keyCode === 46) {
		// backspace or delete pressed
		clearField(field);
	}
}

//enable import from encounter
function importFromEnct(reqInfo,txtArea)
{
    var info = "";
    switch( reqInfo )
    {
        case "MedicalHistory":
            <%String value = "";
				if (demo != null)
				{
					if (useNewCmgmt)
					{
						value = listNotes(cmgmtMgr, "MedHistory", providerNo, demo);
					}
					else
					{
						oscar.oscarDemographic.data.EctInformation EctInfo = new oscar.oscarDemographic.data.EctInformation(LoggedInInfo.getLoggedInInfoFromSession(request), demo);
						value = EctInfo.getMedicalHistory();
					}
					if (pasteFmt == null || pasteFmt.equalsIgnoreCase("single"))
					{
						value = StringUtils.lineBreaks(value);
					}
					value = org.apache.commons.lang.StringEscapeUtils.escapeJavaScript(value);
					out.println("info = '" + value + "'");
				}%>
             break;
          case "ongoingConcerns":
             <%if (demo != null)
				{
					if (useNewCmgmt)
					{
						value = listNotes(cmgmtMgr, "Concerns", providerNo, demo);
					}
					else
					{
						oscar.oscarDemographic.data.EctInformation EctInfo = new oscar.oscarDemographic.data.EctInformation(LoggedInInfo.getLoggedInInfoFromSession(request),demo);
						value = EctInfo.getOngoingConcerns();
					}
					if (pasteFmt == null || pasteFmt.equalsIgnoreCase("single"))
					{
						value = StringUtils.lineBreaks(value);
					}
					value = org.apache.commons.lang.StringEscapeUtils.escapeJavaScript(value);
					out.println("info = '" + value + "'");
				}%>
             break;
           case "FamilyHistory":
              <%if (demo != null)
				{
					if (OscarProperties.getInstance().getBooleanProperty("caisi", "on"))
					{
						oscar.oscarDemographic.data.EctInformation EctInfo = new oscar.oscarDemographic.data.EctInformation(LoggedInInfo.getLoggedInInfoFromSession(request),demo);
						value = EctInfo.getFamilyHistory();
					}
					else
					{
						if (useNewCmgmt)
						{
							value = listNotes(cmgmtMgr, "FamHistory", providerNo, demo);
						}
						else
						{
							oscar.oscarDemographic.data.EctInformation EctInfo = new oscar.oscarDemographic.data.EctInformation(LoggedInInfo.getLoggedInInfoFromSession(request),demo);
							value = EctInfo.getFamilyHistory();
						}
					}
					if (pasteFmt == null || pasteFmt.equalsIgnoreCase("single"))
					{
						value = StringUtils.lineBreaks(value);
					}
					value = org.apache.commons.lang.StringEscapeUtils.escapeJavaScript(value);
					out.println("info = '" + value + "'");
				}%>
              break;
           case "SocialHistory":
               <%if (demo != null)
 				{
 					if (useNewCmgmt)
 					{
 						value = listNotes(cmgmtMgr, "SocHistory", providerNo, demo);
 					}
 					else
 					{
 						oscar.oscarDemographic.data.EctInformation EctInfo = new oscar.oscarDemographic.data.EctInformation(LoggedInInfo.getLoggedInInfoFromSession(request),demo);
 						value = EctInfo.getSocialHistory();
 					}
 					if (pasteFmt == null || pasteFmt.equalsIgnoreCase("single"))
 					{
 						value = StringUtils.lineBreaks(value);
 					}
 					value = org.apache.commons.lang.StringEscapeUtils.escapeJavaScript(value);
 					out.println("info = '" + value + "'");
 				}%>
               break;
            case "OtherMeds":
              <%if (demo != null)
				{
					if (OscarProperties.getInstance().getBooleanProperty("caisi", "on"))
					{
						value = "";
					}
					else
					{
						if (useNewCmgmt)
						{
							value = listNotes(cmgmtMgr, "OMeds", providerNo, demo);
						}
						else
						{
							//family history was used as bucket for Other Meds in old encounter
							oscar.oscarDemographic.data.EctInformation EctInfo = new oscar.oscarDemographic.data.EctInformation(LoggedInInfo.getLoggedInInfoFromSession(request),demo);
							value = EctInfo.getFamilyHistory();
						}
					}
					if (pasteFmt == null || pasteFmt.equalsIgnoreCase("single"))
					{
						value = StringUtils.lineBreaks(value);
					}
					value = org.apache.commons.lang.StringEscapeUtils.escapeJavaScript(value);
					out.println("info = '" + value + "'");

				}%>
                break;
            case "Reminders":
              <%if (demo != null)
				{
					if (useNewCmgmt)
					{
						value = listNotes(cmgmtMgr, "Reminders", providerNo, demo);
					}
					else
					{
						oscar.oscarDemographic.data.EctInformation EctInfo = new oscar.oscarDemographic.data.EctInformation(LoggedInInfo.getLoggedInInfoFromSession(request),demo);
						value = EctInfo.getReminders();
					}
					//if( !value.equals("") ) {
					if (pasteFmt == null || pasteFmt.equalsIgnoreCase("single"))
					{
						value = StringUtils.lineBreaks(value);
					}

					value = org.apache.commons.lang.StringEscapeUtils.escapeJavaScript(value);
					out.println("info = '" + value + "'");
					//}
				}%>
    } //end switch

    if( txtArea.value.length > 0 && info.length > 0 )
        txtArea.value += '\n';

    txtArea.value += info;
    txtArea.scrollTop = txtArea.scrollHeight;
    txtArea.focus();

}



function updateAttached() {
    var t = setTimeout('fetchAttached()', 2000);
}

function fetchAttached() {
    var updateElem = 'tdAttachedDocs';
    var params = "demo=<%=demo%>&requestId=<%=requestId%>";
    var url = "<rewrite:reWrite jspPage="displayAttachedFiles.jsp" />";

    var objAjax = new Ajax.Request (
                url,
                {
                    method: 'get',
                    parameters: params,
                    onSuccess: function(request) {
                                    $(updateElem).innerHTML = request.responseText;
                                },
                    onFailure: function(request) {
                                    $(updateElem).innerHTML = "<h3>Error: " + + request.status + "</h3>";
                                }
                }

            );

}

function addCCName(){
        if (document.EctConsultationFormRequestForm.ext_cc.value.length<=0)
                document.EctConsultationFormRequestForm.ext_cc.value=document.EctConsultationFormRequestForm.docName.value;
        else document.EctConsultationFormRequestForm.ext_cc.value+="; "+document.EctConsultationFormRequestForm.docName.value;
}

function popupMasterFile(vheight,vwidth,varpage) {
	var page = varpage;
	windowprops = "height="+vheight+",width="+vwidth+",location=no,scrollbars=yes,menubars=no,toolbars=no,resizable=yes,screenX=0,screenY=0,top=0,left=0";
	var popup=window.open(varpage, "<bean:message key="global.oscarRx"/>_demosearch", windowprops);
	if (popup != null) {
		if (popup.opener == null) {
			popup.opener = self;
		}
		popup.focus();
	}
}

</script>


<script>

var providerData = new Object(); //{};
<%
for (Provider p : prList) {
	if (!p.getProviderNo().equalsIgnoreCase("-1")) {
		String provNo = p.getProviderNo();
		String address;
		UserProperty addressProperty = userPropertyDAO.getProp(provNo,"rxAddress");
		if (addressProperty!=null && addressProperty.getValue() != null && addressProperty.getValue().length()>0) {
			address = (addressProperty.getValue()  + " " + userPropertyDAO.getProp(provNo, "rxCity").getValue()  + " " + userPropertyDAO.getProp(provNo, "rxProvince").getValue() + " " +userPropertyDAO.getProp(provNo, "rxPostal").getValue());
		} else { address = p.getFullAddress(); }
		String phone;
		UserProperty phoneProperty = userPropertyDAO.getProp(provNo,"rxPhone");
		if(phoneProperty!=null && phoneProperty.getValue() != null && phoneProperty.getValue().length()>0){
			phone = phoneProperty.getValue();
		} else { phone = p.getClinicPhone().trim(); }
		String faxNumber;
		UserProperty faxNumberProperty = userPropertyDAO.getProp(provNo,"faxnumber");
		if(faxNumberProperty!=null && faxNumberProperty.getValue() != null  && faxNumberProperty.getValue().length()>0){
			faxNumber = faxNumberProperty.getValue();
		} else { faxNumber = p.getClinicFax().trim(); }
		%>
	providerData['<%="prov_"+provNo%>'] = new Object(); //{};
	providerData['<%="prov_"+provNo%>'].address = "<%=Encode.forJavaScript(address)%>";
	providerData['<%="prov_"+provNo%>'].phone = "<%=Encode.forJavaScript(phone)%>";
	providerData['<%="prov_"+provNo%>'].fax = "<%=Encode.forJavaScript(faxNumber)%>";

<%	}
}

	if (clinic != null)
	{
	    String clinic_no = "clinic_" + clinic.getClinicNo();
%>
		providerData['<%=clinic_no%>'] = new Object();
		providerData['<%=clinic_no%>'].address = "<%=Encode.forJavaScript((clinic.getClinicAddress() + "   " + clinic.getClinicCity() + "   " + clinic.getClinicProvince() + "  " + clinic.getClinicPostal()).trim())%>" ;
		providerData['<%=clinic_no%>'].phone = "<%=Encode.forJavaScript(clinic.getClinicPhone().trim())%>";
		providerData['<%=clinic_no%>'].fax = "<%=Encode.forJavaScript(clinic.getClinicFax().trim())%>";
<%
	}

ProgramDao programDao = (ProgramDao) SpringUtils.getBean("programDao");
List<Program> programList = programDao.getAllActivePrograms();

if (OscarProperties.getInstance().getBooleanProperty("consultation_program_letterhead_enabled", "true")) {
	if (programList != null) {
		for (Program p : programList) {
			String progNo = "prog_" + p.getId();
%>
		providerData['<%=progNo %>'] = new Object();
		providerData['<%=progNo %>'].address = "<%=Encode.forJavaScript((p.getAddress() != null && p.getAddress().trim().length() > 0) ? p.getAddress().trim() : ((clinic.getClinicAddress() + "  " + clinic.getClinicCity() + "   " + clinic.getClinicProvince() + "  " + clinic.getClinicPostal()).trim()))%>";
		providerData['<%=progNo %>'].phone = "<%=Encode.forJavaScript((p.getPhone() != null && p.getPhone().trim().length() > 0) ? p.getPhone().trim() : clinic.getClinicPhone().trim())%>";
		providerData['<%=progNo %>'].fax = "<%=Encode.forJavaScript((p.getFax() != null && p.getFax().trim().length() > 0) ? p.getFax().trim() : clinic.getClinicFax().trim())%>";
<%
		}
	}
} %>
var siteData = new Object();
<%
if (bMultisites) {
    for (Site site : sites) { %>
		siteData['<%=StringEscapeUtils.escapeJavaScript(site.getName())%>'] = new Object();
		siteData['<%=StringEscapeUtils.escapeJavaScript(site.getName())%>'].address = "<%=Encode.forJavaScript(site.getAddress() + " " + site.getCity() + " " + site.getProvince() + " " + site.getPostal())%>";
		siteData['<%=StringEscapeUtils.escapeJavaScript(site.getName())%>'].phone = "<%=Encode.forJavaScript(site.getPhone())%>";
		siteData['<%=StringEscapeUtils.escapeJavaScript(site.getName())%>'].fax = "<%=Encode.forJavaScript(site.getFax())%>";
    <% }
}
%>


function switchProvider(value) {
	if (value==-1)
	{
		document.getElementById("letterheadName").value = value;
		document.getElementById("letterheadAddress").value = "<%=(clinic.getClinicAddress() + "  " + clinic.getClinicCity() + "   " + clinic.getClinicProvince() + "  " + clinic.getClinicPostal()).trim() %>";
        document.getElementById("letterheadAddressSpan").innerHTML = "<%=(clinic.getClinicAddress() + "  " + clinic.getClinicCity() + "   " + clinic.getClinicProvince() + "  " + clinic.getClinicPostal()).trim() %>";
        document.getElementById("letterheadPhone").value = "<%=clinic.getClinicPhone().trim() %>";
        document.getElementById("letterheadPhoneSpan").innerHTML = "<%=clinic.getClinicPhone().trim() %>";
        document.getElementById("letterheadFax").value = "<%=clinic.getClinicFax().trim() %>";
        document.getElementById("letterheadFaxSpan").innerHTML = "<%=clinic.getClinicFax().trim() %>";
    }
    else
	{
        if (typeof providerData["prov_" + value] !== "undefined" || typeof providerData[value] !== "undefined")
		{
            selectedProvider = value;
		}

		if (typeof providerData["prov_" + value] !== "undefined" )
		{
            value = "prov_" + value;
		}

        document.getElementById("letterheadName").value = selectedProvider;
        document.getElementById("letterheadAddress").value = providerData[value]['address'];
		document.getElementById("letterheadAddressSpan").innerHTML = providerData[value]['address'].replace(" ", "&nbsp;");
		document.getElementById("letterheadPhone").value = providerData[value]['phone'];
		document.getElementById("letterheadPhoneSpan").innerHTML = providerData[value]['phone'];
		document.getElementById("letterheadFax").value = providerData[value]['fax'].replace(/-/g, "");
		document.getElementById("letterheadFaxSpan").innerHTML = providerData[value]['fax'];
	}
	<%
		if (bMultisites) {
		UserProperty consultation_letterhead_address_multisite =
		userPropertyDAO.getProp(
			providerNo,
			UserProperty.CONSULTATION_LETTERHEAD_ADDRESS_MULTISITE);
		boolean useSitePreference = consultation_letterhead_address_multisite == null
			|| "site".equals(consultation_letterhead_address_multisite.getValue());
	%>
		var siteSelect = document.getElementsByName("siteName")[0];
		siteSelect.style.backgroundColor=siteSelect.options[siteSelect.selectedIndex].style.backgroundColor;
		var siteValue = siteSelect.value;
		<% if (useSitePreference) { %>
			if (siteValue != '') {
				document.getElementById("letterheadAddress").value = siteData[siteValue]['address'];
				document.getElementById("letterheadAddressSpan").innerHTML = siteData[siteValue]['address'].replace(" ", "&nbsp;");
				document.getElementById("letterheadPhone").value = siteData[siteValue]['phone'];
				document.getElementById("letterheadPhoneSpan").innerHTML = siteData[siteValue]['phone'];
				document.getElementById("letterheadFax").value = siteData[siteValue]['fax'].replace(/-/g, "");
				document.getElementById("letterheadFaxSpan").innerHTML = siteData[siteValue]['fax'];
			}
		<% } %>
	<% } %>
}
</script>
<script type="text/javascript">
<%
String signatureRequestId=DigitalSignatureUtils.generateSignatureRequestId(loggedInInfo.getLoggedInProviderNo());
String imageUrl=request.getContextPath()+"/imageRenderingServlet?source="+ImageRenderingServlet.Source.signature_preview.name()+"&"+DigitalSignatureUtils.SIGNATURE_REQUEST_ID_KEY+"="+signatureRequestId;
String storedImgUrl=request.getContextPath()+"/imageRenderingServlet?source="+ImageRenderingServlet.Source.signature_stored.name()+"&digitalSignatureId=";
%>
var POLL_TIME=1500;
var counter=0;
function refreshImage()
{
	counter=counter+1;
	document.getElementById('signatureImgTag').src='<%=imageUrl%>&rand='+counter;
	document.getElementById('signatureImg').value='<%=signatureRequestId%>';
	document.getElementById('newSignatureImg').value='<%=signatureRequestId%>';
}

function showSignatureImage()
{
	if (document.getElementById('signatureImg') != null && document.getElementById('signatureImg').value.length > 0) {
		document.getElementById('signatureImgTag').src = "<%=storedImgUrl %>" + document.getElementById('signatureImg').value;

		<% if (OscarProperties.getInstance().getBooleanProperty("topaz_enabled", "true")) {
		  //this is empty
		%>

		document.getElementById('clickToSign').style.display = "none";

		<% } else {
		  //this is empty
		%>

		document.getElementById("signatureFrame").style.display = "none";

		<% } %>


		document.getElementById('signatureShow').style.display = "block";
	}

	return true;
}

<%
String userAgent = request.getHeader("User-Agent");
String browserType = "";
if (userAgent != null) {
	if (userAgent.toLowerCase().indexOf("ipad") > -1) {
		browserType = "IPAD";
	} else {
		browserType = "ALL";
	}
}
%>

function requestSignature()
{


	<% if (OscarProperties.getInstance().getBooleanProperty("topaz_enabled", "true")) { %>
	document.getElementById('newSignature').value = "true";
	document.getElementById('signatureShow').style.display = "block";
	document.getElementById('clickToSign').style.display = "none";
	setInterval('refreshImage()', POLL_TIME);
	document.location='<%=request.getContextPath()%>/signature_pad/topaz_signature_pad.jnlp.jsp?<%=DigitalSignatureUtils.SIGNATURE_REQUEST_ID_KEY%>=<%=signatureRequestId%>';

	<% } %>
}

var isSignatureDirty = false;
var isSignatureSaved = <%= consultUtil.signatureImg != null && !"".equals(consultUtil.signatureImg) ? "true" : "false" %>;

function signatureHandler(e) {
	isSignatureDirty = e.isDirty;
	isSignatureSaved = e.isSave;
	<%
	if (consultationFaxEnabled) { //
	%>
	updateFaxButton();
	<% } %>
	if (e.isSave) {
		refreshImage();
		document.getElementById('newSignature').value = "true";
	}
	else if (e.isCleared) {
		document.getElementById('signatureImg').value = "";
		document.getElementById('newSignatureImg').value = "";
		document.getElementById('newSignature').value = "false";
	}
}

var requestIdKey = "<%=signatureRequestId %>";

function AddOtherFaxProvider() {
	var selected = $jq("#otherFaxSelect option:selected");
	_AddOtherFax(selected.text(),selected.val());
}
function AddOtherFax() {
	var number = $jq("#otherFaxInput").val();
	if (checkPhone(number)) {
		_AddOtherFax(number, number.replace(/(\D|-)+/g, ''));
	}
	else {
		alert("The fax number you entered is invalid.");
	}
}

function _AddOtherFax(name, number) {
	var remove = "<a href='javascript:void(0);' onclick='removeRecipient(this)'>remove</a>";
	var html = "<li>"+name+"<b>, Fax No: </b>"+number+ " " +remove+"<input type='hidden' name='faxRecipients' value='"+number+"'></input></li>";
	$jq("#faxRecipients").append($jq(html));
	updateFaxButton();
}

function checkPhone(str)
{
	var phone =  /(([0-9]-{0,1}){0,1}(\([0-9]{3}\)|[0-9]{3}-{0,1})[0-9]{3}-{0,1}[0-9]{4})/;
	var numbersOnly = str.replace(/(\D|-)+/g, '');
	if (str.match(phone) && numbersOnly.length >= 10 && numbersOnly.length <= 11)  {
			return true;
 	} else {
 		return false;
 	}
}

function removeRecipient(el) {
	var el = $jq(el);
	if (el) { el.parent().remove(); updateFaxButton(); }
	else { alert("Unable to remove recipient."); }
}

function hasFaxNumber() {
	return specialistFaxNumber.length > 0 || $jq("#faxRecipients").children().size() > 0;
}
function updateFaxButton() {
	var disabled = !hasFaxNumber();
	<% if (!lockForm) { %>
	if(document.getElementById("fax_button")!=null) {
		document.getElementById("fax_button").disabled = disabled;
	}
	if(document.getElementById("fax_button2")!=null) {
		document.getElementById("fax_button2").disabled = disabled;
	}
	<% } %>
}
</script>

<%=WebUtilsOld.popErrorMessagesAsAlert(session)%>
<link rel="stylesheet" type="text/css" href="../encounterStyles.css">
<body topmargin="0" leftmargin="0" vlink="#0000FF"
	onload="window.focus();disableDateFields();fetchAttached();disableEditing();showSignatureImage();">
<well-ai-voice:script/>
<html:errors />
<html:form action="/oscarEncounter/RequestConsultation"
	onsubmit="alert('HTHT'); return false;">
	<%
		EctConsultationFormRequestForm thisForm = (EctConsultationFormRequestForm)request.getAttribute("EctConsultationFormRequestForm");

		if (requestId != null)
		{
			EctViewRequestAction.fillFormValues(LoggedInInfo.getLoggedInInfoFromSession(request), thisForm, new Integer(requestId), archiveId);
                thisForm.setSiteName(consultUtil.siteName);
                defaultSiteName = consultUtil.siteName ;

		}
		else if (segmentId != null)
		{
			EctViewRequestAction.fillFormValues(thisForm, segmentId);
                thisForm.setSiteName(consultUtil.siteName);
                defaultSiteName = consultUtil.siteName ;
		}
		else if (request.getAttribute("validateError") == null)
		{
			//  new request
			if (demo != null)
			{
				oscar.oscarDemographic.data.RxInformation RxInfo = new oscar.oscarDemographic.data.RxInformation();
                EctViewRequestAction.fillFormValues(thisForm,consultUtil);
				boolean enableConsultationAutoIncludeAllergiesSystemPreference = SystemPreferencesUtils
						.isReadBooleanPreferenceWithDefault("enable_consultation_auto_incl_allergies", true);
				boolean enableConsultationAutoIncludeMedicationsSystemPreference = SystemPreferencesUtils
						.isReadBooleanPreferenceWithDefault("enable_consultation_auto_inc_medications", true);

				if (enableConsultationAutoIncludeAllergiesSystemPreference) {
					thisForm.setAllergies(RxInfo.getDescriptiveAllergies(loggedInInfo, Integer.parseInt(demo)));
                }
                if (enableConsultationAutoIncludeMedicationsSystemPreference) {
					if (props.getProperty("currentMedications", "").equalsIgnoreCase("otherMedications"))
					{
						oscar.oscarDemographic.data.EctInformation EctInfo = new oscar.oscarDemographic.data.EctInformation(LoggedInInfo.getLoggedInInfoFromSession(request),demo);
						thisForm.setCurrentMedications(EctInfo.getFamilyHistory());
					}
					else
					{
						thisForm.setCurrentMedications(RxInfo.getCurrentMedication(demo));
					}
				}
				String familyDoctorTeam = consultUtil.getProviderTeam(consultUtil.mrp);
				if(team == null || team.trim().equals("")){ team = familyDoctorTeam; }
			}

			thisForm.setStatus("1");

			thisForm.setSendTo(team);

       		if (bMultisites) {
        		thisForm.setSiteName(defaultSiteName);
       		}

			// Set Default Appointment Instructions
			thisForm.setAppointmentInstructions(defaultAppointmentInstructions);
		}

		if (thisForm.iseReferral() || lockForm)
		{
			%>
				<SCRIPT LANGUAGE="JavaScript">
					disableFields=true;
				</SCRIPT>
			<%
		}

		boolean showPrefName = false;
		String prefName = "";

		boolean enableConsultationAppointmentInstructionsLookupSystemPreference = SystemPreferencesUtils
				.isReadBooleanPreferenceWithDefault("enable_consultation_appt_instr_lookup", false);

		if (demographic != null) {
			Map<String, Boolean> generalSettingsMap = SystemPreferencesUtils.findByKeysAsMap(SystemPreferences.GENERAL_SETTINGS_KEYS);
			showPrefName = generalSettingsMap.getOrDefault("replace_demographic_name_with_preferred", false) && StringUtils.filled(demographic.getPreferredName());
			prefName = demographic.getPreferredName();
		}

		HashMap<String, Boolean> echartPreferencesMap = new HashMap<String, Boolean>();
		List<SystemPreferences> systemPreferences = SystemPreferencesUtils.findPreferencesByNames(SystemPreferences.ECHART_PREFERENCE_KEYS);
		for (SystemPreferences preference : systemPreferences) {
			echartPreferencesMap.put(preference.getName(), Boolean.parseBoolean(preference.getValue()));
		}
		String creatorName = !StringUtils.isNullOrEmpty(thisForm.getProviderName())
				? thisForm.getProviderName() : providerName;
	%>

	<% if (!consultationFaxEnabled || !enableConsultationDynamicLabellingSystemPreference) { %>
	<input type="hidden" id="providerNo" name="providerNo" value="<%=providerNo%>">
	<% } %>
	<input type="hidden" id="demographicNo" name="demographicNo" value="<%=demo%>">
	<input type="hidden" name="requestId" value="<%=requestId%>">
	<input type="hidden" id="documents" name="documents" value="">
	<input type="hidden" name="ext_appNo" value="<%=request.getParameter("appNo") %>">
	<input type="hidden" name="source" value="<%=(requestId!=null)?thisForm.getSource():request.getParameter("source") %>">
	<input type="hidden" id="contextPath" value="<%=request.getContextPath()%>">
	<input type="hidden" id="proContextPath" value="<%=OscarProperties.getKaiemrDeployedContext()%>">
	<input type="hidden" id="creatorName" name="creatorName" value="<%= creatorName %>">

        <input type="hidden" id="saved" value="false">
	<!--  -->
	<table class="MainTable" id="scrollNumber1" name="encounterTable">
		<tr class="MainTableTopRow">
			<td class="MainTableTopRowLeftColumn">Consultation</td>
			<td class="MainTableTopRowRightColumn">
			<table class="TopStatusBar">
				<tr>
					<td class="Header" style="padding-left: 2px; padding-right: 2px; border-right: 2px solid #003399; text-align: left; font-size: 80%; font-weight: bold; width: 100%;" NOWRAP>
						<a href="#" onclick="popupMasterFile(600,900,'<%=request.getContextPath()%>/demographic/demographiccontrol.jsp?demographic_no=<%=demo%>&displaymode=edit&dboperation=search_detail'); return false;" >
							<%=Encode.forHtmlContent(thisForm.getPatientName())%>
						</a>
						<%=thisForm.getPatientSex()%>	<%=thisForm.getPatientAge()%>
					</td>
						<% if ("ocean".equals(props.get("cme_js"))) { %>
					<td>
                        <span id="ocean" style="display:none"></span>
                        <% if (requestId == null) { %>
						<span id="oceanReferButton" class="oceanRefer"></span>
					</td>
						<% }
						}%>
				</tr>
			</table>
			</td>
		</tr>
		<tr style="vertical-align: top">
			<td class="MainTableLeftColumn">
			<table>
				<tr>
					<td class="tite4" colspan="2">
					<table>
						<tr>
							<td class="stat" colspan="2"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgCreated" />:</td>
						</tr>
						<tr>
							<td class="stat" colspan="2" align="right" nowrap>
								<%= Encode.forHtml(creatorName) %>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td class="tite4" colspan="2"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgStatus" />
					</td>
				</tr>
				<tr>
					<td class="tite4" colspan="2">
					<table>
						<tr>
							<td class="stat"><html:radio property="status" value="1" />
							</td>
							<td class="stat"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgNoth" />:
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td class="tite4" colspan="2">
					<table>
						<tr>
							<td class="stat"><html:radio property="status" value="2" />
							</td>
							<td class="stat"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgSpecCall" />
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td class="tite4" colspan="2">
					<table>
						<tr>
							<td class="stat"><html:radio property="status" value="3" />
							</td>
							<td class="stat"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgPatCall" />
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
				<td class="tite4" colspan="2">
					<table>
						<tr>
							<td class="stat"><html:radio property="status" value="5" />
							</td>
							<td class="stat"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgBookingConfirmed" /></td>
						</tr>
					</table>
				</td>
				</tr>
				<tr>
					<td class="tite4" colspan="2">
					<table>
						<tr>
							<td class="stat"><html:radio property="status" value="4" />
							</td>
							<td class="stat"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgCompleted" /></td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td class="tite4" colspan="2">
					<table>
						<tr>
							<td class="stat">&nbsp;</td>
						</tr>
						<% if (!attachmentManagerConsultationEnabled) { %>
						<% if (!lockForm) { %>
						<tr>
							<td style="text-align: center" class="stat">
							<%
								if (thisForm.iseReferral())
								{

									%>
								<a href="#" onclick="popupAttach(700,960,'<rewrite:reWrite jspPage="attachConsultation2.jsp"/>?provNo=<%=consultUtil.providerNo%>&demo=<%=demo%>&requestId=<%=requestId%>&editOnOcean=true','_blank');return false;">
									<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.attachDoc" />
								</a>
									<%
								}
								else
								{
									%>
									<% if (OscarProperties.getInstance().isPropertyActive("consultation_indivica_attachment_enabled")) { %>
									<a href="#" onclick="popupAttach(700,960,'<rewrite:reWrite jspPage="attachConsultation2.jsp"/>?provNo=<%=consultUtil.providerNo%>&demo=<%=demo%>&requestId=<%=requestId%>','_blank');return false;">
										<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.attachDoc" />
									</a>
									<% } else { %>
									<a href="#" onclick="popup('<rewrite:reWrite jspPage="attachConsultation.jsp"/>?provNo=<%=consultUtil.providerNo%>&demo=<%=demo%>&requestId=<%=requestId%>');return false;">
										<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.attachDoc" />
									</a>
									<% }
								}
							%>
							</td>
						</tr>
						<% } %>
						<tr>
							<td style="text-align: center"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.curAttachDoc"/>:</td>
						</tr>
						<tr>
							<td id="tdAttachedDocs"></td>
						</tr>
						<tr>
							<td style="text-align: center"><bean:message
								key="oscarEncounter.oscarConsultationRequest.AttachDoc.Legend" /><br />
							<span class="doc"><bean:message
								key="oscarEncounter.oscarConsultationRequest.AttachDoc.LegendDocs" /></span><br />
							<span class="lab"><bean:message
								key="oscarEncounter.oscarConsultationRequest.AttachDoc.LegendLabs" /></span><br />
							<span class="hrm"><bean:message
								key="oscarEncounter.oscarConsultationRequest.AttachDoc.LegendHRMs" /></span><br />
							<span class="eForm"><bean:message
									key="oscarEncounter.oscarConsultationRequest.AttachDoc.LegendEForms" /></span><br />
                            <span class="form"><bean:message
                                    key="oscarEncounter.oscarConsultationRequest.AttachDoc.LegendForms" /></span>
                            </td>
						</tr>
						<% } %>
					</table>
					</td>
				</tr>
			</table>
			</td>
			<td class="MainTableRightColumn">
			<table cellpadding="0" cellspacing="2"
				style="border-collapse: collapse" bordercolor="#111111" width="100%"
				height="100%" border=1>
				<% if (requestId != null && "ocean".equals(props.get("cme_js"))) {
					ConsultationRequestExtDao consultationRequestExtDao = SpringUtils.getBean(ConsultationRequestExtDao.class);
					Integer consultId = Integer.parseInt(requestId);
					String eReferralRef = consultationRequestExtDao.getConsultationRequestExtsByKey(consultId, "ereferral_ref");
					if(eReferralRef != null) {
				%>
				<input id="ereferral_ref" type="hidden" value="<%= Encode.forHtmlAttribute(eReferralRef) %>"/>
				<span id="editOnOcean" class="oceanRefer"></span>
				<%	}
				   } %>
				<!----Start new rows here-->
				<% if ((consultUtil.locked && OscarProperties.getInstance().isPropertyActive("consultation_lock_on_print")) && request.getAttribute("id") != null) { %>
				<tr>
					<td class="tite4" colspan=2>
						<input name="updateLocked" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnUpdate"/>"
							   onclick="document.forms[0].service.disabled = false; document.forms['EctConsultationFormRequestForm'].submit();"/>
					</td>
				</tr>
				<% } else if (!lockForm && thisForm.geteReferralId() == null) { %>
				<tr>
					<td class="tite4" colspan=2>
					<% boolean faxEnabled = props.getBooleanProperty("faxEnable", "yes"); %>
					<security:oscarSec roleName="<%=roleName$%>" objectName="_con" rights="w" reverse="false">
						<% if (request.getAttribute("id") != null) { %>

						<input name="update" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnUpdate"/>" onclick="return checkForm('Update Consultation Request','EctConsultationFormRequestForm');" />
						<input name="updateAndPrint" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnUpdateAndPrint"/>" onclick="return checkForm('Update Consultation Request And Print Preview','EctConsultationFormRequestForm');" />
						<input name="printPreview" type="button" value="Print Preview" onclick="return checkForm('And Print Preview','EctConsultationFormRequestForm');" />

						<logic:equal value="true" name="EctConsultationFormRequestForm" property="eReferral">
							<input name="updateAndSendElectronicallyTop" type="button"
								value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnUpdateAndSendElectronicReferral"/>"
								onclick="return checkForm('Update_esend','EctConsultationFormRequestForm');" />
						</logic:equal>

						<oscar:oscarPropertiesCheck value="yes" property="faxEnable">
							<% if (echartPreferencesMap.getOrDefault("echart_paste_fax_note", false)) { %>
							<input id="fax_button" name="updateAndFax" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnUpdateAndFax"/>" onclick="paste2Note();return checkForm('Update And Fax','EctConsultationFormRequestForm');" />
							<%} else {%>
							<input id="fax_button" name="updateAndFax" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnUpdateAndFax"/>" onclick="return checkForm('Update And Fax','EctConsultationFormRequestForm');" />
							<%} %>
							<input id="update_print_fax_button" name="updatePrintAndFax" type="button" value="Update, Print, & Fax" onclick="return checkForm('Update Print And Fax','EctConsultationFormRequestForm');" />
						</oscar:oscarPropertiesCheck>

						<% } else { %>

						<input name="submitSaveOnly" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnSubmit"/>" onclick="return checkForm('Submit Consultation Request','EctConsultationFormRequestForm'); " />
						<input name="submitAndPrint" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnSubmitAndPrint"/>" onclick="return checkForm('Submit Consultation Request And Print Preview','EctConsultationFormRequestForm'); " />

						<logic:equal value="true" name="EctConsultationFormRequestForm" property="eReferral">
							<input name="submitAndSendElectronicallyTop" type="button"
							value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnSubmitAndSendElectronicReferral"/>"
							onclick="return checkForm('Submit_esend','EctConsultationFormRequestForm');" />
						</logic:equal>
							<% if (echartPreferencesMap.getOrDefault("echart_paste_fax_note", false)) { %>
						<oscar:oscarPropertiesCheck value="yes" property="faxEnable">
							<input id="fax_button" name="submitAndFax" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnSubmitAndFax"/>" onclick="paste2Note();return checkForm('Submit And Fax','EctConsultationFormRequestForm');" />
							<input id="submit_print_fax_button2" name="submitPrintAndFax" type="button" value="Submit, Print, & Fax" onclick="paste2Note();return checkForm('Submit Print And Fax','EctConsultationFormRequestForm');" />
						</oscar:oscarPropertiesCheck>
							<%} else { %>
						<oscar:oscarPropertiesCheck value="yes" property="faxEnable">
							<input id="fax_button" name="submitAndFax" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnSubmitAndFax"/>" onclick="return checkForm('Submit And Fax','EctConsultationFormRequestForm');" />
							<input id="submit_print_fax_button2" name="submitPrintAndFax" type="button" value="Submit, Print, & Fax" onclick="return checkForm('Submit Print And Fax','EctConsultationFormRequestForm');" />
						</oscar:oscarPropertiesCheck>
							<%} %>
						<% }%>

						<logic:equal value="true" name="EctConsultationFormRequestForm" property="eReferral">
							<input type="button" value="Send eResponse" onclick="$('saved').value='true';document.location='<%=thisForm.getOruR01UrlString(request)%>'" />
						</logic:equal>
					</security:oscarSec>

					</td>
                    </tr>
					<% } %>
                    <tr>
					<td>
						<% // Determine if curUser has selected a default practitioner in preferences
							UserProperty defaultReferralPracProperty = userPropertyDAO.getProp(providerNo,  UserProperty.DEFAULT_REF_PRACTITIONER);
							String defaultReferralPractitioner = "";
							if (defaultReferralPracProperty != null && defaultReferralPracProperty.getValue() != null) {
								defaultReferralPractitioner = defaultReferralPracProperty.getValue();
							}
						%>
					<table border=0 width="100%">
						<% if (consultationFaxEnabled && enableConsultationDynamicLabellingSystemPreference) { %>
						<tr>
							<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.consultationFormPrint.msgAssociated2" />:</td>
							<td align="right" class="tite1">
								<html:select property="providerNo" onchange="switchProvider(this.value)">
									<%
										DemographicDao demographicDao=(DemographicDao)SpringUtils.getBean("demographicDao");
										demographic = demographicDao.getDemographic(demo);

										if (consultUtil.providerNo==null) {consultUtil.providerNo = "";}
										org.oscarehr.common.model.Provider currentConsultationProvider = providerDao.getProvider(consultUtil.providerNo);

										String practitionerNo = "";
										boolean exisitingConsultInactiveOrNoCPSO = false;
										if (currentConsultationProvider != null && isNotBlank(currentConsultationProvider.getPractitionerNo())) {
											practitionerNo = currentConsultationProvider.getPractitionerNo();
										}
										if (practitionerNo==null) {practitionerNo="";}

										if (practitionerNo.isEmpty() || practitionerNo.equals("-1"))
										{
										    if (defaultReferralPractitioner.equalsIgnoreCase("all") || defaultReferralPractitioner.equalsIgnoreCase(""))
											{
												String loggedInPractitionerNo = loggedInInfo.getLoggedInProvider().getPractitionerNo();

												if (loggedInPractitionerNo != null && !loggedInPractitionerNo.equals(""))
												{
													practitionerNo = loggedInPractitionerNo;
												}
												else if (demographic.getProvider() != null && demographic.getProvider().getPractitionerNo() != null && !demographic.getProvider().getPractitionerNo().equals(""))
												{
													practitionerNo = demographic.getProvider().getPractitionerNo();
												}
											}
											else
											{
											    practitionerNo = defaultReferralPractitioner;
											}
										}
										List<org.oscarehr.common.model.Provider> referralPractitionerList;
										if (OscarProperties.getInstance().isBritishColumbiaBillingRegion()) {
											referralPractitionerList = providerDao.getReferringPractitioner(false);
										}
										else {
											referralPractitionerList = providerDao.getReferringPractitioner(true);
										}

										if (consultUtil.providerNo != null && currentConsultationProvider != null
												&& (((OscarProperties.getInstance().isOntarioBillingRegion()) && isBlank(currentConsultationProvider.getPractitionerNo()))
                                                                    || !"1".equalsIgnoreCase(currentConsultationProvider.getStatus()))) {
                                            if (practitionerNotInList(referralPractitionerList, currentConsultationProvider.getProviderNo())) {
                                                referralPractitionerList.add(currentConsultationProvider);
                                            }
											exisitingConsultInactiveOrNoCPSO = (!"1".equalsIgnoreCase(currentConsultationProvider.getStatus()))
                                                    || (!OscarProperties.getInstance().isBritishColumbiaBillingRegion() && isBlank(currentConsultationProvider.getPractitionerNo()));
										}

										for (org.oscarehr.common.model.Provider p : referralPractitionerList) {
											if (p.getProviderNo().compareTo("-1") != 0) {
												if (currentConsultationProvider != null) {
									%>
												<option value="<%= p.getProviderNo() %>" <%= p.getProviderNo().equals((currentConsultationProvider.getProviderNo())) ? "selected='selected'" : "" %> >
													<%= Encode.forHtmlContent(p.getFormattedName()) %>
												</option>
												<% } else {%>
													<option value="<%= p.getProviderNo() %>" <%= !exisitingConsultInactiveOrNoCPSO ? (Objects.equals(p.getPractitionerNo(), practitionerNo)
															? "selected='selected'" : "") : (p.getProviderNo().equals((currentConsultationProvider.getProviderNo())) ? "selected='selected'" : "") %> >
														<%= Encode.forHtmlContent(p.getFormattedName()) %>
													</option>
												<%
												}
											}
										}
									%>
								</html:select>
							</td>
						</tr>
						<% } %>
						<tr>						
							<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formRefDate" />:
							</td>	
							<td align="right" class="tite1"><img alt="calendar" id="referalDate_cal" src="../../images/cal.gif">
								<% 	if (request.getAttribute("id") != null)
								{ %>
									<html:text styleId="referalDate" styleClass="righty" readonly="<%=isConsultationDateReadOnly%>" property="referalDate" ondblclick="this.value='';"/>
							<% 	}
					 			else
					 			{ %>
									<html:text styleId="referalDate" styleClass="righty" readonly="<%=isConsultationDateReadOnly%>" property="referalDate" ondblclick="this.value='';" value="<%=formattedDate%>" />
							<% 	} %>
							</td>
						</tr>
						<tr>
							<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formService" />:
							</td>
							<td align="right" class="tite1">
								<% if (thisForm.iseReferral() && !thisForm.geteReferralService().isEmpty()) { %>
									<%= thisForm.geteReferralService() %>
								<% } else { %>
									<html:select styleId="service" property="service" onchange="fillSpecialistSelect(this);">
									<!-- <option value="-1">------ <bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formServSelect"/> ------</option>
						<option/>
							<option/>
							<option/>
							<option/> -->
									</html:select>
								<% } %>
							</td>
						</tr>
						<tr>
							<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formCons" />:
							</td>
							<td align="right" class="tite2">
 							<%
								if (thisForm.iseReferral())
								{
									%>
										<%=thisForm.getProfessionalSpecialistName()%>
									<%
								}
								else
								{
									%>
									
									<span id="consult-disclaimer" title="When consult was saved this was the saved consultant but is no longer on this specialist list." style="display:none;font-size:24px;">*</span> <html:select styleId="specialist" property="specialist" size="1" onchange="onSelectSpecialist(this)">
									
									</html:select>
									
									
									<%
								}
							%>
							</td>
						</tr>
                                                <tr>
                                                    <td class="tite4">
                                                        <bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formInstructions" />
                                                    </td>
                                                    <td align="right" class="tite2">
                                                        <textarea id="annotation" style="color: blue;" readonly></textarea>
                                                    </td>
                                                </tr>
						<tr>
							<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formUrgency" /></td>
							<td align="right" class="tite2">
								<html:select property="urgency">
									<html:option value="2">
										<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgNUrgent" />
									</html:option>
									<html:option value="1">
										<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgUrgent" />
									</html:option>
									<html:option value="3">
										<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgReturn" />
									</html:option>
									<html:option value="4">
										<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgPriority" />
									</html:option>
								</html:select>
							</td>
						</tr>
						<tr>
							<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formPhone" />:
							</td>
							<td align="right" class="tite2"><input type="text" name="phone" class="righty" value="<%=thisForm.getProfessionalSpecialistPhone()%>" /></td>
						</tr>
						<tr>
							<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formFax" />:
							</td>
							<td align="right" class="tite3"><input type="text" id="fax" name="fax" class="righty" /></td>
						</tr>

						<tr>
							<td class="tite4">
								<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formAddr" />:
							</td>
							<td align="right" class="tite3">
								<textarea name="address" cols=20 ><%=thisForm.getProfessionalSpecialistAddress()%></textarea>
							</td>
						</tr>

						<% if (enableConsultationAppointmentInstructionsLookupSystemPreference) { %>
							<tr>
								<td class="tite4">
									<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.appointmentInstr" />
								</td>
								<td align="right" class="tite3">														
									<html:select property="appointmentInstructions" styleId="appointmentInstructions" >
										<html:option value="" ></html:option>
										<c:forEach items="${ appointmentInstructionList.items }" var="appointmentInstruction">
											<%-- Ensure that only active items are shown --%>
											<c:if test="${ appointmentInstruction.active }" >
												<html:option value="${ appointmentInstruction.value }" >
													<c:out value="${ appointmentInstruction.label }" />
												</html:option>
											</c:if>
										</c:forEach>								
									</html:select>
								</td>
							</tr>
						<% } %>

						<% if (enableConsultationPatientWillBookSystemPreference) { %>
							<tr>
								<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formPatientBook" />:</td>
								<td align="right" class="tite3"><html:checkbox property="patientWillBook" value="1" onclick="disableDateFields()">
								</html:checkbox></td>
							</tr>
						<% } %>
						<tr>						
							<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnAppointmentDate" />:
							</td>
                            <td align="right" class="tite3"><img alt="calendar" id="appointmentDate_cal" src="../../images/cal.gif"> 
 							<html:text styleId="appointmentDate" property="appointmentDate" readonly="true" onclick="this.select()" ondblclick="clearField(this);" onkeydown="clearOnBackspaceDelete(event)" />
							</td>
						</tr>
						<tr>
							<td class="tite4"><bean:message	key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formAppointmentTime" />:
							</td>
							<td align="right" class="tite3">
							<table>
								<tr>
									<td><html:select property="appointmentHour">
										<html:option value=""></html:option>
										<%
											for (int i = 1; i < 13; i = i + 1)
														{
															String hourOfday = Integer.toString(i);
										%>
										<html:option value="<%=hourOfday%>"><%=hourOfday%></html:option>
										<%
											}
										%>
									</html:select></td>
									<td><html:select property="appointmentMinute">
										<html:option value=""></html:option>
										<%
											for (int i = 0; i < 60; i = i + 1)
														{
															String minuteOfhour = Integer.toString(i);
															if (i < 10)
															{
																minuteOfhour = "0" + minuteOfhour;
															}
										%>
										<html:option value="<%=String.valueOf(i)%>"><%=minuteOfhour%></html:option>
										<%
											}
										%>
									</html:select></td>
									<td><html:select property="appointmentPm">
										<html:option value="AM">AM</html:option>
										<html:option value="PM">PM</html:option>
									</html:select></td>
								</tr>
							</table>
							</td>
						</tr>
						<%if (bMultisites) { %>
						<tr>
							<td  class="tite4">
								<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.siteName" />:
							</td>
							<td class="tite3">
								<html:select property="siteName" value='<%=defaultSiteName%>' onchange="switchProvider(document.getElementById('letterheadName').value)">
									<html:option value="" style="background-color: #FFFFFF">Use provider default</html:option>
									<% for (Site site: sites) {
										String backgroundStyle = "background-color: " + site.getBgColor();
									%>
										<html:option value="<%=site.getName()%>" style="<%=backgroundStyle%>"> <%=site.getName()%> </html:option>
									<% } %>
								</html:select>
							</td>
						</tr>
						<%} %>
					</table>
					</td>
					<td valign="top" cellspacing="1" class="tite4">
					<table border=0 width="100%" bgcolor="white">
						<tr>
							<td class="tite4"><bean:message
								key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgPatient" />:
							</td>
                                                        <td class="tite1"><a href="javascript:void();" onClick="popupAttach(600,900,'<%=request.getContextPath()%>/demographic/demographiccontrol.jsp?demographic_no=<%=demo%>&displaymode=edit&dboperation=search_detail')"><%=Encode.forHtml(thisForm.getPatientName())%></a></td>
						</tr>
						<% if (showPrefName) { %>
						<tr>
							<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgPreferredName" />:
							</td>
							<td class="tite1"><%=Encode.forHtml(prefName)%></td>
						</tr>
						<% } %>
						<tr>
							<td class="tite4"><bean:message
								key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgAddress" />:
							</td>
							<td class="tite1"><%=thisForm.getPatientAddress().replace("null", "")%></td>
						</tr>
						<tr>
							<td class="tite4">
								<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgPhone" />:
							</td>
							<td class="tite2"><%=thisForm.getPatientPhone()%></td>
						</tr>
						<tr>
							<td class="tite4">
								<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgWPhone" />:
							</td>
							<td class="tite2"><%=thisForm.getPatientWPhone()%></td>
						</tr>
						<%
							DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
							Map<String,String> demoExt = demographicExtDao.getAllValuesForDemo(Integer.parseInt(demo));
						%>
						<tr>
							<td class="tite4">
								<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgCPhone" />:
							</td>
							<td class="tite2"><%=StringUtils.noNull(demoExt.get("demo_cell"))%></td>
						</tr>
						<tr>
							<td class="tite4"><bean:message
								key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgEmail" />:
							</td>
							<td class="tite2"><%=thisForm.getPatientEmail()%></td>
						</tr>
						<tr>
							<td class="tite4"><bean:message
								key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgBirthDate" />:
							</td>
							<td class="tite2"><%=thisForm.getPatientDOB()%></td>
						</tr>
						<tr>
							<td class="tite4"><bean:message
								key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgSex" />:
							</td>
							<td class="tite3"><%=thisForm.getPatientSex()%></td>
						</tr>
						<tr>
							<td class="tite4"><bean:message
								key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgHealthCard" />:
							</td>
							<td class="tite3"><%=thisForm.getPatientHealthNum()%>&nbsp;<%=thisForm.getPatientHealthCardVersionCode()%>&nbsp;<%=thisForm.getPatientHealthCardType()%>
							</td>
						</tr>
						<tr id="conReqSendTo">
							<td class="tite4"><bean:message
								key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgSendTo" />:
							</td>
							<td class="tite3"><html:select property="sendTo" styleId="sendTo">
								<html:option value="-1">---- <bean:message
										key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.msgTeams" /> ----</html:option>
								<%
									for (int i = 0; i < consultUtil.teamVec.size(); i++)
										{
											String te = (String)consultUtil.teamVec.elementAt(i);
								%>
								<html:option value="<%=te%>"><%=te%></html:option>
								<%
									}
								%>
							</html:select></td>
						</tr>

<!--add for special encounter-->
<plugin:hideWhenCompExists componentName="specialencounterComp" reverse="true">
<special:SpecialEncounterTag moduleName="eyeform">

<%
	String aburl1 = "/EyeForm.do?method=addCC&demographicNo=" + demo;
					if (requestId != null) aburl1 += "&requestId=" + requestId;
%>
<plugin:include componentName="specialencounterComp" absoluteUrl="<%=aburl1 %>"></plugin:include>
</special:SpecialEncounterTag>
</plugin:hideWhenCompExists>
<!-- end -->

						<tr>
							<td colspan="2" class="tite4"><bean:message
								key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formAppointmentNotes" />:
							</td>
						</tr>
						<tr>
							<td colspan="2" class="tite3"><html:textarea cols="50"
								rows="3" property="appointmentNotes"></html:textarea></td>
						</tr>
                       
						
						<tr>
							<td class="tite4"><bean:message
								key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formLastFollowup" />:
							</td>
							<td colspan="2" class="tite3"><img alt="calendar" id="followUpDate_cal" src="../../images/cal.gif">&nbsp;<html:text styleId="followUpDate" property="followUpDate" readonly="true" onclick="this.select()" ondblclick="clearField(this);" onkeydown="clearOnBackspaceDelete(event)" />
						</tr>
						
						<%
							if(thisForm.getFdid() != null) {
						%>
						<tr>
							<td class="tite4">EForm:
							</td>
							<td class="tite2">
								<a href="<%=request.getContextPath()%>/eform/efmshowform_data.jsp?fdid=<%=thisForm.getFdid() %>">Click to view</a>
							</td>
						</tr>
						<%
							}
						%>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan=2>
					<table border=0 width="100%">
						<tr>
							<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.letterheadName" />:
							</td>							
							<td align="right" class="tite3">				
								<select name="letterheadName" id="letterheadName" onchange="switchProvider(this.value)">
									<option value="clinic_<%=StringEscapeUtils.escapeHtml(clinic.getClinicNo())%>" <%=(consultUtil.letterheadName != null && consultUtil.letterheadName.equalsIgnoreCase(clinic.getClinicName()) ? "selected='selected'" : lhndType.equals("clinic") ? "selected='selected'" : "" )%>><%=clinic.getClinicName() %></option>
								<%
									for (Provider p : prList) {
										if (p.getProviderNo().compareTo("-1") != 0 && (p.getFirstName() != null || p.getSurname() != null)) {
								%>
								<option value="<%=p.getProviderNo() %>" 
								<%=(p.getProviderNo().equalsIgnoreCase(providerDefault)&&!lhndType.equals("clinic")?"selected='selected'":"") %>>
									<%=Encode.forHtmlContent(p.getFormattedName()) %>
								</option>
								<% }
								}

								if (OscarProperties.getInstance().getBooleanProperty("consultation_program_letterhead_enabled", "true")) {
								for (Program p : programList) {
								%>
									<option value="prog_<%=p.getId() %>" <%=(consultUtil.letterheadName != null && consultUtil.letterheadName.equalsIgnoreCase("prog_" + p.getId()) ? "selected='selected'"  : "") %>>
									<%=p.getName() %>
									</option>
								<% }
								}%>
								</select>
								<% if (consultationFaxEnabled) { %>
									<div style="font-size:12px"><input type="checkbox" name="ext_letterheadTitle" value="Dr" <%=(consultUtil.letterheadTitle != null && consultUtil.letterheadTitle.equals("Dr") ? "checked"  : "") %>>Include Dr. with name</div>
								<% } %>
							</td>
						</tr>
						<tr>
							<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.letterheadAddress" />:
							</td>
							<td align="right" class="tite3">
								<% if (consultUtil.letterheadAddress != null) { %>
									<input type="hidden" name="letterheadAddress" id="letterheadAddress" value="<%=StringEscapeUtils.escapeHtml(consultUtil.letterheadAddress) %>" />
									<span id="letterheadAddressSpan">
										<%=consultUtil.letterheadAddress %>
									</span>
								<% } else { %>
									<input type="hidden" name="letterheadAddress" id="letterheadAddress" value="<%=StringEscapeUtils.escapeHtml(clinic.getClinicAddress()) %>  <%=StringEscapeUtils.escapeHtml(clinic.getClinicCity()) %>  <%=StringEscapeUtils.escapeHtml(clinic.getClinicProvince()) %>  <%=StringEscapeUtils.escapeHtml(clinic.getClinicPostal()) %>" />
									<span id="letterheadAddressSpan">
										<%=clinic.getClinicAddress() %>&nbsp;&nbsp;<%=clinic.getClinicCity() %>&nbsp;&nbsp;<%=clinic.getClinicProvince() %>&nbsp;&nbsp;<%=clinic.getClinicPostal() %>
									</span>
								<% } %>
							</td>
						</tr>
						<tr>
							<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.letterheadPhone" />:
							</td>
							<td align="right" class="tite3">
								<% if (consultUtil.letterheadPhone != null) {
								%>
									<input type="hidden" name="letterheadPhone" id="letterheadPhone" value="<%=StringEscapeUtils.escapeHtml(consultUtil.letterheadPhone) %>" />
								 	<span id="letterheadPhoneSpan">
										<%=consultUtil.letterheadPhone%>
									</span>
								<% } else { %>
									<input type="hidden" name="letterheadPhone" id="letterheadPhone" value="<%=StringEscapeUtils.escapeHtml(clinic.getClinicPhone()) %>" />
									<span id="letterheadPhoneSpan">
										<%=clinic.getClinicPhone()%>
									</span>
								<% } %>
							</td>
						</tr>
						<tr>
							<td class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.letterheadFax" />:
							</td>
							<td align="right" class="tite3">
								<%								
									FaxConfigDao faxConfigDao = SpringUtils.getBean(FaxConfigDao.class);
									List<FaxConfig> faxConfigs = faxConfigDao.findAll(null, null);
									String letterheadFax = consultUtil.letterheadFax != null ? consultUtil.letterheadFax : "";
								%>
									<span id="letterheadFaxSpan"><%=letterheadFax%></span>
								<%
									if (letterheadFax.equals(""))
									{
								%>		
										<select name="letterheadFax" id="letterheadFax" onchange="document.getElementById('letterheadFaxSpan').innerHTML = this.value">
								<%
									
									for( FaxConfig faxConfig : faxConfigs ) {
								%>
										<option value="<%=faxConfig.getFaxNumber().replace("-", "")%>" <%=faxConfig.getFaxNumber().replace("-", "").equals(letterheadFax.replace("-", "")) ? "selected" : ""%>><%=faxConfig.getFaxUser()%></option>
								<%	    
									}								
								%>
									</select>
								<%
									}
									else
									{
								%>
									<input type="hidden" name="letterheadFax" id="letterheadFax" value="<%=letterheadFax.replaceAll("-", "")%>" />
								<%
									}		
								%>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan=2>
					<td>
				</tr>
				<tr>
					<td colspan="2" class="tite4"><bean:message
						key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formReason" />
					</td>
				</tr>
				<tr>
					<td colspan=2><html:textarea property="reasonForConsultation"
						cols="90" rows="3"></html:textarea></td>
				</tr>
				<tr>
					<td colspan=2 class="tite4">
					<table width="100%">
						<tr>
							<td width="30%" rowspan="2" class="tite4">
								<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formClinInf" />:
							</td>
							<td id="clinicalInfoButtonBar">
								<% if (!lockForm && thisForm.geteReferralId() == null) { %>
								<input type="button" class="btn" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnImportSocHistory"/>" onclick="importFromEnct('SocialHistory',document.forms[0].clinicalInformation);" />&nbsp;
								<input type="button" class="btn" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnImportFamHistory"/>" onclick="importFromEnct('FamilyHistory',document.forms[0].clinicalInformation);" />&nbsp;
								<input type="button" class="btn" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnImportMedHistory"/>" onclick="importFromEnct('MedicalHistory',document.forms[0].clinicalInformation);" />&nbsp;
								<input id="btnOngoingConcerns" type="button" class="btn" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnImportConcerns"/>" onclick="importFromEnct('ongoingConcerns',document.forms[0].clinicalInformation);" />&nbsp;
								<input type="button" class="btn" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnImportOtherMeds"/>" onclick="importFromEnct('OtherMeds',document.forms[0].clinicalInformation);" />&nbsp;

								<span id="clinicalInfoButtons"></span>
								<% } %>
							</td>
						</tr>
						<tr>
							<td>
								<% if (!lockForm && thisForm.geteReferralId() == null) { %>
								<input id="btnReminders" type="button" class="btn" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnImportReminders"/>" onclick="importFromEnct('Reminders',document.forms[0].clinicalInformation);" />&nbsp;								
								<input id="fetchRiskFactors_clinicalInformation" type="button" class="btn clinicalData" value="Risk Factors" />&nbsp;
								<input id="fetchMedications_clinicalInformation" type="button" class="btn clinicalData" value="Medications" />&nbsp;
								<input id="fetchLongTermMedications_clinicalInformation" type="button" class="btn clinicalData" value="Long Term Medications" />&nbsp;
								<% } %>
							</td>
						</tr>
					</table>
				</tr>
				<tr>
					<td colspan=2>
					<html:textarea cols="90" rows="10" styleId="clinicalInformation" property="clinicalInformation"></html:textarea></td>
				</tr>
				<tr>
					<td colspan=2 class="tite4">
					<table width="100%">
						<tr>
							<td width="30%" rowspan="2" class="tite4">
							<%
								if (props.getProperty("significantConcurrentProblemsTitle", "").length() > 1)
										{
											out.print(props.getProperty("significantConcurrentProblemsTitle", ""));
										}
										else
										{
							%> <bean:message
								key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formSignificantProblems" />:
							<%
 	}
 %>
							</td>
							<td id="concurrentProblemsButtonBar">
								<% if (!lockForm && thisForm.geteReferralId() == null) { %>
								<input type="button" class="btn" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnImportSocHistory"/>" onclick="importFromEnct('SocialHistory',document.forms[0].concurrentProblems);" />&nbsp;
								<input type="button" class="btn" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnImportFamHistory"/>" onclick="importFromEnct('FamilyHistory',document.forms[0].concurrentProblems);" />&nbsp;
								<input type="button" class="btn" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnImportMedHistory"/>" onclick="importFromEnct('MedicalHistory',document.forms[0].concurrentProblems);" />&nbsp;
								<input id="btnOngoingConcerns2" type="button" class="btn" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnImportConcerns"/>" onclick="importFromEnct('ongoingConcerns',document.forms[0].concurrentProblems);" />&nbsp;
								<input type="button" class="btn" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnImportOtherMeds"/>" onclick="importFromEnct('OtherMeds',document.forms[0].concurrentProblems);" />&nbsp;
								<% } %>
							</td>
						</tr>
						<tr>
							
							<td>
								<% if (!lockForm && thisForm.geteReferralId() == null) { %>
								<input id="btnReminders2" type="button" class="btn" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnImportReminders"/>" onclick="importFromEnct('Reminders',document.forms[0].concurrentProblems);" />&nbsp;
								<input id="fetchRiskFactors_concurrentProblems" type="button" class="btn clinicalData" value="Risk Factors" />&nbsp;
								<input id="fetchMedications_concurrentProblems" type="button" class="btn clinicalData" value="Medications" />&nbsp;
								<input id="fetchLongTermMedications_concurrentProblems" type="button" class="btn clinicalData" value="Long Term Medications" />&nbsp;
								<% } %>
							</td>
						</tr>
					</table>

					</td>
				</tr>
				<tr id="trConcurrentProblems">
					<td colspan=2>
					
					<html:textarea cols="90" rows="3" styleId="concurrentProblems" property="concurrentProblems">

					</html:textarea></td>
				</tr>
 <!--add for special encounter-->
<plugin:hideWhenCompExists componentName="specialencounterComp" reverse="true">
<special:SpecialEncounterTag moduleName="eyeform">
<%
	String aburl2 = "/EyeForm.do?method=specialConRequest&demographicNo=" + demo + "&appNo=" + request.getParameter("appNo");
					if (requestId != null) aburl2 += "&requestId=" + requestId;
if (defaultSiteId!=0) aburl2+="&site="+defaultSiteId;
%>
<html:hidden property="specialencounterFlag" value="true"/>
<plugin:include componentName="specialencounterComp" absoluteUrl="<%=aburl2 %>"></plugin:include>
</special:SpecialEncounterTag>
</plugin:hideWhenCompExists>
				<tr>
					<td colspan="2" class="tite4">
					<table width="100%">
						<tr>
							<td width="30%" class="tite4">
								<% if (props.getProperty("currentMedicationsTitle", "").length() > 1) {
									out.print( props.getProperty("currentMedicationsTitle", "") );
								}else { %> 										
									<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formCurrMedications" />:
								<% }  %>
							</td>
							<td id="medsButtonBar">
								<% if (!lockForm && thisForm.geteReferralId() == null) { %>
								<input type="button" class="btn" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnImportOtherMeds"/>" 
								onclick="importFromEnct('OtherMeds',document.forms[0].currentMedications);" />
								
								<input id="fetchMedications_currentMedications" type="button" class="btn clinicalData" value="Medications" />&nbsp;
								<input id="fetchLongTermMedications_currentMedications" type="button" class="btn clinicalData" value="Long Term Medications" />&nbsp;
								
								<span id="medsButtons"></span>
								<% } %>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td colspan=2>
						<html:textarea cols="90" rows="3" styleId="currentMedications" property="currentMedications"></html:textarea>
					</td>
				</tr>
				<tr>
					<td colspan=2  class="tite4" >
						<table width="100%">
						<tr>
							<td width="30%" class="tite4">
							<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formAllergies" />:
							</td>
							<td>
								<% if (!lockForm && thisForm.geteReferralId() == null) { %>
								<input id="fetchAllergies_allergies" type="button" class="btn clinicalData" value="Allergies" />
								<% } %>
							</td>
						</tr>
						
						</table>
					</td>
					</tr>
				<tr>
					<td colspan=2>
						<html:textarea cols="90" rows="3" styleId="allergies" property="allergies"></html:textarea></td>
				</tr>

                <%
                    if ("ocean".equals(props.get("cme_js"))) {
                        ConsultationRequestExtDao consultationRequestExtDao = SpringUtils
                                .getBean(ConsultationRequestExtDao.class);
                        Integer consultId = null;
                        String notes = "";
                        if (requestId != null) {
                            consultId = Integer.parseInt(requestId);
                            notes = org.apache.commons.lang.StringUtils.trimToEmpty(
                                    consultationRequestExtDao
                                            .getConsultationRequestExtsByKey(consultId, "notes"));
                        }

                %>
				<tr>
					<td colspan=2 class="tite4">Notes:</td>
				</tr>
				<tr>
					<td colspan=2>
						<textarea maxlength="65535" cols="90" rows="3" id="ext_notes"
								  name="ext_notes"><%=Encode.forHtmlAttribute(notes)%></textarea>
					</td>
				</tr>
				<%}%>

<%
				if (consultationSignatureEnabledSystemPreference) {
				%>
				<tr>
					<td colspan=2 class="tite4"><bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formSignature" />:
					</td>
				</tr>
				<tr>
					<td colspan=2>
						<%
							UserProperty signatureProperty = null;
							if (OscarProperties.getInstance().isPropertyActive("consult_auto_load_signature")) {
								signatureProperty = userPropertyDAO.getProp(providerNo,UserProperty.PROVIDER_CONSULT_SIGNATURE);
							}
							boolean signWithStamp = signatureProperty != null && requestId == null;
						%>
						<input type="hidden" name="newSignature" id="newSignature" value="<%=String.valueOf(consultUtil.signatureImg == null)%>" />
						<input type="hidden" name="signatureImg" id="signatureImg" value="<%=(consultUtil.signatureImg != null ? consultUtil.signatureImg : "") %>" />
						<input type="hidden" name="newSignatureImg" id="newSignatureImg" value="" />
						<input type="hidden" name="signedWithStamp" id="signedWithStamp" value="<%=signWithStamp%>" />

						<div id="signatureShow" style="display: none;">
							<img id="signatureImgTag" src="" />
						</div>

						<%
						
						if (OscarProperties.getInstance().getBooleanProperty("topaz_enabled", "true")) { %>
						<input type="button" id="clickToSign" onclick="requestSignature()" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.formClickToSign" />" />
						<% } else if (signWithStamp) { %>
						<img id="signatureFrame" src="<%=request.getContextPath()%>/eform/displayImage.do?imagefile=<%=signatureProperty.getValue()%>"/>
						<% } else { %>
						<iframe style="width:500px; height:132px;"id="signatureFrame" src="<%= request.getContextPath() %>/signature_pad/tabletSignature.jsp?inWindow=true&<%=DigitalSignatureUtils.SIGNATURE_REQUEST_ID_KEY%>=<%=signatureRequestId%>" ></iframe>
						<% } %>

					</td>
				</tr>
				<% }
				if (attachmentManagerConsultationEnabled) {
					String attachmentWidgetJson = "";
					if (requestId != null) {
						attachmentWidgetJson = thisForm.getLegacyAttachment()
								? consultationManager.getLegacyAttachmentsPrintableJson(request)
								: thisForm.getAttachments();
					}
				%>
				<tr>
					<td colspan=2>
						<%@ include file="/well/feature/attachment-manager/attachmentManagerWidget.jspf" %>
					</td>
				</tr>
				<% }
				if (consultationFaxEnabled && !lockForm  && thisForm.geteReferralId() == null) {
				%>
				<tr><td colspan=2 class="tite4">Additional Fax Recipients:</td></tr>
				<tr>
					<td colspan=2>
					    <%
					    String rdohip = "";
					    if (demographic!=null) {
					    	rdohip = demographic.getReferralPhysicianOhip();
					    }
					    %>
						<table width="100%">
						<tr>

							<td class="tite4" width="10%">  Providers: </td>
							<td class="tite3" width="20%">
								<select id="otherFaxSelect">
									<option value="">Select additional fax recipients</option>
								<%
								String rdName = "";
								String rdFaxNo = "";
								for (int i=0;i < displayServiceUtil.specIdVec.size(); i++) {
		                                 String  specId     =  displayServiceUtil.specIdVec.elementAt(i);
		                                 String  fName      =  displayServiceUtil.fNameVec.elementAt(i);
		                                 String  lName      =  displayServiceUtil.lNameVec.elementAt(i);
		                                 String  proLetters =  displayServiceUtil.proLettersVec.elementAt(i);
		                                 String  address    =  displayServiceUtil.addressVec.elementAt(i);
		                                 String  phone      =  displayServiceUtil.phoneVec.elementAt(i);
		                                 String  fax        =  displayServiceUtil.faxVec.elementAt(i);
		                                 String annotation = displayServiceUtil.annotationVec.elementAt(i);
		                                 boolean annotateInSearch = displayServiceUtil.annotateInSearchVec.elementAt(i) && StringUtils.filled(annotation);
		                                 String  referralNo = ""; // TODO: add referal number to specialists ((String) displayServiceUtil.referralNoList.get(i)).trim();
		                                 if (rdohip != null && !"".equals(rdohip) && rdohip.equals(referralNo)) {
		                                	 rdName = String.format("%s, %s", lName, fName);
		                                	 rdFaxNo = fax;
		                                 }
									if (!"".equals(fax)) {
									%>

									<option value="<%= fax %>"> <%= Encode.forHtml(String.format("%s, %s %s", lName, fName, (annotateInSearch ? "(" + annotation + ")" : "")))%> </option>
									<%
									}
								}
		                        %>
								</select>
							</td>
							<td class="tite3">
								<button onclick="AddOtherFaxProvider(); return false;">Add Provider</button>
							</td>
						</tr>
						<tr>
							<td class="tite4" width="20%"> Other Fax Number: </td>
							<td class="tite3" width="32%">
								<input type="text" id="otherFaxInput"></input>

							<font size="1"> <bean:message key="global.phoneformat1" />  </font></td>
							<td class="tite3">
								<button onclick="AddOtherFax(); return false;">Add Other Fax Recipient</button>
							</td>
						</tr>
						<tr>
							<td colspan=3>
								<ul id="faxRecipients">
								<%
								if (!"".equals(rdName) && !"".equals(rdFaxNo)) {
									%>
								<!--<li>-->
										<!-- <%--= rdName %> <b>Fax No: </b><%= rdFaxNo --%> <a href="javascript:void(0);" onclick="removeRecipient(this)">remove</a>-->
										<input type="hidden" name="faxRecipients" value="<%= rdFaxNo %>" />
								<!--</li>-->
									<%
								}
								%>
								</ul>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				<% } %>



				<% if ((consultUtil.locked && OscarProperties.getInstance().isPropertyActive("consultation_lock_on_print")) && request.getAttribute("id") != null) { %>
				<tr>
					<td class="tite4" colspan=2>
						<input type="hidden" name="submission" value="updateLocked"/>
						<input name="updateLocked" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnUpdate"/>"
							   onclick="document.forms[0].service.disabled = false; document.forms['EctConsultationFormRequestForm'].submit();"/>
					</td>
				</tr>
				<% } else if (!lockForm && thisForm.geteReferralId() == null) { %>
				<tr>
					<td colspan=2><input type="hidden" name="submission" value="">
					<security:oscarSec roleName="<%=roleName$%>" objectName="_con" rights="w" reverse="false">
						<%
							if (request.getAttribute("id") != null)
							{
						%>
						<input name="update" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnUpdate"/>" onclick="return checkForm('Update Consultation Request','EctConsultationFormRequestForm');" />
						<input name="updateAndPrint" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnUpdateAndPrint"/>" onclick="return checkForm('Update Consultation Request And Print Preview','EctConsultationFormRequestForm');" />
						<logic:equal value="true" name="EctConsultationFormRequestForm" property="eReferral">
							<input name="updateAndSendElectronically" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnUpdateAndSendElectronicReferral"/>" onclick="return checkForm('Update_esend','EctConsultationFormRequestForm');" />
						</logic:equal>
								<% if (echartPreferencesMap.getOrDefault("echart_paste_fax_note", false)) { %>
						<oscar:oscarPropertiesCheck value="yes" property="faxEnable">
							<input id="fax_button2" name="updateAndFax" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnUpdateAndFax"/>" onclick="paste2Note();return checkForm('Update And Fax','EctConsultationFormRequestForm');" />
							<input id="update_print_fax_button2" name="updatePrintAndFax" type="button" value="Update, Print, & Fax" onclick="paste2Note();return checkForm('Update Print And Fax','EctConsultationFormRequestForm');" />
						</oscar:oscarPropertiesCheck>
								<%} else { %>
						<oscar:oscarPropertiesCheck value="yes" property="faxEnable">
							<input id="fax_button2" name="updateAndFax" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnUpdateAndFax"/>" onclick="return checkForm('Update And Fax','EctConsultationFormRequestForm');" />
							<input id="update_print_fax_button2" name="updatePrintAndFax" type="button" value="Update, Print, & Fax" onclick="return checkForm('Update Print And Fax','EctConsultationFormRequestForm');" />
						</oscar:oscarPropertiesCheck>
								<%} %>
						<%
						}
						else
						{
						%>
						<input name="submitSaveOnly" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnSubmit"/>" onclick="return checkForm('Submit Consultation Request','EctConsultationFormRequestForm'); " />
						<input name="submitAndPrint" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnSubmitAndPrint"/>" onclick="return checkForm('Submit Consultation Request And Print Preview','EctConsultationFormRequestForm'); " />
						<logic:equal value="true" property="eReferral" name="EctConsultationFormRequestForm" >
							<input name="submitAndSendElectronically" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnSubmitAndSendElectronicReferral"/>" onclick="return checkForm('Submit_esend','EctConsultationFormRequestForm');" />
						</logic:equal>
						
							<% if (echartPreferencesMap.getOrDefault("echart_paste_fax_note", false)) { %>
						<oscar:oscarPropertiesCheck value="yes" property="faxEnable">
							<input id="fax_button2" name="submitAndFax" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnSubmitAndFax"/>" onclick="paste2Note();return checkForm('Submit And Fax','EctConsultationFormRequestForm');" />
							<input id="submit_print_fax_button2" name="submitPrintAndFax" type="button" value="Submit, Print, & Fax" onclick="paste2Note();return checkForm('Submit Print And Fax','EctConsultationFormRequestForm');" />
						</oscar:oscarPropertiesCheck>
							<%} else { %>
						<oscar:oscarPropertiesCheck value="yes" property="faxEnable">
							<input id="fax_button2" name="submitAndFax" type="button" value="<bean:message key="oscarEncounter.oscarConsultationRequest.ConsultationFormRequest.btnSubmitAndFax"/>" onclick="return checkForm('Submit And Fax','EctConsultationFormRequestForm');" />
							<input id="submit_print_fax_button2" name="submitPrintAndFax" type="button" value="Submit, Print, & Fax" onclick="return checkForm('Submit Print And Fax','EctConsultationFormRequestForm');" />
						</oscar:oscarPropertiesCheck>
							<%} %>
						<%
							}%>

						<logic:equal value="true" name="EctConsultationFormRequestForm" property="eReferral">
							<input type="button" value="Send eResponse" onclick="$('saved').value='true';document.location='<%=thisForm.getOruR01UrlString(request)%>'" />
						</logic:equal>
					</security:oscarSec>
					</td>
				</tr>
				<% } %>

				<script type="text/javascript">

	        initMaster();
	        initService('<%=consultUtil.service%>',
				'<%=((consultUtil.service==null)?"":StringEscapeUtils.escapeJavaScript(consultUtil.getServiceName(consultUtil.service.toString())))%>',
				'<%=consultUtil.specialist%>',
				'<%=((consultUtil.specialist==null)?"":StringEscapeUtils.escapeJavaScript(consultUtil.getSpecialistsName(consultUtil.specialist.toString())))%>',
				'<%=StringEscapeUtils.escapeJavaScript(consultUtil.specPhone)%>',
				'<%=StringEscapeUtils.escapeJavaScript(consultUtil.specFax)%>',
				'<%=StringEscapeUtils.escapeJavaScript(consultUtil.specAddr)%>'.replace(new RegExp("\\\\r\\\\n", 'g'), "\r\n"));
            initSpec();
            document.EctConsultationFormRequestForm.phone.value = ("");
        	document.EctConsultationFormRequestForm.fax.value = ("");
        	document.EctConsultationFormRequestForm.address.value = ("");
            <%if (request.getAttribute("id") != null)
					{%>
                setSpec('<%=consultUtil.service%>','<%=consultUtil.specialist%>');
                FillThreeBoxes('<%=consultUtil.specialist%>');
            <%}
					else
					{%>
                document.EctConsultationFormRequestForm.service.options.selectedIndex = 0;
                document.EctConsultationFormRequestForm.specialist.options.selectedIndex = 0;
            <%}%>

            onSelectSpecialist(document.EctConsultationFormRequestForm.specialist);
            
            <%
            	//new with BORN referrals. Allow form to be loaded with service and 
            	//specialist pre-selected
            	String reqService = request.getParameter("service");
            	
            	String reqSpecialist = request.getParameter("specialist");
            	if(reqService != null && reqSpecialist != null) {
            		ConsultationServices consultService = consultationServiceDao.findByDescription(reqService);
            		if(consultService != null) {
            		%>
            		$jq("#service").val('<%=consultService.getId()%>');
            		fillSpecialistSelect(document.getElementById('service'));
            		$jq("#specialist").val('<%=reqSpecialist%>');
            		onSelectSpecialist(document.getElementById('specialist'));
            		<%
            	} }
            	
            	String serviceId = request.getParameter("serviceId");
            	if(serviceId != null) {
            		%>
            		$jq("#service").val('<%=serviceId%>');
            		fillSpecialistSelect(document.getElementById('service'));
            		<%
            	}
            %>
        //-->
        </script>




				<!----End new rows here-->

				<tr height="100%">
					<td></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td class="MainTableBottomRowLeftColumn"></td>
			<td class="MainTableBottomRowRightColumn"></td>
		</tr>
	</table>
</html:form>
</body>

<script type="text/javascript" language="javascript">
createStandardDatepicker(jQuery_3_1_0('#referalDate'), "referalDate_cal");
createStandardDatepicker(jQuery_3_1_0('#followUpDate'), "followUpDate_cal");
createStandardDatepicker(jQuery_3_1_0('#appointmentDate'), "appointmentDate_cal");
</script>
</html:html>

<%! protected String listNotes(CaseManagementManager cmgmtMgr, String code, String providerNo, String demoNo)
	{
		// filter the notes by the checked issues
		List<Issue> issues = cmgmtMgr.getIssueInfoByCode(providerNo, code);

		String[] issueIds = new String[issues.size()];
		int idx = 0;
		for (Issue issue : issues)
		{
			issueIds[idx] = String.valueOf(issue.getId());
		}

		// need to apply issue filter
		List<CaseManagementNote> notes = cmgmtMgr.getNotes(demoNo, issueIds);
		StringBuffer noteStr = new StringBuffer();
		for (CaseManagementNote n : notes)
		{
			if (!n.isLocked() && !n.isArchived()) noteStr.append(n.getNote() + "\n");
		}

		return noteStr.toString();
	}

    protected boolean practitionerNotInList(List<org.oscarehr.common.model.Provider>
      practitionerList, String currentPractitioner) {
      for (org.oscarehr.common.model.Provider provider : practitionerList) {
        if (!provider.getProviderNo().equals(currentPractitioner)) {
          return true;
        }
      }
      return false;
    }
%>
