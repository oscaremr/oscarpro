<%--

    Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
    This software is published under the GPL GNU General Public License.
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

    This software was written for the
    Department of Family Medicine
    McMaster University
    Hamilton
    Ontario, Canada

--%>

<%@ page import="org.oscarehr.managers.ConsultationManager.ConsultationReferringPractitioner" %>
<%@ page import="org.oscarehr.PMmodule.dao.ProviderDao" %>
<%@ taglib uri="/WEB-INF/security.tld" prefix="security"%>
<%
    String roleName$ = (String)session.getAttribute("userrole") + "," + (String) session.getAttribute("user");
    boolean authed=true;
%>
<security:oscarSec roleName="<%=roleName$%>" objectName="_admin,_admin.consult" rights="w" reverse="<%=true%>">
    <%authed=false; %>
    <%response.sendRedirect("../../../securityError.jsp?type=_admin&type=_admin.consult");%>
</security:oscarSec>
<%
    if(!authed) {
        return;
    }
%>

<%@ page import="org.oscarehr.util.SpringUtils" %>
<%@ page import="org.oscarehr.managers.ConsultationManager" %>
<%@ page import="org.oscarehr.util.LoggedInInfo" %>
<%@ page import="oscar.oscarEncounter.oscarConsultationRequest.config.pageUtil.EctConRequestSettingsForm" %>
<%@ page import="org.oscarehr.common.model.SystemPreferences" %>
<%@ page import="oscar.util.SystemPreferencesUtils" %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/wellAiVoice.tld" prefix="well-ai-voice"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
    ConsultationManager consultationManager = SpringUtils.getBean(ConsultationManager.class);

    String defaultAppointmentInstructions = consultationManager.getConsultDefaultAppointmentInstructions();
    boolean displayProviderOnConsultationPdfEnabled = consultationManager.shouldDisplayProviderOnConsultationPdf();

    // Look up list 
    org.oscarehr.managers.LookupListManager lookupListManager = SpringUtils.getBean(org.oscarehr.managers.LookupListManager.class);
    pageContext.setAttribute("appointmentInstructionList", lookupListManager.findLookupListByName(loggedInInfo, "consultApptInst") );

    SystemPreferences filterMeasurementByAppointmentPreference = SystemPreferencesUtils.findPreferenceByName(
            ConsultationManager.CON_FILTER_OCULAR_MEASUREMENTS_BY_APPOINTMENT);
    if (filterMeasurementByAppointmentPreference == null) {
        filterMeasurementByAppointmentPreference = new SystemPreferences(
                ConsultationManager.CON_FILTER_OCULAR_MEASUREMENTS_BY_APPOINTMENT, "false");
    }
%>
<html:html locale="true">
    <head>
        <script type="text/javascript" src="<%= request.getContextPath() %>/js/global.js"></script>
        <title><bean:message
                key="oscarEncounter.oscarConsultationRequest.config.ConsultRequestSettings.title" />
        </title>
        <html:base />
    </head>

    <link rel="stylesheet" type="text/css" href="../../encounterStyles.css">
    <body class="BodyStyle" vlink="#0000FF">
      <well-ai-voice:script/>
    <!--  -->
    <table class="MainTable" id="scrollNumber1" name="encounterTable">
        <tr class="MainTableTopRow">
            <td class="MainTableTopRowLeftColumn">Consultation</td>
            <td class="MainTableTopRowRightColumn">
                <table class="TopStatusBar">
                    <tr>
                        <td class="Header"><bean:message
                                key="oscarEncounter.oscarConsultationRequest.config.ConsultRequestSettings.title" />
                        </td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style="vertical-align: top">
            <td class="MainTableLeftColumn">
                <%oscar.oscarEncounter.oscarConsultationRequest.config.pageUtil.EctConTitlebar titlebar = new oscar.oscarEncounter.oscarConsultationRequest.config.pageUtil.EctConTitlebar(request);
                    out.print(titlebar.estBar(request));
                %>
            </td>
            <td class="MainTableRightColumn">
                <table cellpadding="0" cellspacing="2"
                       style="border-collapse: collapse" bordercolor="#111111" width="100%"
                       height="100%">

                    <tr><td>&nbsp;</td></tr>
                    <%
                        String updated = (String) request.getAttribute("CONSULTATION_REQUEST_SETTINGS_UPDATED");
                        if (updated != null) {  %>
                    <tr>
                        <td><font color="red"><bean:message
                                key="oscarEncounter.oscarConsultationRequest.config.ConsultRequestSettings.msgUpdated" /></font></td>
                    </tr>
                    <%}%>
                    <tr>
                        <td>
                            <table>
                                <html:form action="/oscarEncounter/ConsultRequestSettings">
<%
    // Get Defaults for the form
    EctConRequestSettingsForm thisForm = (EctConRequestSettingsForm) request.getAttribute("EctConRequestSettingsForm");
    if (thisForm != null) {
        thisForm.setDefaultAppointmentInstructions(defaultAppointmentInstructions);
        thisForm.setDisplayProviderOnConsultationPdf(displayProviderOnConsultationPdfEnabled);
    }
%>
                                    <tr>
                                        <td>
                                            <bean:message key="oscarEncounter.oscarConsultationRequest.config.ConsultRequestSettings.defaultAppointmentInstructions" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <html:select property="defaultAppointmentInstructions" styleId="appointmentInstructions">
                                                <html:option value="" ></html:option>
                                                <c:forEach items="${ appointmentInstructionList.items }" var="appointmentInstruction">
                                                    <%-- Ensure that only active items are shown --%>
                                                    <c:if test="${ appointmentInstruction.active }" >
                                                        <html:option value="${ appointmentInstruction.value }" >
                                                            <c:out value="${ appointmentInstruction.label }" />
                                                        </html:option>
                                                    </c:if>
                                                </c:forEach>
                                            </html:select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <bean:message
                                                    key="oscarEncounter.oscarConsultationRequest.config.ConsultRequestSettings.displayCreatedByProviderOnThePdfVersion"/>
                                        </td>
                                        <td>
                                            <label style="display: inline">
                                                <input type="radio" value="true"
                                                       name="displayProviderOnConsultationPdf" <%= (displayProviderOnConsultationPdfEnabled? "checked" : "") %>/> Yes
                                            </label>
                                            &nbsp;&nbsp;&nbsp;
                                            <label style="display: inline">
                                                <input type="radio" value="false"
                                                       name="displayProviderOnConsultationPdf" <%= (!displayProviderOnConsultationPdfEnabled? "checked" : "") %> /> No
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <bean:message key="oscarEncounter.oscarConsultationRequest.config.ConsultRequestSettings.measurementsFilterByAppointment" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="radio" name="consultFilterOcularMeasurementsByAppt" value="false"
                                                    <%= filterMeasurementByAppointmentPreference.getValueAsBoolean()
                                                            ? "" : "checked" %>/>Off
                                            <br/>
                                            <input type="radio" name="consultFilterOcularMeasurementsByAppt" value="true"
                                                    <%= filterMeasurementByAppointmentPreference.getValueAsBoolean()
                                                            ? "checked" : "" %>/>On
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <bean:message key="oscarEncounter.oscarConsultationRequest.config.ConsultRequestSettings.referringPractitioner" />
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td>
                                    		<% for (ConsultationReferringPractitioner referringPractitioner : ConsultationManager.ConsultationReferringPractitioner.values()) { %>
	                                    		<input type="checkbox"
	                                			        name="<%= referringPractitioner.getConsultationManagerReferringString() %>"
	                                			        <%= ProviderDao.isReferringPractitionerValueTrue(SystemPreferencesUtils.findPreferenceByName(referringPractitioner.getConsultationManagerReferringString())) ? "checked" : "" %> />
	                                			<%= referringPractitioner.getDisplayValue() %> <br/>
                                    		<% } %>
                                    	</td>
                                    </tr>
                                    <tr><td>&nbsp;</td></tr>
                                    <tr>
                                        <td><input type="submit"
                                                   value="<bean:message key="oscarEncounter.oscarConsultationRequest.config.ConsultRequestSettings.btnUpdate"/>" />
                                        </td>
                                    </tr>
                                </html:form>
                            </table>
                        </td>
                    </tr>

                    <tr height="100%">
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="MainTableBottomRowLeftColumn"></td>
            <td class="MainTableBottomRowRightColumn"></td>
        </tr>
    </table>
    </body>
</html:html>
