﻿// myMain.js

/*jshint onevar: false */
/*jslint newcap: true*/
/*jslint white: true */
/*jshint eqnull:true */

/*global window : false */
/*global ExternalCommandNames : false */
/*global updateGetImageHyperlink : false */
/*global timerFunction : false */
/*global setInterval : false */
/*global clearInterval : false */
/*global QueryArchiveServiceProxy : false */
/*global PatientServiceProxy : false */
/*global AuthenticationServiceProxy : false */

/*global onClick_showPatient : false */
/*global onClick_showStudy : false */
/*global onClick_showSeries : false */
/*global onChange_updateStudies : false */
/*global onChange_updateSeries : false */
/*global onChange_updateInstances : false */


/*global ExternalCommand : false */

/*global location : false */
/*global document : false */

/*global initializeViewInstances : false */
/*global initializeUpdatePatient : false */
/*global initializeAddPatient : false */
/*global initializeUpdateUser : false */
/*global initializeAddUser : false */


/*global Logout : false */
/*global WebMedicalMessage : false */
/*global MainPageController : false */
/*global Logger : false */


var controller = null;
var logger = null;
var timerFunctionId = null;

// Limit the number of query results for servers with large amounts of data
var maxPatientQueryResults = 1000;
var maxStudyQueryResults = 1000;
var maxSeriesQueryResults = 1000;  
var maxInstanceQueryResults = 4000;  

//var hostIP = "http://10.28.75.196/";
//var globalUserName = "hartuser";
//var globalUserPassword = "00HART!";
var hostIP = hostIP1;
var globalUserName = globalUserName1;
var globalUserPassword = globalUserPassword1;

//Maged added
var VieweImagesViewerSettings   =  "location=no,resizable=1, left=0,top=0,width="+ (screen.width-200) +',height='+ (screen.height-150);
var GetStudiesViewerSettings    = "toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no,left=10000, top=10000, width=50, height=50, visible=none";
var functionToCallAfterLoad     = '';
var intervalVar = null;
var timeoutBeforeshowVeiwer = 5000;
var loadingMode = 0;        //0 login, 1 load sieres, 2 display images

window.onbeforeunload = function () {
    controller.LogOff();
}

window.onload = function () {
    controller = new MainPageController();
    logger = new Logger();

    // login/Logout
    $("#idButtonClearLog").click(onClick_ClearLog);

    // viewInstances
    $("#idButton_viewInstances_showPatient").click(controller.ShowPatient);
    $("#idButton_viewInstances_showStudy").click(controller.ShowStudy);
    $("#idButton_viewInstances_showSeries").click(controller.ShowSeries);

    $("#idDropDownList_viewInstances_study").change(controller.UpdateSeries);
    $("#idDropDownList_viewInstances_series").change(controller.UpdateInstances);

    //patientID = '33289';
    patientID = demoNo;
    controller.LogIn();
};

function EnableButtons(enable) {
    document.getElementById("idButton_viewInstances_showPatient").disabled = !enable;
    document.getElementById("idButton_viewInstances_showStudy").disabled = !enable;
    document.getElementById("idButton_viewInstances_showSeries").disabled = !enable;
    document.getElementById("idDropDownList_viewInstances_study").disabled = !enable;
    document.getElementById("idDropDownList_viewInstances_series").disabled = !enable;
}

// First argument is command name
// remaining arguments are arg1, arg2, ...
function ExternalCommand() {

    if (arguments.length >= 1) {
        this.name = arguments[0];
    }

    if (arguments.length >= 2) {
        this.arg1 = arguments[1];
    }

    if (arguments.length >= 3) {
        this.arg2 = arguments[2];
    }
}

function sendExternalControllerCommand(externalCommand, viewerWindow, url) {
    var p = JSON.stringify(externalCommand);
    viewerWindow.postMessage(p.toString(), url);
}

// First argument is command name
// remaining arguments are arg1, arg2, ...
function runExternalControllerCommand()
{
    var externalCommand;

    switch (arguments.length) {
        case 1:
            externalCommand = new ExternalCommand(arguments[0]); 
            break;
        case 2:
            externalCommand = new ExternalCommand(arguments[0], arguments[1]);
            break;
        case 3:
            externalCommand = new ExternalCommand(arguments[0], arguments[1], arguments[2]);
            break;
        case 4:
            externalCommand = new ExternalCommand(arguments[0], arguments[1], arguments[2], arguments[3]);
            break;
        case 5:
            externalCommand = new ExternalCommand(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
            break;
        case 6:
            externalCommand = new ExternalCommand(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5]);
            break;
    }

    sendExternalControllerCommand(externalCommand, controller.ViewerWindow, "*"); 
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function MainPageController()
{
    this.AuthenticationToken = "";
    this.AuthenticationProxy = null;
    this.QueryProxy = null;
    this.ViewerWindow = null;
    this.RemoteLogOut = false;

    window.onmessage = function (event) {
        if (event.source === controller.ViewerWindow) {
            if (IsJsonString(event.data)) {
                var result = JSON.parse(event.data);
                switch (result.commandName) {
                    case ExternalCommandNames.GetCurrentPatient:
                        logger.LogMessage(result.commandName, result.commandResult);
                        logger.LogMessagePatientInfo(result.patientInfo);
                        break;

                    case ExternalCommandNames.GetImage:
                        updateGetImageHyperlink(result.url);
                        var sop = "SOP Instance UID: " + result.sopInstanceUID;
                        logger.LogMessage(result.commandName, sop, result.url, result.commandResult);
                        break;

                    case ExternalCommandNames.LogOut:
                        if (controller.RemoteLogOut === false) {
                            logger.LogMessage(result.commandName, result.commandResult);
                        }
                        controller.RemoteLogOut = true;
                        break;

                    default:
                        logger.LogMessage(result.commandName, result.commandResult);
                        break;

                }
            }
        }
    };
}

///
MainPageController.prototype.LogIn = function () {
    //Maged added
    loadingMode = 0;
    controller.Authenticate(controller.onAuthenticationError, controller.onAuthenticationSuccess);
};

//Maged
MainPageController.prototype.ShowSelected = function (functionName, myLoadingMode) {
    EnableButtons(false);
    loadingMode = myLoadingMode;
    functionToCallAfterLoad = functionName;
    Logout(false);
    controller.Authenticate(controller.onAuthenticationError, controller.onAuthenticationSuccess);
}

//Maged added
MainPageController.prototype.ShowPatient = function () {
    controller.ShowSelected('onClick_showPatient', 2);
};


//Maged added
MainPageController.prototype.ShowStudy = function () {
    controller.ShowSelected('onClick_showStudy', 2);
};

//Maged added
MainPageController.prototype.ShowSeries = function () {
    controller.ShowSelected('onClick_showSeries', 2);
};

//Maged added
MainPageController.prototype.UpdateSeries = function () {
    controller.ShowSelected('onChange_updateSeries', 1);
};

//Maged added
MainPageController.prototype.UpdateInstances = function () {
    controller.ShowSelected('onChange_updateInstances', 1);
};

MainPageController.prototype.onAuthenticationError = function (xhr, status, ex) {
    var errorString = "";

    if (ex.hasOwnProperty("message")) {
        var n = ex.message.indexOf("{");
        var j = ex.message.substr(n);
        var error = JSON.parse(j);
        errorString = error.Message;
    }
    else {
        errorString = ex;
    }
    logger.LogMessage("Login: ", "Error: " + errorString);

    Logout(false);
};

MainPageController.prototype.onAuthenticationSuccess = function (authentication) {

    if (IsJsonString(authentication)) {
        var result = JSON.parse(authentication);
        var errorString = result.Message;
        logger.LogMessage("Login: ", "Error: " + errorString);
        Logout(false);
    }
    else {
        controller.RemoteLogOut = false;
        logger.LogMessage("Login", "Success", globalUserName, globalUserPassword);

        controller.AuthenticationToken = authentication;
        controller.AuthenticationProxy = new AuthenticationServiceProxy(hostIP+"MedicalViewerService19/AuthenticationService.svc");
        controller.AuthenticationProxy.SetAuthenticationCookie(authentication);

        controller.QueryProxy = new QueryArchiveServiceProxy(hostIP + "MedicalViewerService19/ObjectQueryService.svc", controller.AuthenticationProxy);

        controller.PatientProxy = new PatientServiceProxy(hostIP + "MedicalViewerService19/PatientService.svc", controller.AuthenticationProxy);

        controller.RunViewer();
    }
};

MainPageController.prototype.Authenticate = function (errorHandler, successHandler) {
    var serviceUrl = hostIP + "MedicalViewerService19/AuthenticationService.svc/AuthenticateUser";
    var parameters = { userName: globalUserName, password: globalUserPassword, userData: null };

    var p = JSON.stringify(parameters);

    return $.ajax({
        type: "POST",
        contentType: "application/json",
        async: false,
        url: serviceUrl,
        data: JSON.stringify(parameters),
        error: errorHandler,
        success: successHandler
    });
};

MainPageController.prototype.RunViewer = function () {
    if ("" === controller.AuthenticationToken) {
        logger.LogMessage("RunViewer", "Error: Not currently logged in.");
        return;
    }

    var url = hostIP + "MedicalViewer19/#/login/autologin/" + "0/" + encodeURIComponent(controller.AuthenticationToken);

    //Maged added
    if (loadingMode == 0 || loadingMode==1)
        controller.ViewerWindow = window.open(url, "MedicalViewer19", GetStudiesViewerSettings);
    else
        controller.ViewerWindow = window.open(url, "MedicalViewer19", VieweImagesViewerSettings);

    if (!controller.ViewerWindow) {
        logger.LogMessage("RunViewer", "Error: Viewer could not be opened.");
    }
    else {
        if (loadingMode == 0) {
            controller.ShowSelected('updatePatients', 1);
        }
        else {
            justInitializeViewInstances(controller.ViewerWindow);

            if (loadingMode == 0 || loadingMode == 1)
                intervalVar = setInterval(showCurrStudy, 2000);
            else
                intervalVar = setInterval(showCurrStudy, timeoutBeforeshowVeiwer);
        }
    }
};

function showCurrStudy() {
    clearInterval(intervalVar);
    intervalVar = null;
    window[functionToCallAfterLoad]();
    functionToCallAfterLoad = '';
    EnableButtons(true);
}

MainPageController.prototype.IsViewerActive = function () {
    return (controller.ViewerWindow != null) && (!controller.ViewerWindow.closed);
};

function Logout(logErrors) {
    var commandName = "Logout";
    if (controller.IsViewerActive()) {
        controller.ViewerWindow.close();
        if (logErrors === true) {
            logger.LogMessage(commandName, "Success");
        }
    }
    else {
        if (logErrors === true) {
            logger.LogMessage(commandName, "User has already logged out");
        }
    }

    controller.AuthenticationToken = "";
    controller.AuthenticationProxy = null;
    controller.QueryProxy = null;
    controller.PatientProxy = null;
}


MainPageController.prototype.LogOff = function () {
    Logout(true);
};
