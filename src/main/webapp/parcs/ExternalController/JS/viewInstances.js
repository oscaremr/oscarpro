﻿// viewInstances.js 

/*jshint eqnull:true */
/*jslint plusplus: true */
/*jslint white: true */
/*global describe:true*/
/*jslint newcap: true*/
/*jslint nomen: true*/
/*jshint onevar: false */

/*global runExternalControllerCommand : false */
/*global window : false */

/*global logger : false */ 
/*global document : false */ 
/*global controller : false */ 
/*global ExternalCommandNames : false */ 
/*global updateGetImageHyperlink : false */ 
/*global Option : false */ 
/*global onChange_updateStudies : false */ 
/*global onChange_updateSeries : false */ 
/*global onChange_updateInstances : false */ 
/*global xxxxxxxxxxxxxxxx : false */ 
/*global xxxxxxxxxxxxxxxx : false */ 
/*global xxxxxxxxxxxxxxxx : false */ 
/*global xxxxxxxxxxxxxxxx : false */ 

var patients = [];
var studies = [];
var series = [];
var instances = [];
var allStudyResults = [];
var ViewerWindow = null;
var currImageIndex = -1;
var maxColumns = 7;
var patientID = '';

function getDropDownSelectedText(ddl) {
    var dropdown = document.getElementById(ddl);
    if (dropdown === undefined) {
        return undefined;
    }
    var selectedText = "";
    if (dropdown.options.length > 0) {
        selectedText = dropdown.options[dropdown.selectedIndex].text;
    }
    return selectedText;
}

function getDropDownSelectedIndex(ddl) {
    var dropdown = document.getElementById(ddl);
    if (dropdown === undefined) {
        return undefined;
    }
    var selectedIndex = "";
    if (dropdown.options.length > 0) {
        selectedIndex = dropdown.selectedIndex;
    }
    return selectedIndex;
}

function setDropDownText(ddl, textToFind) {
    var dd = document.getElementById(ddl);
    for (var i = 0; i < dd.options.length; i++) {
        if (dd.options[i].text === textToFind) {
            dd.selectedIndex = i;
            break;
        }
    }
}

function removeOptions(ddl) {
    var dropdown = document.getElementById(ddl);
    var i;
    for (i = dropdown.options.length - 1; i >= 0; i--) {
        dropdown.remove(i);
    }
}

function populateDropDownList(ddl, values) {

    removeOptions(ddl);
    var dropdown = document.getElementById(ddl);

    for (var i = 0; i < values.length; i++) {
        dropdown[dropdown.length] = new Option(values[i], values[i]);
    }
}

///
function closeViewerWindow() {
    if (ViewerWindow != null && !ViewerWindow.closed)
        ViewerWindow.close();

    ViewerWindow = null;
}

function onUpdatePatientsError(xhr, status, ex) {
    logger.LogMessage("FindPatients", "Failed to query for patients: " + ex);
    closeViewerWindow();
}

function viewInstances_DisableItems(disable) {
    // Buttons
    document.getElementById("idButton_viewInstances_showPatient").disabled = disable;
    document.getElementById("idButton_viewInstances_showStudy").disabled = disable;
    document.getElementById("idButton_viewInstances_showSeries").disabled = disable;
    
    // DropDowns
    document.getElementById("idDropDownList_viewInstances_study").disabled = disable;
    document.getElementById("idDropDownList_viewInstances_series").disabled = disable;
}

function onUpdatePatientsSuccess(patientResults) {
    patients = new Array();
    if (patientResults) {
        for (var i = 0; i < patientResults.length; i++) {
            patients[i] = patientResults[i].ID;
        }
    }

    if (patients.length > 0) {
        viewInstances_DisableItems(false);
    }
    else {
        viewInstances_DisableItems(true);
    }

    onChange_updateStudies();
}

function updatePatients() {
    if (controller.QueryProxy) {
        var queryParams = {};
        queryParams.options = {};
        queryParams.options.PatientsOptions = {};
        queryParams.options.PatientsOptions.PatientID = patientID;
        controller.QueryProxy.FindPatientsMax(queryParams, maxPatientQueryResults, onUpdatePatientsError, onUpdatePatientsSuccess);
    }
}

function initializeViewInstances(patientID, OpenedViewerWindow) {
    try
    {
        ViewerWindow = OpenedViewerWindow;
        updateGetImageHyperlink(null);
        updatePatients(patientID);
    }
    catch (error) {
        alert('initializeViewInstances:' + error.message);
    }
}

function justInitializeViewInstances(OpenedViewerWindow) {
    ViewerWindow = OpenedViewerWindow;
}

function onUpdateStudiesError(xhr, stata, ex) {
    logger.LogMessage("FindStudies", "Failed to query for studies: " + ex);
}

function onUpdateStudiesSuccess(studyResults) {
    studyResults.sort(function (a, b) {
        var c = new Date(a.Date);
        var d = new Date(b.Date);
        return d - c;
    });

    allStudyResults = studyResults;
    if (studyResults)
        for (var i = 0; i < studyResults.length; i++)
            studies[i] = studyResults[i].Date;

    populateDropDownList("idDropDownList_viewInstances_study", studies);
    onChange_updateSeries();
}

function onChange_updateStudies() {
    if (controller.QueryProxy) {
        var id = patientID;

        if (id == "") {
            onUpdateStudiesSuccess(null);
        }
        else {

            var queryParams = {};
            queryParams.options = {};
            queryParams.options.PatientsOptions = {};
            queryParams.options.PatientsOptions.PatientID = id;

            controller.QueryProxy.FindStudiesMax(queryParams, maxStudyQueryResults, onUpdateStudiesError, onUpdateStudiesSuccess);
        }
    }
}

function onUpdateSeriesError(xhr, status, ex) {
    logger.LogMessage("FindSeries", "Failed to query for series: " + ex);
    //closeViewerWindow();
}

function onUpdateSeriesSuccess(seriesResults) {
    series = new Array();
    if (seriesResults && seriesResults.length > 0) {
        for (var i = 0; i < seriesResults.length; i++) {
            series[i] = seriesResults[i].InstanceUID;
        }
    }
    populateDropDownList("idDropDownList_viewInstances_series", series);

    onChange_updateInstances();
}

function GetSelectedStudyID() {
    var selectedStuydIndex = getDropDownSelectedIndex("idDropDownList_viewInstances_study");
    if (selectedStuydIndex == "" && selectedStuydIndex != 0)
        return "";
    else
        return allStudyResults[selectedStuydIndex].InstanceUID;
}

function onChange_updateSeries() {
    if (controller.QueryProxy) {
        var id = GetSelectedStudyID();

        if (id == "" && id != 0) {
            onUpdateSeriesSuccess(null);
        }
        else {
            var queryParams = {};
            queryParams.options = {};
            queryParams.options.StudiesOptions = {};
            queryParams.options.StudiesOptions.StudyInstanceUID = id;

            controller.QueryProxy.FindSeriesMax(queryParams, maxSeriesQueryResults, onUpdateSeriesError, onUpdateSeriesSuccess);
        }
    }
}


function onUpdateInstancesError(xhr, status, ex) {
    logger.LogMessage("FindInstances", "Failed to query for instances: " + ex);
}

function onUpdateInstancesSuccess(instanceResults) {
    clearPACSThumbnail();
    instances = new Array();
    if (instanceResults && instanceResults.length > 0) {
        for (var i = 0; i < instanceResults.length; i++) {
            instances[i] = instanceResults[i].SOPInstanceUID;
        }
    }

    getNextImage();
}

function onChange_updateInstances() {
    if (controller.QueryProxy) {
        var id = getDropDownSelectedText("idDropDownList_viewInstances_series");
        if (id == "") {
            //onUpdateInstancesSuccess(null);
        }
        else {
            var queryParams = {};
            queryParams.options = {};
            queryParams.options.SeriesOptions = {};
            queryParams.options.SeriesOptions.SeriesInstanceUID = id;

            controller.QueryProxy.FindInstancesMax(queryParams, maxInstanceQueryResults, onUpdateInstancesError, onUpdateInstancesSuccess);
        }
    }
}

// Show methods
function onClick_showPatient() {
    var id = patientID;
    runExternalControllerCommand(ExternalCommandNames.ShowPatient, id);
}

function onClick_showStudy() {
    var id = GetSelectedStudyID();
    runExternalControllerCommand(ExternalCommandNames.ShowStudy, id);
}

function onClick_showSeries() {
    var id = getDropDownSelectedText("idDropDownList_viewInstances_series");
    runExternalControllerCommand(ExternalCommandNames.ShowSeries, id);
}

///
function getNextImage() {
    try {
        if (currImageIndex + 1 < instances.length) {
            currImageIndex++;
            runExternalControllerCommand(ExternalCommandNames.GetImage, instances[currImageIndex]);
        }
        else
            closeViewerWindow();
    }
    catch (error) {
        alert('getNextImage' + error.message);
    }
}

// Get Current Patient
function onClick_getCurrentPatient() {
        runExternalControllerCommand(ExternalCommandNames.GetCurrentPatient);
}

///
function displayResult(url) {
    if (url != null) {
        var table = document.getElementById("ucPACSThumbnail");
        var rowCount = table.rows.length;
        var row;
        var cell;

        if (rowCount == 0)
            row = table.insertRow(-1);
        else
            row = table.rows[rowCount - 1];

        if (row.cells.length < maxColumns)
            cell = row.insertCell(-1);
        else {
            row = table.insertRow(-1);
            cell = row.insertCell(-1);
        }

        cell.innerHTML = "<img width='80' src='" + url + "' onclick=\"window.open('" + url + "', '_blank');\"  />";
    }
}

///
function clearPACSThumbnail() {
    try
    {
        currImageIndex = -1;
        var tableHeaderRowCount = 0;
        var table = document.getElementById('ucPACSThumbnail');
        var rowCount = table.rows.length;
        for (var i = tableHeaderRowCount; i < rowCount; i++) {
            table.deleteRow(tableHeaderRowCount);
        }

    }
    catch (error) {
        alert(error.message);
    }
}

////
function updateGetImageHyperlink(url) {
    try
    {
        if (url != null)
            displayResult(url);

        if (url != null)
            getNextImage();
    }
    catch (error) {
        alert('updateGetImageHyperlink:' + error.message);
    }
}
