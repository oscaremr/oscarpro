﻿/// <reference path="../../lib/angular/angular.d.ts" />
/// <reference path="../../Scripts/Services/EventService.ts" />
/// <reference path="../../Scripts/models/Tab.ts" />
/// <reference path="../../Scripts/Services/TabService.ts" />
/// <reference path="ExternalCommands.ts" />
/// <reference path="ExternalCommandHandlerService.ts" />
/// <reference path="WebViewerCommands.ts" />

var ViewerMessageRecieverModule;
(function (ViewerMessageRecieverModule) {
    var CommandClass = (function () {
        function CommandClass() {
        }
        return CommandClass;
    })();
    ViewerMessageRecieverModule.CommandClass = CommandClass;

    if (typeof window.onload != 'function') {
        window.onload = function () {
            initialize();
        };
    } else {
        var oldOnLoad = window.onload;
        window.onload = function () {
            oldOnLoad();
            initialize();
        };
    }

    var _messageReceiver;

    function initialize() {
        _messageReceiver = new WebViewerMessageReceiver();

        if (window.opener) {
            window.opener.window.postMessage(WebViewerCommandNames.ALIVE, "*");
        }
    }

    var WebViewerMessageReceiver = (function () {
        function WebViewerMessageReceiver() {
            LogUtils.DebugLog("WebViewerMessageReceiver Constructor called");
            window.onmessage = this.onMessageReceived;

            var injector = angular.element(document.getElementById('app')).injector();
            WebViewerMessageReceiver._eventService = injector.get('eventService');
            WebViewerMessageReceiver._dataService = injector.get('dataService');
            WebViewerMessageReceiver._externalCommandHandlerService = injector.get('externalCommandHandlerService');
            WebViewerMessageReceiver._seriesManagerService = injector.get('seriesManagerService');
        }
        // Create a command to pass on to ExternalWebViewerDControllerProxy.ProcessCommand
        WebViewerMessageReceiver.CreateCommand = function (commandName) {
            var myArgs = [];
            for (var _i = 0; _i < (arguments.length - 1); _i++) {
                myArgs[_i] = arguments[_i + 1];
            }
            var command = new CommandClass();
            command.Args = [];

            if (arguments.length == 0) {
                return command;
            }

            command.Name = commandName;

            for (var i = 0; i < myArgs.length; i++) {
                command.Args[i] = myArgs[i];
            }
            return command;
        };

        WebViewerMessageReceiver.prototype.onMessageReceived = function (args) {
            var cmd = JSON.parse(args.data);
            var message = new WebViewerMessage();

            message.Parse(args.data);
            switch (cmd.name) {
                case ExternalCommandNames.ShowPatient:
                case ExternalCommandNames.ShowStudy:
                case ExternalCommandNames.ShowSeries:
                case ExternalCommandNames.ShowInstance:
                case ExternalCommandNames.GetImage:
                     {
                        var command;
                        command = WebViewerMessageReceiver.CreateCommand(cmd.name, cmd.arg1);
                        WebViewerMessageReceiver._externalCommandHandlerService.ProcessCommand(command);
                        Controllers.MedicalWebViewerController;
                    }
                    break;

                case ExternalCommandNames.GetCurrentPatient:
                     {
                        var command;
                        command = WebViewerMessageReceiver.CreateCommand(cmd.name);
                        WebViewerMessageReceiver._externalCommandHandlerService.ProcessCommand(command);
                    }
                    break;

                case WebViewerCommandNames.LOAD_SERIES_EXT:
                     {
                        var seriesArray = JSON.parse(cmd.seriesArray);
                        var seriesSelected = JSON.parse(cmd.seriesSelected);
                        WebViewerMessageReceiver.seriesLoaderExt(seriesArray, seriesSelected);
                    }
                    ;
                    break;

                case WebViewerCommandNames.SET_SERIES_VIEWER_MODE:
                     {
                        var params = message.get_Params();
                        SetSeriesViewerMode(cmd.args.mode, -1);
                    }
                    break;

                case WebViewerCommandNames.SUBSCRIBE:
                     {
                        WebViewerMessageReceiver.EventsDispatcher(args.source, message);
                    }
                    break;

                default:
                    alert("Unknown message received");
            }
        };

        WebViewerMessageReceiver.seriesLoaderExt = function (seriesArray, seriesInstance) {
            WebViewerMessageReceiver._dataService.set_Series(seriesArray);
            WebViewerMessageReceiver._seriesManagerService.currentLoadingSeries = seriesInstance.InstanceUID;
            WebViewerMessageReceiver._eventService.publish(EventNames.SeriesSelected, { study: seriesArray, series: seriesInstance });
        };

        WebViewerMessageReceiver.EventsDispatcher = function (receiver, message) {
            var params = message.get_Params();

            var loadSeries = params[SubscribeToEventsCmdParams.PARAM_LoadSeries];
            var presentationStateCreated = params[SubscribeToEventsCmdParams.PARAM_PresentationStateCreated];
            var presentationStateDeleted = params[SubscribeToEventsCmdParams.PARAM_PresentationStateDeleted];
            var derivedImageCreated = params[SubscribeToEventsCmdParams.PARAM_DerivedImageCreated];
            var imageExported = params[SubscribeToEventsCmdParams.PARAM_ImageExported];

            if (loadSeries) {
                EventBroker.add_seriesLoaded(onSeriesLoaded);
            }

            if (presentationStateCreated) {
                EventBroker.add_presentationStateCreated(onPresentationStateCreated);
            }

            if (presentationStateDeleted) {
                EventBroker.add_presentationStateDeleted(onPresentationStateDeleted);
            }

            if (derivedImageCreated) {
                EventBroker.add_derivedImageCreated(onDerivedImageCreated);
            }

            if (imageExported) {
                EventBroker.add_imageExported(onImageExported);
            }

            function onSeriesLoaded(args, seriesInstanceUID) {
                receiver.postMessage("Series Loaded: Series UID=" + seriesInstanceUID, "*");
            }

            function onPresentationStateCreated(args, annId) {
                receiver.postMessage("Presentation State Created: SOP UID=" + annId.SOPInstanceUID, "*");
            }

            function onPresentationStateDeleted(args, annId) {
                receiver.postMessage("Presentation State Deleted: SOP UID=" + annId.SOPInstanceUID, "*");
            }

            function onDerivedImageCreated(args, sopInstance) {
                receiver.postMessage("Derived Image Created: SOP UID=" + sopInstance, "*");
            }

            function onImageExported(args, sopInstance) {
                receiver.postMessage("Image Exported: SOP UID=" + sopInstance, "*");
            }
        };
        return WebViewerMessageReceiver;
    })();
    ViewerMessageRecieverModule.WebViewerMessageReceiver = WebViewerMessageReceiver;
})(ViewerMessageRecieverModule || (ViewerMessageRecieverModule = {})); // Module
//# sourceMappingURL=WebViewerMessageReceiver.js.map
