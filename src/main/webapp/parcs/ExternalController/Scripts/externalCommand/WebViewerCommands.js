﻿var WebViewerCommandNames;
(function (WebViewerCommandNames) {
    WebViewerCommandNames.LOAD_SERIES = "LoadSeriesCommand";
    WebViewerCommandNames.LOAD_SERIES_EXT = "LoadSeriesCommandExt";
    WebViewerCommandNames.SET_SERIES_VIEWER_MODE = "SetSeriesViewerModeCommand";
    WebViewerCommandNames.ALIVE = "ALIVE";
    WebViewerCommandNames.SUBSCRIBE = "SubscribeToEventsCommand";
})(WebViewerCommandNames || (WebViewerCommandNames = {}));
;

var WebViewerMessage = (function () {
    function WebViewerMessage() {
        this._params = [];
    }
    WebViewerMessage.prototype.get_Command = function () {
        return this._cmd;
    };

    WebViewerMessage.prototype.get_Params = function () {
        return this._params;
    };

    WebViewerMessage.prototype.Parse = function (msgData) {
        var commandParams = msgData.split("?");

        if (commandParams.length === 2) {
            this._cmd = commandParams[0];
            this._params = WebViewerMessage.ParseQueryString(msgData);
        }
    };

    WebViewerMessage.SendCommand = function (cmd, receiver, url) {
        receiver.postMessage(cmd.toString(), url);
    };

    WebViewerMessage.ParseQueryString = function (url) {
        var query = url.split("?");
        var queryParams = [];

        if (query && query.length == 2) {
            query = query[1].split("&");
        } else {
            return queryParams;
        }

        if (query.length == 1 && query[0] === "") {
            return queryParams;
        }

        var length = query.length;

        while (length--) {
            var keyValue = query[length].split("=");

            var key = decodeURIComponent(keyValue[0]);
            var value = decodeURIComponent(keyValue[1]);

            if (key && key.length) {
                queryParams[key] = value;
            }
        }

        return queryParams;
    };
    return WebViewerMessage;
})();
//# sourceMappingURL=WebViewerCommands.js.map
