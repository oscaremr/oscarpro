﻿/// <reference path="../Services/QueryArchiveService.ts" />
/// <reference path="../Services/ObjectRetrieveService.ts" />
/// <reference path="../Services/ObjectStoreService.ts" />
/// <reference path="../Services/PatientService.ts" />
/// <reference path="WebViewerCommands.ts" />
/// <reference path="../Controllers/Scopes.ts" />
/// <reference path="../Models/QueryOptions.ts" />
var WebViewerCommandHandlerService = (function () {
    function WebViewerCommandHandlerService(authenticationService, webViewerAuthenticationToken, viewerWindow) {
        var injector = angular.element(document.getElementById('app')).injector();
        this._queryArchiveService = injector.get('queryArchiveService');
        this._objectRetrieveService = injector.get('objectRetrieveService');
        this._objectStoreService = injector.get('objectStoreService');
        this._patientService = injector.get('patientService');
        this._optionsService = injector.get('optionsService');

        this._authenticationToken = webViewerAuthenticationToken;
        this._authenticationService = authenticationService;
        this._authenticationService.authenticationCode = webViewerAuthenticationToken;

        this._viewerWindow = viewerWindow;
    }
    WebViewerCommandHandlerService.prototype.SendCommand = function (cmd, receiver, url) {
        receiver.postMessage(JSON.stringify(cmd), url);
    };

    WebViewerCommandHandlerService.prototype.Authenticate = function (errorHandler, successHandler) {
        //var hostIP = "http://10.28.75.196/";
    	var hostIP = hostIP1;
        var serviceUrl = hostIP + "MedicalViewerService19/AuthenticationService.svc/AuthenticateUser";
        var parameters = { userName: userName, password: password, userData: null };

        return $.ajax({
            type: "POST",
            contentType: "application/json",
            url: serviceUrl,
            data: JSON.stringify(parameters),
            error: errorHandler,
            success: successHandler
        });
    };

    WebViewerCommandHandlerService.prototype.LogOut = function () {
        this._authenticationService.logout();
        this.Close();
    };

    WebViewerCommandHandlerService.prototype.Close = function () {
        if (this._viewerWindow === window) {
            var win = window.open("", "_self");
            win.close();
        } else if (this._viewerWindow) {
            this._viewerWindow.close();
        }
    };

    WebViewerCommandHandlerService.prototype.isDental = function () {
        return this._optionsService.get(OptionNames.DentalMode);
    };

    WebViewerCommandHandlerService.prototype.FindPatient = function (patientID, findPatientOptions, errorHandler, successHandler) {
        var _this = this;
        var options = new Models.QueryOptions();

        options.PatientsOptions.PatientID = patientID;
        options.PatientsOptions.PatientName = "";
        options.StudiesOptions.AccessionNumber = "";
        options.StudiesOptions.ReferDoctorName = "";
        options.StudiesOptions.ModalitiesInStudy = new Array();

        if (findPatientOptions === "All") {
            this._queryArchiveService.FindPatients(options).then(successHandler, errorHandler);
        } else {
            var maxStudies = this._optionsService.get(OptionNames.MaxStudyResults);

            this._queryArchiveService.FindStudies(options, maxStudies).then(function (e) {
                _this.onSearchStudiesError(e);
                errorHandler(e);
            }, $.proxy(successHandler, this));
        }
    };

    WebViewerCommandHandlerService.prototype.FindPatientFromSeries = function (seriesInstanceUID, findPatientOptions, errorHandler, successHandler) {
        var _this = this;
        var queryParams = new Models.QueryOptions();
        queryParams.SeriesOptions.SeriesInstanceUID = seriesInstanceUID;

        if (findPatientOptions === "All") {
            this._queryArchiveService.FindPatients(queryParams).then(successHandler, errorHandler);
        } else {
            var maxStudies = this._optionsService.get(OptionNames.MaxStudyResults);

            this._queryArchiveService.FindStudies(queryParams, maxStudies).then(function (e) {
                _this.onSearchStudiesError(e);
                errorHandler(e);
            }, $.proxy(successHandler, this));
        }
    };

    WebViewerCommandHandlerService.prototype.UpdatePatient = function (patientInfo, errorHandler, successHandler) {
        var _this = this;

        this._patientService.UpdatePatient(patientInfo).then(successHandler, function (e) {
            _this.onUpdatePatientError(e);
            errorHandler(e);
        });
    };

    WebViewerCommandHandlerService.prototype.DeletePatient = function (patientId, errorHandler, successHandler) {
        var _this = this;
        this._patientService.DeletePatient(patientId).then(successHandler, function (e) {
            _this.onDeletePatientError(e);
            errorHandler(e);
        });
    };

    WebViewerCommandHandlerService.prototype.AddPatient = function (patientInfo, errorHandler, successHandler) {
        var _this = this;
        this._patientService.AddPatient(patientInfo).then(successHandler, function (e) {
            _this.onAddPatientError(e);
            errorHandler(e);
        });
    };

    WebViewerCommandHandlerService.prototype.onSearchStudiesSuccess = function (studies) {
        LogUtils.DebugLog(studies);
    };

    WebViewerCommandHandlerService.prototype.onSearchStudiesError = function (/*xhr,*/ textStatus /*, ex*/ ) {
        LogUtils.DebugLog("Failed to find series: " + textStatus);
    };

    WebViewerCommandHandlerService.prototype.onUpdatePatientError = function (textStatus) {
        LogUtils.DebugLog("Failed to update patient: " + textStatus);
    };

    WebViewerCommandHandlerService.prototype.onDeletePatientError = function (textStatus) {
        LogUtils.DebugLog("Failed to delete patient: " + textStatus);
    };

    WebViewerCommandHandlerService.prototype.onAddPatientError = function (textStatus) {
        LogUtils.DebugLog("Failed to add patient: " + textStatus);
    };

    WebViewerCommandHandlerService.prototype.onAuthenticationError = function (xhr, status, ex) {
        alert("failed to authenticate user: " + ex);
    };

    WebViewerCommandHandlerService.prototype.onAuthenticationSuccess = function (authentication) {
        alert("authentication success");

        this._authenticationToken = authentication;
    };

    WebViewerCommandHandlerService.prototype.onFindStudiesError = function (xhr, status, ex) {
        alert("Failed to query for studies: " + ex);
    };

    WebViewerCommandHandlerService.FilterPresentationState = function (series) {
        var removedPresentationStateInstanceUIDs = [];
        var length = series.length;

        while (length--) {
            if (series[length].Modality == "PR") {
                var prSeries = series.splice(length, 1);
                prSeries = prSeries[0];
                removedPresentationStateInstanceUIDs.push(prSeries.InstanceUID);
            }
        }
        return removedPresentationStateInstanceUIDs;
    };

    WebViewerCommandHandlerService.prototype.FilterSeriesInstances = function (instances, seriesInstanceUIDs) {
        var length = instances.length;

        while (length--) {
            if (seriesInstanceUIDs.indexOf(instances[length].SeriesInstanceUID) != -1) {
                instances.splice(length, 1);
            }
        }
    };

    WebViewerCommandHandlerService.prototype.FindSeriesExt = function (patientID, studyInstanceUID, seriesInstanceUID, errorHandler, successHandler) {
        var queryOptions = new Models.QueryOptions();
        var maxSeries = this._optionsService.get(OptionNames.MaxStudyResults);

        queryOptions.PatientsOptions.PatientID = patientID;
        queryOptions.StudiesOptions.StudyInstanceUID = studyInstanceUID;
        queryOptions.SeriesOptions.SeriesInstanceUID = seriesInstanceUID;

        this._queryArchiveService.FindSeries(queryOptions, maxSeries).then(function (series) {
            //Filter out the presentation state instances
            WebViewerCommandHandlerService.FilterPresentationState(series.data);
            if (successHandler !== null) {
                successHandler(series);
            }
        }, errorHandler);
    };

    WebViewerCommandHandlerService.prototype.GetInstanceImageURL = function (sopInstanceUID, successHandler) {
        var frame = {};
        frame.FrameNumber = 1;
        frame.Instance = {};
        frame.Instance.SOPInstanceUID = sopInstanceUID;

        var url = this._objectRetrieveService.GetImageUrl(frame, 1024, 1024);
        if (successHandler != null) {
            successHandler(url);
        }
    };

    WebViewerCommandHandlerService.prototype.ShowSeriesInstanceExt = function (seriesArray, seriesInstance, completed) {
        var loadSeriesCommandExt = {};
        loadSeriesCommandExt.name = WebViewerCommandNames.LOAD_SERIES_EXT;

        loadSeriesCommandExt.seriesArray = JSON.stringify(seriesArray);
        loadSeriesCommandExt.seriesSelected = JSON.stringify(seriesInstance);

        this.SendCommand(loadSeriesCommandExt, this._viewerWindow, "*");
        if (completed != null) {
            completed();
        }
    };

    WebViewerCommandHandlerService.prototype.SetSeriesViewerMode = function (seriesViewerMode) {
        //var setSeriesViewerModeCommand = new SetSeriesViewerModeCommand(seriesViewerMode);
        //SendCommand(setSeriesViewerModeCommand, webViewerControllerInstance.ViewerWindow, "*");
    };
    return WebViewerCommandHandlerService;
})();
//# sourceMappingURL=WebViewerCommandHandlerService.js.map
