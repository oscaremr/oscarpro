<!DOCTYPE html>
<html>
<head>
    <title>CharTech External WebViewer Controller Demo</title>

    <link rel="stylesheet" type="text/css" href="StyleSheet.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery-ui.css" />
    <style type='text/css'></style>
    <%@ page import="oscar.OscarProperties" %>
    <%@ page import="java.util.Properties" %>
    <%@ page import="java.util.Map" %>
    <%@ page import="java.util.HashMap" %>
    
<%
Properties oscarVariables = OscarProperties.getInstance();
String parc_ip = oscarVariables.getProperty("parcs_ip");

//parcs_login=siteId1:pacsUsername1:password1|siteId2:pacsUsername2:password2|siteId3:pacsUsername3:password3|siteId4:pacsUsername4:password4
//parcs_login=1:oscartest:00OSCARTEST!|2:aaaa2:bbbb2|3:aaaa3:bbbb3|4:aaaa4:bbbb4
String parcs_login = oscarVariables.getProperty("parcs_login");
			
String[] pacs = parcs_login.split("\\|");
Map<String, String> SiteId_Username_Map = new HashMap<String, String>();
Map<String, String> SiteId_Password_Map = new HashMap<String, String>();

for (String p : pacs) {
	String[] pacs_info = p.split(":");
	if (pacs_info.length == 3) {
		SiteId_Username_Map.put(pacs_info[0], pacs_info[1]);		
		SiteId_Password_Map.put(pacs_info[0], pacs_info[2]);	
	}
}
String site_id = request.getParameter("site");
String parcs_username = SiteId_Username_Map.get(site_id);
String parcs_pw = SiteId_Password_Map.get(site_id);
//String parcs_username = oscarVariables.getProperty("parcs_username");
//String parcs_pw = oscarVariables.getProperty("parcs_password");

String demographicNo = request.getParameter("demographicNo");

%>
<script>
var demoNo = "<%=demographicNo %>";
var hostIP1 = "<%=parc_ip %>";
var globalUserName1 = "<%= parcs_username%>";
var globalUserPassword1 = "<%= parcs_pw%>";
</script>
    <script src="Scripts/externalCommand/ExternalCommands.js" type="text/javascript"></script>

    <script src="ServiceProxy/ServiceProxy.js" type="text/javascript"></script>
    <script src="ServiceProxy/AuthenticationServiceProxy.js" type="text/javascript"></script>
    <script src="ServiceProxy/QueryArchiveServiceProxy.js" type="text/javascript"></script>
    <script src="ServiceProxy/PatientServiceProxy.js" type="text/javascript"></script>
    
<script src="JS/logger.js" type="text/javascript"></script>
    <script src="JS/viewInstances.js" type="text/javascript"></script>
    <script src="JS/updatePatient.js" type="text/javascript"></script>
    <script src="JS/addPatient.js" type="text/javascript"></script>
    <script src="JS/updateUser.js" type="text/javascript"></script>
    <script src="JS/addUser.js" type="text/javascript"></script>
    <script src="JS/myMain.js" type="text/javascript"></script>


</head>
<body style="padding: 5px;">
   <script src="Scripts/jquery/jquery-1.8.2.js" type="text/javascript"></script>
   <script src="Scripts/jquery/jquery-ui.js" type="text/javascript"></script>

    <div>
        <div class="DialogTextButtonsRow">
            <div class="DialogTextButtonsCell">
                <div><input type="button" id="idButton_viewInstances_showPatient" value="Show All studies"/></div>
            </div>
        </div>
        
        <div class="DialogTextButtonsRow">
            <div class="DialogTextButtonsCell">
                <div><input type="button" id="idButton_viewInstances_showStudy" value="Show Study"/></div>
            </div> 
            
            <div class="DialogTextButtonsCell">
                <div><label for="idDropDownList_viewInstances_study">Study Instance UID</label></div>
                <div> <select id="idDropDownList_viewInstances_study" class="selectFixedWidth" aria-sort="none"></select></div>
            </div>
        </div>

        <div class="DialogTextButtonsRow">
            <div class="DialogTextButtonsCell">
                <div><input type="button" id="idButton_viewInstances_showSeries" value="Show Series"/></div>
            </div> 
            <div class="DialogTextButtonsCell">
                <div><label for="idDropDownList_viewInstances_series">Series Instance UID</label></div>
                <div><select id="idDropDownList_viewInstances_series" class="selectFixedWidth"></select></div>
            </div>
           
        </div>
    </div>
<!-- Do not need thumbnail images shown here in the echart. But we need it in rich text eform -->
<div style="display:none">
    <table id="ucPACSThumbnail">
    </table>
</div>
    <div id="idLogControl" class="classLogControl">
        <div id="idLogger" class="classLogger"></div>
        <div><input type="button" id="idButtonClearLog" value="Clear Log" /></div>
    </div>
  
</body>


</html>

