/**
 * Copyright (c) 2024 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package health.apps.gateway;

public interface LinkData {
  String getRemoteSystemId();
  String getGuid();
  boolean isLinked();
  boolean isRemote();
}
