/**
 * Copyright (c) 2024 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package health.apps.gateway.service;

import health.apps.gateway.LinkData;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;

/**
 * GatewayDao interface
 */
public interface GatewayDao {

  Bundle findAllRemoteByDemographicId(
      Class<? extends IBaseResource> resource,
      LinkData parent
  ) throws Exception;

  IBaseResource findRemoteById(
      Class<? extends IBaseResource> resource,
      LinkData parent,
      String identifier
  ) throws Exception;

  IBaseResource saveRemote(
      LinkData parent,
      IBaseResource resource
  );
}
