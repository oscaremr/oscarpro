/**
 * Copyright (c) 2024 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package health.apps.gateway.service;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.SearchStyleEnum;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.ServerValidationModeEnum;
import ca.uhn.fhir.rest.client.interceptor.AdditionalRequestHeadersInterceptor;
import ca.uhn.fhir.rest.gclient.StringClientParam;
import health.apps.gateway.LinkData;
import java.util.function.Supplier;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Slf4j
@Repository
public class GatewayDaoImpl implements GatewayDao {
  private final AppsHealthGatewayConfigurationService configurationService;
  private final StringEncryptionService stringEncryptionService;
  private final Supplier<FhirContext> fhirContextSupplier;
  private FhirContext fhirContext;

  @Autowired
  public GatewayDaoImpl(
      final AppsHealthGatewayConfigurationService configurationService,
      final StringEncryptionService stringEncryptionService,
      final Supplier<FhirContext> fhirContextSupplier
  ) {
    this.fhirContextSupplier = fhirContextSupplier;
    this.configurationService = configurationService;
    this.stringEncryptionService = stringEncryptionService;
  }

  private void initialize() {
    fhirContext = fhirContext == null ? fhirContextSupplier.get() : fhirContext;
    val factory = fhirContext.getRestfulClientFactory();
    factory.setConnectTimeout(3 * 60 * 1000);
    factory.setSocketTimeout(3 * 60 * 1000);
    // Disable validation call to /metadata  -- do we implement?
    factory.setServerValidationMode(ServerValidationModeEnum.NEVER);
  }

  @Override
  public Bundle findAllRemoteByDemographicId(
      final Class<? extends IBaseResource> resource,
      final LinkData parent
  ) {
    if (!isValidParentAndConfiguration(parent)) {
      return null;
    }
    val client = configureClient(parent.getRemoteSystemId());
    return client.search()
        .forResource(resource)
        .usingStyle(SearchStyleEnum.GET)
        .where(new StringClientParam("$demographic").matches().values(parent.getGuid()))
        .returnBundle(Bundle.class)
        .execute();
  }

  @Override
  public IBaseResource findRemoteById(
      final Class<? extends IBaseResource> resource, LinkData parent,
      final String identifier
  ) {
    if (!isValidParentAndConfiguration(parent)) {
      return null;
    }
    if (identifier == null) {
      log.info("Unable to find with no identifier");
      return null;
    }
    val client = configureClient(parent.getRemoteSystemId());
    return client.read()
        .resource(resource)
        .withId(identifier)
        .execute();
  }

  @Override
  public IBaseResource saveRemote(LinkData parent, IBaseResource resource) {
    if (!isValidParentAndConfiguration(parent)) {
      return null;
    }
    val client = configureClient(parent.getRemoteSystemId());
    val outcome = client.create()
        .resource(resource)
        .encodedJson()
        .execute();

    if (outcome.getCreated()) {
      val operationOutcome = outcome.getOperationOutcome();
      log.info("Created over gateway : {}", operationOutcome.getIdElement());
      return outcome.getResource();
    } else {
      log.info("Create over gateway failed: {}", outcome.getOperationOutcome());
      return null;
    }
  }

  private boolean isValidParentAndConfiguration(
      final LinkData parent
  ) {
    return failFast(parent != null, "Unable to find: parent is null")
        && failFast(parent.isLinked(), "Unable to find: parent is not linked")
        && failFast(configurationService.isGatewayEnabled(),
        "Unable to find: gateway is not enabled")
        && failFast(parent.getGuid() != null, "Unable to find: parent guid invalid")
        && failFast(parent.getRemoteSystemId() != null,
        "Unable to find: parent remote-system-id invalid");
  }

  private static boolean failFast(boolean condition, String message) {
    if (!condition) {
      log.info(message);
      return false;
    }
    return true;
  }

  private IGenericClient configureClient(
      final String targetSystemId
  ) {
    if (fhirContext == null) {
      initialize();
    }
    val systemId = configurationService.getSystemId();
    val systemKey = configurationService.getSystemKey();
    val baseUrl = getBaseUrl().replaceAll(":system-id:", targetSystemId);
    val client = fhirContext.newRestfulGenericClient(baseUrl);
    // Configure Headers
    val interceptor = new AdditionalRequestHeadersInterceptor();
    interceptor.addHeaderValue("Version", "2.0");
    interceptor.addHeaderValue("X-Target-System-ID", targetSystemId);
    interceptor.addHeaderValue("X-System-ID", systemId);
    interceptor.addHeaderValue("X-System-Key", stringEncryptionService.decrypt(systemKey));
    client.registerInterceptor(interceptor);
    return client;
  }

  private String getBaseUrl() {
    return configurationService.getUrl() + configurationService.getFhirPath();
  }
}


