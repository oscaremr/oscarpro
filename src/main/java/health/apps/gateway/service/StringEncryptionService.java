/**
 * Copyright (c) 2024 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package health.apps.gateway.service;

import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.stereotype.Service;
import oscar.OscarProperties;

@Service
public class StringEncryptionService {

  public StringEncryptionService() {
    val oscarProperties = OscarProperties.getInstance();
    this.aesPassword = oscarProperties.getProperty("security.encryption.aes.password");
    this.aesSalt = oscarProperties.getProperty("security.encryption.aes.salt");
  }

  private String aesPassword;

  private String aesSalt;

  private TextEncryptor aesTextEncryptor;

  public String decrypt(final String systemKey) {
    initAesEncryptorIfNeeded();
    return aesTextEncryptor.decrypt(systemKey);
  }

  private void initAesEncryptorIfNeeded() {
    if (aesTextEncryptor != null) {
      return;
    }
    if (StringUtils.isAnyBlank(aesPassword, aesSalt)) {
      throw new IllegalStateException("AES password or salt is not set");
    }
    aesTextEncryptor = Encryptors.delux(aesPassword, aesSalt);
  }
}
