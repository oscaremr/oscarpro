/**
 * Copyright (c) 2024 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package health.apps.gateway.service;

import org.oscarehr.common.dao.SystemPreferencesDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import oscar.OscarProperties;

@Service
public class AppsHealthGatewayConfigurationService {

  private final SystemPreferencesDao systemPreferencesDao;
  public static final String PROP_GATEWAY_SYSTEM_ID = "apps_health.gateway.system_id";
  public static final String PROP_GATEWAY_SYSTEM_KEY= "apps_health.gateway.system_key";
  public static final String PROP_GATEWAY_URL = "apps_health.gateway_url";
  public static final String PROP_GATEWAY_FHIR_PATH = "apps_health.gateway.fhir_path";

  @Autowired
  public AppsHealthGatewayConfigurationService(SystemPreferencesDao systemPreferencesDao) {
    this.systemPreferencesDao = systemPreferencesDao;
  }

  public boolean isGatewayEnabledInProperties() {
    return OscarProperties.getInstance().getBooleanProperty("apps_health.gateway.enabled", "true");
  }

  public boolean isGatewayEnabledInSystemPreferences() {
    return systemPreferencesDao.isReadBooleanPreference("apps_health.gateway.enabled");
  }

  public String getFhirPath() {
    return OscarProperties.getInstance()
        .getProperty(PROP_GATEWAY_FHIR_PATH, "/system/:system-id:/fhir");
  }

  public String getSystemId() {
    return systemPreferencesDao.getPreferenceValueByName(PROP_GATEWAY_SYSTEM_ID, "");
  }

  public String getSystemKey() {
    return systemPreferencesDao.getPreferenceValueByName(PROP_GATEWAY_SYSTEM_KEY, "");
  }

  public String getUrl() {
    return systemPreferencesDao.getPreferenceValueByName(PROP_GATEWAY_URL, "");
  }

  public boolean isGatewayEnabled() {
    return isGatewayEnabledInProperties() && isGatewayEnabledInSystemPreferences();
  }
}