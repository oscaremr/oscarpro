/**
 * Copyright (c) 2024 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package health.apps.gateway.service;

import ca.uhn.fhir.context.FhirContext;
import java.util.function.Supplier;

public class FhirContextSupplierForR4 implements Supplier<FhirContext> {

  /**
   * A FhirContext is expensive to create, so we should store it once created.
   * https://hapifhir.io/hapi-fhir/docs/client/generic_client.html#generic-fluent-client
   */
  private FhirContext fhirContext = null;
  @Override
  public FhirContext get() {
    return fhirContext == null ? fhirContext = FhirContext.forR4() : fhirContext;
  }
}
