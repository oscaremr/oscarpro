/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.progressSheet.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class FormFieldPK implements Serializable {
	
	private static final long serialVersionUID = 9079805981190193948L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "form_id", nullable=false)
	private ProgressSheet progressSheet;
	
	@Column(name="field_key")
	private String fieldKey;
	
	public FormFieldPK() {
	}
	public FormFieldPK(String fieldKey) {
		this.fieldKey = fieldKey;
	}

	public ProgressSheet getProgressSheet() {
		return progressSheet;
	}

	public void setProgressSheet(ProgressSheet progressSheet) {
		this.progressSheet = progressSheet;
	}

	public String getFieldKey() {
		return fieldKey;
	}

	public void setFieldKey(String fieldKey) {
		this.fieldKey = fieldKey;
	}

	public int hashCode() {
		return (int) fieldKey.hashCode() + (int) progressSheet.getFormId();
	}

	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (obj == this) return true;
		if (!(obj instanceof FormFieldPK)) return false;
		FormFieldPK pk = (FormFieldPK) obj;
		return this.hashCode() == pk.hashCode();
	}
}
