/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.progressSheet.model;

import org.json.JSONObject;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@Table(name="progress_sheet")
public class ProgressSheet {

	@Id
	@Column(name="form_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long formId;
	
	@Column(name="demographic_id")
	private int demographicId;

	//Provider that saved the progress sheet
	@Column(name="provider_id")
	private String providerId;

	//Provider for which the appointment the progress sheet is for
	@Column(name="appointment_provider_id")
	private String appointmentProviderId;

	@Column(name="appointment_id")
	private int appointmentId;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "primaryKey.progressSheet", cascade = CascadeType.ALL)
	private List<FormField> formFields;

	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="form_id")
	private PainDiagram painDiagram;
	
	@Column(name = "demographic_name")
	private String demographicName;

	@Column(name = "demographic_dob")
	private String demographicDob;

	@Column(name = "demographic_address")
	private String demographicAddress;

	@Column(name="signature_file")
	private String signatureFile;

	@Column(name="encounter_date")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date encounterDate;
	
	public ProgressSheet() { super(); } //required hibernate constructor
	public ProgressSheet(int demographicId, int appointmentId, String providerId, List<FormField> formFields, String demographicName, String demographicDob, String demographicAddress, Date encounterDate, PainDiagram painDiagram) {
		super();
		this.demographicId = demographicId;
		this.appointmentId = appointmentId;
		this.providerId = providerId;
		this.formFields = formFields;
		this.demographicName = demographicName;
		this.demographicDob = demographicDob;
		this.demographicAddress = demographicAddress;
		this.encounterDate = encounterDate;
		this.painDiagram = painDiagram;
	}
	public ProgressSheet(int demographicId, int appointmentId, String providerId, List<FormField> formFields, String demographicName, String demographicDob, String demographicAddress, Date encounterDate, PainDiagram painDiagram, String signatureFilePath) {
		this(demographicId, appointmentId, providerId, formFields, demographicName, demographicDob, demographicAddress, encounterDate, painDiagram);
		this.signatureFile = signatureFilePath;
	}

	public void setFormId(long formId) {
		this.formId = formId;
	}

	public long getFormId() {
		return formId;
	}

	public int getDemographicId() {
		return demographicId;
	}

	public void setDemographicId(int demographicId) {
		this.demographicId = demographicId;
	}

	public String getProviderId() {
		return providerId;
	}
	
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public String getAppointmentProviderId() {
		return appointmentProviderId;
	}

	public void setAppointmentProviderId(String appointmentProviderId) {
		this.appointmentProviderId = appointmentProviderId;
	}

	public int getAppointmentId() {
		return appointmentId;
	}
	
	public void setAppointmentId(int appointment) {
		this.appointmentId = appointment;
	}

	public List<FormField> getFormFields() {
		return formFields;
	}

	public void setFormFields(List<FormField> formFields) {
		this.formFields = formFields;
	}
	
	public Map<String, String> getFormFieldMap() {
		Map<String, String> fieldsMap = new HashMap<String, String>();
		for (FormField field : this.formFields) {
			fieldsMap.put(field.getFieldKey(), field.getFieldValue());
		}
		return fieldsMap;
	}

	public String getDemographicName() {
		return demographicName;
	}
	public void setDemographicName(String demographicName) {
		this.demographicName = demographicName;
	}

	public String getDemographicDob() {
		return demographicDob;
	}
	public void setDemographicDob(String demographicDob) {
		this.demographicDob = demographicDob;
	}

	public String getDemographicAddress() {
		return demographicAddress;
	}
	public void setDemographicAddress(String demographicAddress) {
		this.demographicAddress = demographicAddress;
	}

	public Date getEncounterDate() {
		return encounterDate;
	}
	public void setEncounterDate(Date encounterDate) {
		this.encounterDate = encounterDate;
	}
	
	public PainDiagram getPainDiagram(){
		return painDiagram;
	}
	public void setPainDiagram(PainDiagram painDiagram) {
		this.painDiagram = painDiagram;
	}

	public String getSignatureFile() {
		return signatureFile;
	}
	public void setSignatureFile(String signatureFile) {
		this.signatureFile = signatureFile;
	}
	
	public JSONObject getAsJsonSimple() {
		JSONObject json = new JSONObject();
		json.put("formId", getFormId());
		json.put("demographicId", getDemographicId());
		json.put("appointmentId", getAppointmentId());
		json.put("encounterDate", getEncounterDate());
		return json;
	}
}
