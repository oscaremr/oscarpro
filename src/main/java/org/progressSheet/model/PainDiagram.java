/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.progressSheet.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="pain_diagram")
public class PainDiagram implements Serializable{

	private static final long serialVersionUID = 2595146514947386362L;
	public static final String NEW_SCALE_FACTOR = "72";

	@Id
	@Column(name = "form_id")
	private long formId;

	@Column(name="diagram")
	private String diagram;

	@Column(name="scaled_percent")
	private String scaledPercent = NEW_SCALE_FACTOR;
	
	public PainDiagram() { super(); } //required hibernate constructor
	public PainDiagram(long formId, String diagram, String scaledPercent) {
		super();
		this.formId = formId;
		this.diagram = diagram;
		this.scaledPercent = scaledPercent;
	}
	
	public long getFormId() {
		return formId;
	}
	public void setFormId(long formId) {
		this.formId = formId;
	}

	public String getDiagram() {
		return diagram;
	}

	public void setDiagram(String diagram) {
		this.diagram = diagram;
	}

	public String getScaledPercent() {
		return scaledPercent;
	}

	public void setScaledPercent(String scaledPercent) {
		this.scaledPercent = scaledPercent;
	}
}
