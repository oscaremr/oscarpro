/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.progressSheet.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class BillingFormFieldPK implements Serializable {
	
	private static final long serialVersionUID = 9079805981190193948L;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "form_id", nullable=false)
	private BillingSheet billingSheet;

	
	@Column(name="field_key")
	private String fieldKey;
	
	public BillingFormFieldPK() {
	}
	public BillingFormFieldPK(String fieldKey) {
		this.fieldKey = fieldKey;
	}

	public BillingSheet getBillingSheet() {
		return billingSheet;
	}
	public void setBillingSheet(BillingSheet billingSheet) {
		this.billingSheet = billingSheet;
	}

	public String getFieldKey() {
		return fieldKey;
	}
	public void setFieldKey(String fieldKey) {
		this.fieldKey = fieldKey;
	}

	public int hashCode() {
		return (int) fieldKey.hashCode() + (int) billingSheet.getFormId();
	}

	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (obj == this) return true;
		if (!(obj instanceof BillingFormFieldPK)) return false;
		BillingFormFieldPK pk = (BillingFormFieldPK) obj;
		return this.hashCode() == pk.hashCode();
	}
}
