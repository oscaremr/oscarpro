/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.progressSheet.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name="billing_form_field")
public class BillingFormField implements Serializable {
	
	private static final long serialVersionUID = 4847803753268989851L;

	@EmbeddedId
	BillingFormFieldPK primaryKey;
	
	@Column(name="field_value")
	private String fieldValue;

	public BillingFormField() { super(); } //required hibernate constructor
	public BillingFormField(String fieldKey, String fieldValue) {
		super();
		this.primaryKey = new BillingFormFieldPK(fieldKey);
		this.fieldValue = fieldValue;
	}
	public BillingFormFieldPK getPrimaryKey() {
		return primaryKey;
	}
	
	public void setPrimaryKey(BillingFormFieldPK pk) {
		primaryKey = pk;
	}
	public void setPrimaryKeyBillingSheet(BillingSheet billingSheet) {
		primaryKey.setBillingSheet(billingSheet);
	}
	
	public String getFieldKey() {
		return primaryKey.getFieldKey();
	}
	public void setFieldKey(String fieldKey) {
		this.primaryKey.setFieldKey(fieldKey);
	}

	public String getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
}

