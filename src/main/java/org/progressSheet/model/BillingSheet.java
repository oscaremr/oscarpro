/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.progressSheet.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Entity
@Table(name="billing_sheet")
public class BillingSheet {

	@Id
	@Column(name="form_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long formId;

	@Column(name="progress_sheet_id")
	private Long progressSheetId;
	
	@Column(name="demographic_id")
	private int demographicId;
	
	@Column(name="provider_id")
	private String providerId;

	@Column(name="appointment_id")
	private int appointmentId;

	@Column(name="invoice_id")
	private Integer invoiceId;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "primaryKey.billingSheet", cascade = CascadeType.ALL)
	private List<BillingFormField> formFields;
	
	@Column(name="encounter_date")
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(style = "M-")
	private Date encounterDate;
	
	public BillingSheet() { super(); } //required hibernate constructor
	public BillingSheet(Long progressSheetId, int demographicId, String providerId, int appointmentId, int invoiceId, List<BillingFormField> formFields, Date encounterDate) {
		super();
		this.progressSheetId = progressSheetId;
		this.demographicId = demographicId;
		this.providerId = providerId;
		this.appointmentId = appointmentId;
		this.invoiceId = invoiceId;
		this.formFields = formFields;
		this.encounterDate = encounterDate;
	}

	public void setFormId(long formId) {
		this.formId = formId;
	}
	public long getFormId() {
		return formId;
	}

	public long getProgressSheetId() {
		return progressSheetId;
	}
	public void setProgressSheetId(long progressSheetId) {
		this.progressSheetId = progressSheetId;
	}

	public int getDemographicId() {
		return demographicId;
	}
	public void setDemographicId(int demographicId) {
		this.demographicId = demographicId;
	}
	
	public int getAppointmentId() {
		return appointmentId;
	}
	public void setAppointmentId(int appointment) {
		this.appointmentId = appointment;
	}

	public int getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(int invoiceId) {
		this.invoiceId = invoiceId;
	}

	public String getProviderId() {
		return providerId;
	}
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}

	public List<BillingFormField> getFormFields() {
		return formFields;
	}
	public void setFormFields(List<BillingFormField> formFields) {
		this.formFields = formFields;
	}
	public Map<String, BillingFormField> getBillingSheetFieldsMap() {
		Map<String, BillingFormField> results = new HashMap<String, BillingFormField>();
		for (BillingFormField formField : getFormFields()) {
			results.put(formField.getFieldKey(), formField);
		}
		return results;
	}
	
	public Map<String, Object> getFormFieldMap() {
		Map<String, Object> fieldsMap = new HashMap<String, Object>();
		for (BillingFormField field : this.formFields) {
			fieldsMap.put(field.getFieldKey(), field.getFieldValue());
		}
		fieldsMap.put("billingFormId", Long.toString(formId));
		fieldsMap.put("progressSheetId", Long.toString(progressSheetId));
		fieldsMap.put("appointmentId", Integer.toString(appointmentId));
		if (invoiceId != null && invoiceId != 0) {
			fieldsMap.put("invoiceId", Integer.toString(invoiceId));
		}
		return fieldsMap;
	}

	public Date getEncounterDate() {
		return encounterDate;
	}
	public void setEncounterDate(Date encounterDate) {
		this.encounterDate = encounterDate;
	}
}
