/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.progressSheet.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="drug_record")
public class DrugRecord {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private long id;
	@Column(name = "progress_sheet_form_id")
	private Long progressSheetFormId;
	@Column(name="oscar_drug_id")
	private Integer oscarDrugId;
	@Column(name="outline", length = 1000)
	private String outline;

	public DrugRecord() { super(); } //required hibernate constructor
	public DrugRecord(long progressSheetFormId, String outline) {
		this.progressSheetFormId = progressSheetFormId;
		this.outline = outline;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Long getProgressSheetFormId() {
		return progressSheetFormId;
	}

	public void setProgressSheetFormId(Long progressSheetFormId) {
		this.progressSheetFormId = progressSheetFormId;
	}

	public Integer getOscarDrugId() {
		return oscarDrugId;
	}

	public void setOscarDrugId(Integer oscarDrugId) {
		this.oscarDrugId = oscarDrugId;
	}

	public String getOutline() {
		return outline;
	}

	public void setOutline(String outline) {
		this.outline = outline;
	}
}

