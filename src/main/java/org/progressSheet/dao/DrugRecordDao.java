/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.progressSheet.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.progressSheet.model.DrugRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class DrugRecordDao {
	
	private static final Logger logger = LoggerFactory.getLogger(DrugRecord.class);
	
	private SessionFactory sessionFactory;
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Transactional
	public List<DrugRecord> findAllByProgressSheet(Long progressSheetFormId) {
		Session session = this.sessionFactory.getCurrentSession();
		Query queryObject = session.createQuery("FROM DrugRecord dr WHERE dr.progressSheetFormId = :progressSheetId ORDER BY dr.oscarDrugId DESC");
		queryObject.setParameter("progressSheetId", progressSheetFormId);
		return queryObject.list();
	}

	@Transactional
	public void addDrugRecords(List<DrugRecord> drugRecordList) {
		Session session = this.sessionFactory.getCurrentSession();
		for (DrugRecord drugRecord : drugRecordList) {
			session.persist(drugRecord);
		}
	}
}
