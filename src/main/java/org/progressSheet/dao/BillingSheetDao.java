/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.progressSheet.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.progressSheet.model.BillingFormField;
import org.progressSheet.model.BillingSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class BillingSheetDao {

	private static final Logger logger = LoggerFactory.getLogger(BillingSheetDao.class);
	
	@Autowired
	private BillingFormFieldDao billingFormFieldDAO;
	
	private SessionFactory sessionFactory;
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public BillingSheet addBillingSheet(BillingSheet bs) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(bs);

		List<BillingFormField> fields = bs.getFormFields();
		for (BillingFormField field : fields) {
			field.setPrimaryKeyBillingSheet(bs);
			billingFormFieldDAO.addFormField(field);
		}
		
		logger.info("Billing sheet saved successfully, Billing sheet Details=" + bs.getFormId());
		return bs;
	}
	
	public BillingSheet updateBillingSheet(BillingSheet bs) {
		Session session = this.sessionFactory.getCurrentSession();
		
		session.update(bs);
		logger.info("Billing sheet saved successfully, Billing sheet Details=" + bs.getFormId());
		return bs;
	}

	@Transactional
	public BillingSheet getBillingSheetById(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query =  session.createQuery("FROM BillingSheet b WHERE b.formId = :id ORDER BY b.encounterDate DESC, b.formId DESC");
		query.setParameter("id", id);

		List<BillingSheet> resultsList = query.list();
		if (!resultsList.isEmpty()) {
			logger.info("Billing Sheet loaded successfully, Billing Sheet details=" + resultsList.get(0).getFormId());
			return resultsList.get(0);
		} else {
			return null;
		}
	}

	@Transactional
	public BillingSheet getBillingSheetByAppointmentId(int appointmentId) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM BillingSheet b WHERE b.appointmentId = :appointmentId ORDER BY formId DESC");
		query.setParameter("appointmentId", appointmentId);
		List<BillingSheet> resultsList = query.list();
		if (!resultsList.isEmpty()) {
			return resultsList.get(0);
		}
		return null;
	}

	@Transactional
	public BillingSheet getBillingSheetByProgressSheetId(Long progressSheetId) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query =  session.createQuery("FROM BillingSheet b WHERE b.progressSheetId = :progressSheetId ORDER BY b.encounterDate DESC, b.formId DESC");
		query.setParameter("progressSheetId", progressSheetId);

		List<BillingSheet> resultsList = query.list();
		if (!resultsList.isEmpty()) {
			logger.info("Billing Sheet loaded successfully, Billing Sheet details=" + resultsList.get(0).getFormId());
			return resultsList.get(0);
		} else {
			return null;
		}
	}

	@Transactional
	public List<BillingSheet> getLatestBillingSheetsByAppointmentIds(List<Integer> appointmentIds) {
		Session session = this.sessionFactory.getCurrentSession();
		String sqll = "FROM BillingSheet bs " +
				"WHERE bs.progressSheetId IN (SELECT MAX(ps1.formId) FROM ProgressSheet ps1 WHERE ps1.appointmentId IN (:appointmentIds) GROUP BY ps1.appointmentId)";
		Query queryy = session.createQuery(sqll);
		queryy.setParameterList("appointmentIds", appointmentIds);
		List<BillingSheet> results = queryy.list();
		return results;
	}
}