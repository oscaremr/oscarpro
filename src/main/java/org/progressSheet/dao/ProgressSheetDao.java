/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.progressSheet.dao;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.progressSheet.model.ProgressSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public class ProgressSheetDao {

	private static final Logger logger = LoggerFactory.getLogger(ProgressSheetDao.class);
	
	private SessionFactory sessionFactory;
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public void addProgressSheet(ProgressSheet p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(p);
		logger.info("Progress sheet saved successfully, Progress sheet Details="+p.getAsJsonSimple());
	}
	
	public void updateProgressSheet(ProgressSheet p) {
		Session session = this.sessionFactory.getCurrentSession();
		
		session.update(p);
		logger.info("Progress sheet saved successfully, Progress sheet Details="+p.getAsJsonSimple());
	}

	@Transactional
	public ProgressSheet getProgressSheetById(Long id) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query =  session.createQuery("FROM ProgressSheet p WHERE p.formId = :formId ORDER BY p.encounterDate DESC, p.formId DESC");
		query.setParameter("formId", id);
		List<ProgressSheet> resultsList = query.list();
		ProgressSheet progressSheet = null;
		if (!resultsList.isEmpty()) {
			progressSheet = resultsList.get(0);
		}
		return progressSheet;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public ProgressSheet getNewestProgressSheetByDemographic(int demographicId) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query =  session.createQuery("FROM ProgressSheet p WHERE p.demographicId = :demographicId ORDER BY p.encounterDate DESC, p.formId DESC");
		query.setParameter("demographicId", demographicId);
		List<ProgressSheet> resultsList = query.list();
		ProgressSheet progressSheet = null;
		if (!resultsList.isEmpty()) {
			progressSheet = resultsList.get(0);
		}
		return progressSheet;
	}
	@Transactional
	@SuppressWarnings("unchecked")
	public ProgressSheet getPreviousProgressSheetByIdAndDemographic(long formId, int demographicId) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query =  session.createQuery("FROM ProgressSheet p WHERE p.demographicId = :demographicId" 
														+ " AND p.formId < :formId ORDER BY formId DESC");
		query.setMaxResults(1);
		query.setParameter("demographicId", demographicId);
		query.setParameter("formId", formId);
		List<ProgressSheet> resultsList = query.list();
		ProgressSheet progressSheet = null;
		if (!resultsList.isEmpty()) {
			progressSheet = resultsList.get(0);
		}
		return progressSheet;
		
	}
	@Transactional
	@SuppressWarnings("unchecked")
	public ProgressSheet getProgressSheetByAppointment(int appointmentId) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM ProgressSheet p WHERE p.appointmentId = :appointmentId ORDER BY formId DESC");
		query.setParameter("appointmentId", appointmentId);
		List<ProgressSheet> resultsList = query.list();
		ProgressSheet progressSheet = null;
		if (!resultsList.isEmpty()) {
			progressSheet = resultsList.get(0);
		}
		return progressSheet;
	}
	@Transactional
	@SuppressWarnings("unchecked")
	public ProgressSheet getProgressSheetByDemographicAndAppointment(int demographicId, int appointmentId) {
		Session session = this.sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM ProgressSheet p WHERE p.demographicId = :demographicId AND p.appointmentId = :appointmentId ORDER BY formId DESC");
		query.setParameter("demographicId", demographicId);
		query.setParameter("appointmentId", appointmentId);
		List<ProgressSheet> resultsList = query.list();
		ProgressSheet progressSheet = null;
		if (!resultsList.isEmpty()) {
			progressSheet = resultsList.get(0);
		}
		return progressSheet;
	}

	@Transactional
	@SuppressWarnings("unchecked")
	public List<ProgressSheet> getProgressSheetsByDemographic(int demographicId) {
		Session session = this.sessionFactory.getCurrentSession();
		String sql = "SELECT p1.* " +
				"FROM progress_sheet.progress_sheet p1 " +
				"LEFT JOIN progress_sheet.progress_sheet p2 " +
				"		ON (p1.appointment_id = p2.appointment_id AND p1.form_id < p2.form_id) " +
				"WHERE p2.form_id IS NULL " +
				"AND p1.demographic_id = :demographicId " +
				"ORDER BY p1.encounter_date DESC";
		SQLQuery query =  session.createSQLQuery(sql);
		query.setParameter("demographicId", demographicId);
		query.addEntity(ProgressSheet.class);
		
		return query.list();
	}
}
