/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.progressSheet.dao;

import org.oscarehr.util.SpringUtils;
import org.progressSheet.model.BillingFormField;
import org.progressSheet.model.BillingSheet;
import org.progressSheet.model.FormField;
import org.progressSheet.model.PainDiagram;
import org.progressSheet.model.ProgressSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProgressSheetService {

	private static final Logger logger = LoggerFactory.getLogger(ProgressSheetService.class);

	@Transactional
	public ProgressSheet addProgressSheet(ProgressSheet p) {
		ProgressSheetDao progressSheetDao = SpringUtils.getBean(ProgressSheetDao.class);
		PainDiagramDao painDiagramDao = SpringUtils.getBean(PainDiagramDao.class);

		progressSheetDao.addProgressSheet(p);

		// Set form field ids
		List<FormField> fields = p.getFormFields();
		for (FormField field : fields) {
			field.setPrimaryKeyProgressSheet(p);
		}

		PainDiagram diagram = p.getPainDiagram();
		if (diagram != null) {
			diagram.setFormId(p.getFormId());
			painDiagramDao.addPainDiagram(diagram);
		}
		return p;
	}

	@Transactional
	public BillingSheet addBillingBillingSheetDaoSheet(BillingSheet b) {
		BillingSheetDao billingSheetDao = SpringUtils.getBean(BillingSheetDao.class);

		billingSheetDao.addBillingSheet(b);

		// Set form field ids
		List<BillingFormField> fields = b.getFormFields();
		for (BillingFormField field : fields) {
			field.setPrimaryKeyBillingSheet(b);
		}

		return b;
	}
}
