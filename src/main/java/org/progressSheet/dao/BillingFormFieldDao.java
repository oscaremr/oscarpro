/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.progressSheet.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.progressSheet.model.BillingFormField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class BillingFormFieldDao {

	private static final Logger logger = LoggerFactory.getLogger(BillingFormFieldDao.class);
	
	private SessionFactory sessionFactory;
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	public void addFormField(BillingFormField ff) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(ff);
	}

	public void updateFormField(BillingFormField ff) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(ff);
	}
}