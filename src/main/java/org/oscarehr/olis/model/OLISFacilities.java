package org.oscarehr.olis.model;

import org.oscarehr.common.model.AbstractModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "olis_facilities")
public class OLISFacilities extends AbstractModel<Integer> implements OlisFacilityHospital {

    @Id
    @GeneratedValue
    @Column(name = "licence_number")
    private Integer id;
    @Column(name = "facility_name")
    private String name;
    @Column(name = "facility_address_line1")
    private String addressLine1;
    @Column(name = "facility_address_line2")
    private String addressLine2;
    @Column(name = "facility_address_city")
    private String city;
    @Column(name = "facility_postal_code")
    private String postalCode;
    @Column(name = "oid")
    private String oid;
    @Column(name = "full_id")
    private String fullId;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return "ON";
    }
    
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getFullId() {
        return fullId;
    }

    public void setFullId(String fullId) {
        this.fullId = fullId;
    }
}
