package org.oscarehr.ws;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingMessage;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import org.oscarehr.util.LoggedInInfo;
import org.owasp.csrfguard.http.InterceptRedirectResponse;
import oscar.OscarProperties;

import javax.servlet.http.HttpServletRequest;
import java.util.logging.Level;

public class OscarLoggingInInterceptor extends LoggingInInterceptor {

    @Override
    public void handleMessage(Message message) throws Fault {
        OscarProperties oscarProperties = OscarProperties.getInstance();
        String wsLogLevel = oscarProperties.getProperty("WS_LOG_LEVEL", "BASIC");

        if (wsLogLevel.equalsIgnoreCase("full")) {
            //If the log level is full, revert to the built-in logging
            super.handleMessage(message);
        } else if (wsLogLevel.equalsIgnoreCase("basic")) {
            //If the log level is basic (the default), just log the basic info we need
            final StringBuilder buffer = new StringBuilder();

            String id = (String) message.getExchange().get(LoggingMessage.ID_KEY);
            if (id == null) {
                id = LoggingMessage.nextId();
                message.getExchange().put(LoggingMessage.ID_KEY, id);
            }
            buffer.append("Request ID ").append(id);
            buffer.append(" - ");

            HttpServletRequest httpServletRequest = (HttpServletRequest) message.get(AbstractHTTPDestination.HTTP_REQUEST);
            if (httpServletRequest != null) {
                buffer.append(LoggedInInfo.obtainClientIpAddress(httpServletRequest));
                buffer.append(" - ");
            }

            String httpMethod = (String) message.get(Message.HTTP_REQUEST_METHOD);
            if (httpMethod != null) {
                buffer.append(httpMethod);
                buffer.append(" - ");
            }

            String uri = (String) message.get(Message.REQUEST_URL);
            if (uri == null) {
                String address = (String) message.get(Message.ENDPOINT_ADDRESS);
                uri = (String) message.get(Message.REQUEST_URI);
                if (uri != null && uri.startsWith("/")) {
                    if (address != null && !address.startsWith(uri)) {
                        if (address.endsWith("/") && address.length() > 1) {
                            address = address.substring(0, address.length());
                        }
                        uri = address + uri;
                    }
                } else {
                    uri = address;
                }
            }
            if (uri != null) {
                buffer.append(uri);
                String query = (String) message.get(Message.QUERY_STRING);
                if (query != null) {
                    buffer.append("?").append(query);
                }
                buffer.append(" - ");
            }

            InterceptRedirectResponse interceptRedirectResponse = (InterceptRedirectResponse) message.get(AbstractHTTPDestination.HTTP_RESPONSE);
            if (interceptRedirectResponse != null) {
                Integer responseCode = interceptRedirectResponse.getStatus();
                if (responseCode != null) {
                    buffer.append(responseCode);
                }
            }

            super.getLogger().log(Level.INFO, buffer.toString());
        }
    }
}
