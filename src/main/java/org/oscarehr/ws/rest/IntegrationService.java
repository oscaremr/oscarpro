/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.ws.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.well.integration.Integration;
import com.well.integration.IntegrationDao;
import com.well.integration.IntegrationManager;
import com.well.integration.config.*;
import com.well.integration.config.parameters.ConfigParams;
import com.well.integration.provision.ProvisionRequest;
import com.well.integration.provision.ProvisionResponse;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.rs.security.oauth.data.OAuthContext;
import org.apache.cxf.security.SecurityContext;
import org.apache.log4j.Logger;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.managers.DemographicManager;
import org.oscarehr.managers.OscarLogManager;
import org.oscarehr.managers.ProviderManager2;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.ws.rest.to.UrlResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component("IntegrationService")
@Path("/integration")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class IntegrationService extends AbstractServiceImpl {

	@Autowired
	ProviderDao providerDao;

	@Autowired
	ProviderManager2 providerManager;

	@Autowired
	OscarLogManager oscarLogManager;

	@Autowired
	DemographicManager demographicManager;

	@Autowired
	ConfigPropertyDao configPropertyDao;

	@Autowired
	IntegrationDao integrationDao;

	@Autowired
	IntegrationManager integrationManager;

	@Autowired
	protected SecurityInfoManager securityInfoManager;

	private final ConfigPropertyConverter converter = new ConfigPropertyConverter();
	private Logger logger = MiscUtils.getLogger();

	protected SecurityContext getSecurityContext() {
		Message m = PhaseInterceptorChain.getCurrentMessage();
    	SecurityContext sc = m.getContent(SecurityContext.class);
    	return sc;
	}

	protected OAuthContext getOAuthContext() {
		Message m = PhaseInterceptorChain.getCurrentMessage();
		OAuthContext sc = m.getContent(OAuthContext.class);
    	return sc;
	}

	protected Gson getGson() {
		return new GsonBuilder()
				.serializeNulls()
				.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
				.create();
	}

    public IntegrationService() {
    }

    private Integration getIntegration(String integration) {
		Integration integrationObject = integrationDao.getIntegration(integration);
		if (integrationObject == null) { throw new NotFoundException(); }
		return integrationObject;
	}

	@GET
	@Path("/config/{integration}")
	public Response getConfig(@PathParam("integration") String name) throws NotAuthorizedException {
		LoggedInInfo loggedInInfo = getLoggedInInfo();
		if (!securityInfoManager.hasPrivilege(loggedInInfo, "_admin,_admin.misc", "r", null)) {
			integrationManager.log(loggedInInfo.getLoggedInProviderNo(), loggedInInfo.getIp(),
					"Denied access to integration config, insufficient permissions.");
			throw new NotAuthorizedException("Missing admin or misc admin permission.");
		}
		try {
			Integration integration = getIntegration(name);
			ConfigProperty config = configPropertyDao.getConfig(integration);

			// Adding display name to config object.
			ConfigPropertyTo asTransferObject = converter.getAsTransferObject(loggedInInfo, config);
			Gson gson = new GsonBuilder().serializeNulls().create();
			JsonObject json = gson.toJsonTree(asTransferObject).getAsJsonObject();
			json.addProperty("name", integration.getDisplayName());
			return Response.ok(json.toString()).build();
		} catch (Throwable t) {
			logger.error("Unable to retrieve integration config.", t);
			return Response.serverError()
					.entity(t.getMessage())
					.build();
		}
	}

	@GET
	@Path("/config/{integration}/generate")
	public Response generateConfig(@PathParam("integration") String name) throws NotAuthorizedException {
		LoggedInInfo loggedInInfo = getLoggedInInfo();
		if (!securityInfoManager.hasPrivilege(loggedInInfo, "_admin,_admin.misc", "r", null)) {
			integrationManager.log(loggedInInfo.getLoggedInProviderNo(), loggedInInfo.getIp(),
					"Denied access to integration config, insufficient permissions.");
			throw new NotAuthorizedException("Missing admin or misc admin permission.");
		}
		try {
			Integration integration = getIntegration(name);
			ConfigParams params = GeneratorFactory
					.getGenerator(integration)
					.generateConfig(integrationManager, integration);
			return Response.ok(params).build();
		} catch (Throwable t) {
			logger.error("Unable to generate integration config.", t);
			return Response.serverError()
					.entity(t.getMessage())
					.build();
		}		
	}

	@POST
	@Path("/config/{integration}")
	public Response saveConfig(@PathParam("integration") String name, String body) throws NotAuthorizedException {
		LoggedInInfo loggedInInfo = getLoggedInInfo();
		if (!securityInfoManager.hasPrivilege(loggedInInfo, "_admin,_admin.misc", "w", null)) {
			integrationManager.log(loggedInInfo.getLoggedInProviderNo(), loggedInInfo.getIp(), "Denied access to integration config, insufficient permissions.");
			throw new NotAuthorizedException("Missing admin or misc admin permission.");
		}
		try {
			Integration integration = getIntegration(name);
			ConfigParams config = ParameterFactory.getParams(integration, body);
			config.setIntegration(integration);
			ProvisionResponse provisionResponse = integrationManager.saveConfig(
					loggedInInfo.getLoggedInProviderNo(), loggedInInfo.getIp(), config);
			return Response.ok(provisionResponse).build();
		} catch (Throwable t) {
			logger.error("Unable to save integration config.", t);
			return Response.serverError()
					.entity(t.getMessage())
					.build();
		}
	}

	/**
	 * Used to determine if a connection to integration is established.
	 * @param healthCheck when true integration will check that credentials configured are valid
	 * @param providerNo provider to check for registration along with connectivity
	 * @return Response proxies response from integration connectivity endpoint
	 *                  200 when provisioned,
	 *                  206 when connected but provider not provisioned
	 *                  401 when API key is not recognized
	 *                  404 when not provisioned
	 *                  408 when integration is not available (special case not proxied)
	 */
	@GET
	@Path("/connection/{integration}")
	public Response getConnectionStatus(@PathParam("integration") String name,
										@QueryParam("healthCheck") Boolean healthCheck,
										@QueryParam("providerNo") String providerNo) throws NotAuthorizedException {
		try {
			Integration integration = getIntegration(name);
			ProvisionResponse response;
			if (healthCheck != null) {
				response = ProvisionRequest.checkHealth(integration).execute();
			} else if (providerNo != null) {
				response = ProvisionRequest.checkProvider(integration, providerNo).execute();
			} else {
				response = ProvisionRequest.checkConnection(integration).execute();
			}
			return Response.ok().status(response.getStatus()).build();
		} catch (Throwable t) {
			logger.error("Error while checking connection status.", t);
			return Response.serverError().entity(t.getMessage()).build();
		}
	}

	@GET
	@Path("/adminUrl/{integration}")
	public Response getAdminUrl(@PathParam("integration") String name) throws NotAuthorizedException {
		Integration integration = getIntegration(name);
		String adminUrl = integrationDao.getIntegrationAdminUrl(integration);
		UrlResponse adminUrlResponse = new UrlResponse(adminUrl);
		return Response.ok().entity(adminUrlResponse).build();
	}
}
