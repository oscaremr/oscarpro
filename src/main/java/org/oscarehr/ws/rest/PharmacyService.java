/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.ws.rest;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DemographicPharmacyDao;
import org.oscarehr.common.dao.PharmacyInfoDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.PharmacyInfo;
import org.oscarehr.util.FaxUtils;
import org.oscarehr.ws.rest.conversion.PharmacyInfoConverter;
import org.oscarehr.ws.rest.to.OscarSearchResponse;
import org.oscarehr.ws.rest.to.model.PharmacyInfoTo1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import oscar.util.StringUtils;

/**
 * Defines service contract for create/read/update/delete operations on pharmacies.
 * 
 * TODO: switch DAOs to PharmacyManager
 */
@Path("/pharmacies/")
@Component("pharmacyService")
public class PharmacyService extends AbstractServiceImpl {

	@Autowired
	private PharmacyInfoDao pharmacyInfoDao;
	
	@Autowired
	private DemographicPharmacyDao demographicPharmacyDao;
	
	@Autowired
	private DemographicDao demographicDao;

	private PharmacyInfoConverter converter = new PharmacyInfoConverter();
	
	/**
	 * Loads all active pharmacies available on the system
	 * <p/>
	 * In case limit or offset parameters are set to null or zero, the entire result set is returned.
	 * 
	 * @param offset
	 * 		First record in the entire result set to be returned
	 * @param limit
	 * 		Maximum total number of records that should be returned
	 * @return
	 * 		Returns all pharmacy records available on the system
	 */
	@GET
	@Path("/")
	public OscarSearchResponse<PharmacyInfoTo1> getPharmacies(@QueryParam("offset") Integer offset, @QueryParam("limit") Integer limit) {
		OscarSearchResponse<PharmacyInfoTo1> result = new OscarSearchResponse<PharmacyInfoTo1>();
		result.getContent().addAll(converter.getAllAsTransferObjects(getLoggedInInfo(),pharmacyInfoDao.findAll(offset, limit))); 
		return result;
	}

	/**
	 * Finds pharmacy with the specified id.
	 * 
	 * @param id
	 * 		Id of the pharmacy to be found
	 * @return
	 * 		Returns the pharmacy or null if it can not be found
	 */
	@GET
	@Path("/{pharmacyId}")
	public PharmacyInfoTo1 getPharmacy(@PathParam("pharmacyId") Integer id) {
		return converter.getAsTransferObject(getLoggedInInfo(),pharmacyInfoDao.find(id));
	}

	/**
	 * Adds pharmacy to the system
	 * 
	 * @param pharmacyInfo
	 * 		Pharmacy info to be added
	 * @return
	 * 		Returns the added pharmacy info
	 */
	@POST
	@Path("/")
	public PharmacyInfoTo1 addPharmacy(PharmacyInfoTo1 pharmacyInfo) {
		return converter.getAsTransferObject(getLoggedInInfo(),pharmacyInfoDao.saveEntity(converter.getAsDomainObject(getLoggedInInfo(),pharmacyInfo)));
	}

  /**
   * Updates a preferred pharmacy for a demographic if the pharmacy exists using fax number. If it
   * doesn't exist, the pharmacy is added and the demographic preferred pharmacy updated.
   *
   * @param pharmacyInfoTo1 a PharmacyInfo transfer object containing details of the pharmacy to be
   *     added/updated. Fax Number and Pharmacy Name are mandatory.
   * @return Returns response with code and entity message
   */
  @POST
  @Path("/addPreferredPharmacy")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response addPreferredPharmacy(
      @Context HttpServletRequest httpServletRequest,
      @QueryParam("demographicNo") Integer demographicNo,
      PharmacyInfoTo1 pharmacyInfoTo1) {

    if (demographicNo == null) {
      return Response.status(400).entity("Error: Invalid Demographic Number").build();
    }
    Demographic demographic = demographicDao.getDemographic(demographicNo);
    if (demographic == null) {
      return Response.status(404).entity("Error: Demographic not found").build();
    }

    String name = pharmacyInfoTo1.getName();
    String faxNumber = pharmacyInfoTo1.getFax();
    if (faxNumber == null || StringUtils.isNullOrEmpty(name)) {
      return Response.status(400).entity("Error: Missing Fax Number / Pharmacy Name.").build();
    }
    if (!FaxUtils.isValidFaxNumber(faxNumber)) {
      return Response.status(400)
          .entity(
              "Error: Invalid Fax Number. "
                  + "Number must be in range of 10-12 digits with following formats:\n"
                  + "###-###-####\n"
                  + "1-###-###-###\n"
                  + "9-###-###-####\n"
                  + "9-1-###-###-####\n"
                  + "9-9-###-###-####")
          .build();
    }
    String faxRegExp = FaxUtils.convertToRegExp(faxNumber);
    PharmacyInfo pharmacyInfo = pharmacyInfoDao.getPharmacyByFax(faxRegExp);
    if (pharmacyInfo != null) {
      demographicPharmacyDao.addPharmacyToDemographic(pharmacyInfo.getId(), demographicNo, 0);
      return Response.ok()
          .status(200)
          .entity("Success: Demographic preferred pharmacy updated.")
          .build();
    } else {
      if (pharmacyInfoTo1.getAddDate() == null) {
        pharmacyInfoTo1.setAddDate(new Date());
      }
      if (pharmacyInfoTo1.getStatus() == null) {
        pharmacyInfoTo1.setStatus('1');
      }
      pharmacyInfo = pharmacyInfoDao.saveEntity(converter.getAsDomainObject(getLoggedInInfo(), pharmacyInfoTo1));
      demographicPharmacyDao.addPharmacyToDemographic(pharmacyInfo.getId(), demographicNo, 0);
      return Response.ok()
          .status(200)
          .entity("Success: Pharmacy added. Demographic preferred pharmacy updated.")
          .build();
    }
  }

	/**
	 * Updates pharmacy on the system
	 * 
	 * @param pharmacyInfo
	 * 		Pharmacy info to be updated
	 * @return
	 * 		Returns the updated pharmacy info
	 */
	@PUT
	@Path("/")
	public PharmacyInfoTo1 updatePharmacy(PharmacyInfoTo1 pharmacyInfo) {
		return converter.getAsTransferObject(getLoggedInInfo(),pharmacyInfoDao.saveEntity(converter.getAsDomainObject(getLoggedInInfo(),pharmacyInfo)));
	}

	/**
	 * Deletes pharmacy from the system
	 * 
	 * @param id
	 * 		ID of the pharmacy to be deleted
	 * @return
	 * 		Returns the deleted pharmacy info
	 */
	@DELETE
	@Path("/{pharmacyId}")
	public PharmacyInfoTo1 removePharmacy(@PathParam("pharmacyId") Integer id) {
		PharmacyInfo pharmacyInfo = pharmacyInfoDao.find(id);
		pharmacyInfo.setStatus(PharmacyInfo.DELETED);
		return converter.getAsTransferObject(getLoggedInInfo(),pharmacyInfoDao.saveEntity(pharmacyInfo));

	}

}
