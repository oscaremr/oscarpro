/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.ws.rest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import net.sf.json.JSONObject;
import org.oscarehr.PMmodule.caisi_integrator.CaisiIntegratorManager;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.PMmodule.dao.SecUserRoleDao;
import org.oscarehr.PMmodule.model.SecUserRole;
import org.oscarehr.caisi_integrator.ws.DemographicTransfer;
import org.oscarehr.caisi_integrator.ws.MatchingDemographicTransferScore;
import org.oscarehr.common.dao.ContactDao;
import org.oscarehr.common.dao.ProfessionalSpecialistDao;
import org.oscarehr.common.dao.WaitingListDao;
import org.oscarehr.common.dao.WaitingListNameDao;
import org.oscarehr.common.exception.PatientDirectiveException;
import org.oscarehr.common.model.Allergy;
import org.oscarehr.common.model.ConsultDocs;
import org.oscarehr.common.model.ConsultResponseDoc;
import org.oscarehr.common.model.ConsultationRequest;
import org.oscarehr.common.model.ConsultationResponse;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicContact;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.DemographicExtKey;
import org.oscarehr.common.model.ProfessionalSpecialist;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.WaitingListName;
import org.oscarehr.managers.AllergyManager;
import org.oscarehr.managers.ConsultationManager;
import org.oscarehr.managers.DemographicManager;
import org.oscarehr.managers.DocumentManager;
import org.oscarehr.managers.MeasurementManager;
import org.oscarehr.managers.NoteManager;
import org.oscarehr.managers.RxManager;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.web.DemographicSearchHelper;
import org.oscarehr.ws.rest.conversion.AllergyConverter;
import org.oscarehr.ws.rest.conversion.ConsultationRequestConverter;
import org.oscarehr.ws.rest.conversion.ConsultationResponseConverter;
import org.oscarehr.ws.rest.conversion.DemographicContactFewConverter;
import org.oscarehr.ws.rest.conversion.DemographicConverter;
import org.oscarehr.ws.rest.conversion.DocumentConverter;
import org.oscarehr.ws.rest.conversion.MeasurementConverter;
import org.oscarehr.ws.rest.conversion.ProfessionalSpecialistConverter;
import org.oscarehr.ws.rest.conversion.ProviderConverter;
import org.oscarehr.ws.rest.conversion.WaitingListNameConverter;
import org.oscarehr.ws.rest.to.AbstractSearchResponse;
import org.oscarehr.ws.rest.to.OscarSearchResponse;
import org.oscarehr.ws.rest.to.model.AllergyTo1;
import org.oscarehr.ws.rest.to.model.ConsultationRequestTo1;
import org.oscarehr.ws.rest.to.model.ConsultationResponseTo1;
import org.oscarehr.ws.rest.to.model.DemographicContactFewTo1;
import org.oscarehr.ws.rest.to.model.DemographicSearchRequest;
import org.oscarehr.ws.rest.to.model.DemographicSearchRequest.SEARCHMODE;
import org.oscarehr.ws.rest.to.model.DemographicSearchRequest.SORTDIR;
import org.oscarehr.ws.rest.to.model.DemographicSearchRequest.SORTMODE;
import org.oscarehr.ws.rest.to.model.DemographicSearchResult;
import org.oscarehr.ws.rest.to.model.DemographicTo1;
import org.oscarehr.ws.rest.to.model.DocumentTo1;
import org.oscarehr.ws.rest.to.model.MeasurementTo1;
import org.oscarehr.ws.rest.to.model.NoteTo1;
import org.oscarehr.ws.rest.to.model.StatusValueTo1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import oscar.OscarProperties;
import oscar.oscarWaitingList.util.WLWaitingListUtil;
import oscar.util.StringUtils;

/**
 * Defines a service contract for main operations on demographic. 
 */
@Path("/demographics")
@Component("demographicService")
@Slf4j
public class DemographicService extends AbstractServiceImpl {
	private static final String ALLERGIES = "allergies";
	private static final String MEASUREMENTS = "measurements";
	private static final String NOTES = "notes";
	private static final String MEDICATIONS = "medications";
	private static final String CONTACTS = "contacts";
	
	@Autowired private ContactDao contactDao;
	@Autowired private ProviderDao providerDao;
	@Autowired private SecUserRoleDao secUserRoleDao;
	@Autowired private ProfessionalSpecialistDao specialistDao;
	@Autowired private WaitingListDao waitingListDao;
	@Autowired private WaitingListNameDao waitingListNameDao;

	@Autowired private AllergyManager allergyManager;
	@Autowired private ConsultationManager consultationManager;
	@Autowired private DemographicManager demographicManager;
	@Autowired private DocumentManager documentManager;
	@Autowired private MeasurementManager measurementManager;
	@Autowired private NoteManager noteManager;
	@Autowired private RxManager rxManager;
	@Autowired private SecurityInfoManager securityInfoManager;

	private final DemographicConverter demoConverter = new DemographicConverter();
	private final DemographicContactFewConverter demoContactFewConverter = new DemographicContactFewConverter();
	private final WaitingListNameConverter waitingListNameConverter = new WaitingListNameConverter();
	private final ProviderConverter providerConverter = new ProviderConverter();
	private final ProfessionalSpecialistConverter specialistConverter = new ProfessionalSpecialistConverter();

	/**
	 * Finds all demographics.
	 * <p/>
	 * In case limit or offset parameters are set to null or zero, the entire result set is returned.
	 * 
	 * @param offset
	 * 		First record in the entire result set to be returned
	 * @param limit
	 * 		Maximum total number of records that should be returned
	 * @return
	 * 		Returns all demographics.
	 */
	@GET
	public OscarSearchResponse<DemographicTo1> getAllDemographics(
			@QueryParam("offset") Integer offset,
			@QueryParam("limit") Integer limit
	) {
		val result = new OscarSearchResponse<DemographicTo1>();
		if (offset == null) {
			offset = 0;
		}
		if (limit == null) {
			limit = 0;
		}
		result.setLimit(limit);
		result.setOffset(offset);
		result.setTotal(demographicManager.getActiveDemographicCount(getLoggedInInfo()).intValue());
		for (var demo : demographicManager.getActiveDemographics(getLoggedInInfo(), offset, limit)) {
			result.getContent().add(demoConverter.getAsTransferObject(getLoggedInInfo(),demo));
		}
		return result;
	}

	/**
	 * Gets detailed demographic data.
	 * 
	 * @param id
	 * 		Id of the demographic to get data for 
	 * @return
	 * 		Returns data for the demographic provided 
	 */
	@GET
	@Path("/{dataId}")
	@Produces({MediaType.APPLICATION_JSON , MediaType.APPLICATION_XML})
	public DemographicTo1 getDemographicData(
			@PathParam("dataId") final Integer id,
			@QueryParam("includes[]") final List<String> include
	) throws PatientDirectiveException {
		val loggedInInfo = getLoggedInInfo();
		val demographic = demographicManager.getDemographic(getLoggedInInfo(),id);
		if (demographic == null) {
			return null;
		}
		val extensions = demographicManager.getDemographicExts(getLoggedInInfo(),id);
		if (extensions != null && !extensions.isEmpty()) {
			val demoExtArray = extensions.toArray(new DemographicExt[0]);
			demographic.setExtras(demoExtArray);
		}
		val result = demoConverter.getAsTransferObject(getLoggedInInfo(),demographic);
		setOldCustExtensions(demographic, result);
		setDemographicContacts(result, id);
		setDemographicLists(result);
		setWaitingList(result);

		val waitingListNames = waitingListNameDao.findAll(null, null);
		if (waitingListNames!=null) {
			for (WaitingListName waitingListName : waitingListNames) {
				if (waitingListName.getIsHistory().equals("Y")) {
					continue;
				}
				val waitingListNameTo1 = waitingListNameConverter.getAsTransferObject(
						getLoggedInInfo(), waitingListName
				);
				result.getWaitingListNames().add(waitingListNameTo1);
			}
		}
		val referralDocs = specialistDao.findAll();
		if (referralDocs!=null) {
			for (ProfessionalSpecialist referralDoc : referralDocs) {
				if (referralDoc != null) {
					result.getReferralDoctors().add(specialistConverter.getAsTransferObject(
							getLoggedInInfo(),referralDoc
					));
				}
			}
		}
		val doctorRoles = secUserRoleDao.getSecUserRolesByRoleName("doctor");
		if (doctorRoles!=null) {
			for (SecUserRole doctor : doctorRoles) {
				Provider provider = providerDao.getProvider(doctor.getProviderNo());
				if (provider != null) {
					result.getDoctors().add(providerConverter.getAsTransferObject(getLoggedInInfo(),provider));
				}
			}
		}
		val nurseRoles = secUserRoleDao.getSecUserRolesByRoleName("nurse");
		if (nurseRoles!=null) {
			for (SecUserRole nurse : nurseRoles) {
				Provider provider = providerDao.getProvider(nurse.getProviderNo());
				if (provider != null) {
					result.getNurses().add(providerConverter.getAsTransferObject(getLoggedInInfo(), provider));
				}
			}
		}
		val midwifeRoles = secUserRoleDao.getSecUserRolesByRoleName("midwife");
		if (midwifeRoles!=null) {
			for (SecUserRole midwife : midwifeRoles) {
				Provider provider = providerDao.getProvider(midwife.getProviderNo());
				if (provider != null) {
					result.getMidwives().add(providerConverter.getAsTransferObject(getLoggedInInfo(),provider));
				}
			}
		}
		if (include.contains(ALLERGIES)) {
			result.setAllergies(new AllergyConverter().getAllAsTransferObjects(
					loggedInInfo,
					allergyManager.getActiveAllergies(getLoggedInInfo(), demographic.getDemographicNo())
			));
		}
		if (include.contains(MEASUREMENTS)) {
			val heightType = new ArrayList<String>();
			heightType.add("ht");
			val weightType = new ArrayList<String>();
			weightType.add("wt");
			val heights = measurementManager.getMeasurementByType(
					loggedInInfo, demographic.getDemographicNo(), heightType
			);
			val weights = measurementManager.getMeasurementByType(
					loggedInInfo, demographic.getDemographicNo(), weightType
			);
			val calendar = Calendar.getInstance();
			calendar.add(Calendar.YEAR, -3);
			val measurements
					= measurementManager.getLatestMeasurementsByDemographicIdObservedAfter(
							loggedInInfo, demographic.getDemographicNo(), calendar.getTime()
			);
			//Just send most recent height and weight
			if (!heights.isEmpty() && !measurements.contains(heights.get(0))) {
				measurements.add(heights.get(0));
			}
			if (!weights.isEmpty() && !measurements.contains(weights.get(0))) {
				measurements.add(weights.get(0));
			}
			result.setMeasurements(new MeasurementConverter().getAllAsTransferObjects(
					loggedInInfo, new ArrayList<>(measurements)
			));
		}
		if (include.contains(NOTES)) {
			result.setEncounterNotes(noteManager.getCppNotes(loggedInInfo, demographic.getDemographicNo()));
		}
		if (include.contains(MEDICATIONS)) {
			val singleLineMedications = rxManager.getCurrentSingleLineMedications(
					loggedInInfo, demographic.getDemographicNo()
			);
			result.setMedicationSummary(singleLineMedications);
		}
		return result;
	}

	/**
	 * Gets basic demographic data.
	 *
	 * @param demographicNumber
	 * 		Id of the demographic to get data for 
	 * @param includes
	 * 		An array of strings that include additional information in the returned data
	 * 		Possible includes are:
	 * 			- contacts = includes the DemographicContacts in the results	
	 * @return
	 * 		Returns data for the demographic provided 
	 */
	@GET
	@Path("/basic/{dataId}")
	@Produces({MediaType.APPLICATION_JSON , MediaType.APPLICATION_XML})
	public DemographicTo1 getBasicDemographicData(
			@PathParam("dataId") final Integer demographicNumber,
			@QueryParam("includes[]") final List<String> includes
	) throws PatientDirectiveException {
		val demographic
				= demographicManager.getDemographic(getLoggedInInfo(), demographicNumber);
		if (demographic == null) {
			return null;
		}
		val result = demoConverter.getAsTransferObject(getLoggedInInfo(), demographic);
		setOldCustExtensions(demographic, result);
		setDemographicLists(result);
		// If the contacts are included add Demographic Contacts to the basic results
		// (relationships/healthcareteam/etc)
		if (includes.contains(CONTACTS)) {
			setDemographicContacts(result, demographicNumber);
		}
		return result;
	}

	/**
	 * Saves demographic information. 
	 * 
	 * @param data
	 * 		Detailed demographic data to be saved
	 * @return
	 * 		Returns the saved demographic data
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces({MediaType.APPLICATION_JSON , MediaType.APPLICATION_XML})
	public DemographicTo1 createDemographicData(final DemographicTo1 data) {
		val demographic = demoConverter.getAsDomainObject(getLoggedInInfo(), data);
		demographicManager.createDemographic(getLoggedInInfo(), demographic, data.getAdmissionProgramId());
		List<AllergyTo1> responseAllergies = new ArrayList<>();
		if (data.getAllergies() != null && !data.getAllergies().isEmpty()) {
			val allergies = new ArrayList<Allergy>();
			val allergyConverter = new AllergyConverter();
			for (var allergyTo1 : data.getAllergies()) {
				val allergy = allergyConverter.getAsDomainObject(getLoggedInInfo(), allergyTo1);
				allergy.setDemographicNo(demographic.getDemographicNo());
				allergies.add(allergy);
			}
			if (demographic.getDemographicNo().equals(data.getDemographicNo())) {
				allergyManager.saveAllergies(allergies);
			} else {
				allergyManager.createAllergies(allergies);
			}
			responseAllergies = allergyConverter.getAllAsTransferObjects(getLoggedInInfo(), allergies);
		}
		val responseMeasurements = new ArrayList<MeasurementTo1>();
		if (data.getMeasurements() != null && !data.getMeasurements().isEmpty()) {
			val measurementConverter = new MeasurementConverter();
			for (MeasurementTo1 measurementTo1 : data.getMeasurements()) {
				val measurement = measurementConverter.getAsDomainObject(
						getLoggedInInfo(), measurementTo1
				);
				measurement.setDemographicId(demographic.getDemographicNo());
				measurementManager.addMeasurement(getLoggedInInfo(), measurement);
				measurementTo1.setId(measurement.getId());
				responseMeasurements.add(measurementTo1);
			}
		}
		val responseConsultRequests = new ArrayList<ConsultationRequestTo1>();
		if (data.getConsultationRequests() != null && !data.getConsultationRequests().isEmpty()) {
			val requestConverter = new ConsultationRequestConverter();
			for (var requestTo1 : data.getConsultationRequests()) {
				ConsultationRequest request = null;
				requestTo1.setDemographicId(demographic.getDemographicNo());
				if (!demographic.getDemographicNo().equals(data.getDemographicNo())) {
					requestTo1.setId(null);
				}				
				if (requestTo1.getId() == null) { //new consultation request
					request = requestConverter.getAsDomainObject(getLoggedInInfo(), requestTo1);
				} else {
					request = requestConverter.getAsDomainObject(
							getLoggedInInfo(),
							requestTo1,
							consultationManager.getRequest(getLoggedInInfo(), requestTo1.getId())
					);
				}
				request.setProfessionalSpecialist(consultationManager.getProfessionalSpecialist(
								requestTo1.getProfessionalSpecialist().getId()
				));
				request.setLegacyAttachment(true);
				consultationManager.saveConsultationRequest(getLoggedInInfo(), request);
				requestTo1.setId(request.getId());
				//save attachments
				saveRequestAttachments(requestTo1);
				responseConsultRequests.add(requestTo1);
			}
		}
		val responseConsultResponse = new ArrayList<ConsultationResponseTo1>();
		if (data.getConsultationResponses() != null && !data.getConsultationResponses().isEmpty()) {
			val responseConverter = new ConsultationResponseConverter();
			for (var responseTo1 : data.getConsultationResponses()) {
				ConsultationResponse response = null;
				if (!demographic.getDemographicNo().equals(data.getDemographicNo())) {
					responseTo1.setId(null);
				}
				responseTo1.setDemographic(demoConverter.getAsTransferObject(getLoggedInInfo(), demographic));
				if (responseTo1.getId()==null) { //new consultation response
					response = responseConverter.getAsDomainObject(getLoggedInInfo(), responseTo1);
				} else {
					response = responseConverter.getAsDomainObject(
							getLoggedInInfo(),
							responseTo1,
							consultationManager.getResponse(getLoggedInInfo(), responseTo1.getId())
					);
				}
				consultationManager.saveConsultationResponse(getLoggedInInfo(), response);
				responseTo1.setId(response.getId());
				//save attachments
				saveResponseAttachments(responseTo1);
				responseConsultResponse.add(responseTo1);
			}
		}
		val responseEncounterNotes = new ArrayList<NoteTo1>();
		if (data.getEncounterNotes() != null && !data.getEncounterNotes().isEmpty()) {
			for (var note : data.getEncounterNotes()) {
                if (!demographic.getDemographicNo().equals(data.getDemographicNo())) {
                    note.setNoteId(null);
                    note.setUuid(null);
                }
                note = noteManager.saveNote(getLoggedInInfo(), demographic.getDemographicNo(), note);
			    if (note != null) {
			        note.setNoteId(note.getNoteId());
			        note.setUuid(note.getUuid());
			        responseEncounterNotes.add(note);
			    }
			}
		}
		val responseDocuments = new ArrayList<DocumentTo1>();
		if (data.getDocuments() != null && !data.getDocuments().isEmpty()) {
			val documentConverter = new DocumentConverter();
			for (var documentTo1 : data.getDocuments()) {
				var document = documentConverter.getAsDomainObject(getLoggedInInfo(), documentTo1);
				document = documentManager.createDocument(
						getLoggedInInfo(),
						document,
						demographic.getDemographicNo(),
						documentTo1.getProviderNo(),
						documentTo1.getFileContents(),
						null
				);
				documentTo1.setId(document.getDocumentNo());
				responseDocuments.add(documentTo1);
			}
		}
		val responseDemographic = demoConverter.getAsTransferObject(getLoggedInInfo(), demographic);
		responseDemographic.setAllergies(responseAllergies);
		responseDemographic.setMeasurements(responseMeasurements);
		responseDemographic.setConsultationRequests(responseConsultRequests);
		responseDemographic.setConsultationResponses(responseConsultResponse);
		responseDemographic.setEncounterNotes(responseEncounterNotes);
		responseDemographic.setDocuments(responseDocuments);
		return responseDemographic;
	}

	/**
	 * Updates demographic information. 
	 * 
	 * @param data
	 * 		Detailed demographic data to be updated
	 * @return
	 * 		Returns the updated demographic data
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public DemographicTo1 updateDemographicData(final DemographicTo1 data) {
		saveDemographicExtensions(data);
		if (data.getWaitingListID() != null) {
			WLWaitingListUtil.updateWaitingListRecord(
					data.getWaitingListID().toString(),
					data.getWaitingListNote(),
					data.getDemographicNo().toString(),
					null
			);
		}
		val demographic = demoConverter.getAsDomainObject(getLoggedInInfo(), data);
		demographicManager.updateDemographic(getLoggedInInfo(), demographic);
		return demoConverter.getAsTransferObject(getLoggedInInfo(), demographic);
	}

	/**
	 * Deletes demographic information. 
	 * 
	 * @param id
	 * 		Id of the demographic data to be deleted
	 * @return
	 * 		Returns the deleted demographic data
	 */
	@DELETE
	@Path("/{dataId}")
	public DemographicTo1 deleteDemographicData(@PathParam("dataId") final Integer id) {
		Demographic demo = demographicManager.getDemographic(getLoggedInInfo(), id);
		DemographicTo1 result = getDemographicData(id, Collections.EMPTY_LIST);
		if (demo == null) {
			throw new IllegalArgumentException("Unable to find demographic record with ID " + id);
		}
		demographicManager.deleteDemographic(getLoggedInInfo(), demo);
		return result;
	}

	/**
	 * Search demographics - used by navigation of OSCAR webapp.
	 * Currently supports LastName[,FirstName] and address searches.
	 * 
	 * @param query The query string
	 * @return Returns data for the demographic provided
	 */
	@GET
	@Path("/quickSearch")
	@Produces("application/json")
	public AbstractSearchResponse<DemographicSearchResult> search(
			@QueryParam("query") final String query
	) {		
		if (!securityInfoManager.hasPrivilege(getLoggedInInfo(), "_demographic", "r", null)) {
				throw new RuntimeException("Access Denied");
		}
		val response = new AbstractSearchResponse<DemographicSearchResult>();
		if (query == null ) {
			return response;
		}
		DemographicSearchRequest req = new DemographicSearchRequest();
		req.setActive(true);
		req.setIntegrator(false); //this should be configurable by persona
		//caisi
		boolean outOfDomain 
				= !OscarProperties.getInstance().getProperty("ModuleNames", "").contains("Caisi");
		req.setOutOfDomain(outOfDomain);
		if (query.startsWith("addr:")) {
			req.setMode(SEARCHMODE.Address);
			req.setKeyword(query.substring("addr:".length()));
		} else if (query.startsWith("chartNo:")) {
			req.setMode(SEARCHMODE.ChartNo);
			req.setKeyword(query.substring("chartNo:".length()));
		} else {
			req.setMode(SEARCHMODE.Name);
			req.setKeyword(query);
		}
		int count = demographicManager.searchPatientsCount(getLoggedInInfo(),req);
		if (count > 0) {
			val results = demographicManager.searchPatients(getLoggedInInfo(), req, 0, 10);
			response.setContent(results);
			response.setTotal(count);
			response.setQuery(query);
		}
		return response;
	}
	
	@POST
	@Path("/search")
	@Produces("application/json")
	@Consumes("application/json")
	public AbstractSearchResponse<DemographicSearchResult> search(
			final JSONObject json,
			@QueryParam("startIndex") final Integer startIndex,
			@QueryParam("itemsToReturn") final Integer itemsToReturn
	) {
		val response = new AbstractSearchResponse<DemographicSearchResult>();
		if (!securityInfoManager.hasPrivilege(getLoggedInInfo(), "_demographic", "r", null)) {
			throw new RuntimeException("Access Denied");
		}
		DemographicSearchRequest req = convertFromJSON(json);
		//caisi
		boolean outOfDomain =
				!OscarProperties.getInstance().getProperty("ModuleNames", "").contains("Caisi");
		req.setOutOfDomain(outOfDomain);
		if (json.getString("term").length() >= 1) {
			int count = demographicManager.searchPatientsCount(getLoggedInInfo(), req);
			if (count > 0) {
				List<DemographicSearchResult> results
						= demographicManager.searchPatients(getLoggedInInfo(), req, startIndex, itemsToReturn);
				response.setContent(results);
				response.setTotal(count);
			}
		}
		return response;
	}
	
	@POST
	@Path("/searchIntegrator")
	@Produces("application/json")
	@Consumes("application/json")
	public AbstractSearchResponse<DemographicSearchResult> searchIntegrator(
			final JSONObject json, 
			@QueryParam("itemsToReturn") final Integer itemsToReturn
	) {
		val response = new AbstractSearchResponse<DemographicSearchResult>();
		if (!securityInfoManager.hasPrivilege(getLoggedInInfo(), "_demographic", "r", null)) {
			throw new RuntimeException("Access Denied");
		}
		val results = new ArrayList<DemographicSearchResult>();
		if (json.getString("term").length() >= 1) {
			val matches = CaisiIntegratorManager.getMatchingDemographicParameters(getLoggedInInfo(), convertFromJSON(json));
			List<MatchingDemographicTransferScore> integratorSearchResults = null;
			try {			
				matches.setMaxEntriesToReturn(itemsToReturn);
				matches.setMinScore(7); 
				integratorSearchResults = DemographicSearchHelper.getIntegratedSearchResults(getLoggedInInfo(), matches);
				log.info("Integrator search results : "+(integratorSearchResults==null?"null":String.valueOf(integratorSearchResults.size())));
			} catch (Exception e) {
				log.error("error searching integrator", e);
			}
			if (integratorSearchResults != null) {
				for (var matchingDemographicTransferScore : integratorSearchResults) {
					if (isLocal(matchingDemographicTransferScore)) {
						log.warn("ignoring remote demographic since we already have them locally");
						continue;
					}
					if (matchingDemographicTransferScore.getDemographicTransfer() != null) {
						DemographicTransfer obj = matchingDemographicTransferScore.getDemographicTransfer();
						DemographicSearchResult item = new DemographicSearchResult();
						item.setLastName(obj.getLastName());
						item.setFirstName(obj.getFirstName());
						item.setSex(obj.getGender().toString());
						item.setDob(obj.getBirthDate().getTime());
						item.setRemoteFacilityId(obj.getIntegratorFacilityId());
						item.setDemographicNo(obj.getCaisiDemographicId());
						results.add(item);
					}
				}
			}
		}
		 response.setContent(results);
		 response.setTotal((response.getContent()!=null)?response.getContent().size():0);
		return response;
	}
	
	@POST
	@Path("/matchDemographic")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject matchDemographic(final JSONObject json) {
		JSONObject responseObject;
		val dateOfBirth = Calendar.getInstance();
		val hin = json.getString("hin");
		val dob = json.getString("dob");
		try {
			val sdf = new SimpleDateFormat("yyyy-MM-dd");
			dateOfBirth.setTime(sdf.parse(dob));
			responseObject = demographicManager.matchDemographic(getLoggedInInfo(), dateOfBirth, hin);
		} catch (ParseException e) {
			log.warn("Could not parse the provided date of birth: " + dob);
			responseObject = new JSONObject();
			responseObject.put("code", "E");
			responseObject.put("message", "The date of birth could not be correctly read.\nPlease review your information and try again or contact the clinic for further assistance.");
		}
		return responseObject;
	}
	
	@POST
	@Path("/matchDemographicPartial")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response matchDemographicPartial(final JSONObject searchParameters) {
		val lastName = searchParameters.optString("lastName", null);
		val dateOfBirth = searchParameters.optString("dateOfBirth", null);
		Response response;
		if (StringUtils.isNullOrEmpty(lastName) || StringUtils.isNullOrEmpty(dateOfBirth)) {
			response = Response.status(Response.Status.BAD_REQUEST).entity(
					"The attributes lastName and dateOfBirth are both required to match a demographic"
			).build();
		} else {
			try {
				// Sets the calendar with the DOB, errors if it is an invalid date
				val birthDate = dateOfBirth.split("-");
				val dob = Calendar.getInstance();
				dob.setLenient(false);
				dob.set(Calendar.YEAR, Integer.parseInt(birthDate[0]));
				dob.set(Calendar.MONTH, Integer.parseInt(birthDate[1]) - 1);
				dob.set(Calendar.DATE, Integer.parseInt(birthDate[2]));
				val matchedDemographics = demographicManager.findByLastNameDob(lastName, dob);
				val demographicTs = new DemographicConverter().getAllAsTransferObjects(getLoggedInInfo(), matchedDemographics);
				response = Response.ok().entity(demographicTs).build();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
				response = Response.status(Response.Status.BAD_REQUEST).entity(
						"The date of birth must be a valid date in the format yyyy-MM-dd. "
								+ "There is an issue with " + e.getMessage()
				).build();
			}
		}
		return response;
	}

	/* PRIVATE HELPERS */

	private void saveDemographicExtensions(@NonNull final DemographicTo1 data) {
		val demographicNumber = data.getDemographicNo();
		if (org.apache.commons.lang3.StringUtils.isNotEmpty(data.getNurse())) {
			saveDemographicExtension(demographicNumber, DemographicExtKey.NURSE, data.getNurse());
		}
		if (org.apache.commons.lang3.StringUtils.isNotEmpty(data.getResident())) {
			saveDemographicExtension(demographicNumber, DemographicExtKey.RESIDENT, data.getResident());
		}
		if (org.apache.commons.lang3.StringUtils.isNotEmpty(data.getMidwife())) {
			saveDemographicExtension(demographicNumber, DemographicExtKey.MIDWIFE, data.getMidwife());
		}
		if (org.apache.commons.lang3.StringUtils.isNotEmpty(data.getAlert())) {
			saveDemographicExtension(demographicNumber, DemographicExtKey.ALERT_BOOKING, data.getAlert());
		}
		if (org.apache.commons.lang3.StringUtils.isNotEmpty(data.getNotes())) {
			saveDemographicExtension(demographicNumber, DemographicExtKey.NOTES, data.getNotes());
		}
	}

	private void saveDemographicExtension(
			@NonNull final Integer demographicNumber,
			@NonNull final DemographicExtKey key,
			@NonNull final String value
	) {
		var extension
				= demographicManager.getDemographicExt(getLoggedInInfo(), demographicNumber, key.getKey());
		if (extension == null) {
			extension = new DemographicExt();
			extension.setDemographicNo(demographicNumber);
			extension.setDemographicExtKey(key);
		}
		extension.setValue(value);
		extension.setProviderNo(getCurrentProvider().getProviderNo());
	}

	private void setDemographicContacts(
			final DemographicTo1 demographicTo1,
			final Integer demographicNo
	) {
		val demoContacts = demographicManager.getDemographicContacts(getLoggedInInfo(), demographicNo);
		if (demoContacts!=null) {
			for (var demoContact : demoContacts) {
				val contactId = Integer.valueOf(demoContact.getContactId());
				var demoContactTo1 = new DemographicContactFewTo1();
				if (demoContact.getCategory().equals(DemographicContact.CATEGORY_PERSONAL)) {
					if (demoContact.getType()==DemographicContact.TYPE_DEMOGRAPHIC) {
						val contactD = demographicManager.getDemographic(getLoggedInInfo(),contactId);
						demoContactTo1 = demoContactFewConverter.getAsTransferObject(demoContact, contactD);
						if (demoContactTo1.getPhone()==null || demoContactTo1.getPhone().equals("")) {
							val ext = demographicManager.getDemographicExt(
									getLoggedInInfo(), demographicNo, "demo_cell"
							);
							if (ext!=null) {
								demoContactTo1.setPhone(ext.getValue());
							}
						}
					}
					else if (demoContact.getType()==DemographicContact.TYPE_CONTACT) {
						val contactC = contactDao.find(contactId);
						demoContactTo1 = demoContactFewConverter.getAsTransferObject(demoContact, contactC);
					}
					demographicTo1.getDemoContacts().add(demoContactTo1);
				}
				else if (demoContact.getCategory().equals(DemographicContact.CATEGORY_PROFESSIONAL)) {
					if (demoContact.getType()==DemographicContact.TYPE_PROVIDER) {
						val contactP = providerDao.getProvider(contactId.toString());
						demoContactTo1 = demoContactFewConverter.getAsTransferObject(demoContact, contactP);
					}
					else if (demoContact.getType()==DemographicContact.TYPE_PROFESSIONALSPECIALIST) {
						val contactS = specialistDao.find(contactId);
						demoContactTo1 = demoContactFewConverter.getAsTransferObject(demoContact, contactS);
					}
					demographicTo1.getDemoContactPros().add(demoContactTo1);
				}
			}
		}
	}

	private void setOldCustExtensions(
		  @NonNull final Demographic demographic,
		  @NonNull final DemographicTo1 demographicTo1
	) {
		val demographicNumber = demographic.getDemographicNo();
		val extensions
				= demographicManager.getDemographicExts(getLoggedInInfo(), demographicNumber);
		if (extensions != null && !extensions.isEmpty()) {
			val demoExtArray = extensions.toArray(new DemographicExt[0]);
			demographic.setExtras(demoExtArray);
		}
		setOldCustExtension(demographicTo1, DemographicExtKey.NURSE);
		setOldCustExtension(demographicTo1, DemographicExtKey.RESIDENT);
		setOldCustExtension(demographicTo1, DemographicExtKey.MIDWIFE);
		setOldCustExtension(demographicTo1, DemographicExtKey.ALERT_BOOKING);
		setOldCustExtension(demographicTo1, DemographicExtKey.NOTES);
	}

	private void setOldCustExtension(DemographicTo1 demographicTo1, DemographicExtKey key) {
		val demographicNumber = demographicTo1.getDemographicNo();
		val extension
				= demographicManager.getDemographicExt(
				getLoggedInInfo(), demographicNumber, key.getKey()
		);
		if (extension != null) {
			if (DemographicExtKey.NURSE.equals(key)) {
				demographicTo1.setNurse(extension.getValue());
			} else if (DemographicExtKey.RESIDENT.equals(key)) {
				demographicTo1.setResident(extension.getValue());
			} else if (DemographicExtKey.MIDWIFE.equals(key)) {
				demographicTo1.setMidwife(extension.getValue());
			} else if (DemographicExtKey.ALERT_BOOKING.equals(key)) {
				demographicTo1.setAlert(extension.getValue());
			} else if (DemographicExtKey.NOTES.equals(key)) {
				demographicTo1.setNotes(extension.getValue());
			}
		}
	}

	private void setDemographicLists(final DemographicTo1 demographicTo1) {
		setWaitingList(demographicTo1);
		setPatientStatusList(demographicTo1);
		setRosterStatusList(demographicTo1);
	}

	private void setRosterStatusList(final DemographicTo1 demographicTo1) {
		val rosterStatusList = demographicManager.getRosterStatusList();
		if (rosterStatusList != null) {
			for (String rs : rosterStatusList) {
				val value = new StatusValueTo1(rs);
				demographicTo1.getRosterStatusList().add(value);
			}
		}
	}

	private void setPatientStatusList(final DemographicTo1 demographicTo1) {
		val patientStatusList = demographicManager.getPatientStatusList();
		if (patientStatusList != null) {
			for (String ps : patientStatusList) {
				val value = new StatusValueTo1(ps);
				demographicTo1.getPatientStatusList().add(value);
			}
		}
	}

	private void setWaitingList(final DemographicTo1 demographicTo1) {
		val waitingLists = waitingListDao.search_wlstatus(demographicTo1.getDemographicNo());
		if (waitingLists != null && !waitingLists.isEmpty()) {
			val waitingList = waitingLists.get(0);
			demographicTo1.setWaitingListID(waitingList.getListId());
			demographicTo1.setWaitingListNote(waitingList.getNote());
			demographicTo1.setOnWaitingListSinceDate(waitingList.getOnListSince());
		}
	}

	private DemographicSearchRequest convertFromJSON(final JSONObject json) {
		if (json == null) {
			return null;
		}
		val searchType = json.getString("type");
		val req = new DemographicSearchRequest();
		req.setMode(SEARCHMODE.valueOf(searchType));
		if (req.getMode() == null) {
			req.setMode(SEARCHMODE.Name);
		}
		req.setKeyword(json.getString("term"));
		req.setActive(Boolean.parseBoolean(json.getString("active")));
		req.setIntegrator(Boolean.parseBoolean(json.getString("integrator")));
		req.setOutOfDomain(Boolean.parseBoolean(json.getString("outofdomain")));
		val pattern = Pattern.compile("sorting\\[(\\w+)\\]");
		val params = json.getJSONObject("params");
		if (params != null) {
			for (var key : params.keySet()) {
				val matcher = pattern.matcher((String)key);
				if (matcher.find()) {
					val var = matcher.group(1);
				   req.setSortMode(SORTMODE.valueOf(var));
				   req.setSortDir(SORTDIR.valueOf(params.getString((String)key)));
				}
			}
		}
		return req;
	}
	
	private boolean isLocal(final MatchingDemographicTransferScore matchingDemographicTransferScore) {
		val hin = matchingDemographicTransferScore.getDemographicTransfer().getHin();
	    if (hin != null && !hin.isEmpty()) {
				val dsr = new DemographicSearchRequest();
		    dsr.setActive(true);
		    dsr.setKeyword(hin);
		    dsr.setMode(SEARCHMODE.HIN);
		    dsr.setOutOfDomain(true);
		    dsr.setSortMode(SORTMODE.Name);
		    dsr.setSortDir(SORTDIR.asc);
				return demographicManager.searchPatientsCount(getLoggedInInfo(), dsr) > 0;
	    }
	    return false;
	}

	private void saveRequestAttachments(final ConsultationRequestTo1 request) {
		val newAttachments = request.getAttachments();
		val currentDocs = consultationManager.getConsultRequestDocs(getLoggedInInfo(), request.getId());
		if (newAttachments==null || currentDocs==null) return;
		//first assume all current docs detached (set delete)
		for (ConsultDocs doc : currentDocs) {
			doc.setDeleted(ConsultDocs.DELETED);
		}
		//compare current & new, remove from current list the unchanged ones - no need to update them
		for (var newAttachment : newAttachments) {
			boolean isNew = true;
			for (ConsultDocs doc : currentDocs) {
				if (doc.getDocType().equals(newAttachment.getDocumentType())
						&& doc.getDocumentNo() == newAttachment.getDocumentNo()) {
					currentDocs.remove(doc);
					isNew = false;
					break;
				}
			}
			if (isNew) { //save the new attachment
				consultationManager.saveConsultRequestDoc(
						getLoggedInInfo(),
						new ConsultDocs(
								request.getId(),
								newAttachment.getDocumentNo(),
								newAttachment.getDocumentType(),
								getLoggedInInfo().getLoggedInProviderNo()
						)
				);
			}
		}
		//update what remains in current docs, they are detached (set delete)
		for (var doc : currentDocs) {
			consultationManager.saveConsultRequestDoc(getLoggedInInfo(), doc);
		}
	}
	
	private void saveResponseAttachments(final ConsultationResponseTo1 response) {
		val newAttachments = response.getAttachments();
		val currentDocs = consultationManager.getConsultResponseDocs(getLoggedInInfo(), response.getId());
		if (newAttachments == null || currentDocs == null) {
			return;
		}
		//first assume all current docs detached (set delete)
		for (var doc : currentDocs) {
			doc.setDeleted(ConsultResponseDoc.DELETED);
		}
		//compare current & new, remove from current list the unchanged ones - no need to update them
		for (var newAtth : newAttachments) {
			boolean isNew = true;
			for (var doc : currentDocs) {
				if (doc.getDocType().equals(newAtth.getDocumentType())
						&& doc.getDocumentNo()==newAtth.getDocumentNo()) {
					currentDocs.remove(doc);
					isNew = false;
					break;
				}
			}
			if (isNew) { //save the new attachment
				consultationManager.saveConsultResponseDoc(
						getLoggedInInfo(),
						new ConsultResponseDoc(
								response.getId(),
								newAtth.getDocumentNo(),
								newAtth.getDocumentType(),
								getLoggedInInfo().getLoggedInProviderNo()
						)
				);
			}
		}
		//update what remains in current docs, they are detached (set delete)
		for (var doc : currentDocs) {
			consultationManager.saveConsultResponseDoc(getLoggedInInfo(), doc);
		}
	}
}
