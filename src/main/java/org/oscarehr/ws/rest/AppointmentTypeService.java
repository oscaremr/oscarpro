/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.ws.rest;

import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.rs.security.oauth.data.OAuthContext;
import org.apache.cxf.security.SecurityContext;
import org.oscarehr.common.dao.AppointmentTypeDao;
import org.oscarehr.common.model.AppointmentType;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.ws.rest.conversion.AppointmentTypeConverter;
import org.oscarehr.ws.rest.to.model.AppointmentTypeTo1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component("AppointmentTypeService")
@Path("/appointmentType")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AppointmentTypeService extends AbstractServiceImpl {

	@Autowired
	AppointmentTypeDao appointmentTypeDao;

	@Autowired
	protected SecurityInfoManager securityInfoManager;

	private AppointmentTypeConverter converter = new AppointmentTypeConverter();

	protected SecurityContext getSecurityContext() {
		Message m = PhaseInterceptorChain.getCurrentMessage();
    	SecurityContext sc = m.getContent(SecurityContext.class);
    	return sc;
	}

	protected OAuthContext getOAuthContext() {
		Message m = PhaseInterceptorChain.getCurrentMessage();
		OAuthContext sc = m.getContent(OAuthContext.class);
    	return sc;
	}

    public AppointmentTypeService() { }

	@GET
	@Path("/all")
	public Response listAll() {
		return Response
				.ok()
				.entity(converter.getAllAsTransferObjects(getLoggedInInfo(), appointmentTypeDao.listAll()))
				.build();
	}

	@POST
	public Response createAppointmentType(AppointmentTypeTo1 data) {
		if (!securityInfoManager.hasPrivilege(getLoggedInInfo(), "_admin,_admin.schedule", "w", null)) {
			throw new NotAuthorizedException("Missing admin or schedule admin permission.");
		}
		if (appointmentTypeDao.findByAppointmentTypeByName(data.getName()) != null) {
			throw new BadRequestException(new IllegalArgumentException(
					String.format("Appointment type name '%s' should be unique.", data.getName())));
		}
		AppointmentType type = converter.getAsDomainObject(getLoggedInInfo(), data);
		appointmentTypeDao.persist(type);
		return Response
			.ok()
			.entity(converter.getAsTransferObject(getLoggedInInfo(), type))
			.build();
	}
}
