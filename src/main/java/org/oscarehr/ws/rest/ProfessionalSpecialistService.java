/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.ws.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.oscarehr.common.dao.ProfessionalSpecialistDao;
import org.oscarehr.common.model.ProfessionalSpecialist;
import org.oscarehr.util.FaxUtils;
import org.oscarehr.util.ProfessionalSpecialistUtil;
import org.oscarehr.ws.rest.conversion.ProfessionalSpecialistConverter;
import org.oscarehr.ws.rest.to.model.ProfessionalSpecialistTo1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import oscar.util.StringUtils;

@Path("/professionalSpecialist")
@Component("professionalSpecialistService")
public class ProfessionalSpecialistService extends AbstractServiceImpl {

  @Autowired private ProfessionalSpecialistDao professionalSpecialistDao;

  private final ProfessionalSpecialistConverter converter = new ProfessionalSpecialistConverter();

  @POST
  @Path("/add")
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response addProfessionalSpecialist(ProfessionalSpecialistTo1 professionalSpecialistTo1) {
    Response response;
    Response validateInputResponse = validateFields(professionalSpecialistTo1);

    if (validateInputResponse.getStatus() != 200) {
      return validateInputResponse;
    }
    ProfessionalSpecialist professionalSpecialist =
        professionalSpecialistDao.findByReferralNoAndLastName(
            professionalSpecialistTo1.getReferralNo(), professionalSpecialistTo1.getLastName());
    if (professionalSpecialist == null) {
      ProfessionalSpecialistTo1 addedSpecialist;
      if (professionalSpecialistTo1.getDepartmentId() == null) {
        professionalSpecialistTo1.setDepartmentId(0);
      }
      if (professionalSpecialistTo1.getInstitutionId() == null) {
        professionalSpecialistTo1.setInstitutionId(0);
      }
      addedSpecialist =
          converter.getAsTransferObject(
              getLoggedInInfo(),
              professionalSpecialistDao.saveEntity(
                  converter.getAsDomainObject(getLoggedInInfo(), professionalSpecialistTo1)));
      response = Response.ok().status(200).entity(addedSpecialist).build();
    } else {
      response =
          Response.ok()
              .status(400)
              .entity("Error: Professional Specialist already exists!")
              .build();
    }
    return response;
  }

  @GET
  @Path("/search")
  @Produces(MediaType.APPLICATION_JSON)
  public Response searchProfessionalSpecialist(
      @QueryParam("referralNo") String referralNo, @QueryParam("lastName") String lastName) {
    Response response;
    if (StringUtils.isNullOrEmpty(referralNo) || StringUtils.isNullOrEmpty(lastName)) {
      return Response.ok()
          .status(400)
          .entity("Error: Both last name and referral no. required to perform search.")
          .build();
    }
    ProfessionalSpecialist professionalSpecialist =
        professionalSpecialistDao.findByReferralNoAndLastName(referralNo, lastName);
    if (professionalSpecialist != null) {
      ProfessionalSpecialistTo1 professionalSpecialistTo1 =
          converter.getAsTransferObject(getLoggedInInfo(), professionalSpecialist);
      response = Response.ok().status(200).entity(professionalSpecialistTo1).build();
    } else {
      response = Response.ok().status(404).entity("Professional Specialist Not Found.").build();
    }
    return response;
  }

  private Response validateFields(ProfessionalSpecialistTo1 professionalSpecialistTo1) {
    String phoneRegex = "^(1|9[19]?)?\\(?[0-9]{3}\\)?[ ,-]?[0-9]{3}[ ,-]?[0-9]{4}$";
    Response response = Response.ok().status(200).entity("Fields valid").build();

    if (StringUtils.isNullOrEmpty(professionalSpecialistTo1.getStreetAddress())) {
      return Response.ok().status(400).entity("Error: Address required.").build();
    }
    if (StringUtils.isNullOrEmpty(professionalSpecialistTo1.getFirstName())) {
      return Response.ok().status(400).entity("Error: First Name required.").build();
    }
    if (StringUtils.isNullOrEmpty(professionalSpecialistTo1.getLastName())) {
      return Response.ok().status(400).entity("Error: Last name required.").build();
    }
    if (StringUtils.isNullOrEmpty(professionalSpecialistTo1.getPhoneNumber())) {
      return Response.ok().status(400).entity("Error: Phone number required.").build();
    }
    if (StringUtils.isNullOrEmpty(professionalSpecialistTo1.getReferralNo())) {
      return Response.ok().status(400).entity("Error: Referral No. required").build();
    }
    if (!ProfessionalSpecialistUtil.isReferralNoValid(professionalSpecialistTo1.getReferralNo())) {
      return Response.ok()
          .status(400)
          .entity(
              "Error: Specified Referral No.: "
                  + professionalSpecialistTo1.getReferralNo()
                  + " is invalid")
          .build();
    }
    if (!professionalSpecialistTo1.getPhoneNumber().matches(phoneRegex)) {
      return Response.ok()
          .status(400)
          .entity(
              "Error: Phone number must be 10 or 11 digits with following formats: \n"
                  + "###-###-####\n"
                  + "### ### ####\n"
                  + "(###)###-####\n"
                  + "(###)#######\n"
                  + "##########\n")
          .build();
    }
    if (!StringUtils.isNullOrEmpty(professionalSpecialistTo1.getFaxNumber())) {
      if (!FaxUtils.isValidFaxNumber(professionalSpecialistTo1.getFaxNumber())) {
        return Response.status(400)
            .entity(
                "Error: Invalid Fax Number. "
                    + "Number must be in range of 10-12 digits with following formats:\n"
                    + "###-###-####\n"
                    + "1-###-###-###\n"
                    + "9-###-###-####\n"
                    + "9-1-###-###-####\n"
                    + "9-9-###-###-####")
            .build();
      }
    }
    return response;
  }
}
