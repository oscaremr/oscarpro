package org.oscarehr.ws.rest.util;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.util.Date;

public class UnixTimestampAdapter extends TypeAdapter<Date> {
  private static final int MILLISECONDS_IN_SECOND = 1000;

  @Override
  public void write(JsonWriter out, Date value) throws IOException {
    if (value == null) {
      out.nullValue();
      return;
    }
    out.value(value.getTime() / MILLISECONDS_IN_SECOND);
  }

  @Override
  public Date read(JsonReader in) throws IOException {
    if (in.peek() == JsonToken.NULL) {
      in.nextNull();
      return null;
    }
    return new Date(in.nextLong() * MILLISECONDS_IN_SECOND);
  }
}
