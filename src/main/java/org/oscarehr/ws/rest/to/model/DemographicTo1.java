/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.ws.rest.to.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement
public class DemographicTo1 implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer demographicNo;
	private AddressTo1 address = new AddressTo1();
	private String phone;
	private String alternativePhone;
	private String patientStatus;
	private Date patientStatusDate;
	private String rosterStatus;
	private String providerNo;
	private String myOscarUserName;
	private String hin;
	private String ver;
	private Date dateOfBirth;
	private String dobYear;
	private String dobMonth;
	private String dobDay;
	private String sex;
	private String sexDesc;
	private Date dateJoined;
	private String familyDoctor;
	private String familyPhysician;
	private String firstName;
	private String lastName;
	private Date hcRenewDate;
	private String pcnIndicator;
	private Date endDate;
	private String hcType;
	private String chartNo;
	private String email;
	private Date effDate;
	private Date rosterDate;
	private Date rosterTerminationDate;
	private String rosterTerminationReason;
	private String links;
	private String alias;
	private AddressTo1 previousAddress = new AddressTo1();
	private String children;
	private String sourceOfIncome;
	private String citizenship;
	private String sin;
	private String anonymous;
	private String spokenLanguage;
	private int activeCount;
	private int hsAlertCount;
	private String displayName;
	private ProviderTo1 provider;
	private String lastUpdateUser;
	private Date lastUpdateDate;
	private String title;
	private String officialLanguage;
	private String countryOfOrigin;
	private String newsletter;
	private String nurse;
	private String resident;
	private String alert;
	private String midwife;
	private String notes;
	private Integer waitingListID;
	private String waitingListNote;
	private Date onWaitingListSinceDate;
	private AgeTo1 age;
	private Integer admissionProgramId;
	private String prefName = "";
	private Boolean consentToUseEmailForCare;
	private Boolean consentToUseEmailForEOrder;
	private Integer primarySystemId;

	private List<DemographicContactFewTo1> demoContacts = new ArrayList<>();
	private List<DemographicContactFewTo1> demoContactPros = new ArrayList<>();
	private List<DemographicExtTo1> extras = new ArrayList<>();
	private List<ProviderTo1> doctors = new ArrayList<>();
	private List<ProviderTo1> nurses = new ArrayList<>();
	private List<ProviderTo1> midwives = new ArrayList<>();
	private List<ProfessionalSpecialistTo1> referralDoctors = new ArrayList<>();
	private List<WaitingListNameTo1> waitingListNames = new ArrayList<>();
	private List<StatusValueTo1> patientStatusList = new ArrayList<>();
	private List<StatusValueTo1> rosterStatusList = new ArrayList<>();
	private List<AllergyTo1> allergies = new ArrayList<>();
	private List<MeasurementTo1> measurements = new ArrayList<>();
	private List<ConsultationRequestTo1> consultationRequests = new ArrayList<>();
	private List<ConsultationResponseTo1> consultationResponses = new ArrayList<>();
	private List<NoteTo1> encounterNotes = new ArrayList<>();
	private List<DocumentTo1> documents = new ArrayList<>();
	private List<String> medicationSummary = Collections.emptyList();
}
