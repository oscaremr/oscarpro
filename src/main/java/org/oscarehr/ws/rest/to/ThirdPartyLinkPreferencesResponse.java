/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.ws.rest.to;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Objects;

@XmlRootElement
public class ThirdPartyLinkPreferencesResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    private Boolean tpLinkEnabled;
    private String tpLinkType;
    private String tpLinkDisplay;

    public ThirdPartyLinkPreferencesResponse() { }

    public ThirdPartyLinkPreferencesResponse(Boolean tpLinkEnabled, String tpLinkType, String tpLinkDisplay) {
        this.tpLinkEnabled = tpLinkEnabled;
        this.tpLinkType = tpLinkType;
        this.tpLinkDisplay = tpLinkDisplay;
    }

    public Boolean getTpLinkEnabled() {
        return tpLinkEnabled;
    }

    public void setTpLinkEnabled(Boolean tpLinkEnabled) {
        this.tpLinkEnabled = tpLinkEnabled;
    }

    public String getTpLinkType() {
        return tpLinkType;
    }

    public void setTpLinkType(String tpLinkType) {
        this.tpLinkType = tpLinkType;
    }

    public String getTpLinkDisplay() {
        return tpLinkDisplay;
    }

    public void setTpLinkDisplay(String tpLinkDisplay) {
        this.tpLinkDisplay = tpLinkDisplay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThirdPartyLinkPreferencesResponse that = (ThirdPartyLinkPreferencesResponse) o;
        return tpLinkEnabled.equals(that.tpLinkEnabled)
                && tpLinkType.equals(that.tpLinkType)
                && tpLinkDisplay.equals(that.tpLinkDisplay);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tpLinkEnabled, tpLinkType, tpLinkDisplay);
    }
}
