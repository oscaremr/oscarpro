package org.oscarehr.ws.rest.to.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ScheduleTemplateTo implements Serializable {
    private String providerNo;
    private String date;
    private ArrayList<ScheduleTimeslotTo> timeslotList;

    public String getProviderNo() {
        return providerNo;
    }

    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<ScheduleTimeslotTo> getTimeslotList() {
        return timeslotList;
    }

    public void setTimeslotList(ArrayList<ScheduleTimeslotTo> timeslotList) {
        this.timeslotList = timeslotList;
    }
}
