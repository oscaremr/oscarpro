/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.ws.rest.to.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "emailLog")
public class EmailLogTo1 implements Serializable {

	@JsonFormat(shape= JsonFormat.Shape.OBJECT)
	public enum EmailSentStatus {
		SUCCESS ("Sent"),
		FAILURE ("Not Sent");

		String code;

		EmailSentStatus(String code) {
			this.code = code;
		}

		public String getCode() {
			return this.code;
		}
	}

	public EmailLogTo1() {
	}

	public EmailLogTo1(Date sentDate) {
		this.sentDate = sentDate;
	}

	private Integer id;
	private Date sentDate;
	private String status;
	private String content;
	private String emailType;
	private DemographicTo1 recipient;
	private List<EmailAttachmentTo1> attachments = new ArrayList<EmailAttachmentTo1>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public DemographicTo1 getRecipient() {
		return recipient;
	}

	public void setRecipient(DemographicTo1 recipient) {
		this.recipient = recipient;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public List<EmailAttachmentTo1> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<EmailAttachmentTo1> attachments) {
		this.attachments = attachments;
	}
}
