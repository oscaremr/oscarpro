/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.ws.rest.to;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class OceanSettingTo1 {
  private String settings;

  public OceanSettingTo1() {
  }

  public OceanSettingTo1(String settings) {
    this.settings = settings;
  }

  public String getSettings() {
    return settings;
  }
  public void setSettings(String settings) {
    this.settings = settings;
  }
}