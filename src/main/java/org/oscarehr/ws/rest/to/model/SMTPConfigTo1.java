/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.ws.rest.to.model;

import java.io.Serializable;

public class SMTPConfigTo1 implements Serializable {

	private Integer id;
	private String url;
	private String username;
	private String password;
	private Integer port;
	private Boolean useSecurity;
	private Boolean enableEmail;
	private String replyToEmail;
	private String displayName;
	private String signature;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Boolean getUseSecurity() {
		return useSecurity;
	}

	public void setUseSecurity(Boolean useSecurity) {
		this.useSecurity = useSecurity;
	}

	public Boolean getEnableEmail() {
		return enableEmail;
	}

	public void setEnableEmail(Boolean enableEmail) {
		this.enableEmail = enableEmail;
	}

	public String getReplyToEmail() {
		return replyToEmail;
	}

	public void setReplyToEmail(String replyToEmail) {
		this.replyToEmail = replyToEmail;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}
}
