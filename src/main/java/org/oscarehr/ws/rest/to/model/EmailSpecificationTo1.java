/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.ws.rest.to.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "emailSpecification")
public class EmailSpecificationTo1 {
	
	public enum EmailType {
		APPOINTMENT_LIST("AppointmentList"),
		APPOINTMENT_CALENDAR("AppointmentCalendar"),
		AUTOMATED_APPOINTMENT_REMINDER("AutomatedAppointmentReminder"),
		CUSTOM_TEMPLATE("CustomTemplate"),
		PATIENT_INTAKE_FORM_REQUEST("PatientIntakeFormRequest"),
		LAB_REQUISITION("LabRequisition");

        String code;

        EmailType(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }
	
	private String emailTemplateId;
	private EmailType emailType;
	private List<String> demographicNoList;
    private Integer startYear;
    private Integer startMonth;
    private Integer numberOfMonths;
    private Boolean ignoreAllowAppointmenReminderPreference;
    private Integer authTokenId;
	
	public EmailSpecificationTo1() {
		super();
		this.demographicNoList = new ArrayList<>();
	}

	public EmailSpecificationTo1(String emailTemplateId, EmailType emailType, List<String> demographicNoList, 
			Integer startYear, Integer startMonth, Integer numberOfMonths, Boolean ignoreAllowAppointmenReminderPreference) {
		super();
		this.emailTemplateId = emailTemplateId;
		this.emailType = emailType;
		this.demographicNoList = demographicNoList;
		this.startYear = startYear;
		this.startMonth = startMonth;
		this.numberOfMonths = numberOfMonths;
		this.ignoreAllowAppointmenReminderPreference = ignoreAllowAppointmenReminderPreference;
	}

	public String getEmailTemplateId() {
		return emailTemplateId;
	}

	public void setEmailTemplateId(String emailTemplateId) {
		this.emailTemplateId = emailTemplateId;
	}

	public EmailType getEmailType() {
		return emailType;
	}

	public void setEmailType(EmailType emailType) {
		this.emailType = emailType;
	}

	public List<String> getDemographicNoList() {
		return demographicNoList;
	}

	public void setDemographicNoList(List<String> demographicNoList) {
		this.demographicNoList = demographicNoList;
	}
	
	public Integer getStartYear() {
		return startYear;
	}

	public void setStartYear(Integer startYear) {
		this.startYear = startYear;
	}
	
	public Integer getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(Integer startMonth) {
		this.startMonth = startMonth;
	}
	
	public Integer getNumberOfMonths() {
		return numberOfMonths;
	}

	public void setNumberOfMonths(Integer numberOfMonths) {
		this.numberOfMonths = numberOfMonths;
	}

	public Boolean getIgnoreAllowAppointmenReminderPreference() {
		return ignoreAllowAppointmenReminderPreference;
	}

	public void setIgnoreAllowAppointmenReminderPreference(Boolean ignoreAllowAppointmenReminderPreference) {
		this.ignoreAllowAppointmenReminderPreference = ignoreAllowAppointmenReminderPreference;
	}

	public Integer getAuthTokenId() {
		return authTokenId;
	}

	public void setAuthTokenId(Integer authTokenId)
	{
		this.authTokenId = authTokenId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((demographicNoList == null) ? 0 : demographicNoList.hashCode());
		result = prime * result + ((emailTemplateId == null) ? 0 : emailTemplateId.hashCode());
		result = prime * result + ((emailType == null) ? 0 : emailType.hashCode());
		result = prime * result + ((ignoreAllowAppointmenReminderPreference == null) ? 0 : ignoreAllowAppointmenReminderPreference.hashCode());
		result = prime * result + ((numberOfMonths == null) ? 0 : numberOfMonths.hashCode());
		result = prime * result + ((startMonth == null) ? 0 : startMonth.hashCode());
		result = prime * result + ((startYear == null) ? 0 : startYear.hashCode());
		result = prime * result + ((authTokenId == null ) ? 0 : authTokenId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		EmailSpecificationTo1 other = (EmailSpecificationTo1) obj;
		if (demographicNoList == null) {
			if (other.demographicNoList != null) return false;
		} else if (!demographicNoList.equals(other.demographicNoList)) return false;
		if (emailTemplateId == null) {
			if (other.emailTemplateId != null) return false;
		} else if (!emailTemplateId.equals(other.emailTemplateId)) return false;
		if (emailType != other.emailType) return false;
		if (ignoreAllowAppointmenReminderPreference == null) {
			if (other.ignoreAllowAppointmenReminderPreference != null) return false;
		} else if (!ignoreAllowAppointmenReminderPreference.equals(other.ignoreAllowAppointmenReminderPreference)) return false;
		if (numberOfMonths == null) {
			if (other.numberOfMonths != null) return false;
		} else if (!numberOfMonths.equals(other.numberOfMonths)) return false;
		if (startMonth == null) {
			if (other.startMonth != null) return false;
		} else if (!startMonth.equals(other.startMonth)) return false;
		if (startYear == null) {
			if (other.startYear != null) return false;
		} else if (!startYear.equals(other.startYear)) return false;

		if( authTokenId == null ) {
			if( other.authTokenId != null ) {
				return false;
			}
		} else if( !authTokenId.equals(other.authTokenId)) {
			return false;
		}

		return true;
	}

	@Override
	public String toString() {
		return "EmailSpecificationTo1 [emailTemplateId=" + emailTemplateId + ", emailType=" + emailType + ", demographicNoList=" + demographicNoList + ", startYear=" + startYear + ", startMonth=" + startMonth + ", numberOfMonths=" + numberOfMonths + ", ignoreAllowAppointmenReminderPreference=" + ignoreAllowAppointmenReminderPreference + ", authTokenId="+authTokenId+"]";
	}
}
