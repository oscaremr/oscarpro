package org.oscarehr.ws.rest.to.model;

import java.io.Serializable;

public class ScheduleTimeslotTo implements Serializable {
    private String dateTime;
    private String scheduleTemplateCode;
    private String duration;
    private boolean availability;

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getScheduleTemplateCode() {
        return scheduleTemplateCode;
    }

    public void setScheduleTemplateCode(String scheduleTemplateCode) {
        this.scheduleTemplateCode = scheduleTemplateCode;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }
}
