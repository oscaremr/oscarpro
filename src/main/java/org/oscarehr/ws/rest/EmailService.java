/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.ws.rest;

import java.io.UnsupportedEncodingException;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.oscarehr.common.model.SMTPConfig;
import org.oscarehr.managers.EmailManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.ws.rest.conversion.EmailConfigConverter;
import org.oscarehr.ws.rest.to.model.SMTPConfigTo1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.sf.json.JSONObject;

@Path("/email")
@Component("emailService")
public class EmailService extends AbstractServiceImpl {
	
	private Logger logger = MiscUtils.getLogger();
	
	@Autowired
    EmailManager emailManager;
	
	@GET
    @Path("/getEmailConfig")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public SMTPConfigTo1 getEmailConfiguration() {
        LoggedInInfo loggedInInfo = getLoggedInInfo();
        SMTPConfig smtpConfig = emailManager.getEmailConfig(loggedInInfo);

        SMTPConfigTo1 smtpConfigTo1 = null;
        if (smtpConfig != null) {
            smtpConfigTo1 = new EmailConfigConverter().getAsTransferObject(loggedInInfo, smtpConfig);
        }

        return smtpConfigTo1;
    }
	
	@POST
    @Path("/saveEmailConfig")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response saveEmailConfiguration(SMTPConfigTo1 smtpConfigTo1) throws UnsupportedEncodingException {
        Response response;
        smtpConfigTo1 = emailManager.saveEmailConfig(getLoggedInInfo(), smtpConfigTo1);

        if (smtpConfigTo1.getId() != null) {
            response = Response.status(Response.Status.OK).entity(smtpConfigTo1).build();
        } else {
            response = Response.status(Response.Status.CONFLICT).entity(new JSONObject().accumulate("error", "Problem saving the SMTP Configuration")).build();
        }

        return response;
    }
	
	@POST
    @Path("/testEmailConfig")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response testEmailConfiguration(SMTPConfigTo1 smtpConfigTo1) {
        Response response;
        boolean success = false;

        success = emailManager.testEmailConfig(getLoggedInInfo(), smtpConfigTo1);

        if (success) {
            response = Response.status(Response.Status.OK).entity(smtpConfigTo1).build();
        } else {
            response = Response.status(Response.Status.CONFLICT).entity(new JSONObject().accumulate("error", "Problem with the SMTP Configuration")).build();
        }
        return response;
    }
}