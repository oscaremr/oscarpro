/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.ws.rest;

import java.nio.charset.StandardCharsets;
import java.util.List;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import lombok.val;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.dao.EReferAttachmentDao;
import org.oscarehr.common.dao.EncounterTemplateDao;
import org.oscarehr.common.dao.OceanSettingDao;
import org.oscarehr.common.model.OceanWorkflowTypeEnum;
import org.oscarehr.common.model.EncounterTemplate;
import org.oscarehr.common.model.OceanSetting;
import org.oscarehr.managers.ConsultationManager;
import org.oscarehr.ws.rest.to.OceanSettingTo1;
import org.oscarehr.ws.rest.to.model.OceanApiAttachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import oscar.log.LogAction;

@Component("OceanService")
@Path("/ocean")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OceanService extends AbstractServiceImpl {

  @Autowired
  private ConsultationManager consultationManager;

  @Autowired
  OceanSettingDao oceanSettingDao;
  
  @Autowired
  EncounterTemplateDao encounterTemplateDao;
  
  @Autowired
  EReferAttachmentDao eReferAttachmentDao;

  @GET
  @Path("/attachments")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getAttachments(
      final @Context HttpServletRequest request,
      final @QueryParam("demographicNo") Integer demographicNumber,
      final @QueryParam("type") String type) {
    if (demographicNumber == null) {
      return Response.status(Status.INTERNAL_SERVER_ERROR)
          .entity("Demographic number is required")
          .build();
    }

    val upperCaseType = convertToUpperCase(type);
    if (!StringUtils.isEmpty(upperCaseType) && !isValidOceanWorkflowType(upperCaseType)) {
      return Response.status(Status.INTERNAL_SERVER_ERROR)
          .entity("Invalid type parameter. It should be either 'ereferral' or 'messenger'")
          .build();
    }
    
    val attachmentType = StringUtils.isEmpty(upperCaseType)
        ? null
        : OceanWorkflowTypeEnum.valueOf(upperCaseType);

    Response response;
    try {
      val attachments =
          consultationManager.getLatestDemographicEReferOceanApiAttachments(
              getLoggedInInfo(), request, demographicNumber, attachmentType);
        response = Response.ok().entity(attachments).build();
    } catch (Exception e) {
      response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("An error occurred while generating the attachment data").build();
    }

    return response;
  }
  
  @GET
  @Path("/getSettings")
  public Response getOceanSettings() {
    OceanSetting oceanSetting = oceanSettingDao.getSettings();
    if (oceanSetting == null) {
      oceanSetting = getSettingsFromEncounterTemplate();
    }
    String settings = oceanSetting != null 
        ? StringUtils.trimToEmpty(oceanSetting.getSettings())
        : "";
    OceanSettingTo1 settingResponse = new OceanSettingTo1(settings);
    return Response.ok().entity(settingResponse).build();
  }

  @POST
  @Path("/saveSettings")
  public Response saveOceanSettings(OceanSettingTo1 oceanSettingT) {
    final int TEXT_SIZE = 65_535;
    if (oceanSettingT == null || StringUtils.isEmpty(oceanSettingT.getSettings())) {
      return Response.status(Status.BAD_REQUEST)
          .entity("No settings were provided")
          .build();
    } else if (oceanSettingT.getSettings().getBytes(StandardCharsets.UTF_8).length > TEXT_SIZE) {
      return Response.status(Status.BAD_REQUEST)
          .entity("Settings cannot exceed 65,535 bytes")
          .build();
    }

    OceanSetting oceanSetting = oceanSettingDao.getSettings();
    if (oceanSetting == null) {
      oceanSetting = new OceanSetting();
    }
    oceanSetting.setSettings(oceanSettingT.getSettings());
    oceanSettingDao.saveEntity(oceanSetting);
    LogAction.addLog(getLoggedInInfo(), "OceanService.saveOceanSettings", "", "", null, "");

    return Response.ok().build();
  }

  /** Get and save OceanSetting from existing EncounterTemplate for __Ocean */
  private OceanSetting getSettingsFromEncounterTemplate() {
    List<EncounterTemplate> templates = encounterTemplateDao.findByName("__Ocean");
    if (templates.isEmpty()) {
      return null;
    }
    
    OceanSetting oceanSetting = new OceanSetting(templates.get(0).getEncounterTemplateValue());
    oceanSettingDao.saveEntity(oceanSetting);
    return oceanSetting;
  }
  
  private boolean isValidOceanWorkflowType(final String type) {
    try {
      OceanWorkflowTypeEnum.valueOf(type);
      return true;
    } catch (IllegalArgumentException e) {
      return false;
    }
  }

  @Nullable
  private String convertToUpperCase(final String type) {
    return StringUtils.isEmpty(type) ? null : type.toUpperCase();
  }
}
