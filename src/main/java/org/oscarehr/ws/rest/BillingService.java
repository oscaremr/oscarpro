/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.ws.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.PathParam;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.AppointmentArchiveDao;
import org.oscarehr.common.dao.BillingONCHeader1Dao;
import org.oscarehr.common.dao.BillingONExtDao;
import org.oscarehr.common.dao.BillingONRepoDao;
import org.oscarehr.common.dao.BillingOnTransactionDao;
import org.oscarehr.common.dao.BillingServiceDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DxresearchDAO;
import org.oscarehr.common.dao.OscarAppointmentDao;
import org.oscarehr.common.model.Appointment;
import org.oscarehr.common.model.BillingONCHeader1;
import org.oscarehr.common.model.BillingONExt;
import org.oscarehr.common.model.BillingONItem;
import org.oscarehr.common.model.BillingOnTransaction;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.Dxresearch;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.service.BillingONService;
import org.oscarehr.managers.BillingManager;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.DateRange;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.ws.rest.conversion.ProviderConverter;
import org.oscarehr.ws.rest.conversion.ServiceTypeConverter;
import org.oscarehr.ws.rest.to.AbstractSearchResponse;
import org.oscarehr.ws.rest.to.GenericRESTResponse;
import org.oscarehr.ws.rest.to.InvoiceCreateResponse;
import org.oscarehr.ws.rest.to.model.BillingInvoiceTo1;
import org.oscarehr.ws.rest.to.model.BillingItemTo1;
import org.oscarehr.ws.rest.to.model.ProviderTo1;
import org.oscarehr.ws.rest.to.model.ServiceTypeTo;
import org.springframework.beans.factory.annotation.Autowired;

import org.oscarehr.managers.BillingONManager;
import org.oscarehr.common.model.BillingAutoRules;
import org.oscarehr.common.model.BillingAutoRulesMap;
import org.oscarehr.common.model.BillingAutoRulesExt;
import org.oscarehr.common.model.ClinicLocation;
import org.oscarehr.ws.rest.to.BillingAutoRuleResponse;
import org.oscarehr.ws.rest.to.model.BillingAutoRuleTo1;
import org.oscarehr.ws.rest.to.BillingAutoRuleMapResponse;
import org.oscarehr.ws.rest.to.model.BillingAutoRuleMapTo1;
import org.springframework.beans.BeanUtils;
import org.oscarehr.common.dao.BillingAutoRulesExtDao;
import org.oscarehr.common.dao.ClinicLocationDao;

import oscar.OscarProperties;
import oscar.log.LogAction;
import oscar.log.LogConst;
import oscar.oscarBilling.ca.on.data.BillingDataHlp;
import oscar.oscarBilling.ca.on.pageUtil.BillingCorrectionPrep;
import oscar.oscarBilling.ca.on.pageUtil.BillingCorrectionUtil;
import oscar.util.ChangedField;
import oscar.util.ConversionUtils;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Arrays;

@Path("/billing")
public class BillingService extends AbstractServiceImpl {

	@Autowired
    private BillingManager billingManager;
	@Autowired
    private ProviderDao providerDao;
    @Autowired
    private DemographicDao demographicDao;
    @Autowired
    private BillingONCHeader1Dao billingONCHeader1Dao;
    @Autowired
    private BillingServiceDao billingServiceDao;
    @Autowired
    private OscarAppointmentDao appointmentDao;
    @Autowired
    private AppointmentArchiveDao appointmentArchiveDao;
    @Autowired
    private DxresearchDAO dxresearchDAO;
    @Autowired
    private BillingONExtDao billingONExtDao;
    @Autowired
    private BillingOnTransactionDao billingOnTransactionDao;
    @Autowired
    private BillingONService billingONService;
    @Autowired
    private SecurityInfoManager securityInfoManager;

	@Autowired
	BillingONManager billingONManager;

	@Autowired
	private BillingAutoRulesExtDao billingAutoRuleExtDao;

	@Autowired
	ClinicLocationDao clinicLocationDao;


	private OscarProperties oscarProperties = OscarProperties.getInstance();

	@GET
	@Path("/uniqueServiceTypes")
	@Produces("application/json")
	public AbstractSearchResponse<ServiceTypeTo> getUniqueServiceTypes(@QueryParam("type")  String type) {
		AbstractSearchResponse<ServiceTypeTo> response = new AbstractSearchResponse<ServiceTypeTo>();
		ServiceTypeConverter converter = new ServiceTypeConverter();
		if(type == null) {
			response.setContent(converter.getAllAsTransferObjects(getLoggedInInfo(),billingManager.getUniqueServiceTypes(getLoggedInInfo())));
		} else {
			response.setContent(converter.getAllAsTransferObjects(getLoggedInInfo(),billingManager.getUniqueServiceTypes(getLoggedInInfo(),type)));
		}
		response.setTotal(response.getContent().size());
		return response;

	}

    @GET
    @Path("/billingRegion")
    @Produces("application/json")
    public GenericRESTResponse billingRegion() {
        boolean billRegionSet = true;
        String billRegion = oscarProperties.getProperty("billregion", "").trim().toUpperCase();
        if(billRegion.isEmpty()){
            billRegionSet = false;
        }
        return new GenericRESTResponse(billRegionSet, billRegion);
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private class FilterCriteria {
        String providerNo;
        DateRange dateRange;
        String dateBegin;
        String dateEnd;
    }

    private DateRange convertStringToDateRange(String dateBegin, String dateEnd) {
        DateRange dateRange = null;
        if (StringUtils.isEmpty(dateEnd)) {
            dateEnd = new Date().toString();
        }
        if (StringUtils.isEmpty(dateBegin)) {
            dateRange = new DateRange(null, ConversionUtils.fromDateString(dateEnd));
        } else {
            dateRange = new DateRange(ConversionUtils.fromDateString(dateBegin), ConversionUtils.fromDateString(dateEnd));
        }
        return dateRange;
    }

    // Get all demographics, for selected provider, within the date range, status = 'O' in billing_on_cheader1 table
    @GET
    @Path("/getDemographicsWithFilters")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<BillingONCHeader1> getDemographicsWithFilters(@QueryParam("providerNo") String providerNo,
            @QueryParam("dateBegin") String dateBegin, @QueryParam("dateEnd") String dateEnd) {
        DateRange dateRange = convertStringToDateRange(dateBegin, dateEnd);
        List<BillingONCHeader1> list = billingONCHeader1Dao.getDemographicsWithFilters(providerNo, dateRange);
        return list;
    }

    @GET
    @Path("/getDemographicsWithNoFilters")
    @Produces(MediaType.APPLICATION_JSON)
    public List<BillingONCHeader1> getDemographicsWithNoFilters() {
        List<BillingONCHeader1> list = billingONCHeader1Dao.getDemographicsWithNoFilters();
        return list;
    }

    @GET
    @Path("/defaultView")
    @Produces("application/json")
    public GenericRESTResponse defaultView() {
        boolean defaultViewSet = true;
        String defaultView = oscarProperties.getProperty("default_view", "").trim();
        if(defaultView.isEmpty()){
        	defaultViewSet = false;
        }
        return new GenericRESTResponse(defaultViewSet, defaultView);
    }

    @GET
    @Path("/getBillingProviders")
    @Produces("application/json")
    public AbstractSearchResponse<ProviderTo1> getBillingProviders() {
        AbstractSearchResponse<ProviderTo1> response = new AbstractSearchResponse<ProviderTo1>();

        LoggedInInfo loggedInInfo = getLoggedInInfo();
	    List<ProviderTo1> billableProviderResults = new ArrayList<ProviderTo1>();
        ProviderConverter providerConverter = new ProviderConverter();

        for (Provider provider : providerDao.getProvidersWithNonEmptyCredentials()) {
            billableProviderResults.add(providerConverter.getAsTransferObject(loggedInInfo, provider));
        }

        response.setContent(billableProviderResults);
        response.setTotal(billableProviderResults.size());

        return response;
    }

    @POST
    @Path("/createInvoice")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public GenericRESTResponse createInvoice(BillingInvoiceTo1 invoiceRequest) {
        LoggedInInfo loggedInInfo = getLoggedInInfo();
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_billing", "w", invoiceRequest.getDemographicNo())) {
            throw new IllegalArgumentException("Missing required security object (_billing)");
        }
        Provider loggedInProvider = loggedInInfo.getLoggedInProvider();

        try {
            Provider billingProvider = providerDao.getProvider(invoiceRequest.getProviderNo());
            Demographic billedDemographic = demographicDao.getDemographicById(invoiceRequest.getDemographicNo());
            Appointment billedAppointment = appointmentDao.find(invoiceRequest.getAppointmentNo());

            BillingONCHeader1 invoiceToFill = invoiceRequest.toBillingONCHeader1();
            invoiceToFill.setHeaderId(0);
            BillingONCHeader1 invoice = prepareInvoiceRequestForSave(invoiceRequest, invoiceToFill, billingProvider, billedDemographic, billedAppointment);
            saveInvoice(invoice, billedDemographic, loggedInProvider, billedAppointment);

            if (invoice.getId() != null) {
                return new InvoiceCreateResponse(true, invoice.getId().toString(), invoice.getId());
            } else {
                return new InvoiceCreateResponse(false, "Error creating invoice.", null);
            }
        } catch (IllegalArgumentException e) {
            return new InvoiceCreateResponse(false, e.getMessage(), null);
        }

    }

    @POST
    @Path("/updateInvoice/{invoiceId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public GenericRESTResponse updateInvoice(BillingInvoiceTo1 invoiceUpdateRequest, @PathParam("invoiceId") Integer invoiceId) {
        LoggedInInfo loggedInInfo = getLoggedInInfo();
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_billing", "w", invoiceUpdateRequest.getDemographicNo())) {
            throw new IllegalArgumentException("Missing required security object (_billing)");
        }
        BillingONCHeader1 existingBillingHeader1 = billingONCHeader1Dao.find(invoiceId);
        if (existingBillingHeader1 == null) {
            throw new IllegalArgumentException("Cannot update invoice, past invoice with id " + invoiceId + " does not exist.");
        }

        try {
            Provider billingProvider = providerDao.getProvider(invoiceUpdateRequest.getProviderNo());
            Demographic billedDemographic = demographicDao.getDemographicById(invoiceUpdateRequest.getDemographicNo());
            Appointment billedAppointment = appointmentDao.find(invoiceUpdateRequest.getAppointmentNo());

            BillingONCHeader1 oldBillingHeader1 = new BillingONCHeader1(existingBillingHeader1);

            BillingONCHeader1 updateBillingHeader1 = prepareInvoiceRequestForSave(invoiceUpdateRequest, existingBillingHeader1, billingProvider, billedDemographic, billedAppointment);

            BillingONService billingONService = (BillingONService) SpringUtils.getBean("billingONService");
            billingONService.updateTotal(updateBillingHeader1);

            //Add Existing state of Invoice to Billing Repository
            BillingONRepoDao billRepoDao = (BillingONRepoDao) SpringUtils.getBean("billingONRepoDao");
            billRepoDao.createBillingONCHeader1Entry(updateBillingHeader1, getLocale());

            billingONCHeader1Dao.merge(updateBillingHeader1);
            List<ChangedField> changedFields = ChangedField.getChangedFieldsAndValues(oldBillingHeader1, updateBillingHeader1);


            // set old 'useBillTo' billing ext to archived
            BillingONExt billExt = billingONExtDao.getUseBillTo(updateBillingHeader1);
            if (billExt != null) {
                billExt.setStatus('0');
                billingONExtDao.merge(billExt);
            }

            if (!changedFields.isEmpty()) {
                LogAction.addLog(loggedInInfo, LogConst.UPDATE, LogConst.CON_BILL,
                    "billingNo=" + invoiceId, String.valueOf(updateBillingHeader1.getDemographicNo()), changedFields);
            }


            return new InvoiceCreateResponse(true, invoiceId.toString(), invoiceId);
        } catch (IllegalArgumentException e) {
            return new InvoiceCreateResponse(false, e.getMessage(), null);
        }
    }

    @POST
    @Path("/unbillInvoice/{invoiceId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public GenericRESTResponse unbillInvoice(@PathParam("invoiceId") Integer invoiceId, @QueryParam("newApptStatus") String newApptStatus) {
        LoggedInInfo loggedInInfo = getLoggedInInfo();
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_billing", "w", null)) {
            throw new IllegalArgumentException("Missing required security object (_billing)");
        }
        BillingONCHeader1 existingBillingHeader1 = billingONCHeader1Dao.find(invoiceId);
        if (existingBillingHeader1 == null) {
            throw new IllegalArgumentException("Cannot update invoice, past invoice with id " + invoiceId + " does not exist.");
        }
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_billing", "w", existingBillingHeader1.getDemographicNo())) {
            throw new IllegalArgumentException("Missing required security object (_billing) for the specified demographic");
        }

        Integer appointmentNo = existingBillingHeader1.getAppointmentNo();
        BillingCorrectionPrep dbObj = new BillingCorrectionPrep();
        if (dbObj.deleteBilling(String.valueOf(invoiceId),"D", loggedInInfo.getLoggedInProviderNo()) && appointmentNo != null) {
            String logData = "";
            Appointment appointment = appointmentDao.find(appointmentNo);
            appointmentArchiveDao.archiveAppointment(appointment);
            if (appointment != null) {
                appointment.setStatus(newApptStatus);
                appointment.setLastUpdateUser(loggedInInfo.getLoggedInProviderNo());
                appointmentDao.merge(appointment);
                logData = "appointment_no=" + appointment.getId();
            }
            LogAction.addLog(loggedInInfo, "unbill", LogConst.CON_BILL, "billingId=" + invoiceId, String.valueOf(existingBillingHeader1.getDemographicNo()), logData);
        }

        return new GenericRESTResponse(true, String.valueOf(invoiceId));
    }

    private BillingONCHeader1 prepareInvoiceRequestForSave(BillingInvoiceTo1 invoiceRequest, BillingONCHeader1 invoiceToFill, Provider billingProvider, Demographic billedDemographic, Appointment billedAppointment) throws IllegalArgumentException {

        if (billingProvider == null) {
            throw new IllegalArgumentException("Cannot create invoice, billing provider with id " + invoiceRequest.getProviderNo() + " does not exist.");
        }
        if (billedDemographic == null) {
            throw new IllegalArgumentException("Cannot create invoice, billed demographic with id " + invoiceRequest.getDemographicNo() + " does not exist.");
        }
        if (invoiceRequest.getBillingItems().isEmpty()) {
            throw new IllegalArgumentException("Cannot create invoice, no items billed.");
        }

        // Set invoice demographic info
        invoiceToFill.setHin(billedDemographic.getHin());
        invoiceToFill.setVer(billedDemographic.getVer());
        invoiceToFill.setDob(billedDemographic.getFormattedDob().replaceAll("-", ""));
        invoiceToFill.setDemographicName(billedDemographic.getDisplayName());
        if ("F".equals(billedDemographic.getSex())) {
            invoiceToFill.setSex("2");
        } else {
            invoiceToFill.setSex("1");
        }

        // Set invoice billing provider info
        invoiceToFill.setProviderOhipNo(billingProvider.getOhipNo());
        invoiceToFill.setProviderRmaNo(billingProvider.getRmaNo());
        invoiceToFill.setAsstProviderNo("");
        invoiceToFill.setCreator(billingProvider.getPractitionerNo());

        // Set clinic info
        invoiceToFill.setApptProviderNo("none");
        if (billedAppointment != null) { invoiceToFill.setApptProviderNo(billedAppointment.getProviderNo()); }

        invoiceToFill.setBillingDate(new Date());
        invoiceToFill.setBillingTime(new Date());
        invoiceToFill.setStatus("O");

        BigDecimal totalFee = new BigDecimal("0.00");
        List<BillingONItem> newBillingItems = new ArrayList<BillingONItem>();
        // Create a copy of the existing billing items to track new items
        List<BillingONItem> oldBillingItems = new ArrayList<BillingONItem>(invoiceToFill.getBillingItems());

        for (BillingItemTo1 requestItem : invoiceRequest.getBillingItems()) {
            org.oscarehr.common.model.BillingService service = billingServiceDao.searchBillingCode(requestItem.getServiceCode(), invoiceToFill.getProvince());
            if (service == null) {
                throw new IllegalArgumentException("Cannot create invoice, billed service number " + requestItem.getServiceCode() + " does not exist.");
            } else {
                BillingONItem billingItem = new BillingONItem();
                billingItem.setTranscId(requestItem.getTransactionId());
                billingItem.setRecId(requestItem.getRecordId());
                billingItem.setServiceCode(requestItem.getServiceCode());
                billingItem.setStatus(requestItem.getStatus());
                billingItem.setServiceDate(invoiceToFill.getBillingDate());
                billingItem.setDx(requestItem.getDx());
                billingItem.setServiceCount(requestItem.getServiceCount().toString());
                BigDecimal fee = new BigDecimal(requestItem.getServiceCount()).multiply(new BigDecimal(service.getValue()));
                totalFee = totalFee.add(fee);
                billingItem.setFee(fee.setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString());

                if (invoiceToFill.getId() != null) {
                    billingItem.setCh1Id(invoiceToFill.getId());
                }
                if (invoiceToFill.getTranscId() != null) {
                    billingItem.setTranscId(invoiceToFill.getTranscId());
                }

                if (oldBillingItems.contains(billingItem)) {
                    // Update an existing billing items  that is now modified, not deleted.
                    int index = oldBillingItems.indexOf(billingItem);
                    BillingONItem existingItem = oldBillingItems.get(index);

                    BillingCorrectionUtil.processUpdatedBillingItem(existingItem, billingItem, invoiceToFill.getBillingDate(), getLocale());
                } else {
                    invoiceToFill.getBillingItems().add(billingItem);
                }
                newBillingItems.add(billingItem);
            }
        }

        // Update status on existing billing items now removed
        for (BillingONItem oldBillingItem : oldBillingItems) {
            if (!newBillingItems.contains(oldBillingItem)){
                oldBillingItem.setStatus("D");
            }
        }

        invoiceToFill.setTotal(totalFee.setScale(2, BigDecimal.ROUND_HALF_UP));
        invoiceToFill.setPaid(new BigDecimal("0.00").setScale(2, BigDecimal.ROUND_HALF_UP));

        boolean isThirdParty = invoiceToFill.getPayProgram().substring(0, 3).matches(BillingDataHlp.BILLINGMATCHSTRING_3RDPARTY);
        if (isThirdParty && "ODP".equalsIgnoreCase(invoiceToFill.getPayProgram())) {
            String payProgram = billedDemographic.getHcType().equals("ON") ? "HCP" : "RMB";
            invoiceToFill.setPayProgram(payProgram);
        }
        return invoiceToFill;
    }

    private void saveInvoice(BillingONCHeader1 invoice, Demographic billedDemographic, Provider loggedInProvider, Appointment billedAppointment) {
        boolean isThirdParty = invoice.getPayProgram().substring(0, 3).matches(BillingDataHlp.BILLINGMATCHSTRING_3RDPARTY);

        billingONCHeader1Dao.persist(invoice);
        LogAction.addLog(invoice.getProviderNo(), LogConst.ADD, LogConst.CON_BILL,
                "billingId=" + invoice.getId(), null, String.valueOf(billedDemographic.getDemographicNo()), "appointment_no=" + invoice.getAppointmentNo());

        if (invoice.getId() > 0) {
            List<String> demographicDiagnoses = dxresearchDAO.getCodesByDemographicNo(billedDemographic.getDemographicNo());
            List<String> newDemoDiagnoses = new ArrayList<String>();

            BillingONExt ext = new BillingONExt();
            ext.setBillingNo(invoice.getId());
            ext.setDemographicNo(invoice.getDemographicNo());
            ext.setKeyVal("payee");
            ext.setValue(null);
            ext.setDateTime(new Date());
            billingONExtDao.persist(ext);

            List<BillingONItem> billingItems = invoice.getBillingItems();
            for (BillingONItem item : billingItems) {
                if (!isThirdParty) {
                    BillingOnTransaction transaction = new BillingOnTransaction();
                    try {
                        transaction.setAdmissionDate(invoice.getAdmissionDate());
                    } catch (ParseException e) {
                        transaction.setAdmissionDate(null);
                    }
                    transaction.setBillingDate(invoice.getBillingDate());
                    transaction.setBillingNotes(invoice.getComment());
                    transaction.setBillingOnItemPaymentId(item.getId());
                    transaction.setCh1Id(invoice.getId());
                    transaction.setClinic(invoice.getClinic());
                    transaction.setCreator(invoice.getCreator());
                    transaction.setDemographicNo(invoice.getDemographicNo());
                    transaction.setDxCode(item.getDx());
                    transaction.setFacilityNum(invoice.getFaciltyNum());
                    transaction.setManReview(invoice.getManReview());
                    transaction.setPayProgram(invoice.getPayProgram());
                    transaction.setPaymentDate(null);
                    transaction.setProviderNo(invoice.getProviderNo());
                    transaction.setPaymentId(0);
                    transaction.setProvince(billedDemographic.getProvince());
                    transaction.setRefNum(invoice.getRefLabNum());
                    transaction.setServiceCode(item.getServiceCode());
                    transaction.setServiceCodeInvoiced(item.getFee());
                    transaction.setServiceCodeNum(item.getServiceCount());
                    transaction.setServiceCodeDiscount(new BigDecimal("0.00"));
                    transaction.setServiceCodePaid(new BigDecimal("0.00"));
                    transaction.setSliCode(invoice.getLocation());
                    transaction.setUpdateProviderNo(loggedInProvider.getProviderNo());
                    transaction.setVisittype(invoice.getVisitType());
                    transaction.setPaymentType(0);

                    billingOnTransactionDao.persist(transaction);
                }
                if (StringUtils.trimToNull(item.getDx()) != null && !demographicDiagnoses.contains(item.getDx()) && !newDemoDiagnoses.contains(item.getDx())) {
                    newDemoDiagnoses.add(item.getDx());
                }
                if (StringUtils.trimToNull(item.getDx1()) != null && !demographicDiagnoses.contains(item.getDx1()) && !newDemoDiagnoses.contains(item.getDx1())) {
                    newDemoDiagnoses.add(item.getDx1());
                }
                if (StringUtils.trimToNull(item.getDx2()) != null && !demographicDiagnoses.contains(item.getDx2()) && !newDemoDiagnoses.contains(item.getDx2())) {
                    newDemoDiagnoses.add(item.getDx2());
                }
            }

            for (String newDx : newDemoDiagnoses) {
                dxresearchDAO.save((new Dxresearch(billedDemographic.getDemographicNo(), new Date(),  new Date(), 'A', newDx, "icd9", (byte) 0, loggedInProvider.getProviderNo())));
            }
            if (billedAppointment != null) {
                appointmentArchiveDao.archiveAppointment(billedAppointment);
                Appointment oldAppointment = new Appointment(billedAppointment);

                billedAppointment.setStatus("B");
                appointmentDao.merge(billedAppointment);

                List<ChangedField> changedFields = new ArrayList<ChangedField>(ChangedField.getChangedFieldsAndValues(oldAppointment, billedAppointment));
                LogAction.addLog("N/A", LogConst.UPDATE, LogConst.CON_APPT,
                        "appointment_no=" + billedAppointment.getId(), String.valueOf(billedAppointment.getDemographicNo()), changedFields);
            }
        }
    }


/*
*
*
	Auto Generated Billing
*
*/
	@POST
	@Path("/saveBillingAutoRule")
	@Produces("application/json")
	@Consumes("application/x-www-form-urlencoded")
	public BillingAutoRuleResponse saveBillingAutoRule(MultivaluedMap<String, String> params) throws Exception {

		LoggedInInfo loggedInInfo = getLoggedInInfo();
		String provider_no = loggedInInfo.getLoggedInProviderNo();

	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date date = new Date();
	    String date_time = formatter.format(date);
	    date = formatter.parse(date_time);

	    Boolean service_code = false;
	    if(params.getFirst("service_code_list") != null && params.getFirst("service_code_list").length()>0){
	    	service_code = true;
	    }

	    Boolean dx_code = false;
	    if(params.getFirst("dx_code_list") != null && params.getFirst("dx_code_list").length()>0){
	    	dx_code = true;
	    }
	    
	    Integer id = null;
	    
	    try{
            id = Integer.parseInt(params.getFirst("id"));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        BillingAutoRules autoRule = new BillingAutoRules();
	    autoRule.setId(id);
		autoRule.setRuleName(params.getFirst("rule_name"));
		autoRule.setServiceCode(service_code);
		autoRule.setDx(dx_code);
		autoRule.setReferralDoctorType(params.getFirst("referral_doctor_type"));
		autoRule.setReferralDoctor(params.getFirst("referral_doctor"));
		autoRule.setBillingDoctorType(params.getFirst("billing_doctor_type"));
		autoRule.setBillingDoctor(params.getFirst("billing_doctor"));
		autoRule.setVisitType(params.getFirst("visit_type"));
		autoRule.setVisitLocation(params.getFirst("visit_location"));
		autoRule.setSliCode(params.getFirst("sli_code"));
		autoRule.setCreatedDate(date);
		autoRule.setCreator(provider_no);
		autoRule.setModifiedDate(date);
		autoRule.setLastModifier(provider_no);
		autoRule.setStatus(1);


		BillingAutoRules result = null;
		if(autoRule.getId() == null) {
            billingManager.saveBillingAutoRule(getLoggedInInfo(), autoRule);
        } else {
		    billingManager.updateBillingAutoRule(getLoggedInInfo(), autoRule);
        }
		result = autoRule;

		Integer rule_id = result.getId();

	    if(service_code){
	    List<String> service_code_list = Arrays.asList(params.getFirst("service_code_list").split(","));
		    for(String code:service_code_list){
		    	BillingAutoRulesExt codeExt = new BillingAutoRulesExt();
		    	codeExt.setExtKey("service_code");
		    	codeExt.setExtValue(code);
		    	codeExt.setCreatedDate(date);
		    	codeExt.setModifiedDate(date);
		    	codeExt.setRuleId(rule_id);
		    	codeExt.setStatus(1);
		    	billingAutoRuleExtDao.persist(codeExt);
		    }
	    }


	    if(dx_code){
	    List<String> dx_code_list = Arrays.asList(params.getFirst("dx_code_list").split(","));
		    for(String dx:dx_code_list){
		    	BillingAutoRulesExt dxExt = new BillingAutoRulesExt();
		    	dxExt.setExtKey("dx_code");
		    	dxExt.setExtValue(dx);
		    	dxExt.setCreatedDate(date);
		    	dxExt.setModifiedDate(date);
		    	dxExt.setRuleId(rule_id);
		    	dxExt.setStatus(1);
		    	billingAutoRuleExtDao.persist(dxExt);
		    }
	    }



		return getBillingAutoRule(rule_id);
	}

	@GET
	@Path("/billingAutoRules/active")
	@Produces("application/json")
	public BillingAutoRuleResponse getAllBillingAutoRules(@QueryParam("offset") Integer offset, @QueryParam("limit") Integer limit){
		List<BillingAutoRules> results = billingManager.getAllBillingAutoRules(getLoggedInInfo(), offset, limit);
		BillingAutoRuleResponse response = new BillingAutoRuleResponse();
		for(BillingAutoRules result:results){
			BillingAutoRuleTo1 to = new BillingAutoRuleTo1();
			BeanUtils.copyProperties(result, to);
			response.getContent().add(to);
		}
		return response;
	}

	@GET
	@Path("/billingAutoRule/{billingAutoRuleId}")
	@Produces("application/json")
	public BillingAutoRuleResponse getBillingAutoRule(@PathParam("billingAutoRuleId") Integer billingAutoRuleId) {
		BillingAutoRules result = billingManager.getBillingAutoRule(getLoggedInInfo(),billingAutoRuleId);
		BillingAutoRuleResponse response = new BillingAutoRuleResponse();
		BillingAutoRuleTo1 to = new BillingAutoRuleTo1();
		BeanUtils.copyProperties(result, to);
		response.getContent().add(to);
		return response;
	}

	@GET
	@Path("/billingRuleMappings/{ruleId}")
	@Produces("application/json")
	public BillingAutoRuleMapResponse getBillingAutoRulesMappedByRuleId(@PathParam("ruleId") String ruleId){
		List<BillingAutoRulesMap> results = billingManager.getBillingAutoRulesMapByRuleId(getLoggedInInfo(), ruleId);
		BillingAutoRuleMapResponse response = new BillingAutoRuleMapResponse();
		for(BillingAutoRulesMap result:results){
			BillingAutoRuleMapTo1 to = new BillingAutoRuleMapTo1();
			BeanUtils.copyProperties(result, to);
			response.getContent().add(to);
		}
		return response;
	}

	@GET
	@Path("/removeBillingAutoRule/{billingAutoRuleId}")
	@Produces("application/json")
	public GenericRESTResponse removeBillingAutoRule(@PathParam("billingAutoRuleId") Integer billingAutoRuleId)  {
		billingManager.removeBillingAutoRule(getLoggedInInfo(), billingAutoRuleId);

		GenericRESTResponse response = new GenericRESTResponse();
		response.setMessage("Auto billing rule deleted: " + billingAutoRuleId);

		return response;
	}

	@GET
	@Path("/billingInvoices/pending")
	@Produces("application/json")
	public BillingAutoRuleResponse getAllPendingInvoices(@QueryParam("offset") Integer offset, @QueryParam("limit") Integer limit){
		List<BillingAutoRules> results = billingManager.getAllBillingAutoRules(getLoggedInInfo(), offset, limit);
		BillingAutoRuleResponse response = new BillingAutoRuleResponse();
		for(BillingAutoRules result:results){
			BillingAutoRuleTo1 to = new BillingAutoRuleTo1();
			BeanUtils.copyProperties(result, to);
			response.getContent().add(to);
		}
		return response;
	}

	@GET
	@Path("/billing/approve/{billingId}")
	@Produces("application/json")
	public GenericRESTResponse billingApprove(@PathParam("billingId") Integer billingId) {
    	boolean testSet = true;
        String message = "success";
        billingONManager.approveBilling(billingId);
        return new GenericRESTResponse(testSet, message);
    }

	@GET
	@Path("/billing/delete/{billingId}")
	@Produces("application/json")
	public GenericRESTResponse billingDelete(@PathParam("billingId") Integer billingId) {
    	boolean testSet = true;
        String message = "success";
        billingONManager.deleteBilling(billingId);
        return new GenericRESTResponse(testSet, message);
    }

	@GET
	@Path("/billingAutoRulesMap/{appointmentTypeId}")
	@Produces("application/json")
	public BillingAutoRuleResponse getBillingAutoRulesMap(@PathParam("appointmentTypeId") String appointmentTypeId){
		List<BillingAutoRules> results = billingManager.getBillingAutoRulesMap(getLoggedInInfo(), appointmentTypeId);

		BillingAutoRuleResponse response = new BillingAutoRuleResponse();
		for(BillingAutoRules result:results){
			BillingAutoRuleTo1 to = new BillingAutoRuleTo1();
			BeanUtils.copyProperties(result, to);
			response.getContent().add(to);
		}
		return response;
	}

	@GET
	@Path("/billingAutoRuleMap/delete")
	@Produces("application/json")
	public GenericRESTResponse deleteBillingAutoRuleMap(@QueryParam("appointmentTypeId") String appointmentTypeId, @QueryParam("ruleId") String ruleId)  {
		Integer deletedId  = billingManager.deleteAutoRuleMapping(getLoggedInInfo(), appointmentTypeId, ruleId);

		GenericRESTResponse response = new GenericRESTResponse();

		if(deletedId>0){
			response.setMessage("app="+appointmentTypeId+" ruleId="+ruleId+" Auto Billing Rule Mapping deleted id="+ deletedId);
		}else{
			response.setMessage("Auto Billing Rule Mapping NOT deleted");
		}

		return response;
	}

	@GET
	@Path("/searchServiceCodes/{phrase}")
	@Produces("application/json")
	public Response searchServiceCodes(@PathParam("phrase") String phrase) {
	  List<org.oscarehr.common.model.BillingService> codes = billingServiceDao.findBillingCodesByCode(phrase, "ON");
	  return Response.ok(codes).build();
	}


	@GET
	@Path("/getClinicLocations")
	@Produces("application/json")
	public Response getClinicLocations() {
		List<ClinicLocation> clinicLocations = clinicLocationDao.findByClinicNo(1);
		return Response.ok(clinicLocations).build();
	}


}
