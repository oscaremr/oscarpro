package org.oscarehr.ws.rest.conversion;

import org.apache.commons.lang.StringUtils;
import org.oscarehr.managers.TimeSlotInfo;
import org.oscarehr.ws.rest.to.model.ScheduleTimeslotTo;
import org.oscarehr.managers.DayWorkSchedule;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.ws.rest.to.model.ScheduleTemplateTo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

public class ScheduleTemplateConverter extends AbstractConverter<DayWorkSchedule, ScheduleTemplateTo>  {

    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static SimpleDateFormat simpleDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    @Override
    public DayWorkSchedule getAsDomainObject(LoggedInInfo loggedInInfo, ScheduleTemplateTo t) throws ConversionException {
        DayWorkSchedule dayWorkSchedule = new DayWorkSchedule();
        
        dayWorkSchedule.setProviderNo(t.getProviderNo());
        Calendar calendar = Calendar.getInstance();
        try {
            Date scheduleTemplateDate = simpleDateFormat.parse(t.getDate());
            calendar.setTime(scheduleTemplateDate);
            dayWorkSchedule.setDate(calendar);
        }
        catch (ParseException pe) {
        }

        ArrayList<ScheduleTimeslotTo> timeslotList = t.getTimeslotList();
        TreeMap<Calendar, TimeSlotInfo> dayWorkScheduleTimeSlots = dayWorkSchedule.getTimeSlots();
        for (ScheduleTimeslotTo scheduleTimeslotTo : timeslotList) {
            String templateCodeString = scheduleTimeslotTo.getScheduleTemplateCode();
            Character templateCode = '\0';
            if (StringUtils.isNotEmpty(templateCodeString)) {
                templateCode = templateCodeString.charAt(0);
            }
            TimeSlotInfo timeSlotInfo = new TimeSlotInfo(templateCode);

            timeSlotInfo.setAvailability(scheduleTimeslotTo.isAvailability());
            timeSlotInfo.setDuration(scheduleTimeslotTo.getDuration());
            Calendar timeslot = Calendar.getInstance();
            try {
                Date timeslotDate = simpleDateTimeFormat.parse(scheduleTimeslotTo.getDateTime());
                timeslot.setTime(timeslotDate);
            }
            catch (ParseException pe) {
            }
            dayWorkScheduleTimeSlots.put(timeslot, timeSlotInfo);
        }
        
        return dayWorkSchedule;
    }

    @Override
    public ScheduleTemplateTo getAsTransferObject(LoggedInInfo loggedInInfo, DayWorkSchedule d) throws ConversionException {
        ScheduleTemplateTo scheduleTemplateTo = new ScheduleTemplateTo();
        
        scheduleTemplateTo.setProviderNo(d.getProviderNo());
        // Convert Date to readable string format (YYYY-MM-DD)
        scheduleTemplateTo.setDate(simpleDateFormat.format(d.getDate().getTime()));

        // Process all of the timeslots and add them to a list
        ArrayList<ScheduleTimeslotTo> timeslotList = new ArrayList<>();
        Map<Calendar, TimeSlotInfo> timeSlots = d.getTimeSlots();
        for (Map.Entry<Calendar, TimeSlotInfo> timeSlot : timeSlots.entrySet()) {
            ScheduleTimeslotTo scheduleTimeslotTo = new ScheduleTimeslotTo();

            // Convert DateTime to readable string format (YYYY-MM-DD HH:MM:SS-ZZ:ZZ)
            scheduleTimeslotTo.setDateTime(simpleDateTimeFormat.format(timeSlot.getKey().getTime()));

            scheduleTimeslotTo.setAvailability(timeSlot.getValue().isAvailability());
            scheduleTimeslotTo.setDuration(timeSlot.getValue().getDuration());
            scheduleTimeslotTo.setScheduleTemplateCode(timeSlot.getValue().getTemplateCode().toString());
            
            timeslotList.add(scheduleTimeslotTo);
        }
        scheduleTemplateTo.setTimeslotList(timeslotList);
        
        return scheduleTemplateTo;
    }
}
