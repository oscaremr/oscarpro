/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.ws.rest.conversion;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nullable;
import lombok.val;
import lombok.var;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.oscarehr.common.dao.ProfessionalSpecialistDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.DemographicExtKey;
import org.oscarehr.common.model.ProfessionalSpecialist;
import org.oscarehr.util.AgeCalculator;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.ws.rest.to.model.AgeTo1;
import org.oscarehr.ws.rest.to.model.DemographicTo1;
import oscar.SxmlMisc;

public class DemographicConverter extends AbstractConverter<Demographic, DemographicTo1> {
	
	private static Logger logger = Logger.getLogger(DemographicConverter.class);
	
	private DemographicExtConverter demoExtConverter = new DemographicExtConverter();
	private ProviderConverter providerConverter = new ProviderConverter();

	/**
	 * Converts TO, excluding provider and extras.
	 */
	@Override
	public Demographic getAsDomainObject(LoggedInInfo loggedInInfo,DemographicTo1 t) throws ConversionException {
		Demographic d = new Demographic();
		
		d.setDemographicNo(t.getDemographicNo());
		d.setPhone(t.getPhone());
		d.setPatientStatus(t.getPatientStatus());
		d.setPatientStatusDate(t.getPatientStatusDate());
		d.setRosterStatus(t.getRosterStatus());
		d.setProviderNo(t.getProviderNo());
		d.setMyOscarUserName(t.getMyOscarUserName());
		d.setHin(t.getHin());
		d.setAddress(t.getAddress().getAddress());
		d.setProvince(t.getAddress().getProvince());
		d.setVer(t.getVer());
		d.setSex(t.getSex());
		d.setDateOfBirth(t.getDobDay());
		d.setMonthOfBirth(t.getDobMonth());
		d.setYearOfBirth(t.getDobYear());
		d.setSexDesc(t.getSexDesc());
		d.setDateJoined(t.getDateJoined());
		d.setCity(t.getAddress().getCity());
		d.setFirstName(t.getFirstName());
		d.setPostal(t.getAddress().getPostal());
		d.setHcRenewDate(t.getHcRenewDate());
		d.setPhone2(t.getAlternativePhone());
		d.setPcnIndicator(t.getPcnIndicator());
		d.setEndDate(t.getEndDate());
		d.setLastName(t.getLastName());
		d.setHcType(t.getHcType());
		d.setChartNo(t.getChartNo());
		d.setEmail(t.getEmail());
		d.setPreferredName(t.getPrefName());
		
		d.setEffDate(t.getEffDate());
		d.setRosterDate(t.getRosterDate());
		d.setRosterTerminationDate(t.getRosterTerminationDate());
		d.setRosterTerminationReason(t.getRosterTerminationReason());
		d.setLinks(t.getRosterTerminationReason());
		d.setAlias(t.getAlias());
		d.setPreviousAddress(t.getPreviousAddress().getAddress());
		d.setChildren(t.getChildren());
		d.setSourceOfIncome(t.getSourceOfIncome());
		d.setCitizenship(t.getCitizenship());
		d.setSin(t.getSin());
		d.setAnonymous(t.getAnonymous());
		d.setSpokenLanguage(t.getSpokenLanguage());
		d.setActiveCount(t.getActiveCount());
		d.setHsAlertCount(t.getHsAlertCount());
		d.setTitle(t.getTitle());
		d.setOfficialLanguage(t.getOfficialLanguage());
		d.setCountryOfOrigin(t.getCountryOfOrigin());
		d.setNewsletter(t.getNewsletter());
		d.setConsentToUseEmailForEOrder(t.getConsentToUseEmailForEOrder());
		d.setConsentToUseEmailForCare(t.getConsentToUseEmailForCare());
		d.setPrimarySystemId(t.getPrimarySystemId());

		val exts = new DemographicExt[t.getExtras().size()];
		for (var i = 0; i < t.getExtras().size(); i++) {
		  exts[i] = demoExtConverter.getAsDomainObject(loggedInInfo, t.getExtras().get(i));
		  if (exts[i].getDemographicNo() == null) {
			exts[i].setDemographicNo(d.getDemographicNo());
		  }
		  if (exts[i].getProviderNo() == null) {
			exts[i].setProviderNo(loggedInInfo.getLoggedInProviderNo());
		  }
		}
		d.setExtras(exts);
		this.referralPhysicianExtSetter(d, t, d.getExtras());
		this.familyPhysicianExtSetter(d, t, d.getExtras());
		if (t.getProvider() != null) {
		  d.setProvider(providerConverter.getAsDomainObject(loggedInInfo, t.getProvider()));
		}
		return d;
	}

	@Override
	public DemographicTo1 getAsTransferObject(LoggedInInfo loggedInInfo,Demographic d) throws ConversionException {
		DemographicTo1 t = new DemographicTo1();
		
		t.setDemographicNo(d.getDemographicNo());
		t.setPhone(d.getPhone());
		t.setPatientStatus(d.getPatientStatus());
		t.setPatientStatusDate(d.getPatientStatusDate());
		t.setRosterStatus(d.getRosterStatus());
		t.setProviderNo(d.getProviderNo());
		t.setMyOscarUserName(d.getMyOscarUserName());
		t.setHin(d.getHin());
		t.getAddress().setAddress(d.getAddress());
		t.getAddress().setProvince(d.getProvince());
		t.setVer(d.getVer());
		t.setSex(d.getSex());
		try {
			t.setDateOfBirth(d.getBirthDay().getTime());
		} catch (Exception e ) {
			logger.warn("Unable to parse date: " + d.getBirthDayAsString());
		}
		t.setDobYear(d.getYearOfBirth());
		t.setDobMonth(d.getMonthOfBirth());
		t.setDobDay(d.getDateOfBirth());
		t.setPrefName(d.getPreferredName());
		t.setSexDesc(d.getSexDesc());
		t.setDateJoined(d.getDateJoined());
		t.setFamilyDoctor("<rdohip>" + d.getReferralPhysicianOhip() + "</rdohip>" + "<rd>" +
				d.getReferralPhysicianName() + "</rd>");
		t.setFamilyPhysician("<fdohip>" + d.getFamilyPhysicianOhip() + "</fdohip>" + "<fd>" +
				d.getFamilyPhysicianName() + "</fd>");
		t.getAddress().setCity(d.getCity());
		t.setFirstName(d.getFirstName());
		t.getAddress().setPostal(d.getPostal());
		t.setHcRenewDate(d.getHcRenewDate());
		t.setAlternativePhone(d.getPhone2());
		t.setPcnIndicator(d.getPcnIndicator());
		t.setEndDate(d.getEndDate());
		t.setLastName(d.getLastName());
		t.setHcType(d.getHcType());
		t.setChartNo(d.getChartNo());
		t.setEmail(d.getEmail());
		t.setEffDate(d.getEffDate());
		t.setRosterDate(d.getRosterDate());
		t.setRosterTerminationDate(d.getRosterTerminationDate());
		t.setRosterTerminationReason(d.getRosterTerminationReason());
		t.setLinks(d.getRosterTerminationReason());
		t.setAlias(d.getAlias());
		t.getPreviousAddress().setAddress(d.getPreviousAddress());
		t.setChildren(d.getChildren());
		t.setSourceOfIncome(d.getSourceOfIncome());
		t.setCitizenship(d.getCitizenship());
		t.setSin(d.getSin());
		t.setAnonymous(d.getAnonymous());
		t.setSpokenLanguage(d.getSpokenLanguage());
		t.setActiveCount(d.getActiveCount());
		t.setHsAlertCount(d.getHsAlertCount());
		t.setLastUpdateUser(d.getLastUpdateUser());
		t.setLastUpdateDate(d.getLastUpdateDate());
		t.setTitle(d.getTitle());
		t.setOfficialLanguage(d.getOfficialLanguage());
		t.setCountryOfOrigin(d.getCountryOfOrigin());
		t.setNewsletter(d.getNewsletter());
		t.setConsentToUseEmailForEOrder(d.getConsentToUseEmailForEOrder());
		t.setConsentToUseEmailForCare(d.getConsentToUseEmailForCare());
		t.setPrimarySystemId(d.getPrimarySystemId());

		if (d.getExtras() != null) {
			for (DemographicExt ext : d.getExtras()) {
				t.getExtras().add(demoExtConverter.getAsTransferObject(loggedInInfo,ext));
			}
		}

		if (d.getProvider() != null) {
			t.setProvider(providerConverter.getAsTransferObject(loggedInInfo,d.getProvider()));
		}
		
		t.setAge(new AgeTo1(AgeCalculator.calculateAge(d.getBirthDay())));

		return t;
	}

	private void referralPhysicianExtSetter(
      final Demographic demographic,
      final DemographicTo1 t,
      final DemographicExt[] exts
  ) {
    val referralPhysicianXml = t.getFamilyDoctor();
    val referralPhysicianName = parseXmlContent(referralPhysicianXml, "rd");
    val referralPhysicianOhip = parseXmlContent(referralPhysicianXml, "rdohip");
    if (StringUtils.isBlank(referralPhysicianName)) {
      return;
    }
    val referralPhysician = findPhysicianByXml(referralPhysicianOhip, t.getFamilyDoctor());
    if (referralPhysician == null) {
      return;
    }
    val referralPhysicianRowId = String.valueOf(referralPhysician.getId());
    var newExts = addExtensionToArray(exts, createDemographicExtension(
        t,
        DemographicExtKey.DOCTOR_ROSTER.getKey(),
        referralPhysicianRowId
    ));
    newExts = addExtensionToArray(newExts, createDemographicExtension(
        t,
        DemographicExtKey.DOCTOR_ROSTER_NAME.getKey(),
        referralPhysicianName
    ));
    newExts = addExtensionToArray(newExts, createDemographicExtension(
        t,
        DemographicExtKey.DOCTOR_ROSTER_OHIP.getKey(),
        referralPhysicianOhip
    ));
    demographic.setExtras(newExts);
  }

  private void familyPhysicianExtSetter(
      final Demographic demographic,
      final DemographicTo1 t,
      final DemographicExt[] exts
  ) {
    val familyPhysicianXml = t.getFamilyPhysician();
    val familyPhysicianName = parseXmlContent(familyPhysicianXml, "fd");
    val familyPhysicianOhip = parseXmlContent(familyPhysicianXml, "fdohip");
    if (StringUtils.isBlank(familyPhysicianName)) {
      return;
    }
    val familyPhysician = findPhysicianByXml(familyPhysicianOhip, t.getFamilyPhysician());
    if (familyPhysician == null) {
      return;
    }
    val familyPhysicianRowId = String.valueOf(familyPhysician.getId());
    var newExts = addExtensionToArray(exts, createDemographicExtension(
        t,
        DemographicExtKey.DOCTOR_FAMILY.getKey(),
        familyPhysicianRowId
    ));
    newExts = addExtensionToArray(newExts, createDemographicExtension(
        t,
        DemographicExtKey.DOCTOR_FAMILY_NAME.getKey(),
        familyPhysicianName
    ));
    newExts = addExtensionToArray(newExts, createDemographicExtension(
        t,
        DemographicExtKey.DOCTOR_FAMILY_OHIP.getKey(),
        familyPhysicianOhip
    ));
    demographic.setExtras(newExts);
  }

  private String parseXmlContent(final String xml, final String tag) {
    return SxmlMisc.getXmlContent(xml, tag);
  }

  @Nullable
  private ProfessionalSpecialist findPhysicianByXml(
      final String familyDoctorId,
      final String physicianXml
  ) {
    if (StringUtils.isBlank(familyDoctorId)) {
      return null;
    }
    val professionalSpecialistDao =
        (ProfessionalSpecialistDao) SpringUtils.getBean(ProfessionalSpecialistDao.class);
    val familyDoctorPhone = parseXmlContent(physicianXml, "phone");
    val familyDoctorAddress = parseXmlContent(physicianXml, "address");
    val familyDoctorFax = parseXmlContent(physicianXml, "fax");
    val professionalSpecialists = professionalSpecialistDao.findByReferralNo(familyDoctorId);
    if (professionalSpecialists == null || professionalSpecialists.isEmpty()) {
      return null;
    }
    if (professionalSpecialists.size() == 1) {
      return professionalSpecialists.get(0);
    }
    return findMostSimilarPhysician(
        professionalSpecialists, familyDoctorPhone, familyDoctorAddress, familyDoctorFax
    );
  }

  /**
   * This method takes in a list of professional specialists and compares their contact information
   * (phone number, address, fax number) with the provided information of a family doctor. It then
   * returns the professional specialist that has the highest number of matches with the family
   * doctor information.
   *
   * @param professionalSpecialists A list of professional specialists to compare with the family
   *                                doctor.
   * @param familyDoctorPhone       The phone number of the family doctor.
   * @param familyDoctorAddress     The address of the family doctor.
   * @param familyDoctorFax         The fax number of the family doctor.
   * @return The professional specialist that has the highest number of matches with the family
   * doctor information.
   */
  public ProfessionalSpecialist findMostSimilarPhysician(
      final List<ProfessionalSpecialist> professionalSpecialists,
      final String familyDoctorPhone,
      final String familyDoctorAddress,
      final String familyDoctorFax
  ) {
    var mostSimilarFamilyDoctor = new ProfessionalSpecialist();
    var maxScore = 0;

    for (var familyDoctor : professionalSpecialists) {
      var score = 0;
      if (Objects.equals(familyDoctor.getPhoneNumber(), familyDoctorPhone)) {
        score++;
      }
      if (Objects.equals(familyDoctor.getFaxNumber(), familyDoctorFax)) {
        score++;
      }
      if (Objects.equals(familyDoctor.getStreetAddress(), familyDoctorAddress)) {
        score++;
      }
      if (score == 3) {
        // If the specialist has a score of 3, return it immediately
        return familyDoctor;
      } else if (score > maxScore) {
        // If the specialist has a higher score than the current maxScore, update the
        // mostSimilarFamilyDoctor and maxScore variables
        maxScore = score;
        mostSimilarFamilyDoctor = familyDoctor;
      }
    }

    // Return the most similar professional specialist
    return mostSimilarFamilyDoctor;
  }

  protected DemographicExt createDemographicExtension(
      final DemographicTo1 t,
      final String key,
      final String value
  ) {
    return new DemographicExt(
        t.getProviderNo(),
        t.getDemographicNo(),
        key,
        value
    );
  }

  protected DemographicExt[] addExtensionToArray(
      DemographicExt[] exts,
      final DemographicExt extension
  ) {
    if (exts == null) {
      exts = new DemographicExt[]{};
    }
    var extras = new LinkedList<>(Arrays.asList(exts));
    extras.add(extension);
    return extras.toArray(new DemographicExt[exts.length + 1]);
  }
}
