/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.ws.rest.conversion;

import org.apache.cxf.common.util.StringUtils;
import org.apache.log4j.Logger;
import org.oscarehr.common.model.SMTPConfig;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.ws.rest.to.model.SMTPConfigTo1;

public class EmailConfigConverter extends AbstractConverter<SMTPConfig, SMTPConfigTo1> {
	
	private String SCRAMBLED_PASSWORD = "*******************************++++++++++++++++++++++++++++++++++++++";
	
	private Logger logger = MiscUtils.getLogger();
	
    @Override
    public SMTPConfig getAsDomainObject(LoggedInInfo loggedInInfo, SMTPConfigTo1 t) throws ConversionException {
        SMTPConfig d = new SMTPConfig();
        d.setId(t.getId() == 0 ? null : t.getId());
        d.setUrl(t.getUrl());
        d.setUsername(t.getUsername());
        if(!StringUtils.isEmpty(t.getPassword()) && !SCRAMBLED_PASSWORD.equals(t.getPassword())) {
        	try {
        		d.setPassword(t.getPassword().getBytes("UTF-8"));
        	} catch (Exception e) {
        		logger.error("Error setting password.", e);
        	}
        }
        d.setPort(t.getPort());
        d.setUseSecurity(t.getUseSecurity());
        d.setEnableEmail(t.getEnableEmail());
        d.setReplyToEmail(t.getReplyToEmail());
        d.setDisplayName(t.getDisplayName());
        d.setSignature(t.getSignature());
        
        return d;
    }

    @Override
    public SMTPConfigTo1 getAsTransferObject(LoggedInInfo loggedInInfo, SMTPConfig d) throws ConversionException {
        SMTPConfigTo1 t = new SMTPConfigTo1();
        t.setId(d.getId());
        t.setUrl(d.getUrl());
        t.setUsername(d.getUsername());
        t.setPassword(SCRAMBLED_PASSWORD);
        t.setPort(d.getPort());
        t.setUseSecurity(d.getUseSecurity());
        t.setEnableEmail(d.getEnableEmail());
        t.setReplyToEmail(d.getReplyToEmail());
        t.setDisplayName(d.getDisplayName());
        t.setSignature(d.getSignature());

        return t;
    }
}
