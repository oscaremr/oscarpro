/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.ws.rest;

import com.well.integration.Integration;
import com.well.integration.IntegrationDao;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.MiscUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import oscar.oscarBilling.ca.bc.Teleplan.TeleplanAPI;
import oscar.oscarBilling.ca.bc.Teleplan.TeleplanResponse;
import oscar.oscarBilling.ca.bc.Teleplan.TeleplanService;
import oscar.oscarBilling.ca.bc.Teleplan.TeleplanUserPassDAO;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

@Path("/hcv")
@Component("healthCardValidationService")
public class HealthCardValidationService extends AbstractServiceImpl {
    
    @Autowired
    DemographicDao demographicDao;
    
    @Autowired
    SystemPreferencesDao systemPreferencesDao;
    
    @Autowired
    IntegrationDao integrationDao;

    private static final String INSIG = "insig";
    private static final String X_INSIG_API_KEY_HEADER_NAME = "X-INSIG-API-KEY";
    
    @GET
    @Path("/demographic")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getHealthCardStatus(@Context HttpServletRequest request, 
                                        @QueryParam("demographicNo") Integer demographicNo, 
                                        @QueryParam("serviceDate") String serviceDate) throws IOException {
        
        Integration insig = integrationDao.getIntegration(INSIG);
        String apiKey = SystemPreferences.asString(systemPreferencesDao.findPreferenceByName(insig.getKeys().REST_API_KEY));
        String apiKeyHeader = request.getHeader(X_INSIG_API_KEY_HEADER_NAME);
        
        HCVResponse response;

        if (!StringUtils.isEmpty(apiKeyHeader) && apiKeyHeader.equals(apiKey)) {
            Demographic demographic = null;
            if (demographicNo != null) {
                demographic = demographicDao.getDemographicById(demographicNo);
            }

            if (demographic != null) {
                String dob = demographic.getYearOfBirth() + "-" + demographic.getMonthOfBirth() + "-" + demographic.getDateOfBirth();
                if (demographic.getHcType().equals("ON")) {
                    response = createEligibilityResponse(demographic.getHin(), demographic.getVer(), dob, null, "ON");
                } else if (demographic.getHcType().equals("BC")) {
                    if (!StringUtils.isEmpty(serviceDate)) {
                        response = createEligibilityResponse(demographic.getHin(), null, dob, serviceDate, "BC");
                    } else {
                        response = new HCVResponse(400, "Invalid request, missing service date");
                    }
                } else {
                    response = new HCVResponse(400, "Invalid request, ensure health card type is ON or BC");
                }
            } else {
                response = new HCVResponse(404, "Demographic not found");
            }

        } else {
            response = new HCVResponse(401,  "Invalid/missing API Key");
        }

        return Response.ok().status(response.getCode()).entity(response).build();
    }
    
    @POST
    @Path("/patient")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response checkPatientEligibility(@Context HttpServletRequest request,
                                            JSONObject jsonObject) throws IOException {

        String type = jsonObject.optString("type", null);
        String dob = jsonObject.optString("dob", null);
        String hin = jsonObject.optString("hin", null);
        String vc = jsonObject.optString("vc", null);
        String serviceDate = jsonObject.optString("serviceDate", null);
        
        String apiKeyHeader = request.getHeader(X_INSIG_API_KEY_HEADER_NAME);
        Integration insig = integrationDao.getIntegration(INSIG);
        String apiKey = SystemPreferences.asString(systemPreferencesDao.findPreferenceByName(insig.getKeys().REST_API_KEY));
        
        HCVResponse response;
        
        if (!StringUtils.isEmpty(apiKeyHeader) && apiKeyHeader.equals(apiKey)) {
            if (!StringUtils.isEmpty(type)) {
                if (type.equals("ON")) {
                    response = createEligibilityResponse(hin, vc, null, null, "ON");
                } else if (type.equals("BC")) {
                    response = createEligibilityResponse(hin, null, dob, serviceDate, "BC");
                } else {
                    response = new HCVResponse(400, "Invalid request, ensure health card type is ON or BC");
                }
            } else {
                response = new HCVResponse(400, "Invalid request, missing HCV type");
                
            }
        } else {
            response = new HCVResponse(401, "Invalid/missing API Key");
        }
        
        return Response.ok().status(response.getCode()).entity(response).build();
    }
  
    private HCVResponse createEligibilityResponse(String hin, String vc, String dob, String serviceDate, String type) throws IOException {
        
        if (type.equals("BC") && !StringUtils.isEmpty(hin) && !StringUtils.isEmpty(dob) &&!StringUtils.isEmpty(serviceDate)) {
            return checkBCEligibility(hin, dob, serviceDate);
        } else if (type.equals("ON")) {
            return checkONEligibility(hin, vc);
        } 
    
       return new HCVResponse(400, "Invalid request, check that parameters meet requirements and all required parameters for HCV type are supplied");
    }

    private HCVResponse checkBCEligibility(String hin, String dob, String billingDate) throws IOException {
        HCVResponse response = new HCVResponse(500, "Unexpected error");

        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime serviceDate;
        DateTime birthDate;
        
        if (isValidDate(dob)) {
           birthDate = new DateTime(dob);
        } else {
            response = new HCVResponse(400, "Invalid request, incorrect birth date");
            return response;
        }
        
        if (isValidDate(billingDate)) {
            serviceDate = (formatter.parseDateTime(billingDate));
        } else {
            response = new HCVResponse(400, "Invalid request, incorrect service date");
            return response;
        }
        

        TeleplanUserPassDAO dao = new TeleplanUserPassDAO();
        String[] userPass = dao.getUsernamePassword();
        TeleplanService tService = new TeleplanService();

        TeleplanAPI tAPI = null;

        try {
            tAPI = tService.getTeleplanAPI(userPass[0], userPass[1]);
        } catch (Exception e) {
            MiscUtils.getLogger().error(e.getMessage(), e);
        }

        if (tAPI != null) {

            String serviceDateYear = String.valueOf(serviceDate.getYear());
            String serviceDateMonth = String.valueOf(serviceDate.getMonthOfYear());
            String serviceDateDay = String.valueOf(serviceDate.getDayOfMonth());

            String birthDateYear = String.valueOf(birthDate.getYear());
            String birthDateMonth = String.valueOf(birthDate.getMonthOfYear());
            String birthDateDay = String.valueOf(birthDate.getDayOfMonth());

            TeleplanResponse tr = tAPI.checkElig(hin, birthDateYear, birthDateMonth, birthDateDay, serviceDateYear, serviceDateMonth, serviceDateDay, true, true, true);
            String realFile = tr.getRealFilename();
            if(tr.getResult().equalsIgnoreCase("SUCCESS") && realFile != null && !realFile.trim().equals("")) {
                File file = tr.getFile();
                try (BufferedReader buff = new BufferedReader(new FileReader(file))) {
                    String line = null;

                    while ((line = buff.readLine()) != null) {

                        if (line.startsWith("ELIG_ON_DOS:")) {
                            String el = line.substring(12).trim();
                            if (el.equalsIgnoreCase("no")) {
                                response = new HCVResponse(403, "Demographic is not eligible");
                            } else if (el.equalsIgnoreCase("yes")) {
                                response = new HCVResponse(200, "Demographic is eligible");
                            } else {
                                response = new HCVResponse(400, "Invalid request, check that parameters meet requirements and all required parameters for HCV type are supplied");
                            }
                            break;
                        }
                    }
                }

            } else if(tr.getResult().equalsIgnoreCase("FAILURE") && tr.getMsgs().equalsIgnoreCase("Invalid: PERSONAL HEALTH NUMBER")){
                response = new HCVResponse(403, "Demographic ineligible, " + tr.getMsgs());
            } else {
                response = new HCVResponse(500, "Unexpected error, " + tr.getMsgs());
            }
        }
        return response;
    }

    private HCVResponse checkONEligibility(String hin, String vc) throws IOException {
        
        HCVResponse response = new HCVResponse(500, "Unexpected error");
        String serviceUrl = "http://localhost:8080/CardSwipe/hcv?hin=" + hin + "&vc=" + vc;
        HttpGet httpGet = new HttpGet(serviceUrl);
        //Execute request to CardSwipe servlet
        try(CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            CloseableHttpResponse httpResponse = httpClient.execute(httpGet)) {

            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                response = new HCVResponse(200, "Demographic is eligible");
            } else if (httpResponse.getStatusLine().getStatusCode() == 400) {
                response = new HCVResponse(400, "Demographic is missing required information for eligibility check");
            } else if (httpResponse.getStatusLine().getStatusCode() == 403) {
                response = new HCVResponse(403, "Demographic is not eligible");
            } 
        }
        
        return response;
    }

    private boolean isValidDate(String dateToValidate){
        String pattern = "yyyy-MM-dd";

        try {
            DateTimeFormatter fmt = DateTimeFormat.forPattern(pattern);
            fmt.parseDateTime(dateToValidate);
        } catch (Exception e) {
            return false;
        }
        
        return true;
    }

    public static class HCVResponse {
        private int code;
        private String message;

        public HCVResponse() {
        }

        public HCVResponse(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
