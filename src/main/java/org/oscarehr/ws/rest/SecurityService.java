/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.ws.rest;

import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.ws.rest.to.GenericRESTResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Component("SecurityService")
@Path("/security")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class SecurityService extends AbstractServiceImpl {
    
    @Autowired
    private SecurityInfoManager securityInfoManager;
    
    @GET
    @Path("/hasRight")
    public GenericRESTResponse hasRight(
            @QueryParam("objectName") String objectName, 
            @QueryParam("privilege") String privilege, 
            @QueryParam("demographicNo") String demographicNo) {
        GenericRESTResponse response = new GenericRESTResponse();
        response.setSuccess(
                securityInfoManager.hasPrivilege(getLoggedInInfo(),
                        objectName,
                        privilege,
                        demographicNo)
        );
        return response;
    }
}
