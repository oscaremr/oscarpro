/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.ws.rest;

import org.apache.commons.lang.StringUtils;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;
import org.apache.cxf.rs.security.oauth.data.OAuthContext;
import org.apache.cxf.security.SecurityContext;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.ws.rest.to.ThirdPartyLinkPreferencesResponse;
import org.oscarehr.ws.rest.to.UrlResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

import static org.oscarehr.common.model.SystemPreferences.asBoolean;
import static org.oscarehr.common.model.SystemPreferences.asString;

@Component("SystemPreferencesService")
@Path("/systemPreferences")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
/* ***************************
 *          IMPORTANT
 * ***************************
 *
 * System credentials are stored in SystemPreferences table and under no circumstances
 * should any of those be exposed publicly.
 * Including but not limited to any of the following:
 *   - Insig API key
 *
 * ***************************
 *          IMPORTANT
 * ***************************
 */
public class SystemPreferencesService extends AbstractServiceImpl {

	@Autowired
	SystemPreferencesDao systemPreferencesDao;

	@Autowired
	protected SecurityInfoManager securityInfoManager;

	protected SecurityContext getSecurityContext() {
		Message m = PhaseInterceptorChain.getCurrentMessage();
    	SecurityContext sc = m.getContent(SecurityContext.class);
    	return sc;
	}

	protected OAuthContext getOAuthContext() {
		Message m = PhaseInterceptorChain.getCurrentMessage();
		OAuthContext sc = m.getContent(OAuthContext.class);
    	return sc;
	}

    public SystemPreferencesService() { }

	@GET
	@Path("/thirdPartyLinkPreferences")
	public Response getThirdPartyLinkPreferences() {
		Map<String, SystemPreferences> tpPref = systemPreferencesDao.findByKeysAsPreferenceMap(SystemPreferences.SCHEDULE_PREFERENCE_KEYS);
		ThirdPartyLinkPreferencesResponse linkPrefs = new ThirdPartyLinkPreferencesResponse(
				asBoolean(tpPref.get("schedule_tp_link_enabled")),
				StringUtils.trimToNull(asString(tpPref.get("schedule_tp_link_type"))),
				StringUtils.trimToNull(asString(tpPref.get("schedule_tp_link_display")))
		);
		return Response.ok().entity(linkPrefs).build();
	}

	@POST
	@Path("/thirdPartyLinkPreferences")
	public Response setThirdPartyLinkPreferences(ThirdPartyLinkPreferencesResponse data) {
		if (!securityInfoManager.hasPrivilege(getLoggedInInfo(), "_admin,_admin.schedule", "w", null)) {
			throw new NotAuthorizedException("Missing admin or schedule admin permission.");
		}
		systemPreferencesDao.mergeOrPersist("schedule_tp_link_enabled", data.getTpLinkEnabled().toString());
		systemPreferencesDao.mergeOrPersist("schedule_tp_link_type", data.getTpLinkType());
		systemPreferencesDao.mergeOrPersist("schedule_tp_link_display", data.getTpLinkDisplay());
		return getThirdPartyLinkPreferences();
	}
	
	@GET
	@Path("/getPatientPortalUrl")
	public Response getPatientPortalUrl() {
		SystemPreferences urlPreference = 
				systemPreferencesDao.findPreferenceByName(SystemPreferences.INSIG_PATIENT_PORTAL_LINK);
		UrlResponse portalUrl = 
				new UrlResponse(urlPreference != null ? urlPreference.getValue() : "");
		return Response.ok().entity(portalUrl).build();
		
	}
}
