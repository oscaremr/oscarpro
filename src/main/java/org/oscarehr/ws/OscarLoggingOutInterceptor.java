package org.oscarehr.ws;

import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.interceptor.LoggingMessage;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.message.Message;
import org.apache.cxf.service.model.MessageInfo;
import org.apache.cxf.transport.http.AbstractHTTPDestination;
import oscar.OscarProperties;

import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;

public class OscarLoggingOutInterceptor extends LoggingOutInterceptor {
    @Override
    public void handleMessage(Message message) throws Fault {
        OscarProperties oscarProperties = OscarProperties.getInstance();
        String wsLogLevel = oscarProperties.getProperty("WS_LOG_LEVEL", "BASIC");
        
        if (wsLogLevel.equalsIgnoreCase("full")) {
            //If the log level is full, revert to the built-in logging
            super.handleMessage(message);
        } else if (wsLogLevel.equalsIgnoreCase("basic")) {
            //If the log level is basic (the default), just log the basic info we need
            final StringBuilder buffer = new StringBuilder();

            String id = (String) message.getExchange().get(LoggingMessage.ID_KEY);
            if (id == null) {
                id = LoggingMessage.nextId();
                message.getExchange().put(LoggingMessage.ID_KEY, id);

            }
            buffer.append("Response ID ").append(id);
            buffer.append(" - ");

            MessageInfo messageInfo = (MessageInfo) message.get(MessageInfo.class.getName());
            if (messageInfo != null) {
                buffer.append(messageInfo.getOperation().getInputName());
                buffer.append(" - ");
            }

            HttpServletResponse httpServletResponse = (HttpServletResponse) message.get(AbstractHTTPDestination.HTTP_RESPONSE);
            if (httpServletResponse != null) {
                Integer responseCode = httpServletResponse.getStatus();
                if (responseCode != null) {
                    buffer.append(responseCode);
                }
            }
            
            super.getLogger().log(Level.INFO, buffer.toString());
        } 
    }
}
