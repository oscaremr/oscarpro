/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.provider.web.okta;

import com.fasterxml.jackson.annotation.JsonInclude;

/** class properties must match the same UserCredentials class in OSCAR Pro */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserCredentials {

  private String username;
  private String password;

  /**
   * oldPassword is optional, it is not used in create users requests, but is necessary when a user
   * requests to change their own password. It can olso be empty when an admin user with sufficient
   * rights requests to change another users password
   */
  private String oldPassword;

  // region ///  getters/setters  /////////////////////////////////////

  public String getUsername() {
    return username;
  }

  public UserCredentials setUsername(String username) {
    this.username = username;
    return this;
  }

  public String getPassword() {
    return password;
  }

  public UserCredentials setPassword(String password) {
    this.password = password;
    return this;
  }

  public String getOldPassword() {
    return oldPassword;
  }

  public UserCredentials setOldPassword(String oldPassword) {
    this.oldPassword = oldPassword;
    return this;
  }

  @Override
  public String toString() {
    return "UserCredentials{" + "username='" + username + '\'' + '}';
  }

  // endregion
}
