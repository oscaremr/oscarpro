/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */


package org.oscarehr.provider.web;

import java.util.*;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.val;
import lombok.var;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.util.LabelValueBean;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.CtlBillingServiceDao;
import org.oscarehr.common.dao.QueueDao;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.model.Facility;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.UserProperty;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

import oscar.OscarProperties;
import oscar.eform.EFormUtil;
import oscar.oscarEncounter.oscarConsultationRequest.pageUtil.EctConsultationFormRequestUtil;
import oscar.oscarRx.data.RxProviderData;

/**
 *
 * @author rjonasz
 */
public class ProviderPropertyAction extends DispatchAction {

    private final List<LabelValueBean> painFormDefaultBillingCodeList = Arrays.asList(
            new LabelValueBean("A937 PAIN MANAGEMENT", "pain_management"),
            new LabelValueBean("K013 Counselling up to 3 per Year", "counselling_up_to_3"),
            new LabelValueBean("K033 Counselling after 3 of K013", "counselling_after_3"),
            new LabelValueBean("K080 MINOR ASSESSMENT PHONE CALL", "minor_assessment_gp"),
            new LabelValueBean("K081 INTERMEDIATE ASSESSMENT PHONE CALL", "general_assessment_gp"),
            new LabelValueBean("A005 CONSULTATION GP", "consultation_gp"),
            new LabelValueBean("A006 REPEAT CONSULTATION GP", "repeat_consultation_gp"),
            new LabelValueBean("K082 30 MIN ASSESSMENT PHONE CALL", "intermediate_assessment"),
            new LabelValueBean("A013 SPECIFIC ASSESS ANAES", "specific_assess_anaes"),
            new LabelValueBean("A014 PARTIAL ASSESS ANAES", "partial_assess_anaes"),
            new LabelValueBean("A015 CONSULT ANAES", "consult_anaes"),
            new LabelValueBean("A016 REPEAT CONSULT ANAES", "repeat_consult_anaes"),
            new LabelValueBean("K730 + K731	MD to MD phone consults", "md_to_md_phone_consults"),
            new LabelValueBean("A905 LIMITED CONSULT GP", "limited_consult_gp"),
            new LabelValueBean("K005 PRIMARY MENTAL HEALTH CARE", "primary_mental_health_care"),
            new LabelValueBean("K037 FIBROMYALGIA /CHRONIC FATIGUE S", "chronic_fatigue_s"),
            new LabelValueBean("A911 SPECIAL CONSULTATION GP", "special_consultation_gp"),
            new LabelValueBean("A912 COMPREHENSIVE CONSULTATION GP", "comprehensive_consultation_gp"),
            new LabelValueBean("A007 INTERMEDIATE ASSESSMENT", "chronic_pain_care_reference_gp")
    );

    private UserPropertyDAO userPropertyDAO;

    public void setUserPropertyDAO(UserPropertyDAO dao) {
        this.userPropertyDAO = dao;
    }

    public ActionForward unspecified(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

        return view(actionmapping, actionform, request, response);
    }

    public ActionForward OscarMsgRecvd(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {
        
        
        userPropertyDAO.saveProp(request.getParameter("provider_no"), UserProperty.OSCAR_MSG_RECVD, request.getParameter("value"));
        
        return null;
    }
            
    public ActionForward remove(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

         DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = (UserProperty)frm.get("dateProperty");
         UserProperty prop2 = (UserProperty)frm.get("singleViewProperty");
         var staleLineNumberProperty = (UserProperty) frm.get("staleLineNumberProperty");

         frm.reset(actionmapping,request);
         this.userPropertyDAO.delete(prop);
         this.userPropertyDAO.delete(prop2);
         this.userPropertyDAO.delete(staleLineNumberProperty);

         request.setAttribute("status", "success");

         return actionmapping.findForward("success");
    }

    public ActionForward view(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

         DynaActionForm frm = (DynaActionForm)actionform;
         String provider = LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo();
         UserProperty prop = this.userPropertyDAO.getProp(provider, UserProperty.STALE_NOTEDATE);

         if( prop == null ) {
             prop = new UserProperty();
             prop.setProviderNo(provider);
             prop.setName(UserProperty.STALE_NOTEDATE);
         }

         frm.set("dateProperty", prop);

         UserProperty prop2 = this.userPropertyDAO.getProp(provider, UserProperty.STALE_FORMAT);

         if( prop2 == null ) {
             prop2 = new UserProperty();
             prop2.setProviderNo(provider);
             prop2.setName(UserProperty.STALE_FORMAT);
         }

         frm.set("singleViewProperty", prop2);

         var staleLineNumberProperty = this.userPropertyDAO.getProp(provider, UserProperty.STALE_LINE);

         if (staleLineNumberProperty == null) {
           staleLineNumberProperty = new UserProperty();
           staleLineNumberProperty.setProviderNo(provider);
           staleLineNumberProperty.setName(UserProperty.STALE_LINE);
         }
         frm.set("staleLineNumberProperty", staleLineNumberProperty);

         return actionmapping.findForward("success");
     }


    public ActionForward save(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

         DynaActionForm frm = (DynaActionForm)actionform;

         var staleLineDateProperty = (UserProperty) frm.get("dateProperty");
         if (staleLineDateProperty != null) {
           this.userPropertyDAO.saveProp(staleLineDateProperty);
         }

         var staleLineSingleViewProperty = (UserProperty) frm.get("singleViewProperty");
         if (staleLineSingleViewProperty != null) {
           this.userPropertyDAO.saveProp(staleLineSingleViewProperty);
         }

         var staleLineNumberProperty = (UserProperty) frm.get("staleLineNumberProperty");
         if (staleLineNumberProperty != null && staleLineNumberProperty.getValue() != null
             && !staleLineNumberProperty.getValue().isEmpty()) {
           try {
             Integer.parseInt(staleLineNumberProperty.getValue());
             userPropertyDAO.saveProp(staleLineNumberProperty);
           } catch (NumberFormatException ex) {
             request.setAttribute("status", "fail");
             request.setAttribute("reason", "Stale Line Number Need valid Integer value.");
             return actionmapping.findForward("fail");
           }
         }

         request.setAttribute("status", "success");
         return actionmapping.findForward("success");
    }

    public static void updateOrCreateProviderProperties(HttpServletRequest request) {
        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        UserPropertyDAO propertyDAO = SpringUtils.getBean(UserPropertyDAO.class);
        String providerNo = loggedInInfo.getLoggedInProviderNo();

        List<UserProperty> userProperties = new ArrayList<>();
        String propertyValue;
        UserProperty property;

        propertyValue = StringUtils.trimToNull(request.getParameter(UserProperty.SCHEDULE_WEEK_VIEW_WEEKENDS));
        property = propertyDAO.getProp(providerNo, UserProperty.SCHEDULE_WEEK_VIEW_WEEKENDS);
        if (property == null) {
            property = new UserProperty();
            property.setProviderNo(providerNo);
            property.setName(UserProperty.SCHEDULE_WEEK_VIEW_WEEKENDS);
        }
        property.setValue(String.valueOf(Boolean.parseBoolean(propertyValue)));
        propertyDAO.saveProp(property);
        
        propertyValue = StringUtils.trimToNull(request.getParameter("ticklerforproviderno"));
        if (propertyValue != null) {
            property = propertyDAO.getProp(providerNo, UserProperty.PROVIDER_FOR_TICKLER_WARNING);
            if (property == null) {
                property = new UserProperty();
                property.setProviderNo(providerNo);
                property.setName(UserProperty.PROVIDER_FOR_TICKLER_WARNING);
            }
            property.setValue(propertyValue);
            propertyDAO.saveProp(property);
        }

        propertyValue = StringUtils.trimToNull(request.getParameter("coverpage"));
        property = propertyDAO.getProp(providerNo, UserProperty.DEFAULT_COVER_PAGE);
        if (property == null) {
            property = new UserProperty();
            property.setProviderNo(providerNo);
            property.setName(UserProperty.DEFAULT_COVER_PAGE);
        }
        property.setValue("on".equals(propertyValue) ? "on" : "off");
        propertyDAO.saveProp(property);

        propertyValue = StringUtils.trimToNull(request.getParameter("allow_online_booking"));
        property = propertyDAO.getProp(providerNo, "allow_online_booking");
        if (property == null) {
            property = new UserProperty();
            property.setProviderNo(providerNo);
            property.setName("allow_online_booking");
        }
        property.setValue("on".equals(propertyValue) ? "true" : "false");
        propertyDAO.saveProp(property);
        
        propertyValue = StringUtils.trimToNull(request.getParameter("insigPortalEnabled"));
        property = propertyDAO.getProp(providerNo, UserProperty.INSIG_PORTAL_ENABLED);
        if (property == null) {
            property = new UserProperty();
            property.setProviderNo(providerNo);
            property.setName(UserProperty.INSIG_PORTAL_ENABLED);
        }
        property.setValue(String.valueOf("on".equals(propertyValue)));
        propertyDAO.saveProp(property);

        if (OscarProperties.getInstance().isOscarProEnabled()) {
          propertyValue = StringUtils.trimToNull(request.getParameter("pillwayButtonEnabled"));
          property = propertyDAO.getProp(providerNo, UserProperty.PILLWAY_BUTTON_ENABLED);
          if (property == null) {
            property = new UserProperty();
            property.setProviderNo(providerNo);
            property.setName(UserProperty.PILLWAY_BUTTON_ENABLED);
          }
          property.setValue(String.valueOf("on".equals(propertyValue)));
          propertyDAO.saveProp(property);
        }

        if (org.oscarehr.common.IsPropertiesOn.isMultisitesEnable()) {
          propertyValue = StringUtils.trimToNull(request.getParameter("ticklerDefaultLocation"));
          property = propertyDAO.getProp(providerNo, UserProperty.TICKLER_DEFAULT_SITE);
          if (property == null) {
            property = new UserProperty();
            property.setProviderNo(providerNo);
            property.setName(UserProperty.TICKLER_DEFAULT_SITE);
          }
          property.setValue(propertyValue);
          propertyDAO.saveProp(property);
        }

        if (!org.oscarehr.common.IsPropertiesOn.isMultisitesEnable()) {
          propertyValue = StringUtils.trimToNull(request.getParameter("ticklerDefaultRecipient"));
          property = propertyDAO.getProp(providerNo, UserProperty.TICKLER_DEFAULT_RECIPIENT);
          if (property == null) {
            property = new UserProperty();
            property.setProviderNo(providerNo);
            property.setName(UserProperty.TICKLER_DEFAULT_RECIPIENT);
          }
          property.setValue(propertyValue);
          propertyDAO.saveProp(property);
        } else {
          
          String[] ticklerDefaultSiteAndRecipientValues = request.getParameterValues("ticklerDefaultSiteAndRecipient");
          List<UserProperty> ticklerDefaultSites = propertyDAO.getAllProperties(UserProperty.TICKLER_DEFAULT_SITE_AND_RECIPIENT, providerNo);
          
          for (String value : ticklerDefaultSiteAndRecipientValues) {
            if (StringUtils.isNotEmpty(value)) {
              String siteId = value.split(":")[0];
              String recipientId = value.split(":")[1];
            
              UserProperty existingSiteAndRecipient = null;
              for (UserProperty siteProperty : ticklerDefaultSites) {
                String siteAndRecipient = siteProperty.getValue();
                if (siteAndRecipient.contains(":") && siteAndRecipient.split(":")[0].equals(siteId)) {
                  existingSiteAndRecipient = siteProperty;
                  break;
                }
              }
              
              if (recipientId.equals("none") && existingSiteAndRecipient != null) {
                propertyDAO.delete(existingSiteAndRecipient);
              } else {
                if (existingSiteAndRecipient == null) {
                  existingSiteAndRecipient = new UserProperty();
                  existingSiteAndRecipient.setProviderNo(providerNo);
                  existingSiteAndRecipient.setName(UserProperty.TICKLER_DEFAULT_SITE_AND_RECIPIENT);
                }
                existingSiteAndRecipient.setValue(siteId + ":" + recipientId);
                propertyDAO.saveProp(existingSiteAndRecipient);
              }
            }
          }
        }
        

        propertyValue = StringUtils.trimToNull(request.getParameter("default_pharmacy"));
        property = propertyDAO.getProp(providerNo, UserProperty.DEFAULT_PHARMACY);
        if (property == null) {
            property = new UserProperty();
            property.setProviderNo(providerNo);
            property.setName(UserProperty.DEFAULT_PHARMACY);
        }
        property.setValue(propertyValue);
        propertyDAO.saveProp(property);

        propertyValue = StringUtils.trimToNull(request.getParameter("default_service_other"));
        if (propertyValue != null) {
            property = propertyDAO.getProp(providerNo, UserProperty.DEFAULT_SERVICE_OTHER);
            if (property == null) {
                property = new UserProperty();
                property.setProviderNo(providerNo);
                property.setName(UserProperty.DEFAULT_SERVICE_OTHER);
            }
            property.setValue(propertyValue);
            propertyDAO.saveProp(property);
        }

        propertyValue = String.valueOf(Boolean.parseBoolean(request.getParameter(UserProperty.DISABLE_DX_HISTORY)));
        property = propertyDAO.getProp(providerNo, UserProperty.DISABLE_DX_HISTORY);
        if (property == null) {
            property = new UserProperty();
            property.setProviderNo(providerNo);
            property.setName(UserProperty.DISABLE_DX_HISTORY);
        }
        property.setValue(propertyValue);
        propertyDAO.saveProp(property);

        propertyValue = StringUtils.trimToNull(request.getParameter(UserProperty.DEFAULT_BILL_TO_OTHER));
        propertyDAO.saveUserPropertyIfChanged(providerNo, UserProperty.DEFAULT_BILL_TO_OTHER, propertyValue);

        propertyValue = request.getParameter(UserProperty.BILL_TO_OTHER_TEXT);
        propertyDAO.saveUserPropertyIfChanged(providerNo, UserProperty.BILL_TO_OTHER_TEXT, propertyValue);

        propertyValue = request.getParameter(UserProperty.REMIT_TO_OTHER_TEXT);
        propertyDAO.saveUserPropertyIfChanged(providerNo, UserProperty.REMIT_TO_OTHER_TEXT, propertyValue);

        propertyValue = request.getParameter(UserProperty.BILL_TO_OTHER_DATABASE_FIELD);
        propertyDAO.saveUserPropertyIfChanged(providerNo, UserProperty.BILL_TO_OTHER_DATABASE_FIELD, propertyValue);

        propertyValue = StringUtils.trimToNull(request.getParameter("default_service_quebec"));
        if (propertyValue != null) {
            property = propertyDAO.getProp(providerNo, UserProperty.DEFAULT_SERVICE_QUEBEC);
            if (property == null) {
                property = new UserProperty();
                property.setProviderNo(providerNo);
                property.setName(UserProperty.DEFAULT_SERVICE_QUEBEC);
            }
            property.setValue(propertyValue);
            propertyDAO.saveProp(property);
        }

        propertyValue = StringUtils.trimToNull(request.getParameter(UserProperty.DEFAULT_BILL_TO_QUEBEC));
        propertyDAO.saveUserPropertyIfChanged(providerNo, UserProperty.DEFAULT_BILL_TO_QUEBEC, propertyValue);

        propertyValue = request.getParameter(UserProperty.BILL_TO_QUEBEC_TEXT);
        propertyDAO.saveUserPropertyIfChanged(providerNo, UserProperty.BILL_TO_QUEBEC_TEXT, propertyValue);

        propertyValue = request.getParameter(UserProperty.REMIT_TO_QUEBEC_TEXT);
        propertyDAO.saveUserPropertyIfChanged(providerNo, UserProperty.REMIT_TO_QUEBEC_TEXT, propertyValue);

        propertyValue = request.getParameter(UserProperty.BILL_TO_QUEBEC_DATABASE_FIELD);
        propertyDAO.saveUserPropertyIfChanged(providerNo, UserProperty.BILL_TO_QUEBEC_DATABASE_FIELD, propertyValue);
    }
    public ActionForward viewDefaultSex(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.DEFAULT_SEX);

         if (prop == null){
             prop = new UserProperty();
         }

         ArrayList<LabelValueBean> serviceList = new ArrayList<LabelValueBean>();
         serviceList.add(new LabelValueBean("M", "M"));
         serviceList.add(new LabelValueBean("F", "F"));

         request.setAttribute("dropOpts",serviceList);

         request.setAttribute("dateProperty",prop);

         request.setAttribute("providertitle","provider.setDefaultSex.title");
         request.setAttribute("providermsgPrefs","provider.setDefaultSex.msgPrefs");
         request.setAttribute("providermsgProvider","provider.setDefaultSex.msgDefaultSex");
         request.setAttribute("providermsgEdit","provider.setDefaultSex.msgEdit");
         request.setAttribute("providerbtnSubmit","provider.setDefaultSex.btnSubmit");
         request.setAttribute("providermsgSuccess","provider.setDefaultSex.msgSuccess");
         request.setAttribute("method","saveDefaultSex");

         frm.set("dateProperty", prop);
         return actionmapping.findForward("gen");
     }

    public ActionForward saveDefaultSex(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = (UserProperty)frm.get("dateProperty");
         String fmt = prop != null ? prop.getValue() : "";
         UserProperty saveProperty = this.userPropertyDAO.getProp(providerNo,UserProperty.DEFAULT_SEX);

         if( saveProperty == null ) {
             saveProperty = new UserProperty();
             saveProperty.setProviderNo(providerNo);
             saveProperty.setName(UserProperty.DEFAULT_SEX);
         }

         saveProperty.setValue(fmt);
         this.userPropertyDAO.saveProp(saveProperty);

         request.setAttribute("status", "success");
         request.setAttribute("providertitle","provider.setDefaultSex.title");
         request.setAttribute("providermsgPrefs","provider.setDefaultSex.msgPrefs");
         request.setAttribute("providermsgProvider","provider.setDefaultSex.msgDefaultSex");
         request.setAttribute("providermsgEdit","provider.setDefaultSex.msgEdit");
         request.setAttribute("providerbtnSubmit","provider.btnSubmit");
         request.setAttribute("providermsgSuccess","provider.setDefaultSex.msgSuccess");
         request.setAttribute("method","saveDefaultSex");

         return actionmapping.findForward("gen");
    }

    public ActionForward viewDefaultRefPractitioner(ActionMapping actionmapping,
                                                    ActionForm actionform,
                                                    HttpServletRequest request,
                                                    HttpServletResponse response) {

        LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
        String providerNo=loggedInInfo.getLoggedInProviderNo();
        DynaActionForm frm = (DynaActionForm)actionform;
        UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.DEFAULT_REF_PRACTITIONER);
        RxProviderData rx = new RxProviderData();
        List<RxProviderData.Provider> prList = rx.getAllProviders();

        if (prop == null){
            prop = new UserProperty();
        }

        ArrayList<LabelValueBean> optionList = new ArrayList<LabelValueBean>();
        

        optionList.add(new LabelValueBean("All Practitioners", "all"));
        for (RxProviderData.Provider doctor : prList) 
        {
            if (doctor.getPractitionerNo() != null)
            {
                if (doctor.getPractitionerNo().length() > 0)
                {
                    optionList.add(new LabelValueBean(doctor.getFormattedName(), doctor.getPractitionerNo()));
                }  
            }
        }

        request.setAttribute("dateProperty",prop);
        request.setAttribute("dropOpts",optionList);

        request.setAttribute("providertitle","provider.setDefaultRefPractitioner.title");
        request.setAttribute("providermsgPrefs","provider.setDefaultRefPractitioner.msgPrefs");
        request.setAttribute("providermsgProvider","provider.setDefaultRefPractitioner.msgDefaultRefPrac");
        request.setAttribute("providermsgEdit","provider.setDefaultRefPractitioner.msgEdit");
        request.setAttribute("providerbtnSubmit","provider.setDefaultRefPractitioner.btnSubmit");
        request.setAttribute("providermsgSuccess","provider.setDefaultRefPractitioner.msgSuccess");
        request.setAttribute("method","saveDefaultRefPractitioner");

        frm.set("dateProperty", prop);
        return actionmapping.findForward("gen");
    }

    public ActionForward saveDefaultRefPractitioner(ActionMapping actionmapping,
                                        ActionForm actionform,
                                        HttpServletRequest request,
                                        HttpServletResponse response) {

        LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
        String providerNo=loggedInInfo.getLoggedInProviderNo();

        DynaActionForm frm = (DynaActionForm)actionform;
        UserProperty prop = (UserProperty)frm.get("dateProperty");
        String fmt = prop != null ? prop.getValue() : "";
        UserProperty saveProperty = this.userPropertyDAO.getProp(providerNo,UserProperty.DEFAULT_REF_PRACTITIONER);

        if( saveProperty == null ) {
            saveProperty = new UserProperty();
            saveProperty.setProviderNo(providerNo);
            saveProperty.setName(UserProperty.DEFAULT_REF_PRACTITIONER);
        }

        saveProperty.setValue(fmt);
        this.userPropertyDAO.saveProp(saveProperty);

        request.setAttribute("status", "success");
        request.setAttribute("providertitle","provider.setDefaultRefPractitioner.title");
        request.setAttribute("providermsgPrefs","provider.setDefaultRefPractitioner.msgPrefs");
        request.setAttribute("providermsgProvider","provider.setDefaultRefPractitioner.msgDefaultRefPrac");
        request.setAttribute("providermsgEdit","provider.setDefaultRefPractitioner.msgEdit");
        request.setAttribute("providerbtnSubmit","provider.setDefaultRefPractitioner.btnSubmit");
        request.setAttribute("providermsgSuccess","provider.setDefaultRefPractitioner.msgSuccess");
        request.setAttribute("method","saveDefaultSex");

        return actionmapping.findForward("gen");
    }

    public ActionForward viewConsultDefaultLetterhead(ActionMapping actionmapping,
                                                      ActionForm actionform,
                                                      HttpServletRequest request,
                                                      HttpServletResponse response) {
        LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
        String providerNo=loggedInInfo.getLoggedInProviderNo();
        DynaActionForm frm = (DynaActionForm)actionform;
        UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.CONSULTATION_LETTERHEADNAME_DEFAULT);

        if (prop == null){
            prop = new UserProperty();
        }

        ArrayList<LabelValueBean> optionList = new ArrayList<LabelValueBean>();


        optionList.add(new LabelValueBean("Provider (User)", "1"));
        optionList.add(new LabelValueBean("MRP", "2"));
        optionList.add(new LabelValueBean("Clinic", "3"));

        request.setAttribute("dateProperty",prop);
        request.setAttribute("dropOpts",optionList);

        request.setAttribute("providertitle","provider.setConsultDefaultLetterhead.title");
        request.setAttribute("providermsgPrefs","provider.setConsultDefaultLetterhead.msgPrefs");
        request.setAttribute("providermsgProvider","provider.setConsultDefaultLetterhead.msgDefaultLetterhead");
        request.setAttribute("providermsgEdit","provider.setConsultDefaultLetterhead.msgEdit");
        request.setAttribute("providerbtnSubmit","provider.setConsultDefaultLetterhead.btnSubmit");
        request.setAttribute("providermsgSuccess","provider.setConsultDefaultLetterhead.msgSuccess");
        request.setAttribute("method","saveConsultDefaultLetterhead");

        frm.set("dateProperty", prop);
        return actionmapping.findForward("gen");
    }

    public ActionForward saveConsultDefaultLetterhead(ActionMapping actionmapping,
                                                      ActionForm actionform,
                                                      HttpServletRequest request,
                                                      HttpServletResponse response) {

        LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
        String providerNo=loggedInInfo.getLoggedInProviderNo();

        DynaActionForm frm = (DynaActionForm)actionform;
        UserProperty prop = (UserProperty)frm.get("dateProperty");
        String fmt = prop != null ? prop.getValue() : "";
        UserProperty saveProperty = this.userPropertyDAO.getProp(providerNo,UserProperty.CONSULTATION_LETTERHEADNAME_DEFAULT);

        if( saveProperty == null ) {
            saveProperty = new UserProperty();
            saveProperty.setProviderNo(providerNo);
            saveProperty.setName(UserProperty.CONSULTATION_LETTERHEADNAME_DEFAULT);
        }

        saveProperty.setValue(fmt);
        this.userPropertyDAO.saveProp(saveProperty);

        request.setAttribute("status", "success");
        request.setAttribute("providertitle","provider.setConsultDefaultLetterhead.title");
        request.setAttribute("providermsgPrefs","provider.setConsultDefaultLetterhead.msgPrefs");
        request.setAttribute("providermsgProvider","provider.setConsultDefaultLetterhead.msgDefaultLetterhead");
        request.setAttribute("providermsgEdit","provider.setConsultDefaultLetterhead.msgEdit");
        request.setAttribute("providerbtnSubmit","provider.setConsultDefaultLetterhead.btnSubmit");
        request.setAttribute("providermsgSuccess","provider.setConsultDefaultLetterhead.msgSuccess");
        request.setAttribute("method","saveConsultDefaultLetterhead");

        return actionmapping.findForward("gen");
    }


  public ActionForward viewDefaultLetterheadAddress(
      ActionMapping actionMapping,
      ActionForm actionForm,
      HttpServletRequest request,
      HttpServletResponse response
  ) {
    val providerNumber = getLoggedInProviderNumber(request);
    var prop = getPropertyFromPropertyName(providerNumber,
        UserProperty.CONSULTATION_LETTERHEAD_ADDRESS_MULTISITE);
    addItemsToOptionList(request,
        new LabelValueBean("Site/Location", "site"),
        new LabelValueBean("Provider Preferences", "preference"));
    setPreferenceAttributesForDefaultLetterhead(request, false);
    setDateProperty((DynaActionForm) actionForm, prop);
    return actionMapping.findForward("gen");
  }

  public ActionForward saveDefaultLetterheadAddress(
      ActionMapping actionMapping,
      ActionForm actionForm,
      HttpServletRequest request,
      HttpServletResponse response
  ) {
      val providerNumber = getLoggedInProviderNumber(request);
      createOrGetUserProperty(providerNumber,
          getDateProperty((DynaActionForm) actionForm),
          UserProperty.CONSULTATION_LETTERHEAD_ADDRESS_MULTISITE);
      setPreferenceAttributesForDefaultLetterhead(request, true);
      return actionMapping.findForward("gen");
    }

  public ActionForward viewHCType(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.HC_TYPE);

         if (prop == null){
             prop = new UserProperty();
         }

         // Add all provinces / states to serviceList
         ArrayList<LabelValueBean> serviceList = constructProvinceList();

         request.setAttribute("dropOpts",serviceList);

         request.setAttribute("dateProperty",prop);

         request.setAttribute("providertitle","provider.setHCType.title");
         request.setAttribute("providermsgPrefs","provider.setHCType.msgPrefs");
         request.setAttribute("providermsgProvider","provider.setHCType.msgHCType");
         request.setAttribute("providermsgEdit","provider.setHCType.msgEdit");
         request.setAttribute("providerbtnSubmit","provider.setHCType.btnSubmit");
         request.setAttribute("providermsgSuccess","provider.setHCType.msgSuccess");
         request.setAttribute("method","saveHCType");

         frm.set("dateProperty", prop);
         return actionmapping.findForward("gen");
     }

    public ActionForward saveHCType(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

         DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = (UserProperty)frm.get("dateProperty");
         String fmt = prop != null ? prop.getValue() : "";
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

        UserProperty saveProperty = this.userPropertyDAO.getProp(providerNo,UserProperty.HC_TYPE);

         if( saveProperty == null ) {
             saveProperty = new UserProperty();
             saveProperty.setProviderNo(providerNo);
             saveProperty.setName(UserProperty.HC_TYPE);
         }

         saveProperty.setValue(fmt);
         this.userPropertyDAO.saveProp(saveProperty);

         request.setAttribute("status", "success");
         request.setAttribute("providertitle","provider.setHCType.title");
         request.setAttribute("providermsgPrefs","provider.setHCType.msgPrefs");
         request.setAttribute("providermsgProvider","provider.setHCType.msgHCType");
         request.setAttribute("providermsgEdit","provider.setHCType.msgEdit");
         request.setAttribute("providerbtnSubmit","provider.setHCType.btnSubmit");
         request.setAttribute("providermsgSuccess","provider.setHCType.msgSuccess");
         request.setAttribute("method","saveHCType");

         return actionmapping.findForward("gen");
    }



    /////

    public ActionForward viewMyDrugrefId(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

         DynaActionForm frm = (DynaActionForm)actionform;
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.MYDRUGREF_ID);
         

         if (prop == null){
             prop = new UserProperty();
         }

         request.setAttribute("dateProperty",prop);


         request.setAttribute("providertitle","provider.setmyDrugrefId.title"); //=Set myDrugref ID
         request.setAttribute("providermsgPrefs","provider.setmyDrugrefId.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setmyDrugrefId.msgProvider"); //=myDrugref ID
         request.setAttribute("providermsgEdit","provider.setmyDrugrefId.msgEdit"); //=Enter your desired login for myDrugref
         request.setAttribute("providerbtnSubmit","provider.setmyDrugrefId.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setmyDrugrefId.msgSuccess"); //=myDrugref Id saved
         request.setAttribute("method","saveMyDrugrefId");

         frm.set("dateProperty", prop);
         return actionmapping.findForward("gen");
     }
    public ActionForward viewRxPageSize(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

         DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.RX_PAGE_SIZE);


         if (prop == null){
             prop = new UserProperty();
         }

         request.setAttribute("providertitle","provider.setRxPageSize.title"); //=Set Rx Script Page Size
         request.setAttribute("providermsgPrefs","provider.setRxPageSize.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setRxPageSize.msgPageSize"); //=Rx Script Page Size
         request.setAttribute("providermsgEdit","provider.setRxPageSize.msgEdit"); //=Select your desired page size
         request.setAttribute("providerbtnSubmit","provider.setRxPageSize.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setRxPageSize.msgSuccess"); //=Rx Script Page Size saved
         request.setAttribute("method","saveRxPageSize");

         frm.set("rxPageSizeProperty", prop);
         return actionmapping.findForward("genRxPageSize");
     }

   public ActionForward saveRxPageSize(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm=(DynaActionForm)actionform;
        UserProperty UPageSize=(UserProperty)frm.get("rxPageSizeProperty");
        String rxPageSize="";
        if(UPageSize!=null)
            rxPageSize=UPageSize.getValue();
        UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.RX_PAGE_SIZE);
        if(prop==null){
            prop=new UserProperty();
            prop.setName(UserProperty.RX_PAGE_SIZE);
            prop.setProviderNo(providerNo);
        }
        prop.setValue(rxPageSize);
        this.userPropertyDAO.saveProp(prop);

         request.setAttribute("status", "success");
         request.setAttribute("rxPageSizeProperty",prop);
         request.setAttribute("providertitle","provider.setRxPageSize.title"); //=Set Rx Script Page Size
         request.setAttribute("providermsgPrefs","provider.setRxPageSize.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setRxPageSize.msgPageSize"); //=Rx Script Page Size
         request.setAttribute("providermsgEdit","provider.setRxPageSize.msgEdit"); //=Select your desired page size
         request.setAttribute("providerbtnSubmit","provider.setRxPageSize.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setRxPageSize.msgSuccess"); //=Rx Script Page Size saved
         request.setAttribute("method","saveRxPageSize");
         return actionmapping.findForward("genRxPageSize");
    }

   public ActionForward viewHideNoShowsAndCancellations(ActionMapping actionmapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
	
		DynaActionForm frm = (DynaActionForm)actionform;
		String provider = (String) request.getSession().getAttribute("user");
		UserProperty prop = this.userPropertyDAO.getProp(provider, UserProperty.HIDE_NO_SHOWS_AND_CANCELLATIONS);
		
		if (prop == null){
			prop = new UserProperty();
		}		
		
		request.setAttribute("providertitle","provider.setHideNoShowsAndCancellations.title");
		request.setAttribute("providermsgPrefs","provider.setHideNoShowsAndCancellations.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setHideNoShowsAndCancellations.msgHideTypes");
		request.setAttribute("providermsgEdit","provider.setHideNoShowsAndCancellations.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setHideNoShowsAndCancellations.btnSubmit");
		request.setAttribute("providermsgSuccess","provider.setHideNoShowsAndCancellations.msgSuccess");
		request.setAttribute("noShows", UserProperty.HIDE_NO_SHOWS_AND_CANCELLATIONS_NO_SHOW);
		request.setAttribute("cancellations", UserProperty.HIDE_NO_SHOWS_AND_CANCELLATIONS_CANCELLATION);
		request.setAttribute("both", UserProperty.HIDE_NO_SHOWS_AND_CANCELLATIONS_BOTH);
		request.setAttribute("method","saveHideNoShowsAndCancellations");
		
		frm.set("hideNoShowsAndCancellationsProperty", prop);
		return actionmapping.findForward("genHideNoShowsAndCancellations");
	}
	
	public ActionForward saveHideNoShowsAndCancellations(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){
	
		String provider = (String)request.getSession().getAttribute("user");
		
		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty hideNoShowsAndCancellations = (UserProperty)frm.get("hideNoShowsAndCancellationsProperty");
		String hideNoShowsAndCancellationsValue="";
		if(hideNoShowsAndCancellations != null)
			hideNoShowsAndCancellationsValue = hideNoShowsAndCancellations.getValue();
		
		UserProperty prop = this.userPropertyDAO.getProp(provider, UserProperty.HIDE_NO_SHOWS_AND_CANCELLATIONS);
		if(prop == null){
			prop = new UserProperty();
			prop.setName(UserProperty.HIDE_NO_SHOWS_AND_CANCELLATIONS);
			prop.setProviderNo(provider);
		}
		prop.setValue(hideNoShowsAndCancellationsValue);
		
		this.userPropertyDAO.saveProp(prop);
		
		request.setAttribute("providertitle","provider.setHideNoShowsAndCancellations.title");
		request.setAttribute("providermsgPrefs","provider.setHideNoShowsAndCancellations.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setHideNoShowsAndCancellations.msgHideTypes");
		request.setAttribute("providermsgEdit","provider.setHideNoShowsAndCancellations.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setHideNoShowsAndCancellations.btnSubmit");
		request.setAttribute("providermsgSuccess","provider.setHideNoShowsAndCancellations.msgSuccess");
		request.setAttribute("noShows", UserProperty.HIDE_NO_SHOWS_AND_CANCELLATIONS_NO_SHOW);
		request.setAttribute("cancellations", UserProperty.HIDE_NO_SHOWS_AND_CANCELLATIONS_CANCELLATION);
		request.setAttribute("both", UserProperty.HIDE_NO_SHOWS_AND_CANCELLATIONS_BOTH);
		request.setAttribute("method","saveHideNoShowsAndCancellations");
		
		return actionmapping.findForward("genHideNoShowsAndCancellations");
	}
   
      public ActionForward saveDefaultDocQueue(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm=(DynaActionForm)actionform;
        UserProperty existingQ=(UserProperty)frm.get("existingDefaultDocQueueProperty");
        UserProperty newQ=(UserProperty)frm.get("newDefaultDocQueueProperty");
        String mode=request.getParameter("chooseMode");
        String defaultQ="";
        if(mode.equalsIgnoreCase("new")&&newQ!=null)
            defaultQ=newQ.getValue();
        else if(mode.equalsIgnoreCase("existing")&&existingQ!=null)
            defaultQ=existingQ.getValue();
        else{
                 request.setAttribute("status", "success");
                 request.setAttribute("providertitle","provider.setDefaultDocumentQueue.title"); //=Set Default Document Queue
                 request.setAttribute("providermsgPrefs","provider.setDefaultDocumentQueue.msgPrefs"); //=Preferences
                 request.setAttribute("providermsgProvider","provider.setDefaultDocumentQueue.msgProfileView"); //=Default Document Queue
                 request.setAttribute("providermsgSuccess","provider.setDefaultDocumentQueue.msgNotSaved"); //=Default Document Queue has NOT been saved
                 request.setAttribute("method","saveDefaultDocQueue");
                 return actionmapping.findForward("genDefaultDocQueue");
        }
        UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.DOC_DEFAULT_QUEUE);
        if(prop==null){
            prop=new UserProperty();
            prop.setName(UserProperty.DOC_DEFAULT_QUEUE);
            prop.setProviderNo(providerNo);
        }
        if(mode.equals("new")){
            //save and get most recent id
            QueueDao queueDao = (QueueDao) SpringUtils.getBean("queueDao");
            queueDao.addNewQueue(defaultQ);
            String lastId=queueDao.getLastId();
            prop.setValue(lastId);
            this.userPropertyDAO.saveProp(prop);
        }else{
            prop.setValue(defaultQ);
            this.userPropertyDAO.saveProp(prop);
        }
         request.setAttribute("status", "success");
         request.setAttribute("defaultDocQueueProperty",prop);
         request.setAttribute("providertitle","provider.setDefaultDocumentQueue.title"); //=Set Default Document Queue
         request.setAttribute("providermsgPrefs","provider.setDefaultDocumentQueue.msgPrefs"); //=Preferences
         request.setAttribute("providermsgProvider","provider.setDefaultDocumentQueue.msgProfileView"); //=Default Document Queue
         request.setAttribute("providermsgSuccess","provider.setDefaultDocumentQueue.msgSuccess"); //=Default Document Queue saved
         request.setAttribute("method","saveDefaultDocQueue");
         return actionmapping.findForward("genDefaultDocQueue");
    }
    //public ActionForward viewDefaultDocQueue(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){
    //    return actionmapping.findForward("genDefaultDocQueue");
    //}
    public ActionForward viewDefaultDocQueue(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm=(DynaActionForm)actionform;
        UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.DOC_DEFAULT_QUEUE);
        UserProperty propNew=new UserProperty();

        if(prop==null){
            prop=new UserProperty();
        }
        QueueDao queueDao = (QueueDao) SpringUtils.getBean("queueDao");
        List<Hashtable> queues= queueDao.getQueues();
        Collection<LabelValueBean> viewChoices=new ArrayList<LabelValueBean>();
        viewChoices.add(new LabelValueBean("None","-1"));
        for(Hashtable ht:queues){
            viewChoices.add(new LabelValueBean((String)ht.get("queue"),(String)ht.get("id")));
        }
         request.setAttribute("viewChoices", viewChoices);
         request.setAttribute("providertitle","provider.setDefaultDocumentQueue.title"); //=Set Default Document Queue
         request.setAttribute("providermsgPrefs","provider.setDefaultDocumentQueue.msgPrefs"); //=Preferences
         request.setAttribute("providermsgProvider","provider.setDefaultDocumentQueue.msgProfileView"); //=Default Document Queue
         request.setAttribute("providermsgEditFromExisting","provider.setDefaultDocumentQueue.msgEditFromExisting"); //=Choose a default queue from existing queues
         request.setAttribute("providermsgEditSaveNew","provider.setDefaultDocumentQueue.msgEditSaveNew"); //=Save a new default queue
         request.setAttribute("providerbtnSubmit","provider.setDefaultDocumentQueue.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setDefaultDocumentQueue.msgSuccess"); //=Default Document Queue saved
         request.setAttribute("method","saveDefaultDocQueue");
         frm.set("existingDefaultDocQueueProperty", prop);
         frm.set("newDefaultDocQueueProperty", propNew);
        return actionmapping.findForward("genDefaultDocQueue");
    }

   public ActionForward viewRxProfileView(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.RX_PROFILE_VIEW);

         String propValue="";
         if (prop == null){
             prop = new UserProperty();
         }else{
            propValue=prop.getValue();
         }

         String [] propertyArray= new String[7];
         String [] va={" show_current "," show_all "," active "," inactive "," all "," longterm_acute "," longterm_acute_inactive_external "};

         for(int i=0;i<propertyArray.length;i++){
             if(propValue.contains(va[i]))  {
                 propertyArray[i]=va[i].trim();
             }//element of array has to match exactly with viewChoices values
         }
         prop.setValueArray(propertyArray);
         Collection<LabelValueBean> viewChoices=new ArrayList<LabelValueBean>();
         viewChoices.add(new LabelValueBean("Current","show_current"));
         viewChoices.add(new LabelValueBean("All","show_all"));
         viewChoices.add(new LabelValueBean("Active","active"));
         viewChoices.add(new LabelValueBean("Expired","inactive"));
         viewChoices.add(new LabelValueBean("Longterm/Acute","longterm_acute"));
         viewChoices.add(new LabelValueBean("Longterm/Acute/Inactive/External","longterm_acute_inactive_external"));
         request.setAttribute("viewChoices", viewChoices);
         request.setAttribute("providertitle","provider.setRxProfileView.title"); //=Set Rx Profile View
         request.setAttribute("providermsgPrefs","provider.setRxProfileView.msgPrefs"); //=Preferences
         request.setAttribute("providermsgProvider","provider.setRxProfileView.msgProfileView"); //=Rx Profile View
         request.setAttribute("providermsgEdit","provider.setRxProfileView.msgEdit"); //=Select your desired display
         request.setAttribute("providerbtnSubmit","provider.setRxProfileView.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setRxProfileView.msgSuccess"); //=Rx Profile View saved
         request.setAttribute("method","saveRxProfileView");

         frm.set("rxProfileViewProperty", prop);

         return actionmapping.findForward("genRxProfileView");
     }

   public ActionForward saveRxProfileView(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){

       try{
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm=(DynaActionForm)actionform;
        UserProperty UProfileView=(UserProperty)frm.get("rxProfileViewProperty");
        String[] va=null;
        if(UProfileView!=null)
            va=UProfileView.getValueArray();
        UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.RX_PROFILE_VIEW);
        if(prop==null){
            prop=new UserProperty();
            prop.setName(UserProperty.RX_PROFILE_VIEW);
            prop.setProviderNo(providerNo);
        }

        String rxProfileView="";
        if(va!=null){
            for(int i=0;i<va.length;i++){
                rxProfileView+=" "+va[i]+" ";
            }
        }
        prop.setValue(rxProfileView);
        this.userPropertyDAO.saveProp(prop);

         request.setAttribute("status", "success");
         request.setAttribute("defaultDocQueueProperty",prop);
         request.setAttribute("providertitle","provider.setRxProfileView.title"); //=Set Rx Profile View
         request.setAttribute("providermsgPrefs","provider.setRxProfileView.msgPrefs"); //=Preferences
         request.setAttribute("providermsgProvider","provider.setRxProfileView.msgProfileView"); //=Rx Profile View
         request.setAttribute("providermsgEdit","provider.setRxProfileView.msgEdit"); //=Select your desired display
         request.setAttribute("providerbtnSubmit","provider.setRxProfileView.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setRxProfileView.msgSuccess"); //=Rx Profile View saved
         request.setAttribute("method","saveRxProfileView");
       }catch(Exception e){
           MiscUtils.getLogger().error("Error", e);
       }

         return actionmapping.findForward("genRxProfileView");
    }

      public ActionForward viewShowPatientDOB(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.RX_SHOW_PATIENT_DOB);

         String propValue="";
         if (prop == null){
             prop = new UserProperty();
         }else{
            propValue=prop.getValue();
         }

         //String [] propertyArray= new String[7];
         boolean checked;
         if(propValue.equalsIgnoreCase("yes"))
             checked=true;
         else
             checked=false;

         prop.setChecked(checked);
         request.setAttribute("rxShowPatientDOBProperty", prop);
         request.setAttribute("providertitle","provider.setShowPatientDOB.title"); //=Select if you want to use Rx3
         request.setAttribute("providermsgPrefs","provider.setShowPatientDOB.msgPrefs"); //=Preferences
         request.setAttribute("providermsgProvider","provider.setShowPatientDOB.msgProfileView"); //=Use Rx3
         request.setAttribute("providermsgEdit","provider.setShowPatientDOB.msgEdit"); //=Do you want to use Rx3?
         request.setAttribute("providerbtnSubmit","provider.setShowPatientDOB.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setShowPatientDOB.msgSuccess"); //=Rx3 Selection saved
         request.setAttribute("method","saveShowPatientDOB");

         frm.set("rxShowPatientDOBProperty", prop);
         return actionmapping.findForward("genShowPatientDOB");
     }

   public ActionForward saveShowPatientDOB(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){
	    DynaActionForm frm=(DynaActionForm)actionform;
        UserProperty UShowPatientDOB=(UserProperty)frm.get("rxShowPatientDOBProperty");

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		boolean checked=false;
        if(UShowPatientDOB!=null)
            checked = UShowPatientDOB.isChecked();
        UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.RX_SHOW_PATIENT_DOB);
        if(prop==null){
            prop=new UserProperty();
            prop.setName(UserProperty.RX_SHOW_PATIENT_DOB);
            prop.setProviderNo(providerNo);
        }
        String showPatientDOB="no";
        if(checked)
            showPatientDOB="yes";
        prop.setValue(showPatientDOB);
        this.userPropertyDAO.saveProp(prop);

         request.setAttribute("status", "success");
         request.setAttribute("rxShowPatientDOBProperty",prop);
         request.setAttribute("providertitle","provider.setShowPatientDOB.title"); //=Select if you want to use Rx3
         request.setAttribute("providermsgPrefs","provider.setShowPatientDOB.msgPrefs"); //=Preferences
         request.setAttribute("providermsgProvider","provider.setShowPatientDOB.msgProfileView"); //=Use Rx3
         request.setAttribute("providermsgEdit","provider.setShowPatientDOB.msgEdit"); //=Do you want to use Rx3?
         request.setAttribute("providerbtnSubmit","provider.setShowPatientDOB.btnSubmit"); //=Save
         if(checked)
            request.setAttribute("providermsgSuccess","provider.setShowPatientDOB.msgSuccess_selected"); //=Rx3 is selected
         else
            request.setAttribute("providermsgSuccess","provider.setShowPatientDOB.msgSuccess_unselected"); //=Rx3 is unselected
         request.setAttribute("method","saveShowPatientDOB");
         return actionmapping.findForward("genShowPatientDOB");
    }

   public ActionForward viewUseMyMeds(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.USE_MYMEDS);
         if (prop == null) prop = new UserProperty();

         String propValue=prop.getValue();
         boolean checked=Boolean.parseBoolean(propValue);

         prop.setChecked(checked);
         request.setAttribute("useMyMedsProperty", prop);
         request.setAttribute("providertitle","provider.setUseMyMeds.title"); //=Select if you want to use MyMeds
         request.setAttribute("providermsgPrefs","provider.setUseMyMeds.msgPrefs"); //=Preferences
         request.setAttribute("providermsgProvider","provider.setUseMyMeds.msgProfileView"); //=Use MyMeds
         request.setAttribute("providermsgEdit","provider.setUseMyMeds.msgEdit"); //=Do you want to use MyMeds?
         request.setAttribute("providerbtnSubmit","provider.setUseMyMeds.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setUseMyMeds.msgSuccess"); //=MyMeds Selection saved
         request.setAttribute("method","saveUseMyMeds");

         frm.set("useMyMedsProperty", prop);
         return actionmapping.findForward("genUseMyMeds");
     }

      public ActionForward saveUseMyMeds(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){
        DynaActionForm frm=(DynaActionForm)actionform;
        UserProperty UUseMyMeds=(UserProperty)frm.get("useMyMedsProperty");
        //UserProperty UUseRx3=(UserProperty)request.getAttribute("rxUseRx3Property");

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		boolean checked=false;
        if(UUseMyMeds!=null)
            checked = UUseMyMeds.isChecked();
        UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.USE_MYMEDS);
        if(prop==null){
            prop=new UserProperty();
            prop.setName(UserProperty.USE_MYMEDS);
            prop.setProviderNo(providerNo);
        }
        prop.setValue(String.valueOf(checked));
        this.userPropertyDAO.saveProp(prop);

         request.setAttribute("status", "success");
         request.setAttribute("useMyMedsProperty",prop);
         request.setAttribute("providertitle","provider.setUseMyMeds.title"); //=Select if you want to use Rx3
         request.setAttribute("providermsgPrefs","provider.setUseMyMeds.msgPrefs"); //=Preferences
         request.setAttribute("providermsgProvider","provider.setUseMyMeds.msgProfileView"); //=Use Rx3
         request.setAttribute("providermsgEdit","provider.setUseMyMeds.msgEdit"); //=Check if you want to use Rx3
         request.setAttribute("providerbtnSubmit","provider.setUseMyMeds.btnSubmit"); //=Save
         if(checked)
            request.setAttribute("providermsgSuccess","provider.setUseMyMeds.msgSuccess_selected"); //=Rx3 is selected
         else
            request.setAttribute("providermsgSuccess","provider.setUseMyMeds.msgSuccess_unselected"); //=Rx3 is unselected
         request.setAttribute("method","saveUseMyMeds");
         return actionmapping.findForward("genUseMyMeds");
    }

      public ActionForward viewUseRx3(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {
    	  
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.RX_USE_RX3);

         String propValue="";
         if (prop == null){
             prop = new UserProperty();
         }else{
            propValue=prop.getValue();
         }

         //String [] propertyArray= new String[7];
         boolean checked;
         if(propValue.equalsIgnoreCase("yes"))
             checked=true;
         else
             checked=false;

         prop.setChecked(checked);
         request.setAttribute("rxUseRx3Property", prop);
         request.setAttribute("providertitle","provider.setRxRxUseRx3.title"); //=Select if you want to use Rx3
         request.setAttribute("providermsgPrefs","provider.setRxRxUseRx3.msgPrefs"); //=Preferences
         request.setAttribute("providermsgProvider","provider.setRxRxUseRx3.msgProfileView"); //=Use Rx3
         request.setAttribute("providermsgEdit","provider.setRxUseRx3.msgEdit"); //=Do you want to use Rx3?
         request.setAttribute("providerbtnSubmit","provider.setRxUseRx3.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setRxUseRx3.msgSuccess"); //=Rx3 Selection saved
         request.setAttribute("method","saveUseRx3");

         frm.set("rxUseRx3Property", prop);
         return actionmapping.findForward("genRxUseRx3");
     }

  public ActionForward configureSignAndSaveButtonInEchart(ActionMapping actionmapping,
      ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    val providerNumber = loggedInInfo.getLoggedInProviderNo();

    val actionForm = (DynaActionForm) actionform;
    val useConfigureSignAndSaveButtonInEchartProperty = (this.userPropertyDAO.getProp(providerNumber, UserProperty.CONFIGURE_SIGN_AND_SAVE_BUTTON_IN_ECHART) == null) ?
        new UserProperty() : this.userPropertyDAO.getProp(providerNumber, UserProperty.CONFIGURE_SIGN_AND_SAVE_BUTTON_IN_ECHART);
    val propValue = (useConfigureSignAndSaveButtonInEchartProperty.getValue() == null) ? "no" : useConfigureSignAndSaveButtonInEchartProperty.getValue();
    val checked = propValue.equalsIgnoreCase("yes");

    useConfigureSignAndSaveButtonInEchartProperty.setChecked(checked);
    request.setAttribute("configureSignAndSaveButtonInEchartProperty", useConfigureSignAndSaveButtonInEchartProperty);
    request.setAttribute("providertitle", "provider.configureSignAndSaveButtonInEchart.title"); //=Select if you want to use configureSignAndSaveButtonIneChart
    request.setAttribute("providermsgPrefs", "provider.configureSignAndSaveButtonInEchart.msgPrefs"); //=Preferences
    request.setAttribute("providermsgProvider", "provider.configureSignAndSaveButtonInEchart.msgProfileView"); //=Use configureSignAndSaveButtonIneChart
    request.setAttribute("providermsgEdit", "provider.configureSignAndSaveButtonInEchart.msgEdit"); //=Do you want to use configureSignAndSaveButtonIneChart?
    request.setAttribute("providerbtnSubmit", "provider.configureSignAndSaveButtonInEchart.btnSubmit"); //=Save
    request.setAttribute("providermsgSuccess", "provider.configureSignAndSaveButtonInEchart.msgSuccess"); //=configureSignAndSaveButtonIneChart Selection saved
    request.setAttribute("method", "saveConfigureSignAndSaveButtonInEchart");

    actionForm.set("configureSignAndSaveButtonInEchartProperty", useConfigureSignAndSaveButtonInEchartProperty);
    return actionmapping.findForward("genSignAndSaveButtonInEchart");
  }

  public ActionForward saveConfigureSignAndSaveButtonInEchart(ActionMapping actionmapping,
      ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    val providerNumber = loggedInInfo.getLoggedInProviderNo();
    val actionForm = (DynaActionForm) actionform;
    val useConfigureSignAndSaveButtonInEchartProperty = (UserProperty) actionForm.get("configureSignAndSaveButtonInEchartProperty");
    val checked = useConfigureSignAndSaveButtonInEchartProperty != null && useConfigureSignAndSaveButtonInEchartProperty.isChecked();

    var userProperty = this.userPropertyDAO.getProp(providerNumber, UserProperty.CONFIGURE_SIGN_AND_SAVE_BUTTON_IN_ECHART);
    if (userProperty == null) {
      userProperty = new UserProperty();
      userProperty.setName(UserProperty.CONFIGURE_SIGN_AND_SAVE_BUTTON_IN_ECHART);
      userProperty.setProviderNo(providerNumber);
    }
    val useConfigureSignAndSaveButtonInEchart = checked ? "yes" : "no";
    userProperty.setValue(useConfigureSignAndSaveButtonInEchart);
    this.userPropertyDAO.saveProp(userProperty);

    request.setAttribute("status", "success");
    request.setAttribute("configureSignAndSaveButtonInEchartProperty", userProperty);
    request.setAttribute("providertitle", "provider.configureSignAndSaveButtonInEchart.title"); //=Select if you want to use Rx3
    request.setAttribute("providermsgPrefs", "provider.configureSignAndSaveButtonInEchart.msgPrefs"); //=Preferences
    request.setAttribute("providermsgProvider", "provider.configureSignAndSaveButtonInEchart.msgProfileView"); //=Use Rx3
    request.setAttribute("providermsgEdit", "provider.configureSignAndSaveButtonInEchart.msgEdit"); //=Check if you want to use Rx3
    request.setAttribute("providerbtnSubmit", "provider.configureSignAndSaveButtonInEchart.btnSubmit"); //=Save
    if (checked) {
      request.setAttribute("providermsgSuccess", "provider.configureSignAndSaveButtonInEchart.msgSuccess_selected"); //=Rx3 is selected
    } else {
      request.setAttribute("providermsgSuccess", "provider.configureSignAndSaveButtonInEchart.msgSuccess_unselected"); //=Rx3 is unselected
    }
    request.setAttribute("method", "configureSignAndSaveButtonInEchartProperty");
    return actionmapping.findForward("genSignAndSaveButtonInEchart");
  }

   public ActionForward saveUseRx3(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm=(DynaActionForm)actionform;
        UserProperty UUseRx3=(UserProperty)frm.get("rxUseRx3Property");
      
        boolean checked=false;
        if(UUseRx3!=null)
            checked = UUseRx3.isChecked();
        UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.RX_USE_RX3);
        if(prop==null){
            prop=new UserProperty();
            prop.setName(UserProperty.RX_USE_RX3);
            prop.setProviderNo(providerNo);
        }
        String useRx3="no";
        if(checked)
            useRx3="yes";
        prop.setValue(useRx3);
        this.userPropertyDAO.saveProp(prop);

         request.setAttribute("status", "success");
         request.setAttribute("rxUseRx3Property",prop);
         request.setAttribute("providertitle","provider.setRxRxUseRx3.title"); //=Select if you want to use Rx3
         request.setAttribute("providermsgPrefs","provider.setRxRxUseRx3.msgPrefs"); //=Preferences
         request.setAttribute("providermsgProvider","provider.setRxRxUseRx3.msgProfileView"); //=Use Rx3
         request.setAttribute("providermsgEdit","provider.setRxUseRx3.msgEdit"); //=Check if you want to use Rx3
         request.setAttribute("providerbtnSubmit","provider.setRxUseRx3.btnSubmit"); //=Save
         if(checked)
            request.setAttribute("providermsgSuccess","provider.setRxUseRx3.msgSuccess_selected"); //=Rx3 is selected
         else
            request.setAttribute("providermsgSuccess","provider.setRxUseRx3.msgSuccess_unselected"); //=Rx3 is unselected
         request.setAttribute("method","saveUseRx3");
         return actionmapping.findForward("genRxUseRx3");
    }

       public ActionForward viewDefaultQuantity(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

         DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.RX_DEFAULT_QUANTITY);


         if (prop == null){
             prop = new UserProperty();
         }

         //request.setAttribute("propert",propertyToSet);
         request.setAttribute("rxDefaultQuantityProperty",prop);
         request.setAttribute("providertitle","provider.setRxDefaultQuantity.title"); //=Set Rx Default Quantity
         request.setAttribute("providermsgPrefs","provider.setRxDefaultQuantity.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setRxDefaultQuantity.msgDefaultQuantity"); //=Rx Default Quantity
         request.setAttribute("providermsgEdit","provider.setRxDefaultQuantity.msgEdit"); //=Enter your desired quantity
         request.setAttribute("providerbtnSubmit","provider.setRxDefaultQuantity.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setRxDefaultQuantity.msgSuccess"); //=Rx Default Quantity saved
         request.setAttribute("method","saveDefaultQuantity");

         frm.set("rxDefaultQuantityProperty", prop);

         return actionmapping.findForward("genRxDefaultQuantity");
     }

   public ActionForward saveDefaultQuantity(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
		
		DynaActionForm frm=(DynaActionForm)actionform;
        UserProperty UDefaultQuantity=(UserProperty)frm.get("rxDefaultQuantityProperty");
        String rxDefaultQuantity="";
        if(UDefaultQuantity!=null)
            rxDefaultQuantity=UDefaultQuantity.getValue();
        UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.RX_DEFAULT_QUANTITY);
        if(prop==null){
            prop=new UserProperty();
            prop.setName(UserProperty.RX_DEFAULT_QUANTITY);
            prop.setProviderNo(providerNo);
        }
        prop.setValue(rxDefaultQuantity);
        this.userPropertyDAO.saveProp(prop);

         request.setAttribute("status", "success");
         request.setAttribute("rxDefaultQuantityProperty",prop);
         request.setAttribute("providertitle","provider.setRxDefaultQuantity.title"); //=Set Rx Default Quantity
         request.setAttribute("providermsgPrefs","provider.setRxDefaultQuantity.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setRxDefaultQuantity.msgDefaultQuantity"); //=Rx Default Quantity
         request.setAttribute("providermsgEdit","provider.setRxDefaultQuantity.msgEdit"); //=Enter your desired quantity
         request.setAttribute("providerbtnSubmit","provider.setRxDefaultQuantity.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setRxDefaultQuantity.msgSuccess"); //=Rx Default Quantity saved
         request.setAttribute("method","saveDefaultQuantity");
         return actionmapping.findForward("genRxDefaultQuantity");

    }
    public ActionForward saveMyDrugrefId(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
   	
    	DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty  UdrugrefId = (UserProperty)frm.get("dateProperty");
         String drugrefId = "";

         if (UdrugrefId != null){
             drugrefId = UdrugrefId.getValue();
         }

         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.MYDRUGREF_ID);

         if (prop ==null){
             prop = new UserProperty();
             prop.setName(UserProperty.MYDRUGREF_ID);
             prop.setProviderNo(providerNo);
         }
         prop.setValue(drugrefId);

         this.userPropertyDAO.saveProp(prop);

         request.setAttribute("status", "success");
         request.setAttribute("dateProperty",prop);
         request.setAttribute("providertitle","provider.setmyDrugrefId.title"); //=Set myDrugref ID
         request.setAttribute("providermsgPrefs","provider.setmyDrugrefId.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setmyDrugrefId.msgProvider"); //=myDrugref ID
         request.setAttribute("providermsgEdit","provider.setmyDrugrefId.msgEdit"); //=Enter your desired login for myDrugref
         request.setAttribute("providerbtnSubmit","provider.setmyDrugrefId.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setmyDrugrefId.msgSuccess"); //=myDrugref Id saved
         request.setAttribute("method","saveMyDrugrefId");
         return actionmapping.findForward("gen");
     }
    /////



    /*ontario md*/
    public ActionForward viewOntarioMDId(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

         DynaActionForm frm = (DynaActionForm)actionform;
         
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.ONTARIO_MD_USERNAME);
         UserProperty prop2 = this.userPropertyDAO.getProp(providerNo, UserProperty.ONTARIO_MD_PASSWORD);

         if (prop == null){
             prop = new UserProperty();
         }

         if (prop2 == null){
             prop2 = new UserProperty();
         }

         //request.setAttribute("propert",propertyToSet);
         request.setAttribute("dateProperty",prop);
         request.setAttribute("dateProperty2",prop2);


         request.setAttribute("providertitle","provider.setOntarioMD.title"); //=Set myDrugref ID
         request.setAttribute("providermsgPrefs","provider.setOntarioMD.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setOntarioMD.msgProvider"); //=myDrugref ID
         request.setAttribute("providermsgEdit","provider.setOntarioMD.msgEdit"); //=Enter your desired login for myDrugref
         request.setAttribute("providerbtnSubmit","provider.setOntarioMD.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setOntarioMD.msgSuccess"); //=myDrugref Id saved
         request.setAttribute("method","saveOntarioMDId");

         frm.set("dateProperty", prop);
         return actionmapping.findForward("gen");
     }


    public ActionForward saveOntarioMDId(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
    	
    	DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty  UdrugrefId = (UserProperty)frm.get("dateProperty");
         String drugrefId = "";

         if (UdrugrefId != null){
             drugrefId = UdrugrefId.getValue();
         }

         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.ONTARIO_MD_USERNAME);

         if (prop ==null){
             prop = new UserProperty();
             prop.setName(UserProperty.ONTARIO_MD_USERNAME);
             prop.setProviderNo(providerNo);
         }
         prop.setValue(drugrefId);

         this.userPropertyDAO.saveProp(prop);


         UserProperty  UdrugrefId2 = (UserProperty)frm.get("dateProperty2");
         String drugrefId2 = "";

         if (UdrugrefId2 != null){
             drugrefId2 = UdrugrefId2.getValue();
         }

         UserProperty prop2 = this.userPropertyDAO.getProp(providerNo, UserProperty.ONTARIO_MD_PASSWORD);

         if (prop2 ==null){
             prop2 = new UserProperty();
             prop2.setName(UserProperty.ONTARIO_MD_PASSWORD);
             prop2.setProviderNo(providerNo);
         }
         prop2.setValue(drugrefId2);

         this.userPropertyDAO.saveProp(prop2);


         request.setAttribute("status", "success");
         request.setAttribute("dateProperty",prop);
         request.setAttribute("providertitle","provider.setOntarioMD.title"); //=Set myDrugref ID
         request.setAttribute("providermsgPrefs","provider.setOntarioMD.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setOntarioMD.msgProvider"); //=myDrugref ID
         request.setAttribute("providermsgEdit","provider.setOntarioMD.msgEdit"); //=Enter your desired login for myDrugref
         request.setAttribute("providerbtnSubmit","provider.setOntarioMD.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setOntarioMD.msgSuccess"); //=myDrugref Id saved
         request.setAttribute("method","saveOntarioMDId");
         return actionmapping.findForward("gen");
     }
    /*ontario md*/

    public ActionForward viewConsultationRequestCuffOffDate(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.CONSULTATION_TIME_PERIOD_WARNING);

         if (prop == null){
             prop = new UserProperty();
         }

         request.setAttribute("dateProperty",prop);

         request.setAttribute("providertitle","provider.setConsultationCutOffDate.title"); //=Set myDrugref ID
         request.setAttribute("providermsgPrefs","provider.setConsultationCutOffDate.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setConsultationCutOffDate.msgProvider"); //=myDrugref ID
         request.setAttribute("providermsgEdit","provider.setConsultationCutOffDate.msgEdit"); //=Enter your desired login for myDrugref
         request.setAttribute("providerbtnSubmit","provider.setConsultationCutOffDate.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setConsultationCutOffDate.msgSuccess"); //=myDrugref Id saved
         request.setAttribute("method","saveConsultationRequestCuffOffDate");

         frm.set("dateProperty", prop);
         return actionmapping.findForward("gen");
     }


    public ActionForward saveConsultationRequestCuffOffDate(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {
		
    	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty  UdrugrefId = (UserProperty)frm.get("dateProperty");
         String drugrefId = "";

         if (UdrugrefId != null){
             drugrefId = UdrugrefId.getValue();
         }

         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.CONSULTATION_TIME_PERIOD_WARNING);

         if (prop ==null){
             prop = new UserProperty();
             prop.setName(UserProperty.CONSULTATION_TIME_PERIOD_WARNING);
             prop.setProviderNo(providerNo);
         }
         prop.setValue(drugrefId);

         this.userPropertyDAO.saveProp(prop);

         request.setAttribute("status", "success");
         request.setAttribute("dateProperty",prop);
         request.setAttribute("providertitle","provider.setConsultationCutOffDate.title"); //=Set myDrugref ID
         request.setAttribute("providermsgPrefs","provider.setConsultationCutOffDate.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setConsultationCutOffDate.msgProvider"); //=myDrugref ID
         request.setAttribute("providermsgEdit","provider.setConsultationCutOffDate.msgEdit"); //=Enter your desired login for myDrugref
         request.setAttribute("providerbtnSubmit","provider.setConsultationCutOffDate.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setConsultationCutOffDate.msgSuccess"); //=myDrugref Id saved
         request.setAttribute("method","saveConsultationRequestCuffOffDate");
         return actionmapping.findForward("gen");
     }




    //// CONSULT TEAM

    public ActionForward viewConsultationRequestTeamWarning(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.CONSULTATION_TEAM_WARNING);

         if (prop == null){
             prop = new UserProperty();
         }

         EctConsultationFormRequestUtil conUtil = new EctConsultationFormRequestUtil();
         conUtil.estTeams();
         Vector<String> vect = conUtil.teamVec;

         ArrayList<LabelValueBean> serviceList = new ArrayList<LabelValueBean>();
         serviceList.add(new  LabelValueBean("All","-1"));
         for (String s: vect ){
                 serviceList.add(new LabelValueBean(s,s));
         }
         serviceList.add(new  LabelValueBean("None",""));



         //conUtil.teamVec.add("All");
         //conUtil.teamVec.add("None");

         request.setAttribute("dropOpts",serviceList);

         request.setAttribute("dateProperty",prop);

         request.setAttribute("providertitle","provider.setConsultationTeamWarning.title"); //=Set myDrugref ID
         request.setAttribute("providermsgPrefs","provider.setConsultationTeamWarning.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setConsultationTeamWarning.msgProvider"); //=myDrugref ID
         request.setAttribute("providermsgEdit","provider.setConsultationTeamWarning.msgEdit"); //=Enter your desired login for myDrugref
         request.setAttribute("providerbtnSubmit","provider.setConsultationTeamWarning.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setConsultationTeamWarning.msgSuccess"); //=myDrugref Id saved
         request.setAttribute("method","saveConsultationRequestTeamWarning");

         frm.set("dateProperty", prop);
         return actionmapping.findForward("gen");
     }


    public ActionForward saveConsultationRequestTeamWarning(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {
		
    	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty  UdrugrefId = (UserProperty)frm.get("dateProperty");
         String drugrefId = "";

         if (UdrugrefId != null){
             drugrefId = UdrugrefId.getValue();
         }

         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.CONSULTATION_TEAM_WARNING);

         if (prop ==null){
             prop = new UserProperty();
             prop.setName(UserProperty.CONSULTATION_TEAM_WARNING);
             prop.setProviderNo(providerNo);
         }
         prop.setValue(drugrefId);

         this.userPropertyDAO.saveProp(prop);


         EctConsultationFormRequestUtil conUtil = new EctConsultationFormRequestUtil();
         conUtil.estTeams();
         Vector<String> vect = conUtil.teamVec;

         ArrayList<LabelValueBean> serviceList = new ArrayList<LabelValueBean>();
         serviceList.add(new  LabelValueBean("All","-1"));
         for (String s: vect ){
                 serviceList.add(new LabelValueBean(s,s));
         }
         serviceList.add(new  LabelValueBean("None",""));
         request.setAttribute("dropOpts",serviceList);


         request.setAttribute("status", "success");
         request.setAttribute("dateProperty",prop);
         request.setAttribute("providertitle","provider.setConsultationTeamWarning.title"); //=Set myDrugref ID
         request.setAttribute("providermsgPrefs","provider.setConsultationTeamWarning.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setConsultationTeamWarning.msgProvider"); //=myDrugref ID
         request.setAttribute("providermsgEdit","provider.setConsultationTeamWarning.msgEdit"); //=Enter your desired login for myDrugref
         request.setAttribute("providerbtnSubmit","provider.setConsultationTeamWarning.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setConsultationTeamWarning.msgSuccess"); //=myDrugref Id saved
         request.setAttribute("method","saveConsultationRequestTeamWarning");
         return actionmapping.findForward("gen");
     }


    //WORKLOAD MANAGEMENT SCREEN PROPERTY
    public ActionForward viewWorkLoadManagement(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.WORKLOAD_MANAGEMENT);

         if (prop == null)
             prop = new UserProperty();

         ArrayList<LabelValueBean> serviceList = new ArrayList<LabelValueBean>();         
		 CtlBillingServiceDao dao = SpringUtils.getBean(CtlBillingServiceDao.class);
		 for (Object[] service : dao.getUniqueServiceTypes())
			serviceList.add(new LabelValueBean(String.valueOf(service[0]), String.valueOf(service[1])));
         serviceList.add(new  LabelValueBean("None",""));

         request.setAttribute("dropOpts",serviceList);

         request.setAttribute("dateProperty",prop);

         request.setAttribute("providertitle","provider.setWorkLoadManagement.title"); //=Set myDrugref ID
         request.setAttribute("providermsgPrefs","provider.setWorkLoadManagement.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setWorkLoadManagement.msgProvider"); //=myDrugref ID
         request.setAttribute("providermsgEdit","provider.setWorkLoadManagement.msgEdit"); //=Enter your desired login for myDrugref
         request.setAttribute("providerbtnSubmit","provider.setWorkLoadManagement.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setWorkLoadManagement.msgSuccess"); //=myDrugref Id saved
         request.setAttribute("method","saveWorkLoadManagement");

         frm.set("dateProperty", prop);
         return actionmapping.findForward("gen");
     }


    public ActionForward saveWorkLoadManagement(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {
		
    	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty  UdrugrefId = (UserProperty)frm.get("dateProperty");
         String drugrefId = "";

         if (UdrugrefId != null){
             drugrefId = UdrugrefId.getValue();
         }

         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.WORKLOAD_MANAGEMENT);

         if (prop ==null){
             prop = new UserProperty();
             prop.setName(UserProperty.WORKLOAD_MANAGEMENT);
             prop.setProviderNo(providerNo);
         }
         prop.setValue(drugrefId);

         this.userPropertyDAO.saveProp(prop);

         ArrayList<LabelValueBean> serviceList = new ArrayList<LabelValueBean>();
         CtlBillingServiceDao dao = SpringUtils.getBean(CtlBillingServiceDao.class);
         for(Object[] service : dao.getUniqueServiceTypes())
        	 serviceList.add(new LabelValueBean(String.valueOf(service[0]), String.valueOf(service[1])));

         request.setAttribute("dropOpts",serviceList);

         request.setAttribute("status", "success");
         request.setAttribute("dateProperty",prop);
         request.setAttribute("providertitle","provider.setWorkLoadManagement.title"); //=Set myDrugref ID
         request.setAttribute("providermsgPrefs","provider.setWorkLoadManagement.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setWorkLoadManagement.msgProvider"); //=myDrugref ID
         request.setAttribute("providermsgEdit","provider.setWorkLoadManagement.msgEdit"); //=Enter your desired login for myDrugref
         request.setAttribute("providerbtnSubmit","provider.setWorkLoadManagement.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setWorkLoadManagement.msgSuccess"); //=myDrugref Id saved
         request.setAttribute("method","saveWorkLoadManagement");
         return actionmapping.findForward("gen");
     }

    //How does cpp paste into consult request property
    public ActionForward viewConsultPasteFmt(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.CONSULTATION_REQ_PASTE_FMT);

         if (prop == null){
             prop = new UserProperty();
         }

         ArrayList<LabelValueBean> serviceList = new ArrayList<LabelValueBean>();
         serviceList.add(new LabelValueBean("Single Line", "single"));
         serviceList.add(new LabelValueBean("Multi Line", "multi"));

         request.setAttribute("dropOpts",serviceList);

         request.setAttribute("dateProperty",prop);

         request.setAttribute("providertitle","provider.setConsulReqtPasteFmt.title");
         request.setAttribute("providermsgPrefs","provider.setConsulReqtPasteFmt.msgPrefs");
         request.setAttribute("providermsgProvider","provider.setConsulReqtPasteFmt.msgProvider");
         request.setAttribute("providermsgEdit","provider.setConsulReqtPasteFmt.msgEdit");
         request.setAttribute("providerbtnSubmit","provider.setConsulReqtPasteFmt.btnSubmit");
         request.setAttribute("providermsgSuccess","provider.setConsulReqtPasteFmt.msgSuccess");
         request.setAttribute("method","saveConsultPasteFmt");

         frm.set("dateProperty", prop);
         return actionmapping.findForward("gen");
     }

    public ActionForward saveConsultPasteFmt(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = (UserProperty)frm.get("dateProperty");
         String fmt = prop != null ? prop.getValue() : "";

         UserProperty saveProperty = this.userPropertyDAO.getProp(providerNo,UserProperty.CONSULTATION_REQ_PASTE_FMT);

         if( saveProperty == null ) {
             saveProperty = new UserProperty();
             saveProperty.setProviderNo(providerNo);
             saveProperty.setName(UserProperty.CONSULTATION_REQ_PASTE_FMT);
         }

         saveProperty.setValue(fmt);
         this.userPropertyDAO.saveProp(saveProperty);

         request.setAttribute("status", "success");
         request.setAttribute("providertitle","provider.setConsulReqtPasteFmt.title");
         request.setAttribute("providermsgPrefs","provider.setConsulReqtPasteFmt.msgPrefs");
         request.setAttribute("providermsgProvider","provider.setConsulReqtPasteFmt.msgProvider");
         request.setAttribute("providermsgEdit","provider.setConsulReqtPasteFmt.msgEdit");
         request.setAttribute("providerbtnSubmit","provider.setConsulReqtPasteFmt.btnSubmit");
         request.setAttribute("providermsgSuccess","provider.setConsulReqtPasteFmt.msgSuccess");
         request.setAttribute("method","saveConsultPasteFmt");

         return actionmapping.findForward("gen");
     }
    // Constructs a list of LabelValueBeans, to be used as the dropdown list
    // when viewing a HCType preference
    public ArrayList<LabelValueBean> constructProvinceList() {

         ArrayList<LabelValueBean> provinces = new ArrayList<LabelValueBean>();

         provinces.add(new LabelValueBean("AB-Alberta", "AB"));
         provinces.add(new LabelValueBean("BC-British Columbia", "BC"));
         provinces.add(new LabelValueBean("MB-Manitoba", "MB"));
         provinces.add(new LabelValueBean("NB-New Brunswick", "NB"));
         provinces.add(new LabelValueBean("NL-Newfoundland", "NL"));
         provinces.add(new LabelValueBean("NT-Northwest Territory", "NT"));
         provinces.add(new LabelValueBean("NS-Nova Scotia", "NS"));
         provinces.add(new LabelValueBean("NU-Nunavut", "NU"));
         provinces.add(new LabelValueBean("ON-Ontario", "ON"));
         provinces.add(new LabelValueBean("PE-Prince Edward Island", "PE"));
         provinces.add(new LabelValueBean("QC-Quebec", "QC"));
         provinces.add(new LabelValueBean("SK-Saskatchewan", "SK"));
         provinces.add(new LabelValueBean("YT-Yukon", "YK"));
         provinces.add(new LabelValueBean("US resident", "US"));
         provinces.add(new LabelValueBean("US-AK-Alaska", "US-AK"));
         provinces.add(new LabelValueBean("US-AL-Alabama","US-AL"));
         provinces.add(new LabelValueBean("US-AR-Arkansas","US-AR"));
         provinces.add(new LabelValueBean("US-AZ-Arizona","US-AZ"));
         provinces.add(new LabelValueBean("US-CA-California","US-CA"));
         provinces.add(new LabelValueBean("US-CO-Colorado","US-CO"));
         provinces.add(new LabelValueBean("US-CT-Connecticut","US-CT"));
         provinces.add(new LabelValueBean("US-CZ-Canal Zone","US-CZ"));
         provinces.add(new LabelValueBean("US-DC-District of Columbia","US-DC"));
         provinces.add(new LabelValueBean("US-DE-Delaware","US-DE"));
         provinces.add(new LabelValueBean("US-FL-Florida","US-FL"));
         provinces.add(new LabelValueBean("US-GA-Georgia","US-GA"));
         provinces.add(new LabelValueBean("US-GU-Guam","US-GU"));
         provinces.add(new LabelValueBean("US-HI-Hawaii","US-HI"));
         provinces.add(new LabelValueBean("US-IA-Iowa","US-IA"));
         provinces.add(new LabelValueBean("US-ID-Idaho","US-ID"));
         provinces.add(new LabelValueBean("US-IL-Illinois","US-IL"));
         provinces.add(new LabelValueBean("US-IN-Indiana","US-IN"));
         provinces.add(new LabelValueBean("US-KS-Kansas","US-KS"));
         provinces.add(new LabelValueBean("US-KY-Kentucky","US-KY"));
         provinces.add(new LabelValueBean("US-LA-Louisiana","US-LA"));
         provinces.add(new LabelValueBean("US-MA-Massachusetts","US-MA"));
         provinces.add(new LabelValueBean("US-MD-Maryland","US-MD"));
         provinces.add(new LabelValueBean("US-ME-Maine","US-ME"));
         provinces.add(new LabelValueBean("US-MI-Michigan","US-MI"));
         provinces.add(new LabelValueBean("US-MN-Minnesota","US-MN"));
         provinces.add(new LabelValueBean("US-MO-Missouri","US-MO"));
         provinces.add(new LabelValueBean("US-MS-Mississippi","US-MS"));
         provinces.add(new LabelValueBean("US-MT-Montana","US-MT"));
         provinces.add(new LabelValueBean("US-NC-North Carolina","US-NC"));
         provinces.add(new LabelValueBean("US-ND-North Dakota","US-ND"));
         provinces.add(new LabelValueBean("US-NE-Nebraska","US-NE"));
         provinces.add(new LabelValueBean("US-NH-New Hampshire","US-NH"));
         provinces.add(new LabelValueBean("US-NJ-New Jersey","US-NJ"));
         provinces.add(new LabelValueBean("US-NM-New Mexico","US-NM"));
         provinces.add(new LabelValueBean("US-NU-Nunavut","US-NU"));
         provinces.add(new LabelValueBean("US-NV-Nevada","US-NV"));
         provinces.add(new LabelValueBean("US-NY-New York","US-NY"));
         provinces.add(new LabelValueBean("US-OH-Ohio","US-OH"));
         provinces.add(new LabelValueBean("US-OK-Oklahoma","US-OK"));
         provinces.add(new LabelValueBean("US-OR-Oregon","US-OR"));
         provinces.add(new LabelValueBean("US-PA-Pennsylvania","US-PA"));
         provinces.add(new LabelValueBean("US-PR-Puerto Rico","US-PR"));
         provinces.add(new LabelValueBean("US-RI-Rhode Island","US-RI"));
         provinces.add(new LabelValueBean("US-SC-South Carolina","US-SC"));
         provinces.add(new LabelValueBean("US-SD-South Dakota","US-SD"));
         provinces.add(new LabelValueBean("US-TN-Tennessee","US-TN"));
         provinces.add(new LabelValueBean("US-TX-Texas","US-TX"));
         provinces.add(new LabelValueBean("US-UT-Utah","US-UT"));
         provinces.add(new LabelValueBean("US-VA-Virginia","US-VA"));
         provinces.add(new LabelValueBean("US-VI-Virgin Islands","US-VI"));
         provinces.add(new LabelValueBean("US-VT-Vermont","US-VT"));
         provinces.add(new LabelValueBean("US-WA-Washington","US-WA"));
         provinces.add(new LabelValueBean("US-WI-Wisconsin","US-WI"));
         provinces.add(new LabelValueBean("US-WV-West Virginia","US-WV"));
         provinces.add(new LabelValueBean("US-WY-Wyoming","US-WY"));

         return provinces;
    }



    public ActionForward viewFavouriteEformGroup(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {
		
    	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
        var prop = getEformFavouriteGroupProperty(providerNo);
        var displayOnHover = getDisplayEformGroupOnHoverProperty(providerNo);

        frm.set("favouriteGroup", prop);
        frm.set("displayOnHover", displayOnHover);
        ArrayList<HashMap<String,String>> groups = EFormUtil.getEFormGroups();
        ArrayList<LabelValueBean> groupList = new ArrayList<LabelValueBean>();
        String name;
        groupList.add(new LabelValueBean("None",""));
         for (HashMap<String,String> h: groups ){
             name = h.get("groupName");
             groupList.add(new LabelValueBean(name,name));
         }

         request.setAttribute("dropOpts",groupList);

         request.setAttribute("favouriteGroup",prop);
         request.setAttribute("displayOnHover", displayOnHover);

         request.setAttribute("providertitle","provider.setFavEfrmGrp.title"); //=Set Favourite Eform Group
         request.setAttribute("providermsgPrefs","provider.setFavEfrmGrp.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setFavEfrmGrp.msgProvider"); //=Default Eform Group
         request.setAttribute("providermsgEdit","provider.setFavEfrmGrp.msgEdit"); //=Select your favourite Eform Group
         request.setAttribute("providerbtnSubmit","provider.setFavEfrmGrp.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setFavEfrmGrp.msgSuccess"); //=Favourite Eform Group saved
         request.setAttribute("method","saveFavouriteEformGroup");
         return actionmapping.findForward("genFavouriteEformGroup");
    }

    public ActionForward saveFavouriteEformGroup(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = (UserProperty)frm.get("favouriteGroup");
         String group = prop != null ? prop.getValue() : "";
         val displayOnHover = (UserProperty) frm.get("displayOnHover");

         UserProperty saveProperty = this.userPropertyDAO.getProp(providerNo,UserProperty.EFORM_FAVOURITE_GROUP);
         var saveDisplayOnHover = this.userPropertyDAO.getProp(providerNo, UserProperty.DISPLAY_EFORM_FAVOURITE_GROUP);

         if(saveProperty == null) {
           saveProperty = new UserProperty(providerNo, UserProperty.EFORM_FAVOURITE_GROUP);
         }

         if (saveDisplayOnHover == null) {
           saveDisplayOnHover = new UserProperty(
               providerNo, UserProperty.DISPLAY_EFORM_FAVOURITE_GROUP);
         }

         if (group.equalsIgnoreCase("") && saveProperty.getValue() != null) {
             this.userPropertyDAO.delete(saveProperty);
         } else {
            saveProperty.setValue(group);
            this.userPropertyDAO.saveProp(saveProperty);
         }

         saveDisplayOnHover.setValue(displayOnHover.getValue());
         this.userPropertyDAO.saveProp(saveDisplayOnHover);

         request.setAttribute("status", "success");
         request.setAttribute("providertitle","provider.setFavEfrmGrp.title"); //=Set Favourite Eform Group
         request.setAttribute("providermsgPrefs","provider.setFavEfrmGrp.msgPrefs"); //=Preferences"); //
         request.setAttribute("providermsgProvider","provider.setFavEfrmGrp.msgProvider"); //=Default Eform Group
         request.setAttribute("providermsgEdit","provider.setFavEfrmGrp.msgEdit"); //=Select your favourite Eform Group
         request.setAttribute("providerbtnSubmit","provider.setFavEfrmGrp.btnSubmit"); //=Save
         request.setAttribute("providermsgSuccess","provider.setFavEfrmGrp.msgSuccess"); //=Favourite Eform Group saved
         request.setAttribute("method","saveFavouriteEformGroup");

         return actionmapping.findForward("genFavouriteEformGroup");
    }

    public ActionForward viewCppSingleLine(ActionMapping actionmapping,
            ActionForm actionform,
            HttpServletRequest request,
            HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.CPP_SINGLE_LINE);

		String propValue="";
		if (prop == null){
			prop = new UserProperty();
		}else{
			propValue=prop.getValue();
		}

		boolean checked;
		if(propValue.equalsIgnoreCase("yes"))
			checked=true;
		else
			checked=false;

		prop.setChecked(checked);
		request.setAttribute("cppSingleLineProperty", prop);
		request.setAttribute("providertitle","provider.setCppSingleLine.title"); //=Select if you want to use Rx3
		request.setAttribute("providermsgPrefs","provider.setCppSingleLine.msgPrefs"); //=Preferences
		request.setAttribute("providermsgProvider","provider.setCppSingleLine.msgProfileView"); //=Use Rx3
		request.setAttribute("providermsgEdit","provider.setCppSingleLine.msgEdit"); //=Do you want to use Rx3?
		request.setAttribute("providerbtnSubmit","provider.setCppSingleLine.btnSubmit"); //=Save
		request.setAttribute("providermsgSuccess","provider.setCppSingleLine.msgSuccess"); //=Rx3 Selection saved
		request.setAttribute("method","saveUseCppSingleLine");

		frm.set("cppSingleLineProperty", prop);

		return actionmapping.findForward("genCppSingleLine");
	}


    public ActionForward saveUseCppSingleLine(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){
    	DynaActionForm frm=(DynaActionForm)actionform;
    	UserProperty UUseRx3=(UserProperty)frm.get("cppSingleLineProperty");

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		boolean checked=false;
		if(UUseRx3!=null)
			checked = UUseRx3.isChecked();
		UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.CPP_SINGLE_LINE);
		if(prop==null){
			prop=new UserProperty();
			prop.setName(UserProperty.CPP_SINGLE_LINE);
			prop.setProviderNo(providerNo);
		}
		String useRx3="no";
		if(checked)
			useRx3="yes";

		prop.setValue(useRx3);
		this.userPropertyDAO.saveProp(prop);

		request.setAttribute("status", "success");
		request.setAttribute("cppSingleLineProperty",prop);
		request.setAttribute("providertitle","provider.setCppSingleLine.title"); //=Select if you want to use Rx3
		request.setAttribute("providermsgPrefs","provider.setCppSingleLine.msgPrefs"); //=Preferences
		request.setAttribute("providermsgProvider","provider.setCppSingleLine.msgProfileView"); //=Use Rx3
		request.setAttribute("providermsgEdit","provider.setCppSingleLine.msgEdit"); //=Check if you want to use Rx3
		request.setAttribute("providerbtnSubmit","provider.setCppSingleLine.btnSubmit"); //=Save
		if(checked)
			request.setAttribute("providermsgSuccess","provider.setCppSingleLine.msgSuccess_selected"); //=Rx3 is selected
		else
			request.setAttribute("providermsgSuccess","provider.setCppSingleLine.msgSuccess_unselected"); //=Rx3 is unselected
		request.setAttribute("method","saveUseCppSingleLine");

		return actionmapping.findForward("genCppSingleLine");
	}
    
public ActionForward viewEDocBrowserInDocumentReport(ActionMapping actionmapping,
            ActionForm actionform,
            HttpServletRequest request,
            HttpServletResponse response) {

	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
	String providerNo=loggedInInfo.getLoggedInProviderNo();

	DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.EDOC_BROWSER_IN_DOCUMENT_REPORT);

		String propValue="";
		if (prop == null){
			prop = new UserProperty();
		}else{
			propValue=prop.getValue();
		}

		boolean checked;
		if(propValue.equalsIgnoreCase("yes"))
			checked=true;
		else
			checked=false;

		prop.setChecked(checked);
		request.setAttribute("eDocBrowserInDocumentReportProperty", prop);
		request.setAttribute("providertitle","provider.setEDocBrowserInDocumentReport.title"); 
		request.setAttribute("providermsgPrefs","provider.setEDocBrowserInDocumentReport.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setEDocBrowserInDocumentReport.msgProfileView");
		request.setAttribute("providermsgEdit","provider.setEDocBrowserInDocumentReport.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setEDocBrowserInDocumentReport.btnSubmit");
		request.setAttribute("providermsgSuccess","provider.setEDocBrowserInDocumentReport.msgSuccess");
		request.setAttribute("method","saveEDocBrowserInDocumentReport");

		frm.set("eDocBrowserInDocumentReportProperty", prop);

		return actionmapping.findForward("genEDocBrowserInDocumentReport");
	}


    public ActionForward saveEDocBrowserInDocumentReport(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm=(DynaActionForm)actionform;
    	UserProperty Uprop=(UserProperty)frm.get("eDocBrowserInDocumentReportProperty");

		boolean checked=false;
		if(Uprop!=null)
			checked = Uprop.isChecked();
		UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.EDOC_BROWSER_IN_DOCUMENT_REPORT);
		if(prop==null){
			prop=new UserProperty();
			prop.setName(UserProperty.EDOC_BROWSER_IN_DOCUMENT_REPORT);
			prop.setProviderNo(providerNo);
		}
		String propValue="no";
		if(checked)
			propValue="yes";

		prop.setValue(propValue);
		this.userPropertyDAO.saveProp(prop);

		request.setAttribute("status", "success");
		request.setAttribute("eDocBrowserInDocumentReportProperty",prop);
		request.setAttribute("providertitle","provider.setEDocBrowserInDocumentReport.title");
		request.setAttribute("providermsgPrefs","provider.setEDocBrowserInDocumentReport.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setEDocBrowserInDocumentReport.msgProfileView");
		request.setAttribute("providermsgEdit","provider.setEDocBrowserInDocumentReport.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setEDocBrowserInDocumentReport.btnSubmit");
		if(checked)
			request.setAttribute("providermsgSuccess","provider.setEDocBrowserInDocumentReport.msgSuccess_selected");
		else
			request.setAttribute("providermsgSuccess","provider.setEDocBrowserInDocumentReport.msgSuccess_unselected");
		request.setAttribute("method","saveEDocBrowserInDocumentReport");

		return actionmapping.findForward("genEDocBrowserInDocumentReport");
	}

    public ActionForward viewEDocBrowserInMasterFile(ActionMapping actionmapping,
            ActionForm actionform,
            HttpServletRequest request,
            HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.EDOC_BROWSER_IN_MASTER_FILE);

		String propValue="";
		if (prop == null){
			prop = new UserProperty();
		}else{
			propValue=prop.getValue();
		}

		boolean checked;
		if(propValue.equalsIgnoreCase("yes"))
			checked=true;
		else
			checked=false;

		prop.setChecked(checked);
		request.setAttribute("eDocBrowserInMasterFileProperty", prop);
		request.setAttribute("providertitle","provider.setEDocBrowserInMasterFile.title"); //=Select if you want to use Rx3
		request.setAttribute("providermsgPrefs","provider.setEDocBrowserInMasterFile.msgPrefs"); //=Preferences
		request.setAttribute("providermsgProvider","provider.setEDocBrowserInMasterFile.msgProfileView"); //=Use Rx3
		request.setAttribute("providermsgEdit","provider.setEDocBrowserInMasterFile.msgEdit"); //=Do you want to use Rx3?
		request.setAttribute("providerbtnSubmit","provider.setEDocBrowserInMasterFile.btnSubmit"); //=Save
		request.setAttribute("providermsgSuccess","provider.setEDocBrowserInMasterFile.msgSuccess"); //=Rx3 Selection saved
		request.setAttribute("method","saveEDocBrowserInMasterFile");

		frm.set("eDocBrowserInMasterFileProperty", prop);

		return actionmapping.findForward("genEDocBrowserInMasterFile");
	}


    public ActionForward saveEDocBrowserInMasterFile(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){
    	DynaActionForm frm=(DynaActionForm)actionform;
    	UserProperty Uprop=(UserProperty)frm.get("eDocBrowserInMasterFileProperty");

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		boolean checked=false;
		if(Uprop!=null)
			checked = Uprop.isChecked();
		UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.EDOC_BROWSER_IN_MASTER_FILE);
		if(prop==null){
			prop=new UserProperty();
			prop.setName(UserProperty.EDOC_BROWSER_IN_MASTER_FILE);
			prop.setProviderNo(providerNo);
		}
		String propValue="no";
		if(checked)
			propValue="yes";

		prop.setValue(propValue);
		this.userPropertyDAO.saveProp(prop);

		request.setAttribute("status", "success");
		request.setAttribute("eDocBrowserInMasterFileProperty",prop);
		request.setAttribute("providertitle","provider.setEDocBrowserInMasterFile.title");
		request.setAttribute("providermsgPrefs","provider.setEDocBrowserInMasterFile.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setEDocBrowserInMasterFile.msgProfileView");
		request.setAttribute("providermsgEdit","provider.setEDocBrowserInMasterFile.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setEDocBrowserInMasterFile.btnSubmit");
		if(checked)
			request.setAttribute("providermsgSuccess","provider.setEDocBrowserInMasterFile.msgSuccess_selected");
		else
			request.setAttribute("providermsgSuccess","provider.setEDocBrowserInMasterFile.msgSuccess_unselected");
		request.setAttribute("method","saveEDocBrowserInMasterFile");

		return actionmapping.findForward("genEDocBrowserInMasterFile");
	}

    public ActionForward viewCommentLab(ActionMapping actionmapping,
            ActionForm actionform,
            HttpServletRequest request,
            HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.LAB_ACK_COMMENT);

		String propValue="";
		if (prop == null){
			prop = new UserProperty();
		}else{
			propValue=prop.getValue();
		}

		boolean checked;
		if(propValue.equalsIgnoreCase("yes"))
			checked=true;
		else
			checked=false;

		prop.setChecked(checked);
		request.setAttribute("labAckComment", prop);
		request.setAttribute("providertitle","provider.setAckComment.title");
		request.setAttribute("providermsgPrefs","provider.setAckComment.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setAckComment.msgProfileView");
		request.setAttribute("providermsgEdit","provider.setAckComment.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setAckComment.btnSubmit");
		request.setAttribute("providermsgSuccess","provider.setAckComment.msgSuccess");
		request.setAttribute("method","saveCommentLab");

		frm.set("labAckCommentProperty", prop);

		return actionmapping.findForward("genAckCommentLab");
	}

    public ActionForward saveCommentLab(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm=(DynaActionForm)actionform;
    	UserProperty Uprop=(UserProperty)frm.get("labAckCommentProperty");

		boolean checked=false;
		if(Uprop!=null)
			checked = Uprop.isChecked();
		UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.LAB_ACK_COMMENT);
		if(prop==null){
			prop=new UserProperty();
			prop.setName(UserProperty.LAB_ACK_COMMENT);
			prop.setProviderNo(providerNo);
		}
		String disableComment="no";
		if(checked)
			disableComment="yes";

		prop.setValue(disableComment);
		this.userPropertyDAO.saveProp(prop);

		request.setAttribute("status", "success");
		request.setAttribute("labAckComment", prop);
		request.setAttribute("providertitle","provider.setAckComment.title");
		request.setAttribute("providermsgPrefs","provider.setAckComment.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setAckComment.msgProfileView");
		request.setAttribute("providermsgEdit","provider.setAckComment.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setAckComment.btnSubmit");
		request.setAttribute("providermsgSuccess","provider.setAckComment.msgSuccess");
		request.setAttribute("method","saveCommentLab");

		if(checked)
			request.setAttribute("providermsgSuccess","provider.setAckComment.msgSuccess_selected");
		else
			request.setAttribute("providermsgSuccess","provider.setAckComment.msgSuccess_unselected");


		return actionmapping.findForward("genAckCommentLab");
	}

    @SuppressWarnings("unchecked")
	public ActionForward viewLabRecall(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty delegate = this.userPropertyDAO.getProp(providerNo, UserProperty.LAB_RECALL_DELEGATE);
		UserProperty subject = this.userPropertyDAO.getProp(providerNo, UserProperty.LAB_RECALL_MSG_SUBJECT);
		UserProperty ticklerAssignee = this.userPropertyDAO.getProp(providerNo, UserProperty.LAB_RECALL_TICKLER_ASSIGNEE);
		UserProperty priority = this.userPropertyDAO.getProp(providerNo, UserProperty.LAB_RECALL_TICKLER_PRIORITY);

		if (delegate == null){
			delegate = new UserProperty();
		}

		if (subject == null){
			subject = new UserProperty();
		}
		
		String defaultToDelegate = "";
		if (ticklerAssignee == null){
			ticklerAssignee = new UserProperty();
		}else{
			defaultToDelegate = ticklerAssignee.getValue();
		}
		
		boolean checked;
		if(defaultToDelegate.equalsIgnoreCase("yes")){
            checked=true;
		}else{
            checked=false;
		}
		
		if (priority == null){
			priority = new UserProperty();
		}

        ArrayList<LabelValueBean> providerList = new ArrayList<LabelValueBean>();
        providerList.add(new LabelValueBean("Select", "")); //key , value
        
		ProviderDao dao = SpringUtils.getBean(ProviderDao.class);
		List<Provider> ps = dao.getProviders();
		Collections.sort(ps, new BeanComparator("lastName"));
		try {
			
			for (Provider p : ps) {
				if(!p.getProviderNo().equals("-1")){
				 providerList.add(new LabelValueBean(p.getLastName() + ", " + p.getFirstName(), p.getProviderNo()));
				}
			}
			
			
			} catch (Exception e) {
				MiscUtils.getLogger().error("Error", e);
			}
		request.setAttribute("providerSelect",providerList);
		
        ArrayList<LabelValueBean> priorityList = new ArrayList<LabelValueBean>();
        priorityList.add(new LabelValueBean("Select", "")); //key , value
        priorityList.add(new LabelValueBean("High", "High"));
        priorityList.add(new LabelValueBean("Normal", "Normal"));
        priorityList.add(new LabelValueBean("Low", "Low"));

        request.setAttribute("prioritySelect",priorityList);
		
		request.setAttribute("labRecallDelegate", delegate);
		request.setAttribute("labRecallMsgSubject", subject);
		
		ticklerAssignee.setChecked(checked);
		request.setAttribute("labRecallTicklerAssignee", ticklerAssignee);
		
		request.setAttribute("labRecallTicklerPriority", priority);
		
		request.setAttribute("providertitle","provider.setLabRecall.title");
		request.setAttribute("providermsgPrefs","provider.setLabRecall.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setLabRecall.msgProfileView");
		request.setAttribute("providermsgEdit","provider.setLabRecall.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setLabRecall.btnSubmit");
		request.setAttribute("providermsgSuccess","provider.setLabRecall.msgSuccess");
		request.setAttribute("method","saveLabRecallPrefs");

		frm.set("labRecallDelegate", delegate);
		frm.set("labRecallMsgSubject", subject);
		frm.set("labRecallTicklerAssignee", ticklerAssignee);
		frm.set("labRecallTicklerPriority", priority);

		return actionmapping.findForward("genLabRecallPrefs");
	}

    public ActionForward saveLabRecallPrefs(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request,HttpServletResponse response){
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm=(DynaActionForm)actionform;
    	UserProperty d=(UserProperty)frm.get("labRecallDelegate");
    	UserProperty s=(UserProperty)frm.get("labRecallMsgSubject");
    	UserProperty a=(UserProperty)frm.get("labRecallTicklerAssignee");   	
    	UserProperty p=(UserProperty)frm.get("labRecallTicklerPriority");

		String delegate = d != null ? d.getValue() : "";
		String subject = s != null ? s.getValue() : "";
		
		boolean checked = a != null ? a.isChecked() : false;
		
		String priority = p != null ? p.getValue() : "";

		boolean delete = false;
		if(delegate.equals("")){delete=true;}
   
		UserProperty dProperty = this.userPropertyDAO.getProp(providerNo, UserProperty.LAB_RECALL_DELEGATE);
		if( dProperty == null ) {
			dProperty = new UserProperty();
			dProperty.setProviderNo(providerNo);
			dProperty.setName(UserProperty.LAB_RECALL_DELEGATE);
		}

		if(delete){
		 userPropertyDAO.delete(dProperty);
		}else{
		 dProperty.setValue(delegate);
		 userPropertyDAO.saveProp(dProperty);
		}

		UserProperty sProperty = this.userPropertyDAO.getProp(providerNo, UserProperty.LAB_RECALL_MSG_SUBJECT);
		if( sProperty == null ) {
			sProperty = new UserProperty();
			sProperty.setProviderNo(providerNo);
			sProperty.setName(UserProperty.LAB_RECALL_MSG_SUBJECT);
		}
		if(delete){
		 userPropertyDAO.delete(sProperty);
		}else{
		 sProperty.setValue(subject);
		 userPropertyDAO.saveProp(sProperty);
		}
		
		String defaultToDelegate = "no";
		UserProperty aProperty = this.userPropertyDAO.getProp(providerNo, UserProperty.LAB_RECALL_TICKLER_ASSIGNEE);
		if( aProperty == null ) {
			aProperty = new UserProperty();
			aProperty.setProviderNo(providerNo);
			aProperty.setName(UserProperty.LAB_RECALL_TICKLER_ASSIGNEE);
		}
		if(delete){
		 userPropertyDAO.delete(aProperty);
		}else{
		 if(checked){
			 defaultToDelegate="yes";
		 }
			
		 aProperty.setValue(defaultToDelegate);
		 userPropertyDAO.saveProp(aProperty);
		}		
		
		UserProperty pProperty = this.userPropertyDAO.getProp(providerNo, UserProperty.LAB_RECALL_TICKLER_PRIORITY);
		if( pProperty == null ) {
			pProperty = new UserProperty();
			pProperty.setProviderNo(providerNo);
			pProperty.setName(UserProperty.LAB_RECALL_TICKLER_PRIORITY);
		}
		if(delete){
		 userPropertyDAO.delete(pProperty);
		}else{
		 pProperty.setValue(priority);
		 userPropertyDAO.saveProp(pProperty);
		}	
    	
		request.setAttribute("status", "success");
		request.setAttribute("providertitle","provider.setLabRecall.title");
		request.setAttribute("providermsgPrefs","provider.setLabRecall.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setLabRecall.msgProfileView");
		request.setAttribute("providermsgEdit","provider.setLabRecall.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setLabRecall.btnSubmit");
		
		String msgSuccess = "provider.setLabRecall.msgSuccess";
		if(delete){
		 msgSuccess = "provider.setLabRecall.msgDeleted";
		}
		request.setAttribute("providermsgSuccess",msgSuccess);
		
		request.setAttribute("method","saveLabRecallPrefs");
		
		return actionmapping.findForward("genLabRecallPrefs");
	}     

    public ActionForward viewEncounterWindowSize(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request, HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty width = this.userPropertyDAO.getProp(providerNo, "encounterWindowWidth");
		UserProperty height = this.userPropertyDAO.getProp(providerNo, "encounterWindowHeight");
		UserProperty maximize = this.userPropertyDAO.getProp(providerNo, "encounterWindowMaximize");

		if (width == null){
			width = new UserProperty();
		}
		if (height == null){
			height = new UserProperty();
		}
		if (maximize == null){
			maximize = new UserProperty();
		}
		if(maximize.getValue()!=null) {
			maximize.setChecked(maximize.getValue().equals("yes")?true:false);
		}

		request.setAttribute("width",width);
		request.setAttribute("height",height);
		request.setAttribute("maximize",maximize);


		request.setAttribute("providertitle","provider.encounterWindowSize.title"); //=Set myDrugref ID
		request.setAttribute("providermsgPrefs","provider.encounterWindowSize.msgPrefs"); //=Preferences"); //
		request.setAttribute("providermsgProvider","provider.encounterWindowSize.msgProvider"); //=myDrugref ID
		request.setAttribute("providermsgEdit","provider.encounterWindowSize.msgEdit"); //=Enter your desired login for myDrugref
		request.setAttribute("providerbtnSubmit","provider.encounterWindowSize.btnSubmit"); //=Save
		request.setAttribute("providermsgSuccess","provider.encounterWindowSize.msgSuccess"); //=myDrugref Id saved
		request.setAttribute("method","saveEncounterWindowSize");

		frm.set("encounterWindowWidth", width);
		frm.set("encounterWindowHeight", height);
		frm.set("encounterWindowMaximize", maximize);

		return actionmapping.findForward("genEncounterWindowSize");
    }

    public ActionForward saveEncounterWindowSize(ActionMapping actionmapping,ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty w = (UserProperty)frm.get("encounterWindowWidth");
		UserProperty h = (UserProperty)frm.get("encounterWindowHeight");
		UserProperty m = (UserProperty)frm.get("encounterWindowMaximize");

		String width = w != null ? w.getValue() : "";
		String height = h != null ? h.getValue() : "";
		boolean maximize = m != null ? m.isChecked() : false;

		UserProperty wProperty = this.userPropertyDAO.getProp(providerNo,"encounterWindowWidth");
		if( wProperty == null ) {
			wProperty = new UserProperty();
			wProperty.setProviderNo(providerNo);
			wProperty.setName("encounterWindowWidth");
		}
		wProperty.setValue(width);
		userPropertyDAO.saveProp(wProperty);

		UserProperty hProperty = this.userPropertyDAO.getProp(providerNo,"encounterWindowHeight");
		if( hProperty == null ) {
			hProperty = new UserProperty();
			hProperty.setProviderNo(providerNo);
			hProperty.setName("encounterWindowHeight");
		}
		hProperty.setValue(height);
		userPropertyDAO.saveProp(hProperty);

		UserProperty mProperty = this.userPropertyDAO.getProp(providerNo,"encounterWindowMaximize");
		if( mProperty == null ) {
			mProperty = new UserProperty();
			mProperty.setProviderNo(providerNo);
			mProperty.setName("encounterWindowMaximize");
		}
		mProperty.setValue(maximize?"yes":"no");
		userPropertyDAO.saveProp(mProperty);

		request.setAttribute("status", "success");
		request.setAttribute("providertitle","provider.encounterWindowSize.title"); //=Set myDrugref ID
		request.setAttribute("providermsgPrefs","provider.encounterWindowSize.msgPrefs"); //=Preferences"); //
		request.setAttribute("providermsgProvider","provider.encounterWindowSize.msgProvider"); //=myDrugref ID
		request.setAttribute("providermsgEdit","provider.encounterWindowSize.msgEdit"); //=Enter your desired login for myDrugref
		request.setAttribute("providerbtnSubmit","provider.encounterWindowSize.btnSubmit"); //=Save
		request.setAttribute("providermsgSuccess","provider.encounterWindowSize.msgSuccess"); //=myDrugref Id saved
		request.setAttribute("method","saveEncounterWindowSize");

		return actionmapping.findForward("genEncounterWindowSize");
	}

    public ActionForward viewEncounterPrintOptions(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request, HttpServletResponse response) {

        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        String providerNo = loggedInInfo.getLoggedInProviderNo();

        DynaActionForm frm = (DynaActionForm)actionform;
        UserProperty printOp = this.userPropertyDAO.getProp(providerNo, UserProperty.ENCOUNTER_PRINT_OPTION);
        UserProperty printCPP = this.userPropertyDAO.getProp(providerNo, UserProperty.ENCOUNTER_PRINT_CPP);
        UserProperty printRx = this.userPropertyDAO.getProp(providerNo, UserProperty.ENCOUNTER_PRINT_RX);
        UserProperty printLabs = this.userPropertyDAO.getProp(providerNo, UserProperty.ENCOUNTER_PRINT_LABS);

        ArrayList<LabelValueBean> printOptions = new ArrayList<LabelValueBean>(Arrays.asList(new LabelValueBean("All", "all"), new LabelValueBean("Dates", "dates")));

        if (printOp == null){
            printOp = new UserProperty();
            printOp.setValue("all");
        }
        if (printCPP == null){
            printCPP = new UserProperty();
        }
        if (printRx == null){
            printRx = new UserProperty();
        }
        if(printLabs == null) {
            printLabs = new UserProperty();
        }

        if(printCPP.getValue()!=null) {
            printCPP.setChecked(printCPP.getValue().equals("true")?true:false);
        }
        if(printRx.getValue()!=null) {
            printRx.setChecked(printRx.getValue().equals("true")?true:false);
        }
        if(printLabs.getValue()!=null) {
            printLabs.setChecked(printLabs.getValue().equals("true")?true:false);
        }

        request.setAttribute("dropOpts",printOptions);

        request.setAttribute("printOp",printOp);
        request.setAttribute("printCPP",printCPP);
        request.setAttribute("printRx",printRx);
        request.setAttribute("printLabs",printLabs);

        request.setAttribute("providertitle","provider.encounterPrintOptions.title"); //=Set myDrugref ID
        request.setAttribute("providermsgPrefs","provider.encounterPrintOptions.msgPrefs"); //=Preferences"); //
        request.setAttribute("providermsgProvider","provider.encounterPrintOptions.msgProvider"); //=myDrugref ID
        request.setAttribute("providermsgEdit","provider.encounterPrintOptions.msgEdit"); //=Enter your desired login for myDrugref
        request.setAttribute("providerbtnSubmit","provider.encounterPrintOptions.btnSubmit"); //=Save
        request.setAttribute("providermsgSuccess","provider.encounterPrintOptions.msgSuccess"); //=myDrugref Id saved
        request.setAttribute("method","saveEncounterPrintOptions");

        frm.set("printOp", printOp);
        frm.set("printCPP", printCPP);
        frm.set("printRx", printRx);
        frm.set("printLabs", printLabs);

        return actionmapping.findForward("genEncounterPrintOptions");
    }

    public ActionForward saveEncounterPrintOptions(ActionMapping actionmapping,ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {

        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        String providerNo = loggedInInfo.getLoggedInProviderNo();

        DynaActionForm frm = (DynaActionForm)actionform;
        UserProperty optionProp =(UserProperty)frm.get("printOp");
        UserProperty cppProp = (UserProperty)frm.get("printCPP");
        UserProperty rxProp = (UserProperty)frm.get("printRx");
        UserProperty labsProp = (UserProperty)frm.get("printLabs");

        String option = optionProp != null ? optionProp.getValue() : "";
        boolean cpp = cppProp != null ? cppProp.isChecked() : false;
        boolean rx = rxProp != null ? rxProp.isChecked() : false;
        boolean labs = labsProp != null ? labsProp.isChecked() : false;

        UserProperty printOp = this.userPropertyDAO.getProp(providerNo, UserProperty.ENCOUNTER_PRINT_OPTION);
        UserProperty printCPP = this.userPropertyDAO.getProp(providerNo, UserProperty.ENCOUNTER_PRINT_CPP);
        UserProperty printRx = this.userPropertyDAO.getProp(providerNo, UserProperty.ENCOUNTER_PRINT_RX);
        UserProperty printLabs = this.userPropertyDAO.getProp(providerNo, UserProperty.ENCOUNTER_PRINT_LABS);

        if (printOp == null){
            printOp = new UserProperty();
            printOp.setProviderNo(providerNo);
            printOp.setName(UserProperty.ENCOUNTER_PRINT_OPTION);
        }
        printOp.setValue(option);
        userPropertyDAO.saveProp(printOp);

        if (printCPP == null){
            printCPP = new UserProperty();
            printCPP.setProviderNo(providerNo);
            printCPP.setName(UserProperty.ENCOUNTER_PRINT_CPP);
        }
        printCPP.setValue(cpp?"true":"false");
        userPropertyDAO.saveProp(printCPP);

        if (printRx == null){
            printRx = new UserProperty();
            printRx.setProviderNo(providerNo);
            printRx.setName(UserProperty.ENCOUNTER_PRINT_RX);
        }
        printRx.setValue(rx?"true":"false");
        userPropertyDAO.saveProp(printRx);

        if(printLabs == null) {
            printLabs = new UserProperty();
            printLabs.setProviderNo(providerNo);
            printLabs.setName(UserProperty.ENCOUNTER_PRINT_LABS);
        }
        printLabs.setValue(labs?"true":"false");
        userPropertyDAO.saveProp(printLabs);

        request.setAttribute("status", "success");
        request.setAttribute("providertitle","provider.encounterPrintOptions.title"); //=Set myDrugref ID
        request.setAttribute("providermsgPrefs","provider.encounterPrintOptions.msgPrefs"); //=Preferences"); //
        request.setAttribute("providermsgProvider","provider.encounterPrintOptions.msgProvider"); //=myDrugref ID
        request.setAttribute("providermsgEdit","provider.encounterPrintOptions.msgEdit"); //=Enter your desired login for myDrugref
        request.setAttribute("providerbtnSubmit","provider.encounterPrintOptions.btnSubmit"); //=Save
        request.setAttribute("providermsgSuccess","provider.encounterPrintOptions.msgSuccess"); //=myDrugref Id saved
        request.setAttribute("method","saveEncounterWindowSize");

        return actionmapping.findForward("genEncounterPrintOptions");
    }

    public ActionForward viewQuickChartSize(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request, HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
				UserProperty size = this.userPropertyDAO.getProp(providerNo, "quickChartSize");

		if (size == null){
			size = new UserProperty();
		}


		request.setAttribute("size",size);


		request.setAttribute("providertitle","provider.quickChartSize.title"); //=Set myDrugref ID
		request.setAttribute("providermsgPrefs","provider.quickChartSize.msgPrefs"); //=Preferences"); //
		request.setAttribute("providermsgProvider","provider.quickChartSize.msgProvider"); //=myDrugref ID
		request.setAttribute("providermsgEdit","provider.quickChartSize.msgEdit"); //=Enter your desired login for myDrugref
		request.setAttribute("providerbtnSubmit","provider.quickChartSize.btnSubmit"); //=Save
		request.setAttribute("providermsgSuccess","provider.quickChartSize.msgSuccess"); //=myDrugref Id saved
		request.setAttribute("method","saveQuickChartSize");

		frm.set("quickChartSize", size);

		return actionmapping.findForward("genQuickChartSize");
    }

    public ActionForward saveQuickChartSize(ActionMapping actionmapping,ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty s = (UserProperty)frm.get("quickChartSize");

		String size = s != null ? s.getValue() : "";

		UserProperty wProperty = this.userPropertyDAO.getProp(providerNo,"quickChartSize");
		if( wProperty == null ) {
			wProperty = new UserProperty();
			wProperty.setProviderNo(providerNo);
			wProperty.setName("quickChartsize");
		}
		wProperty.setValue(size);
		userPropertyDAO.saveProp(wProperty);


		request.setAttribute("status", "success");
		request.setAttribute("providertitle","provider.quickChartSize.title"); //=Set myDrugref ID
		request.setAttribute("providermsgPrefs","provider.quickChartSize.msgPrefs"); //=Preferences"); //
		request.setAttribute("providermsgProvider","provider.quickChartSize.msgProvider"); //=myDrugref ID
		request.setAttribute("providermsgEdit","provider.quickChartSize.msgEdit"); //=Enter your desired login for myDrugref
		request.setAttribute("providerbtnSubmit","provider.quickChartSize.btnSubmit"); //=Save
		request.setAttribute("providermsgSuccess","provider.quickChartSize.msgSuccess"); //=myDrugref Id saved
		request.setAttribute("method","saveQuickChartSize");

		return actionmapping.findForward("genQuickChartSize");
	}

    public ActionForward viewIntegratorProperties(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
    	Facility facility = loggedInInfo.getCurrentFacility();
        UserProperty[] integratorProperties = new UserProperty[21];

        integratorProperties[0] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_SYNC);
        integratorProperties[1] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_ADMISSIONS);
        integratorProperties[2] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_ALLERGIES);
        integratorProperties[3] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_APPOINTMENTS);
        integratorProperties[4] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_BILLING);
        integratorProperties[5] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_CONSENT);
        integratorProperties[6] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_DOCUMENTS);
        integratorProperties[7] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_DRUGS);
        integratorProperties[8] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_DXRESEARCH);
        integratorProperties[9] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_EFORMS);
        integratorProperties[10] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_ISSUES);
        integratorProperties[11] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_LABREQ);
        integratorProperties[12] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_MEASUREMENTS);
        integratorProperties[13] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_NOTES);
        integratorProperties[14] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_PREVENTIONS);
        integratorProperties[15] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_FACILITY);
        integratorProperties[16] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_PROGRAMS);
        integratorProperties[17] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_PROVIDERS);
        integratorProperties[18] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_FULL_PUSH+facility.getId());
        integratorProperties[19] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_LAST_PUSH);
        integratorProperties[20] = this.userPropertyDAO.getProp(UserProperty.INTEGRATOR_PATIENT_CONSENT);
        
        request.setAttribute("integratorProperties", integratorProperties);
        return mapping.findForward("genIntegrator");
    }

    public ActionForward saveIntegratorProperties(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
    	Facility facility = loggedInInfo.getCurrentFacility();
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_ADMISSIONS, request.getParameter("integrator_demographic_admissions"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_ALLERGIES, request.getParameter("integrator_demographic_allergies"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_APPOINTMENTS, request.getParameter("integrator_demographic_appointments"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_BILLING, request.getParameter("integrator_demographic_billing"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_CONSENT, request.getParameter("integrator_demographic_consent"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_DOCUMENTS, request.getParameter("integrator_demographic_documents"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_DRUGS, request.getParameter("integrator_demographic_drugs"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_DXRESEARCH, request.getParameter("integrator_demographic_dxresearch"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_EFORMS, request.getParameter("integrator_demographic_eforms"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_ISSUES, request.getParameter("integrator_demographic_issues"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_LABREQ, request.getParameter("integrator_demographic_labreq"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_MEASUREMENTS, request.getParameter("integrator_demographic_measurements"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_NOTES, request.getParameter("integrator_demographic_notes"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_PREVENTIONS, request.getParameter("integrator_demographic_preventions"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_DEMOGRAPHIC_SYNC, request.getParameter("integrator_demographic_sync"));               
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_FACILITY, request.getParameter("integrator_facility"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_PROGRAMS, request.getParameter("integrator_programs"));
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_PROVIDERS, request.getParameter("integrator_providers"));
        
        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_PATIENT_CONSENT, 
        		( request.getParameter(UserProperty.INTEGRATOR_PATIENT_CONSENT) != null ) ? request.getParameter(UserProperty.INTEGRATOR_PATIENT_CONSENT) : "0" );

        this.userPropertyDAO.saveProp(UserProperty.INTEGRATOR_FULL_PUSH+facility.getId(), request.getParameter("integrator_full_push"));

        request.setAttribute("saved", true);

        return viewIntegratorProperties(mapping, form, request, response);
    }
 public ActionForward viewPatientNameLength(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request, HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty length = this.userPropertyDAO.getProp(providerNo, UserProperty.PATIENT_NAME_LENGTH);

		if (length == null){
			length = new UserProperty();
		}


		request.setAttribute("patientnameLength",length);


		request.setAttribute("providertitle","provider.patientNameLength.title"); 
		request.setAttribute("providermsgPrefs","provider.patientNameLength.msgPrefs"); 
		request.setAttribute("providermsgProvider","provider.patientNameLength.msgProvider"); 
		request.setAttribute("providermsgEdit","provider.patientNameLength.msgEdit"); 
		request.setAttribute("providerbtnSubmit","provider.patientNameLength.btnSubmit"); 
		request.setAttribute("providermsgSuccess","provider.patientNameLength.msgSuccess"); 
		request.setAttribute("method","savePatientNameLength");

		frm.set("patientNameLength", length);

		return actionmapping.findForward("genPatientNameLength");
    }

    public ActionForward savePatientNameLength(ActionMapping actionmapping,ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty s = (UserProperty)frm.get("patientNameLength");

		String length = s != null ? s.getValue() : "";

		UserProperty wProperty = this.userPropertyDAO.getProp(providerNo,UserProperty.PATIENT_NAME_LENGTH);
		if( wProperty == null ) {
			wProperty = new UserProperty();
			wProperty.setProviderNo(providerNo);
			wProperty.setName(UserProperty.PATIENT_NAME_LENGTH);
		}
		wProperty.setValue(length);
		userPropertyDAO.saveProp(wProperty);


		request.setAttribute("status", "success");
		request.setAttribute("providertitle","provider.patientNameLength.title"); //=Set myDrugref ID
		request.setAttribute("providermsgPrefs","provider.patientNameLength.msgPrefs"); //=Preferences"); //
		request.setAttribute("providermsgProvider","provider.patientNameLength.msgProvider"); //=myDrugref ID
		request.setAttribute("providermsgEdit","provider.patientNameLength.msgEdit"); //=Enter your desired login for myDrugref
		request.setAttribute("providerbtnSubmit","provider.patientNameLength.btnSubmit"); //=Save
		request.setAttribute("providermsgSuccess","provider.patientNameLength.msgSuccess"); //=myDrugref Id saved
		request.setAttribute("method","savePatientNameLength");

		return actionmapping.findForward("genPatientNameLength");
	}
    
     public ActionForward viewDisplayDocumentAs(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

         DynaActionForm frm = (DynaActionForm)actionform;
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
         UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.DISPLAY_DOCUMENT_AS);

         if (prop == null){
             prop = new UserProperty();
         }

         ArrayList<LabelValueBean> serviceList = new ArrayList<LabelValueBean>();
         serviceList.add(new LabelValueBean(UserProperty.PDF, UserProperty.PDF));
         serviceList.add(new LabelValueBean(UserProperty.IMAGE, UserProperty.IMAGE));

         request.setAttribute("dropOpts",serviceList);

         request.setAttribute("displayDocumentAsProperty",prop);

         request.setAttribute("providertitle","provider.displayDocumentAs.title");
         request.setAttribute("providermsgPrefs","provider.displayDocumentAs.msgPrefs");
         request.setAttribute("providermsgProvider","provider.displayDocumentAs.msgProvider");
         request.setAttribute("providermsgEdit","provider.displayDocumentAs.msgEdit");
         request.setAttribute("providerbtnSubmit","provider.displayDocumentAs.btnSubmit");
         request.setAttribute("providermsgSuccess","provider.displayDocumentAs.msgSuccess");
         request.setAttribute("method","saveDisplayDocumentAs");

         frm.set("displayDocumentAsProperty", prop);
         return actionmapping.findForward("genDisplayDocumentAs");
     }

    public ActionForward saveDisplayDocumentAs(ActionMapping actionmapping,
                               ActionForm actionform,
                               HttpServletRequest request,
                               HttpServletResponse response) {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
		
		DynaActionForm frm = (DynaActionForm)actionform;
         UserProperty prop = (UserProperty)frm.get("displayDocumentAsProperty");
         String fmt = prop != null ? prop.getValue() : "";
         UserProperty saveProperty = this.userPropertyDAO.getProp(providerNo,UserProperty.DISPLAY_DOCUMENT_AS);

         if( saveProperty == null ) {
             saveProperty = new UserProperty();
             saveProperty.setProviderNo(providerNo);
             saveProperty.setName(UserProperty.DISPLAY_DOCUMENT_AS);
         }

         saveProperty.setValue(fmt);
         this.userPropertyDAO.saveProp(saveProperty);

         request.setAttribute("status", "success");
         request.setAttribute("providertitle","provider.displayDocumentAs.title");
         request.setAttribute("providermsgPrefs","provider.displayDocumentAs.msgPrefs");
         request.setAttribute("providermsgProvider","provider.displayDocumentAs.msgProvider");
         request.setAttribute("providermsgEdit","provider.displayDocumentAs.msgEdit");
         request.setAttribute("providerbtnSubmit","provider.btnSubmit");
         request.setAttribute("providermsgSuccess","provider.displayDocumentAs.msgSuccess");
         request.setAttribute("method","saveDisplayDocumentAs");

         return actionmapping.findForward("genDisplayDocumentAs");
    }
    
    

    public ActionForward viewCobalt(ActionMapping actionmapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
		DynaActionForm frm = (DynaActionForm)actionform;
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
		UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.COBALT);

		String propValue="";
		if (prop == null){
			prop = new UserProperty();
		}else{
			propValue=prop.getValue();
		}

		boolean checked;
		if(propValue.equalsIgnoreCase("yes"))
			checked=true;
		else
			checked=false;

		prop.setChecked(checked);
		request.setAttribute("cobaltProperty", prop);
		request.setAttribute("providertitle","provider.cobalt.title"); //=Select if you want to use Rx3
		request.setAttribute("providermsgPrefs","provider.cobalt.msgPrefs"); //=Preferences
		request.setAttribute("providermsgProvider","provider.cobalt.msgProfileView"); //=Use Rx3
		request.setAttribute("providermsgEdit","provider.cobalt.msgEdit"); //=Do you want to use Rx3?
		request.setAttribute("providerbtnSubmit","provider.cobalt.btnSubmit"); //=Save
		request.setAttribute("providermsgSuccess","provider.cobalt.msgSuccess"); //=Rx3 Selection saved
		request.setAttribute("method","saveCobalt");

		frm.set("cobaltProperty", prop);

		return actionmapping.findForward("genCobalt");
    }
    

    public ActionForward saveCobalt(ActionMapping actionmapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
    	DynaActionForm frm=(DynaActionForm)actionform;
    	UserProperty Uprop=(UserProperty)frm.get("cobaltProperty");

		boolean checked=false;
		if(Uprop!=null)
			checked = Uprop.isChecked();
		UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.COBALT);
		if(prop==null){
			prop=new UserProperty();
			prop.setName(UserProperty.COBALT);
			prop.setProviderNo(providerNo);
		}
		String propValue="no";
		if(checked)
			propValue="yes";

		prop.setValue(propValue);
		this.userPropertyDAO.saveProp(prop);

		request.setAttribute("status", "success");
		request.setAttribute("cobaltProperty",prop);
		request.setAttribute("providertitle","provider.cobalt.title");
		request.setAttribute("providermsgPrefs","provider.cobalt.msgPrefs");
		request.setAttribute("providermsgProvider","provider.cobalt.msgProfileView");
		request.setAttribute("providermsgEdit","provider.cobalt.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.cobalt.btnSubmit");
		if(checked)
			request.setAttribute("providermsgSuccess","provider.cobalt.msgSuccess_selected");
		else
			request.setAttribute("providermsgSuccess","provider.cobalt.msgSuccess_unselected");
		request.setAttribute("method","saveCobalt");

		return actionmapping.findForward("genCobalt");
    }
    
    
    public ActionForward viewHideOldEchartLinkInAppt(ActionMapping actionmapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
		DynaActionForm frm = (DynaActionForm)actionform;
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
		UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.HIDE_OLD_ECHART_LINK_IN_APPT);

		String propValue="";
		if (prop == null){
			prop = new UserProperty();
		}else{
			propValue=prop.getValue();
		}

		boolean checked;
		if(propValue.equals("Y"))
			checked=true;
		else
			checked=false;

		prop.setChecked(checked);
		request.setAttribute("hideOldEchartLinkInApptProperty", prop);
		request.setAttribute("providertitle","provider.hideOldEchartLinkInAppt.title"); //=Hide Old Echart Link in Appointment
		request.setAttribute("providermsgPrefs","provider.hideOldEchartLinkInAppt.msgPrefs"); //=Preferences
		request.setAttribute("providerbtnSubmit","provider.hideOldEchartLinkInAppt.btnSubmit"); //=Save
		request.setAttribute("providerbtnCancel","provider.hideOldEchartLinkInAppt.btnCancel"); //=Cancel
		request.setAttribute("method","saveHideOldEchartLinkInAppt");

		frm.set("hideOldEchartLinkInApptProperty", prop);

		return actionmapping.findForward("genHideOldEchartLinkInAppt");
    }
    

    public ActionForward saveHideOldEchartLinkInAppt(ActionMapping actionmapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
    	DynaActionForm frm=(DynaActionForm)actionform;
    	UserProperty Uprop=(UserProperty)frm.get("hideOldEchartLinkInApptProperty");

		boolean checked=false;
		if(Uprop!=null)
			checked = Uprop.isChecked();
		UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.HIDE_OLD_ECHART_LINK_IN_APPT);
		if(prop==null){
			prop=new UserProperty();
			prop.setName(UserProperty.HIDE_OLD_ECHART_LINK_IN_APPT);
			prop.setProviderNo(providerNo);
		}
		String propValue="N";
		if(checked) propValue="Y";

		prop.setValue(propValue);
		this.userPropertyDAO.saveProp(prop);

		request.setAttribute("status", "success");
		request.setAttribute("hideOldEchartLinkInApptProperty",prop);
		request.setAttribute("providertitle","provider.hideOldEchartLinkInAppt.title"); //=Hide Old Echart Link in Appointment
		request.setAttribute("providermsgPrefs","provider.hideOldEchartLinkInAppt.msgPrefs"); //=Preferences
		request.setAttribute("providerbtnClose","provider.hideOldEchartLinkInAppt.btnClose"); //=Close
		if(checked)
			request.setAttribute("providermsgSuccess","provider.hideOldEchartLinkInAppt.msgSuccess_selected"); //=Old Echart Link Hidden in Appointment
		else
			request.setAttribute("providermsgSuccess","provider.hideOldEchartLinkInAppt.msgSuccess_unselected"); //Old Echart Link Shown in Appointment
		request.setAttribute("method","saveHideOldEchartLinkInAppt");

		return actionmapping.findForward("genHideOldEchartLinkInAppt");
    }

    
    public ActionForward viewDashboardPrefs(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request, HttpServletResponse response) {
    	DynaActionForm frm = (DynaActionForm)actionform;
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
		UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.DASHBOARD_SHARE);

		String propValue="";
		if (prop == null){
			prop = new UserProperty();
		}else{
			propValue=prop.getValue();
		}

		boolean checked;
		if(propValue.equals("true"))
			checked=true;
		else
			checked=false;

		prop.setChecked(checked);
		request.setAttribute("dashboardShareProperty", prop);
		request.setAttribute("providertitle","provider.dashboardPrefs.title"); 
		request.setAttribute("providermsgPrefs","provider.dashboardPrefs.msgPrefs"); //=Preferences
		request.setAttribute("providerbtnSubmit","provider.dashboardPrefs.btnSubmit"); //=Save
		request.setAttribute("providerbtnCancel","provider.dashboardPrefs.btnCancel"); //=Cancel
		request.setAttribute("method","saveDashboardPrefs");

		frm.set("dashboardShareProperty", prop);

		return actionmapping.findForward("genDashboardPrefs");
    }

    public ActionForward saveDashboardPrefs(ActionMapping actionmapping,ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
    	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
    	DynaActionForm frm=(DynaActionForm)actionform;
    	UserProperty Uprop=(UserProperty)frm.get("dashboardShareProperty");

		boolean checked=false;
		if(Uprop!=null)
			checked = Uprop.isChecked();
		UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.DASHBOARD_SHARE);
		if(prop==null){
			prop=new UserProperty();
			prop.setName(UserProperty.DASHBOARD_SHARE);
			prop.setProviderNo(providerNo);
		}
		String propValue="false";
		if(checked) propValue="true";

		prop.setValue(propValue);
		this.userPropertyDAO.saveProp(prop);

		request.setAttribute("status", "success");
		request.setAttribute("dashboardShareProperty",prop);
		request.setAttribute("providertitle","provider.dashboardPrefs.title"); 
		request.setAttribute("providermsgPrefs","provider.dashboardPrefs.msgPrefs"); //=Preferences
		request.setAttribute("providerbtnClose","provider.dashboardPrefs.btnClose"); //=Close
		if(checked)
			request.setAttribute("providermsgSuccess","provider.dashboardPrefs.msgSuccess_selected"); 
		else
			request.setAttribute("providermsgSuccess","provider.dashboardPrefs.msgSuccess_unselected"); 
		request.setAttribute("method","saveDashboardPrefs");

		return actionmapping.findForward("genDashboardPrefs");
	}

    public ActionForward viewAppointmentCardPrefs(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request, HttpServletResponse response) {

		DynaActionForm frm = (DynaActionForm)actionform;
		String provider = (String) request.getSession().getAttribute("user");

		UserProperty name = this.userPropertyDAO.getProp(provider, "APPT_CARD_NAME");
		UserProperty phone = this.userPropertyDAO.getProp(provider, "APPT_CARD_PHONE");
		UserProperty fax = this.userPropertyDAO.getProp(provider, "APPT_CARD_FAX");

		if (name == null){
			name = new UserProperty();
		}
		if (phone == null){
			phone = new UserProperty();
		}
		if (fax == null){
			fax = new UserProperty();
		}
		
		request.setAttribute("appointmentCardName",name);
		request.setAttribute("appointmentCardPhone",phone);
		request.setAttribute("appointmentCardFax",fax);


		request.setAttribute("providertitle","provider.appointmentCardPrefs.title"); //=Set myDrugref ID
		request.setAttribute("providermsgPrefs","provider.appointmentCardPrefs.msgPrefs"); //=Preferences"); //
		request.setAttribute("providermsgProvider","provider.appointmentCardPrefs.msgProvider"); //=myDrugref ID
		request.setAttribute("providermsgEdit","provider.appointmentCardPrefs.msgEdit"); //=Enter your desired login for myDrugref
		request.setAttribute("providerbtnSubmit","provider.appointmentCardPrefs.btnSubmit"); //=Save
		request.setAttribute("providermsgSuccess","provider.appointmentCardPrefs.msgSuccess"); //=myDrugref Id saved
		request.setAttribute("method","saveAppointmentCardPrefs");

		frm.set("appointmentCardName", name);
		frm.set("appointmentCardPhone", phone);
		frm.set("appointmentCardFax", fax);

		return actionmapping.findForward("genAppointmentCardPrefs");
    }

    public ActionForward saveAppointmentCardPrefs(ActionMapping actionmapping,ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {

		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty n = (UserProperty)frm.get("appointmentCardName");
		UserProperty p = (UserProperty)frm.get("appointmentCardPhone");
		UserProperty f = (UserProperty)frm.get("appointmentCardFax");

		String name = n != null ? n.getValue() : "";
		String phone = p != null ? p.getValue() : "";
		String fax = f != null ? f.getValue() : "";
		
		String provider = (String) request.getSession().getAttribute("user");

		UserProperty wProperty = this.userPropertyDAO.getProp(provider,"APPT_CARD_NAME");
		if( wProperty == null ) {
			wProperty = new UserProperty();
			wProperty.setProviderNo(provider);
			wProperty.setName("APPT_CARD_NAME");
		}
		wProperty.setValue(name);

		userPropertyDAO.saveProp(wProperty);

		UserProperty hProperty = this.userPropertyDAO.getProp(provider,"APPT_CARD_PHONE");
		if( hProperty == null ) {
			hProperty = new UserProperty();
			hProperty.setProviderNo(provider);
			hProperty.setName("APPT_CARD_PHONE");
		}
		hProperty.setValue(phone);
		userPropertyDAO.saveProp(hProperty);

		UserProperty mProperty = this.userPropertyDAO.getProp(provider,"APPT_CARD_FAX");
		if( mProperty == null ) {
			mProperty = new UserProperty();
			mProperty.setProviderNo(provider);
			mProperty.setName("APPT_CARD_FAX");
		}
		mProperty.setValue(fax);
		userPropertyDAO.saveProp(mProperty);

		request.setAttribute("status", "success");
		request.setAttribute("providertitle","provider.appointmentCardPrefs.title"); //=Set myDrugref ID
		request.setAttribute("providermsgPrefs","provider.appointmentCardPrefs.msgPrefs"); //=Preferences"); //
		request.setAttribute("providermsgProvider","provider.appointmentCardPrefs.msgProvider"); //=myDrugref ID
		request.setAttribute("providermsgEdit","provider.appointmentCardPrefs.msgEdit"); //=Enter your desired login for myDrugref
		request.setAttribute("providerbtnSubmit","provider.appointmentCardPrefs.btnSubmit"); //=Save
		request.setAttribute("providermsgSuccess","provider.appointmentCardPrefs.msgSuccess"); //=myDrugref Id saved
		request.setAttribute("method","saveAppointmentCardPrefs");

		return actionmapping.findForward("genAppointmentCardPrefs");
	}
    
    public ActionForward viewBornPrefs(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request, HttpServletResponse response) {
    	DynaActionForm frm = (DynaActionForm)actionform;
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
		UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.DISABLE_BORN_PROMPTS);

		String propValue="";
		if (prop == null){
			prop = new UserProperty();
		}else{
			propValue=prop.getValue();
		}

		boolean checked;
		if(propValue.equals("Y"))
			checked=true;
		else
			checked=false;

		prop.setChecked(checked);
		request.setAttribute("bornPromptsProperty", prop);
		request.setAttribute("providertitle","provider.bornPrefs.title"); 
		request.setAttribute("providermsgPrefs","provider.bornPrefs.msgPrefs"); //=Preferences
		request.setAttribute("providerbtnSubmit","provider.bornPrefs.btnSubmit"); //=Save
		request.setAttribute("providerbtnCancel","provider.bornPrefs.btnCancel"); //=Cancel
		request.setAttribute("method","saveBornPrefs");

		frm.set("bornPromptsProperty", prop);

		return actionmapping.findForward("genBornPrefs");
    }

    public ActionForward saveBornPrefs(ActionMapping actionmapping,ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
    	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();
    	DynaActionForm frm=(DynaActionForm)actionform;
    	UserProperty Uprop=(UserProperty)frm.get("bornPromptsProperty");

		boolean checked=false;
		if(Uprop!=null)
			checked = Uprop.isChecked();
		UserProperty prop=this.userPropertyDAO.getProp(providerNo, UserProperty.DISABLE_BORN_PROMPTS);
		if(prop==null){
			prop=new UserProperty();
			prop.setName(UserProperty.DISABLE_BORN_PROMPTS);
			prop.setProviderNo(providerNo);
		}
		String propValue="N";
		if(checked) propValue="Y";

		prop.setValue(propValue);
		this.userPropertyDAO.saveProp(prop);

		request.setAttribute("status", "success");
		request.setAttribute("bornPromptsProperty",prop);
		request.setAttribute("providertitle","provider.bornPrefs.title"); 
		request.setAttribute("providermsgPrefs","provider.bornPrefs.msgPrefs"); //=Preferences
		request.setAttribute("providerbtnClose","provider.bornPrefs.btnClose"); //=Close
		if(checked)
			request.setAttribute("providermsgSuccess","provider.bornPrefs.msgSuccess_selected"); 
		else
			request.setAttribute("providermsgSuccess","provider.bornPrefs.msgSuccess_unselected"); 
		request.setAttribute("method","saveBornPrefs");

		return actionmapping.findForward("genBornPrefs");
	}

    public ActionForward viewDefaultSearchMode(ActionMapping actionmapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo = loggedInInfo.getLoggedInProviderNo();
		ResourceBundle oscarResources = ResourceBundle.getBundle("oscarResources", Locale.ENGLISH);
		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.DEFAULT_SEARCH_MODE);

		if (prop == null){
			prop = new UserProperty();
		}

		ArrayList<LabelValueBean> serviceList = new ArrayList<LabelValueBean>();
		serviceList.add(new LabelValueBean(oscarResources.getString("demographic.zdemographicfulltitlesearch.formName"), "search_name"));
		serviceList.add(new LabelValueBean(oscarResources.getString("demographic.zdemographicfulltitlesearch.formPhone"), "search_phone"));
		serviceList.add(new LabelValueBean(oscarResources.getString("demographic.zdemographicfulltitlesearch.formDOB"), "search_dob"));
		serviceList.add(new LabelValueBean(oscarResources.getString("demographic.zdemographicfulltitlesearch.formAddr"), "search_address"));
		serviceList.add(new LabelValueBean(oscarResources.getString("demographic.zdemographicfulltitlesearch.formHIN"), "search_hin"));
		serviceList.add(new LabelValueBean(oscarResources.getString("demographic.zdemographicfulltitlesearch.formChart"), "search_chart_no"));
		serviceList.add(new LabelValueBean(oscarResources.getString("demographic.zdemographicfulltitlesearch.formDemographicNo"), "search_demographic_no"));
		if (OscarProperties.getInstance().isPropertyActive("FIRST_NATIONS_MODULE")) {
			serviceList.add(new LabelValueBean(oscarResources.getString("demographic.zdemographicfulltitlesearch.formBandNumber"), "search_band_number"));
		}
		
		request.setAttribute("dropOpts",serviceList);

		request.setAttribute("dateProperty",prop);


		request.setAttribute("providertitle","provider.setDefaultSearchMode.title");
		request.setAttribute("providermsgPrefs","provider.setDefaultSearchMode.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setDefaultSearchMode.msgDefaultSex");
		request.setAttribute("providermsgEdit","provider.setDefaultSearchMode.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setDefaultSearchMode.btnSubmit");
		request.setAttribute("providermsgSuccess","provider.setDefaultSearchMode.msgSuccess");
		request.setAttribute("method","saveDefaultSearchMode");

		frm.set("dateProperty", prop);
		
		return  actionmapping.findForward("gen");
	}
	public ActionForward saveDefaultSearchMode(ActionMapping actionmapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty prop = (UserProperty)frm.get("dateProperty");
		String fmt = prop != null ? prop.getValue() : "";
		UserProperty saveProperty = this.userPropertyDAO.getProp(providerNo,UserProperty.DEFAULT_SEARCH_MODE);

		if( saveProperty == null ) {
			saveProperty = new UserProperty();
			saveProperty.setProviderNo(providerNo);
			saveProperty.setName(UserProperty.DEFAULT_SEARCH_MODE);
		}

		saveProperty.setValue(fmt);
		this.userPropertyDAO.saveProp(saveProperty);

		request.setAttribute("status", "success");
		request.setAttribute("providertitle","provider.setDefaultSearchMode.title");
		request.setAttribute("providermsgPrefs","provider.setDefaultSearchMode.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setDefaultSearchMode.msgDefaultSex");
		request.setAttribute("providermsgEdit","provider.setDefaultSearchMode.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.btnSubmit");
		request.setAttribute("providermsgSuccess","provider.setDefaultSearchMode.msgSuccess");
		request.setAttribute("method","saveDefaultSearchMode");

		return actionmapping.findForward("gen");
	}

	public ActionForward viewDefaultOhipProvider(ActionMapping actionmapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
		String providerNo = loggedInInfo.getLoggedInProviderNo();
		ResourceBundle oscarResources = ResourceBundle.getBundle("oscarResources", Locale.ENGLISH);
		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.DEFAULT_OHIP_PROVIDER);

		if (prop == null){
			prop = new UserProperty();
		}

		ArrayList<LabelValueBean> optionList = new ArrayList<LabelValueBean>();
		List<Provider> providers = providerDao.getBillableProviders();
		
		optionList.add(new LabelValueBean("All Providers", "all"));
		for (Provider provider : providers) {
			optionList.add(new LabelValueBean(provider.getFormattedName(), provider.getProviderNo()));
		}
		
		request.setAttribute("dropOpts",optionList);

		request.setAttribute("dateProperty",prop);
		request.setAttribute("providertitle","provider.setDefaultOhipProvider.title");
		request.setAttribute("providermsgPrefs","provider.setDefaultOhipProvider.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setDefaultOhipProvider.msgDefaultText");
		request.setAttribute("providermsgEdit","provider.setDefaultOhipProvider.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setDefaultOhipProvider.btnSubmit");
		request.setAttribute("providermsgSuccess","provider.setDefaultOhipProvider.msgSuccess");
		request.setAttribute("method","saveDefaultOhipProvider");

		frm.set("dateProperty", prop);

		return  actionmapping.findForward("gen");
	}
	public ActionForward saveDefaultOhipProvider(ActionMapping actionmapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty prop = (UserProperty)frm.get("dateProperty");
		String fmt = prop != null ? prop.getValue() : "";
		UserProperty saveProperty = this.userPropertyDAO.getProp(providerNo,UserProperty.DEFAULT_OHIP_PROVIDER);

		if( saveProperty == null ) {
			saveProperty = new UserProperty();
			saveProperty.setProviderNo(providerNo);
			saveProperty.setName(UserProperty.DEFAULT_OHIP_PROVIDER);
		}

		saveProperty.setValue(fmt);
		this.userPropertyDAO.saveProp(saveProperty);

		request.setAttribute("status", "success");
		request.setAttribute("providertitle","provider.setDefaultOhipProvider.title");
		request.setAttribute("providermsgPrefs","provider.setDefaultOhipProvider.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setDefaultOhipProvider.msgDefaultText");
		request.setAttribute("providermsgEdit","provider.setDefaultOhipProvider.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setDefaultOhipProvider.btnSubmit");
		request.setAttribute("providermsgSuccess","provider.setDefaultOhipProvider.msgSuccess");
		request.setAttribute("method","saveDefaultOhipProvider");

		return actionmapping.findForward("gen");
	}
	
	public ActionForward viewDefaultRxPrintFullPage(ActionMapping actionmapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo = loggedInInfo.getLoggedInProviderNo();
		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.RX_DEFAULT_FULL_PRINT);

		if (prop == null){ prop = new UserProperty(); }

		ArrayList<LabelValueBean> optionList = new ArrayList<LabelValueBean>();
		optionList.add(new LabelValueBean("Yes", "yes"));
		optionList.add(new LabelValueBean("No", "no"));
		request.setAttribute("dropOpts",optionList);

		request.setAttribute("dateProperty",prop);
		request.setAttribute("providertitle","provider.setDefaultRxPrintFullPage.title");
		request.setAttribute("providermsgPrefs","provider.setDefaultRxPrintFullPage.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setDefaultRxPrintFullPage.msgDefaultText");
		request.setAttribute("providermsgEdit","provider.setDefaultRxPrintFullPage.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setDefaultRxPrintFullPage.btnSubmit");
		request.setAttribute("providermsgSuccess","provider.setDefaultRxPrintFullPage.msgSuccess");
		request.setAttribute("method","saveDefaultRxPrintFullPage");

		frm.set("dateProperty", prop);

		return  actionmapping.findForward("gen");
	}
	public ActionForward saveDefaultRxPrintFullPage(ActionMapping actionmapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String providerNo=loggedInInfo.getLoggedInProviderNo();

		DynaActionForm frm = (DynaActionForm)actionform;
		UserProperty prop = (UserProperty)frm.get("dateProperty");
		String fmt = prop != null ? prop.getValue() : "";
		UserProperty saveProperty = this.userPropertyDAO.getProp(providerNo,UserProperty.RX_DEFAULT_FULL_PRINT);

		if( saveProperty == null ) {
			saveProperty = new UserProperty();
			saveProperty.setProviderNo(providerNo);
			saveProperty.setName(UserProperty.RX_DEFAULT_FULL_PRINT);
		}

		saveProperty.setValue(fmt);
		this.userPropertyDAO.saveProp(saveProperty);

		request.setAttribute("status", "success");
		request.setAttribute("providertitle","provider.setDefaultRxPrintFullPage.title");
		request.setAttribute("providermsgPrefs","provider.setDefaultRxPrintFullPage.msgPrefs");
		request.setAttribute("providermsgProvider","provider.setDefaultRxPrintFullPage.msgDefaultText");
		request.setAttribute("providermsgEdit","provider.setDefaultRxPrintFullPage.msgEdit");
		request.setAttribute("providerbtnSubmit","provider.setDefaultRxPrintFullPage.btnSubmit");
		request.setAttribute("providermsgSuccess","provider.setDefaultRxPrintFullPage.msgSuccess");
		request.setAttribute("method","saveDefaultRxPrintFullPage");

		return actionmapping.findForward("gen");
	}

    public ActionForward viewDisplayCustomRosterStatus(ActionMapping actionmapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        String providerNo = loggedInInfo.getLoggedInProviderNo();
        DynaActionForm frm = (DynaActionForm)actionform;
        UserProperty prop = this.userPropertyDAO.getProp(providerNo, UserProperty.DISPLAY_CUSTOM_ROSTER_STATUS);

        if (prop == null){ prop = new UserProperty(); }

        ArrayList<LabelValueBean> optionList = new ArrayList<LabelValueBean>();
        optionList.add(new LabelValueBean("Yes", "true"));
        optionList.add(new LabelValueBean("No", "false"));
        request.setAttribute("dropOpts",optionList);

        request.setAttribute("dateProperty",prop);
        request.setAttribute("providertitle","provider.setDisplayCustomRosterStatus.title");
        request.setAttribute("providermsgPrefs","provider.setDisplayCustomRosterStatus.msgPrefs");
        request.setAttribute("providermsgProvider","provider.setDisplayCustomRosterStatus.msgDefaultText");
        request.setAttribute("providermsgEdit","provider.setDisplayCustomRosterStatus.msgEdit");
        request.setAttribute("providerbtnSubmit","provider.setDisplayCustomRosterStatus.btnSubmit");
        request.setAttribute("providermsgSuccess","provider.setDisplayCustomRosterStatus.msgSuccess");
        request.setAttribute("method","saveDisplayCustomRosterStatus");

        frm.set("dateProperty", prop);

        return  actionmapping.findForward("gen");
    }

    public ActionForward saveDisplayCustomRosterStatus(ActionMapping actionmapping, ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
        LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
        String providerNo=loggedInInfo.getLoggedInProviderNo();

        DynaActionForm frm = (DynaActionForm)actionform;
        UserProperty prop = (UserProperty)frm.get("dateProperty");
        String fmt = prop != null ? prop.getValue() : "";
        UserProperty saveProperty = this.userPropertyDAO.getProp(providerNo,UserProperty.DISPLAY_CUSTOM_ROSTER_STATUS);

        if( saveProperty == null ) {
            saveProperty = new UserProperty();
            saveProperty.setProviderNo(providerNo);
            saveProperty.setName(UserProperty.DISPLAY_CUSTOM_ROSTER_STATUS);
        }

        saveProperty.setValue(fmt);
        this.userPropertyDAO.saveProp(saveProperty);

        request.setAttribute("status", "success");
        request.setAttribute("providertitle","provider.setDisplayCustomRosterStatus.title");
        request.setAttribute("providermsgPrefs","provider.setDisplayCustomRosterStatus.msgPrefs");
        request.setAttribute("providermsgProvider","provider.setDisplayCustomRosterStatus.msgDefaultText");
        request.setAttribute("providermsgEdit","provider.setDisplayCustomRosterStatus.msgEdit");
        request.setAttribute("providerbtnSubmit","provider.setDisplayCustomRosterStatus.btnSubmit");
        request.setAttribute("providermsgSuccess","provider.setDisplayCustomRosterStatus.msgSuccess");
        request.setAttribute("method","saveDisplayCustomRosterStatus");

        return actionmapping.findForward("gen");
    }
	private UserProperty loadProperty(String providerNo, String name) {
        UserProperty prop = this.userPropertyDAO.getProp(providerNo, name);
        String propValue="";
        if (prop == null){
                prop = new UserProperty();
        }else{
                propValue=prop.getValue();
        }

        boolean checked;
        if(propValue.equals("true"))
                checked=true;
        else
                checked=false;

        prop.setChecked(checked);
        
        return prop;
    }

    
    public ActionForward viewClinicalConnectPrefs(ActionMapping actionmapping,ActionForm actionform,HttpServletRequest request, HttpServletResponse response) {
        DynaActionForm frm = (DynaActionForm)actionform;
        LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
        String providerNo=loggedInInfo.getLoggedInProviderNo();
       
        request.setAttribute("providertitle","provider.clinicalConnectPrefs.title"); 
        request.setAttribute("providermsgPrefs","provider.clinicalConnectPrefs.msgPrefs"); //=Preferences
        request.setAttribute("providerbtnSubmit","provider.clinicalConnectPrefs.btnSubmit"); //=Save
        request.setAttribute("providerbtnCancel","provider.clinicalConnectPrefs.btnCancel"); //=Cancel
        request.setAttribute("method","saveClinicalConnectPrefs");
        
        return actionmapping.findForward("genClinicalConnectPrefs");
    }
    
    private UserProperty saveProperty(DynaActionForm frm, String providerNo, String formName, String name) {
        
        UserProperty Uprop=(UserProperty)frm.get(formName);

                boolean checked=false;
                if(Uprop!=null)
                        checked = Uprop.isChecked();
                UserProperty prop=this.userPropertyDAO.getProp(providerNo, name);
                if(prop==null){
                        prop=new UserProperty();
                        prop.setName(name);
                        prop.setProviderNo(providerNo);
                }
                String propValue="false";
                if(checked) propValue="true";

                prop.setValue(propValue);
                this.userPropertyDAO.saveProp(prop);

                return prop;

    }


    public ActionForward saveClinicalConnectPrefs(ActionMapping actionmapping,ActionForm actionform, HttpServletRequest request, HttpServletResponse response) {
        LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
        String providerNo=loggedInInfo.getLoggedInProviderNo();
        DynaActionForm frm=(DynaActionForm)actionform;

        request.setAttribute("status", "success");
       
        request.setAttribute("providertitle","provider.clinicalConnectPrefs.title"); 
        request.setAttribute("providermsgPrefs","provider.clinicalConnectPrefs.msgPrefs"); //=Preferences
        request.setAttribute("providerbtnClose","provider.clinicalConnectPrefs.btnClose"); //=Close
        request.setAttribute("providermsgSuccess","provider.clinicalConnectPrefs.msgSuccess"); 
        
        request.setAttribute("method","saveClinicalConnectPrefs");

        return actionmapping.findForward("genClinicalConnectPrefs");

	}

    public ActionForward viewDefaultPsBillingCode(final ActionMapping actionMapping,
                                                  final @NonNull ActionForm actionForm,
                                                  final HttpServletRequest request,
                                                  final HttpServletResponse response) {
        val loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        val form = (DynaActionForm) actionForm;
        var userProperty = this.userPropertyDAO.getProp(loggedInInfo.getLoggedInProviderNo(), UserProperty.DEFAULT_PS_BILLING_CODE);
        if (userProperty == null) {
            userProperty = new UserProperty();
        }

        request.setAttribute("dateProperty", userProperty);
        request.setAttribute("dropOpts", painFormDefaultBillingCodeList);

        request.setAttribute("providertitle", "provider.setDefaultPsBillingCode.msgEdit");
        request.setAttribute("providermsgPrefs", "provider.cobalt.msgProvider");
        request.setAttribute("providermsgEdit", "provider.cobalt.msgProvider");
        request.setAttribute("providermsgProvider", "provider.setDefaultPsBillingCode.msgEdit");
        request.setAttribute("providerbtnSubmit", "provider.setDefaultRefPractitioner.btnSubmit");
        request.setAttribute("providermsgSuccess", "provider.setDefaultRefPractitioner.msgSuccess");
        request.setAttribute("hideValue", "true");
        request.setAttribute("method", "saveDefaultPsBillingCode");

        form.set("dateProperty", userProperty);
        return actionMapping.findForward("gen");
    }

    public ActionForward saveDefaultPsBillingCode(final ActionMapping actionmapping,
                                                  final @NonNull ActionForm actionForm,
                                                  final HttpServletRequest request,
                                                  final HttpServletResponse response) {

        val loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        val form = (DynaActionForm) actionForm;
        var userProperty = (UserProperty) form.get("dateProperty");
        UserProperty saveProperty = this.userPropertyDAO.getProp(loggedInInfo.getLoggedInProviderNo(),
                UserProperty.DEFAULT_PS_BILLING_CODE);

        if (saveProperty == null) {
            saveProperty = new UserProperty();
            saveProperty.setProviderNo(loggedInInfo.getLoggedInProviderNo());
            saveProperty.setName(UserProperty.DEFAULT_PS_BILLING_CODE);
        }
        saveProperty.setValue(userProperty != null ? userProperty.getValue() : "");
        this.userPropertyDAO.saveProp(saveProperty);

        request.setAttribute("providertitle", "provider.setDefaultPsBillingCode.msgEdit");
        request.setAttribute("providermsgPrefs", "provider.cobalt.msgProvider");
        request.setAttribute("providermsgEdit", "provider.cobalt.msgProvider");
        request.setAttribute("providermsgProvider", "provider.setDefaultPsBillingCode.msgEdit");
        request.setAttribute("providerbtnSubmit", "provider.setDefaultRefPractitioner.btnSubmit");
        request.setAttribute("providermsgSuccess", "provider.setDefaultPsBillingCode.msgSuccess");
        request.setAttribute("method", "saveDefaultPsBillingCode");
        request.setAttribute("status", "success");

        return actionmapping.findForward("gen");
    }

    /**
     * Creates a new instance of ProviderPropertyAction
     */
    public ProviderPropertyAction() {
    }

    private UserProperty getProperty(final String providerNumber, final String name) {
      return this.userPropertyDAO.getProp(providerNumber, name);
    }

    private void setCheckedValue(@NonNull final UserProperty property) {
      if (property.getValue() != null) {
        property.setChecked("on".equals(property.getValue()));
      }
    }

    private UserProperty getEformFavouriteGroupProperty(final String providerNumber) {
      var property = this.userPropertyDAO.getProp(providerNumber, UserProperty.EFORM_FAVOURITE_GROUP);
      if (property == null) {
        property = new UserProperty();
      }
      return property;
    }

    private UserProperty getDisplayEformGroupOnHoverProperty(final String providerNumber) {
      var property = this.userPropertyDAO.getProp(providerNumber, UserProperty.DISPLAY_EFORM_FAVOURITE_GROUP);
      if (property == null) {
        property = new UserProperty();
      }
      setCheckedValue(property);
      return property;
    }

    private String getLoggedInProviderNumber(final HttpServletRequest request) {
      return LoggedInInfo
          .getLoggedInInfoFromSession(request)
          .getLoggedInProviderNo();
    }

    private UserProperty getPropertyFromPropertyName(
        final String providerNumber, 
        final String propertyName
    ) {
      var property = this.userPropertyDAO.getProp(providerNumber, propertyName);
      if (property == null) {
        property = new UserProperty();
      }
      return property;
    }

    private static void addItemsToOptionList(
        final HttpServletRequest request,
        final LabelValueBean ... labels) {
      val optionList = new ArrayList<>(Arrays.asList(labels));
      request.setAttribute("dropOpts", optionList);
    }

    private static void setPreferenceAttributesForDefaultLetterhead(
        final HttpServletRequest request,
        final boolean saveSuccess
    ) {
      if (saveSuccess) {
        request.setAttribute("status", "success");
      }
      request.setAttribute("providertitle", "provider.setConsultDefaultLetterheadMultisite.title");
      request.setAttribute("providermsgPrefs", "provider.setConsultDefaultLetterheadMultisite.msgPrefs");
      request.setAttribute("providermsgProvider", "provider.setConsultDefaultLetterheadMultisite.msgDefaultLetterhead");
      request.setAttribute("providermsgEdit","provider.setConsultDefaultLetterheadMultisite.msgEdit");
      request.setAttribute("providerbtnSubmit","provider.setConsultDefaultLetterheadMultisite.btnSubmit");
      request.setAttribute("providermsgSuccess","provider.setConsultDefaultLetterheadMultisite.msgSuccess");
      request.setAttribute("method","saveDefaultLetterheadAddress");
    }

    private static void setDateProperty(
        final DynaActionForm actionForm,
        final UserProperty prop) {
      actionForm.set("dateProperty", prop);
    }

    private static String getDateProperty(
       final DynaActionForm actionForm
    ) {
      val prop = (UserProperty) actionForm.get("dateProperty");
      return prop != null ? prop.getValue() : "";
    }

    private void createOrGetUserProperty(
        final String providerNumber,
        final String fmt,
        final String propertyName
    ) {
      var saveProperty = this.userPropertyDAO.getProp(providerNumber, propertyName);
      if (saveProperty == null) {
        saveProperty = new UserProperty();
        saveProperty.setProviderNo(providerNumber);
        saveProperty.setName(propertyName);
      }

      saveProperty.setValue(fmt);
      this.userPropertyDAO.saveProp(saveProperty);
    }
}
