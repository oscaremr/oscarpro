package org.oscarehr.measurements.colcamexFlowSheets.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.val;
import lombok.var;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.oscarehr.common.dao.MeasurementTypeDao;
import org.oscarehr.common.dao.ValidationsDao;
import org.oscarehr.common.model.Allergy;
import org.oscarehr.common.model.Clinic;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.Measurement;
import org.oscarehr.common.model.MeasurementType;
import org.oscarehr.common.model.Validations;
import org.oscarehr.util.SpringUtils;

@Getter
@Setter
@NoArgsConstructor
public class ColcamexFlowSheetBean extends ActionForm implements Serializable {

  private Map<String, List<Measurement>> flowsheetMeasurements;
  private List<Measurement> measurementObjects;
  private String formId;
  private String measurementGroupName;
  private List<String> measurementGroupNames;
  private Demographic demographic;
  private Clinic clinicNfo;
  private Measurement patientHeight;
  private Measurement other;
  private Measurement problem;
  private Measurement site;
  private List<Allergy> allergies;
  private List<Measurement> problems;
  private List<Measurement> others;
  private Date date;
  private String title;
  private String imageLink;
  private boolean isError;
  private String appointmentNo;
  private static boolean setPatientHeight = false;
  private static final long serialVersionUID = 1L;
  private ValidationsDao validationsDao = SpringUtils.getBean(ValidationsDao.class);
  private MeasurementTypeDao measurementTypeDao = SpringUtils.getBean(MeasurementTypeDao.class);

  public static boolean isSetPatientHeight() {
    return setPatientHeight;
  }

  public ActionErrors validate(final ActionMapping mapping, final HttpServletRequest request) {
    ActionErrors errors = null;
    if (request.getParameter("save") != null
        && "save".equalsIgnoreCase(request.getParameter("save"))) {
      errors = new ActionErrors();
      Validations validation = null;
      String measurementData = null;

      for (var measurement : measurementObjects) {
        val typeName = measurement.getType();

        if (retrieveValidation(measurement) != null) {
          validation = retrieveValidation(measurement);
          measurementData = measurement.getDataField();
        }

        Boolean isInteger;
        if (measurementData != null || validation != null) {
          int minLength;
          if (validation.getMaxLength() != null) {
            minLength = validation.getMaxLength();
            if (measurementData.length() > minLength) {
              errors.add(
                  typeName,
                  new ActionMessage(
                      "form.colcamexFlowSheets.formColcamexFlowSheet.errors.maxlength",
                      typeName,
                      minLength
                  )
              );
            }
          }

          if (validation.getMinLength() != null) {
            minLength = validation.getMinLength();
            if (measurementData.length() < minLength) {
              errors.add(
                  typeName,
                  new ActionMessage(
                      "form.colcamexFlowSheets.formColcamexFlowSheet.errors.minlength",
                      typeName,
                      minLength
                  )
              );
            }
          }

          Double maxValue;
          Double measurementDataDouble;
          if (validation.getMinValue() != null) {
            maxValue = validation.getMinValue();

            try {
              measurementDataDouble = Double.valueOf(measurementData.trim());
              if (measurementDataDouble < maxValue) {
                errors.add(
                    typeName,
                    new ActionMessage(
                        "form.colcamexFlowSheets.formColcamexFlowSheet.errors.minvalue",
                        typeName,
                        maxValue
                    )
                );
              }
            } catch (NumberFormatException var24) {
              validation.setNumeric(true);
            }
          }

          if (validation.getMaxValue() != null) {
            maxValue = validation.getMaxValue();

            try {
              measurementDataDouble = Double.valueOf(measurementData.trim());
              if (measurementDataDouble > maxValue) {
                errors.add(
                    typeName,
                    new ActionMessage(
                        "form.colcamexFlowSheets.formColcamexFlowSheet.errors.maxvalue",
                        typeName,
                        maxValue
                    )
                );
              }
            } catch (NumberFormatException var23) {
              validation.setNumeric(true);
            }
          }

          if (validation.getRegularExp() != null && !measurementData.matches(
              validation.getRegularExp())) {
            errors.add(
                typeName,
                new ActionMessage(
                    "form.colcamexFlowSheets.formColcamexFlowSheet.errors.invalid",
                    measurementData,
                    typeName
                )
            );
          }

          if (validation.isDate() != null) {
            String pattern = "^(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])$";
            if (!measurementData.matches(pattern)) {
              errors.add(
                  typeName,
                  new ActionMessage(
                      "form.colcamexFlowSheets.formColcamexFlowSheet.errors.date",
                      typeName
                  )
              );
            }
          }

          if (validation.isNumeric() != null) {
            isInteger = validation.isNumeric();
            var isInputInteger = true;

            try {
              Double.valueOf(measurementData.trim());
            } catch (NumberFormatException var22) {
              isInputInteger = false;
            } finally {
              if (isInputInteger && !isInteger) {
                errors.add(
                    typeName,
                    new ActionMessage(
                        "form.colcamexFlowSheets.formColcamexFlowSheet.errors.nointeger",
                        typeName
                    )
                );
              } else if (!isInputInteger && isInteger) {
                errors.add(
                    typeName,
                    new ActionMessage(
                        "form.colcamexFlowSheets.formColcamexFlowSheet.errors.integer",
                        typeName
                    )
                );
              }

            }
          }
        } else if (validation.isTrue() != null) {
          isInteger = validation.isTrue();
          if (isInteger) {
            errors.add(
                typeName,
                new ActionMessage(
                    "form.colcamexFlowSheets.formColcamexFlowSheet.errors.required",
                    typeName
                )
            );
          }
        }
      }

      if (errors.size() > 0) {
        this.setError(true);
      }
    }

    return errors;
  }

  public void reset(final ActionMapping mapping, final HttpServletRequest request) {
    if (request.getParameter("save") == null) {
      if (this.problems != null) {
        this.problems.clear();
      }

      if (this.others != null) {
        this.others.clear();
      }

      if (this.allergies != null) {
        this.allergies.clear();
      }

      if (this.site != null) {
        this.site = null;
      }

      if (this.flowsheetMeasurements != null) {
        this.flowsheetMeasurements.clear();
      }
    }

  }

  public void setAppointmentNo(int appointmentNo) {
    this.setAppointmentNo("" + appointmentNo);
  }

  public void setAppointmentNo(String appointmentNo) {
    if (appointmentNo == null || appointmentNo.isEmpty()) {
      appointmentNo = "0";
    }

    this.appointmentNo = appointmentNo;
  }

  public void setDate(final Date date) {
    if (date != null) {
      this.date = date;
    }
  }

  public int getListSize() {
    return this.measurementObjects.size();
  }

  public static void isSetPatientHeight(final boolean isSetPatientHeight) {
    ColcamexFlowSheetBean.setPatientHeight = isSetPatientHeight;
  }

  public void setProblems(final List<Measurement> problems) {
    if (problems != null) {
      this.problems = problems;
    }
  }

  public void setOthers(final List<Measurement> others) {
    if (others != null) {
      this.others = others;
    }
  }

  public void setAllergies(final List<Allergy> allergies) {
    if (allergies != null) {
      this.allergies = allergies;
    }
  }

  public Measurement getMeasurement(int index) {
    return (Measurement) this.measurementObjects.get(index);
  }

  private Validations retrieveValidation(final Measurement measurement) {
    if (measurement == null) {
      return null;
    }
    MeasurementType measurementType =
        measurementTypeDao.findByType(measurement.getType()).size() > 0
            ? measurementTypeDao.findByType(measurement.getType()).get(0)
            : null;
    if (measurementType == null || measurementType.getValidation() == null) {
      return null;
    }
    return validationsDao.getValidationById(Integer.parseInt(measurementType.getValidation()));
  }
}
