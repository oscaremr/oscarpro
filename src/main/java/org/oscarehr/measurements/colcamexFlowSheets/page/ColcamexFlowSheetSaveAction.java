package org.oscarehr.measurements.colcamexFlowSheets.page;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.NoArgsConstructor;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.MeasurementDao;
import org.oscarehr.common.model.Measurement;
import org.oscarehr.measurements.colcamexFlowSheets.bean.ColcamexFlowSheetBean;
import org.oscarehr.util.SpringUtils;

@NoArgsConstructor
public class ColcamexFlowSheetSaveAction extends Action implements Serializable {

  private static final String DEFAULT_INSTRUCTION = "N/C";
  private static final String DEFAULT_COMMENT = "Colcamex Flowsheet";
  private static final String DEFAULT_DATA_FIELD = "[NT]";
  private MeasurementDao measurementDao = (MeasurementDao) SpringUtils.getBean("measurementDao");
  private ColcamexFlowSheetBean flowSheet;

  public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {
    this.flowSheet = (ColcamexFlowSheetBean) form;
    this.save();
    return mapping.findForward("success");
  }

  private void save() {
    Date date = new Date();
    val appointmentNo = this.flowSheet.getAppointmentNo();
    val newHeight = this.flowSheet.getPatientHeight();
    if (!ColcamexFlowSheetBean.isSetPatientHeight() && newHeight.getDataField() != null) {
      this.measurementDao.persist(this.setDefaultData(newHeight, date, appointmentNo));
    }

    val newSite = this.flowSheet.getSite();
    if (newSite.getComments() != null) {
      this.measurementDao.persist(this.setDefaultData(newSite, date, appointmentNo));
    }

    val newOther = this.flowSheet.getOther();
    val otherComment = newOther.getComments();
    if (otherComment != null) {
      this.measurementDao.persist(this.setDefaultData(newOther, date, appointmentNo));
    }

    val newProblem = this.flowSheet.getProblem();
    val problemComment = newProblem.getComments();
    if (problemComment != null || problemComment != "") {
      this.measurementDao.persist(this.setDefaultData(newProblem, date, appointmentNo));
    }

    List<Measurement> measures = this.flowSheet.getMeasurementObjects();

    for (int i = 0; measures.size() > i; ++i) {
      val measurement = (Measurement) measures.get(i);
      if (measurement.getDataField() != null) {
        this.measurementDao.persist(this.setDefaultData(measurement, date, appointmentNo));
      }
    }
  }

  private Measurement setDefaultData(final Measurement measurement, final Date date, final String appointmentNo) {

    measurement.setAppointmentNo(
        Integer.parseInt(StringUtils.isNotBlank(appointmentNo) ? appointmentNo : "0"));
    if (measurement.getComments() == null) {
      measurement.setComments("Colcamex Flowsheet");
    }

    if (measurement.getMeasuringInstruction() == null) {
      measurement.setMeasuringInstruction("N/C");
    }

    if (measurement.getDataField() == null) {
      measurement.setDataField("[NT]");
    }

    measurement.setDateObserved(date);
    return measurement;
  }
}
