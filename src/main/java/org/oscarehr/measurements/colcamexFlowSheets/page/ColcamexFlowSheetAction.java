package org.oscarehr.measurements.colcamexFlowSheets.page;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.NoArgsConstructor;
import lombok.val;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.AllergyDao;
import org.oscarehr.common.dao.ClinicDAO;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.MeasurementDao;
import org.oscarehr.common.dao.MeasurementGroupDao;
import org.oscarehr.common.dao.MeasurementTypeDao;
import org.oscarehr.common.model.Allergy;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.Measurement;
import org.oscarehr.common.model.MeasurementGroup;
import org.oscarehr.common.model.MeasurementType;
import org.oscarehr.measurements.colcamexFlowSheets.bean.ColcamexFlowSheetBean;
import org.oscarehr.measurements.colcamexFlowSheets.util.SortMeasurementsUtil;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import oscar.util.StringUtils;

@NoArgsConstructor
public class ColcamexFlowSheetAction extends Action implements Serializable {

  Logger logger = Logger.getLogger(this.getClass());
  public static final String DEFAULT_FORM_NAME = "ColcamexFlowsheet";

  public static final String DEFAULT_FORM_LINK = "../colcamexFlowSheet.do?demographic_no=";

  private static final String LOCAL_IMAGE_DIR = "/colcamexFlowSheetBeanForm/colcamexFlowsheets/images";

  private static final String DEFAULT_IMAGE_NAME = "tfsHeadingImg.png";
  private String eformImageDir = OscarProperties.getInstance().getProperty("eform_image");
  private DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
  private AllergyDao allergyDao = SpringUtils.getBean(AllergyDao.class);
  private MeasurementDao measurementDao = SpringUtils.getBean(MeasurementDao.class);
  private MeasurementGroupDao measurementGroupDao = SpringUtils.getBean(MeasurementGroupDao.class);
  private MeasurementTypeDao measurementTypeDao = SpringUtils.getBean(MeasurementTypeDao.class);
  private ClinicDAO clinicDao = SpringUtils.getBean(ClinicDAO.class);
  private String formName;
  private String imageLink;
  private ColcamexFlowSheetBean flowSheet;
  private Demographic demographic;
  private List<Allergy> allergy;
  private String demographicNumber;
  private String providerNumber;
  private List<Measurement> measurements;
  private List<MeasurementGroup> measurementGroup;
  private List<MeasurementType> measurementTypes;
  private SortMeasurementsUtil sortMeasurements;
  private Map<String, List<Measurement>> flowsheetMeasurements;
  private Measurement ptHeight;
  private ArrayList<Measurement> measurementObjects;
  private String formIdNumber;

  public ActionForward execute(
      final ActionMapping mapping,
      final ActionForm colcamexFlowSheetBeanForm,
      final HttpServletRequest request,
      HttpServletResponse response
  ) throws ServletException, IOException {
    this.flowSheet = (ColcamexFlowSheetBean) colcamexFlowSheetBeanForm;
    this.providerNumber = (String) request.getSession().getAttribute("user");
    this.sortMeasurements = new SortMeasurementsUtil();

    this.setDemographicForColcamexFlowSheetBeanForm(request.getParameter("demographic_no"));
    this.setFormIdForColcamexFlowSheetBeanForm(request.getParameter("formId"));
    this.setFormNameForColcamexFlowSheetBeanForm(request.getParameter("formname"));

    if (this.formName != null) {
      this.sortMeasurements.setMeasurementFormName(this.formName);
    }

    this.imageLink = this.getHeadingImage(
        new File(this.eformImageDir, "tfsHeadingImg.png"),
        "/colcamexFlowSheetBeanForm/colcamexFlowsheets/imagestfsHeadingImg.png",
        request.getContextPath()
    );
    this.flowSheet.setClinicNfo(this.clinicDao.getClinic());
    this.flowSheet.setImageLink(this.imageLink);
    this.flowSheet.setDate(new Date());
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CREATING COLCAMEX FLOWSHEET: ");
    stringBuilder.append("Form ID number: " + this.formIdNumber);
    stringBuilder.append("; Form title: " + this.flowSheet.getTitle());
    stringBuilder.append("; Form measurement group: " + this.formName);
    stringBuilder.append(
        "; Demographic name and number: " + this.flowSheet.getDemographic().getDisplayName() + " "
            + this.flowSheet.getDemographic().getDemographicNo());
    this.logger.debug(stringBuilder.toString());
    List heights;
    if (this.formIdNumber.equalsIgnoreCase("0")) {
      heights = this.measurementGroupDao.getAllGroupNames();
      this.flowSheet.setMeasurementGroupNames(heights);
      this.flowSheet.setFormId("-1");
      return mapping.findForward("success");
    } else {
      if (this.formIdNumber != null) {
        this.flowSheet.setFormId(this.formIdNumber);
        this.flowSheet.setAppointmentNo(this.formIdNumber);
        this.sortMeasurements.setMeasurementFormId(this.formIdNumber);
      } else {
        this.logger.error("Null returned on Form ID number.");
      }

      if (this.flowSheet.getDemographic().getDemographicNo() > 0) {
        this.allergy = this.allergyDao.findAllergies(Integer.parseInt(this.demographicNumber));
        this.measurementGroup = this.measurementGroupDao.getAllByGroup(this.formName);
        if (this.measurementGroup.size() <= 0) {
          request.setAttribute("error",
              "There are no measurement types inside the measurement group: " + this.formName);
          return mapping.findForward("error");
        }

        this.measurementTypes = this.measurementTypeDao.findByMeasurementGroupList(
            this.measurementGroup);
        this.sortMeasurements.sortByMeasurementTypeId(this.measurementTypes);
        this.measurements = this.measurementDao.findByDemographicId(
            Integer.parseInt(this.demographicNumber));
        if (this.measurementTypes != null) {
          this.measurementObjects = new ArrayList();
          val iterator = this.measurementTypes.iterator();

          while (iterator.hasNext()) {
            val type = (MeasurementType) iterator.next();
            val measurement = this.createMeasurementObject(type.getType());
            this.measurementObjects.add(measurement);
          }

          this.measurementObjects.add(this.createMeasurementObject("SITE"));
          this.flowSheet.setMeasurementObjects(this.measurementObjects);
        }

        if (this.measurements != null && this.measurements.size() > 0) {
          this.sortMeasurements.setMeasurements(this.measurements);
          this.flowsheetMeasurements = this.sortMeasurements.sortByType(this.measurementObjects);
          this.flowSheet.setFlowsheetMeasurements(this.flowsheetMeasurements);
          this.flowSheet.setOthers(this.sortMeasurements.sortByType("OTHR"));
          this.flowSheet.setProblems(this.sortMeasurements.sortByType("PBLM"));
          heights = this.sortMeasurements.sortByType("HT");
          if (heights != null && heights.size() > 0) {
            this.ptHeight = (Measurement) heights.get(0);
          }
        }

        if (this.ptHeight != null) {
          this.flowSheet.setPatientHeight(this.ptHeight);
          ColcamexFlowSheetBean.isSetPatientHeight(true);
          this.ptHeight = null;
        } else {
          this.flowSheet.setPatientHeight(this.createMeasurementObject("HT"));
          ColcamexFlowSheetBean.isSetPatientHeight(false);
        }

        this.flowSheet.setOther(this.createMeasurementObject("OTHR"));
        this.flowSheet.setProblem(this.createMeasurementObject("PBLM"));
        this.flowSheet.setSite(this.createMeasurementObject("SITE"));
        this.flowSheet.setAllergies(this.allergy);
      }

      return mapping.findForward("success");
    }
  }

  private void setDemographicForColcamexFlowSheetBeanForm(final String demographicNumber) {
    if (!StringUtils.isNullOrEmpty(demographicNumber)) {
      this.demographicNumber = demographicNumber;
      this.demographic = this.demographicDao.getDemographic(this.demographicNumber);
      if (this.demographic == null) {
        this.logger.error("Missing demographic information. ID returned NULL.");
        return;
      }

      this.flowSheet.setDemographic(this.demographic);
      this.flowSheet.setAppointmentNo(0);
    }
  }

  private void setFormIdForColcamexFlowSheetBeanForm(final String formId) {
    if (!StringUtils.isNullOrEmpty(formId)) {
      this.formIdNumber = formId;
    } else if (!this.flowSheet.getFormId().equals("-1")) {
      this.formIdNumber = this.flowSheet.getFormId();
    } else {
      this.formIdNumber = "-1";
    }
  }

  private void setFormNameForColcamexFlowSheetBeanForm(final String formName) {
    if (!StringUtils.isNullOrEmpty(formName)) {
      this.formName = formName;
      this.flowSheet.setTitle(this.formName);
    } else if (this.flowSheet.getTitle() != null) {
      this.formName = this.flowSheet.getTitle();
    } else if (this.flowSheet.getMeasurementGroupName() != null) {
      this.formName = this.flowSheet.getMeasurementGroupName();
    } else {
      this.formName = "ColcamexFlowsheet";
    }
  }

  private Measurement createMeasurementObject(final String type) {
    Measurement measurement = new Measurement();
    measurement.setType(type);
    measurement.setProviderNo(this.providerNumber);
    measurement.setDemographicId(Integer.parseInt(this.demographicNumber));
    return measurement;
  }

  private String getHeadingImage(final File from, final String to, final String context) {
    val outputPath = context + to;
    val absPath = this.getServlet().getServletContext().getRealPath("/") + to;
    val writeTo = new File(absPath);
    if (!from.exists() || !from.isFile() || !from.canRead()) {
      return null;
    } else if (writeTo.exists()) {
      return outputPath;
    } else {
      FileInputStream varFromSource = null;
      FileOutputStream VarToDestination = null;

      try {
        varFromSource = new FileInputStream(from);
        VarToDestination = new FileOutputStream(writeTo);
        byte[] buffer = new byte[4096];

        int bytes_read;
        while ((bytes_read = varFromSource.read(buffer)) != -1) {
          VarToDestination.write(buffer, 0, bytes_read);
        }
      } catch (Exception var23) {
        this.logger.fatal("Error occured " + var23.getMessage());
      } finally {
        if (varFromSource != null) {
          try {
            varFromSource.close();
          } catch (IOException var22) {
            this.logger.fatal("Error is " + var22.getMessage());
          }
        }

        if (VarToDestination != null) {
          try {
            VarToDestination.close();
          } catch (IOException var21) {
            this.logger.fatal("Error is " + var21.getMessage());
          }
        }
      }
      return outputPath;
    }
  }
}
