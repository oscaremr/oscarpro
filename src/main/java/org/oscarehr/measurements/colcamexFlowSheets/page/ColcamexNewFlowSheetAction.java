package org.oscarehr.measurements.colcamexFlowSheets.page;

import java.io.Serializable;
import java.sql.Time;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import lombok.var;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.EncounterFormDao;
import org.oscarehr.common.dao.FormDao;
import org.oscarehr.common.model.EncounterForm;
import org.oscarehr.common.model.Form;
import org.oscarehr.measurements.colcamexFlowSheets.bean.ColcamexFlowSheetBean;
import org.oscarehr.util.SpringUtils;

public class ColcamexNewFlowSheetAction extends Action implements Serializable {
  Logger logger = Logger.getLogger(this.getClass());
  private String linkPattern = ".*colcamexFlowSheet[\\_]{1}[\\d]{1,}[\\.]{1}[do]{1}.*";
  private String DEFAULT_FORM_TABLE = "form";

  public ColcamexNewFlowSheetAction() {
  }

  public ActionForward execute(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request, final HttpServletResponse response) {
    ColcamexFlowSheetBean flowSheet = (ColcamexFlowSheetBean)form;
    FormDao formDao = (FormDao)SpringUtils.getBean("formDao");
    EncounterFormDao encounterFormDao = (EncounterFormDao)SpringUtils.getBean("encounterFormDao");
    val measurementGroupName = flowSheet.getMeasurementGroupName();
    val demographicNo = flowSheet.getDemographic().getDemographicNo();
    val providerNo = (String)request.getSession().getAttribute("user");
    String defaultFormLink = null;
    var formTable = this.DEFAULT_FORM_TABLE;
    String formLink = null;
    int lastLinkNumber = 0;
    boolean isThisFormNew = true;
    Form flowsheetForm = new Form();
    flowsheetForm.setContent("Colcamex Flowsheet");
    flowsheetForm.setDemographicNo(demographicNo);
    flowsheetForm.setFormDate(flowSheet.getDate());
    flowsheetForm.setFormName(measurementGroupName);
    flowsheetForm.setFormTime(new Time(flowSheet.getDate().getTime()));
    flowsheetForm.setProviderNo(providerNo);
    formDao.persist(flowsheetForm);
    int flowsheetFormId = flowsheetForm.getId();
    flowSheet.setFormId(String.valueOf(flowsheetFormId));
    flowSheet.setTitle(measurementGroupName);
    List<EncounterForm> encounterFormList = encounterFormDao.findByFormName(measurementGroupName);
    Iterator<EncounterForm> it = null;
    if (encounterFormList != null && encounterFormList.size() > 0) {
      it = encounterFormList.iterator();

      while(it.hasNext()) {
        if (((EncounterForm)it.next()).getFormName().equalsIgnoreCase(measurementGroupName)) {
          isThisFormNew = false;
        }
      }
    }

    if (isThisFormNew) {
      List<EncounterForm> defaultEncounterFormList = encounterFormDao.findByFormName("ColcamexFlowsheet");
      List<EncounterForm> entireTable = encounterFormDao.findAll();
      if (defaultEncounterFormList == null || defaultEncounterFormList.size() <= 0) {
        this.logger.error("The default form entry for Colcamex Flowsheet is missing (or incorrect) from the EncounterForm data table. Ensure that an entry with the form name: ColcamexFlowsheet is present in the EncounterForm DB table.");
        return mapping.findForward("error");
      }

      EncounterForm encounterForm;
      for(it = defaultEncounterFormList.iterator(); it.hasNext(); defaultFormLink = encounterForm.getFormValue()) {
        encounterForm = (EncounterForm)it.next();
        val table = encounterForm.getFormTable();
        if (!table.isEmpty()) {
          formTable = table;
        }
      }

      if (entireTable != null && entireTable.size() > 0) {
        it = entireTable.iterator();

        while(it.hasNext()) {
          int linkNumber = 0;
          EncounterForm entireEncounterForm = (EncounterForm)it.next();
          val link = entireEncounterForm.getFormValue();
          if (link.matches(this.linkPattern)) {
            linkNumber = Integer.parseInt(link.substring(link.indexOf("_") + 1, link.indexOf(".do")));
          }

          if (linkNumber > lastLinkNumber) {
            lastLinkNumber = linkNumber;
          }
        }

        formLink = defaultFormLink.replace(".do", "_" + (lastLinkNumber + 1) + ".do");
      }

      this.logger.debug("Creating new Colcamex Flowsheet based on measurement group: " + measurementGroupName + " New link: " + formLink);
      encounterForm = new EncounterForm();
      encounterForm.setDisplayOrder(0);
      encounterForm.setFormName(measurementGroupName);
      encounterForm.setFormTable(formTable);
      encounterForm.setFormValue(formLink);
      encounterFormDao.persist(encounterForm);
    }

    return mapping.findForward("success");
  }
}
