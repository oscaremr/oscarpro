package org.oscarehr.measurements.colcamexFlowSheets.thc;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.val;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.oscarehr.common.model.Measurement;

@NoArgsConstructor
public class ResultTable extends BodyTagSupport {
  Logger logger = Logger.getLogger(this.getClass());
  private static final long serialVersionUID = 1L;
  @Setter private Map resultsMap;
  @Setter private List<Measurement> entryRowList;
  @Setter private String property;
  @Setter private String id;
  @Setter private String name;
  @Setter private String entryRow;
  @Setter private String addColumn = null;
  @Setter private String addRow = null;

  public void setBodyContent(BodyContent bc) {
    super.setBodyContent(bc);
  }

  public int doStartTag() throws JspException {
    JspWriter out = this.pageContext.getOut();
    Object bean = this.pageContext.findAttribute(this.name);

    try {
      if (this.property != null) {
        this.resultsMap = (Map)PropertyUtils.getProperty(bean, this.property);
      }

      if (this.entryRow != null) {
        this.entryRowList = (List)PropertyUtils.getProperty(bean, this.entryRow);
      }
    } catch (IllegalAccessException var5) {
      this.logger.fatal("Error is " + var5.getMessage());
    } catch (InvocationTargetException var6) {
      this.logger.fatal("Error is " + var6.getMessage());
    } catch (NoSuchMethodException var7) {
      this.logger.fatal("Error is " + var7.getMessage());
    }

    try {
      out.append("<table ");
      if (this.id != null) {
        out.append("id=\"" + this.id + "\"");
      }

      out.append(">");
      if (this.entryRowList != null && this.entryRowList.size() > 0) {
        Iterator<Measurement> it = this.entryRowList.iterator();
        out.append("<tr>");
        if (this.addColumn != null) {
          out.append("<th>");
          out.append(this.addColumn);
          out.append("</th>");
        }

        String type = null;

        while(it.hasNext()) {
          type = ((Measurement)it.next()).getType();
          if (!this.addRow.equalsIgnoreCase(type)) {
            out.append("<th>");
            out.append(type);
            out.append("</th>");
          }
        }

        out.append("</tr>\n");
      }
    } catch (IOException var8) {
      this.logger.fatal("Error is " + var8.getMessage());
    }

    return 1;
  }

  public int doAfterBody() {
    if (this.entryRowList != null && this.entryRowList.size() > 0 && this.resultsMap != null && this.resultsMap.size() > 0) {
      JspWriter out = this.pageContext.getOut();
      String rowClass = "";
      String rowData = "n/c";
      Iterator<Measurement> it = this.entryRowList.iterator();
      int rowcount = ((List)this.resultsMap.get(((Measurement)it.next()).getType())).size();
      int colcount = this.entryRowList.size();

      try {
        for(int row = 0; rowcount > row; ++row) {
          it = this.entryRowList.iterator();

          for(int col = 1; it.hasNext(); ++col) {
            val colName = ((Measurement)it.next()).getType();
            List<Measurement> measurements = (List)this.resultsMap.get(colName);
            Measurement measurement = (Measurement)measurements.get(row);
            val type = measurement.getType();
            val txDate = measurement.getDateObserved();
            if (col == 1) {
              if (row % 2 == 0) {
                rowClass = "even";
              } else {
                rowClass = "odd";
              }

              out.append("\n<tr class=\"");
              out.append(rowClass);
              out.append("\" >");
              if (this.addColumn != null) {
                out.append("<td class=\"datecol\" >");
                if (txDate != null) {
                  out.append(txDate.toString());
                } else {
                  out.append("Null");
                }

                out.append("</td>");
              }
            }

            if (type.equalsIgnoreCase(this.addRow)) {
              rowData = measurement.getComments();
            } else {
              out.append("<td id=\"");
              out.append(type + txDate.toString());
              out.append("\" >");
              out.append(measurement.getDataField());
              out.append("</td>");
            }

            if (col == colcount) {
              out.append("</tr>\n");
              out.append("\n<tr class=\"");
              out.append(rowClass);
              out.append("\" >");
              out.append("<td align=\"right\" class=\"datecol\" >SITE:</td>");
              out.append("<td colspan=\" ");
              out.append(Integer.toString(colcount));
              out.append("\" id=\"");
              out.append(txDate.toString());
              out.append("\" >");
              out.append(rowData);
              out.append("</td>");
              out.append("</tr>\n");
            }
          }
        }
      } catch (IOException var14) {
        this.logger.fatal("Error is " + var14.getMessage());
      }
    }

    return 1;
  }

  public int doEndTag() {
    JspWriter out = this.pageContext.getOut();

    try {
      out.print("</table>");
    } catch (IOException var3) {
      this.logger.fatal("Error is " + var3.getMessage());
    }

    return 6;
  }
}
