package org.oscarehr.measurements.colcamexFlowSheets.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.val;
import org.apache.log4j.Logger;
import org.oscarehr.common.model.Measurement;
import org.oscarehr.common.model.MeasurementType;

@NoArgsConstructor
public class SortMeasurementsUtil {

  public static final String PATIENT_HEIGHT = "HT";
  public static final String TYPE_OTHER = "OTHR";
  public static final String TYPE_PROBLEM = "PBLM";
  public static final String TYPE_SITE = "SITE";
  Logger logger = Logger.getLogger(this.getClass());
  private List<Measurement> measurements;
  private Map<String, List<Measurement>> flowsheetMeasurements;
  private String typeFilter = null;
  @Getter private String measurementFormId;
  @Getter private String measurementFormName;
  private static Comparator<MeasurementType> byMeasurementOrder = new Comparator<MeasurementType>() {
    String patternString = "\\[([1-9]?[1-9]*?)\\]";
    Pattern pattern;

    {
      this.pattern = Pattern.compile(this.patternString);
    }

    public int compare(final MeasurementType s1, final MeasurementType s2) {
      val n1String = s1.getTypeDescription();
      val n2String = s2.getTypeDescription();
      Integer n1Integer = 0;
      Integer n2Integer = 0;
      Matcher n1matcher = this.pattern.matcher(n1String);
      Matcher n2matcher = this.pattern.matcher(n2String);
      if (n1matcher.find()) {
        n1Integer = Integer.parseInt(n1matcher.group(1));
      }

      if (n2matcher.find()) {
        n2Integer = Integer.parseInt(n2matcher.group(1));
      }

      if (n1Integer == 0) {
        return -1;
      } else {
        return n2Integer == 0 ? -1 : n1Integer.compareTo(n2Integer);
      }
    }
  };
  private static Comparator<Measurement> byDate = new Comparator<Measurement>() {
    public int compare(Measurement o1, Measurement o2) {
      val n1Date = o1.getDateObserved();
      val n2Date = o2.getDateObserved();
      if (n1Date == null) {
        return -1;
      } else {
        return n2Date == null ? -1 : n1Date.compareTo(n2Date);
      }
    }
  };

  public void setMeasurementFormName(final String measurementFormName) {
    this.measurementFormName = measurementFormName.trim();
  }

  public void setMeasurementFormId(final String measurementFormId) {
    this.measurementFormId = measurementFormId.trim();
  }

  public SortMeasurementsUtil(final List<Measurement> measurements) {
    if (measurements != null) {
      this.setMeasurements(measurements);
    }

  }

  public void setMeasurements(final List<Measurement> measurements) {
    if (measurements != null) {
      Iterator<Measurement> it = measurements.iterator();
      while (it.hasNext()) {
        if (((Measurement) it.next()).getDateObserved() == null) {
          it.remove();
        }
      }

      this.measurements = measurements;
    }

  }

  public void addTypeFilter(final String type) {
    if (this.typeFilter == null) {
      this.typeFilter = type;
    }

  }

  public List<Measurement> sortByType(final String type) {
    List<Measurement> measurements = this.filterMeasurement(type);
    return measurements.size() > 0 ? measurements : null;
  }

  public Map<String, List<Measurement>> sortByType(final List<Measurement> measurementTypes) {
    this.flowsheetMeasurements = new HashMap();
    Iterator<Measurement> typeIt = measurementTypes.iterator();
    List<Date> masterColumn = null;
    int masterColumnSize = 0;

    while (typeIt.hasNext()) {
      String type = ((Measurement) typeIt.next()).getType();
      this.flowsheetMeasurements.put(type, this.filterMeasurement(type));
    }

    if (this.typeFilter != null) {
      this.flowsheetMeasurements.put(this.typeFilter, this.filterMeasurement(this.typeFilter));
    }

    if (this.flowsheetMeasurements != null && this.flowsheetMeasurements.size() > 0) {
      masterColumn = this.findMasterColumn(this.flowsheetMeasurements);
      if (masterColumn != null) {
        masterColumnSize = masterColumn.size();
      }
    }

    if (masterColumnSize > 0) {
      Iterator<String> keyIt = this.flowsheetMeasurements.keySet().iterator();

      label49:
      while (true) {
        String columnName;
        ArrayList column;
        int columnSize;
        do {
          if (!keyIt.hasNext()) {
            break label49;
          }

          columnName = (String) keyIt.next();
          column = (ArrayList) this.flowsheetMeasurements.get(columnName);
          columnSize = column.size();
        } while (masterColumnSize == columnSize);

        Iterator var10 = masterColumn.iterator();

        while (var10.hasNext()) {
          Date masterDate = (Date) var10.next();
          boolean result = false;
          Iterator var13 = column.iterator();

          Measurement blankMeasurement;
          while (var13.hasNext()) {
            blankMeasurement = (Measurement) var13.next();
            if (masterDate.compareTo(blankMeasurement.getDateObserved()) == 0) {
              result = true;
            }
          }

          if (!result) {
            blankMeasurement = new Measurement();
            blankMeasurement.setDataField("[NT]");
            blankMeasurement.setComments("No Comment");
            blankMeasurement.setDateObserved(masterDate);
            blankMeasurement.setType(columnName);
            column.add(blankMeasurement);
          }
        }
      }
    }
    this.sortOutput(this.flowsheetMeasurements);
    return this.flowsheetMeasurements;
  }

  public void sortByDate(final List<Measurement> dateList) {
    Collections.sort(dateList, Collections.reverseOrder(byDate));
  }

  public void sortByDate(final Map<String, List<Measurement>> measurementMap) {
    Iterator<String> keyIt = measurementMap.keySet().iterator();
    while (keyIt.hasNext()) {
      this.sortByDate((List) measurementMap.get(keyIt.next()));
    }

  }

  private void sortOutput(final Map<String, List<Measurement>> measurementMap) {
    this.sortByDate(measurementMap);
  }

  public void sortByMeasurementTypeId(final List<MeasurementType> typeList) {
    Collections.sort(typeList, byMeasurementOrder);
  }

  private List<Measurement> filterMeasurement(final String type) {
    ArrayList<Measurement> measurementGroup = new ArrayList();
    if (this.measurements != null) {
      Iterator<Measurement> it = this.measurements.iterator();

      while (it.hasNext()) {
        Measurement measurementSelected = (Measurement) it.next();
        if (measurementSelected.getType().equalsIgnoreCase(type)) {
          val measurementId = "" + measurementSelected.getAppointmentNo();
          if (measurementId.equalsIgnoreCase("0")) {
            measurementGroup.add(measurementSelected);
          }

          if (measurementId.equalsIgnoreCase(this.getMeasurementFormId())) {
            measurementGroup.add(measurementSelected);
          }
        }
      }
    }
    return measurementGroup;
  }

  private List<Date> findMasterColumn(final Map<String, List<Measurement>> map) {
    if (map.size() <= 1) {
      return null;
    } else {
      ArrayList<Date> longestList = this.getDateArray(map);
      Iterator<String> keyIt = map.keySet().iterator();
      if (longestList != null) {
        while (true) {
          while (true) {
            ArrayList measurements;
            int numberRows;
            do {
              if (!keyIt.hasNext()) {
                return longestList;
              }

              measurements = (ArrayList) map.get(keyIt.next());
              numberRows = measurements.size();
            } while (numberRows <= 0);

            Date date;
            if (numberRows == 1) {
              date = ((Measurement) measurements.get(0)).getDateObserved();
              if (!longestList.contains(date)) {
                longestList.add(date);
              }
            } else {
              Iterator var8 = measurements.iterator();

              while (var8.hasNext()) {
                Measurement measurement = (Measurement) var8.next();
                date = measurement.getDateObserved();
                if (!longestList.contains(date)) {
                  longestList.add(date);
                }
              }
            }
          }
        }
      } else {
        return longestList;
      }
    }
  }

  public ArrayList<Date> getDateArray(final Map<String, List<Measurement>> map) {
    Iterator<String> keyIt = map.keySet().iterator();
    ArrayList<Measurement> masterColumn = null;
    ArrayList<Date> dateList = null;
    int longestColumn = 0;

    while (keyIt.hasNext()) {
      ArrayList<Measurement> measurements = (ArrayList) map.get(keyIt.next());
      int measurementsSize = measurements.size();
      if (measurementsSize > longestColumn) {
        longestColumn = measurementsSize;
        masterColumn = measurements;
      }
    }

    if (masterColumn != null && masterColumn.size() > 0) {
      this.sortByDate((List) masterColumn);
      dateList = new ArrayList();
      Iterator var9 = masterColumn.iterator();

      while (var9.hasNext()) {
        Measurement measurement = (Measurement) var9.next();
        dateList.add(measurement.getDateObserved());
      }
    }
    return dateList;
  }
}