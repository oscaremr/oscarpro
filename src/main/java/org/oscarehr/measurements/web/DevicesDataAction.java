package org.oscarehr.measurements.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.common.dao.DevicesDao;
import org.oscarehr.common.model.Devices;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

public class DevicesDataAction extends DispatchAction {

	private DevicesDao devicesDao = SpringUtils.getBean(DevicesDao.class);
	
	public ActionForward getAllDevicesName(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException{
		JSONArray ret = new JSONArray();
		List<String> deviceName = devicesDao.findAllDevicesName();
		ret = JSONArray.fromObject(deviceName);
		try{
			PrintWriter out = response.getWriter();
			out.write(ret.toString());
			out.flush();
			out.close();
		}catch(Exception e){
			MiscUtils.getLogger().error(e.toString());
		}
		return null;
	}
	
	public ActionForward getDeviceDataByName(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException{
		String deviceName = request.getParameter("deviceName");
		String demoNo = request.getParameter("demographicNo");
		Devices deviceData = null;
		Devices dd1 = devicesDao.findLastTimeDeviceDataByName(deviceName);
		Devices dd2 = devicesDao.findLastTimeDeviceDataByNameAndDemoIdNotNull(deviceName);
		
		if(null == dd2){
			deviceData = dd1;
		}else{
			if(null != dd1){
				if(dd1.getId() > dd2.getId()){
					deviceData = dd1;
				}
			}
		}
		
		JSONObject ret = new JSONObject();
		if(null != deviceData){
			String data = deviceData.getData();
			data = data.replace("\r", "");
			boolean canUsed = true;
			if(deviceName.indexOf("LM-600Pd") > -1){
				if(data.length() == 89 || data.length() == 56 || data.length() == 103 || data.length() == 59){
					ret.put("dataIsRight", "true");
					if(data.indexOf("AR") > -1 || data.indexOf("AL") > -1){
						ret.put("dataType", "bifocal");
					}else{
						ret.put("dataType", "blank");
					}
				}else{
					ret.put("dataIsRight", "false");
					canUsed = false;
				}
				
				if(canUsed){
					String gl = data.substring(20);
					
					if(gl.indexOf("L") > -1){
						String gl_l = gl.substring(gl.indexOf("L") + 1);
						String str = gl_l.substring(0, 6);
						ret.put("osSph", RegularString(str));
						
						str = gl_l.substring(6, 12);
						ret.put("osCyl", RegularString(str));
						
						str = gl_l.substring(12, 15);
						ret.put("osAxis", RegularString(str));
						
						if(gl_l.indexOf("AL") > -1){
							String al = gl_l.substring(gl_l.indexOf("AL") + 2);
							str = al.substring(0, 5);
							ret.put("osAdd", RegularString(str));
						}
						
						String pl = gl_l.substring(gl_l.indexOf("PL") + 2);
						String pl_value = "";
						str = pl.substring(0, 6);
						pl_value += RegularString(str);
						 
						pl = pl.substring(pl.lastIndexOf("PL") + 2);
						str = pl.substring(0, 6);
						pl_value += RegularString(str);
						ret.put("osPrism", pl_value);
					}
					
					if(gl.indexOf("R") > -1){
						String gl_r = gl.substring(gl.indexOf("R") + 1);
						String str = gl_r.substring(0, 6);
						ret.put("odSph", RegularString(str));
						
						str = gl_r.substring(6, 12);
						ret.put("odCyl", RegularString(str));
						
						str = gl_r.substring(12, 15);
						ret.put("odAxis", RegularString(str));
						
						if(gl_r.indexOf("AR") > -1){
							String al = gl_r.substring(gl_r.indexOf("AR") + 2);
							str = al.substring(0, 5);
							ret.put("odAdd", RegularString(str));
						}
						
						String pr = gl_r.substring(gl_r.indexOf("PR") + 2);
						String pr_value = "";
						str = pr.substring(0, 6);
						pr_value += RegularString(str);
						 
						pr = pr.substring(pr.lastIndexOf("PR") + 2);
						str = pr.substring(0, 6);
						pr_value += RegularString(str);
						ret.put("odPrism", pr_value);
					}
				}
			}
			if(deviceName.indexOf("TONOREF3") > -1){
				//default
				ret.put("dataIsRight", "true");
				data = data.replace("TONOREF3", "");
				int num = 0;
				//AR
				num = data.indexOf("OL");
				String str = "";
				String ar_s = "";
				String m = "";
				if(num > 0){
					str = data.substring(num + 2);
				}else{
					str = data;
				}
				num = str.indexOf("OL");
				if(num >= 0){
					ar_s = str.substring(0, num);
					m = ar_s.substring(0, 6);
					ret.put("v_ls", RegularString(m));
					
					m = ar_s.substring(6, 12);
					ret.put("v_lc", RegularString(m));
					
					m = ar_s.substring(12, 15);
					ret.put("v_lx", RegularString(m));
				}
				
				String ar_l = "";
				num = str.indexOf("OR");
				if(num > 0){
					str = str.substring(num + 2);
				}
				num = str.indexOf("OR");
				if(num > 0){
					ar_l = str.substring(0, num);
					m = ar_l.substring(0, 6);
					ret.put("v_rs", RegularString(m));
					
					m = ar_l.substring(6, 12);
					ret.put("v_rc", RegularString(m));
					
					m = ar_l.substring(12, 15);
					ret.put("v_rx", RegularString(m));
				}
				
//				String pd = "";
//				num = str.indexOf("PD");
//				if(num > 0){
//					str = str.substring(num + 2);
//					
//					num = str.indexOf("?");
//					if(num > 0){
//						pd = str.substring(0, num);
//						m = pd.substring(0, 2);
//						ret.put("v_pd", RegularString(m));
//					}
//				}
				
				//IOP
				num = str.indexOf("DPM");
				if(num > 0){
					String iop = str.substring(0,num);
					num = iop.lastIndexOf("L");
					if(num > -1){
						iop = str.substring(num);
					}else{
						num = iop.lastIndexOf("R");
						if(num > -1){
							iop = str.substring(num);
						}
					}
					
//					num = iop.lastIndexOf("AV");
//					if(num > -1){
//						iop = str.substring(num);
//					}
					
					num = iop.indexOf("L");
					if(num > -1){
//						iop = str.substring(num);
//						num = iop.lastIndexOf("L");
//						if(num > -1){
//							iop = str.substring(num);
//						}
						
						num = iop.indexOf("AV");
						if(num > -1){
							String iop_l = iop.substring(num + 2);
							m = iop_l.substring(0, 4);
							ret.put("iop_ln", RegularString(m));
							
							num = iop_l.indexOf("L");
							if(num > -1){
								iop_l = iop_l.substring(num);
								if(iop_l.indexOf("AV") > -1){
									iop_l = iop_l.substring(iop_l.indexOf("AV") + 2);
									m = iop_l.substring(0, 4);
									ret.put("cct_l", RegularString(m));
								}
							}
						}
					}
					
					num = iop.indexOf("R");
					if(num > -1){
						iop = iop.substring(num);
//						num = iop.lastIndexOf("R");
//						if(num > -1){
//							iop = iop.substring(num);
//						}
						
						num = iop.indexOf("AV");
						if(num > -1){
							String iop_r = iop.substring(num + 2);
							m = iop_r.substring(0, 4);
							ret.put("iop_rn", RegularString(m));
							
							num = iop_r.indexOf("R");
							if(num > -1){
								iop_r = iop_r.substring(num);
								if(iop_r.indexOf("AV") > -1){
									iop_r = iop_r.substring(iop_r.indexOf("AV") + 2);
									m = iop_r.substring(0, 4);
									ret.put("cct_r", RegularString(m));
								}
							}
						}
					}
				}
				
				//R,K
				num = str.indexOf("Dkm");
				if(num > 0){
					String rk = str.substring(num + 4);
					if(rk.indexOf("L") > -1){
						num = rk.indexOf("L");
						String rk_l = rk.substring(num + 1);
						
						m = rk_l.substring(0, 5);
						ret.put("v_lr1", RegularString(m));
						
						m = rk_l.substring(5, 10);
						ret.put("v_lr2", RegularString(m));
						
						num = rk_l.indexOf("DL");
						if(num > 0){
							rk_l = rk_l.substring(num + 2);
							
							m = rk_l.substring(0, 5);
							ret.put("v_lk1", RegularString(m));
							m = rk_l.substring(5, 10);
							ret.put("v_lk2", RegularString(m));
							m = rk_l.substring(10, 13);
							ret.put("v_lkx", RegularString(m));
						}
					}
					if(rk.indexOf("R") > -1){
						num = rk.indexOf("R");
						String rk_r = rk.substring(num + 1);
						
						m = rk_r.substring(0, 5);
						ret.put("v_rr1", RegularString(m));
						
						m = rk_r.substring(5, 10);
						ret.put("v_rr2", RegularString(m));
						
						num = rk_r.indexOf("DR");
						if(num > 0){
							rk_r = rk_r.substring(num + 2);
							
							m = rk_r.substring(0, 5);
							ret.put("v_rk1", RegularString(m));
							m = rk_r.substring(5, 10);
							ret.put("v_rk2", RegularString(m));
							m = rk_r.substring(10, 13);
							ret.put("v_rkx", RegularString(m));
						}
					}
				}
			}
			
			if(deviceName.indexOf("RT5100") > -1){
//				if(data.length() == 106){
//					ret.put("dataIsRight", "true");
//				}else{
//					ret.put("dataIsRight", "false");
//					canUsed = false;
//				}
				ret.put("dataIsRight", "true");
				//old data
//				if(canUsed){
//					//OD
//					String str = data.substring(50, 56);
//					ret.put("v_rs_finial", RegularString(str));
//					
//					str = data.substring(56, 62);
//					ret.put("v_rc_finial", RegularString(str));
//					
//					str = data.substring(62, 65);
//					ret.put("v_rx_finial", RegularString(str));
//					
//					//OS
//					str = data.substring(69, 75);
//					ret.put("v_ls_finial", RegularString(str));
//					
//					str = data.substring(75, 81);
//					ret.put("v_lc_finial", RegularString(str));
//					
//					str = data.substring(81, 84);
//					ret.put("v_lx_finial", RegularString(str));
//				}
				
				//new data
				//m_dist
				if(data.indexOf("FR") > -1){
					//m dist OD
					String dist = data.substring(data.indexOf("FR") + 2);
					String str = dist.substring(0, 6);
					ret.put("v_rds", RegularString(str));
					
					str = dist.substring(6, 12);
					ret.put("v_rdc", RegularString(str));
					
					str = dist.substring(12, 15);
					ret.put("v_rdx", RegularString(str));
				}
				
				if(data.indexOf("FL") > -1){
					//m dist OS
					String dist = data.substring(data.indexOf("FL") + 2);
					String str = dist.substring(0, 6);
					ret.put("v_lds", RegularString(str));
					
					str = dist.substring(6, 12);
					ret.put("v_ldc", RegularString(str));
					
					str = dist.substring(12, 15);
					ret.put("v_ldx", RegularString(str));
				}
				
				if(data.indexOf("AR") > -1){
					//m near OD
					String near = data.substring(data.indexOf("AR") + 2);
					String str = near.substring(0, 6);
					ret.put("v_rns", RegularString(str));
				}
				
				if(data.indexOf("AL") > -1){
					//m near OS
					String near = data.substring(data.indexOf("AL") + 2);
					String str = near.substring(0, 6);
					ret.put("v_lns", RegularString(str));
				}
				
				String pd = "";
				if(data.indexOf("PD") > -1){
					String pd_str = data.substring(data.indexOf("PD") + 2);
					String str = pd_str.substring(0, 4);
					pd = RegularString(str);
				}
				if(data.indexOf("Pd") > -1){
					String pd_str = data.substring(data.indexOf("Pd") + 2);
					String str = pd_str.substring(0, 4);
					if(pd.length() > 0){
						pd = pd + "/" + RegularString(str);
					}
				}
				if(pd.length() > 0){
					ret.put("v_pd", RegularString(pd));
				}
			}
			if(canUsed){
				deviceData.setDemoId(Integer.parseInt(demoNo));
				deviceData.setStatus(1);
				devicesDao.merge(deviceData);
			}
		}
		
		try{
			PrintWriter out = response.getWriter();
			out.write(ret.toString());
			out.flush();
			out.close();
		}catch(Exception e){
			MiscUtils.getLogger().error(e.toString());
		}
		return null;
	}
	
	private String RegularString(String str){
//		str = str.replace("+", "");
		
		if(str.indexOf(".") == -1 && str.indexOf("00") == 0){
			str = str.substring(2);
		}
		if(str.indexOf("0") == 0 && str.length() > 1){
			str = str.substring(1);
		}
		if(str.indexOf("+0") == 0){
			str = str.replace("+0", "+");
		}
		if(str.indexOf("-0") == 0){
			str = str.replace("-0", "-");
		}
		if(str.indexOf("+ ") == 0){
			str = str.replace("+ ", "+");
		}
		if(str.indexOf("- ") == 0){
			str = str.replace("- ", "-");
		}
		if(str.equals("00.00") || str.equals("0.00") || str.equals("+0.00") || str.equals("-0.00")){
			str = "0";
		}
		
		return str;
	}
}
