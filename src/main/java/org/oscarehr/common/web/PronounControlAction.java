/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.web;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.common.dao.DemographicPronounDao;
import org.oscarehr.common.model.DemographicPronoun;
import org.oscarehr.util.SpringUtils;

public class PronounControlAction extends DispatchAction {

  DemographicPronounDao demographicPronounDao = SpringUtils.getBean(DemographicPronounDao.class);

  public ActionForward addPronoun(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException {
    val pronoun = request.getParameter("pronoun");
    val demographicPronoun = new DemographicPronoun();
    demographicPronoun.setEditable(true);
    demographicPronoun.setValue(pronoun);
    demographicPronounDao.persist(demographicPronoun);
    response.getWriter().print(demographicPronoun.getId());
    return null;
  }
  public ActionForward deletePronounById(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException {
    val idStr = request.getParameter("id");
    val id = Integer.parseInt(idStr);
    demographicPronounDao.deletePronounById(id);
    return null;
  }

  public ActionForward editPronoun(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException {
    val pronoun = request.getParameter("pronoun");
    val idStr = request.getParameter("id");
    val id = Integer.parseInt(idStr);
    val demographicPronoun = new DemographicPronoun();
    demographicPronoun.setValue(pronoun);
    demographicPronoun.setId(id);
    demographicPronounDao.updatePronoun(demographicPronoun);
    return null;
  }

}
