/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.web;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.common.dao.DemographicGenderDao;
import org.oscarehr.common.model.DemographicGender;
import org.oscarehr.util.SpringUtils;

public class GenderControlAction extends DispatchAction {
  DemographicGenderDao demographicGenderDao = SpringUtils.getBean(DemographicGenderDao.class);

  public ActionForward addGender(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException {
    val gender = request.getParameter("gender");
    val demographicGender = new DemographicGender();
    demographicGender.setEditable(true);
    demographicGender.setValue(gender);
    demographicGenderDao.persist(demographicGender);
    response.getWriter().print(demographicGender.getId());
    return null;
  }

  public ActionForward deleteGenderById(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException {
    val idStr = request.getParameter("id");
    val id = Integer.parseInt(idStr);
    demographicGenderDao.deleteGenderById(id);
    return null;
  }

  public ActionForward editGender(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException {
    val gender = request.getParameter("gender");
    val idStr = request.getParameter("id");
    val id = Integer.parseInt(idStr);
    val demographicGender = new DemographicGender();
    demographicGender.setValue(gender);
    demographicGender.setId(id);
    demographicGenderDao.updateGender(demographicGender);
    return null;
  }

}
