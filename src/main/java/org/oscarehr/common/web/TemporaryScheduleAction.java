/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */


package org.oscarehr.common.web;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.MyGroupDao;
import org.oscarehr.common.model.MyGroup;
import org.oscarehr.common.model.MyGroupPrimaryKey;
import org.oscarehr.common.model.Provider;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

public class TemporaryScheduleAction extends DispatchAction {
    private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
    
    public TemporaryScheduleAction() {}

    public ActionForward execute(ActionMapping mapping, ActionForm form, 
                                          HttpServletRequest request, HttpServletResponse response) {

        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_appointment,_day", "r", null)) {
            throw new SecurityException("missing required security object (_appointment,_day)");
        }
        
        Provider loggedInProvider = LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProvider();
        ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
        MyGroupDao myGroupDao = SpringUtils.getBean(MyGroupDao.class);
        
        String[] providerNos = request.getParameterValues("provider_checked");
        List<Provider> providerList = providerDao.getProviders(providerNos);
        String customGroupNo = "tmp-" + loggedInProvider.getProviderNo();

        myGroupDao.deleteTemporaryGroup(customGroupNo);
        
        for (Provider p : providerList){
            MyGroup myGroup = new MyGroup();
            myGroup.setId(new MyGroupPrimaryKey(customGroupNo, p.getProviderNo()));
            myGroup.setFirstName(p.getFirstName());
            myGroup.setLastName(p.getLastName());
            myGroup.setViewOrder(Arrays.asList(providerNos).indexOf(p.getProviderNo()));
            myGroupDao.saveEntity(myGroup);
        }
        
        request.setAttribute("refreshAndClose", true);
        request.setAttribute("customGroupNo", customGroupNo);
        
        return mapping.findForward("success");
    }
}
