/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.web;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.common.dao.DemographicFirstNationDao;
import org.oscarehr.common.model.DemographicExtKey;
import org.oscarehr.common.model.DemographicFirstNation;
import org.oscarehr.util.SpringUtils;
import lombok.val;
import oscar.util.StringUtils;

public class DemographicFirstNationControlAction extends DispatchAction {
  DemographicFirstNationDao demographicFirstNationDao
      = SpringUtils.getBean(DemographicFirstNationDao.class);

  public ActionForward addDemographicFirstNationCommunity(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException {
    val demographicFirstNationCommunity = request.getParameter(DemographicExtKey.DEMOGRAPHIC_FIRST_NATION_COMMUNITY.getKey());
    val demographicFirstNation
        = DemographicFirstNation.builder().editable(true).firstNationCommunity(demographicFirstNationCommunity).build();
    demographicFirstNationDao.persist(demographicFirstNation);
    response.getWriter().print(demographicFirstNation.getId());
    return null;
  }
  
  public ActionForward deleteDemographicFirstNationCommunityById(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws IOException {
    if (request.getParameter("id") != null) {
      val id = StringUtils.parseInt(request.getParameter("id"));
      demographicFirstNationDao.deleteBandNameById(id);
    }
    return null;
  }
	
  public ActionForward editDemographicFirstNationCommunity(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws IOException {
    if (request.getParameter(DemographicExtKey.DEMOGRAPHIC_FIRST_NATION_COMMUNITY.getKey()) != null
        && request.getParameter("id") != null) {
      val firstNationCommunity = request.getParameter(DemographicExtKey.DEMOGRAPHIC_FIRST_NATION_COMMUNITY.getKey());
      val id = StringUtils.parseInt(request.getParameter("id"));
      val demographicFirstNation
          = DemographicFirstNation.builder().firstNationCommunity(firstNationCommunity).id(id).build();
      demographicFirstNationDao.updateFirstNation(demographicFirstNation);
    }
    return null;
  }
}
