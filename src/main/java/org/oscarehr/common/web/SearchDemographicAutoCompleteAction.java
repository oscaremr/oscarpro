/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.var;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicExtKey;
import org.oscarehr.util.AppointmentUtil;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.oscarRx.data.RxProviderData;
import oscar.oscarRx.data.RxProviderData.Provider;
import oscar.OscarProperties;

public class SearchDemographicAutoCompleteAction extends Action {

	private final DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
	private final DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);

    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws Exception{

    	String providerNo = LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo();
        String searchStr = request.getParameter("demographicKeyword");
        if (searchStr == null){
           searchStr = request.getParameter("query");
        }
        if (searchStr == null){
           searchStr = request.getParameter("name");
        }
        if(searchStr == null){
        	searchStr = request.getParameter("term");
        }
        boolean outOfDomain = "true".equals(request.getParameter("outofdomain"));
        boolean activeOnly = "true".equalsIgnoreCase(request.getParameter("activeOnly"));
        boolean jqueryJSON = "true".equalsIgnoreCase(request.getParameter("jqueryJSON"));
        RxProviderData rx = new RxProviderData();
        List<Demographic> list;
        List<String>stati = OscarProperties.getPatientInactiveStatusList();
        if (searchStr.length() == 8 && searchStr.matches("([0-9]*)")) {
        	if (activeOnly) {
				list = demographicDao.searchDemographicByDOBAndNotStatus(searchStr.substring(0,4)+"-"+searchStr.substring(4,6)+"-"+searchStr.substring(6,8), stati, 100, 0,providerNo,outOfDomain);
			} else {
				list = demographicDao.searchDemographicByDOB(searchStr.substring(0,4)+"-"+searchStr.substring(4,6)+"-"+searchStr.substring(6,8), 100, 0,providerNo,outOfDomain);
			}
        } else if (searchStr.length() == 10 && searchStr.matches("([0-9]{4}-[0-9]{2}-[0-9]{2})")) {
			if (activeOnly) {
				list = demographicDao.searchDemographicByDOBAndNotStatus(searchStr, stati, 100, 0,providerNo,outOfDomain);
			} else {
				list = demographicDao.searchDemographicByDOB(searchStr, 100, 0,providerNo,outOfDomain);
			}
        } else if (searchStr.length() == 10 && searchStr.matches("([0-9]{10})")) {
			if (activeOnly) {
				list = demographicDao.searchDemographicByHINAndNotStatus(searchStr, stati, 100, 0,providerNo,outOfDomain);
			} else {
				list = demographicDao.searchDemographicByHIN(searchStr, 100, 0,providerNo,outOfDomain);
			}
        }
        else if( activeOnly ) {
        	list = demographicDao.searchDemographicByNameAndNotStatus(searchStr, stati, 100, 0, providerNo, outOfDomain);
        	if(list.size() == 100) {
        		MiscUtils.getLogger().warn("More results exists than returned");
        	}
        }
        else {
        	list = demographicDao.searchDemographicByName(searchStr, 100, 0, providerNo, outOfDomain);
        	if(list.size() == 100) {
        		MiscUtils.getLogger().warn("More results exists than returned");
        	}
        }
        List<HashMap<String, String>> secondList= new ArrayList<HashMap<String,String>>();
        for(Demographic demo :list){
            HashMap<String,String> h = new HashMap<String,String>();
             h.put("fomattedDob",demo.getFormattedDob());
             h.put("formattedName",StringEscapeUtils.escapeJava(demo.getFormattedName().replaceAll("\"", "\\\"")));
             h.put("demographicNo",String.valueOf(demo.getDemographicNo()));
             h.put("status",demo.getPatientStatus());
            Provider p = rx.getProvider(demo.getProviderNo());
            if ( demo.getProviderNo() != null ) {
                h.put("providerNo", demo.getProviderNo());
            }
            if ( p.getSurname() != null && p.getFirstName() != null ) {
                h.put("providerName", p.getSurname() + ", " + p.getFirstName());
            }
            if (OscarProperties.getInstance().isPropertyActive("workflow_enhance")) {
            	 h.put("nextAppointment", AppointmentUtil.getNextAppointment(demo.getDemographicNo() + ""));
							Map<String, String> demoExt
									= demographicExtDao.getAllValuesForDemo(demo.getDemographicNo());
							String nurse = demoExt.get(DemographicExtKey.NURSE.getKey());
							String resident = demoExt.get(DemographicExtKey.RESIDENT.getKey());
							String midwife = demoExt.get(DemographicExtKey.MIDWIFE.getKey());
					if (nurse != null) {
						h.put("nurse", nurse);
						p = rx.getProvider(nurse);
						h.put("nurseName", p.getSurname() + ", " + p.getFirstName());
					}
					if (resident != null) {
						h.put("resident", resident);
						p = rx.getProvider(resident);
						h.put("residentName", p.getSurname() + ", " + p.getFirstName());
					}
					if (midwife != null) {
						h.put("midwife", midwife);
						p = rx.getProvider(midwife);
						h.put("midwifeName", p.getSurname() + ", " + p.getFirstName());
					}
 			}
             secondList.add(h);
        }
        HashMap<String,List<HashMap<String, String>>> d = new HashMap<String,List<HashMap<String, String>>>();
        d.put("results",secondList);
        response.setContentType("text/x-json");
        if( jqueryJSON ) {
        	response.getWriter().print(formatJSON(secondList));
        	response.getWriter().flush();
        }
        else {
        	JSONObject jsonArray = (JSONObject) JSONSerializer.toJSON( d );
        	jsonArray.write(response.getWriter());        	
        }
        return null;
    }
    
    private String formatJSON(List<HashMap<String, String>>info) {
    	StringBuilder json = new StringBuilder("[");
    	HashMap<String, String> record;
    	int size = info.size();
    	for(int idx = 0; idx < size; ++idx) {
    		record = info.get(idx);
    		json.append("{\"label\":\"")
						.append(record.get("formattedName")).append(" ")
						.append(record.get("fomattedDob"))
						.append(" (").append(record.get("status")).append(")\",\"value\":\"")
						.append(record.get("demographicNo")).append("\"");
    		json.append(",\"providerNo\":\"").append(record.get("providerNo"))
						.append("\",\"provider\":\"").append(record.get("providerName"));
				json.append("\",\"nextAppt\":\"").append(record.get("nextAppointment")).append("\",");
    		if (OscarProperties.getInstance().isPropertyActive("queens_resident_tagging")) {
	    		json.append("\"nurse\":\"").append(record.get("nurse"))
							.append("\",\"nurseName\":\"").append(record.get("nurseName")).append("\",");
	    		json.append("\"resident\":\"").append(record.get("resident"))
							.append("\",\"residentName\":\"").append(record.get("residentName")).append("\",");
	    		json.append("\"midwife\":\"").append(record.get("midwife"))
							.append("\",\"midwifeName\":\"").append(record.get("midwifeName")).append("\",");
    		}
    		json.append("\"formattedName\":\"").append(record.get("formattedName")).append("\"}");
    		if( idx < size-1) {
    			json.append(",");
    		}
    	}    	
    		json.append("]");
    	MiscUtils.getLogger().info(json.toString());
    	return json.toString();
    }
}
