/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.common.dao.BillingPaymentTypeDao;
import org.oscarehr.common.model.BillingPermission;
import org.oscarehr.common.service.PdfRecordPrinter;
import org.oscarehr.managers.BillingONManager;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import oscar.oscarBilling.ca.on.data.BillingClaimHeader1Data;
import oscar.oscarBilling.ca.on.data.RAData;
import oscar.oscarBilling.ca.on.pageUtil.BillingStatusPrep;
import oscar.util.ConcatPDF;
import oscar.util.StringUtils;
import oscar.util.UtilDateUtilities;

/**
 *
 * @author mweston4
 */
public class BillingInvoiceAction extends DispatchAction {
    
	private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
	   
	
    public ActionForward getPrintPDF(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)  throws IOException {
        String invoiceNo = request.getParameter("invoiceNo");                
        String actionResult = "failure";
        
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_billing", "r", null)) {
        	throw new SecurityException("missing required security object (_billing)");
        }

        
        if (invoiceNo != null) {
            response.setContentType("application/pdf"); // octet-stream
            response.setHeader("Content-Disposition", "attachment; filename=\"BillingInvoice" + invoiceNo + "_" + UtilDateUtilities.getToday("yyyy-MM-dd.hh.mm.ss") + ".pdf\"");
            boolean bResult = processPrintPDF(Integer.parseInt(invoiceNo),request.getLocale(), response.getOutputStream());
            if (bResult) {
                actionResult = "success";
            }
        }
        return mapping.findForward(actionResult);
    }
    
    public ActionForward getListPrintPDF(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)  throws IOException {
       
    	 if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_billing", "r", null)) {
         	throw new SecurityException("missing required security object (_billing)");
         }
    	 
        String actionResult = "failure";       
        String[] invoiceNos = request.getParameterValues("invoiceAction");
        
        ArrayList<Object> fileList = new ArrayList<Object>();
        OutputStream fos = null;
        if (invoiceNos != null) {
            for (String invoiceNoStr : invoiceNos) {
                try {               
                        Integer invoiceNo = Integer.parseInt(invoiceNoStr);
                        String filename = "BillingInvoice" + invoiceNo + "_" + UtilDateUtilities.getToday("yyyy-MM-dd.hh.mm.ss") + ".pdf";
                        String savePath = oscar.OscarProperties.getInstance().getProperty("INVOICE_DIR") + "/" + filename;
                        fos = new FileOutputStream(savePath);                
                        processPrintPDF(invoiceNo, request.getLocale(), fos);
                        fileList.add(savePath);                
                } catch (Exception e) {
                    MiscUtils.getLogger().error("Error", e);
                } finally {
                    if (fos != null) fos.close();
                }
            }
        }
        if (!fileList.isEmpty()) {
            response.setContentType("application/pdf"); // octet-stream
            response.setHeader("Content-Disposition", "attachment; filename=\"BillingInvoices" + "_" + UtilDateUtilities.getToday("yyyy-MM-dd.hh.mm.ss") + ".pdf\"");
            ConcatPDF.concat(fileList, response.getOutputStream());
            actionResult = "listSuccess";
        }
        
       return mapping.findForward(actionResult);
    }

    public ActionForward exportToExcel (ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_billing", "w", null)) {
            throw new SecurityException("missing required security object (_billing)");
        }

        StringBuilder reportString = createReportString(request);

        OscarProperties oscarProperties = OscarProperties.getInstance();
        String base_doc_dir = oscarProperties.getProperty("BASE_DOCUMENT_DIR");

        File file = new File(base_doc_dir + "/invoiceReports/download/invoice_report_"+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+".xls");

        response.setContentType("text/plain");
        response.setHeader("Content-disposition", "attachment; filename=" + file.getAbsolutePath());

        try {
            FileUtils.writeStringToFile(file, reportString.toString(), Charset.defaultCharset());
            response.getOutputStream().write(reportString.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            response.getOutputStream().flush();
            response.getOutputStream().close();
        }

        return mapping.findForward("listSuccess");
    }

    public ActionForward exportToCsv (ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_billing", "w", null)) {
            throw new SecurityException("missing required security object (_billing)");
        }

        StringBuilder reportString = createReportString(request);
        
        OscarProperties oscarProperties = OscarProperties.getInstance();
        String base_doc_dir = oscarProperties.getProperty("BASE_DOCUMENT_DIR");

        File file = new File(base_doc_dir + "/invoiceReports/download/invoice_report_"+new SimpleDateFormat("yyyyMMddHHmmss").format(new Date())+".csv");
        
        response.setContentType("text/plain");
        response.setHeader("Content-disposition", "attachment; filename=" + file.getAbsolutePath());
        
        String csvReportString = reportString.toString().replaceAll("<td>","").replaceAll("</td>",",").replaceAll("<tr>","").replaceAll("</tr>", "\n").replaceAll("<table>","").replaceAll("</table>","");

        try {
            FileUtils.writeStringToFile(file, csvReportString, Charset.defaultCharset());
            response.getOutputStream().write(csvReportString.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            response.getOutputStream().flush();
            response.getOutputStream().close();
        }

        return mapping.findForward("listSuccess");
    }
    
    private StringBuilder createReportString (HttpServletRequest request) {

        boolean bSearch = true;
        String[] billType = request.getParameterValues("billType");
        String[] strBillType;
        if (billType == null || billType.length == 0) { // no boxes checked
            bSearch = false;
            strBillType = new String[] {"HCP","WCB","RMB","NOT","PAT","OCF","ODS","CPP","STD","IFH"};
        } else {
            strBillType = billType;
        }

        String accountingNumber = StringUtils.noNull(request.getParameter("accountingNumber"));
        String claimNumber = StringUtils.noNull(request.getParameter("claimNumber"));
        String statusType = request.getParameter("statusType");
        String providerNo = request.getParameter("providerview");
        String startDate  = request.getParameter("xml_vdate");
        String endDate    = request.getParameter("xml_appointment_date");
        String demoHin = StringUtils.noNull(request.getParameter("demographicHin"));
        String demoName = StringUtils.noNull(request.getParameter("demographicName"));
        String demoNo     = request.getParameter("demographicNo");
        String serviceCode     = request.getParameter("serviceCode");
        String dx = request.getParameter("dx");
        String raCode = request.getParameter("raCode") != null ?  request.getParameter("raCode") : "";
        String visitType = request.getParameter("visitType");
        String selectedSite = request.getParameter("site");

        String billingForm = request.getParameter("billing_form");

        String visitLocation = request.getParameter("xml_location");

        String sortName = request.getParameter("sortName");
        String sortOrder = request.getParameter("sortOrder");

        String paymentStartDate  = request.getParameter("paymentStartDate");
        String paymentEndDate    = request.getParameter("paymentEndDate");

        int pageNumber = -1;
        int resultsPerPage = -1;

        if ( statusType == null ) { statusType = "O"; }
        if ( "_".equals(statusType) ) { demoNo = "";}
        if ( startDate == null ) { startDate = ""; }
        if ( endDate == null ) { endDate = ""; }
        if ( providerNo == null ) { providerNo = "" ; }
        if ( dx == null ) { dx = "" ; }
        if ( visitType == null ) { visitType = "-" ; }
        if ( serviceCode == null || serviceCode.equals("")) serviceCode = "%";
        if ( billingForm == null ) { billingForm = "-" ; }
        if ( visitLocation == null) { visitLocation = "";}
        if ( sortName == null) { sortName = "ServiceDate";}
        if ( sortOrder == null) { sortOrder = "asc";}
        if ( paymentStartDate == null ) { paymentStartDate = ""; }
        if ( paymentEndDate == null ) { paymentEndDate = ""; }

        BillingStatusPrep billingStatusPrep = new BillingStatusPrep();
        billingStatusPrep.setRequest(request);

        List<BillingClaimHeader1Data> invoiceReports;

        BigDecimal billedTotal = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal paidTotal = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal paidTotalPrivate = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal paidTotalOhip = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal delTotal = (BigDecimal.ZERO).setScale(2, BigDecimal.ROUND_HALF_UP);
        
        if((serviceCode == null || billingForm == null) && dx.length()<2 && visitType.length()<2) {
            invoiceReports = bSearch ? billingStatusPrep.getBills(BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo, startDate, endDate, demoNo, visitLocation,paymentStartDate, paymentEndDate, pageNumber, resultsPerPage) : new ArrayList<BillingClaimHeader1Data>();
        } else {
            serviceCode = (serviceCode == null || serviceCode.length()<2)? "%" : serviceCode;
            if (bSearch) {
                invoiceReports = billingStatusPrep.getBillsWithSorting(BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo, startDate, endDate, demoNo, demoName, demoHin, serviceCode, accountingNumber, claimNumber, dx, raCode, visitType, billingForm, visitLocation, sortName, sortOrder, paymentStartDate, paymentEndDate, pageNumber, resultsPerPage, selectedSite);
                billedTotal = new BigDecimal(billingStatusPrep.getInvoiceSummary("sumFee", BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo, startDate, endDate, demoNo, demoName, demoHin, serviceCode, accountingNumber, claimNumber, dx, raCode, visitType, billingForm, visitLocation, sortName, sortOrder, paymentStartDate, paymentEndDate, selectedSite)).setScale(2, BigDecimal.ROUND_HALF_UP);
                paidTotalPrivate =
                    new BigDecimal(billingStatusPrep.getInvoiceSummary("sumPrivatePay",
                        BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo,
                        startDate, endDate, demoNo, demoName, demoHin, serviceCode,
                        accountingNumber, claimNumber, dx, raCode, visitType, billingForm,
                        visitLocation, sortName, sortOrder, paymentStartDate, paymentEndDate,
                        selectedSite)).setScale(2, BigDecimal.ROUND_HALF_UP);
                paidTotalOhip = new BigDecimal(billingStatusPrep.getInvoiceSummary("sumAmountPay",
                    BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo,
                    startDate, endDate, demoNo, demoName, demoHin, serviceCode, accountingNumber,
                    claimNumber, dx, raCode, visitType, billingForm, visitLocation, sortName,
                    sortOrder, paymentStartDate, paymentEndDate, selectedSite)).setScale(2,
                        BigDecimal.ROUND_HALF_UP);
                paidTotal = paidTotalPrivate.add(paidTotalOhip);
                delTotal = new BigDecimal(billingStatusPrep.getInvoiceSummary("sumDeleted", BillingPermission.INVOICE_REPORT, strBillType, statusType, providerNo, startDate, endDate, demoNo, demoName, demoHin, serviceCode, accountingNumber, claimNumber, dx, raCode, visitType, billingForm, visitLocation, sortName, sortOrder, paymentStartDate, paymentEndDate, selectedSite)).setScale(2, BigDecimal.ROUND_HALF_UP);
            } else {
                invoiceReports = new ArrayList<BillingClaimHeader1Data>();
            }
        }
        
        boolean bMultisites = org.oscarehr.common.IsPropertiesOn.isMultisitesEnable();

        RAData raData = new RAData();

        ArrayList<Hashtable<String, String>> raMasterList = raData.getRADataInternListFromCh1ObjList(invoiceReports);

        StringBuilder reportString = new StringBuilder();

        reportString.append("<table>");

        reportString.append("<tr>");

        reportString.append("<td>Service Date</td>");
        reportString.append("<td>Patient</td>");
        reportString.append("<td>Patient Name</td>");
        reportString.append("<td>Location</td>");
        reportString.append("<td>Billing Date</td>");
        reportString.append("<td>Stat</td>");
        reportString.append("<td>Settled/Paid</td>");
        reportString.append("<td>Code</td>");
        reportString.append("<td>Billed</td>");
        reportString.append("<td>Paid</td>");
        reportString.append("<td>Adj</td>");
        reportString.append("<td>Del</td>");
        reportString.append("<td>Dx</td>");
        reportString.append("<td>Type</td>");
        reportString.append("<td>Invoice #</td>");
        reportString.append("<td>Messages</td>");
        reportString.append("<td>Method</td>");
        reportString.append("<td>Quantity</td>");
        reportString.append("<td>Provider</td>");
        if (bMultisites) {
            reportString.append("<td>Site</td>");
        }

        reportString.append("</tr>");

        for (BillingClaimHeader1Data report : invoiceReports) {

            ArrayList<Hashtable<String, String>> raList = new ArrayList<Hashtable<String, String>>();

            for (Hashtable<String, String> raItem : raMasterList) {
                if (raItem.get("billing_no").equals(report.getId())) {
                    raList.add(raItem);
                }
            }

            String errorCode = "";
            if(raList.size() > 0) {
                errorCode = raData.getErrorCodes(raList);
            }
            reportString.append("<tr>");

            String adj = Double.toString((Double.parseDouble(report.getTotal()) - Double.parseDouble(report.getPaid())));

            String settleDate = report.getSettle_date();
            if( settleDate == null || !report.getStatus().equals("S")) {
                settleDate = "N/A";
            }
            else if (settleDate.indexOf(" ")>0){
                settleDate = settleDate.substring(0, settleDate.indexOf(" "));
            }

            BillingPaymentTypeDao paymentTypeDao = SpringUtils.getBean(BillingPaymentTypeDao.class);
            StringBuilder paymentMethod = new StringBuilder(report.getPay_program());
            if (report.getPay_program().matches("PAT|OCF|ODS|CPP|STD|IFH")) {
                paymentMethod = new StringBuilder();
                for (Map.Entry<Integer, BigDecimal> payment : report.getPaymentTotals().entrySet()){
                    if (payment.getValue()!=null){
                        paymentMethod.append(paymentTypeDao.find(payment.getKey()).getPaymentType());
                    } else if (report.getPaid() == null || report.getPaid().equals("0.00")){
                        paymentMethod = new StringBuilder("------");
                    }
                }
            }

            reportString.append("<td>").append(report.getService_date()).append("</td>");
            reportString.append("<td>").append(report.getDemographic_no()).append("</td>");
            reportString.append("<td>\"").append(report.getDemographic_name()).append("\"</td>");
            reportString.append("<td>").append(report.getFacilty_num()!=null?report.getFacilty_num():"").append("</td>");
            reportString.append("<td>").append(report.getBilling_date()).append("</td>");
            reportString.append("<td>").append(report.getStatus()).append("</td>");
            reportString.append("<td>").append(settleDate).append("</td>");
            reportString.append("<td>").append(report.getTransc_id()).append("</td>");
            reportString.append("<td>").append(report.getTotal()).append("</td>");
            reportString.append("<td>").append(report.getPaid()).append("</td>");
            reportString.append("<td>").append(adj).append("</td>");
            reportString.append("<td>").append(report.getTotalDeleted()).append("</td>");
            reportString.append("<td>").append(report.getRec_id()).append("</td>");
            reportString.append("<td>").append(report.getPay_program()).append("</td>");
            reportString.append("<td>").append(report.getId()).append("</td>");
            reportString.append("<td>").append(errorCode).append("</td>");
            reportString.append("<td>").append(paymentMethod).append("</td>");
            reportString.append("<td>").append(report.getNumItems()).append("</td>");
            reportString.append("<td>\"").append(report.getProviderName()).append("\"</td>");
            if (bMultisites) {
                reportString.append("<td>\"").append(StringUtils.noNull(report.getClinic())).append("\"</td>");
            }

            reportString.append("</tr>");
        }


        BigDecimal adjTotal = billedTotal.subtract(paidTotal);

        reportString.append("<tr>");

        reportString.append("<td>Count:</td>");
        reportString.append("<td>"+invoiceReports.size()+"</td>");
        reportString.append("<td></td>");
        reportString.append("<td></td>");
        reportString.append("<td></td>");
        reportString.append("<td></td>");
        reportString.append("<td></td>");
        reportString.append("<td>Total:</td>");
        reportString.append("<td>"+billedTotal+"</td>");
        reportString.append("<td>"+paidTotal+"</td>");
        reportString.append("<td>"+adjTotal+"</td>");
        reportString.append("<td>"+delTotal+"</td>");
        reportString.append("<td></td>");
        reportString.append("<td></td>");
        reportString.append("<td></td>");
        reportString.append("<td></td>");
        reportString.append("<td></td>");
        reportString.append("<td></td>");
        reportString.append("<td></td>");

        reportString.append("</tr>");
        reportString.append("</table>");
        
        return reportString;
    }
    
    public ActionForward sendEmail(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)  {
        
    	 if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_billing", "w", null)) {
         	throw new SecurityException("missing required security object (_billing)");
         }
    	 
        String invoiceNoStr = request.getParameter("invoiceNo");
        Integer invoiceNo = Integer.parseInt(invoiceNoStr);
        Locale locale = request.getLocale();
        String actionResult = "failure";
        
        if (invoiceNo != null) {
            BillingONManager billingManager = (BillingONManager) SpringUtils.getBean("billingONManager");
            billingManager.sendInvoiceEmailNotification(invoiceNo, locale);
            billingManager.addEmailedBillingComment(invoiceNo, locale); 
            actionResult = "success";
        }

        ActionRedirect redirect = new ActionRedirect(mapping.findForward(actionResult));
        redirect.addParameter("billing_no", invoiceNo);
        return redirect;
    }
    
    public ActionForward sendListEmail(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)  {
        
    	 if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_billing", "w", null)) {
         	throw new SecurityException("missing required security object (_billing)");
         }
    	 
        String actionResult = "failure";       
        String[] invoiceNos = request.getParameterValues("invoiceAction");
        Locale locale = request.getLocale();
        
        if (invoiceNos != null) {
            for (String invoiceNoStr : invoiceNos) {
                Integer invoiceNo = Integer.parseInt(invoiceNoStr);
                BillingONManager billingManager = (BillingONManager) SpringUtils.getBean("billingONManager");
                billingManager.sendInvoiceEmailNotification(invoiceNo, locale);
                billingManager.addEmailedBillingComment(invoiceNo, locale);               
            }
            actionResult = "listSuccess";
        }

        return mapping.findForward(actionResult);
    }
    
    private boolean processPrintPDF(Integer invoiceNo, Locale locale, OutputStream os) {
        
        boolean bResult = false;
        
        if (invoiceNo != null){                                    
            //Create PDF of the invoice
            PdfRecordPrinter printer = new PdfRecordPrinter(os);                           
            printer.printBillingInvoice(invoiceNo, locale);

            BillingONManager billingManager = (BillingONManager) SpringUtils.getBean("billingONManager");
            billingManager.addPrintedBillingComment(invoiceNo, locale);
            bResult = true;          
        }
        
        return bResult;        
    }                

}
