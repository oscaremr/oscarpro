/*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.PMmodule.model.ProgramProvider;
import org.oscarehr.casemgmt.model.CaseManagementNote;
import org.oscarehr.casemgmt.model.CaseManagementNoteLink;
import org.oscarehr.casemgmt.service.CaseManagementManager;
import org.oscarehr.common.dao.DocumentDao;
import org.oscarehr.common.dao.FreeDrawDataDao;
import org.oscarehr.common.dao.SecRoleDao;
import org.oscarehr.common.model.Document;
import org.oscarehr.common.model.FreeDrawData;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.SecRole;
import org.oscarehr.managers.ProgramManager2;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import net.sf.json.JSONObject;
import oscar.MyDateFormat;
import oscar.dms.EDoc;
import oscar.dms.EDocUtil;
import oscar.oscarEncounter.data.EctProgram;

public class FreeDrawingAction extends DispatchAction {

  private FreeDrawDataDao freeDrawDataDao;
  private DocumentDao docDao = SpringUtils.getBean(DocumentDao.class);

  public void setFreeDrawDataDao(FreeDrawDataDao freeDrawDataDao) {
    this.freeDrawDataDao = freeDrawDataDao;
  }

  public ActionForward unspecified(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    return mapping.findForward("new");
  }

  public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {
    String name = request.getParameter("name");
    String imgNum = request.getParameter("imgnum");
    String drawData = request.getParameter("drawingData");
    String apptNo = request.getParameter("apptNo");
    String demographicNo = request.getParameter("demographicNo");
    String providerNo = request.getParameter("providerNo");
    String handWriteNotes = request.getParameter("SubmitData");
    String type = request.getParameter("type");
    if (Integer.valueOf(imgNum) != -1) {
      drawData = drawData + "oscarsmart" + imgNum;
    }
    if (type == null) {
      type = "sketchtoy";
    }
    String forward = request.getParameter("forward");
    String thumbnailId = request.getParameter("thumbnailId");
    if (thumbnailId == null) {
      thumbnailId = "";
    }
    // set free draw name by document image name
    if (Integer.valueOf(imgNum) != -1) {
      Document docs = docDao.getDocument(imgNum);
      if (null != docs.getDocdesc() && docs.getDocdesc().length() > 0) {
        name = docs.getDocdesc() + "-" + docs.getDocfilename();
      } else {
        name = docs.getDocfilename();
      }
    }

    FreeDrawData data = new FreeDrawData();
    data.setDrawData(drawData);
    data.setName(name);
    data.setUpdateTime(new Date());
    data.setProviderNo(providerNo);
    data.setHandWriteNotes(handWriteNotes);
    data.setDataType(type);
    data.setThumbnailId(thumbnailId);

    try {
      data.setAppointmentNo(Integer.parseInt(apptNo));
    } catch (Exception e) {
      data.setAppointmentNo(0);
    }

    try {
      data.setDemographicNo(Integer.parseInt(demographicNo));
    } catch (Exception e) {
      data.setDemographicNo(0);
    }

    freeDrawDataDao.persist(data);

    if ("json".equals(forward)) {
      try {
        JSONObject ret = new JSONObject();
        ret.put("retCode", "OK");
        PrintWriter out = response.getWriter();
        out.write(ret.toString());
        out.flush();
        out.close();
      } catch (Exception e) {
        MiscUtils.getLogger().info(e.toString());
      }
      return null;
    }
    return mapping.findForward("close");
  }

  public ActionForward displayFreebgImg(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    String docNo = request.getParameter("docNo");
    DocumentDao docDao = (DocumentDao) SpringUtils.getBean("documentDao");
    Document doc = docDao.getDocument(docNo);
    if (doc == null) {
      MiscUtils.getLogger().info("Can't find the document with docNo: " + docNo);
      return null;
    }
    String absFileName = EDocUtil.getDocumentPath(doc.getDocfilename());
    try {
      FileInputStream in = new FileInputStream(new File(absFileName));
      byte[] buf = new byte[1024];
      OutputStream out = response.getOutputStream();
      int count = 0;
      while ((count = in.read(buf)) != -1) {
        out.write(buf, 0, count);
      }
      out.close();
      in.close();
    } catch (Exception e) {
      MiscUtils.getLogger().error(e.toString());
    }

    return null;
  }

  public ActionForward getDrawingData(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    JSONObject ret = new JSONObject();

    do {
      String freeDrawId = request.getParameter("freeDrawId");
      if (freeDrawId == null) {
        ret.put("retCode", "ERR_PARAM");
        break;
      }
      FreeDrawData drawData = freeDrawDataDao.find(Integer.valueOf(freeDrawId));
      if (drawData == null) {
        ret.put("retCode", "NOT_FOUND");
        break;
      }
      ret.put("retCode", "OK");
      if (drawData.getDrawData().indexOf("oscarsmart") > -1) {
        String[] strings = drawData.getDrawData().split("oscarsmart");
        ret.put("data", strings[0]);
        ret.put("imgnum", strings[1]);
      } else {
        ret.put("data", drawData.getDrawData());
        ret.put("imgnum", "");
      }
    } while (false);

    try {
      PrintWriter out = response.getWriter();
      out.write(ret.toString());
      out.flush();
      out.close();
    } catch (Exception e) {
      MiscUtils.getLogger().error(e.toString());
    }

    return null;
  }

  public ActionForward saveAndDocument(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response) throws Exception {
    // save free drawing image
    String name = request.getParameter("name");
    String imgNum = request.getParameter("imgnum");
    String drawData = request.getParameter("drawingData");
    String apptNo = request.getParameter("apptNo");
    String demographicNo = request.getParameter("demographicNo");
    String providerNo = request.getParameter("providerNo");
    String handWriteNotes = request.getParameter("SubmitData");
    String type = request.getParameter("type");
    if (Integer.valueOf(imgNum) != -1) {
      drawData = drawData + "oscarsmart" + imgNum;
    }
    if (type == null) {
      type = "sketchtoy";
    }
    String forward = request.getParameter("forward");
    String thumbnailId = request.getParameter("thumbnailId");
    if (thumbnailId == null) {
      thumbnailId = "";
    }
    // set free draw name by document image name
    String freeDrawingDescription = "";
    if (Integer.valueOf(imgNum) != -1) {
      Document docs = docDao.getDocument(imgNum);
      if (null != docs.getDocdesc() && docs.getDocdesc().length() > 0) {
        name = docs.getDocdesc() + "-" + docs.getDocfilename();
        freeDrawingDescription = docs.getDocdesc();
      } else {
        name = docs.getDocfilename();
      }
    }
    FreeDrawData data = new FreeDrawData();
    data.setDrawData(drawData);
    data.setName(name);
    data.setUpdateTime(new Date());
    data.setProviderNo(providerNo);
    data.setHandWriteNotes(handWriteNotes);
    data.setDataType(type);
    data.setThumbnailId(thumbnailId);
    try {
      data.setAppointmentNo(Integer.parseInt(apptNo));
    } catch (Exception e) {
      data.setAppointmentNo(0);
    }
    try {
      data.setDemographicNo(Integer.parseInt(demographicNo));
    } catch (Exception e) {
      data.setDemographicNo(0);
    }
    freeDrawDataDao.persist(data);

    // save image to pdf then send to document
    String rootPath = oscar.OscarProperties.getInstance().getProperty("drawing_tool.folder");
    String imagePath =
        rootPath + File.separator + data.getDemographicNo() + "_" + thumbnailId + ".png";
    File imageFile = new File(imagePath);
    String pdfFileName = data.getName();
    if (null == pdfFileName) {
      pdfFileName = System.currentTimeMillis() + "_" + demographicNo + "_FreeDrawing.pdf";
    } else {
      pdfFileName = pdfFileName.substring(0, pdfFileName.lastIndexOf(".")) + "_"
          + System.currentTimeMillis() + "_" + demographicNo + "_FreeDrawing.pdf";
    }
    String savePdfPath = EDocUtil.getDocumentPath(pdfFileName);
    if (imageFile.exists()) {
      com.itextpdf.text.Document document = new com.itextpdf.text.Document();
      FileOutputStream fos = null;
      try {
        fos = new FileOutputStream(savePdfPath);
        PdfWriter.getInstance(document, fos);
        document.setPageSize(PageSize.A4);
        document.open();
        Image image = Image.getInstance(imageFile.getPath());
        float imageHeight = image.getScaledHeight();
        float imageWidth = image.getScaledWidth();
        int i = 0;
        while (imageHeight > 500 || imageWidth > 500) {
          image.scalePercent(100 - i);
          i++;
          imageHeight = image.getScaledHeight();
          imageWidth = image.getScaledWidth();
        }
        image.setAlignment(Image.ALIGN_CENTER);
        document.add(image);
      } catch (DocumentException | IOException de) {
        MiscUtils.getLogger().error(de);
      }

      document.close();
      fos.flush();
      fos.close();

      // save document
      Date nowDate = new Date();
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
      EDoc newDoc = new EDoc("Free Drawing PDF: ".concat(freeDrawingDescription), "photo",
          pdfFileName, "", data.getProviderNo(), "", "", 'A', sdf.format(nowDate), "", "",
          "demographic", String.valueOf(data.getDemographicNo()));
      newDoc.setFileName(pdfFileName);
      newDoc.setDocPublic("0");
      if (null != apptNo && apptNo.length() > 0) {
        newDoc.setAppointmentNo(Integer.parseInt(apptNo));
      }
      newDoc.setDocClass("");
      newDoc.setDocSubClass("");
      newDoc.setContentType("application/pdf");

      // if the document was added in the context of a program
      ProgramManager2 programManager = SpringUtils.getBean(ProgramManager2.class);
      LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
      ProgramProvider pp = programManager.getCurrentProgramInDomain(loggedInInfo,
          loggedInInfo.getLoggedInProviderNo());
      if (pp != null && pp.getProgramId() != null) {
        newDoc.setProgramId(pp.getProgramId().intValue());
      }
      String restrictToProgramStr = request.getParameter("restrictToProgram");
      newDoc.setRestrictToProgram("on".equals(restrictToProgramStr));
      EDocUtil.addDocumentSQL(newDoc);

      // add note
      Date now = EDocUtil.getDmsDateTimeAsDate();
      String docDesc = EDocUtil.getLastDocumentDesc();
      CaseManagementNote cmn = new CaseManagementNote();
      cmn.setUpdate_date(now);
      java.sql.Date od1 = MyDateFormat.getSysDate(newDoc.getObservationDate());
      cmn.setObservation_date(od1);
      cmn.setDemographic_no(String.valueOf(data.getDemographicNo()));
      HttpSession se = request.getSession();
      String user_no = (String) se.getAttribute("user");
      String prog_no = new EctProgram(se).getProgram(user_no);
      WebApplicationContext ctx =
          WebApplicationContextUtils.getRequiredWebApplicationContext(se.getServletContext());
      CaseManagementManager cmm = (CaseManagementManager) ctx.getBean("caseManagementManager");
      cmn.setProviderNo("-1");

      Provider provider = EDocUtil.getProvider(data.getProviderNo());
      String provFirstName = "";
      String provLastName = "";
      if (provider != null) {
        provFirstName = provider.getFirstName();
        provLastName = provider.getLastName();
      }

      String strNote = "Document" + " " + docDesc + " " + "created at " + now + " by "
          + provFirstName + " " + provLastName + ".";
      cmn.setNote(strNote);
      cmn.setSigned(true);
      cmn.setSigning_provider_no("-1");
      cmn.setProgram_no(prog_no);

      SecRoleDao secRoleDao = (SecRoleDao) SpringUtils.getBean("secRoleDao");
      SecRole doctorRole = secRoleDao.findByName("doctor");
      cmn.setReporter_caisi_role(doctorRole.getId().toString());
      cmn.setReporter_program_team("0");
      cmn.setPassword("NULL");
      cmn.setLocked(false);
      cmn.setHistory(strNote);
      cmn.setPosition(0);
      Long note_id = cmm.saveNoteSimpleReturnID(cmn);

      // Add a noteLink to casemgmt_note_link
      CaseManagementNoteLink cmnl = new CaseManagementNoteLink();
      cmnl.setTableName(CaseManagementNoteLink.DOCUMENT);
      cmnl.setTableId(Long.parseLong(EDocUtil.getLastDocumentNo()));
      cmnl.setNoteId(note_id);
      EDocUtil.addCaseMgmtNoteLink(cmnl);
    }

    if ("json".equals(forward)) {
      try {
        JSONObject ret = new JSONObject();
        ret.put("retCode", "OK");
        PrintWriter out = response.getWriter();
        out.write(ret.toString());
        out.flush();
        out.close();
      } catch (Exception e) {
        MiscUtils.getLogger().error(e.toString());
      }
      return null;
    }
    return mapping.findForward("close");
  }
}
