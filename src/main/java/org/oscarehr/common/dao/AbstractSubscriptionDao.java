/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import java.util.List;
import lombok.SneakyThrows;
import org.oscarehr.common.dao.exception.OperationNotSupportedException;
import org.oscarehr.common.model.AbstractModel;
import org.oscarehr.common.model.AbstractSubscriptionModel;
import org.oscarehr.managers.FhirSubscriptionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

@Transactional
public abstract class AbstractSubscriptionDao<T extends AbstractModel<?>> extends AbstractDao<T> {

  @Autowired FhirSubscriptionManager fhirSubscriptionManager;
  @Autowired SystemPreferencesDao systemPreferencesDao;

  /**
   * Abstract class for dynamically keeping track of updates to modules in OSCAR
   * currently supported modules: Demographics, Appointments
   *
   * Note: Currently batch operations are not supported, as Demographics and Appointments don't make use of them
   */
  protected AbstractSubscriptionDao(Class<T> modelClass) {
    super(modelClass);
  }

  /* BATCH OPERATIONS NOT SUPPORTED */

  @Override
  @SneakyThrows
  public void batchMerge(List<AbstractModel<?>> oList) {
    throw new OperationNotSupportedException(
        "Batch calls to merge are not supported.");
  }

  @Override
  @SneakyThrows
  public void batchMerge(List<AbstractModel<?>> oList, int batchSize) {
    throw new OperationNotSupportedException(
        "Batch calls to merge are not supported.");
  }

  @Override
  @SneakyThrows
  public void batchPersist(List<AbstractModel<?>> oList) {
    throw new OperationNotSupportedException(
        "Batch calls to persist are not supported.");
  }

  @Override
  @SneakyThrows
  public void batchPersist(List<AbstractModel<?>> oList, int batchSize) {
    throw new OperationNotSupportedException(
        "Batch calls to persist are not supported.");
  }

  @Override
  @SneakyThrows
  public void batchRemove(List<AbstractModel<?>> oList) {
    throw new OperationNotSupportedException(
        "Batch calls to remove are not supported.");
  }

  @Override
  @SneakyThrows
  public void batchRemove(List<AbstractModel<?>> oList, int batchSize) {
    throw new OperationNotSupportedException(
        "Batch calls to remove are not supported.");
  }

  /* SUPPORTED MERGE, PERSIST, AND REMOVE */
  @Override
  public void merge(AbstractModel<?> o) {
    TransactionSynchronizationManager.registerSynchronization(getTransactionSynchronization());
    super.merge(o);

    // check if there is a Fhir Subscription
    if (systemPreferencesDao.isFhirSubscriptionEnabled()) {
      fhirSubscriptionManager.createCheckSubRequest(((AbstractSubscriptionModel<?>) o).getModule(),
          ((AbstractSubscriptionModel<?>) o).getModuleId());
    }
  }

  @Override
  public void persist(AbstractModel<?> o) {
    TransactionSynchronizationManager.registerSynchronization(getTransactionSynchronization());
    super.persist(o);

    // check if there is a Fhir Subscription - request to check subscription will be persisted in
    // the same transaction with the persist method triggering this. Needs to be in the same tx
    if (systemPreferencesDao.isFhirSubscriptionEnabled()) {
      fhirSubscriptionManager.createCheckSubRequest(((AbstractSubscriptionModel<?>) o).getModule(),
          ((AbstractSubscriptionModel<?>) o).getModuleId());
    }
  }

  @Override
  public void remove(AbstractModel<?> o) {
    TransactionSynchronizationManager.registerSynchronization(getTransactionSynchronization());
    super.remove(o);

    // check if there is a Fhir Subscription
    if (systemPreferencesDao.isFhirSubscriptionEnabled()) {
      fhirSubscriptionManager.createCheckSubRequest(((AbstractSubscriptionModel<?>) o).getModule(),
          ((AbstractSubscriptionModel<?>) o).getModuleId());
    }
  }

  /* PRIVATE HELPER METHODS */

  private TransactionSynchronization getTransactionSynchronization() {
    return (new TransactionSynchronizationAdapter() {
      public void afterCommit() {
        // ping PRO to process the check subscription request
        if (systemPreferencesDao.isFhirSubscriptionEnabled()) {
          fhirSubscriptionManager.pingProToCheckSub();
        }
      }
    });
  }
}
