package org.oscarehr.common.dao;

import java.util.List;

import javax.persistence.Query;

import org.oscarehr.common.model.Devices;
import org.springframework.stereotype.Repository;

@Repository
public class DevicesDao extends AbstractDao<Devices>{

	public DevicesDao(){
		super(Devices.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<String> findAllDevicesName(){
		String sql = "select distinct deviceName from Devices";
		Query query = entityManager.createQuery(sql);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Devices findLastTimeDeviceDataByName(String deviceName){
		String sql = "select d from Devices d where d.deviceName = ? and d.demoId is null and d.status = '0' order by d.time desc";
		Query query = entityManager.createQuery(sql);
		query.setParameter(1, deviceName);
		
		List<Devices> dataList = query.getResultList();
		if(dataList.size() > 0){
			return dataList.get(0);
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public Devices findLastTimeDeviceDataByDemoIdAndName(String deviceName, String demoId){
		String sql = "select d from Devices d where d.deviceName = ? and d.demoId = ? and d.status = '1' order by d.time desc";
		Query query = entityManager.createQuery(sql);
		query.setParameter(1, deviceName);
		query.setParameter(2, Integer.parseInt(demoId));
		
		List<Devices> dataList = query.getResultList();
		if(dataList.size() > 0){
			return dataList.get(0);
		}
		
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public Devices findLastTimeDeviceDataByNameAndDemoIdNotNull(String deviceName){
		String sql = "select d from Devices d where d.deviceName = ? and d.demoId is not null and d.status = '1' order by d.time desc";
		Query query = entityManager.createQuery(sql);
		query.setParameter(1, deviceName);
		
		List<Devices> dataList = query.getResultList();
		if(dataList.size() > 0){
			return dataList.get(0);
		}
		
		return null;
	}
}
