/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import javax.persistence.Query;
import org.oscarehr.common.model.DataSharingSettings;
import org.springframework.stereotype.Repository;

@Repository
public class DataSharingSettingsDao extends AbstractDao<DataSharingSettings> {

  public DataSharingSettingsDao() {
    super(DataSharingSettings.class);
  }

  public DataSharingSettings getOrganizationSettings() {
    Query query = entityManager.createQuery(
            "select x from " + modelClass.getSimpleName() + " x where isOrgSettings=true AND demographicNo=null");

    return this.getSingleResultOrNull(query);
  }

  /**
   * @return results ordered by lastUpdateDate
   */
  public DataSharingSettings findByDemographicNo(int demographicNo) {
    Query query = entityManager.createQuery(
            "select x from " + modelClass.getSimpleName() + " x where x.demographicNo=?1 AND isOrgSettings=false");
    query.setParameter(1, demographicNo);
    return this.getSingleResultOrNull(query);
  }

}
