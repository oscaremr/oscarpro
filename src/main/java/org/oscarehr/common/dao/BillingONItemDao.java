/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.oscarehr.common.model.BillingONCHeader1;
import org.oscarehr.common.model.BillingONItem;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.SpringUtils;
import org.springframework.stereotype.Repository;
import oscar.OscarProperties;

@Repository
@SuppressWarnings("unchecked")
public class BillingONItemDao extends AbstractDao<BillingONItem>{

	
	public BillingONItemDao() {
		super(BillingONItem.class);
	}

    public List<BillingONItem> getBillingItemByCh1Id(Integer ch1_id) {
        String queryStr = "FROM BillingONItem b WHERE b.ch1Id = ?1";
        Query q = entityManager.createQuery(queryStr);
        q.setParameter(1, ch1_id);
        
        
        List<BillingONItem> rs = q.getResultList();

        return rs;
    }
        
    public List<BillingONItem> getActiveBillingItemByCh1Id(Integer ch1_id) {
        String queryStr = "FROM BillingONItem b WHERE b.ch1Id = ?1 AND b.status <> ?2";
        Query q = entityManager.createQuery(queryStr);
        q.setParameter(1, ch1_id);
        q.setParameter(2, "D");
        
        
        List<BillingONItem> rs = q.getResultList();

        return rs;
    }

    public List<BillingONCHeader1> getCh1ByDemographicNo(Integer demographic_no) {
        String queryStr = "FROM BillingONCHeader1 b WHERE b.demographicNo = ?1 ORDER BY b.id DESC";
        Query q = entityManager.createQuery(queryStr);
        q.setParameter(1, demographic_no);
        
        List<BillingONCHeader1> rs = q.getResultList();

        return rs;
    }

	public List<BillingONItem> findByCh1Id(Integer id) {
	    Query query = createQuery("bi", "bi.ch1Id = :ch1Id AND bi.status <> 'D' AND bi.status <> 'S'");
		query.setParameter("ch1Id", id);
		return query.getResultList();
    }

	public List<BillingONItem> findByCh1IdAndStatusNotEqual(Integer chId, String string) {
		Query query = createQuery("i", "i.ch1Id= :chId AND i.status != 'D'");
		query.setParameter("chId", chId);
		return query.getResultList();
    }
	
    public List<BillingONCHeader1> getCh1ByDemographicNoSince(Integer demographic_no, Date lastUpdateDate) {
        String queryStr = "FROM BillingONCHeader1 b WHERE b.demographicNo = "+demographic_no+" and b.timestamp > ? ORDER BY b.id";
        Query q = entityManager.createQuery(queryStr);
        q.setParameter(1, lastUpdateDate);

        List<BillingONCHeader1> rs = q.getResultList();

        return rs;
    }
    
    public List<Integer> getDemographicNoSince(Date lastUpdateDate) {
        String queryStr = "select b.demographicNo FROM BillingONCHeader1 b WHERE b.timestamp > ? ORDER BY b.id";
        Query q = entityManager.createQuery(queryStr);
        q.setParameter(1, lastUpdateDate);
        
        List<Integer> rs = q.getResultList();

        return rs;
    }
	
    public List<String> getPreventionExclusionsByDemographicNo(Integer demographicNo) {
	    String sql = "SELECT i.* FROM billing_on_item i LEFT JOIN billing_on_cheader1 c ON i.ch1_id = c.id "
                + "WHERE c.demographic_no = :demographicNo AND i.service_code IN ('Q140A', 'Q141A', 'Q142A')";
	    Query query = entityManager.createNativeQuery(sql, BillingONItem.class);
	    query.setParameter("demographicNo", demographicNo);
	    List<BillingONItem> billedItems = query.getResultList();
	    
	    List<String> exclusions = new ArrayList<>();
	    
	    for(BillingONItem billedItem : billedItems) {
	        switch (billedItem.getServiceCode()) {
                case "Q140A":
                    exclusions.add("PAP");
                    break;
                case "Q141A":
                    exclusions.add("MAM");
                    break;
                case "Q142A":
                    exclusions.add("FOBT");
                    exclusions.add("COLONOSCOPY");
                    break;
            }
        }
        
        return exclusions;
    }

    public void updateByCh1Id(Integer id, Date billingDate) {
        Query query = createQuery("bi", "bi.ch1Id = :ch1Id AND bi.status <> 'D' AND bi.status <> 'S' AND bi.serviceDate <> :billingDate");
        query.setParameter("ch1Id", id);
        query.setParameter("billingDate", billingDate);
        List<BillingONItem> items = query.getResultList();
        for (BillingONItem item : items) {
            item.setServiceDate(billingDate);
            saveEntity(item);
        }
    }

    public boolean checkA233AEligible(Integer demoNo, String dxCode) {
      SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
      boolean isBillingA233ReplaceA234AndG435A =
          systemPreferencesDao.isReadBooleanPreferenceWithDefault(
              SystemPreferences.BILLING_A233_REPLACE_A234_AND_G435A, false);
      if (!isBillingA233ReplaceA234AndG435A) {
        return false;
      }

      // 1) A233A code can only be billed 1 time a year (assuming same diagnostic code)
      Query query = entityManager.createQuery("select b from BillingONItem b, BillingONCHeader1 boh "
          + "where b.ch1Id = boh.id and boh.status != 'D' " + "and b.serviceCode = 'A233A' "
          + "and b.dx = ?1 and b.status != 'D' "
          + "and b.serviceDate > ?2 and boh.demographicNo = ?3 ");
      Calendar calendar = Calendar.getInstance();
      calendar.add(Calendar.YEAR, -1);
      query.setParameter(1, dxCode);
      query.setParameter(2, calendar.getTime());
      query.setParameter(3, demoNo);
      List<BillingONItem> bList = query.getResultList();
      
      if (bList != null && !bList.isEmpty()) {
        return false;
      }

      // 2) A233A can be billed 2 times a year if different diagnostic codes
      query = entityManager.createQuery("select b from BillingONItem b, BillingONCHeader1 boh "
          + "where b.ch1Id = boh.id and boh.status != 'D' "
          + "and b.serviceCode = 'A233A' and b.dx != ?1 and b.status != 'D' "
          + "and b.serviceDate > ?2 and boh.demographicNo = ?3 ");
      query.setParameter(1, dxCode);
      query.setParameter(2, calendar.getTime());
      query.setParameter(3, demoNo);
      bList = query.getResultList();
      
      if (bList != null && bList.size() > 1) {
        return false;
      }

      // 3) A233A should not be used within 3 month among with A235A or A253A codes
      calendar = Calendar.getInstance();
      calendar.add(Calendar.MONTH, -3);
      query =
          entityManager.createQuery(
              "select b from BillingONItem b, BillingONCHeader1 boh "
                  + "where b.ch1Id = boh.id and boh.status != 'D' "
                  + "and (b.serviceCode='A235A' or b.serviceCode='A253A') "
                  + "and b.status!='D' and b.serviceDate > ?1 "
                  + "and boh.demographicNo = ?2 ");
      query.setParameter(1, calendar.getTime());
      query.setParameter(2, demoNo);
      bList = query.getResultList();

      if (bList != null && !bList.isEmpty()) {
        return false;
      }

      // 4) A233A should not be used within 2 weeks of E140A code
      calendar = Calendar.getInstance();
      calendar.add(Calendar.DATE, -14);
      query =
          entityManager.createQuery(
              "select b from BillingONItem b, BillingONCHeader1 boh "
                  + "where b.ch1Id = boh.id and boh.status != 'D' "
                  + "and b.serviceCode='E140A' and b.status!='D' "
                  + "and b.serviceDate > ?1 and boh.demographicNo = ?2 ");
      query.setParameter(1, calendar.getTime());
      query.setParameter(2, demoNo);
      bList = query.getResultList();

      if (bList != null && !bList.isEmpty()) {
        return false;
      }

      return true;
    }
}
