/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */

package org.oscarehr.common.dao;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import lombok.val;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.model.AbstractModel;
import org.oscarehr.common.model.OscarLog;
import org.springframework.stereotype.Repository;

@Repository
public class OscarLogDao extends AbstractDao<OscarLog> {

  // demographics accessed by a provider beyond this many days will not be included in results
  private static final int RECENT_DEMOGRAPHIC_CUTOFF_DAYS = 5;

  public OscarLogDao() {
    super(OscarLog.class);
  }

  public List<OscarLog> findByDemographicId(Integer demographicId) {

    String sqlCommand =
        "select x from " + modelClass.getSimpleName() + " x where x.demographicId=?1";

    Query query = entityManager.createQuery(sqlCommand);
    query.setParameter(1, demographicId);

    @SuppressWarnings("unchecked")
    List<OscarLog> results = query.getResultList();

    return (results);
  }


  public boolean hasRead(String providerNo, String content, String contentId) {
    String sqlCommand = "select x from " + modelClass.getSimpleName()
        + " x where x.action = 'read' and  x.providerNo=?1 and x.content = ?2 and x.contentId = ?3";
    Query query = entityManager.createQuery(sqlCommand);
    query.setParameter(1, providerNo);
    query.setParameter(2, content);
    query.setParameter(3, contentId);

    @SuppressWarnings("unchecked")
    List<OscarLog> results = query.getResultList();
    if (results.size() == 0) {
      return false;
    }

    return true;
  }

  public List<OscarLog> findByActionAndData(String action, String data) {
    String sqlCommand = "select x from " + modelClass.getSimpleName()
        + " x where x.action = ?1 and x.data = ?2 order by x.created DESC";
    Query query = entityManager.createQuery(sqlCommand);
    query.setParameter(1, action);
    query.setParameter(2, data);

    @SuppressWarnings("unchecked")
    List<OscarLog> results = query.getResultList();

    return results;
  }


  public List<OscarLog> findByActionContentAndDemographicId(String action, String content,
      Integer demographicId) {

    String sqlCommand = "select x from " + modelClass.getSimpleName()
        + " x where x.action=?1 and x.content = ?2 and x.demographicId=?3 order by x.created desc";

    Query query = entityManager.createQuery(sqlCommand);
    query.setParameter(1, action);
    query.setParameter(2, content);
    query.setParameter(3, demographicId);

    @SuppressWarnings("unchecked")
    List<OscarLog> results = query.getResultList();

    return (results);
  }

  public List<Integer> getDemographicIdsOpenedSinceTime(Date value) {
    String sqlCommand = "select distinct demographicId from " + modelClass.getSimpleName()
        + " where dateTime >= ?1";

    Query query = entityManager.createQuery(sqlCommand);
    query.setParameter(1, value);

    @SuppressWarnings("unchecked")
    List<Integer> results = query.getResultList();
    results.removeAll(Collections.singleton(null));

    return (results);
  }

  /**
   *
   * @param providerNo
   * @param startPosition
   * @param itemsToReturn
   * @return List of Object array [demographicId (Integer), lastDateViewed Date]
   */
  public List<Object[]> getRecentDemographicsViewedByProvider(String providerNo, int startPosition,
      int itemsToReturn) {
    String sqlCommand =
        "select l.demographicId,MAX(l.created) as dt from " + modelClass.getSimpleName()
            + " l where l.providerNo = ?1 and l.demographicId is not null and l.demographicId != '-1' group by l.demographicId order by MAX(l.created) desc";

    Query query = entityManager.createQuery(sqlCommand);
    query.setParameter(1, providerNo);
    query.setFirstResult(startPosition);
    setLimit(query, itemsToReturn);

    @SuppressWarnings("unchecked")
    List<Object[]> results = query.getResultList();

    return (results);
  }

  public List<Integer> getRecentDemographicsAccessedByProvider(
          final String providerNo,
          final int startPosition,
          final int itemsToReturn
  ) {
    LocalDateTime cutoffDateTime = LocalDateTime.now().minusDays(RECENT_DEMOGRAPHIC_CUTOFF_DAYS);
    val sql =
        "SELECT l.demographicId FROM "
            + modelClass.getSimpleName()
            + " l WHERE l.providerNo = :providerId"
            + " AND l.demographicId IS NOT NULL"
            + " AND l.demographicId NOT IN (-1, 0)"
            + " AND l.created >= :cutoffDateTime"
            + " GROUP BY l.demographicId"
            + " ORDER BY MAX(l.created) DESC";

    val query = entityManager.createQuery(sql);
    query.setParameter("providerId", providerNo);
    query.setParameter(
        "cutoffDateTime",
        Date.from(cutoffDateTime.atZone(ZoneId.systemDefault()).toInstant()),
        TemporalType.TIMESTAMP);
    setLimit(query, startPosition, itemsToReturn);

    return (List<Integer>) query.getResultList();
  }

  /**
   *
   * @param providerNo
   * @param startPosition
   * @param itemsToReturn
   * @return List of Object array [demographicId (Integer), lastDateViewed Date]
   */
  public List<Object[]> getRecentDemographicsViewedByProviderAfterDateIncluded(String providerNo,
      Date date, int startPosition, int itemsToReturn) {
    String sqlCommand =
        "select l.demographicId,MAX(l.created) as dt from " + modelClass.getSimpleName()
            + " l where l.providerNo = :providerNo and l.created >= :date and l.demographicId is not null and l.demographicId != '-1' group by l.demographicId order by MAX(l.created) desc";

    Query query = entityManager.createQuery(sqlCommand);
    query.setParameter("providerNo", providerNo);
    query.setParameter("date", date);
    query.setFirstResult(startPosition);
    setLimit(query, itemsToReturn);

    @SuppressWarnings("unchecked")
    List<Object[]> results = query.getResultList();

    return (results);
  }

  /*
   * Warning. Don't use this. It's only for the log purging feature.
   */
  public int purgeLogEntries(Date maxDateToRemove) {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    String sqlCommand =
        "DELETE FROM " + modelClass.getSimpleName() + " WHERE dateTime <= :dateTime";

    Query query = entityManager.createQuery(sqlCommand);
    query.setParameter("dateTime", formatter.format(maxDateToRemove));
    int ret = query.executeUpdate();

    return ret;


  }

  @Override
  public void remove(AbstractModel<?> o) {
    throw new SecurityException("Cannot remove audit log entries!");
  }

  @Override
  public boolean remove(Object id) {
    throw new SecurityException("Cannot remove audit log entries!");
  }

  /**
   * Find logs by date and content
   *
   * @param start start date
   * @param end end date
   * @param content content type
   * @return List of logs found
   */
  public List<OscarLog> findByDateContentDemographic(
      Date start, Date end, String content, String demographicNo) {
    StringBuilder stringBuilder = new StringBuilder("SELECT x.* FROM log x");
    stringBuilder.append(" WHERE x.content LIKE :content");

    if (StringUtils.isNotEmpty(demographicNo)) {
      stringBuilder.append(" AND x.demographic_no LIKE :demographicNo");
    }

    return getSecurityLogs(content, start, end, demographicNo, stringBuilder);
  }

  /**
   * Find logs by providerNo, date and content
   *
   * @param providerNo provider number provided
   * @param start start date
   * @param end end date
   * @param content content type
   * @return List of logs found
   */
  public List<OscarLog> findByProviderDateContentDemographic(
      String providerNo, Date start, Date end, String content, String demographicNo) {
    StringBuilder stringBuilder = new StringBuilder("SELECT x.* FROM log x");
    stringBuilder
        .append(" WHERE x.content LIKE :content")
        .append(" AND x.provider_no = :providerNo");

    if (StringUtils.isNotEmpty(demographicNo)) {
      stringBuilder.append(" AND x.demographic_no LIKE :demographicNo");
    }

    return getSecurityLogs(content, providerNo, start, end, demographicNo, stringBuilder);
  }

  /**
   * Find logs by site, date and content The logic for finding the logs using site this way was
   *
   * @param providerNo provider number provided
   * @param start start date
   * @param end end date
   * @param content content type
   * @return List of logs found
   */
  public List<OscarLog> findBySite(
      String providerNo, Date start, Date end, String content, String demographicNo) {
    StringBuilder stringBuilder = new StringBuilder("SELECT x.* FROM log x ");
    stringBuilder
        .append("LEFT JOIN providersite ps ON x.provider_no = ps.provider_no")
        .append(" WHERE x.content LIKE :content")
        .append(" AND ps.provider_no = :providerNo");

    if (StringUtils.isNotEmpty(demographicNo)) {
      stringBuilder.append(" AND x.demographic_no LIKE :demographicNo");
    }

    return getSecurityLogs(content, providerNo, start, end, demographicNo, stringBuilder);
  }

  /**
   * Find logs by site, date and content The logic for finding the logs using site this way was
   *
   * @param content content type
   * @param start start date
   * @param end end date
   * @param stringBuilder current SQL query string
   * @return List of logs found
   */
  private List<OscarLog> getSecurityLogs(
      String content, Date start, Date end, String demographicNo, StringBuilder stringBuilder) {
    return getSecurityLogs(content, null, start, end, demographicNo, stringBuilder);
  }

  /**
   * Find logs by site, date and content The logic for finding the logs using site this way was
   *
   * @param content content type
   * @param providerNo provider number provided
   * @param start start date
   * @param end end date
   * @param stringBuilder current SQL query string
   * @return List of logs found
   */
  private List<OscarLog> getSecurityLogs(
      String content,
      String providerNo,
      Date start,
      Date end,
      String demographicNo,
      StringBuilder stringBuilder) {
    end.setTime(end.getTime() + (1000 * 3600 * 24)); //search whole of end day
    if (start != null && end != null) {
      stringBuilder.append(" AND x.dateTime BETWEEN :start AND :end");
    } else if (start != null) {
      stringBuilder.append(" AND x.dateTime >= :start");
    } else if (end != null) {
      stringBuilder.append(" AND x.dateTime <= :end");
    }
    stringBuilder.append(" ORDER BY x.dateTime DESC");

    Query query = entityManager.createNativeQuery(stringBuilder.toString(), modelClass);
    query.setParameter("content", content);

    if (StringUtils.isNotEmpty(providerNo)) {
      query.setParameter("providerNo", providerNo);
    }

    if (StringUtils.isNotEmpty(demographicNo)) {
      query.setParameter("demographicNo", demographicNo);
    }

    if (start != null) {
      query.setParameter("start", start);
    }

    if (end != null) {
      query.setParameter("end", end);
    }

    return query.getResultList();
  }
}
