/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import lombok.val;
import org.oscarehr.common.model.OnAccountDepositService;
import org.oscarehr.common.model.OnAccountService;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OnAccountDepositServiceDao extends AbstractDao<OnAccountDepositService> {

    public OnAccountDepositServiceDao() {
        super(OnAccountDepositService.class);
    }

    public List<OnAccountDepositService> findByInvoiceNo(Integer invoiceNo) {
        val query = entityManager.createQuery("SELECT entity FROM OnAccountDepositService entity " +
                "WHERE entity.invoiceNo = :invoiceNo");
        query.setParameter("invoiceNo", invoiceNo);
        return query.getResultList();
    }
    public void updateStatusByInvoiceNumber(final Integer invoiceNumber,
                                            final String newStatus) {
        val query = entityManager.createQuery(
                "UPDATE OnAccountDepositService entity SET entity.status = :newStatus " +
                        "WHERE entity.invoiceNo = :invoiceNumber");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.setParameter("newStatus", newStatus);
        query.executeUpdate();
    }

    public void deleteByInvoiceNumber(Integer invoiceNumber) {
        val query = entityManager.createQuery("DELETE FROM OnAccountDepositService " +
                "WHERE invoiceNo = :invoiceNumber");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.executeUpdate();
    }
}