package org.oscarehr.common.dao;

import java.util.List;

import javax.persistence.Query;

import org.oscarehr.common.model.EformDocs;

public class EformDocsDao extends AbstractDao<EformDocs> {
	
	public EformDocsDao(){
		super(EformDocs.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> findLabs(Integer fdid) {
		String sql = "FROM EformDocs cd, PatientLabRouting plr " +
				"WHERE plr.labNo = cd.documentNo " +
				"AND cd.requestId = :eformId " +
				"AND cd.docType = :docType " +
				"AND cd.deleted IS NULL " +
				"ORDER BY cd.documentNo";
		Query q = entityManager.createQuery(sql);
		q.setParameter("eformId", fdid);
		q.setParameter("docType", EformDocs.DOCTYPE_LAB);
		return q.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> findCMLLabs(Integer fdid) {
		String sql = "FROM EformDocs cd, LabPatientPhysicianInfo lpp " +
				"WHERE lpp.id = cd.documentNo " +
				"AND cd.requestId = :eformId " +
				"AND cd.docType = :docType " +
				"AND cd.deleted IS NULL " +
				"ORDER BY cd.documentNo";
		Query q = entityManager.createQuery(sql);
		q.setParameter("eformId", fdid);
		q.setParameter("docType", EformDocs.DOCTYPE_LAB);
		return q.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<EformDocs> findByRequestIdDocNoDocType(Integer requestId, Integer documentNo, String docType) {
	  	String sql = "select x from EformDocs x where x.requestId=? and x.documentNo=? and x.docType=? and x.deleted is NULL";
    	Query query = entityManager.createQuery(sql);
    	query.setParameter(1,requestId);
    	query.setParameter(2,documentNo);
    	query.setParameter(3,docType);

        List<EformDocs> results = query.getResultList();
        return results;
	}
	
	@SuppressWarnings("unchecked")
	public List<EformDocs> findByRequestIdDocumentNoAndDocumentType(Integer requestId, Integer documentNo, String docType) {
	  	String sql = "select x from EformDocs x where x.requestId=? and x.documentNo=? and x.docType=?";
    	Query query = entityManager.createQuery(sql);
    	query.setParameter(1,requestId);
    	query.setParameter(2,documentNo);
    	query.setParameter(3,docType);

        List<EformDocs> results = query.getResultList();
        return results;
	}
	
	@SuppressWarnings("unchecked")
	public List<EformDocs> findByRequestIdDocumentNo(Integer requestId, Integer documentNo) {
	  	String sql = "select x from EformDocs x where x.requestId=? and x.documentNo=?";
    	Query query = entityManager.createQuery(sql);
    	query.setParameter(1,requestId);
    	query.setParameter(2,documentNo);

        List<EformDocs> results = query.getResultList();
        return results;
	}
	
	@SuppressWarnings("unchecked")
	public List<EformDocs> findByRequestIdAndDocType(Integer requestId, String docType) {
	  	String sql = "select x from EformDocs x where x.requestId=? and x.docType=? and x.deleted is NULL";
    	Query query = entityManager.createQuery(sql);
    	query.setParameter(1,requestId);
    	query.setParameter(2,docType);

        List<EformDocs> results = query.getResultList();
        return results;
	}
}
