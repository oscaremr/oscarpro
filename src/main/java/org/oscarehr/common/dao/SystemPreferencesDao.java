package org.oscarehr.common.dao;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Nullable;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.SpringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.val;
import oscar.OscarProperties;

@Repository
@SuppressWarnings("unchecked")
public class SystemPreferencesDao extends AbstractDao<SystemPreferences> {

    private final OscarProperties oscarProps = OscarProperties.getInstance();
    
    public static final Map<String, String> newToOldPreferencesNamesMap;
    static {
        Map<String, String> map = new HashMap<String, String>();
        map.put("fax_enabled", "faxEnable");
        map.put("enable_pathnet_labs", "PATHNET_LABS");
        map.put("enable_hl7text_labs", "HL7TEXT_LABS");
        map.put("enable_cml_labs", "CML_LABS");
        map.put("enable_mds_labs", "MDS_LABS");
        map.put("enable_epsilon_labs", "Epsilon_LABS");
        map.put("show_sexual_health_label", "showSexualHealthLabel");
        map.put("show_single_page_chart", "SINGLE_PAGE_CHART");
        map.put("use_program_location_enabled", "useProgramLocation");
        map.put("assign_def_issue_to_no_notes_waitlist", "wl_default_issue");
        map.put("ect_autosave_timer", "ECT_AUTOSAVE_TIMER");
        map.put("ect_save_feedback_timer", "ECT_SAVE_FEEDBACK_TIMER");
        map.put("display_flag_as_abnormal", "flag_as_abnormal");
        map.put("allow_update_document_content", 
            "ALLOW_UPDATE_DOCUMENT_CONTENT");
        map.put("enable_move_delete_doc_to_deleted_folder", 
            "INCOMINGDOCUMENT_RECYCLEBIN");
        map.put("enable_signature_tablet", "signature_tablet");
        map.put("enable_rx_order_by_date", "RX_ORDER_BY_DATE");
        map.put("rx_footer_text", "RX_FOOTER");
        map.put("auto_generated_billing_enabled", "auto_generated_billing");
        map.put("bc_default_alt_billing_enabled", "BC_DEFAULT_ALT_BILLING");
        map.put("billing_review_auto_payment_enabled", 
            "BILLING_REVIEW_AUTO_PAYMENT");
        map.put("invoice_reports_print_hide_name_enabled", 
            "invoice_reports.print.hide_name");
        map.put("rma_billing_enabled", "rma_enabled");
        map.put("sob_check_all_enabled", "SOB_CHECKALL");
        map.put("delete_bill_confirm_action_enabled", "warnOnDeleteBill");
        map.put("new_billing_ui.enabled", "isNewBillingUiEnabled");
        map.put("new_on_billing_enabled", "isNewONbilling");
        map.put("renal_dosing_ds_enabled", "RENAL_DOSING_DS");
        map.put("prevention_enabled", "PREVENTION");
        map.put("immunization_in_prevention_enabled", 
            "IMMUNIZATION_IN_PREVENTION");
        map.put("rx_show_band_number", "showRxBandNumber");
        map.put("rx_show_chart_number", "showRxChartNo");
        map.put("rx_show_hin", "showRxHin");
        map.put("rx_hide_drug_of_choice", "rx.drugofchoice.hide");
        map.put("enable_rx_internal_dispensing", 
            "rx.enable_internal_dispensing");
        map.put("enable_rx_allergy_checking", "RX_ALLERGY_CHECKING");
        map.put("enable_rx_interact_local_drugref_reg_id", 
            "RX_INTERACTION_LOCAL_DRUGREF_REGIONAL_IDENTIFIER");
        map.put("enable_consultation_appt_instr_lookup", 
            "CONSULTATION_APPOINTMENT_INSTRUCTIONS_LOOKUP");
        map.put("enable_consultation_auto_incl_allergies", 
            "CONSULTATION_AUTO_INCLUDE_ALLERGIES");
        map.put("enable_consultation_auto_inc_medications", 
            "CONSULTATION_AUTO_INCLUDE_MEDICATIONS");
        map.put("enable_consultation_lock_referral_date", 
            "CONSULTATION_LOCK_REFERRAL_DATE");
        map.put("enable_consultation_patient_will_book", 
            "CONSULTATION_PATIENT_WILL_BOOK");
        map.put("enable_alerts_on_schedule_screen", 
            "displayAlertsOnScheduleScreen");
        map.put("enable_notes_on_schedule_screen", 
            "displayNotesOnScheduleScreen");
        map.put("enable_default_schedule_viewall", 
            "default_schedule_viewall");
        map.put("enable_demographic_patient_clinic_status", 
            "DEMOGRAPHIC_PATIENT_CLINIC_STATUS");
        map.put("enable_demogr_patient_health_care_team", 
            "DEMOGRAPHIC_PATIENT_HEALTH_CARE_TEAM");
        map.put("enable_demographic_patient_rostering",
            "DEMOGRAPHIC_PATIENT_ROSTERING");
        map.put("enable_demographic_waiting_list", 
            "DEMOGRAPHIC_WAITING_LIST");
        map.put("enable_external_name_on_schedule", 
            "external_name_on_schedule");
        map.put("enable_provider_schedule_note", "provider_schedule_note");
        map.put("enable_receptionist_alt_view", "receptionist_alt_view");
        map.put("chart_print_include_cellphone", "print.includeCellPhone");
        map.put("chart_print_include_hin", "print.includeHin");
        map.put("chart_print_include_home_phone", "print.includeHomePhone");
        map.put("chart_print_include_mrp", "print.includeMRP");
        map.put("chart_print_include_work_phone", "print.includeWorkPhone");
        map.put("chart_print_use_provider_current_program", 
            "print.useCurrentProgramInfoInHeader");
        map.put("echart_display_demographic_no", 
            "echart.display_demographic_no");
        map.put("program_domain_show_echart", "program_domain.show_echart");
        map.put("enable_eform_in_appointment", "eform_in_appointment");
        map.put("display_meditech_id", "meditech_id");
        map.put("enable_appointment_mc_number", "mc_number");
        map.put("allow_multiple_same_day_group_appts", 
            "allow_multiple_same_day_group_appts");
        map.put("enable_appointment_intake_form", "appt_intake_form");
        map.put("display_appointment_daysheet_button", 
            "view.appointmentdaysheetbutton");
        map.put("encounter_layout_refresh_timeout", 
            "refresh.encounterLayout.jsp");
        map.put("show_appointment_reason", "SHOW_APPT_REASON");
        map.put("hide_con_report_link", "hide_ConReport_link");
        map.put("hide_econsult_link", "hide_eConsult_link");
        map.put("retrieve_more_recent_tmp_notes", "maxTmpSave");
        map.put("measurements_create_new_note_enabled", 
            "measurements_create_new_note");
        map.put("new_contacts_ui_health_care_team_linked", 
            "NEW_CONTACTS_UI_HEALTH_CARE_TEAM_LINKED");
        map.put("new_label_print_enabled", "new_label_print");
        map.put("enable_new_user_pin_control", "NEW_USER_PIN_CONTROL");
        map.put("enable_printpdf_referring_practitioner", 
            "printPDF_referring_prac");
        map.put("private_informed_consent_enabled", "privateConsentEnabled");
        map.put("show_referral_menu", "referral_menu");
        map.put("enable_resident_review_workflow", "resident_review");
        map.put("enable_save_as_xml", "save_as_xml");
        map.put("display_admin_hph", "admin.hph");
        map.put("caseload_default_all_providers_enabled", 
            "CASELOAD_DEFAULT_ALL_PROVIDERS");
        map.put("client_dropdown_enabled", "clientdropbox");
        map.put("display_masterfile_referral_source", 
            "masterfile_referral_source");
        map.put("lab_require_included_chartno", "lab_req_include_chartno");
        map.put("require_acknowledged_docs_confirm_dialog", "confirmAck");
        map.put("lab_nomatch_names_enabled", "LAB_NOMATCH_NAMES");
        newToOldPreferencesNamesMap = Collections.unmodifiableMap(map);
    }
    
    public SystemPreferencesDao() {
        super(SystemPreferences.class);
    }

    @Nullable
    public SystemPreferences findPreferenceByName(String name) {
        Query query = entityManager.createQuery(
            "FROM SystemPreferences sp WHERE sp.name = :name");
        query.setParameter("name", name);
        val oldName = newToOldPreferencesNamesMap.containsKey(name) ? 
            newToOldPreferencesNamesMap.get(name) : name;

        List<SystemPreferences> results = query.getResultList();
        if (!results.isEmpty()) {
            return results.get(0);
        } else if (oscarProps.hasProperty(oldName)) {
            //if the property is not present in the database check in the files
            val property = oscarProps.getProperty(oldName);
            Set<String> activeMarkers = new HashSet<String>(
                Arrays.asList(new String[] { "true", "yes", "on" }));
            Set<String> disableMarkers = new HashSet<String>(
                Arrays.asList(new String[]{ "false", "no", "off" }));
            val preferenceValue = activeMarkers.contains(property) ? "true" :
                disableMarkers.contains(property) ? "false" :
                    property;
            return new SystemPreferences(name, preferenceValue);
        }

        return null;
    }

    public String getPreferenceValueByName(String name, String defaultValue) {
        SystemPreferences preferences = findPreferenceByName(name);
        if (preferences != null && preferences.getValue() != null) {
            return preferences.getValue();
        }
        return defaultValue;
    }

    public List<SystemPreferences> findPreferencesByNames(List<String> names) {
        Query query = entityManager.createQuery(
            "FROM SystemPreferences sp WHERE sp.name IN (:names)");
        query.setParameter("names", names);

        List<SystemPreferences> results = query.getResultList();
        return results;
    }

    /**
     * Gets a map of system preference values
     *
     * @param keys List of preference keys to search for in the database
     * @return Map of preference keys with their associated boolean value
     */
    public Map<String, Boolean> findByKeysAsMap(List<String> keys) {
        List<SystemPreferences> preferences = findPreferencesByNames(keys);
        Map<String, Boolean> preferenceMap = new HashMap<String, Boolean>();

        for (SystemPreferences preference : preferences) {
            preferenceMap.put(preference.getName(), preference.getValueAsBoolean());
        }

        return preferenceMap;
    }

    /**
     * Gets a map of system preferences with the preference name as the key
     *
     * @param keys List of keys to get the preferences for
     * @return A map of SystemPreferences with the preference name as the key
     */
    public Map<String, SystemPreferences> findByKeysAsPreferenceMap(List<String> keys) {
        Map<String, SystemPreferences> preferenceMap = new HashMap<>();

        List<SystemPreferences> preferences = findPreferencesByNames(keys);

        for (SystemPreferences preference : preferences) {
            preferenceMap.put(preference.getName(), preference);
        }

        return preferenceMap;
    }

    public boolean isReadBooleanPreference(String name) {
        SystemPreferences preference = findPreferenceByName(name);
        return (preference != null && Boolean.parseBoolean(preference.getValue()));
    }

    public boolean isReadBooleanPreferenceWithDefault(String name, boolean defaultIfNotFound) {
        SystemPreferences preference = findPreferenceByName(name);
        if (preference != null) {
            return Boolean.parseBoolean(preference.getValue());
        } else {
            return defaultIfNotFound;
        }
    }

    public boolean isKioskEnabled() {
        SystemPreferences kioskProvider = findPreferenceByName("kioskProvider");
        ProviderDataDao providerDataDao = SpringUtils.getBean(ProviderDataDao.class);
        return kioskProvider != null && kioskProvider.getValue() != null
            && providerDataDao.findByProviderNo(kioskProvider.getValue()) != null;
    }

    public boolean isPreferenceValueEquals(String preferenceName, String trueValueStr) {
        SystemPreferences preference = findPreferenceByName(preferenceName);
        return (preference != null && trueValueStr.equals(preference.getValue()));
    }

    public void mergeOrPersist(String name, String value) {
        SystemPreferences newPref = new SystemPreferences();
        newPref.setName(name);
        newPref.setValue(value);
        mergeOrPersist(newPref);
    }

    public void mergeOrPersist(SystemPreferences pref) {
        SystemPreferences existingPref = findPreferenceByName(pref.getName());
        pref.setUpdateDate(new Date());
        if (existingPref == null) {
            persist(pref);
        } else {
            pref.setId(existingPref.getId());
            merge(pref);
        }
    }

    public boolean isFhirSubscriptionEnabled() {
        SystemPreferences value = findPreferenceByName(SystemPreferences.FHIR_SUBSCRIPTION_ENABLED);
        return value != null && value.getValueAsBoolean();
    }

    public boolean isAddNewDocumentTypeShown() {
      val addNewDocType = this.findPreferenceByName("add_new_button_enabled");
      return addNewDocType == null || addNewDocType.getValueAsBoolean();
    }

    public boolean isValidationOnSpecialistEnabled() {
      return this.isReadBooleanPreferenceWithDefault("enable_validation_on_specialist", false);
    }

    public boolean isOneIdEnabled() {
        return this.isReadBooleanPreferenceWithDefault("oneid.enabled", false);
    }

    public boolean isDhdrEnabled() {
        return this.isReadBooleanPreferenceWithDefault("oneid.dhdr.enabled", false);
    }

    public boolean isDhirEnabled() {
        return this.isReadBooleanPreferenceWithDefault("oneid.dhir.enabled", false);
    }

    public boolean isClinicalConnectEnabled() {
        return this.isReadBooleanPreferenceWithDefault("oneid.clinical_connect.enabled", false);
    }

    public boolean isAppointmentRemindersEnabled() {
        return this.isReadBooleanPreferenceWithDefault("enable_appointment_reminders", true);
    }

    public boolean isEformsEnabled() {
        return this.isReadBooleanPreferenceWithDefault("oneid.eforms.enabled", false);
    }

    public boolean isAttachmentManagerEnabled() {
        return this.isReadBooleanPreferenceWithDefault("attachment_manager.enabled", true);
    }
    
    public boolean isAttachmentManagerConsultationEnabled () {
        return this.isReadBooleanPreferenceWithDefault("attachment_manager.consultation.enabled", false);
    }
}