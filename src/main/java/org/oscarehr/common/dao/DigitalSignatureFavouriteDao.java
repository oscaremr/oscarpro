/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import org.oscarehr.common.model.DigitalSignatureFavourite;
import org.springframework.stereotype.Repository;

@Repository
public class DigitalSignatureFavouriteDao extends AbstractDao<DigitalSignatureFavourite> {

  public DigitalSignatureFavouriteDao() {
    super(DigitalSignatureFavourite.class);
  }

  public DigitalSignatureFavourite findFavouriteSignatureById(final int id) {
    return (DigitalSignatureFavourite) entityManager.createQuery(
            "SELECT dsf FROM DigitalSignatureFavourite dsf WHERE dsf.signatureId = :id")
        .setParameter("id", id)
        .getSingleResult();
  }

}
