/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */

package org.oscarehr.common.dao;

import javax.persistence.Query;
import org.oscarehr.common.model.ExcellerisEorder;
import org.springframework.stereotype.Repository;

@Repository
public class EOrderDao extends AbstractDao<ExcellerisEorder> {


	public EOrderDao() {
		super(ExcellerisEorder.class);
	}

    /**
     * Finds an eOrder based on internal id.
     *
     * @param eOrderInternalId
     * 		internal id of eOrder.
     * @return
     * 		Returns the eOrder with the specified internal id.
     */
    public ExcellerisEorder findById(Integer eOrderInternalId)
    {
    	Query query = entityManager.createQuery("select x from " + modelClass.getSimpleName() + " x where x.fid=?1");
    	query.setParameter(1, eOrderInternalId);

    	return this.getSingleResultOrNull(query);
    }

  /**
   * Finds an eOrder based on internal id.
   *
   * @param eOrderExternalId external id of eOrder.
   * @return Returns the eOrder with the specified external id.
   */
  public ExcellerisEorder findByExtId(Integer eOrderExternalId) {
    Query query =
        entityManager.createQuery(
            "select x from " + modelClass.getSimpleName() + " x where x.externalOrderId=?1");
    query.setParameter(1, eOrderExternalId);

    return this.getSingleResultOrNull(query);
  }

    public ExcellerisEorder findByExtEFormDataId(Integer eFormDataId)
    {
	Query query = entityManager.createQuery("select x from " + modelClass.getSimpleName() + " x where x.eFormDataId=?1");
    	query.setParameter(1, eFormDataId);
    	
    	return this.getSingleResultOrNull(query);
    }


}
