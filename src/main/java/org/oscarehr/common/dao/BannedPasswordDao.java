/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import java.util.HashSet;
import java.util.Set;
import lombok.val;
import org.oscarehr.common.model.BannedPassword;
import org.springframework.stereotype.Repository;

@Repository
public class BannedPasswordDao extends AbstractDao<BannedPassword> {

  protected BannedPasswordDao() {
    super(BannedPassword.class);
  }

  public Set<BannedPassword> getAllBannedPasswords() {
    val query = entityManager.createQuery(
        "FROM " + modelClass.getSimpleName() + " b");
    return new HashSet<BannedPassword>(query.getResultList());
  }
}
