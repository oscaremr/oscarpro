/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import java.util.List;
import javax.persistence.Query;
import org.apache.log4j.Logger;
import org.oscarehr.common.model.DynacareCopyToProvider;
import org.oscarehr.util.MiscUtils;
import org.springframework.stereotype.Repository;

@Repository
public class DynacareCopyToProviderDao extends AbstractDao<DynacareCopyToProvider> {

  private static final Logger logger = MiscUtils.getLogger();

  public DynacareCopyToProviderDao() {
    super(DynacareCopyToProvider.class);
  }

  /** Sorted by lastname,firstname */
  public List<DynacareCopyToProvider> findAll() {
    logger.debug("Entering copy to dao find all");
    Query query =
        entityManager.createQuery(
            "select x from "
                + modelClass.getName()
                + " x order by x.lastName,x.firstName");

    @SuppressWarnings("unchecked")
    List<DynacareCopyToProvider> results = query.getResultList();
    return (results);
  }

  public List<DynacareCopyToProvider> findByFullName(String lastName, String firstName) {
    Query query =
        entityManager.createQuery(
            "select x from "
                + modelClass.getName()
                + " x WHERE x.lastName like ? and x.firstName like ?  order by x.lastName");
    query.setParameter(1, lastName + "%");
    query.setParameter(2, firstName + "%");

    @SuppressWarnings("unchecked")
    List<DynacareCopyToProvider> cList = query.getResultList();
    if (cList != null && cList.size() > 0) {
      return cList;
    }

    return null;
  }

  public List<DynacareCopyToProvider> findByLastName(String lastName) {
    return findByFullName(lastName, "");
  }
}
