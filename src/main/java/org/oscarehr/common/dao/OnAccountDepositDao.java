/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import java.util.List;

import lombok.val;
import org.oscarehr.common.model.OnAccountBill;
import org.oscarehr.common.model.OnAccountDeposit;
import org.springframework.stereotype.Repository;

@Repository
public class OnAccountDepositDao extends AbstractDao<OnAccountDeposit> {

    public OnAccountDepositDao() {
        super(OnAccountDeposit.class);
    }

    public List<OnAccountDeposit> findByDemographicNumber(Integer demographicNumber) {
        val query = entityManager.createQuery("SELECT entity FROM OnAccountDeposit entity " +
                "WHERE entity.demographicNo = :demographicNumber");
        query.setParameter("demographicNumber", demographicNumber);
        return query.getResultList();
    }
    
    public OnAccountDeposit findByDemographicAndInvoiceNumber(final Integer demographicNumber,
                                                              final Integer invoiceNumber) {
        val query = entityManager.createQuery("SELECT entity FROM OnAccountDeposit entity " +
                "WHERE entity.demographicNo = :demographicNumber and entity.id = :invoiceNumber");
        query.setParameter("demographicNumber", demographicNumber);
        query.setParameter("invoiceNumber", invoiceNumber);
        List<OnAccountDeposit> results = query.getResultList();
        return results.isEmpty() ? null : results.get(0);
    }

    public Double getTotalDepositsByDemographicNumber(Integer demographicNumber) {
        val query = entityManager.createQuery("SELECT SUM(totalDeposits) FROM OnAccountDeposit entity " +
                "WHERE entity.demographicNo = :demographicNumber");
        query.setParameter("demographicNumber", demographicNumber);
        val result = (Double) query.getSingleResult();
        return result == null ? 0.0 : result;
    }

    public Double getTotalBalanceByDemographicNumber(Integer demographicNumber) {
        val query = entityManager.createQuery("SELECT SUM(balance) FROM OnAccountDeposit entity " +
                "WHERE entity.demographicNo = :demographicNumber");
        query.setParameter("demographicNumber", demographicNumber);
        val result = (Double) query.getSingleResult();
        return result == null ? 0.0 : result;
    }

    public void deleteTempDepositByInvoiceNumber(final Integer invoiceNumber) {
        val query = entityManager.createQuery(
                "DELETE FROM OnAccountDeposit entity WHERE entity.id = :invoiceNumber " +
                "AND entity.depositStatus = 'temp'");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.executeUpdate();
    }

    public void setDepositVoidByInvoiceNumber(Integer invoiceNumber) {
        val query = entityManager.createQuery("UPDATE OnAccountDeposit entity " +
                "SET entity.depositStatus = 'Void', " +
                "entity.totalDeposits = 0, " +
                "entity.totalServices = 0, " +
                "entity.balance = 0 " +
                "WHERE entity.id = :invoiceNumber");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.executeUpdate();
    }
}