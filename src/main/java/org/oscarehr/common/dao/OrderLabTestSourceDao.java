/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import javax.persistence.Query;

import org.oscarehr.common.model.OrderLabTestCodeSource;
import org.springframework.stereotype.Repository;

@Repository
public class OrderLabTestSourceDao extends AbstractDao<OrderLabTestCodeSource> {

  public OrderLabTestSourceDao() {
    super(OrderLabTestCodeSource.class);
  }

  public OrderLabTestCodeSource findBySourceName(final String province, final String fieldId, final String sourceName)
    {
    Query query = entityManager
        .createQuery("SELECT x FROM " + modelClass.getSimpleName() + " x WHERE x.province=?1 AND x.uiId=?2 AND x.sourceName=?3");
     query.setParameter(1, province);
     query.setParameter(2, fieldId);
     query.setParameter(3, sourceName);

      return this.getSingleResultOrNull(query);
    }


  public OrderLabTestCodeSource findByFieldId(final String province, final String fieldId)
  {
    Query query = entityManager
        .createQuery("SELECT x FROM " + modelClass.getSimpleName() + " x WHERE x.province=?1 AND x.uiId=?2");
    query.setParameter(1, province);
    query.setParameter(2, fieldId);

    return this.getSingleResultOrNull(query);
  }

}

