/*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import java.util.List;
import javax.persistence.Query;
import org.oscarehr.common.model.DemographicSite;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class DemographicSiteDao extends AbstractDao<DemographicSite> {
  public DemographicSiteDao() {
    super(DemographicSite.class);
  }

  @SuppressWarnings("unchecked")
  public List<DemographicSite> findDemographicBySiteId(Integer siteId) {
    Query q = entityManager.createQuery("select x from DemographicSite x where x.siteId = :siteId");
    q.setParameter("siteId", siteId);
    return q.getResultList();
  }

  @SuppressWarnings("unchecked")
  public List<DemographicSite> findSitesByDemographicId(Integer demoId) {
    Query q = entityManager
        .createQuery("select x from DemographicSite x where x.demographicId = :demoId");
    q.setParameter("demoId", demoId);
    return q.getResultList();
  }

  public void removeSitesByDemographicId(Integer demoId) {
    Query q =
        entityManager.createQuery("delete from DemographicSite x where x.demographicId = :demoId");
    q.setParameter("demoId", demoId);
    q.executeUpdate();
  }
}
