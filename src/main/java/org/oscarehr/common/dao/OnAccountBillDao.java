/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import java.util.List;

import lombok.val;
import org.oscarehr.common.model.OnAccountBill;
import org.springframework.stereotype.Repository;

@Repository
public class OnAccountBillDao extends AbstractDao<OnAccountBill> {

    public OnAccountBillDao() {
        super(OnAccountBill.class);
    }


    public List<OnAccountBill> findByDemographicNumber(Integer demographicNo) {
        val query = entityManager.createQuery("SELECT entity FROM OnAccountBill entity " +
                "WHERE entity.demographicNo = :demographicNo");
        query.setParameter("demographicNo", demographicNo);
        return query.getResultList();
    }

    public Double getTotalBillsByDemographicNumber(Integer demographicNumber) {
        val query = entityManager.createQuery("SELECT SUM(amountCharged) FROM OnAccountBill entity " +
                "WHERE entity.demographicNo = :demographicNumber");
        query.setParameter("demographicNumber", demographicNumber);
        val result = (Double) query.getSingleResult();
        return result == null ? 0.0 : result;
    }

    public Double getTotalBalanceByDemographicNumber(Integer demographicNumber) {
        val query = entityManager.createQuery("SELECT SUM(balance) FROM OnAccountBill entity " +
                "WHERE entity.demographicNo = :demographicNumber");
        query.setParameter("demographicNumber", demographicNumber);
        val result = (Double) query.getSingleResult();
        return result == null ? 0.0 : result;
    }

    public OnAccountBill findByInvoiceAndDemographicNumber(final Integer invoiceNo,
                                                           final Integer demographicNumber) {
        val query = entityManager.createQuery("SELECT entity FROM OnAccountBill entity " +
                "WHERE entity.id = :invoiceNo AND entity.demographicNo = :demographicNumber");
        query.setParameter("invoiceNo", invoiceNo);
        query.setParameter("demographicNumber", demographicNumber);
        List<OnAccountBill> results = query.getResultList();
        return results.isEmpty() ? null : results.get(0);
    }

    public void voidBillByInvoiceNumber(Integer invoiceNumber) {
        val query = entityManager.createQuery("UPDATE OnAccountBill entity " +
                "SET entity.billStatus = 'Void', " +
                "entity.totalServices = 0, " +
                "entity.amountCharged = 0, " +
                "entity.hst = 0, " +
                "entity.balance = 0 " +
                "WHERE entity.id = :invoiceNumber");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.executeUpdate();
    }

}