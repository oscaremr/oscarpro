/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import java.util.List;
import javax.persistence.Query;
import lombok.val;
import org.oscarehr.common.model.DemographicGender;

public class DemographicGenderDao extends AbstractDao<DemographicGender> {

  public DemographicGenderDao() {
    super(DemographicGender.class);
  }

  public List<DemographicGender> findAllDemographicGender() {
    val sql = "SELECT g FROM DemographicGender g WHERE g.deleted = false";
    return entityManager.createQuery(sql).getResultList();
  }
  
  public List<DemographicGender> findbyId(Integer id) {
    val sql = "SELECT g FROM DemographicGender g WHERE id=:id";
    val query = entityManager.createQuery(sql);
    query.setParameter("id", id);
    return query.getResultList();
  }

  public int deleteGenderById(Integer id) {
    val sql = "UPDATE DemographicGender SET deleted = true WHERE id=:id AND editable = true";
    val query = entityManager.createQuery(sql);
    query.setParameter("id", id);
    return query.executeUpdate();
  }

  public int updateGender(DemographicGender demographicGender) {
    val sql = "UPDATE DemographicGender SET value=:gender WHERE id=:id AND editable = true";
    val query = entityManager.createQuery(sql);
    query.setParameter("gender", demographicGender.getValue());
    query.setParameter("id", demographicGender.getId());
    return query.executeUpdate();
  }

}
