/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import java.util.List;
import javax.persistence.Query;
import org.oscarehr.common.model.OceanSetting;
import org.springframework.stereotype.Repository;

@Repository
public class OceanSettingDao extends AbstractDao<OceanSetting>{
  
  public OceanSettingDao() {
    super(OceanSetting.class);
  }
  
  public OceanSetting getSettings() {
    List<OceanSetting> settings = this.findAll(0, 1);
    return settings.isEmpty() ? null : settings.get(0);
  }
}
