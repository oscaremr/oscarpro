/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
 
package org.oscarehr.common.dao;

import java.util.List;
import javax.persistence.Query;
import lombok.val;
import org.oscarehr.common.model.BillingSliCode;
import org.oscarehr.common.model.BillingType;
import org.springframework.stereotype.Repository;

@Repository
public class BillingTypeDao extends AbstractDao<BillingType> {

  public BillingTypeDao() {
    super(BillingType.class);
  }

  @SuppressWarnings("unchecked")
  public List<BillingType> findAll() {
    val query = entityManager.createQuery("SELECT x FROM " + modelClass.getSimpleName() + " x");
    return query.getResultList();
  }

}
