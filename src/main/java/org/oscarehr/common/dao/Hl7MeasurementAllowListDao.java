package org.oscarehr.common.dao;

import org.oscarehr.common.NativeSql;
import org.oscarehr.common.model.Hl7MeasurementAllowList;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class Hl7MeasurementAllowListDao extends AbstractDao<Hl7MeasurementAllowList>{

    public Hl7MeasurementAllowListDao() {
        super(Hl7MeasurementAllowList.class);
    }

    @NativeSql("hl7MeasurementAllowList")
    public List<String> getAllowedHl7Measurements() {

        String sql = "select keyword from hl7MeasurementAllowList";
        Query query = entityManager.createNativeQuery(sql);

        return query.getResultList();
    }

    @NativeSql("hl7MeasurementAllowList")
    public boolean isKeywordAllowed(String keyword) {

        String sql = "select keyword from hl7MeasurementAllowList where keyword = ?1";
        
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, keyword);
        
        return !query.getResultList().isEmpty();
    }
}
