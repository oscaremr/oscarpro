/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import java.util.List;
import javax.persistence.Query;
import org.oscarehr.common.model.DynacareEorderLabTestCode;
import org.springframework.stereotype.Repository;

@Repository
public class DynacareEorderLabTestCodeDao extends AbstractDao<DynacareEorderLabTestCode> {

  public DynacareEorderLabTestCodeDao() {
    super(DynacareEorderLabTestCode.class);
  }

  public DynacareEorderLabTestCode findByTestCode(String testCode) {
    Query query =
        entityManager.createQuery(
            "select x from " + modelClass.getSimpleName() + " x where x.labTestCode=?1");
    query.setParameter(1, testCode);

    return this.getSingleResultOrNull(query);
  }
  public List<DynacareEorderLabTestCode> findByAll() {
	    Query query =
	        entityManager.createQuery(
	            "select x from " + modelClass.getSimpleName() + " x ORDER BY x.longName");
	    return query.getResultList();
	  }
}
