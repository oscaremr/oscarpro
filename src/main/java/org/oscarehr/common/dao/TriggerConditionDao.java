package org.oscarehr.common.dao;

import org.oscarehr.common.model.OscarLog;
import org.oscarehr.common.model.TriggerCondition;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.springframework.stereotype.Repository;
import oscar.log.LogAction;

import javax.persistence.Query;
import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public class TriggerConditionDao extends AbstractDao<TriggerCondition>
{
    public TriggerConditionDao() { super(TriggerCondition.class); }
    
    public void removeConditionById (TriggerCondition triggerCondition) {
        try {
            entityManager.remove(entityManager.contains(triggerCondition) ? triggerCondition : entityManager.merge(triggerCondition));
        } catch (Exception e) {
            e.printStackTrace();;
        }
    }
}