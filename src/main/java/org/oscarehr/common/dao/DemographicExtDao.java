/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import javax.persistence.Query;
import lombok.SneakyThrows;
import lombok.val;
import lombok.var;
import org.oscarehr.common.dao.exception.OperationNotSupportedException;
import org.oscarehr.common.model.AbstractModel;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.DemographicExtKey;
import org.oscarehr.managers.DemographicManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class DemographicExtDao extends AbstractSubscriptionDao<DemographicExt> {

	public DemographicExtDao() {
		super(DemographicExt.class);
	}

	/* OVERRIDE METHODS TO ENFORCE ARCHIVING */

	@Override
	@SneakyThrows
	public void merge(AbstractModel<?> o) {
		throw new OperationNotSupportedException(
				"Direct calls to merge are not supported. Use saveDemographicExt instead.");
	}

	@Override
	@SneakyThrows
	public void persist(AbstractModel<?> o) {
		throw new OperationNotSupportedException(
				"Direct calls to persist are not supported. Use saveDemographicExt instead.");
	}

	@Override
	@SneakyThrows
	public void remove(AbstractModel<?> o)  {
		throw new OperationNotSupportedException(
				"Direct calls to remove are not supported. Use removeDemographicExt instead.");
	}

	/* CREATE & UPDATE */

	/**
	 * This Method is used to add a key value pair for a patient
	 * @param providerNumber providers Number entering the key value pair
	 * @param demographicNumber Demographic number of the patient that the  key/value  pair is for
	 *
	 * @param value The value for this key
	 */
	public void createDemographicExt(
			String providerNumber,
			Integer demographicNumber,
			String key,
			String value
	) {
		createAndArchiveDemographicExt(providerNumber, demographicNumber, key, value);
	}

	/**
	 * This Method is used to add a key value pair for a patient
	 * @param extension the DemographicExt object to save
	 */
	public void saveDemographicExt(DemographicExt extension) {
		saveDemographicExt(
				extension.getDemographicNo(),
				extension.getProviderNo(),
				extension.getKey(),
				extension.getValue(),
				false
		);
	}

	public void saveDemographicExts(List<DemographicExt> extensions, boolean skipArchiving) {
		for (DemographicExt extension : extensions) {
			saveDemographicExt(
					extension.getDemographicNo(),
					extension.getProviderNo(),
					extension.getKey(),
					extension.getValue(),
					skipArchiving
			);
		}
	}

	public void saveDemographicExt(
			Integer demographicNumber,
			String providerNumber,
			String key,
			String value
	) {
		saveDemographicExt(new DemographicExt(providerNumber, demographicNumber, key, value), false);
	}

	@Transactional
	public void saveDemographicExt(
			Integer demographicNumber,
			String providerNumber,
			String key,
			String value,
			boolean skipArchiving
	) {
		saveDemographicExt(new DemographicExt(providerNumber, demographicNumber, key, value), skipArchiving);
	}

	public void saveDemographicExt(DemographicExt extension, boolean skipArchiving) {
		validateExtensionData(extension.getDemographicNo(), extension.getKey());
		if (extension.getValue() == null) {
			return;
		}
		var existingExtension =
				this.getDemographicExt(extension.getDemographicNo(), extension.getKey());
		if (existingExtension != null) {
			extension.setId(existingExtension.getId());
			extension.setDateCreated(new Date());
			super.merge(extension);
		} else {
			extension.setDateCreated(new Date());
			extension.setHidden(false);
			super.persist(extension);
		}
		if (!skipArchiving) {
			DemographicManager.archiveDemographicExt(extension);
		}
	}

	/* DELETE */

	public void removeDemographicExt(Integer id) {
		if (id == null || id <= 0) {
			throw new IllegalArgumentException();
		}
		DemographicExt abstractModel = find(id);
		if (abstractModel != null) {
			super.remove(abstractModel);
		}
	}

	/* READ */

	public List<String> getAllDistinctValuesByKey(String key) {
		var query = entityManager.createQuery(
				"SELECT DISTINCT d.value from DemographicExt d where d.key = ? order by d.dateCreated DESC"
		);
		query.setParameter(1, key);

		@SuppressWarnings("unchecked")
		List<String> results = query.getResultList();
		results.removeAll(Collections.singleton(null));
		return results;
	}

	public Map<String, String> getAllValuesForDemo(Integer demographicNumber) {
		Map<String, String> retval = new HashMap<>();
		var query = entityManager.createQuery(
				"SELECT d from DemographicExt d where d.demographicNo=? order by d.dateCreated"
		);
		query.setParameter(1, demographicNumber);

		@SuppressWarnings("unchecked")
		List<DemographicExt> demographicExts = query.getResultList();
		for (DemographicExt demographicExt : demographicExts) {
			retval.put(demographicExt.getKey(), demographicExt.getValue());
			retval.put(demographicExt.getKey() + "_id", demographicExt.getId().toString());
		}
		return retval;
	}

	public List<DemographicExt> getDemographicValByKey() {
		var query = entityManager.createQuery("SELECT d from DemographicExt d where d.key=?");
		query.setParameter(1, "fNationCom");
		@SuppressWarnings("unchecked")
		List<DemographicExt> results = query.getResultList();
		return results;
	}

	public List<Integer> getDemographicIdsByKeyVal(String key, String val) {
		var query = entityManager.createQuery(
				"SELECT distinct d.demographicNo from DemographicExt d where d.key=? and d.value=?"
		);
		query.setParameter(1, key);
		query.setParameter(2, val);

		@SuppressWarnings("unchecked")
		List<Integer> results = query.getResultList();
		return results;
	}

	public DemographicExt getDemographicExt(Integer id) {
		return find(id);
	}

	@Nullable
	public DemographicExt getDemographicExt(final Integer demographicNumber, final String key) {
		var results = queryDemographicExtForKey(demographicNumber, key, false).getResultList();
		return !results.isEmpty()
				? (DemographicExt) results.get(0)
				: null;
	}

	@Nullable
	public DemographicExt getDemographicExt(
			final Integer demographicNumber,
			final String key,
			final boolean skipValidation
	) {
		var results = queryDemographicExtForKey(demographicNumber, key, skipValidation).getResultList();
		return !results.isEmpty()
				? (DemographicExt) results.get(0)
				: null;
	}

	@Nullable
	public DemographicExt getDemographicExt(Integer demographicNumber, DemographicExtKey key) {
		var results = queryDemographicExtForKey(demographicNumber, key.getKey(), false).getResultList();
		return !results.isEmpty()
				? (DemographicExt) results.get(0)
				: null;
	}
	@Nullable
	public DemographicExt getDemographicExt(
			final Integer demographicNumber,
			final DemographicExtKey key,
			final boolean skipValidation

	) {
		var results = queryDemographicExtForKey(demographicNumber, key.getKey(), skipValidation).getResultList();
		return !results.isEmpty()
				? (DemographicExt) results.get(0)
				: null;
	}

	@SuppressWarnings("unchecked")
	public List<DemographicExt> getDemographicExtByDemographicNo(Integer demographicNumber) {
		return queryDemographicExtForDemographic(demographicNumber).getResultList();
	}

	@Nullable
	public DemographicExt getDemographicExtKeyAndProvider(
			Integer demographicNumber,
			String key,
			String providerNumber
	) {
		var query = queryDemographicExtForKeyAndProviderNo(demographicNumber, key, providerNumber);

		@SuppressWarnings("unchecked")
		List<DemographicExt> results = query.getResultList();

		return !results.isEmpty()
				? results.get(0)
				: null;
	}

	@SuppressWarnings("unchecked")
	public List<DemographicExt> getDemographicExtByKeyAndValue(String key, String value) {
		return (List<DemographicExt>) queryDemographicExtForKeyAndValue(key, value).getResultList();
	}

	@Nullable
	public DemographicExt getLatestDemographicExt(Integer demographicNumber, String key) {
		validateExtensionData(demographicNumber, key);

		var query = entityManager.createQuery(
				"SELECT d from DemographicExt d where d.demographicNo=? and d.key = ? order by d.dateCreated DESC, d.id DESC"
		);
		query.setParameter(1, demographicNumber);
		query.setParameter(2, key);

		@SuppressWarnings("unchecked")
		List<DemographicExt> results = query.getResultList();

		return !results.isEmpty()
				? results.get(0)
				: null;
	}

	@SuppressWarnings("unchecked")
	public List<DemographicExt> getMultipleDemographicExt(
			final Integer demographicNumber,
			final String key
	) {
		return queryDemographicExtForKey(demographicNumber, key, false).getResultList();
	}

	public List<DemographicExt> getMultipleDemographicExtKeyForDemographicNumbersByProviderNumber(
			final DemographicExtKey key,
			final Collection<Integer> demographicNumbers,
			final String midwifeNumber
	) {
		val sql = "select x from DemographicExt x where x.demographicNo IN (?1) "
				+ "and x.key = ?2 "
				+ "and x.value = ?3";
		val query = entityManager.createQuery(sql);
		query.setParameter(1, demographicNumbers);
		query.setParameter(2, key.getKey());
		query.setParameter(3, midwifeNumber);

		@SuppressWarnings("unchecked")
		List<DemographicExt> results = query.getResultList();
		return results;
	}

	public List<DemographicExt> getMultipleMidwifeForDemographicNumbersByProviderNumber(
			final Collection<Integer> demographicNumbers,
			final String midwifeNumber
	) {
		return getMultipleDemographicExtKeyForDemographicNumbersByProviderNumber(
				DemographicExtKey.MIDWIFE,
				demographicNumbers,
				midwifeNumber
		);
	}

	public List<DemographicExt> getMultipleNurseForDemographicNumbersByProviderNumber(
			final Collection<Integer> demographicNumbers,
			final String nurseNumber
	) {
		return getMultipleDemographicExtKeyForDemographicNumbersByProviderNumber(
				DemographicExtKey.NURSE,
				demographicNumbers,
				nurseNumber
		);
	}

	public List<DemographicExt> getMultipleResidentForDemographicNumbersByProviderNumber(
			final Collection<Integer> demographicNumbers,
			final String residentNumber
	) {
		return getMultipleDemographicExtKeyForDemographicNumbersByProviderNumber(
				DemographicExtKey.RESIDENT,
				demographicNumbers,
				residentNumber
		);
	}

	public List<Integer> getDemographicNumbersByDemographicExtKeyAndProviderNumberAndDemographicLastNameRegex(
			final DemographicExtKey key,
			final String providerNumber,
			final String lastNameRegex
	) {
		val sql = "select d.demographic_no from demographic d, demographicExt e "
				+ "where e.key_val = ? "
				+ "and e.value = ? "
				+ "and d.demographic_no = e.demographic_no "
				+ "and d.last_name REGEXP ?";
		val query = entityManager.createNativeQuery(sql);
		query.setParameter(1, key.getKey());
		query.setParameter(2, providerNumber);
		query.setParameter(3, lastNameRegex);

		@SuppressWarnings("unchecked")
		List<Integer> results = query.getResultList();
		if(results.size() > 0) {
			return results;
		}
		return null;
	}

	public List<Integer> getDemographicNumbersByMidwifeNumberAndDemographicLastNameRegex(
			final String midwifeNumber,
			final String lastNameRegex
	) {
		return getDemographicNumbersByDemographicExtKeyAndProviderNumberAndDemographicLastNameRegex(
				DemographicExtKey.MIDWIFE,
				midwifeNumber,
				lastNameRegex
		);
	}

	public List<Integer> getDemographicNumbersByNurseNumberAndDemographicLastNameRegex(
			final String nurseNumber,
			final String lastNameRegex
	) {
		return getDemographicNumbersByDemographicExtKeyAndProviderNumberAndDemographicLastNameRegex(
				DemographicExtKey.NURSE,
				nurseNumber,
				lastNameRegex
		);
	}

	public List<Integer> getDemographicNumbersByResidentNumberAndDemographicLastNameRegex(
			final String residentNumber,
			final String lastNameRegex
	) {
		return getDemographicNumbersByDemographicExtKeyAndProviderNumberAndDemographicLastNameRegex(
				DemographicExtKey.RESIDENT,
				residentNumber,
				lastNameRegex
		);
	}

	public String getValueForDemoKey(Integer demographicNumber, DemographicExtKey key) {
		var ext = this.getDemographicExt(demographicNumber, key.getKey());
		return ext != null
				? ext.getValue()
				: null;
	}

	public String getValueForDemoKey(final Integer demographicNumber, final String key) {
		return getValueForDemoKey(demographicNumber, key, false);
	}

	public String getValueForDemoKey(
			final Integer demographicNumber,
			final String key,
			final boolean skipValidation
	) {
		var ext = this.getDemographicExt(demographicNumber, key, skipValidation);
		return ext != null
				? ext.getValue()
				: null;
	}

	/* PRIVATE HELPERS */

	private void createAndArchiveDemographicExt(
			String providerNumber,
			Integer demographicNumber,
			String key,
			String value
	) {
		var extension = new DemographicExt();
		extension.setProviderNo(providerNumber);
		extension.setDemographicNo(demographicNumber);
		extension.setKey(key);
		extension.setValue(value);
		extension.setDateCreated(new Date());
		super.persist(extension);
		DemographicManager.archiveDemographicExt(extension);
	}

	private Query queryDemographicExtForDemographic(Integer demographicNumber) {
		validateDemographicNumber(demographicNumber);
		var query = entityManager.createQuery(
				"SELECT d from DemographicExt d where d.demographicNo=? order by d.dateCreated"
		);
		query.setParameter(1, demographicNumber);
		return query;
	}

	private Query queryDemographicExtForKey(
			final Integer demographicNumber,
			final String key,
			final boolean skipValidation
	) {
		if (!skipValidation) {
			validateExtensionData(demographicNumber, key);
		}
		var query = entityManager.createQuery(
				"SELECT d from DemographicExt d where d.demographicNo=? and d.key = ? order by d.dateCreated DESC"
		);
		query.setParameter(1, demographicNumber);
		query.setParameter(2, key);
		return query;
	}

	private Query queryDemographicExtForKeyAndProviderNo(
			Integer demographicNumber,
			String key,
			String providerNumber
	) {
		validateExtensionData(demographicNumber, key);
		var query = entityManager.createQuery(
				"SELECT d from DemographicExt d where d.demographicNo=? and d.key = ? and d.providerNo = ? order by d.dateCreated DESC"
		);
		query.setParameter(1, demographicNumber);
		query.setParameter(2, key);
		query.setParameter(3, providerNumber);
		return query;
	}

	private Query queryDemographicExtForKeyAndValue(String key, String value) {
		var query = entityManager.createQuery(
				"SELECT d from DemographicExt d where d.key = ? and d.value=? order by d.dateCreated DESC"
		);
		query.setParameter(1, key);
		query.setParameter(2, value);
		return query;
	}

	private void validateNumber(Integer number) {
		if (number == null || number <= 0) {
			throw new IllegalArgumentException();
		}
	}

	private void validateExtensionData(Integer demographicNumber, String key) {
		validateDemographicNumber(demographicNumber);
		validateKey(key);
	}
	private void validateDemographicNumber(Integer demographicNumber) {
		validateNumber(demographicNumber);
	}

	private void validateKey(String key) {
		if (key == null || key.length() <= 0) {
			throw new IllegalArgumentException();
		}
	}
}
