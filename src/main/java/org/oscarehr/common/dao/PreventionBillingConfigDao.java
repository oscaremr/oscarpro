/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
 
package org.oscarehr.common.dao;

import java.util.List;
import javax.persistence.NoResultException;
import lombok.val;
import javax.annotation.Nullable;
import org.oscarehr.common.model.PreventionBillingConfig;
import org.oscarehr.common.model.Property;
import org.springframework.stereotype.Repository;

@Repository
public class PreventionBillingConfigDao extends AbstractDao<PreventionBillingConfig> {

  public PreventionBillingConfigDao() {
    super(PreventionBillingConfig.class);
  }

  @SuppressWarnings("unchecked")
  public List<PreventionBillingConfig> findAll() {
    val query = entityManager.createQuery("SELECT x FROM " + modelClass.getSimpleName() + " x order by preventionType");
    return query.getResultList();
  }

  @SuppressWarnings("unchecked")
  public List<PreventionBillingConfig> findAllActive() {
    val query = entityManager.createQuery("SELECT x FROM " + modelClass.getSimpleName() + " x where billingType != 'none' and billingServiceCodeAndUnit IS NOT NULL order by preventionType");
    return query.getResultList();
  }

  @SuppressWarnings("unchecked")
  public List<PreventionBillingConfig> findAllInactive() {
    val query = entityManager.createQuery("SELECT x FROM " + modelClass.getSimpleName() + " x where billingType = 'none' and billingServiceCodeAndUnit ='' order by preventionType");
    return query.getResultList();
  }

  @Nullable
  public PreventionBillingConfig getByPreventionType(final String prevention) {
    val query = entityManager.createQuery("SELECT x FROM " + modelClass.getSimpleName() + " x where preventionType = ?");
    query.setParameter(1, prevention);
    try {
      return (PreventionBillingConfig) query.getSingleResult();
    } catch (NoResultException ex) {
      return null;
    }
  }

}
