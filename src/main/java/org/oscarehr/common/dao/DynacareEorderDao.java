/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import javax.persistence.Query;
import org.oscarehr.common.model.DynacareEorder;
import org.springframework.stereotype.Repository;

@Repository
public class DynacareEorderDao extends AbstractDao<DynacareEorder> {

  public DynacareEorderDao() {
    super(DynacareEorder.class);
  }

  /**
   * Finds an eOrder based on internal id.
   *
   * @param eOrderInternalId internal id of eOrder.
   * @return Returns the eOrder with the specified internal id.
   */
  public DynacareEorder findById(Integer eOrderInternalId) {
    Query query =
        entityManager.createQuery(
            "select x from " + modelClass.getSimpleName() + " x where x.fid=?1");
    query.setParameter(1, eOrderInternalId);

    return this.getSingleResultOrNull(query);
  }

  /**
   * Finds an eOrder based on internal id.
   *
   * @param eOrderExternalId external id of eOrder.
   * @return Returns the eOrder with the specified external id.
   */
  public DynacareEorder findByExtId(Integer eOrderExternalId) {
    Query query =
        entityManager.createQuery(
            "select x from " + modelClass.getSimpleName() + " x where x.externalOrderId=?1");
    query.setParameter(1, eOrderExternalId);

    return this.getSingleResultOrNull(query);
  }

  public DynacareEorder findByExtEFormDataId(Integer eFormDataId) {
    Query query =
        entityManager.createQuery(
            "select x from " + modelClass.getSimpleName() + " x where x.eFormDataId=?1");
    query.setParameter(1, eFormDataId);

    return this.getSingleResultOrNull(query);
  }
}
