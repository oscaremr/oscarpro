/**
 * Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.oscarehr.common.dao;

import java.util.List;
import javax.persistence.Query;
import org.oscarehr.common.model.BillingAutoRulesExt;
import org.springframework.stereotype.Repository;


@Repository
@SuppressWarnings("unchecked")
public class BillingAutoRulesExtDao extends AbstractDao<BillingAutoRulesExt> {

	public BillingAutoRulesExtDao() {
		super(BillingAutoRulesExt.class);
	}

	public List<BillingAutoRulesExt> findActive() {
		Query q = entityManager.createQuery("select x from BillingAutoRulesExt x where x.status <> ?");
		q.setParameter(1, "1");

		List<BillingAutoRulesExt> results = q.getResultList();

		return results;
	}

	public List<BillingAutoRulesExt> findActiveValuesByExtKey(String ext_key, Integer rule_id) {
		Query q = entityManager.createQuery("select x.ext_value from BillingAutoRulesExt x where x.ext_key=? and x.rule_id=? and x.status=?");
		q.setParameter(1, ext_key);
		q.setParameter(2, rule_id);
		q.setParameter(3, 1);

		List<BillingAutoRulesExt> results = q.getResultList();

		return results;
	}

}
