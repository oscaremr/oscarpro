/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.oscarehr.common.model.ServiceClient;
import org.springframework.stereotype.Repository;

@Repository
public class ServiceClientDao extends AbstractDao<ServiceClient>{

	public ServiceClientDao() {
		super(ServiceClient.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<ServiceClient> findAll() {
		Query query = createQuery("x", null);
		return query.getResultList();
	}
	
	public ServiceClient findByName(String name) {
		Query query = entityManager.createQuery("SELECT x FROM ServiceClient x WHERE x.name=?");
		query.setParameter(1,name);
		
		return this.getSingleResultOrNull(query);
	}

	public List<ServiceClient> findByNamesLike(List<String> names) {
		if (names == null || names.size() == 0) { return new ArrayList<>(); }

		StringBuilder builder = new StringBuilder();
		builder.append("SELECT x FROM ServiceClient x WHERE ");

		Map<String, String> params = new HashMap<>();
		for (int i = 0; i < names.size(); i ++) {
			String value = "%" + names.get(i) + "%";
			params.put("name" + i, value);
			if (i > 0) {
				builder.append(" OR ");
			}
			builder.append(" name like :name");
			builder.append(i);
		}

		Query query = entityManager.createQuery(builder.toString());
		for(Map.Entry<String, String> entry : params.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		return query.getResultList();
	}
	
	public ServiceClient findByKey(String key) {
		Query query = entityManager.createQuery("SELECT x FROM ServiceClient x WHERE x.key=?");
		query.setParameter(1,key);
		
		return this.getSingleResultOrNull(query);
	}
}
