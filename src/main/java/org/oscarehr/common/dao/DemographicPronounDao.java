/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import java.util.List;
import javax.persistence.Query;
import lombok.val;
import org.oscarehr.common.model.DemographicGender;
import org.oscarehr.common.model.DemographicPronoun;

public class DemographicPronounDao extends AbstractDao<DemographicPronoun> {
	
  public DemographicPronounDao() {
    super(DemographicPronoun.class);
  }
	
  public List <DemographicPronoun>findAllDemographicPronoun() {
    val sql = "SELECT p FROM DemographicPronoun p WHERE p.deleted = false";
    return entityManager.createQuery(sql).getResultList();
  }
	
  public List<DemographicPronoun> findbyId(Integer id) {
    val sql = "SELECT p FROM DemographicPronoun p WHERE id=:id";
    val query = entityManager.createQuery(sql);
    query.setParameter("id", id);
    return query.getResultList();
  }

  public int deletePronounById(Integer id) {
    val sql = "UPDATE DemographicPronoun SET deleted = true WHERE id=:id AND editable = true";
    val query = entityManager.createQuery(sql);
    query.setParameter("id", id);
    return query.executeUpdate();
  }

  public int updatePronoun(DemographicPronoun demographicPronoun) {
    val sql = "UPDATE DemographicPronoun SET value=:pronoun WHERE id=:id AND editable = true";
    val query = entityManager.createQuery(sql);
    query.setParameter("pronoun", demographicPronoun.getValue());
    query.setParameter("id", demographicPronoun.getId());
    return query.executeUpdate();
  }

}
