/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import java.util.List;

import lombok.val;
import org.oscarehr.common.model.OnAccountService;
import org.springframework.stereotype.Repository;

@Repository
public class OnAccountServiceDao extends AbstractDao<OnAccountService> {

    public OnAccountServiceDao() {
        super(OnAccountService.class);
    }

    public List<OnAccountService> findByInvoiceNo(final Integer invoiceNo) {
        val query = entityManager.createQuery("SELECT entity FROM OnAccountService entity " +
                "WHERE entity.invoiceNo = :invoiceNo");
        query.setParameter("invoiceNo", invoiceNo);
        return query.getResultList();
    }

    public void setBillVoidByInvoiceNumber(final Integer invoiceNumber) {
        val query = entityManager.createQuery("UPDATE OnAccountService entity " +
                "SET entity.status = 'Void' " +
                "WHERE entity.invoiceNo = :invoiceNumber");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.executeUpdate();
    }

    public void deleteByInvoiceNumber(final Integer invoiceNumber) {
        val query = entityManager.createQuery("DELETE FROM OnAccountService entity " +
                "WHERE entity.invoiceNo = :invoiceNumber");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.executeUpdate();
    }
}