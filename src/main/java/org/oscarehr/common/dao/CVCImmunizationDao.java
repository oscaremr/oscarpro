/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;
import javax.persistence.Query;
import org.oscarehr.common.model.CVCImmunization;
import org.springframework.stereotype.Repository;

@Repository
public class CVCImmunizationDao extends AbstractDao<CVCImmunization> {

  public CVCImmunizationDao() {
    super(CVCImmunization.class);
  }

  public void removeAll() {
    Query query = entityManager.createQuery("DELETE FROM CVCImmunizationName");
    query.executeUpdate();
    query = entityManager.createQuery("DELETE FROM CVCImmunization");
    query.executeUpdate();
  }

  @SuppressWarnings("unchecked")
  public List<CVCImmunization> findAllGeneric() {
    Query query = entityManager.createQuery(
        "SELECT x FROM CVCImmunization x WHERE x.generic = :generic");
    query.setParameter("generic", true);
    return (List<CVCImmunization>) query.getResultList();
  }

  @SuppressWarnings("unchecked")
  public List<CVCImmunization> findByParent(String conceptCodeId) {
    Query query = entityManager.createQuery(
        "SELECT x FROM CVCImmunization x WHERE x.parentConceptId = :parentConceptId");
    query.setParameter("parentConceptId", conceptCodeId);
    return (List<CVCImmunization>) query.getResultList();
  }

  @Nullable
  public CVCImmunization findBySnomedConceptId(String conceptCodeId) {
    Query query = entityManager.createQuery(
        "SELECT x FROM CVCImmunization x WHERE x.snomedConceptId = :snomedConceptId");
    query.setParameter("snomedConceptId", conceptCodeId);
    query.setMaxResults(1);
    @SuppressWarnings("unchecked")
    List<CVCImmunization> results = query.getResultList();
    return !results.isEmpty() ? results.get(0) : null;
  }

  @SuppressWarnings("unchecked")
  public List<CVCImmunization> query(String term, boolean includeGenerics, boolean includeBrands) {
    if (!includeBrands && !includeGenerics) {
      return new ArrayList<>();
    }
    String segment = "";
    if (includeBrands && !includeGenerics) {
      segment = " AND generic=0 ";
    }
    if (!includeBrands) {
      segment = " AND generic=1 ";
    }
    Query query = entityManager.createQuery(
        "SELECT x FROM CVCImmunization x JOIN x.names y WHERE y.value like :term" + segment);
    query.setParameter("term", "%" + term + "%");
    return (List<CVCImmunization>) query.getResultList();
  }
}
