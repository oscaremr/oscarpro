/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import java.util.List;
import javax.persistence.Query;
import org.oscarehr.common.model.ReferringMdRequiredCodes;
import org.springframework.stereotype.Repository;

@Repository
public class ReferringMdRequiredCodesDao extends AbstractDao<ReferringMdRequiredCodes>{

  public ReferringMdRequiredCodesDao() {
    super(ReferringMdRequiredCodes.class);
  }
  
  public List<ReferringMdRequiredCodes> getAllMdRequiredCodes() {
    Query query = entityManager.createQuery("FROM " + modelClass.getSimpleName());
    @SuppressWarnings("unchecked")
    List<ReferringMdRequiredCodes> results = query.getResultList();
    return results;
  }
  public ReferringMdRequiredCodes findMdRequiredCodeByCode(String code) {
    Query query = entityManager.createQuery("FROM " + modelClass.getSimpleName() + " x where x.code = ?1 order by x.code");
    query.setParameter(1, code);
    return (ReferringMdRequiredCodes) query.getSingleResult();
  }

}