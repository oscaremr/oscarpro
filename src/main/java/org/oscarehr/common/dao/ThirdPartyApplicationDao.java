/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import java.util.List;
import javax.persistence.Query;
import org.oscarehr.common.model.EmrContextEnum;
import org.oscarehr.common.model.ThirdPartyApplication;
import org.springframework.stereotype.Repository;

@Repository
public class ThirdPartyApplicationDao extends AbstractDao<ThirdPartyApplication> {

  public ThirdPartyApplicationDao() {
    super(ThirdPartyApplication.class);
	}


  public List<ThirdPartyApplication> findByEmrContext(EmrContextEnum context) {
    Query query = entityManager.createNativeQuery(
            "select a.* from ThirdPartyApplication a JOIN ThirdPartyAppEmrContext c where c.applicationNo = a.applicationNo and c.emrContext = ?1",
            ThirdPartyApplication.class).setParameter(1, context.toString());
    return query.getResultList();
	}

    
	
	
}
