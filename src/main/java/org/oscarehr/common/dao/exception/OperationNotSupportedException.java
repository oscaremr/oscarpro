package org.oscarehr.common.dao.exception;

public class OperationNotSupportedException extends Exception {
  public OperationNotSupportedException() {
    this(null, null);
  }

  public OperationNotSupportedException(String message) {
    this(message, null);
  }

  public OperationNotSupportedException(Throwable cause) {
    this(cause != null ? cause.getMessage() : null, cause);
  }

  public OperationNotSupportedException(String message, Throwable cause) {
    super(message);
    if (cause != null) super.initCause(cause);
  }
}
