/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.model.EmailLog;

import javax.persistence.Query;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class EmailLogDao extends AbstractDao<EmailLog> {

	public static final int MAX_RESULTS = 1000;

	public EmailLogDao() {
		super(EmailLog.class);
	}


	public List<EmailLog> findAll() {
		Query query = entityManager.createQuery("FROM " + modelClass.getSimpleName());
		return query.getResultList();
	}

	public List<EmailLog> findAllByQueryParams(String startDate, String endDate, String status,
												String emailType, Integer recipientId) {

		List<EmailLog> list;
		Map<String, Object> params = new HashMap<String, Object>();
		String queryString = "select * from email_log ";

		if (recipientId != null && recipientId > 0) {
			queryString += " where recipientId = :recipientId ";
			params.put("recipientId", recipientId);
		} else {
			queryString += "where 1=1 ";
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		if (StringUtils.isNotEmpty(startDate)) {
			queryString += "and sentDate >= :startDate ";
			Date date = null;
			try {
				// set the time to the start of the day
				Calendar cal = Calendar.getInstance();
				cal.setTime(sdf.parse(startDate));
				cal.set(Calendar.HOUR_OF_DAY,0);
				cal.set(Calendar.MINUTE,0);
				cal.set(Calendar.SECOND,0);
				cal.set(Calendar.MILLISECOND,0);
				date = cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			params.put("startDate", date);
		}
		if (StringUtils.isNotEmpty(endDate)) {
			queryString += "and sentDate <= :endDate ";
			Date date = null;
			try {
				// set the time to the end of the day
				Calendar cal = Calendar.getInstance();
				cal.setTime(sdf.parse(endDate));
				cal.set(Calendar.HOUR_OF_DAY,23);
				cal.set(Calendar.MINUTE,59);
				cal.set(Calendar.SECOND,59);
				cal.set(Calendar.MILLISECOND,9);
				date = cal.getTime();
			} catch (ParseException e) {
				e.printStackTrace();
			}
			params.put("endDate", date);
		}
		if (StringUtils.isNotEmpty(status)) {
			queryString += "and status = :status ";
			params.put("status", status);
		}
		if (StringUtils.isNotEmpty(emailType)) {
			queryString += "and emailType = :emailType ";
			params.put("emailType", emailType);
		}

		Query query = entityManager.createNativeQuery(queryString, modelClass);
		query.setMaxResults(MAX_RESULTS);

		for(Map.Entry<String, Object> o : params.entrySet()) {
			query.setParameter(o.getKey(), o.getValue());
		}

		list = query.getResultList();

		return list;
	}
}