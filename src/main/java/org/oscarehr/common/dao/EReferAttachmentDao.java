package org.oscarehr.common.dao;

import java.util.List;
import javax.annotation.Nullable;
import javax.persistence.NoResultException;
import lombok.val;
import org.hibernate.Hibernate;
import org.oscarehr.common.model.EReferAttachment;
import org.oscarehr.common.model.OceanWorkflowTypeEnum;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.Calendar;

@Repository
public class EReferAttachmentDao extends AbstractDao<EReferAttachment> {
  public EReferAttachmentDao() {
    super(EReferAttachment.class);
  }

  @Nullable
  public EReferAttachment getLatestCreatedByDemographic(final Integer demographicNo) {
    return getLatestCreatedByDemographic(demographicNo, null, null);
  }
  
  public List<EReferAttachment> getAttachmentsByType(
        final Integer demographicNo, final OceanWorkflowTypeEnum type) {
    
    String sql = "SELECT e FROM " + modelClass.getSimpleName() + " e WHERE e.archived = FALSE "
        + "AND e.demographicNo = :demographicNo";
    if (type != null) {
      sql += " AND e.type = :type";
    }
  
    Query query = entityManager.createQuery(sql);
    query.setParameter("demographicNo", demographicNo);
    if (type != null) {
      query.setParameter("type", type.name());
    }

    val eReferAttachments = query.getResultList();
    for (Object eReferAttachment : eReferAttachments) {
      Hibernate.initialize(((EReferAttachment) eReferAttachment).getAttachments());
    }
    return eReferAttachments;
  }

  @Nullable
  public EReferAttachment getLatestCreatedByDemographic(
      final Integer demographicNo, final OceanWorkflowTypeEnum type, final Integer expiryHours) {
    Calendar calendar = Calendar.getInstance();
    if (expiryHours != null) {
      calendar.add(Calendar.HOUR_OF_DAY, -1 * expiryHours);
    } else {
      calendar.add(Calendar.HOUR_OF_DAY, -1);
    }

    String sql = "SELECT e FROM " + modelClass.getSimpleName() + " e WHERE e.archived = FALSE "
        + "AND e.demographicNo = :demographicNo AND e.created > :expiry";
    if (type != null) {
      sql += " AND e.type = :type";
    }
    sql += " ORDER BY e.created DESC";

    Query query = entityManager.createQuery(sql);
    query.setParameter("demographicNo", demographicNo);
    query.setParameter("expiry", calendar.getTime());
    if (type != null) {
      query.setParameter("type", type.name());
    }

    query.setMaxResults(1);

    try {
      EReferAttachment eReferAttachment = (EReferAttachment) query.getSingleResult();
      if (eReferAttachment != null) {
        Hibernate.initialize(eReferAttachment.getAttachments());
        Hibernate.initialize(eReferAttachment.getAttachmentManagerData());
      }
      return eReferAttachment;
    } catch (NoResultException e) {
      return null;
    }
  }
}
