package org.oscarehr.common.dao;

import org.oscarehr.common.model.MSPFacilityMappingCode;
import org.oscarehr.common.model.OscarLog;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public class MSPFacilityMappingCodesDao extends AbstractDao<MSPFacilityMappingCode>
{
    public MSPFacilityMappingCodesDao() { super(MSPFacilityMappingCode.class); }

    public List<MSPFacilityMappingCode> findAll () {
        Query query = entityManager.createQuery("FROM MSPFacilityMappingCode b ORDER BY b.id.clinicId ASC, b.id.billingCode ASC");
        return query.getResultList();
    }
    
    public MSPFacilityMappingCode find (Integer clinicId, String billingCode) {
        Query query = entityManager.createQuery("FROM MSPFacilityMappingCode b WHERE b.id.clinicId = :clinicId AND b.id.billingCode = :billingCode");
        query.setParameter("clinicId", clinicId);
        query.setParameter("billingCode", billingCode);
        try {
            return (MSPFacilityMappingCode) query.getSingleResult();
        } catch (Exception e) {
            
        }
        return null;
    }
    
    public List<MSPFacilityMappingCode> findByAllClinicIdAndBillingNoList (Integer clinicId, List<String> billingNoList) {
        if (billingNoList == null || billingNoList.isEmpty()) {
            return new ArrayList<MSPFacilityMappingCode>();
        }
        StringBuilder sql = new StringBuilder("FROM MSPFacilityMappingCode b WHERE b.id.clinicId = :clinicId AND (");
        for (int i = 0; i < billingNoList.size(); i++) {
            sql.append(" b.id.billingCode = ?").append(i);
            if (i < billingNoList.size()-1) {
                sql.append(" OR");
            }
        }
        sql.append(")");
        Query query = entityManager.createQuery(sql.toString());
        query.setParameter("clinicId", clinicId);
        for (int i = 0; i < billingNoList.size(); i++) {
            query.setParameter(i, billingNoList.get(i));
        }
        return (List<MSPFacilityMappingCode>) query.getResultList();
    }
    
    public void removeCode(Integer clinicId, String billingCode, LoggedInInfo loggedInInfo) {
        try {
            MSPFacilityMappingCode result = this.find(clinicId, billingCode);

            OscarLogDao oscarLogDao = SpringUtils.getBean(OscarLogDao.class);
            OscarLog oscarLog = new OscarLog();
            oscarLog.setAction("remove");
            oscarLog.setContent("MSPFacilityMapping");
            oscarLog.setContentId("ClinicId=" + result.getClinicId() + "; BillingCode=" + result.getBillingCode());
            oscarLog.setIp(loggedInInfo.getIp());
            oscarLog.setProviderNo(loggedInInfo.getLoggedInProviderNo());
            oscarLog.setSecurityId(loggedInInfo.getLoggedInSecurity().getSecurityNo());
            oscarLogDao.persist(oscarLog);
            
            entityManager.remove(entityManager.contains(result) ? result : entityManager.merge(result));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
