/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import java.util.List;

import lombok.val;
import org.oscarehr.common.model.OnAccountRefund;
import org.springframework.stereotype.Repository;

@Repository
public class OnAccountRefundDao extends AbstractDao<OnAccountRefund> {

    public OnAccountRefundDao() {
        super(OnAccountRefund.class);
    }

    public List<OnAccountRefund> findByDemographicNumber(final Integer demographicNumber) {
        val query = entityManager.createQuery("SELECT entity FROM OnAccountRefund entity " +
                "WHERE entity.demographicNo = :demographicNumber");
        query.setParameter("demographicNumber", demographicNumber);
        return query.getResultList();
    }

    public List<OnAccountRefund> findByDemographicAndInvoiceNumber(final Integer demographicNumber, final Integer invoiceNumber) {
        val query = entityManager.createQuery("SELECT entity FROM OnAccountRefund entity " +
                "WHERE entity.demographicNo = :demographicNumber AND entity.id = :invoiceNumber");
        query.setParameter("demographicNumber", demographicNumber);
        query.setParameter("invoiceNumber", invoiceNumber);
        return query.getResultList();
    }

    public Double getTotalRefundsByDemographicNumber(final Integer demographicNumber) {
        val query = entityManager.createQuery("SELECT SUM(totalRefund) FROM OnAccountRefund entity " +
                "WHERE entity.demographicNo = :demographicNumber");
        query.setParameter("demographicNumber", demographicNumber);
        val result = (Double) query.getSingleResult();
        return result == null ? 0.0 : result;
    }

    public Double getTotalRefundsPendingByDemographicNumber(final Integer demographicNumber) {
        val query = entityManager.createQuery("SELECT SUM(totalRefund) FROM OnAccountRefund entity " +
                "WHERE entity.demographicNo = :demographicNumber and refundStatus = 'Pending'");
        query.setParameter("demographicNumber", demographicNumber);
        val result = (Double) query.getSingleResult();
        return result == null ? 0.0 : result;
    }

    public void setRefundVoidByInvoiceNumber(final Integer invoiceNumber) {
        val query = entityManager.createQuery("UPDATE OnAccountRefund entity " +
                "SET entity.refundStatus = 'Void', entity.totalRefund = 0 " +
                "WHERE entity.id = :invoiceNumber");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.executeUpdate();
    }
}