/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import java.util.List;
import org.oscarehr.common.model.DemographicFirstNation;
import lombok.val;

public class DemographicFirstNationDao extends AbstractDao<DemographicFirstNation> {
  public DemographicFirstNationDao() {
    super(DemographicFirstNation.class);
  }

  public List<DemographicFirstNation> findAllDemographicFirstNation() {
    val sql = "SELECT f FROM DemographicFirstNation f WHERE f.deleted = false";
    return entityManager.createQuery(sql).getResultList();
  }
  
  public List<DemographicFirstNation> findFirstNationById(Integer id) {
    val sql = "SELECT f FROM DemographicFirstNation f WHERE id = :id AND deleted = false";
    val query = entityManager.createQuery(sql);
    query.setParameter("id", id);
    return query.getResultList();
  }
  
  public int deleteBandNameById(Integer id) {
    val sql = "UPDATE DemographicFirstNation SET deleted = true WHERE id = :id AND editable = true";
    val query = entityManager.createQuery(sql);
    query.setParameter("id", id);
    return query.executeUpdate();
  }
		  
  public int updateFirstNation(DemographicFirstNation demographicFirstNation) {
    val sql = "UPDATE DemographicFirstNation "
        + "SET firstNationCommunity = :fNationCom "
        + "WHERE id = :id AND editable = true";
    val query = entityManager.createQuery(sql);
    query.setParameter("fNationCom", demographicFirstNation.getFirstNationCommunity());
    query.setParameter("id", demographicFirstNation.getId());
    return query.executeUpdate();
  }
}
