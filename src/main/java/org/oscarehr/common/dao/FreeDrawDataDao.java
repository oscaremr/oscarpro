/*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;
import org.oscarehr.common.model.FreeDrawData;
import org.springframework.stereotype.Repository;
import oscar.oscarEncounter.data.FreeDrawMetaData;

@Repository
public class FreeDrawDataDao extends AbstractDao<FreeDrawData> {
  public FreeDrawDataDao() {
    super(FreeDrawData.class);
  }

  @SuppressWarnings("unchecked")
  public List<FreeDrawMetaData> getDrawingListByDemoNo(int demoNo) {
    Query q = entityManager.createQuery(
        "select max(f.id) from FreeDrawData f where f.demographicNo = :demoNo and f.status = 0 group by f.name order by f.updateTime desc");
    q.setParameter("demoNo", demoNo);
    List<Integer> freeDraws = q.getResultList();
    List<FreeDrawMetaData> freeDrawDatas = new ArrayList<FreeDrawMetaData>();
    for (Integer i : freeDraws) {
      FreeDrawData f = find(i);

      FreeDrawMetaData free = new FreeDrawMetaData(f.getId(), f.getProviderNo(), f.getName(),
          f.getUpdateTime(), f.getDataType(), f.getThumbnailId());

      freeDrawDatas.add(free);
    }

    return freeDrawDatas;
  }

  @SuppressWarnings("unchecked")
  public List<FreeDrawMetaData> getAllDrawingListByDemo(int demoNo) {
    Query q = entityManager
        .createQuery("select new oscar.oscarEncounter.data.FreeDrawMetaData(f.id, f.providerNo, "
            + "f.name, f.updateTime, f.dataType, f.thumbnailId) from FreeDrawData f"
            + " where f.demographicNo = :demoNo order by f.updateTime desc");
    q.setParameter("demoNo", demoNo);
    return q.getResultList();
  }
}
