/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import lombok.val;
import org.oscarehr.common.model.OnAccountPayment;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OnAccountPaymentsDao extends AbstractDao<OnAccountPayment> {

    public OnAccountPaymentsDao() {
        super(OnAccountPayment.class);
    }

    public List<OnAccountPayment> findByInvoiceNo(Integer invoiceNo) {
        val query = entityManager.createQuery("SELECT entity FROM OnAccountPayment entity " +
                "WHERE entity.invoiceNumber = :invoiceNo");
        query.setParameter("invoiceNo", invoiceNo);
        return query.getResultList();
    }

    public void deleteTempPaymentByInvoiceNumber(final Integer invoiceNumber) {
        val query = entityManager.createQuery(
                "DELETE FROM OnAccountPayment entity WHERE entity.invoiceNumber = :invoiceNumber " +
                "AND entity.status = 'temp'");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.executeUpdate();
    }
    public void updateStatusByInvoiceNumber(final Integer invoiceNumber,
                                            final String newStatus) {
        val query = entityManager.createQuery(
                "UPDATE OnAccountPayment entity SET entity.status = :newStatus " +
                        "WHERE entity.invoiceNumber = :invoiceNumber ");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.setParameter("newStatus", newStatus);
        query.executeUpdate();
    }
    public void updateStatusByInvoiceNumberAndStatusIn(final Integer invoiceNumber,
                                                       final List<String> statuses,
                                                       final String newStatus) {
        val query = entityManager.createQuery(
                "UPDATE OnAccountPayment entity SET entity.status = :newStatus " +
                "WHERE entity.invoiceNumber = :invoiceNumber " +
                "AND entity.status IN (:statuses)");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.setParameter("statuses", statuses);
        query.setParameter("newStatus", newStatus);
        query.executeUpdate();
    }

    public void deleteByInvoiceNumber(Integer invoiceNumber) {
        val query = entityManager.createQuery("DELETE FROM OnAccountPayment " +
                "WHERE invoiceNumber = :invoiceNumber");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.executeUpdate();
    }

}