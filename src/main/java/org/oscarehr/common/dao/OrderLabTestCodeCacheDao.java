/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import java.util.List;
import javax.persistence.Query;
import lombok.val;
import org.oscarehr.common.model.OrderLabTestCodeCache;
import org.springframework.stereotype.Repository;

@Repository
public class OrderLabTestCodeCacheDao extends AbstractDao<OrderLabTestCodeCache> {

	public OrderLabTestCodeCacheDao() {
		super(OrderLabTestCodeCache.class);
	}
	
	public int removeAllTestCodes() {
		val query = entityManager.createQuery(
				"DELETE from OrderLabTestCodeCache x WHERE x.updateable = 1");
    	return query.executeUpdate();
    }
	  
	public OrderLabTestCodeCache findByTestCode(String testCode)
    {
		Query query = entityManager
				.createQuery("select x from " + modelClass.getSimpleName() + " x where x.labTestCode=?1");
		query.setParameter(1, testCode);
    	
    	return this.getSingleResultOrNull(query);
    }

	@SuppressWarnings("unchecked")
	public List<OrderLabTestCodeCache> findAllTestCode() {
		val query = entityManager.createQuery("select x from " + modelClass.getSimpleName() + " x ");
		return query.getResultList();
    }

    public OrderLabTestCodeCache findByKeyName(String keyName) {
    	Query query = entityManager.createQuery("select x from " + modelClass.getSimpleName() + " x where x.keyName=?1");
    	query.setParameter(1, keyName);
    	return this.getSingleResultOrNull(query);
    }
    
    @SuppressWarnings("unchecked")
    public List<OrderLabTestCodeCache> findAllTestCodeByProvince(String province) {
      Query query = entityManager.createQuery("select x from " + modelClass.getSimpleName() + " x where x.province IS NULL OR x.province=?1");
      query.setParameter(1, province);
      return query.getResultList();
    }

    public List<OrderLabTestCodeCache> findTestCodeByProvince(String keyName, String province) {
      val query = entityManager.createQuery("select x from " + modelClass.getSimpleName()
					+ " x where x.keyName=?1 AND  x.province=?2");
      query.setParameter(1, keyName);
      query.setParameter(2, province);
      return query.getResultList();
    }

    public List<OrderLabTestCodeCache> findTestCodeByProvinceNull(String keyName) {
      val query = entityManager.createQuery("select x from " + modelClass.getSimpleName()
					+ " x where x.keyName=?1 AND  x.province IS NULL");
      query.setParameter(1, keyName);
      return query.getResultList();
    }

  public OrderLabTestCodeCache findByTestCodeAndProvince(String testCode,
      String province) {
    Query query = entityManager.createQuery(
        "select x from " + modelClass.getSimpleName() + " x "
            + "where x.labTestCode = ?1 and x.province= ?2 ");
    query.setParameter(1, testCode);
    query.setParameter(2, province);

    return this.getSingleResultOrNull(query);
  }
}
