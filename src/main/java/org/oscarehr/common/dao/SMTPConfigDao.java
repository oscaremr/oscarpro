/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import org.oscarehr.common.model.SMTPConfig;

import javax.persistence.Query;

public class SMTPConfigDao extends AbstractDao<SMTPConfig> {

    public SMTPConfigDao() {
        super(SMTPConfig.class);
    }


    public SMTPConfig getEmailConfig() {

        Query query = entityManager.createQuery("select ec from " + modelClass.getSimpleName() + " ec");

        return getSingleResultOrNull(query);
    }
}