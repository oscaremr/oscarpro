/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import java.util.List;

import lombok.val;
import org.oscarehr.common.model.OnAccountRefundDetail;
import org.springframework.stereotype.Repository;

@Repository
public class OnAccountRefundDetailDao extends AbstractDao<OnAccountRefundDetail> {

    public OnAccountRefundDetailDao() {
        super(OnAccountRefundDetail.class);
    }

    public List<OnAccountRefundDetail> findByInvoiceNumber(final Integer invoiceNumber) {
        val query = entityManager.createQuery("SELECT entity FROM OnAccountRefundDetail entity " +
                "WHERE entity.invoiceNumber = :invoiceNumber");
        query.setParameter("invoiceNumber", invoiceNumber);
        return query.getResultList();
    }

    public void deleteTempStatusByInvoiceNumber(final Integer invoiceNumber) {
        val query = entityManager.createQuery(
                "DELETE FROM OnAccountRefundDetail entity WHERE entity.invoiceNumber = :invoiceNumber " +
                        "AND entity.status = 'temp'");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.executeUpdate();
    }

    public void updateStatusByInvoiceNumberAndStatusIn(final Integer invoiceNumber,
                                                       final List<String> statuses) {
        val query = entityManager.createQuery(
                "UPDATE OnAccountRefundDetail entity SET entity.status = 'Saved' " +
                        "WHERE entity.id = :invoiceNumber " +
                        "AND entity.status IN (:statuses)");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.setParameter("statuses", statuses);
        query.executeUpdate();
    }

    public void deleteTempByInvoiceNumber(final Integer invoiceNumber) {
        val query = entityManager.createQuery(
                "DELETE FROM OnAccountRefundDetail entity WHERE entity.invoiceNumber = :invoiceNumber " +
                        "AND entity.status = 'temp'");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.executeUpdate();
    }
    public void deleteByInvoiceNumber(final Integer invoiceNumber) {
        val query = entityManager.createQuery(
                "DELETE FROM OnAccountRefundDetail entity WHERE entity.invoiceNumber = :invoiceNumber ");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.executeUpdate();
    }

    public void setStatusByInvoiceNumber(final Integer invoiceNumber,
                                         final String status) {
        val query = entityManager.createQuery(
                "UPDATE OnAccountRefundDetail entity SET entity.status = :status " +
                        "WHERE entity.invoiceNumber = :invoiceNumber");
        query.setParameter("invoiceNumber", invoiceNumber);
        query.setParameter("status", status);
        query.executeUpdate();
    }
}