package org.oscarehr.common.dao;

import org.oscarehr.common.model.TriggerList;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
@SuppressWarnings("unchecked")
public class TriggerListDao extends AbstractDao<TriggerList>
{
    public TriggerListDao() { super(TriggerList.class); }
    
    public List<TriggerList> findAll () {
        Query query = entityManager.createQuery("SELECT t FROM TriggerList t WHERE t.archived = false ORDER BY t.name ASC");
        return query.getResultList();
    }
}