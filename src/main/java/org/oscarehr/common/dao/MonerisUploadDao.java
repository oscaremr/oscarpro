/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.dao;

import org.oscarehr.common.model.MonerisUpload;
import org.springframework.stereotype.Repository;

@Repository(value = "monerisUploadDao")
public class MonerisUploadDao extends AbstractDao<MonerisUpload>{

	public MonerisUploadDao() {
		super(MonerisUpload.class);
	}

}
