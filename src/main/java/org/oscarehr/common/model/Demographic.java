/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */
package org.oscarehr.common.model;

import ca.kai.util.DateUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.oscarehr.PMmodule.utility.DateTimeFormatUtils;
import org.oscarehr.PMmodule.utility.Utility;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.dao.DemographicGenderDao;
import org.oscarehr.common.dao.DemographicPronounDao;
import org.oscarehr.managers.adapter.DemographicAdapter;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.owasp.encoder.Encode;
import oscar.MyDateFormat;
import oscar.util.UtilDateUtilities;

/**
 * This is the object class that relates to the demographic table. Any customizations belong here.
 */
public class Demographic extends AbstractSubscriptionModel<Integer> {

	private static final String DEFAULT_MONTH = "01";
	private static final String DEFAULT_DATE = "01";
	private static final String DEFAULT_YEAR = "1900";
	private static final String DEFAULT_SEX = "M";
	private static final String DEFAULT_PATIENT_STATUS = PatientStatus.AC.name();
	private static final String DEFAULT_HEATH_CARD_TYPE = "ON";
	private static final String DEFAULT_FUTURE_DATE = "2100-01-01";
	public static final String ANONYMOUS = "ANONYMOUS";
	public static final String UNIQUE_ANONYMOUS = "UNIQUE_ANONYMOUS";
	public static Pattern DOCTOR_XML_PATTERN = Pattern.compile("<[fr]dohip>(.*)</[fr]dohip><[fr]d>(.*)</[fr]d>");
	private final static Pattern FD_LAST_NAME = Pattern.compile(".*<([fr])d>([^,]*),.*</([fr])d>.*");
	private final static Pattern FD_FIRST_NAME = Pattern.compile(".*<([fr])d>[^,]*,(.*)</([fr])d>.*");
	private final static Pattern FD_FULL_NAME = Pattern.compile(".*<([fr])d>(.*)</([fr])d>.*");
	private final static Pattern FD_OHIP = Pattern.compile("<([fr])dohip>(.*)</[fr]dohip>.*");
	public static final String ENROLLED_STATUS_CODE = "EN";
	public static final String NOT_ENROLLED_STATUS_CODE = "NE";
	public static final String ROSTERED_STATUS_CODE = "RO";
	public static final String NOT_ROSTERED_STATUS_CODE = "NR";

	public static final String FHIR_RESOURCE_PATIENT = "Patient";

	private int hashCode = Integer.MIN_VALUE;// primary key
	@Getter private Integer demographicNo;// fields

	@Getter @Setter private Integer activeCount = 0;
	@Getter @Setter private String address;
	@Getter @Setter private String alias;
	@Getter @Setter private String anonymous;
	@Getter @Setter private String chartNo;
	@Getter @Setter private String children;
	@Getter @Setter private String citizenship;
	@Getter @Setter private String city;
	@Getter @Setter private Boolean consentToUseEmailForCare;
	@Getter @Setter private Boolean consentToUseEmailForEOrder;
	@Getter @Setter private String countryOfOrigin;
	@Getter @Setter private Date dateJoined;
	@Getter private String dateOfBirth; 					// setter uses StringUtils.trimToNull
	@Getter @Setter private DemographicGender genderIdentity;
	@Getter @Setter private DemographicPronoun pronoun;
	@Getter @Setter private Integer genderId;
	@Getter @Setter private Integer pronounId;
	private String displayName; 									// setter builds name out of "last, first" names.
	@Getter @Setter private Date effDate;
	@Getter @Setter private String email;
	@Getter @Setter private Date endDate;
	@Getter @Setter private DemographicExt[] extras;
	@Getter @Setter private String firstName;
	@Getter @Setter private Date hcRenewDate;
	@Getter @Setter private String hcType;
	@Getter @Setter private Integer headRecord;
	@Getter @Setter private String hin;
	@Getter @Setter private Integer hsAlertCount = 0;
	@Getter @Setter private String lastName;
	@Getter @Setter private Date lastUpdateDate = new Date();
	@Getter @Setter private String lastUpdateUser;
	@Getter @Setter private String links;
	@Getter @Setter private String middleName;
	@Getter private String monthOfBirth; 					// setter uses StringUtils.trimToNull
	@Getter private String myOscarUserName; 			// setter uses StringUtils.trimToNull
	@Getter @Setter private String newsletter;
	@Getter @Setter private String officialLanguage;
	@Getter @Setter private String patientId;
	@Getter @Setter private String patientStatus;
	@Getter @Setter private Date patientStatusDate;
	@Getter @Setter private String patientType;
	@Getter @Setter private String pcnIndicator;
	@Getter @Setter private String phone;
	@Getter @Setter private String phone2;
	@Getter @Setter private String portalUserId;
	@Getter @Setter private String postal;
	@Getter @Setter private String preferredName = StringUtils.EMPTY;
	@Getter @Setter private String previousAddress;
	@Getter @Setter private Integer primarySystemId;
	@Getter @Setter private Provider provider;
	@Getter @Setter private String providerNo;
	@Getter private String province;  					// setter uses StringUtils.trimToNull and .toUpperCase
	@Getter @Setter private Date rosterDate;
	@Getter @Setter private String rosterStatus;
	@Getter @Setter private Date rosterTerminationDate;
	@Getter @Setter private String rosterTerminationReason;
	@Getter @Setter private String sex;
	@Getter @Setter private String sexDesc;
	@Getter @Setter private String sin;
	@Getter @Setter private String sourceOfIncome;
	@Getter @Setter private String spokenLanguage;
	@Getter @Setter private Set<Integer> subRecord;
	@Getter @Setter private String title;
	@Getter @Setter private String ver;
	@Getter private String yearOfBirth; 				// setter uses StringUtils.trimToNull
	@Getter @Setter private String middleNames;
	@Getter @Setter private String rosterEnrolledTo;

	@Override
	public String getModule() {
		return FHIR_RESOURCE_PATIENT;
	}

	@Override
	public String getModuleId() {
		return getId().toString();
	}

	public enum PatientStatus {
		AC, IN, DE, IC, ID, MO, FI
	}

	/**
	 * @deprecated default for birthday should be null
	 */
	@Deprecated
	public static Demographic create(
			String firstName,
			String lastName,
			String gender,
			String monthOfBirth,
			String dateOfBirth,
			String yearOfBirth,
			String hin,
			String ver,
			boolean applyDefaultBirthDate
	) {
		return create(firstName, lastName, gender, monthOfBirth, dateOfBirth, yearOfBirth, hin, ver);
	}

	/**
	 *
	 * @param firstName
	 * @param lastName
	 * @param gender
	 * @param monthOfBirth
	 * @param dateOfBirth
	 * @param yearOfBirth
	 * @param hin
	 * @param ver
	 * @return Demographic
	 */
	public static Demographic create(
			String firstName,
			String lastName,
			String gender,
			String monthOfBirth,
			String dateOfBirth,
			String yearOfBirth,
			String hin,
			String ver
	) {
		Demographic demographic = new Demographic();

		demographic.setFirstName(firstName);
		demographic.setLastName(lastName);
		demographic.setMonthOfBirth(monthOfBirth);
		demographic.setDateOfBirth(dateOfBirth);
		demographic.setYearOfBirth(yearOfBirth);
		demographic.setHin(hin);
		demographic.setVer(ver);

		demographic.setHcType(DEFAULT_HEATH_CARD_TYPE);
		demographic.setPatientStatus(DEFAULT_PATIENT_STATUS);
		demographic.setPatientStatusDate(new Date());
		demographic.setSex(gender == null || gender.length() == 0 ? DEFAULT_SEX : gender.substring(0, 1).toUpperCase());

		demographic.setDateJoined(new Date());
		//demographic.setEffDate(new Date());
		demographic.setEndDate(DateTimeFormatUtils.getDateFromString(DEFAULT_FUTURE_DATE));
		//demographic.setHcRenewDate(DateTimeFormatUtils.getDateFromString(DEFAULT_FUTURE_DATE));

		return demographic;
	}

	// constructors
	public Demographic() {
		initialize();
	}

	public Demographic(Demographic d) {
		this.hashCode = d.hashCode();
		this.demographicNo = d.getDemographicNo();
		this.phone = d.getPhone();
		this.patientStatus = d.getPatientStatus();
		this.patientStatusDate = d.getPatientStatusDate();
		this.rosterStatus = d.getRosterStatus();
		this.providerNo = d.getProviderNo();
		this.myOscarUserName = d.getMyOscarUserName();
		this.hin = d.getHin();
		this.address = d.getAddress();
		this.province = d.getProvince();
		this.monthOfBirth = d.getMonthOfBirth();
		this.ver = d.getVer();
		this.dateOfBirth = d.getDateOfBirth();
		this.sex = d.getSex();
		this.sexDesc = d.getSexDesc();
		this.dateJoined = d.getDateJoined();
		this.city = d.getCity();
		this.firstName = d.getFirstName();
		this.preferredName = d.getPreferredName();
		this.postal = d.getPostal();
		this.hcRenewDate = d.getHcRenewDate();
		this.phone2 = d.getPhone2();
		this.pcnIndicator = d.getPcnIndicator();
		this.endDate = d.getEndDate();
		this.lastName = d.getLastName();
		this.hcType = d.getHcType();
		this.chartNo = d.getChartNo();
		this.email = d.getEmail();
		this.yearOfBirth = d.getYearOfBirth();
		this.effDate = d.getEffDate();
		this.rosterDate = d.getRosterDate();
		this.rosterTerminationDate = d.getRosterTerminationDate();
		this.rosterTerminationReason = d.getRosterTerminationReason();
		this.links = d.getLinks();
		this.extras = d.getExtras();
		this.alias = d.getAlias();
		this.previousAddress = d.getPreviousAddress();
		this.children = d.getChildren();
		this.sourceOfIncome = d.getSourceOfIncome();
		this.citizenship = d.getCitizenship();
		this.sin = d.getSin();
		this.headRecord = d.getHeadRecord();
		this.subRecord = d.getSubRecord();
		this.anonymous = d.getAnonymous();
		this.spokenLanguage = d.getSpokenLanguage();
		this.activeCount = d.getActiveCount();
		this.hsAlertCount = d.getHsAlertCount();
		this.displayName = d.getDisplayName();
		this.provider = d.getProvider();
		this.lastUpdateUser = d.getLastUpdateUser();
		this.lastUpdateDate = d.getLastUpdateDate();
		this.title = d.getTitle();
		this.officialLanguage = d.getOfficialLanguage();
		this.countryOfOrigin = d.getCountryOfOrigin();
		this.newsletter = d.getNewsletter();
		this.patientType = d.getPatientType();
		this.patientId = d.getPatientId();
		this.portalUserId = d.getPortalUserId();
		this.genderId = d.getGenderId();
		this.pronounId = d.getPronounId();
		this.genderIdentity = d.getGenderIdentity();
		this.pronoun = d.getPronoun();
		this.consentToUseEmailForCare = d.getConsentToUseEmailForCare();
		this.consentToUseEmailForEOrder = d.getConsentToUseEmailForEOrder();
		this.middleName = d.getMiddleName();
		this.middleNames = d.getMiddleNames();
		this.rosterEnrolledTo = d.getRosterEnrolledTo();
		this.primarySystemId = d.getPrimarySystemId();
	}

	public Demographic(final HttpServletRequest request, final String user) {
		this.lastName = request.getParameter("last_name").trim();
		this.firstName = request.getParameter("first_name").trim();
		this.preferredName = request.getParameter("pref_name").trim();
		this.address = request.getParameter("address");
		this.city = request.getParameter("city");
		this.province = request.getParameter("province");
		this.postal = request.getParameter("postal");
		this.phone = request.getParameter("phone");
		this.phone2 = request.getParameter("phone2");
		this.email = request.getParameter("email");
		this.myOscarUserName = StringUtils.trimToNull(request.getParameter("myOscarUserName"));
		this.yearOfBirth = request.getParameter("year_of_birth");
		this.monthOfBirth =
				request.getParameter("month_of_birth") != null
						&& request.getParameter("month_of_birth").length() == 1
						? "0" + request.getParameter("month_of_birth")
						: request.getParameter("month_of_birth");
		this.dateOfBirth =
				request.getParameter("date_of_birth") != null
						&& request.getParameter("date_of_birth").length() == 1
						? "0" + request.getParameter("date_of_birth")
						: request.getParameter("date_of_birth");
		this.hin = request.getParameter("hin");
		this.ver = request.getParameter("ver");
		this.rosterStatus = processEnrollmentStatus(request);
		this.patientStatus = request.getParameter("patient_status");
		this.dateJoined = processJoinedDate(request);
		this.chartNo = request.getParameter("chart_no");
		this.providerNo = processProviderNumber(request);
		this.sex = request.getParameter("sex");
		this.patientType = request.getParameter("patientTypeOrig");
		this.patientId = request.getParameter("demographicMiscId");
		this.endDate = processEndDate(request);
		this.effDate = processEffDate(request);
		this.pcnIndicator = request.getParameter("pcn_indicator");
		this.hcType = request.getParameter("hc_type");
		this.rosterDate = processRosterDate(request);
		this.hcRenewDate = processHcRenewDate(request);
		this.countryOfOrigin = request.getParameter("countryOfOrigin");
		this.newsletter = request.getParameter("newsletter");
		this.sin = request.getParameter("sin");
		this.title = request.getParameter("title");
		this.officialLanguage = request.getParameter("official_lang");
		this.spokenLanguage = request.getParameter("spoken_lang");
		this.lastUpdateUser = user;
		this.lastUpdateDate = new Date();
		this.patientStatusDate = new Date();
		this.genderId = StringUtils.isEmpty(request.getParameter("gender")) ? null : Integer.parseInt(request.getParameter("gender"));
		this.pronounId = StringUtils.isEmpty(request.getParameter("pronoun")) ? null : Integer.parseInt(request.getParameter("pronoun"));
	}

	public static void updateDemographic(
			final HttpServletRequest request,
			final Demographic demographic,
			final String userNumber
	) {
		DemographicGenderDao demographicGenderDao = SpringUtils.getBean(DemographicGenderDao.class);
		DemographicPronounDao demographicPronounDao = SpringUtils.getBean(DemographicPronounDao.class);
		String enrollmentStatus = request.getParameter("roster_status");
		if (StringUtils.equals(enrollmentStatus, "EN")) {
			enrollmentStatus = "RO";
		} else if (StringUtils.equals(enrollmentStatus, "NE")) {
			enrollmentStatus = "NR";
		}
		
		demographic.setGenderId(parseIntegerOrDefault(StringUtils.trimToNull(request.getParameter("gender")), null));
		demographic.setPronounId(parseIntegerOrDefault(StringUtils.trimToNull(request.getParameter("pronoun")), null));
		List<DemographicGender> newGender = demographicGenderDao.findbyId(demographic.getGenderId());
		if (newGender != null && newGender.size() > 0) {
			demographic.setGenderIdentity(newGender.get(0));
		} else {
			demographic.setGenderIdentity(null);
		}
		List<DemographicPronoun> newPronoun = demographicPronounDao.findbyId(demographic.getPronounId());
		if (newPronoun != null && newPronoun.size() > 0) {
			demographic.setPronoun(newPronoun.get(0));
		} else {
			demographic.setPronoun(null);
		}
		demographic.setLastName(request.getParameter("last_name").trim());
		demographic.setFirstName(request.getParameter("first_name").trim());
		demographic.setPreferredName(request.getParameter("pref_name").trim());
		demographic.setAddress(request.getParameter("address"));
		demographic.setCity(request.getParameter("city"));
		demographic.setProvince(request.getParameter("province"));
		demographic.setPostal(request.getParameter("postal"));
		demographic.setPhone(request.getParameter("phone"));
		demographic.setPhone2(request.getParameter("phone2"));
		demographic.setEmail(request.getParameter("email"));

		if ("yes".equals(request.getParameter("consentToUseEmailForCare"))) {
			demographic.setConsentToUseEmailForCare(Boolean.TRUE);
		} else if ("no".equals(request.getParameter("consentToUseEmailForCare"))) {
			demographic.setConsentToUseEmailForCare(Boolean.FALSE);
		} else {
			demographic.setConsentToUseEmailForCare(null);
		}
		if ("yes".equals(request.getParameter("consentToUseEmailForEOrder"))) {
			demographic.setConsentToUseEmailForEOrder(Boolean.TRUE);
		} else if ("no".equals(request.getParameter("consentToUseEmailForEOrder"))) {
			demographic.setConsentToUseEmailForEOrder(Boolean.FALSE);
		} else {
			demographic.setConsentToUseEmailForEOrder(null);
		}
		demographic.setMyOscarUserName(StringUtils.trimToNull(
				request.getParameter("myOscarUserName")
		));
		demographic.setYearOfBirth(request.getParameter("year_of_birth"));
		demographic.setMonthOfBirth(
				request.getParameter("month_of_birth") != null
						&& request.getParameter("month_of_birth").length() == 1
						? "0"+request.getParameter("month_of_birth")
						: request.getParameter("month_of_birth")
		);
		demographic.setDateOfBirth(
				request.getParameter("date_of_birth") != null
						&& request.getParameter("date_of_birth").length() == 1
						? "0"+request.getParameter("date_of_birth")
						: request.getParameter("date_of_birth")
		);
		val demographicDao = (DemographicDao) SpringUtils.getBean(DemographicDao.class);
		demographic.setHin(
				DemographicAdapter.processUniqueDemographicHin(request, demographic.getDemographicNo()));
		demographic.setVer(request.getParameter("ver"));
		demographic.setRosterStatus(enrollmentStatus);
		demographic.setPatientStatus(request.getParameter("patient_status"));
		demographic.setChartNo(request.getParameter("chart_no"));
		demographic.setProviderNo(processProviderNumber(request));
		demographic.setSex(request.getParameter("sex"));
		demographic.setPcnIndicator(request.getParameter("pcn_indicator"));
		demographic.setHcType(request.getParameter("hc_type"));
		demographic.setCountryOfOrigin(request.getParameter("countryOfOrigin"));
		demographic.setNewsletter(request.getParameter("newsletter"));
		demographic.setSin(request.getParameter("sin"));
		demographic.setTitle(request.getParameter("title"));
		demographic.setOfficialLanguage(request.getParameter("official_lang"));
		demographic.setSpokenLanguage(request.getParameter("spoken_lang"));
		demographic.setRosterTerminationReason(
				request.getParameter("roster_termination_reason")
		);
		demographic.setLastUpdateUser(userNumber);
		demographic.setLastUpdateDate(new java.util.Date());
		demographic.setPatientType(request.getParameter("patientType"));
		demographic.setPatientId(request.getParameter("patientId"));

		val  dateJoined = StringUtils.trimToNull(request.getParameter("date_joined"));
		if (dateJoined != null) {
			demographic.setDateJoined(MyDateFormat.getSysDate(dateJoined));
		} else {
			demographic.setDateJoined(null);
		}

		val  endDate = StringUtils.trimToNull(request.getParameter("end_date"));
		if (endDate != null ) {
			demographic.setEndDate(MyDateFormat.getSysDate(endDate));
		} else {
			demographic.setEndDate(null);
		}

		val  effDate = StringUtils.trimToNull(request.getParameter("eff_date"));
		if (effDate != null ) {
			demographic.setEffDate(MyDateFormat.getSysDate(effDate));
		} else {
			demographic.setEffDate(null);
		}

		val  hcRenewDate  =StringUtils.trimToNull(request.getParameter("hc_renew_date"));
		if (hcRenewDate != null) {
			demographic.setHcRenewDate(MyDateFormat.getSysDate(hcRenewDate));
		} else {
			demographic.setHcRenewDate(null);
		}

		val  rosterDate = StringUtils.trimToNull(request.getParameter("roster_date"));
		if (rosterDate != null) {
			demographic.setRosterDate(MyDateFormat.getSysDate(rosterDate));
		} else {
			demographic.setRosterDate(null);
		}

		val  rosterTerminationDate = StringUtils.trimToNull(
				request.getParameter("roster_termination_date")
		);
		if (rosterTerminationDate != null) {
			demographic.setRosterTerminationDate(MyDateFormat.getSysDate(rosterTerminationDate));
		} else {
			demographic.setRosterTerminationDate(null);
		}

		val  patientStatusDate=StringUtils.trimToNull(request.getParameter("patientstatus_date"));
		if (patientStatusDate != null ) {
			demographic.setPatientStatusDate(MyDateFormat.getSysDate(patientStatusDate));
		} else {
			demographic.setPatientStatusDate(null);
		}
	}
	
	public static Integer parseIntegerOrDefault(final String value, final Integer defaultValue) {
		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException ignored) {
			return defaultValue;
		}
	}

	private String processEnrollmentStatus(HttpServletRequest request) {
		return StringUtils.trimToEmpty(request.getParameter("roster_status"));
	}

	private Date processEffDate(HttpServletRequest request) {
		return processDateFromRequest(StringUtils.trimToNull(request.getParameter("eff_date")));
	}

	private Date processEndDate(HttpServletRequest request) {
		return processDateFromRequest(StringUtils.trimToNull(request.getParameter("end_date")));
	}

	private Date processJoinedDate(HttpServletRequest request) {
		return processDateFromRequest(StringUtils.trimToNull(request.getParameter("date_joined")));
	}

	private Date processHcRenewDate(HttpServletRequest request) {
		return processDateFromRequest(StringUtils.trimToNull(request.getParameter("hc_renew_date")));
	}

	private Date processRosterDate(HttpServletRequest request) {
		return processDateFromRequest(StringUtils.trimToNull(request.getParameter("roster_date")));
	}

	private Date processDateFromRequest(String date) {
		val  endDate = StringUtils.trimToNull(date);
		return endDate != null
				? MyDateFormat.getSysDate(endDate)
				: null;
	}

	private static String processProviderNumber(HttpServletRequest request) {
		val provider = request.getParameter("fromAppt") != null
				? request.getParameter("staff")
				: request.getParameter("provider_no");
		return StringUtils.trimToEmpty(provider);
	}

	/**
	 * Constructor for primary key
	 */
	public Demographic(Integer demographicNo) {
		this.demographicNo = demographicNo;
		initialize();
	}

	/**
	 * Constructor for required fields
	 */
	public Demographic(Integer demographicNo, String firstName, String lastName) {
		this.demographicNo = demographicNo;
		this.firstName = firstName;
		this.lastName = lastName;
		initialize();
	}

	public String getDisplayName(){
		if (displayName == null) {
			displayName = getLastName() + ", " + getFirstName();
		}
		return displayName;
	}

	/**
	 * Set the unique identifier of this class
	 *
	 * @param demographicNo the new ID
	 */
	public void setDemographicNo(Integer demographicNo) {
		this.demographicNo = demographicNo;
		this.hashCode = Integer.MIN_VALUE;
	}

	public void setMyOscarUserName(String myOscarUserName) {
		this.myOscarUserName = StringUtils.trimToNull(myOscarUserName);
	}

	/**
	 * Set the value related to the column: province
	 *
	 * @param province the province value
	 */
	public void setProvince(String province) {
		province = StringUtils.trimToNull(province);

		if (province != null) {
			province = province.toUpperCase();
		}
		this.province = province;
	}

	/**
	 * Set the value related to the column: month_of_birth
	 *
	 * @param monthOfBirth the month_of_birth value
	 */
	public void setMonthOfBirth(String monthOfBirth) {
		this.monthOfBirth = StringUtils.trimToNull(monthOfBirth);
	}

	/**
	 * Set the value related to the column: date_of_birth
	 *
	 * @param dateOfBirth the date_of_birth value
	 */
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = StringUtils.trimToNull(dateOfBirth);
	}

	public String getFormattedDateJoined() {
		Date d = getDateJoined();
		if (d == null) {
			return ("");
		}
		return (DateFormatUtils.ISO_DATE_FORMAT.format(d));
	}

	public String getReferralPhysicianRowId() {
		return getExtensionValue(DemographicExtKey.DOCTOR_ROSTER);
	}

	public String getReferralPhysicianName() {
		return getExtensionValue(DemographicExtKey.DOCTOR_ROSTER_NAME);
	}

	public String getReferralPhysicianOhip() {
		return getExtensionValue(DemographicExtKey.DOCTOR_ROSTER_OHIP);
	}

	public String getFamilyPhysicianRowId() {
		return getExtensionValue(DemographicExtKey.DOCTOR_FAMILY);
	}

	public String getFamilyPhysicianName() {
		return getExtensionValue(DemographicExtKey.DOCTOR_FAMILY_NAME);
	}

	public String getFamilyPhysicianOhip() {
		return getExtensionValue(DemographicExtKey.DOCTOR_FAMILY_OHIP);
	}

	public String getLinkStatus() {
    return getExtensionValue(DemographicExtKey.LINK_STATUS);
	}

	public String getMrpName() {
		return provider == null ? "" : getProvider().getFormattedName();
	}

	public String getMrpPractitionerNumber() {
		return provider == null ? "" : getProvider().getOhipNo();
	}

	private String getExtensionValue(final DemographicExtKey key) {
		return DemographicAdapter.getDemographicExtensionValueAsString(getDemographicNo(), key);
	}

	/**
	 * Sets the name and OHIP number of the referral physician using XML string into a new or existing
	 * demographicExt record.
	 * @deprecated
	 * This method should not be used as XML strings are no longer used to set referral physician name/OHIP
	 * number and family_doctor is no longer a column in the demographic table.
	 * <p> Use {@link #setReferralPhysicianName(String)} and {@link #setReferralPhysicianOhip(String)} instead.</p>
	 *
	 * @param referralPhysicianXml XML string in the format of "{@code <rdohip></rdohip><rd></rd>}"
	 */
	@Deprecated
	public void setReferralPhysicianXml(final String referralPhysicianXml) {
		if (referralPhysicianXml != null && !referralPhysicianXml.isEmpty()) {
			val matcher = DOCTOR_XML_PATTERN.matcher(referralPhysicianXml);
			if (!matcher.find()) {
				return;
			}
			val name = matcher.group(2);
			val ohip = matcher.group(1);
			if (!name.equals(this.getReferralPhysicianName())) {
				setReferralPhysicianName(name);
			}
			if (!ohip.equals(this.getReferralPhysicianOhip())) {
				setReferralPhysicianOhip(ohip);
			}
		}
	}

	/**
	 * Sets the name and OHIP number of the referral physician using XML string into a new or existing
	 * demographicExt record.
	 * @deprecated
	 * This method should not be used as XML strings are no longer used to set family physician name/OHIP
	 * number and family_physician is no longer a column in the demographic table.
	 * <p> Use {@link #setFamilyPhysicianName(String)} and {@link #setFamilyPhysicianOhip(String)} instead.</p>
	 *
	 * @param familyPhysicianXml XML string in the format of {@code <fdohip></fdohip><fd></fd>}
	 */
	@Deprecated
	public void setFamilyPhysicianXml(final String familyPhysicianXml) {
		if (familyPhysicianXml != null && !familyPhysicianXml.isEmpty()) {
			val matcher = DOCTOR_XML_PATTERN.matcher(familyPhysicianXml);
			if (!matcher.find()) {
				return;
			}
			val name = matcher.group(2);
			val ohip = matcher.group(1);
			if (!name.equals(this.getFamilyPhysicianName())) {
				setFamilyPhysicianName(name);
			}
			if (!ohip.equals(this.getFamilyPhysicianOhip())) {
				setFamilyPhysicianOhip(ohip);
			}
		}
	}

	public void setReferralPhysicianRowId(final String referralPhysicianRowId) {
		setPhysicianExtensionValue(DemographicExtKey.DOCTOR_ROSTER, referralPhysicianRowId);
	}

	public void setReferralPhysicianName(final String referralPhysicianName) {
		setPhysicianExtensionValue(DemographicExtKey.DOCTOR_ROSTER_NAME, referralPhysicianName);
	}

	public void setReferralPhysicianOhip(final String referralPhysicianOhip) {
		setPhysicianExtensionValue(DemographicExtKey.DOCTOR_ROSTER_OHIP, referralPhysicianOhip);
	}

	public void setFamilyPhysicianRowId(final String familyPhysicianRowId) {
		setPhysicianExtensionValue(DemographicExtKey.DOCTOR_FAMILY, familyPhysicianRowId);
	}

	public void setFamilyPhysicianName(final String familyPhysicianName) {
		setPhysicianExtensionValue(DemographicExtKey.DOCTOR_FAMILY_NAME, familyPhysicianName);
	}

	public void setFamilyPhysicianOhip(final String familyPhysicianOhip) {
		setPhysicianExtensionValue(DemographicExtKey.DOCTOR_FAMILY_OHIP, familyPhysicianOhip);
	}

	private void setPhysicianExtensionValue(final DemographicExtKey key, final String value) {
		DemographicAdapter.updateOrCreateDemographicExtension(this, key, value);
	}

	/**
	 * Return the last name from demographic extension referralPhysicianName
	 *
	 * @return referral physician last name
	 */
	public String getReferralPhysicianLastName() {
		val fullName = getReferralPhysicianName();
		val nameParts = fullName.split(",\\s*");
		return nameParts.length == 2 ? nameParts[0].trim() : fullName;
	}

	/**
	 * Return the first name from demographic extension referralPhysicianName
	 *
	 * @return referral physician first name
	 */
	public String getReferralPhysicianFirstName() {
		val fullName = getReferralPhysicianName();
		val nameParts = fullName.split(",\\s*");
		return nameParts.length == 2 ? nameParts[1].trim() : fullName;
	}

	/**
	 * Gets demographic's full name.
	 *
	 * @return
	 * 		Returns the last name, first name pair.
	 */
	public String getFullName() {
		return getLastName() + ", " + getFirstName();
	}

	public String getFormattedEndDate() {
		Date d = getEndDate();
		return d != null
			? (DateFormatUtils.ISO_DATE_FORMAT.format(d))
			: StringUtils.EMPTY;
	}

	/**
	 * Set the value related to the column: year_of_birth
	 *
	 * @param yearOfBirth the year_of_birth value
	 */
	public void setYearOfBirth(String yearOfBirth) {
		this.yearOfBirth = StringUtils.trimToNull(yearOfBirth);
	}

	public String getFormattedEffDate() {
		Date d = getEffDate();
		if (d != null) return (DateFormatUtils.ISO_DATE_FORMAT.format(d));
		else return ("");
	}

	public void setFormattedEffDate(String formattedDate) {
		if (StringUtils.isBlank(formattedDate))
			return;

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			this.effDate = sdf.parse(formattedDate);
		} catch (ParseException e) {
			MiscUtils.getLogger().error("Error", e);
		}

	}

	public String getFormattedRenewDate() {
		Date d = getHcRenewDate();
		if (d != null) return (DateFormatUtils.ISO_DATE_FORMAT.format(d));
		else return ("");
	}

	public void setFormattedRenewDate(String formattedDate) {
		if (StringUtils.isBlank(formattedDate))
			return;

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			this.hcRenewDate = sdf.parse(formattedDate);
		} catch (ParseException e) {
			MiscUtils.getLogger().error("Error", e);
		}

	}

	@Override
	public boolean equals(Object obj) {
		if (null == obj) return false;
		if (!(obj instanceof Demographic)) return false;
		else {
			Demographic demographic = (Demographic) obj;
			if (null == this.getDemographicNo() || null == demographic.getDemographicNo()) return false;
			else return (this.getDemographicNo().equals(demographic.getDemographicNo()));
		}
	}

	@Override
	public int hashCode() {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getDemographicNo()) return super.hashCode();
			else {
				String hashStr = this.getClass().getName() + ":" + this.getDemographicNo().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}

	@Override
	public Integer getId() {
		return getDemographicNo();
	}

	@Override
	public String toString() {
		return super.toString();
	}

	protected void initialize() {
		links = StringUtils.EMPTY;
	}

	public String addZero(String text, int num) {
		text = text.trim();

		for (int i = text.length(); i < num; i++) {
			text = "0" + text;
		}

		return text;
	}

	public String getAge() {
		return (String.valueOf(Utility.calcAge(Utility.convertToReplaceStrIfEmptyStr(getYearOfBirth(), DEFAULT_YEAR), Utility.convertToReplaceStrIfEmptyStr(getMonthOfBirth(), DEFAULT_MONTH), Utility.convertToReplaceStrIfEmptyStr(getDateOfBirth(), DEFAULT_DATE))));
	}

	public String getAgeAsOf(Date asofDate) {
		return Utility.calcAgeAtDate(Utility.calcDate(Utility.convertToReplaceStrIfEmptyStr(getYearOfBirth(), DEFAULT_YEAR), Utility.convertToReplaceStrIfEmptyStr(getMonthOfBirth(), DEFAULT_MONTH), Utility.convertToReplaceStrIfEmptyStr(getDateOfBirth(), DEFAULT_DATE)), asofDate);
	}
	public String getSubjectPronoun() {
		if ("M".equals(sex)) {
			return "he";
		} else if ("F".equals(sex)) {
			return "she";
		} else {
			return "they";
		}
	}
	public String getPossessivePronoun() {
		if ("M".equals(sex)) {
			return "his";
		} else if ("F".equals(sex)) {
			return "her";
		} else {
			return "their";
		}
	}

	public int getAgeInYears() {
		return Utility.getNumYears(Utility.calcDate(Utility.convertToReplaceStrIfEmptyStr(getYearOfBirth(), DEFAULT_YEAR), Utility.convertToReplaceStrIfEmptyStr(getMonthOfBirth(), DEFAULT_MONTH), Utility.convertToReplaceStrIfEmptyStr(getDateOfBirth(), DEFAULT_DATE)), Calendar.getInstance().getTime());
	}

	public int getAgeInYearsAsOf(Date asofDate) {
		return Utility.getNumYears(Utility.calcDate(Utility.convertToReplaceStrIfEmptyStr(getYearOfBirth(), DEFAULT_YEAR), Utility.convertToReplaceStrIfEmptyStr(getMonthOfBirth(), DEFAULT_MONTH), Utility.convertToReplaceStrIfEmptyStr(getDateOfBirth(), DEFAULT_DATE)), asofDate);
	}

	public String getFormattedDob() {
		Calendar cal = getBirthDay();
		if (cal != null) return (DateFormatUtils.ISO_DATE_FORMAT.format(cal));
		else return ("");
	}

	public void setFormattedDob(String formattedDate) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date d = sdf.parse(formattedDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(d);
			this.setBirthDay(cal);
		} catch (ParseException e) {
			MiscUtils.getLogger().error("Error", e);
		}

	}

	public String getFormattedLinks() {
		StringBuilder response = new StringBuilder();

		if (getNumLinks() > 0) {
			String[] links = getLinks().split(",");
			for (int x = 0; x < links.length; x++) {
				if (response.length() > 0) {
					response.append(",");
				}
			}
		}

		return response.toString();
	}

	public String getFormattedName() {
		return getLastName() + ", " + getFirstName();
	}

	public int getNumLinks() {
		if (getLinks() == null) {
			return 0;
		}

		if (getLinks().equals("")) {
			return 0;
		}

		return getLinks().split(",").length;
	}

	public Integer getCurrentRecord() {
		if (headRecord != null) return headRecord;
		return demographicNo;
	}

	public boolean isActive() {
		return activeCount > 0;
	}

	public boolean hasHsAlert() {
		return hsAlertCount > 0;
	}

	public void setBirthDay(Calendar cal) {
		if (cal == null) {
			dateOfBirth = monthOfBirth = yearOfBirth = null;
		} else {
			dateOfBirth = addZero(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)), 2);
			monthOfBirth = addZero(String.valueOf(cal.get(Calendar.MONTH) + 1), 2);
			yearOfBirth = addZero(String.valueOf(cal.get(Calendar.YEAR)), 4);
		}
	}

	public GregorianCalendar getBirthDay() {
		GregorianCalendar cal = null;

		if (dateOfBirth != null && monthOfBirth != null && yearOfBirth != null) {
			cal = new GregorianCalendar();
			cal.setTimeInMillis(0);
			cal.set(Integer.parseInt(yearOfBirth), Integer.parseInt(monthOfBirth) - 1, Integer.parseInt(dateOfBirth));

			// force materialisation of data
			cal.getTimeInMillis();
		}

		return (cal);
	}

	// Returns birthday in the format yyyy-mm-dd
	public String getBirthDayAsString() {
		return getYearOfBirth() + "-" + getMonthOfBirth() + "-" + getDateOfBirth();
	}

  public String getDemographicDateOfBirth() {
    return DateUtils.getDateString(
        "dd/MM/yyyy",
        UtilDateUtilities.calcDate(
            this.getYearOfBirth(),
            this.getMonthOfBirth(),
            this.getDateOfBirth()
        )
    );
  }

	public String getStandardIdentificationHtml() {
		val sb = new StringBuilder();
		//name: <b>LAST, FIRST</b><br/>
		sb.append("<b>").append(Encode.forHtml(getLastName().toUpperCase())).append("</b>").append(",");
		sb.append(getFirstName());
		if (getTitle() != null && getTitle().length()>0) {
			sb.append(" ").append("(").append(getTitle()).append(")");
		}
		sb.append("<br/>");
		// birthday: Born <b>DATE_OF_BIRTH</b>
		sb.append("Born ").append("<b>").append(getFormattedDob()).append("</b>");

		// hin: <br/>HC <b>HIN VER (TYPE)</b>
		if (getHin() != null && getHin().length()>0) {
			sb.append("<br/>");
			sb.append("HC ")
					.append("<b>")
					.append(getHin()).append(" ").append(getVer())
					.append("(").append(getHcType()).append(")")
					.append("</b>");
		}

		// chart number: <br/> Chart No <b>CHART_NO</b>
		if (getChartNo() != null && getChartNo().length()>0) {
			sb.append("<br/>");
			sb.append("Chart No ").append("<b>").append(getChartNo()).append("</b>");
		}
		return sb.toString();
	}

	public String getLabel() {
		String label = getDisplayName() + "\n";
		List<String> addressLineValues = new ArrayList<String>();
		if (!StringUtils.isEmpty(getAddress())) { addressLineValues.add(getAddress()); }
		if (!StringUtils.isEmpty(getCity())) { addressLineValues.add(getCity()); }
		if (!StringUtils.isEmpty(getProvince())) { addressLineValues.add(getProvince()); }
		if (!StringUtils.isEmpty(getPostal())) { addressLineValues.add(getPostal()); }
		if (!addressLineValues.isEmpty()) {
			label += StringUtils.join(addressLineValues, ", ") + "\n";
		}
		label += "Tel: " + (!StringUtils.isEmpty(getPhone()) ? getPhone() + "(H)" : "") + (!StringUtils.isEmpty(getPhone2()) ? " " + getPhone2() + "(W)" : "") + "\n";
		label += StringUtils.trimToEmpty(getDateOfBirth()) + "/" + StringUtils.trimToEmpty(getMonthOfBirth()) + "/" + StringUtils.trimToEmpty(getYearOfBirth());
		label += "(" + getSex() + ")";
		label += " HIN:" + getHin() + getVer();
		return label;
	}

	/**
	 * @return the patient's preferred phone number based on what's in demographicext (Default: Home Phone)
	 */
	public String getPreferredPhone() {
		DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
		String phonePreference = demographicExtDao.getValueForDemoKey(demographicNo, "phone_preference");
		if (phonePreference != null && !phonePreference.equals("")) {
			switch (phonePreference) {
				case "H":
					return getPhone();
				case "W":
					return getPhone2();
				case "C":
					return demographicExtDao.getValueForDemoKey(demographicNo, "demo_cell");
			}
		}
		return getPhone();
	}

	@Nullable
	public String getGenderIdentityValue() {
		return this.getGenderIdentity() == null ? null : this.getGenderIdentity().getValue();
	}

	@Nullable
	public String getPronounValue() {
		return this.getPronoun() == null ? null : this.getPronoun().getValue();
	}

	/* COMPARATORS */

	public static final Comparator<Demographic> FormattedNameComparator = new Comparator<Demographic>() {
		@Override
		public int compare(Demographic dm1, Demographic dm2) {
			return dm1.getFormattedName().compareToIgnoreCase(dm2.getFormattedName());
		}
	};
	public static final Comparator<Demographic> LastNameComparator = new Comparator<Demographic>() {
		public int compare(Demographic dm1, Demographic dm2) {
			return dm1.getLastName().compareTo(dm2.getLastName());
		}
	};
	public static final Comparator<Demographic> FirstNameComparator = new Comparator<Demographic>() {
		public int compare(Demographic dm1, Demographic dm2) {
			return dm1.getFirstName().compareTo(dm2.getFirstName());
		}
	};
	public static final Comparator<Demographic> LastAndFirstNameComparator = new Comparator<Demographic>() {
		public int compare(Demographic dm1, Demographic dm2)
		{
			int res = dm1.getLastName().compareToIgnoreCase(dm2.getLastName());
			if (res != 0)
			{
				return res;
			}
			return dm1.getFirstName().compareTo(dm2.getFirstName());
		}
	};
	public static final Comparator<Demographic> DemographicNoComparator = new Comparator<Demographic>() {
		public int compare(Demographic dm1, Demographic dm2) {
			return dm1.getDemographicNo().compareTo(dm2.getDemographicNo());
		}
	};
	public static final Comparator<Demographic> SexComparator = new Comparator<Demographic>() {
		public int compare(Demographic dm1, Demographic dm2) {
			return dm1.getSex().compareTo(dm2.getSex());
		}
	};
	public static final Comparator<Demographic> AgeComparator = new Comparator<Demographic>() {
		public int compare(Demographic dm1, Demographic dm2) {
			return dm1.getAge().compareTo(dm2.getAge());
		}
	};
	public static final Comparator<Demographic> DateOfBirthComparator = new Comparator<Demographic>() {
		public int compare(Demographic dm1, Demographic dm2) {
			return dm1.getBirthDayAsString().compareTo(dm2.getBirthDayAsString());
		}
	};
	public static final Comparator<Demographic> RosterStatusComparator = new Comparator<Demographic>() {
		public int compare(Demographic dm1, Demographic dm2) {
			return dm1.getRosterStatus().compareTo(dm2.getRosterStatus());
		}
	};
	public static final Comparator<Demographic> ChartNoComparator = new Comparator<Demographic>() {
		public int compare(Demographic dm1, Demographic dm2) {
			return dm1.getChartNo().compareTo(dm2.getChartNo());
		}
	};
	public static final Comparator<Demographic> ProviderNoComparator = new Comparator<Demographic>() {
		public int compare(Demographic dm1, Demographic dm2) {
			return dm1.getProviderNo().compareTo(dm2.getProviderNo());
		}
	};
	public static final Comparator<Demographic> PatientStatusComparator = new Comparator<Demographic>() {
		public int compare(Demographic dm1, Demographic dm2) {
			return dm1.getPatientStatus().compareTo(dm2.getPatientStatus());
		}
	};
	public static final Comparator<Demographic> PhoneComparator = new Comparator<Demographic>() {
		public int compare(Demographic dm1, Demographic dm2) {
			return dm1.getPhone().compareTo(dm2.getPhone());
		}
	};
}
