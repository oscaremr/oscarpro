/**
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto. All Rights Reserved. This software is published under the GPL GNU General Public
 * License. This program is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */
package org.oscarehr.common.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.val;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.model.provider.ProviderType;
import oscar.MyDateFormat;
import oscar.SxmlMisc;

/**
 * This is the object class that relates to the provider table. Any customizations belong here.
 */
@Data
@NoArgsConstructor
public class Provider extends AbstractModel<String> implements Serializable, Comparable<Provider> {

  public static final String SYSTEM_PROVIDER_NO = "-1";
  private String providerNo;
  private String comments;
  private String phone;
  private String billingNo;
  private String workPhone;
  private String address;
  private String city;
  private String province;
  private String postalCode;
  private String team;
  private String status;
  private String lastName;
  private String providerType;
  private String sex;
  private String ohipNo;
  private String specialty;
  private String credentials = "";
  private Date dob;
  private String hsoNo;
  private String providerActivity;
  private String firstName;
  private String rmaNo;
  private boolean thirdPartyOnly = false;
  private Date SignedConfidentiality;
  private String practitionerNo;
  private String practitionerNoType;
  private String email;
  private String title;
  private String lastUpdateUser;
  private Date lastUpdateDate = new Date();
  private String supervisor;
  private Boolean hasSchedule = true;
  private boolean receivesTicklers = true;
  private String lastUsedUuid;
  private String signingKey;
  private String excellerisId;
  private String lifelabsId;
  private String prescribeItType;

  /**
   * Constructor for primary key
   */
  public Provider(String providerNo) {
    this.setProviderNo(providerNo);
  }

  /**
   * Constructor for required fields
   */
  public Provider(String providerNo, String lastName, String providerType, String sex,
      String specialty, String firstName) {
    this.setProviderNo(providerNo);
    this.setLastName(lastName);
    this.setProviderType(providerType);
    this.setSex(sex);
    this.setSpecialty(specialty);
    this.setFirstName(firstName);
  }

  public Provider(Provider provider) {
    providerNo = provider.providerNo;
    comments = provider.comments;
    phone = provider.phone;
    billingNo = provider.billingNo;
    workPhone = provider.workPhone;
    address = provider.address;
    city = provider.city;
    province = provider.province;
    postalCode = provider.postalCode;
    team = provider.team;
    status = provider.status;
    lastName = provider.lastName;
    providerType = provider.providerType;
    sex = provider.sex;
    ohipNo = provider.ohipNo;
    specialty = provider.specialty;
    credentials = provider.credentials;
    dob = provider.dob;
    hsoNo = provider.hsoNo;
    providerActivity = provider.providerActivity;
    firstName = provider.firstName;
    rmaNo = provider.rmaNo;
    thirdPartyOnly = provider.thirdPartyOnly;
    SignedConfidentiality = provider.SignedConfidentiality;
    practitionerNo = provider.practitionerNo;
    practitionerNoType = provider.practitionerNoType;
    email = provider.email;
    title = provider.title;
    lastUpdateUser = provider.lastUpdateUser;
    lastUpdateDate = provider.lastUpdateDate;
    supervisor = provider.supervisor;
    hasSchedule = provider.hasSchedule;
    receivesTicklers = provider.receivesTicklers;
    excellerisId = provider.excellerisId;
    lifelabsId = provider.lifelabsId;
    lastUsedUuid = provider.lastUsedUuid;
    prescribeItType = provider.prescribeItType;
    signingKey = provider.signingKey;
  }

  public Provider(
      final HttpServletRequest request,
      final String user,
      final boolean allowBillingFieldSave
  ) {
    this.providerNo = request.getParameter("provider_no");
    this.lastName = request.getParameter("last_name");
    this.firstName = request.getParameter("first_name");
    this.providerType = request.getParameter("provider_type");
    this.hasSchedule = "true".equals(request.getParameter("has_schedule"));
    this.receivesTicklers = "true".equals(request.getParameter("receives_ticklers"));
    this.specialty = request.getParameter("specialty");
    this.credentials = request.getParameter("credentials");
    this.team = request.getParameter("team");
    this.sex = request.getParameter("sex");
    this.dob = MyDateFormat.getSysDate(request.getParameter("dob"));
    this.address = request.getParameter("address");
    this.city = request.getParameter("city");
    this.province = request.getParameter("province");
    this.postalCode = request.getParameter("postalCode");
    this.phone = request.getParameter("phone");
    this.workPhone = request.getParameter("workphone");
    this.email = request.getParameter("email");
    this.status = request.getParameter("status");
    this.comments = SxmlMisc.createXmlDataString(request, "xml_p");
    this.providerActivity = request.getParameter("provider_activity");
    this.practitionerNo = request.getParameter("practitionerNo");
    this.practitionerNoType = request.getParameter("practitionerNoType");
    this.lastUpdateUser = user;
    this.lastUpdateDate = new java.util.Date();
    this.supervisor = request.getParameter("supervisor");
    this.excellerisId = request.getParameter("excellerisId");
    this.lifelabsId = request.getParameter("lifelabsId");
    if (allowBillingFieldSave) {
      this.ohipNo = request.getParameter("ohip_no");
      this.rmaNo = request.getParameter("rma_no");
      this.thirdPartyOnly = request.getParameter("thirdPartyOnly") != null
          && request.getParameter("thirdPartyOnly").equals("on");
      this.billingNo = request.getParameter("billing_no");
      this.hsoNo = request.getParameter("hso_no");
    } else {
      this.ohipNo = "";
      this.rmaNo = "";
      this.thirdPartyOnly = false;
      this.billingNo = "";
      this.hsoNo = "";
    }
    this.prescribeItType = request.getParameter("prescribe_it_type");
  }

  public static Provider updateProvider(
      final HttpServletRequest request,
      final Provider provider,
      final String user,
      final boolean allowBillingFieldSave
  ) {
    provider.setLastName(request.getParameter("last_name"));
    provider.setFirstName(request.getParameter("first_name"));
    provider.setProviderType(request.getParameter("provider_type"));
    provider.setHasSchedule("true".equals(request.getParameter("has_schedule")));
    provider.setReceivesTicklers("true".equals(request.getParameter("receives_ticklers")));
    provider.setSpecialty(request.getParameter("specialty"));
    provider.setCredentials(request.getParameter("credentials"));
    provider.setTeam(request.getParameter("team"));
    provider.setSex(request.getParameter("sex"));
    provider.setDob(MyDateFormat.getSysDate(request.getParameter("dob")));
    provider.setAddress(request.getParameter("address"));
    provider.setCity(request.getParameter("city"));
    provider.setProvince(request.getParameter("province"));
    provider.setPostalCode(request.getParameter("postalCode"));
    provider.setPhone(request.getParameter("phone"));
    provider.setWorkPhone(request.getParameter("workphone"));
    provider.setEmail(request.getParameter("email"));
    provider.setStatus(request.getParameter("status"));
    provider.setComments(SxmlMisc.createXmlDataString(request, "xml_p"));
    provider.setProviderActivity(request.getParameter("provider_activity"));
    provider.setPractitionerNo(request.getParameter("practitionerNo"));
    provider.setExcellerisId(request.getParameter("excellerisId"));
    provider.setLifelabsId(request.getParameter("lifelabsId"));
    provider.setPractitionerNoType(request.getParameter("practitionerNoType"));
    provider.setLastUpdateUser(user);
    provider.setLastUpdateDate(new java.util.Date());
    if (allowBillingFieldSave) {
      provider.setOhipNo(request.getParameter("ohip_no"));
      provider.setRmaNo(request.getParameter("rma_no"));
      provider.setThirdPartyOnly(
          request.getParameter("thirdPartyOnly") != null && request.getParameter("thirdPartyOnly")
              .equals("on"));
      provider.setBillingNo(request.getParameter("billing_no"));
      provider.setHsoNo(request.getParameter("hso_no"));
    }
    val supervisor = request.getParameter("supervisor");
    if (supervisor == null || supervisor.equalsIgnoreCase("null") || supervisor.equals("")) {
      provider.setSupervisor(null);
    } else {
      provider.setSupervisor(supervisor);
    }
    provider.setPrescribeItType(request.getParameter("prescribe_it_type"));
    return provider;
  }

  /* CUSTOM BOOLEAN  */

  public Boolean doesReceiveTicklers() {
    return receivesTicklers;
  }

  public Boolean hasSchedule() {
    return hasSchedule;
  }

  /* CUSTOM GETTERS */

  public String getFormattedName() {
    return getFormattedName(false);
  }

  public String getFormattedName(final boolean external) {
    String prefix = "";
    if (external && getOhipNo() != null && !getOhipNo().equals("")) {
      prefix = "Dr. ";
    }
    return prefix + getLastName() + ", " + getFirstName();
  }

  public String getFullName() {
    return getFirstName() + " " + getLastName();
  }

  public String getBillingGroupNo() {
    String groupNo = SxmlMisc.getXmlContent(comments, "<xml_p_billinggroup_no>",
        "</xml_p_billinggroup_no>");
    groupNo = StringUtils.isBlank(groupNo) || groupNo.equals("0000") ? null : groupNo;
    return groupNo;
  }

  public String getSpecialtyCode() {
    String specialtyCod = SxmlMisc.getXmlContent(comments, "<xml_p_specialty_code>",
        "</xml_p_specialty_code>");
    specialtyCod = StringUtils.isBlank(specialtyCod) ? null : specialtyCod;
    return specialtyCod;
  }

  public ProviderType getProviderTypeAsEnum() {
    return ProviderType.fromStringValue(providerType);
  }

  public ProviderType getPrescribeItTypeAsEnum() {
    return ProviderType.fromStringValue(prescribeItType);
  }

  public ProviderType getPrescribeItTypeOverride() {
    return ProviderType.NONE.equals(ProviderType.fromStringValue(prescribeItType))
        ? ProviderType.fromStringValue(providerType)
        : ProviderType.fromStringValue(prescribeItType);
  }

  /* COMPARATORS */

  public ComparatorName ComparatorName() {
    return new ComparatorName();
  }

  @Override
  public int hashCode() {
    if (providerNo == null) {
      return (super.hashCode());
    } else {
      return (providerNo.hashCode());
    }
  }

  public static class ComparatorName implements Comparator<Provider>, Serializable {

    public int compare(final Provider o1, final Provider o2) {
      String lhs = o1.getLastName() + o1.getFirstName();
      String rhs = o2.getLastName() + o2.getFirstName();

      return lhs.compareTo(rhs);
    }
  }

  public int compareTo(final Provider o) {
    if (providerNo == null) {
      return 0;
    }
    return providerNo.compareTo(o.providerNo);
  }

  public static final Comparator<Provider> LastNameComparator = new Comparator<Provider>() {
    public int compare(final Provider o1, final Provider o2) {
      return o1.getLastName().compareTo(o2.getLastName());
    }
  };

  @Override
  public String getId() {
    return providerNo;
  }
}
