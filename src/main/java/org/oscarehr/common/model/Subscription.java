/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Subscription extends AbstractModel<Integer> {

    public static final String DEMOGRAPHIC_MODULE = "Demographic";
    public static final String APPOINTMENT_MODULE = "Appointment";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String module = "";
    private String moduleId = "";

    public Subscription() {
        super();
    }

    public Subscription(String module, String moduleId) {
        super();
        this.module = module;
        this.moduleId = moduleId;
    }
}
