package org.oscarehr.common.model;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.NotFound;
import org.oscarehr.common.dao.Hl7TextInfoDao;
import org.oscarehr.common.dao.ProviderLabRoutingDao;
import org.oscarehr.common.dao.TriggerLogDao;
import org.oscarehr.util.SpringUtils;
import oscar.oscarLab.ca.all.parsers.MessageHandler;
import oscar.oscarLab.ca.all.upload.ProviderLabRouting;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hibernate.annotations.NotFoundAction.IGNORE;

@Entity
@Table(name="trigger_data")
public class Trigger extends AbstractModel<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;
    
    @Column(name="type")
    private String type;
    
    @Column(name="name")
    private String name;
    
    @Column(name="trigger_status")
    private String triggerStatus;
    
    @Column(name="last_updated_date")
    private Date lastUpdatedDate;
    
    @Column(name="last_updated_by")
    private String lastUpdatedBy;
    
    @Column(name="internal_id")
    private String internalId;

    @NotFound(action = IGNORE)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "trigger_id", referencedColumnName = "id")
    @OrderBy("orderPosition ASC")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<TriggerCondition> triggerConditionList = new ArrayList<>();

    @NotFound(action = IGNORE)
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "trigger_id", referencedColumnName = "id")
    @OrderBy("orderPosition ASC")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<TriggerAction> triggerActionList = new ArrayList<>();

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTriggerStatus() {
        return triggerStatus;
    }

    public void setTriggerStatus(String archived) {
        this.triggerStatus = archived;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public String getInternalId() {
        return internalId;
    }

    public void setInternalId(String internalId) {
        this.internalId = internalId;
    }

    public List<TriggerCondition> getTriggerConditionList() {
        return triggerConditionList;
    }

    public void setTriggerConditionList(List<TriggerCondition> triggerConditionList) {
        this.triggerConditionList = triggerConditionList;
    }

    public List<TriggerAction> getTriggerActionList() {
        return triggerActionList;
    }

    public void setTriggerActionList(List<TriggerAction> triggerActionList) {
        this.triggerActionList = triggerActionList;
    }
    
    public String parseLabKey (String key, Hl7TextInfo hl7TextInfo, MessageHandler messageHandler) {
        TriggerConditionType triggerConditionType = TriggerConditionType.getByKey(key);
        String result = null;
        switch (triggerConditionType) {
            case ACCESSION_NUMBER:
                result = hl7TextInfo.getAccessionNumber();
                break;
            case RESULT_STATUS:
                result = hl7TextInfo.getResultStatus();
                break;
            case OBR_COUNT:
                result = String.valueOf(messageHandler.getOBRCount());
                break;
            case OBR_DATE:
                result = hl7TextInfo.getObrDate();
                break;
            case OBR_4_2:
                StringBuilder OBR42String = new StringBuilder();
                for (int i = 0; i < messageHandler.getOBRCount(); i++) {
                    OBR42String.append(messageHandler.getOBRName(i));
                    if (i < messageHandler.getOBRCount()-1) {
                        OBR42String.append("|");
                    }
                }
                result = OBR42String.toString();
                break;
            case OBR_4_1:
                StringBuilder OBR41String = new StringBuilder();
                for (int i = 0; i < messageHandler.getOBRCount(); i++) {
                    OBR41String.append(messageHandler.getOBRIdentifier(i));
                    if (i < messageHandler.getOBRCount()-1) {
                        OBR41String.append("|");
                    }
                }
                result = OBR41String.toString();
                break;
            case OBR_4:
                StringBuilder OBR4String = new StringBuilder();
                for (int i = 0; i < messageHandler.getOBRCount(); i++) {
                    OBR4String.append(messageHandler.getOBRIdentifier(i)).append("^").append(messageHandler.getOBRName(i));
                    if (i < messageHandler.getOBRCount()-1) {
                        OBR4String.append("|");
                    }
                }
                result = OBR4String.toString();
                break;
            case REQUESTING_CLIENT:
                result = hl7TextInfo.getRequestingProvider();
                break;
            case PRIORITY:
                result = hl7TextInfo.getPriority();
                break;
            case SENDING_FACILITY:
                result = hl7TextInfo.getSendingFacility();
                break;
            case LOINC_CODE:
                StringBuilder LOINCString = new StringBuilder();
                for (int i = 0; i < messageHandler.getOBRCount(); i++) {
                    for (int j = 0; j < messageHandler.getOBXCount(i); j++) {
                        LOINCString.append(messageHandler.getOBXIdentifier(i, j));
                        if (i < messageHandler.getOBRCount() && j < messageHandler.getOBXCount(i)-1) {
                            LOINCString.append("|");
                        }
                    }
                }
                result = LOINCString.toString();
                break;
            case DISCIPLINE:
                result = hl7TextInfo.getDiscipline();
                break;
        }
        return result;
    }
    
    public void parseIncomingLabTrigger (Hl7TextInfo hl7TextInfo, MessageHandler messageHandler) {
        TriggerLogDao triggerLogDao = SpringUtils.getBean(TriggerLogDao.class);
        
        //First we need to make sure all the conditions in the trigger are met
        boolean allConditionsMet = true;
        for (TriggerCondition triggerCondition : this.getTriggerConditionList()) {
            TriggerConditionType triggerConditionType = TriggerConditionType.getByKey(triggerCondition.getKeyValue());
            String parsedKeyValue = parseLabKey (triggerCondition.getKeyValue(), hl7TextInfo, messageHandler);
            //If the condition is doing a comparison between two key values, we need to parse the second key value as well. If not, we can just check if the single key value meets the condition.
            if (triggerConditionType != null && triggerConditionType.getValueType().equals(TriggerValueType.KEY_VALUE)) {
                if (!triggerCondition.isConditionMet(parsedKeyValue, parseLabKey(triggerCondition.getValue(), hl7TextInfo, messageHandler))) {
                    allConditionsMet = false;
                }
            } else if (!triggerCondition.isConditionMet(parsedKeyValue)) {
                allConditionsMet = false;
            }
        }
        
        //If all conditions are met, then we need to run all the actions for the trigger
        if (allConditionsMet) {
            Hl7TextInfoDao hl7TextInfoDao = SpringUtils.getBean(Hl7TextInfoDao.class);
            ProviderLabRoutingDao providerLabRoutingDao = SpringUtils.getBean(ProviderLabRoutingDao.class);
            for (TriggerAction triggerAction : this.getTriggerActionList()) {
                TriggerActionType triggerActionType = TriggerActionType.getByKey(triggerAction.getKeyValue());
                if (triggerActionType != null) {
                    //Before running the action we want to log it in the trigger log
                    StringBuilder value = new StringBuilder(StringUtils.trimToEmpty(triggerAction.getValue()));
                    
                    //If the action uses key values we want to log the parsed key value. We use the double pipes as a separator between the key value name and the parsed value
                    if (triggerActionType.getValueType().equals(TriggerValueType.KEY_VALUE)) {
                        //If the key value is of type STRING_LIST, that means there's multiple in the HL7 Lab and we need to only grab the first
                        if (TriggerConditionType.getByKey(triggerAction.getValue()).getValueType().equals(TriggerValueType.STRING_LIST)) {
                            String parsedKey = parseLabKey(triggerAction.getValue(), hl7TextInfo, messageHandler);
                            if (StringUtils.isNotEmpty(parsedKey) && parsedKey.contains("|") && parsedKey.length() > 1) {
                                value.append("||").append(parsedKey.split("\\|")[0]);
                            } else {
                                value.append("||").append(parsedKey);
                            }
                        } else {
                            value.append("||").append(parseLabKey(triggerAction.getValue(), hl7TextInfo, messageHandler));
                        }
                    }
                    
                    TriggerLog triggerLog = new TriggerLog(this.id, triggerAction.getKeyValue(), value.toString(), this.type, String.valueOf(hl7TextInfo.getId()), new Date());
                    triggerLogDao.persist(triggerLog);
                    
                    //Finally we run the action
                    switch (triggerActionType) {
                        case FLAG_PROVIDER:
                            ProviderLabRouting flagProviderRouting = new ProviderLabRouting();
                            flagProviderRouting.route(hl7TextInfo.getLabNumber(), triggerAction.getValue(), "HL7");
                            break;
                        case CLEAR_FLAGGED_PROVIDERS:
                            List<ProviderLabRoutingModel> providerLabRoutingList = providerLabRoutingDao.getProviderLabRoutingsByLabNoAndLabType(hl7TextInfo.getLabNumber(), "HL7");
                            for (ProviderLabRoutingModel providerLabRoutingModel : providerLabRoutingList) {
                                providerLabRoutingDao.removeAndLog(providerLabRoutingModel);
                            }
                            break;
                        case SEND_LAB_TO_UNCLAIMED_INBOX:
                            ProviderLabRouting flagUnclaimedInbox = new ProviderLabRouting();
                            flagUnclaimedInbox.route(hl7TextInfo.getLabNumber(), "0", "HL7");
                            break;
                        case LABEL_AS:
                            if (StringUtils.isEmpty(hl7TextInfo.getLabel())) {
                                hl7TextInfo.setLabel(triggerAction.getValue());
                            }
                            break;
                        case LABEL_AS_KEY:
                            if (StringUtils.isEmpty(hl7TextInfo.getLabel())) {
                                if (TriggerConditionType.getByKey(triggerAction.getValue()).getValueType().equals(TriggerValueType.STRING_LIST)) {
                                    String parsedKey = parseLabKey(triggerAction.getValue(), hl7TextInfo, messageHandler);
                                    if (StringUtils.isNotEmpty(parsedKey) && parsedKey.contains("|")) {
                                        hl7TextInfo.setLabel(parsedKey.split("\\|")[0]);
                                    } else {
                                        hl7TextInfo.setLabel(parsedKey);
                                    }
                                } else {
                                    hl7TextInfo.setLabel(parseLabKey(triggerAction.getValue(), hl7TextInfo, messageHandler));
                                }
                            }
                            break;
                    }
                }
            }
            
            //Once all the actions are complete, we merge the hl7TextInfo object back into the database
            hl7TextInfoDao.merge(hl7TextInfo);
        }
    }
}