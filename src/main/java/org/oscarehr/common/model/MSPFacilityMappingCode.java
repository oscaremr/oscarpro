package org.oscarehr.common.model;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="billing_msp_facility_mapping_codes")
public class MSPFacilityMappingCode extends AbstractModel<MSPFacilityMappingCodePK> {
    @EmbeddedId
    private MSPFacilityMappingCodePK id;

    @Override
    public MSPFacilityMappingCodePK getId() {
        return id;
    }

    public void setId(MSPFacilityMappingCodePK id) {
        this.id = id;
    }

    public Integer getClinicId() {
        return this.id.getClinicId();
    }

    public void setClinicId(Integer clinicId) {
        this.id.setClinicId(clinicId);
    }

    public String getBillingCode() {
        return this.id.getBillingCode();
    }

    public void setBillingCode(String billingCode) {
        this.id.setBillingCode(billingCode);
    }
}
