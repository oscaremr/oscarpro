/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "onaccountpayments")
@Data
public class OnAccountPayment extends AbstractModel<Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "paymentID")
    private Integer paymentId;
    @Column
    private String date;
    @Column
    private Double amount;
    @Column
    private String details;
    @Column
    private String status;
    @Column(name = "invoiceNo")
    private Integer invoiceNumber;

    @Override
    public Integer getId() {
        return paymentId;
    }
}