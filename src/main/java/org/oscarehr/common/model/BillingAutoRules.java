/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="billing_auto_rules")
public class BillingAutoRules extends AbstractModel<Integer> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String rule_name;
	private Boolean service_code;
	private Boolean dx;
	
	private String referral_doctor_type;
	private String referral_doctor;
	
	private String billing_doctor_type;
	private String billing_doctor;
	
	private String visit_type;
	private String visit_location;
	private String sli_code;
		
	@Temporal(TemporalType.DATE)
	private Date created;
	private String creator;
	
	@Temporal(TemporalType.DATE)
	private Date modified;
	private String last_modifier;
	
	private Integer status;
	
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	

	public String getRuleName() {
		return rule_name;
	}

	public void setRuleName(String rule_name) {
		this.rule_name = rule_name;
	}
	
	
	public Boolean getServiceCode() {
		return service_code;
	}

	public void setServiceCode(Boolean service_code) {
		this.service_code = service_code;
	}
	
	public Boolean getDx() {
		return dx;
	}

	public void setDx(Boolean dx) {
		this.dx = dx;
	}
	
	public String getReferralDoctorType() {
		return referral_doctor_type;
	}

	public void setReferralDoctorType(String referral_doctor_type) {
		this.referral_doctor_type = referral_doctor_type;
	}
	

	public String getReferralDoctor() {
		return referral_doctor;
	}

	public void setReferralDoctor(String referral_doctor) {
		this.referral_doctor = referral_doctor;
	}
	
	public String getBillingDoctorType() {
		return billing_doctor_type;
	}

	public void setBillingDoctorType(String billing_doctor_type) {
		this.billing_doctor_type = billing_doctor_type;
	}
	

	public String getBillingDoctor() {
		return billing_doctor;
	}

	public void setBillingDoctor(String billing_doctor) {
		this.billing_doctor = billing_doctor;
	}
	
	public String getVisitType() {
		return visit_type;
	}

	public void setVisitType(String visit_type) {
		this.visit_type = visit_type;
	}
	

	public String getVisitLocation() {
		return visit_location;
	}

	public void setVisitLocation(String visit_location) {
		this.visit_location = visit_location;
	}
	
	public String getSliCode() {
		return sli_code;
	}

	public void setSliCode(String sli_code) {
		this.sli_code = sli_code;
	}	
	
	public Date getCreatedDate() {
		return created;
	}

	public void setCreatedDate(Date created) {
		this.created = created;
	}	
	
	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
	
	public Date getModifiedDate() {
		return modified;
	}
	
	public void setModifiedDate(Date modified) {
		this.modified = modified;
	}	
	
	public String getLastModifier() {
		return last_modifier;
	}

	public void setLastModifier(String last_modifier) {
		this.last_modifier = last_modifier;
	}
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
	public String getCreatedDateAsString() {
		if(getCreatedDate() != null) {
			SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
			return f.format(getCreatedDate());
		}else {
			return "";
		}
	}
	
	public String getModifiedDateAsString() {
		if(getModifiedDate() != null) {
			SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
			return f.format(getModifiedDate());
		}else {
			return "";
		}
	}
	
}
