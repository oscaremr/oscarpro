/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "OrderLabTestCodeCache")
public class OrderLabTestCodeCache extends AbstractModel<Integer>
                                   implements Comparable {
		
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer Id;
	
	@Column(name = "Searchable")
	private boolean searchable;

	@Column(name = "TestOnForm")
	private boolean testOnForm;

	@Column(name = "RequireAppointment")
	private boolean requireAppointment;

	@Column(name = "PatientPay")
	private Boolean patientPay;
	
	@Column(name = "Test_Name")
	private String testName;
	
	@Column(name = "Key_Name")
	private String keyName;
	
	@Column(name = "Lab_Test_Code")
	private String labTestCode;
	@Column(name = "isUpdateable")
	private Boolean updateable;
	private String province;

	@Override
    public Integer getId() {
		return Id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.Id = id;
	}

	public boolean isSearchable() {
		return searchable;
	}

	public void setSearchable(boolean searchable) {
		this.searchable = searchable;
	}

	public boolean isTestOnForm() {
		return testOnForm;
	}

	public void setTestOnForm(boolean testOnForm) {
		this.testOnForm = testOnForm;
	}

	public boolean isRequireAppointment() {
		return requireAppointment;
	}

	public void setRequireAppointment(boolean requireAppointment) {
		this.requireAppointment = requireAppointment;
	}

	public Boolean getPatientPay() {
		return patientPay;
	}

	public void setPatientPay(Boolean patientPay) {
		this.patientPay = patientPay;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}
	
	public String getKeyName() {
		return keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}
	
	public String getLabTestCode() {
		return labTestCode;
	}

	public void setLabTestCode(String labTestCode) {
		this.labTestCode = labTestCode;
	}

	public Boolean getUpdateable() {
		return updateable;
	}

	public void setUpdateable(Boolean updateable) {
		this.updateable = updateable;
	}
	
    public String getProvince() {
      return province;
    }

    public void setProvince(String province) {
      this.province = province;
    }

  @Override
	public int compareTo(final Object o) {
		final OrderLabTestCodeCache other = (OrderLabTestCodeCache) o;
		return getTestName().compareTo(other.getTestName());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((Id == null) ? 0 : Id.hashCode());
		result = prime * result + (updateable ? 1231 : 1237);
		result = prime * result + ((keyName == null) ? 0 : keyName.hashCode());
		result = prime * result + ((labTestCode == null) ? 0 : labTestCode.hashCode());
		result = prime * result + ((patientPay == null) ? 0 : patientPay.hashCode());
		result = prime * result + (requireAppointment ? 1231 : 1237);
		result = prime * result + (searchable ? 1231 : 1237);
		result = prime * result + ((testName == null) ? 0 : testName.hashCode());
		result = prime * result + (testOnForm ? 1231 : 1237);
		result = prime * result + ((province == null) ? 0 : province.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderLabTestCodeCache other = (OrderLabTestCodeCache) obj;
		if (Id == null) {
			if (other.Id != null)
				return false;
		} else if (!Id.equals(other.Id))
			return false;
		if (updateable != other.updateable) {
			return false;
		}
		if (keyName == null) {
			if (other.keyName != null) {
				return false;
			}
		} else if (!keyName.equals(other.keyName)) {
			return false;
		}
		if (labTestCode == null) {
			if (other.labTestCode != null)
				return false;
		} else if (!labTestCode.equals(other.labTestCode))
			return false;
		if (patientPay == null) {
			if (other.patientPay != null)
				return false;
		} else if (!patientPay.equals(other.patientPay))
			return false;
		if (requireAppointment != other.requireAppointment)
			return false;
		if (searchable != other.searchable)
			return false;
		if (testName == null) {
			if (other.testName != null)
				return false;
		} else if (!testName.equals(other.testName))
			return false;
		if (testOnForm != other.testOnForm)
			return false;
		if (province == null) {
		  if (other.province != null) {
		    return false;
		  }
		} else if (!province.equals(other.province)) {
		  return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "OrderLabTestCodeCache [Id=" + Id + ", searchable=" + searchable + ", testOnForm="
				+ testOnForm + ", requireAppointment=" + requireAppointment + ", patientPay=" + patientPay
				+ ", testName=" + testName + ", keyName=" + keyName + ", labTestCode=" + labTestCode
				+ ", updateable=" + updateable +" , province=" + province + "]";
	}

}
