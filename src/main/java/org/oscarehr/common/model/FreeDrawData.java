/*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "FreeDrawData")
public class FreeDrawData extends AbstractModel<Integer> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Lob
  private String drawData;

  private String name;

  @Temporal(TemporalType.TIMESTAMP)
  private Date updateTime;

  private int demographicNo;

  private int appointmentNo;

  private boolean status = false;

  private String providerNo;

  private String handWriteNotes;

  private String dataType = "sketchtoy";

  private String thumbnailId;

  public FreeDrawData() {}

  public Integer getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getDrawData() {
    return this.drawData;
  }

  public void setDrawData(String drawData) {
    this.drawData = drawData;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getUpdateTime() {
    return this.updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

  public int getDemographicNo() {
    return demographicNo;
  }

  public void setDemographicNo(int demographicNo) {
    this.demographicNo = demographicNo;
  }

  public int getAppointmentNo() {
    return appointmentNo;
  }

  public void setAppointmentNo(int appointmentNo) {
    this.appointmentNo = appointmentNo;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  public String getProviderNo() {
    return providerNo;
  }

  public void setProviderNo(String providerNo) {
    this.providerNo = providerNo;
  }

  public String getHandWriteNotes() {
    return handWriteNotes;
  }

  public void setHandWriteNotes(String handWriteNotes) {
    this.handWriteNotes = handWriteNotes;
  }

  public String getDataType() {
    return dataType;
  }

  public void setDataType(String dataType) {
    this.dataType = dataType;
  }

  public String getThumbnailId() {
    return thumbnailId;
  }

  public void setThumbnailId(String thumbnailId) {
    this.thumbnailId = thumbnailId;
  }

}
