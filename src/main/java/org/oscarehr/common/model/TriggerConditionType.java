package org.oscarehr.common.model;

import com.google.common.collect.Maps;

import java.util.Comparator;
import java.util.Map;

public enum TriggerConditionType {
    SENDING_FACILITY("SENDING_FACILITY","Sending Facility", TriggerValueType.STRING),
    OBR_4("OBR_4","OBR 4", TriggerValueType.STRING_LIST),
    OBR_4_1("OBR_4.1","OBR 4.1", TriggerValueType.STRING_LIST),
    OBR_4_2("OBR_4.2","OBR 4.2", TriggerValueType.STRING_LIST),
    ACCESSION_NUMBER("ACCESSION_NUMBER","Accession Number", TriggerValueType.STRING),
    LOINC_CODE("LOINC_CODE","LOINC Code", TriggerValueType.STRING_LIST),
    OBR_COUNT("OBR_COUNT","OBR Count", TriggerValueType.NUMBER),
    OBR_DATE("OBR_DATE","OBR Date", TriggerValueType.DATE),
    PRIORITY("PRIORITY","Priority", TriggerValueType.STRING),
    RESULT_STATUS("RESULT_STATUS","Result Status", TriggerValueType.STRING),
    REQUESTING_CLIENT("REQUESTING_CLIENT","Requesting Client", TriggerValueType.STRING),
    DISCIPLINE("DISCIPLINE","Discipline", TriggerValueType.STRING);
    private String key;
    private String displayName;
    private TriggerValueType valueType;

    private static final Map<String, TriggerConditionType> keyMap = Maps.newHashMapWithExpectedSize(TriggerConditionType.values().length);
    static {
        for (TriggerConditionType keyEnum : TriggerConditionType.values()) {
            keyMap.put(keyEnum.getKey(), keyEnum);
        }
    }
    
    public static Comparator<TriggerConditionType> triggerConditionTypeComparator () {
        return new Comparator<TriggerConditionType>() {
            public int compare (TriggerConditionType o1, TriggerConditionType o2){
                return o1.getKey().compareTo(o2.getKey());
            }
        };
    }
    
    TriggerConditionType(String key, String displayName, TriggerValueType type) {
        this.key = key;
        this.displayName = displayName;
        this.valueType = type;
    }

    public String getKey() {
        return key;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public TriggerValueType getValueType() {
        return valueType;
    }

    public static TriggerConditionType getByKey(String key) {
        return keyMap.get(key);
    }
}