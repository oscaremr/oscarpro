/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PreRemove;
import javax.persistence.Table;

@Entity
@Table(name = "excelleris_eorder")
public class ExcellerisEorder extends EOrder {

  private static final long serialVersionUID = 1L;

  @Column(name = "questionnaire")
  private String questionnaire;

  @Column(name = "service_request")
  private String serviceRequest;

  /** @return the serialversionuid */
  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public String getQuestionnaire() {
    return questionnaire;
  }

  public void setQuestionnaire(String questionnaire) {
    this.questionnaire = questionnaire;
  }

  public String getServiceRequest() {
    return serviceRequest;
  }

  public void setServiceRequest(final String serviceRequest) {
    this.serviceRequest = serviceRequest;
  }

  @PreRemove
  protected void jpa_preventDelete() {
    throw (new UnsupportedOperationException("Remove is not allowed for this type of item."));
  }
}
