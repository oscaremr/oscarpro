package org.oscarehr.common.model;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.apache.commons.lang.StringUtils;

@Entity
@Table(name="SystemPreferences")
public class SystemPreferences extends AbstractModel<Integer>
{

    public static final List<String> RX_PREFERENCE_KEYS = Arrays.asList(
        "rx_expand_lu_code_descriptions",
        "rx_show_lab", "rx_paste_provider_to_echart",
        "rx_show_end_dates","rx_show_start_dates",
        "rx_show_refill_duration",
        "rx_paste_to_encounter_default",
        "rx_show_refill_quantity",
        "rx_methadone_end_date_calc",
        "rx_show_pillway_button",
        "save_rx_signature",
        "rx_watermark",
        "rx_enable_internal_dispensing",
        "autoprint_rx_pharmacy_info",
        "use_rx_date_for_interaction",
        "rx_results_for_number_of_days_in_past",
        "rx_display_din_on_prescription_output");
    public static final List<String> SCHEDULE_PREFERENCE_KEYS = Arrays.asList(
            "display_chart_no_in_create_appointment",
            "increase_last_name_field_width_enabled",
            "schedule_display_custom_roster_status",
            "schedule_display_enrollment_dr_enabled",
            "schedule_display_outside_use_enabled",
            "schedule_display_type",
            "schedule_tp_link_display",
            "schedule_eligibility_enabled",
            "schedule_tp_link_enabled",
            "schedule_tp_link_type");
    public static final List<String> ECHART_PREFERENCE_KEYS = Arrays.asList(
        "allergyAlertPopup",
        "caisi_note_filter_enabled",
        "displayChartNo",
        "eChartDisplayInitialTicklerMessage",
        "echartDisplayInboxLink",
        "echartShowAppointmentTime",
        "echartShowOutsideUseNote",
        "echartShowOverduePreventionsOnly",
        "echartShowRosterStatus",
        "echart_email_indicator",
        "echart_paste_fax_note",
        "echart_show_fam_doc_widget",
        "echart_show_group_document_by_type",
        "echart_show_larger_font_size",
        "echart_show_ref_doc_widget",
        "echart_show_report_module",
        "hideCppNotes",
        "hideDocumentNotes",
        "hideEformNotes",
        "hideEncounterLink",
        "hideFormNotes",
        "hideInvoiceNotes",
        "oscarEncounter.pdfPrint.chartNo",
        "echart_hide_timer",
        "display_health_insurance_number",
        "echartDisplayConsultationReason"
    );

    public static final List<String> MASTER_FILE_PREFERENCE_KEYS = Arrays
        .asList("display_former_name", "redirect_for_contact", "populateChartNoWithDemographicNo");
    public static final List<String> GENERAL_SETTINGS_KEYS =
        Arrays.asList(
            "assignDemographicsToSites",
            "display_patient_name_on_message_print",
            "enable_validation_on_specialist",
            "force_logout_when_inactive",
            "force_logout_when_inactive_time",
            "invoice_custom_clinic_info",
            "msg_use_create_date",
            "patient_search_select",
            "reminder_send_window",
            "replace_demographic_name_with_preferred",
            "require_cpsid_and_doctor_for_mrp"
        );
    public static final List<String> LAB_DISPLAY_PREFERENCE_KEYS = Arrays.asList("code_show_hide_column", 
        "lab_embed_pdf", "lab_pdf_max_size", "display_discipline_as_label_in_inbox", "discipline_character_limit_in_inbox", 
        "show_obgyn_shortcuts", "sticky_label_different_labs", "historic_lab_value_match_test_name", "showAccessionNumberColumn",
        "display_lab_value_based_on");
    public static final List<String> EFORM_SETTINGS = Arrays.asList("rtl_template_document_type", "patient_intake_eform", "patient_intake_letter_eform", "perinatal_eforms");
    public static final List<String> REFERRAL_SOURCE_PREFERENCE_KEYS = Arrays.asList("enable_referral_source");
    public static final String KIOSK_DISPLAY_PREFERENCE_KEYS = "check_in_all_appointments";
    public static final String AUTO_FLAG_ALWAYS_TO_MRP_ON_DOCUMENTS = "auto_flag_always_to_mrp_on_documents";
    public static final List<String> DOCUMENT_SETTINGS_KEYS = Arrays.asList(
        "add_new_button_enabled",
        "allow_faxing_unassigned_documents",
        "display_prompt_mrp_remove",
        "document_description_typeahead",
        "document_discipline_column_display",
        "inbox_use_fax_dropdown",
        "split_new_window"
    );
    public static final List<String> RTL_TEMPLATE_SETTINGS = Arrays.asList("rtl_template_document_type");
    public static final List<String> GST_SETTINGS_KEYS = Arrays.asList("clinic_gst_number");
    public static final List<String> INTEGRATION_PREFERENCE_KEYS = Arrays.asList("care_connect_url","care_connect_orgs");
    public static final List<String> WELL_AI_VOICE_SETTINGS = Arrays.asList("well_ai_voice.enabled");
    public static final String INSIG_PATIENT_PORTAL_LINK = "insig.integration.patient_portal_url";
    public static final String PILLWAY_ADD_TO_DEMOGRAPHIC_ENABLED =
        "pillway.add_to_demographic_enabled";
    public static final String CAISI_NOTE_FILTER_ENABLED = "caisi_note_filter_enabled";
    public static final String PASSWORD_POLICY = "password_policy";
    public static final String FHIR_SUBSCRIPTION_ENABLED = "fhir.subscription.enabled";
    public static final String DEMOGRAPHIC_FILTER_ENABLED = "demographic_filter_enabled";
    public static final String BILLING_A233_REPLACE_A234_AND_G435A = "billing_A233_replace_A234_and_G435A";
    public static final List<String> OSCAR_PROPERTIES_BILLING_SETTINGS_KEYS = Arrays.asList(
        "moh_file_management_enabled",
        "new_on_billing_enabled",
        "auto_generated_billing_enabled",
        "bc_default_alt_billing_enabled",
        "bill_note_enabled",
        "billing_review_auto_payment_enabled",
        "invoice_reports_print_hide_name_enabled",
        "remove_patient_detail_in_third_party",
        "rma_billing_enabled",
        "sob_check_all_enabled",
        "delete_bill_confirm_action_enabled",
        "new_billing_ui.enabled"
    );
    public static final List<String> OSCAR_PROPERTIES_DOCUMENT_SETTINGS_KEYS = Arrays.asList(
        "enable_except_rx_document_filter",
        "display_flag_as_abnormal",
        "allow_update_document_content",
        "enable_move_delete_doc_to_deleted_folder"
    );
    public static final List<String> OSCAR_PROPERTIES_ECHART_SETTINGS_KEYS = Arrays.asList(
        "chart_print_include_cellphone",
        "chart_print_include_hin",
        "chart_print_include_home_phone",
        "chart_print_include_work_phone",
        "chart_print_include_mrp",
        "chart_print_use_provider_current_program",
        "echart_show_relations",
        "echart_display_demographic_no",
        "program_domain_show_echart"
    );
    public static final List<String> OSCAR_PROPERTIES_FAX_SETTINGS_KEYS = Arrays.asList(
        "fax_enabled",
        "rx_fax_enabled",
        "consultation_fax_enabled",
        "eform_fax_enabled"
    );
    public static final List<String> OSCAR_PROPERTIES_GENERAL_SETTINGS_KEYS = Arrays.asList(
        "display_admin_hph",
        "caseload_default_all_providers_enabled",
        "ckd_screening_disabled",
        "client_dropdown_enabled",
        "display_masterfile_referral_source",
        "enable_new_user_pin_control",
        "number_of_flowsheet_values",
        "prevention_show_comments",
        "enable_printpdf_referring_practitioner",
        "private_informed_consent_enabled",
        "show_referral_menu",
        "require_referring_md",
        "enable_resident_review_workflow",
        "enable_save_as_xml",
        "admin_user_billing_control",
        "allow_online_booking",
        "new_flowsheet_enabled",
        "new_inbox_enabled",
        "daily_appointments_page_refresh_timeout",
        "tickler_warn_period",
        "tickler_edit_enabled",
        "tickler_email_enabled",
        "show_sexual_health_label",
        "show_single_page_chart",
        "skip_postal_code_validation",
        "use_program_location_enabled",
        "assign_def_issue_to_no_notes_waitlist",
        "ect_autosave_timer",
        "ect_save_feedback_timer",
        "renal_dosing_ds_enabled",
        "prevention_enabled",
        "immunization_in_prevention_enabled",
        "enable_consultation_appt_instr_lookup",
        "enable_consultation_auto_incl_allergies",
        "enable_consultation_auto_inc_medications",
        "consultation_dynamic_labelling_enabled",
        "enable_consultation_lock_referral_date",
        "enable_consultation_patient_will_book",
        "consultation_signature_enabled",
        "enable_alerts_on_schedule_screen",
        "enable_notes_on_schedule_screen",
        "enable_default_schedule_viewall",
        "enable_demographic_patient_clinic_status",
        "enable_demogr_patient_health_care_team",
        "enable_demographic_patient_rostering",
        "enable_demographic_waiting_list",
        "enable_urgent_messages",
        "enable_external_name_on_demographic",
        "enable_external_name_on_schedule",
        "enable_provider_schedule_note",
        "enable_receptionist_alt_view",
        "enable_referral_menu",
        "enable_eform_in_appointment",
        "enable_meditech_id",
        "enable_appointment_mc_number",
        "eform_signature_enabled",
        "allow_multiple_same_day_group_appts",
        "appointment_locking_timeout",
        "enable_appointment_intake_form",
        "display_appointment_daysheet_button",
        "encounter_layout_refresh_timeout",
        "show_appointment_reason",
        "hide_con_report_link",
        "hide_econsult_link",
        "measurements_create_new_note_enabled",
        "moneris_upload_enabled",
        "new_contacts_ui_health_care_team_linked",
        "new_label_print_enabled"
    );
    public static final List<String> OSCAR_PROPERTIES_LAB_SETTINGS_KEYS = Arrays.asList(
        "lab_require_included_chartno",
        "require_acknowledged_docs_confirm_dialog",
        "lab_nomatch_names_enabled",
        "enable_pathnet_labs",
        "enable_hl7text_labs",
        "enable_mds_labs",
        "enable_cml_labs",
        "enable_epsilon_labs"
    );
    public static final List<String> OSCAR_PROPERTIES_RX_SETTINGS_KEYS = Arrays.asList(
        "rx_signature_enabled",
        "rx_show_band_number",
        "rx_show_chart_number",
        "rx_show_hin",
        "rx_hide_dispensing_units",
        "rx_hide_drug_of_choice",
        "enable_rx_internal_dispensing",
        "enable_rx_watermark",
        "enable_rx_allergy_checking",
        "enable_rx_interact_local_drugref_reg_id",
        "enable_signature_tablet",
        "enable_rx_order_by_date",
        "rx_footer_text"
    );

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="name")
    private String name;

    @Column(name="value")
    private String value;

    @Column(name="updateDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    public SystemPreferences() {}
    public SystemPreferences(final String name) {
        this.name = name;
        this.updateDate = new Date();
    }
    public SystemPreferences(String name, String value) {
        this.name = name;
        this.value = value;
        this.updateDate = new Date();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value != null ? value : "";
    }

    /**
     * Gets the system preference as a boolean
     * @return true if value is "true", false otherwise
     */
    public Boolean getValueAsBoolean() {
        return "true".equals(value);
    }

    /**
     * Gets the system preference as an integer
     * @return null if empty, integer value otherwise
     * @throws NumberFormatException when value is non empty and not a number
     */
    public Integer getValueAsInteger() throws NumberFormatException {
        return StringUtils.isBlank(value) ? null : Integer.parseInt(value);
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    /**
     * Null safe helper to convert SystemPreferences value to Boolean
     * @param pref the preference to convert
     * @return null if nothing found otherwise the Boolean value of the preference
     */
    public static Boolean asBoolean(SystemPreferences pref) {
        return pref == null ? null : pref.getValueAsBoolean();
    }

    /**
     * Null safe helper to retrieve SystemPreferences value
     * @param pref the preference to retrieve the value for
     * @return null if nothing found otherwise the preference value
     */
    public static String asString(SystemPreferences pref) {
        return pref == null ? null : pref.getValue();
    }

    /**
     * Null safe helper to convert SystemPreferences to Integer
     * @param pref the preference to convert
     * @return null if nothing found otherwise the Integer value of the preference
     * @throws NumberFormatException when preference value is non empty and not a number
     */
    public static Integer asInteger(SystemPreferences pref) throws NumberFormatException {
        return pref == null ? null : pref.getValueAsInteger();
    }
}
