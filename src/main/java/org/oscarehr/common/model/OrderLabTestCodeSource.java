/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "order_lab_test_source")
public class OrderLabTestCodeSource extends AbstractModel<Integer>
                                   implements Comparable {
    
  private static final long serialVersionUID = 1L;
  
  @Id
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  @Column(name = "id")
  private Integer Id;
  
  @Column(name = "test_code")
  private String testCode;

  @Column(name = "ui_id")
  private String uiId;

  @Column(name = "source_name")
  private String sourceName;

  @Column(name = "province")
  private String province;
    
  @Override
    public Integer getId() {
    return Id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.Id = id;
  }


  public String getTestCode() {
    return testCode;
  }
  public void setTestCode(final String testCode) {
    this.testCode = testCode;
  }

  public String getUiId() {
    return uiId;
  }
  public void setUiId(final String uiId) {
    this.uiId = uiId;
  }

  public String getSourceName() {
    return sourceName;
  }
  public void setSourceName(final String sourceName) {
    this.sourceName = sourceName;
  }

  public String getProvince() {
    return province;
  }
  public void setProvince(final String province) {
    this.province = province;
  }

  @Override
  public int compareTo(final Object o) {
    final OrderLabTestCodeSource other = (OrderLabTestCodeSource) o;
    return getSourceName().compareTo(other.getSourceName());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    
// surrogate key is  not important for equivalence.
//    result = prime * result + ((Id == null) ? 0 : Id.hashCode());
    result = prime * result + ((testCode == null) ? 0 : testCode.hashCode());
    result = prime * result + ((uiId == null) ? 0 : uiId.hashCode());
    result = prime * result + ((sourceName == null) ? 0 : sourceName.hashCode());
    result = prime * result + ((province == null) ? 0 : province.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    OrderLabTestCodeSource other = (OrderLabTestCodeSource) obj;
    if (Id == null) {
      if (other.Id != null)
        return false;
    } else if (!Id.equals(other.Id))
      return false;

    if (testCode == null) {
      if (other.testCode != null)
        return false;
    } else if (!testCode.equals(other.testCode))
      return false;

    if (uiId == null) {
      if (other.uiId != null)
        return false;
    } else if (!uiId.equals(other.uiId))
      return false;

    if (sourceName == null) {
      if (other.sourceName != null)
        return false;
    } else if (!sourceName.equals(other.sourceName))
      return false;

    if (province == null) {
      if (other.province != null)
        return false;
    } else if (!province.equals(other.province))
      return false;

    return true;
  }

  @Override
  public String toString() {
    return "OrderLabTestCodeSource [Id=" + Id + ", testCode="
        + testCode + ", uiId=" + uiId + ", sourceName=" + sourceName
        + ", province=" + province + "]";
  }
}
