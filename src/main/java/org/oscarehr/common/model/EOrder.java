/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@SuppressWarnings("serial")
@MappedSuperclass
public abstract class EOrder extends AbstractModel<Integer> {

	public enum OrderState {
		NEW, FAILED, SUBMITTED
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer Id;
	@Column(name = "eform_data_id")
	private Integer eFormDataId;
	@Column(name = "ext_order_id")
	private String externalOrderId;
	@Column(name = "generated_pdf")
	private String generatedPDF;
	@Enumerated(EnumType.STRING)
	@Column(name = "state")
	private OrderState state;

	@Override
	public Integer getId() {
		return Id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.Id = id;
	}

	/**
	* @return the eform data id
	*/
	public Integer getEFormDataId() {
	    return eFormDataId;
	}

	/**
	 * @param eFormDataId the eFormDataId to set
	 */
	public void setEFormDataId(Integer eFormDataId) {
		this.eFormDataId = eFormDataId;
	}

	/**
	 * @return the external order id
	 */
	public String getExternalOrderId() {
		return externalOrderId;
	}

	/**
	 * @param externalOrderId the externalOrderId to set
	 */
	public void setExternalOrderId(String externalOrderId) {
		this.externalOrderId = externalOrderId;
	}

	/**
	 * @return the generatedPDF
	 */
	public String getGeneratedPDF() {
		return generatedPDF;
	}

	/**
	 * @param generatedPDF the generatedPDF to set
	 */
	public void setGeneratedPDF(String generatedPDF) {
		this.generatedPDF = generatedPDF;
	}

	public OrderState getState() {
		return state;
	}

	public void setState(OrderState state) {
		this.state = state;
	}

	public Integer geteFormDataId() {
		return eFormDataId;
	}

	public void seteFormDataId(Integer eFormDataId) {
		this.eFormDataId = eFormDataId;
	}

	@Override
	public String toString() {
		return "EOrder [Id=" + Id +
				", eFormDataId=" + eFormDataId +
				", externalOrderId=" + externalOrderId +
				", generatedPDF=" + generatedPDF +
				", state=" + state +
				"]";
	}
}