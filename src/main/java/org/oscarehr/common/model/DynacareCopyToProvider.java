/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.model;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "DynacareCopyToProvider")
@AttributeOverrides({
    @AttributeOverride(name = "lastName", column = @Column(name = "lastName")),
    @AttributeOverride(name = "firstName", column = @Column(name = "firstName")),
    @AttributeOverride(name = "middleName", column = @Column(name = "middleName")),
    @AttributeOverride(name = "stateOrProvince", column = @Column(name = "stateOrProvince")),
    @AttributeOverride(name = "zipOrPostal", column = @Column(name = "zipOrPostal")),
    @AttributeOverride(name = "cpso", column = @Column(name = "cpso")),
    @AttributeOverride(name = "billingNumber", column = @Column(name = "billingNumber"))})

public class DynacareCopyToProvider extends EorderCopyToProvider { }