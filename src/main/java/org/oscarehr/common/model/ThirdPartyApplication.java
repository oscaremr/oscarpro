/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import org.hibernate.annotations.CollectionOfElements;

@Entity
public class ThirdPartyApplication extends AbstractModel<Integer> implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue
  private Integer applicationNo;

  @Column(nullable = false)
  private String description;

  @Enumerated(EnumType.STRING)
  private ThirdPartyAppDesignForEnum designedFor;

  private String licenseKey;

  @Column(nullable = false)
  private String name;

  @Enumerated(EnumType.STRING)
  @Column(name = "oauth2AppType", nullable = false)
  private OAuth2ApplicationTypeEnum oauth2AppType;

  @Enumerated(EnumType.STRING)
  private ThirdPartyAppPricingEnum pricing;

  @Column(nullable = false)
  private String registrationNumber;

  @Column(nullable = false)
  private String vendor;

  private String appVersion;

  private String logo;

  @Column(name = "appType", nullable = false)
  @Enumerated(EnumType.STRING)
  private ThirdPartyApplicationTypeEnum type;

  private Date createdDate = new Date();

  @CollectionOfElements(targetElement = String.class)
  @JoinTable(name = "ThirdPartyAppCategory", joinColumns = @JoinColumn(name = "applicationNo"))
  @Column(name = "category")
  private List<String> categories;

  @Enumerated(EnumType.STRING)
  @CollectionOfElements(targetElement = EmrContextEnum.class)
  @JoinTable(name = "ThirdPartyAppEmrContext", joinColumns = @JoinColumn(name = "applicationNo"))
  @Column(name = "emrContext")
  private List<EmrContextEnum> emrContexts;

  @CollectionOfElements(targetElement = String.class)
  @JoinTable(name = "ThirdPartyAppLaunchUri", joinColumns = @JoinColumn(name = "applicationNo"))
  @Column(name = "uri")
  private List<String> launchUris;

  @CollectionOfElements(targetElement = String.class)
  @JoinTable(name = "ThirdPartyAppRedirectUri", joinColumns = @JoinColumn(name = "applicationNo"))
  @Column(name = "uri")
  private List<String> redirectUris;

  @CollectionOfElements(targetElement = String.class)
  @JoinTable(name = "ThirdPartyAppSpecialties", joinColumns = @JoinColumn(name = "applicationNo"))
  @Column(name = "specialty")
  private List<String> specialties;

  @Override
  public Integer getId() {
    return this.applicationNo;
  }

  public ThirdPartyApplication() {
  }

  public ThirdPartyApplication(String description, ThirdPartyAppDesignForEnum designFor, String licenseKey, String name,
          OAuth2ApplicationTypeEnum oauth2AppType, ThirdPartyAppPricingEnum pricing, String registrationNumber,
          String vendor, String appVersion, String logo, ThirdPartyApplicationTypeEnum type, Date createdDate,
          List<String> categories, List<EmrContextEnum> emrContexts, List<String> launchUris, List<String> redirectUris,
          List<String> specialties) {
    super();
    this.description = description;
    this.designedFor = designFor;
    this.licenseKey = licenseKey;
    this.name = name;
    this.oauth2AppType = oauth2AppType;
    this.pricing = pricing;
    this.registrationNumber = registrationNumber;
    this.vendor = vendor;
    this.appVersion = appVersion;
    this.logo = logo;
    this.type = type;
    this.createdDate = createdDate;
    this.categories = categories;
    this.emrContexts = emrContexts;
    this.launchUris = launchUris;
    this.redirectUris = redirectUris;
    this.specialties = specialties;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public ThirdPartyAppDesignForEnum getDesignedFor() {
    return designedFor;
  }

  public void setDesignedFor(ThirdPartyAppDesignForEnum designedFor) {
    this.designedFor = designedFor;
  }

  public String getLicenseKey() {
    return licenseKey;
  }

  public void setLicenseKey(String licenseKey) {
    this.licenseKey = licenseKey;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public OAuth2ApplicationTypeEnum getOauth2AppType() {
    return oauth2AppType;
  }

  public void setOauth2AppType(OAuth2ApplicationTypeEnum oauth2AppType) {
    this.oauth2AppType = oauth2AppType;
  }

  public ThirdPartyAppPricingEnum getPricing() {
    return pricing;
  }

  public void setPricing(ThirdPartyAppPricingEnum pricing) {
    this.pricing = pricing;
  }

  public String getRegistrationNumber() {
    return registrationNumber;
  }

  public void setRegistrationNumber(String registrationNumber) {
    this.registrationNumber = registrationNumber;
  }

  public String getVendor() {
    return vendor;
  }

  public void setVendor(String vendor) {
    this.vendor = vendor;
  }

  public String getAppVersion() {
    return appVersion;
  }

  public void setAppVersion(String appVersion) {
    this.appVersion = appVersion;
  }

  public String getLogo() {
    return logo;
  }

  public void setLogo(String logo) {
    this.logo = logo;
  }

  public ThirdPartyApplicationTypeEnum getType() {
    return type;
  }

  public void setType(ThirdPartyApplicationTypeEnum type) {
    this.type = type;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public List<String> getCategories() {
    return categories;
  }

  public void setCategories(List<String> categories) {
    this.categories = categories;
  }

  public List<EmrContextEnum> getEmrContexts() {
    return emrContexts;
  }

  public void setEmrContexts(List<EmrContextEnum> emrContexts) {
    this.emrContexts = emrContexts;
  }

  public List<String> getLaunchUris() {
    return launchUris;
  }

  public void setLaunchUris(List<String> launchUris) {
    this.launchUris = launchUris;
  }

  public List<String> getRedirectUris() {
    return redirectUris;
  }

  public void setRedirectUris(List<String> redirectUris) {
    this.redirectUris = redirectUris;
  }

  public List<String> getSpecialties() {
    return specialties;
  }

  public void setSpecialties(List<String> specialties) {
    this.specialties = specialties;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((appVersion == null) ? 0 : appVersion.hashCode());
    result = prime * result + ((applicationNo == null) ? 0 : applicationNo.hashCode());
    result = prime * result + ((categories == null) ? 0 : categories.hashCode());
    result = prime * result + ((createdDate == null) ? 0 : createdDate.hashCode());
    result = prime * result + ((description == null) ? 0 : description.hashCode());
    result = prime * result + ((designedFor == null) ? 0 : designedFor.hashCode());
    result = prime * result + ((emrContexts == null) ? 0 : emrContexts.hashCode());
    result = prime * result + ((launchUris == null) ? 0 : launchUris.hashCode());
    result = prime * result + ((licenseKey == null) ? 0 : licenseKey.hashCode());
    result = prime * result + ((logo == null) ? 0 : logo.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((oauth2AppType == null) ? 0 : oauth2AppType.hashCode());
    result = prime * result + ((pricing == null) ? 0 : pricing.hashCode());
    result = prime * result + ((redirectUris == null) ? 0 : redirectUris.hashCode());
    result = prime * result + ((registrationNumber == null) ? 0 : registrationNumber.hashCode());
    result = prime * result + ((specialties == null) ? 0 : specialties.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    result = prime * result + ((vendor == null) ? 0 : vendor.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    ThirdPartyApplication other = (ThirdPartyApplication) obj;
    if (appVersion == null) {
      if (other.appVersion != null)
        return false;
    } else if (!appVersion.equals(other.appVersion))
      return false;
    if (applicationNo == null) {
      if (other.applicationNo != null)
        return false;
    } else if (!applicationNo.equals(other.applicationNo))
      return false;
    if (categories == null) {
      if (other.categories != null)
        return false;
    } else if (!categories.equals(other.categories))
      return false;
    if (createdDate == null) {
      if (other.createdDate != null)
        return false;
    } else if (!createdDate.equals(other.createdDate))
      return false;
    if (description == null) {
      if (other.description != null)
        return false;
    } else if (!description.equals(other.description))
      return false;
    if (designedFor != other.designedFor)
      return false;
    if (emrContexts == null) {
      if (other.emrContexts != null)
        return false;
    } else if (!emrContexts.equals(other.emrContexts))
      return false;
    if (launchUris == null) {
      if (other.launchUris != null)
        return false;
    } else if (!launchUris.equals(other.launchUris))
      return false;
    if (licenseKey == null) {
      if (other.licenseKey != null)
        return false;
    } else if (!licenseKey.equals(other.licenseKey))
      return false;
    if (logo == null) {
      if (other.logo != null)
        return false;
    } else if (!logo.equals(other.logo))
      return false;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    if (oauth2AppType != other.oauth2AppType)
      return false;
    if (pricing != other.pricing)
      return false;
    if (redirectUris == null) {
      if (other.redirectUris != null)
        return false;
    } else if (!redirectUris.equals(other.redirectUris))
      return false;
    if (registrationNumber == null) {
      if (other.registrationNumber != null)
        return false;
    } else if (!registrationNumber.equals(other.registrationNumber))
      return false;
    if (specialties == null) {
      if (other.specialties != null)
        return false;
    } else if (!specialties.equals(other.specialties))
      return false;
    if (type != other.type)
      return false;
    if (vendor == null) {
      if (other.vendor != null)
        return false;
    } else if (!vendor.equals(other.vendor))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "ThirdPartyApplication [applicationNo=" + applicationNo + ", description=" + description + ", designedFor="
            + designedFor + ", licenseKey=" + licenseKey + ", name=" + name + ", oauth2AppType=" + oauth2AppType
            + ", pricing=" + pricing + ", registrationNumber=" + registrationNumber + ", vendor=" + vendor
            + ", appVersion=" + appVersion + ", logo=" + logo + ", type=" + type + ", createdDate=" + createdDate
            + ", categories=" + categories + ", emrContexts=" + emrContexts + ", launchUris=" + launchUris
            + ", redirectUris=" + redirectUris + ", specialties=" + specialties + "]";
  }

}