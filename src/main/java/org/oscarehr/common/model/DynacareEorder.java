/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.model;

import java.awt.Font;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import javax.imageio.ImageIO;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.linear.code39.Code39Barcode;
import oscar.eform.util.BarcodeUtil;

@Entity
@Table(name = "DynacareEorder")

@AttributeOverrides({
    @AttributeOverride(name = "eFormDataId", column = @Column(name = "eFormDataId")),
    @AttributeOverride(name = "externalOrderId", column = @Column(name = "extOrderId")),
    @AttributeOverride(name = "generatedPDF", column = @Column(name = "generatedPdf"))})
public class DynacareEorder extends EOrder {

  private static final long serialVersionUID = 1L;

  @Column(name = "barcode")
  @Getter @Setter private String barcode;

  /** @return the serialversionuid */
  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  /**
   * This method generates a barcode image from the eOrder object's barcode attribute.
   *
   * @return A byte array (Base64 encoded) of the barcode image
   * @throws BarcodeException
   * @throws IOException
   */
  public String generateBarcodeImage() throws BarcodeException, IOException {
    val barcodeImage = (Code39Barcode) BarcodeFactory.createCode39(
        BarcodeUtil.addCheckDigit(this.barcode), false);
    BarcodeUtil.changeBarcodeLabel(barcodeImage, this.barcode);
    barcodeImage.setBarHeight(40);
    barcodeImage.setBarWidth(2);
    barcodeImage.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
    val bufferedImage = BarcodeImageHandler.getImage(barcodeImage);
    val output = new ByteArrayOutputStream();
    ImageIO.write(bufferedImage, "png", output);
    return Base64.getEncoder().encodeToString(output.toByteArray());
  }

  @PreRemove
  protected void jpaPreventDelete() {
    throw (new UnsupportedOperationException("Remove is not allowed for this type of item."));
  }
}
