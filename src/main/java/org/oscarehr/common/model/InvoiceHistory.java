package org.oscarehr.common.model;

import com.sun.istack.NotNull;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "invoice_history")
public class InvoiceHistory extends AbstractModel<Integer> implements Serializable {
    public enum ActionType {
        ADDED("A"), DELETED("D"), UPDATED("U");
        String code;
        
        ActionType(String code) {
            this.code = code;
        }

        public String getCode() {
            return code;
        }
        
        public static ActionType getByCode(@NotNull String code) {
            code = code.toUpperCase();
            
            switch (code) {
                case "A":
                    return ActionType.ADDED;
                case "D":
                    return ActionType.DELETED;
                case "U":
                    return ActionType.UPDATED;
                default:
                    return null;
            }
        }
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "ch1_id")
    private Integer billingNo;

    @Column(name = "item_id")
    private Integer itemNo = 0;

    private String field;

    @Column(name = "val")
    private String value = "";

    @Column(name = "old_value")
    private String oldValue = "";

    private String action;

    @Column(name = "provider_no")
    private String providerNo;

    @Column(name = "updated_timestamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp = new Timestamp(System.currentTimeMillis());

    @Column(name = "disk_id")
    private Integer diskId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "provider_no", referencedColumnName = "provider_no", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private Provider provider;

    public InvoiceHistory() {
    }
    
    public InvoiceHistory(Integer billingNo, String field, String oldValue, String value, ActionType actionType, String updatedProvider) {
        this.billingNo = billingNo;
        this.field = field;
        this.oldValue = oldValue;
        this.value = value;
        this.action = actionType.getCode();
        this.providerNo = updatedProvider;
    }

    public InvoiceHistory(Integer billingNo, String field, String oldValue, String value, ActionType actionType, String updatedProvider, Integer diskId) {
        this.billingNo = billingNo;
        this.field = field;
        this.oldValue = oldValue;
        this.value = value;
        this.action = actionType.getCode();
        this.providerNo = updatedProvider;
        this.diskId = diskId;
    }

    public InvoiceHistory(Integer billingNo, Integer itemNo, String field, String oldValue, String value, ActionType actionType, String updatedProvider) {
        this.billingNo = billingNo;
        this.itemNo = itemNo;
        this.field = field;
        this.oldValue = oldValue;
        this.value = value;
        this.action = actionType.getCode();
        this.providerNo = updatedProvider;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBillingNo() {
        return billingNo;
    }

    public void setBillingNo(Integer billingNo) {
        this.billingNo = billingNo;
    }

    public Integer getItemNo() {
        return itemNo;
    }

    public void setItemNo(Integer itemNo) {
        this.itemNo = itemNo;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getProviderNo() {
        return providerNo;
    }

    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Integer getDiskId() {
        return diskId;
    }

    public void setDiskId(Integer diskId) {
        this.diskId = diskId;
    }

    @PrePersist
    protected void preSave(){
        this.timestamp = new Timestamp(System.currentTimeMillis());
    }
}
