package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "billing_rule")
public class BillingRule extends AbstractModel<Integer> implements Serializable {
    public final static String[] ON_PEDIATRIC_CODES = {"K267A", "K269A"};
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "service_code")
    private String serviceCode;
    @Column(name = "bill_region")
    private String billRegion = "";
    @Column(name = "enabled")
    private Boolean enabled = true;

    public BillingRule() {
        
    }

    public BillingRule(String serviceCode, String billRegion, Boolean enabled) {
        this.serviceCode = serviceCode;
        this.billRegion = billRegion;
        this.enabled = enabled;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getServiceCode() {
        return serviceCode;
    }
    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getBillRegion() {
        return billRegion;
    }
    public void setBillRegion(String billRegion) {
        this.billRegion = billRegion;
    }

    public Boolean getEnabled() {
        return enabled;
    }
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }
}
