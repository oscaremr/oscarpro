/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.model;

import org.oscarehr.ws.rest.to.model.EmailSpecificationTo1;

import java.util.List;

public class EmailRequest {

    private Demographic recipient;
    private EmailSpecificationTo1.EmailType emailType;
    private String subject;
    private String body;
    private List<EmailAttachment> attachments;

    public EmailRequest() {
    }

    public EmailRequest(Demographic demographic, String subject, String body, List<EmailAttachment> attachments,
                        EmailSpecificationTo1.EmailType emailType) {
        this.recipient = demographic;
        this.subject = subject;
        this.body = body;
        this.attachments = attachments;
        this.emailType = emailType;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public List<EmailAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<EmailAttachment> attachments) {
        this.attachments = attachments;
    }

    public Demographic getRecipient() {
        return recipient;
    }

    public void setRecipient(Demographic recipient) {
        this.recipient = recipient;
    }

    public EmailSpecificationTo1.EmailType getEmailType() {
        return emailType;
    }

    public void setEmailType(EmailSpecificationTo1.EmailType emailType) {
        this.emailType = emailType;
    }
}
