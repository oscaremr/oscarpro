/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class OceanSetting extends AbstractModel<Integer> {
  @Id
  @GeneratedValue
  private Integer id;
  private String settings;

  public OceanSetting() {
  }

  public OceanSetting(String settings) {
    this.settings = settings;
  }

  @Override
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }

  public String getSettings() {
    return settings;
  }
  public void setSettings(String settings) {
    this.settings = settings;
  }
}
