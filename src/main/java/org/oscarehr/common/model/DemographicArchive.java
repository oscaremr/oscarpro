/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "demographicArchive")
public class DemographicArchive extends AbstractModel<Long> implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "demographic_no")
	private Integer demographicNo;

	@Column(name = "title")
	private String title;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "address")
	private String address;

	@Column(name = "city")
	private String city;

	@Column(name = "province")
	private String province;

	@Column(name = "postal")
	private String postal;

	@Column(name = "phone")
	private String phone;

	@Column(name = "phone2")
	private String phone2;

	@Column(name = "email")
	private String email;

	private String myOscarUserName;

	@Column(name = "year_of_birth")
	private String yearOfBirth;

	@Column(name = "month_of_birth")
	private String monthOfBirth;

	@Column(name = "date_of_birth")
	private String dateOfBirth;

	@Column(name = "hin")
	private String hin;

	@Column(name = "ver")
	private String ver;

	@Column(name = "roster_status")
	private String rosterStatus;

	@Column(name = "roster_date")
	@Temporal(TemporalType.DATE)
	private Date rosterDate;

	@Column(name = "roster_termination_date")
	@Temporal(TemporalType.DATE)
	private Date rosterTerminationDate;

	@Column(name = "roster_termination_reason")
	private String rosterTerminationReason;

	@Column(name = "patient_status")
	private String patientStatus;

	@Column(name = "patient_status_date")
	@Temporal(TemporalType.DATE)
	private Date patientStatusDate;

	@Column(name = "date_joined")
	@Temporal(TemporalType.DATE)
	private Date dateJoined;

	@Column(name = "chart_no")
	private String chartNo;

	@Column(name = "official_lang")
	private String officialLanguage;

	@Column(name = "spoken_lang")
	private String spokenLanguage;

	@Column(name = "provider_no")
	private String providerNo;

	@Column(name = "sex")
	private String sex;

	@Column(name = "end_date")
	@Temporal(TemporalType.DATE)
	private Date endDate;

	@Column(name = "eff_date")
	@Temporal(TemporalType.DATE)
	private Date effDate;

	@Column(name = "pcn_indicator")
	private String pcnIndicator;

	@Column(name = "hc_type")
	private String hcType;

	@Column(name = "hc_renew_date")
	@Temporal(TemporalType.DATE)
	private Date hcRenewDate;

	@Column(name = "alias")
	private String alias;

	@Column(name = "previousAddress")
	private String previousAddress;

	@Column(name = "children")
	private String children;

	@Column(name = "sourceOfIncome")
	private String sourceOfIncome;

	@Column(name = "citizenship")
	private String citizenship;

	@Column(name = "sin")
	private String sin;

	@Column(name = "country_of_origin")
	private String countryOfOrigin;

	@Column(name = "newsletter")
	private String newsletter;

	@Column(name = "anonymous")
	private String anonymous;

	@Column(name = "lastUpdateUser")
	private String lastUpdateUser;

	@Column(name = "lastUpdateDate")
	@Temporal(TemporalType.DATE)
	private Date lastUpdateDate;
	private Integer genderId;
	private Integer pronounId;
	private String gender;
	private String pronoun;
	@Column(name = "preferredName")
	private String preferredName;
	private String patientType;
	private String patientId;
	private String portalUserId;
	private Boolean consentToUseEmailForCare;
	private Boolean consentToUseEmailForEOrder;
	private Integer primarySystemId;

	public DemographicArchive(Demographic demographic) {
		this.address = demographic.getAddress();
		this.alias = demographic.getAlias();
		this.anonymous = demographic.getAnonymous();
		this.chartNo = demographic.getChartNo();
		this.children = demographic.getChildren();
		this.citizenship = demographic.getCitizenship();
		this.city = demographic.getCity();
		this.countryOfOrigin = demographic.getCountryOfOrigin();
		this.dateJoined = demographic.getDateJoined();
		this.dateOfBirth = demographic.getDateOfBirth();
		this.demographicNo = demographic.getDemographicNo();
		this.effDate = demographic.getEffDate();
		this.email = demographic.getEmail();
		this.endDate = demographic.getEndDate();
		this.firstName = demographic.getFirstName();
		this.hcRenewDate = demographic.getHcRenewDate();
		this.hcType = demographic.getHcType();
		this.hin = demographic.getHin();
		this.lastName = demographic.getLastName();
		this.lastUpdateDate = new Date();
		this.lastUpdateUser = demographic.getLastUpdateUser();
		this.monthOfBirth = demographic.getMonthOfBirth();
		this.myOscarUserName = demographic.getMyOscarUserName();
		this.newsletter = demographic.getNewsletter();
		this.officialLanguage = demographic.getOfficialLanguage();
		this.patientStatus = demographic.getPatientStatus();
		this.patientStatusDate = demographic.getPatientStatusDate();
		this.pcnIndicator = demographic.getPcnIndicator();
		this.phone = demographic.getPhone();
		this.phone2 = demographic.getPhone2();
		this.postal = demographic.getPostal();
		this.previousAddress = demographic.getPreviousAddress();
		this.providerNo = demographic.getProviderNo();
		this.province = demographic.getProvince();
		this.rosterDate = demographic.getRosterDate();
		this.rosterStatus = demographic.getRosterStatus();
		this.rosterTerminationDate = demographic.getRosterTerminationDate();
		this.rosterTerminationReason = demographic.getRosterTerminationReason();
		this.sex = demographic.getSex();
		this.sin = demographic.getSin();
		this.sourceOfIncome = demographic.getSourceOfIncome();
		this.spokenLanguage = demographic.getSpokenLanguage();
		this.title = demographic.getTitle();
		this.ver = demographic.getVer();
		this.yearOfBirth = demographic.getYearOfBirth();
		this.genderId = demographic.getGenderId();
		this.pronounId = demographic.getPronounId();
		this.gender = demographic.getGenderIdentityValue();
		this.pronoun = demographic.getPronounValue();
		this.preferredName = demographic.getPreferredName();
		this.patientType = demographic.getPatientType();
		this.patientId = demographic.getPatientId();
		this.portalUserId = demographic.getPortalUserId();
		this.consentToUseEmailForCare = demographic.getConsentToUseEmailForCare();
		this.consentToUseEmailForEOrder = demographic.getConsentToUseEmailForEOrder();
		this.primarySystemId = demographic.getPrimarySystemId();
	}
}
