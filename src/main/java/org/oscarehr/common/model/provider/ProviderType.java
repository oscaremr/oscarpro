package org.oscarehr.common.model.provider;

public enum ProviderType {
  ADMIN,
  DENTIST,
  DOCTOR,
  MIDWIFE,
  NONE,
  NURSE,
  RECEPTIONIST,
  RESIDENT;

  public static ProviderType fromStringValue(final String providerType) {
    if (null == providerType) {
      return NONE;
    }
    switch (providerType.toLowerCase()) {
      case "admin":
        return ADMIN;
      case "doctor":
        return DOCTOR;
      case "midwife":
        return MIDWIFE;
      case "nurse":
        return NURSE;
      case "receptionist":
        return RECEPTIONIST;
      case "resident":
        return RESIDENT;
      case "none":
      default:
        return NONE;
    }
  }

  public String toStringValue() {
    switch (this) {
      case ADMIN:
        return "admin";
      case DOCTOR:
        return "doctor";
      case MIDWIFE:
        return "midwife";
      case NONE:
        return "none";
      case NURSE:
        return "nurse";
      case RECEPTIONIST:
        return "receptionist";
      case RESIDENT:
        return "resident";
      default:
        return "";
    }
  }
}
