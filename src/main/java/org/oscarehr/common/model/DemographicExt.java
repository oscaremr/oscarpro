/**
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto. All Rights Reserved. This software is published under the GPL GNU General Public
 * License. This program is free software; you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */
package org.oscarehr.common.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Entity
@NoArgsConstructor
@Table(name = "demographicExt")
public class DemographicExt extends AbstractSubscriptionModel<Integer> implements Serializable {

  @Transient
  private int hashCode = Integer.MIN_VALUE; // primary key

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "demographic_no")
  private Integer demographicNo;

  @Column(name = "provider_no")
  private String providerNo;

  @Column(name = "key_val")
  private String key;

  @Column(name = "date_time")
  @Temporal(TemporalType.TIMESTAMP)
  private java.util.Date dateCreated;

  private String value;
  private boolean hidden;

  public enum DemographicProperty { PHU, EmploymentStatus, HasPrimaryCarePhyscian,
    	informedConsent, privacyConsent, usSigned, fNationCom, statusNum, area, ethnicity,
    	cytolNum, wPhoneExt, hPhoneExt, demo_cell, phoneComment }

  @PrePersist
  @PreUpdate
  protected void prePersist() {
    this.dateCreated = new Date();
  }

  /**
   * Creates a demographicExt object with following parameters:
   */
  public DemographicExt(Integer id) {
    this.setId(id);
  }

  /**
   * Creates a demographicExt object populated with following parameters:
   *
   * @param providerNo    the provider who created the object
   * @param demographicNo the demographic the extension is for
   * @param key           they key value for the data
   * @param value         the actual value for the data
   */
  public DemographicExt(String providerNo, Integer demographicNo, String key, String value) {
    this.providerNo = providerNo;
    this.demographicNo = demographicNo;
    this.key = key;
    this.value = value;
    this.dateCreated = new Date();
  }

  /**
   * Creates a demographicExt object populated with following parameters:
   *
   * @param id            the id of the extension object to use
   * @param providerNo    the provider who created the object
   * @param demographicNo the demographic the extension is for
   * @param key           they key value for the data
   * @param value         the actual value for the data
   */
  public DemographicExt(
      Integer id,
      String providerNo,
      Integer demographicNo,
      String key,
      String value
  ) {
    this.id = id;
    this.providerNo = providerNo;
    this.demographicNo = demographicNo;
    this.key = key;
    this.value = value;
  }

  /**
   * Creates a demographicExt object populated with following parameters:
   *
   * @param strId         the id of the extension object to use
   * @param providerNo    the provider who created the object
   * @param demographicNo the demographic the extension is for
   * @param key           they key value for the data
   * @param value         the actual value for the data
   */
  public  DemographicExt(
      String strId,
      String providerNo,
      Integer demographicNo,
      String key,
      String value
  ) {
    try {
      this.id = Integer.parseInt(strId);
    } catch (NumberFormatException e) {
      this.id = null;
    }
    this.providerNo = providerNo;
    this.demographicNo = demographicNo;
    this.key = key;
    this.value = value;
  }

  public void setDemographicExtKey(DemographicExtKey key) {
    this.key = key.getKey();
  }

  @Override
  public String getModule() {
    return Demographic.FHIR_RESOURCE_PATIENT;
  }

  @Override
  public String getModuleId() {
    return String.valueOf(demographicNo);
  }
}
