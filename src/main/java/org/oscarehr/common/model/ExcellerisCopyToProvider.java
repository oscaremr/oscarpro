/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */

package org.oscarehr.common.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "excelleris_copy_to_providers")
public class ExcellerisCopyToProvider extends EorderCopyToProvider implements Serializable {

	@Column(name = "ontario_lifelabs_id")
	private String ontarioLifeLabsId;

	@Column(name = "rover_id")
	private String roverId;

	@Column(name = "on_provincial_govt_id")
	private String onProvincialGovtId;

	@Column(name = "pathnet_code")
	private String pathnetCode;

	@Column(name = "deleted")
	private Boolean deleted;

	@Column(name = "visible")
	private Boolean visible;

	public String getOntarioLifeLabsId() {
		return ontarioLifeLabsId;
	}


	public void setOntariaLifeLabsId(String ontariaLifeLabsId) {
		this.ontarioLifeLabsId = ontariaLifeLabsId;
	}

	public String getRoverId() {
		return roverId;
	}


	public void setRoverId(String roverId) {
		this.roverId = roverId;
	}

	public String getOnProvincialGovtId() {
		return onProvincialGovtId;
	}

	public void setOnProvincialGovtId(String onProvincialGovtId) {
		this.onProvincialGovtId = onProvincialGovtId;
	}

	public String getPathnetCode() {
		return pathnetCode;
	}

	public void setPathnetCode(String pathnetCode) {
		this.pathnetCode = pathnetCode;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public void setOntarioLifeLabsId(String ontarioLifeLabsId) {
		this.ontarioLifeLabsId = ontarioLifeLabsId;
	}

	public String getFormattedName() {
		return getLastName() + "," + getFirstName();
	}

}
