package org.oscarehr.common.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "erefer_attachment_manager_data")
@Getter
@Setter
public class EReferAttachmentManagerData extends AbstractModel<Integer> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @ManyToOne
  @JoinColumn(name = "erefer_attachment_id", referencedColumnName = "id")
  private EReferAttachment eReferAttachment;

  private String printable;

  public EReferAttachmentManagerData() {
  }

  public EReferAttachmentManagerData(final EReferAttachment eReferAttachment,
      final String printable) {
    this.eReferAttachment = eReferAttachment;
    this.printable = printable;
  }
}
