/**
 * Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.oscarehr.common.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="dxresearch")
public class Dxresearch  extends AbstractModel<Integer> implements java.io.Serializable {


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="dxresearch_no")
    private Integer dxresearchNo;
	@Column(name="demographic_no")
    private Integer demographicNo;
	@Column(name="start_date")
	@Temporal(TemporalType.DATE)
    private Date startDate;
	@Column(name="update_date")
	@Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    private Character status;
    @Column(name="dxresearch_code")
    private String dxresearchCode;
    @Column(name="coding_system")
    private String codingSystem;
    private byte association;

    private String providerNo;
    
    @Column(name = "isAvailable")
    private boolean isAvailable;

    @Column(name = "autoSyncDate")
    private Date autoSyncDate;

    @Column(name = "lastSyncedDate")
    private Date lastSyncedDate;
    
    public Dxresearch() {
    }


    public Dxresearch(byte association) {
        this.association = association;
    }
    public Dxresearch(Integer demographicNo, Date startDate, Date updateDate, Character status, String dxresearchCode, String codingSystem, byte association, String providerNo) {
       this.demographicNo = demographicNo;
       this.startDate = startDate;
       this.updateDate = updateDate;
       this.status = status;
       this.dxresearchCode = dxresearchCode;
       this.codingSystem = codingSystem;
       this.association = association;
       this.providerNo = providerNo;
    }

    public Integer getId() {
    	return getDxresearchNo();
    }

    public Integer getDxresearchNo() {
        return this.dxresearchNo;
    }

    public void setDxresearchNo(Integer dxresearchNo) {
        this.dxresearchNo = dxresearchNo;
    }
    public Integer getDemographicNo() {
        return this.demographicNo;
    }

    public void setDemographicNo(Integer demographicNo) {
        this.demographicNo = demographicNo;
    }
    public Date getStartDate() {
        return this.startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getUpdateDate() {
        return this.updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
    public Character getStatus() {
        return this.status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }
    public String getDxresearchCode() {
        return this.dxresearchCode;
    }

    public void setDxresearchCode(String dxresearchCode) {
        this.dxresearchCode = dxresearchCode;
    }
    public String getCodingSystem() {
        return this.codingSystem;
    }

    public void setCodingSystem(String codingSystem) {
        this.codingSystem = codingSystem;
    }
    public byte getAssociation() {
        return this.association;
    }

    public void setAssociation(byte association) {
        this.association = association;
    }
    
    

    public String getProviderNo() {
		return providerNo;
	}


	public void setProviderNo(String providerNo) {
		this.providerNo = providerNo;
	}

	public boolean isAvailable() {
		return isAvailable;
	}


	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}


	public Date getAutoSyncDate() {
		return autoSyncDate;
	}


	public void setAutoSyncDate(Date autoSyncDate) {
		this.autoSyncDate = autoSyncDate;
	}


	public Date getLastSyncedDate() {
		return lastSyncedDate;
	}


	public void setLastSyncedDate(Date lastSyncedDate) {
		this.lastSyncedDate = lastSyncedDate;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!super.equals(obj)) return false;
		if (getClass() != obj.getClass()) return false;
		Dxresearch other = (Dxresearch) obj;
		if (association != other.association) return false;
		if (autoSyncDate == null) {
			if (other.autoSyncDate != null) return false;
		} else if (!autoSyncDate.equals(other.autoSyncDate)) return false;
		if (codingSystem == null) {
			if (other.codingSystem != null) return false;
		} else if (!codingSystem.equals(other.codingSystem)) return false;
		if (demographicNo == null) {
			if (other.demographicNo != null) return false;
		} else if (!demographicNo.equals(other.demographicNo)) return false;
		if (dxresearchCode == null) {
			if (other.dxresearchCode != null) return false;
		} else if (!dxresearchCode.equals(other.dxresearchCode)) return false;
		if (dxresearchNo == null) {
			if (other.dxresearchNo != null) return false;
		} else if (!dxresearchNo.equals(other.dxresearchNo)) return false;
		if (isAvailable != other.isAvailable) return false;
		if (lastSyncedDate == null) {
			if (other.lastSyncedDate != null) return false;
		} else if (!lastSyncedDate.equals(other.lastSyncedDate)) return false;
		if (providerNo == null) {
			if (other.providerNo != null) return false;
		} else if (!providerNo.equals(other.providerNo)) return false;
		if (startDate == null) {
			if (other.startDate != null) return false;
		} else if (!startDate.equals(other.startDate)) return false;
		if (status == null) {
			if (other.status != null) return false;
		} else if (!status.equals(other.status)) return false;
		if (updateDate == null) {
			if (other.updateDate != null) return false;
		} else if (!updateDate.equals(other.updateDate)) return false;
		return true;
	}

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + association;
		result = prime * result + ((autoSyncDate == null) ? 0 : autoSyncDate.hashCode());
		result = prime * result + ((codingSystem == null) ? 0 : codingSystem.hashCode());
		result = prime * result + ((demographicNo == null) ? 0 : demographicNo.hashCode());
		result = prime * result + ((dxresearchCode == null) ? 0 : dxresearchCode.hashCode());
		result = prime * result + ((dxresearchNo == null) ? 0 : dxresearchNo.hashCode());
		result = prime * result + (isAvailable ? 1231 : 1237);
		result = prime * result + ((lastSyncedDate == null) ? 0 : lastSyncedDate.hashCode());
		result = prime * result + ((providerNo == null) ? 0 : providerNo.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((updateDate == null) ? 0 : updateDate.hashCode());
		return result;
	}


    @PrePersist
	@PreUpdate
	protected void jpaUpdateDate() {
		this.updateDate = new Date();
	}

}
