package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="billing_msp_facility_mapping_blackwhite")
public class MSPFacilityMappingBlackWhite extends AbstractModel<String> {

    @Id
    @Column(name="provider_no")
    private String providerNo;
    
    @Column(name="clinic_id")
    private Integer clinicId;
    
    public String getId() {
        return providerNo;
    }

    public String getProviderNo() {
        return providerNo;
    }

    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }

    public Integer getClinicId() {
        return clinicId;
    }

    public void setClinicId(Integer clinicId) {
        this.clinicId = clinicId;
    }
}
