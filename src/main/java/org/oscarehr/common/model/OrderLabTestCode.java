/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "OrderLabTestCodes")
public class OrderLabTestCode extends AbstractModel<Integer> {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer Id;


	@Column(name = "Patient_Instructions")
	private String patientInstructions;

	@Column(name = "Searchable")
	private boolean searchable;

	@Column(name = "TestOnForm")
	private boolean testOnForm;

	@Column(name = "RequireAppointment")
	private boolean requireAppointment;

	@Column(name = "PatientPay")
	private Boolean patientPay;

	@Column(name = "Test_Name")
	private String testName;

	@Column(name = "Print_Heading")
	private String printHeading;

	@Column(name = "Lab_Test_Code")
	private String labTestCode;

	@Column(name = "Base_BCOTC")
	private String baseBCOTC;
        
	

	@Override
        public Integer getId() {
	        return Id;
        }

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.Id = id;
	}

	public String getPatientInstructions() {
		return patientInstructions;
	}

	public void setPatientInstructions(String patientInstructions) {
		this.patientInstructions = patientInstructions;
	}

	public boolean isSearchable() {
		return searchable;
	}

	public void setSearchable(boolean searchable) {
		this.searchable = searchable;
	}

	public boolean isTestOnForm() {
		return testOnForm;
	}

	public void setTestOnForm(boolean testOnForm) {
		this.testOnForm = testOnForm;
	}

	public boolean isRequireAppointment() {
		return requireAppointment;
	}

	public void setRequireAppointment(boolean requireAppointment) {
		this.requireAppointment = requireAppointment;
	}

	public Boolean getPatientPay() {
		return patientPay;
	}

	public void setPatientPay(Boolean patientPay) {
		this.patientPay = patientPay;
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}

	public String getPrintHeading() {
		return printHeading;
	}

	public void setPrintHeading(String printHeading) {
		this.printHeading = printHeading;
	}

	public String getLabTestCode() {
		return labTestCode;
	}

	public void setLabTestCode(String labTestCode) {
		this.labTestCode = labTestCode;
	}

	public String getBaseBCOTC() {
		return baseBCOTC;
	}

	public void setBaseBCOTC(String baseBCOTC) {
		this.baseBCOTC = baseBCOTC;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((Id == null) ? 0 : Id.hashCode());
		result = prime * result + ((baseBCOTC == null) ? 0 : baseBCOTC.hashCode());
		result = prime * result + ((labTestCode == null) ? 0 : labTestCode.hashCode());
		result = prime * result + ((patientInstructions == null) ? 0 : patientInstructions.hashCode());
		result = prime * result + ((patientPay == null) ? 0 : patientPay.hashCode());
		result = prime * result + ((printHeading == null) ? 0 : printHeading.hashCode());
		result = prime * result + (requireAppointment ? 1231 : 1237);
		result = prime * result + (searchable ? 1231 : 1237);
		result = prime * result + ((testName == null) ? 0 : testName.hashCode());
		result = prime * result + (testOnForm ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderLabTestCode other = (OrderLabTestCode) obj;
		if (Id == null) {
			if (other.Id != null)
				return false;
		} else if (!Id.equals(other.Id))
			return false;
		if (baseBCOTC == null) {
			if (other.baseBCOTC != null)
				return false;
		} else if (!baseBCOTC.equals(other.baseBCOTC))
			return false;
		if (labTestCode == null) {
			if (other.labTestCode != null)
				return false;
		} else if (!labTestCode.equals(other.labTestCode))
			return false;
		if (patientInstructions == null) {
			if (other.patientInstructions != null)
				return false;
		} else if (!patientInstructions.equals(other.patientInstructions))
			return false;
		if (patientPay == null) {
			if (other.patientPay != null)
				return false;
		} else if (!patientPay.equals(other.patientPay))
			return false;
		if (printHeading == null) {
			if (other.printHeading != null)
				return false;
		} else if (!printHeading.equals(other.printHeading))
			return false;
		if (requireAppointment != other.requireAppointment)
			return false;
		if (searchable != other.searchable)
			return false;
		if (testName == null) {
			if (other.testName != null)
				return false;
		} else if (!testName.equals(other.testName))
			return false;
		if (testOnForm != other.testOnForm)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "OrderLabTestCode [Id=" + Id + ", patientInstructions=" + patientInstructions + ", searchable="
				+ searchable + ", testOnForm=" + testOnForm + ", requireAppointment=" + requireAppointment
				+ ", patientPay=" + patientPay + ", testName=" + testName + ", printHeading=" + printHeading
				+ ", labTestCode=" + labTestCode + ", baseBCOTC=" + baseBCOTC + "]";
	}

}
