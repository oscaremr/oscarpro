/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import org.oscarehr.common.model.AbstractModel;

import java.util.Date;

@Entity
@Table(name = "onaccountbills")
@Data
public class OnAccountBill extends AbstractModel<Integer> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Column(name = "invoiceNo")
    private Integer id;
    @Column
    private Integer demographicNo;
    @Column
    private String date;
    @Column
    private String dxCode;
    @Column
    private String primaryPhysician;
    @Column
    private String billingPhysician;
    @Column
    private String referringPhysician;
    @Column
    private Double totalServices;
    @Column
    private Double hst;
    @Column
    private Double amountCharged;
    @Column
    private Double balance;
    @Column
    private String notes;
    @Column
    private String billStatus;
    @Column
    private String creationDate;
}