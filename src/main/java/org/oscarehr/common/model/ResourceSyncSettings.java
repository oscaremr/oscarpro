/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class ResourceSyncSettings {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Integer id;

  @Column(name = "resourceType")
  @Enumerated(EnumType.STRING)
  private ResourceTypeEnum resourceType;

  @Column(name = "autoSync")
  private boolean autoSync;

  @Column(name = "createdDateTime")
  private Date createdDateTime;

  @Column(name = "updatedDateTime")
  private Date updatedDateTime;



  public ResourceSyncSettings() {

  }

  public ResourceSyncSettings(ResourceTypeEnum resourceType, boolean autoSync) {
    this.resourceType = resourceType;
    this.autoSync = autoSync;
  }

  public int getId() {
    return id;
  }


  public ResourceTypeEnum getResourceType() {
    return resourceType;
  }

  public void setResourceType(ResourceTypeEnum resourceType) {
    this.resourceType = resourceType;
  }

  public boolean isAutoSync() {
    return autoSync;
  }

  public void setAutoSync(boolean autoSync) {
    this.autoSync = autoSync;
  }


  public Date getCreatedDateTime() {
    return createdDateTime;
  }

  public void setCreatedDateTime(Date createdDateTime) {
    this.createdDateTime = createdDateTime;
  }

  public Date getUpdatedDateTime() {
    return updatedDateTime;
  }

  public void setUpdatedDateTime(Date updatedDateTime) {
    this.updatedDateTime = updatedDateTime;
  }


  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (autoSync ? 1231 : 1237);
    result = prime * result + ((createdDateTime == null) ? 0 : createdDateTime.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((resourceType == null) ? 0 : resourceType.hashCode());
    result = prime * result + ((updatedDateTime == null) ? 0 : updatedDateTime.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    ResourceSyncSettings other = (ResourceSyncSettings) obj;
    if (autoSync != other.autoSync) {
      return false;
    }
    if (createdDateTime == null) {
      if (other.createdDateTime != null) {
        return false;
      }
    } else if (!createdDateTime.equals(other.createdDateTime)) {
      return false;
    }
    if (id == null) {
      if (other.id != null) {
        return false;
      }
    } else if (!id.equals(other.id)) {
      return false;
    }
    if (resourceType != other.resourceType) {
      return false;
    }
    if (updatedDateTime == null) {
      if (other.updatedDateTime != null) {
        return false;
      }
    } else if (!updatedDateTime.equals(other.updatedDateTime)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "ResourceSyncSettings [id=" + id + ", resourceType=" + resourceType + ", autoSync=" + autoSync
            + ", createdDateTime=" + createdDateTime + ", updatedDateTime=" + updatedDateTime + "]";
  }

}