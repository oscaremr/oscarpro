/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PreventionBillingConfig extends AbstractModel<Integer> implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  private String preventionType = "";

  private String billingServiceCodeAndUnit = "";

  private String billingType = "";

  private String visitType = "";

  private String billingDxCode = "";

  private String visitLocation = "";

  private String sliCode = "";

  public PreventionBillingConfig(final String preventionType) {
    this.preventionType = preventionType;
  }

  public PreventionBillingConfig(final HttpServletRequest request, final int index) {
    this.id = request.getParameterValues("preventionId")[index].equals("null") 
        ? null : Integer.valueOf(request.getParameterValues("preventionId")[index]);
    this.preventionType = request.getParameterValues("preventionType")[index];
    this.billingServiceCodeAndUnit = request.getParameterValues("serviceCode")[index];
    this.billingType = request.getParameterValues("billingType")[index];
    this.visitType = request.getParameterValues("billingVisitType")[index];
    this.billingDxCode = request.getParameterValues("dxCode")[index];
    this.visitLocation = request.getParameterValues("clinicLocation")[index];
    this.sliCode = request.getParameterValues("billingSliCodeType")[index];
  }
  public PreventionBillingConfig(final PreventionBillingConfig preventionBillingConfig) {
    this.id = preventionBillingConfig.getId();
    this.preventionType = preventionBillingConfig.getPreventionType();
    this.billingServiceCodeAndUnit = preventionBillingConfig.getBillingServiceCodeAndUnit();
    this.billingType = preventionBillingConfig.getBillingType();
    this.visitType = preventionBillingConfig.getVisitType();
    this.billingDxCode = preventionBillingConfig.getBillingDxCode();
    this.visitLocation = preventionBillingConfig.getVisitLocation();
    this.sliCode = preventionBillingConfig.getSliCode();
  }
}
