/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package org.oscarehr.common.model;

import javax.annotation.Nullable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="measurementGroup")
public class MeasurementGroup extends AbstractModel<Integer> implements Comparable<MeasurementGroup> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	@Getter @Setter private Integer id;

	@Getter @Setter private String name;

	@Getter @Setter @Nullable private String typeDisplayName;

	@Getter @Setter private boolean archived;
	@Override
	public final int compareTo(MeasurementGroup measurementGroup) {
		return this.getTypeDisplayName().compareToIgnoreCase(measurementGroup.getTypeDisplayName());
	}
}
