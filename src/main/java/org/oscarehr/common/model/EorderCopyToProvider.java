/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuppressWarnings("serial")
@MappedSuperclass
public abstract class EorderCopyToProvider extends AbstractModel<Integer> {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Integer id;

  @Column(name = "salutation")
  @Setter(AccessLevel.NONE)
  protected String salutation;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "middle_name")
  private String middleName;

  @Column(name = "address1")
  private String address1;

  @Column(name = "address2")
  private String address2;

  @Column(name = "city")
  private String city;

  @Column(name = "state_or_province")
  private String stateOrProvince;

  @Column(name = "zip_or_postal")
  private String zipOrPostal;

  @Column(name = "country")
  private String country;

  @Column(name = "region")
  private String region;

  @Column(name = "specialty")
  protected String specialty;

  private String cpso;

  private String billingNumber;

  public EorderCopyToProvider() {
    super();
  }

  public String getLastName() {
    if (lastName != null) {
      return lastName.replaceAll("^\"|\"$", "");
    } else {
      return null;
    }
  }

  public String getFirstName() {
    if (firstName != null) {
      return firstName.replaceAll("^\"|\"$", "");
    } else {
      return null;
    }
  }

  public String getMiddleName() {
    if (middleName != null) {
      return middleName.replaceAll("^\"|\"$", "");
    } else {
      return null;
    }
  }

  public String getAddress1() {
    if (address1 != null) {
      return address1.replaceAll("^\"|\"$", "");
    } else {
      return null;
    }
  }

  public String getAddress2() {
    if (address2 != null) {
      return address2.replaceAll("^\"|\"$", "");
    } else {
      return null;
    }
  }

  public String getCity() {
    if (city != null) {
      return city.replaceAll("^\"|\"$", "");
    } else {
      return null;
    }
  }

  public String getStateOrProvince() {
    if (stateOrProvince != null) {
      return stateOrProvince.replaceAll("^\"|\"$", "");
    } else {
      return null;
    }
  }

  public void setStateOrProvince(String stateOrProvince) {
    this.stateOrProvince = stateOrProvince;
  }

  public String getZipOrPostal() {
    if (zipOrPostal != null) {
      return zipOrPostal.replaceAll("^\"|\"$", "");
    } else {
      return null;
    }
  }

  public String getCountry() {
    if (country != null) {
      return country.replaceAll("^\"|\"$", "");
    } else {
      return null;
    }
  }

  public String getRegion() {
    if (region != null) {
      return region.replaceAll("^\"|\"$", "");
    } else {
      return null;
    }
  }

  @Override
  public String toString() {
    return "EOrderCopyToProvider [id="
        + id
        + ", salutation="
        + salutation
        + ", lastName="
        + lastName
        + ", firstName="
        + firstName
        + ", middleName="
        + middleName
        + ", address1="
        + address1
        + ", address2="
        + address2
        + ", city="
        + city
        + ", stateOrProvince="
        + stateOrProvince
        + ", zipOrPostal="
        + zipOrPostal
        + ", country="
        + country
        + ", region="
        + region
        + ", specialty="
        + specialty
        + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((address1 == null) ? 0 : address1.hashCode());
    result = prime * result + ((address2 == null) ? 0 : address2.hashCode());
    result = prime * result + ((city == null) ? 0 : city.hashCode());
    result = prime * result + ((country == null) ? 0 : country.hashCode());
    result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
    result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
    result = prime * result + ((region == null) ? 0 : region.hashCode());
    result = prime * result + ((salutation == null) ? 0 : salutation.hashCode());
    result = prime * result + ((specialty == null) ? 0 : specialty.hashCode());
    result = prime * result + ((stateOrProvince == null) ? 0 : stateOrProvince.hashCode());
    result = prime * result + ((zipOrPostal == null) ? 0 : zipOrPostal.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (!super.equals(obj)) return false;
    if (getClass() != obj.getClass()) return false;
    EorderCopyToProvider other = (EorderCopyToProvider) obj;
    if (address1 == null) {
      if (other.address1 != null) return false;
    } else if (!address1.equals(other.address1)) return false;
    if (address2 == null) {
      if (other.address2 != null) return false;
    } else if (!address2.equals(other.address2)) return false;
    if (city == null) {
      if (other.city != null) return false;
    } else if (!city.equals(other.city)) return false;
    if (country == null) {
      if (other.country != null) return false;
    } else if (!country.equals(other.country)) return false;
    if (firstName == null) {
      if (other.firstName != null) return false;
    } else if (!firstName.equals(other.firstName)) return false;
    if (id == null) {
      if (other.id != null) return false;
    } else if (!id.equals(other.id)) return false;
    if (lastName == null) {
      if (other.lastName != null) return false;
    } else if (!lastName.equals(other.lastName)) return false;
    if (middleName == null) {
      if (other.middleName != null) return false;
    } else if (!middleName.equals(other.middleName)) return false;
    if (region == null) {
      if (other.region != null) return false;
    } else if (!region.equals(other.region)) return false;
    if (salutation == null) {
      if (other.salutation != null) return false;
    } else if (!salutation.equals(other.salutation)) return false;
    if (specialty == null) {
      if (other.specialty != null) return false;
    } else if (!specialty.equals(other.specialty)) return false;
    if (stateOrProvince == null) {
      if (other.stateOrProvince != null) return false;
    } else if (!stateOrProvince.equals(other.stateOrProvince)) return false;
    if (zipOrPostal == null) {
      if (other.zipOrPostal != null) return false;
    } else if (!zipOrPostal.equals(other.zipOrPostal)) return false;
    return true;
  }

  public String getFormattedName() {
    return getLastName() + "," + getFirstName();
  }
}
