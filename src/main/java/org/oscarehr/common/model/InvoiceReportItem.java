package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class InvoiceReportItem extends AbstractModel<Integer> {

    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "ch1_id")
    private Integer ch1Id;
    @Column(name = "bi_id")
    private Integer biId;
    @Column(name = "demographic_no")
    private Integer demographicNo;
    @Column(name = "demographic_name")
    private String demographicName;
    @Column(name = "billing_date")
    private Date billingDate;
    @Column(name = "billing_time")
    private Date billingTime;
    @Column(name = "status")
    private String status;
    @Column(name = "provider_no")
    private String providerNo;
    @Column(name = "provider_ohip_no")
    private String providerOhipNo;
    @Column(name = "apptProvider_no")
    private String apptProviderNo;
    @Column(name = "timestamp1")
    private Date timestamp;
    @Column(name = "service_date")
    private Date serviceDate;
    @Column(name = "clinic")
    private String clinic;
    @Column(name = "pay_program")
    private String payProgram = "HCP";
    @Column(name = "total")
    private BigDecimal total;
    @Column(name = "paid")
    private BigDecimal paid;
    @Column(name = "facility_num")
    private String facilityNum;
    
    @Column(name = "fee")
    private String fee;
    @Column(name = "dx")
    private String dx;
    @Column(name = "service_code")
    private String serviceCode;
    @Column(name = "service_count")
    private String serviceCount;

    @Override
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCh1Id() {
        return ch1Id;
    }

    public void setCh1Id(Integer ch1Id) {
        this.ch1Id = ch1Id;
    }

    public Integer getBiId() {
        return biId;
    }

    public void setBiId(Integer biId) {
        this.biId = biId;
    }

    public Integer getDemographicNo() {
        return demographicNo;
    }

    public void setDemographicNo(Integer demographicNo) {
        this.demographicNo = demographicNo;
    }

    public String getDemographicName() {
        return demographicName;
    }

    public void setDemographicName(String demographicName) {
        this.demographicName = demographicName;
    }

    public Date getBillingDate() {
        return billingDate;
    }

    public void setBillingDate(Date billingDate) {
        this.billingDate = billingDate;
    }

    public Date getBillingTime() {
        return billingTime;
    }

    public void setBillingTime(Date billingTime) {
        this.billingTime = billingTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProviderNo() {
        return providerNo;
    }

    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }

    public String getProviderOhipNo() {
        return providerOhipNo;
    }

    public void setProviderOhipNo(String providerOhipNo) {
        this.providerOhipNo = providerOhipNo;
    }

    public String getApptProviderNo() {
        return apptProviderNo;
    }

    public void setApptProviderNo(String apptProviderNo) {
        this.apptProviderNo = apptProviderNo;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Date getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(Date serviceDate) {
        this.serviceDate = serviceDate;
    }

    public String getClinic() {
        return clinic;
    }

    public void setClinic(String clinic) {
        this.clinic = clinic;
    }

    public String getPayProgram() {
        return payProgram;
    }

    public void setPayProgram(String payProgram) {
        this.payProgram = payProgram;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getPaid() {
        return paid;
    }

    public void setPaid(BigDecimal paid) {
        this.paid = paid;
    }

    public String getFacilityNum() {
        return facilityNum;
    }

    public void setFacilityNum(String facilityNum) {
        this.facilityNum = facilityNum;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getDx() {
        return dx;
    }

    public void setDx(String dx) {
        this.dx = dx;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceCount() {
        return serviceCount;
    }

    public void setServiceCount(String serviceCount) {
        this.serviceCount = serviceCount;
    }
}
