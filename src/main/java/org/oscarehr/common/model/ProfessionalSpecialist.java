/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */

package org.oscarehr.common.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

@Entity
@Table(name = "professionalSpecialists")
@Getter @Setter
@EqualsAndHashCode(callSuper = true)
public class ProfessionalSpecialist extends AbstractModel<Integer> implements Serializable {

    public enum DemographicRelationship {

        REFERRAL_DOCTOR("referralPhysicianRowId"),
        FAMILY_DOCTOR("familyPhysicianRowId");
        private String demographicExtKeyVal;

        DemographicRelationship(String demographicExtKeyVal) {
            this.demographicExtKeyVal = demographicExtKeyVal;
        }

        public String getDemographicExtKeyVal() {
            return demographicExtKeyVal;
        }

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "specId")
    private Integer id;

    @Column(name = "fName")
    private String firstName;

    @Column(name = "lName")
    private String lastName;

    @Column(name = "proLetters")
    private String professionalLetters;

    @Column(name = "address")
    private String streetAddress;

    @Column(name = "phone")
    private String phoneNumber;

    private String phoneExtension;

    @Column(name = "fax")
    private String faxNumber;

    @Column(name = "website")
    private String webSite;

    @Column(name = "email")
    private String emailAddress;

    @Column(name = "specType")
    private String specialtyType;

    private String edataUrl;
    private String edataOscarKey;
    private String edataServiceKey;
    private String edataServiceName;
    private String annotation;

    @Column(name="annotate_in_search")
    private boolean annotatedInSearch = false;

    private Integer departmentId = 0;
    private Integer eformId;
    private Integer institutionId = 0;
    private String privatePhoneNumber;
    private String cellPhoneNumber;
    private String pagerNumber;
    private String salutation;
    private String province;
    private String referralNo;

    @Column(name="hideFromView")
    private boolean hiddenFromView = false;

    private boolean deleted=false;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdated=new Date();

    @PreUpdate
    protected void jpaUpdateLastUpdateTime() {
        lastUpdated = new Date();
    }

    @Override
    public Integer getId() {
        return(id);
    }

    public void setFirstName(String firstName) {
        this.firstName = StringUtils.trimToNull(firstName);
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = StringUtils.trimToNull(lastName);
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setProfessionalLetters(String professionalLetters) {
        this.professionalLetters = StringUtils.trimToNull(professionalLetters);
    }

    public String getStreetAddress() {
        return StringUtils.trimToEmpty(streetAddress);
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = StringUtils.trimToEmpty(streetAddress);
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = StringUtils.trimToNull(phoneNumber);
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = StringUtils.trimToNull(faxNumber);
    }

    public void setWebSite(String webSite) {
        this.webSite = StringUtils.trimToNull(webSite);
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = StringUtils.trimToNull(emailAddress);
    }

    public void setSpecialtyType(String specialtyType) {
        this.specialtyType = StringUtils.trimToNull(specialtyType);
    }

    public void setEdataUrl(final String edataUrl) {
        this.edataUrl = StringUtils.trimToNull(edataUrl);
    }

    public void setEdataOscarKey(final String edataOscarKey) {
        this.edataOscarKey = StringUtils.trimToNull(edataOscarKey);
    }

    public void setEdataServiceKey(final String edataServiceKey) {
        this.edataServiceKey = StringUtils.trimToNull(edataServiceKey);
    }

    public void setEdataServiceName(final String edataServiceName) {
        this.edataServiceName = StringUtils.trimToNull(edataServiceName);
    }

    /**
     * @param annotation the annotation to set
     */
    public void setAnnotation(String annotation) {
        this.annotation = StringUtils.trimToNull(annotation);
    }

    public void setAnnotatedInSearch(Boolean annotatedInSearch) {
        this.annotatedInSearch = annotatedInSearch != null ? annotatedInSearch : false;
    }

    public String getFormattedName() {
        return getFormattedName(false);
    }

    public String getFormattedName(Boolean inSearch) {
        return this.lastName + ", " + this.firstName
                + (inSearch ? ((StringUtils.isNotBlank(this.professionalLetters) ? " " + this.professionalLetters : "") +
                (this.annotatedInSearch && StringUtils.isNotBlank(this.annotation) ? " ("+this.annotation+")" : "")) : "");
    }

    /**
     * Gets a comma-separated first and last names,
     * suffixed by the professional title letters
     *
     * @return
     *         Returns the formatted title
     */
    public String getFormattedTitle() {
        StringBuilder buf = new StringBuilder();
        boolean isAppended = false;

        if (getLastName() != null && !getLastName().isEmpty()) {
            buf.append(getLastName());
            isAppended = true;
        }

        if (getFirstName() != null && !getFirstName().isEmpty()) {
            if (isAppended) {
                buf.append(", ");
            }
            buf.append(getFirstName());
            isAppended = true;
        }

        if (getProfessionalLetters() != null && !getProfessionalLetters().isEmpty()) {
            if (isAppended) {
                buf.append(" ");
            }
            buf.append(getProfessionalLetters());
        }
        return buf.toString();
    }

    /**
     * Creates a contact string for billing invoices for this provider
     * @return A new string describing the provider's contact details
     */
    public String createContactString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getLastName()).append(", ").append(getFirstName());
        if (getStreetAddress() != null) {
            sb.append("\n").append(getStreetAddress());
        }
        if (getPhoneNumber() != null) {
            sb.append("\nTel: ").append(getPhoneNumber());
        }
        if (getFaxNumber() != null) {
            sb.append("\nFax: ").append(getFaxNumber());
        }
        if (getEmailAddress() != null) {
            sb.append("\n").append(getEmailAddress());
        }
        if (getWebSite() != null) {
            sb.append("\n").append(getWebSite());
        }
        return sb.toString();
    }

}
