package org.oscarehr.common.model;

import com.google.common.collect.Maps;

import java.util.Map;

public enum TriggerType {
    NEW_TRIGGER("NEW_TRIGGER", "", new TriggerConditionType[]{}, new TriggerActionType[]{}),
    INCOMING_LAB("INCOMING_LAB", "Incoming Lab",
            new TriggerConditionType[]{TriggerConditionType.SENDING_FACILITY, TriggerConditionType.REQUESTING_CLIENT, TriggerConditionType.PRIORITY,
                    TriggerConditionType.OBR_DATE, TriggerConditionType.OBR_4, TriggerConditionType.OBR_4_1, TriggerConditionType.OBR_4_2, TriggerConditionType.ACCESSION_NUMBER,
                    TriggerConditionType.OBR_COUNT, TriggerConditionType.LOINC_CODE, TriggerConditionType.RESULT_STATUS, TriggerConditionType.DISCIPLINE},
            new TriggerActionType[]{TriggerActionType.FLAG_PROVIDER, TriggerActionType.LABEL_AS, TriggerActionType.LABEL_AS_KEY, TriggerActionType.CLEAR_FLAGGED_PROVIDERS, TriggerActionType.SEND_LAB_TO_UNCLAIMED_INBOX});
    private String key;
    private String displayName;
    private TriggerConditionType[] triggerConditionTypes;
    private TriggerActionType[] triggerActionTypes;

    private static final Map<String, TriggerType> keyMap = Maps.newHashMapWithExpectedSize(TriggerType.values().length);
    static {
        for (TriggerType keyEnum : TriggerType.values()) {
            keyMap.put(keyEnum.getKey(), keyEnum);
        }
    }
    
    TriggerType(String key, String displayName, TriggerConditionType[] triggerConditionTypes, TriggerActionType[] triggerActionTypes) {
        this.key = key;
        this.displayName = displayName;
        this.triggerActionTypes = triggerActionTypes;
        this.triggerConditionTypes = triggerConditionTypes;
    }

    public String getKey() {
        return key;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public TriggerConditionType[] getTriggerConditionTypes() {
        return triggerConditionTypes;
    }

    public TriggerActionType[] getTriggerActionTypes() {
        return triggerActionTypes;
    }

    public static TriggerType getByKey(String key) {
        return keyMap.get(key);
    }
}