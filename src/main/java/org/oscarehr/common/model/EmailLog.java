/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.model;

import org.oscarehr.ws.rest.to.model.EmailLogTo1;
import org.oscarehr.ws.rest.to.model.EmailSpecificationTo1;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="email_log")
public class EmailLog extends AbstractModel<Integer> {

    public EmailLog() {
    }

    public EmailLog(Date sentDate) {
        this.sentDate = sentDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Date sentDate;

    @Enumerated(EnumType.STRING)
    private EmailLogTo1.EmailSentStatus status;

    @Column(name = "content")
    private String content;

    @Enumerated(EnumType.STRING)
    @Column(name = "emailType", nullable=false)
    private EmailSpecificationTo1.EmailType emailType;

    @ManyToOne
    @JoinColumn(name = "recipientId")
    private Demographic recipient;

    @OneToMany(cascade = {CascadeType.ALL/*, CascadeType.MERGE, CascadeType.PERSIST*/}, fetch = FetchType.EAGER)
    @JoinColumn(name = "emailLogId")
    private List<EmailAttachment> attachments = new ArrayList<EmailAttachment>();

	public Integer getId() {
    	return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public EmailLogTo1.EmailSentStatus getStatus() {
        return status;
    }

    public void setStatus(EmailLogTo1.EmailSentStatus status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Demographic getRecipient() {
        return recipient;
    }

    public void setRecipient(Demographic recipient) {
        this.recipient = recipient;
    }

    public EmailSpecificationTo1.EmailType getEmailType() {
        return emailType;
    }

    public void setEmailType(EmailSpecificationTo1.EmailType emailType) {
        this.emailType = emailType;
    }

    public List<EmailAttachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<EmailAttachment> attachments) {
        this.attachments = attachments;
    }
}
