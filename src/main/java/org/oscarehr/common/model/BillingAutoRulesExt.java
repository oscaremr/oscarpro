/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="billing_auto_rules_ext")
public class BillingAutoRulesExt extends AbstractModel<Integer> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String ext_key;
	private String ext_value;
	private Integer rule_id;
	@Temporal(TemporalType.DATE)
	private Date created;
	@Temporal(TemporalType.DATE)
	private Date modified;	
	private Integer status;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getExtKey() {
		return ext_key;
	}

	public void setExtKey(String ext_key) {
		this.ext_key = ext_key;
	}
	
	public String getExtValue(){
		return ext_value;
	}
	
	public void setExtValue(String ext_value){
		this.ext_value = ext_value;
	}
	
	public Integer getRuleId() {
		return rule_id;
	}

	public void setRuleId(Integer rule_id) {
		this.rule_id = rule_id;
	}

	public Date getCreatedDate() {
		return created;
	}

	public void setCreatedDate(Date created) {
		this.created = created;
	}	
	
	public Date getModifiedDate() {
		return modified;
	}
	
	public void setModifiedDate(Date modified) {
		this.modified = modified;
	}	
	
	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	
	public String getCreatedDateAsString() {
		if(getCreatedDate() != null) {
			SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
			return f.format(getCreatedDate());
		}else {
			return "";
		}
	}
	
	public String getModifiedDateAsString() {
		if(getModifiedDate() != null) {
			SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
			return f.format(getModifiedDate());
		}else {
			return "";
		}
	}
	
}
