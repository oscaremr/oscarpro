package org.oscarehr.common.model;

import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "erefer_attachment")
@AllArgsConstructor
public class EReferAttachment extends AbstractModel<Integer> {

	@Id
	@GeneratedValue
	private Integer id;

	@Column(name = "demographic_no")
	private Integer demographicNo;

	private Date created;

	private boolean archived = false;

	private String type;

	@OneToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "erefer_attachment_id", referencedColumnName = "id")
	private List<EReferAttachmentData> attachments;

	@OneToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "erefer_attachment_id", referencedColumnName = "id")
	private List<EReferAttachmentManagerData> attachmentManagerData;

	public EReferAttachment() {
	}

	public EReferAttachment(Integer demographicNo) {
		this.demographicNo = demographicNo;
		this.created = new Date();
	}

	public EReferAttachment(final Integer demographicNo, final OceanWorkflowTypeEnum type,
			final Date created) {
		this.demographicNo = demographicNo;
		this.created = created;
		this.type = type.name();
	}
}
