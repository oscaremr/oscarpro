/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.model;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;
import org.oscarehr.common.model.provider.ProviderType;

@Data
@Entity
@Table(name = "provider")
public class ProviderData extends AbstractModel<String> implements Serializable {

  @Id @Column(name = "provider_no")
  private String id;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "provider_type")
  private String providerType;

  private String supervisor;

  private String specialty;

  private String credentials = "";

  private String team;

  private String sex;

  @Temporal(TemporalType.DATE)
  private Date dob;

  private String address;

  private String phone;

  @Column(name = "work_phone")
  private String workPhone;

  @Column(name = "ohip_no")
  private String ohipNo;

  @Column(name = "rma_no")
  private String rmaNo;

  @Column(name = "billing_no")
  private String billingNo;

  @Column(name = "hso_no")
  private String hsoNo;

  private String status;

  private String comments;

  @Column(name = "provider_activity")
  private String providerActivity;

  private String practitionerNo;

  private String init;

  @Column(name = "job_title")
  private String jobTitle;

  private String email;

  private String title;

  private String lastUpdateUser;

  @Temporal(TemporalType.DATE)
  private Date lastUpdateDate;

  @Column(name = "signed_confidentiality")
  @Temporal(TemporalType.DATE)
  private Date signedConfidentiality;

  private Boolean thirdPartyOnly = false;

  @Column(name = "has_schedule")
  private Boolean hasSchedule = true;

  @Column(name = "receives_ticklers")
  private boolean receivesTicklers = true;

  private String practitionerNoType;

  @Column(name = "excelleris_id")
  private String excellerisId;

  @Column(name = "lifelabs_id")
  private String lifelabsId;

  @Column(name = "prescribe_it_type")
  private String prescribeItType;

  private String city;

  private String province;

  private String postalCode;

  /* CUSTOMER GETTERS */

  public String getFormattedName() {
    return this.lastName + ", " + this.firstName;
  }

  public ProviderType getProviderTypeAsEnum() {
    return ProviderType.fromStringValue(providerType);
  }

  public ProviderType getPrescribeItTypeAsEnum() {
    return ProviderType.fromStringValue(prescribeItType);
  }

  public boolean doesReceiveTicklers() {
    return receivesTicklers;
  }

  public Boolean hasSchedule() {
    return hasSchedule;
  }

  public Boolean isThirdPartyOnly() {
    return thirdPartyOnly;
  }

  /* COMPARATOR */

  public boolean equals(Object object) {
    if (!(object instanceof ProviderData)) {
      return false;
    }
    ProviderData other = (ProviderData) object;
    return ProviderData.ProviderNoComparator.compare(this, other) == 0;
  }

  public static final Comparator<ProviderData> LastNameComparator = new Comparator<ProviderData>() {
    public int compare(ProviderData pd1, ProviderData pd2) {
      return pd1.getLastName().compareTo(pd2.getLastName());
    }
  };

  public static final Comparator<ProviderData> FirstNameComparator = new Comparator<ProviderData>() {
    public int compare(ProviderData pd1, ProviderData pd2) {
      return pd1.getFirstName().compareTo(pd2.getFirstName());
    }
  };

  public static final Comparator<ProviderData> ProviderNoComparator = new Comparator<ProviderData>() {
    public int compare(ProviderData pd1, ProviderData pd2) {
      return pd1.getId().compareTo(pd2.getId());
    }
  };
}
