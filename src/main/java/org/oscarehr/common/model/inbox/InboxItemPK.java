package org.oscarehr.common.model.inbox;

import javax.persistence.Column;
import java.io.Serializable;


public class InboxItemPK implements Serializable {
	@Column(name = "segment_id")
	private Integer segmentId;
	
	@Column(name = "lab_type")
	private String labType;

	public Integer getSegmentId() {
		return segmentId;
	}
	public void setSegmentId(Integer segmentId) {
		this.segmentId = segmentId;
	}

	public String getLabType() {
		return labType;
	}
	public void setLabType(String labType) {
		this.labType = labType;
	}
}
