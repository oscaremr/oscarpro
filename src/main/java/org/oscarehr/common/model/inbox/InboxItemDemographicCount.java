package org.oscarehr.common.model.inbox;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class InboxItemDemographicCount {

	@EmbeddedId
	private InboxItemDemographicCountPK id;
	@Column(name="last_name")
	private String lastName;
	@Column(name="first_name")
	private String firstName;
	@Column(name="count")
	private Integer count;

	public String getLabType() {
		return id.getLabType();
	}

	public String getPatientId() {
		return id.getLabPatientId();
	}

	public String getLastName() {
		return lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public Integer getCount() {
		return count;
	}
}
