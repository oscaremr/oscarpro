package org.oscarehr.common.model.inbox;

import javax.persistence.Column;
import java.io.Serializable;

public class InboxItemDemographicCountPK implements Serializable {
	@Column(name = "lab_patient_id")
	private String labPatientId;
	@Column(name = "lab_type")
	private String labType;

	public String getLabPatientId() {
		return labPatientId;
	}
	public void setLabPatientId(String labPatientId) {
		this.labPatientId = labPatientId;
	}

	public String getLabType() {
		return labType;
	}
	public void setLabType(String labType) {
		this.labType = labType;
	}
}
