package org.oscarehr.common.model.inbox;

import org.oscarehr.common.model.Provider;

/**
 * An abstract class to collect and pass inbox query parameters
 * Extending classes should translate provided parameters to the valid formats used by the inbox repository
 */
public class OscarInboxQueryParameters extends InboxQueryParameters {

	public OscarInboxQueryParameters(Provider loggedInProvider) {
		super(loggedInProvider);
	}

	@Override
	public InboxQueryParameters whereProviderNumber(String providerNumber) {
		if ("-1".equals(providerNumber)) {
			providerNumber = "";
		}
		return super.whereProviderNumber(providerNumber);
	}

	@Override
	public void setStartDate(String startDate) {
		if (startDate == null) {
			startDate = "";
		}
		super.setStartDate(startDate);
	}

	@Override
	public void setEndDate(String endDate) {
		if (endDate == null) {
			endDate = "";
		}
		super.setEndDate(endDate);
	}

	@Override
	public void setStatus(String status) {
		if ("".equals(status)) {
			status = "L";
		}
		super.setStatus(status);
	}

	@Override
	public void setAbnormalStatus(String abnormalStatus) {
		switch (abnormalStatus) {
			case "all":
				abnormalStatus = "L";
				break;
			case "normalOnly":
				abnormalStatus = "N";
				break;
			case "abnormalOnly":
				abnormalStatus = "A";
				break;
		}
		super.setAbnormalStatus(abnormalStatus);
	}

	@Override
	public void setMatchedStatus(MatchedStatus matchedStatus) {
		super.setMatchedStatus(matchedStatus);
	}

	@Override
	public void setSortBy(String sortBy) {
		super.setSortBy(sortBy);
	}

	@Override
	public void setSortOrder(String sortOrder) {
		super.setSortOrder(sortOrder);
	}

	@Override
	public void setPage(Integer page) {
		super.setPage(page);
	}

	@Override
	public void setResultsPerPage(Integer resultsPerPage) {
		super.setResultsPerPage(resultsPerPage);
	}

	@Override
	public void setShowDocuments(Boolean showDocuments) {
		super.setShowDocuments(showDocuments);
	}

	@Override
	public void setShowLabs(Boolean showLabs) {
		super.setShowLabs(showLabs);
	}

	@Override
	public void setShowHrm(Boolean showHrm) {
		super.setShowHrm(showHrm);
	}

	@Override
	public void setGetCounts(Boolean getCounts) {
		super.setGetCounts(getCounts);
	}

	@Override
	public void setGetDemographicCounts(Boolean getDemographicCounts) {
		super.setGetDemographicCounts(getDemographicCounts);
	}
}
