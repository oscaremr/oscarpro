/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */
package org.oscarehr.common.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Table(name = "demographicExtArchive")
public class DemographicExtArchive extends AbstractModel<Integer> implements Serializable {
	private static final long serialVersionUID = -2981357879423093412L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter private Integer id;

	@Column(name = "archiveId")
	@Getter @Setter	private Integer archiveId;

	@Column(name = "demographic_no")
	@Getter @Setter private Integer demographicNo;

	@Column(name = "provider_no")
	@Getter @Setter private String providerNo;

	@Column(name = "key_val")
	@Getter @Setter private String key;

	@Column(name = "date_time")
	@Temporal(TemporalType.TIMESTAMP)
	@Getter @Setter	private java.util.Date dateCreated;

	@Getter @Setter private String value;
	@Getter @Setter	private boolean hidden;


	public DemographicExtArchive(DemographicExt de) {
		if (de == null) {
			throw new IllegalArgumentException();
		}
		this.dateCreated = de.getDateCreated() != null ? de.getDateCreated() : new Date();
		this.demographicNo = de.getDemographicNo();
		this.providerNo = de.getProviderNo();
		this.key = de.getKey();
		this.value = de.getValue();
		this.hidden = de.isHidden();
	}

	/**
	 * Return the unique identifier of this class generator-class="native"
	 * column="id"
	 */
	@Override
	public Integer getId() {
		return id;
	}
}
