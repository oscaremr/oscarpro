/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "DataSharingSettings")
public class DataSharingSettings extends AbstractModel<Integer> implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Integer id;

  @Column(name = "allHistory")
  private boolean allHistory;

  @Column(name = "gracePeriod")
  private int gracePeriod;

  @Column(name = "gracePeriodUnit")
  @Enumerated(EnumType.STRING)
  private GracePeriodUnit gracePeriodUnit;

  @Column(name = "demographicNo")
  private Integer demographicNo;

  @Column(name = "isOrgSettings")
  private Boolean isOrgSettings;

  @Column(name = "createdDateTime")
  private Date createdDateTime;

  @Column(name = "updatedDateTime")
  private Date updatedDateTime;

  @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
  @JoinColumn(name = "dataSharingSettingsId")
  private List<ResourceSyncSettings> resourceSyncSettings;

  public DataSharingSettings(
      boolean allHistory,
      int gracePeriod,
      GracePeriodUnit gracePeriodUnit,
      int demographicNo,
      Boolean isOrgSettings,
      Date createdDateTime,
      Date updatedDateTime,
      List<ResourceSyncSettings> resourceSyncSettings
  ) {
    this.allHistory = allHistory;
    this.gracePeriod = gracePeriod;
    this.gracePeriodUnit = gracePeriodUnit;
    this.demographicNo = demographicNo;
    this.isOrgSettings = isOrgSettings;
    this.createdDateTime = createdDateTime;
    this.updatedDateTime = updatedDateTime;
    this.resourceSyncSettings = resourceSyncSettings;
  }

  @Override
  public Integer getId() {
    return id;
  }
}
