/**
 * Copyright (c) 2006-. KAI INNOVATIONS, OpenSoft System. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.util.List;
import java.util.ArrayList;

/**
 * Billingreferral generated by hbm2java
 */

@Entity
@Table(name="billing_permission")
public class BillingPermission  extends AbstractModel<Integer> implements java.io.Serializable {

	public static final String GENERATE_SIMULATE 		= "generate_simulate";
	public static final String OHIP_INVOICES 			= "ohip_invoices";
	public static final String THIRD_PARTY_INVOICES 	= "third_party_invoices";
	public static final String INVOICE_REPORT	 		= "invoice_report";
	public static final String MAILBOX			 		= "mailbox";
	public static final String VIEW_MOH_FILES			= "view_moh_files";
	public static final String BILLING_RECONCILIATION	= "billing_reconciliation";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
     private Integer id;
	@Column(name="provider_no")
     private String providerNo;
	@Column(name="viewer_no")
     private String viewerNo;
     private String permission;
     private boolean allow;

    public BillingPermission() {
    }


    public BillingPermission(String providerNo) {
        this.providerNo = providerNo;
    }

    public BillingPermission(String providerNo, String viewerNo, String permission, boolean allow) {
       this.providerNo = providerNo;
       this.viewerNo = viewerNo;
       this.permission = permission;
       this.allow = allow;
    }
	
	@Override
    public Integer getId() {
    	return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getProviderNo() {
        return this.providerNo;
    }

    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }
    public String getViewerNo() {
        return this.viewerNo;
    }

    public void setViewerNo(String viewerNo) {
        this.viewerNo = viewerNo;
    }
    public String getPermission() {
        return this.permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }
    public boolean isAllowed() {
        return getAllow();
    }

    public boolean getAllow() {
        return this.allow;
    }

    public void setAllow(boolean allow) {
        this.allow = allow;
    }
	
	public static List<String> getPermissionList(){
		List<String> permissionList = new ArrayList<String>();
		
		permissionList.add(BillingPermission.GENERATE_SIMULATE);
		permissionList.add(BillingPermission.OHIP_INVOICES);
		permissionList.add(BillingPermission.THIRD_PARTY_INVOICES);
		permissionList.add(BillingPermission.INVOICE_REPORT);
		//permissionList.add(BillingPermission.MAILBOX);
		permissionList.add(BillingPermission.VIEW_MOH_FILES);
		permissionList.add(BillingPermission.BILLING_RECONCILIATION);
		
		return permissionList;
	}
   
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BillingPermission other = (BillingPermission) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if ((this.providerNo == null) ? (other.providerNo != null) : !this.providerNo.equals(other.providerNo)) {
            return false;
        }
        if ((this.viewerNo == null) ? (other.viewerNo != null) : !this.viewerNo.equals(other.viewerNo)) {
            return false;
        }
        if ((this.permission == null) ? (other.permission != null) : !this.permission.equals(other.permission)) {
            return false;
        }
        return true;
    }
}
