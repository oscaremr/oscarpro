/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */

package org.oscarehr.common.model;

import javax.persistence.*;
import java.util.Comparator;

@Entity
@Table(name="appointmentType")
public class AppointmentType extends AbstractModel<Integer> {
        
    @Id
   	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
    
    private String name=null;
    private String notes=null;
	private Integer reasonCode = 0;
    private String reason=null;
    private String location=null;
    private String resources=null;    
    private int duration=15;
    @Column(name = "template_id")
    private Integer templateId;
    @Column(name = "provider_no")
    private String providerNo;
    @Column(name = "enabled")
    private Boolean enabled;

    private int auto_bill=0;

    public AppointmentType() {}
    public AppointmentType(AppointmentType oldType) {
        this.name = oldType.getName();
        this.notes = oldType.getNotes();
        this.reasonCode = oldType.getReasonCode();
        this.reason = oldType.getReason();
        this.location = oldType.getLocation();
        this.resources = oldType.getResources();
        this.duration = oldType.getDuration();
        this.templateId = oldType.getTemplateId();
        this.providerNo = oldType.getProviderNo();
        this.enabled = oldType.isEnabled();
        this.auto_bill = oldType.getAutoBill();
    }

    public static final Comparator<AppointmentType> ORDER_BY_NAME = new Comparator<AppointmentType>() {
        @Override
        public int compare(AppointmentType o1, AppointmentType o2) {
            return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
        }
    };
    
	@Override
    public Integer getId() {
		return id;
	}
		   
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getNotes() {
		return notes;
	}
	
	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getReasonCode() {
		return reasonCode;
	}

	public void setReasonCode(Integer reasonCode) {
		this.reasonCode = reasonCode;
	}

    public String getReason() {
		return reason;
	}
    
	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}

    public String getResources() {
		return resources;
	}
    
	public void setResources(String resources) {
		this.resources = resources;
	}

    public int getDuration() {
		return duration;
	}
    
	public void setDuration(int duration) {
		this.duration = duration;
	}

    public Integer getTemplateId() {
        return templateId;
    }
    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public String getProviderNo() {
        return providerNo;
    }
    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }

    public Boolean isEnabled() {
        return enabled;
    }
    public void setEnabled(Boolean enabled) {
	    if (enabled == null) { enabled = false; }
        this.enabled = enabled;
    }

    public int getAutoBill() {
	     return auto_bill;
    }

    public void setAutoBill(int auto_bill) {
	     this.auto_bill = auto_bill;
    }
}
