/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package org.oscarehr.common.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="raheader")
public class RaHeader extends AbstractModel<Integer> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="raheader_no")
	private Integer id;

	@Override
	public Integer getId() {
	  return id;
	}

	@Getter @Setter private String filename;
	
	@Column(name="group_no")
	@Getter @Setter private String groupNo = "";

	@Column(name="paymentdate")
	@Getter @Setter private String paymentDate;

	@Getter @Setter private String payable;

	@Column(name="totalamount")
	@Getter @Setter private String totalAmount;

	@Getter @Setter private String records;

	@Getter @Setter private String claims;

	@Getter @Setter private String status;

	@Column(name="readdate")
	@Getter @Setter private String readDate;

	@Getter @Setter private String content;
}
