/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
 
 package org.oscarehr.common.service;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import lombok.val;
import lombok.var;
import org.oscarehr.common.dao.BillingSliCodeDao;
import org.oscarehr.common.dao.BillingTypeDao;
import org.oscarehr.common.dao.BillingVisitTypeDao;
import org.oscarehr.common.dao.ClinicLocationDao;
import org.oscarehr.common.dao.PreventionBillingConfigDao;
import org.oscarehr.common.model.BillingSliCode;
import org.oscarehr.common.model.BillingType;
import org.oscarehr.common.model.BillingVisitType;
import org.oscarehr.common.model.ClinicLocation;
import org.oscarehr.common.model.PreventionBillingConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import oscar.oscarPrevention.PreventionDisplayConfig;

@Service
public class PreventionBillingSettingsService {

  @Autowired
  private BillingTypeDao billingTypeDao;

  @Autowired
  private BillingVisitTypeDao billingVisitTypeDao;

  @Autowired
  private BillingSliCodeDao billingSliCodeDao;

  @Autowired
  private ClinicLocationDao clinicLocationDao;

  @Autowired
  private PreventionBillingConfigDao preventionBillingConfigDao;

  public void savePreventionBillingConfig(final HttpServletRequest request){
    val uiPreventionBillingConfigs = new HashMap<String, PreventionBillingConfig>();
    for (int i = 0; i < request.getParameterValues("preventionType").length; i++) {
      val preventionBillingConfig = new PreventionBillingConfig(request, i);
      uiPreventionBillingConfigs.put(preventionBillingConfig.getPreventionType(), preventionBillingConfig);
    }
    for (var dbPreventionBillingConfig : getAllPreventionBillingConfigs()) {
      val uiPreventionBillingConfig = uiPreventionBillingConfigs.get(dbPreventionBillingConfig.getPreventionType());
      if (!dbPreventionBillingConfig.equals(uiPreventionBillingConfig)) {
        preventionBillingConfigDao.saveEntity(new PreventionBillingConfig(uiPreventionBillingConfig));
      }
    }
  }

  public List<PreventionBillingConfig> getAllPreventionBillingConfigs(){
    val preventionBillingConfigs = preventionBillingConfigDao.findAllActive();
    preventionBillingConfigs.addAll(preventionBillingConfigDao.findAllInactive());
    val pdc = PreventionDisplayConfig.getInstance();
    val prev = pdc.getPreventionsHash();
    for (var preventionBillingConfig : preventionBillingConfigs) {
      prev.remove(preventionBillingConfig.getPreventionType());
    }
    for (var preventionName : prev.keySet()) {
      preventionBillingConfigs.add(new PreventionBillingConfig(preventionName));
    }
    return preventionBillingConfigs;
  }

  public boolean hasSave(final HttpServletRequest request) {
    return "Save".equals(request.getParameter("dboperation"));
  }

  public List<BillingType> getAllBillingTypes() {
    return billingTypeDao.findAll();
  }

  public List<BillingVisitType> getAllBillingVisitTypes() {
    return billingVisitTypeDao.findAll();
  }

  public List<BillingSliCode> getBillingSliCodes() {
    return billingSliCodeDao.findAll();
  }

  public List<ClinicLocation> getClinicLocations() {
    return clinicLocationDao.findAll();
  }

}
