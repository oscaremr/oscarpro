/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.oscarehr.billing.CA.BC.dao.PeriodDefDao;
import org.oscarehr.billing.CA.BC.dao.TeleplanAlertDao;
import org.oscarehr.billing.CA.BC.model.PeriodDef;
import org.oscarehr.billing.CA.BC.model.TeleplanAlert;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;

public class TeleplanAutomateService {

	protected PeriodDefDao defDao;
	private TeleplanAlertDao teleplanAlertDao;

	public TeleplanAutomateService() {
		this.defDao = (PeriodDefDao) SpringUtils.getBean("periodDefDao");
		this.teleplanAlertDao = (TeleplanAlertDao) SpringUtils.getBean("teleplanAlertDao");
	}

	public String generateTaskNames(List<String> codes) {
		Map<String, String> taskNameMap = getTaskNameMap();
		StringBuilder sb = new StringBuilder();
		for (String code : codes) {
		    sb.append(taskNameMap.get(code)).append(", ");
		}
		return sb.substring(0, sb.toString().length() - 2);
	}

	public Map<String, String> getTaskNameMap() {
		Map<String, String> taskNameMap = new HashMap<String, String>();
		taskNameMap.put("teleplan_password_renew", "Automate Password Renewal");
		taskNameMap.put("teleplan_remit_download", "Automate Get Remittance");
		taskNameMap.put("excelleris_hl7_download", "Excelleris Download");
		return taskNameMap;
	}

	public Map<String, Object> getStatusInfo() {
		boolean excellerisEnabled = OscarProperties.getInstance().isPropertyActive("excelleris_download");
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("alerts", new ArrayList<TeleplanAlert>());
		String statusClass = "";
		String statusLabel = "";
		String statusCode = "";
		if (!isAutomateEnabled()) {
			statusClass = "grey";
			statusLabel = "Disabled";
			if (!isAutomateEnabled() && !excellerisEnabled) {
				statusCode = "all_conf_disabled";
			} else {
				statusCode = "teleplan_conf_disabled";
			}
		} else {
			List<TeleplanAlert> alerts = teleplanAlertDao.getAllFailedAlerts();
			boolean allSuccess = true;
			if (alerts != null && alerts.size() > 0) {
				allSuccess = false;
				statusClass = allSuccess ? "success" : "error";
				statusLabel = allSuccess ? "Success" : "Error";
				statusCode = allSuccess ? "all_run_success" : "run_has_error";
				map.put("alerts", alerts);
			}
			if (allSuccess) {
				PeriodDef pwdTaskDef = defDao.getByName("teleplan_password_renew");
				PeriodDef remitTaskDef = defDao.getByName("teleplan_remit_download");
				PeriodDef excellerisTaskDef = defDao.getByName("excelleris_hl7_download");
				List<String> deactivaedTasks = new ArrayList<String>();
				if (!pwdTaskDef.getActive()) {
					deactivaedTasks.add("teleplan_password_renew");
				}
				if (!remitTaskDef.getActive()) {
					deactivaedTasks.add("teleplan_remit_download");
				}
				if (!excellerisTaskDef.getActive()) {
					deactivaedTasks.add("excelleris_hl7_download");
				}
				if (deactivaedTasks.size() == 0) {
					statusClass = "success";
					statusLabel = "Connection";
					statusCode = "all_task_active";
				} else {
					statusClass = "grey";
					statusLabel = "Task Deactivated";
					statusCode = "some_task_active";
					map.put("deactivaedTasks", deactivaedTasks);
				}
			}
		}
		map.put("statusClass", statusClass);
		map.put("statusLabel", statusLabel);
		map.put("statusCode", statusCode);
		return map;
	}

	protected static boolean isAutomateEnabled() {
		return OscarProperties.getInstance().isPropertyActive("teleplan_automate");
	}
}