/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.service;

import java.util.Date;
import org.apache.log4j.Logger;
import org.oscarehr.billing.CA.BC.dao.PeriodDefDao;
import org.oscarehr.billing.CA.BC.dao.PeriodTaskLogDao;
import org.oscarehr.billing.CA.BC.dao.TeleplanAlertDao;
import org.oscarehr.billing.CA.BC.model.PeriodDef;
import org.oscarehr.billing.CA.BC.model.PeriodTaskLog;
import org.oscarehr.billing.CA.BC.model.TeleplanAlert;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.oscarBilling.ca.bc.Teleplan.TeleplanResponse;

public abstract class TeleplanAutomateThread extends Thread {

	private static Logger logger = MiscUtils.getLogger();
	protected PeriodTaskLogDao taskLogDao;
	protected PeriodDefDao defDao;
	protected TeleplanAlertDao teleplanAlertDao;
	protected PeriodDef periodDef;
	protected String taskName = "";
	public static boolean isRunning = false;

	public TeleplanAutomateThread(String taskName) {
		this.taskLogDao = (PeriodTaskLogDao) SpringUtils.getBean("periodTaskLogDao");
		this.defDao = (PeriodDefDao) SpringUtils.getBean("periodDefDao");
		this.teleplanAlertDao = (TeleplanAlertDao) SpringUtils.getBean("teleplanAlertDao");
		this.taskName = taskName;
		this.periodDef = defDao.getByName(taskName);
	}

	public void run() {
		boolean enabled = TeleplanAutomateService.isAutomateEnabled();
		logger.info("**********teleplan_automate config item is " + enabled);
		if (enabled) {
			if (this.isTaskAcitve()) {
				boolean timeToRun = this.timeToRun();
				if (isRunning) {
					logger.warn(taskName + " task is running.Wait next time to execute");
				}
				if (!timeToRun) {
					logger.warn("It's not time to run task:" + taskName);
				}
				if (timeToRun && !isRunning) {
					isRunning = true;
					TeleplanResponse tr;
					try {
						tr = this.taskRun();
						if (tr.isSuccess()) {
							saveExecuteLog("success");
						} else {
							deactivateTask();
							saveOrUpdateAlert(tr.getResult(), tr.getMsgs(), false);
							saveExecuteLog(tr.getResult() + ";" + tr.getMsgs());
						}
					} catch (Exception e) {
						logger.error("exception occurs when doing task:" + taskName, e);
						deactivateTask();
						saveOrUpdateAlert("Unknown exception", e.toString(), false);
						saveExecuteLog("Unknown exception:" + e.toString());
					}
				}
			} else {
				logger.info(this.taskName + " task is not active");
			}
		}
		isRunning = false;
	}

	protected boolean isTaskAcitve() {
		return this.periodDef != null && this.periodDef.getActive();
	}

	protected void deactivateTask() {
		if (this.deactivateTaskWhenFail()) {
			this.periodDef.setActive(false);
			defDao.merge(this.periodDef);
		}
	}

	protected void saveExecuteLog(String result) {
		PeriodTaskLog log = new PeriodTaskLog();
		log.setExecuteTime(new Date());
		log.setJobName(this.taskName);
		log.setResult(result);
		taskLogDao.createPeriodTaskLog(log);
		logger.info("**********log " + this.taskName + " task");
	}

	protected void saveOrUpdateAlert(String result, String msg, boolean isSuccess) {
		TeleplanAlert alert = teleplanAlertDao.getAlertByName(this.taskName);
		if (alert == null) {
			alert = new TeleplanAlert();
			alert.setJobName(this.taskName);
		}
		alert.setCreateTime(new Date());
		alert.setResult(result);
		alert.setMsg(msg);
		alert.setSuccess(isSuccess);
		teleplanAlertDao.saveEntity(alert);
	}

	protected abstract TeleplanResponse taskRun() throws Exception;

	protected boolean deactivateTaskWhenFail() {
		return false;
	}

	protected abstract boolean timeToRun();
}
