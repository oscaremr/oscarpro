/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.service;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.apache.log4j.Logger;
import org.oscarehr.billing.CA.BC.model.PeriodTaskLog;
import org.oscarehr.util.MiscUtils;
import oscar.oscarBilling.ca.bc.Teleplan.TeleplanAPI;
import oscar.oscarBilling.ca.bc.Teleplan.TeleplanResponse;
import oscar.oscarBilling.ca.bc.Teleplan.TeleplanService;
import oscar.oscarBilling.ca.bc.Teleplan.TeleplanUserPassDAO;

public class TeleplanRemitDownloadThread extends TeleplanAutomateThread {

	private static Logger logger = MiscUtils.getLogger();
	private static final String TASK_NAME = "teleplan_remit_download";

	public TeleplanRemitDownloadThread() {
		super(TASK_NAME);
	}

	protected boolean timeToRun() {
		Calendar calNow = GregorianCalendar.getInstance();
		String exp = periodDef.getExpression();
		String[] expArray = exp.split("\\|");
		String type = expArray[0];
		Integer date;
		Integer clock;
		Integer min = 5;
		if ("daily".equalsIgnoreCase(type)) {
			clock = Integer.parseInt(expArray[1]);
			if (expArray.length == 3) {
				min = Integer.parseInt(expArray[2]);
			}
			return isWithinOneHour(clock, min);
		} else {
			date = Integer.parseInt(expArray[1]);
			clock = Integer.parseInt(expArray[2]);
			if (expArray.length == 4) {
				min = Integer.parseInt(expArray[3]);
			}
			if ("weekly".equalsIgnoreCase(type)) {
				return date == calNow.get(Calendar.DAY_OF_WEEK) && isWithinOneHour(clock, min);
			} else {
				int lastDay = calNow.getActualMaximum(Calendar.DAY_OF_MONTH);
				int currentDay = calNow.get(Calendar.DAY_OF_MONTH);
				return (date == currentDay || (date > lastDay && currentDay == lastDay)) && isWithinOneHour(clock, min);
			}
		}
	}

	private boolean isWithinOneHour(int clock, int min) {
		Calendar calNow = GregorianCalendar.getInstance();
		calNow.set(Calendar.SECOND, 0);
		Calendar cal = GregorianCalendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, clock);
		cal.set(Calendar.MINUTE, min);
		cal.set(Calendar.SECOND, 0);
		long gap = calNow.getTimeInMillis() - cal.getTimeInMillis();
		return (gap >= 0 && gap < 60 * 60 * 1000) && calNow.get(Calendar.HOUR_OF_DAY) == clock && !executedToday();
	}

	private boolean executedToday() {
		PeriodTaskLog log = taskLogDao.getLastLogByName(taskName);
		if (log != null) {
			Calendar calNow = GregorianCalendar.getInstance();
			Calendar cal = GregorianCalendar.getInstance();
			cal.setTime(log.getExecuteTime());
			long gap = 1 * 3600 * 1000;
			boolean flag = calNow.getTimeInMillis() - cal.getTimeInMillis() < gap;
			if (flag) {
				logger.info(taskName + " task has been executed today.");
			}
			return flag;
		} else {
			return false;
		}
	}

	protected TeleplanResponse taskRun() throws Exception {
		TeleplanUserPassDAO dao = new TeleplanUserPassDAO();
		String[] userpass = dao.getUsernamePassword();
		TeleplanService tService = new TeleplanService();

		TeleplanAPI tAPI = null;
		logger.info("**********started to download teleplan remit");
		tAPI = tService.getTeleplanAPI(userpass[0], userpass[1]);
		TeleplanResponse tr = tAPI.getRemittance(true);
		logger.info(tr.toString());
		logger.info("real filename " + tr.getRealFilename());
		logger.info("**********download teleplan remit result:" + tr.getResult());
		if (tr.isSuccess()) {
			handleFile(tr.getRealFilename());
		}
		return tr;
	}

	/**
	 * Parses the remittance file in a thread
	 *
	 * @param fileName the name of the remittance file
	 * @throws IOException
	 */
	private void handleFile(final String fileName) throws IOException {
		TeleplanService teleplanService = new TeleplanService();
		teleplanService.parseRemittanceFile(fileName);
	}
}