/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.service;

import java.util.TimerTask;

public class TeleplanRemitDownloadJob extends TimerTask {

    @Override
    public void run() {
        TeleplanRemitDownloadThread thread = new TeleplanRemitDownloadThread();
        thread.start();
    }
}
