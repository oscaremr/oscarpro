/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.service;

class M01 {
	String message;

	void parse(String nextLine) {
		message = nextLine.substring(12);
	}

	public M01(String s) {
		parse(s);
	}

}