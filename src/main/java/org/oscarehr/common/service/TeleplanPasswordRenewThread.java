/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.service;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;
import org.oscarehr.billing.CA.BC.model.PeriodDef;
import org.oscarehr.billing.CA.BC.model.PeriodTaskLog;
import org.oscarehr.util.MiscUtils;
import oscar.oscarBilling.ca.bc.Teleplan.TeleplanAPI;
import oscar.oscarBilling.ca.bc.Teleplan.TeleplanResponse;
import oscar.oscarBilling.ca.bc.Teleplan.TeleplanUserPassDAO;

public class TeleplanPasswordRenewThread extends TeleplanAutomateThread {

	private static Logger logger = MiscUtils.getLogger();
	private static final String TASK_NAME = "teleplan_password_renew";

	public TeleplanPasswordRenewThread() {
		super(TASK_NAME);
	}

	protected boolean timeToRun() {
		PeriodTaskLog lastLog = taskLogDao.getLastLogByName(TASK_NAME);
		if (lastLog == null) {
			return true;
		}
		int daysGap = Integer.parseInt(periodDef.getExpression());
		Calendar lastExecutedLogDateWithDaysGap = GregorianCalendar.getInstance();

		lastExecutedLogDateWithDaysGap.setTime(lastLog.getExecuteTime());
		lastExecutedLogDateWithDaysGap.add(Calendar.DAY_OF_YEAR, daysGap);

		Calendar currentDate = GregorianCalendar.getInstance();
		return currentDate.compareTo(lastExecutedLogDateWithDaysGap) >= 0;
	}

	protected TeleplanResponse taskRun() throws Exception {
		logger.info("**********started to renew teleplan password");
		String newPwd = RandomStringUtils.random(12, true, true);
		TeleplanUserPassDAO dao = new TeleplanUserPassDAO();
		String[] userpass = dao.getUsernamePassword();

		TeleplanAPI tAPI = new TeleplanAPI();
		tAPI.login(userpass[0], userpass[1]);
		TeleplanResponse tr = tAPI.changePassword(userpass[0], userpass[1], newPwd, newPwd);
		logger.info("**********renew teleplan password result:" + tr.getResult());
		if (tr.isSuccess()) {
			dao.saveUpdatePasssword(newPwd);
		} else {
			deactivateAutoDownloadRemitTask();
		}
		return tr;
	}

	private void deactivateAutoDownloadRemitTask() {
		PeriodDef def = defDao.getByName("teleplan_remit_download");
		def.setActive(false);
		defDao.merge(def);
	}
}
