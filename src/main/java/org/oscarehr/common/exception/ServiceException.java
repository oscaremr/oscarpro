/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.common.exception;

public class ServiceException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    private String code;

    public ServiceException() {
        super();
    }

    public ServiceException(String code, String message) {
        super(message);
        this.code = code;
    }

    public ServiceException(String code, String message, Throwable t) {
        super(message, t);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ServiceException(String message)
    {
        super(message);
    }

    public ServiceException(String message, Throwable t)
    {
        super(message, t);
    }
}
