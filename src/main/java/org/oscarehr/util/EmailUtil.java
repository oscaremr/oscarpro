/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.util;

import javax.activation.DataHandler;
import javax.crypto.SecretKey;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.PreencodedMimeBodyPart;
import javax.mail.util.ByteArrayDataSource;
import javax.ws.rs.core.MediaType;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.sun.mail.smtp.SMTPTransport;
import com.sun.mail.util.MailSSLSocketFactory;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.oscarehr.common.dao.EmailLogDao;
import org.oscarehr.common.dao.SMTPConfigDao;
import org.oscarehr.common.exception.ServiceException;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.EmailAttachment;
import org.oscarehr.common.model.EmailLog;
import org.oscarehr.common.model.EmailRequest;
import org.oscarehr.common.model.SMTPConfig;
import org.oscarehr.ws.rest.to.model.EmailLogTo1;
import org.oscarehr.ws.rest.to.model.EmailSpecificationTo1;
import org.oscarehr.ws.rest.to.model.SMTPConfigTo1;
import org.springframework.beans.factory.annotation.Autowired;

public class EmailUtil {

  public static final String RETURN_PATH = "Return-path";
  public static final String TRUE = "true";
  public static final String MAIL_TRANSPORT_PROTOCOL = "mail.transport.protocol";
  public static final String MAIL_SMTP_HOST = "mail.smtp.host";
  public static final String MAIL_SMTPS_HOST = "mail.smtps.host";
  public static final String MAIL_SMTPS_AUTH = "mail.smtps.auth";
  public static final String MAIL_SMTP_PORT = "mail.smtp.port";
  public static final String MAIL_SMTPS_SSL_SOCKET_FACTORY = "mail.smtps.ssl.socketFactory";
  public static final String MAIL_SMTPS_USER = "mail.smtps.user";
  public static final String MAIL_SMTPS_PASSWORD = "mail.smtps.password";
  public static final String MAIL_SMTPS_SSL_TRUST = "mail.smtps.ssl.trust";
  public static final String MAIL_SMTPS_PORT = "mail.smtps.port";
  public static final String MAIL_SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
  public static final String TEST_EMAIL_MESSAGE_BODY =
      "This is a test email message sent automatically while testing the configuration settings for your server. Your configuration is successful.";

  private static final String SCRAMBLED_PASSWORD = "*******************************++++++++++++++++++++++++++++++++++++++";

  private static final String PASSWORD_KEY = "a9998s7h7p886i2r2r556e";
  private static final char SPLIT = 'i';
  private static final char JOIN = '6';
  private static final List<String> SUPPORTED_IMAGE_TYPES = Arrays.asList("jpeg", "jpg", "png", "gif");
  private Logger logger = MiscUtils.getLogger();

  @Autowired
  SMTPConfigDao smtpConfigDao;
  @Autowired
  EmailLogDao emailLogDao;

  public void processSendEmailRequest(EmailRequest emailRequest) {

    SMTPConfig config = smtpConfigDao.getEmailConfig();
    if (config == null) {
      throw new ServiceException("SMTPConfig is null ; emails cannot be sent.");
    }

    String password = decryptPassword(config.getPassword());

    sendHtmlMail(
        config.getUrl(),
        emailRequest.getRecipient(),
        config.getDisplayName(),
        config.getReplyToEmail(), // fromAddress
        config.getReplyToEmail(), // replyToAddress
        config.getUseSecurity(),
        config.getUsername(),
        password,
        Integer.toString(config.getPort()),
        emailRequest.getSubject(),
        emailRequest.getBody(),
        emailRequest.getAttachments(),
        emailRequest.getEmailType(),
        false);
  }

  public boolean sendTestEmail(SMTPConfigTo1 smtpConfigTo1) {

    // set up a test email request with a null dempgraphic
    Demographic dem = new Demographic();
    dem.setEmail(smtpConfigTo1.getReplyToEmail());
    EmailRequest emailRequest = new EmailRequest(
        dem, "Test Message",
        TEST_EMAIL_MESSAGE_BODY,
        new ArrayList<EmailAttachment>(), EmailSpecificationTo1.EmailType.CUSTOM_TEMPLATE);

    String password = smtpConfigTo1.getPassword();
    // if the password is equal to the scrambled password, then it has already been saved and not changed, so get it from the DB
    if (StringUtils.isNotEmpty(password) && SCRAMBLED_PASSWORD.equals(password)) {
      SMTPConfig config = smtpConfigDao.getEmailConfig();
      password = decryptPassword(config.getPassword());
    }

    sendHtmlMail(
        smtpConfigTo1.getUrl(),
        emailRequest.getRecipient(),
        smtpConfigTo1.getDisplayName(),
        smtpConfigTo1.getReplyToEmail(), // fromAddress
        smtpConfigTo1.getReplyToEmail(), // replyToAddress
        smtpConfigTo1.getUseSecurity(),
        smtpConfigTo1.getUsername(),
        password,
        Integer.toString(smtpConfigTo1.getPort()),
        emailRequest.getSubject(),
        emailRequest.getBody(),
        emailRequest.getAttachments(),
        emailRequest.getEmailType(),
        true);

    return true;
  }

  /**
   * Configure & send a message using SMTP
   */
  private void sendHtmlMail(
      final String smtpHost,
      final Demographic recipient,
      final String displayName,
      final String fromAddress,
      final String replyToAddress,
      final boolean useTLSSSL,
      final String userName,
      final String password,
      final String port,
      final String subject,
      final String content,
      final List<EmailAttachment> attachments,
      final EmailSpecificationTo1.EmailType emailType,
      boolean isTestEmail) {

    // create the log object and set prelim data
    final Date sentDate = new Date();
    EmailLog emailLog = new EmailLog(sentDate);
    if (!isTestEmail) {
      emailLog.setEmailType(emailType);
      emailLog.setRecipient(recipient);
      if (getPersistEmailContentsConfig()) {
        emailLog.setContent(content);
      }
      // set to SUCCESS once it has completed successfully
      emailLog.setStatus(EmailLogTo1.EmailSentStatus.FAILURE);
    }

    final List<String> contentList = Arrays.asList(content);

    try {
      // set the SMTP host property
      Properties props = System.getProperties();
      props.put(MAIL_TRANSPORT_PROTOCOL, "smtp");

      Session session = null;
      if (useTLSSSL) {
        props.put(MAIL_SMTPS_HOST, smtpHost);
        props.put(MAIL_SMTPS_AUTH, TRUE);
        props.put(MAIL_SMTPS_USER, userName);
        props.put(MAIL_SMTPS_PASSWORD, password);
        props.put(MAIL_SMTPS_SSL_TRUST, "*");
        props.put(MAIL_SMTPS_PORT, port);
        props.put(MAIL_SMTP_STARTTLS_ENABLE, TRUE);

        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        // or
        // sf.setTrustedHosts(new String[] { "my-server" });
        // also use following for additional safety
        //props.put("mail.smtp.ssl.checkserveridentity", "true");
        props.put(MAIL_SMTPS_SSL_SOCKET_FACTORY, sf);

        // create the SMTP host property and get the default Session with password authenticator
        session = Session.getInstance(props, new Authenticator() {
          protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(userName, password);
          }
        });
      } else {
        props.put(MAIL_SMTP_HOST, smtpHost);
        props.put(MAIL_SMTP_PORT, port); // default port 25 or 587 (non TLS/SSL), 465 (with TLS/SSL)
        // create the SMTP host property and get the default Session
        session = Session.getInstance(props, null);
      }

      // Create the message, set the subject, from, to, reply to, and content
      Message message = new MimeMessage(session);
      message.addHeader(RETURN_PATH, replyToAddress);
      message.setSentDate(sentDate);
      if (StringUtils.isNotEmpty(displayName)) {
        message.setFrom(toInternetAddressWithName(fromAddress, displayName));
      } else {
        message.setFrom(toInternetAddress(fromAddress));
      }
      message.setRecipient(Message.RecipientType.TO, toInternetAddress(recipient.getEmail()));
      message.setReplyTo(toInternetAddresses(Arrays.asList(replyToAddress)));
      setMessageSubject(message, subject);
      setMessageContent(message, contentList, attachments, emailLog);

      if (useTLSSSL) {
        SMTPTransport smtpTransport = (SMTPTransport) session.getTransport("smtp");
        // connect
        smtpTransport.connect(smtpHost, userName, password);
        // send the message
        smtpTransport.sendMessage(message, message.getAllRecipients());
        smtpTransport.close();
        emailLog.setStatus(EmailLogTo1.EmailSentStatus.SUCCESS);
      } else {
        // send the message
        Transport.send(message);
        emailLog.setStatus(EmailLogTo1.EmailSentStatus.SUCCESS);
      }
    } catch (MessagingException | UnsupportedEncodingException | GeneralSecurityException e) {
      throw new ServiceException("Failed to process SMTP email request", e);
    } finally {
      if (!isTestEmail) {
        emailLogDao.saveEntity(emailLog);
      }
    }
  }

  /**
   * Decrypt password
   */
  private String decryptPassword(byte[] password) {
    try {
      byte[] decryptBytePassword = EncryptionUtils.decrypt(getSecretKey(), password);
      return new String(decryptBytePassword, "UTF-8");
    } catch (Exception e) {
      logger.error("Error decrypting password", e);
    }
    return null;
  }

  public SecretKey getSecretKey() {
    return EncryptionUtils.generateEncryptionKey(getKeyPass(PASSWORD_KEY, SPLIT, JOIN));
  }

  private static String getKeyPass(String str, char split, char join) {

    String[] tokens = StringUtils.split(str, split);
    Arrays.sort(tokens);
    return StringUtils.join(tokens, join);
  }

  /**
   * Convert given address string to an InternetAddress instance
   */
  private static InternetAddress toInternetAddress(String address) throws AddressException {
    return new InternetAddress(address, true /* strict */);
  }

  private static InternetAddress toInternetAddressWithName(String address, String name) throws UnsupportedEncodingException {
    return new InternetAddress(address, name);
  }

  /**
   * Convert given list of address strings to an array of InternetAddress instances
   */
  private static InternetAddress[] toInternetAddresses(List<String> addressList) throws AddressException {

    if (addressList == null || addressList.isEmpty()) {
      return new InternetAddress[0];
    }

    InternetAddress[] iAddresses = new InternetAddress[addressList.size()];
    for (int i = 0; i < addressList.size(); i++) {
      iAddresses[i] = new InternetAddress(addressList.get(i), true /* strict */);
    }
    return iAddresses;
  }

  /**
   * Set subject of the given message to the given string if not null/blank
   */
  private static void setMessageSubject(Message message, String subject) throws MessagingException {
    if (!StringUtils.isBlank(subject)) {
      message.setSubject(subject);
    }
  }

  /**
   * Populate the content of the given message with the given data elements
   */
  private static void setMessageContent(Message message, List<String> contentList, List<EmailAttachment> attachments, EmailLog emailLog)
      throws MessagingException {
    Pattern imgRegExp = Pattern
        .compile("(<img[^>]+src\\s*=\\s*['\"]\\s*data:image/(\\w+);base64,([^'\"]+)['\"][^>]*>)");

    Multipart multipart = new MimeMultipart("related");
    Logger logger = MiscUtils.getLogger();
    int i = 1;
    for (String content : contentList) {
      List<MimeBodyPart> images = new ArrayList<MimeBodyPart>();

//			convert base64 embedded images to use CID + attachement so they will be displayed in more email clients
      final Matcher matcher = imgRegExp.matcher(content);
      while (matcher.find()) {
        String imageTag = matcher.group(1);
        String imageType = matcher.group(2);
        String image = matcher.group(3);

        if (content.indexOf(imageTag) != -1) {
          if (!SUPPORTED_IMAGE_TYPES.contains(imageType.toLowerCase())) {
            logger.error("Unsupported Image file type: " + imageType);
            throw new ServiceException("Unsupported Image file type");
          }

          MimeBodyPart imagePart = new PreencodedMimeBodyPart("base64");
          imagePart.setFileName("image" + i + "." + imageType);
          imagePart.setHeader("Content-ID", "<image" + i + ">");
          imagePart.setContent(image, "image/" + imageType);
          imagePart.setDisposition(MimeBodyPart.INLINE);
          images.add(imagePart);

          content = content.replace(imageTag, "<img src=\"cid:image" + i + "\">");
          i++;
        }
      }


      MimeBodyPart htmlContent = new MimeBodyPart();
      htmlContent.setContent(content, MediaType.TEXT_HTML);
      multipart.addBodyPart(htmlContent);

      for (MimeBodyPart image : images) {
        multipart.addBodyPart(image);
      }

    }

    for (EmailAttachment attachment : attachments) {
      if (getPersistEmailContentsConfig()) {
        emailLog.getAttachments().add(attachment);
      }
      MimeBodyPart bodyPart = new MimeBodyPart();
      ByteArrayDataSource bds = new ByteArrayDataSource(attachment.getFileContent(),
          MediaType.APPLICATION_OCTET_STREAM);
      bodyPart.setDataHandler(new DataHandler(bds));
      bodyPart.setFileName(attachment.getFileName());
      multipart.addBodyPart(bodyPart);
    }

    message.setContent(multipart);
  }

  private static boolean getPersistEmailContentsConfig() {
    return oscar.OscarProperties.getInstance().getBooleanProperty("EMAIL_PERSIST_CONTENTS", "true");
  }
}
