/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.util;

public class FaxUtils {

  /**
   * Determine if fax number entered is valid
   * @param faxNumber The fax number to check
   * @return Returns true if valid, false otherwise
   */
  public static boolean isValidFaxNumber(String faxNumber) {
    String faxNumberTrimmed = trimFaxNumber(faxNumber);
    String phoneRegex = "^(1|9[19]?)?\\d{10}$";
    return faxNumberTrimmed.matches(phoneRegex);
  }

  public static String convertToRegExp(String faxNumber) {
    return FaxUtils.trimFaxNumber(faxNumber)
        .replaceAll(
            "(1|9[19]?)?(\\d{3})(\\d{3})(\\d{4})",
            "(1|9[19]?)?[()\\-\\s]*($2)[()\\-\\s]*($3)[()\\-\\s]*($4)");
  }

  static String trimFaxNumber(String faxNumber) {
    return faxNumber.replaceAll("[^0-9]", "");
  }
}
