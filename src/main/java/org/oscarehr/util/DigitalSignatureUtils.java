/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */

package org.oscarehr.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.oscarehr.common.dao.DigitalSignatureDao;
import org.oscarehr.common.dao.DigitalSignatureFavouriteDao;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.model.DigitalSignature;
import org.oscarehr.common.model.DigitalSignatureFavourite;
import org.oscarehr.common.model.UserProperty;
import oscar.OscarProperties;

@Slf4j
public class DigitalSignatureUtils {

	public static final String SIGNATURE_REQUEST_ID_KEY = "signatureRequestId";

	public static String generateSignatureRequestId(String providerNo) {
		return (providerNo + System.currentTimeMillis());
	}

	public static String getTempFilePath(String signatureRequestId) {
		return (System.getProperty("java.io.tmpdir") + "/signature_" + signatureRequestId + ".jpg");
	}

	/**
	 * This method will check if digital signatures is enabled or not. It will only attempt to save it
	 * if it's enabled.
	 * @param loggedInInfo			  to check if digital signatures is enabled
	 * @param signatureRequestId  to generate the temp file name
	 * @param demographicId       demographic to link to
	 * @return									  the digital signature object
	 */
  public static DigitalSignature storeDigitalSignatureFromTempFileToDB(
      LoggedInInfo loggedInInfo, String signatureRequestId, int demographicId) {
    return storeDigitalSignatureFromTempFileToDB(
        loggedInInfo, signatureRequestId, demographicId, null);
  }

  /**
   * This method will check if digital signatures is enabled or not. It will only attempt to save it
   * if it's enabled. If a label is provided, the signature will be saved as a favourite.
   *
   * @param demographicId of the owner of this signature
   */
  public static DigitalSignature storeDigitalSignatureFromTempFileToDB(
      final LoggedInInfo loggedInInfo,
      final String signatureRequestId,
      final int demographicId,
      final String label) {
		DigitalSignature digitalSignature = null;
		if (loggedInInfo.getCurrentFacility().isEnableDigitalSignatures()) {
			String filename = DigitalSignatureUtils.getTempFilePath(signatureRequestId);
			digitalSignature = saveSignature(loggedInInfo, filename, demographicId, label);
		}
		return (digitalSignature);
	}
	
	public static DigitalSignature storeSignatureStamp(LoggedInInfo loggedInInfo, int demographicId) {
		DigitalSignature digitalSignature = null;
		
		if (loggedInInfo.getCurrentFacility().isEnableDigitalSignatures()) {
			UserPropertyDAO userPropertyDao = SpringUtils.getBean(UserPropertyDAO.class);
			UserProperty signatureProperty = userPropertyDao.getProp(loggedInInfo.getLoggedInProviderNo(), UserProperty.PROVIDER_CONSULT_SIGNATURE);
			if (signatureProperty != null) {
				String filepath = OscarProperties.getInstance().getProperty("eform_image");
				String filename = filepath + (!filepath.endsWith("/") ? "/" : "") +  signatureProperty.getValue();
				
				digitalSignature = saveSignature(loggedInInfo, filename, demographicId);
			}
		}
		
		return digitalSignature;
	}

  static DigitalSignature saveSignature(
      final LoggedInInfo loggedInInfo, final String fileName, final int demographicId) {
    return saveSignature(loggedInInfo, fileName, demographicId, null);
  }

	static DigitalSignature saveSignature(
      final LoggedInInfo loggedInInfo,
      final String filename,
      final int demographicId,
      final String label) {
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = createFileInputStream(filename);
			byte[] image = new byte[1024 * 256];
			fileInputStream.read(image);

			DigitalSignature digitalSignature = new DigitalSignature();
			digitalSignature.setDateSigned(new Date());
			digitalSignature.setDemographicId(demographicId);
			digitalSignature.setFacilityId(loggedInInfo.getCurrentFacility().getId());
			digitalSignature.setProviderNo(loggedInInfo.getLoggedInProviderNo());
			digitalSignature.setSignatureImage(image);

			val digitalSignatureDao = (DigitalSignatureDao) SpringUtils.getBean(
					"digitalSignatureDao");
			val savedDigitalSignature = digitalSignatureDao.saveEntity(digitalSignature);

			// signatures with labels are always favourites
			if (StringUtils.isNotEmpty(label)) {
				savedDigitalSignature.setLabel(label);
				digitalSignatureDao.saveEntity(savedDigitalSignature);
				saveFavouriteSignature(savedDigitalSignature);
			}

			return savedDigitalSignature;
		} catch (FileNotFoundException e) {
			log.warn("Signature file not found. User probably didn't collect a signature.", e);
			return null;
		} catch (Exception e) {
			log.error("UnexpectedError.", e);
			return null;
		} finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException e) {
					log.error("Unexpected error.", e);
				}
			}
		}
	}

	/**
	 * This method helps with testing by allowing us to mock the FileInputStream object.
	 */
  protected static FileInputStream createFileInputStream(String filename)
      throws FileNotFoundException {
    return new FileInputStream(filename);
  }

	static void saveFavouriteSignature(final DigitalSignature signature) {
		val digitalSignatureFavourite = new DigitalSignatureFavourite();
		digitalSignatureFavourite.setSignatureId(signature.getId());
		digitalSignatureFavourite.setArchived(false);
		DigitalSignatureFavouriteDao digitalSignatureFavouriteDao = SpringUtils.getBean(
				DigitalSignatureFavouriteDao.class);
		digitalSignatureFavouriteDao.saveEntity(digitalSignatureFavourite);
	}

	/**
	 * Updates a signature that already exists in the database
	 * @param loggedInInfo Logged in info of the logged in user
	 * @param signatureId The id of the signature to update
	 * @param filePath The path to the temporary file that the new signature is stored in
	 * @return The updated DigitalSignature
	 */
	public static DigitalSignature updateSignature(LoggedInInfo loggedInInfo, Integer signatureId, String filePath) {
		DigitalSignature signature = null;
		if (loggedInInfo.getCurrentFacility().isEnableDigitalSignatures()) {
			DigitalSignatureDao digitalSignatureDao = (DigitalSignatureDao) SpringUtils.getBean("digitalSignatureDao");

			String filename = getTempFilePath(filePath);
			try (FileInputStream fileInputStream = new FileInputStream(filename)) {
				byte[] image = new byte[1024 * 256];
				fileInputStream.read(image);

				signature = digitalSignatureDao.find(signatureId);
				signature.setSignatureImage(image);
				digitalSignatureDao.merge(signature);
			} catch (IOException e) {
				log.error("Could not update signature " + signatureId);
			}
		}
		return signature;
	}

    public static boolean uploadThumbnail(String signatureId, String demographicNo,
        String thumbnailImage) {
      try {
        String rootPath = oscar.OscarProperties.getInstance().getProperty("drawing_tool.folder");
        File folder = new File(rootPath);
        if (!folder.exists()) {
          folder.mkdirs();
        }
        if (demographicNo == null) {
          demographicNo = "";
        }
        String filename = folder + File.separator + demographicNo + "_" + signatureId + ".png";
        FileOutputStream fos = new FileOutputStream(new File(filename));

        String imageString = thumbnailImage.substring(thumbnailImage.indexOf(",") + 1);

        Base64 b64 = new Base64();
        byte[] imageByteData = imageString.getBytes();
        byte[] imageData = b64.decode(imageByteData);
        if (imageData != null) {
          fos.write(imageData);
        }

        fos.flush();
        fos.close();

        MiscUtils.getLogger()
            .debug("Thumbnail uploaded: " + filename + ", size=" + imageData.length);
        return true;
      } catch (Exception e) {
        MiscUtils.getLogger().error("Error receiving thumbnail : ", e);
        return false;
      }
    }
}
