/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.util;

import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.SystemPreferences;
import oscar.OscarProperties;

public class CareConnectUtils {
    public enum CareConnectURL {
        PPN("https://bc.careconnect.ca/Welcome/Search"),
        NOPPN("https://health.careconnect.ca/Welcome/Search"),
        DISABLED("");

        public final String url;

        CareConnectURL(String url) {
            this.url = url;
        }
    }
    
    public static String[] getEffectiveCareConnectURL() {
        SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
        OscarProperties oscarProperties = OscarProperties.getInstance();

        // Get the OSCAR Property value for the BC_CareConnect_URL
        // If this property is set, this will override anything set in the front-end
        String careConnectURL = oscarProperties.getProperty("BC_CareConnect_URL", "").trim();
        String careConnectCode;
        
        // If there is no oscar property set, get the system preference
        if (StringUtils.isBlank(careConnectURL)) {
            SystemPreferences careConnectURLSystemPreference = systemPreferencesDao.findPreferenceByName("care_connect_url");
            if (careConnectURLSystemPreference == null) {
                careConnectURLSystemPreference = new SystemPreferences("care_connect_url", "DISABLED");
            }
            careConnectCode = careConnectURLSystemPreference.getValue();
            careConnectURL = CareConnectUtils.CareConnectURL.valueOf(careConnectURLSystemPreference.getValue()).url;
        }
        else {
            careConnectCode = "CUSTOM";
        }
        
        return new String[]{careConnectCode,careConnectURL};
    }
    
    public static String getEffectiveCareConnectOrgs() {
        SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
        OscarProperties oscarProperties = OscarProperties.getInstance();
        
        // This is the opposite of getEffectiveCareConnectURL
        // Check for a system preference first, if none exists then fall back on OSCAR Properties
        String careConnectOrgs;
        SystemPreferences careConnectOrgsSystemPreference = systemPreferencesDao.findPreferenceByName("care_connect_orgs");
        if (careConnectOrgsSystemPreference != null) {
            careConnectOrgs = careConnectOrgsSystemPreference.getValue();
        } else {
            careConnectOrgs = oscarProperties.getProperty("BC_CareConnect_Orgs", "").trim();
        }

        return careConnectOrgs;
    }
}
