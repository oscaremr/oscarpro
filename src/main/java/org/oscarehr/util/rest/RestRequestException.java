package org.oscarehr.util.rest;


public class RestRequestException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private final StatusResponse statusResponse;

  public RestRequestException() {
    statusResponse = null;
  }

  public RestRequestException(StatusResponse statusResponse) {
    super(statusResponse.getMessage());
    this.statusResponse = statusResponse;
  }

  public RestRequestException(Throwable cause) {
    super(cause);
    statusResponse = null;
  }

  public StatusResponse getStatusResponse() {
    return statusResponse;
  }
}
