/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.util.rest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.StringUtils;

/**
 * This is the response returned by the OscarPro API for the JIT provisioning request, and
 * changePassword request. It is used as a JSON template. Added additional fields to handle
 * SpringBoot auth errors.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class StatusResponse {

  public enum StatusValues {
    OK,
    FAIL
  }

  String status;
  String message;
  String responseJson;
  String errorCode;
  String error;
  String timestamp;
  String exception;
  String path;

  @JsonIgnore
  public static StatusResponse Fail(String msg) {
    return new StatusResponse().setStatus(StatusValues.FAIL.name()).setError(msg);
  }

  @JsonIgnore
  public static StatusResponse Ok(String msg) {
    return new StatusResponse().setStatus(StatusValues.OK.name()).setMessage(msg);
  }

  @JsonIgnore
  public StatusResponse failed(String msg) {
    status = StatusValues.FAIL.name();
    error = msg;
    return this;
  }

  @JsonIgnore
  public StatusResponse ok(String msg) {
    status = StatusValues.OK.name();
    message = msg;
    return this;
  }

  @JsonIgnore
  public boolean isOk() {
    return StatusValues.OK.name().equals(status);
  }

  public String getStatus() {
    return status;
  }

  public StatusResponse setStatus(String status) {
    this.status = status;
    return this;
  }

  public String getMessage() {
    return message;
  }

  public StatusResponse setMessage(String message) {
    this.message = message;
    return this;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public StatusResponse setErrorCode(String errorCode) {
    this.errorCode = errorCode;
    return this;
  }

  public String getError() {
    return error;
  }

  public StatusResponse setError(String error) {
    this.error = error;
    return this;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public StatusResponse setTimestamp(String timestamp) {
    this.timestamp = timestamp;
    return this;
  }

  public String getException() {
    return exception;
  }

  public StatusResponse setException(String exception) {
    this.exception = exception;
    return this;
  }

  public String getPath() {
    return path;
  }

  public StatusResponse setPath(String path) {
    this.path = path;
    return this;
  }

  @JsonIgnore
  public String getResponseJson() {
    return responseJson;
  }

  public StatusResponse setResponseJson(String responseJson) {
    this.responseJson = responseJson;
    return this;
  }

  /** A more concise message that can be displayed to the user */
  public String toUserMessage() {
    return StringUtils.isNotBlank(message) ? message : error;
  }

  /** These details usually for log files or administrators, and not presented to the average user */
  public String toDetailedMessage() {
    return message + " : " + errorCode + " - " + error;
  }

  @Override
  public String toString() {
    return "StatusResponse{"
        + "status='" + status
        + "', message='" + message
        + "', errorCode='" + errorCode
        + "', error='" + error
        + "'}";
  }
}
