/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.util.rest;

import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.ClientCookie;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.oscarehr.util.MiscUtils;
import oscar.OscarProperties;

/**
 * <code>RestApiClient</code> provides methods for making REST requests to OSCAR Pro OSCAR Pro lives
 * in the same omtcat service on the same server, so requests are authorized by session cookies and
 * not OAuth tokens.
 *
 * <p>Implemented June 2021 for AH-255 - Okta Change Password
 */
public class RestApiClient {
  private Logger logger = MiscUtils.getLogger();

  /** objectMapper can be a hit on performance, avoid creating too many * */
  private ObjectMapper jsonMapper = new ObjectMapper();

  private String getCookieByName(String name, BasicCookieStore cookies) {
    if (name == null || cookies == null || cookies.getCookies() == null) {
      return null;
    }
    for (org.apache.http.cookie.Cookie cookie : cookies.getCookies()) {
      if (name.equals(cookie.getName())) {
        return cookie.getValue();
      }
    }
    return null;
  }

  /**
   * Make REST request to Oscar Pro to change Not public because it does not check okta status for
   * user, see other method.
   *
   * @param requestBody
   * @param request - used for forwarding cookies from original request
   * @return
   */
  public <Rq,Rs> Rs postRequest(String url, Rq requestBody, Class<Rs> responseType, HttpServletRequest request) {

    if (requestBody == null) {
      throw new RestRequestException(StatusResponse.Fail("No request body."));
    }
    String requestType = requestBody.getClass().getSimpleName();
    String jsonResponseBody = null;
    try {
      logger.debug("  -- POST request for " + requestType);
      BasicCookieStore cookies = copyRequestCookies(request);
      String jsonRequest = jsonMapper.writeValueAsString(requestBody);
      jsonResponseBody = sendHttpPostRequest(url, jsonRequest, cookies);
      logger.debug("  -- POSTed request, response was: " + jsonResponseBody);
      try {
        if (StringUtils.isNotBlank(jsonResponseBody)) {
          return jsonMapper.readValue(jsonResponseBody, responseType);
        } else {
          return responseType.newInstance();
        }
      } catch (Exception ex) {
        // Any exception while parsing response
        logger.warn("Failed to parse API response: " +jsonResponseBody + " " + ex.getMessage());
        throw new RestRequestException(StatusResponse.Fail("POST request failed, " + ex.getMessage()).setResponseJson(jsonResponseBody));
      }

    } catch (RestRequestException ex) {
      throw ex;
    } catch (Exception ex) {
      String msg = "Failed to post REST request for " + requestType + " : "
          + (jsonResponseBody != null ? jsonResponseBody + " :  " : "");
      if (ex instanceof UnrecognizedPropertyException) {
        logger.warn(msg); // don't want stack trace for JSON parsing
      } else {
        logger.warn(msg, ex);
      }
      throw new RestRequestException(StatusResponse.Fail(msg));
    }
  }

  private String sendHttpPostRequest(String url, String jsonRequestBody, BasicCookieStore cookies)
      throws IOException {

    HttpEntity reqEntity = new ByteArrayEntity(jsonRequestBody.getBytes("UTF-8"));

    String xsrfTokenFromCookie = getCookieByName("XSRF-TOKEN", cookies);
    logger.debug("sendHttpPostRequest: url: " + url);
    HttpPost httpPost = new HttpPost(url);
    httpPost.setEntity(reqEntity);
    httpPost.setHeader("Content-type", "application/json");
    httpPost.setHeader("'X-XSRF-TOKEN", xsrfTokenFromCookie);
    HttpContext localContext = new BasicHttpContext();
    localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookies);

    // Send REST request
    try {
      HttpClient httpClient = new DefaultHttpClient();
      HttpResponse httpResponse = httpClient.execute(httpPost, localContext);
      // Parse the response code, and take appropriate acction
      HttpEntity responseBody = httpResponse.getEntity();
      String jsonResponseBody = EntityUtils.toString(responseBody);
      int statusCode = httpResponse.getStatusLine().getStatusCode();
      if (statusCode >= 200 && statusCode < 300) {
        return jsonResponseBody;
      } else if (statusCode >= 400 && statusCode < 500) {
        StatusResponse statusResponse =
            jsonMapper.readValue(jsonResponseBody, StatusResponse.class);
        throw new RestRequestException(statusResponse);
      }
      throw new RestRequestException(
          StatusResponse.Fail("Unexpected response from request:" + url));

    } catch (RestRequestException ex) {
      throw ex;
    } catch (Exception ex) {
      String msg = "Request failed for " + url + ", no response received";
      logger.warn(msg, ex);
      throw new RestRequestException(StatusResponse.Fail(msg));
    }
  }
  
  /**
   * Make REST request to Oscar Pro to remove user by okta_user_id
   * @param request - used for forwarding cookies from original request
   * @return StatusResponse of delete request outcome
   */
  public StatusResponse deleteRequest(String url, HttpServletRequest request) {
    try {
      logger.debug("  -- DELETE request for " + url);
      BasicCookieStore cookies = copyRequestCookies(request);
      sendHttpDeleteRequest(url, cookies);
    } catch (RestRequestException ex) {
      throw ex;
    } catch (Exception ex) {
      String msg = "Failed to post DELETE request for " + url;
      logger.warn(msg, ex);
      throw new RestRequestException(StatusResponse.Fail(msg));
    }
    return StatusResponse.Ok("OK");
  }

  private void sendHttpDeleteRequest(String url, BasicCookieStore cookies) {

    String xsrfTokenFromCookie = getCookieByName("XSRF-TOKEN", cookies);
    logger.debug("sendHttpPostRequest: url: " + url);
    HttpDelete httpDelete = new HttpDelete(url);
    httpDelete.setHeader("Content-type", "application/json");
    httpDelete.setHeader("'X-XSRF-TOKEN", xsrfTokenFromCookie);
    HttpContext localContext = new BasicHttpContext();
    localContext.setAttribute(HttpClientContext.COOKIE_STORE, cookies);

    // Send REST request
    try {
      HttpClient httpClient = new DefaultHttpClient();
      HttpResponse httpResponse = httpClient.execute(httpDelete, localContext);
      // Parse the response code, and take appropriate action on non-successful outcome
      HttpEntity responseBody = httpResponse.getEntity();
      String jsonResponseBody = EntityUtils.toString(responseBody);
      int statusCode = httpResponse.getStatusLine().getStatusCode();
      if (statusCode >= 400 && statusCode < 500) {
        StatusResponse statusResponse = jsonMapper.readValue(jsonResponseBody, StatusResponse.class);
        throw new RestRequestException(statusResponse);
      } else if (statusCode < 200 || statusCode >= 300) {
        throw new RestRequestException(
            StatusResponse.Fail("Unexpected response from request:" + url));
      }
    } catch (RestRequestException ex) {
      throw ex;
    } catch (Exception ex) {
      String msg = "Request failed for " + url + ", no response received";
      logger.warn(msg, ex);
      throw new RestRequestException(StatusResponse.Fail(msg));
    }
  }

  /**
   * Build a cookie store from cookies copied from provided request
   *
   * @param request - HttpServletRequest, the request to this service that is currently being
   *     processed
   */
  private BasicCookieStore copyRequestCookies(HttpServletRequest request) {
    Cookie[] cookies = request.getCookies();
    BasicCookieStore cookieStore = new BasicCookieStore();
    for (Cookie cookie : cookies) {
      String logMsg = "++++ Adding Cookie " + cookie.getName() + "=" + cookie.getValue();
      logger.debug(logMsg + ",  with domain:" + cookie.getDomain());
      BasicClientCookie clientCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
      clientCookie.setAttribute(ClientCookie.DOMAIN_ATTR, "true");
      // The domain must be set, otherwise the cookies will be sent by the HttpClient
      String domainName = getServerDomain();
      if (StringUtils.isNotBlank(cookie.getDomain())) {
        logger.debug(logMsg + ",  set domain from existing domain " + cookie.getDomain());
        clientCookie.setDomain(cookie.getDomain());
      } else {
        logger.debug(logMsg + ",  no existing domain, set domain " + domainName);
        clientCookie.setDomain(domainName);
      }
      if (StringUtils.isNotBlank(cookie.getPath())) {
        clientCookie.setPath(cookie.getPath());
      } else {
        clientCookie.setPath("/");
      }
      cookieStore.addCookie(clientCookie);
    }

    return cookieStore;
  }

  /** @return domain derived from the clinic.url property */
  private String getServerDomain() {
    String clinicUrl = OscarProperties.getLocalApiUrl();
    String domain = null;
    try {
      URL url = new URL(clinicUrl);
      domain = url.getHost();
    } catch (MalformedURLException e) {
      logger.info("Can't get domain from url='" + clinicUrl + "',  using 'localhost' instead");
    }
    return StringUtils.isNotBlank(domain) ? domain : "localhost";
  }

  /**
   * Build the URL for a local request to OSCAR Pro Use localhost for REST request to OSCAR Pro that
   * is deployed on the same server as OSCAR classic
   */
  public String buildOscarProApiUrl(String urlPath) {
    return OscarProperties.getOscarProApiUrl() + urlPath;
  }
}
