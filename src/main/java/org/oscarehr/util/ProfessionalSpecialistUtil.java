/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.util;

import oscar.OscarProperties;

public class ProfessionalSpecialistUtil {
  public static boolean isReferralNoValid(String referralNo) {

    String pattern = OscarProperties.getInstance().getProperty("referral_no.pattern", "\\d{6}");

    try {
      return referralNo.matches(pattern);
    } catch (Exception e) {
      MiscUtils.getLogger().info("Specified referral number invalid (" + referralNo + ")", e);
    }

    return false;
  }
}
