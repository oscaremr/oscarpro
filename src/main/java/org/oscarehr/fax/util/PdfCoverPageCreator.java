/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.fax.util;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.annotation.Nullable;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.oscarehr.common.dao.ClinicDAO;
import org.oscarehr.common.dao.ProfessionalSpecialistDao;
import org.oscarehr.common.model.ProfessionalSpecialist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class PdfCoverPageCreator {
  @Autowired
  private ClinicDAO clinicDao;

  @Autowired
  private ProfessionalSpecialistDao professionalSpecialistDao;

  public byte[] createCoverPage(final String note) {
    return createCoverPage(null, note);
  }

	public byte[] createCoverPage(final String subject, final String note) {
    try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
      val document = new Document();
      try {
        // We do not need to use the instance, but it must be created for the cover page to print
        PdfWriter.getInstance(document, os);
        document.open();

        val table = new PdfPTable(1);
        table.setWidthPercentage(95);

        PdfPCell cell = new PdfPCell(table);
        cell.setBorder(0);
        cell.setPadding(3);
        cell.setColspan(1);
        table.addCell(cell);

        val clinic = clinicDao.getClinic();
        val baseFont = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        val headerFont = new Font(baseFont, 28, Font.BOLD);
        val infoFont = new Font(baseFont, 12, Font.NORMAL);

        if (clinic != null) {
          cell = new PdfPCell(
                  new Phrase(
                      String.format(
                          "%s\n %s, %s, %s %s",
                          clinic.getClinicName(),
                          clinic.getClinicAddress(),
                          clinic.getClinicCity(),
                          clinic.getClinicProvince(),
                          clinic.getClinicPostal()),
                      headerFont));
        } else {
          cell = new PdfPCell(new Phrase("OSCAR", headerFont));
        }

        cell.setPaddingTop(100);
        cell.setPaddingLeft(25);
        cell.setPaddingBottom(25);
        cell.setBorderWidthBottom(1);
        table.addCell(cell);

        val infoTable = new PdfPTable(1);
        cell = new PdfPCell();
        cell.setPaddingTop(25);
        cell.setPaddingLeft(25);
        cell.setPaddingRight(25);

        Phrase p;
        if (subject != null && !subject.isEmpty()) {
          val subjectFont = new Font(baseFont, 14, Font.BOLD);
          p = new Phrase(subject, subjectFont);
          Paragraph par = new Paragraph(p);
          par.setSpacingAfter(16);

          cell.addElement(par);
        }

        p = new Phrase(infoFont.getSize() * 1.5f, note, infoFont);
        cell.addElement(p);

        infoTable.addCell(cell);
        table.addCell(infoTable);

        document.add(table);
      } finally {
        document.close();
      }

      return os.toByteArray();
		} catch (DocumentException | IOException ex) {
			log.error("PDF COVER PAGE ERROR", ex);
			return new byte[] {};
		}
	}

	public byte[] createStandardCoverPage(final String note, final @Nullable String specialistId) {
    try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
      val document = new Document();
      try {
        document.setPageSize(new Rectangle(PageSize.LETTER));
        // We do not need to use the instance, but it must be created for the cover page to print
        PdfWriter.getInstance(document, os);
        document.open();
        val baseFont = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        val headerFont = new Font(baseFont, 50, Font.NORMAL);
        val infoFont = new Font(baseFont, 12, Font.NORMAL);

        document.add(new Phrase("\n\n", infoFont));
        document.add(new Phrase("Fax Message", headerFont));

        if (specialistId != null && !specialistId.isEmpty()) {
          final ProfessionalSpecialist specialist = professionalSpecialistDao.find(Integer.parseInt(specialistId));
          if (specialist != null) {
            val clinicInfo = "\n\nRecipient: " + specialist.getFormattedTitle() + "\n"
								+ "Phone Number: " + specialist.getPhoneNumber() + "\n"
								+ "Fax Number: " + specialist.getFaxNumber() + "\n";
            document.add(new Phrase(clinicInfo, infoFont));
          }
        }

        document.add(new Phrase("\n\n\n" + note, infoFont));
      } finally {
        document.close();
      }

      return os.toByteArray();
		} catch (DocumentException | IOException ex) {
			log.error("PDF COVER PAGE ERROR", ex);
			return new byte[] {};
		}
	}
}
