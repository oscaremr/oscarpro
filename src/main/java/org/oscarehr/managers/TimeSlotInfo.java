package org.oscarehr.managers;

public final class TimeSlotInfo {
    private Character templateCode;
    private String duration;
    private boolean availability;

    public TimeSlotInfo(Character templateCode) {
        this.templateCode = templateCode;
    }
    public Character getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(Character templateCode) {
        this.templateCode = templateCode;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }
    
    public String toString() {
        return templateCode.toString();
    }
}
