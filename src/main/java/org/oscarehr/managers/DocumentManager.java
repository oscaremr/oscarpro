/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.managers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import lombok.NonNull;
import lombok.val;
import lombok.var;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.oscarehr.common.dao.CtlDocumentDao;
import org.oscarehr.common.dao.DocumentDao;
import org.oscarehr.common.dao.PatientLabRoutingDao;
import org.oscarehr.common.dao.ProviderInboxRoutingDao;
import org.oscarehr.common.dao.ProviderLabRoutingDao;
import org.oscarehr.common.model.ConsentType;
import org.oscarehr.common.model.CtlDocument;
import org.oscarehr.common.model.CtlDocumentPK;
import org.oscarehr.common.model.Document;
import org.oscarehr.common.model.PatientLabRouting;
import org.oscarehr.common.model.ProviderInboxItem;
import org.oscarehr.common.model.ProviderLabRoutingModel;
import org.oscarehr.util.LoggedInInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import oscar.log.LogAction;
import oscar.oscarLab.ca.all.upload.ProviderLabRouting;

@Service
public class DocumentManager {
	@Autowired
	private DocumentDao documentDao;

	@Autowired
	private CtlDocumentDao ctlDocumentDao;

	@Autowired
	private PatientConsentManager patientConsentManager;
	
	@Autowired
	private ProviderInboxRoutingDao providerInboxRoutingDao;

	@Autowired
	private ProviderLabRoutingDao providerLabRoutingDao;
	
	@Autowired
	private PatientLabRoutingDao patientLabRoutingDao;
	
	public Document getDocument(LoggedInInfo loggedInInfo, Integer id)
	{
		Document result=documentDao.find(id);
		
		//--- log action ---
		if (result != null) {
			LogAction.addLogSynchronous(loggedInInfo, "DocumentManager.getDocument", "id=" + id);
		}

		return(result);
	}
	
	public CtlDocument getCtlDocumentByDocumentId(LoggedInInfo loggedInInfo, Integer documentId)
	{
		CtlDocument result=ctlDocumentDao.getCtrlDocument(documentId);
		
		//--- log action ---
		if (result != null) {
			LogAction.addLogSynchronous(loggedInInfo, "DocumentManager.getCtlDocumentByDocumentNoAndModule", "id=" + documentId);
		}

		return(result);
	}
	
	public Document addDocument(LoggedInInfo loggedInInfo, Document document, CtlDocument ctlDocument) {
		documentDao.persist(document);
		ctlDocument.getId().setDocumentNo(document.getDocumentNo());
		ctlDocumentDao.persist(ctlDocument);
		LogAction.addLogSynchronous(loggedInInfo, "DocumentManager.addDocument", "id=" + document.getId());
		return(document);
	}

  /**
   * Creates a document and saves it to the provided demographic
   *
   * @param loggedInInfo  The logged in info of the current user
   * @param document      Document to create
   * @param demographicNo The demographic number to save the document to
   * @param providerNo    The optional provider number to route the document to
   * @param documentData  The document byte data
   * @return Document record from the database once it has been created
   * @throws IOException If actions related to getting document data fail
   */
  public Document createDocument(
      @NonNull final LoggedInInfo loggedInInfo,
      @NonNull final Document document,
      final Integer demographicNo,
      final String providerNo,
      @NonNull final byte[] documentData,
      final String docCreator
  ) {
    try {
      if (documentData.length == 0) {
        throw new IllegalArgumentException("Document data must not be empty");
      }
      saveDocumentToFileSystem(document, documentData);
      setDocumentCreator(document, docCreator, loggedInInfo);
      saveDocument(document, demographicNo, providerNo);
      LogAction.addLog(
          loggedInInfo,
          "Save Document to Patient",
          "Document Id",
          document.getId().toString(),
          demographicNo.toString(),
          document.getDocfilename()
      );
      return document;
    } catch (IOException e) {
      throw new RuntimeException("Error occurred while creating the document", e);
    }
  }

  private void saveDocumentToFileSystem(
      final Document document,
      final byte[] documentData
  ) throws IOException {
    val dateTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    val today = new Date();
    val documentPath = oscar.OscarProperties.getInstance().getProperty("DOCUMENT_DIR");
    if (documentPath == null || documentPath.isEmpty()) {
      throw new FileNotFoundException("Document path not found or not configured");
    }
    val fileName = dateTimeFormat.format(today) + "_" + document.getDocfilename();
    val file = new File(documentPath + File.separator + fileName);
    try {
      FileUtils.writeByteArrayToFile(file, documentData);
    } catch (IOException e) {
      throw new IOException("Error occurred while saving the document to the file system", e);
    }
    val numberOfPages = determineNumberOfPages(fileName, file);
    document.setNumberofpages(numberOfPages);
    document.setDocfilename(fileName);
  }

  private void setDocumentCreator(
      final Document document,
      final String docCreator,
      final LoggedInInfo loggedInInfo
  ) {
    document.setDoccreator(
        StringUtils.isNotBlank(docCreator) ? docCreator :
            StringUtils.isNotBlank(loggedInInfo.getLoggedInProviderNo())
                ? loggedInInfo.getLoggedInProviderNo() :
                ""
    );
  }

  private int determineNumberOfPages(final String fileName, final File file) throws IOException {
    if (!file.exists() || file.isDirectory()) {
      throw new FileNotFoundException(
          "The specified document file does not exist or is a directory");
    }
    var numberOfPages = 1;
    try {
      if (fileName.toLowerCase().endsWith("pdf")) {
        try (val pdDocument = PDDocument.load(file)) {
          numberOfPages = pdDocument.getNumberOfPages();
        }
      } else if (fileName.toLowerCase().endsWith("html")) {
        numberOfPages = 0;
      }
      return numberOfPages;
    } catch (IOException e) {
      throw new IOException("Error occurred while determining the number of pages in the document",
          e);
    }
  }
	
	public List<Document> getDocumentsUpdateAfterDate(LoggedInInfo loggedInInfo, Date updatedAfterThisDateExclusive, int itemsToReturn) {
		List<Document> results = documentDao.findByUpdateDate(updatedAfterThisDateExclusive, itemsToReturn);

		LogAction.addLogSynchronous(loggedInInfo, "DocumentManager.getUpdateAfterDate", "updatedAfterThisDateExclusive=" + updatedAfterThisDateExclusive);

		return (results);
	}
	
	public List<Document> getDocumentsByDemographicIdUpdateAfterDate(LoggedInInfo loggedInInfo, Integer demographicId, Date updatedAfterThisDateExclusive) {
		List<Document> results = new ArrayList<Document>();
        //If the consent type does not exist in the table assume this consent type is not being managed by the clinic, otherwise ensure patient has consented
        boolean hasConsent = patientConsentManager.hasProviderSpecificConsent(loggedInInfo) || patientConsentManager.getConsentType(ConsentType.PROVIDER_CONSENT_FILTER) == null;
        if (hasConsent) {
			results = documentDao.findByDemographicUpdateAfterDate(demographicId, updatedAfterThisDateExclusive);
			LogAction.addLogSynchronous(loggedInInfo, "DocumentManager.getDocumentsByDemographicIdUpdateAfterDate", "demographicId="+demographicId+" updatedAfterThisDateExclusive="+updatedAfterThisDateExclusive);
		}
		return (results);
	}

	public List<Document> getDocumentsByProgramProviderDemographicDate(LoggedInInfo loggedInInfo, Integer programId, String providerNo, Integer demographicId, Calendar updatedAfterThisDateExclusive, int itemsToReturn) {
		List<Document> results = documentDao.findByProgramProviderDemographicUpdateDate(programId, providerNo, demographicId, updatedAfterThisDateExclusive.getTime(), itemsToReturn);

		LogAction.addLogSynchronous(loggedInInfo, "DocumentManager.getDocumentsByProgramProviderDemographicDate", "programId=" + programId+", providerNo="+providerNo+", demographicId="+demographicId+", updatedAfterThisDateExclusive="+updatedAfterThisDateExclusive.getTime());

		return (results);
	}

	public List<String> getProvidersThatHaveAcknowledgedDocument(LoggedInInfo loggedInInfo, Integer documentId) {
		List<ProviderInboxItem> inboxList = providerInboxRoutingDao.getProvidersWithRoutingForDocument("DOC", documentId);
		List<String> providerList = new ArrayList<String>();
		for(ProviderInboxItem item: inboxList) {
			if(ProviderInboxItem.ACK.equals(item.getStatus())){ 
				//If this has been acknowledge add the provider_no to the list.
				providerList.add(item.getProviderNo());
			}
		}
		return providerList;
	}
	
	private void saveDocument(Document document, Integer demographicNo, String providerNo) {
		// Saves the document
		documentDao.persist(document);

		// Check that the demographic number is a valid number for a demographic, sets it to -1 if it isn't
		if (demographicNo == null || demographicNo < 1) {
			demographicNo = -1;
		}
		// Creates and saves the CtlDocument record
		CtlDocumentPK ctlDocumentPK = new CtlDocumentPK("demographic", demographicNo, document.getId());
		CtlDocument ctlDocument = new CtlDocument();
		ctlDocument.setId(ctlDocumentPK);
		ctlDocument.setStatus(String.valueOf(document.getStatus()));
		ctlDocumentDao.persist(ctlDocument);

		// Saves the patient and provider lab routings if the provided numbers are valid
		if (demographicNo > 0) {
			PatientLabRouting patientLabRouting = new PatientLabRouting(document.getId(), "DOC", demographicNo);
			patientLabRoutingDao.persist(patientLabRouting);
		}

		if (StringUtils.isNotEmpty(providerNo)) {
			new ProviderLabRouting().route(document.getId(), providerNo, "DOC");
		}
	}
}
