/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.managers;

import javax.crypto.SecretKey;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.oscarehr.common.dao.SMTPConfigDao;
import org.oscarehr.common.exception.ServiceException;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.EmailAttachment;
import org.oscarehr.common.model.EmailRequest;
import org.oscarehr.common.model.SMTPConfig;
import org.oscarehr.util.EmailUtil;
import org.oscarehr.util.EncryptionUtils;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.ws.rest.conversion.EmailConfigConverter;
import org.oscarehr.ws.rest.to.model.EmailSpecificationTo1.EmailType;
import org.oscarehr.ws.rest.to.model.SMTPConfigTo1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import oscar.OscarProperties;
import oscar.log.LogAction;

@Service
public class EmailManager {
  protected Logger logger = MiscUtils.getLogger();

  private static final String PASSWORD_KEY = "a9998s7h7p886i2r2r556e";
  private static final char SPLIT = 'i';
  private static final char JOIN = '6';

  @Autowired
  SMTPConfigDao smtpConfigDao;

  @Autowired
  private EmailUtil emailUtil;

  public SMTPConfig getEmailConfig(LoggedInInfo loggedInInfo) {
    SMTPConfig smtpConfig = smtpConfigDao.getEmailConfig();
    return smtpConfig;
  }

  public SMTPConfigTo1 saveEmailConfig(LoggedInInfo loggedInInfo, SMTPConfigTo1 smtpConfigTo1) {
    EmailConfigConverter converter = new EmailConfigConverter();
    // convert into a database savable object
    SMTPConfig smtpConfig = converter.getAsDomainObject(loggedInInfo, smtpConfigTo1);
    SMTPConfig smtpConfigDB = null;
    if (smtpConfigTo1.getId() != null) {
      smtpConfigDB = smtpConfigDao.find(smtpConfigTo1.getId());
    }
    if (smtpConfig.getId() == null) { // if id is null then password must be new
      smtpConfig.setPassword(encryptPassword(smtpConfig.getPassword()));
      smtpConfigDao.persist(smtpConfig);
    } else { // if id is not null then password was previously existing
      if (smtpConfig.getPassword() == null || smtpConfig.getPassword().length == 0) {
        smtpConfig.setPassword(smtpConfigDB.getPassword());
      } else {
        smtpConfig.setPassword(encryptPassword(smtpConfig.getPassword()));
      }
      smtpConfigDao.merge(smtpConfig); // update SMTP config with encrypted password
    }
    smtpConfigTo1 = converter.getAsTransferObject(loggedInInfo, smtpConfig);

    LogAction.addLogSynchronous(loggedInInfo, "AdminManager.saveEmailConfig", "SMTP Configuration", "id=" + smtpConfig.getId());

    return smtpConfigTo1;

  }

  public boolean testEmailConfig(LoggedInInfo loggedInInfo, SMTPConfigTo1 smtpConfigTo1) {
    return emailUtil.sendTestEmail(smtpConfigTo1);
  }

  public boolean sendLabRequisitionEmail(final Demographic demographic, final String orderId) {
    boolean result = true;
    String subject = "Lab Requisition: " + orderId;
    String htmlBody = wrapBodyWithValidHtml(getRequisitionEmailContent(demographic.getFirstName(), demographic.getLastName(), orderId));
    List<EmailAttachment> attachments = new ArrayList<EmailAttachment>();
    EmailRequest emailReq = new EmailRequest(demographic, subject, htmlBody, attachments, EmailType.LAB_REQUISITION);

    try {
      emailUtil.processSendEmailRequest(emailReq);
    } catch (ServiceException e) {
      logger.error("Unexpected error.", e);
      result = false;
    }

    return result;
  }

  public String getRequisitionEmailContent(String firstName, String lastName, String orderId) {
    String body = buildRequisitionEmail(firstName, lastName, orderId);
    String signature = buildRequisitionEmailSignature();
    return "<div>" + body + "</div><br/><div>" + signature + "</div>";
  }

  public byte[] encryptPassword(byte[] password) {
    try {
      return EncryptionUtils.encrypt(getSecretKey(), password);
    } catch (Exception e) {
      logger.error("Error encrypt password", e);
    }
    return null;
  }

  public SecretKey getSecretKey() {
    return EncryptionUtils.generateEncryptionKey(getKeyPass(PASSWORD_KEY, SPLIT, JOIN));
  }

  // -------------------------------------------------------------------- Private Methods

  private static String getKeyPass(String str, char split, char join) {
    String[] tokens = StringUtils.split(str, split);
    Arrays.sort(tokens);
    return StringUtils.join(tokens, join);
  }

  /**
   * Load requisition email template.
   *
   * @return template as string or null if it doesn't exist
   */
  private String loadRequisitionEmailTemplate() {
    StringWriter writer = new StringWriter();
    try {
      File file = new File(OscarProperties.getInstance().getProperty("REQUISITION_EMAIL_TEMPLATE"));
      InputStream inputStream = new FileInputStream(file);
      IOUtils.copy(inputStream, writer, "UTF-8");
      inputStream.close();
    } catch (Exception ex) {
      logger.info("File dose not exist. " + ex.getMessage());
    }
    String template = writer.toString();

    return ((template.length() > 0) ? template : null);
  }

  /**
   * Build and return the lab requisition email
   *
   * @param demographic the Demographic object
   * @param orderId     the eorder id
   * @return lab requisition email html snippet
   */
  private String buildRequisitionEmail(String firstName, String lastName, String orderId) {
    String emailTemplate = loadRequisitionEmailTemplate();
    if (emailTemplate == null) {
      ResourceBundle oscarResource = ResourceBundle.getBundle("oscarResources", Locale.getDefault());
      emailTemplate = oscarResource.getString("eform.email.requisition.template");
    }
    emailTemplate = emailTemplate.replace("{0}", (firstName != null) ? firstName : "");
    emailTemplate = emailTemplate.replace("{1}", (lastName != null) ? lastName : "");
    emailTemplate = emailTemplate.replace("{2}", (orderId != null) ? orderId : "");

    return emailTemplate;
  }

  private String buildRequisitionEmailSignature() {
    StringBuilder sb = new StringBuilder();
    SMTPConfig smtpConfig = smtpConfigDao.getEmailConfig();
    if (smtpConfig != null) {
      String signature = smtpConfig.getSignature();
      if (signature != null && signature.trim().length() > 0) {
        try {
          BufferedReader bf = new BufferedReader(new StringReader(signature));
          String line = bf.readLine();
          while (line != null) {
            sb.append("<div>").append(line).append("</div>");
            line = bf.readLine();
          }
        } catch (IOException e) {
          logger.error("Error reading signature line ", e);
        }
      }
    }
    return sb.toString();
  }

  /**
   * Returns a valid html wrapping a body segment
   *
   * @param body the body email segment
   * @return a valid html wrapping body
   */
  private String wrapBodyWithValidHtml(String body) {
    StringBuilder sb = new StringBuilder();
    sb.append("<!DOCTYPE html><html lang=\"en\" xml:lang=\"en\" xmlns=\"http://www.w3.org/1999/xhtml\">");

    sb.append("<head><title>OEI Email</title><style>");
    sb.append("</style></head>");

    sb.append("<body><div>");
    sb.append(body);
    sb.append("</div></body></html>");

    return sb.toString();
  }
}