/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.managers;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.oscarehr.app.FhirSubscriptionFilter;
import org.oscarehr.common.dao.FhirCheckSubscriptionRequestDao;
import org.oscarehr.common.model.FhirCheckSubscriptionRequest;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.rest.RestApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import oscar.OscarProperties;

@Service
public class FhirSubscriptionManager {

  private final FhirCheckSubscriptionRequestDao fhirCheckSubscriptionRequestDao;

  private final Logger logger = MiscUtils.getLogger();

  private final String CHECK_SUBSCRIPTION_URL = "/api/checksubscription";

  private RestApiClient restApiClient = new RestApiClient();

  @Autowired
  public FhirSubscriptionManager(FhirCheckSubscriptionRequestDao fhirCheckSubscriptionRequestDao) {
    this.fhirCheckSubscriptionRequestDao = fhirCheckSubscriptionRequestDao;
  }

  // handle new fhir compatible subscriptions
  public void createCheckSubRequest(String resourceTypeName, String resourceId) {
    logger.info("createCheckSubRequest invoked: resourceTypeName = " + resourceTypeName
            + " , resourceId = " + resourceId);

    // ask PRO to do the actual checking by putting relevant info into a db
    // table. A table is used so that it is guaranteed that when the data
    // triggering the check is committed, this table will also be committed at
    // the same time to avoid condition when PRO starts to process the info. To
    // avoid having PRO to poll the table, a RESTful call is made to ping PRO to
    // do the processing.
    FhirCheckSubscriptionRequest fhirCheckSubscriptionRequest = new FhirCheckSubscriptionRequest();
    fhirCheckSubscriptionRequest.setResourceTypeName(resourceTypeName);
    fhirCheckSubscriptionRequest.setResourceId(new Integer(resourceId));
    fhirCheckSubscriptionRequest.setCreatedDate(new Date());

    fhirCheckSubscriptionRequestDao.persist(fhirCheckSubscriptionRequest);

    logger.info("createCheckSubRequest: done persisting data");

    return;
  }

  public void pingProToCheckSub() {
    // invoke RESTful call to ping Subscription Manager in PRO to do the
    // work after the createCheckSubRequest has been called. in case the RESTful
    // call is lost due to network problems, the request is assumed to have been
    // persisted in the table and will be processed when PRO starts processing
    // requests in that table. Since we don't really care about the result of
    // this RESTful call, nothing needs to be returned here.

    Boolean success = processCheckSubscriptionRequest();
    logger.info("processCheckSubscriptionRequest status: " + success);
    return;
  }

  // ---------------------------------------------------------- Private Methods

  /**
   * Process check subscription request
   *
   */
  private Boolean processCheckSubscriptionRequest() {
    try {

      HttpServletRequest request = FhirSubscriptionFilter.OSCAR_REQUEST_URL.get();
      logger.debug("picked up request from thread local: " + request);

      String url = OscarProperties.getOscarProApiUrl() + CHECK_SUBSCRIPTION_URL;
      logger.info("url: " + url);
      String emptyRequestBody = "";
      String response = restApiClient.postRequest(url, emptyRequestBody, String.class, request);
      logger.debug("entity=" + response);
      return true;

    } catch (Exception e) {
      logger.error("Failed to process subscription.", e);
      return false;
    }
  }

}
