/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.managers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.oscarehr.common.dao.MeasurementDao;
import org.oscarehr.common.model.Measurement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IntakeFormManager {

  @Autowired
  private MeasurementDao measurementDao;
  
  public enum FormField {
    // Recreational Drug Fields
    MARIJUANA_USE("marijuana_use", "Marijuana Use:", "MJUS"),
    VAPE_USE("vape_use", "E-Cigarette/Vape Use:", "VAUS"),
    OTHER_RECREATIONAL_DRUG("other_recreational_drug", "Other Recreational Drug Use:", "RDUS"),
    // Trinity Health Fields
    GLUCOSE_MONITOR("glucose_monitor", "Glucose Monitor Test", "GLMT"),
    LEFT_EYE_CHECK_UP("eye_check_up_left", "Left Eye Check Up Measurement", "LECM"),
    PREGNANCY_TEST("pregnancy_test", "Pregnancy Test", "PRGT"),
    RIGHT_EYE_CHECK_UP("eye_check_up_right", "Right Eye Check Up Measurement", "RECM"),
    STREP_TEST("strep_test", "STREP Test", "STREP"),
    URINE_DIP("urine_dip", "Urine Dip Test", "URDT");

    private String measurementName;
    private String description;
    private String measurementType;

    private static final Map<String, FormField> recreationalDrugFields = new HashMap<String, FormField>();
    private static final Map<String, FormField> trinityHealthFields = new HashMap<String, FormField>();
    private static final Map<String, FormField> allFields = new HashMap<String, FormField>();

    static {
      recreationalDrugFields.put(MARIJUANA_USE.getMeasurementName(), MARIJUANA_USE);
      recreationalDrugFields.put(VAPE_USE.getMeasurementName(), VAPE_USE);
      recreationalDrugFields.put(OTHER_RECREATIONAL_DRUG.getMeasurementName(), OTHER_RECREATIONAL_DRUG);

      trinityHealthFields.put(GLUCOSE_MONITOR.getMeasurementName(), GLUCOSE_MONITOR);
      trinityHealthFields.put(LEFT_EYE_CHECK_UP.getMeasurementName(), LEFT_EYE_CHECK_UP);
      trinityHealthFields.put(PREGNANCY_TEST.getMeasurementName(), PREGNANCY_TEST);
      trinityHealthFields.put(RIGHT_EYE_CHECK_UP.getMeasurementName(), RIGHT_EYE_CHECK_UP);
      trinityHealthFields.put(STREP_TEST.getMeasurementName(), STREP_TEST);
      trinityHealthFields.put(URINE_DIP.getMeasurementName(), URINE_DIP);

      allFields.putAll(recreationalDrugFields);
      allFields.putAll(trinityHealthFields);
    }

    FormField(String measurementName, String description, String measurementType) {
      this.measurementName = measurementName;
      this.description = description;
      this.measurementType = measurementType;
    }
    
    public String getMeasurementName() {
      return measurementName;
    }

    public String getDescription() {
      return description;
    }
    
    public String getMeasurementType() {
      return measurementType;
    }
    
    public static List<FormField> getRecreationalDrugFields() {
      return new ArrayList<FormField>(recreationalDrugFields.values());
    }
    
    public static List<FormField> getTrinityHealthFields() {
      return new ArrayList<FormField>(trinityHealthFields.values());
    }

    public static FormField getFieldByName(String name) {
      return allFields.get(name);
    }
  }

  public String saveIntakeFieldsAndBuildNote(HttpServletRequest request, String demographicNo, 
      String loggedInProviderNo, String currentNote, String currentComments, 
      List<FormField> fieldsToSave) {

    Collections.sort(fieldsToSave, new Comparator<FormField>() {
      public int compare(FormField f1, FormField f2) {
        return f1.getMeasurementName().compareTo(f2.getMeasurementName());
      }
    });

    for (IntakeFormManager.FormField field : fieldsToSave) {
      String fieldName = field.getMeasurementName();
      String fieldValue = request.getParameter(fieldName + "_value");
      String fieldComment = request.getParameter(fieldName + "_comment");
      FormField formField = FormField.getFieldByName(fieldName);
      int appointmentNo = (request.getParameter("appointmentNo") != null) ? Integer.parseInt(request.getParameter("appointmentNo")) : 0 ;
      if (formField != null && StringUtils.isNotEmpty(fieldValue)) {
        Measurement m = measurementDao.saveMeasurement(demographicNo, loggedInProviderNo,
            formField.getMeasurementType(), fieldValue, fieldComment, appointmentNo);

        // Add saved measurement description and value to echart note and comment
        currentNote += " \n" + field.getDescription() + " " + fieldValue;
        currentComments += (StringUtils.isBlank(fieldComment) ? "" : m.getComments() + "\n");
      }
    }
    
    return currentNote + "\n" + currentComments;
  }
  
  public Map<String, Measurement> getRecreationalDrugFields(String demographicNo) {
    Map<String, Measurement> recreationalDrugFieldsMap = new HashMap<String, Measurement>();
    for (IntakeFormManager.FormField recreationalDrugField : FormField.getRecreationalDrugFields()) {
      Measurement measurement = 
          measurementDao.findNewestByTypeAndDemographicNo(Integer.parseInt(demographicNo),
          recreationalDrugField.getMeasurementType());
      if (measurement != null) {
        recreationalDrugFieldsMap.put(measurement.getType(), measurement);
      }
    }
    return recreationalDrugFieldsMap;
  }

  public Map<String, Measurement> getTrinityHealthIntakeFields(String demographicNo) {
    Map<String, Measurement> recreationalDrugFieldsMap = new HashMap<String, Measurement>();
    for (IntakeFormManager.FormField recreationalDrugField : FormField.getTrinityHealthFields()) {
      Measurement measurement =
          measurementDao.findNewestByTypeAndDemographicNo(Integer.parseInt(demographicNo),
              recreationalDrugField.getMeasurementType());
      if (measurement != null) {
        recreationalDrugFieldsMap.put(measurement.getType(), measurement);
      }
    }
    return recreationalDrugFieldsMap;
  }
}
