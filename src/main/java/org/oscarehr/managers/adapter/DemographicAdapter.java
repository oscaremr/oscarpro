/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.managers.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.dao.DemographicArchiveDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DemographicExtArchiveDao;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.DemographicExtArchive;
import org.oscarehr.common.model.DemographicExtKey;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.event.DemographicCreateEvent;
import org.oscarehr.event.DemographicUpdateEvent;
import org.oscarehr.integration.hl7.generators.HL7A04Generator;
import org.oscarehr.managers.FhirSubscriptionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.stereotype.Service;
import oscar.OscarProperties;

@Service
@Slf4j
public class DemographicAdapter {

  private static HibernateTemplate demographicDaoTemplate;
  private static OscarProperties oscarProperties;
  private static Map<String, Boolean> masterFilePreferences;

  /*
   * These DAOs are not meant to be called from this service for creating/updating/deleting.
   * These DAOs are only to populate the templates and entity manager.
   * Calling the DAOs will cause infinite loops.
   */
  private static DemographicDao demographicDao;
  private static DemographicArchiveDao demographicArchiveDao;
  private static DemographicExtDao demographicExtDao;
  private static DemographicExtArchiveDao demographicExtArchiveDao;
  private static SystemPreferencesDao systemPreferencesDao;
  private static ApplicationEventPublisher applicationEventPublisher;
  private static FhirSubscriptionManager fhirSubscriptionManager;

  @Autowired private DemographicDao tDemographicDao;
  @Autowired private DemographicArchiveDao tDemographicArchiveDao;
  @Autowired private DemographicExtDao tDemographicExtDao;
  @Autowired private DemographicExtArchiveDao tDemographicExtArchiveDao;
  @Autowired private SystemPreferencesDao tSystemPreferencesDao;
  @Autowired private ApplicationEventPublisher tApplicationEventPublisher;
  @Autowired private FhirSubscriptionManager tFhirSubscriptionManager;

  private static final String PATIENT_RESOURCE = "Patient";
  private static final String HIN = "hin";
  private static final String VER = "ver";
  private static final String VER_DEPENDENT_66 = "66";

  @PostConstruct
  private void initialize() {
    DemographicAdapter.demographicDaoTemplate = tDemographicDao.getHibernateTemplate();
    DemographicAdapter.demographicDao = tDemographicDao;
    DemographicAdapter.demographicArchiveDao = tDemographicArchiveDao;
    DemographicAdapter.demographicExtDao = tDemographicExtDao;
    DemographicAdapter.demographicExtArchiveDao = tDemographicExtArchiveDao;
    DemographicAdapter.applicationEventPublisher = tApplicationEventPublisher;
    DemographicAdapter.fhirSubscriptionManager = tFhirSubscriptionManager;
    DemographicAdapter.systemPreferencesDao = tSystemPreferencesDao;

    DemographicAdapter.oscarProperties = OscarProperties.getInstance();
    DemographicAdapter.masterFilePreferences
        = systemPreferencesDao.findByKeysAsMap(SystemPreferences.MASTER_FILE_PREFERENCE_KEYS);
  }

  /* DEMOGRAPHIC FUNCTIONALITY */

  /**
   * Archive: Archives the Demographic and DemographicExts for the given demographic.
   *
   * @param demographic the new demographic to archive in the database.
   */
  protected static void archiveDemographic(final Demographic demographic) {
    val archiveId = demographicArchiveDao.archiveRecord(demographic).getId().intValue();
    val extensions = demographicExtDao.getDemographicExtByDemographicNo(
        demographic.getDemographicNo()
    );
    archiveExtensions(archiveId, extensions);
  }

  /**
   * Create & Archive: Creates a new demographic record in the system with the corresponding
   * demographic extensions in the Demographic and DemographicExt tables and automatically archives
   * the data in the DemographicArchive and DemographicExtArchive tables.
   *
   * @param demographic the new demographic to save in the database.
   * @param extensions  the demographic extensions to be saved in the database.
   */
  public static void createDemographic(
      final Demographic demographic,
      final List<DemographicExt> extensions
  ) {
    createDemographic(demographic, extensions, false, false);
  }

  /**
   * Create & Optional Archive: Some use cases require a demographic to be persisted for the
   * demographicNo ID to be generated before using the newly generated ID to populate
   * DemographicExts. In this case we are actively choosing not to archive the data because the
   * Demographic and DemographicExts will be persisted once the demographicNo is populated.
   * <p>
   * Creates a new demographic record in the system with the corresponding demographic extensions in
   * the Demographic and DemographicExt tables and automatically archives the data in the
   * DemographicArchive and DemographicExtArchive tables.
   *
   * @param demographic   the new demographic to save in the database.
   * @param extensions    the demographic extensions to be saved in the database.
   * @param skipArchiving skip archiving process
   */
  public static void createDemographic(
      final Demographic demographic,
      final List<DemographicExt> extensions,
      final boolean skipArchiving,
      final boolean skipFhirSubscription
  ) {
    saveOrUpdateDemographic(demographic);
    for (DemographicExt extension : extensions) {
      extension.setDemographicNo(demographic.getDemographicNo());
      demographicExtDao.saveDemographicExt(extension);
    }
    if (!skipArchiving) {
      archiveDemographic(demographic, extensions);
    }
    if (!skipFhirSubscription) {
      processFhirSubscription(demographic);
    }
  }

  /**
   * Create & Archive: Creates a new demographic record in the system without any demographic
   * extensions in the Demographic table and automatically archives the data in the
   * DemographicArchive and the default values in the DemographicExtArchive tables.
   *
   * @param demographic the new demographic to save in the database.
   */
  public static void createDemographic(final Demographic demographic) {
    createDemographic(demographic, false, false);
  }

  /**
   * Create & Optional Archive: Some use cases require a demographic to be persisted for the
   * demographicNo ID to be generated before using the newly generated ID to populate
   * DemographicExts. In this case we are actively choosing not to archive the data because the
   * Demographic and DemographicExts will be persisted once the demographicNo is populated.
   * <p>
   * Creates a new demographic record in the system with the corresponding demographic extensions in
   * the Demographic and DemographicExt tables and automatically archives the data in the
   * DemographicArchive and DemographicExtArchive tables.
   *
   * @param demographic   the new demographic to save in the database.
   * @param skipArchiving skip archiving process
   */
  public static void createDemographic(
      final Demographic demographic,
      final boolean skipArchiving,
      final boolean skipFhirSubscription
  ) {
    saveOrUpdateDemographic(demographic);;
    if (!skipArchiving) {
      archiveDemographic(demographic);
    }
    if (!skipFhirSubscription) {
      processFhirSubscription(demographic);
    }
  }

  /**
   * Delete & Archive: Deletes the given Demographic and corresponding DemographicExt values, and
   * archives the final copy.
   * <p>
   * Demographic value is not actually deleted, just set to 'IN'.
   *
   * @param demographic the new demographic to save in the database.
   * @param user        the provider number for the user deleting the demographic.
   */
  public static void deleteDemographic(final Demographic demographic, final String user) {
    demographicArchiveDao.archiveRecord(demographic);
    demographic.setPatientStatus(Demographic.PatientStatus.IN.name());
    demographic.setLastUpdateUser(user);
    archiveDemographic(demographic);
    deleteExtensions(demographic);
  }

  /**
   * Save & Archive: Gets, saves and archives the Demographic and DemographicExts for the given
   * demographic number.
   *
   * @param demographicNumber the demographic number of the demographic to save in the database.
   */
  public static void saveDemographic(final Integer demographicNumber) {
    val demographic = demographicDao.getDemographic(demographicNumber);
    saveDemographic(demographic, false);
  }

  /**
   * Save & Archive: Saves and archives the Demographic and DemographicExts for the given
   * demographic.
   *
   * @param demographic the new demographic to save in the database.
   */
  public static void saveDemographic(final Demographic demographic) {
    saveDemographic(demographic, false);
  }

  /**
   * Save & Archive: Saves and archives the Demographic and DemographicExts for the given
   * demographic.
   *
   * @param demographic   the new demographic to save in the database.
   * @param skipArchiving skip archiving process
   */
  public static void saveDemographic(final Demographic demographic, boolean skipArchiving) {
    saveOrUpdateDemographic(demographic);
    if (!skipArchiving) {
      archiveDemographic(demographic);
    }
    processFhirSubscription(demographic);
  }

  /**
   * Save & Archive: Saves and archives the Demographic and DemographicExts for the given
   * demographic.
   *
   * @param demographic the new demographic to save in the database.
   * @param extensions  the demographic extensions to be saved in the database.
   */
  public static void saveDemographic(
      final Demographic demographic,
      final Demographic oldVersion,
      final List<DemographicExt> extensions,
      final boolean skipArchiving
  ) {
    saveOrUpdateDemographic(demographic);
    if (!skipArchiving) {
      archiveDemographic(oldVersion);
    }
    demographicExtDao.saveDemographicExts(extensions, true);
    processFhirSubscription(demographic);
  }

  public static void saveDemographic(
      final Demographic demographic,
      final List<DemographicExt> extensions
  ) {
    saveDemographic(demographic, new Demographic(demographic), extensions, false);
  }

  /* DEMOGRAPHIC EXTENSION ADAPTER FUNCTIONALITY */

  /**
   * Archive: Archives a full copy of the demographic along with this change.
   *
   * @param ext the new demographic to archive in the database.
   */
  public static void archiveDemographicExt(final DemographicExt ext) {
    saveDemographic(ext.getDemographicNo());
  }

  /**
   * Create a new demographic and update any extensions with their default values.
   *
   * @param request               the HttpServletRequest from add/update a demographic record.
   * @param providerNumber        the user taking the action
   * @param demographicNumber     the demographic number the data belongs to
   * @return a list of DemographicExts based on the data in the request.
   */
  public static List<DemographicExt> createDemographicExtensions(
      final HttpServletRequest request,
      final String providerNumber,
      final Integer demographicNumber
  ) {
    return processDemographicExtensions(request, providerNumber, demographicNumber, true);
  }

  /**
   * Process WITHOUT Save: Takes an input HttpServletRequest and creates DemographicExt objects with
   * it for the given demographic information.
   *
   * @param request               the HttpServletRequest from add/update a demographic record.
   * @param providerNumber        the user taking the action
   * @param demographicNumber     the demographic number the data belongs to
   * @return a list of DemographicExts based on the data in the request.
   */
  public static List<DemographicExt> processDemographicExtensions(
      final HttpServletRequest request,
      final String providerNumber,
      final Integer demographicNumber
  ) {
    return processDemographicExtensions(request, providerNumber, demographicNumber, false);
  }

  /**
   * Returns the HIN from the request if it is unique otherwise ""
   *
   * @param request the HttpServletRequest from add/update a demographic record.
   */
  public static String processUniqueDemographicHin(HttpServletRequest request, int demographicNumber) {
    val requestHin = StringUtils.trimToEmpty(request.getParameter(HIN));
    if (requestHin.isEmpty()) {
      return "";
    }
    val demographicsWithSameHinAndVer66 = demographicDao.searchDemographicByHinAndVer(requestHin, VER_DEPENDENT_66);
    val ver = StringUtils.trimToEmpty(request.getParameter(VER));
    if (VER_DEPENDENT_66.equals(ver) && demographicsWithSameHinAndVer66.size() > 0) {
      return requestHin;
    }
    val demographicsWithSameHin = demographicDao.searchDemographicByHinAndNotVer(requestHin, VER_DEPENDENT_66);
    if (demographicsWithSameHin.isEmpty() || (demographicsWithSameHin.size() == 1
        && (demographicsWithSameHin.get(0).getDemographicNo().equals(demographicNumber)))) {
      return requestHin;
    }
    return "";
  }

  /**
   * Returns a String representing demographicExtKey value if non-null, otherwise
   * returns an empty String.
   *
   * @param demographicNumber demographic number to retrieve extension by
   * @param demographicExtKey demographic extension key relating to physician (rowId, name , OHIP)
   * @return value of the demographic extension for the corresponding key
   */
  public static String getDemographicExtensionValueAsString(
      final @Nullable Integer demographicNumber,
      final DemographicExtKey demographicExtKey
  ) {
    if (demographicNumber == null) {
      return "";
    }
    val extension = demographicExtDao.getDemographicExt(demographicNumber, demographicExtKey);
    if (extension != null) {
      return extension.getValue();
    }
    return "";
  }

  /**
   * Returns a String representing the referral/family physician demographicExtKey value if non-null for
   * demographicNumber of type String, otherwise returns an empty String.
   * @see #getDemographicExtensionValueAsString(Integer, DemographicExtKey)
   *
   * @param demographicNumber demographic number to retrieve extension by
   * @param demographicExtKey demographic extension key relating to physician (rowId, name , OHIP)
   * @return value of the demographic extension for the corresponding key
   */
  public static String getDemographicExtensionValueAsString(
      final @Nullable String demographicNumber,
      final DemographicExtKey demographicExtKey
  ) {
    try {
      val extension = demographicExtDao
          .getDemographicExt(Integer.parseInt(demographicNumber), demographicExtKey);
      if (extension != null) {
        return extension.getValue();
      }
    } catch (NumberFormatException e) {
      e.printStackTrace();
    }
    return StringUtils.EMPTY;
  }

  /**
   * Update an existing DemographicExt for demographic if it exists, otherwise create a new one.
   *
   * @param demographic        the demographic to update/create the given extension value for
   * @param demographicExtKey  the key of the demographic extension that needs to be updated
   * @param value              the new value of the demographic extension
   * @return an updated/new DemographicExt object
   */
  @Nullable
  public static DemographicExt updateOrCreateDemographicExtension(
      final @Nullable Demographic demographic,
      final DemographicExtKey demographicExtKey,
      final String value
  ) {
    if (!hasDemographicNumber(demographic)) {
      return null;
    }
    val extension = demographicExtDao.getDemographicExt(demographic.getDemographicNo(), demographicExtKey.getKey());
    if (extension != null) {
      return updateDemographicExtension(demographic, extension, value);
    } else {
     return createDemographicExtension(demographic, demographicExtKey, value);
    }
  }

  /* PRIVATE DEMOGRAPHIC EXTENSION FUNCTIONS */

  private static DemographicExt updateDemographicExtension(
      final Demographic demographic,
      final DemographicExt extension,
      final String value
  ) {
    extension.setValue(value);
    extension.setDateCreated(new Date());
    extension.setProviderNo(demographic.getLastUpdateUser() == null
        ? StringUtils.EMPTY
        : demographic.getLastUpdateUser());
    demographicExtDao.saveDemographicExt(extension, true);
    return extension;
  }

  private static DemographicExt createDemographicExtension(
      final Demographic demographic,
      final DemographicExtKey demographicExtKey,
      final String value
  ) {
    val newExtension = new DemographicExt(
        demographic.getLastUpdateUser() == null ? StringUtils.EMPTY : demographic.getLastUpdateUser(),
        demographic.getDemographicNo(),
        demographicExtKey.getKey(),
        value
    );
    demographicExtDao.saveDemographicExt(newExtension, true);
    return newExtension;
  }

  private static void archiveDemographic(
      final Demographic demographic,
      final List<DemographicExt> extensions
  ) {
    val archiveId = demographicArchiveDao.archiveRecord(demographic).getId().intValue();
    archiveExtensions(archiveId, extensions);
    deleteDuplicateExtensions(demographic.getDemographicNo());
  }

  private static void archiveExtensions(
      final Integer archiveId,
      final List<DemographicExt> extensions
  ) {
    for (var extension : extensions) {
      val archive = new DemographicExtArchive(extension);
      archive.setArchiveId(archiveId);
      demographicExtArchiveDao.saveEntity(archive);
    }
  }

  public static boolean demographicExists(final Integer demographicNumber) {
    return demographicDaoTemplate.get(Demographic.class, demographicNumber) != null;
  }

  private static List<DemographicExt> collectDuplicateExtensionIds(
      final List<DemographicExt> extensions
  ) {
    val duplicates = new ArrayList<DemographicExt>();
    val keys = new HashSet<String>();
    for (var extension : extensions) {
      if (keys.contains(extension.getKey())) {
        duplicates.add(extension);
      } else {
        keys.add(extension.getKey());
      }
    }
    return duplicates;
  }

  private static void deleteDuplicateExtensions(final Integer demographicNumber) {
    val extensions = demographicExtDao.getDemographicExtByDemographicNo(demographicNumber);
    deleteDuplicateExtensions(extensions);
  }

  private static void deleteDuplicateExtensions(final List<DemographicExt> extensions) {
    Collections.sort(extensions, new Comparator<DemographicExt>() {
      @Override
      public int compare(DemographicExt o1, DemographicExt o2) {
        int c;
        c = o1.getKey().compareTo(o2.getKey());
        if (c == 0) {
          c = o1.getDateCreated().compareTo(o2.getDateCreated());
        }
        return c;
      }
    });
    val duplicatesForDeletion = collectDuplicateExtensionIds(extensions);
    for (var duplicateExtension : duplicatesForDeletion) {
      demographicExtDao.removeDemographicExt(duplicateExtension.getId());
    }
  }

  private static void deleteExtensions(final Demographic demographic) {
    val extensions =
        demographicExtDao.getDemographicExtByDemographicNo(demographic.getDemographicNo());
    for (var extension : extensions) {
      demographicExtDao.removeDemographicExt(extension.getId());
    }
  }

  private static List<DemographicExt> processDemographicExtensions(
      final HttpServletRequest request,
      final String providerNumber,
      final Integer demographicNumber,
      final boolean useDefaultValues
  ) {
    val extensionAdapter = new DemographicExtensionAdapter(
        demographicExtDao,
        oscarProperties,
        masterFilePreferences,
        request,
        providerNumber,
        demographicNumber,
        systemPreferencesDao.isAppointmentRemindersEnabled()
    );
    return extensionAdapter.processDemographicExtensions(useDefaultValues);
  }

  private static void processFhirSubscription(final Demographic demographic) {
    boolean demographicExists
        = demographic.getDemographicNo() != null
        && demographicExists(demographic.getDemographicNo());
    runHl7A04Generator(demographic, demographicExists);
    publishDemographic(demographic, demographicExists);
    queueWithFhirSubscriptionManager(demographic);
  }

  private static void processNewExtensionValue(
      final List<DemographicExt> extensions,
      final Map<String, String> oldExtensions,
      final String providerNumber,
      final  Integer demographicNumber,
      final String key,
      final  String value
  ) {
    if (!StringUtils.trimToEmpty(oldExtensions.get(key)).equals(StringUtils.trimToEmpty(value))) {
      extensions.add(new DemographicExt(providerNumber, demographicNumber, key, value));
    }
  }

  private static void publishDemographic(Demographic demographic, boolean demographicExists) {
    if (!demographicExists) {
      applicationEventPublisher.publishEvent(
          new DemographicCreateEvent(demographic,demographic.getDemographicNo())
      );
    } else {
      applicationEventPublisher.publishEvent(
          new DemographicUpdateEvent(demographic,demographic.getDemographicNo())
      );
    }
  }

  private static void runHl7A04Generator(final Demographic demographic, boolean exists) {
    if (oscarProperties.isHL7A04GenerationEnabled() && !exists) {
      (new HL7A04Generator()).generateHL7A04(demographic);
    }
  }

  private static void saveOrUpdateDemographic(@NonNull final Demographic demographic) {
    demographicDaoTemplate.saveOrUpdate(demographic);
  }

  /**
   * Call the fhirSubscriptionManger to check if there is a Fhir Subscription
   * @param demographic the demographic to check the subscription manager with
   */
  private static void queueWithFhirSubscriptionManager(final Demographic demographic) {
    if (systemPreferencesDao.isFhirSubscriptionEnabled()) {
      fhirSubscriptionManager.createCheckSubRequest(
          PATIENT_RESOURCE,
          demographic.getDemographicNo().toString()
      );
      fhirSubscriptionManager.pingProToCheckSub();
    }
  }

  private static boolean hasDemographicNumber(final @Nullable Demographic demographic) {
    return demographic != null && demographic.getDemographicNo() != null;
  }

  /**
   * for unit testing ONLY.
   */
  @Deprecated
  protected static void assignStaticDaos(
      HibernateTemplate tDemographicDaoTemplate,
      DemographicDao tDemographicDao,
      DemographicArchiveDao tDemographicArchiveDao,
      DemographicExtDao tDemographicExtDao,
      DemographicExtArchiveDao tDemographicExtArchiveDao,
      SystemPreferencesDao tSystemPreferencesDao,
      ApplicationEventPublisher tApplicationEventPublisher,
      FhirSubscriptionManager tFhirSubscriptionManager,
      OscarProperties tOscarProperties
  ) {
    DemographicAdapter.demographicDaoTemplate = tDemographicDaoTemplate;
    DemographicAdapter.demographicDao = tDemographicDao;
    DemographicAdapter.demographicArchiveDao = tDemographicArchiveDao;
    DemographicAdapter.demographicExtDao = tDemographicExtDao;
    DemographicAdapter.demographicExtArchiveDao = tDemographicExtArchiveDao;
    DemographicAdapter.systemPreferencesDao = tSystemPreferencesDao;
    DemographicAdapter.applicationEventPublisher = tApplicationEventPublisher;
    DemographicAdapter.fhirSubscriptionManager = tFhirSubscriptionManager;
    DemographicAdapter.oscarProperties = tOscarProperties;
  }
}
