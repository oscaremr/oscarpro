/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.managers.adapter;

import ca.kai.util.MapUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import org.apache.commons.lang3.EnumUtils;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.DemographicExtKey;
import org.oscarehr.common.model.DemographicExtKey.CustomParameter;
import oscar.OscarProperties;

/**
 * Helper for building DemographicExt objects based on HttpServletRequest input ie. from
 * demographicaddarecord.jsp, demographicupdatearecord.jsp
 */
@Slf4j
public class DemographicExtensionAdapter {

  private static HttpServletRequest enumRequest;
  private static Map<DemographicExtKey, DemographicExtConditionalKey> keyToConditionalKeyMap;
  private static Map<String, Boolean> masterFilePreferences;
  private static OscarProperties oscarProperties;
  private static Set<DemographicExtKey> activeKeys;
  private static Set<DemographicExtKey> conditionalKeys;
  private static boolean appointmentRemindersEnabled;

  private final HttpServletRequest request;
  private final String providerNumber;
  private final Integer demographicNumber;
  private final Map<String, String> oldExtensions;

  DemographicExtensionAdapter(
      final DemographicExtDao demographicExtDao,
      final OscarProperties oscarProperties,
      final Map<String, Boolean> masterFilePreferences,
      final HttpServletRequest request,
      final String providerNumber,
      final Integer demographicNumber,
      final boolean appointmentRemindersEnabled
  ) {
    this.request = request;
    this.providerNumber = providerNumber;
    this.demographicNumber = demographicNumber;
    this.oldExtensions = demographicExtDao.getAllValuesForDemo(demographicNumber);

    DemographicExtensionAdapter.enumRequest = request;
    DemographicExtensionAdapter.oscarProperties = oscarProperties;
    DemographicExtensionAdapter.masterFilePreferences = masterFilePreferences;
    DemographicExtensionAdapter.appointmentRemindersEnabled = appointmentRemindersEnabled;

    val conditionalKeys = new HashSet<DemographicExtKey>();
    val activeKeys = new HashSet<DemographicExtKey>();
    val keyMap = new HashMap<DemographicExtKey, DemographicExtensionAdapter.DemographicExtConditionalKey>();
    for (var conditionalKey : DemographicExtensionAdapter.DemographicExtConditionalKey.values()) {
      val set = conditionalKey.getKeySet();
      conditionalKeys.addAll(set);
      for (var extKey : set) {
        keyMap.put(extKey, conditionalKey);
        if (conditionalKey.isActive()) {
          activeKeys.add(extKey);
        }
      }
    }
    DemographicExtensionAdapter.conditionalKeys = conditionalKeys;
    DemographicExtensionAdapter.activeKeys = activeKeys;
    DemographicExtensionAdapter.keyToConditionalKeyMap = keyMap;
  }

  /**
   * DemographicExtConditionalKeys are DemographicExtKeys that are gated behind system properties or
   * system preferences.
   * <p>
   * Current List customerReferralSource "Other".equals(request.getParameter("referralSource"));
   * <p>
   * former_name:               masterFilePreferences.getOrDefault("display_former_name", false)
   * <p>
   * referralDate:              properties.isPropertyActive("show_referral_date") EmploymentStatus:
   * properties.isPropertyActive("showEmploymentStatus") HasPrimaryCarePhysician:
   * properties.isPropertyActive("showPrimaryCarePhysicianCheck")
   */
  @RequiredArgsConstructor
  enum DemographicExtConditionalKey {
    DISPLAY_FORMER_NAME(
        Collections.singleton(DemographicExtKey.FORMER_NAME),
        MapUtils.getOrDefault(masterFilePreferences, "display_former_name", false)
    ),
    EMPLOYMENT_STATUS(
        Collections.singleton(DemographicExtKey.EMPLOYMENT_STATUS),
        oscarProperties.isPropertyActive("showEmploymentStatus")
    ),
    PRIMARY_CARE_PHYSICIAN_EXISTS(
        Collections.singleton(DemographicExtKey.PRIMARY_CARE_PHYSICIAN_EXISTS),
        oscarProperties.isPropertyActive("showPrimaryCarePhysicianCheck")
    ),
    REFERRAL_DATE(
        Collections.singleton(DemographicExtKey.REFERRAL_DATE),
        oscarProperties.isPropertyActive("show_referral_date")
    ),
    REFERRAL_SOURCE(
        Collections.singleton(DemographicExtKey.REFERRAL_SOURCE),
        !"Other".equals(enumRequest.getParameter("referralSource"))
    ),
    REFERRAL_SOURCE_CUSTOM(
        Collections.singleton(DemographicExtKey.REFERRAL_SOURCE_CUSTOM),
        "Other".equals(enumRequest.getParameter("referralSource"))
    ),
    REMINDERS(
        new HashSet<>(Arrays.asList(
            DemographicExtKey.REMINDER_ALLOW_REMINDERS,
            DemographicExtKey.REMINDER_CELL,
            DemographicExtKey.REMINDER_EMAIL,
            DemographicExtKey.REMINDER_PHONE
        )),
        appointmentRemindersEnabled
    );

    /* @param keySet the set of demographic ext keys that are conditional on a given boolean */
    @Getter private final Set<DemographicExtKey> keySet;
    @Getter private final boolean active;

    /**
     * @param key DemographicExtKey to check if active
     * @return true if active
     */
    public static boolean isActiveKey(final DemographicExtKey key) {
      return DemographicExtensionAdapter.getActiveKeys().contains(key);
    }

    /**
     * @param key DemographicExtKey to check if conditional
     * @return true if conditional
     */
    public static boolean isConditionalKey(final DemographicExtKey key) {
      return EnumUtils.isValidEnum(DemographicExtConditionalKey.class, key.name());
    }

    /**
     * @param key DemographicExtKey to get ConditionalKey for
     * @return the DemographicExtKey representing conditional key of the input
     */
    public static DemographicExtConditionalKey getConditionalKeyForExtKey(
        final DemographicExtKey key
    ) {
      return DemographicExtensionAdapter.getConditionalKeys().contains(key)
          ? DemographicExtensionAdapter.getKeyToConditionalKeyMap().get(key)
          : null;
    }
  }

  public List<DemographicExt> processDemographicExtensions(final boolean useDefaultValue) {
    val extensions = new ArrayList<DemographicExt>();
    for (var key : DemographicExtKey.values()) {
      processDemographicExtKey(extensions, key, useDefaultValue);
    }
    processCustomDemographicExtensions(extensions);
    return extensions;
  }

  private void processDemographicExtKey(
      final List<DemographicExt> extensions,
      final DemographicExtKey key,
      boolean useDefaultValue
  ) {
    if (!DemographicExtConditionalKey.isConditionalKey(key)
        || DemographicExtConditionalKey.isActiveKey(key)) {
      addNewDemographicExt(
          extensions,
          getProviderNumberOrClinic(key, providerNumber),
          demographicNumber,
          key,
          getValueForKeyName(request, key, useDefaultValue),
          oldExtensions.get(key.getKey())
      );
    }
  }

  private void processCustomDemographicExtensions(final List<DemographicExt> extensions) {
    if (oscarProperties.getProperty("demographicExt") == null) {
      return;
    }
    String[] customExtKeys =
        oscarProperties.getProperty("demographicExt", "").split("\\|");

    for (var customExtKey : customExtKeys) {
      customExtKey = customExtKey.replace(' ', '_');
      processNewExtensionValueFromCustomExtKey(
          extensions,
          providerNumber,
          demographicNumber,
          customExtKey,
          getValueForKeyName(request, customExtKey),
          oldExtensions.get(customExtKey)
      );
    }
  }

  private String getProviderNumberOrClinic(final DemographicExtKey key, final String providerNo) {
    // Reminders require providerNo == "CLINIC"
    val conditional = DemographicExtConditionalKey.getConditionalKeyForExtKey(key);
    if (conditional == DemographicExtConditionalKey.REMINDERS) {
      return "CLINIC";
    }
    return providerNo;
  }

  private static void addNewDemographicExt(
      final List<DemographicExt> extensions,
      final String providerNumber,
      final Integer demographicNumber,
      final DemographicExtKey key,
      final String newValue,
      final String oldValue
  ) {
    if (!areValuesEqual(oldValue, newValue)) {
      extensions.add(
          new DemographicExt(providerNumber, demographicNumber, key.getKey(), newValue)
      );
    }
  }

  private static void processNewExtensionValueFromCustomExtKey(
      final List<DemographicExt> extensions,
      final String providerNumber,
      final Integer demographicNumber,
      final String key,
      final String value,
      final String oldValue
  ) {
    if (!org.apache.commons.lang.StringUtils.trimToEmpty(oldValue).equals(
        org.apache.commons.lang.StringUtils.trimToEmpty(value))) {
      extensions.add(new DemographicExt(providerNumber, demographicNumber, key, value));
    }
  }

  private static boolean areValuesEqual(final String oldValue, final String newValue) {
    return org.apache.commons.lang.StringUtils.trimToEmpty(oldValue).equals(
        org.apache.commons.lang.StringUtils.trimToEmpty(newValue));
  }

  private static String getValueForKeyName(
      final HttpServletRequest request,
      final DemographicExtKey key,
      boolean useDefaultValue
  ) {
    if (key.getCustomParameter() != CustomParameter.NONE) {
      return processCustomParameters(request, key, useDefaultValue);
    } else if (request.getParameter(key.getRequestParameter()) != null) {
      return processXml(key, request.getParameter(key.getRequestParameter()));
    }
    return processXml(key, request.getParameter(key.getKey()));
  }

  private static String getValueForKeyName(
      final HttpServletRequest request,
      final String key
  ) {
    return request.getParameter(key);
  }

  private static String processCustomParameters(
      final HttpServletRequest request,
      final DemographicExtKey key,
      boolean useDefaultValue
  ) {
    if (key.getCustomParameter().getCondition()
        .equals(request.getParameter(key.getKey()))
        || key.getCustomParameter().getCondition()
        .equals(request.getParameter(key.getRequestParameter()))) {
      return "true";
    } else if (key.getDefaultValue() != null && useDefaultValue) {
      return key.getDefaultValue();
    }
    return "false";
  }

  private static String processXml(
      final DemographicExtKey key,
      final String value
  ) {
    return key.hasXml()
        ? String.format("<%s>%s</%s>",
        key.getXmlTag(),
        value,
        key.getXmlTag()
    )
        : value;
  }

  /* ************************************** INITIALIZATION ************************************** */

  private static Set<DemographicExtKey> getActiveKeys() {
    if (DemographicExtensionAdapter.activeKeys == null) {
      initializeConditionalKeys();
    }
    return DemographicExtensionAdapter.conditionalKeys;
  }

  private static Set<DemographicExtKey> getConditionalKeys() {
    if (DemographicExtensionAdapter.conditionalKeys == null) {
      initializeConditionalKeys();
    }
    return DemographicExtensionAdapter.conditionalKeys;
  }

  private static Map<DemographicExtKey, DemographicExtConditionalKey> getKeyToConditionalKeyMap() {
    if (DemographicExtensionAdapter.keyToConditionalKeyMap == null) {
      initializeConditionalKeys();
    }
    return DemographicExtensionAdapter.keyToConditionalKeyMap;
  }

  private static void initializeConditionalKeys() {
    val conditionalKeys = new HashSet<DemographicExtKey>();
    val activeKeys = new HashSet<DemographicExtKey>();
    val keyMap = new HashMap<DemographicExtKey, DemographicExtConditionalKey>();
    for (var conditionalKey : DemographicExtConditionalKey.values()) {
      val set = conditionalKey.getKeySet();
      conditionalKeys.addAll(set);
      for (var extKey : set) {
        keyMap.put(extKey, conditionalKey);
        if (conditionalKey.isActive()) {
          activeKeys.add(extKey);
        }
      }
    }
    DemographicExtensionAdapter.conditionalKeys = conditionalKeys;
    DemographicExtensionAdapter.activeKeys = activeKeys;
    DemographicExtensionAdapter.keyToConditionalKeyMap = keyMap;
  }
}
