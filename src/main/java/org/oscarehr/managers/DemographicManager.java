/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
package org.oscarehr.managers;

import ca.kai.util.MapUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.PMmodule.dao.ProgramDao;
import org.oscarehr.PMmodule.model.Program;
import org.oscarehr.common.Gender;
import org.oscarehr.common.dao.AdmissionDao;
import org.oscarehr.common.dao.ConsentDao;
import org.oscarehr.common.dao.DemographicContactDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.dao.DemographicMergedDao;
import org.oscarehr.common.dao.OscarLogDao;
import org.oscarehr.common.dao.PHRVerificationDao;
import org.oscarehr.common.exception.PatientDirectiveException;
import org.oscarehr.common.model.Admission;
import org.oscarehr.common.model.Consent;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.Demographic.PatientStatus;
import org.oscarehr.common.model.DemographicContact;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.DemographicExtKey;
import org.oscarehr.common.model.DemographicMerged;
import org.oscarehr.common.model.PHRVerification;
import org.oscarehr.common.model.Provider;
import org.oscarehr.managers.adapter.DemographicAdapter;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.ws.rest.to.model.DemographicSearchRequest;
import org.oscarehr.ws.rest.to.model.DemographicSearchResult;
import org.owasp.encoder.Encode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import oscar.OscarProperties;
import oscar.log.LogAction;
import oscar.log.LogConst;

/**
 * Will provide access to demographic data, as well as closely related data such as extensions
 * (DemographicExt), merge data, archive data, etc.
 * <p>
 * Future Use: Add privacy, security, and consent profiles
 */
@Service
@Slf4j
public class DemographicManager {

  @Getter
  public enum PatientSearchStatus {
      AC("active"),
      IN("inactive"),
      ALL("");

      private final String status;

      PatientSearchStatus(final String status) {
          this.status = status;
      }
  }

  @Getter
  public enum PatientSearchMode {
      NAME("search_name"),
      PHONE("search_phone"),
      BIRTHDAY("search_dob"),
      ADDRESS("search_address"),
      HIN("search_hin"),
      CHART_NUMBER("search_chart_no"),
      DEMOGRAPHIC_NUMBER("search_demographic_no"),
      EMAIL("search_email"),
      BAND_NUMBER("search_band_number");

      private final String searchMode;

      PatientSearchMode(final String searchMode) {
          this.searchMode = searchMode;
      }
  }

  @Autowired private AdmissionDao admissionDao;
  @Autowired private DemographicDao demographicDao;
  @Autowired private DemographicContactDao demographicContactDao;
  @Autowired private DemographicExtDao demographicExtDao;
  @Autowired private DemographicMergedDao demographicMergedDao;
  @Autowired private OscarLogDao oscarLogDao;
  @Autowired private PHRVerificationDao phrVerificationDao;
  @Autowired private ProgramDao programDao;
  @Autowired private SecurityInfoManager securityInfoManager;
  @Autowired AppManager appManager;
  @Autowired ConsentDao consentDao;

  /*
   * New functionality using DemographicManagerHelper to ensure the demographic content is properly
   * archived in DemographicArchive and DemographicExtArchive.
   */

  /* DEMOGRAPHIC */

  /**
   * Archive: Archives the Demographic and DemographicExts for the given demographic.
   *
   * @param demographic the new demographic to archive in the database.
   */
  public static void archiveDemographic(final Demographic demographic) {
    DemographicAdapter.saveDemographic(demographic);
  }

  /**
   * Create & Archive: Creates a new demographic record in the system with the corresponding
   * demographic extensions in the Demographic and DemographicExt tables and automatically archives
   * the data in the DemographicArchive and DemographicExtArchive tables.
   *
   * @param demographic the new demographic to save in the database.
   * @param extensions  the demographic extensions to be saved in the database.
   */
  public static void createDemographic(
      final Demographic demographic,
      final List<DemographicExt> extensions
  ) {
    DemographicAdapter.createDemographic(demographic, extensions);
  }

  /**
   * Create & Optional Archive: Some use cases require a demographic to be persisted for the
   * demographicNo ID to be generated before using the newly generated ID to populate
   * DemographicExts. In this case we are actively choosing not to archive the data because the
   * Demographic and DemographicExts will be persisted once the demographicNo is populated.
   * <p>
   * Creates a new demographic record in the system with the corresponding demographic extensions in
   * the Demographic and DemographicExt tables and automatically archives the data in the
   * DemographicArchive and DemographicExtArchive tables.
   *
   * @param demographic          the new demographic to save in the database.
   * @param extensions           the demographic extensions to be saved in the database.
   * @param skipArchiving        skip archiving process
   * @param skipFhirSubscription skip triggering the fhir subscription
   */
  public static void createDemographic(
      final Demographic demographic,
      final List<DemographicExt> extensions,
      final boolean skipArchiving,
      final boolean skipFhirSubscription
  ) {
    DemographicAdapter.createDemographic(
        demographic, extensions, skipArchiving, skipFhirSubscription
    );
  }

  /**
   * Create & Archive: Creates a new demographic record in the system without any demographic
   * extensions in the Demographic table and automatically archives the data in the
   * DemographicArchive and the default values in the DemographicExtArchive tables.
   *
   * @param demographic the new demographic to save in the database.
   */
  public static void createDemographic(final Demographic demographic) {
    DemographicAdapter.createDemographic(demographic);
  }


  /**
   * Create & Optional Archive: Some use cases require a demographic to be persisted for the
   * demographicNo ID to be generated before using the newly generated ID to populate
   * DemographicExts. In this case we are actively choosing not to archive the data because the
   * Demographic and DemographicExts will be persisted once the demographicNo is populated.
   * <p>
   * Creates a new demographic record in the system with the corresponding demographic extensions in
   * the Demographic and DemographicExt tables and automatically archives the data in the
   * DemographicArchive and DemographicExtArchive tables.
   *
   * @param demographic   the new demographic to save in the database.
   * @param skipArchiving skip archiving process
   */
  public static void createDemographic(
      final Demographic demographic,
      final boolean skipArchiving,
      final boolean skipFhirSubscription
  ) {
    DemographicAdapter.createDemographic(demographic, skipArchiving, skipFhirSubscription);
  }

  /**
   * Delete & Archive: Deletes the given Demographic and corresponding DemographicExt values, and
   * archives the final copy.
   * <p>
   * Demographic value is not actually deleted, just set to 'IN'.
   *
   * @param demographic the new demographic to save in the database.
   * @param user        the provider number for the user deleting the demographic.
   */
  public static void deleteDemographic(final Demographic demographic, final String user) {
    DemographicAdapter.deleteDemographic(demographic, user);
  }

  public List<Demographic> getDemographicsBySearchSelect(
      String searchPreference,
      final String searchMode,
      final String status,
      final String searchString,
      final String providerNumber,
      final String orderBy,
      final int limit,
      final int offset,
      final boolean outOfDomain
  ) {
    List<Demographic> demographics;

    if (isRecentAccessSearch(searchPreference, searchString)) {
      demographics = findRecentDemographicsForProvider(10, providerNumber);
    } else {
      demographics = findDemographics(searchMode, status, searchString, providerNumber, orderBy, limit, offset, outOfDomain);
    }
    return demographics;
  }

  /**
   * Finds the most recently accessed demographics by the provider using providerNumber
   * and the entries in the "log" table.
   * <br></br>
   * If demographics are not null they will be added to list and returned.
   *
   * @param recentPatientListSize size of the patient list to be returned
   * @param providerNumber        the provider number for the user to find the recently accessed patients by
   * @return list of demographics recently accessed by provider provided in providerNumber
   */
  public List<Demographic> findRecentDemographicsForProvider(
      final int recentPatientListSize,
      final String providerNumber
  ) {
    val demographics = new ArrayList<Demographic>();
    val demographicIds = oscarLogDao.getRecentDemographicsAccessedByProvider(providerNumber, 0, recentPatientListSize);
    for (var demographicId : demographicIds) {
      val demographic = demographicDao.getDemographicById(demographicId);
      if (demographic != null) {
        demographics.add(demographic);
      }
    }
    return demographics;
  }

  public List<Demographic> findDemographics(
      final String searchMode,
      final String patientStatus,
      final String keyword,
      final String providerNumber,
      final String orderBy,
      final int limit,
      final int offset,
      final boolean outOfDomain
  ) {
    val statuses = getInactivePatientStatuses();

    List<Demographic> demographics = new ArrayList<Demographic>();
    if ((PatientSearchStatus.ALL.getStatus()).equals(patientStatus)) {
      demographics = searchAllDemographics(searchMode, keyword, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchStatus.AC.getStatus().equals(patientStatus)) {
      demographics = searchActiveDemographics(statuses, searchMode, keyword, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if ("inactive".equals(PatientSearchStatus.IN.getStatus())) {
      demographics = searchInactiveDemographics(statuses, searchMode, keyword, limit, offset, orderBy, providerNumber, outOfDomain);
    }
    return new ArrayList<Demographic>(new HashSet<Demographic>(demographics));
  }

  public List<Demographic> sortPatientSearch(
      final List<Demographic> demoList,
      final String orderBy
  ) {
    if (orderBy.equals("recently_accessed")) {
      return demoList;
    }

    if (orderBy.equals("last_name")) {
      Collections.sort(demoList, Demographic.LastNameComparator);
    } else if (orderBy.equals("last_name, first_name")) {
      Collections.sort(demoList, Demographic.LastAndFirstNameComparator);
    } else if (orderBy.equals("demographic_no")) {
      Collections.sort(demoList, Demographic.DemographicNoComparator);
    } else if (orderBy.equals("chart_no")) {
      Collections.sort(demoList, Demographic.ChartNoComparator);
    } else if (orderBy.equals("sex")) {
      Collections.sort(demoList, Demographic.SexComparator);
    } else if (orderBy.equals("dob")) {
      Collections.sort(demoList, Demographic.DateOfBirthComparator);
    } else if (orderBy.equals("provider_no")) {
      Collections.sort(demoList, Demographic.ProviderNoComparator);
    } else if (orderBy.equals("roster_status")) {
      Collections.sort(demoList, Demographic.RosterStatusComparator);
    } else if (orderBy.equals("patient_status")) {
      Collections.sort(demoList, Demographic.PatientStatusComparator);
    } else if (orderBy.equals("phone")) {
      Collections.sort(demoList, Demographic.PhoneComparator);
    }

    return demoList;
  }

  private boolean isRecentAccessSearch(String preference, final String searchString) {
    preference = preference == null ? "allDemographics" : preference;
    return preference.equals("recentlyAccessedDemographics") && searchString.isEmpty();
  }

  private List<Demographic> searchAllDemographics(
      final String searchMode,
      final String searchString,
      final int limit,
      final int offset,
      final String orderBy,
      final String providerNumber,
      final boolean outOfDomain
  ) {
    List<Demographic> demographics = new ArrayList<Demographic>();
    if (PatientSearchMode.NAME.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByName(searchString, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.PHONE.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByPhone(searchString, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.BIRTHDAY.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByDOB(searchString, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.ADDRESS.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByAddress(searchString, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.HIN.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByHIN(searchString, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.CHART_NUMBER.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .findDemographicByChartNo(searchString, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.DEMOGRAPHIC_NUMBER.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .findDemographicByDemographicNo(searchString, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.BAND_NUMBER.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .findDemographicByDemographicNo(
              getDemographicNumberWithBandNumber(searchString), limit, offset, orderBy, providerNumber, outOfDomain
          );
    } else if (PatientSearchMode.EMAIL.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .findDemographicByEmail(searchString, limit, offset, orderBy, providerNumber, outOfDomain);
    }
    return demographics;
  }

  private List<Demographic> searchActiveDemographics(
      final List<String> statuses,
      final String searchMode,
      final String searchString,
      final int limit,
      final int offset,
      final String orderBy,
      final String providerNumber,
      final boolean outOfDomain
  ) {
    List<Demographic> demographics = new ArrayList<Demographic>();
    if (PatientSearchMode.NAME.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByNameAndNotStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.PHONE.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByPhoneAndNotStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.BIRTHDAY.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByDOBAndNotStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.ADDRESS.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByAddressAndNotStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.HIN.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByHINAndNotStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.CHART_NUMBER.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .findDemographicByChartNoAndNotStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.DEMOGRAPHIC_NUMBER.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .findDemographicByDemographicNoAndNotStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.BAND_NUMBER.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .findDemographicByDemographicNoAndNotStatus(
              getDemographicNumberWithBandNumber(searchString), statuses, limit, offset, orderBy, providerNumber, outOfDomain
          );
    } else if (PatientSearchMode.EMAIL.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .findDemographicByEmailAndNotStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    }
    return demographics;
  }

  private List<Demographic> searchInactiveDemographics(
      final List<String> statuses,
      final String searchMode,
      final String searchString,
      final int limit,
      final int offset,
      final String orderBy,
      final String providerNumber,
      final boolean outOfDomain
  ) {
    List<Demographic> demographics = new ArrayList<Demographic>();
    if (PatientSearchMode.NAME.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByNameAndStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.PHONE.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByPhoneAndStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.BIRTHDAY.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByDOBAndStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.ADDRESS.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByAddressAndStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.HIN.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .searchDemographicByHINAndStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.CHART_NUMBER.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .findDemographicByChartNoAndStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.DEMOGRAPHIC_NUMBER.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .findDemographicByDemographicNoAndStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    } else if (PatientSearchMode.BAND_NUMBER.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .findDemographicByDemographicNoAndStatus(
              getDemographicNumberWithBandNumber(searchString), statuses, limit, offset, orderBy, providerNumber, outOfDomain
          );
    } else if (PatientSearchMode.EMAIL.getSearchMode().equals(searchMode)) {
      demographics = demographicDao
          .findDemographicByEmailAndStatus(searchString, statuses, limit, offset, orderBy, providerNumber, outOfDomain);
    }
    return demographics;
  }

  private List<String> getInactivePatientStatuses() {
    return Arrays.asList(
        OscarProperties.getInstance()
        .getProperty("inactive_statuses", "IN, DE, IC, ID, MO, FI")
        .replaceAll("'", "")
        .replaceAll("\\s", "")
        .split(",")
    );
  }

  public String getDemographicNumberWithBandNumber(String bandNumber) {
    val demographicExts = demographicExtDao.getDemographicExtByKeyAndValue("statusNum", bandNumber);
    var demographicNumber = "-1";

    if (demographicExts != null && !demographicExts.isEmpty()) {
      demographicNumber = demographicExts.get(0).getDemographicNo().toString();
    }
    return demographicNumber;
  }
  /**
   * Save & Archive: Gets, saves and archives the Demographic and DemographicExts for the given
   * demographic number.
   *
   * @param demographicNumber the demographic number of the demographic to save in the database.
   */
  public static void saveDemographic(final Integer demographicNumber) {
    DemographicAdapter.saveDemographic(demographicNumber);
  }

  /**
   * Save & Archive: Saves and archives the Demographic and DemographicExts for the given
   * demographic.
   *
   * @param demographic the new demographic to save in the database.
   */
  public static void saveDemographic(final Demographic demographic) {
    DemographicAdapter.saveDemographic(demographic);
  }

  /**
   * Save & Archive: Saves and archives the Demographic and DemographicExts for the given
   * demographic.
   *
   * @param demographic   the new demographic to save in the database.
   * @param skipArchiving skip archiving process
   */
  public static void saveDemographic(final Demographic demographic, boolean skipArchiving) {
    DemographicAdapter.saveDemographic(demographic, skipArchiving);
  }

  /**
   * Save & Archive: Saves and archives the Demographic and DemographicExts for the given
   * demographic.
   *
   * @param demographic the new demographic to save in the database.
   * @param extensions  the demographic extensions to be saved in the database.
   */
  public static void saveDemographic(
      final Demographic demographic,
      final Demographic previousVersion,
      final List<DemographicExt> extensions,
      final boolean skipArchiving
  ) {
    DemographicAdapter.saveDemographic(demographic, previousVersion, extensions, skipArchiving);
  }

  public static void updateDemographicAndSave(
      final HttpServletRequest request,
      final Demographic demographic,
      final String userNumber,
      final boolean skipArchiving
  ) {
    val oldDemographic = new Demographic(demographic);
    Demographic.updateDemographic(request, demographic, userNumber);
    DemographicManager.saveDemographic(
        demographic,
        oldDemographic,
        DemographicManager.processDemographicExtensions(
            request,
            userNumber,
            demographic.getDemographicNo()
        ),
        skipArchiving
    );
  }
  /* DEMOGRAPHIC EXT */

  /**
   * Archive: Archives a full copy of the demographic along with this change.
   *
   * @param extension the new demographic to archive in the database.
   */
  public static void archiveDemographicExt(final DemographicExt extension) {
    DemographicAdapter.archiveDemographicExt(extension);
  }

  /**
   * Process WITHOUT Save: Takes an input HttpServletRequest and creates DemographicExt objects with
   * it for the given demographic information.
   *
   * @param request           the HttpServletRequest from add/update a demographic record.
   * @param providerNumber    the user taking the action
   * @param demographicNumber the demographic number the data belongs to
   * @return a list of DemographicExts based on the data in the request.
   */
  public static List<DemographicExt> processDemographicExtensions(
      HttpServletRequest request,
      String providerNumber,
      Integer demographicNumber
  ) {
    return DemographicAdapter.processDemographicExtensions(
        request, providerNumber, demographicNumber
    );
  }

  /* ORIGINAL DEMOGRAPHIC MANAGER FUNCTIONALITY */

  public Demographic getDemographic(LoggedInInfo loggedInInfo, Integer demographicId)
      throws PatientDirectiveException {
    securityInfoManager.checkReadPrivilege(loggedInInfo, demographicId);
    return demographicDao.getDemographicById(demographicId);
  }

  public Demographic getDemographic(Integer demographicId) {
    return demographicDao.getDemographicById(demographicId);
  }

  public Demographic getDemographic(LoggedInInfo loggedInInfo, String demographicNo) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    try {
      return getDemographic(loggedInInfo, Integer.parseInt(demographicNo));
    } catch (NumberFormatException e) {
      log.error("Unable to parse demographic number [{}]", demographicNo);
      return null;
    }
  }

  public Demographic getDemographicWithExt(LoggedInInfo loggedInInfo, Integer demographicId) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    Demographic result = getDemographic(loggedInInfo, demographicId);
    if (result != null) {
      List<DemographicExt> demoExts = getDemographicExts(loggedInInfo, demographicId);
      if (demoExts != null && !demoExts.isEmpty()) {
        DemographicExt[] demoExtArray = demoExts.toArray(new DemographicExt[0]);
        result.setExtras(demoExtArray);
      }
    }
    return result;
  }

  public String getDemographicFormattedName(LoggedInInfo loggedInInfo, Integer demographicId) {
    var demographic = getDemographic(loggedInInfo, demographicId);
    return demographic != null
        ? demographic.getLastName() + ", " + demographic.getFirstName()
        : null;
  }

  public String getDemographicPreferredName(LoggedInInfo loggedInInfo, Integer demographicId) {
    Demographic demographic = getDemographic(loggedInInfo, demographicId);
    if (demographic == null) {
      return "";
    }
    return validateDemographicPreferredName(demographic);
  }

  private String validateDemographicPreferredName(final Demographic demographic) {
    return hasPreferredName(demographic)
        ? demographic.getPreferredName()
        : "";
  }

  private boolean hasPreferredName(final Demographic demographic) {
    return demographic.getPreferredName() != null
        && !demographic.getPreferredName().isEmpty()
        && !"NULL".equals(demographic.getPreferredName().trim().toUpperCase());
  }

  public Demographic getDemographicByMyOscarUserName(
      final LoggedInInfo loggedInInfo,
      final String myOscarUserName
  ) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicDao.getDemographicByMyOscarUserName(myOscarUserName);
  }

  public List<Demographic> searchDemographicByName(
      final LoggedInInfo loggedInInfo,
      final String searchString,
      int startIndex,
      final int itemsToReturn
  ) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    List<Demographic> results = demographicDao.searchDemographicByNameString(searchString,
        startIndex, itemsToReturn);
    log.debug("searchDemographicByName, searchString={}, result.size={}",
        searchString,
        results.size()
    );
    return results;
  }

  public List<Demographic> getActiveDemographicAfter(
      final LoggedInInfo loggedInInfo,
      final Date afterDateExclusive
  ) {
    // lastDate format: yyyy-MM-dd HH:mm:ss
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    List<Demographic> results = demographicDao.getActiveDemographicAfter(afterDateExclusive);
    if (results != null) {
      for (Demographic item : results) {
        LogAction.addLogSynchronous(
            loggedInInfo,
            "DemographicManager.getActiveDemographicAfter(date)",
            "id=" + item.getDemographicNo()
        );
      }
    }
    return results;
  }

  public List<DemographicExt> getDemographicExts(LoggedInInfo loggedInInfo, Integer id) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicExtDao.getDemographicExtByDemographicNo(id);
  }

  public DemographicExt getDemographicExt(LoggedInInfo loggedInInfo, Integer demographicNo,
      String key) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicExtDao.getDemographicExt(demographicNo, key);
  }

  public List<DemographicContact> getDemographicContacts(LoggedInInfo loggedInInfo, Integer id) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicContactDao.findActiveByDemographicNo(id);
  }

  /**
   * Returns a list of all the internal providers assigned to this demographic.
   */
  public List<Provider> getDemographicMostResponsibleProviders(LoggedInInfo loggedInInfo,
      int demographicNo) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    List<DemographicContact> demographicContacts = demographicContactDao.findAllByDemographicNoAndCategoryAndType(
        demographicNo, "professional", 0);
    ProviderManager2 providerManager = SpringUtils.getBean(ProviderManager2.class);
    List<Provider> providerList = null;

    for (DemographicContact demographicContact : demographicContacts) {
      Provider provider = providerManager.getProvider(loggedInInfo,
          demographicContact.getContactId());
      if (providerList == null) {
        providerList = new ArrayList<Provider>();
      }
      if (provider != null) {
        providerList.add(provider);
      }
    }

    if (providerList == null) {
      providerList = Collections.emptyList();
    }

    return providerList;
  }

  public void createDemographic(
      final LoggedInInfo loggedInInfo,
      final Demographic demographic,
      Integer admissionProgramId
  ) {
    securityInfoManager.checkWritePrivilege(loggedInInfo);
    try {
      demographic.getBirthDay();
    } catch (Exception e) {
      throw new IllegalArgumentException(
          "Birth date was specified for " + Encode.forJava(demographic.getFullName()) + ": "
              + demographic.getBirthDayAsString());
    }
    demographic.setLastName(demographic.getLastName().toUpperCase());
    demographic.setFirstName(demographic.getFirstName().toUpperCase());
    demographic.setPatientStatus(PatientStatus.AC.name());
    demographic.setLastUpdateUser(loggedInInfo.getLoggedInProviderNo());
    if (demographic.getHcType() == null || demographic.getHcType().trim().isEmpty()) {
      demographic.setHcType("OT"); // default hc type to OT if none is set
    }
    DemographicManager.createDemographic(demographic, Arrays.asList(demographic.getExtras()), true, true);
    if (admissionProgramId == null) {
      Program program = programDao.getProgramByName("OSCAR");
      admissionProgramId = program.getId();
    }
    Admission admission = new Admission();
    admission.setClientId(demographic.getDemographicNo());
    admission.setProgramId(admissionProgramId);
    admission.setProviderNo(loggedInInfo.getLoggedInProviderNo());
    admission.setAdmissionDate(new Date());
    admission.setAdmissionStatus(Admission.STATUS_CURRENT);
    admission.setAdmissionNotes("");
    admissionDao.saveAdmission(admission);
    LogAction.addLogSynchronous(
        loggedInInfo,
        "DemographicManager.createDemographic",
        "new id is =" + demographic.getDemographicNo()
    );
  }

  public void updateDemographic(LoggedInInfo loggedInInfo, Demographic demographic) {
    val oldDemographic = new Demographic(demographic);
    securityInfoManager.checkUpdatePrivilege(loggedInInfo);
    try {
      demographic.getBirthDay();
    } catch (Exception e) {
      throw new IllegalArgumentException(
          "Birth date was specified for " + Encode.forJava(demographic.getFullName()) + ": "
              + demographic.getBirthDayAsString());
    }
    var prevDemo = demographicDao.getDemographicById(demographic.getDemographicNo());
    //retain merge info
    demographic.setSubRecord(prevDemo.getSubRecord());
    //save current demo
    demographic.setLastUpdateUser(loggedInInfo.getLoggedInProviderNo());
    if (hasFamilyPhysician(demographic)) {
      setFamilyPhysicianExtensions(demographic);
    }
    if (demographic.getExtras() == null) {
      DemographicAdapter.saveDemographic(demographic);
    } else {
      DemographicAdapter.saveDemographic(demographic, oldDemographic, Arrays.asList(demographic.getExtras()), false);
    }
    LogAction.addLogSynchronous(
        loggedInInfo,
        LogConst.UPDATE,
        "DemographicManager.updateDemographic",
        String.valueOf(demographic.getDemographicNo())
    );
  }

  public void createUpdateDemographicContact(
      final LoggedInInfo loggedInInfo,
      final DemographicContact demoContact
  ) {
    securityInfoManager.checkWritePrivilege(loggedInInfo);
    demographicContactDao.merge(demoContact);
    LogAction.addLogSynchronous(
        loggedInInfo,
        "DemographicManager.createUpdateDemographicContact",
        "id=" + demoContact.getId()
    );
  }

  public void deleteDemographic(LoggedInInfo loggedInInfo, Demographic demographic) {
    securityInfoManager.checkWritePrivilege(loggedInInfo);
    DemographicAdapter.deleteDemographic(
        demographic,
        loggedInInfo.getLoggedInProviderNo()
    );
    LogAction.addLogSynchronous(loggedInInfo,
        LogConst.DELETE,
        "DemographicManager.deleteDemographic",
        String.valueOf(demographic.getDemographicNo())
    );
  }

  public void mergeDemographics(
      final LoggedInInfo loggedInInfo,
      final Integer parentId,
      final List<Integer> children
  ) {
    for (Integer child : children) {
      DemographicMerged dm = new DemographicMerged();
      dm.setDemographicNo(child);
      dm.setMergedTo(parentId);
      demographicMergedDao.persist(dm);
      LogAction.addLogSynchronous(loggedInInfo, "DemographicManager.mergeDemographics",
          "id=" + dm.getId());
    }
  }

  public void unmergeDemographics(
      final LoggedInInfo loggedInInfo,
      final Integer parentId,
      final List<Integer> children
  ) {
    for (Integer childId : children) {
      List<DemographicMerged> dms = demographicMergedDao.findByParentAndChildIds(parentId, childId);
      if (dms.isEmpty()) {
        throw new IllegalArgumentException(
            "Unable to find merge record for parent " + parentId + " and child " + childId);
      }
      for (DemographicMerged dm : demographicMergedDao.findByParentAndChildIds(parentId, childId)) {
        dm.setDeleted(1);
        demographicMergedDao.merge(dm);
        LogAction.addLogSynchronous(loggedInInfo, "DemographicManager.unmergeDemographics",
            "id=" + dm.getId());
      }
    }
  }

  public Long getActiveDemographicCount(LoggedInInfo loggedInInfo) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicDao.getActiveDemographicCount();
  }

  public List<Demographic> getActiveDemographics(LoggedInInfo loggedInInfo, int offset, int limit) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicDao.getActiveDemographics(offset, limit);
  }

  /**
   * Gets all merged demographic for the specified parent record ID
   *
   * @param parentId ID of the parent demographic record
   * @return Returns all merged demographic records for the specified parent id.
   */
  public List<DemographicMerged> getMergedDemographics(
      final LoggedInInfo loggedInInfo,
      final Integer parentId
  ) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicMergedDao.findCurrentByMergedTo(parentId);
  }

  public PHRVerification getLatestPhrVerificationByDemographicId(
      final LoggedInInfo loggedInInfo,
      final Integer demographicId
  ) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return (phrVerificationDao.findLatestByDemographicId(demographicId));
  }

  public boolean getPhrVerificationLevelByDemographicId(LoggedInInfo loggedInInfo,
      Integer demographicId) {
    if (appManager.hasAppDefinition(loggedInInfo, "PHR")) {
      Integer consentId = appManager.getAppDefinitionConsentId(loggedInInfo, "PHR");
      if (consentId != null) {
        Consent consent = consentDao.findByDemographicAndConsentTypeId(demographicId, consentId);
        return consent != null && consent.getPatientConsented();
      }
    } else {
      PHRVerification phrVerification = getLatestPhrVerificationByDemographicId(loggedInInfo,
          demographicId);
      if (phrVerification != null) {
        String authLevel = phrVerification.getVerificationLevel();
        return PHRVerification.VERIFICATION_METHOD_INPERSON.equals(authLevel);
      }
    }
    return false;
  }

  /**
   * This method should only return true if the demographic passed in is "phr verified" to a
   * sufficient level to allow a provider to send this phr account messages.
   */
  public boolean isPhrVerifiedToSendMessages(LoggedInInfo loggedInInfo, Integer demographicId) {
    return getPhrVerificationLevelByDemographicId(loggedInInfo, demographicId);
  }

  /**
   * This method should only return true if the demographic passed in is "phr verified" to a
   * sufficient level to allow a provider to send this phr account medicalData.
   */
  public boolean isPhrVerifiedToSendMedicalData(LoggedInInfo loggedInInfo, Integer demographicId) {
    return getPhrVerificationLevelByDemographicId(loggedInInfo, demographicId);
  }

  /**
   * @deprecated there should be a generic call for getDemographicExt(Integer demoId, String key)
   * instead. Then the caller should assemble what it needs from the demographic and ext call
   * itself.
   */
  public String getDemographicWorkPhoneAndExtension(LoggedInInfo loggedInInfo,
      Integer demographicNo) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    Demographic result = demographicDao.getDemographicById(demographicNo);
    String workPhone = result.getPhone2();
    if (workPhone != null && workPhone.length() > 0) {
      String value = demographicExtDao.getValueForDemoKey(demographicNo, "wPhoneExt");
      if (value != null && value.length() > 0) {
        workPhone += "x" + value;
      }
    }
    return (workPhone);
  }

  /**
   * @see DemographicDao findByAttributes for parameter details
   */
  public List<Demographic> searchDemographicsByAttributes(
      final LoggedInInfo loggedInInfo,
      final String hin,
      final String firstName,
      final String lastName,
      final Gender gender,
      final Calendar dateOfBirth,
      final String city,
      final String province,
      final String phone,
      final String email,
      final String alias,
      final int startIndex,
      final int itemsToReturn
  ) {
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicDao.findByAttributes(
        hin, firstName, lastName, gender, dateOfBirth, city,
        province, phone, email, alias, startIndex, itemsToReturn
    );
  }

  public List<String> getPatientStatusList() {
    return demographicDao.search_ptstatus();
  }

  public List<String> getRosterStatusList() {
    return demographicDao.getRosterStatuses();
  }

  public List<DemographicSearchResult> searchPatients(
      final LoggedInInfo loggedInInfo,
      final DemographicSearchRequest searchRequest,
      final int startIndex,
      final int itemsToReturn
  ) {
    return demographicDao.searchPatients(loggedInInfo, searchRequest, startIndex, itemsToReturn);
  }

  public int searchPatientsCount(
      final LoggedInInfo loggedInInfo,
      final DemographicSearchRequest searchRequest
  ) {
    return demographicDao.searchPatientCount(loggedInInfo, searchRequest);
  }

  /**
   * @programId can be null for all/any program
   */
  public List<Integer> getAdmittedDemographicIdsByProgramAndProvider(
      final LoggedInInfo loggedInInfo,
      final Integer programId,
      final String providerNo
  ) {
    securityInfoManager.validateLoggedInInfo(loggedInInfo);
    return admissionDao.getAdmittedDemographicIdByProgramAndProvider(programId, providerNo);
  }

  public List<Integer> getDemographicIdsWithMyOscarAccounts(
      final LoggedInInfo loggedInInfo,
      final Integer startDemographicIdExclusive,
      final int itemsToReturn
  ) {
    securityInfoManager.validateLoggedInInfo(loggedInInfo);
    return demographicDao.getDemographicIdsWithMyOscarAccounts(
        startDemographicIdExclusive, itemsToReturn
    );
  }

  public List<Demographic> getDemographics(
      final LoggedInInfo loggedInInfo,
      final List<Integer> demographicIds
  ) {
    securityInfoManager.validateLoggedInInfo(loggedInInfo);
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicDao.getDemographics(demographicIds);
  }

  public List<Demographic> searchDemographic(LoggedInInfo loggedInInfo, String searchStr) {
    securityInfoManager.validateLoggedInInfo(loggedInInfo);
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicDao.searchDemographic(searchStr);
  }

  public List<Demographic> getActiveDemosByHealthCardNo(
      final LoggedInInfo loggedInInfo,
      final String hcn,
      final String hcnType
  ) {
    securityInfoManager.validateLoggedInInfo(loggedInInfo);
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicDao.getActiveDemosByHealthCardNo(hcn, hcnType);
  }

  public List<Integer> getMergedDemographicIds(
      final LoggedInInfo loggedInInfo,
      final Integer demographicNo
  ) {
    securityInfoManager.validateLoggedInInfo(loggedInInfo);
    return demographicDao.getMergedDemographics(demographicNo);
  }

  public List<Demographic> getDemosByChartNo(LoggedInInfo loggedInInfo, String chartNo) {
    securityInfoManager.validateLoggedInInfo(loggedInInfo);
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicDao.getClientsByChartNo(chartNo);
  }

  public List<Demographic> searchByHealthCard(LoggedInInfo loggedInInfo, String hin) {
    securityInfoManager.validateLoggedInInfo(loggedInInfo);
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    List<Demographic> demographics = demographicDao.searchByHealthCard(hin);
    LogAction.addLogSynchronous(
        loggedInInfo,
        "DemographicManager.searchByHealthCard",
        "hin=" + hin
    );
    return (demographics);
  }

  public Demographic getDemographicByNamePhoneEmail(
      final LoggedInInfo loggedInInfo,
      final String firstName,
      final String lastName,
      final String hPhone,
      final String wPhone,
      final String email
  ) {
    securityInfoManager.validateLoggedInInfo(loggedInInfo);
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicDao.getDemographicByNamePhoneEmail(
        firstName, lastName, hPhone, wPhone, email
    );
  }

  public List<Demographic> getDemographicWithLastFirstDOB(
      final LoggedInInfo loggedInInfo,
      final String lastname,
      final String firstname,
      final String yearOfBirth,
      final String monthOfBirth,
      final String dateOfBirth
  ) {
    securityInfoManager.validateLoggedInInfo(loggedInInfo);
    securityInfoManager.checkReadPrivilege(loggedInInfo);
    return demographicDao.getDemographicWithLastFirstDOB(
        lastname, firstname, yearOfBirth, monthOfBirth, dateOfBirth
    );
  }

  public JSONObject matchDemographic(LoggedInInfo loggedInInfo, Calendar dateOfBirth, String hin) {
    List<Demographic> demographics
        = searchDemographicsByAttributes(
        loggedInInfo, hin, null, null, null, dateOfBirth, null, null, null, null, null, 0, 2
    );
    JSONObject responseObject = new JSONObject();

    if (demographics.size() == 1) {
      Integer demographicNo = demographics.get(0).getDemographicNo();
      responseObject.put("code", "A");
      responseObject.put("demographicNo", demographicNo);
    } else {
      responseObject.put("code", "F");
      if (demographics.size() > 1) {
        responseObject.put("message",
            "There are more than one demographics that match this information.\nPlease contact the clinic for further assistance.");
      } else {
        responseObject.put("message",
            "No demographics matched the provided information.\nPlease try again or contact the clinic for further assistance.");
      }
    }
    return responseObject;
  }

  /**
   * Find a demographic by 3 parameters, Last Name, Date of Birth, and HIN. The match can be partial
   * (2/3 must match) or full (all 3 must match).
   *
   * @param lastName The lastname to match
   * @param dob      The date of birth to match
   * @return List of demographics matching the supplied parameters
   */
  public List<Demographic> findByLastNameDob(String lastName, Calendar dob) {
    return demographicDao.findByLastNameAndDob(lastName, dob);
  }

  /* QUEEN'S UNIVERSITY FUNCTIONALITY */

  public List<String> getQueensResidentProviderNumbers(final String demographicNumber) {
    return getQueensResidentProviderNumbers(Integer.valueOf(demographicNumber));
  }

  public List<String> getQueensResidentProviderNumbers(final Integer demographicNumber) {
    val residentIds = new ArrayList<String>();
    if (OscarProperties.getInstance().isPropertyActive("queens_resident_tagging")) {
      val demographicExtValues
          = demographicExtDao.getAllValuesForDemo(demographicNumber);
      residentIds.add(
          MapUtils.getOrDefault(demographicExtValues, DemographicExtKey.MIDWIFE.getKey(), "")
      );
      residentIds.add(
          MapUtils.getOrDefault(demographicExtValues, DemographicExtKey.NURSE.getKey(), "")
      );
      residentIds.add(
          MapUtils.getOrDefault(demographicExtValues, DemographicExtKey.RESIDENT.getKey(), "")
      );
    }
    return residentIds;
  }

  private boolean hasFamilyPhysician(final Demographic demographic) {
    return !demographic.getFamilyPhysicianRowId().isEmpty()
        || !demographic.getFamilyPhysicianName().isEmpty()
        || !demographic.getFamilyPhysicianOhip().isEmpty();
  }

  private void setFamilyPhysicianExtensions(final Demographic demographic) {
    demographic.setFamilyPhysicianRowId(demographic.getFamilyPhysicianRowId());
    demographic.setFamilyPhysicianName(demographic.getFamilyPhysicianName());
    demographic.setFamilyPhysicianOhip(demographic.getFamilyPhysicianOhip());
  }
}
