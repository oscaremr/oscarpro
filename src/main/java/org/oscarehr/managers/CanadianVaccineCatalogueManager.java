/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
package org.oscarehr.managers;

import ca.uhn.fhir.context.FhirContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.oscarehr.common.dao.CVCImmunizationDao;
import org.oscarehr.common.dao.CVCMedicationDao;
import org.oscarehr.common.dao.CVCMedicationGTINDao;
import org.oscarehr.common.dao.CVCMedicationLotNumberDao;
import org.oscarehr.common.model.CVCImmunization;
import org.oscarehr.common.model.CVCMedication;
import org.oscarehr.common.model.CVCMedicationGTIN;
import org.oscarehr.common.model.CVCMedicationLotNumber;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import oscar.log.LogAction;

@Service
public class CanadianVaccineCatalogueManager {


  protected static FhirContext ctx = null;
  protected static FhirContext ctxR4 = null;
  Logger logger = MiscUtils.getLogger();

  @Autowired
  CVCMedicationDao medicationDao;
  @Autowired
  CVCMedicationLotNumberDao lotNumberDao;
  @Autowired
  CVCMedicationGTINDao gtinDao;
  @Autowired
  CVCImmunizationDao immunizationDao;

  static {
    ctx = FhirContext.forDstu3();
    ctxR4 = FhirContext.forR4();
  }

  public List<CVCImmunization> getImmunizationsByParent(String conceptId) {
    return immunizationDao.findByParent(conceptId);
  }

  public CVCMedication getMedicationBySnomedConceptId(String conceptId) {
    return medicationDao.findBySNOMED(conceptId);
  }

  public CVCMedicationLotNumber findByLotNumber(LoggedInInfo loggedInInfo, String lotNumber) {
    CVCMedicationLotNumber result = lotNumberDao.findByLotNumber(lotNumber);
    LogAction.addLogSynchronous(loggedInInfo, "CanadianVaccineCatalogueManager.findByLotNumber",
        "lotNumber:" + lotNumber);
    return result;
  }

  public CVCImmunization getBrandNameImmunizationBySnomedCode(LoggedInInfo loggedInInfo,
      String snomedCode) {
    CVCImmunization result = immunizationDao.findBySnomedConceptId(snomedCode);
    LogAction.addLogSynchronous(loggedInInfo,
        "CanadianVaccineCatalogueManager.getBrandNameImmunizationBySnomedCode",
        "snomedCode:" + snomedCode);
    return result;
  }

  public List<CVCImmunization> query(String term, boolean includeGenerics, boolean includeBrands,
      boolean includeLotNumbers, boolean includeGTINs, StringBuilder matchedLotNumber) {
    List<CVCImmunization> results = new ArrayList<>();
    if (includeGenerics || includeBrands) {
      results.addAll(immunizationDao.query(term, includeGenerics, includeBrands));
    }
    if (includeLotNumbers) {
      List<CVCMedicationLotNumber> res = lotNumberDao.query(term);
      if (res.size() == 1) {
        if (matchedLotNumber != null) {
          matchedLotNumber.append(res.get(0).getLotNumber());
        }
      }
      for (CVCMedicationLotNumber t : res) {
        String snomedId = t.getMedication().getSnomedCode();
        results.add(immunizationDao.findBySnomedConceptId(snomedId));
      }
    }
    if (includeGTINs) {
      for (CVCMedicationGTIN t : gtinDao.query(term)) {
        String snomedId = t.getMedication().getSnomedCode();
        results.add(immunizationDao.findBySnomedConceptId(snomedId));
      }
    }
    //unique it
    Map<String, CVCImmunization> tmp = new HashMap<>();
    for (CVCImmunization i : results) {
      tmp.put(i.getSnomedConceptId(), i);
    }
    List<CVCImmunization> uniqueResults = new ArrayList<>(tmp.values());
    //sort it
    Collections.sort(uniqueResults, new PrevalenceComparator());
    return uniqueResults;
  }
}

class PrevalenceComparator implements Comparator<CVCImmunization> {
  public int compare(CVCImmunization i1, CVCImmunization i2) {
    Integer d1 = i1.getPrevalence();
    Integer d2 = i2.getPrevalence();
		if (d1 == null && d2 != null) {
			return 1;
		} else if (d1 != null && d2 == null) {
			return -1;
		} else if (d1 == null && d2 == null) {
			return 0;
		} else {
			return d1.compareTo(d2) * -1;
		}
  }
}
