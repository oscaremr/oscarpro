/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.managers;

import org.oscarehr.common.model.BatchBilling;
import org.springframework.stereotype.Service;
import java.util.Collections;
import java.util.List;

@Service
public class BatchBillingManager {

    public static final String DEMOGRAPHIC_NAME = "demographic_name";
    public static final String MSG_DIAGNOSTIC = "diagnostic";
    public static final String MSG_SERVICE = "msg_service";
    public static final String MSG_LAST_BILL_DATE = "last_bill_date";
    public static final String SORT_ASC = "asc";
    public static final String SORT_DESC = "desc";

    public List<BatchBilling> sortBatchBillingList(Boolean isSortAscending, String sortColumn, List<BatchBilling> batchbillings) {
      if (isSortAscending) {
          switch (sortColumn) {
              case DEMOGRAPHIC_NAME:
                  Collections.sort(batchbillings, BatchBilling.DemographicNameAscComparator);
                  break;
              case MSG_SERVICE:
                  Collections.sort(batchbillings, BatchBilling.ServiceAscComparator);
                  break;
              case MSG_DIAGNOSTIC:
                  Collections.sort(batchbillings, BatchBilling.DxCodeAscComparator);
                  break;
              case MSG_LAST_BILL_DATE:
                  Collections.sort(batchbillings, BatchBilling.LastBillDateAscComparator);
                  break;
              default :
                  Collections.sort(batchbillings, BatchBilling.DemographicNoAscComparator);
          }
      } else {

          switch (sortColumn) {
              case DEMOGRAPHIC_NAME:
                  Collections.sort(batchbillings, BatchBilling.DemographicNameDescComparator);
                  break;
              case MSG_SERVICE:
                  Collections.sort(batchbillings, BatchBilling.ServiceDescComparator);
                  break;
              case MSG_DIAGNOSTIC:
                  Collections.sort(batchbillings, BatchBilling.DxCodeDescComparator);
                  break;
              case MSG_LAST_BILL_DATE:
                  Collections.sort(batchbillings, BatchBilling.LastBillDateDescComparator);
                  break;
              default :
                  Collections.sort(batchbillings, BatchBilling.DemographicNoDescComparator);
          }
      }

      return batchbillings;
    }

}
