/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */


package org.oscarehr.managers;

import org.apache.log4j.Logger;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.util.MiscUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import oscar.OscarProperties;

/**
 *  Properties that are specific to Oscar Pro implementation
 *  Okta propeties and preferences are included here.
 */
@Service
public class OktaManager {

	private final Logger logger = MiscUtils.getLogger();
	protected static final String ENABLE_OKTA_AUTH = "enable_okta_auth";
	protected static final String OSCAR_PRO_OKTA_ENABLED = "oscar_pro_okta_enabled";
	protected static final String OSCAR_OKTA_CLIENT_ID = "oscar_okta_client_id";
	protected static final String OSCAR_OKTA_AUTHORIZATION_SERVER_ISSUER = "oscar_okta_authorization_server_issuer";

	private OscarProperties oscarProperties;
	private final SystemPreferencesDao systemPreferencesDao;

	@Autowired
	public OktaManager(SystemPreferencesDao systemPreferencesDao) {
		this.systemPreferencesDao = systemPreferencesDao;
	}

	public OscarProperties oscarProperties() {
		if (oscarProperties == null) {
			oscarProperties = OscarProperties.getInstance();
		}
		return oscarProperties;
	}

	/** Enabling Okta is dependent on a configuration parameter and other constraints
	 *   - the enable_okta_auth property must be set to true in the oscar.properties file
	 *   - Okta must be properly configured (which is performed by Oscar Pro
	 *   - Oscar Pro sets system preferences based on Okta provisioning and enabled state
	 * @return
	 */
	public boolean isOktaEnabled() {
		boolean isEnableOktaProperty = oscarProperties().isPropertyActive(ENABLE_OKTA_AUTH);
		boolean isOscarProEnabled = oscarProperties().isOscarProEnabled();
		boolean isOscarProOktaEnabled = systemPreferencesDao.isReadBooleanPreferenceWithDefault(OSCAR_PRO_OKTA_ENABLED, false);
		String oktaIssuer = systemPreferencesDao.getPreferenceValueByName(OSCAR_OKTA_AUTHORIZATION_SERVER_ISSUER,"");
		boolean result = isEnableOktaProperty && isOscarProEnabled &&isOscarProOktaEnabled && oktaIssuer!=null;
    logger.debug(" ###  isOktaServiceEnabled = " + result + ",  isEnableOktaProperty=" + isEnableOktaProperty);
		return result;
	}

	/**
	 * Find the okta client id from system preference
	 * @return String "oscar_okta_client_id" from System preference
	 */
	public String getClientId() {
		return systemPreferencesDao.getPreferenceValueByName(OSCAR_OKTA_CLIENT_ID, "");
	}
}