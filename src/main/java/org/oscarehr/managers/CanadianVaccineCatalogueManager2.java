/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
package org.oscarehr.managers;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.core.Response;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.log4j.Logger;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.Medication;
import org.hl7.fhir.r4.model.Resource;
import org.hl7.fhir.r4.model.ResourceType;
import org.hl7.fhir.r4.model.ValueSet;
import org.hl7.fhir.r4.model.ValueSet.ConceptReferenceComponent;
import org.hl7.fhir.r4.model.ValueSet.ConceptReferenceDesignationComponent;
import org.hl7.fhir.r4.model.ValueSet.ConceptSetComponent;
import org.oscarehr.common.dao.CVCImmunizationDao;
import org.oscarehr.common.dao.CVCMedicationDao;
import org.oscarehr.common.dao.CVCMedicationGTINDao;
import org.oscarehr.common.dao.CVCMedicationLotNumberDao;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.model.CVCImmunization;
import org.oscarehr.common.model.CVCImmunizationName;
import org.oscarehr.common.model.CVCMedication;
import org.oscarehr.common.model.CVCMedicationGTIN;
import org.oscarehr.common.model.CVCMedicationLotNumber;
import org.oscarehr.common.model.LookupList;
import org.oscarehr.common.model.LookupListItem;
import org.oscarehr.common.model.UserProperty;
import org.oscarehr.integration.dhdr.OmdGateway;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import oscar.OscarProperties;
import oscar.log.LogAction;

@Service
public class CanadianVaccineCatalogueManager2 {

  static FhirContext ctxR4 = FhirContext.forR4();
  Logger logger = MiscUtils.getLogger();
  private static final String CVCFirstDate = "cvc.firstdate";

  @Autowired
  CVCMedicationDao medicationDao;
  @Autowired
  CVCMedicationLotNumberDao lotNumberDao;
  @Autowired
  CVCMedicationGTINDao gtinDao;
  @Autowired
  CVCImmunizationDao immunizationDao;
  Map<String, String> dinManufactureMap = new HashMap<>();

  public void update(LoggedInInfo loggedInInfo) {
    OmdGateway omdGateway = new OmdGateway();
    org.hl7.fhir.instance.model.api.IBaseResource base = null;
    Bundle bundle = null;
    try {
      bundle = getBundleFromServer();
    } catch (Exception e) {
      omdGateway.logError(loggedInInfo, "CVC", "DOWNLOAD", e.getLocalizedMessage());
      throw (e);
    }
    String bundleJSON = ctxR4.newJsonParser().setPrettyPrint(true).encodeResourceToString(bundle);
    omdGateway.logDataReceived(loggedInInfo, "CVC", "DOWNLOAD", "data loaded", null);
    OscarProperties oscarProperties = OscarProperties.getInstance();
    if (oscarProperties.hasProperty("CVC_BUNDLE_LOCAL_FILE")) {
      try {
        FileUtils.writeStringToFile(new File(oscarProperties.getProperty("CVC_BUNDLE_LOCAL_FILE")),
            bundleJSON);
      } catch (IOException e) {
        logger.error("Error", e);
      }
    } else {
      logger.info(
          "CVC_BUNDLE_LOCAL_FILE property not set. Not writing to file to disk. (not needed) ");
    }
    clearCurrentData();
    for (Bundle.BundleEntryComponent bec : bundle.getEntry()) {
      Resource res = bec.getResource();
      if (res.getResourceType() == ResourceType.ValueSet) {
        if (res.getIdElement().getIdPart().equals("Generic")) {
          updateGenericImmunizations(loggedInInfo, (ValueSet) res);
        } else if (res.getIdElement().getIdPart().equals("Tradename")) {
          updateBrandNameImmunizations(loggedInInfo, (ValueSet) res);
        } else if (res.getIdElement().getIdPart().equals("AnatomicalSite")) {
          updateAnatomicalSites(loggedInInfo, (ValueSet) res);
        } else if (res.getIdElement().getIdPart().equals("RouteOfAdmin")) {
          updateRoutes(loggedInInfo, (ValueSet) res);
        } else {
          logger.debug("value-set " + res.getId());
        }
      } else if (res.getResourceType() == ResourceType.Bundle) {
        if (res.getIdElement().getIdPart().equals("Tradename")) {
          updateMedications(loggedInInfo, (Bundle) res);
        }
      } else {
        logger.warn("resource type = " + res.getResourceType().toString());
      }
    }
    setUpdatedInPropertyTable();
    setFirstDateInPropertyTable();
  }

  private void setUpdatedInPropertyTable() {
    UserPropertyDAO userPropertyDao = SpringUtils.getBean(UserPropertyDAO.class);
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    UserProperty up = userPropertyDao.getProp("cvc.updated");
    if (up == null) {
      up = new UserProperty();
      up.setName("cvc.updated");
    }
    up.setValue(formatter.format(new Date()));
    userPropertyDao.merge(up);
  }

  private void setFirstDateInPropertyTable() {
    UserPropertyDAO userPropertyDao = SpringUtils.getBean(UserPropertyDAO.class);
    UserProperty up = userPropertyDao.getProp(CVCFirstDate);
    if (up == null) {
      up = new UserProperty();
      up.setName(CVCFirstDate);
      up.setValue("" + (new Date()).getTime());
      userPropertyDao.persist(up);
    }
  }

  private void clearCurrentData() {
    medicationDao.removeAll();
    lotNumberDao.removeAll();
    gtinDao.removeAll();
    immunizationDao.removeAll();
  }

  private Bundle getBundleFromServer() {
    try {
      IBaseResource instance = Bundle.class.newInstance();
      logger.info("getClass get Name " + instance.getClass().getName());
      logger.info("getClass get Name " + instance.getClass().getCanonicalName());
      logger.info("getClass get Name " + instance.getClass().getTypeName());
      Method[] methods = instance.getClass().getMethods();
      for (Method method : methods) {
        logger.info("method = " + method.getName());
      }
      org.hl7.fhir.r4.model.Bundle bbbbcccc = (Bundle) instance;
      logger.info("structurefhir " + bbbbcccc.getStructureFhirVersionEnum());
      logger.info("structurefhir " + instance.getStructureFhirVersionEnum());
    } catch (Exception j) {
      logger.error("instance ", j);
    }
    String xAppDesc = OscarProperties.getInstance()
        .getProperty("oneid.oauth2.clientId", "OSCAREMR");
    WebClient wc = WebClient.create(CanadianVaccineCatalogueManager2.getCVCURL() + "/Bundle/CVC");
    Response response2 = null;
    Bundle bundle = null;
    try {
      response2 = wc.header("x-app-desc", xAppDesc).get();
      String body = response2.readEntity(String.class);
      logger.info("body" + body);
      IParser parser = ctxR4.newJsonParser();
      bundle = (Bundle) parser.parseResource(Bundle.class, body);
    } catch (Exception e) {
      logger.error("error getting bundle from Server", e);
    }
    return bundle;
  }

  public void updateGenericImmunizations(LoggedInInfo loggedInInfo, ValueSet vs) {
    for (ConceptSetComponent c : vs.getCompose().getInclude()) {
      List<ConceptReferenceComponent> cons = c.getConcept();
      for (ConceptReferenceComponent cc : cons) {
        CVCImmunization imm = new CVCImmunization();
        imm.setSnomedConceptId(cc.getCode());
        imm.setVersionId(0);
        for (ConceptReferenceDesignationComponent cr : cc.getDesignation()) {
          Coding use = cr.getUse();
          CVCImmunizationName name = new CVCImmunizationName();
          name.setLanguage(cr.getLanguage());
          if (use != null) {
            name.setUseSystem(use.getSystem());
            name.setUseCode(use.getCode());
            name.setUseDisplay(use.getDisplay());
            logger.info(cc.getCode() + " display name " + use.getDisplay() + " cc display "
                + cc.getDisplay());
          } else {
            logger.error("USE WAS NULL for " + cr.getValue() + " " + c);
          }
          name.setValue(cr.getValue());
          imm.getNames().add(name);
        }
        for (Extension ext : cc.getExtension()) {
          if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-shelf-status".equals(
              ext.getUrl())) {
            CodeableConcept shelfStatusConcept = (CodeableConcept) ext.getValue();
            for (Coding parentConceptCode : shelfStatusConcept.getCoding()) {
              if ("https://cvc.canimmunize.ca/v3/Valueset/ShelfStatus".equals(
                  parentConceptCode.getSystem())) {
                imm.setShelfStatus(parentConceptCode.getDisplay());
              }
            }
          }
          if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-concept-last-updated".equals(
              ext.getUrl())) {
          }
          if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-ontario-ispa-vaccine".equals(
              ext.getUrl())) {
            Boolean ispa = (Boolean) ext.getValueAsPrimitive().getValue();
            imm.setIspa(ispa);
          }
          if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-contains-antigens".equals(
              ext.getUrl())) {
          }
          if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-protects-against-diseases".equals(
              ext.getUrl())) {
          }
        }
        imm.setGeneric(true);
        saveImmunization(loggedInInfo, imm);
      }
    }
  }

  public void saveImmunization(LoggedInInfo loggedInInfo, CVCImmunization immunization) {
    immunizationDao.saveEntity(immunization);
    LogAction.addLogSynchronous(loggedInInfo, "CanadianVaccineCatalogueManager.saveImmunization",
        immunization.getId().toString());
  }

  public void updateBrandNameImmunizations(LoggedInInfo loggedInInfo, ValueSet vs) {
    for (ConceptSetComponent c : vs.getCompose().getInclude()) {
      List<ConceptReferenceComponent> cons = c.getConcept();
      for (ConceptReferenceComponent cc : cons) {
        CVCImmunization imm = new CVCImmunization();
        imm.setSnomedConceptId(cc.getCode());
        imm.setVersionId(0);
        for (ConceptReferenceDesignationComponent cr : cc.getDesignation()) {
          Coding use = cr.getUse();
          CVCImmunizationName name = new CVCImmunizationName();
          name.setLanguage(cr.getLanguage());
          if (use != null) {
            name.setUseSystem(use.getSystem());
            name.setUseCode(use.getCode());
            name.setUseDisplay(use.getDisplay());
          } else {
            logger.info("USE WAS NULL for " + cr.getValue() + " " + c);
          }
          name.setValue(cr.getValue());
          imm.getNames().add(name);
        }
        String manufactureDisplay = null;
        for (Extension ext : cc.getExtension()) {
          if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-shelf-status".equals(
              ext.getUrl())) {
            CodeableConcept shelfStatusConcept = (CodeableConcept) ext.getValue();
            for (Coding parentConceptCode : shelfStatusConcept.getCoding()) {
              if ("https://cvc.canimmunize.ca/v3/CodeSystem/ShelfStatus".equals(
                  parentConceptCode.getSystem())) {
                imm.setShelfStatus(parentConceptCode.getDisplay());
              }
            }
          }
          if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-ontario-ispa-vaccine".equals(
              ext.getUrl())) {
            Boolean ispa = (Boolean) ext.getValueAsPrimitive().getValue();
            imm.setIspa(ispa);
          }
          if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-parent-concept".equals(
              ext.getUrl())) {
            CodeableConcept parentConcept = (CodeableConcept) ext.getValue();
            for (Coding parentConceptCode : parentConcept.getCoding()) {
              if ("https://cvc.canimmunize.ca/v3/ValueSet/Generic".equals(
                  parentConceptCode.getSystem())) {
                imm.setParentConceptId(parentConceptCode.getCode());
              }
            }
          }
          if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-typical-dose-size".equals(
              ext.getUrl())) {
            String typicalDose = ext.getValueAsPrimitive().getValueAsString();
            imm.setTypicalDose(typicalDose);
          }
          if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-typical-dose-size-uom".equals(
              ext.getUrl())) {
            String typicalDoseUofM = ext.getValueAsPrimitive().getValueAsString();
            imm.setTypicalDoseUofM(typicalDoseUofM);
          }
          if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-strength".equals(
              ext.getUrl())) {
            String strength = ext.getValueAsPrimitive().getValueAsString();
            imm.setStrength(strength);
          }
        }
        if (imm.getSnomedConceptId() != null && manufactureDisplay != null) {
          dinManufactureMap.put(imm.getSnomedConceptId(), manufactureDisplay);
        }
        imm.setGeneric(false);
        saveImmunization(loggedInInfo, imm);
      }
    }
  }

  public void updateMedications(LoggedInInfo loggedInInfo, Bundle bundle) {
    processMedicationBundle(loggedInInfo, bundle);
  }

  private void processMedicationBundle(LoggedInInfo loggedInInfo, Bundle bundle) {

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    for (BundleEntryComponent entry : bundle.getEntry()) {
      CVCMedication cMed = new CVCMedication();
      Medication med = (Medication) entry.getResource();
      logger.debug("processing " + med.getIdBase() + " : " + med.getIdElement().getIdPart());
      if (dinManufactureMap.containsKey(med.getIdElement().getIdPart())) {
        cMed.setManufacturerDisplay(dinManufactureMap.get(med.getIdElement().getIdPart()));
      }
      cMed.setStatus(med.getStatus().toString());
      for (Coding c : med.getCode().getCoding()) {
        if ("http://hl7.org/fhir/sid/ca-hc-din".equals(c.getSystem())) {
          cMed.setDin(c.getCode());
          cMed.setDinDisplayName(c.getDisplay());

        }
        if ("https://fhir.infoway-inforoute.ca/CodeSystem/snomedctcaextension".equals(
            c.getSystem())) {
          cMed.setSnomedCode(c.getCode());
          cMed.setSnomedDisplay(c.getDisplay());
        }
        if ("http://www.gs1.org/gtin".equals(c.getSystem())) {
          cMed.getGtinList().add(new CVCMedicationGTIN(cMed, c.getCode()));
        }
      }
      for (Extension ext : med.getExtension()) {
        if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-market-authorization-holder".equals(
            ext.getUrl())) {
          cMed.setManufacturerDisplay(ext.getValue().primitiveValue());
        }
        if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-shelf-status".equals(
            ext.getUrl())) {
          CodeableConcept shelfStatusConcept = (CodeableConcept) ext.getValue();
          for (Coding parentConceptCode : shelfStatusConcept.getCoding()) {
            if ("https://cvc.canimmunize.ca/v3/ValueSet/ShelfStatus".equals(
                parentConceptCode.getSystem())) {
              cMed.setStatus(parentConceptCode.getDisplay());
            }
          }
        }
        if ("https://cvc.canimmunize.ca/v3/StructureDefinition/ca-cvc-lots".equals(ext.getUrl())) {
          for (Extension lotsExt : ext.getExtension()) {
            if ("ca-cvc-lot".equals(lotsExt.getUrl())) {
              String lotNumber = null;
              String expiry = null;
              for (Extension lotExt : lotsExt.getExtension()) {
                if ("lotNumber".equals(lotExt.getUrl())) {
                  lotNumber = lotExt.getValueAsPrimitive().getValueAsString();
                }
                if ("expiryDate".equals(lotExt.getUrl())) {
                  expiry = lotExt.getValueAsPrimitive().getValueAsString();
                }
              }
              try {
                cMed.getLotNumberList()
                    .add(new CVCMedicationLotNumber(cMed, lotNumber, formatter.parse(expiry)));
              } catch (ParseException e) {
                logger.warn("Error", e);
              }
            }
          }
        }
      }
      saveMedication(loggedInInfo, cMed);
    }
  }

  public void saveMedication(LoggedInInfo loggedInInfo, CVCMedication medication) {
    Set<CVCMedicationGTIN> gtins = medication.getGtinList();
    Set<CVCMedicationLotNumber> lotNumbers = medication.getLotNumberList();
    medication.setGtinList(null);
    medication.setLotNumberList(null);
    medicationDao.saveEntity(medication);
    for (CVCMedicationGTIN g : gtins) {
      gtinDao.saveEntity(g);
    }
    for (CVCMedicationLotNumber l : lotNumbers) {
      lotNumberDao.saveEntity(l);
    }
    LogAction.addLogSynchronous(loggedInInfo, "CanadianVaccineCatalogueManager.saveMedication",
        medication.getId().toString());
  }

  public void updateAnatomicalSites(LoggedInInfo loggedInInfo, ValueSet vs) {
    int displayOrder = 0;
    LookupListManager llm = SpringUtils.getBean(LookupListManager.class);
    LookupList ll = llm.findLookupListByName(loggedInInfo, "AnatomicalSite");
    if (ll == null) {
      ll = new LookupList();
      ll.setActive(true);
      ll.setCreatedBy("OSCAR");
      ll.setDateCreated(new Date());
      ll.setDescription("Anatomical Sites from CVC");
      ll.setName("AnatomicalSite");
      ll.setListTitle("Anatomical Site");
      ll = llm.addLookupList(loggedInInfo, ll);
    } else {
      llm.removeLookupListItems(loggedInInfo, ll.getId());
      ll = llm.findLookupListByName(loggedInInfo, "AnatomicalSite");
    }
    for (ConceptSetComponent c : vs.getCompose().getInclude()) {
      List<ConceptReferenceComponent> cons = c.getConcept();
      for (ConceptReferenceComponent cc : cons) {
        String code = cc.getCode();
        String display = cc.getDisplay();
        LookupListItem lli = new LookupListItem();
        lli.setActive(true);
        lli.setCreatedBy("OSCAR");
        lli.setDateCreated(new Date());
        lli.setLabel(display);
        lli.setValue(code);
        lli.setLookupListId(ll.getId());
        lli.setDisplayOrder(displayOrder++);
        llm.addLookupListItem(loggedInInfo, lli);
      }
    }
  }

  public void updateRoutes(LoggedInInfo loggedInInfo, ValueSet vs) {
    int displayOrder = 0;
    LookupListManager llm = SpringUtils.getBean(LookupListManager.class);
    LookupList ll = llm.findLookupListByName(loggedInInfo, "RouteOfAdmin");
    if (ll == null) {
      ll = new LookupList();
      ll.setActive(true);
      ll.setCreatedBy("OSCAR");
      ll.setDateCreated(new Date());
      ll.setDescription("Routes of Administration from CVC");
      ll.setName("RouteOfAdmin");
      ll.setListTitle("Routes of Administration");
      ll = llm.addLookupList(loggedInInfo, ll);
    } else {
      llm.removeLookupListItems(loggedInInfo, ll.getId());
      ll = llm.findLookupListByName(loggedInInfo, "RouteOfAdmin");
    }
    for (ConceptSetComponent c : vs.getCompose().getInclude()) {
      List<ConceptReferenceComponent> cons = c.getConcept();
      for (ConceptReferenceComponent cc : cons) {
        String code = cc.getCode();
        String display = cc.getDisplay();
        LookupListItem lli = new LookupListItem();
        lli.setActive(true);
        lli.setCreatedBy("OSCAR");
        lli.setDateCreated(new Date());
        lli.setLabel(display);
        lli.setValue(code);
        lli.setLookupListId(ll.getId());
        lli.setDisplayOrder(displayOrder++);
        llm.addLookupListItem(loggedInInfo, lli);
      }
    }
  }

  public static String getCVCURL() {
    String url = OscarProperties.getInstance().getProperty("cvc.url");
    UserPropertyDAO upDao = SpringUtils.getBean(UserPropertyDAO.class);
    UserProperty up = upDao.getProp("cvc.url");
    if (up != null && !StringUtils.isEmpty(up.getValue())) {
      url = up.getValue();
    }
    return url;
  }

  public static boolean getCVCActive(Date creationDate) {
    boolean cvcActive = false;
    UserPropertyDAO upDao = SpringUtils.getBean(UserPropertyDAO.class);
    UserProperty up = upDao.getProp(CVCFirstDate);
    if (up != null && !StringUtils.isEmpty(up.getValue())) {
      if (creationDate == null) {
        cvcActive = true;
      } else {
        long timeInMillis = Long.parseLong(up.getValue());
        Date cvcfirstDate = new Date(timeInMillis);
        if (cvcfirstDate.before(creationDate)) {
          cvcActive = true;
        }
      }
    }
    return cvcActive;
  }
}
