/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.managers;

import java.util.ArrayList;
import java.util.List;

import org.oscarehr.common.dao.CtlBillingServiceDao;
import org.oscarehr.common.dao.PropertyDao;
import org.oscarehr.common.model.Property;
import org.oscarehr.common.dao.BillingDao;
import org.oscarehr.common.model.BillingAutoRules;
import org.oscarehr.common.model.BillingAutoRulesMap;
import org.oscarehr.managers.model.ServiceType;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BillingManager {

	@Autowired
	CtlBillingServiceDao ctlBillingServiceDao;

	@Autowired
	private BillingDao billingDao;

	/*
	 * I'm only doing that conversion in the manager because I don't have time to refactor the DAO method..but given more time..that's where I would do it.
	 * Regardless those other calls should be moved over to calling this one.
	 */
	public List<ServiceType> getUniqueServiceTypes(LoggedInInfo loggedInInfo) {
		return getUniqueServiceTypes(loggedInInfo,CtlBillingServiceDao.DEFAULT_STATUS);
	}

	/*
	 * I'm only doing that conversion in the manager because I don't have time to refactor the DAO method..but given more time..that's where I would do it.
	 * Regardless those other calls should be moved over to calling this one.
	 */
	public List<ServiceType> getUniqueServiceTypes(LoggedInInfo loggedInInfo, String type) {
		List<ServiceType> result = new ArrayList<ServiceType>();

		for(Object[] r:ctlBillingServiceDao.getUniqueServiceTypes(type)) {
			result.add(new ServiceType((String)r[0],(String)r[1]));
		}

		return result;
	}

    public String getThirdPartyFooterText(Integer demographicNo) {
        PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
        StringBuilder text = new StringBuilder();

        if (demographicNo == -1) {
            return "";
        }

        Property enableFooterPatientId = propertyDao.checkByName("enable_footer_patient_id");
        if (enableFooterPatientId != null && Boolean.parseBoolean(enableFooterPatientId.getValue())) {
            text.append("Pt ID Number: ").append(demographicNo).append("\n\n");
        }
        Property thirdPartyFooterText = propertyDao.checkByName("3rd_party_billing_footer_text");
        if (thirdPartyFooterText != null && !org.apache.commons.lang3.StringUtils.isEmpty(thirdPartyFooterText.getValue())) {
            text.append(thirdPartyFooterText.getValue());
        }

        return text.toString();
    }


	public void saveBillingAutoRule(LoggedInInfo loggedInInfo, BillingAutoRules billingAutoRule) {
		billingDao.persist(billingAutoRule);
	}
	
	public void updateBillingAutoRule(LoggedInInfo loggedInInfo, BillingAutoRules billingAutoRule) {
		billingDao.merge(billingAutoRule);
	}

	public void removeBillingAutoRule(LoggedInInfo loggedInInfo, Integer billingAutoRuleId) {
		BillingAutoRules rule = billingDao.findAutoRuleById(billingAutoRuleId);
		rule.setStatus(0);
		billingDao.merge(rule);

		//delete all mappings with ruleId TODO: this is crud may want to no delete but do a status change
		billingDao.deleteAllAutoRuleMappingWithRuleId(Integer.toString(billingAutoRuleId));
	}

	public BillingAutoRules getBillingAutoRule(LoggedInInfo loggedInInfo, Integer id) {
		BillingAutoRules result = billingDao.findAutoRuleById(id);
		return result;
	}

	public List<BillingAutoRules> getAllBillingAutoRules(LoggedInInfo loggedInInfo, Integer offset, Integer limit) {
		List<BillingAutoRules> results = billingDao.findAllActiveAutoRules(offset, limit);
		return results;
	}

	public List<BillingAutoRules> getBillingAutoRulesMap(LoggedInInfo loggedInInfo, String appointmentTypeId) {
		List<BillingAutoRules> results = billingDao.findAutoRulesMap(appointmentTypeId);
		return results;
	}

	public List<BillingAutoRulesMap> getBillingAutoRulesMapByRuleId(LoggedInInfo loggedInInfo, String ruleId) {
		List<BillingAutoRulesMap> results = billingDao.findAutoRulesMapByRuleId(ruleId);
		return results;
	}

	public Integer deleteAutoRuleMapping(LoggedInInfo loggedInInfo, String appointmentTypeId, String ruleId){
		Integer autoRuleMapping = billingDao.findAutoRuleMapId(appointmentTypeId, ruleId);
		//billingDao.remove(autoRuleMapping)

		Integer deletedId = 0;
		if(autoRuleMapping>0){
			billingDao.deleteAutoRuleMapping(autoRuleMapping);
			deletedId = autoRuleMapping;
		}

		return deletedId;
	}

}
