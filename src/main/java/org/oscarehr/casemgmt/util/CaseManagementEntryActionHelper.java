package org.oscarehr.casemgmt.util;

import javax.servlet.http.HttpServletRequest;
import lombok.val;
import lombok.var;
import org.apache.commons.lang.StringUtils;
import oscar.oscarEncounter.pageUtil.EctSessionBean;

public class CaseManagementEntryActionHelper {
  public static String setEncounterType(
      final HttpServletRequest request,
      final String defaultEncounterType,
      final EctSessionBean bean
  ) {

    var type = "";
    val reason = request.getParameter("reason");
    val reasonCondition = StringUtils.isNotEmpty(reason);
    val beanCondition = StringUtils.isNotEmpty(bean.encType);
    val defaultCondition = StringUtils.isNotEmpty(defaultEncounterType);
    val encounterType = request.getParameter("encType");
    val encounterCondition = StringUtils.isNotEmpty(encounterType);
    if (reasonCondition && beanCondition) {
      type = bean.encType;
    } else if (reasonCondition || (defaultCondition && !encounterCondition)) {
      type = defaultEncounterType;
    } else if (encounterCondition) {
      type = encounterType;
    } else {
      type = bean.encType;
    }

    return type;
  }
}
