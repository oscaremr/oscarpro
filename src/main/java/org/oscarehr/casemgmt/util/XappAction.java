/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.casemgmt.util;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.util.rest.RestRequestException;
import org.oscarehr.util.rest.StatusResponse;
import oscar.OscarProperties;
import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.cert.X509Certificate;
import java.util.Arrays;

public class XappAction extends Action {

  private final String apiKey;
  private final String clientId;
  private final String url;
  private final Logger logger;

  public XappAction() {
    apiKey = OscarProperties.getInstance().getProperty("innomar.integration.api_key");
    clientId = OscarProperties.getInstance().getProperty("innomar.integration.client_id");
    url = OscarProperties.getInstance().getProperty("XAPP_URL");

    logger = Logger.getLogger(XappAction.class);
  }

  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws Exception {

    try (CloseableHttpClient httpClient = HttpClients.custom()
            .setSSLSocketFactory(getSslConnectionSocketFactory())
            .build()) {

      HttpPost httpPost = buildPost(request.getParameter("demoNo"), request.getParameter("providerNo"));
      HttpResponse httpResponse = httpClient.execute(httpPost);

      return getActionForward(mapping, response, httpResponse, httpPost);
    } catch (RestRequestException ex) {
      String error = ex.getStatusResponse().getError();
      logger.error(error, ex);
      throw new RestRequestException(StatusResponse.Fail(error));
    }
  }

  private SSLConnectionSocketFactory getSslConnectionSocketFactory() throws Exception {

    TrustStrategy trustStrategy = new TrustStrategy() {
      @Override
      public boolean isTrusted(final X509Certificate[] chain, final String authType) {
        return true;
      }
    };

    SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, trustStrategy).build();

    return new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
  }

  private ActionForward getActionForward(ActionMapping mapping, HttpServletResponse response,
      HttpResponse httpResponse, HttpPost httpPost) throws RestRequestException, IOException {

    String responseEntity = EntityUtils.toString(httpResponse.getEntity());
    int statusCode = httpResponse.getStatusLine().getStatusCode();
    String failStatusResponse = getFailStatusResponse(httpResponse, responseEntity, httpPost);
    logger.info(failStatusResponse);

    if (statusCode == 302) {
      String locationUrl = httpResponse.getFirstHeader("location").getValue();
      response.getWriter().write("redirect:" + locationUrl);
      return mapping.findForward("success");
    } else if (statusCode < 200 || statusCode >= 300) {
      throw new RestRequestException(StatusResponse.Fail(failStatusResponse));
    } else {
      response.getWriter().write(responseEntity);
      return mapping.findForward("success");
    }
  }

  private HttpPost buildPost(String demoNo, String providerNo) throws UnsupportedEncodingException {
    HttpPost httpPost = new HttpPost(url);
    httpPost.setEntity(createHttpEntity(clientId, demoNo, providerNo));
    httpPost.addHeader("X-WELL-EMR-API-KEY", apiKey);
    httpPost.setHeader("Content-type", "application/json");
    return httpPost;
  }

  private String getFailStatusResponse(HttpResponse httpResponse, String resMsg, HttpPost httpPost)
      throws IOException {
    String httpPostHeaders = Arrays.toString(httpPost.getAllHeaders());
    InputStream httpPostEntity = httpPost.getEntity().getContent();
    int statusCode = httpResponse.getStatusLine().getStatusCode();

    String postEntityString = IOUtils.toString(httpPostEntity, StandardCharsets.UTF_8);

    return resMsg + ("\nResponseHeader: "
        + Arrays.toString(httpResponse.getAllHeaders())
        + "\nStatusCode: "
        + statusCode
        + "\n httpPostHeaders: "
        + httpPostHeaders
        + "\n httpPostEntity: "
        + postEntityString);
  }

  private StringEntity createHttpEntity(String clientId, String demoNo, String providerNo)
      throws UnsupportedEncodingException {
    XappRequest request = new XappRequest(demoNo, providerNo, clientId);
    return new StringEntity(new GsonBuilder().create().toJson(request));
  }

  @RequiredArgsConstructor
  public static class XappRequest {

    @SerializedName("PatientID")
    @NonNull @Getter @Setter private String demoNo;

    @SerializedName("ProviderID")
    @NonNull @Getter @Setter private String providerNo;

    @SerializedName("SiteCredential")
    @NonNull
    @Getter
    @Setter
    private String clientId;

    @SerializedName("SiteKey")
    @Getter @Setter private String siteKey = "OSCAR_CDS_KEY";
  }
}
