/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.casemgmt.service;

import ca.oscarpro.ehr.patient.projection.ClassicPatientChartProjection;
import com.lowagie.text.DocumentException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.oscarehr.PMmodule.caisi_integrator.CaisiIntegratorManager;
import org.oscarehr.PMmodule.model.ProgramProvider;
import org.oscarehr.PMmodule.service.ProgramManager;
import org.oscarehr.caisi_integrator.ws.CachedDemographicNote;
import org.oscarehr.caisi_integrator.ws.DemographicWs;
import org.oscarehr.casemgmt.dao.CaseManagementNoteLinkDAO;
import org.oscarehr.casemgmt.model.CaseManagementNote;
import org.oscarehr.casemgmt.model.CaseManagementNoteExt;
import org.oscarehr.casemgmt.model.Issue;
import org.oscarehr.casemgmt.service.impl.DefaultNoteService;
import org.oscarehr.casemgmt.util.ExtPrint;
import org.oscarehr.casemgmt.web.NoteDisplay;
import org.oscarehr.casemgmt.web.NoteDisplayLocal;
import org.oscarehr.common.dao.CaseManagementIssueNotesDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.dao.MeasurementDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.Measurement;
import org.oscarehr.common.model.Prevention;
import org.oscarehr.common.model.Provider;
import org.oscarehr.managers.PreventionManager;
import org.oscarehr.managers.ProgramManager2;
import org.oscarehr.sharingcenter.util.CaseManagementUtil;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.springframework.dao.DataAccessException;
import oscar.OscarProperties;
import oscar.oscarLab.ca.all.pageUtil.LabPDFCreator;
import oscar.oscarLab.ca.all.pageUtil.OLISLabPDFCreator;
import oscar.oscarLab.ca.all.parsers.Factory;
import oscar.oscarLab.ca.all.parsers.MessageHandler;
import oscar.oscarLab.ca.all.parsers.OLISHL7Handler;
import oscar.oscarLab.ca.on.CommonLabResultData;
import oscar.oscarLab.ca.on.LabResultData;
import oscar.util.ConcatPDF;
import oscar.util.ConversionUtils;

public class CaseManagementPrint {
	
	private static Logger logger = MiscUtils.getLogger();
	
	
	
	CaseManagementManager caseManagementMgr = SpringUtils.getBean(CaseManagementManager.class);

	private DefaultNoteService defaultNoteService = SpringUtils.getBean(DefaultNoteService.class);
	
	
	private ProgramManager2 programManager2 = SpringUtils.getBean(ProgramManager2.class);
	
	private ProgramManager programMgr = SpringUtils.getBean(ProgramManager.class);

    private DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
    private DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
	private CaseManagementNoteLinkDAO caseManagementNoteLinkDao = SpringUtils.getBean(CaseManagementNoteLinkDAO.class);
	private CaseManagementIssueNotesDao caseManagementIssueNotesDao = SpringUtils.getBean(CaseManagementIssueNotesDao.class);
	private PreventionManager preventionManager = SpringUtils.getBean(PreventionManager.class);

  public void doPrint(ClassicPatientChartProjection classicPatientChartProjection)
      throws DocumentException, IOException {
    doPrint(
        classicPatientChartProjection.getLoggedInInfo(),
        classicPatientChartProjection.getDemographicNumber(),
        classicPatientChartProjection.isPrintAllNotes(),
        classicPatientChartProjection.getNoteIds(), classicPatientChartProjection.isPrintCPP(),
        classicPatientChartProjection.isPrintRx(), classicPatientChartProjection.isPrintLabs(),
        classicPatientChartProjection.isPrintPreventions(),
        classicPatientChartProjection.isPrintMeasurements(),
        classicPatientChartProjection.isPrintDocuments(),
        classicPatientChartProjection.isPrintHrms(),
        classicPatientChartProjection.getStartDate(),
        classicPatientChartProjection.getEndDate(),
        classicPatientChartProjection.getRequest(),
        classicPatientChartProjection.getStream()
    );
  }

  public void doPrint(
      final LoggedInInfo loggedInInfo,
      final Integer demographicNo,
      final boolean printAllNotes,
      String[] noteIds,
      final boolean printCPP,
      final boolean printRx,
      final boolean printLabs,
      final boolean printPreventions,
      final boolean printMeasurements,
      final boolean printDocuments,
      final boolean printHrms,
      final Calendar startDate,
      final Calendar endDate,
      final HttpServletRequest request,
      final OutputStream os
  ) throws IOException, DocumentException {
    doPrint(
        loggedInInfo,
        demographicNo,
        printAllNotes,
        noteIds,
        printCPP,
        printRx,
        printLabs,
        printPreventions,
        printMeasurements,
        printDocuments,
        printHrms,
        startDate,
        endDate,
        request,
        os,
        Boolean.parseBoolean(request.getHeader("Is-Gateway-Request"))
    );
  }

	/*
	 *This method was in CaseManagementEntryAction but has been moved out so that both the classic Echart and the flat echart can use the same printing method.
	 * 
	 */
    public void doPrint(
        final LoggedInInfo loggedInInfo,
        final Integer demographicNo,
        final boolean printAllNotes,
        String[] noteIds,
        final boolean printCPP,
        final boolean printRx,
        final boolean printLabs,
        final boolean printPreventions,
        final boolean printMeasurements,
        final boolean printDocuments,
        final boolean printHrms,
        final Calendar startDate,
        final Calendar endDate,
        final HttpServletRequest request,
        final OutputStream os,
        final boolean isGatewayRequest
    ) throws IOException, DocumentException {
		
		String providerNo=loggedInInfo.getLoggedInProviderNo();
      if (printAllNotes) {
        try {
          logger.debug("NOTES2PRINT: " + noteIds);
          noteIds = getAllNoteIds(loggedInInfo, request, "" + demographicNo, isGatewayRequest);
        } catch (NullPointerException npe) {
          logger.error("Null pointer encountered while printing All Notes in doPrint()", npe);
        } catch (Exception e) {
          logger.error("Unexpected error occurred while printing All Notes in doPrint()", e);
        }
      }
		String demono = ""+demographicNo;
		Demographic demographic = demographicDao.getDemographic(demono);
		request.setAttribute("demoName", "");
		request.setAttribute("demoSex", "");
		request.setAttribute("demoAge", "");
		request.setAttribute("mrp", "");
		request.setAttribute("mrn", "");
		request.setAttribute("demoCity", "");
		request.setAttribute("demoBandNumber", "");
		request.setAttribute("demoDOB", "");
		request.setAttribute("hin", "");
		request.setAttribute("homePhone", "");
		request.setAttribute("workPhone", "");
		request.setAttribute("cellPhone", "");
    if (demographic != null) {
      request.setAttribute(
          "demoName", demographic.getFirstName() + " " + demographic.getLastName());
      request.setAttribute("demoSex", demographic.getSex());
      request.setAttribute("demoAge", demographic.getAge());
      Provider mrp = demographic.getProvider();
      if (mrp != null) {
        request.setAttribute("mrp", mrp.getFirstName() + " " + mrp.getLastName());
      }
      if(demographic.getChartNo() != null) {
        request.setAttribute("mrn", demographic.getChartNo());
      }
      if (OscarProperties.getInstance().isPropertyActive("FIRST_NATIONS_MODULE")) {
        request.setAttribute("demoCity", demographic.getCity());
        String bandNumber = demographicExtDao.getValueForDemoKey(demographicNo, "statusNum");
        if (bandNumber != null) {
          request.setAttribute("demoBandNumber", bandNumber);
        }
      }
      request.setAttribute("demoDOB", convertDateFmt(demographic.getFormattedDob(), request));
      request.setAttribute("hin", demographic.getHin());
			String homePhoneExt = demographicExtDao.getValueForDemoKey(demographicNo, "hPhoneExt");
			request.setAttribute("homePhone", demographic.getPhone() + (StringUtils.isNotEmpty(homePhoneExt) ? " Ext. " + homePhoneExt : ""));
			String workPhoneExt = demographicExtDao.getValueForDemoKey(demographicNo, "wPhoneExt");
			request.setAttribute("workPhone", demographic.getPhone2() + (StringUtils.isNotEmpty(workPhoneExt) ? " Ext. " + workPhoneExt : ""));
			String cellPhone = demographicExtDao.getValueForDemoKey(demographicNo, "demo_cell");
      if (cellPhone != null) {
        request.setAttribute("cellPhone", cellPhone);
			}
		}

		
        String[] issueCodes = {"OMeds", "SocHistory", "MedHistory", "Concerns", "Reminders",
            "FamHistory", "RiskFactors"};
		List<CaseManagementNote> notes = new ArrayList<CaseManagementNote>();
		List<String> remoteNoteUUIDs = new ArrayList<String>();
		String uuid;
		for (int idx = 0; idx < noteIds.length; ++idx) {
			if (noteIds[idx].startsWith("UUID")) {
				uuid = noteIds[idx].substring(4);
				remoteNoteUUIDs.add(uuid);
			} else {
				Long noteId = ConversionUtils.fromLongString(noteIds[idx]);
				if (noteId > 0) {
					CaseManagementNote note = this.caseManagementMgr.getNote(noteId.toString());
					//Filter out document uploaded notes, all note linked notes (eg. comments on documents), all CPP (there is a separate toggle to enable these)
                    if (note != null && note.getProviderNo() != null
                        && Integer.parseInt(note.getProviderNo()) != -1
                        && caseManagementNoteLinkDao.getLastLinkByNote(noteId) == null
                        && caseManagementIssueNotesDao
                            .getNoteCppIssues(noteId.intValue(), issueCodes).isEmpty()) {
						notes.add(note);
					}
				}
			}
		}

		if (loggedInInfo.getCurrentFacility().isIntegratorEnabled() && remoteNoteUUIDs.size() > 0) {
			DemographicWs demographicWs = CaisiIntegratorManager.getDemographicWs(loggedInInfo, loggedInInfo.getCurrentFacility());
			List<CachedDemographicNote> remoteNotes = demographicWs.getLinkedCachedDemographicNotes(Integer.parseInt(demono));
			for (CachedDemographicNote remoteNote : remoteNotes) {
				for (String remoteUUID : remoteNoteUUIDs) {
					if (remoteUUID.equals(remoteNote.getCachedDemographicNoteCompositePk().getUuid())) {
						CaseManagementNote fakeNote = getFakedNote(remoteNote);
						notes.add(fakeNote);
						break;
					}
				}
			}
		}

		// we're not guaranteed any ordering of notes given to us, so sort by observation date
		oscar.OscarProperties p = oscar.OscarProperties.getInstance();
		String noteSort = p.getProperty("CMESort", "");
		if (noteSort.trim().equalsIgnoreCase("UP")) {
			Collections.sort(notes, CaseManagementNote.noteObservationDateComparator);
			Collections.reverse(notes);
		} else { 
			Collections.sort(notes, CaseManagementNote.noteObservationDateComparator);
		}
		
		//How should i filter out observation dates?
		if(startDate != null && endDate != null){
			List<CaseManagementNote> dateFilteredList = new ArrayList<CaseManagementNote>();
			logger.debug("start date "+startDate);
			logger.debug("end date "+endDate);
			
			for (CaseManagementNote cmn : notes){
				logger.debug("cmn "+cmn.getId()+"  -- "+cmn.getObservation_date()+ " ? start date "+startDate.getTime().before(cmn.getObservation_date())+" end date "+endDate.getTime().after(cmn.getObservation_date()));
				if(startDate.getTime().before(cmn.getObservation_date()) && endDate.getTime().after(cmn.getObservation_date())){
					dateFilteredList.add(cmn);
				}
			}
			notes = dateFilteredList;
		}

		List<CaseManagementNote> issueNotes;
		List<CaseManagementNote> tmpNotes;
		HashMap<String, List<CaseManagementNote>> cpp = null;
      if (printCPP) {
        try {
          logger.debug("Processing CPP data");
          cpp = new HashMap<String, List<CaseManagementNote>>();

          for (int j = 0; j < issueCodes.length; ++j) {
            List<Issue> issues = caseManagementMgr.getIssueInfoByCode(providerNo, issueCodes[j]);
            String[] issueIds = getIssueIds(issues);// = new String[issues.size()];
            tmpNotes = caseManagementMgr.getNotes(demono, issueIds);
            issueNotes = new ArrayList<CaseManagementNote>();
            for (int k = 0; k < tmpNotes.size(); ++k) {
              if (!tmpNotes.get(k).isLocked() && !tmpNotes.get(k).isArchived()) {
                List<CaseManagementNoteExt> exts
                    = caseManagementMgr.getExtByNote(tmpNotes.get(k).getId());
                boolean exclude = false;
                for (CaseManagementNoteExt ext : exts) {
                  if (ext.getKeyVal().equals("Hide Cpp")) {
                    if (ext.getValue().equals("1")) {
                      exclude = true;
                    }
                  }
                }
                if (!exclude) {
                  // filter cpp within specified date range
                  if (startDate != null && endDate != null) {
                    Date observationDate = tmpNotes.get(k).getObservation_date();
                    if (startDate.getTime().before(observationDate) && endDate.getTime()
                        .after(observationDate)) {
                      issueNotes.add(tmpNotes.get(k));
                    }
                  } else {
                    issueNotes.add(tmpNotes.get(k));
                  }
                }
              }
            }
            cpp.put(issueCodes[j], issueNotes);
          }
        } catch (DataAccessException dae) {
          logger.error("Database error occurred while fetching Issues in CPP doPrint()", dae);
        }catch (IndexOutOfBoundsException iobe) {
          logger.error("Index out of bounds while processing CPP in doPrint()", iobe);
        } catch (NullPointerException npe) {
          logger.error("Encountered a null reference while processing CPP in doPrint()", npe);
        } catch (Exception e) {
          logger.error("Error occurred while printing CPP in doPrint()", e);
        }
      }
		String demoNo = null;
		List<CaseManagementNote> othermeds = null;
		if (printRx) {
			try {
        logger.debug("Processing Rx data");
        demoNo = demono;
        if (cpp == null) {
          List<Issue> issues = caseManagementMgr.getIssueInfoByCode(providerNo, "OMeds");
          String[] issueIds = getIssueIds(issues);// new String[issues.size()];
          othermeds = caseManagementMgr.getNotes(demono, issueIds);
        } else {
          othermeds = cpp.get("OMeds");
        }

        if(startDate != null && endDate !=null && othermeds.size() > 0){
          for (CaseManagementNote med : othermeds){
            Date observationDate = med.getObservation_date();
            // remove rx not in specified date range from already created list
            if(!startDate.getTime().before(observationDate) && !endDate.getTime().after(observationDate)){
              othermeds.remove(med);
            }
          }
        }
      } catch (DataAccessException dae) {
        logger.error("Database error occurred while fetching Issues in CPP doPrint()", dae);
      } catch (IndexOutOfBoundsException iobe) {
        logger.error("Index out of bounds while processing Rx in doPrint()", iobe);
      } catch (NullPointerException npe) {
        logger.error("Encountered a null reference while processing Rx in doPrint()", npe);
      } catch (ConcurrentModificationException cme) {
        logger.error("Attempted to modify the list while iterating through it in doPrint()", cme);
      } catch (Exception e) {
        logger.error("Unexpected error occurred while processing Rx in doPrint()", e);
      }
		}

		List<Measurement> measurements = null;
		if (printMeasurements) {
			try {
        MeasurementDao measurementsDao = SpringUtils.getBean(MeasurementDao.class);

        if(startDate != null && endDate != null) {
          measurements = measurementsDao.findByDemographicIdObservedDate(demographicNo, startDate.getTime(), endDate.getTime());

        } else {
          measurements = measurementsDao.findByDemographicId(demographicNo);
        }
        Collections.sort(measurements, Measurement.MEASUREMENT_TYPE_COMPARATOR);
      } catch (DataAccessException dae) {
        logger.error("Database error occurred while fetching Measurements in doPrint()", dae);
      } catch (NullPointerException npe) {
        logger.error("Encountered a null reference while processing Measurements in doPrint()", npe);
      } catch (ClassCastException cce) {
        logger.error("Type casting error during measurement sorting in doPrint()", cce);
      } catch (Exception e) {
        logger.error("Unexpected error occurred while processing Measurements in doPrint()", e);
      }
		}

		List<Prevention> preventions = null;
		if(printPreventions) {
      try {
        logger.debug("Processing Preventions data");
        preventions = preventionManager.getPreventionsByDemographicNo(loggedInInfo, Integer.parseInt(demono));
      } catch (DataAccessException dae) {
        logger.error("Database error occurred while fetching Preventions in doPrint()", dae);
      } catch (NumberFormatException nfe) {
        logger.error("Error parsing demographic number in doPrint()", nfe);
      } catch (Exception e) {
        logger.error("Unexpected error occurred while processing Preventions in doPrint()", e);
      }
		}

		SimpleDateFormat headerFormat = new SimpleDateFormat("yyyy-MM-dd.hh.mm.ss");
	    Date now = new Date();
	    String headerDate = headerFormat.format(now);
		
		// Create new file to save form to
		String path = OscarProperties.getInstance().getProperty("DOCUMENT_DIR");
		String fileName = path + "EncounterForm-" + headerDate + ".pdf";
                File file=null;
                FileOutputStream out=null;
                File file2=null;
                FileOutputStream os2=null;
		List<Object> pdfDocs = new ArrayList<Object>();

                try {
                file= new File(fileName);
		out = new FileOutputStream(file);

		CaseManagementPrintPdf printer = new CaseManagementPrintPdf(request, out);
		printer.printDocHeaderFooter();
		printer.printCPP(cpp);
		if (cpp != null){
			// Only print allergies when printing CPP
			printer.printAllergies(demographicNo);
		}
		if(startDate!=null&&endDate!=null){
			printer.printRx(demoNo, othermeds, startDate, endDate);
		}
		else{
			printer.printRx(demoNo, othermeds);
		}
		printer.printMeasurements(measurements);
		printer.printPreventions(preventions);
		printer.printNotes(notes);

		/* check extensions */
		Enumeration<String> extensions = request.getParameterNames();
		while (extensions.hasMoreElements()) {
			String name = extensions.nextElement();
			if (name.startsWith("extPrint")) {
				if (request.getParameter(name).equals("true")) {
					ExtPrint printBean = (ExtPrint) SpringUtils.getBean(name);
					if (printBean != null) {
						printBean.printExt(printer, request);
					}
				}
			}
		}
		printer.finish();

		pdfDocs.add(fileName);

		if (printLabs) {
      try {
        logger.debug("Processing Labs data");
        // get the labs which fall into the date range which are attached to this patient
        CommonLabResultData comLab = new CommonLabResultData();
        ArrayList<LabResultData> labs = comLab.populateLabResultsData(loggedInInfo, "", demono, "", "", "", "U");
        LinkedHashMap<String, LabResultData> accessionMap = new LinkedHashMap<String, LabResultData>();
        for (int i = 0; i < labs.size(); i++) {
          LabResultData result = labs.get(i);
          if (result.isHL7TEXT()) {
            if (result.accessionNumber == null || result.accessionNumber.equals("")) {
              accessionMap.put("noAccessionNum" + i + result.labType, result);
            } else {
              if (!accessionMap.containsKey(result.accessionNumber + result.labType)) accessionMap.put(result.accessionNumber + result.labType, result);
            }
          }
        }
        for (LabResultData result : accessionMap.values()) {
          Date observationDate = result.getDateObj();
          String segmentId = result.segmentID;
          MessageHandler handler = Factory.getHandler(segmentId);
          if(handler == null || handler.getHeaders().get(0).equals("CELLPATHR")) {
            continue;
          }

          String fileName2 = OscarProperties.getInstance().getProperty("DOCUMENT_DIR") + "//" + handler.getPatientName().replaceAll("\\s", "_") + "_" + segmentId + "_" + handler.getMsgDate().replaceAll("\\s", "_").replaceAll(":", "") + "_LabReport.pdf";
          file2= new File(fileName2);
          os2 = new FileOutputStream(file2);
          if (handler instanceof OLISHL7Handler) {
            OLISLabPDFCreator olisLabPdfCreator = new OLISLabPDFCreator(os2, request, segmentId);
            olisLabPdfCreator.printPdf();
          }
          else {
            //LabPDFCreator pdfCreator = new LabPDFCreator(os2, segmentId, loggedInInfo.getLoggedInProviderNo());
            File f = File.createTempFile(String.format("%03d", Integer.parseInt(segmentId)),"pdf");
            FileOutputStream fos = new FileOutputStream(f);
            LabPDFCreator pdf = new LabPDFCreator(handler, fos, segmentId, result.multiLabId, "");
            pdf.printPdf();
            pdf.addEmbeddedDocuments(f,os2);
            f.delete();
          }
          // filter labs within specified date range
          if(startDate != null && endDate !=null){
            if(startDate.getTime().before(observationDate) && endDate.getTime().after(observationDate)){
              pdfDocs.add(fileName2);
            }
          }
          else{
            pdfDocs.add(fileName2);
          }
        }
      } catch (NullPointerException npe) {
        logger.error("Encountered a null reference while processing Labs in doPrint()", npe);
      } catch (DataAccessException dae) {
        logger.error("Database error occurred while fetching Labs in doPrint()", dae);
      } catch (NumberFormatException nfe) {
        logger.error("Error parsing segment ID in Labs doPrint()", nfe);
      } catch (IOException ioe) {
        logger.error("File I/O error occurred while processing Labs in doPrint()", ioe);
      } catch (DocumentException de) {
        logger.error("Error generating Lab PDF document in doPrint()", de);
      } catch (Exception e) {
        logger.error("Unexpected error occurred while processing Labs in doPrint()", e);
      }
		}

    if (printDocuments) {
      try {
        pdfDocs.addAll(CaseManagementUtil.printDocuments(demono, startDate, endDate, loggedInInfo));
      } catch (NullPointerException npe) {
        logger.error("Null pointer encountered while processing Documents in doPrint()", npe);
      } catch (IOException ioe) {
        logger.error("IO error occurred while processing Documents in doPrint()", ioe);
      } catch (Exception e) {
        logger.error("Unexpected error occurred while processing Documents in doPrint()", e);
      }
    }

    if (printHrms) {
      try {
        pdfDocs.addAll(CaseManagementUtil.printHrms(demono, startDate, endDate, loggedInInfo));
      } catch (NullPointerException npe) {
        logger.error("Null pointer encountered while processing Hrms in doPrint()", npe);
      } catch (DocumentException de) {
        logger.error("Document error occurred while processing HRMS in doPrint()", de);
      } catch (IOException ioe) {
        logger.error("IO error occurred while processing HRMS in doPrint()", ioe);
      } catch (Exception e) {
        logger.error("Unexpected error occurred while processing HRMS in doPrint()", e);
      }
    }

		ConcatPDF.concat(pdfDocs, os);
                } catch (IOException e)
                {
                    logger.error("Error ",e);
                    
                }
                finally {
                  if (out!=null) {
                      out.close();
                  }
                  if (os2!=null) {
                      os2.close();
                  }
                  if (file!=null) {
                      file.delete();
                  }
                  if (file2 != null) {
                    file2.delete();
                  }
                  for(Object o:pdfDocs) {
                      new File((String)o).delete();
                  }
                }
	}
	
	public String[] getIssueIds(List<Issue> issues) {
		String[] issueIds = new String[issues.size()];
		int idx = 0;
		for (Issue i : issues) {
			issueIds[idx] = String.valueOf(i.getId());
			++idx;
		}
		return issueIds;
	}
	
	private CaseManagementNote getFakedNote(CachedDemographicNote remoteNote) {
		CaseManagementNote note = new CaseManagementNote();

		if (remoteNote.getObservationDate() != null) note.setObservation_date(remoteNote.getObservationDate().getTime());
		note.setNote(remoteNote.getNote());

		return (note);
	}
	
	
	@SuppressWarnings("unchecked")
    private String[] getAllNoteIds(
      final LoggedInInfo loggedInInfo,
      final HttpServletRequest request,
      final String demoNo,
      final boolean isGatewayRequest
  ) {
		
		HttpSession se = loggedInInfo.getSession();

    String programId = null;
    if (!isGatewayRequest) {
      ProgramProvider pp =
          programManager2.getCurrentProgramInDomain(
              loggedInInfo,
              loggedInInfo.getLoggedInProviderNo()
          );
      if (pp != null && pp.getProgramId() != null) {
        programId = pp.getProgramId().toString();
      }
    }
    if (programId == null) {
      programId = programMgr.getProgramIdByProgramName("OSCAR").toString();
    }
		
		NoteSelectionCriteria criteria = new NoteSelectionCriteria();
		criteria.setMaxResults(Integer.MAX_VALUE);
		criteria.setDemographicId(ConversionUtils.fromIntString(demoNo));
		criteria.setUserRole((String) request.getSession().getAttribute("userrole"));
		criteria.setUserName((String) request.getSession().getAttribute("user"));
		if (request.getParameter("note_sort") != null && request.getParameter("note_sort").length() > 0) {
			criteria.setNoteSort(request.getParameter("note_sort"));
		}
		if (programId != null && !programId.trim().isEmpty()) {
			criteria.setProgramId(programId);
		}
		
		
		if (se.getAttribute("CaseManagementViewAction_filter_roles") != null) {
			criteria.getRoles().addAll((List<String>) se.getAttribute("CaseManagementViewAction_filter_roles"));
		}
		
		if (se.getAttribute("CaseManagementViewAction_filter_providers") != null) {
			criteria.getProviders().addAll((List<String>) se.getAttribute("CaseManagementViewAction_filter_providers"));
		}

		if (se.getAttribute("CaseManagementViewAction_filter_issues") != null) {
			criteria.getIssues().addAll((List<String>) se.getAttribute("CaseManagementViewAction_filter_issues"));
		}

		if (!StringUtils.isBlank((String) se.getAttribute("CaseManagementViewAction_filter_encounter_type"))) {
			criteria.setEncounterType((String) se.getAttribute("CaseManagementViewAction_filter_encounter_type"));
		}

		if (logger.isDebugEnabled()) {
			logger.debug("SEARCHING FOR NOTES WITH CRITERIA: " + criteria);
		}
		
		NoteSelectionResult result = defaultNoteService.findNotes(loggedInInfo, criteria, isGatewayRequest);
		
		List<String>  buf = new ArrayList<String>();
		for(NoteDisplay nd : result.getNotes()) {
			if (!(nd instanceof NoteDisplayLocal)) {
				continue;
			}
			buf.add(nd.getNoteId().toString());
		}
		
		
		return buf.toArray(new String[0]);
    }

	
	protected String getDemoName(String demoNo) {
		if (demoNo == null) {
			return "";
		}
		return caseManagementMgr.getDemoName(demoNo);
	}


	protected String convertDateFmt(String strOldDate, HttpServletRequest request) {
		String strNewDate = new String();
		if (strOldDate != null && strOldDate.length() > 0) {
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd", request.getLocale());
			try {

				Date tempDate = fmt.parse(strOldDate);
				strNewDate = new SimpleDateFormat("dd-MMM-yyyy", request.getLocale()).format(tempDate);

			} catch (ParseException ex) {
				MiscUtils.getLogger().error("Error", ex);
			}
		}

		return strNewDate;
	}
	
}
