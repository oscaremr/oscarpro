/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */
package org.oscarehr.casemgmt.model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import org.caisi.model.BaseObject;
import org.oscarehr.casemgmt.dao.CaseManagementNoteLinkDAO;
import org.oscarehr.common.model.Provider;
import org.oscarehr.util.SpringUtils;
import oscar.oscarRx.data.RxPrescriptionData;

public class CaseManagementNote extends BaseObject {

	public static final String DELETED = "DELETED";
	@Getter @Setter private Long id;

	@Getter @Setter private int appointmentNo;
	@Getter @Setter private boolean archived;
	@Getter @Setter private boolean available;
	@Getter @Setter private String billing_code = "";
	@Getter @Setter private Date create_date;
	@Getter @Setter private String demographic_no;
	@Getter @Setter private List<Provider> editors = new ArrayList<>();
	@Getter @Setter private String encounter_type = "";
	@Getter @Setter private Set<Object> extend = new HashSet<>();
	@Getter @Setter private String facilityName = "None Specified";
	@Getter @Setter private String history;
	@Getter @Setter private Integer hourOfEncounterTime;
	@Getter @Setter private Integer hourOfEncTransportationTime;
	@Getter @Setter private boolean includeissue = true;
	@Getter @Setter private Date autoSyncDate;
	@Getter @Setter private Date lastSyncedDate;
	@Getter @Setter private boolean locked;
	@Getter @Setter private Integer minuteOfEncounterTime;
	@Getter @Setter private Integer minuteOfEncTransportationTime;
	@Getter @Setter private String note;
	@Getter @Setter private Date observation_date;
	@Getter @Setter private String password;
	@Getter @Setter private String passwordConfirm;
	@Getter @Setter private int position = 0;
	@Getter @Setter private String programName;
	@Getter @Setter private String program_no;
	@Getter @Setter private Provider provider;
	@Getter @Setter private String providerNo;
	@Getter @Setter private boolean remote = false;
	@Getter @Setter private String revision;
	@Getter @Setter private String reporter_caisi_role;
	@Getter @Setter private String reporter_program_team;
	@Getter @Setter private String roleName;
	@Getter @Setter private boolean signed = false;
	@Getter @Setter private String signing_provider_no;
	@Getter @Setter private Date update_date;
	@Getter @Setter private String uuid;

	@Deprecated
	private Set<CaseManagementIssue> issues = new HashSet<>();

	CaseManagementNoteLinkDAO caseManagementNoteLinkDao =
			(CaseManagementNoteLinkDAO) SpringUtils.getBean("CaseManagementNoteLinkDAO");

	private CaseManagementNoteLink cmnLink = null;
	private boolean cmnLinkRetrieved = false;
	private int hashCode = Integer.MIN_VALUE;

	public CaseManagementNote() {
		update_date = new Date();
	}

	public Map<String, Object> getMap() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("update_date", update_date);
		map.put("create_date", create_date);
		map.put("observation_date", observation_date);
		map.put("demographic_no", demographic_no);
		map.put("note", note);
		map.put("signed", signed);
		map.put("includeissue", includeissue);
		map.put("provider_no", providerNo);
		map.put("signing_provider_no", signing_provider_no);
		map.put("encounter_type", encounter_type);
		map.put("billing_code", billing_code);
		map.put("program_no", program_no);
		map.put("reporter_caisi_role", reporter_caisi_role);
		map.put("reporter_caisi_team", reporter_program_team);
		map.put("history", history);
		map.put("provider", provider);
		map.put("editors", editors);
		map.put("role_name", roleName);
		map.put("program_name", programName);
		map.put("uuid", uuid);
		map.put("revision", revision);
		map.put("locked", locked);
		map.put("archived", archived);
		map.put("remote", remote);
		map.put("facility_name", facilityName);
		map.put("appointment_no", appointmentNo);
		return map;
	}

	@Override
	public boolean equals(Object noteObject) {
		if (!(noteObject instanceof CaseManagementNote)) {
			return false;
		} else {
			CaseManagementNote note = (CaseManagementNote) noteObject;
			return null != this.getId() && null != note.getId() && (this.getId().equals(note.getId()));
		}
	}

	@Override
	public int hashCode() {
		if (Integer.MIN_VALUE == this.hashCode) {
			if (null == this.getId()) {
				return super.hashCode();
			}
			else {
				String hashStr = this.getClass().getName() + ":" + this.getId().hashCode();
				this.hashCode = hashStr.hashCode();
			}
		}
		return this.hashCode;
	}

	public String getAuditString() {
		StringBuilder auditStr = new StringBuilder(getNote());
		Iterator<CaseManagementIssue> iter = issues.iterator();
		auditStr.append("\nIssues\n");
		int count = 0;
		while (iter.hasNext()) {
			auditStr.append(iter.next().getIssue().getDescription()).append("\n");
			++count;
		}
		if (count == 0) {
			auditStr.append("None");
		}
		return auditStr.toString();
	}

	/**
	 * @deprecated too inefficient and too many dependencies use CaseManagementIssueNotesDao
	 */
	@Deprecated
	public Set<CaseManagementIssue> getIssues() {
		return issues;
	}

	/**
	 * @deprecated too inefficient and too many dependencies use CaseManagementIssueNotesDao
	 */
	@Deprecated
	public void setIssues(Set<CaseManagementIssue> issues) {
		this.issues = issues;
	}

	public String getProviderName() {
		if (getProvider() == null) {
			return DELETED;
		}
		return getProvider().getFormattedName();
	}

	public String getProviderNameFirstLast() {
		if (getProvider() == null) {
			return DELETED;
		}
		return getProvider().getFullName();
	}

	public boolean getHasHistory() {
		val history = getHistory();
		return history != null && history.contains("----------------History Record----------------");
	}

	public String getStatus() {
		String status = "";
		if (isSigned()) {
			status = "Signed";
		} else {
			status = "Unsigned";
		}

		if (getPassword() != null && getPassword().length() > 0) {
			// locked note - can be temporarily unlocked
			if (locked) {
				status += "/Locked";
			} else {
				status += "/Unlocked";
			}
		}
		return status;
	}

	public RxPrescriptionData.Prescription getRxFromAnnotation(CaseManagementNoteLink cmnl) {
		if (this.isRxAnnotation()) {
			String drugId = cmnl.getTableId().toString();
			//get drug id from cmn_link table
			RxPrescriptionData rxData = new RxPrescriptionData();
			// create Prescription
			return rxData.getLatestPrescriptionScriptByPatientDrugId(
					Integer.parseInt(this.getDemographic_no()),
					drugId
			);
		}
		return null;
	}

	public boolean isDocumentNote() {
		return isLinkTo(CaseManagementNoteLink.DOCUMENT);
	}

	public boolean isRxAnnotation() {
		return isLinkTo(CaseManagementNoteLink.DRUGS);
	}

	public boolean isEformData() {
		return isLinkTo(CaseManagementNoteLink.EFORMDATA);
	}

	private boolean isLinkTo(Integer tableName) {
		if (!cmnLinkRetrieved) {
			cmnLink = caseManagementNoteLinkDao.getLastLinkByNote(this.id);
			cmnLinkRetrieved = true;
		}
		return cmnLink != null && cmnLink.getTableName().equals(tableName);
	}

	/* COMPARATORS */

	public static Comparator<CaseManagementNote> getProviderComparator() {
		return new Comparator<CaseManagementNote>() {
			public int compare(CaseManagementNote note1, CaseManagementNote note2) {
				if (note1 == null || note2 == null) {
					return 0;
				}

				return note1.getProviderName().compareTo(note2.getProviderName());
			}
		};

	}

	public static Comparator<CaseManagementNote> getProgramComparator() {
		return new Comparator<CaseManagementNote>() {
			public int compare(CaseManagementNote note1, CaseManagementNote note2) {
				if (note1 == null || note1.getProgramName() == null || note2 == null || note2.getProgramName() == null) {
					return 0;
				}
				return note1.getProgramName().compareTo(note2.getProgramName());
			}
		};

	}

	public static Comparator<CaseManagementNote> getRoleComparator() {
		return new Comparator<CaseManagementNote>() {
			public int compare(CaseManagementNote note1, CaseManagementNote note2) {
				if (note1 == null || note2 == null) {
					return 0;
				}
				return note1.getRoleName().compareTo(note2.getRoleName());
			}
		};

	}

	public static Comparator<CaseManagementNote> noteObservationDateComparator = new Comparator<CaseManagementNote>() {

            public int compare(CaseManagementNote note1, CaseManagementNote note2) {
			if (note1 == null || note2 == null) {
				return 0;
			}
			return note2.getObservation_date().compareTo(note1.getObservation_date());
		}
	};

	public static Comparator<CaseManagementNote> getPositionComparator() {
		return new Comparator<CaseManagementNote>() {
			public int compare(CaseManagementNote note1, CaseManagementNote note2) {
				if (note1 == null || note2 == null) {
					return 0;
				}
				return Integer.compare(note1.getPosition(), note2.getPosition());
			}
		};
	}
}
