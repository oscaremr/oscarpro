/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.admin.web;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.SecurityDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.ProviderArchive;
import org.oscarehr.common.model.Security;
import org.oscarehr.managers.OktaManager;
import org.oscarehr.provider.web.okta.UserCredentials;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.util.rest.RestApiClient;
import org.oscarehr.util.rest.RestRequestException;
import org.oscarehr.util.rest.StatusResponse;

/**
 * <code>OktaUserHelper</code> is a utility class for creating Okta User using REST service provided
 * by OSCAR Pro when kai emr and okta integration are enabled.
 */
public class OktaUserHelper {

  private static final String CREATE_USER_URL = "/api/integration/okta/user";
  private static final String UPDATE_USER_URL = "/api/integration/okta/user";
  private static final String CHANGE_USER_PASSWORD_URL = "/api/integration/okta/user-change-password";
  private static final String EXPIRE_USER_PASSWORD_URL = "/api/integration/okta/user-expire-password";
  private static final String OBJECT_ALREADY_EXIST_MESSAGE = "Object already exists in Okta.";

  private SecurityDao securityDao = SpringUtils.getBean(SecurityDao.class);
  private ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
  private OktaManager oktaManager = SpringUtils.getBean(OktaManager.class);
  private SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
  private Logger logger = MiscUtils.getLogger();

  private RestApiClient restApiClient = new RestApiClient();

  /**
   * Adds a user to Okta
   *
   * <p>Processing status is available as a "message" variable.
   *
   * @param pageContext JSP page context
   */
  public void addOktaUser(final PageContext pageContext, final boolean forceResetPassword) {
    if (!oktaManager.isOktaEnabled()) {
      return;
    }
    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
    String username = request.getParameter("user_name");
    if (StringUtils.isBlank(username)) {
      pageContext.setAttribute("message", "No username entered");
      return;
    }
    if (isKaiSupportUser(username)) {
      return;
    }
    StatusResponse status = processAddUserRequest(request);
    pageContext.setAttribute("message", status.toUserMessage());
    if (!status.isOk()) {
      removeOscarUserByUsername(username);
    } else if (forceResetPassword) {
      if (resetOktaUserPassword(username, request).isOk()) {
        pageContext.setAttribute("message", "admin.securityaddsecurity.msgAdditionSuccessPasswordForceReset");
      }
    }
  }

  /** Update a user profile in Okta for specified Provider
   *  This will only change information contained in the provider records, such as first name, last name, email
   */
  public String updateOktaUser(
      PageContext pageContext, Provider updatedProvider, ProviderArchive archivedProvider) {
    if (!oktaManager.isOktaEnabled()) {
      return null;
    }
    // sync-ed attributes were not changed, no updates in Okta is needed
    if (!syncedProviderAttributesChanged(updatedProvider, archivedProvider)) {
      logger.debug("No changes to the synced provider attributes. No updates in Okta is needed.");
      return "admin.providerupdate.msgUpdateSuccess";
    }

    // user associated with provider is not provisioned in Okta, no updates in Okta is needed
    List<Security> securities = securityDao.findByProviderNo(updatedProvider.getProviderNo());
    if (securities == null || securities.isEmpty()) {

      return "admin.providerupdate.msgUpdateSuccess";
    }

    Security securityUser = securities.get(0);
    // Do not forward Okta requests if user is Support User
    if (isKaiSupportUser(securityUser.getUserName())) {
      return null;
    }

    return processUpdateUserRequest(pageContext, securityUser, updatedProvider);
  }


  /**
   * Update a user credential in Okta for user specified by specified Security record
   *  - the username may have been changed, oldUserName identifies the previously use name
   *
   * IMPORTANT - Assume this is called before updating the Oscar User, so no Oscar rollback is needed on failure.
   *
   * @return msg key for success or failure, null if nothing done because user is not provisioned in Okta
   */
  public String updateOktaUser(PageContext pageContext, String oldUserName, Security newSecurityValues) {
    if (!oktaManager.isOktaEnabled()) {
      return null;
    }
    // Ensure the new user exists in Oscar DB, it should because Oscar updates already applied
    List<Security> securities = securityDao.findByUserName(oldUserName);
    if (securities == null || securities.isEmpty()) {
      logger.warn("User " + oldUserName + " does not exist.");
      return "admin.securityupdate.msgUpdateFailure";
    }

    Security existingSecurityEntity = securities.get(0);

    // if user associated with provider is not provisioned in Okta, then no updates in Okta is needed
    // null means this request was ignored because no Okta user, so behave like ordinary Oscar user
    if (!existingSecurityEntity.isProvisionedInOkta()) {
      return null;
    }

    // Do not forward Okta requests if user is Support User
    if (isKaiSupportUser(oldUserName)) {
      return null;
    }

    /*
     * Update Okta user profile that is associated with the security object This method is for an
     * admin user updating another user's attributes
     */
    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

    try {

      // create user JSON request
      Provider provider = providerDao.getProvider(existingSecurityEntity.getProviderNo());
      UserProfileAndCredentials userCreds = createUserProfileAndCredentialsObject(request, provider, newSecurityValues);
      String url = getUpdateUserUrl(request, oldUserName);
      OktaUser response = restApiClient.postRequest(url, userCreds, OktaUser.class, request);

      logger.info("updated user attributes, response=" + response);
      return "admin.securityupdate.msgUpdateSuccess";

    } catch (RestRequestException ex) {
      logger.warn("Failed to update okta account.  " + ex.getMessage());
      StatusResponse statusResponse = ex.getStatusResponse();
      sanitizeOktaResponse(statusResponse, "Failed to update user");
      return statusResponse.toUserMessage();
    } catch (Exception e) {
      logger.error("Failed to update okta account.", e);
      return "admin.securityupdate.msgUpdateFailure";
    }

  }

  /**
   * This request minimized use of additional classes for use by JSP code
   *
   * @return FAIL - Attempt to change failed, OK - successfully changed (or Okta not provisioned)
   */
  public StatusResponse changeOktaUserPassword(
      Security userSecurity,
      String currentPassword,
      String newPassword,
      HttpServletRequest request) {
    if (!oktaManager.isOktaEnabled() || userSecurity == null) {
      return StatusResponse.Ok("OK");
    }
    if (isKaiSupportUser(userSecurity.getUserName())) {
      return StatusResponse.Ok("User is support user");
    }

    try {
      if (!userSecurity.isProvisionedInOkta()) {
        return StatusResponse.Ok("OIDC user not activated");
      }
      UserCredentials userCredentials =
          new UserCredentials()
              .setUsername(userSecurity.getUserName())
              .setPassword(newPassword)
              .setOldPassword(currentPassword);
      String url = restApiClient.buildOscarProApiUrl(CHANGE_USER_PASSWORD_URL);
      return restApiClient.postRequest(url, userCredentials, StatusResponse.class, request);

    } catch (RestRequestException ex) {
      logger.warn("Failed to update okta password:  " + ex.getMessage());
      StatusResponse statusResponse = ex.getStatusResponse();
      sanitizeOktaResponse(statusResponse, "Password not changed ");
      return statusResponse;
    } catch (Exception ex) {
      logger.warn("Could not change okta user password",ex);
      return StatusResponse.Fail("Password not changed ");
    }
  }


  public StatusResponse resetOktaUserPassword(final String username, final HttpServletRequest request) {
    List<Security> securities = securityDao.findByUserName(username);
    if (securities == null || securities.isEmpty()) {
      String msg = "User " + username + " does not exist.";
      logger.warn(msg);
      throw new RuntimeException(msg);
    }
    return resetOktaUserPassword(securities.get(0), request);
  }

  public StatusResponse resetOktaUserPassword(final Security userSecurity, final HttpServletRequest request) {
    if (!oktaManager.isOktaEnabled() || userSecurity == null) {
      return StatusResponse.Ok("OK");
    }
    if (isKaiSupportUser(userSecurity.getUserName())) {
      return StatusResponse.Ok("User is support user");
    }
    // attempt to build and post reset password request for user
    try {
      if (!userSecurity.isProvisionedInOkta()) {
        return StatusResponse.Ok("OIDC user not activated");
      }
      val userCredentials = new UserCredentials()
              .setUsername(userSecurity.getUserName());
      val url = restApiClient.buildOscarProApiUrl(EXPIRE_USER_PASSWORD_URL);
      return restApiClient.postRequest(url, userCredentials, StatusResponse.class, request);
    } catch (RestRequestException ex) {
      logger.warn("Failed to expire okta password:  " + ex.getMessage());
      val statusResponse = ex.getStatusResponse();
      sanitizeOktaResponse(statusResponse, "Password not changed ");
      return statusResponse;
    } catch (Exception ex) {
      logger.warn("Could expire okta user password", ex);
      return StatusResponse.Fail("Password not changed ");
    }
  }

  /**
   * Delete a user in Okta for user specified by okta_user_id
   *
   * IMPORTANT - Assume this is called before updating the Oscar User, so no Oscar rollback is needed on failure.
   * DELETE /api/v1/users/${userId} - @see https://developer.okta.com/docs/reference/api/users/#delete-user
   * POST /api/v1/users/${userId}/
   *  - @see https://developer.okta .com/docs/reference/api/users/#deactivate-user
   * @return msg key for success or failure, null if nothing done because user is not provisioned in Okta
   */
  public StatusResponse deleteOktaUser(Security userSecurity, HttpServletRequest request) {
    if (!oktaManager.isOktaEnabled() || userSecurity == null) {
      return StatusResponse.Ok("OK");
    }
    if (isKaiSupportUser(userSecurity.getUserName())) {
      return StatusResponse.Ok("User is support user");
    }

    try {
      if (!userSecurity.isProvisionedInOkta()) {
        return StatusResponse.Ok("OIDC user not activated");
      }
      String url = restApiClient.buildOscarProApiUrl(UPDATE_USER_URL) + "/" + userSecurity.getOktaUserId();
      return restApiClient.deleteRequest(url, request);

    } catch (RestRequestException ex) {
      logger.warn("Failed to delete okta user:  " + ex.getMessage());
      StatusResponse statusResponse = ex.getStatusResponse();
      sanitizeOktaResponse(statusResponse, "User not deleted ");
      return statusResponse;
    } catch (Exception ex) {
      logger.warn("Could not delete okta user", ex);
      return StatusResponse.Fail("Could not delete okta user");
    }
  }
  // ---------------------------------------------------------- Private Methods

  /** Process add Okta user request */
  private StatusResponse processAddUserRequest(HttpServletRequest request) {
    try {
      String url = getCreateUserUrl(request);
      OktaUser newOktaUser = createOktaUserObject(request);
      OktaUser response = restApiClient.postRequest(url, newOktaUser, OktaUser.class, request);
      logger.info(" Successfully added Okta user, response=" + response);
      return StatusResponse.Ok("admin.securityaddsecurity.msgAdditionSuccess");
    } catch (RestRequestException ex) {
      StatusResponse statusResponse = ex.getStatusResponse();
      logger.warn("Failed to create okta account." + statusResponse);
      sanitizeOktaResponse(statusResponse, "Failed to create User");
      return statusResponse;
    } catch (Exception e) {
      logger.error("Failed to create okta account.", e);
      return StatusResponse.Fail("admin.securityaddsecurity.msgAdditionFailure");
    }
  }

  private boolean isKaiSupportUser(String username) {
    String kaiSupportUser = systemPreferencesDao.getPreferenceValueByName("kai_username", null);
    boolean isKaiSupport = kaiSupportUser != null && kaiSupportUser.equals(username);
    return isKaiSupport;
  }

  /**
   * Update Okta user profile information for the user associated with the given provider
   *
   * @param pageContext page context
   * @param updatedProvider updated provider
   * @return message key
   */
  private String processUpdateUserRequest(PageContext pageContext, Security security, Provider updatedProvider) {
    HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
    try {

      // null means this request was ignored because no Okta user, so behave like ordinary Oscar user
      if (!security.isProvisionedInOkta()) {
        return null;
      }

      UserProfileAndCredentials userCreds = createUserProfileAndCredentialsObject(request, security, updatedProvider);
      // the pre-existing username is needed for the Okta request
      String url = getUpdateUserUrl(request, security.getUserName());
      StatusResponse response = restApiClient.postRequest(url, userCreds, StatusResponse.class, request);
      String entity = response.getResponseJson();

      logger.info("entity=" + entity);
      return processUpdateProviderResponse(entity, request);

    } catch (RestRequestException ex) {
      logger.warn("Failed to update okta account." + ex.getMessage());
      return processAddUserResponse(ex.getStatusResponse().getResponseJson(), request);
    } catch (Exception e) {
      logger.error("Failed to update okta account.", e);
      return "admin.providerupdate.msgUpdateFailure";
    }
  }


  /**
   * Create Okta user JSON string
   *
   * @param request servlet request
   * @return Okta user
   */
  private OktaUser createOktaUserObject(ServletRequest request) {

    String username = request.getParameter("user_name");
    List<Security> securities = securityDao.findByUserName(username);
    if (securities == null || securities.isEmpty()) {
      String msg = "User " + username + " does not exist.";
      logger.warn(msg);
      throw new RuntimeException(msg);
    }

    // retrieve provider details
    String providerNo = request.getParameter("provider_no");
    Provider provider = providerDao.getProvider(providerNo);
    if (provider == null) {
      String msg = "Provider " + providerNo + " does not exist.";
      logger.warn(msg);
      throw new RuntimeException(msg);
    }

    OktaUser oktaUser = new OktaUser();
    OktaUser.Profile profile = new OktaUser.Profile();
    profile.setLogin(username);
    profile.setFirstName(provider.getFirstName());
    profile.setLastName(provider.getLastName());
    profile.setMobilePhone(StringUtils.isNotBlank(provider.getPhone()) ? provider.getPhone() : provider.getWorkPhone());
    if (StringUtils.isNotBlank(provider.getEmail())) {
      profile.setEmail(provider.getEmail());
    }
    oktaUser.setProfile(profile);
    OktaUser.Password password = new OktaUser.Password();
    password.setValue(request.getParameter("password"));
    OktaUser.Credentials credentials = new OktaUser.Credentials();
    credentials.setPassword(password);
    oktaUser.setCredentials(credentials);
    return oktaUser;
  }

  /**
   * Create Okta user JSON string
   *
   * @param request servlet request
   * @return JSON string for Okta user
   */
  private UserProfileAndCredentials createUserProfileAndCredentialsObject(
      ServletRequest request, Security security, Provider provider) {

    // create user profile and credentials user
    UserProfileAndCredentials profileCredentials = new UserProfileAndCredentials();
    OktaUser.Profile profile = new OktaUser.Profile();
    profile.setLogin(security.getUserName());
    profile.setFirstName(provider.getFirstName());
    profile.setLastName(provider.getLastName());

    if (StringUtils.isNotBlank(provider.getEmail())) {
      profile.setEmail(provider.getEmail());
    }
    if (StringUtils.isNotBlank(provider.getPhone())) {
      profile.setMobilePhone(provider.getPhone());
    } else if (StringUtils.isNotBlank(provider.getWorkPhone())) {
        profile.setMobilePhone(provider.getWorkPhone());
    }
    if (StringUtils.isNotBlank(provider.getWorkPhone())) {
      profile.setPrimaryPhone(provider.getWorkPhone());
    }

    profileCredentials.setProfile(profile);
    return profileCredentials;
  }

  /**
   * Create Okta user JSON string
   *
   * @param request servlet request
   * @return JSON string for Okta user
   */
  private UserProfileAndCredentials createUserProfileAndCredentialsObject(
      ServletRequest request, Provider provider, Security security) {
    // create user profile and credentials user
    UserProfileAndCredentials profileCredentials = new UserProfileAndCredentials();
    OktaUser.Profile profile = new OktaUser.Profile();
    profile.setFirstName(provider.getFirstName());
    profile.setLastName(provider.getLastName());
    profile.setLogin(security.getUserName());
    profileCredentials.setProfile(profile);
    if (StringUtils.isNotBlank(security.getPassword())) {
      OktaUser.Credentials credentials = new OktaUser.Credentials();
      OktaUser.Password password = new OktaUser.Password();
      password.setValue(security.getPassword());
      credentials.setPassword(password);
      profileCredentials.setCredentials(credentials);
    }
    return profileCredentials;
  }

  /** Get create user url */
  private String getCreateUserUrl(ServletRequest request) {
    return restApiClient.buildOscarProApiUrl(CREATE_USER_URL);
  }

  /** Get update user url */
  private String getUpdateUserUrl(ServletRequest request, String username)
      throws UnsupportedEncodingException {
    return restApiClient.buildOscarProApiUrl(UPDATE_USER_URL + "/" + URLEncoder.encode(username, "UTF-8"));
  }

  /**
   * Process add Okta user response
   *
   * @param entity response JSON string
   * @param request servlet request
   * @return message key
   */
  private String processAddUserResponse(String entity, ServletRequest request) {

    try {
      if (entity == null) {
        return "admin.securityaddsecurity.msgAdditionFailure";
      }
      JSONObject object = new JSONObject(entity);
      logger.info("object=" + object.toString());

      // use error field to decide if it is an error response
      if (object.has("error")) {
        if (object.has("message")) {
          String message = (String) object.get("message");
          if (StringUtils.isNotBlank(message)
              && message.toLowerCase().contains(OBJECT_ALREADY_EXIST_MESSAGE.toLowerCase())) {
            // user already exists in Okta, remove newly created user from OSCAR
            String username = request.getParameter("user_name");
            removeOscarUserByUsername(username);
            return "admin.securityaddsecurity.msgAdditionFailureDuplicate";
          } else {
            return "admin.securityaddsecurity.msgAdditionFailure";
          }
        } else {
          return "admin.securityaddsecurity.msgAdditionFailure";
        }
      } else {
        return "admin.securityaddsecurity.msgAdditionSuccess";
      }
    } catch (JSONException e) {
      return "admin.securityaddsecurity.msgAdditionFailure";
    }
  }

  private void removeOscarUserByUsername(String username) {
    List<Security> securities = securityDao.findByUserName(username);
    if (securities == null || securities.isEmpty()) {
      String msg = "User " + username + " does not exist.";
      logger.warn(msg);
      throw new RuntimeException(msg);
    }
    Security user = securities.get(0);
    logger.info("Rollback Oscar User: " + username + " : " + user);
    securityDao.remove(user.getId());
  }

  /**
   * Process update Okta user profile response
   *
   * @param entity response JSON string
   * @param request servlet request
   * @return message key
   */
  private String processUpdateProviderResponse(String entity, ServletRequest request) {

    try {
      JSONObject object = new JSONObject(entity);
      logger.info("object=" + object.toString());

      // use error field to decide if it is an error response
      if (object.has("error")) {
        return "admin.providerupdate.msgUpdateFailure";
      } else {
        return "admin.providerupdate.msgUpdateSuccess";
      }
    } catch (JSONException e) {
      return "admin.providerupdate.msgUpdateFailure";
    }
  }

  private void sanitizeOktaResponse(StatusResponse statusResponse, String message) {
    if (statusResponse != null && statusResponse.getException() != null) {
      switch (statusResponse.getException()) {
        case "OktaApiValidationException":
          if (statusResponse
              .getError()
              .startsWith("password: Password requirements were not met")) {
            statusResponse.setMessage(message + ": " + statusResponse.getError().substring(9));
          }
          break;
        case "ObjectAlreadyExistException":
          statusResponse.setMessage(message + ": Username already exists in OIDC Service");
          break;
        default:
          statusResponse.setMessage(message + " : " + statusResponse.toUserMessage());
      }
      statusResponse.setMessage(message + " : " + statusResponse.toUserMessage());
    }
  }

  /**
   * If this message matches a key in MessagesResources, then lookup and return the message.
   * Otherwise, assume the param is just a message and return that.
   *
   * @param messageOrKey a message resource key, or just a message
   * @return
   */
  public String resolveMessage(String messageOrKey) {
    MessageResources resources = MessageResources.getMessageResources("ApplicationResources");
    MessageResources messages = MessageResources.getMessageResources("org.apache.struts.taglib.bean.LocalStrings");
    ResourceBundle rb = ResourceBundle.getBundle("oscarResources");

    if (rb.containsKey(messageOrKey)) {
      return rb.getString(messageOrKey);
    } else if (messages.isPresent(messageOrKey)) {
      return messages.getMessage(messageOrKey);
    } else if (resources.isPresent(messageOrKey)) {
      return resources.getMessage(messageOrKey);
    } else {
      return messageOrKey;
    }
  }


  /**
   * Check if any synced provider attributes have changed. The following attributes are synced
   * between OSCAR and Okta
   *
   * <p>- first name - last name - email and - cell phone number (cell phone is stored in comments
   * as <xml_p_cell>cell phone number</xml_p_cell>
   */
  private boolean syncedProviderAttributesChanged(Provider updatedProvider, ProviderArchive archivedProvider) {
    return (!StringUtils.equals(updatedProvider.getFirstName(), archivedProvider.getFirstName())
        || !StringUtils.equals(updatedProvider.getLastName(), archivedProvider.getLastName())
        || !StringUtils.equals(updatedProvider.getEmail(), archivedProvider.getEmail())
        || !StringUtils.equals(getCellPhone(updatedProvider.getComments()), getCellPhone(archivedProvider.getComments())));
  }

  private String getCellPhone(String comments) {
    return "";
  }
}
