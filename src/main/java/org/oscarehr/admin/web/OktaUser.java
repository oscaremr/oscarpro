/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.admin.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.List;

/**
 * Sample JSON (returned from GET users/:userid
 *
 * <p>{ "id": "00usloaiv8j4TZNoq1d6", "status": "ACTIVE", "created": "2021-06-04T06:21:13.000Z",
 * "activated": "2021-06-04T06:21:14.000Z", "statusChanged": "2021-06-04T06:21:14.000Z",
 * "lastLogin": null, "lastUpdated": "2021-06-04T06:21:14.000Z", "passwordChanged":
 * "2021-06-04T06:21:14.000Z", "type": { "id": "otyslbzhbVLXu6ZNf1d6" }, "profile": { "firstName":
 * "Dr", "lastName": "Doolittle", "mobilePhone": null, "secondEmail": null, "login": "dolittle",
 * "email": "ima.hack@hackers.com" }, "credentials": { "password": {}, "provider": { "type": "OKTA",
 * "name": "OKTA" } }, "_links": { } }
 *
 * <p>or
 *
 * <p>{"id":"00u1c18po12GQvDpJ1d7","status":"ACTIVE","created":"2021-09-08T07:53:16.000Z","activated":"2021-09-08T07:53:16.000Z",
 * "statusChanged":"2021-09-08T07:53:16.000Z",
 * "profile":{"firstName":"Ima","lastName":"Mocker","email":"chuck.bc.ca@gmail.com","login":"imamock2"},
 * "type":{"id":"otyslbzhbVLXu6ZNf1d6"}, "credentials":{"password":{},
 * "provider":{"question":null,"answer":null}},
 * "groupIds":[]}
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OktaUser {

  private String id;
  private String status;
  private String created;
  private String activated;
  private String statusChanged;
  private Profile profile;
  private Type type;
  private Credentials credentials;
  private List<String> groupIds = new ArrayList<>();


  /**
   * For most OSCAR users, must set user.type to be a type that does not require email format for
   * username. The following usertype was created for this purpose. It is the userType.id that must
   * be specified in the user.type field. { "id": "otyslbzhbVLXu6ZNf1d6", "displayName": "OSCAR",
   * "name": "oscar",
   */

  // region ///  getters/setters  /////////////////////////////////////
  public OktaUser() {
    super();
  }

  public String getId() {
    return id;
  }

  public OktaUser setId(String id) {
    this.id = id;
    return this;
  }

  public Type getType() {
    return type;
  }

  public OktaUser setType(Type type) {
    this.type = type;
    return this;
  }

  public String getStatus() {
    return status;
  }

  public OktaUser setStatus(String status) {
    this.status = status;
    return this;
  }

  public String getCreated() {
    return created;
  }

  public OktaUser setCreated(String created) {
    this.created = created;
    return this;
  }

  public String getActivated() {
    return activated;
  }

  public OktaUser setActivated(String activated) {
    this.activated = activated;
    return this;
  }

  public String getStatusChanged() {
    return statusChanged;
  }

  public OktaUser setStatusChanged(String statusChanged) {
    this.statusChanged = statusChanged;
    return this;
  }

  public Profile getProfile() {
    return profile;
  }

  public OktaUser setProfile(Profile profile) {
    this.profile = profile;
    return this;
  }

  public Credentials getCredentials() {
    return credentials;
  }

  public OktaUser setCredentials(Credentials credentials) {
    this.credentials = credentials;
    return this;
  }

  public List<String> getGroupIds() {
    return groupIds;
  }

  public OktaUser setGroupIds(List<String> groupIds) {
    this.groupIds = groupIds;
    return this;
  }

  // endregion ///  getters/setters  /////////////////////////////////////

  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class Profile {
    String firstName;
    String lastName;
    String email;
    String login;
    String mobilePhone;

    String primaryPhone;

    public Profile() {}

    public String getFirstName() {
      return firstName;
    }

    public Profile setFirstName(String firstName) {
      this.firstName = firstName;
      return this;
    }

    public String getMobilePhone() {
      return mobilePhone;
    }

    public Profile setMobilePhone(String mobilePhone) {
      this.mobilePhone = mobilePhone;
      return this;
    }

    public String getPrimaryPhone() {
      return primaryPhone;
    }

    public Profile setPrimaryPhone(String primaryPhone) {
      this.primaryPhone = primaryPhone;
      return this;
    }

    public String getLastName() {
      return lastName;
    }

    public Profile setLastName(String lastName) {
      this.lastName = lastName;
      return this;
    }

    public String getEmail() {
      return email;
    }

    public Profile setEmail(String email) {
      this.email = email;
      return this;
    }

    public String getLogin() {
      return login;
    }

    public Profile setLogin(String login) {
      this.login = login;
      return this;
    }

    @Override
    public String toString() {
      return "{" +
          "firstName='" + firstName +
          "', lastName='" + lastName +
          "', email='" + email +
          "', login='" + login +
          "'}";
    }

  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class Credentials {
    private Password password;
    private Provider provider;

    public Credentials() {}

    public Credentials(String passwordText) {
      this.password = new Password(passwordText);
    }

    public Password getPassword() {
      return password;
    }

    public Credentials setPassword(Password password) {
      this.password = password;
      return this;
    }

    public Provider getProvider() {
      return provider;
    }

    public Credentials setProvider(Provider provider) {
      this.provider = provider;
      return this;
    }
  }

  /**
   * sometimes within Credentials
   * "provider":{"question":null,"answer":null}
   */
  @JsonIgnoreProperties(ignoreUnknown = true)
  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static class Provider {
    private String question;
    private String answer;

    public Provider() {
    }

    public String getQuestion() {
      return question;
    }

    public Provider setQuestion(String question) {
      this.question = question;
      return this;
    }

    public String getAnswer() {
      return answer;
    }

    public Provider setAnswer(String answer) {
      this.answer = answer;
      return this;
    }
  }

  public static class Password {
    private String value;

    public Password() {}

    public Password(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    public void setValue(String value) {
      this.value = value;
    }
  }

  public static class Type {
    private String id;

    public Type(String id) {
      this.id = id;
    }

    public Type() {}

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }
  }

  @Override
  public String toString() {
    return "OktaUser{" +
        "id='" + id +
        "', status='" + status +
        "', activated='" + activated +
        "', profile=" + profile +
        "}";
  }
}
