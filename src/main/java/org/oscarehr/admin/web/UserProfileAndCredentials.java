/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.admin.web;

public class UserProfileAndCredentials {

  private OktaUser.Profile profile;
  private OktaUser.Credentials credentials;

  public OktaUser.Profile getProfile() {
    return profile;
  }

  public void setProfile(OktaUser.Profile profile) {
    this.profile = profile;
  }

  public OktaUser.Credentials getCredentials() {
    return credentials;
  }

  public void setCredentials(OktaUser.Credentials credentials) {
    this.credentials = credentials;
  }
}
