/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.demographic.merge;

public class DemographicTableIndex {
    
    public DemographicTableIndex(String name, String demographicColumn, String primaryKeyColumn, String primaryKeyType) {
        this.name = name;
        this.demographicColumn = demographicColumn;
        this.primaryKeyColumn = primaryKeyColumn;
        this.primaryKeyType = primaryKeyType;
    }
    public DemographicTableIndex(String name, String demographicColumn, String primaryKeyColumn, String primaryKeyType, String additionalWhereClause) {
        this.name = name;
        this.demographicColumn = demographicColumn;
        this.primaryKeyColumn = primaryKeyColumn;
        this.primaryKeyType = primaryKeyType;
        this.additionalWhereClause = additionalWhereClause;
    }
    
    private String name;
    private String demographicColumn;
    private String primaryKeyColumn;
    private String primaryKeyType;
    // Used in cases where demographic link take uses composite key, for example ctl_document
    // table where module = 'demographic' must be specified alongside the the module_d = :demographicNo
    private String additionalWhereClause = "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDemographicColumn() {
        return demographicColumn;
    }

    public void setDemographicColumn(String demographicColumn) {
        this.demographicColumn = demographicColumn;
    }

    public String getPrimaryKeyColumn() {
        return primaryKeyColumn;
    }

    public void setPrimaryKeyColumn(String primaryKeyColumn) {
        this.primaryKeyColumn = primaryKeyColumn;
    }

    public String getPrimaryKeyType() {
        return primaryKeyType;
    }

    public void setPrimaryKeyType(String primaryKeyType) {
        this.primaryKeyType = primaryKeyType;
    }

    public String getAdditionalWhereClause() {
        return additionalWhereClause;
    }

    public void setAdditionalWhereClause(String additionalWhereClause) {
        this.additionalWhereClause = additionalWhereClause;
    }
}