/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.demographic.merge;

import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.oscarehr.casemgmt.dao.CaseManagementIssueDAO;
import org.oscarehr.casemgmt.model.CaseManagementIssue;
import org.oscarehr.common.dao.AbstractDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.util.SpringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@Repository
public class DemographicMergeOperationDao extends AbstractDao<DemographicMergeOperation> {

  public static final String REFERRAL_PHYSICIAN_TYPE = "referral_physician";
  public static final String FAMILY_PHYSICIAN_TYPE = "family_physician";

    @Autowired private DemographicDao demographicDao;

    public DemographicMergeOperationDao() {
        super(DemographicMergeOperation.class);
    }
    
    public void mergeDemographicWithSql(Integer oldDemographic, Integer mainDemographic, String providerNo, String contentType, String idSql, String updateSql) {
        Query query = entityManager.createNativeQuery(idSql);
        query.setParameter("oldDemo", oldDemographic);
        List<Object> idList = query.getResultList();

        for (Object id : idList) {
            persist(new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), contentType, id.toString()));
        }

        query = entityManager.createNativeQuery(updateSql);
        query.setParameter("oldDemo", oldDemographic);
        query.setParameter("mainDemo", mainDemographic);
        query.executeUpdate();
    }
    
    public void mergeDemographicForCaseMgmtIssue(Integer oldDemographic, Integer mainDemographic, String providerNo) {
        String idSql = "SELECT id FROM casemgmt_issue WHERE demographic_no = :oldDemo";
        String updateSql = "update casemgmt_issue set demographic_no = :mainDemo where id IN (:idList)";
        Query query = entityManager.createNativeQuery(idSql);
        query.setParameter("oldDemo", oldDemographic);
        List<Integer> idList = query.getResultList();
        CaseManagementIssueDAO caseManagementIssueDAO = SpringUtils.getBean(CaseManagementIssueDAO.class);

        for (Integer id : idList) {
            CaseManagementIssue caseManagementIssue = caseManagementIssueDAO.getIssuebyIssueCode(mainDemographic.toString(), id.toString());
            if (caseManagementIssue == null) {
                persist(new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), "casemgmt_issue", id.toString()));
            } else {
                idList.remove(id);
            }
        }
        
        if (!idList.isEmpty()) {
            query = entityManager.createNativeQuery(updateSql);
            query.setParameter("idList", idList);
            query.setParameter("mainDemo", mainDemographic);
            query.executeUpdate();
        }
    }

    public void mergeDemographic(Integer oldDemographic, Integer mainDemographic, String providerNo, DemographicTableIndex demographicTableIndex, String databaseName) {
        String dbName = ("progress_sheet".equals(demographicTableIndex.getName()) ? "progress_sheet" : databaseName);
        String checkSql = "SELECT count(*) "
            + "FROM information_schema.TABLES "
            + "WHERE (TABLE_NAME = '" + demographicTableIndex.getName() + "') AND " 
            + "(TABLE_SCHEMA = '" + dbName + "')";
        Query query = entityManager.createNativeQuery(checkSql);
        if (!query.getSingleResult().equals(BigInteger.ZERO)) { 
            
            boolean skipUpdate = false;
            // some items do not need to overwrite if they already exist in the mainDemo
            if ("alert".equals(demographicTableIndex.getName())) {
                String entryCheckSql = "SELECT " + demographicTableIndex.getPrimaryKeyColumn() + " FROM " + dbName + "." + demographicTableIndex.getName() + " WHERE " + demographicTableIndex.getDemographicColumn() + " = :mainDemo";
                query = entityManager.createNativeQuery(entryCheckSql);
                query.setParameter("mainDemo", mainDemographic);
                if (!query.getResultList().isEmpty()) {
                    skipUpdate = true;
                }
            }            
            String idSql = "SELECT " + demographicTableIndex.getPrimaryKeyColumn() + " FROM " + dbName + "." + demographicTableIndex.getName() + " WHERE " + demographicTableIndex.getDemographicColumn() + " = :oldDemo";
            String updateSql = "update " + dbName + "." + demographicTableIndex.getName() + " set " + demographicTableIndex.getDemographicColumn() + " = :mainDemo where " + demographicTableIndex.getDemographicColumn() + " = :oldDemo";
            if (!demographicTableIndex.getAdditionalWhereClause().isEmpty()) {
                idSql += " AND " + demographicTableIndex.getAdditionalWhereClause();
                updateSql += " AND " + demographicTableIndex.getAdditionalWhereClause();
            }
            query = entityManager.createNativeQuery(idSql);
            query.setParameter("oldDemo", oldDemographic);
            List<Object> idList = query.getResultList();
            
            if (!idList.isEmpty() && !skipUpdate) {
                for (Object id : idList) {
                    persist(new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), demographicTableIndex.getName(), id.toString()));
                }
                
                query = entityManager.createNativeQuery(updateSql);
                query.setParameter("oldDemo", oldDemographic);
                query.setParameter("mainDemo", mainDemographic);
                query.executeUpdate();
            }
        }
    }

    public void unMergeDemographic(Integer oldDemographic, String id, String tableName, String tableDemographicName, String tableIdName, String tableIdType, String additionalWhereClause, String databaseName) {
        String dbName = ("progress_sheet".equals(tableName) ? "progress_sheet" : databaseName);
        String updateSql = "update " + dbName + "." + tableName + " set " + tableDemographicName + " = :oldDemo where " + tableIdName + " = :id";
        if (!additionalWhereClause.isEmpty()) {
            updateSql += " AND " + additionalWhereClause;
        }
        Query query = entityManager.createNativeQuery(updateSql);
        query.setParameter("oldDemo", oldDemographic);
        switch (tableIdType) {
            case "int":
                query.setParameter("id", Integer.parseInt(id));
                break;
            case "bigint":
                query.setParameter("id", new BigInteger(id));
                break;
            default:
                query.setParameter("id", id);
                break;
        }
        query.executeUpdate();
    }
    public void unMergeDemographic(Integer oldDemographic, String id, String tableName, String tableDemographicName, String tableIdName, String tableIdType, String databaseName) {
        unMergeDemographic(oldDemographic, id, tableName, tableDemographicName, tableIdName, tableIdType, "", databaseName);
    }
    
    public List<DemographicMergeOperation> findAllByOldDemographicAndMainDemographic(Integer oldDemographic, Integer mainDemographic) {
        Query query = entityManager.createQuery("SELECT d FROM DemographicMergeOperation d WHERE d.oldDemographic = :old and d.mainDemographic = :main");
        query.setParameter("old", oldDemographic);
        query.setParameter("main", mainDemographic);
        return query.getResultList();
    }
    
    public void updateDemographic(Integer oldDemographic, Integer mainDemographic, String providerNo) {
        Query query = entityManager.createNativeQuery("SELECT hin FROM demographic WHERE demographic_no = ?1");
        query.setParameter(1, oldDemographic);
        String oldDemoHIN = (String)query.getSingleResult();
        query.setParameter(1, mainDemographic);
        String mainDemoHIN = (String)query.getSingleResult();

        query = entityManager.createNativeQuery("UPDATE demographic SET hin = ?1 WHERE demographic_no = ?2");
        if (StringUtils.isEmpty(mainDemoHIN)) {
            query.setParameter(1, oldDemoHIN);
            query.setParameter(2, mainDemographic);
            query.executeUpdate();
            persist(new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), "Demographic HIN Main", mainDemoHIN));
        }
        query.setParameter(1, "");
        query.setParameter(2, oldDemographic);
        query.executeUpdate();
        persist(new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), "Demographic HIN Old", oldDemoHIN));
        
        query = entityManager.createNativeQuery("SELECT patient_status FROM demographic WHERE demographic_no = :oldDemo");
        query.setParameter("oldDemo", oldDemographic);
        String oldPatientStatus = (String)query.getSingleResult();
        
        query = entityManager.createNativeQuery("update demographic set patient_status='MERGED' where demographic_no = :oldDemo");
        query.setParameter("oldDemo", oldDemographic);
        query.executeUpdate();
        persist(new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), "Demographic Patient Status", oldPatientStatus));
    }
    
    public void revertUpdateDemographic(Integer oldDemographic, Integer mainDemographic, String providerNo) {
        //Check if the HIN was moved first by looking in the demographic merge operations for a regex
        Query query = entityManager.createQuery("SELECT d FROM DemographicMergeOperation d WHERE " +
            "d.oldDemographic = :oldDemo AND " +  
            "d.mainDemographic = :mainDemo AND " +
            "(d.contentType LIKE 'Demographic HIN%' OR d.contentType = 'Demographic Patient Status')");
        query.setParameter("oldDemo", oldDemographic);
        query.setParameter("mainDemo", mainDemographic);
        List<DemographicMergeOperation> demographicMergeOperationList = query.getResultList();

        for (DemographicMergeOperation demographicMergeOperation : demographicMergeOperationList) {
            switch (demographicMergeOperation.getContentType()) {
                case "Demographic Patient Status":
                    updateDemographicField(oldDemographic, demographicMergeOperation.getContentID(), "patient_status");
                    break;
                case "Demographic HIN Old":
                    updateDemographicField(oldDemographic, demographicMergeOperation.getContentID(), "hin");
                    break;
                case "Demographic HIN Version Old":
                    updateDemographicField(oldDemographic, demographicMergeOperation.getContentID(), "ver");
                    break;
                case "Demographic HIN Main":
                    updateDemographicField(mainDemographic, "", "hin");
                    break;
                case "Demographic HIN Version Main":
                    updateDemographicField(mainDemographic, "", "ver");
                    break;
                case "Demographic Phone Main":
                    updateDemographicField(mainDemographic, demographicMergeOperation.getContentID(), "phone");
                    break;
                case "Demographic Phone2 Main":
                    updateDemographicField(mainDemographic, demographicMergeOperation.getContentID(), "phone2");
                    break;
                case "Demographic family_doctor Main":
                    updateDemographicPhysicianExtension(mainDemographic, demographicMergeOperation.getContentID(), REFERRAL_PHYSICIAN_TYPE);
                    break;
                case "Demographic family_physician Main":
                    updateDemographicPhysicianExtension(mainDemographic, demographicMergeOperation.getContentID(), FAMILY_PHYSICIAN_TYPE);
                    break;
            }
        }
    }
    
    public void updateDemographicField(Integer demographicId, String contentId, String fieldToUpdate) {
        Query query = entityManager.createNativeQuery("update demographic set " + fieldToUpdate + " = :content where demographic_no = :mainDemo");
        query.setParameter("mainDemo", demographicId);
        query.setParameter("content", contentId);
        query.executeUpdate();
    }

  /**
   * Reverts the merge of the demographic physician name/ohip extension value of given demographic
   * number. The content is in the format of "physicianName,physicianOhip"
   * as it is set in {@link DemographicMergeRecordAction#mergeDemographics(Integer, Integer, String)}.
   *
   * @param demographicNumber number of the demographic to revert the merge for
   * @param content           value of physician name/ohip to be reverted to
   * @param physicianType     type of physician to revert the merge for
   */
  protected void updateDemographicPhysicianExtension(
      final Integer demographicNumber,
      final String content,
      final String physicianType
  ) {
    if (StringUtils.isEmpty(content)) {
      return;
    }
    val demographic = demographicDao.getDemographic(demographicNumber);
    if (demographic == null) {
      return;
    }
    val contentItems = content.split(",");
    if (contentItems.length < 2) {
      return;
    }
    val physicianName = contentItems[0] + "," + contentItems[1];
    val physicianOhip = (contentItems.length == 3) ? contentItems[2] : "";

    if (REFERRAL_PHYSICIAN_TYPE.equals(physicianType)) {
      demographic.setReferralPhysicianName(physicianName);
      if (StringUtils.isNotBlank(physicianOhip)) {
        demographic.setReferralPhysicianOhip(physicianOhip);
      }
    } else if (FAMILY_PHYSICIAN_TYPE.equals(physicianType)) {
      demographic.setFamilyPhysicianName(physicianName);
      if (StringUtils.isNotBlank(physicianOhip)) {
        demographic.setFamilyPhysicianOhip(physicianOhip);
      }
    }
  }
}
