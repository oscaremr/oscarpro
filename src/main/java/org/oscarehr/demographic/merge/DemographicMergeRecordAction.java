/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


/*
 * DemographicMergeRecordAction.java
 *
 * Created on September 11, 2007, 3:52 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package org.oscarehr.demographic.merge;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.managers.DemographicManager;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import oscar.oscarDemographic.data.DemographicMerged;

/**
 *
 * @author wrighd
 */
public class DemographicMergeRecordAction extends Action {

    Logger logger = Logger.getLogger(DemographicMergeRecordAction.class);
    private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
    
    private List<DemographicTableIndex> demographicTableIndexList = Arrays.asList(
            new DemographicTableIndex("alert", "demographic_no", "id", "int"),
            new DemographicTableIndex("allergies", "demographic_no", "allergyid", "int"),
            new DemographicTableIndex("appointment", "demographic_no", "appointment_no", "int"),
            new DemographicTableIndex("appointmentArchive", "demographic_no", "id", "int"),
            new DemographicTableIndex("batch_billing", "demographic_no", "id", "int"),
            new DemographicTableIndex("bed_demographic", "demographic_no", "bed_id", "int"),
            new DemographicTableIndex("bed_demographic_historical", "demographic_no", "bed_id", "int"),
            new DemographicTableIndex("billing", "demographic_no", "billing_no", "int"),
            new DemographicTableIndex("billinginr", "demographic_no", "billinginr_no", "int"),
            new DemographicTableIndex("billingmaster", "demographic_no", "billingmaster_no", "int"),
            new DemographicTableIndex("billing_on_cheader1", "demographic_no", "id", "int"),
            new DemographicTableIndex("billing_on_ext", "demographic_no", "id", "int"),
            new DemographicTableIndex("billing_on_transaction", "demographic_no", "id", "int"),
            new DemographicTableIndex("casemgmt_cpp", "demographic_no", "id", "varchar"),
            new DemographicTableIndex("casemgmt_note", "demographic_no", "note_id", "int"),
            new DemographicTableIndex("casemgmt_note_lock", "demographic_no", "id", "int"),
            new DemographicTableIndex("casemgmt_tmpsave", "demographic_no", "id", "bigint"),
            new DemographicTableIndex("client_image", "demographic_no", "image_id", "int"),
            new DemographicTableIndex("clinician_communication", "demographic_no", "id", "int"),
            new DemographicTableIndex("clinician_communication_draft", "demographic_no", "id", "int"),
            new DemographicTableIndex("Consent", "demographic_no", "id", "int"),
            new DemographicTableIndex("consultationRequests", "demographicNo", "requestId", "int"),
            new DemographicTableIndex("consultationResponse", "demographicNo", "responseId", "int"),
            new DemographicTableIndex("consultation_requests_archive", "demographic_id", "id", "int"),
            new DemographicTableIndex("custom_filter", "demographic_no", "id", "varchar"),
            new DemographicTableIndex("demographicArchive", "demographic_no", "id", "int"),
            new DemographicTableIndex("DemographicContact", "demographicNo", "id", "int"),
            new DemographicTableIndex("demographicPharmacy", "demographic_no", "id", "int"),
            new DemographicTableIndex("demographicSets", "demographic_no", "id", "int"),
            new DemographicTableIndex("demographicstudy", "demographic_no", "study_no", "int"),
            new DemographicTableIndex("demographic_group_link", "demographic_no", "demographic_group_id", "int"),
            new DemographicTableIndex("demographic_merged", "demographic_no", "id", "int"),
            new DemographicTableIndex("desannualreviewplan", "demographic_no", "des_no", "int"),
            new DemographicTableIndex("desaprisk", "demographic_no", "desaprisk_no", "int"),
            new DemographicTableIndex("DigitalSignature", "demographicId", "id", "int"),
            new DemographicTableIndex("diseases", "demographic_no", "diseaseid", "int"),
            new DemographicTableIndex("doc_manager", "demographic_no", "id", "int"),
            new DemographicTableIndex("drugReason", "demographicNo", "id", "int"),
            new DemographicTableIndex("drugs", "demographic_no", "drugid", "int"),
            new DemographicTableIndex("dxresearch", "demographic_no", "dxresearch_no", "int"),
            new DemographicTableIndex("eChart", "demographicNo", "eChartId", "int"),
            new DemographicTableIndex("eform_data", "demographic_no", "fdid", "int"),
            new DemographicTableIndex("eform_values", "demographic_no", "id", "int"),
            new DemographicTableIndex("encounter", "demographic_no", "encounter_no", "int"),
            new DemographicTableIndex("Episode", "demographicNo", "id", "int"),
            new DemographicTableIndex("erefer_attachment", "demographic_no", "id", "int"),
            new DemographicTableIndex("erx_task", "demographic_no", "id", "int"),
            new DemographicTableIndex("EyeformConsultationReport", "demographicNo", "id", "int"),
            new DemographicTableIndex("EyeformFollowUp", "demographic_no", "id", "int"),
            new DemographicTableIndex("EyeformOcularProcedure", "demographicNo", "id", "int"),
            new DemographicTableIndex("EyeformProcedureBook", "demographic_no", "id", "int"),
            new DemographicTableIndex("EyeformSpecsHistory", "demographicNo", "id", "int"),
            new DemographicTableIndex("EyeformTestBook", "demographic_no", "id", "int"),
            new DemographicTableIndex("faxes", "demographicNo", "id", "int"),
            new DemographicTableIndex("flowsheet_customization", "demographic_no", "id", "int"),
            new DemographicTableIndex("flowsheet_drug", "demographic_no", "id", "int"),
            new DemographicTableIndex("flowsheet_dx", "demographic_no", "id", "int"),
            new DemographicTableIndex("form", "demographic_no", "form_no", "int"),
            new DemographicTableIndex("form2MinWalk", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formAdf", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formAdfV2", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formAlpha", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formAnnual", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formAnnualV2", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formAR", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formBCAR", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formBCAR2007", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formBCAR2012", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formBCAR2020", "demographic_no", "id", "int"),
            new DemographicTableIndex("formBCBirthSumMo", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formBCBirthSumMo2008", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formBCClientChartChecklist", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formBCHP", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formBCINR", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formBCNewBorn", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formBCNewBorn2008", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formBPMH", "demographic_no", "id", "int"),
            new DemographicTableIndex("formCaregiver", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formCESD", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formchf", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formConsult", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formConsultLetter", "demographic_no", "id", "int"),
            new DemographicTableIndex("formCostQuestionnaire", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formCounseling", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formDischargeSummary", "demographic_no", "id", "bigint"),
            new DemographicTableIndex("formFalls", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formfollowup", "demographic_no", "ID", "bigint"),
            new DemographicTableIndex("formGripStrength", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formGrowth0_36", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formGrowthChart", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formgyane", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formHomeFalls", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formImmunAllergy", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formIntakeHx", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formIntakeInfo", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formInternetAccess", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formLabReq", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formLabReq07", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formLabReq10", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formLateLifeFDIDisability", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formLateLifeFDIFunction", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formMentalHealth", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formMentalHealthForm1", "demographic_no", "id", "bigint"),
            new DemographicTableIndex("formMentalHealthForm14", "demographic_no", "id", "bigint"),
            new DemographicTableIndex("formMentalHealthForm42", "demographic_no", "id", "bigint"),
            new DemographicTableIndex("formMMSE", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formNoShowPolicy", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formONAR", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formONAR2017", "demographic_no", "id", "int"),
            new DemographicTableIndex("formONAREnhanced", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formONAREnhancedRecord", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formovulation", "demographic_no", "ID", "bigint"),
            new DemographicTableIndex("formPalliativeCare", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formPeriMenopausal", "demographic_no", "ID", "int"),
            new DemographicTableIndex("form_on_perinatal_2017", "demographic_no", "id", "int"),
            new DemographicTableIndex("formPositionHazard", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formreceptionassessment", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formRhImmuneGlobulin", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formRourke", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formRourke2006", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formRourke2009", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formRourke2017", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formSatisfactionScale", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formSelfAdministered", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formSelfAssessment", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formSelfEfficacy", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formSelfManagement", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formSF36", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formSF36Caregiver", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formTreatmentPref", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formType2Diabetes", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formVTForm", "demographic_no", "ID", "int"),
            new DemographicTableIndex("formWCB", "demographic_no", "ID", "int"),
            new DemographicTableIndex("form_drawing_tool", "demographic_no", "ID", "int"),
            new DemographicTableIndex("form_hsfo2_visit", "demographic_no", "ID", "int"),
            new DemographicTableIndex("form_hsfo_visit", "demographic_no", "ID", "int"),
            new DemographicTableIndex("form_male_consult", "demographic_no", "id", "int"),
            new DemographicTableIndex("form_smart_encounter", "demographic_no", "id", "int"),
            new DemographicTableIndex("GroupNoteLink", "demographicNo", "id", "int"),
            new DemographicTableIndex("health_safety", "demographic_no", "id", "bigint"),
            new DemographicTableIndex("hl7_link", "demographic_no", "pid_id", "int"),
            new DemographicTableIndex("HRMDocumentToDemographic", "demographicNo", "id", "int"),
            new DemographicTableIndex("immunizations", "demographic_no", "ID", "int"),
            new DemographicTableIndex("IntegratorConsent", "demographicId", "id", "int"),
            new DemographicTableIndex("IntegratorConsentComplexExitInterview", "demographicId", "facilityId", "int"),
            new DemographicTableIndex("IntegratorProgressItem", "demographicNo", "id", "int"),
            new DemographicTableIndex("log", "demographic_no", "id", "int"),
            new DemographicTableIndex("mdsMSH", "demographic_no", "segmentID", "int"),
            new DemographicTableIndex("measurements", "demographicNo", "id", "int"),
            new DemographicTableIndex("measurementsDeleted", "demographicNo", "id", "int"),
            new DemographicTableIndex("msgDemoMap", "demographic_no", "id", "int"),
            new DemographicTableIndex("officecommunication", "demographic_no", "id", "int"),
            new DemographicTableIndex("oscar_annotations", "demographic_no", "id", "int"),
            new DemographicTableIndex("patientLabRouting", "demographic_no", "id", "int"),
            new DemographicTableIndex("ctl_document", "module_id", "document_no", "int(6)", "module = 'demographic'"),
            new DemographicTableIndex("patient_external_pharmacy_id", "demographic_no", "external_patient_id", "int"),
            new DemographicTableIndex("PHRVerification", "demographicNo", "id", "int"),
            new DemographicTableIndex("prescribe", "demographic_no", "prescribe_no", "int"),
            new DemographicTableIndex("prescription", "demographic_no", "script_no", "int"),
            new DemographicTableIndex("preventions", "demographic_no", "id", "int"),
            new DemographicTableIndex("program_client_restriction", "demographic_no", "id", "int"),
            new DemographicTableIndex("progress_sheet", "demographic_id", "form_id", "int"),
            new DemographicTableIndex("rehabStudy2004", "demographic_no", "studyID", "int"),
            new DemographicTableIndex("relationships", "demographic_no", "id", "int"),
            new DemographicTableIndex("remoteAttachments", "demographic_no", "id", "int"),
            new DemographicTableIndex("RemoteIntegratedDataCopy", "demographic_no", "id", "int"),
            new DemographicTableIndex("RemoteReferral", "demographicId", "id", "int"),
            new DemographicTableIndex("reportagesex", "demographic_no", "id", "int"),
            new DemographicTableIndex("resident_oscarMsg", "demographic_no", "id", "int"),
            new DemographicTableIndex("room_demographic", "demographic_no", "room_id", "int"),
            new DemographicTableIndex("SentToPHRTracking", "demographicNo", "id", "int"),
            new DemographicTableIndex("sharing_document_export", "demographic_no", "id", "int"),
            new DemographicTableIndex("sharing_exported_doc", "demographic_no", "id", "int"),
            new DemographicTableIndex("sharing_patient_document", "demographic_no", "patientDocumentId", "int"),
            new DemographicTableIndex("sharing_patient_network", "demographic_no", "id", "int"),
            new DemographicTableIndex("sharing_patient_policy_consent", "demographic_no", "id", "int"),
            new DemographicTableIndex("studydata", "demographic_no", "studydata_no", "int"),
            new DemographicTableIndex("surveyData", "demographic_no", "surveyDataId", "int"),
            new DemographicTableIndex("table_modification", "demographic_no", "id", "int"),
            new DemographicTableIndex("teleplan_eligibility_check", "demographic_no", "id", "int"),
            new DemographicTableIndex("tickler", "demographic_no", "tickler_no", "int"),
            new DemographicTableIndex("waitingList", "demographic_no", "id", "int"),
            new DemographicTableIndex("wcb", "demographic_no", "ID", "int"),
            new DemographicTableIndex("workflow", "demographic_no", "ID", "int")
    );

    private List<String> errors = new ArrayList<String>();
    
    public DemographicMergeRecordAction() {

    }
    public ActionForward execute(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) {

    	LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    	
    	if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "w", null)) {
			throw new SecurityException("missing required security object (_demographic)");
		}
    	
        if (request.getParameterValues("records")==null) {
            return mapping.findForward("failure");
        }
        
        String outcome = "success";
        ArrayList<String> records = new ArrayList<String>(Arrays.asList(request.getParameterValues("records")));
        String head = request.getParameter("head");
        String action = request.getParameter("mergeAction");
        String provider_no = request.getParameter("provider_no");
        DemographicMerged dmDAO = new DemographicMerged();
        errors = new ArrayList<String>();

        if (records.size() > 1 && records.contains(head)) {
            if (action.equals("merge") && head != null) {
                for (int i = 0; i < records.size(); i++) {
                    if (!(records.get(i)).equals(head)) {
                        mergeDemographics(Integer.parseInt(records.get(i)), Integer.parseInt(head), loggedInInfo.getLoggedInProviderNo());
                    }
                }
            } else if (action.equals("unmerge") && records.size() > 0) {
                outcome = "successUnMerge";
                for (int i = 0; i < records.size(); i++) {
                    if (!(records.get(i)).equals(head)) {
                        unMergeDemographics(Integer.parseInt(records.get(i)), Integer.parseInt(head), loggedInInfo.getLoggedInProviderNo());
                    }
                }
            }
        } else {
             outcome = "failure";
        }
        
        if (!errors.isEmpty()) {
            outcome = "partialfailure";
        }
        
        request.setAttribute("mergeoutcome",outcome);

        if (request.getParameter("caisiSearch") != null && request.getParameter("caisiSearch").equalsIgnoreCase("yes")){
            outcome = "caisiSearch";
        }


        ActionRedirect actionRedirect = new ActionRedirect(mapping.findForward(outcome));
        actionRedirect.addParameter("errors", errors);
        return actionRedirect;
    }

    private void mergeDemographics(Integer oldDemographic, Integer mainDemographic, String providerNo) {
        DemographicMergeOperationDao demographicMergeOperationDao = SpringUtils.getBean(DemographicMergeOperationDao.class);
        DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
        
        if (!NumberUtils.isParsable(providerNo) || oldDemographic <= 0 || mainDemographic <= 0) {
            return;
        }
        
        String databaseName = OscarProperties.getInstance().getProperty("db_name");
        if (databaseName.contains("?")) {
            databaseName = databaseName.split("\\?")[0];
        }
        for (DemographicTableIndex demographicTableIndex : demographicTableIndexList) {
            try {
                demographicMergeOperationDao.mergeDemographic(oldDemographic, mainDemographic, providerNo, demographicTableIndex, databaseName);
            } catch (Exception e) {
                System.out.println("Demographic merge error occurred when merging demographic " + oldDemographic + " to " + mainDemographic + " on table " + demographicTableIndex.getName() + " because of: ");
                e.printStackTrace();
                errors.add(demographicTableIndex.getName());
             }
        }

        demographicMergeOperationDao.mergeDemographicWithSql(oldDemographic, mainDemographic, providerNo, "casemgmt_note",
                "SELECT note_id FROM casemgmt_note WHERE demographic_no = :oldDemo",
                "update casemgmt_note set demographic_no = :mainDemo where demographic_no = :oldDemo and note not like 'import%'");
        
        demographicMergeOperationDao.mergeDemographicForCaseMgmtIssue(oldDemographic, mainDemographic, providerNo);
        
        // Merge demographic table data
        Demographic oldDemo = demographicDao.getDemographicById(oldDemographic);
        Demographic mainDemo = demographicDao.getDemographicById(mainDemographic);

        if (StringUtils.isNotEmpty(oldDemo.getHin()) && StringUtils.isNotEmpty(mainDemo.getHin()) || StringUtils.isNotEmpty(oldDemo.getHin()) && StringUtils.isEmpty(mainDemo.getHin())) {
            String mainHin = mainDemo.getHin();
            String mainVer = mainDemo.getVer();
            mainDemo.setHin(oldDemo.getHin());
            mainDemo.setVer(oldDemo.getVer());
            demographicMergeOperationDao.persist((new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), "Demographic HIN Main", mainHin)));
            demographicMergeOperationDao.persist((new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), "Demographic HIN Version Main", mainVer)));
            // blank out existing oldDemo Hin
            String oldHin = oldDemo.getHin();
            String oldVer = oldDemo.getVer();
            oldDemo.setHin("");
            oldDemo.setVer("");
            demographicMergeOperationDao.persist(new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), "Demographic HIN Old", oldHin));
            demographicMergeOperationDao.persist(new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), "Demographic HIN Version Old", oldVer));
        }
        String oldPatientStatus = oldDemo.getPatientStatus();
        demographicMergeOperationDao.persist(new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), "Demographic Patient Status", oldPatientStatus));
        oldDemo.setPatientStatus("MERGED");
        
        if (StringUtils.isNotEmpty(oldDemo.getPhone()) && StringUtils.isNotEmpty(mainDemo.getPhone()) || StringUtils.isNotEmpty(oldDemo.getPhone()) && StringUtils.isEmpty(oldDemo.getPhone())) {
            String mainPhone = mainDemo.getPhone();
            mainDemo.setPhone(oldDemo.getPhone());
            demographicMergeOperationDao.persist((new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), "Demographic Phone Main", mainPhone)));
        }
        if (StringUtils.isNotEmpty(oldDemo.getPhone2()) && StringUtils.isNotEmpty(mainDemo.getPhone2()) || StringUtils.isNotEmpty(oldDemo.getPhone2()) && StringUtils.isEmpty(oldDemo.getPhone2())) {
            String mainPhone2 = mainDemo.getPhone2();
            mainDemo.setPhone2(oldDemo.getPhone2());
            demographicMergeOperationDao.persist((new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), "Demographic Phone2 Main", mainPhone2)));
        }
        if (hasReferralPhysician(oldDemo) && hasReferralPhysician(mainDemo)) {
            val mainReferralPhysician = mainDemo.getReferralPhysicianName() + "," + mainDemo.getReferralPhysicianOhip();
            setMainDemographicReferralPhysicianExtensions(mainDemo, oldDemo);
            demographicMergeOperationDao.persist((new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), "Demographic family_doctor Main", mainReferralPhysician)));
        }
        if (hasFamilyPhysician(oldDemo) && hasFamilyPhysician(mainDemo)) {
            val mainFamilyPhysician = mainDemo.getFamilyPhysicianName() + "," + mainDemo.getFamilyPhysicianOhip();
            setMainDemographicFamilyPhysicianExtensions(mainDemo, oldDemo);
            demographicMergeOperationDao.persist((new DemographicMergeOperation(oldDemographic, mainDemographic, providerNo, new Date(), "Demographic family_physician Main", mainFamilyPhysician)));
        }
        
        DemographicManager.saveDemographic(oldDemo);
        DemographicManager.saveDemographic(mainDemo);
    }
    
    private void unMergeDemographics (Integer oldDemographic, Integer mainDemographic, String providerNo) {
        DemographicMergeOperationDao demographicMergeOperationDao = SpringUtils.getBean(DemographicMergeOperationDao.class);
 
        if (!NumberUtils.isParsable(providerNo) || oldDemographic <= 0 || mainDemographic <= 0) {
            return;
        }
        
        List<DemographicMergeOperation> demographicMergeOperationList = demographicMergeOperationDao.findAllByOldDemographicAndMainDemographic(oldDemographic, mainDemographic);
        String databaseName = OscarProperties.getInstance().getProperty("db_name");
        if (databaseName.contains("?")) {
            databaseName = databaseName.split("\\?")[0];
        }
        
        for (DemographicMergeOperation demographicMergeOperation : demographicMergeOperationList) {
            DemographicTableIndex table = null;
            for (DemographicTableIndex demographicTableIndex : demographicTableIndexList) {
                if (demographicTableIndex.getName().equals(demographicMergeOperation.getContentType())) {
                    table = demographicTableIndex;
                }
            }
            if (table != null) {
                demographicMergeOperationDao.unMergeDemographic(
                    oldDemographic, demographicMergeOperation.getContentID(), demographicMergeOperation.getContentType(), 
                    table.getDemographicColumn(), table.getPrimaryKeyColumn(), table.getPrimaryKeyType(), table.getAdditionalWhereClause(), databaseName);
            } else if ("casemgmt_issue".equals(demographicMergeOperation.getContentType())) {
                demographicMergeOperationDao.unMergeDemographic(oldDemographic, demographicMergeOperation.getContentID(), "casemgmt_issue", "demographic_no", "id", "int", databaseName);
            } else if ("casemgmt_note".equals(demographicMergeOperation.getContentType())) {
                demographicMergeOperationDao.unMergeDemographic(oldDemographic, demographicMergeOperation.getContentID(), "casemgmt_note", "demographic_no", "note_id", "int", databaseName);
            }
        }
        
        demographicMergeOperationDao.revertUpdateDemographic(oldDemographic, mainDemographic, providerNo);
    }

    private boolean hasReferralPhysician(final Demographic demographic) {
        return StringUtils.isNotEmpty(demographic.getReferralPhysicianRowId())
            || StringUtils.isNotEmpty(demographic.getReferralPhysicianName())
            || StringUtils.isNotEmpty(demographic.getReferralPhysicianOhip());
    }

    private boolean hasFamilyPhysician(final Demographic demographic) {
        return StringUtils.isNotEmpty(demographic.getFamilyPhysicianRowId())
            || StringUtils.isNotEmpty(demographic.getFamilyPhysicianName())
            || StringUtils.isNotEmpty(demographic.getFamilyPhysicianOhip());
    }

    private void setMainDemographicReferralPhysicianExtensions(final Demographic mainDemo, final Demographic oldDemo) {
        mainDemo.setReferralPhysicianRowId(oldDemo.getReferralPhysicianRowId());
        mainDemo.setReferralPhysicianName(oldDemo.getReferralPhysicianName());
        mainDemo.setReferralPhysicianOhip(oldDemo.getReferralPhysicianOhip());
    }

    private void setMainDemographicFamilyPhysicianExtensions(final Demographic mainDemo, final Demographic oldDemo) {
        mainDemo.setFamilyPhysicianRowId(oldDemo.getFamilyPhysicianRowId());
        mainDemo.setFamilyPhysicianName(oldDemo.getFamilyPhysicianName());
        mainDemo.setFamilyPhysicianOhip(oldDemo.getFamilyPhysicianOhip());
    }
}