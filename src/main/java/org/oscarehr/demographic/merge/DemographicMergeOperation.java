/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.demographic.merge;

import org.oscarehr.common.model.AbstractModel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Entity
public class DemographicMergeOperation extends AbstractModel<Integer> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer oldDemographic;
    private Integer mainDemographic;
    private String providerNo;
    private Date dateMerged;
    private String contentType;
    private String contentID;

    public DemographicMergeOperation () {

    }

    public DemographicMergeOperation (int oldDemographic, int mainDemographic, String providerNo, Date dateMerged, String contentType, String contentID) {
        this.oldDemographic = oldDemographic;
        this.mainDemographic = mainDemographic;
        this.providerNo = providerNo;
        this.dateMerged = dateMerged;
        this.contentType = contentType;
        this.contentID = contentID;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOldDemographic() {
        return oldDemographic;
    }

    public void setOldDemographic(Integer oldDemographic) {
        this.oldDemographic = oldDemographic;
    }

    public Integer getMainDemographic() {
        return mainDemographic;
    }

    public void setMainDemographic(Integer mainDemographic) {
        this.mainDemographic = mainDemographic;
    }

    public String getProviderNo() {
        return providerNo;
    }

    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }

    public Date getdateMerged() {
        return dateMerged;
    }

    public void setdateMerged(Date dateMerged) {
        this.dateMerged = dateMerged;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentID() {
        return contentID;
    }

    public void setContentID(String contentID) {
        this.contentID = contentID;
    }
}