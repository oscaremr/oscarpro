package org.oscarehr.document.web;

import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.oscarehr.util.MiscUtils;

import java.io.IOException;
import java.util.Date;
import java.util.Hashtable;

/**
 * Bean for storing PDDocuments when rendering several pdf preview images for split documents
 * 
 */
public class PDDocumentBean {
    private Hashtable<String, PDDocument> docTable = new Hashtable<>();
    private Hashtable<String, Integer> pageCounterTable = new Hashtable<>();
    private Hashtable<String, Date> lastUpdateTable = new Hashtable<>();
    private unusedDocChecker docChecker = new unusedDocChecker();
    private Logger logger = MiscUtils.getLogger();

    public void incrementPageCounter(String key){
        Integer counter = this.pageCounterTable.get(key);
        if(counter != null) {
            this.pageCounterTable.put(key, counter + 1);
        }
    }
    
    public void resetPageCounter(String key){
        this.pageCounterTable.put(key, 0);
    }
    
    public Integer getPageCounter(String key){
        return this.pageCounterTable.get(key);
    }
    
    public void setPDDocument(String key, PDDocument pdDocument){
        this.lastUpdateTable.put(key, new Date());
        this.docTable.put(key, pdDocument);

        docChecker.start();
    }
    
    public PDDocument getPDDocument(String key){
        this.lastUpdateTable.put(key, new Date());
        return this.docTable.get(key);
    }
    
    public Date getLastUpdateDate(String key){
        return this.lastUpdateTable.get(key);
    }

    public void clearPDDocument(String key){
        PDDocument doc = this.docTable.remove(key);
        if(doc != null){
            try {
                doc.close();
            } catch (IOException e){
                logger.warn("Document " + key + " could not be closed properly", e);
            }
        }
        this.pageCounterTable.remove(key);
        this.lastUpdateTable.remove(key);
        if (this.docTable.isEmpty()){
            this.docChecker.stop();
        }
    }

    /**
     *     This class runs a separate thread to close and remove unused docs. 
     *      This is done in this way in case the frontend loses connection and fails to properly close and remove the document.
      */
    private class unusedDocChecker implements Runnable {
        private Thread thread;
        public void run(){
            try {
                while(!docTable.isEmpty()){
                    Thread.sleep(120000);
                    checkAndRemoveUnusedDocs();
                }
            } catch (InterruptedException ie){ 
                //do nothing
            }
            logger.debug("No more docs, stopping unused document checker...");
        }
        public void start(){
            if (thread == null || !thread.isAlive()){
                thread = new Thread(this, "doc checker");
                logger.debug("Starting unused document checker");
                thread.start();
            }
        }
        public void stop(){
            checkAndRemoveUnusedDocs();
            if (thread != null && thread.isAlive() && lastUpdateTable.isEmpty()) {
                thread.interrupt();
            }
        }

        private void checkAndRemoveUnusedDocs(){
            logger.debug("Checking for unused docs");
            for(String key : lastUpdateTable.keySet().toArray(new String[0])){
                Date lastUpdate = lastUpdateTable.get(key);
                if(lastUpdate == null || new Date().getTime() - lastUpdate.getTime() > 60000){
                    logger.debug("removing " + key);
                    PDDocument doc = docTable.remove(key);
                    if(doc != null){
                        try {
                            doc.close();
                        } catch (IOException e){
                            logger.warn("Document " + key + " could not be closed properly", e);
                        }
                    }
                    pageCounterTable.remove(key);
                    lastUpdateTable.remove(key);
                }
            }
        }
    }
}
