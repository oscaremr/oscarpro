/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package org.oscarehr.document.web;

import ca.oscarpro.fax.FaxFacade;
import ca.oscarpro.fax.FaxFileType;
import ca.oscarpro.fax.SendFaxData;
import com.Ostermiller.util.Base64;
import com.lowagie.text.DocumentException;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.persistence.EntityExistsException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lombok.val;
import lombok.var;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.tika.io.IOUtils;
import org.jpedal.PdfDecoder;
import org.jpedal.fonts.FontMappings;
import org.oscarehr.PMmodule.caisi_integrator.CaisiIntegratorManager;
import org.oscarehr.PMmodule.caisi_integrator.IntegratorFallBackManager;
import org.oscarehr.PMmodule.model.ProgramProvider;
import org.oscarehr.caisi_integrator.ws.CachedDemographicDocument;
import org.oscarehr.caisi_integrator.ws.CachedDemographicDocumentContents;
import org.oscarehr.caisi_integrator.ws.FacilityIdIntegerCompositePk;
import org.oscarehr.casemgmt.model.CaseManagementNote;
import org.oscarehr.casemgmt.model.CaseManagementNoteLink;
import org.oscarehr.casemgmt.service.CaseManagementManager;
import org.oscarehr.common.dao.ClinicDAO;
import org.oscarehr.common.dao.CtlDocumentDao;
import org.oscarehr.common.dao.DocumentDao;
import org.oscarehr.common.dao.FaxConfigDao;
import org.oscarehr.common.dao.FaxJobDao;
import org.oscarehr.common.dao.OscarLogDao;
import org.oscarehr.common.dao.PatientLabRoutingDao;
import org.oscarehr.common.dao.ProviderInboxRoutingDao;
import org.oscarehr.common.dao.ProviderLabRoutingDao;
import org.oscarehr.common.dao.SecRoleDao;
import org.oscarehr.common.model.CtlDocument;
import org.oscarehr.common.model.Document;
import org.oscarehr.common.model.FaxConfig;
import org.oscarehr.common.model.FaxJob;
import org.oscarehr.common.model.IncomingLabRules;
import org.oscarehr.common.model.OscarLog;
import org.oscarehr.common.model.PatientLabRouting;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.ProviderLabRoutingModel;
import org.oscarehr.common.model.SecRole;
import org.oscarehr.managers.ProgramManager2;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.sharingcenter.SharingCenterUtil;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.util.WebUtils;
import org.owasp.encoder.Encode;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import oscar.OscarProperties;
import oscar.dms.EDoc;
import oscar.dms.EDocUtil;
import oscar.dms.IncomingDocUtil;
import oscar.log.LogAction;
import oscar.log.LogConst;
import oscar.oscarDemographic.data.DemographicData;
import oscar.oscarEncounter.data.EctProgram;
import oscar.oscarLab.ForwardingRules;
import oscar.oscarLab.ca.on.LabResultData;
import oscar.util.UtilDateUtilities;

/**
 * @author jaygallagher
 */
public class ManageDocumentAction extends DispatchAction {

	private static Logger log = MiscUtils.getLogger();

	private DocumentDao documentDao = SpringUtils.getBean(DocumentDao.class);
	private CtlDocumentDao ctlDocumentDao = SpringUtils.getBean(CtlDocumentDao.class);
	private ProviderInboxRoutingDao providerInboxRoutingDAO = SpringUtils.getBean(ProviderInboxRoutingDao.class);
	private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class) ;
	private static OscarProperties oscarProperties = oscar.OscarProperties.getInstance();
	private ForwardingRules forwardingRules = new ForwardingRules();
	private ProviderLabRoutingDao providerLabRoutingDao = SpringUtils.getBean(ProviderLabRoutingDao.class);
	private final FaxFacade faxFacade = SpringUtils.getBean(FaxFacade.class);

	public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

		return null;
	}

	public ActionForward documentUpdateAjax(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

		String ret = "";

		String observationDate = request.getParameter("observationDate");// :2008-08-22<
		String documentDescription = request.getParameter("documentDescription");// :test2<
		String documentId = request.getParameter("documentId");// :29<
		String docType = request.getParameter("docType");// :consult<
		String source = request.getParameter("source");
		String sourceFacility = request.getParameter("source_facility");
		boolean isAbnormal = WebUtils.isChecked(request, "abnormalFlag");
		if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "w", null)) {
        	throw new SecurityException("missing required security object (_edoc)");
        }

		LogAction.addLog((String) request.getSession().getAttribute("user"), LogConst.ADD,
				LogConst.CON_DOCUMENT, documentId, LoggedInInfo.obtainClientIpAddress(request));

		String demog = request.getParameter("demog");

		String[] flagproviders = request.getParameterValues("flagproviders");
		// String demoLink=request.getParameter("demoLink");

		// TODO: if demoLink is "on", check if msp is in flagproviders, if not save to providerInboxRouting, if yes, don't save.

		// DONT COPY THIS !!!
		if (flagproviders != null && flagproviders.length > 0) { // TODO: THIS NEEDS TO RUN THRU THE lab forwarding rules!
			try {
				for (String proNo : flagproviders) {
					providerInboxRoutingDAO.addToProviderInbox(proNo, Integer.parseInt(documentId), LabResultData.DOCUMENT);
				}
			} catch (Exception e) {
				MiscUtils.getLogger().error("Error", e);
			}
		}
		
		//Check to see if we have to route document to patient
		PatientLabRoutingDao patientLabRoutingDao = SpringUtils.getBean(PatientLabRoutingDao.class);
		List<PatientLabRouting>patientLabRoutingList = patientLabRoutingDao.findByLabNoAndLabType(Integer.parseInt(documentId), docType);
		if( patientLabRoutingList == null || patientLabRoutingList.size() == 0 ) {
			PatientLabRouting patientLabRouting = new PatientLabRouting();
			patientLabRouting.setDemographicNo(Integer.parseInt(demog));
			patientLabRouting.setLabNo(Integer.parseInt(documentId));
			patientLabRouting.setLabType("DOC");
			patientLabRoutingDao.persist(patientLabRouting);
		}
		
		
		Document d = documentDao.getDocument(documentId);

		if(d != null) {
			d.setDocdesc(documentDescription);
			d.setDoctype(docType);
			d.setSource(source);
			d.setSourceFacility(sourceFacility);
			Date obDate = UtilDateUtilities.StringToDate(observationDate);
			d.setAbnormal(isAbnormal);
			
			if (obDate != null) {
				d.setObservationdate(obDate);
			}
	
			documentDao.merge(d);
		}

		
		try {

			CtlDocument ctlDocument = ctlDocumentDao.getCtrlDocument(Integer.parseInt(documentId));
			int demographicNumber = Integer.parseInt(demog);
			// If this ctlDocument is a document module type and is not for the demographic being saved then create a new entry and remove the old one
			if(ctlDocument != null && (ctlDocument.isDemographicDocument() && demographicNumber != ctlDocument.getId().getModuleId())) {
				
				CtlDocument matchedCtlDocument = new CtlDocument();
				matchedCtlDocument.getId().setDocumentNo(ctlDocument.getId().getDocumentNo());
				matchedCtlDocument.getId().setModule(ctlDocument.getId().getModule());
				matchedCtlDocument.getId().setModuleId(demographicNumber);
				matchedCtlDocument.setStatus(ctlDocument.getStatus());
				
				ctlDocumentDao.persist(matchedCtlDocument);
				
				ctlDocumentDao.remove(ctlDocument.getId());
				
				// save a document created note
				if (ctlDocument.isDemographicDocument()) {
					// save note
					saveDocNote(request, d.getDocdesc(), demog, documentId);
				}
			}
		} catch (Exception e) {
			MiscUtils.getLogger().error("Error", e);
		}
		
		
		if (ret != null && !ret.equals("")) {
			// response.getOutputStream().print(ret);
		}
		HashMap hm = new HashMap();
		hm.put("patientId", demog);
		JSONObject jsonObject = JSONObject.fromObject(hm);
		try {
			response.getOutputStream().write(jsonObject.toString().getBytes());
		} catch (IOException e) {
			MiscUtils.getLogger().error("Error", e);
		}

		return null;

	}

	public ActionForward getDemoNameAjax(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String dn = request.getParameter("demo_no");
		
		if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "r", dn)) {
        	throw new SecurityException("missing required security object (_demographic)");
        }
		
		HashMap hm = new HashMap();
		hm.put("demoName", Encode.forJavaScript(getDemoName(LoggedInInfo.getLoggedInInfoFromSession(request), dn)));
		JSONObject jsonObject = JSONObject.fromObject(hm);
		try {
			response.getOutputStream().write(jsonObject.toString().getBytes());
		} catch (IOException e) {
			MiscUtils.getLogger().error("Error", e);
		}

		return null;
	}

	public ActionForward removeLinkFromDocument(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) {
		String docType = request.getParameter("docType");
		String docId = request.getParameter("docId");
		String providerNo = request.getParameter("providerNo");
		
		if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "w", null)) {
        	throw new SecurityException("missing required security object (_edoc)");
        }
		

		providerInboxRoutingDAO.removeLinkFromDocument(docType, Integer.parseInt(docId), providerNo);
		HashMap hm = new HashMap();
		hm.put("linkedProviders", providerInboxRoutingDAO.getProvidersWithRoutingForDocument(docType, Integer.parseInt(docId)));

		JSONObject jsonObject = JSONObject.fromObject(hm);
		try {
			response.getOutputStream().write(jsonObject.toString().getBytes());
		} catch (IOException e) {
			MiscUtils.getLogger().error("Error",e);
		}

		return null;
	}	
        public ActionForward refileDocumentAjax(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
	
		String documentId = request.getParameter("documentId");
		String queueId = request.getParameter("queueId");

		if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "w", null)) {
        	throw new SecurityException("missing required security object (_edoc)");
                }
                
                try {
                    EDocUtil.refileDocument(documentId,queueId);
                } catch (Exception e) {
                    MiscUtils.getLogger().error("Error", e);
                }
                return null;
        }
        
	public ActionForward documentUpdate(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {

		String ret = "";

		String observationDate = request.getParameter("observationDate");// :2008-08-22<
		String documentDescription = request.getParameter("documentDescription");// :test2<
		String documentId = request.getParameter("documentId");// :29<
		String docType = request.getParameter("docType");// :consult<
		String sourceAuthor = request.getParameter("source");
		String sourceFacility = request.getParameter("source_facility");
		boolean abnormal = WebUtils.isChecked(request, "abnormalFlag");

		if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "w", null)) {
        	throw new SecurityException("missing required security object (_edoc)");
        }

		LogAction.addLog((String) request.getSession().getAttribute("user"), LogConst.ADD,
				LogConst.CON_DOCUMENT, documentId, LoggedInInfo.obtainClientIpAddress(request));

		String demog = request.getParameter("demog");

		String[] flagproviders = request.getParameterValues("flagproviders");
		// String demoLink=request.getParameter("demoLink");

		// TODO: if demoLink is "on", check if msp is in flagproviders, if not save to providerInboxRouting, if yes, don't save.

		// DONT COPY THIS !!!
		if (flagproviders != null && flagproviders.length > 0) { // TODO: THIS NEEDS TO RUN THRU THE lab forwarding rules!
			try {
				for (String proNo : flagproviders) {
					providerInboxRoutingDAO.addToProviderInbox(proNo, Integer.parseInt(documentId), LabResultData.DOCUMENT);
				}
			} catch (Exception e) {
				MiscUtils.getLogger().error("Error", e);
			}
		}
		Document d = documentDao.getDocument(documentId);

		if(d != null) {
			d.setDocdesc(documentDescription);
			d.setDoctype(docType);
			Date obDate = UtilDateUtilities.StringToDate(observationDate);
			d.setAbnormal(abnormal);
			d.setSource(sourceAuthor);
			d.setSourceFacility(sourceFacility);
			
			if (obDate != null) {
				d.setObservationdate(obDate);
			}
	
			documentDao.merge(d);
		}

		try {

			CtlDocument ctlDocument = ctlDocumentDao.getCtrlDocument(Integer.parseInt(documentId));
			if(ctlDocument != null) {
				ctlDocument.getId().setModuleId(Integer.parseInt(demog));
				ctlDocumentDao.merge(ctlDocument);
				// save a document created note
				if (ctlDocument.isDemographicDocument()) {
					// save note
					saveDocNote(request, d.getDocdesc(), demog, documentId);
				}
			}
		} catch (Exception e) {
			MiscUtils.getLogger().error("Error", e);
		}

		if (ret != null && !ret.equals("")) {
			// response.getOutputStream().print(ret);
		}
				
		
		String providerNo = request.getParameter("providerNo");
		String searchProviderNo = request.getParameter("searchProviderNo");
		String ackStatus = request.getParameter("status");
		String demoName = getDemoName(LoggedInInfo.getLoggedInInfoFromSession(request) , demog);
		request.setAttribute("demoName", demoName);
		request.setAttribute("segmentID", documentId);
		request.setAttribute("providerNo", providerNo);
		request.setAttribute("searchProviderNo", searchProviderNo);
		request.setAttribute("status", ackStatus);

		return mapping.findForward("displaySingleDoc");

	}

	private String getDemoName(LoggedInInfo loggedInInfo, String demog) {
		DemographicData demoD = new DemographicData();
		org.oscarehr.common.model.Demographic demo = demoD.getDemographic(loggedInInfo, demog);
		String demoName = demo.getLastName() + ", " + demo.getFirstName();
		return demoName;
	}

	private void saveDocNote(final HttpServletRequest request, String docDesc, String demog, String documentId) {

		Date now = EDocUtil.getDmsDateTimeAsDate();
		// String docDesc=d.getDocdesc();
		CaseManagementNote cmn = new CaseManagementNote();
		cmn.setUpdate_date(now);
		cmn.setObservation_date(now);
		cmn.setDemographic_no(demog);
		HttpSession se = request.getSession();
		String user_no = (String) se.getAttribute("user");
		String prog_no = new EctProgram(se).getProgram(user_no);
		WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(se.getServletContext());
		CaseManagementManager cmm = (CaseManagementManager) ctx.getBean("caseManagementManager");
		cmn.setProviderNo("-1");// set the provider no to be -1 so the editor appear as 'System'.
		Provider provider = EDocUtil.getProvider(user_no);
		String provFirstName = "";
		String provLastName = "";
		if(provider!=null) {
			provFirstName=provider.getFirstName();
			provLastName=provider.getLastName();
		}
		String strNote = "Document" + " " + docDesc + " " + "created at " + now + " by " + provFirstName + " " + provLastName + ".";

		// String strNote="Document"+" "+docDesc+" "+ "created at "+now+".";
		cmn.setNote(strNote);
		cmn.setSigned(true);
		cmn.setSigning_provider_no("-1");
		cmn.setProgram_no(prog_no);
		
		SecRoleDao secRoleDao = (SecRoleDao) SpringUtils.getBean("secRoleDao");
		SecRole doctorRole = secRoleDao.findByName("doctor");		
		cmn.setReporter_caisi_role(doctorRole.getId().toString());
		
		cmn.setReporter_program_team("0");
		cmn.setPassword("NULL");
		cmn.setLocked(false);
		cmn.setHistory(strNote);
		cmn.setPosition(0);

		Long note_id = cmm.saveNoteSimpleReturnID(cmn);
		// Debugging purposes on the live server
		MiscUtils.getLogger().info("Document Note ID: "+note_id.toString());
		
		// Add a noteLink to casemgmt_note_link
		CaseManagementNoteLink cmnl = new CaseManagementNoteLink();
		cmnl.setTableName(CaseManagementNoteLink.DOCUMENT);
		cmnl.setTableId(Long.parseLong(documentId));
		cmnl.setNoteId(note_id);
		EDocUtil.addCaseMgmtNoteLink(cmnl);
	}

	public ActionForward removeDemographicFromDocument(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		
		//Checks if the user has the rights to use this function
		if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "w", null)) {
        	throw new SecurityException("missing required security object (_edoc)");
        }
		
		String documentId = request.getParameter("docId");
		String docType = request.getParameter("docType");
		String providerNo = LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo();

		//Gets the patientLabRoutingDao
		PatientLabRoutingDao patientLabRoutingDao = SpringUtils.getBean(PatientLabRoutingDao.class);
		//Gets the caseManagementManager
		CaseManagementManager caseManagementManager = SpringUtils.getBean(CaseManagementManager.class);
		CaseManagementNote caseManagementNote;
		OscarLogDao logDao = SpringUtils.getBean(OscarLogDao.class);
		OscarLog log;
		
		try {
			//Gets the patientLabRoutng for the given document
			PatientLabRouting patientLabRouting = patientLabRoutingDao.findDemographics(docType, Integer.parseInt(documentId));
			//Gets the ctlDocument for the demographic and document
			CtlDocument ctlDocument = ctlDocumentDao.getCtrlDocument(Integer.parseInt(documentId));
			//Gets all notes on the demographic's echart relating to the given document
			List<CaseManagementNoteLink> caseManagementNoteLinks = caseManagementManager.getLinkByTableIdDesc(CaseManagementNoteLink.DOCUMENT, Long.valueOf(documentId));
			if (patientLabRouting == null && (ctlDocument == null || ctlDocument.getId() == null)) {
				// throw exception to be caught
				throw new Exception("Missing PatientLabRouting and/or CtlDocument link");
			}
			//Gets the demographic number from the patientLabRouting, otherwise from ctl_document
			Integer demographicNumber = patientLabRouting != null ? patientLabRouting.getDemographicNo() : ctlDocument.getId().getModuleId();
			
			//Checks if there is a patientLabRouting
			if (patientLabRouting != null) {
				//Removes the patientLabRouting
				patientLabRoutingDao.remove(patientLabRouting.getId());
			}
			//For each note on the echart relating to the demographic and document, archives and updates it
			for (CaseManagementNoteLink noteLink : caseManagementNoteLinks){
				caseManagementNote = caseManagementManager.getNote(String.valueOf(noteLink.getNoteId()));
				
				if (caseManagementNote != null){
					caseManagementNote.setArchived(true);
					caseManagementManager.updateNote(caseManagementNote);
				}
			}
			//If the ctlDocument exists
			if (ctlDocument != null) {
				//Creates a new CtlDocument for unlinking
				CtlDocument unlinkedCtlDocument = new CtlDocument();
				//Populates the new unlinkedCtlDocument
				unlinkedCtlDocument.getId().setDocumentNo(ctlDocument.getId().getDocumentNo());
				unlinkedCtlDocument.getId().setModule(ctlDocument.getId().getModule());
				unlinkedCtlDocument.getId().setModuleId(-1);
				unlinkedCtlDocument.setStatus(ctlDocument.getStatus());
				
				//Sets the module id of the unlinkedCtlDocument to -1
				unlinkedCtlDocument.getId().setModuleId(-1);
				//Updates the ctlDocument and saves in the unlinkedCtlDocument
				ctlDocumentDao.remove(ctlDocument.getId());
				ctlDocumentDao.persist(unlinkedCtlDocument);
			}
			
			//Logs the unlinking
			log = new OscarLog();
			log.setProviderNo(providerNo);
			log.setAction("unlink");
			log.setContent("document");
			log.setContentId(documentId);
			log.setIp(LoggedInInfo.obtainClientIpAddress(request));
			log.setDemographicId(demographicNumber);
			logDao.persist(log);

			request.setAttribute("success", true);
		} catch (EntityExistsException eee) {
			MiscUtils.getLogger().error("the unlinkedCtlDocument already exists", eee);
			request.setAttribute("success", false);
		} catch (Exception e) {
			MiscUtils.getLogger().error("Tried to remove demographic from document but failed.", e); 
			request.setAttribute("success", false);
		}

		return null;

	}
	
	/*
	 * private void savePatientLabRouting(String demog,String docId,String docType){ CommonLabResultData.updatePatientLabRouting(docId, demog, docType); }
	 */

	private static File getDocumentCacheDir(String docdownload) {
		// File cacheDir = new File(docdownload+"_cache");
		File docDir = new File(docdownload);
		String documentDirName = docDir.getName();
		File parentDir = docDir.getParentFile();

		File cacheDir = new File(parentDir, documentDirName + "_cache");

		if (!cacheDir.exists()) {
			cacheDir.mkdir();
		}
		return cacheDir;
	}

	private File hasCacheVersion2(Document d, Integer pageNum) {
		File documentCacheDir = getDocumentCacheDir(oscarProperties.getProperty("DOCUMENT_DIR"));
		File outfile = new File(documentCacheDir, d.getDocfilename() + "_" + pageNum + ".png");
		if (!outfile.exists()) {
			outfile = null;
		}
		return outfile;
	}

	public static void deleteCacheVersion(Document d, int pageNum) {
		File documentCacheDir = getDocumentCacheDir(oscarProperties.getProperty("DOCUMENT_DIR"));
		//pageNum=pageNum-1;
		File outfile = new File(documentCacheDir,d.getDocfilename()+"_"+pageNum+".png");
		if (outfile.exists()){
			outfile.delete();
		}
	}

	private File hasCacheVersion(Document d, int pageNum){
		File documentCacheDir = getDocumentCacheDir(oscarProperties.getProperty("DOCUMENT_DIR"));
		//pageNum= pageNum-1;
		File outfile = new File(documentCacheDir,d.getDocfilename()+"_"+pageNum+".png");
		if (!outfile.exists()){
			outfile = null;
		}
		return outfile;
	}

	private Integer maxPDFLoadBytesMemory(){
		BigDecimal maxBytes = new BigDecimal(1024* 1024 * 100); //Default maximum is 100 MB
		String propertyMax = oscarProperties.getProperty("doc_preview_max_memory");

		if(propertyMax != null){
			Pattern numberPattern = Pattern.compile("\\d+(\\.\\d+)?");
			Pattern unitPattern = Pattern.compile("[KMG]?[B]", Pattern.CASE_INSENSITIVE);

			Matcher numberMatcher = numberPattern.matcher(propertyMax);
			Matcher unitMatcher = unitPattern.matcher(propertyMax);
			if(numberMatcher.find() && unitMatcher.find()){
				maxBytes = new BigDecimal(propertyMax.substring(numberMatcher.start(), numberMatcher.end()));
				char unit = propertyMax.charAt(unitMatcher.start());

				if("K".equalsIgnoreCase("" + unit)){
					maxBytes = maxBytes.multiply(new BigDecimal(1024));
				}
				if("M".equalsIgnoreCase("" + unit)){
					maxBytes = maxBytes.multiply(new BigDecimal(1024 * 1024));
				}
				if("G".equalsIgnoreCase("" + unit)){
					maxBytes = maxBytes.multiply(new BigDecimal(1024 * 1024 *1024));
				}
			}
		}
		return maxBytes.intValue();
	}
	
	public ActionForward removeDocFromSession(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "r", null)) {
			throw new SecurityException("missing required security object (_edoc)");
		}
		String doc_no = request.getParameter("doc_no");
		PDDocumentBean bean = SpringUtils.getBean(PDDocumentBean.class);
		bean.clearPDDocument("doc" + doc_no + "_" + request.getRequestedSessionId());

		return null;
	}
	
	public ActionForward createTempImagePreview(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException{
		if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "r", null)) {
			throw new SecurityException("missing required security object (_edoc)");
		}
		String doc_no = request.getParameter("doc_no");
		int pageNum = Integer.parseInt(request.getParameter("pageNum"));
		PDDocumentBean bean = SpringUtils.getBean(PDDocumentBean.class);
		Integer count = bean.getPageCounter("doc" + doc_no + "_" + request.getRequestedSessionId());
		PDDocument pdf = bean.getPDDocument("doc" + doc_no + "_" + request.getRequestedSessionId());
		Document d = documentDao.getDocument(doc_no);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		if(count != null && count > 20 && pdf != null){
			pdf.close();
			pdf = null;
		}
		try {
			if(pdf == null){
				String docdownload = oscarProperties.getProperty("DOCUMENT_DIR");
				File documentDir = new File(docdownload);
				log.debug("Document Dir is a dir" + documentDir.isDirectory());
				File pdfFile = new File(documentDir, d.getDocfilename());
				
				pdf = PDDocument.load(pdfFile, MemoryUsageSetting.setupMixed(maxPDFLoadBytesMemory(), -1));
				bean.setPDDocument("doc" + doc_no + "_" + request.getRequestedSessionId(), pdf);
				bean.resetPageCounter("doc" + doc_no + "_" + request.getRequestedSessionId());
			}
			bean.incrementPageCounter("doc" + doc_no + "_" + request.getRequestedSessionId());
			PDFRenderer rend = new PDFRenderer(pdf);
			BufferedImage image = null;
			
			try {
				image = rend.renderImageWithDPI(pageNum, 48f);
			} catch (IOException e){
				log.warn("Failed to render pdf preview for doc " + doc_no + " on page " + pageNum + ". If web page was closed dismiss this warning.");
			}
			
			if(image != null){
				ImageIO.write(image, "png", baos);
				org.apache.commons.io.IOUtils.copy(new ByteArrayInputStream(Base64.encode(baos.toByteArray())), response.getOutputStream());
			}
			
		} catch (IOException e) {
			log.error("Error decoding pdf file " + d.getDocfilename(), e);
			throw e;
		}
		return null;
	}
	
	public byte[] createCacheVersion2(Document d, Integer pageNum) {
		String docdownload = oscarProperties.getProperty("DOCUMENT_DIR");
		File documentDir = new File(docdownload);
		File documentCacheDir = getDocumentCacheDir(docdownload);
		log.debug("Document Dir is a dir" + documentDir.isDirectory());
		File pdfFile = new File(documentDir, d.getDocfilename());
		File pngFile = new File(documentCacheDir, d.getDocfilename() + "_" + pageNum + ".png");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			PDDocument pdf = PDDocument.load(pdfFile, MemoryUsageSetting.setupMixed(maxPDFLoadBytesMemory(), -1));
			PDFRenderer rend = new PDFRenderer(pdf);
			//Page index starts at 0, subtracts 1 to account for that
			BufferedImage image = rend.renderImageWithDPI(pageNum - 1, 144f);
			
			// write cache file
			ImageIO.write(image, "png", pngFile);

			ImageIO.write(image, "png", baos);
		} catch (IOException e) {
			log.error("Error decoding pdf file " + d.getDocfilename(), e);
		}
		return baos.toByteArray();
	}

	public File createCacheVersion(Document d) throws Exception {

		String docdownload = oscarProperties.getProperty("DOCUMENT_DIR");
		File documentDir = new File(docdownload);
		File documentCacheDir = getDocumentCacheDir(docdownload);
		log.debug("Document Dir is a dir" + documentDir.isDirectory());

		File file = new File(documentDir, d.getDocfilename());

		RandomAccessFile raf = new RandomAccessFile(file, "r");
		FileChannel channel = raf.getChannel();
		ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
		PDFFile pdffile = new PDFFile(buf);
		if(raf != null) raf.close();
		if(channel != null) channel.close();
		// long readfile = System.currentTimeMillis() - start;
		// draw the first page to an image
		PDFPage ppage = pdffile.getPage(0);

		log.debug("WIDTH " + (int) ppage.getBBox().getWidth() + " height " + (int) ppage.getBBox().getHeight());

		// get the width and height for the doc at the default zoom
		Rectangle rect = new Rectangle(0, 0, (int) ppage.getBBox().getWidth(), (int) ppage.getBBox().getHeight());

		log.debug("generate the image");
		Image img = ppage.getImage(rect.width, rect.height, // width & height
		        rect, // clip rect
		        null, // null for the ImageObserver
		        true, // fill background with white
		        true // block until drawing is done
		        );

		log.debug("about to Print to stream");
		File outfile = new File(documentCacheDir, d.getDocfilename() + ".png");

		OutputStream outs = null;
		try {
			outs = new FileOutputStream(outfile);

			RenderedImage rendImage = (RenderedImage) img;
			ImageIO.write(rendImage, "png", outs);
			outs.flush();
		} finally {
			if (outs != null) outs.close();
		}

		return outfile;

	}

	public ActionForward searchDocumentDescriptions(ActionMapping mapping,ActionForm form,HttpServletRequest request,HttpServletResponse response) throws IOException {
		if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "w", null)) {
			throw new SecurityException("missing required security object (_edoc)");
		}
		
		String keyword = request.getParameter("term");
		
		List<String> descriptions = documentDao.findDocumentDescriptions(keyword);
		JSONObject jsonObject  = new JSONObject();
		JSONArray jsonArray = new JSONArray();
		for (String desc : descriptions) {
			jsonObject.put("label", desc);
			jsonArray.add(jsonObject);
		}
		
		MiscUtils.getLogger().info(jsonArray.toString());
		response.setContentType("text/x-json");
		response.getWriter().print(jsonArray.toString());
		response.getWriter().flush();

		return null;
	}

	public ActionForward showPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return getPage(mapping, form, request, response, Integer.parseInt(request.getParameter("page")));
	}

	public ActionForward view(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
		return getPage(mapping, form, request, response, 1);
	}

	// PNG version
	public ActionForward getPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response, int pageNum) {

		try {
			String doc_no = request.getParameter("doc_no");
			log.debug("Document No :" + doc_no);

			LogAction.addLog((String) request.getSession().getAttribute("user"), LogConst.READ,
					LogConst.CON_DOCUMENT, doc_no, LoggedInInfo.obtainClientIpAddress(request));

			Document d = documentDao.getDocument(doc_no);
			log.debug("Document Name :" + d.getDocfilename());
			
			ServletOutputStream outs = response.getOutputStream();

			File outfile = hasCacheVersion(d, pageNum);
			if (outfile != null) {
				log.debug("got doc from local cache   ");
				try (FileInputStream fileInputStream = new FileInputStream(outfile)) {
					org.apache.commons.io.IOUtils.copy(fileInputStream, outs);
				} finally {
					org.apache.commons.io.IOUtils.closeQuietly(outs);
				}
			} else {
				byte[] pdfBytes = createCacheVersion2(d, pageNum);
				try (ByteArrayInputStream fileInputStream = new ByteArrayInputStream(pdfBytes)) {
					org.apache.commons.io.IOUtils.copy(fileInputStream, outs);
				} finally {
					org.apache.commons.io.IOUtils.closeQuietly(outs);
				}
			}
			response.setContentType("image/png");
			log.debug("about to Print to stream");
			response.setHeader("Content-Disposition", "attachment;filename=\"" + d.getDocfilename() + "\"");
		} catch (java.net.SocketException se) {
			MiscUtils.getLogger().error("Error", se);
		} catch (Exception e) {
			MiscUtils.getLogger().error("Error", e);
		}
		return null;
	}

	public ActionForward viewDocPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		
		if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "r", null)) {
        	throw new SecurityException("missing required security object (_edoc)");
        }
		
		log.debug("in viewDocPage");
		try {
			String doc_no = request.getParameter("doc_no");
			String pageNum = request.getParameter("curPage");
			if (pageNum == null) {
				pageNum = "0";
			}
			Integer pn = Integer.parseInt(pageNum);
			log.debug("Document No :" + doc_no);
			LogAction.addLog((String) request.getSession().getAttribute("user"), LogConst.READ,
					LogConst.CON_DOCUMENT, doc_no, LoggedInInfo.obtainClientIpAddress(request));

			Document d = documentDao.getDocument(doc_no);
			log.debug("Document Name :" + d.getDocfilename());
			//if the file is not a pdf, use display function
			if(!(d.getContenttype().equals("application/pdf") || d.getDocfilename().endsWith(".pdf"))) {
				display(mapping, form, request, response);
				return null;
			}
			String name = d.getDocfilename() + "_" + pn + ".png";
			log.debug("name " + name);

			ServletOutputStream outs = response.getOutputStream();
			File outfile = hasCacheVersion2(d, pn);
			if (outfile != null) {
				log.debug("got doc from local cache   ");
				try (FileInputStream fileInputStream = new FileInputStream(outfile)) {
					org.apache.commons.io.IOUtils.copy(fileInputStream, outs);
				} finally {
					org.apache.commons.io.IOUtils.closeQuietly(outs);
				}
			} else {
				byte[] pdfBytes = createCacheVersion2(d, pn);
				try (ByteArrayInputStream fileInputStream = new ByteArrayInputStream(pdfBytes)) {
					org.apache.commons.io.IOUtils.copy(fileInputStream, outs);
				} finally {
					org.apache.commons.io.IOUtils.closeQuietly(outs);
				}
			}
			response.setContentType("image/png");
			response.setHeader("Content-Disposition", "attachment;filename=\"" + d.getDocfilename() + "\"");
		} catch (java.net.SocketException se) {
			MiscUtils.getLogger().error("Error", se);
		} catch (Exception e) {
			MiscUtils.getLogger().error("Error", e);
		}
		return null;
	}

	public ActionForward view2(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

		if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "r", null)) {
        	throw new SecurityException("missing required security object (_edoc)");
        }
		
		String doc_no = request.getParameter("doc_no");
		log.debug("Document No :" + doc_no);

		LogAction.addLog((String) request.getSession().getAttribute("user"), LogConst.READ,
				LogConst.CON_DOCUMENT, doc_no, LoggedInInfo.obtainClientIpAddress(request));

		String docdownload = oscarProperties.getProperty("DOCUMENT_DIR");
		File documentDir = new File(docdownload);
		log.debug("Document Dir is a dir" + documentDir.isDirectory());

		Document d = documentDao.getDocument(doc_no);
		log.debug("Document Name :" + d.getDocfilename());

		// TODO: Right now this assumes it's a pdf which it shouldn't

		response.setContentType("image/png");
		// response.setHeader("Content-Disposition", "attachment;filename=\"" + filename+ "\"");
		// read the file name.
		File file = new File(documentDir, d.getDocfilename());

		RandomAccessFile raf = new RandomAccessFile(file, "r");
		FileChannel channel = raf.getChannel();
		ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
		PDFFile pdffile = new PDFFile(buf);
		// long readfile = System.currentTimeMillis() - start;
		// draw the first page to an image
		PDFPage ppage = pdffile.getPage(0);

		log.debug("WIDTH " + (int) ppage.getBBox().getWidth() + " height " + (int) ppage.getBBox().getHeight());

		// get the width and height for the doc at the default zoom
		Rectangle rect = new Rectangle(0, 0, (int) ppage.getBBox().getWidth(), (int) ppage.getBBox().getHeight());

		log.debug("generate the image");
		Image img = ppage.getImage(rect.width, rect.height, // width & height
		        rect, // clip rect
		        null, // null for the ImageObserver
		        true, // fill background with white
		        true // block until drawing is done
		        );

		log.debug("about to Print to stream");
		ServletOutputStream outs = response.getOutputStream();

		RenderedImage rendImage = (RenderedImage) img;
		ImageIO.write(rendImage, "png", outs);
		outs.flush();
		outs.close();
		return null;
	}

	public ActionForward getDocPageNumber(final ActionMapping mapping, final ActionForm form,
			final HttpServletRequest request, final HttpServletResponse response) {

		if (!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc",
				"r", null)) {
			throw new SecurityException("missing required security object (_edoc)");
		}

		String doc_no = request.getParameter("doc_no");
		String docdownload = oscarProperties.getProperty("DOCUMENT_DIR");
		// File documentDir = new File(docdownload);
		Document d = documentDao.getDocument(doc_no);
		String filePath = docdownload + d.getDocfilename();

		int numOfPage = 0;
		try {
			PDDocument reader = PDDocument.load(new File(filePath));
			numOfPage = reader.getNumberOfPages();

			HashMap hm = new HashMap();
			hm.put("numOfPage", numOfPage);
			JSONObject jsonObject = JSONObject.fromObject(hm);
			response.getOutputStream().write(jsonObject.toString().getBytes());
		} catch (IOException e) {
			MiscUtils.getLogger().error("Error", e);
		}

		return null;
	}

	public ActionForward downloadCDS(final ActionMapping mapping, final ActionForm form,
			final HttpServletRequest request, final HttpServletResponse response) throws Exception {

		if (!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc",
				"r", null)) {
			throw new SecurityException("missing required security object (_edoc)");
		}

		val demographicExport = SharingCenterUtil.retrieveDemographicExport(
				Integer.valueOf(request.getParameter("doc_no")));
		String contentType = "application/zip";
		val fileName = String.format("%s_%s", demographicExport.getDocumentType(), demographicExport.getId());
		byte[] contentBytes = demographicExport.getDocument();

		response.setContentType(contentType);
		response.setContentLength(contentBytes.length);
		response.setHeader("Content-Disposition", "inline; filename=\"" + fileName + "\"");
		log.debug("about to Print to stream");
		ServletOutputStream outs = response.getOutputStream();
		outs.write(contentBytes);
		outs.flush();
		outs.close();
		return null;
	}

	public ActionForward display(final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
			final HttpServletResponse response) throws Exception {

		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);

		if (!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc",
				"r", null)) {
			throw new SecurityException("missing required security object (_edoc)");
		}

		Integer remoteFacilityId = null;
		String docxml = null;
		String contentType = null;
		byte[] contentBytes = null;
		String filename = null;
		var documentDownload = "";

		if (Boolean.parseBoolean(request.getParameter("moh"))) {
			documentDownload = oscarProperties.getProperty("DOCUMENT_DIR");
			val documentDir = new File(documentDownload);
			log.debug("Document Dir is a dir" + documentDir.isDirectory());
			docxml = "";
			contentType = "application/pdf";
			val file = new File(documentDir, request.getParameter("filename"));
			if (file.exists()) {
				contentBytes = FileUtils.readFileToByteArray(file);
			} else {
				throw new IllegalStateException("Local document doesn't exist " + file.getAbsolutePath());
			}
		} else {
			val temp = request.getParameter("remoteFacilityId");
			if (temp != null) {
				remoteFacilityId = Integer.parseInt(temp);
			}
			val documentNumber = request.getParameter("doc_no");
			log.debug("Doctor No :" + documentNumber);
			val demographicNumber = request.getParameter("demoNo");

			// local document
			if (remoteFacilityId == null) {
				val ctld = ctlDocumentDao.getCtrlDocument(Integer.parseInt(documentNumber));
				if (ctld.isDemographicDocument()) {
					LogAction.addLog((String) request.getSession().getAttribute("user"), LogConst.READ,
							LogConst.CON_DOCUMENT, documentNumber, LoggedInInfo.obtainClientIpAddress(request),
							"" + ctld.getId().getModuleId());
				} else {
					LogAction.addLog((String) request.getSession().getAttribute("user"), LogConst.READ,
							LogConst.CON_DOCUMENT, documentNumber, LoggedInInfo.obtainClientIpAddress(request));
				}
				documentDownload = oscarProperties.getProperty("DOCUMENT_DIR");
				val documentDir = new File(documentDownload);
				log.debug("Document Dir is a dir" + documentDir.isDirectory());
				val document = documentDao.getDocument(documentNumber);
				log.debug("Document Name :" + document.getDocfilename());

				docxml = document.getDocxml();
				contentType = document.getContenttype();
				File file = new File(documentDir, document.getDocfilename());
				filename = document.getDocfilename();

				if (file.exists()) {
					contentBytes = FileUtils.readFileToByteArray(file);
				} else {
					if (docxml == null || docxml.trim().equals("")) {
						// Only throw exception if the file does not exist and the docxml is null/empty to serve HTML files that were uploaded in OSCAR 12,
						// where HTML file uploads contents were stored in the docxml field of the document table, and the file was never saved.
						throw new IllegalStateException(
								"Local document doesn't exist for eDoc (ID " + document.getId() + "): "
										+ file.getAbsolutePath());
					}
				}
			} else // remote document
			{
				FacilityIdIntegerCompositePk remotePk = new FacilityIdIntegerCompositePk();
				remotePk.setIntegratorFacilityId(remoteFacilityId);
				remotePk.setCaisiItemId(Integer.parseInt(documentNumber));
				CachedDemographicDocument remoteDocument = null;
				CachedDemographicDocumentContents remoteDocumentContents = null;
				try {
					if (!CaisiIntegratorManager.isIntegratorOffline(request.getSession())) {
						val demographicWs = CaisiIntegratorManager.getDemographicWs(loggedInInfo,
								loggedInInfo.getCurrentFacility());
						remoteDocument = demographicWs.getCachedDemographicDocument(remotePk);
						remoteDocumentContents = demographicWs.getCachedDemographicDocumentContents(remotePk);
					}
				} catch (Exception e) {
					MiscUtils.getLogger().error("Unexpected error.", e);
					CaisiIntegratorManager.checkForConnectionError(request.getSession(), e);
				}

				if (CaisiIntegratorManager.isIntegratorOffline(request.getSession())) {
					val demographicId = IntegratorFallBackManager.getDemographicNoFromRemoteDocument(
							loggedInInfo, remotePk);
					MiscUtils.getLogger().debug("got demographic no from remote document " + demographicId);
					val remoteDocuments = IntegratorFallBackManager.getRemoteDocuments(
							loggedInInfo, demographicId);
					for (CachedDemographicDocument demographicDocument : remoteDocuments) {
						if (demographicDocument.getFacilityIntegerPk().getIntegratorFacilityId()
								== remotePk.getIntegratorFacilityId()
								&& demographicDocument.getFacilityIntegerPk().getCaisiItemId()
								== remotePk.getCaisiItemId()) {
							remoteDocument = demographicDocument;
							remoteDocumentContents = IntegratorFallBackManager.getRemoteDocument(loggedInInfo,
									demographicId, remotePk);
							break;
						}
						MiscUtils.getLogger().error("End of the loop and didn't find the remoteDocument");
					}
				}

				docxml = remoteDocument.getDocXml();
				contentType = remoteDocument.getContentType();
				filename = remoteDocument.getDocFilename();
				contentBytes = remoteDocumentContents.getFileContents();
			}

			if (docxml != null && !docxml.trim().equals("")) {
				ServletOutputStream outs = response.getOutputStream();
				outs.write(docxml.getBytes());
				outs.flush();
				outs.close();
				return null;
			}

			if (contentType == null) {
				contentType = "application/pdf";
			}

			val data = "Doctor Number =" + documentNumber;
			LogAction.addLog(loggedInInfo, LogConst.READ, "Document", null, demographicNumber, data);

		}

		response.setContentType(contentType);
		response.setContentLength(contentBytes.length);
		response.setHeader("Content-Disposition", "inline; filename=\"" + filename + "\"");
		log.debug("about to Print to stream");
		ServletOutputStream outs = response.getOutputStream();
		outs.write(contentBytes);
		outs.flush();
		outs.close();
		return null;
	}

	public ActionForward fax(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		String docId = request.getParameter("docId");
		String[] tmpRecipients = request.getParameterValues("faxRecipients");
		String provider_no = loggedInInfo.getLoggedInProviderNo();
		Document doc = documentDao.getDocument(docId);
		PatientLabRoutingDao patientLabRoutingDao = SpringUtils.getBean(PatientLabRoutingDao.class);

		String demoNo = request.getParameter("demoNo");

		for (int i = 0; tmpRecipients != null && i < tmpRecipients.length; i++) {
			tmpRecipients[i] = tmpRecipients[i].trim().replaceAll("[^0-9]", "");
		}
		ArrayList<String> recipients = tmpRecipients == null ? new ArrayList<String>() : new ArrayList<String>(Arrays.asList(tmpRecipients));

		// Removing duplicate phone numbers.
		recipients = new ArrayList<String>(new HashSet<String>(recipients));

		// Writing consultation request to disk as a pdf.
		String path =  oscarProperties.getProperty("DOCUMENT_DIR");
		String faxPdf = String.format("%s%s%s", path, File.separator, doc.getDocfilename());
		String faxClinicId = oscarProperties.getProperty("fax_clinic_id","1234");
		String faxNumber = "";
		ClinicDAO clinicDAO = SpringUtils.getBean(ClinicDAO.class);
		if(!faxClinicId.equals("") && clinicDAO.find(Integer.parseInt(faxClinicId))!=null){
			faxNumber = clinicDAO.find(Integer.parseInt(faxClinicId)).getClinicFax();
			faxNumber = faxNumber.replaceAll("[^0-9]", "");
		}

		FaxConfigDao faxConfigDao = SpringUtils.getBean(FaxConfigDao.class);
		val faxConfigs = faxConfigDao.findAll(null, null);
		val pdfBytes = Files.readAllBytes(Paths.get(faxPdf));

		val sendFaxData = SendFaxData.builder()
				.providerNumber(provider_no)
				.demographicNumber(Integer.parseInt(demoNo))
				.faxFileType(FaxFileType.DOCUMENT)
				.cookies(request.getCookies())
				.faxLine(faxNumber)
				.pdfDocumentBytes(pdfBytes)
				.loggedInInfo(loggedInInfo)
				.existingFilePath(faxPdf)
				.build();

		for (int i = 0; i < recipients.size(); i++) {
			String faxNo = recipients.get(i).replaceAll("\\D", "");
			if (faxNo.length() < 7) {
				throw new DocumentException("Document target fax number '" + faxNo + "' is invalid.");
			}
			String tempName = "DOC-" + faxClinicId + docId + "." + String.valueOf(i) + "."
					+ System.currentTimeMillis();

			sendFaxData.setDestinationFaxNumber(faxNo);
			sendFaxData.setFileName(tempName);
			faxFacade.sendFax(sendFaxData);
		}

		val validFaxConfig = !faxConfigs.isEmpty();

		if (!validFaxConfig) {
			log.error("PROBLEM CREATING FAX JOB", new DocumentException("There are no fax configurations setup for this clinic."));
		}

		LogAction.addLog(provider_no, LogConst.SENT, LogConst.CON_FAX, "DOCUMENT  "+ docId);
		request.getSession().setAttribute("faxSuccessful", validFaxConfig);

		return null;
	}

        public ActionForward viewDocumentInfo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
                response.setContentType("text/html");
		doViewDocumentInfo(request, response.getWriter(),true,true);
		return null;
	}
        
        public ActionForward viewDocumentDescription(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
                response.setContentType("text/html");
		doViewDocumentInfo(request, response.getWriter(),false,true);
		return null;
	}
        public ActionForward viewAnnotationAcknowledgementTickler(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
                response.setContentType("text/html");
		doViewDocumentInfo(request, response.getWriter(),true,false);
		return null;
	}
     
        public void doViewDocumentInfo(HttpServletRequest request, PrintWriter out,boolean viewAnnotationAcknowledgementTicklerFlag, boolean viewDocumentDescriptionFlag) {
        	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
        	
        	if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "r", null)) {
            	throw new SecurityException("missing required security object (_edoc)");
            }
        	
            String doc_no = request.getParameter("doc_no");
            Locale locale=request.getLocale();
                
            String annotation= "",acknowledgement="",tickler="";
                if(doc_no!=null && doc_no.length()>0) { 
                    annotation=EDocUtil.getHtmlAnnotation(doc_no);
                    acknowledgement=EDocUtil.getHtmlAcknowledgement(locale,doc_no);
                    if(acknowledgement==null) {acknowledgement="";}
                    tickler=EDocUtil.getHtmlTicklers(loggedInInfo, doc_no);
                }
                
                out.println("<!DOCTYPE html><html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'></head><body>");
                
                if(viewAnnotationAcknowledgementTicklerFlag) {
                    if(annotation.length()>0)  {
                        out.println(annotation);
                    }
                    if(tickler.length()>0)
                    {
                        out.println(tickler+"<br>");
                    }
                    if(acknowledgement.length()>0) {
                        out.println(acknowledgement+"<br>");
                    }
                }
                
                if(viewDocumentDescriptionFlag) {
                    EDoc curDoc= EDocUtil.getDoc(doc_no);
                    ResourceBundle props = ResourceBundle.getBundle("oscarResources", locale);
                    out.println("<br>"+props.getString("dms.documentBrowser.DocumentUpdated")+": "+curDoc.getDateTimeStamp());
                    out.println("<br>"+props.getString("dms.documentBrowser.ContentUpdated")+": "+curDoc.getContentDateTime());
                    out.println("<br>"+props.getString("dms.documentBrowser.ObservationDate")+": "+curDoc.getObservationDate());
                    out.println("<br>"+props.getString("dms.documentBrowser.Type")+": "+curDoc.getType());
                    out.println("<br>"+props.getString("dms.documentBrowser.Class")+": "+curDoc.getDocClass());
                    out.println("<br>"+props.getString("dms.documentBrowser.Subclass")+": "+curDoc.getDocSubClass());
                    out.println("<br>"+props.getString("dms.documentBrowser.Description")+": "+curDoc.getDescription());
                    out.println("<br>"+props.getString("dms.documentBrowser.Creator")+": "+Encode.forHtml(curDoc.getCreatorName()));
                    out.println("<br>"+props.getString("dms.documentBrowser.Responsible")+": "+Encode.forHtml(curDoc.getResponsibleName()));
                    if (!curDoc.getReviews().isEmpty() && curDoc.getReviews().get(0) != null && curDoc.getReviews().get(0).getReviewer() != null) {
						out.println("<br>"+props.getString("dms.documentBrowser.Reviewer")+": "+ Encode.forHtml(curDoc.getReviews().get(0).getReviewer().getFormattedName()));
					}
                    out.println("<br>"+props.getString("dms.documentBrowser.Source")+": "+curDoc.getSource());
                }
                
                out.println("</body></html>");
		out.flush();
                out.close();
                
        }

        public ActionForward addIncomingDocument(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) throws Exception {

        String pdfDir = request.getParameter("pdfDir");
        String pdfName = request.getParameter("pdfName");
        String demographic_no = request.getParameter("demog");
        String observationDate = request.getParameter("observationDate");
        String documentDescription = request.getParameter("documentDescription");
        String docType = request.getParameter("docType");
        String docClass = request.getParameter("docClass");
        String docSubClass = request.getParameter("docSubClass");
        String[] flagproviders = request.getParameterValues("flagproviders");
        String queueId1 = request.getParameter("queueId");
        String sourceFilePath = IncomingDocUtil.getIncomingDocumentFilePathName(queueId1, pdfDir, pdfName);
        String destFilePath;

        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "w", null)) {
        	throw new SecurityException("missing required security object (_edoc)");
        }
        
        String savePath = oscarProperties.getProperty("DOCUMENT_DIR");
        if (!savePath.endsWith(File.separator)) {
            savePath += File.separator;
        }

        Date obDate = UtilDateUtilities.StringToDate(observationDate);
        String formattedDate = UtilDateUtilities.DateToString(obDate, EDocUtil.DMS_DATE_FORMAT);
        String source = "";


        int numberOfPages = 0;
        String fileName = pdfName;
        String user = (String) request.getSession().getAttribute("user");
        EDoc newDoc = new EDoc(documentDescription, docType, fileName, "", user, user, source, 'A', formattedDate, "", "", "demographic", demographic_no, 0);
        
        // if the document was added in the context of a program
		ProgramManager2 programManager = SpringUtils.getBean(ProgramManager2.class);
		LoggedInInfo loggedInInfo  = LoggedInInfo.getLoggedInInfoFromSession(request);
		ProgramProvider pp = programManager.getCurrentProgramInDomain(loggedInInfo, loggedInInfo.getLoggedInProviderNo());
		if(pp != null && pp.getProgramId() != null) {
			newDoc.setProgramId(pp.getProgramId().intValue());
		}
		
        newDoc.setDocClass(docClass);
        newDoc.setDocSubClass(docSubClass);
        newDoc.setDocPublic("0");
        fileName = newDoc.getFileName();
        destFilePath = savePath + fileName;
        String doc_no = "";


        newDoc.setContentType(docType);
        File f1 = new File(sourceFilePath);
        boolean success = f1.renameTo(new File(destFilePath));
        if (!success) {
            log.error("Not able to move " + f1.getName() + " to " + destFilePath);
            // File was not successfully moved
        } else {

            newDoc.setContentType("application/pdf");
            if (fileName.endsWith(".PDF") || fileName.endsWith(".pdf")) {
                newDoc.setContentType("application/pdf");
                numberOfPages = countNumOfPages(fileName);
            }
            newDoc.setNumberOfPages(numberOfPages);
            doc_no = EDocUtil.addDocumentSQL(newDoc);
					LogAction.addLog((String) request.getSession().getAttribute("user"), LogConst.ADD,
							LogConst.CON_DOCUMENT, doc_no, LoggedInInfo.obtainClientIpAddress(request));

            if (flagproviders != null && flagproviders.length > 0) { 
                try {
                    for (String proNo : flagproviders) {
                        providerInboxRoutingDAO.addToProviderInbox(proNo, Integer.parseInt(doc_no), LabResultData.DOCUMENT);
                    }
                } catch (Exception e) {
                    MiscUtils.getLogger().error("Error", e);
                }
            }

            //Check to see if we have to route document to patient
            PatientLabRoutingDao patientLabRoutingDao = SpringUtils.getBean(PatientLabRoutingDao.class);
            List<PatientLabRouting> patientLabRoutingList = patientLabRoutingDao.findByLabNoAndLabType(Integer.parseInt(doc_no), docType);
            if (patientLabRoutingList == null || patientLabRoutingList.isEmpty()) {
                PatientLabRouting patientLabRouting = new PatientLabRouting();
                patientLabRouting.setDemographicNo(Integer.parseInt(demographic_no));
                patientLabRouting.setLabNo(Integer.parseInt(doc_no));
                patientLabRouting.setLabType("DOC");
                patientLabRoutingDao.persist(patientLabRouting);
            }

            try {

                CtlDocument ctlDocument = ctlDocumentDao.getCtrlDocument(Integer.parseInt(doc_no));

                ctlDocument.getId().setModuleId(Integer.parseInt(demographic_no));
                ctlDocumentDao.merge(ctlDocument);
                //save a document created note
                if (ctlDocument.isDemographicDocument()) {
                    //save note
                    saveDocNote(request, documentDescription, demographic_no, doc_no);
                }
            } catch (Exception e) {
                MiscUtils.getLogger().error("Error", e);
            }
        }

        return (mapping.findForward("nextIncomingDoc"));
    }
        
    public ActionForward viewIncomingDocPageAsPdf(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "r", null)) {
        	throw new SecurityException("missing required security object (_edoc)");
        }

        String pageNum = request.getParameter("curPage");
        String queueId = request.getParameter("queueId");
        String pdfDir = request.getParameter("pdfDir");
        String pdfName = request.getParameter("pdfName");
        String filePath = IncomingDocUtil.getIncomingDocumentFilePathName(queueId, pdfDir, pdfName);
        Locale locale=request.getLocale();
        ResourceBundle props = ResourceBundle.getBundle("oscarResources", locale);
        
        if (pageNum == null) {
            pageNum = "0";
        }

        int pageNumber = Integer.parseInt(pageNum);

        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "inline; filename=\"" + pdfName + UtilDateUtilities.getToday("yyyy-MM-dd.hh.mm.ss") + ".pdf\"");

        try {
            PDDocument reader = PDDocument.load(new File(filePath));
            PDDocument extractedPage = new PDDocument();
            extractedPage.addPage(reader.getDocumentCatalog().getPages().get(pageNumber - 1));
            extractedPage.save(response.getOutputStream());
            extractedPage.close();
            reader.close();
        } catch (Exception ex) {
            response.setContentType("text/html");
            response.getWriter().print(props.getString("dms.incomingDocs.errorInOpening") + pdfName);
            response.getWriter().print("<br>"+props.getString("dms.incomingDocs.PDFCouldBeCorrupted"));

            MiscUtils.getLogger().error("Error", ex);
        }

        return null;
    }

    public int countNumOfPages(String fileName) { // count number of pages in a pdf file
        int numOfPage = 0;
        String docdownload = oscarProperties.getProperty("DOCUMENT_DIR");

        if (!docdownload.endsWith(File.separator)) {
            docdownload += File.separator;
        }

        String filePath = docdownload + fileName;

        try {
            PDDocument reader = PDDocument.load(new File(filePath));
            numOfPage = reader.getNumberOfPages();

            reader.close();
        } catch (IOException e) {
            MiscUtils.getLogger().error("Error", e);
        }
        return numOfPage;
    }

    public ActionForward displayIncomingDocs(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

    	if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "r", null)) {
        	throw new SecurityException("missing required security object (_edoc)");
        }
    	
        String queueId = request.getParameter("queueId");
        String pdfDir = request.getParameter("pdfDir");
        String pdfName = request.getParameter("pdfName");
        String filePath = IncomingDocUtil.getIncomingDocumentFilePathName(queueId, pdfDir, pdfName);

        String contentType = "application/pdf";
        File file = new File(filePath);

        response.setContentType(contentType);
        response.setContentLength((int) file.length());
        response.setHeader("Content-Disposition", "inline; filename=\"" + pdfName + "\"");

        BufferedInputStream bfis = null;
        ServletOutputStream outs = response.getOutputStream();

        try {

            bfis = new BufferedInputStream(new FileInputStream(file));

            org.apache.commons.io.IOUtils.copy(bfis,outs);
            outs.flush();
            
        } catch (Exception e) {
            MiscUtils.getLogger().error("Error", e);
        } finally {
            if (bfis != null) {
                bfis.close();
            }
        }

        return null;
    }
        
    public ActionForward viewIncomingDocPageAsImage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {


    	if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_edoc", "r", null)) {
        	throw new SecurityException("missing required security object (_edoc)");
        }
    	
        String pageNum = request.getParameter("curPage");
        String queueId = request.getParameter("queueId");
        String pdfDir = request.getParameter("pdfDir");
        String pdfName = request.getParameter("pdfName");

        if (pageNum == null) {
            pageNum = "0";
        }

        BufferedInputStream bfis = null;
        ServletOutputStream outs = null;

        try {
            Integer pn = Integer.parseInt(pageNum);
            File outfile = createIncomingCacheVersion(queueId, pdfDir, pdfName, pn);
            outs = response.getOutputStream();

            if (outfile != null) {
                bfis = new BufferedInputStream(new FileInputStream(outfile));


                response.setContentType("image/png");
                response.setHeader("Content-Disposition", "inline;filename=\"" + pdfName + "\"");
                org.apache.commons.io.IOUtils.copy(bfis,outs);
                outs.flush();
                
            } else {
                log.info("Unable to retrieve content for " + queueId + "/" + pdfDir + "/" + pdfName);
            }
        } catch (Exception e) {
            MiscUtils.getLogger().error("Error", e);

        } finally {
            if (bfis != null) {
                bfis.close();
            }
        }
        return null;
    }

    public File createIncomingCacheVersion(String queueId, String pdfDir, String pdfName, Integer pageNum) throws Exception {


        String incomingDocPath = IncomingDocUtil.getIncomingDocumentFilePath(queueId, pdfDir);
        File documentDir = new File(incomingDocPath);
        File documentCacheDir = getDocumentCacheDir(incomingDocPath);
        File file = new File(documentDir, pdfName);

        PdfDecoder decode_pdf = new PdfDecoder(true);
        File ofile = null;
        FileInputStream is = null;
        
        try {

            FontMappings.setFontReplacements();

            decode_pdf.useHiResScreenDisplay(true);

            decode_pdf.setExtractionMode(0, 96, 96 / 72f);

            is = new FileInputStream(file);

            decode_pdf.openPdfFileFromInputStream(is, false);

            BufferedImage image_to_save = decode_pdf.getPageAsImage(pageNum);

            decode_pdf.getObjectStore().saveStoredImage(documentCacheDir.getCanonicalPath() + "/" + pdfName + "_" + pageNum + ".png", image_to_save, true, false, "png");

            decode_pdf.flushObjectValues(true);

        } catch (Exception e) {
            log.error("Error decoding pdf file " + pdfDir + pdfName);
        } finally {
            if (decode_pdf != null) {
                decode_pdf.closePdfFile();
            }
            if (is!=null) {
                is.close();
            }
                
        }
        
        ofile = new File(documentCacheDir, pdfName + "_" + pageNum + ".png");
        
        return ofile;

    }

	public ActionForward findForwardedProviderNames(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		List<Map<String, String>> providers = new ArrayList<>();
		val providerNo = request.getParameter("providerNo");
		val docId = request.getParameter("docId");
		this.findForwardedProviders(providers, providerNo, providerNo, docId);
		val json = new JSONObject();
		json.put("success", true);
		json.put("providers", providers);
		json.put("test", true);
		response.setContentType("text/x-json");
		json.write(response.getWriter());		
		
		return null;
	}

	public void findForwardedProviders(List<Map<String, String>> providers, String providerNo, String mrpProviderNo, String docId) {
		val forwardedRules = forwardingRules.getForwardRulesAndProviders(providerNo);
		for (val object : forwardedRules) {
			val forwardRules = (IncomingLabRules) object[0];
			val provider = (Provider) object[1];
			HashMap<String, String> providerMap = new HashMap<>();
			providerMap.put("providerNo", Encode.forJavaScript(provider.getProviderNo()));
			providerMap.put("providerName", Encode.forJavaScript(provider.getFormattedName()));
			if (!providers.contains(providerMap)) {
				val routings = providerLabRoutingDao.findByLabNoAndLabTypeAndProviderNo(
								Integer.parseInt(docId), "DOC", forwardRules.getFrwdProviderNo());
				if (forwardRules.getForwardTypeStrings().contains("DOC") && routings.isEmpty() && !provider.getProviderNo().equals(mrpProviderNo)) {
					providers.add(providerMap);
				}
				this.findForwardedProviders(providers, forwardRules.getFrwdProviderNo(), mrpProviderNo, docId);
			}
		}
	}
}
