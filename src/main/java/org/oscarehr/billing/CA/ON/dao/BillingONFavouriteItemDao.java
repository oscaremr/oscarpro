package org.oscarehr.billing.CA.ON.dao;

import org.oscarehr.billing.CA.ON.model.BillingONFavouriteItem;
import org.oscarehr.common.dao.AbstractDao;

import javax.persistence.Query;
import java.util.List;

public class BillingONFavouriteItemDao extends AbstractDao<BillingONFavouriteItem> {
    protected BillingONFavouriteItemDao(Class<BillingONFavouriteItem> modelClass) {
        super(modelClass);
    }
    
    public List<BillingONFavouriteItem> findAllByFavourite(Integer favouriteId) {
        Query q = entityManager.createQuery("SELECT b FROM BillingONFavouriteItem b WHERE b.favouriteId = ?");
        q.setParameter(1, favouriteId);

        @SuppressWarnings("unchecked")
        List<BillingONFavouriteItem> results = q.getResultList();

        return results;
    }
}
