/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.billing.CA.ON.web;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import oscar.OscarProperties;
import oscar.oscarBilling.ca.on.pageUtil.BillingRAPrep;
import oscar.oscarDemographic.pageUtil.Util;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

public class RaSummaryAction extends DispatchAction {
    
    private OscarProperties oscarProperties = OscarProperties.getInstance();
    public static final char CSV_SEPARATOR = ',';

    public ActionForward generate(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward generateCsv(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RaSummaryForm raSummaryForm = (RaSummaryForm) form;

        String raNumber = raSummaryForm.getRaNo();
        String providerNo = raSummaryForm.getProviderNo();
        
        BillingRAPrep billingRaPrep = new BillingRAPrep();
        List<String> ObBillingNos = billingRaPrep.getRaBillingNoForObCodes(raNumber);
        List<String> CoBillingNos = billingRaPrep.getRaBillingNoForColposcopyCodes(raNumber);
        List raSummary = new ArrayList();
        Hashtable map = new Hashtable();
        if (providerNo == null || providerNo.isEmpty() || providerNo.equals("all")) { // raNo for all providers
            raSummary = billingRaPrep.getRASummary(raNumber, ObBillingNos, CoBillingNos);
        } else {
            raSummary = billingRaPrep.getRASummary(raNumber, providerNo, ObBillingNos, CoBillingNos, map);
        }
        
        File csvFile = createCsvFile(raSummary);
        Util.downloadFile(csvFile.getName(), oscarProperties.getProperty("TMP_DIR"), response);
        return null;
    }
    
    private File createCsvFile(List raSummary) {
        String directory = oscarProperties.getProperty("TMP_DIR");
        File csvFile = new File(directory, "summary.csv");

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(csvFile.getAbsolutePath()))) {
            writer.append("Billing No").append(CSV_SEPARATOR)
                    .append("Claim No").append(CSV_SEPARATOR)
                    .append("Patient").append(CSV_SEPARATOR)
                    .append("Fam Doc").append(CSV_SEPARATOR)
                    .append("HIN").append(CSV_SEPARATOR)
                    .append("Service Date").append(CSV_SEPARATOR)
                    .append("Service Code").append(CSV_SEPARATOR)
                    .append("Invoiced").append(CSV_SEPARATOR)
                    .append("Paid").append(CSV_SEPARATOR)
                    .append("Clinic Pay").append(CSV_SEPARATOR)
                    .append("Hospital Pay").append(CSV_SEPARATOR)
                    .append("OB").append(CSV_SEPARATOR)
                    .append("Error").append(CSV_SEPARATOR)
                    .append("site").append(CSV_SEPARATOR).append(System.lineSeparator());
            
            for (Object object : raSummary) {
                if (object instanceof Properties) {
                    Properties props = (Properties) object;
                    writer.append(props.getProperty("account", "")).append(CSV_SEPARATOR)
                            .append(props.getProperty("claimNo", "")).append(CSV_SEPARATOR)
                            .append(StringEscapeUtils.escapeCsv(props.getProperty("demo_name", ""))).append(CSV_SEPARATOR)
                            .append(props.getProperty("demo_doc", "")).append(CSV_SEPARATOR)
                            .append(props.getProperty("demo_hin", "")).append(CSV_SEPARATOR)
                            .append(props.getProperty("servicedate", "")).append(CSV_SEPARATOR)
                            .append(props.getProperty("servicecode", "")).append(CSV_SEPARATOR)
                            .append(props.getProperty("amountsubmit", "")).append(CSV_SEPARATOR)
                            .append(props.getProperty("amountpay", "")).append(CSV_SEPARATOR)
                            .append(props.getProperty("clinicPay", "")).append(CSV_SEPARATOR)
                            .append(props.getProperty("hospitalPay", "")).append(CSV_SEPARATOR)
                            .append(props.getProperty("obPay", "")).append(CSV_SEPARATOR)
                            .append(props.getProperty("explain", "")).append(CSV_SEPARATOR)
                            .append(props.getProperty("site", "")).append(CSV_SEPARATOR).append(System.lineSeparator());
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return csvFile;
    }
}