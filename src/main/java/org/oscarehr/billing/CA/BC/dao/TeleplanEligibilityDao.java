package org.oscarehr.billing.CA.BC.dao;

import javax.persistence.Query;

import org.oscarehr.billing.CA.BC.model.TeleplanEligibility;
import org.oscarehr.common.dao.AbstractDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.util.SpringUtils;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Repository
public class TeleplanEligibilityDao  extends AbstractDao<TeleplanEligibility> {
    
    public TeleplanEligibilityDao() { super(TeleplanEligibility.class); }

    /**
     * Gets most recent eligibility for a demographic
     *
     * @param demographicNo
     * 		Demographic number to find eligibility for
     * @return
     * 		Returns the most recent eligibility {@link TeleplanEligibility} instance or null if none exist
     */
    public TeleplanEligibility findByDemographicNo(Integer demographicNo) {
        Query query = entityManager.createQuery("FROM TeleplanEligibility te WHERE te.demographicNo = :demographicNo ORDER BY dateChecked DESC");
        query.setParameter("demographicNo", demographicNo);

        List<TeleplanEligibility> results = query.getResultList();
        if (!results.isEmpty())
        {
            return results.get(0);
        }
        return null;
    }

    /**
     * Get the display code to display on the schedule screen for this demographic and date of service
     * 
     * @param demographicNo
     *      Demographic number to find eligibility for
     * @param dateOfService
     *      Date of service for the check
     * @return
     *      Returns one of the following
     *      "+" Eligible and last checked within the date of service calendar month
     *      "-" Ineligible and last checked within the date of service calendar month
     *      "#+" Eligible and last checked before the date of service calendar month
     *      "#-" Ineligible and last checked before the date of service calendar month
     *      "#" No eligibility on record
     *      "?" Invalid or blank PHN
     *      "^" Out of province
     */
    public String getEligibilityDisplayCode(Integer demographicNo, Date dateOfService){
        String result = "#";

        DemographicDao demographicDao = (DemographicDao) SpringUtils.getBean("demographicDao");
        Demographic demographic = demographicDao.getDemographic(demographicNo);
        
        if (demographic == null) {
            // Cannot find Demographic
            result = "?";
        } else if (!demographic.getHcType().equalsIgnoreCase("BC")) {
            // Out of province (not BC)
            result = "^";
        } else if (!validatePHN(demographic.getHin())) {
            //HIN Invalid
            result = "?";
        } else {
            Calendar thisMonth = Calendar.getInstance();
            thisMonth.setTime(dateOfService);
            thisMonth.set(Calendar.DAY_OF_MONTH, 1);
            thisMonth.set(Calendar.MILLISECOND, 0);
            thisMonth.set(Calendar.SECOND, 0);
            thisMonth.set(Calendar.MINUTE, 0);
            thisMonth.set(Calendar.HOUR_OF_DAY, 0);

            TeleplanEligibility teleplanEligibility = findByDemographicNo(demographicNo);

            if (teleplanEligibility != null) {
                Calendar eligibleDate = Calendar.getInstance();
                eligibleDate.setTime(teleplanEligibility.getDateChecked());
                Boolean eligible = teleplanEligibility.getEligible();
                if (eligible && thisMonth.before(eligibleDate)) {
                    result = "+";
                } else if (eligible && thisMonth.after(eligibleDate)) {
                    result = "#+";
                } else if (!eligible && thisMonth.before(eligibleDate)) {
                    result = "-";
                } else if (!eligible && thisMonth.after(eligibleDate)) {
                    result = "#-";
                }
            }
        }

        return result;
    }

    /**
     * Validation for PHN based on the following criteria:
     * - PHN is not null or empty
     * - PHN is 10 characters long
     * - PHN starts with a 9
     *
     * @param phn
     *      PHN to be validated
     * @return
     *      Returns one of the following
     *      true if the PHN is valid
     *      false if the PHN is invalid
     */
    public boolean validatePHN(String phn){
        boolean isValid = false;
        if (phn != null && phn.length() == 10 && phn.startsWith("9")) {
            isValid = true;
        }
        return isValid;
    }
}
