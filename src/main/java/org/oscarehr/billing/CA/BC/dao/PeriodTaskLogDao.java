/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.billing.CA.BC.dao;

import javax.persistence.Query;
import org.oscarehr.billing.CA.BC.model.PeriodTaskLog;
import org.oscarehr.common.dao.AbstractDao;
import org.springframework.stereotype.Repository;

@Repository
public class PeriodTaskLogDao extends AbstractDao<PeriodTaskLog> {

	public PeriodTaskLogDao() {
		super(PeriodTaskLog.class);
	}

	public PeriodTaskLog getLastLogByName(String jobName) {
		String sql = "select log from PeriodTaskLog log where log.jobName = :name order by log.executeTime desc";
		Query query = entityManager.createQuery(sql);
		query.setParameter("name", jobName);
		query.setMaxResults(1);
		return this.getSingleResultOrNull(query);
	}

	public void createPeriodTaskLog(PeriodTaskLog log) {
		persist(log);
	}
}
