package org.oscarehr.billing.CA.BC.model;

import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.oscarehr.common.model.AbstractModel;
import oscar.util.ConversionUtils;

@Entity
@Getter
@Setter
@Table(name = "teleplanVRC")
public class TeleplanVRC extends AbstractModel<Object> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vrc_id")
    private Integer id;

    @Column(name = "t_filename")
    private String filename;
    
    @Column(name = "t_datacenter")
    private String dataCentre;
    
    @Column(name = "t_payment_date")
    private Date paymentDate;
    
    @Column(name = "t_record_group")
    private char recordGroup;
    
    @Column(name = "t_record_count")
    private Integer recordCount;
    
    @Column(name = "t_timestamp")
    private java.sql.Timestamp timestamp;
    
    @Column(name = "t_filler")
    private String filler;

    public void parse(final String nextLine) {
        final int DATA_CENTRE_START_POSITION = 3;
        final int DATA_CENTRE_END_POSITION = 8;
        final int PAYMENT_DATE_START_POSITION = 8;
        final int PAYMENT_DATE_END_POSITION = 16;
        final int RECORD_GROUP_START_END_POSITION = 17;
        final int RECORD_COUNT_START_POSITION = 17;
        final int RECORD_COUNT_END_POSITION = 24;
        final int TIMESTAMP_START_POSITION = 24;
        final int TIMESTAMP_END_POSITION = 44;
        final int FILLER_START_POSITION = 44;
        final int FILLER_END_POSITION = 166;

        dataCentre = nextLine.substring(DATA_CENTRE_START_POSITION, DATA_CENTRE_END_POSITION);
        paymentDate = ConversionUtils.fromDateString(nextLine.substring(PAYMENT_DATE_START_POSITION, PAYMENT_DATE_END_POSITION), "yyyyMMdd");
        recordGroup = nextLine.charAt(RECORD_GROUP_START_END_POSITION);
        recordCount = Integer.parseInt(nextLine.substring(RECORD_COUNT_START_POSITION, RECORD_COUNT_END_POSITION));
        timestamp = new Timestamp(
                ConversionUtils.fromDateString(nextLine.substring(TIMESTAMP_START_POSITION, TIMESTAMP_END_POSITION),
                        "yyyy-MM-DD HH:mm:ss").getTime());
        filler = nextLine.substring(FILLER_START_POSITION, FILLER_END_POSITION).trim();
    }
}
