/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package org.oscarehr.billing.CA.BC.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.oscarehr.common.model.AbstractModel;

@Entity
@Getter
@Setter
@Table(name = "teleplanS00")
public class TeleplanS00 extends AbstractModel<Integer> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "s00_id")
	private Integer id;

	@Column(name = "s21_id")
	private Integer s21Id;

	@Column(name = "filename")
	private String fileName;

	@Column(name = "t_s00type")
	private String s00Type;

	@Column(name = "t_datacenter")
	private String dataCentre;

	@Column(name = "t_dataseq")
	private String dataSeq;

	@Column(name = "t_payment")
	private String payment;

	@Column(name = "t_linecode")
	private Character lineCode;

	@Column(name = "t_payeeno")
	private String payeeNo;

	@Column(name = "t_mspctlno")
	private String mspCtlNo;

	@Column(name = "t_practitionerno")
	private String practitionerNo;

	@Column(name = "t_msprcddate")
	private String mspRcdDate;

	@Column(name = "t_initial")
	private String initial;

	@Column(name = "t_surname")
	private String surname;

	@Column(name = "t_phn")
	private String phn;

	@Column(name = "t_phndepno")
	private String phnDepNo;

	@Column(name = "t_servicedate")
	private String serviceDate;

	@Column(name = "t_today")
	private String today;

	@Column(name = "t_billnoservices")
	private String billNoServices;

	@Column(name = "t_billClafCode")
	private String billClafCode;

	@Column(name = "t_billfeeschedule")
	private String billFeeSchedule;

	@Column(name = "t_billamt")
	private String billAmount;

	@Column(name = "t_paidNoServices")
	private String paidNoServices;

	@Column(name = "t_paidClafCode")
	private String paidClafCode;

	@Column(name = "t_paidfeeschedule")
	private String paidFeeSchedule;

	@Column(name = "t_paidamt")
	private String paidAmount;

	@Column(name = "t_officeno")
	private String officeNo;

	@Column(name = "t_exp1")
	private String exp1;

	@Column(name = "t_exp2")
	private String exp2;

	@Column(name = "t_exp3")
	private String exp3;

	@Column(name = "t_exp4")
	private String exp4;

	@Column(name = "t_exp5")
	private String exp5;

	@Column(name = "t_exp6")
	private String exp6;

	@Column(name = "t_exp7")
	private String exp7;

	@Column(name = "t_ajc1")
	private String ajc1;

	@Column(name = "t_aja1")
	private String aja1;

	@Column(name = "t_ajc2")
	private String ajc2;

	@Column(name = "t_aja2")
	private String aja2;

	@Column(name = "t_ajc3")
	private String ajc3;

	@Column(name = "t_aja3")
	private String aja3;

	@Column(name = "t_ajc4")
	private String ajc4;

	@Column(name = "t_aja4")
	private String aja4;

	@Column(name = "t_ajc5")
	private String ajc5;

	@Column(name = "t_aja5")
	private String aja5;

	@Column(name = "t_ajc6")
	private String ajc6;

	@Column(name = "t_aja6")
	private String aja6;

	@Column(name = "t_ajc7")
	private String ajc7;

	@Column(name = "t_aja7")
	private String aja7;

	@Column(name = "t_paidRate")
	private String paidRate;

	@Column(name = "t_planrefno")
	private String planRefNo;

	@Column(name = "t_claimsource")
	private String claimSource;

	@Column(name = "t_previouspaiddate")
	private String previousPaidDate;

	@Column(name = "t_icbcwcb")
	private String icBcWcb;

	@Column(name = "t_insurercode")
	private String insurerCode;

	@Column(name = "t_filler")
	private String filler;

	public String[] getExps() {
		return new String[] { getExp1(), getExp2(), getExp3(), getExp4(), getExp5(), getExp6(), getExp7() };
	}

	public String getBillingMasterNo() {
		return officeNo;
	}
}
