package org.oscarehr.billing.CA.BC.dao;

import java.util.Date;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import org.oscarehr.billing.CA.BC.model.TeleplanVRC;
import org.oscarehr.common.dao.AbstractDao;
import org.springframework.stereotype.Repository;

@Repository
public class TeleplanVRCDao extends AbstractDao<TeleplanVRC> {
    public TeleplanVRCDao() {
        super(TeleplanVRC.class);
    }

    public TeleplanVRC findByFilenameDataCentreDateCount(
            final String filename,
            final String dataCentre,
            final Date paymentDate,
            final Integer recordCount) {
        try {
            Query q = entityManager.createQuery(
                    "SELECT t "
                    + "FROM TeleplanVRC t "
                    + "WHERE t.filename=:filename "
                    + "AND t.dataCentre=:dataCentre "
                    + "AND t.paymentDate=:paymentDate "
                    + "AND t.recordCount=:recordCount");
            q.setParameter("filename", filename);
            q.setParameter("dataCentre", dataCentre);
            q.setParameter("paymentDate", paymentDate);
            q.setParameter("recordCount", recordCount);
            return (TeleplanVRC) q.getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
