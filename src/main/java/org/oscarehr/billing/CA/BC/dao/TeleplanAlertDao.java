/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.billing.CA.BC.dao;

import java.util.Date;
import java.util.List;
import javax.persistence.Query;
import org.oscarehr.billing.CA.BC.model.TeleplanAlert;
import org.oscarehr.common.dao.AbstractDao;
import org.springframework.stereotype.Repository;

@Repository
public class TeleplanAlertDao extends AbstractDao<TeleplanAlert> {

	public TeleplanAlertDao() {
		super(TeleplanAlert.class);
	}

	public List<TeleplanAlert> getAllAlerts() {
		String sql = "select log from TeleplanAlert log where log.deleted = 0";
		Query query = entityManager.createQuery(sql);
		List<TeleplanAlert> results = query.getResultList();
		return results;
	}
	
	public List<TeleplanAlert> getAllFailedAlerts() {
		String sql = "select log from TeleplanAlert log where log.deleted = 0 and log.success = 0";
		Query query = entityManager.createQuery(sql);
		List<TeleplanAlert> results = query.getResultList();
		return results;
	}

	public TeleplanAlert getAlertByName(String jobName) {
		String sql = "select log from TeleplanAlert log where log.jobName = :name and log.deleted = 0 order by log.id desc limit 1";
		Query query = entityManager.createQuery(sql);
		query.setParameter("name", jobName);
		List<TeleplanAlert> results = query.getResultList();
		return results != null && results.size() == 1 ? results.get(0) : null;
	}
	
	public int deleteErrorAlerts(String deleteProviderNo) {
		String sql = "update TeleplanAlert set deleted=1, deleteTime=:deleteTime, deleteProviderNo=:deleteProviderNo where success = 0 and deleted = 0";
		Query query = entityManager.createNativeQuery(sql);
		query.setParameter("deleteTime", new Date());
		query.setParameter("deleteProviderNo", deleteProviderNo);
		return query.executeUpdate();
	}
}
