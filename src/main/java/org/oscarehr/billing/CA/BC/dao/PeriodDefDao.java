/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.billing.CA.BC.dao;

import java.util.List;
import javax.persistence.Query;
import org.oscarehr.billing.CA.BC.model.PeriodDef;
import org.oscarehr.common.dao.AbstractDao;
import org.springframework.stereotype.Repository;

@Repository
public class PeriodDefDao extends AbstractDao<PeriodDef> {

	public PeriodDefDao() {
		super(PeriodDef.class);
	}

	public PeriodDef getByName(String name) {
		String sql = "select def from PeriodDef def where def.name = :name";
		Query query = entityManager.createQuery(sql);
		query.setParameter("name", name);
		query.setMaxResults(1);
		PeriodDef def = this.getSingleResultOrNull(query);
		if (def == null) {
			def = new PeriodDef();
			def.setName(name);
		}
		return def;
	}

	public List<PeriodDef> getAll() {
		String sql = "select def from PeriodDef def";
		Query query = entityManager.createQuery(sql);
		List<PeriodDef> results = query.getResultList();
		return results;
	}
}
