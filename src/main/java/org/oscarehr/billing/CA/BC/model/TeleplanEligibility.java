package org.oscarehr.billing.CA.BC.model;

import org.oscarehr.common.model.AbstractModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="teleplan_eligibility_check")
public class TeleplanEligibility extends AbstractModel<Integer> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="demographic_no")
    private Integer demographicNo;
    
    @Column(name="eligible")
    private Boolean eligible;

    @Column(name="date_checked")
    private Date dateChecked;

    @Column(name="checked_by")
    private String checkedBy;
    
    public TeleplanEligibility (){
    }

    public TeleplanEligibility (Integer demographicNo, Boolean eligible, String checkedBy){
        this.demographicNo = demographicNo;
        this.eligible = eligible;
        this.checkedBy = checkedBy;
        this.dateChecked= new Date();
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDemographicNo() {
        return demographicNo;
    }

    public void setDemographicNo(Integer demographicNo) {
        this.demographicNo = demographicNo;
    }

    public Boolean getEligible() {
        return eligible;
    }

    public void setEligible(Boolean eligible) {
        this.eligible = eligible;
    }

    public Date getDateChecked() {
        return dateChecked;
    }

    public void setDateChecked(Date dateChecked) {
        this.dateChecked = dateChecked;
    }

    public String getCheckedBy() {
        return checkedBy;
    }

    public void setCheckedBy(String checkedBy) {
        this.checkedBy = checkedBy;
    }
}
