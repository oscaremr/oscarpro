package org.oscarehr.billing;


import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.common.dao.BillingRuleDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.AbstractModel;
import org.oscarehr.common.model.BillingRule;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import oscar.oscarBilling.ca.bc.decisionSupport.BillingGuidelines;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ManageBillingRulesAction extends DispatchAction {
    private static Logger logger = MiscUtils.getLogger();
    private BillingRuleDao billingRuleDao = SpringUtils.getBean(BillingRuleDao.class);

    private OscarProperties oscarProperties = OscarProperties.getInstance();
    private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
    private SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);

    private String billRegion;
    private List<BillingRule> billingRules;
    
    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_admin", "w", null)) {
            throw new SecurityException("missing required security object (_admin)");
        }
        updateBillingRules();

        // create rule map
        Map<String, Boolean> ruleMap = new HashMap<String, Boolean>();
        for (BillingRule billingRule : billingRules) {
            ruleMap.put(billingRule.getServiceCode(), billingRule.getEnabled());
        }

        boolean isBillingA233ReplaceA234AndG435A =
            systemPreferencesDao.isReadBooleanPreferenceWithDefault(
                SystemPreferences.BILLING_A233_REPLACE_A234_AND_G435A, false);
        request.setAttribute("billingA233ReplaceA234AndG435A", isBillingA233ReplaceA234AndG435A);
        request.setAttribute("billingRules", ruleMap);
        return mapping.findForward("success");
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_admin", "w", null)) {
            throw new SecurityException("missing required security object (_admin)");
        }
        updateBillingRules();

        boolean updateRules = false;
        // create rule map
        Map<String, Boolean> ruleMap = new HashMap<String, Boolean>();
        if (billRegion.equals("ON")) {
            for (String code : BillingRule.ON_PEDIATRIC_CODES) {
                ruleMap.put(code, Boolean.parseBoolean(request.getParameter(code)));
            }
        }
        
        // check which rules have changed
        for (String serviceCode : ruleMap.keySet()) {
            Boolean mappedValue = ruleMap.get(serviceCode);
            BillingRule billingRule = getBillingRuleForCode(serviceCode);
            
            if (billingRule == null) {
                // add
                try {
                    billingRuleDao.saveEntity(new BillingRule(serviceCode, billRegion, mappedValue));
                    updateRules = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
               
            } else if (!billingRule.getEnabled().equals(mappedValue)) {
                // update
                billingRule.setEnabled(mappedValue);
                try {
                    billingRuleDao.saveEntity(billingRule);
                    updateRules = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        
        // update rule map
        if (updateRules) {
            updateBillingRules();
            BillingGuidelines.updateInstance();
        }

        ruleMap = new HashMap<String, Boolean>();
        for (BillingRule billingRule : billingRules) {
            ruleMap.put(billingRule.getServiceCode(), billingRule.getEnabled());
        }
        
        systemPreferencesDao.mergeOrPersist(SystemPreferences.BILLING_A233_REPLACE_A234_AND_G435A,
            request.getParameter("billingA233ReplaceA234AndG435A"));
        boolean isBillingA233ReplaceA234AndG435A = systemPreferencesDao.isReadBooleanPreferenceWithDefault(
            SystemPreferences.BILLING_A233_REPLACE_A234_AND_G435A, false);
        
        request.setAttribute("billingA233ReplaceA234AndG435A", isBillingA233ReplaceA234AndG435A);
        request.setAttribute("billingRules", ruleMap);
        return mapping.findForward("success");
    }
    
    private BillingRule getBillingRuleForCode(String serviceCode) {
        BillingRule billingRule = null;
        for (BillingRule rule : billingRules) {
            if (rule.getServiceCode().equals(serviceCode)) {
                billingRule = rule;
                break;
            }
        }
        return billingRule;
    }
    
    private void updateBillingRules() {
        if (billRegion == null) {
            billRegion = oscarProperties.getProperty("billregion", "").trim().toUpperCase();
        }
        
        billingRules = billingRuleDao.getAllForBillRegion(billRegion);
    }
}
