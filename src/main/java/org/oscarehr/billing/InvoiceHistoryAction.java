package org.oscarehr.billing;

//import com.foo.segment.ERR;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.billing.CA.ON.dao.BillingONDiskNameDao;
import org.oscarehr.billing.CA.ON.model.BillingONDiskName;
import org.oscarehr.common.dao.BillingONCHeader1Dao;
import org.oscarehr.common.dao.BillingONEAReportDao;
import org.oscarehr.common.dao.BillingONErrorCodeDao;
import org.oscarehr.common.dao.BillingONItemDao;
import org.oscarehr.common.dao.BillingONPaymentDao;
import org.oscarehr.common.dao.BillingOnItemPaymentDao;
import org.oscarehr.common.dao.InvoiceHistoryDao;
import org.oscarehr.common.dao.RaDetailDao;
import org.oscarehr.common.dao.RaHeaderDao;
import org.oscarehr.common.model.BillingONCHeader1;
import org.oscarehr.common.model.BillingONEAReport;
import org.oscarehr.common.model.BillingONErrorCode;
import org.oscarehr.common.model.BillingONItem;
import org.oscarehr.common.model.BillingONPayment;
import org.oscarehr.common.model.BillingOnItemPayment;
import org.oscarehr.common.model.InvoiceHistory;
import org.oscarehr.common.model.RaDetail;
import org.oscarehr.common.model.RaHeader;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.util.ChangedField;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InvoiceHistoryAction extends DispatchAction {
    private BillingONCHeader1Dao billingONCHeader1Dao = SpringUtils.getBean(BillingONCHeader1Dao.class);
    private BillingONDiskNameDao billingONDiskNameDao = SpringUtils.getBean(BillingONDiskNameDao.class);
    private BillingONEAReportDao billingONEAReportDao = SpringUtils.getBean(BillingONEAReportDao.class);
    private BillingONErrorCodeDao billingONErrorCodeDao = SpringUtils.getBean(BillingONErrorCodeDao.class);
    private BillingOnItemPaymentDao billingOnItemPaymentDao = SpringUtils.getBean(BillingOnItemPaymentDao.class);
    private BillingONPaymentDao billingONPaymentDao = SpringUtils.getBean(BillingONPaymentDao.class);
    private InvoiceHistoryDao invoiceHistoryDao = SpringUtils.getBean(InvoiceHistoryDao.class);
    private ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
    private RaDetailDao raDetailDao = SpringUtils.getBean(RaDetailDao.class);
    private RaHeaderDao raHeaderDao = SpringUtils.getBean(RaHeaderDao.class);
    


    public final String ERRORS = "errors";
    public final String ITEMS = "service_code";
    public final String STATUS = "status";
    public final SimpleDateFormat dateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public final SimpleDateFormat raReadDateFormat = new SimpleDateFormat("yyyy/MM/dd");

    
    public ActionForward getFilteredInvoiceHistory(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
        JSONArray jsonHistory = new JSONArray();
        String filter = StringUtils.trimToEmpty(request.getParameter("filter"));
        String billingNoString = StringUtils.trimToNull(request.getParameter("billingNo"));
        Boolean forItems = filter.startsWith(ITEMS);
        
        if  (!billingNoString.isEmpty()) {
            Integer billingNo = StringUtils.trimToNull(billingNoString) != null ? Integer.parseInt(billingNoString) : 0;
            
            if (ERRORS.equals(filter)) {
                jsonHistory = errorHistoryJSON(billingNo);
            } else {
                if (forItems) {
                    jsonHistory = itemHistoryJSON(billingNo, filter);
                } else {
                    jsonHistory = getInvoiceHistoryJSON(invoiceHistoryDao.findHistory(billingNo, filter));
                }
            }
        }
        
        jsonHistory.write(response.getWriter());
        return null;
    }

    private JSONArray errorHistoryJSON(Integer billingNo) {
        JSONArray jsonHistory = new JSONArray();

        BillingONCHeader1 invoice = billingONCHeader1Dao.find(billingNo);
        if (invoice != null) {
            
            List<BillingONEAReport> rejections = billingONEAReportDao.findByBillingNo(billingNo);
            for (BillingONEAReport report : rejections) {
                String dateString = "";

                try{
                   dateString = dateFormat.format(report.getProcessDate());
                } catch (Exception e) {
                    MiscUtils.getLogger().error(e);
                }

                BillingONErrorCode errorCode = billingONErrorCodeDao.find(report.getClaimError());
                String codeDesc = "Unknown";
                if (errorCode != null) {
                    codeDesc = errorCode.getDescription();
                }

                JSONObject historyObj = new JSONObject();
                historyObj.put("dateTime", dateString);
                historyObj.put("action", "");
                historyObj.put("data", report.getClaimError() + " - " + codeDesc);
                historyObj.put("provider", "system");
                historyObj.put("file", report.getReportName());
                jsonHistory.add(historyObj);
            }
            
            
            List<RaDetail> errors = raDetailDao.getBillingExplanatoryListObj(billingNo, invoice.getHin(), invoice.getProviderOhipNo());
            for (RaDetail raDetail : errors) {
                RaHeader raHeader = raHeaderDao.find(raDetail.getRaHeaderNo());
                String dateString = "";
                
                try{
                    Date raDate = raReadDateFormat.parse(raHeader.getReadDate());
                    dateString = dateFormat.format(raDate);
                } catch (Exception e) {
                    MiscUtils.getLogger().error(e);
                }
                
                BillingONErrorCode errorCode = billingONErrorCodeDao.find(raDetail.getErrorCode());
                String codeDesc = "Unknown";
                if (errorCode != null) {
                    codeDesc = errorCode.getDescription();
                }

                JSONObject historyObj = new JSONObject();
                historyObj.put("dateTime", dateString);
                historyObj.put("action", "");
                historyObj.put("data", raDetail.getErrorCode() + " - " + codeDesc);
                historyObj.put("provider", "system");
                historyObj.put("file", raHeader.getFilename());
                jsonHistory.add(historyObj);
            }
            
        }
        
        
        
        return jsonHistory;
    }
    
    private JSONArray getInvoiceHistoryJSON(List<InvoiceHistory> invoiceHistory) {
        JSONArray jsonHistory = new JSONArray();
        
        for (InvoiceHistory history : invoiceHistory) {
            InvoiceHistory.ActionType actionType = InvoiceHistory.ActionType.getByCode(history.getAction());

            String fileName = "N/A";
            String providerName = !"system".equals(history.getProviderNo()) && history.getProvider() != null ? history.getProvider().getFormattedName() : history.getProviderNo();
            
            JSONObject historyObj = new JSONObject();
            historyObj.put("dateTime", dateTime.format(history.getTimestamp()));
            historyObj.put("action", actionType != null ? actionType.name() : "");
            historyObj.put("data", getDisplayValue(history.getValue(), history.getField()));
            historyObj.put("provider", providerName);

            if (history.getDiskId() != null) {
                BillingONDiskName billingONDiskName = billingONDiskNameDao.find(history.getDiskId());
                if (billingONDiskName != null && StringUtils.trimToNull(billingONDiskName.getOhipFilename()) != null) {
                    fileName = billingONDiskName.getOhipFilename();
                }
            }

            historyObj.put("file", fileName);

            jsonHistory.add(historyObj);
            
        }
        
        return jsonHistory;
    }

    private JSONArray itemHistoryJSON(Integer billingNo, String filter) {
        JSONArray jsonHistory = new JSONArray();
        BillingONCHeader1 invoice = billingONCHeader1Dao.find(billingNo);
        if (invoice != null) {
            
            for (BillingONItem item : invoice.getBillingItems()) {
                JSONObject serviceHistory  = new JSONObject();
                JSONArray itemHistory = new JSONArray();
                itemHistory = getInvoiceHistoryJSON(invoiceHistoryDao.findHistory(invoice.getId(), item.getId(), filter));

                // if payments return no results, check billing_on_payment tables for relevant data
                if (filter.equals("service_code_payment") && itemHistory.isEmpty()) {
                    List<InvoiceHistory> paymentHistory = new ArrayList<InvoiceHistory>();
                    List<BillingOnItemPayment> itemPayments = billingOnItemPaymentDao.getAllByItemId(item.getId());
                    
                    for (BillingOnItemPayment itemPayment : itemPayments) {
                        if (itemPayment.getPaid().compareTo(BigDecimal.ZERO) == 1) {
                            BillingONPayment paymentRecord = billingONPaymentDao.find(itemPayment.getBillingOnPaymentId());
                            BigDecimal paymentRecordDeletedAmount = billingOnItemPaymentDao.getAmountDeletedByItemId(itemPayment.getBillingOnPaymentId());

                            if (paymentRecord != null) {
                                InvoiceHistory paymentHist = new InvoiceHistory(invoice.getId(), item.getId(), "service_code_payment", "", itemPayment.getPaid().toString(), InvoiceHistory.ActionType.ADDED, paymentRecord.getCreator());
                                
                                if (paymentRecordDeletedAmount != null) {
                                    paymentHist.setValue(paymentRecordDeletedAmount.toString());
                                    paymentHist.setAction(InvoiceHistory.ActionType.DELETED.getCode());
                                }

                                try {
                                    paymentHist.setProvider(providerDao.getProvider(paymentRecord.getCreator()));
                                    invoiceHistoryDao.saveEntity(paymentHist);
                                } catch (Exception e) {
                                    MiscUtils.getLogger().error(e);
                                }
                                paymentHistory.add(paymentHist);
                            }
                        }
                    }
                    
                    // get updated invoice history with items
                    itemHistory = getInvoiceHistoryJSON(invoiceHistoryDao.findHistory(invoice.getId(), item.getId(), filter));
                }
                        
                
                serviceHistory.put("service", item.getServiceCode());
                serviceHistory.put("history", itemHistory);
                if (itemHistory.size() > 0) {
                    jsonHistory.add(serviceHistory);
                }
            }
        }

        return jsonHistory;
    }
    
    private String getDisplayValue(String value, String valueType) {
        if (STATUS.equals(valueType)) {
            return BillingONCHeader1.BillingStatus.valueOf(value).getCodeDescription();
        }
        
        return value;
    }

}
