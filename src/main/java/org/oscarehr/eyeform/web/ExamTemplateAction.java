package org.oscarehr.eyeform.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.validator.DynaValidatorForm;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.eyeform.dao.ExamTemplateDao;
import org.oscarehr.eyeform.dao.ExamTemplateItemDao;
import org.oscarehr.eyeform.model.ExamTemplate;
import org.oscarehr.eyeform.model.ExamTemplateItem;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

public class ExamTemplateAction extends DispatchAction {
	
	ExamTemplateDao examTemplateDao = (ExamTemplateDao)SpringUtils.getBean("examTemplateDao");
	ExamTemplateItemDao examTemplateItemDao = (ExamTemplateItemDao)SpringUtils.getBean("examTemplateItemDao");
	
	ProviderDao providerDao = (ProviderDao)SpringUtils.getBean("providerDao");
	
	@Override
	public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        return form(mapping, form, request, response);
    }
	
	public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		String providerNo = request.getParameter("providerNo");
		List<ExamTemplate> examTemplates = examTemplateDao.getAllMySelfTemplate(providerNo);
		
		request.setAttribute("examTemplates", examTemplates);
		return mapping.findForward("list");
	}
	
	public ActionForward addExamTemplate(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
		return form(mapping, form, request, response);
    }

    public ActionForward cancel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        return form(mapping, form, request, response);
    }
    
    public ActionForward form(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    	String providerNo = request.getParameter("providerNo");
    	request.setAttribute("provider", providerDao.getProvider(providerNo));
    	
    	if(request.getParameter("examTemplate.id") != null) {
    		int examTemplateId = Integer.parseInt(request.getParameter("examTemplate.id"));
    		ExamTemplate template = examTemplateDao.find(examTemplateId);
    		
    		DynaValidatorForm f = (DynaValidatorForm)form;
    		f.set("examTemplate", template);
    		
    		List<ExamTemplateItem> items = examTemplateItemDao.findExamTemplateItemByExamTemplateId(examTemplateId);
    		request.setAttribute("examTemplateItem", items);
    		if(null != template.getExamTemplateProviderNo() && template.getExamTemplateProviderNo().length() > 0){
    			request.setAttribute("examTemplate_providerNo", template.getExamTemplateProviderNo());
    		}else{
    			request.setAttribute("examTemplate_providerNo", "");
    		}
    	}
    	return mapping.findForward("form");
    }

	public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    	DynaValidatorForm f = (DynaValidatorForm)form;
    	ExamTemplate examTemplate = (ExamTemplate)f.get("examTemplate");
    	
    	String providerNo = request.getParameter("providerNo");
    	String examTemplate_providerNo = request.getParameter("examTemplate_providerNo");
    	String measurementList = request.getParameter("measurementList");
    	if(request.getParameter("examTemplate.id") != null && request.getParameter("examTemplate.id").length()>0) {
    		examTemplate.setId(Integer.parseInt(request.getParameter("examTemplate.id")));
    	}
    	if(null != examTemplate_providerNo && examTemplate_providerNo.length() > 0){
    		examTemplate.setExamTemplateProviderNo(examTemplate_providerNo);
    	}
    	if( null == examTemplate.getId()){
    		examTemplate.setId(null);
    	}else{
    		if(examTemplate.getId() == 0){
    			examTemplate.setId(null);
    		}
    	}
    	
    	examTemplate.setLastUpdateProviderNo(providerNo);
    	if(examTemplate.getId() == null){
    		examTemplateDao.persist(examTemplate);
    		
    		if(measurementList.length() > 0){
    			String[] measurement = measurementList.split("&");
    			for(int i = 0;i < measurement.length;i ++){
    				String[] keys = measurement[i].split("=");
    				if(keys.length == 2){
    					String type = keys[0];
    					String value = keys[1];
    					if(value.length() > 0){
    						ExamTemplateItem item = new ExamTemplateItem();
    						
    						item.setExamTemplateId(examTemplate.getId());
    						item.setType(type);
    						item.setDataField(value);
    						item.setDateObserved(new Date());
    						item.setCreateDate(new Date());
    						
    						examTemplateItemDao.persist(item);
    					}
    				}
    			}
    		}
    	}else{
    		examTemplateDao.merge(examTemplate);
    		
    		if(measurementList.length() > 0){
    			String[] measurement = measurementList.split("&");
    			for(int i = 0;i < measurement.length;i ++){
    				String[] keys = measurement[i].split("=");
    				if(keys.length == 2){
    					String type = keys[0];
    					String value = keys[1];
    					if(value.length() > 0){
    						ExamTemplateItem item = examTemplateItemDao.findExamTemplateItemByExamTemplateIdAndType(examTemplate.getId(), type);
    						if(item == null){
    							item = new ExamTemplateItem();
    						}
    						
    						item.setExamTemplateId(examTemplate.getId());
    						item.setType(type);
    						item.setDataField(value);
    						item.setDateObserved(new Date());
    						
    						if(item.getId() == null){
    							item.setCreateDate(new Date());
    							examTemplateItemDao.persist(item);
    						}else{
    							examTemplateItemDao.merge(item);
    						}
    					}
    				}
    			}
    		}
    	}
    	
    	request.setAttribute("parentAjaxId", "examTemplate");
    	return mapping.findForward("success");
    }
    
    public ActionForward deleteExamTemplate(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    	String[] ids = request.getParameterValues("selected_id");
    	for(int x=0;x<ids.length;x++) {
    		examTemplateDao.removeExamTemplate(Integer.parseInt(ids[x]));
    	}
    	return list(mapping,form,request,response);
    }
    
    public ActionForward getExamTemplateItemsById(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response){
    	String examTemplateId = request.getParameter("examTemplateId");
    	
    	JSONArray list = new JSONArray();
    	List<ExamTemplateItem> items = examTemplateItemDao.findExamTemplateItemByExamTemplateId(Integer.parseInt(examTemplateId));
    	for(int i = 0;i < items.size();i ++){
    		JSONObject obj = new JSONObject();
    		obj.put("name", items.get(i).getType());
    		obj.put("value", items.get(i).getDataField());
    		
    		list.add(obj.toString());
    	}
    	
		try {
			PrintWriter out = response.getWriter();
			out.print(list.toString());
			out.flush();
			out.close();
		} catch (IOException e) {
			MiscUtils.getLogger().info(e.toString());
		}
		
		return null;
    }
}
