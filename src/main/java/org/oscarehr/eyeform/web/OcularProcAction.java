/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package org.oscarehr.eyeform.web;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsDateJsonBeanProcessor;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.struts.validator.DynaValidatorForm;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.eyeform.dao.EyeformOcularProcedureDao;
import org.oscarehr.eyeform.dao.MacroDao;
import org.oscarehr.eyeform.model.EyeformOcularProcedure;
import org.oscarehr.eyeform.model.Macro;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

public class OcularProcAction extends DispatchAction {

	EyeformOcularProcedureDao dao = SpringUtils.getBean(EyeformOcularProcedureDao.class);
	
	public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        return form(mapping, form, request, response);
    }

    public ActionForward cancel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        return form(mapping, form, request, response);
    }

    public ActionForward list(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    	String demographicNo = request.getParameter("demographicNo");

    	List<EyeformOcularProcedure> procs = dao.getByDemographicNo(Integer.parseInt(demographicNo));
    	request.setAttribute("procs", procs);

        return mapping.findForward("list");
    }

    public ActionForward form(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    	ProviderDao providerDao = (ProviderDao)SpringUtils.getBean("providerDao");
    	
    	request.setAttribute("providers",providerDao.getBillableProviders());
    	
    	if(request.getParameter("proc.id") != null) {
    		int procId = Integer.parseInt(request.getParameter("proc.id"));
    		EyeformOcularProcedure procedure = dao.find(procId);
    		DynaValidatorForm f = (DynaValidatorForm)form;
    		f.set("proc", procedure);


    		if (request.getParameter("json") != null && request.getParameter("json").equalsIgnoreCase("true")) {
    			try {
    				HashMap<String, EyeformOcularProcedure> hashMap = new HashMap<String, EyeformOcularProcedure>();

    				hashMap.put("proc", procedure);

    				JsonConfig config = new JsonConfig();
    		    	config.registerJsonBeanProcessor(java.sql.Date.class, new JsDateJsonBeanProcessor());

    				JSONObject json = JSONObject.fromObject(hashMap, config);
    				response.getOutputStream().write(json.toString().getBytes());
    			} catch (Exception e) {
    				MiscUtils.getLogger().error("Can't write json encoded message", e);
    			}

    			return null;
    		}

    	}else{
    		if(request.getParameter("macroId") != null && request.getParameter("macroId").length() > 0){
    			MacroDao macroDao = (MacroDao)SpringUtils.getBean("MacroDAO");
        		Integer macroId = Integer.parseInt(request.getParameter("macroId"));
        		Macro macro = macroDao.find(macroId);
        		if(null != macro){
        			EyeformOcularProcedure procedure = new EyeformOcularProcedure();
        			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        			procedure.setDateStr(sdf.format(new Date()));
        			if(null != macro.getProcedureEye() && macro.getProcedureEye().length() > 0){
        				procedure.setEye(macro.getProcedureEye());
        			}
        			if(null != macro.getProcedureName() && macro.getProcedureName().length() > 0){
        				procedure.setProcedureName(macro.getProcedureName());
        			}
        			if(null != macro.getProcedureDoctor() && macro.getProcedureDoctor().length() > 0){
        				procedure.setDoctor(macro.getProcedureDoctor());
        			}
        			if(null != macro.getProcedureLocation()&& macro.getProcedureLocation().length() > 0){
        				procedure.setLocation(macro.getProcedureLocation());
        			}
        			if(null != macro.getProcedureNote()&& macro.getProcedureNote().length() > 0){
        				procedure.setProcedureNote(macro.getProcedureNote());
        			}
        			
        			if(request.getParameter("proc.demographicNo") != null && request.getParameter("proc.demographicNo").length() > 0){
        				procedure.setDemographicNo(Integer.parseInt(request.getParameter("proc.demographicNo")));
        			}
        			if(request.getParameter("proc.appointmentNo") != null && request.getParameter("proc.appointmentNo").length() > 0){
        				procedure.setAppointmentNo(Integer.parseInt(request.getParameter("proc.appointmentNo")));
        			}
        			
            		DynaValidatorForm f = (DynaValidatorForm)form;
            		f.set("proc", procedure);
        		}
        		
    		}
    	}



        return mapping.findForward("form");
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	DynaValidatorForm f = (DynaValidatorForm)form;
    	EyeformOcularProcedure procedure = (EyeformOcularProcedure)f.get("proc");

    	LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);

    	EyeformOcularProcedureDao dao = (EyeformOcularProcedureDao)SpringUtils.getBean("eyeformOcularProcedureDao");
		procedure.setProvider(loggedInInfo.getLoggedInProviderNo());

    	if(request.getParameter("proc.id") != null && request.getParameter("proc.id").length()>0) {
    		procedure.setId(Integer.parseInt(request.getParameter("proc.id")));
    	}

    	if(procedure.getId() != null && procedure.getId() == 0) {
    		procedure.setId(null);
    	}
    	procedure.setUpdateTime(new Date());

    	if(procedure.getId() == null) {
    		dao.persist(procedure);
    	} else {
    		dao.merge(procedure);
    	}

    	if (request.getParameter("json") != null && request.getParameter("json").equalsIgnoreCase("true")) {
			HashMap<String, Integer> hashMap = new HashMap<String, Integer>();
			hashMap.put("saved", procedure.getId());

			JSONObject json = JSONObject.fromObject(hashMap);
			response.getOutputStream().write(json.toString().getBytes());

			return null;
		}

    	request.setAttribute("parentAjaxId", "ocularprocedure");
    	return mapping.findForward("success");
    }

}
