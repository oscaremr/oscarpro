package org.oscarehr.eyeform.dao;

import java.util.List;

import javax.persistence.Query;

import org.oscarehr.common.dao.AbstractDao;
import org.oscarehr.eyeform.model.ExamTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class ExamTemplateDao extends AbstractDao<ExamTemplate>{

	public ExamTemplateDao(){
		super(ExamTemplate.class);
	}
	
	@SuppressWarnings("unchecked")
	public List<ExamTemplate> getAll() {
		String sql="select x from "+modelClass.getSimpleName()+" x where x.status = '0' order by x.lastUpdateDate DESC";
		Query query = entityManager.createQuery(sql);
		
	    List<ExamTemplate> results=query.getResultList();
	    return(results);		   
	}
	
	public void removeExamTemplate(Integer id){
		String sql = "update "+modelClass.getSimpleName()+" x set x.status = '1' where x.id = " + id ;
		Query query = entityManager.createQuery(sql);
		
		query.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public List<ExamTemplate> getAllMySelfTemplate(String providerNo) {
		String sql="select x from "+modelClass.getSimpleName()+" x where (x.examTemplateProviderNo = ?1 or x.examTemplateProviderNo = NULL or x.examTemplateProviderNo = '') and x.status = '0' order by x.lastUpdateDate DESC";
		Query query = entityManager.createQuery(sql);
		query.setParameter(1, providerNo);
		
		return query.getResultList();
	}
}
