package org.oscarehr.eyeform.dao;

import java.util.List;

import javax.persistence.Query;

import org.oscarehr.common.dao.AbstractDao;
import org.oscarehr.eyeform.model.ExamTemplateItem;
import org.springframework.stereotype.Repository;

@Repository
public class ExamTemplateItemDao extends AbstractDao<ExamTemplateItem>{

	public ExamTemplateItemDao(){
		super(ExamTemplateItem.class);
	}
	
	@SuppressWarnings("unchecked")
	public ExamTemplateItem findExamTemplateItemByExamTemplateIdAndType(Integer examTemplateId, String type){
		String sql="select x from "+modelClass.getSimpleName()+" x where x.examTemplateId = '" + examTemplateId +"' and x.type = '" + type + "'";
		Query query = entityManager.createQuery(sql);
		
		List<ExamTemplateItem> items = query.getResultList();
		if(items.size() == 0){
			return null;
		}
		return items.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<ExamTemplateItem> findExamTemplateItemByExamTemplateId(Integer examTemplateId){
		String sql="select x from "+modelClass.getSimpleName()+" x where x.examTemplateId = '" + examTemplateId + "'";
		Query query = entityManager.createQuery(sql);
		
		return query.getResultList();
	}
}
