/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package org.oscarehr.eyeform;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import lombok.val;
import org.oscarehr.common.model.Measurement;
import org.oscarehr.eyeform.dao.EyeformSpecsHistoryDao;
import org.oscarehr.eyeform.model.EyeformSpecsHistory;
import org.oscarehr.util.SpringUtils;

public class MeasurementFormatter {

	Map<String,Measurement> mmap=new HashMap<String,Measurement>();
	private ResourceBundle oscarR = null; 
	
	
	public MeasurementFormatter(List<Measurement> measurements, HttpServletRequest req) {
		
		oscarR = ResourceBundle.getBundle("oscarResources", req.getLocale());
		
		for(Measurement m:measurements) {
			mmap.put(m.getType(), m);
		}
	}
	
	private String getReportResource(String key) {
		return oscarR.getString("oscarEncounter.oscarConsultationReport.PrintReport." + key);
	}

	public String getGlassesHistoryByAppointmentNumber(final Map<String, Boolean> includeMap,
													   final Integer appointmentNumber) {
		val sb = new StringBuilder();
		val glassesRx = getGlassesRxByAppointmentNumber(appointmentNumber);
		if (includeMap.get("Glasses Rx") != null && glassesRx.length() > 0) {
			sb.append("<b>GLASSES HISTORY:</b> ").append(glassesRx);
		}
		return sb.toString();
	}

	public String getGlassesHistoryByDemographicNumber(final Map<String, Boolean> includeMap,
													   final Integer demographicNumber) {
		val sb = new StringBuilder();
		val glassesRx = getGlassesRxByDemographicNo(demographicNumber);
		if (includeMap.get("Glasses Rx") != null && glassesRx.length() > 0) {
			sb.append("<b>GLASSES HISTORY:</b> ").append(glassesRx);
		}
		return sb.toString();
	}
	
	public String getVisionAssessment(Map<String,Boolean> includeMap) {
		StringBuilder sb = new StringBuilder();
		
		if(includeMap.get("Distance vision (sc)") != null && this.getVisionAssessmentDistanceVision_sc().length()>0){
			sb.append("<b>"+getReportResource("DistanceVision")+" (sc)</b> ");
			sb.append(this.getVisionAssessmentDistanceVision_sc());
		}
		if(includeMap.get("Distance vision (cc)") != null && this.getVisionAssessmentDistanceVision_cc().length()>0){
			sb.append("<b>"+getReportResource("DistanceVision")+" (cc)</b> ");
			sb.append(this.getVisionAssessmentDistanceVision_cc());
		}
		if(includeMap.get("Distance vision (ph)") != null && this.getVisionAssessmentDistanceVision_ph().length()>0){
			sb.append("<b>"+getReportResource("DistanceVision")+" (ph)</b> ");
			sb.append(this.getVisionAssessmentDistanceVision_ph());
		}
		if(includeMap.get("Intermediate vision (sc)") != null && this.getVisionAssessmentIntermediateVision_sc().length()>0){
			sb.append("<b>Intermediate vision (sc)</b> ");
			sb.append(this.getVisionAssessmentIntermediateVision_sc());
		}
		if(includeMap.get("Intermediate vision (cc)") != null && this.getVisionAssessmentIntermediateVision_cc().length()>0){
			sb.append("<b>Intermediate vision (cc)</b> ");
			sb.append(this.getVisionAssessmentIntermediateVision_cc());
		}
		if(includeMap.get("Near vision (sc)") != null && this.getVisionAssessmentNearVision_sc().length()>0){
			sb.append("<b>"+getReportResource("NearVision")+" (sc)</b> ");
			sb.append(this.getVisionAssessmentNearVision_sc());
		}
		if(includeMap.get("Near vision (cc)") != null && this.getVisionAssessmentNearVision_cc().length()>0){
			sb.append("<b>"+getReportResource("NearVision")+" (cc)</b> ");
			sb.append(this.getVisionAssessmentNearVision_cc());
		}
		if(sb.length()>0) {
			sb.insert(0, "<b>"+getReportResource("VisionAssessment")+":</b> ");
		}
		
		return sb.toString();
	}
	
	public String getManifestVision(Map<String,Boolean> includeMap) {
		StringBuilder sb = new StringBuilder();
		oscar.OscarProperties props = oscar.OscarProperties.getInstance();
		if(null != props.getProperty("eyeform_vision_measurement_has_two_AR_M") && props.getProperty("eyeform_vision_measurement_has_two_AR_M").equals("yes")){
			if(includeMap.get("Auto-refraction(-)") != null && this.getAutoRefraction().length()>0) {
				sb.append("<b>"+getReportResource("AutoRefraction")+"(-)</b> ");
				sb.append(this.getAutoRefraction());
			}
			if(includeMap.get("Auto-refraction(+)") != null && this.getAutoRefractionAdd().length()>0) {
				sb.append("<b>"+getReportResource("AutoRefraction")+"(+)</b> ");
				sb.append(this.getAutoRefractionAdd());
			}
		}else{
			if(includeMap.get("Auto-refraction") != null && this.getAutoRefraction().length()>0) {
				sb.append("<b>"+getReportResource("AutoRefraction")+"</b> ");
				sb.append(this.getAutoRefraction());
			}
		}
		if(includeMap.get("Keratometry") != null && this.getKeratometry().length()>0) {
			sb.append("<b>Keratometry</b> ");
			sb.append(this.getKeratometry());
		}
		if(null != props.getProperty("eyeform_optometry_device") && props.getProperty("eyeform_optometry_device").equals("yes")){
			if(includeMap.get("Refraction") != null && this.getRefraction().length()>0) {
				sb.append("<b>Refraction</b> ");
				sb.append(this.getRefraction());
			}
		}
		if(null != props.getProperty("eyeform_vision_measurement_has_two_AR_M") && props.getProperty("eyeform_vision_measurement_has_two_AR_M").equals("yes")){
			if(includeMap.get("Manifest distance(-)") != null && getManifestDistance().length()>0) {
				sb.append("<b>Manifest distance(-)</b> ");
				sb.append(getManifestDistance());
			}
			if(includeMap.get("Manifest distance(+)") != null && getManifestDistance().length()>0) {
				sb.append("<b>Manifest distance(+)</b> ");
				sb.append(getManifestDistanceAdd());
			}
		}else{
			if(includeMap.get("Manifest distance") != null && getManifestDistance().length()>0) {
				sb.append("<b>Manifest distance</b> ");
				sb.append(getManifestDistance());
			}
		}
		if(null != props.getProperty("eyeform_optometry_device") && props.getProperty("eyeform_optometry_device").equals("yes")){
			if(includeMap.get("Finial") != null && this.getFinial().length()>0) {
				sb.append("<b>Finial</b> ");
				sb.append(this.getFinial());
			}
		}
		if(includeMap.get("Manifest near") != null && getManifestNear().length()>0) {
			sb.append("<b>Manifest near</b> ");
			sb.append(getManifestNear());
		}
		if(includeMap.get("Cycloplegic refraction") != null && this.getCycloplegicRefraction().length()>0) {
			sb.append("<b>Cycloplegic refraction</b> ");
			sb.append(this.getCycloplegicRefraction());
		}
		if(sb.length()>0) {
			sb.insert(0,"<b>"+getReportResource("VisionMeasurement")+":</b> ");
		}
		
		return sb.toString();
	}
	
	public String getStereoVision(Map<String,Boolean> includeMap){	
		StringBuilder sb = new StringBuilder();
		if(includeMap.get("Fly test") != null && this.getFlytest().length()>0) {
			sb.append("<b>Fly test</b> ");
			sb.append(this.getFlytest());
		}
		if(includeMap.get("Stereo-acuity") != null && this.getStereo_acuity().length()>0) {
			sb.append("<b>Stereo-acuity</b> ");
			sb.append(this.getStereo_acuity());
		}
		oscar.OscarProperties props = oscar.OscarProperties.getInstance();
		if(null != props.getProperty("eyeform_optometry_device") && props.getProperty("eyeform_optometry_device").equals("yes")){
			if(includeMap.get("PD") != null && this.getPD().length()>0) {
				sb.append("<b>PD</b> ");
				sb.append(this.getPD());
			}
		}
		if(sb.length()>0) {
			sb.insert(0,"<b>STEREO VISION:</b> ");
		}
		return sb.toString();
	}
	
	public String getIntraocularPressure(Map<String,Boolean> includeMap) {
		StringBuilder sb = new StringBuilder();
		
		if(includeMap.get("NCT") != null && this.getNCT().length()>0) {
			sb.append("<b>NCT</b> ");
			sb.append(this.getNCT());
		}
		if(includeMap.get("Applanation") != null && this.getApplanation().length()>0) {
			sb.append("<b>"+getReportResource("Applanation")+"</b> ");
			sb.append(this.getApplanation());
		}
		if(includeMap.get("Central corneal thickness") != null && this.getCCT().length()>0) {
			sb.append("<b>Central corneal thickness</b> ");
			sb.append(this.getCCT());
		}
		if(sb.length()>0) {
			sb.insert(0,"<b>"+getReportResource("IntraocularPressure")+":</b> ");
		}
		return sb.toString();
	}
	
	public String getRactive(Map<String,Boolean> includeMap){
		StringBuilder sb = new StringBuilder();
		if(includeMap.get("Dominance") != null && this.getDominance().length()>0) {
			sb.append("<b>Dominance</b> ");
			sb.append(this.getDominance());
		}
		if(includeMap.get("Mesopic pupil size") != null && this.getMesopicPupilSize().length()>0) {
			sb.append("<b>Mesopic " + getReportResource("Pupil") + " size</b> ");
			sb.append(this.getMesopicPupilSize());
		}
		if(includeMap.get("Angle Kappa") != null && this.getAngleKappa().length()>0) {
			sb.append("<b>Angle Kappa</b> ");
			sb.append(this.getAngleKappa());
		}
		if(sb.length() > 0){
			sb.insert(0,"<b>REFRACTIVE:</b> ");
		}
		return sb.toString();
	}
	
	public String getOtherExam(Map<String,Boolean> includeMap) {
		StringBuilder sb = new StringBuilder();
		
		if(includeMap.get("Colour vision") != null && this.getColourVision().length()>0) {
			sb.append("<b>"+getReportResource("ColourVision")+"</b> ");
			sb.append(this.getColourVision());
		}
		if(includeMap.get("Pupil") != null && this.getPupil().length()>0) {
			sb.append("<b>" + getReportResource("Pupil") + "</b> ");
			sb.append(this.getPupil());
		}
		if(includeMap.get("Amsler grid") != null && this.getAmslerGrid().length()>0) {
			sb.append("<b>"+getReportResource("AmslerGrid")+"</b> ");
			sb.append(this.getAmslerGrid());
		}
		if(includeMap.get("Potential acuity meter") != null && this.getPAM().length()>0) {
			sb.append("<b>Potential acuity meter</b> ");
			sb.append(this.getPAM());
		}
		if(includeMap.get("Confrontation fields") != null && this.getConfrontation().length()>0) {
			sb.append("<b>Confrontation fields</b> ");
			sb.append(this.getConfrontation());
		}
		if(includeMap.get("Maddox rod") != null && this.getMaddoxrod().length()>0) {
			sb.append("<b>Maddox rod</b> ");
			sb.append(this.getMaddoxrod());
		}
		if(includeMap.get("Bagolini test") != null && this.getBagolinitest().length()>0) {
			sb.append("<b>Bagolini test</b> ");
			sb.append(this.getBagolinitest());
		}
		if(includeMap.get("Worth 4 Dot (distance)") != null && this.getW4dD().length()>0) {
			sb.append("<b>Worth 4 Dot (distance)</b> ");
			sb.append(this.getW4dD());
		}
		if(includeMap.get("Worth 4 Dot (near)") != null && this.getW4dN().length()>0) {
			sb.append("<b>Worth 4 Dot (near)</b> ");
			sb.append(this.getW4dN());
		}
		if(sb.length()>0) {
			sb.insert(0,"<b>" + getReportResource("OtherExam") + ":</b> ");
		}
		
		return sb.toString();
	}
	
	public String getDuctionTesting(Map<String,Boolean> includeMap){
		StringBuilder sb = new StringBuilder();
		if(includeMap.get("DUCTION/DIPLOPIA TESTING") != null && this.getDuction().length()>0) {
			sb.append("<b>DUCTION/DIPLOPIA TESTING:</b> \n");
			sb.append(this.getDuction());
		}
		return sb.toString();
	}
	
	public String getDeviationMeasurement(Map<String,Boolean> includeMap){ 
		StringBuilder sb = new StringBuilder();
		if(includeMap.get("Primary gaze") != null && this.getPrimarygaze().length()>0) {
			sb.append("<b>Primary gaze:</b>      ");
			sb.append(this.getPrimarygaze());
		}
		if(includeMap.get("Up gaze") != null && this.getUpgaze().length()>0) {
			sb.append("<b>Up gaze:</b> ");
			sb.append(this.getUpgaze());
		}
		if(includeMap.get("Down gaze") != null && this.getDowngaze().length()>0) {
			sb.append("<b>Down gaze:</b> ");
			sb.append(this.getDowngaze());
		}
		if(includeMap.get("Right gaze") != null && this.getRightgaze().length()>0) {
			sb.append("<b>Right gaze:</b> ");
			sb.append(this.getRightgaze());
		}
		if(includeMap.get("Left gaze") != null && this.getLeftgaze().length()>0) {
			sb.append("<b>Left gaze:</b> ");
			sb.append(this.getLeftgaze());
		}
		if(includeMap.get("Right head tilt") != null && this.getRighthead().length()>0) {
			sb.append("<b>Right head tilt:</b> ");
			sb.append(this.getRighthead());
		}
		if(includeMap.get("Left head tilt") != null && this.getLefthead().length()>0) {
			sb.append("<b>Left head tilt:</b> ");
			sb.append(this.getLefthead());
		}
		if(includeMap.get("Near") != null && this.getNear().length()>0) {
			sb.append("<b>Near:</b> ");
			sb.append(this.getNear());
		}
		if(includeMap.get("Near with +3D add") != null && this.getNearwith().length()>0) {
			sb.append("<b>Near with +3D add:</b> ");
			sb.append(this.getNearwith());
		}
		if(includeMap.get("Far distance") != null && this.getFardistance().length()>0) {
			sb.append("<b>Far distance:</b> ");
			sb.append(this.getFardistance());
		}
		if(includeMap.get("NPC") != null && this.getNPC().length()>0) {
			sb.append("<b>NPC:</b> ");
			sb.append(this.getNPC());
		}
		if(includeMap.get("A.O.ACC") != null && this.getAOACC().length()>0) {
			sb.append("<b>A.O.ACC:</b> ");
			sb.append(this.getAOACC());
		}
		if(sb.length()>0) {
			sb.insert(0,"<b>DEVIATION MEASUREMENT:</b> ");
		}
		return sb.toString();
	}

	public String getEOMStereo(Map<String,Boolean> includeMap) {
		StringBuilder sb = new StringBuilder();
		if(includeMap.get("EOM") != null && this.getEomStereo().length()>0) {
			sb.append("EOM/Stereo ");
			sb.append(this.getEomStereo());
		}
				
		return sb.toString();
	}
	
	public String getAnteriorSegment(Map<String,Boolean> includeMap) {
		StringBuilder sb = new StringBuilder();
		
		if(includeMap.get("Cornea") != null && this.getCornea().length()>0) {
			sb.append("<b>"+getReportResource("Cornea")+"</b> ");
			sb.append(this.getCornea());
		}
		if(includeMap.get("Conjunctiva/Sclera") != null && this.getConjuctivaSclera().length()>0) {
			sb.append("<b>"+getReportResource("ConjunctivaSclera")+"</b> ");
			sb.append(this.getConjuctivaSclera());
		}
		if(includeMap.get("Anterior chamber") != null && this.getAnteriorChamber().length()>0) {
			sb.append("<b>"+getReportResource("AnteriorChamber")+"</b> ");
			sb.append(this.getAnteriorChamber());
		}
		if(includeMap.get("Angle") != null && this.getAngle().length()>0) {
			sb.append("<b>"+getReportResource("Angle")+"</b> ");
			sb.append(this.getAngle());
		}
		if(includeMap.get("Iris") != null && this.getIris().length()>0) {
			sb.append("<b>"+getReportResource("Iris")+"</b> ");
			sb.append(this.getIris());
		}
		if(includeMap.get("Lens") != null && this.getLens().length()>0) {
			sb.append("<b>"+getReportResource("Lens")+"</b> ");
			sb.append(this.getLens());
		}
		if(sb.length()>0) {
			sb.insert(0,"<b>"+getReportResource("AnteriorSegment")+":</b> ");
		}
		return sb.toString();
	}
	
	public String getPosteriorSegment(Map<String,Boolean> includeMap) {
		StringBuilder sb = new StringBuilder();
		
		if(includeMap.get("Optic disc") != null && this.getDisc().length()>0) {
			sb.append("<b>"+getReportResource("OpticDisc")+"</b> ");
			sb.append(this.getDisc());
		}
		if(includeMap.get("C/D ratio") != null && this.getCdRatio().length()>0) {
			sb.append("<b>"+getReportResource("CDRatio")+"</b> ");
			sb.append(this.getCdRatio());
		}	
		if(includeMap.get("Macula") != null && this.getMacula().length()>0) {
			sb.append("<b>"+getReportResource("Macula")+"</b> ");
			sb.append(this.getMacula());
		}	
		if(includeMap.get("Retina") != null && this.getRetina().length()>0) {
			sb.append("<b>"+getReportResource("Retina")+"</b> ");
			sb.append(this.getRetina());
		}	
		if(includeMap.get("Vitreous") != null && this.getVitreous().length()>0) {
			sb.append("<b>"+getReportResource("Vitreous")+"</b> ");
			sb.append(this.getVitreous());
		}
		if(includeMap.get("CRT") != null && this.getCrt().length()>0) {
			sb.append("<b>"+getReportResource("CRT")+"</b> ");
			sb.append(this.getCrt());
		}
		if(sb.length()>0) {
			sb.insert(0,"<b>"+getReportResource("PosteriorSegment")+":</b> ");
		}
		return sb.toString();
	}
	
	public String getOCT(Map<String,Boolean> includeMap) {
		StringBuilder sb = new StringBuilder();
		if(this.getOCTS().length() > 0){
			sb.append("<b>S</b> ");
			sb.append(this.getOCTS());
		}
		
		if(this.getOCTI().length() > 0){
			sb.append("<b>I</b> ");
			sb.append(this.getOCTI());
		}
		
		if(this.getOCTTSNIT().length() > 0){
			sb.append("<b>TSNIT</b> ");
			sb.append(this.getOCTTSNIT());
		}
		
		if(this.getOCTBlank().length() > 0){
			sb.append("<b>&nbsp</b> ");
			sb.append(this.getOCTBlank());
		}
		
		if(this.getOCTMacula().length() > 0){
			sb.append("<b>Macula</b> ");
			sb.append(this.getOCTMacula());
		}
		
		if(sb.length()>0) {
			sb.insert(0,"<b>OCT:</b> ");
		}
		return sb.toString();
	}
	
	public String getVF(Map<String,Boolean> includeMap) {
		StringBuilder sb = new StringBuilder();
		
		if(this.getVF().length() > 0){
			sb.append(this.getVF());
		}
		
		if(sb.length()>0) {
			sb.insert(0,"<b>VF:</b> ");
		}
		return sb.toString();
	}
	
	public String getExternalOrbit(Map<String,Boolean> includeMap) {
		StringBuilder sb = new StringBuilder();
		if(includeMap.get("Face") != null && this.getFace().length()>0) {
			sb.append("Face ");
			sb.append(this.getFace());
		}
		if(includeMap.get("Upper lid") != null && this.getUpperLid().length()>0) {
			sb.append("Upper lid ");
			sb.append(this.getUpperLid());
		}
		if(includeMap.get("Lower lid") != null && this.getLowerLid().length()>0) {
			sb.append("Lower lid ");
			sb.append(this.getLowerLid());
		}
		if(includeMap.get("Punctum") != null && this.getPunctum().length()>0) {
			sb.append("Punctum ");
			sb.append(this.getPunctum());
		}
		if(includeMap.get("Lacrimal lake") != null && this.getLacrimalLake().length()>0) {
			sb.append("Lacrimal lake ");
			sb.append(this.getLacrimalLake());
		}
		if(sb.length()>0) {
			sb.insert(0,"<b>EXTERNAL/ORBIT:</b> ");
		}
		return sb.toString();
	}
	
	public String getEyelidDuct(Map<String,Boolean> includeMap){ 
		StringBuilder sb = new StringBuilder();
		if(includeMap.get("Upper lid") != null && this.getUpperLid().length()>0) {
			sb.append("<b>Upper lid</b> ");
			sb.append(this.getUpperLid());
		}
		if(includeMap.get("Lower lid") != null && this.getLowerLid().length()>0) {
			sb.append("<b>Lower lid</b> ");
			sb.append(this.getLowerLid());
		}
		if(includeMap.get("Lacrimal lake") != null && this.getLacrimalLake().length()>0) {
			sb.append("<b>Lacrimal lake</b> ");
			sb.append(this.getLacrimalLake());
		}
		if(includeMap.get("Lacrimal irrigation") != null && this.getLacrimalIrrigation().length()>0) {
			sb.append("<b>Lacrimal irrigation</b> ");
			sb.append(this.getLacrimalIrrigation());
		}
		if(includeMap.get("Punctum") != null && this.getPunctum().length()>0) {
			sb.append("<b>Punctum</b> ");
			sb.append(this.getPunctum());
		}
		if(includeMap.get("Nasolacrimal duct") != null && this.getNLD().length()>0) {
			sb.append("<b>Nasolacrimal duct</b> ");
			sb.append(this.getNLD());
		}
		if(includeMap.get("Dye disappearance") != null && this.getDyeDisappearance().length()>0) {
			sb.append("<b>Dye disappearance</b> ");
			sb.append(this.getDyeDisappearance());
		}
		if(sb.length()>0) {
			sb.insert(0,"<b>EYELID/NASOLACRIMAL DUCT:</b> ");
		}
		return sb.toString();
	}
	
	public String getNasalacrimalDuct(Map<String,Boolean> includeMap) {
		StringBuilder sb = new StringBuilder();
		if(includeMap.get("Lacrimal irrigation") != null && this.getLacrimalIrrigation().length()>0) {
			sb.append("Lacrimal irrigation ");
			sb.append(this.getLacrimalIrrigation());
		}
		if(includeMap.get("Nasolacrimal duct") != null && this.getNLD().length()>0) {
			sb.append("Nasolacrimal duct ");
			sb.append(this.getNLD());
		}
		if(includeMap.get("Dye disappearance") != null && this.getDyeDisappearance().length()>0) {
			sb.append("Dye disappearance ");
			sb.append(this.getDyeDisappearance());
		}
		
		if(sb.length()>0) {
			sb.insert(0,"NASOLACRIMAL DUCT:");
		}
		return sb.toString();
	}
	
	public String getEyelidMeasurement(Map<String,Boolean> includeMap) {
		StringBuilder sb = new StringBuilder();
		
		if(includeMap.get("Margin reflex distance") != null && this.getMarginReflexDistance().length()>0) {
			sb.append("<b>Margin reflex distance</b> ");
			sb.append(this.getMarginReflexDistance());
		}
		if(includeMap.get("Levator function") != null && this.getLevatorFunction().length()>0) {
			sb.append("<b>Levator function</b> ");
			sb.append(this.getLevatorFunction());
		}
		if(includeMap.get("Inferior scleral show") != null && this.getInferiorScleralShow().length()>0) {
			sb.append("<b>Inferior scleral show</b> ");
			sb.append(this.getInferiorScleralShow());
		}
		if(includeMap.get("Lagophthalmos") != null && this.getLagophthalmos().length()>0) {
			sb.append("<b>Lagophthalmos</b> ");
			sb.append(this.getLagophthalmos());
		}
		if(includeMap.get("Blink reflex") != null && this.getBlink().length()>0) {
			sb.append("<b>Blink reflex</b> ");
			sb.append(this.getBlink());
		}
		if(includeMap.get("Cranial Nerve VII function") != null && this.getCNVii().length()>0) {
			sb.append("<b>Cranial nerve VII function</b> ");
			sb.append(this.getCNVii());
		}
		if(includeMap.get("Bells phenomenon") != null && this.getBells().length()>0) {
			sb.append("<b>Bell's phenomenon</b> ");
			sb.append(this.getBells());
		}
		if(includeMap.get("Schirmer test") != null && this.getSchirmertest().length()>0) {
			sb.append("<b>Schirmer test</b> ");
			sb.append(this.getSchirmertest());
		}
		if(sb.length()>0) {
			sb.insert(0,"<b>EYELID MEASUREMENT:</b> ");
		}
		return sb.toString();
	}
	
	
	/*eyeform3 start*/
	public String getVisionAssessmentDistanceVision_sc(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_rdsc")){
			sb.append("OD ");
			sb.append(getValue("v_rdsc"));
			sb.append(";");
		}
		if(isPresent("v_ldsc")){
			sb.append("OS ");
			sb.append(getValue("v_ldsc"));
			sb.append("; ");
		}
		if(isPresent("v_dsc")){
			sb.append("OU ");
			sb.append(getValue("v_dsc"));
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getVisionAssessmentDistanceVision_cc(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_rdcc")){
			sb.append("OD ");
			sb.append(getValue("v_rdcc"));
			sb.append("; ");
		}
		if(isPresent("v_ldcc")){			
			sb.append("OS ");
			sb.append(getValue("v_ldcc"));
			sb.append("; ");
		}
		if(isPresent("v_dcc")){
			sb.append("OU ");
			sb.append(getValue("v_dcc"));
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getVisionAssessmentDistanceVision_ph(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_rph")){
			sb.append("OD ");
			sb.append(getValue("v_rph"));
			sb.append("; ");
		}
		if(isPresent("v_lph")){
			sb.append("OS ");
			sb.append(getValue("v_lph"));
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getVisionAssessmentIntermediateVision_sc(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_risc")){
			sb.append("OD ");
			sb.append(getValue("v_risc"));
			sb.append("; ");
		}
		if(isPresent("v_lisc")){
			sb.append("OS ");
			sb.append(getValue("v_lisc"));
			sb.append("; ");
		}
		if(isPresent("v_isc")){
			sb.append("OU ");
			sb.append(getValue("v_isc"));
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getVisionAssessmentIntermediateVision_cc(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_ricc")){
			sb.append("OD ");
			sb.append(getValue("v_ricc"));
			sb.append("; ");
		}
		if(isPresent("v_licc")){
			sb.append("OS ");
			sb.append(getValue("v_licc"));
			sb.append("; ");
		}
		if(isPresent("v_icc")){
			sb.append("OU ");
			sb.append(getValue("v_icc"));
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getVisionAssessmentNearVision_sc(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_rnsc")){
			sb.append("OD ");
			sb.append(getValue("v_rnsc"));
			sb.append("; ");
		}
		if(isPresent("v_lnsc")){
			sb.append("OS ");
			sb.append(getValue("v_lnsc"));
			sb.append("; ");
		}
		if(isPresent("v_nsc")){
			sb.append("OU ");
			sb.append(getValue("v_nsc"));
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getVisionAssessmentNearVision_cc(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_rncc")){
			sb.append("OD ");
			sb.append(getValue("v_rncc"));
			sb.append("; ");
		}
		if(isPresent("v_lncc")){
			sb.append("OS ");
			sb.append(getValue("v_lncc"));
			sb.append("; ");
		}
		if(isPresent("v_ncc")){
			sb.append("OU ");
			sb.append(getValue("v_ncc"));
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getAutoRefraction(){		
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_rs")) {		
			sb.append(getValue("v_rs"));
		}
		if(isPresent("v_rc")) {
			sb.append(" " +getValue("v_rc"));
		}
		if(isPresent("v_rx")) {
			sb.append(" x " + getValue("v_rx"));
		}
		if(isPresent("v_rar")) {
			sb.append("(" + getValue("v_rar")+ ")");
		}
		if(sb.length() > 0){
			sb.insert(0, "OD ");
			sb.append("; ");
		}
		
		StringBuilder sb1 = new StringBuilder();
		if(isPresent("v_ls")) {			
			sb1.append(" " +getValue("v_ls"));
		}
		if(isPresent("v_lc")) {
			sb1.append(" " +getValue("v_lc"));
		}
		if(isPresent("v_lx")) {
			sb1.append(" x " + getValue("v_lx"));
		}
		if(isPresent("v_lar")) {
			sb1.append("(" + getValue("v_lar")+ ")");
		}
		if(sb1.length() > 0){
			sb1.insert(0, "OS");
			sb1.append(".");
			sb.append(sb1);
		}

		return sb.toString();
	}
	
	public String getAutoRefractionAdd(){		
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_rs_add")) {		
			sb.append(getValue("v_rs_add"));
		}
		if(isPresent("v_rc_add")) {
			sb.append(" " +getValue("v_rc_add"));
		}
		if(isPresent("v_rx_add")) {
			sb.append(" x " + getValue("v_rx_add"));
		}
		if(isPresent("v_rar_add")) {
			sb.append("(" + getValue("v_rar_add")+ ")");
		}
		if(sb.length() > 0){
			sb.insert(0, "OD ");
			sb.append("; ");
		}
		
		StringBuilder sb1 = new StringBuilder();
		if(isPresent("v_ls_add")) {			
			sb1.append(" " +getValue("v_ls_add"));
		}
		if(isPresent("v_lc_add")) {
			sb1.append(" " +getValue("v_lc_add"));
		}
		if(isPresent("v_lx_add")) {
			sb1.append(" x " + getValue("v_lx_add"));
		}
		if(isPresent("v_lar_add")) {
			sb1.append("(" + getValue("v_lar_add")+ ")");
		}
		if(sb1.length() > 0){
			sb1.insert(0, "OS");
			sb1.append(".");
			sb.append(sb1);
		}
		return sb.toString();
	}
	
	public String getKeratometry(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_rk1")){
			sb.append(getValue("v_rk1"));
		}
		if(isPresent("v_rk2")){
			sb.append(" x " + getValue("v_rk2"));
		}
		if(isPresent("v_rkx")){
			sb.append(" @ " + getValue("v_rkx"));
		}
		if(sb.length() > 0){
			sb.insert(0, "OD ");
			sb.append("; ");
		}
		
		StringBuilder sb1 = new StringBuilder();
		if(isPresent("v_lk1")) {
			
			sb1.append(" " +getValue("v_lk1"));
		}
		if(isPresent("v_lk1")) {
			sb1.append(" x " + getValue("v_lk2"));
		}
		if(isPresent("v_lk1")) {
			sb1.append(" @ " + getValue("v_lkx"));
			
		}
		if(sb1.length() > 0){
			sb1.insert(0, "OS ");
			sb1.append(".");
			sb.append(sb1);
		}
		return sb.toString();
	}
	
	public String getRefraction(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_rr1")){
			sb.append(getValue("v_rr1"));
		}
		if(isPresent("v_rr2")){
			sb.append(" x " + getValue("v_rr2"));
		}
		if(sb.length() > 0){
			sb.insert(0, "OD ");
			sb.append("; ");
		}
		
		StringBuilder sb1 = new StringBuilder();
		if(isPresent("v_lr1")) {
			
			sb1.append(" " +getValue("v_lr1"));
		}
		if(isPresent("v_lr1")) {
			sb1.append(" x " + getValue("v_lr2"));
		}
		if(sb1.length() > 0){
			sb1.insert(0, "OS ");
			sb1.append(".");
			sb.append(sb1);
		}
		return sb.toString();
	}
	
	public String getFinial(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_rs_finial")) {		
			sb.append(getValue("v_rs_finial"));
		}
		if(isPresent("v_rc_finial")) {
			sb.append(" " +getValue("v_rc_finial"));
		}
		if(isPresent("v_rx_finial")) {
			sb.append(" x " + getValue("v_rx_finial"));
		}
		if(sb.length() > 0){
			sb.insert(0, "OD ");
			sb.append("; ");
		}
		
		StringBuilder sb1 = new StringBuilder();
		if(isPresent("v_ls_finial")) {			
			sb1.append(" " +getValue("v_ls_finial"));
		}
		if(isPresent("v_lc_finial")) {
			sb1.append(" " +getValue("v_lc_finial"));
		}
		if(isPresent("v_lx_finial")) {
			sb1.append(" x " + getValue("v_lx_finial"));
		}
		if(sb1.length() > 0){
			sb1.insert(0, "OS");
			sb1.append(".");
			sb.append(sb1);
		}
		return sb.toString();
	}
	
	public String getManifestDistance() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_rds")) {		
			sb.append(getValue("v_rds"));
		}
		if(isPresent("v_rdc")) {
			sb.append(" " +getValue("v_rdc"));
		}
		if(isPresent("v_rdx")) {
			sb.append(" x " + getValue("v_rdx"));
		}
		if(isPresent("v_rdp")) {
			sb.append(" " + getValue("v_rdp"));
		}
		if(isPresent("v_rdv")) {
			sb.append(" (" + getValue("v_rdv") + ")");
			//sb.append("; ");
		}
		if(sb.length() > 0){
			sb.append("; ");
			sb.insert(0, "OD ");
		}
		
		StringBuilder sb1 = new StringBuilder();
		if(isPresent("v_lds")) {
			sb1.append(getValue("v_lds"));
		}
		if(isPresent("v_ldc")) {
			sb1.append(" " +getValue("v_ldc"));
		}
		if(isPresent("v_ldx")) {
			sb1.append(" x" + getValue("v_ldx"));
		}
		if(isPresent("v_ldp")) {
			sb1.append(" " + getValue("v_ldp"));
		}
		if(isPresent("v_ldv")) {
			sb1.append(" (" + getValue("v_ldv") + ")");
		}
		if(sb1.length() > 0){
			sb1.insert(0, "OS ");
			sb1.append("; ");
			sb.append(sb1);
		}
		if(isPresent("v_dv")){
			sb.append("OU ");
			sb.append(getValue("v_dv"));
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getManifestDistanceAdd() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_rds_add")) {		
			sb.append(getValue("v_rds_add"));
		}
		if(isPresent("v_rdc_add")) {
			sb.append(" " +getValue("v_rdc_add"));
		}
		if(isPresent("v_rdx_add")) {
			sb.append(" x " + getValue("v_rdx_add"));
		}
		if(isPresent("v_rdp_add")) {
			sb.append(" " + getValue("v_rdp_add"));
		}
		if(isPresent("v_rdv_add")) {
			sb.append(" (" + getValue("v_rdv_add") + ")");
			//sb.append("; ");
		}
		if(sb.length() > 0){
			sb.append("; ");
			sb.insert(0, "OD ");
		}
		
		StringBuilder sb1 = new StringBuilder();
		if(isPresent("v_lds_add")) {
			sb1.append(getValue("v_lds_add"));
		}
		if(isPresent("v_ldc_add")) {
			sb1.append(" " +getValue("v_ldc_add"));
		}
		if(isPresent("v_ldx_add")) {
			sb1.append(" x" + getValue("v_ldx_add"));
		}
		if(isPresent("v_ldp_add")) {
			sb1.append(" " + getValue("v_ldp_add"));
		}
		if(isPresent("v_ldv_add")) {
			sb1.append(" (" + getValue("v_ldv_add") + ")");
		}
		if(sb1.length() > 0){
			sb1.insert(0, "OS ");
			sb1.append("; ");
			sb.append(sb1);
		}
		if(isPresent("v_dv_add")){
			sb.append("OU ");
			sb.append(getValue("v_dv_add"));
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getManifestNear() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("v_rns")) {					
			sb.append(getValue("v_rns"));
		}
		if(isPresent("v_rnc")) {
			sb.append(" " +getValue("v_rnc"));
		}
		if(isPresent("v_rnx")) {
			sb.append(" x " + getValue("v_rnx"));
		}
		if(isPresent("v_rnp")) {
			sb.append(" " + getValue("v_rnp"));
		}
		if(isPresent("v_rnv")) {
			sb.append(" (" + getValue("v_rnv") + ")");
		}
		if(sb.length() > 0){
			sb.insert(0, "OD ");
			sb.append("; ");
		}
		
		StringBuilder sb1 = new StringBuilder();
		if(isPresent("v_lns")) {
			sb1.append(getValue("v_lns"));
		}
		if(isPresent("v_lnc")) {
			sb1.append(" " +getValue("v_lnc"));
		}
		if(isPresent("v_lnx")) {
			sb1.append(" x " + getValue("v_lnx"));
		}
		if(isPresent("v_lnp")) {
			sb1.append(" " + getValue("v_lnp"));
		}
		if(isPresent("v_lnv")) {
			sb1.append(" (" + getValue("v_lnv") + ")");
		}
		if(sb1.length() > 0){
			sb1.insert(0, "OS ");
			sb1.append("; ");
			sb.append(sb1);
		}
		if(isPresent("v_nv")){
			sb.append("OU ");
			sb.append(getValue("v_nv"));
			sb.append(". ");
		}
		
		if(isPresent("v_alt_add")){
			sb.append("OU ");
			sb.append(getValue("v_alt_add"));
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getCycloplegicRefraction() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_rcs")) {				
			sb.append(getValue("v_rcs"));
		}
		if(isPresent("v_rcc")) {
			sb.append(" " +getValue("v_rcc"));
		}
		if(isPresent("v_rcx")) {
			sb.append(" x " + getValue("v_rcx"));
		}
		if(isPresent("v_rcp")) {
			sb.append(" " + getValue("v_rcp"));
		}
		if(isPresent("v_rcv")) {
			sb.append(" (" + getValue("v_rcv") + ")");
		}
		if(sb.length() > 0){
			sb.insert(0, "OD ");
			sb.append("; ");
		}
		
		StringBuilder sb1 = new StringBuilder();
		if(isPresent("v_lcs")) {
			sb1.append(getValue("v_lcs"));
		}
		if(isPresent("v_lcc")) {
			sb1.append(" " +getValue("v_lcc"));
		}
		if(isPresent("v_lcx")) {
			sb1.append(" x" + getValue("v_lcx"));
		}
		if(isPresent("v_lcp")) {
			sb1.append(" " + getValue("v_lcp"));
		}
		if(isPresent("v_lcv")) {
			sb1.append(" (" + getValue("v_lcv") + ")");
		}
		if(sb1.length() > 0){
			sb1.insert(0, "OS ");
			sb1.append(". ");
			sb.append(sb1);
		}
		return sb.toString();
	}
	
	public String getFlytest(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("v_fly")){
			sb.append(getValue("v_fly"));
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getStereo_acuity(){
		StringBuilder sb = new StringBuilder();	
		if(isPresent("v_stereo")){
			sb.append(getValue("v_stereo"));
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getPD(){
		StringBuilder sb = new StringBuilder();	
		if(isPresent("v_pd")){
			sb.append(getValue("v_pd"));
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getNCT() {
		StringBuilder sb = new StringBuilder();		
		Date d1 = null;
		Date d2 = null;
		if(isPresent("iop_rn")){
			sb.append("OD ");
			sb.append(getValue("iop_rn"));
			sb.append("; ");
			d1 = mmap.get("iop_rn").getDateObserved();
		}
		if(isPresent("iop_ln")){
			sb.append("OS ");
			sb.append(getValue("iop_ln"));
			sb.append("; ");
			d2 = mmap.get("iop_ln").getDateObserved();
		}
		Date d = d2;
		if((d1 != null) &&(d2 != null)){
			if(d1.after(d2))
				d=d1;
		}
		if((d1 != null) &&(d2 == null)){
			d=d1;
		}
		if((d1 == null) &&(d2 != null)){
			d=d2;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		if(d != null){
			sb.append("(" + sdf.format(d)  + ")");
			sb.append(". ");
		}		
		return sb.toString();
	}
	
	public String getApplanation() {		
		StringBuilder sb = new StringBuilder();
		Date d1 = null;
		Date d2 = null;
		if(isPresent("iop_ra")){
			sb.append("OD ");
			sb.append(getValue("iop_ra"));
			sb.append("; ");
			d1 = mmap.get("iop_ra").getDateObserved();
		}
		if(isPresent("iop_la")){
			sb.append("OS ");
			sb.append(getValue("iop_la"));
			sb.append("; ");
			d2 = mmap.get("iop_la").getDateObserved();
		}
		Date d = d2;
		if((d1 != null) &&(d2 != null)){
			if(d1.after(d2))
				d=d1;
		}
		if((d1 != null) &&(d2 == null)){
			d=d1;
		}
		if((d1 == null) &&(d2 != null)){
			d=d2;
		}
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		if(d != null){
			sb.append("(" + sdf.format(d)  + ")");
			sb.append(".");
		}
		return sb.toString();
	}
	
	public String getCCT() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("cct_r")) {
			sb.append("OD ");		
			sb.append(getValue("cct_r"));
			sb.append(" microns");
			sb.append("; ");
		}
		if(isPresent("cct_l")) {
			sb.append("OS ");
			sb.append(getValue("cct_l"));
			sb.append(" microns");
			sb.append(".");
		}
		return sb.toString();
	}
	
	public String getDominance(){
		StringBuilder sb = new StringBuilder();	
		if(isPresent("ref_rdom")){
			sb.append("OD ");		
			sb.append(getValue("ref_rdom"));		
			sb.append("; ");
		}
		if(isPresent("ref_ldom")){
			sb.append("OS ");		
			sb.append(getValue("ref_ldom"));		
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getMesopicPupilSize(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("ref_rpdim")){
			sb.append("OD ");		
			sb.append(getValue("ref_rpdim"));		
			sb.append("; ");
		}
		if(isPresent("ref_lpdim")){
			sb.append("OS ");		
			sb.append(getValue("ref_lpdim"));		
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getAngleKappa(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("ref_rkappa")){
			sb.append("OD ");		
			sb.append(getValue("ref_rkappa"));		
			sb.append("; ");
		}
		if(isPresent("ref_lkappa")){
			sb.append("OS ");		
			sb.append(getValue("ref_lkappa"));		
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getColourVision() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("o_rcolour")) {
			sb.append("OD ");		
			sb.append(getValue("o_rcolour"));		
			sb.append("; ");
		}
		if(isPresent("o_lcolour")) {
			sb.append("OS ");
			sb.append(getValue("o_lcolour"));	
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getPupil() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("o_rpupil")) {
			sb.append("OD ");		
			sb.append(getValue("o_rpupil"));		
			sb.append("; ");
		}
		if(isPresent("o_lpupil")) {
			sb.append("OS ");
			sb.append(getValue("o_lpupil"));	
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getAmslerGrid() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("o_ramsler")) {
			sb.append("OD ");		
			sb.append(getValue("o_ramsler"));		
			sb.append("; ");
		}
		if(isPresent("o_lamsler")) {
			sb.append("OS ");
			sb.append(getValue("o_lamsler"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getPAM() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("o_rpam")) {
			sb.append("OD ");		
			sb.append(getValue("o_rpam"));		
			sb.append("; ");
		}
		if(isPresent("o_lpam")) {
			sb.append("OS ");
			sb.append(getValue("o_lpam"));	
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getConfrontation() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("o_rconf")) {
			sb.append("OD ");		
			sb.append(getValue("o_rconf"));		
			sb.append("; ");
		}
		if(isPresent("o_lconf")) {
			sb.append("OS ");
			sb.append(getValue("o_lconf"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getMaddoxrod(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("o_mad")) {
			sb.append(getValue("o_mad"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	public String getBagolinitest(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("o_bag")) {
			sb.append(getValue("o_bag"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	public String getW4dD(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("o_w4dd")) {
			sb.append(getValue("o_w4dd"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getW4dN(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("o_w4dn")) {
			sb.append(getValue("o_w4dn"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getDuction(){
		StringBuilder sb = new StringBuilder();
		int num = 0;
		int num1= 0;
		int num2 = 0;
		if(isPresent("duc_rur")) {
			if(num < getValue("duc_rur").length()){
				num = getValue("duc_rur").length();
			}
		}
		if(isPresent("duc_rul")) {
			if(num < getValue("duc_rul").length()){
				num = getValue("duc_rul").length();
			}
		}
		if(isPresent("duc_rr")) {
			if(num < getValue("duc_rr").length()){
				num = getValue("duc_rr").length();
			}
		}
		if(isPresent("duc_rl")) {
			if(num < getValue("duc_rl").length()){
				num = getValue("duc_rl").length();
			}
		}
		if(isPresent("duc_rdr")) {
			if(num < getValue("duc_rdr").length()){
				num = getValue("duc_rdr").length();
			}
		}
		if(isPresent("duc_rdl")) {
			if(num < getValue("duc_rdl").length()){
				num = getValue("duc_rdl").length();
			}
		}
		
		if(isPresent("duc_lur")) {
			if(num1 < getValue("duc_lur").length()){
				num1 = getValue("duc_lur").length();
			}
		}
		if(isPresent("duc_lul")) {
			if(num1 < getValue("duc_lul").length()){
				num1 = getValue("duc_lul").length();
			}
		}
		if(isPresent("duc_lr")) {
			if(num1 < getValue("duc_lr").length()){
				num1 = getValue("duc_lr").length();
			}
		}
		if(isPresent("duc_ll")) {
			if(num1 < getValue("duc_ll").length()){
				num1 = getValue("duc_ll").length();
			}
		}
		if(isPresent("duc_ldr")) {
			if(num1 < getValue("duc_ldr").length()){
				num1 = getValue("duc_ldr").length();
			}
		}
		if(isPresent("duc_ldl")) {
			if(num1 < getValue("duc_ldl").length()){
				num1 = getValue("duc_ldl").length();
			}
		}
		
		if(isPresent("dip_ur")) {
			if(num2 < getValue("dip_ur").length()){
				num2 = getValue("dip_ur").length();
			}
		}
		if(isPresent("dip_u")) {
			if(num2 < getValue("dip_u").length()){
				num2 = getValue("dip_u").length();
			}
		}
		if(isPresent("dip_r")) {
			if(num2 < getValue("dip_r").length()){
				num2 = getValue("dip_r").length();
			}
		}
		if(isPresent("dip_p")) {
			if(num2 < getValue("dip_p").length()){
				num2 = getValue("dip_p").length();
			}
		}
		if(isPresent("dip_dr")) {
			if(num2 < getValue("dip_dr").length()){
				num2 = getValue("dip_dr").length();
			}
		}
		if(isPresent("dip_d")) {
			if(num2 < getValue("dip_d").length()){
				num2 = getValue("dip_d").length();
			}
		}
		
		if((num == 0) && (num1 == 0) && (num2 == 0)){
			
		}else{
			if(num > 0){
				sb.append("<table><tr><td>OD</td>");
			}else{
				sb.append("<table><tr><td></td>");
			}
			if(isPresent("duc_rur")) {
				sb.append("<td>"+getValue("duc_rur")+"</td>");
			}else{
				sb.append("<td></td>");
			}
			if(isPresent("duc_rul")) {
				sb.append("<td>" + getValue("duc_rul") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			if(num1 > 0){
				sb.append("<td>OS</td>");
			}else{
				sb.append("<td></td>");
			}
			if(isPresent("duc_lur")) {
				sb.append("<td>" + getValue("duc_lur") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			if(isPresent("duc_lul")) {
				sb.append("<td>" +getValue("duc_lul") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			if(num2 > 0){
				sb.append("<td>OU</td>");
			}else{
				sb.append("<td></td>");
			}
			if(isPresent("dip_ur")) {
				sb.append("<td>" + getValue("dip_ur") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			if(isPresent("dip_u")) {
				sb.append("<td>" +getValue("dip_u") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			sb.append("</tr>");
			
			sb.append("<tr><td></td>");
			if(isPresent("duc_rr")) {
				sb.append("<td>" +getValue("duc_rr") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			if(isPresent("duc_rl")) {
				sb.append("<td>" +getValue("duc_rl") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			sb.append("<td></td>");
			if(isPresent("duc_lr")) {
				sb.append("<td>" +getValue("duc_lr") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			if(isPresent("duc_ll")) {
				sb.append("<td>" +getValue("duc_ll") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			sb.append("<td></td>");
			if(isPresent("dip_r")) {
				sb.append("<td>" +getValue("dip_r") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			if(isPresent("dip_p")) {
				sb.append("<td>" +getValue("dip_p") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			sb.append("</tr>");
			
			sb.append("<tr><td></td>");
			if(isPresent("duc_rdr")) {
				sb.append("<td>" +getValue("duc_rdr") + "</td>");
			}else{
				sb.append("<tr><td></td>");
			}
			if(isPresent("duc_rdl")) {
				sb.append("<td>" +getValue("duc_rdl") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			sb.append("<td></td>");
			if(isPresent("duc_ldr")) {
				sb.append("<td>" +getValue("duc_ldr") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			if(isPresent("duc_ldl")) {
				sb.append("<td>" +getValue("duc_ldl") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			sb.append("<td></td>");
			if(isPresent("dip_dr")) {
				sb.append("<td>" +getValue("dip_dr") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			if(isPresent("dip_d")) {
				sb.append("<td>" +getValue("dip_d") + "</td>");
			}else{
				sb.append("<td></td>");
			}
			sb.append("</tr></table>");
		}
			
		return sb.toString();
	}
	
	public String getPrimarygaze(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("dev_p")) {
			sb.append(getValue("dev_p"));
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getUpgaze(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("dev_u")) {
			sb.append(getValue("dev_u"));
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getDowngaze(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("dev_d")) {
			sb.append(getValue("dev_d"));
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getRightgaze(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("dev_r")) {
			sb.append(getValue("dev_r"));
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getLeftgaze(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("dev_l")) {
			sb.append(getValue("dev_l"));
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getRighthead(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("dev_rt")) {
			sb.append(getValue("dev_rt"));
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getLefthead(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("dev_lt")) {
			sb.append(getValue("dev_lt"));
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getNear(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("dev_near")) {
			sb.append(getValue("dev_near"));
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getNearwith(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("dev_plus3")) {
			sb.append(getValue("dev_plus3"));
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getFardistance(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("dev_far")) {
			sb.append(getValue("dev_far"));
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getNPC(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("dev_npc")) {
			sb.append(getValue("dev_npc"));
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getAOACC(){
		StringBuilder sb = new StringBuilder();
		if(isPresent("dev_aoacc_od")) {
			sb.append("od:" + getValue("dev_aoacc_od"));
			sb.append(". ");	
		}
		if(isPresent("dev_aoacc_os")) {
			sb.append(" os:" + getValue("dev_aoacc_os"));
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getEomStereo() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("EOM")) {
			sb.append(getValue("EOM"));				
			sb.append(". ");
		}
		return sb.toString();
	}	
	
	
	public String getCornea() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("a_rk")) {
			sb.append("OD ");		
			sb.append(getValue("a_rk"));		
			sb.append("; ");
		}
		if(isPresent("a_lk")) {
			sb.append("OS ");
			sb.append(getValue("a_lk"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getConjuctivaSclera() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("a_rconj")) {
			sb.append("OD ");		
			sb.append(getValue("a_rconj"));		
			sb.append("; ");
		}
		if(isPresent("a_lconj")) {
			sb.append("OS ");
			sb.append(getValue("a_lconj"));	
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getAnteriorChamber() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("a_rac")) {
			sb.append("OD ");		
			sb.append(getValue("a_rac"));		
			sb.append("; ");
		}
		if(isPresent("a_lac")) {
			sb.append("OS ");
			sb.append(getValue("a_lac"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getAngle() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("a_rangle_1") || isPresent("a_rangle_2") || isPresent("a_rangle_3") || isPresent("a_rangle_4") || isPresent("a_rangle_5")) {			
			sb.append("OD ");
			sb.append(getValue("a_rangle_3"));
			if(isPresent("a_rangle_1") || isPresent("a_rangle_2") || isPresent("a_rangle_4") || isPresent("a_rangle_5")) {
				sb.append(" (");
			}
			boolean flag=false;
			if(isPresent("a_rangle_1")) { 
				sb.append(oscarR.getString("oscarEncounter.eyeExam.Superior") + " " + getValue("a_rangle_1"));
				if(!flag) flag=true;
			}
			if(isPresent("a_rangle_4")) {
				if(flag) {					
					sb.append(", ");
				}
				if(!flag) flag=true;
				sb.append(oscarR.getString("oscarEncounter.eyeExam.Nasal") + " "+getValue("a_rangle_4"));
			}
			if(isPresent("a_rangle_5")) {
				if(flag) {
					sb.append(", ");
				}
				if(!flag) flag=true;
				sb.append(oscarR.getString("oscarEncounter.eyeExam.Inferieur") + " "+getValue("a_rangle_5"));
			}
			if(isPresent("a_rangle_2")) {
				if(flag) {
					sb.append(", ");
				}
				if(!flag) flag=true;
				sb.append(oscarR.getString("oscarEncounter.eyeExam.Temporal") + " " + getValue("a_rangle_2"));
			}				
			if(isPresent("a_rangle_1") || isPresent("a_rangle_2") || isPresent("a_rangle_4") || isPresent("a_rangle_5")) {
				sb.append(")");
			}
			sb.append("; ");
		}

		if(isPresent("a_langle_1") || isPresent("a_langle_2") || isPresent("a_langle_3") || isPresent("a_langle_4") || isPresent("a_langle_5")) {			
			sb.append("OS ");
			sb.append(getValue("a_langle_3"));
			if(isPresent("a_langle_1") || isPresent("a_langle_2") || isPresent("a_langle_4") || isPresent("a_langle_5")) {
				sb.append(" (");
			}
			boolean flag=false;
			if(isPresent("a_langle_1")) {
				sb.append(oscarR.getString("oscarEncounter.eyeExam.Superior") + " " + getValue("a_langle_1"));
				if(!flag) flag=true;
			}
			if(isPresent("a_langle_4")) {
				if(flag) {
					sb.append(", ");
				}
				if(!flag) flag=true;
				sb.append(oscarR.getString("oscarEncounter.eyeExam.Nasal") + " " + getValue("a_langle_4"));
			}
			if(isPresent("a_langle_5")) {
				if(flag) {
					sb.append(", ");
				}
				if(!flag) flag=true;
				sb.append(oscarR.getString("oscarEncounter.eyeExam.Inferieur") + " " +getValue("a_langle_5"));
			}
			if(isPresent("a_langle_2")) {
				if(flag) {					
					sb.append(", ");
				}
				if(!flag) flag=true;
				sb.append(oscarR.getString("oscarEncounter.eyeExam.Temporal") + " " +getValue("a_langle_2"));
			}
			
			if(isPresent("a_langle_1") || isPresent("a_langle_2") || isPresent("a_langle_4") || isPresent("a_langle_5")) {
				sb.append(")");
			}
			sb.append("; ");
		}	
				
		return sb.toString();
	}

	public String getIris() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("a_riris")) {
			sb.append("OD ");		
			sb.append(getValue("a_riris"));		
			sb.append("; ");
		}
		if(isPresent("a_liris")) {
			sb.append("OS ");
			sb.append(getValue("a_liris"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getLens() {
		StringBuilder sb = new StringBuilder(); 
		if(isPresent("a_rlens")) {
			sb.append("OD ");		
			sb.append(getValue("a_rlens"));		
			sb.append("; ");
		}
		if(isPresent("a_llens")) {
			sb.append("OS ");
			sb.append(getValue("a_llens"));	
			sb.append(". ");
		}
		return sb.toString();
	}
	
	public String getDisc() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("p_rdisc")) {
			sb.append("OD ");		
			sb.append(getValue("p_rdisc") );		
			sb.append("; ");
		}
		if(isPresent("p_ldisc")) {
			sb.append("OS ");
			sb.append(getValue("p_ldisc"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getCdRatio() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("p_rcd")) {
			sb.append("OD ");		
			sb.append(getValue("p_rcd") );		
			sb.append("; ");
		}
		if(isPresent("p_lcd")) {
			sb.append("OS ");
			sb.append(getValue("p_lcd"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getMacula() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("p_rmac")) {
			sb.append("OD ");		
			sb.append(getValue("p_rmac"));		
			sb.append("; ");
		}
		if(isPresent("p_lmac")) {
			sb.append("OS ");
			sb.append(getValue("p_lmac"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getRetina() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("p_rret")) {
			sb.append("OD ");		
			sb.append(getValue("p_rret"));		
			sb.append("; ");
		}
		if(isPresent("p_lret")) {
			sb.append("OS ");
			sb.append(getValue("p_lret"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getVitreous() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("p_rvit")) {
			sb.append("OD ");		
			sb.append(getValue("p_rvit"));		
			sb.append("; ");
		}
		if(isPresent("p_lvit")) {
			sb.append("OS ");
			sb.append(getValue("p_lvit"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getCrt() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("p_rcrt")) {
			sb.append("OD ");		
			sb.append(getValue("p_rcrt"));		
			sb.append("; ");
		}
		if(isPresent("p_lcrt")) {
			sb.append("OS ");
			sb.append(getValue("p_lcrt"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	public String getFace() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("ext_rface")) {
			sb.append("Right side ");		
			sb.append(getValue("ext_rface"));		
			sb.append("; ");
		}
		if(isPresent("ext_lface")) {
			sb.append(" Left side ");
			sb.append(getValue("ext_lface"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getUpperLid() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("ext_rul")) {
			sb.append("OD ");		
			sb.append(getValue("ext_rul"));		
			sb.append("; ");
		}
		if(isPresent("ext_lul")) {
			sb.append("OS ");
			sb.append(getValue("ext_lul"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getLowerLid() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("ext_rll")) {
			sb.append("OD ");		
			sb.append(getValue("ext_rll"));		
			sb.append("; ");
		}
		if(isPresent("ext_lll")) {
			sb.append("OS ");
			sb.append(getValue("ext_lll"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getPunctum() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("ext_rpunc")) {
			sb.append("OD ");		
			sb.append(getValue("ext_rpunc"));		
			sb.append("; ");
		}
		if(isPresent("ext_lpunc")) {
			sb.append("OS ");
			sb.append(getValue("ext_lpunc"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getLacrimalLake() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("ext_rlake")) {
			sb.append("OD ");		
			sb.append(getValue("ext_rlake"));		
			sb.append("; ");
		}
		if(isPresent("ext_llake")) {
			sb.append("OS ");
			sb.append(getValue("ext_llake"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getLacrimalIrrigation() {
		StringBuilder sb = new StringBuilder();		
		if(isPresent("ext_rirrig")) {
			sb.append("OD ");		
			sb.append(getValue("ext_rirrig"));		
			sb.append("; ");
		}
		if(isPresent("ext_lirrig")) {
			sb.append("OS ");
			sb.append(getValue("ext_lirrig"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getNLD() {
		StringBuilder sb = new StringBuilder();		
		if(isPresent("ext_rnld")) {
			sb.append("OD ");		
			sb.append(getValue("ext_rnld"));		
			sb.append("; ");
		}
		if(isPresent("ext_lnld")) {
			sb.append("OS ");
			sb.append(getValue("ext_lnld"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getDyeDisappearance() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("ext_rdye")) {
			sb.append("OD ");		
			sb.append(getValue("ext_rdye"));		
			sb.append("; ");
		}
		if(isPresent("ext_ldye")) {
			sb.append("OS ");
			sb.append(getValue("ext_ldye"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getMarginReflexDistance() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("lid_rmrd")) {
			sb.append("OD ");		
			sb.append(getValue("lid_rmrd"));		
			sb.append("; ");
		}
		if(isPresent("lid_lmrd")) {
			sb.append("OS ");
			sb.append(getValue("lid_lmrd"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getLevatorFunction() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("lid_rlev")) {
			sb.append("OD ");		
			sb.append(getValue("lid_rlev"));		
			sb.append("; ");
		}
		if(isPresent("lid_llev")) {
			sb.append("OS ");
			sb.append(getValue("lid_llev"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getInferiorScleralShow() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("lid_riss")) {
			sb.append("OD ");		
			sb.append(getValue("lid_riss"));		
			sb.append("; ");
		}
		if(isPresent("lid_liss")) {
			sb.append("OS ");
			sb.append(getValue("lid_liss"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getCNVii() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("lid_rcn7")) {
			sb.append("OD ");		
			sb.append(getValue("lid_rcn7"));		
			sb.append("; ");
		}
		if(isPresent("lid_lcn7")) {
			sb.append("OS ");
			sb.append(getValue("lid_lcn7"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getOCTS(){
		StringBuilder sb = new StringBuilder();	
		if(isPresent("oct_s_l")) {
			sb.append("OD ");		
			sb.append(getValue("oct_s_l"));		
			sb.append("; ");
		}
		if(isPresent("oct_s_r")) {
			sb.append("OS ");
			sb.append(getValue("oct_s_r"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getOCTI(){
		StringBuilder sb = new StringBuilder();	
		if(isPresent("oct_i_l")) {
			sb.append("OD ");		
			sb.append(getValue("oct_i_l"));		
			sb.append("; ");
		}
		if(isPresent("oct_i_r")) {
			sb.append("OS ");
			sb.append(getValue("oct_i_r"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getOCTTSNIT(){
		StringBuilder sb = new StringBuilder();
		
		if(isPresent("oct_tsnit_l_1") || isPresent("oct_tsnit_l_2") || isPresent("oct_tsnit_l_3") || isPresent("oct_tsnit_l_4")) {
			sb.append("OD ");	
			sb.append(" (");
			if(isPresent("oct_tsnit_l_1")){
				sb.append(getValue("oct_tsnit_l_1"));
				sb.append(", ");
			}
			
			if(isPresent("oct_tsnit_l_2")){
				sb.append(getValue("oct_tsnit_l_2"));	
				sb.append(", ");
			}
			
			if(isPresent("oct_tsnit_l_3")){
				sb.append(getValue("oct_tsnit_l_3"));	
				sb.append(", ");
			}
			
			if(isPresent("oct_tsnit_l_4")){
				sb.append(getValue("oct_tsnit_l_4"));	
			}
			if(sb.toString().indexOf(",") == (sb.length() - 1)){
				sb.substring(0, sb.length() - 1);
			}
			sb.append(" )");
			sb.append("; ");
		}
		
		if(isPresent("oct_tsnit_r_1") || isPresent("oct_tsnit_r_2") || isPresent("oct_tsnit_r_3") || isPresent("oct_tsnit_r_4")) {
			sb.append("OS ");	
			sb.append(" (");
			if(isPresent("oct_tsnit_r_1")){
				sb.append(getValue("oct_tsnit_r_1"));
				sb.append(", ");
			}
			
			if(isPresent("oct_tsnit_r_2")){
				sb.append(getValue("oct_tsnit_r_2"));	
				sb.append(", ");
			}
			
			if(isPresent("oct_tsnit_r_3")){
				sb.append(getValue("oct_tsnit_r_3"));	
				sb.append(", ");
			}
			
			if(isPresent("oct_tsnit_r_4")){
				sb.append(getValue("oct_tsnit_r_4"));	
			}
			if(sb.toString().indexOf(",") == (sb.length() - 1)){
				sb.substring(0, sb.length() - 1);
			}
			sb.append(" )");
			sb.append("; ");
		}
		return sb.toString();
	}
	
	public String getOCTBlank() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("oct_l")) {
			sb.append("OD ");		
			sb.append(getValue("oct_l"));		
			sb.append("; ");
		}
		if(isPresent("oct_r")) {
			sb.append("OS ");		
			sb.append(getValue("oct_r"));		
			sb.append("; ");
		}
		return sb.toString();
	}
	
	public String getOCTMacula() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("oct_macula_l")) {
			sb.append("OD ");		
			sb.append(getValue("oct_macula_l"));		
			sb.append("; ");
		}
		if(isPresent("oct_macula_r")) {
			sb.append("OS ");		
			sb.append(getValue("oct_macula_r"));		
			sb.append("; ");
		}
		return sb.toString();
	}
	
	public String getVF() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("vf_l")) {
			sb.append("OD ");		
			sb.append(getValue("vf_l"));		
			sb.append("; ");
		}
		if(isPresent("vf_r")) {
			sb.append("OS ");		
			sb.append(getValue("vf_r"));		
			sb.append("; ");
		}
		return sb.toString();
	}
	
	public String getBlink() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("lid_rblink")) {
			sb.append("OD ");		
			sb.append(getValue("lid_rblink"));		
			sb.append("; ");
		}
		if(isPresent("lid_lblink")) {
			sb.append("OS ");
			sb.append(getValue("lid_lblink"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getBells() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("lid_rbell")) {
			sb.append("OD ");		
			sb.append(getValue("lid_rbell"));		
			sb.append("; ");
		}
		if(isPresent("lid_lbell")) {
			sb.append("OS ");
			sb.append(getValue("lid_lbell"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getSchirmertest() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("lid_rschirm")) {
			sb.append("OD ");		
			sb.append(getValue("lid_rschirm"));		
			sb.append("; ");
		}
		if(isPresent("lid_lschirm")) {
			sb.append("OS ");
			sb.append(getValue("lid_lschirm"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getLagophthalmos() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("lid_rlag")) {
			sb.append("OD ");		
			sb.append(getValue("lid_rlag"));		
			sb.append("; ");
		}
		if(isPresent("lid_llag")) {
			sb.append("OS ");
			sb.append(getValue("lid_llag"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getHertel() {
		StringBuilder sb = new StringBuilder();	
		if(isPresent("ext_rhertel")) {
			sb.append("OD ");		
			sb.append(getValue("ext_rhertel"));		
			sb.append("; ");
		}
		if(isPresent("ext_lhertel")) {
			sb.append("OS ");
			sb.append(getValue("ext_lhertel"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String getRetropulsion() {
		StringBuilder sb = new StringBuilder();
		if(isPresent("ext_rretro")) {
			sb.append("OD ");		
			sb.append(getValue("ext_rretro"));		
			sb.append("; ");
		}
		if(isPresent("ext_lretro")) {
			sb.append("OS ");
			sb.append(getValue("ext_lretro"));	
			sb.append(". ");	
		}
		return sb.toString();
	}
	
	public String[] getGlassesRx1(int app_no){
		String str[] = new String[4];
		int count = 0;
		EyeformSpecsHistoryDao dao = (EyeformSpecsHistoryDao)SpringUtils.getBean(EyeformSpecsHistoryDao.class);
		List<EyeformSpecsHistory> specs = dao.getByAppointmentNo(app_no);
		for(EyeformSpecsHistory spec:specs){
			StringBuilder sb = new StringBuilder();
			StringBuilder sb1 = new StringBuilder();
			
			if(spec.getOdSph() != null){
				if(spec.getOdSph().length() > 0){
					sb1.append(spec.getOdSph());
				}
			}
			if(spec.getOdCyl() != null){
				if(spec.getOdCyl().length() > 0){
					sb1.append(" " +spec.getOdCyl());
				}
			}
			if(spec.getOdAxis() != null){
				if(spec.getOdAxis().length() > 0){
					sb1.append("x " + spec.getOdAxis());
				}
			}
			if(spec.getOdAdd() != null){
				if(spec.getOdAdd().length() > 0){
					sb1.append(" add " + spec.getOdAdd());
				}
			}
			if(spec.getOdPrism() != null){
				if(spec.getOdPrism().length() > 0){
					sb1.append(" prism " + spec.getOdPrism());
				}
			}
			if(sb1.length() > 0){
				sb1.append("; ");
				sb1.insert(0, "OD ");
				sb.append(sb1);
			}
			
			StringBuilder sb2 = new StringBuilder();
			if(spec.getOsSph() != null){
				if(spec.getOsSph().length() > 0){
					sb2.append(" " +spec.getOsSph());
				}
			}
			if(spec.getOsCyl() != null){
				if(spec.getOsCyl().length() > 0){
					sb2.append(" " +spec.getOsCyl());
				}
			}
			if(spec.getOsAxis() != null){
				if(spec.getOsAxis().length() > 0){
					sb2.append(" x " + spec.getOsAxis());
				}
			}
			if(spec.getOsAdd() != null){
				if(spec.getOsAdd().length() > 0){
					sb2.append(" add " + spec.getOsAdd());
				}
			}
			if(spec.getOsPrism() != null){
				if(spec.getOsPrism().length() > 0){
					sb2.append(" prism " + spec.getOsPrism());
				}
			}
			if(sb2.length() > 0){
				sb2.append("; ");
				sb2.insert(0, "OS ");
				sb.append(sb2);
			}
			
			if(spec.getDateStr() != null){
				if(spec.getDateStr().length() > 0){
					sb.append("date ");
					sb.append(spec.getDateStr());
					
					sb.append("; ");
				}
			}
			if(spec.getNote() != null){
				if(spec.getNote().length() > 0){
					sb.append("note ");
					sb.append(spec.getNote());
					sb.append(".");
				}
			}
			str[count] = sb.toString();
			count ++;
		}
		return str;
	}

	public String getGlassesRxByAppointmentNumber(final Integer appointmentNumber) {
		EyeformSpecsHistoryDao dao = SpringUtils.getBean(EyeformSpecsHistoryDao.class);
		return getGlassesRx(dao.getByAppointmentNo(appointmentNumber));
	}

	public String getGlassesRxByDemographicNo(final Integer demographicNo) {
		EyeformSpecsHistoryDao dao = SpringUtils.getBean(EyeformSpecsHistoryDao.class);
		return getGlassesRx(dao.getByDemographicNo(demographicNo));
	}
	
	public String getGlassesRx(final List<EyeformSpecsHistory> specs){
		StringBuilder sb = new StringBuilder();
		if(specs.size() > 0){
			sb.append("<b>Glasses Rx</b> ");
		}
		for(EyeformSpecsHistory spec:specs){
			if(spec.getType() != null){
				sb.append(" \nType:" + spec.getType());
			}
			StringBuilder sb1 = new StringBuilder();
			if(spec.getOdSph() != null){
				if(spec.getOdSph().length() > 0){
					sb1.append(spec.getOdSph());
				}
			}
			if(spec.getOdCyl() != null){
				if(spec.getOdCyl().length() > 0){
					sb1.append(" " +spec.getOdCyl());
				}
			}
			if(spec.getOdAxis() != null){
				if(spec.getOdAxis().length() > 0){
					sb1.append("x " + spec.getOdAxis());
				}
			}
			if(spec.getOdAdd() != null){
				if(spec.getOdAdd().length() > 0){
					sb1.append(" add " + spec.getOdAdd());
				}
			}
			if(spec.getOdPrism() != null){
				if(spec.getOdPrism().length() > 0){
					sb1.append(" prism " + spec.getOdPrism());
				}
			}
			if(sb1.length() > 0){
				sb1.append("; ");
				sb1.insert(0, " OD ");
				sb.append(sb1);
			}
			
			StringBuilder sb2 = new StringBuilder();
			if(spec.getOsSph() != null){
				if(spec.getOsSph().length() > 0){
					sb2.append(" " +spec.getOsSph());
				}
			}
			if(spec.getOsCyl() != null){
				if(spec.getOsCyl().length() > 0){
					sb2.append(" " +spec.getOsCyl());
				}
			}
			if(spec.getOsAxis() != null){
				if(spec.getOsAxis().length() > 0){
					sb2.append(" x " + spec.getOsAxis());
				}
			}
			if(spec.getOsAdd() != null){
				if(spec.getOsAdd().length() > 0){
					sb2.append(" add " + spec.getOsAdd());
				}
			}
			if(spec.getOsPrism() != null){
				if(spec.getOsPrism().length() > 0){
					sb2.append(" prism " + spec.getOsPrism());
				}
			}
			if(sb2.length() > 0){
				sb2.append("; ");
				sb2.insert(0, " OS ");
				sb.append(sb2);
			}
			
			
			if(spec.getDateStr() != null){
				if(spec.getDateStr().length() > 0){
					sb.append("date ");
					sb.append(spec.getDateStr());
					
					sb.append("; ");
				}
			}
			if(spec.getNote() != null){
				if(spec.getNote().length() > 0){
					sb.append("note ");
					sb.append(spec.getNote());
					sb.append(".");
				}
			}
		}
		return sb.toString();
	}
	/*eyeform3 end*/
	
	
	private String getValue(String key) {
		if(mmap.get(key)!=null)
			return mmap.get(key).getDataField();
		return  "";
	}
	
	private boolean isPresent(String key) {
		if(mmap.get(key)!=null)
			return true;
		return false;
	}
}
