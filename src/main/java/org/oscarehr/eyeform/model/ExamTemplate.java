package org.oscarehr.eyeform.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.oscarehr.common.model.AbstractModel;

@Entity
@Table(name="examTemplate")
public class ExamTemplate extends AbstractModel<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String examTemplateName;
	
	private String lastUpdateProviderNo;
	
	private String examTemplateProviderNo;
	
	private Date lastUpdateDate = new Date();;
	
	private String status = "0";

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getExamTemplateName() {
		return examTemplateName;
	}

	public void setExamTemplateName(String examTemplateName) {
		this.examTemplateName = examTemplateName;
	}

	public String getLastUpdateProviderNo() {
		return lastUpdateProviderNo;
	}

	public void setLastUpdateProviderNo(String lastUpdateProviderNo) {
		this.lastUpdateProviderNo = lastUpdateProviderNo;
	}

	public String getExamTemplateProviderNo() {
		return examTemplateProviderNo;
	}

	public void setExamTemplateProviderNo(String examTemplateProviderNo) {
		this.examTemplateProviderNo = examTemplateProviderNo;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
}
