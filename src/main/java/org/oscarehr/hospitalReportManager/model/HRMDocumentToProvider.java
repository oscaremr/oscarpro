/**
 * Copyright (c) 2008-2012 Indivica Inc.
 *
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "indivica.ca/gplv2"
 * and "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.hospitalReportManager.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.oscarehr.common.model.AbstractModel;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@NoArgsConstructor
public class HRMDocumentToProvider extends AbstractModel<Integer>  {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Setter(AccessLevel.NONE)
	private Integer id;
	
	private String providerNo;
	private Integer hrmDocumentId;
	private Integer signedOff =0;
	private Date signedOffTimestamp;
	private Integer viewed = 0;
	private boolean filed;
	private boolean deleted = false;

	public HRMDocumentToProvider(final String providerNo, final Integer hrmDocumentId) {
		this.providerNo = providerNo;
		this.hrmDocumentId = hrmDocumentId;
		this.deleted = false;
	}
}
