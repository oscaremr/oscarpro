/**
 * Copyright (c) 2008-2012 Indivica Inc.
 *
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "indivica.ca/gplv2"
 * and "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.hospitalReportManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import omd.hrm.OmdCds;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.cxf.helpers.FileUtils;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.IncomingLabRulesDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.IncomingLabRules;
import org.oscarehr.common.model.Provider;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentDao;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentSubClassDao;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentToDemographicDao;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentToProviderDao;
import org.oscarehr.hospitalReportManager.model.HRMDocument;
import org.oscarehr.hospitalReportManager.model.HRMDocumentSubClass;
import org.oscarehr.hospitalReportManager.model.HRMDocumentToDemographic;
import org.oscarehr.hospitalReportManager.model.HRMDocumentToProvider;
import org.oscarehr.managers.DemographicManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.springframework.core.io.ClassPathResource;
import org.xml.sax.SAXException;
import oscar.OscarProperties;

@NoArgsConstructor
@Slf4j
public class HRMReportParser {

	private static final DemographicDao DEMOGRAPHIC_DAO
			= SpringUtils.getBean(DemographicDao.class);
	private static final DemographicManager DEMOGRAPHIC_MANAGER
			= SpringUtils.getBean(DemographicManager.class);
	private static final HRMDocumentDao HRM_DOCUMENT_DAO
			= SpringUtils.getBean(HRMDocumentDao.class);
	private static final HRMDocumentSubClassDao HRM_DOCUMENT_SUB_CLASS_DAO
			= SpringUtils.getBean(HRMDocumentSubClassDao.class);
	private static final HRMDocumentToDemographicDao HRM_DOCUMENT_TO_DEMOGRAPHIC_DAO
			= SpringUtils.getBean(HRMDocumentToDemographicDao.class);
	private static final HRMDocumentToProviderDao HRM_DOCUMENT_TO_PROVIDER_DAO
			= SpringUtils.getBean(HRMDocumentToProviderDao.class);
	private static final IncomingLabRulesDao INCOMING_LAB_RULES_DAO
			= SpringUtils.getBean(IncomingLabRulesDao.class);
	private static final ProviderDao PROVIDER_DAO
			= SpringUtils.getBean(ProviderDao.class);

	public static HRMReport parseReport(LoggedInInfo loggedInInfo, Integer hrmDocumentId) {
		val hrmDocument = HRM_DOCUMENT_DAO.find(hrmDocumentId);
		return hrmDocument != null
			? parseReport(loggedInInfo, hrmDocument.getReportFile())
			: null;
	}

	public static HRMReport parseReport(LoggedInInfo loggedInInfo, String hrmReportFileLocation) {
		OmdCds root = null;
		log.debug("Parsing the Report in the location: {}", hrmReportFileLocation);
		String fileData = null;
		if (hrmReportFileLocation != null) {
			try {
				//a lot of the parsers need to refer to a file and even when they provide functions like parse(String text)
				//it will not parse the same way because it will treat the text as a URL
				//so we take the lab and store them temporarily in a random filename in /tmp/oscar-sftp/
				var tmpXMLholder = new File(hrmReportFileLocation);
				//check the DOCUMENT_DIR
				if(!tmpXMLholder.exists()) {
					String place= OscarProperties.getInstance().getProperty("DOCUMENT_DIR");
					tmpXMLholder = new File(place + File.separator + hrmReportFileLocation);
				}
				if(!tmpXMLholder.exists()) {
					log.warn("unable to find the HRM report. checked " + hrmReportFileLocation + ", and in the document_dir");
					return null;
				}
				if (tmpXMLholder.exists()) fileData = FileUtils.getStringFromFile(tmpXMLholder);
				// Parse an XML document into a DOM tree.
				// Create a SchemaFactory capable of understanding WXS schemas.
				val factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
				// Load a WXS schema, represented by a Schema instance.
				val schemaFile = new ClassPathResource("/xsd/hrm/1.1.2/ontariomd_hrm.xsd").getFile();
				val schemaSource = new StreamSource(schemaFile);
				val schema = factory.newSchema(schemaSource);
				val jc = JAXBContext.newInstance("omd.hrm");
				val u = jc.createUnmarshaller();
				u.setSchema(schema);
				root = (OmdCds) u.unmarshal(new FileInputStream(tmpXMLholder));
			} catch (SAXException e) {
				log.error("SAX ERROR PARSING XML " + e);
			} catch (FileNotFoundException e) {
				log.error("FILE ERROR PARSING XML " + e);
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				log.error("error", e);
				if (e.getLinkedException() != null) {
					SFTPConnector.notifyHrmError(loggedInInfo, e.getLinkedException().getMessage());
				} else {
					SFTPConnector.notifyHrmError(loggedInInfo, e.getMessage());
				}
			} catch (IOException e) {
				log.error("ERROR READING report_manager_cds.xsd RESOURCE" + e);
			}
			if (root != null && fileData != null) {
				return new HRMReport(root, hrmReportFileLocation, fileData);
			}
		}
		return null;
	}

	public static void addReportToInbox(LoggedInInfo loggedInInfo, HRMReport report) {
		if(report == null) {
			log.debug("addReportToInbox cannot continue, report parameter is null");
			return;
		}
		log.debug("Adding Report to Inbox, for file:"+report.getFileLocation());
		val document = new HRMDocument();
		val fileLocation = new File(report.getFileLocation());
		document.setReportFile(fileLocation.getName());
		document.setReportStatus(report.getResultStatus());
		document.setReportType(report.getFirstReportClass());
		document.setTimeReceived(new Date());
		val reportFileData = report.getFileData();
		val noMessageIdFileData = reportFileData.replaceAll("<MessageUniqueID>.*?</MessageUniqueID>", "<MessageUniqueID></MessageUniqueID>");
		val noTransactionInfoFileData = reportFileData.replaceAll("<TransactionInformation>.*?</TransactionInformation>", "<TransactionInformation></TransactionInformation>");
		val noDemograhpicInfoFileData = reportFileData.replaceAll("<Demographics>.*?</Demographics>", "<Demographics></Demographics").replaceAll("<MessageUniqueID>.*?</MessageUniqueID>", "<MessageUniqueID></MessageUniqueID>");
		val noMessageIdHash = DigestUtils.md5Hex(noMessageIdFileData);
		val noTransactionInfoHash = DigestUtils.md5Hex(noTransactionInfoFileData);
		val noDemographicInfoHash = DigestUtils.md5Hex(noDemograhpicInfoFileData);
		document.setReportHash(noMessageIdHash);
		document.setReportLessTransactionInfoHash(noTransactionInfoHash);
		document.setReportLessDemographicInfoHash(noDemographicInfoHash);
		document.setReportDate(HRMReportParser.getAppropriateDateFromReport(report));
		document.setDescription("");
		// We're going to check to see if there's a match in the database already for either of these
		// report hash matches = duplicate report for same recipient
		// no transaction info hash matches = duplicate report, but different recipient
		val exactMatchList = HRM_DOCUMENT_DAO.findByHash(noMessageIdHash);
		if (exactMatchList == null || exactMatchList.size() == 0) {
			val sameReportDifferentRecipientReportList = HRM_DOCUMENT_DAO.findByNoTransactionInfoHash(noTransactionInfoHash);
			if (sameReportDifferentRecipientReportList != null && sameReportDifferentRecipientReportList.size() > 0) {
				log.debug("Same Report Different Recipient, for file:"+report.getFileLocation());
				HRMReportParser.routeReportToProvider(sameReportDifferentRecipientReportList.get(0), report);
			} else {
				// New report
				HRM_DOCUMENT_DAO.persist(document);
				log.debug("MERGED DOCUMENTS ID"+document.getId());
				HRMReportParser.routeReportToDemographic(report, document);
				HRMReportParser.doSimilarReportCheck(loggedInInfo, report, document);
				// Attempt a route to the provider listed in the report -- if they don't exist, note that in the record
				val routeSuccess = HRMReportParser.routeReportToProvider(report, document.getId());
				if (!routeSuccess) {
					log.debug("Adding the provider name to the list of unidentified providers, for file:"+report.getFileLocation());
					// Add the provider name to the list of unidentified providers for this report
					document.setUnmatchedProviders((document.getUnmatchedProviders() != null ? document.getUnmatchedProviders() : "") + "|" + ((report.getDeliverToUserIdLastName()!=null)?report.getDeliverToUserIdLastName() + ", " + report.getDeliverToUserIdFirstName():report.getDeliverToUserId()) + " (" + report.getDeliverToUserId() + ")");
					HRM_DOCUMENT_DAO.merge(document);
					// Route this report to the "system" user so that a search for "all" in the inbox will come up with them
					HRMReportParser.routeReportToProvider(document.getId(), "-1");
				}
				HRMReportParser.routeReportToSubClass(report, document.getId());
			}
		} else {
			// We've seen this one before.  Increment the counter on how many times we've seen it before
			log.debug("We've seen this report before. Increment the counter on how many times we've seen it before, for file:"+report.getFileLocation());
			val existingDocument = HRM_DOCUMENT_DAO.findById(exactMatchList.get(0)).get(0);
			existingDocument.setNumDuplicatesReceived((existingDocument.getNumDuplicatesReceived() != null ? existingDocument.getNumDuplicatesReceived() : 0) + 1);
			HRM_DOCUMENT_DAO.merge(existingDocument);
		}
	}

	private static void routeReportToDemographic(HRMReport report, HRMDocument mergedDocument) {
		if(report == null) {
			log.debug("routeReportToDemographic cannot continue, report parameter is null");
			return;
		}
		log.debug("Routing Report To Demographic, for file:"+report.getFileLocation());
		// Search the demographics on the system for a likely match and route it to them automatically
		val matchingDemographicListByHin = DEMOGRAPHIC_DAO.searchDemographicByHIN(report.getHCN());
		if (matchingDemographicListByHin.size() > 0) {
			if (OscarProperties.getInstance().isPropertyActive("omd_hrm_demo_matching_criteria")) {
				for (Demographic d : matchingDemographicListByHin) {
					if (report.getGender().equalsIgnoreCase(d.getSex())
							&& report.getDateOfBirthAsString().equalsIgnoreCase(d.getBirthDayAsString())
							&& report.getLegalLastName().equalsIgnoreCase(d.getLastName())) {
						HRMReportParser.routeReportToDemographic(mergedDocument.getId(), d.getDemographicNo());
						break;
					}
				}
			} else {
				// if there is a matching record assign to variable
				val demographic = matchingDemographicListByHin.get(0); // searchDemographicByHIN typically returns only one result where there is a match
				// if not empty and DOB matches as well, route report to Demographic
				if (report.getDateOfBirthAsString().equalsIgnoreCase(demographic.getBirthDayAsString())) {
					HRMReportParser.routeReportToDemographic(mergedDocument.getId(), demographic.getDemographicNo());
				}
			}
		}
	}

	private static boolean hasSameStatus(HRMReport report, HRMReport loadedReport) {
		return report.getResultStatus() == null
				|| report.getResultStatus().equalsIgnoreCase(loadedReport.getResultStatus());
	}

	private static void doSimilarReportCheck(LoggedInInfo loggedInInfo, HRMReport report, HRMDocument mergedDocument) {
		if(report == null) {
			log.debug("doSimilarReportCheck cannot continue, report parameter is null");
			return;
		}
		log.debug(
				"Identifying if this is a report that we received before, but was sent to the wrong "
						+ "demographic, for file: {}",
				report.getFileLocation()
		);
		// Check #1: Identify if this is a report that we received before, but was sent to the wrong demographic
		val parentReportList = HRM_DOCUMENT_DAO.findAllWithSameNoDemographicInfoHash(
				mergedDocument.getReportLessDemographicInfoHash()
		);
		if (parentReportList != null && parentReportList.size() > 0) {
			for (Integer id : parentReportList) {
				if (id != null && id.intValue() != mergedDocument.getId().intValue()) {
					mergedDocument.setParentReport(id);
					HRM_DOCUMENT_DAO.merge(mergedDocument);
					return;
				}
			}
		}
		// Load all the reports for this demographic into memory -- check by name only
		val thisDemoHrmReportList = HRMReportParser.loadAllReportsRoutedToDemographic(loggedInInfo, report.getLegalName());
		for (HRMReport loadedReport : thisDemoHrmReportList) {
			val hasSameReportContent = report.getFirstReportTextContent().equalsIgnoreCase(loadedReport.getFirstReportTextContent());
			val hasSameStatus = hasSameStatus(report,loadedReport);
			val hasSameClass = report.getFirstReportClass().equalsIgnoreCase(loadedReport.getFirstReportClass());
			val hasSameDate = HRMReportParser.getAppropriateDateFromReport(report).equals(HRMReportParser.getAppropriateDateFromReport(loadedReport));
			var threshold = 0;
			if (hasSameReportContent) {
				threshold += 100;
			} else {
				threshold += 10;
			}
			if (hasSameStatus) {
				threshold += 5;
			} else {
				threshold += 10;
			}
			threshold += 10;
			if (hasSameDate) {
				threshold += 20;
			} else {
				threshold += 5;
			}
			if (threshold >= 45) {
				// This is probably a changed report addressed to the same demographic, so set the parent id (as long as this isn't the same report) and we're done!
				if (loadedReport.getHrmParentDocumentId() != null && loadedReport.getHrmDocumentId().intValue() != mergedDocument.getId().intValue()) {
					mergedDocument.setParentReport(loadedReport.getHrmParentDocumentId());
					HRM_DOCUMENT_DAO.merge(mergedDocument);
					return;
				} else if (loadedReport.getHrmParentDocumentId() == null) {
					mergedDocument.setParentReport(loadedReport.getHrmDocumentId());
					HRM_DOCUMENT_DAO.merge(mergedDocument);
					return;
				}
			}
		}
	}

	private static List<HRMReport> loadAllReportsRoutedToDemographic(LoggedInInfo loggedInInfo, String legalName) {
		val matchingDemographicListByName = DEMOGRAPHIC_DAO.searchDemographic(legalName);
		val allRoutedReports = new LinkedList<HRMReport>();
		for (Demographic d : matchingDemographicListByName) {
			val matchingHrmDocumentList = HRM_DOCUMENT_TO_DEMOGRAPHIC_DAO.findByDemographicNo(d.getDemographicNo().toString());
			for (HRMDocumentToDemographic matchingHrmDocument : matchingHrmDocumentList) {
				val hrmDocument = HRM_DOCUMENT_DAO.find(matchingHrmDocument.getHrmDocumentId());
				val hrmReport = HRMReportParser.parseReport(loggedInInfo, hrmDocument.getReportFile());
				if (hrmReport != null) {
				hrmReport.setHrmDocumentId(hrmDocument.getId());
				hrmReport.setHrmParentDocumentId(hrmDocument.getParentReport());
				allRoutedReports.add(hrmReport);
				}
			}
		}
		return allRoutedReports;
	}

	public static void routeReportToSubClass(HRMReport report, Integer reportId) {
		if(report == null) {
			log.debug("routeReportToSubClass cannot continue, report parameter is null");
			return;
		}
		log.debug("Routing Report To SubClass, for file:"+report.getFileLocation());
		val subClassList = report.getAccompanyingSubclassList();
        boolean firstSubClass = true;
        for (List<Object> subClass : subClassList) {
					val newSubClass = new HRMDocumentSubClass();
            newSubClass.setSubClass((String) subClass.get(0));
            newSubClass.setSubClassMnemonic((String) subClass.get(1));
            newSubClass.setSubClassDescription((String) subClass.get(2));
            newSubClass.setSubClassDateTime((Date) subClass.get(3));
            newSubClass.setSendingFacilityId(report.getSendingFacilityId());
            if (firstSubClass) {
                newSubClass.setActive(true);
                firstSubClass = false;
            }
            newSubClass.setHrmDocumentId(reportId);
            HRM_DOCUMENT_SUB_CLASS_DAO.merge(newSubClass);
        }
		val reportSubClass = report.getFirstReportSubClass();
        if (StringUtils.isNotEmpty(reportSubClass)) {
					val newSubClass = new HRMDocumentSubClass();
            newSubClass.setSubClass(reportSubClass);
            newSubClass.setSubClassMnemonic("");
            newSubClass.setSubClassDescription("");
            newSubClass.setSubClassDateTime(null);
            newSubClass.setSendingFacilityId(report.getSendingFacilityId());
            if (firstSubClass) {
                newSubClass.setActive(true);
            }
            newSubClass.setHrmDocumentId(reportId);
            HRM_DOCUMENT_SUB_CLASS_DAO.merge(newSubClass);
        }
	}

	public static String getAppropriateDateStringFromReport(HRMReport report) {
		if (report.getFirstReportClass().equalsIgnoreCase("Diagnostic Imaging Report") || report.getFirstReportClass().equalsIgnoreCase("Cardio Respiratory Report")) {
			return (String) report.getAccompanyingSubclassList().get(0).get(4);
		}
		val calendar = report.getFirstReportEventTime();
		val sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
		sdf.setTimeZone(calendar.getTimeZone());
		return sdf.format(calendar.getTime());
	}

	public static Date getAppropriateDateFromReport(HRMReport report) {
		if (report.getFirstReportClass().equalsIgnoreCase("Diagnostic Imaging Report") || report.getFirstReportClass().equalsIgnoreCase("Cardio Respiratory Report")) {
			return ((Date) (report.getAccompanyingSubclassList().get(0).get(3)));
		}
		return report.getFirstReportEventTime().getTime();
	}

	public static boolean routeReportToProvider(HRMReport report, Integer reportId) {
		if(report == null) {
			log.debug("routeReportToProvider cannot continue, report parameter is null");
			return false;
		}
		log.debug("Routing Report to Provider, for file:"+report.getFileLocation());
		val practitionerNo = report.getDeliverToUserId();
		Provider sendToProvider = null;
		if (OscarProperties.getInstance().isPropertyActive("OMD_match_using_OLIS_identifier_type")) {
			if (practitionerNo.startsWith("D")) {
				sendToProvider = PROVIDER_DAO.getProviderByPractitionerNoAndOlisType(practitionerNo.substring(1), "MDL");
			} else if (practitionerNo.startsWith("N")) {
				sendToProvider = PROVIDER_DAO.getProviderByPractitionerNoAndOlisType(practitionerNo.substring(1), "NPL");
			}
		} else {
			sendToProvider = PROVIDER_DAO.getProviderByPractitionerNo(practitionerNo.substring(1));
		}
		val sendToProviderList = new LinkedList<Provider>();
		if (sendToProvider != null) {
			sendToProviderList.add(sendToProvider);
		}
		if (OscarProperties.getInstance().isPropertyActive("queens_resident_tagging")) {
			val matchingDemographicListByHin = DEMOGRAPHIC_DAO.searchDemographicByHIN(report.getHCN());
			if (!matchingDemographicListByHin.isEmpty()) {
				val demographic = DEMOGRAPHIC_DAO.searchDemographicByHIN(report.getHCN()).get(0);
				//add mrp if not already in list
				if (sendToProvider != null && !sendToProvider.getProviderNo().equals(demographic.getProviderNo()) && demographic.getProvider() != null) {
					sendToProviderList.add(demographic.getProvider());
				}
				//get and add alt providers
				val residentIds = DEMOGRAPHIC_MANAGER.getQueensResidentProviderNumbers(
						demographic.getDemographicNo()
				);
					for (String residentId : residentIds) {
						if (residentId != null && !residentId.equals("")) {
							Provider p = PROVIDER_DAO.getProvider(residentId);
							if (p != null) { sendToProviderList.add(p); }
						}
					}
				}
			}
		for (Provider p : sendToProviderList) {
			val existingHRMDocumentToProviders =  HRM_DOCUMENT_TO_PROVIDER_DAO.findByHrmDocumentIdAndProviderNoList(reportId, p.getProviderNo());
			if (existingHRMDocumentToProviders == null || existingHRMDocumentToProviders.size() == 0) {
				val providerRouting = new HRMDocumentToProvider();
				providerRouting.setHrmDocumentId(reportId);
				providerRouting.setProviderNo(p.getProviderNo());
				providerRouting.setSignedOff(0);
				HRM_DOCUMENT_TO_PROVIDER_DAO.merge(providerRouting);
			}
			//Gets the list of IncomingLabRules pertaining to the current provider
			val incomingLabRules = INCOMING_LAB_RULES_DAO.findCurrentByProviderNo(p.getProviderNo());
			//If the list is not null
			if (incomingLabRules != null) {
				//For each labRule in the list
				for (IncomingLabRules labRule : incomingLabRules) {
					if (labRule.getForwardTypeStrings().contains("HRM")) {
						//Creates a string of the provider number that the lab will be forwarded to
						String forwardProviderNumber = labRule.getFrwdProviderNo();
						boolean filedStatus =
								StringUtils.isNotEmpty(labRule.getStatus()) && labRule.getStatus().equals("F");
						//Checks to see if this provider is already linked to this lab
						var hrmDocumentToProvider
								= HRM_DOCUMENT_TO_PROVIDER_DAO.findByHrmDocumentIdAndProviderNo(
										reportId, forwardProviderNumber
						);
						//If a record was not found
						if (hrmDocumentToProvider == null) {
							//Puts the information into the HRMDocumentToProvider object
							hrmDocumentToProvider = new HRMDocumentToProvider();
							hrmDocumentToProvider.setHrmDocumentId(reportId);
							hrmDocumentToProvider.setProviderNo(forwardProviderNumber);
							hrmDocumentToProvider.setSignedOff(0);
							hrmDocumentToProvider.setFiled(false);
							//Stores it in the table
							HRM_DOCUMENT_TO_PROVIDER_DAO.persist(hrmDocumentToProvider);
							val originatingProviderRouting
									= HRM_DOCUMENT_TO_PROVIDER_DAO.findByHrmDocumentIdAndProviderNo(
											reportId, p.getProviderNo()
							);
							originatingProviderRouting.setFiled(filedStatus);
							HRM_DOCUMENT_TO_PROVIDER_DAO.merge(originatingProviderRouting);
						}
					}
				}
			}
		}
		return sendToProviderList.size() > 0;
	}

	public static void setDocumentParent(String reportId, String childReportId) {
		try {
			val childDocument = HRM_DOCUMENT_DAO.find(childReportId);
			childDocument.setParentReport(Integer.parseInt(reportId));
			HRM_DOCUMENT_DAO.merge(childDocument);
		} catch (Exception e) {
			log.error("Can't set HRM document parent", e);
		}
	}

	public static void routeReportToProvider(HRMDocument originalDocument, HRMReport newReport) {
		routeReportToProvider(newReport, originalDocument.getId());
	}

	public static void routeReportToProvider(Integer reportId, String providerNo) {
		val providerRouting = new HRMDocumentToProvider();
		providerRouting.setHrmDocumentId(reportId);
		providerRouting.setProviderNo(providerNo);
		HRM_DOCUMENT_TO_PROVIDER_DAO.merge(providerRouting);
	}

	public static void signOffOnReport(String providerRoutingId, Integer signOffStatus) {
		val providerRouting = HRM_DOCUMENT_TO_PROVIDER_DAO.find(providerRoutingId);
		if (providerRouting != null) {
			providerRouting.setSignedOff(signOffStatus);
			providerRouting.setSignedOffTimestamp(new Date());
			HRM_DOCUMENT_TO_PROVIDER_DAO.merge(providerRouting);
		}
	}

	public static void routeReportToDemographic(Integer reportId, Integer demographicNo) {
		val demographicRouting = new HRMDocumentToDemographic();
		demographicRouting.setDemographicNo(demographicNo);
		demographicRouting.setHrmDocumentId(reportId);
		demographicRouting.setTimeAssigned(new Date());
		HRM_DOCUMENT_TO_DEMOGRAPHIC_DAO.merge(demographicRouting);
	}
}
