/**
 * Copyright (c) 2008-2012 Indivica Inc.
 *
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "indivica.ca/gplv2"
 * and "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.hospitalReportManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentDao;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentSubClassDao;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentToDemographicDao;
import org.oscarehr.hospitalReportManager.dao.HRMSubClassDao;
import org.oscarehr.hospitalReportManager.model.HRMDocument;
import org.oscarehr.hospitalReportManager.model.HRMDocumentSubClass;
import org.oscarehr.hospitalReportManager.model.HRMDocumentToDemographic;
import org.oscarehr.hospitalReportManager.model.HRMSubClass;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

import oscar.oscarLab.ca.on.HRMResultsData;
import oscar.util.StringUtils;

public class HRMUtil {

	private static final Logger logger = MiscUtils.getLogger();
	
	public static final String DATE = "time_received";
	public static final String SUBCLASS = "subclass";
	public static final String TYPE = "report_type";
	
	private static HRMDocumentDao hrmDocumentDao = (HRMDocumentDao) SpringUtils.getBean("HRMDocumentDao");
	private static HRMDocumentToDemographicDao hrmDocumentToDemographicDao = (HRMDocumentToDemographicDao) SpringUtils.getBean("HRMDocumentToDemographicDao");
	private static HRMSubClassDao hrmSubClassDao = (HRMSubClassDao) SpringUtils.getBean("HRMSubClassDao");
	private static HRMDocumentSubClassDao hrmDocumentSubClassDao = (HRMDocumentSubClassDao) SpringUtils.getBean("HRMDocumentSubClassDao");
	
	public HRMUtil() {
		
	}

	public static Map<String, String> getReportInformation(LoggedInInfo loggedInInfo, String hrmFileLocation) {
		HRMReport hrmReport = HRMReportParser.parseReport(loggedInInfo, hrmFileLocation);
		Map<String, String> reportInformation = new HashMap<String, String>();
		HRMSubClass hrmSubClass = null;
		String dispSubClass = "";
		String categoryName = "";
		if (hrmReport != null) {
			List<HRMDocumentSubClass> subClassList = hrmDocumentSubClassDao.getSubClassesByDocumentId(hrmReport.getHrmDocumentId());

			if (hrmReport.getFirstReportClass().equalsIgnoreCase("Diagnostic Imaging Report") || hrmReport.getFirstReportClass().equalsIgnoreCase("Cardio Respiratory Report")) {
				//Get first sub class to display on eChart
				if (subClassList != null && subClassList.size() > 0) {
					HRMDocumentSubClass firstSubClass = subClassList.get(0);
					hrmSubClass = hrmSubClassDao.findApplicableSubClassMapping(hrmReport.getFirstReportClass(), firstSubClass.getSubClass(), firstSubClass.getSubClassMnemonic(), hrmReport.getSendingFacilityId());
					dispSubClass = hrmSubClass != null ? hrmSubClass.getSubClassDescription() : "";
				}

				if (StringUtils.isNullOrEmpty(dispSubClass) && hrmReport.getAccompanyingSubclassList().size() > 0){
					// if sub class doesn't exist, display the accompanying subclass
					dispSubClass = hrmReport.getFirstAccompanyingSubClass();
				}
			} else {
				//Medical Records Report
				String[] reportSubClass = hrmReport.getFirstReportSubClass() != null ? hrmReport.getFirstReportSubClass().split("\\^") : null;
				
				if (reportSubClass != null) {
					hrmSubClass = hrmSubClassDao.findApplicableSubClassMapping(hrmReport.getFirstReportClass(), reportSubClass[0], null, hrmReport.getSendingFacilityId());
				}
				
				dispSubClass = reportSubClass != null && reportSubClass.length > 1 ? reportSubClass[1] : "";
			}

			if (hrmSubClass != null && hrmSubClass.getHrmCategory() != null) {
				categoryName = hrmSubClass.getHrmCategory().getCategoryName();
			}
		}
		reportInformation.put("category", StringUtils.noNull(categoryName));
		reportInformation.put("subclass", StringUtils.noNull(dispSubClass));
		
		return reportInformation;
	}
	
	public static String getDisplaySubclass(LoggedInInfo loggedInInfo, String hrmFileLocation) {
		Map<String, String> reportInformation = getReportInformation(loggedInInfo, hrmFileLocation);
		return StringUtils.noNull(reportInformation.get("subclass"));
	}
	
	@SuppressWarnings("null")
    public static ArrayList<HashMap<String, ? extends Object>> listHRMDocuments(LoggedInInfo loggedInInfo, String sortBy, String demographicNo){
		ArrayList<HashMap<String, ? extends Object>> hrmdocslist = new ArrayList<HashMap<String, ?>>();
		
		List<HRMDocumentToDemographic> hrmDocResultsDemographic = hrmDocumentToDemographicDao.findByDemographicNo(demographicNo);
		List<HRMDocument> hrmDocumentsAll = new LinkedList<HRMDocument>();
		
		HashMap<String,ArrayList<Integer>> duplicateLabIds=new HashMap<String, ArrayList<Integer>>();
		HashMap<String,HRMDocument> docsToDisplay=filterDuplicates(loggedInInfo, hrmDocResultsDemographic, duplicateLabIds);
		
		for (Map.Entry<String, HRMDocument> entry : docsToDisplay.entrySet()) {
			String duplicateKey=entry.getKey();
			HRMDocument hrmDocument = entry.getValue();
			Map<String, String> reportInformation = getReportInformation(loggedInInfo, hrmDocument.getReportFile());
			
			HashMap<String, Object> curht = new HashMap<String, Object>();
			curht.put("id", hrmDocument.getId());
			curht.put("time_received", hrmDocument.getTimeReceived().toString());
			curht.put("report_type", hrmDocument.getReportType());
			curht.put("report_status", hrmDocument.getReportStatus());
			curht.put("category", reportInformation.get("category"));
			curht.put("description", hrmDocument.getDescription());
			curht.put("subclass",  reportInformation.get("subclass"));

			hrmDocument.setSubclass(reportInformation.get("subclass")); // set to be able to sort
			
			StringBuilder duplicateLabIdQueryString=new StringBuilder();
			ArrayList<Integer> duplicateIdList=duplicateLabIds.get(duplicateKey);
        	if (duplicateIdList!=null)
        	{
				for (Integer duplicateLabIdTemp : duplicateIdList)
            	{
            		if (duplicateLabIdQueryString.length()>0) duplicateLabIdQueryString.append(',');
            		duplicateLabIdQueryString.append(duplicateLabIdTemp);
            	}
			}
        	curht.put("duplicateLabIds", duplicateLabIdQueryString.toString());
			
			hrmdocslist.add(curht);
			hrmDocumentsAll.add(hrmDocument);
			
		}
		
		if (SUBCLASS.equals(sortBy)) {
			Collections.sort(hrmDocumentsAll, HRMDocument.HRM_SUBCLASS_COMPARATOR);
		} else if (TYPE.equals(sortBy)) {
			Collections.sort(hrmDocumentsAll, HRMDocument.HRM_TYPE_COMPARATOR);
		}
		else { 
			Collections.sort(hrmDocumentsAll, HRMDocument.HRM_DATE_COMPARATOR) ;
		}
		
		
		return hrmdocslist;
		
	}
	
	 private static HashMap<String,HRMDocument> filterDuplicates(LoggedInInfo loggedInInfo, List<HRMDocumentToDemographic> hrmDocumentToDemographics, HashMap<String,ArrayList<Integer>> duplicateLabIds) {
		 
		HashMap<String,HRMDocument> docsToDisplay = new HashMap<String,HRMDocument>();
		HashMap<String,HRMReport> labReports=new HashMap<String,HRMReport>();

		 for (HRMDocumentToDemographic hrmDocumentToDemographic : hrmDocumentToDemographics)
		 {
			int id = hrmDocumentToDemographic.getHrmDocumentId() != null ? hrmDocumentToDemographic.getHrmDocumentId() : 0;
			List<HRMDocument> hrmDocuments = hrmDocumentDao.findById(id);

			for (HRMDocument hrmDocument : hrmDocuments)
			{
				HRMReport hrmReport = HRMReportParser.parseReport(loggedInInfo, hrmDocument.getReportFile());
				if (hrmReport == null) continue;
				hrmReport.setHrmDocumentId(hrmDocument.getId());
				String duplicateKey=hrmReport.getSendingFacilityId()+':'+hrmReport.getSendingFacilityReportNo()+':'+hrmReport.getDeliverToUserId();
	
				// if no duplicate
				if (!docsToDisplay.containsKey(duplicateKey))
				{
					docsToDisplay.put(duplicateKey,hrmDocument);
					labReports.put(duplicateKey, hrmReport);
				}
				else // there exists an entry like this one
				{
					HRMReport previousHrmReport=labReports.get(duplicateKey);
					
					logger.debug("Duplicate report found : previous="+previousHrmReport.getHrmDocumentId()+", current="+hrmReport.getHrmDocumentId());
					
					Integer duplicateIdToAdd;
					
					// if the current entry is newer than the previous one then replace it, other wise just keep the previous entry
					if (HRMResultsData.isNewer(hrmReport, previousHrmReport))
					{
						HRMDocument previousHRMDocument = docsToDisplay.get(duplicateKey);
						duplicateIdToAdd=previousHRMDocument.getId();
						
						docsToDisplay.put(duplicateKey,hrmDocument);
						labReports.put(duplicateKey, hrmReport);
					}
					else
					{
						duplicateIdToAdd=hrmDocument.getId();
					}
	
					ArrayList<Integer> duplicateIds=duplicateLabIds.get(duplicateKey);
					if (duplicateIds==null)
					{
						duplicateIds=new ArrayList<Integer>();
						duplicateLabIds.put(duplicateKey, duplicateIds);
					}
					
					duplicateIds.add(duplicateIdToAdd);						
				}
			}
		}
		 
		 return(docsToDisplay);
	 }
    public static ArrayList<HashMap<String, ? extends Object>> listAllHRMDocuments(LoggedInInfo loggedInInfo, String sortBy, String demographicNo){
        ArrayList<HashMap<String, ? extends Object>> hrmdocslist = new ArrayList<HashMap<String, ?>>();
        List<HRMDocumentToDemographic> hrmDocResultsDemographic = hrmDocumentToDemographicDao.findByDemographicNo(demographicNo);
        List<HRMDocument> hrmDocumentsAll = new LinkedList<HRMDocument>();
        HashMap<String,ArrayList<Integer>> duplicateLabIds=new HashMap<String, ArrayList<Integer>>();


        for(HRMDocumentToDemographic hrmDocumentToDemographic : hrmDocResultsDemographic){
            HRMDocument hrmDocument = hrmDocumentDao.find(hrmDocumentToDemographic.getHrmDocumentId());
			Map<String, String> reportInformation = getReportInformation(loggedInInfo, hrmDocument.getReportFile());

			HashMap<String, Object> curht = new HashMap<String, Object>();
			curht.put("id", hrmDocument.getId());
			curht.put("time_received", hrmDocument.getTimeReceived().toString());
			curht.put("report_type", hrmDocument.getReportType());
			curht.put("report_status", hrmDocument.getReportStatus());
			curht.put("category", reportInformation.get("category"));
			curht.put("description", hrmDocument.getDescription());
			curht.put("subclass", reportInformation.get("subclass"));

			hrmDocument.setSubclass(reportInformation.get("subclass")); // set to be able to sort

			hrmdocslist.add(curht);
				hrmDocumentsAll.add(hrmDocument);
        }

		if (SUBCLASS.equals(sortBy)) {
			Collections.sort(hrmDocumentsAll, HRMDocument.HRM_SUBCLASS_COMPARATOR);
		} else if (TYPE.equals(sortBy)) {
			Collections.sort(hrmDocumentsAll, HRMDocument.HRM_TYPE_COMPARATOR);
		} else {
            Collections.sort(hrmDocumentsAll, HRMDocument.HRM_DATE_COMPARATOR) ;
        }

        return hrmdocslist;

    }

	public static ArrayList<HashMap<String, ? extends Object>> listMappings(){
			ArrayList<HashMap<String, ? extends Object>> hrmdocslist = new ArrayList<HashMap<String, ?>>();
			
			List<HRMSubClass> hrmSubClasses = hrmSubClassDao.listAll();
			
			for (HRMSubClass hrmSubClass : hrmSubClasses) {
	
				HashMap<String, Object> curht = new HashMap<String, Object>();
				curht.put("id", hrmSubClass.getSendingFacilityId());
				curht.put("sub_class", hrmSubClass.getSubClassName());
				curht.put("class", hrmSubClass.getClassName());
				curht.put("category", hrmSubClass.getHrmCategory());
				curht.put("mnemonic", (hrmSubClass.getSubClassMnemonic() != null ? hrmSubClass.getSubClassMnemonic() : ""));
				curht.put("description", (hrmSubClass.getSubClassDescription() != null ? hrmSubClass.getSubClassDescription() : ""));
				curht.put("mappingId", hrmSubClass.getId());
				
				hrmdocslist.add(curht);
				
			}
			
			return hrmdocslist;
			
		}
		
	/**
	 * Check the HRMSubClass for the corresponding descriptions, change the description
	 * @param subClassList
	 * @param reportType
	 * @param sendingFacilityId
	 */
	public void findCorrespondingHRMSubClassDescriptions(List<HRMDocumentSubClass> subClassList, String reportType, String sendingFacilityId, String reportSubClass ) {
		
		if (reportType == null || reportType.isEmpty()) {
			return;
		}
		
		if (sendingFacilityId == null || sendingFacilityId.isEmpty()) {
			return;
		}
		
	    for(HRMDocumentSubClass hrmDocumentSubClass: subClassList) {
	    	
	    	HRMSubClass hrmSubClass = hrmSubClassDao.findByClassNameMnemonicFacility(reportType, sendingFacilityId, hrmDocumentSubClass.getSubClassMnemonic());
	    	
	    	if (hrmSubClass != null) {
	    		hrmDocumentSubClass.setSubClassDescription(hrmSubClass.getSubClassDescription());
	    	}	    	
	    }		
	    
	    if (subClassList != null && subClassList.size() == 0 && reportType.equalsIgnoreCase("Medical Records Report") && reportSubClass != null && !reportSubClass.isEmpty()) {
	    	String[] subClassFromReport = reportSubClass.split("\\^");
	    	String subClass = "";
	    	if (subClassFromReport.length == 2) {
	    		subClass =  subClassFromReport[1];	    		
	    	}
	    	
	    	HRMSubClass hrmSubClass = hrmSubClassDao.findByClassNameSubClassNameFacility(reportType,  sendingFacilityId, subClass);
	    	if (hrmSubClass != null) {
	    		HRMDocumentSubClass hrmDocumentSubClass = new HRMDocumentSubClass();
	    		hrmDocumentSubClass.setSubClassDescription(hrmSubClass.getSubClassDescription());
	    		subClassList.add(hrmDocumentSubClass);
	    	}
	    }
		
	}
	
}
