package org.oscarehr.hospitalReportManager.util;

import lombok.AllArgsConstructor;
import lombok.val;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentToProviderDao;
import org.oscarehr.hospitalReportManager.model.HRMDocumentToProvider;

@AllArgsConstructor
public class HrmDocumentUtil {

  private HRMDocumentToProviderDao hrmDocumentToProviderDao;
  private final String UNCLAIMED_PROVIDER_NUMBER = "-1";

  /**
   * This function queries for a list of active HRM document to provider links.
   * If no links exist, it will create an entry which links the HRM document to a provider number
   * of -1, allowing the document to be displayed in the unclaimed inbox.
   * @param hrmDocumentToProvider Contains the documentId needed to query the database and
   *                              link the unclaimed entry
   */
  public void handleUnclaimedDocument(final HRMDocumentToProvider hrmDocumentToProvider) {
    val hrmDocumentId = hrmDocumentToProvider.getHrmDocumentId();
    if (hrmDocumentToProviderDao
        .findByHrmDocumentIdNoSystemUser(hrmDocumentId)
        .size() < 1) {
      hrmDocumentToProviderDao.persist(new HRMDocumentToProvider(
          UNCLAIMED_PROVIDER_NUMBER,
          hrmDocumentId
      ));
    }
  }

}
