package org.oscarehr.app;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * A FhirSubscriptionFilter implementation that supports processing of FHIR subscriptions by
 * making sure that cookie info is available for use by FHIR subscription logic in classic
 */
public class FhirSubscriptionFilter implements Filter {

	private FilterConfig filterConfig = null;

	@Override
	public void destroy() {
		filterConfig = null;
	}

	public static ThreadLocal<HttpServletRequest> OSCAR_REQUEST_URL = new ThreadLocal<>();
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		if (request instanceof HttpServletRequest) {
		  HttpServletRequest httpServletRequest = (HttpServletRequest)request;			
		  OSCAR_REQUEST_URL.set(httpServletRequest);	
		} 

		chain.doFilter(request, response);
	}

	@Override
	public void init(@SuppressWarnings("hiding") FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}
}
