/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */

package org.oscarehr.PMmodule.dao;

import com.quatro.model.security.SecProvider;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.var;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.oscarehr.common.NativeSql;
import org.oscarehr.common.dao.ProviderFacilityDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.model.*;
import org.oscarehr.managers.ConsultationManager;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import oscar.OscarProperties;

@SuppressWarnings("unchecked")
public class ProviderDao extends HibernateDaoSupport {
	
	public static final String PR_TYPE_DOCTOR = "doctor";
	public static final String PR_TYPE_RESIDENT = "resident";
	
	private static Logger log = MiscUtils.getLogger();

	public boolean providerExists(String providerNo) {
		String sql = "FROM Provider x WHERE x.id IN (?, TRIM(LEADING '0' FROM ?))";
		
		List<Provider> providers = getHibernateTemplate().find(sql, providerNo, providerNo);

		return providers.size() > 0;
	}

	public Provider getProvider(String providerNo) {
		if (providerNo == null || providerNo.length() <= 0) {
			return null;
		}

		Provider provider = getHibernateTemplate().get(Provider.class, providerNo);

		if (log.isDebugEnabled()) {
			log.debug("getProvider: providerNo=" + providerNo + ",found=" + (provider != null));
		}

		return provider;
	}

	public String getProviderName(String providerNo) {	

		String providerName = "";
		Provider provider = getProvider(providerNo);

		if (provider != null) {
			if (provider.getFirstName() != null) {
				providerName = provider.getFirstName() + " ";
			}
	
			if (provider.getLastName() != null) {
				providerName += provider.getLastName();
			}
	
			if (log.isDebugEnabled()) {
				log.debug("getProviderName: providerNo=" + providerNo + ",result=" + providerName);
			}
		}

		return providerName;
	}

	public String getProviderNameLastFirst(String providerNo) {
		if (providerNo == null || providerNo.length() <= 0) {
			throw new IllegalArgumentException();
		}

		String providerName = "";
		Provider provider = getProvider(providerNo);

		if (provider != null) {
			if (provider.getLastName() != null) {
				providerName = provider.getLastName() + ", ";
			}
	
			if (provider.getFirstName() != null) {
				providerName += provider.getFirstName();
			}
	
			if (log.isDebugEnabled()) {
				log.debug("getProviderNameLastFirst: providerNo=" + providerNo + ",result=" + providerName);
			}
		}

		return providerName;
	}

	public List<Provider> getProviders() {
		
		List<Provider> rs = getHibernateTemplate().find(
				"FROM  Provider p ORDER BY p.LastName");

		if (log.isDebugEnabled()) {
			log.debug("getProviders: # of results=" + rs.size());
		}
		return rs;
	}
	
	public List<Provider> getProviders(String[] providers) {
		Session session = getSession();
		String sql = "FROM Provider p WHERE p.ProviderNo IN (:providerNo)";
		Query q = session.createQuery(sql);
		q.setParameterList("providerNo", providers);
		return q.list();
	}


    public List<Provider> getProviderFromFirstLastName(String firstname,String lastname){
            firstname=firstname.trim();
            lastname=lastname.trim();
            String s="From Provider p where p.FirstName=? and p.LastName=?";
            ArrayList<Object> paramList=new ArrayList<Object>();
            paramList.add(firstname);
            paramList.add(lastname);
            Object params[]=paramList.toArray(new Object[paramList.size()]);
            return getHibernateTemplate().find(s,params);
    }

    public List<Provider> getProviderLikeFirstLastName(String firstname,String lastname){
    	firstname=firstname.trim();
    	lastname=lastname.trim();
    	String s="From Provider p where p.FirstName like ? and p.LastName like ?";
    	ArrayList<Object> paramList=new ArrayList<Object>();
    	paramList.add(firstname);
    	paramList.add(lastname);
    	Object params[]=paramList.toArray(new Object[paramList.size()]);
    	return getHibernateTemplate().find(s,params);
	}

    public List<Provider> getActiveProviderLikeFirstLastName(String firstname,String lastname){
    	firstname=firstname.trim();
    	lastname=lastname.trim();
    	String s="From Provider p where p.FirstName like ? and p.LastName like ? and p.Status='1' order by p.LastName";
    	ArrayList<Object> paramList=new ArrayList<Object>();
    	paramList.add(firstname);
    	paramList.add(lastname);
    	Object params[]=paramList.toArray(new Object[paramList.size()]);
    	return getHibernateTemplate().find(s,params);
	}

	public List<Provider> findByNamesLike(List<String> names) {
		if (names == null || names.size() == 0) { return new ArrayList<>(); }

		StringBuilder builder = new StringBuilder();
		builder.append("SELECT x FROM Provider x where x.Status='1' AND (");

		Map<String, String> params = new HashMap<>();
		for (int i = 0; i < names.size(); i ++) {
			String value = "%" + names.get(i) + "%";
			params.put("name" + i, value);
			if (i > 0) {
				builder.append(" OR ");
			}
			builder.append("x.FirstName like :name");
			builder.append(i);
			builder.append(" OR x.LastName like :name");
			builder.append(i);
		}
		builder.append(")");

		Query query = getSession().createQuery(builder.toString());

		for(Map.Entry<String, String> entry : params.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}

		return query.list();
	}

	
    public List<Provider> getActiveProvidersByGroupNo(String groupno){
    	groupno=groupno.trim();
    	String s="From Provider p where p.Comments like ? and p.Status='1'";
    	ArrayList<Object> paramList=new ArrayList<Object>();
    	paramList.add("%<xml_p_billinggroup_no>"+groupno+"</xml_p_billinggroup_no>%");
    	Object params[]=paramList.toArray(new Object[paramList.size()]);
    	return getHibernateTemplate().find(s,params);
	}

    public List<SecProvider> getActiveProviders(Integer programId) {
        ArrayList<Object> paramList = new ArrayList<Object>();

    	String sSQL="FROM  SecProvider p where p.status='1' and p.providerNo in " +
    	"(select sr.providerNo from secUserRole sr, LstOrgcd o " +
    	" where o.code = 'P' || ? " +
    	" and o.codecsv  like '%' || sr.orgcd || ',%' " +
    	" and not (sr.orgcd like 'R%' or sr.orgcd like 'O%'))" +
    	" ORDER BY p.lastName";

    	paramList.add(programId);
    	Object params[] = paramList.toArray(new Object[paramList.size()]);

    	return  getHibernateTemplate().find(sSQL ,params);
	}

	public List<Provider> getActiveProviders(String facilityId, String programId) {
		ArrayList<Object> paramList = new ArrayList<Object>();

		String sSQL;
		List<Provider> rs;
		if (programId != null && "0".equals(programId) == false) {
			sSQL = "FROM  Provider p where p.Status='1' and p.ProviderNo in "
					+ "(select c.ProviderNo from ProgramProvider c where c.ProgramId =?) ORDER BY p.LastName";
			paramList.add(Long.valueOf(programId));
			Object params[] = paramList.toArray(new Object[paramList.size()]);
			rs = getHibernateTemplate().find(sSQL, params);
		} else if (facilityId != null && "0".equals(facilityId) == false) {
			sSQL = "FROM  Provider p where p.Status='1' and p.ProviderNo in "
					+ "(select c.ProviderNo from ProgramProvider c where c.ProgramId in "
					+ "(select a.id from Program a where a.facilityId=?)) ORDER BY p.LastName";
			// JS 2192700 - string facilityId seems to be throwing class cast
			// exception
			Integer intFacilityId = Integer.valueOf(facilityId);
			paramList.add(intFacilityId);
			Object params[] = paramList.toArray(new Object[paramList.size()]);
			rs = getHibernateTemplate().find(sSQL, params);
		} else {
			sSQL = "FROM  Provider p where p.Status='1' ORDER BY p.LastName";
			rs = getHibernateTemplate().find(sSQL);
		}
		// List<Provider> rs =
		// getHibernateTemplate().find("FROM  Provider p ORDER BY p.LastName");

		return rs;
	}

	public List<Provider> getActiveProviders() {
		
		List<Provider> rs = getHibernateTemplate().find(
				"FROM  Provider p where p.Status='1' ORDER BY p.LastName");

		if (log.isDebugEnabled()) {
			log.debug("getProviders: # of results=" + rs.size());
		}
		return rs;
	}

	public List<Provider> getActiveProvidersByRole(String role) {
		
		List<Provider> rs = getHibernateTemplate().find(
			"select p FROM Provider p, SecUserRole s where p.ProviderNo = s.ProviderNo and p.Status='1' and s.RoleName = ? order by p.LastName, p.FirstName", role);

		if (log.isDebugEnabled()) {
			log.debug("getActiveProvidersByRole: # of results=" + rs.size());
		}
		return rs;
	}
	
	public List<Provider> getActiveProvidersWithSchedule() {
		return getHibernateTemplate().find("FROM  Provider p WHERE p.Status='1' AND p.HasSchedule = TRUE ORDER BY p.LastName");
	}

    public List<Provider> getActiveProvidersThatReceivesTicklers() {
        return getHibernateTemplate().find("FROM  Provider p WHERE p.Status='1' AND p.ReceivesTicklers = TRUE ORDER BY p.LastName");
    }

	public List<Provider> getDoctorsWithOhip(){
		return getHibernateTemplate().find(
				"FROM Provider p " + 
					"WHERE p.ProviderType = 'doctor' " +
					"AND p.Status = '1' " +
					"AND p.OhipNo != ''" + 
					"AND p.OhipNo IS NOT NULL " +
				   	"ORDER BY p.LastName, p.FirstName");
	}

	public List<Provider> getDoctorsWithPractionerNo(){
		return getHibernateTemplate().find(
				"FROM Provider p " +
						"WHERE p.ProviderType = 'doctor' " +
						"AND p.Status = '1' " +
						"AND p.practitionerNo != '' " +
						"AND p.practitionerNo IS NOT NULL " +
						"ORDER BY p.LastName, p.FirstName");
	}

  public List<Provider> getReferringPractitioner(final boolean withPractitionerNo) {
    var query = "FROM Provider p WHERE ";
    query += getReferringPractitionerAppendConditionString();
    query += "AND p.Status = '1' ";
    if (withPractitionerNo) {
      query += "AND p.practitionerNo != '' AND p.practitionerNo IS NOT NULL ";
    }
    query += "ORDER BY p.LastName, p.FirstName";
    return getHibernateTemplate().find(query);
  }

	private String getReferringPractitionerAppendConditionString() {
		var appendCondition = "(";
		for (var type : ConsultationManager.ConsultationReferringPractitioner.values()) {
			if (shouldAppendPractitioner(type.getConsultationManagerReferringString())) {
				appendCondition += "p.ProviderType = '";
				appendCondition += type.getQueryValue();
				appendCondition += "' OR ";
			}
		}
		appendCondition += ")";
		
		if (appendCondition.equals("()")) {
			appendCondition = "p.ProviderType = 'doctor' ";
		} else {
			appendCondition = appendCondition.replaceAll("OR \\)", ")");
		}
		return appendCondition;
	}

	private boolean shouldAppendPractitioner(final String type) {
		SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
		return isReferringPractitionerValueTrue(systemPreferencesDao.findPreferenceByName(type));
	}
	
	public static boolean isReferringPractitionerValueTrue(final SystemPreferences referringPractitioner) {
		return referringPractitioner != null && referringPractitioner.getValue() != null && referringPractitioner.getValue().equalsIgnoreCase("true");
	}

	public List<Provider> getBillableProviders() {
		List<Provider> rs = getHibernateTemplate().find("FROM Provider p where p.OhipNo != '' and p.ThirdPartyOnly = false and p.Status = '1' order by p.LastName");
		return rs;
	}

	public List<Provider> getAllBillableProviders() {
		List<Provider> rs = getHibernateTemplate().find("FROM Provider p where p.OhipNo != '' and p.Status = '1' order by p.LastName");
		return rs;
	}

	public List<Provider> getInactiveBillableProviders() {
		return (List<Provider>) getHibernateTemplate()
				.find("FROM Provider p where p.OhipNo != '' and p.ThirdPartyOnly = false and p.Status = '0' order by p.LastName");
	}
	public List<Provider> getAllInactiveBillableProviders() {
		return (List<Provider>) getHibernateTemplate()
				.find("FROM Provider p where p.OhipNo != '' and p.Status = '0' order by p.LastName");
	}

	public List<Provider> getAllBillableProvidersWithNoSite() {
		List<Provider> rs = getHibernateTemplate().find("select p FROM Provider p " +
				" where not exists (select distinct ps.id.providerNo from ProviderSite ps where ps.id.providerNo = p.ProviderNo ) " +
				" and p.OhipNo != '' " +
				" and p.Status = '1' " +
				" order by p.LastName");
		return rs;
	}
	
	@SuppressWarnings("unchecked")
    public List<Provider> getBillableProvidersInBC() {
		List<Provider> rs = getHibernateTemplate().find("FROM Provider p where (p.OhipNo <> '' or p.RmaNo <> ''  or p.BillingNo <> '' or p.HsoNo <> '')and p.Status = '1' order by p.LastName");
		return rs;
	}
	
	public List<Provider> getBillableProvidersInGroupNo(String groupNo) {
		String sql = "FROM Provider p WHERE p.OhipNo != '' AND p.ThirdPartyOnly = FALSE AND p.Status = '1' AND p.Comments LIKE ? ORDER BY p.LastName";
		ArrayList<Object> paramList = new ArrayList<Object>();
		paramList.add("%<xml_p_billinggroup_no>"+groupNo+"</xml_p_billinggroup_no>%");
		return getHibernateTemplate().find(sql, paramList.toArray());
	}

	public List<Provider> getProviders(boolean active) {
		
		List<Provider> rs = getHibernateTemplate().find(
				"FROM  Provider p where p.Status='" + (active?1:0) + "' order by p.LastName" );
		return rs;
	}

    public List<Provider> getActiveProviders(String providerNo, Integer shelterId) {
    	//
    	String sql;
    	if (shelterId == null || shelterId.intValue() == 0)
    		sql = "FROM  Provider p where p.Status='1'" +
    				" and p.ProviderNo in (select sr.providerNo from Secuserrole sr " +
    				" where sr.orgcd in (select o.code from LstOrgcd o, Secuserrole srb " +
    				" where o.codecsv  like '%' || srb.orgcd || ',%' and srb.providerNo =?))" +
    				" ORDER BY p.LastName";
    	else
    		sql = "FROM  Provider p where p.Status='1'" +
			" and p.ProviderNo in (select sr.providerNo from Secuserrole sr " +
			" where sr.orgcd in (select o.code from LstOrgcd o, Secuserrole srb " +
			" where o.codecsv like '%S" + shelterId.toString()+ ",%' and o.codecsv like '%' || srb.orgcd || ',%' and srb.providerNo =?))" +
			" ORDER BY p.LastName";

    	ArrayList<Object> paramList = new ArrayList<Object>();
    	paramList.add(providerNo);

    	Object params[] = paramList.toArray(new Object[paramList.size()]);

    	List<Provider> rs = getHibernateTemplate().find(sql,params);

		if (log.isDebugEnabled()) {
			log.debug("getProviders: # of results=" + rs.size());
		}
		return rs;
	}

    
	public List<Provider> search(String name) {
		boolean isOracle = OscarProperties.getInstance().getDbType().equals(
				"oracle");
		Session session = getSession();
		
		Criteria c = session.createCriteria(Provider.class);
		if (isOracle) {
			c.add(Restrictions.or(Expression.ilike("FirstName", name + "%"),
					Expression.ilike("LastName", name + "%")));
		} else {
			c.add(Restrictions.or(Expression.like("FirstName", name + "%"),
					Expression.like("LastName", name + "%")));
		}
		c.addOrder(Order.asc("ProviderNo"));

		List<Provider> results = new ArrayList<Provider>();
		
		try {
			results = c.list();
		}finally {
			this.releaseSession(session);
		}

		if (log.isDebugEnabled()) {
			log.debug("search: # of results=" + results.size());
		}
		return results;
	}

	public List<Provider> getProvidersByTypeWithNonEmptyOhipNo(String type) {
		List<Provider> results = this.getHibernateTemplate().find(
				"from Provider p where p.ProviderType = ? and p.OhipNo <> ''", type);
		return results;
	}

    public List<Provider> getActiveProvidersByType(String type) {
        List<Provider> results = this.getHibernateTemplate().find(
                "from Provider p where p.ProviderType = ? AND p.Status = '1' order by last_name", type);
        return results;
    }

	public List<Provider> getActiveProvidersByTypes(List<String> types) {
		Query query = getSession().createQuery("SELECT p FROM Provider p WHERE p.ProviderType IN (:providerTypes) AND p.Status = '1' ORDER BY last_name");
		query.setParameterList("providerTypes", types);
		List<Provider> results = query.list();

		return results;
	}
    
	public List<Provider> getProvidersByType(String type) {
		
		List<Provider> results = this.getHibernateTemplate().find(
				"from Provider p where p.ProviderType = ? order by last_name", type);

		if (log.isDebugEnabled()) {
			log.debug("getProvidersByType: type=" + type + ",# of results="
					+ results.size());
		}

		return results;
	}

	public List<Provider> getProvidersByTypes(List<String> types) {
		Query query = getSession().createQuery("SELECT p FROM Provider p WHERE p.ProviderType IN (:providerTypes) ORDER BY last_name");
		query.setParameterList("providerTypes", types);

		List<Provider> results = query.list();

		if (log.isDebugEnabled()) {
			log.debug("getProvidersByTypes: types=" + types.toString() + ",# of results="
					+ results.size());
		}

		return results;
	}
	
	public List<Provider> getProvidersByTypePattern(String typePattern) {
		
		List<Provider> results = this.getHibernateTemplate().find(
				"from Provider p where p.ProviderType like ?", typePattern);
		return results;
	}

	public List getShelterIds(String provider_no)
	{
		
		String sql ="select distinct c.id as shelter_id from lst_shelter c, lst_orgcd a, secUserRole b  where instr('RO',substr(b.orgcd,1,1)) = 0 and a.codecsv like '%' || b.orgcd || ',%'" +
				" and b.provider_no=? and a.codecsv like '%S' || c.id  || ',%'";
		Session session = getSession();
		
		Query query = session.createSQLQuery(sql);
    	((SQLQuery) query).addScalar("shelter_id", Hibernate.INTEGER);
    	query.setString(0, provider_no);
    	List lst = new ArrayList();
    	try {
    		lst=query.list();
    	}finally {
    		this.releaseSession(session);
    	}
        return lst;

	}

	public static void addProviderToFacility(String provider_no, int facilityId) {
		try {
			ProviderFacility pf = new ProviderFacility();
			pf.setId(new ProviderFacilityPK());
			pf.getId().setProviderNo(provider_no);
			pf.getId().setFacilityId(facilityId);
			ProviderFacilityDao pfDao = SpringUtils.getBean(ProviderFacilityDao.class);
			pfDao.persist(pf);
		} catch (RuntimeException e) {
			// chances are it's a duplicate unique entry exception so it's safe
			// to ignore.
			// this is still unexpected because duplicate calls shouldn't be
			// made
			log.warn("Unexpected exception occurred.", e);
		}
	}

	public static void removeProviderFromFacility(String provider_no,
			int facilityId) {
		ProviderFacilityDao dao = SpringUtils.getBean(ProviderFacilityDao.class);
		for(ProviderFacility p:dao.findByProviderNoAndFacilityId(provider_no,facilityId)) {
			dao.remove(p.getId());
		}
	}

	
	public List<Integer> getFacilityIds(String provider_no) {
		Session session = getSession();
		try {
			SQLQuery query = session.createSQLQuery("select facility_id from provider_facility,Facility where Facility.id=provider_facility.facility_id and Facility.disabled=0 and provider_no=\'"+provider_no +"\'");
			List<Integer> results = query.list();
			return results;
		}finally {
			this.releaseSession(session);
		}
	}

	
	public List<String> getProviderIds(int facilityId) {
		Session session = getSession();
		try {
			SQLQuery query = session.createSQLQuery("select provider_no from provider_facility where facility_id="+facilityId);
			List<String> results = query.list();
			return results;
		}finally {
			this.releaseSession(session);
		}
	
	}

    public void updateProvider( Provider provider) {
        this.getHibernateTemplate().update(provider);
    }

    public void saveProvider( Provider provider) {
        this.getHibernateTemplate().save(provider);
    }

	public Provider getProviderByPractitionerNo(String practitionerNo) {
		if (practitionerNo == null || practitionerNo.length() <= 0) {
			throw new IllegalArgumentException();
		}

		List<Provider> providerList = getHibernateTemplate().find("From Provider p where p.practitionerNo=?",new Object[]{practitionerNo});

		if(providerList.size()>1) {
			logger.warn("Found more than 1 provider with practitionerNo="+practitionerNo);
		}
		if(providerList.size()>0)
			return providerList.get(0);

		return null;
	}
	
	public List<Provider> getProvidersByPractitionerNo(String practitionerNo) {
		if (practitionerNo == null || practitionerNo.length() <= 0) {
			throw new IllegalArgumentException();
		}

		List<Provider> providerList = getHibernateTemplate().find("From Provider p where p.practitionerNo=?",new Object[]{practitionerNo});

		if(providerList.size()>1) {
			logger.warn("Found more than 1 provider with practitionerNo="+practitionerNo);
		}
		return providerList;
	}

	public List<Provider> getProvidersByHsoNo(String hsoNo) {
		if (hsoNo == null || hsoNo.length() <= 0) {
			throw new IllegalArgumentException();
		}

		List<Provider> providerList = getHibernateTemplate().find("From Provider p where p.HsoNo LIKE ?", new Object[]{hsoNo});

		if(providerList.size()>1) {
			logger.warn("Found more than 1 provider with HSO Number=" + hsoNo);
		}
		return providerList;
	}
	
	public List<String> getUniqueTeams() {
		
		List<String> providerList = getHibernateTemplate().find("select distinct p.Team From Provider p");

		return providerList;
	}
	public List<Provider> getProvidersBySiteLocation(String location) {
		List<Provider> pList = new ArrayList<Provider>();
		Session sess = getSession();
		try {
			SQLQuery  q = sess.createSQLQuery(
					"select distinct p.provider_no	" +
					" from provider p " +
					" inner join providersite ps on ps.provider_no = p.provider_no " +
					" inner join site s on s.site_id = ps.site_id " +
					" where  s.site_id = :siteId order by p.last_name, p.first_name") ;
			q.setParameter("siteId", location);
			List providerNos = q.list();
			for(Object no : providerNos) {
				String provNo = (String)no;
				Provider provider = getProvider(provNo);
				pList.add(provider);				
			}
		} catch (Exception e) {
			MiscUtils.getLogger().error("Error", e);
		} finally {
			try {
				sess.close();
			} catch (HibernateException e) {
				MiscUtils.getLogger().error("Error", e);
			}
		}		
		return pList;
	}
	public List<Provider> getProvidersBySiteLocation2(String location) {
		List<Provider> pList = new ArrayList<Provider>();
		Session sess = getSession();
		try {
			SQLQuery  q = sess.createSQLQuery(
					"select distinct p.provider_no	" +
					" from provider p " +
					" inner join providersite ps on ps.provider_no = p.provider_no " +
					" inner join site s on s.site_id = ps.site_id " +
					" where  s.site_id = :siteId and p.ohip_no!='' order by p.last_name, p.first_name") ;
			q.setParameter("siteId", Integer.valueOf(location));
			List providerNos = q.list();
			for(Object no : providerNos) {
				String provNo = (String)no;
				Provider provider = getProvider(provNo);
				pList.add(provider);				
			}
		} catch (Exception e) {
			MiscUtils.getLogger().error("Error", e);
		} finally {
			try {
				sess.close();
			} catch (HibernateException e) {
				MiscUtils.getLogger().error("Error", e);
			}
		}		
		return pList;
	}

  public List<Provider> getBillableProvidersOnTeam(Provider p) {
    return getHibernateTemplate().find("FROM Provider p "
            + "WHERE p.Status = '1' AND p.OhipNo != '' AND p.Team = ? ORDER BY p.LastName,"
            + " p.FirstName", p.getTeam()
    );
  }
        
        public List<Provider> getBillableProvidersByOHIPNo(String ohipNo) {                        
            if (ohipNo == null || ohipNo.length() <= 0) {
		throw new IllegalArgumentException();
            }

            
            List<Provider> providers = this.getHibernateTemplate().find("from Provider p where ohip_no like ? order by last_name, first_name", ohipNo);            
            
            if(providers.size()>1) {
                logger.warn("Found more than 1 provider with ohipNo="+ohipNo);
            }
			if (providers.isEmpty()) {
				return null;
			}
			else {
				return providers;
			}
        }
        
        /**
         * Gets all providers with non-empty OHIP number ordered by last,then first name
         * 
         * @return
         * 		Returns the all found providers 
         */
        
        public List<Provider> getProvidersWithNonEmptyOhip() {
        	return getHibernateTemplate().find("FROM Provider WHERE ohip_no != '' order by last_name, first_name");
        }
        
        public List<Provider> getCurrentTeamProviders(String providerNo) {
        	String hql = "SELECT p FROM Provider p "
    				+ "WHERE p.Status='1' and p.OhipNo != '' " 
        			+  "AND (p.ProviderNo='"+providerNo+"' or team=(SELECT p2.Team FROM Provider p2 where p2.ProviderNo='"+providerNo+"')) "
        			+ "ORDER BY p.LastName, p.FirstName";
    		
        	return this.getHibernateTemplate().find(hql);
        }

		public List<String> getActiveTeams() {	        
			List<String> providerList = getHibernateTemplate().find("select distinct p.Team From Provider p where p.Status = '1' and p.Team != '' order by p.Team");
			return providerList;
        }

	public List<Provider> getCurrentInactiveTeamProviders(final String providerNumber) {
		return (List<Provider>) getHibernateTemplate()
				.find("From Provider p " +
								"WHERE p.Status = '0' and p.OhipNo != '' " +
								"AND (p.ProviderNo = ? " +
								"   or team = (SELECT p2.Team FROM Provider p2 where p2.ProviderNo = ?)) " +
								"ORDER BY p.LastName, p.FirstName",
						new Object[]{providerNumber, providerNumber}
        );
	}

		@NativeSql({"provider", "providersite"})
		
        public List<String> getActiveTeamsViaSites(String providerNo) {
			Session session = getSession();
			try {
				// providersite is not mapped in hibernate - this can be rewritten w.o. subselect with a cross product IHMO 
				SQLQuery query = session.createSQLQuery("select distinct team from provider p inner join providersite s on s.provider_no = p.provider_no " +
	            		" where s.site_id in (select site_id from providersite where provider_no = '" + providerNo + "') order by team ");
				return query.list();
			}finally {
				this.releaseSession(session);
			}
        }

  @NativeSql({"OneIdUa"})
  public boolean hasOneIdUao(String providerNo) {
    Session session = getSession();
    try {
      SQLQuery query =
          session.createSQLQuery(
              "select count(1) from OneIdUaoAssignment where providerNo = '" + providerNo + "' ");
      return !((BigInteger) query.uniqueResult()).equals(BigInteger.ZERO);
    } finally {
      this.releaseSession(session);
    }
  }

        public List<Provider> getProviderByPatientId(Integer patientId) {
	        String hql = "SELECT p FROM Provider p, Demographic d "
	    				+ "WHERE d.ProviderNo = p.ProviderNo " 
	        			+ "AND d.DemographicNo = ?";
        	return this.getHibernateTemplate().find(hql, patientId);
        }
		
		public List<Provider> getDoctorsWithNonEmptyCredentials() {
			String sql = "FROM Provider p WHERE p.ProviderType = 'doctor' " +
					"AND p.Status='1' " +
					"AND p.OhipNo IS NOT NULL " +
					"AND p.OhipNo != '' " +
					"ORDER BY p.LastName, p.FirstName";
			return getHibernateTemplate().find(sql);
		}
		
		public List<Provider> getProvidersWithNonEmptyCredentials() {
			String sql = "FROM Provider p WHERE p.Status='1' " +
					"AND p.OhipNo IS NOT NULL " +
					"AND p.OhipNo != '' " +
					"ORDER BY p.LastName, p.FirstName";
			return getHibernateTemplate().find(sql);
		}

		public List<String> getProvidersInTeam(String teamName) {
			List<String> providerList = getHibernateTemplate().find("select distinct p.ProviderNo from Provider p  where p.Team = ?",new Object[]{teamName});			
			return providerList;
		}

		public List<Object[]> getDistinctProviders() {
			List<Object[]> providerList = getHibernateTemplate().find("select distinct p.ProviderNo, p.ProviderType from Provider p ORDER BY p.LastName");
			return providerList;
        }
		
		public List<String> getRecordsAddedAndUpdatedSinceTime(Date date) {
			@SuppressWarnings("unchecked")
			List<String> providers = getHibernateTemplate().find("select distinct p.ProviderNo From Provider p where p.lastUpdateDate > ? ",date);
			
			return providers;
		}
		
		@SuppressWarnings("unchecked")
		public List<Provider> searchProviderByNamesString(String searchString, int startIndex, int itemsToReturn) {
			String sqlCommand = "select x from Provider x";
			if (searchString != null)  {
				if(searchString.indexOf(",") != -1 &&  searchString.split(",").length>1 && searchString.split(",")[1].length()>0) {
					sqlCommand = sqlCommand + " where x.LastName like :ln AND x.FirstName like :fn";
				} else {
					sqlCommand = sqlCommand + " where x.LastName like :ln";
				}
				
			}

			Session session = this.getSession();
			try {
				Query q = session.createQuery(sqlCommand);
				if (searchString != null) {
					q.setParameter("ln", "%" + searchString.split(",")[0] + "%");
					if(searchString.indexOf(",") != -1 && searchString.split(",").length>1 && searchString.split(",")[1].length()>0) {
						q.setParameter("fn", "%" +  searchString.split(",")[1] + "%");
						
					} 
				}
				q.setFirstResult(startIndex);
				q.setMaxResults(itemsToReturn);
				return (q.list());
			} finally {
				this.releaseSession(session);
			}
		}
		
		@SuppressWarnings("unchecked")
		public List<Provider> search(String term, boolean active, int startIndex, int itemsToReturn) {
			String sqlCommand = "select x from Provider x WHERE x.Status = :status ";
			
			
			if(term != null && term.length()>0) {
				sqlCommand += "AND (x.LastName like :term  OR x.FirstName like :term) ";
			}
			
			sqlCommand += " ORDER BY x.LastName,x.FirstName";

			

			Session session = this.getSession();
			try {
				Query q = session.createQuery(sqlCommand);
				
				q.setString("status", active?"1":"0");
				if(term != null && term.length()>0) {
					q.setString("term", term + "%");
				}
				
				q.setFirstResult(startIndex);
				q.setMaxResults(itemsToReturn);
				return (q.list());
			} finally {
				this.releaseSession(session);
			}
		}

    @NativeSql({"provider", "appointment"})
    public List<String> getProviderNosWithAppointmentsOnDate(Date appointmentDate) {
        Session session = getSession();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String sql = "SELECT p.provider_no FROM provider p WHERE p.provider_no IN (SELECT DISTINCT a.provider_no FROM appointment a WHERE a.appointment_date = '" + sdf.format(appointmentDate) + "') " +
                    "AND p.Status='1'";
            SQLQuery query = session.createSQLQuery(sql);
            
            return query.list();
        }finally {
            this.releaseSession(session);
        }
    }

    public List<Provider> getOlisHicProviders() {
        UserPropertyDAO userPropertyDAO = SpringUtils.getBean(UserPropertyDAO.class);
        Session session = getSession();
        String sql = "FROM Provider p WHERE p.practitionerNo IS NOT NULL AND p.practitionerNo != ''";
        Query query = session.createQuery(sql);
        List<Provider> practitionerNoProviders = query.list();
        
        List<Provider> results = new ArrayList<Provider>();
        for (Provider practitionerNoProvider : practitionerNoProviders) {
            String olisType = userPropertyDAO.getStringValue(practitionerNoProvider.getProviderNo(), UserProperty.OFFICIAL_OLIS_IDTYPE);
            if (olisType != null && !olisType.isEmpty()) {
                results.add(practitionerNoProvider);
            }
        }
        return results;
    }

    public Provider getProviderByPractitionerNoAndOlisType(String practitionerNo, String olisIdentifierType) {
        UserPropertyDAO userPropertyDAO = SpringUtils.getBean(UserPropertyDAO.class);
        String sql = "FROM Provider p WHERE p.practitionerNo=?";
        
        List<Provider> providers = getHibernateTemplate().find(sql, practitionerNo);

        if (!providers.isEmpty()) {
            Provider provider = providers.get(0);
            String olisType = userPropertyDAO.getStringValue(provider.getProviderNo(), UserProperty.OFFICIAL_OLIS_IDTYPE);
            if (olisIdentifierType.equals(olisType)) {
                return providers.get(0);
            }
        }
        return null;
    }
    
    public List<Provider> getOlisProvidersByPractitionerNo(List<String> practitionerNumbers) {
		Session session = getSession();
        String sql = "FROM Provider p WHERE p.practitionerNo IN (:practitionerNumbers)";
		Query query = session.createQuery(sql);
		query.setParameterList("practitionerNumbers", practitionerNumbers);
        List<Provider> providers = query.list();
        
        return providers;
	}

	/**
	 * Gets a list of provider numbers based on the provided list of provider numbers
	 * @param providerNumbers The list of provider numbers to get the related objects for
	 * @return A list of providers
	 */
	public List<Provider> getProvidersByIds(List<String> providerNumbers) {
		Session session = getSession();
		String sql = "FROM Provider p WHERE p.ProviderNo IN (:providerNumbers)";
		Query query = session.createQuery(sql);
		query.setParameterList("providerNumbers", providerNumbers);
		
		List<Provider> providers = query.list();
		return providers;
	}

	/**
	 * Gets a map of provider names with the provider number as the map key based on the provided list of provider numbers
	 * @param providerNumbers A list of provider numbers to get the name map for 
	 * @return A map of provider names with their related provider number as the key
	 */
	public Map<String, String> getProviderNamesByIdsAsMap(List<String> providerNumbers) {
        Map<String, String> providerNameMap = new HashMap<>();
        List<Provider> providers = getProvidersByIds(providerNumbers);
        
        for (Provider provider : providers) {
        	providerNameMap.put(provider.getProviderNo(), provider.getFullName());
		}
        
        return providerNameMap;
	}

	/**
	 * Sorted by lastname,firstname
	 */
	public List<Provider> findAllExcellerisProviders() {
		List<Provider> rs = getHibernateTemplate().find(
				"FROM  Provider p WHERE p.excellerisId IS NOT NULL and p.excellerisId <> '' and p.lifelabsId IS NOT NULL and p.lifelabsId <> '' ORDER BY p.LastName");
		return rs;


	}

	public List<Provider> findExcellerisProvidersByFullName(String lastName, String firstName) {

		firstName = firstName.trim() + "%";
		lastName = lastName.trim() + "%";
		String s = "FROM  Provider p WHERE p.FirstName like ? and p.LastName like ? and p.excellerisId IS NOT NULL  and p.excellerisId <> '' and p.lifelabsId IS NOT NULL and p.lifelabsId <> '' ORDER BY p.LastName";
		ArrayList<Object> paramList = new ArrayList<Object>();
		paramList.add(firstName);
		paramList.add(lastName);
		Object params[] = paramList.toArray(new Object[paramList.size()]);

		List<Provider> providers = getHibernateTemplate().find(s, params);
		if (providers != null && providers.size() > 0) {
			return providers;
		}

		return null;
	}

	public List<Provider> findExcellerisProvidersByLastName(String lastName) {
		return findExcellerisProvidersByFullName(lastName, "");
	}

	public String suggestProviderNo() {
		List<Provider> providers = getProviders();
		List<Integer> providerNumbers = new ArrayList<Integer>();
		for (Provider p : providers) {
			try {
				providerNumbers.add(Integer.valueOf(p.getProviderNo()));
			}
			catch(NumberFormatException nfe) { /* no-op */ }
		}
		for (Integer i = 1; i < 1000000; i++) {
			if (!providerNumbers.contains(i)) {
				return i.toString();
			}
		}
		throw new RuntimeException("No provider numbers available");
	}

  public List<Provider> findDynacareProvidersByFullName(String lastName, String firstName) {
    firstName = firstName.trim() + "%";
    lastName = lastName.trim() + "%";
    return getActiveProviderLikeFirstLastName(firstName, lastName);
  }

  public List<Provider> findDynacareProvidersByLastName(String lastName) {
    return findDynacareProvidersByFullName(lastName, "");
  }
}
