/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.dynacare.eorder;

import com.dynacare.api.Order;
import java.util.List;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.eOrder.common.ServiceException;

public interface DynacareOrderService {

  /**
   * Submit a diagnostic order in Excelleris eOrder system
   *
   * @param eformData eform data
   * @return newly created diagnostic order
   */
  Order createOrder(EFormData eformData, List<EFormValue> eformValues) throws ServiceException;

  Order updateOrder(EFormData eformData, List<EFormValue> eformValues) throws ServiceException;

  Order getASingleOrder(String orderId);
}
