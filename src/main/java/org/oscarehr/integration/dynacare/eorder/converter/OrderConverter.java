/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.dynacare.eorder.converter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.oscarehr.common.dao.DynacareEorderLabTestCodeDao;
import org.oscarehr.common.model.DynacareEorderLabTestCode;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.dynacare.eorder.DynacareOrderPreferences;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.converter.ConverterHelper;
import org.oscarehr.util.MiscUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.dynacare.api.Comment;
import com.dynacare.api.OrderStatusEnum;
import com.dynacare.api.converter.CustomNullStringSerializerProvider;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.val;
import lombok.var;
import oscar.util.StringUtils;

public class OrderConverter {

  public static final String OHIP = "OHIP";
  private static final Logger LOG = MiscUtils.getLogger();

  private static final String LOINC = "LOINC";
  private static final String LAB_TEST_PREFIX = "T_";
  private static final String TEST_NAME_TO_DYNACARE_TEST_CODE_MAPPING_FILE =
      "/DynacareTestCodeMapping.properties";

  private static final String NEONATAL_BILIRUBIN_TEST = "T_neonatal_bilirubin";
  private static final String NEONATAL_BILIRUBIN_CHILD_AGE_DAYS = "EO_child_age_days";
  private static final String NEONATAL_BILIRUBIN_CHILD_AGE_HOURS = "EO_child_age_hours";
  private static final String NEONATAL_BILIRUBIN_PRACTITIONER_PHONE = "EO_practitioner_phone";
  private static final String NEONATAL_BILIRUBIN_PATIENT_PHONE = "EO_patient_phone";
  private static final String ANSWER_UNKNOWN = "UNK";

  private static final String TDM_TIME_COLLECTED_1 = "EO_time_collected_1";
  private static final String TDM_TIME_OF_LAST_DOSE_1 = "EO_time_of_last_dose_1";
  private static final String TDM_TIME_OF_NEXT_DOSE_1 = "EO_time_of_next_dose_1";
  private static final String TDM_TIME_COLLECTED_2 = "EO_time_collected_2";
  private static final String TDM_TIME_OF_LAST_DOSE_2 = "EO_time_of_last_dose_2";
  private static final String TDM_TIME_OF_NEXT_DOSE_2 = "EO_time_of_next_dose_2";

  private static final String THERAPEUTIC_DRUG_MONITORING = "T_therapeutic_drug_monitor";

  private static final String THERAPEUTIC_DRUG_MONITORING_1 = "T_therapeutic_drug_mon_src_1";
  private static final String THERAPEUTIC_DRUG_MONITORING_2 = "T_therapeutic_drug_mon_src_2";

  private static final String VAGINAL_RECTAL_GROUP_B_STREP = "T_vaginal_rectal_group_b";
  private static final String VAGINAL_RECTAL_GROUP_B_STREP_SRC = "T_vaginal_rectal_group_b_src";

  private static final String CHLAMYDIA = "T_chlamydia";
  private static final String CHLAMYDIA_SRC = "T_chlamydia_src";

  private static final String GC = "T_gc";
  private static final String GC_SRC = "T_gc_src";

  private static final String WOUND = "T_wound";
  private static final String WOUND_SRC = "T_wound_src";

  private static final String OTHER_SWABS_PUS = "T_other_swabs_pus";
  private static final String OTHER_SWABS_PUS_SRC = "T_other_swabs_pus_src";

  private static final String OTHER_TESTS = "T_other_tests_";

  private static final String PATIENT_PREGNANT_COMMENT = "The patient is pregnant";

  private static final String SPECIMEN_COLLECTION_DATE_TIME_COMMENT = "Specimen collection time";

  private static final String PAYMENT_TYPE_COMMENT = "Payment Type";
  public static final String EO_PREGNANT = "EO_pregnant";
  public static final String EO_SPECIMEN_COL_DATE = "EO_specimen_col_date";
  public static final String EO_SPECIMEN_COL_TIME = "EO_specimen_col_time";
  public static final String EO_TIMEZONE_OFFSET = "EO_timezone_offset";
  public static final String EO_ADDITIONAL_CLINIC_INFO = "EO_Additional_Clinic_Info";
  public static final String EO_OHIP = "EO_OHIP";
  public static final String EO_WSIB = "EO_WSIB";
  public static final String WSIB = "WSIB";
  public static final String PATIENT = "Patient";
  public static final String T_TOTAL_PSA = "T_total_psa";
  public static final String T_FREE_PSA = "T_free_psa";
  public static final String T_PSA_INSURED = "T_psa_insured";
  public static final String T_PSA_UNINSURED = "T_psa_uninsured";
  public static final String T_CHLAMYDIA_SRC = "T_chlamydia_src";
  public static final String URINE = "URINE";
  public static final String T_CHLAMYDIA_INVESTIGATION_UR = "T_Chlamydia_Investigation_Ur";
  public static final String T_CHLAMYDIA_INVESTIGATION = "T_Chlamydia_Investigation";
  public static final String T_N_GONORRHOEAE_INVEST_URINE = "T_N_gonorrhoeae_Invest_Urine";
  public static final String T_N_GONORRHOEAE_DETECTION = "T_N_Gonorrhoeae_Detection";
  public static final String T_VITAMIN_D_INSURED = "T_vitamin_d_insured";
  public static final String T_VITAMIN_D_UNINSURED = "T_vitamin_d_uninsured";
  public static final String T_VITAMIN_D = "T_vitamin_d";
  @Autowired
  DynacareEorderLabTestCodeDao labTestDao;

  public OrderConverter() {}

  public OrderConverter(DynacareEorderLabTestCodeDao labTestDao) {
    this.labTestDao = labTestDao;
  }


  public RequestEOrder toDynacareOrderObject(final Map<String, EFormValue> valueMap, final Integer orderId) {
    return toDynacareOrderObject(valueMap, orderId, false);
  }
  /**
   * Convert eForm values to a Diagnostic Order
   *
   * @param valueMap hashmap that contains the key value paires to eForm values
   * @return converted Diagnostic Order
   */
  public RequestEOrder toDynacareOrderObject(final Map<String, EFormValue> valueMap, final Integer orderId, final boolean isUpdate) {
    long startTime = System.currentTimeMillis();

    LOG.debug("Calling OrderConverter.toDynacareOrderObject");
    // Load test code mapping file
    InputStream inputStream =
        OrderConverter.class.getResourceAsStream(TEST_NAME_TO_DYNACARE_TEST_CODE_MAPPING_FILE);
    Properties properties = new Properties();
    try {
      properties.load(inputStream);
      properties = ConverterHelper.addLabTestCodesToPropertiesFromLabTestCode(properties,
          labTestDao);
    } catch (IOException e) {
      LOG.error("Error loading " + TEST_NAME_TO_DYNACARE_TEST_CODE_MAPPING_FILE, e);
      throw new ServiceException("Error loading " + TEST_NAME_TO_DYNACARE_TEST_CODE_MAPPING_FILE);
    }

    val order = new RequestEOrder();

    // status
    order.setStatusId(OrderStatusEnum.NOT_PROCESSED);
    order.setTo(DynacareOrderPreferences.getToEmail());
    if (orderId != null) {
      val systemId = getSystemId(orderId);
      order.setSystemId(systemId);
      val orderAlternateId = new OrderAlternateIds();
      orderAlternateId.setAlternateId(systemId);
      val orderAlternateIdList = new ArrayList<OrderAlternateIds>();
      orderAlternateIdList.add(orderAlternateId);
      order.setOrderAlternateIds(orderAlternateIdList);
    }
    // ordering subject pregnancy
    if (valueMap.containsKey(EO_PREGNANT)) {
      order.getComment().add(Comment.createFreeTextComment(PATIENT_PREGNANT_COMMENT));
    }

    // Specimen Collection
    if (valueMap.containsKey(EO_SPECIMEN_COL_DATE)) {
      EFormValue eformValue = valueMap.get(EO_SPECIMEN_COL_DATE);
      String specimenCollectionDate =
          eformValue.getVarValue(); // expect it to be of the form yyyy/mm/dd

      // see if a time is specified too
      String specimenCollectionTime = "00:00";
      if (valueMap.containsKey(EO_SPECIMEN_COL_TIME)) {
        eformValue = valueMap.get(EO_SPECIMEN_COL_TIME);
        specimenCollectionTime =
            eformValue.getVarValue(); // expect it to be of the form HH:MM 24hr clock
      }
      // find timezone info
      eformValue = valueMap.get(EO_TIMEZONE_OFFSET);
      String timeZone = eformValue.getVarValue();
      String specimenCollectionDateTime =
          specimenCollectionDate.replace("/", "-") + "T" + specimenCollectionTime;
      addComment(order, SPECIMEN_COLLECTION_DATE_TIME_COMMENT, specimenCollectionDateTime);
    }

    // diagnosis/notes
    if (valueMap.containsKey(EO_ADDITIONAL_CLINIC_INFO)) {
      EFormValue eformValue = valueMap.get(EO_ADDITIONAL_CLINIC_INFO);
      String diagnosis = eformValue.getVarValue();
      order.getComment().add(Comment.createFreeTextComment(diagnosis));
    }

    Party patient = PartyConverter.toPatientParty(valueMap);
    Party orderingProvider = PartyConverter.toDynacareOrderingProvider(valueMap);

    ArrayList<Party> parties = new ArrayList<Party>();
    parties.add(patient);
    parties.add(orderingProvider);
    String ccProviderId =
        EFormValueHelper.getValue(
            valueMap, PractitionerPropertySet.getCopyToIdPropertyNameByIndex(1));
    if (ccProviderId != null && !ccProviderId.isEmpty()) {
      Party ccProvider = PartyConverter.toDynacareCcProviderPartyObject(valueMap);
      parties.add(ccProvider);
    }

    order.setParties(parties);

    // payer-type
    String payer = null;
    if (valueMap.containsKey(EO_OHIP)) {
      payer = OHIP;
    } else if (valueMap.containsKey(EO_WSIB)) {
      payer = WSIB;
    } else {
      payer = PATIENT;
    }

    addComment(order, PAYMENT_TYPE_COMMENT, payer);

    order.setTests(new ArrayList<Test>());
    for (String key : valueMap.keySet()) {
      if (key.startsWith(LAB_TEST_PREFIX) && isRequired(key)) {
        EFormValue value = valueMap.get(key);
        //				the test has been removed from the order
        if (StringUtils.isNullOrEmpty(value.getVarValue())) {
          continue;
        }
        Test labTest = getTestCode(valueMap, properties, key, labTestDao);
        order.getTests().add(labTest);
      }
    }
    if (order.getTests().isEmpty()) {
      String msg = "There are no tests in the order";
      LOG.error(msg);
      throw new ServiceException(msg);
    }

    long endTime = System.currentTimeMillis();
    LOG.debug("+++++++++++++++++ Performance - Coverter : " + (endTime - startTime));
    return order;
  }

  // --------------------------------------------------------------- Private
  // Methods

  /**
   * Get test code for the given eform value key
   *
   * @param valueMap hashmap for eForm values
   * @param properties test name and code mapping properties file
   * @param key eform value key to be mapped to test code
   * @return test code
   */
  private static Test getTestCode(
      Map<String, EFormValue> valueMap,
      Properties properties,
      String key,
      DynacareEorderLabTestCodeDao labTestDao) {

    String testCode = null;
    Test labTest = new Test();
    if (isComplexLabCodeMapping(key)) {
      // complex mapping
      String derivedKey = null;
      // PSA

      if (key.equals(T_TOTAL_PSA) || key.equals(T_FREE_PSA)) {
        // see if corresponding insured/uninsured is selected to determine code
        if (valueMap.containsKey(T_PSA_INSURED)) {
          labTest.getComment().add(Comment.createFreeTextComment("PSA: Insured"));
          derivedKey = key + "_insured";
        } else if (valueMap.containsKey(T_PSA_UNINSURED)) {
          labTest.getComment().add(Comment.createFreeTextComment("PSA: Uninsured"));
          derivedKey = key + "_uninsured";
        }
        LOG.debug("Key : " + derivedKey);
        testCode = properties.getProperty(derivedKey);
      } else if (key.equals(CHLAMYDIA)) {
        // check the corresponding source
        derivedKey = getDerivedKey(valueMap, T_CHLAMYDIA_SRC, T_CHLAMYDIA_INVESTIGATION_UR,  T_CHLAMYDIA_INVESTIGATION);
        testCode = properties.getProperty(derivedKey);
        addTestSourceComment(valueMap, CHLAMYDIA_SRC, labTest);
      } else if (key.equals(GC)) {
        derivedKey = getDerivedKey(valueMap, GC_SRC, T_N_GONORRHOEAE_INVEST_URINE,  T_N_GONORRHOEAE_DETECTION);
        testCode = properties.getProperty(derivedKey);
        addTestSourceComment(valueMap, GC_SRC, labTest);
      } else if (key.equals(T_VITAMIN_D_INSURED) || (key.equals(T_VITAMIN_D_UNINSURED))) {
        // see if corresponding insured/uninsured is selected to determine code
        if (valueMap.containsKey(T_VITAMIN_D_INSURED)) {
          labTest.getComment().add(Comment.createFreeTextComment("VIT D: Insured"));
        } else if (valueMap.containsKey(T_VITAMIN_D_UNINSURED)) {
          labTest.getComment().add(Comment.createFreeTextComment("VIT D: Uninsured"));
        }
        derivedKey = T_VITAMIN_D;
        LOG.debug("Key : " + derivedKey);
        testCode = properties.getProperty(derivedKey);
      } else if (key.equals(THERAPEUTIC_DRUG_MONITORING_1)) {
        EFormValue eformValue = valueMap.get(key);
        String source = eformValue.getVarValue();
        derivedKey = convertKey(source);
        LOG.debug("Key : " + derivedKey);
        testCode = properties.getProperty(derivedKey);
        // process the source info
        createTDMComment(
            valueMap,
            testCode,
            labTest,
            TDM_TIME_COLLECTED_1,
            TDM_TIME_OF_LAST_DOSE_1,
            TDM_TIME_OF_NEXT_DOSE_1);
      } else if (key.equals(THERAPEUTIC_DRUG_MONITORING_2)) {
        EFormValue eformValue = valueMap.get(key);
        String source = eformValue.getVarValue();
        derivedKey = convertKey(source);
        LOG.debug("Key : " + derivedKey);
        testCode = properties.getProperty(derivedKey);
        // process the source info
        createTDMComment(
            valueMap,
            testCode,
            labTest,
            TDM_TIME_COLLECTED_2,
            TDM_TIME_OF_LAST_DOSE_2,
            TDM_TIME_OF_NEXT_DOSE_2);
      } else if (NEONATAL_BILIRUBIN_TEST.equals(key)) {
        testCode = properties.getProperty(key);
        createNeonatalBilirubinComment(valueMap, testCode, labTest);
      } else if (key.equals(VAGINAL_RECTAL_GROUP_B_STREP)) {
        testCode = properties.getProperty(key);
        addTestSourceComment(valueMap, VAGINAL_RECTAL_GROUP_B_STREP_SRC, labTest);
      } else if (key.equals(WOUND)) {
        testCode = properties.getProperty(key);
        addTestSourceComment(valueMap, WOUND_SRC, labTest);
      } else if (key.equals(OTHER_SWABS_PUS)) {
        testCode = properties.getProperty(key);
        addTestSourceComment(valueMap, OTHER_SWABS_PUS_SRC, labTest);
      }

    } else {
      // simple mapping, just look it up from properties file
      // see if it is one of 'Other Tests'
      if (isOtherTests(key)) {
        // handle other test (find out the name of other tests)
        String derivedKey = null;
        // use naming convention to derive key from names of other tests.
        // for other tests, the test name is stored in the eForm value:value
        EFormValue eformValue = valueMap.get(key);
        derivedKey = eformValue.getVarValue();
        derivedKey = convertKey(derivedKey);
        LOG.debug("Key : " + derivedKey);
        testCode = properties.getProperty(derivedKey);

      } else {
        LOG.debug("Key : " + key);
        testCode = properties.getProperty(key);
      }
    }

    if (testCode == null || testCode.isEmpty()) {
      String msg = "testcode for key: " + key + " does not exist.";
      LOG.error(msg);
      throw new ServiceException(msg);
    }

    DynacareEorderLabTestCode dynacareTest = labTestDao.findByTestCode(testCode);
    labTest.setSystemId(LOINC);
    labTest.setDescription(dynacareTest.getLongName());
    labTest.setCode(dynacareTest.getLabTestCode());

    return labTest;
  }

  private static boolean isComplexLabCodeMapping(String key) {
    // code to determine if lab code mapping is complex or not
    return key.equals(T_TOTAL_PSA)
            || (key.equals(T_FREE_PSA))
            || (key.equals(CHLAMYDIA))
            || (key.equals(T_VITAMIN_D_INSURED))
            || (key.equals(T_VITAMIN_D_UNINSURED))
            || (key.equals(GC))
            || (key.equals(THERAPEUTIC_DRUG_MONITORING_1))
            || (key.equals(THERAPEUTIC_DRUG_MONITORING_2))
            || (key.equals(NEONATAL_BILIRUBIN_TEST))
            || (key.equals(VAGINAL_RECTAL_GROUP_B_STREP))
            || (key.equals(WOUND))
            || (key.equals(OTHER_SWABS_PUS));
  }

  private static boolean isRequired(String key) {
    // code to determine if a form field needs to be mapped to a lab test code
    return !(key.equals(T_PSA_INSURED)
            || (key.equals(T_PSA_UNINSURED))
            || (key.equals(THERAPEUTIC_DRUG_MONITORING))
            || isSource(key));
  }

  private static boolean isOtherTests(String key) {
    // code to determine if this is one of 'Other Tests;
    return key.contains(OTHER_TESTS);
  }


  private static boolean isSource(String key) {
    return key.startsWith("T_")
      && (key.endsWith("_src") || key.contains("_src_"))
      && !key.equals(THERAPEUTIC_DRUG_MONITORING_1)
      && !key.equals(THERAPEUTIC_DRUG_MONITORING_2);
  }

  private static String convertKey(String key) {
    // remove following characters
    String derivedKey = key;
    derivedKey = derivedKey.replaceAll("[+.,()\\[\\]]", "");

    // replace following characters
    derivedKey = derivedKey.replaceAll("[/ ]", "_");
    derivedKey = "T_" + derivedKey;
    if (derivedKey.endsWith("_")) {
      derivedKey = derivedKey.substring(0, derivedKey.length() - 1);
    }
    return derivedKey;
  }

  /**
   * Create questionnaire response for neonatal bilirubin test
   *
   * @param valueMap eform value hash map
   * @param testCode
   * @return questionnaire response for the specific test
   */
  private static void createNeonatalBilirubinComment(
      Map<String, EFormValue> valueMap, String testCode, Test test) {

    // need to send child age days, hours, practitioner phone and patient phone
    // as questionnaire response
    // child age days
    String childAgeDays = EFormValueHelper.getValue(valueMap, NEONATAL_BILIRUBIN_CHILD_AGE_DAYS);
    if (StringUtils.empty(childAgeDays)) {
      childAgeDays = ANSWER_UNKNOWN;
    }

    // child age hours
    String childAgeHours = EFormValueHelper.getValue(valueMap, NEONATAL_BILIRUBIN_CHILD_AGE_HOURS);
    if (StringUtils.empty(childAgeHours)) {
      childAgeHours = ANSWER_UNKNOWN;
    }

    // practitioner phone
    String practitionerPhone =
        EFormValueHelper.getValue(valueMap, NEONATAL_BILIRUBIN_PRACTITIONER_PHONE);
    if (StringUtils.empty(practitionerPhone)) {
      practitionerPhone = ANSWER_UNKNOWN;
    }

    // patient phone
    String patientPhone = EFormValueHelper.getValue(valueMap, NEONATAL_BILIRUBIN_PATIENT_PHONE);
    if (StringUtils.empty(patientPhone)) {
      patientPhone = ANSWER_UNKNOWN;
    }
    test.getComment().add(Comment.createFreeTextComment("Child's age days: " + childAgeDays));
    test.getComment().add(Comment.createFreeTextComment("Child's age hours: " + childAgeHours));
    test.getComment()
        .add(Comment.createFreeTextComment("Practitioner Phone: " + practitionerPhone));
    test.getComment().add(Comment.createFreeTextComment("Patient phone: " + patientPhone));
  }

  /**
   * Create questionnaire response for therapeutic drug monitoring test
   *
   * @param valueMap eform value hash map
   * @param testCode
   * @return questionnaire response for the specific test
   */
  private static void createTDMComment(
      Map<String, EFormValue> valueMap,
      String testCode,
      Test test,
      String timeCollected,
      String timeLastDose,
      String timeNextDose) {

    // need to send time collected, time of last dose, time of next dose
    // as questionnaire response
    // time collected
    String timeCollectedValue = EFormValueHelper.getValue(valueMap, timeCollected);
    if (StringUtils.empty(timeCollectedValue)) {
      timeCollectedValue = ANSWER_UNKNOWN;
    }

    // time of last dose
    String lastDoseValue = EFormValueHelper.getValue(valueMap, timeLastDose);
    if (StringUtils.empty(lastDoseValue)) {
      lastDoseValue = ANSWER_UNKNOWN;
    }

    String nextDoseValue = EFormValueHelper.getValue(valueMap, timeNextDose);
    if (StringUtils.empty(nextDoseValue)) {
      nextDoseValue = ANSWER_UNKNOWN;
    }

    test.getComment()
        .add(Comment.createFreeTextComment(testCode + ": Time Collected: " + timeCollectedValue));
    test.getComment().add(Comment.nextDrugDoseComment(testCode + ": " + nextDoseValue));
    test.getComment().add(Comment.lastDrugDoseComment(testCode + ": " + lastDoseValue));
  }

  private static void addTestSourceComment(
      Map<String, EFormValue> valueMap, String answerKey, Test test) {

    String answerValue = EFormValueHelper.getValue(valueMap, answerKey);
    if (StringUtils.empty(answerValue)) {
      answerValue = ANSWER_UNKNOWN;
    }

    test.getComment().add(Comment.createSpecimenSourceComment(answerValue));
  }

  private static void addComment(RequestEOrder order, String fieldName, String value) {
    if (value != null && !value.isEmpty()) {
      String comment = fieldName + " : " + value;
      order.getComment().add(Comment.createFreeTextComment(comment));
    }
  }

  private static String getDerivedKey(Map<String, EFormValue> valueMap, String sourceKey, String urKey, String investigationKey) {
    if (!valueMap.containsKey(sourceKey)) {
      // no source value specified. - ERROR
      LOG.error("No source specified for:  " + sourceKey);
      throw new ServiceException("No source specified for:  " + sourceKey);
    }

    String derivedKey;
    EFormValue eformValue = valueMap.get(sourceKey);
    String source = eformValue.getVarValue();
    if (source.equals(URINE)) {
      derivedKey = urKey;
    } else {
      derivedKey = investigationKey;
    }
    return derivedKey;
  }

  public String toJsonOrder(final RequestEOrder order) throws Exception {
    final val mapper = new ObjectMapper();
    final val pr = new CustomNullStringSerializerProvider();
    mapper.setSerializerProvider((DefaultSerializerProvider) pr);
    mapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);
    mapper.registerModule((Module) new JavaTimeModule());
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    return mapper.writeValueAsString((Object) order);
  }

  private String getSystemId(final Integer orderId) {
    return DynacareOrderPreferences.getInstanceId() + "-" + orderId;
  }

  private String generateBarcode(final Integer orderId) {
    val oscarInstanceBarcodeId = getSystemId(orderId);
    return oscarInstanceBarcodeId + checkDigit(oscarInstanceBarcodeId);
  }
 
  private String checkDigit(final String barcode) {
    var subtotal = 0;
    val charSet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%";
    for (int i = 0; i < barcode.length(); i++) {
      subtotal += charSet.indexOf(barcode.charAt(i));
    }
    return "-" + subtotal;
  }

}
