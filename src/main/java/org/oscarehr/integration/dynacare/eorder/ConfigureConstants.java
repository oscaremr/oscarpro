/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.dynacare.eorder;

public class ConfigureConstants {

  public static final String DYNACARE_ORDER_URL = "dynacare.order_url";

  public static final String DYNACARE_API_KEY = "dynacare.api_key";

  public static final String DYNACARE_CLINIC_EMAIL = "dynacare.to_email";

  public static final String DYNACARE_INSTANCE_ID = "dynacare.instance_id";
}
