/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.dynacare.eorder;

import static org.oscarehr.integration.dynacare.eorder.DynacareOrderEncryptionUtils.encrypt;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.SpringUtils;

public class DynacareOrderEncryptionAction extends Action {

  private final SystemPreferencesDao systemPreferencesDao =
      SpringUtils.getBean(SystemPreferencesDao.class);

  @Override
  public ActionForward execute(
      ActionMapping mapping,
      ActionForm form,
      HttpServletRequest request,
      HttpServletResponse response) {
    val apiKey = StringUtils.trimToNull(request.getParameter("apiKey"));
    // Set API key in systemPreferences
    if (apiKey != null) {
      systemPreferencesDao.mergeOrPersist(new SystemPreferences(
          ConfigureConstants.DYNACARE_API_KEY,
          encrypt(apiKey)
      ));
    }
    return mapping.findForward("success");
  }
}
