package org.oscarehr.integration.dynacare.eorder;

import com.dynacare.api.Order;
import com.dynacare.api.OrderStatusEnum;
import javax.servlet.http.HttpServletRequest;
import lombok.val;
import org.apache.commons.lang.WordUtils;

public class DynacareOrderStatusHelper {

  public static String convertStatusToString(final OrderStatusEnum status) {
    return WordUtils.capitalizeFully(status.name().replace("_", " "));
  }

  public static String getDescription(final OrderStatusEnum status) {
    String description;
    switch(status) {
      case NOT_PROCESSED:
        description = "Order received from clinician and waiting in holding tanks.";
        break;
      case VALIDATED:
        description = "Order retrieved from the holding tank and processed (patient visited the lab, samples collected.";
        break;
      case PROCESSED:
        description = "Order submitted to the Dynacare LIS.";
        break;
      case IN_PROGRESS:
        description = "Testing of samples is in progress.";
        break;
      case COMPLETED:
        description = "Sample testing completed and results available in Dynacare LIS.";
        break;
      default:
        description = "";
    }
    return description;
  }

  public static void setStatus(HttpServletRequest request, OrderStatusEnum status) {
    request.setAttribute("status", convertStatusToString(status));
    request.setAttribute("status_description", getDescription(status));
  }

  public static void setStatusError(HttpServletRequest request, String message) {
    request.setAttribute("status", "Error obtaining status");
    request.setAttribute("status_description", message);
  }
}
