/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.dynacare.eorder.converter;

import com.dynacare.api.Address;
import com.dynacare.api.PartyRoleEnum;
import com.dynacare.api.PartyTypeEnum;
import com.dynacare.api.PhoneNumber;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class Party {

	private String firstName;
	private String lastName;
	private String companyName;
	private List<Address> addresses;
	private PartyRoleEnum roleId;
	private String role;
	private PartyTypeEnum typeId;
	private String type;
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss[.SSSSSSS][xxx]")
	private LocalDateTime dateOfBirth;
	private String gender = "";
	private String email;
	private boolean emailAuthorized;
	private String language = "";
	private List<PhoneNumber> phoneNumbers;
	private List<PartyIdentifier> partyIdentifiers;
	public Party() {
		this.companyName = "";
		this.firstName = "";
		this.lastName = "";
		this.addresses = new ArrayList<Address>();
		this.role = "";
		this.type = "";
		this.email = "";
		this.phoneNumbers = new ArrayList<PhoneNumber>();
		this.partyIdentifiers = new ArrayList<PartyIdentifier>();
	}
}
