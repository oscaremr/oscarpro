/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.dynacare.eorder.converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Map;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.dynacare.eorder.ConfigureConstants;
import org.oscarehr.integration.dynacare.eorder.DynacareOrderPreferences;
import org.oscarehr.integration.dynacare.eorder.converter.PartyIdentifier.IdentifierAbbreviationEnum;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.util.MiscUtils;
import com.dynacare.api.Address;
import com.dynacare.api.GenderEnum;
import com.dynacare.api.LanguageEnum;
import com.dynacare.api.PartyRoleEnum;
import com.dynacare.api.PartyTypeEnum;
import com.dynacare.api.PhoneNumber;
import com.dynacare.api.PhoneTypeEnum;

public class PartyConverter {

  private static final Logger LOG = MiscUtils.getLogger();

  public static Party toDynacareOrderingProvider(Map<String, EFormValue> valueMap) {

    Party practitioner = new Party();
    String providerNo =
        EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_PROVIDER_NO);
    if (providerNo == null) {
      LOG.error(PractitionerPropertySet.PRACTITIONER_PROVIDER_NO + " is missing.");
      throw new ServiceException(
          PractitionerPropertySet.PRACTITIONER_PROVIDER_NO + " is required.");
    }

    // Name
    String lastName =
        EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_LAST_NAME);
    if (lastName == null) {
      LOG.error(PractitionerPropertySet.PRACTITIONER_LAST_NAME + " is missing.");
      throw new ServiceException(PractitionerPropertySet.PRACTITIONER_LAST_NAME + " is required.");
    }

    String firstName =
        EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_FIRST_NAME);
    if (firstName == null) {
      LOG.error(PractitionerPropertySet.PRACTITIONER_FIRST_NAME + " is missing.");
      throw new ServiceException(PractitionerPropertySet.PRACTITIONER_FIRST_NAME + " is required.");
    }

    String addressLine =
        EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_ADDRESS);
    if (addressLine == null) {
      LOG.error(PractitionerPropertySet.PRACTITIONER_ADDRESS + " is missing.");
      throw new ServiceException(PractitionerPropertySet.PRACTITIONER_ADDRESS + " is required.");
    }
    String city = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_CITY);
    if (city == null) {
      LOG.error(PractitionerPropertySet.PRACTITIONER_CITY + " is missing.");
      throw new ServiceException(PractitionerPropertySet.PRACTITIONER_CITY + " is required.");
    }
    String postal =
        EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_POSTAL);
    if (postal == null) {
      LOG.error(PractitionerPropertySet.PRACTITIONER_POSTAL + " is missing.");
      throw new ServiceException(PractitionerPropertySet.PRACTITIONER_POSTAL + " is required.");
    }
    String provinceEformValue =
        EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_PROVINCE);
    String province = "";
    if (provinceEformValue == null) {
      LOG.error(PractitionerPropertySet.PRACTITIONER_PROVINCE + " is missing.");
      throw new ServiceException(PractitionerPropertySet.PRACTITIONER_PROVINCE + " is required.");
    } else {
      province = ProvinceHelper.getProvinceAbrev(provinceEformValue, postal);
    }

    String phone = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_PHONE);
    if (phone == null) {
      LOG.error(PractitionerPropertySet.PRACTITIONER_PHONE + " is missing.");
      throw new ServiceException(PractitionerPropertySet.PRACTITIONER_PHONE + " is required.");
    }

    String ohipNo =
        EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_OHIP_NO);
    if (ohipNo == null) {
      LOG.error(PractitionerPropertySet.PRACTITIONER_OHIP_NO + " is missing.");
      throw new ServiceException(PractitionerPropertySet.PRACTITIONER_OHIP_NO + " is required.");
    }

    String cpso = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_CPSO);
    if (cpso == null) {
      LOG.error(PractitionerPropertySet.PRACTITIONER_CPSO + " is missing.");
      throw new ServiceException(PractitionerPropertySet.PRACTITIONER_CPSO + " is required.");
    }

    practitioner.setPartyIdentifiers(new ArrayList<PartyIdentifier>());
    practitioner.getPartyIdentifiers().add(PartyIdentifier.createCPSOIdentifier(cpso));
    practitioner.getPartyIdentifiers().add(PartyIdentifier.createOHIPBillingIdentifier(ohipNo));
    practitioner.setFirstName(firstName);
    practitioner.setLastName(lastName);
    practitioner.setAddresses(new ArrayList<Address>());
    practitioner.getAddresses().add(new Address("", addressLine, "", city, province, postal, ""));
    practitioner.setRoleId(PartyRoleEnum.ORDERING);
    practitioner.setRole(PartyRoleEnum.ORDERING.getDisplayName());
    practitioner.setTypeId(PartyTypeEnum.PHYSICIAN);
    practitioner.setType(PartyTypeEnum.PHYSICIAN.getDisplayName());
    practitioner.setEmailAuthorized(false);
    practitioner.setPhoneNumbers(new ArrayList<PhoneNumber>());
    practitioner.getPhoneNumbers().add(new PhoneNumber(phone, PhoneTypeEnum.WORK));

    return practitioner;
  }

  public static Party toDynacareCcProviderPartyObject(Map<String, EFormValue> valueMap) {

    //		Currently only allowing 1 cc provider
    int index = 1;
    Party practitioner = new Party();
    String providerNo =
        EFormValueHelper.getValue(
            valueMap, PractitionerPropertySet.getCopyToIdPropertyNameByIndex(index));
    if (providerNo == null) {
      LOG.error(PractitionerPropertySet.getCopyToIdPropertyNameByIndex(index) + " is missing.");
      throw new ServiceException(
          PractitionerPropertySet.getCopyToIdPropertyNameByIndex(index) + " is required.");
    }

    // Name
    String lastName =
        EFormValueHelper.getValue(
            valueMap, PractitionerPropertySet.getCopyToProviderLastNamePropertyNameByIndex(index));
    if (lastName == null) {
      LOG.error(
          PractitionerPropertySet.getCopyToProviderLastNamePropertyNameByIndex(index)
              + " is missing.");
      throw new ServiceException(
          PractitionerPropertySet.getCopyToProviderLastNamePropertyNameByIndex(index)
              + " is required.");
    }
    String firstName =
        EFormValueHelper.getValue(
            valueMap, PractitionerPropertySet.getCopyToProviderFirstNamePropertyNameByIndex(index));
    if (firstName == null) {
      LOG.error(
          PractitionerPropertySet.getCopyToProviderFirstNamePropertyNameByIndex(index)
              + " is missing.");
      throw new ServiceException(
          PractitionerPropertySet.getCopyToProviderFirstNamePropertyNameByIndex(index)
              + " is required.");
    }
    String addressLine =
        EFormValueHelper.getValue(
            valueMap, PractitionerPropertySet.getCopyToProviderAddressPropertyNameByIndex(index));
    if (addressLine == null) {
      LOG.error(
          PractitionerPropertySet.getCopyToProviderAddressPropertyNameByIndex(index)
              + " is missing.");
      throw new ServiceException(
          PractitionerPropertySet.getCopyToProviderAddressPropertyNameByIndex(index)
              + " is required.");
    }
    String city =
        EFormValueHelper.getValue(
            valueMap, PractitionerPropertySet.getCopyToProviderCityPropertyNameByIndex(index));
    if (city == null) {
      LOG.error(
          PractitionerPropertySet.getCopyToProviderCityPropertyNameByIndex(index) + " is missing.");
      throw new ServiceException(
          PractitionerPropertySet.getCopyToProviderCityPropertyNameByIndex(index)
              + " is required.");
    }
    String postal =
        EFormValueHelper.getValue(
            valueMap, PractitionerPropertySet.getCopyToProviderPostalPropertyNameByIndex(index));
    if (postal == null) {
      LOG.error(
          PractitionerPropertySet.getCopyToProviderPostalPropertyNameByIndex(index)
              + " is missing.");
      throw new ServiceException(
          PractitionerPropertySet.getCopyToProviderPostalPropertyNameByIndex(index)
              + " is required.");
    }

    String country =
        EFormValueHelper.getValue(
            valueMap, PractitionerPropertySet.getCopyToProviderCountryPropertyNameByIndex(index));
    if (country == null) {
      LOG.error(
          PractitionerPropertySet.getCopyToProviderCountryPropertyNameByIndex(index)
              + " is missing.");
      throw new ServiceException(
          PractitionerPropertySet.getCopyToProviderCountryPropertyNameByIndex(index)
              + " is required.");
    }

    String provinceEformValue =
        EFormValueHelper.getValue(
            valueMap, PractitionerPropertySet.getCopyToProviderProvincePropertyNameByIndex(index));
    String province = "";
    if (provinceEformValue == null) {
      LOG.error(
          PractitionerPropertySet.getCopyToProviderProvincePropertyNameByIndex(index)
              + " is missing.");
      throw new ServiceException(
          PractitionerPropertySet.getCopyToProviderProvincePropertyNameByIndex(index)
              + " is required.");
    } else {
      province = ProvinceHelper.getProvinceAbrev(provinceEformValue, postal);
    }

    String cpso = EFormValueHelper.getValue(
        valueMap, PractitionerPropertySet.getCopyToProviderCpsidPropertyNameByIndex(index));
    if (cpso == null) {
      LOG.error(PractitionerPropertySet.getCopyToProviderCpsidPropertyNameByIndex(index)
          + " is missing.");
      throw new ServiceException(
          PractitionerPropertySet.getCopyToProviderCpsidPropertyNameByIndex(index)
              + " is required.");
    }

    String ohipNumber = EFormValueHelper.getValue(
        valueMap, PractitionerPropertySet.getCopyToProviderOhipPropertyNameByIndex(index));
    if (ohipNumber == null) {
      LOG.error(PractitionerPropertySet.getCopyToProviderOhipPropertyNameByIndex(index)
              + " is missing.");
      throw new ServiceException(
          PractitionerPropertySet.getCopyToProviderOhipPropertyNameByIndex(index)
              + " is required.");
    }
    practitioner.setFirstName(firstName);
    practitioner.setLastName(lastName);
    practitioner.setAddresses(new ArrayList<Address>());
    practitioner
        .getAddresses()
        .add(new Address("", addressLine, "", city, province, postal, country));
    practitioner.setRoleId(PartyRoleEnum.COURTESY_COPY);
    practitioner.setRole(PartyRoleEnum.COURTESY_COPY.getDisplayName());
    practitioner.setTypeId(PartyTypeEnum.PHYSICIAN);
    practitioner.setType(PartyTypeEnum.PHYSICIAN.getDisplayName());
    practitioner.getPartyIdentifiers().add(PartyIdentifier.createCPSOIdentifier(cpso));
    practitioner.getPartyIdentifiers().add(PartyIdentifier.createOHIPBillingIdentifier(ohipNumber));
    return practitioner;
  }

  public static Party toPatientParty(Map<String, EFormValue> valueMap) {
    Party patient = new Party();

    String demographicNumber =
        EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_DEMOGRAPHIC_ID);
    if (demographicNumber == null) {
      LOG.error(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID + " is missing.");
      throw new ServiceException(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID + " is required.");
    }

    String firstName = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_FIRST_NAME);
    if (firstName == null) {
      LOG.error(PatientPropertySet.PATIENT_FIRST_NAME + " is missing.");
      throw new ServiceException(PatientPropertySet.PATIENT_FIRST_NAME + " is required.");
    }

    String lastName = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_LAST_NAME);
    if (lastName == null) {
      LOG.error(PatientPropertySet.PATIENT_LAST_NAME + " is missing.");
      throw new ServiceException(PatientPropertySet.PATIENT_LAST_NAME + " is required.");
    }

    String address1 = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_ADDRESS_LINE);
    if (address1 == null) {
      LOG.error(PatientPropertySet.PATIENT_ADDRESS_LINE + " is missing.");
      throw new ServiceException(PatientPropertySet.PATIENT_ADDRESS_LINE + " is required.");
    }

    String city = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_ADDRESS_CITY);
    if (city == null) {
      LOG.error(PatientPropertySet.PATIENT_ADDRESS_CITY + " is missing.");
      throw new ServiceException(PatientPropertySet.PATIENT_ADDRESS_CITY + " is required.");
    }

    String province =
        EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_ADDRESS_PROVINCE);
    if (province == null) {
      LOG.error(PatientPropertySet.PATIENT_ADDRESS_PROVINCE + " is missing.");
      throw new ServiceException(PatientPropertySet.PATIENT_ADDRESS_PROVINCE + " is required.");
    }

    String postal =
        EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE);
    if (postal == null) {
      LOG.error(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE + " is missing.");
      throw new ServiceException(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE + " is required.");
    }

    String dobValue = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_DOB);
    if (dobValue == null) {
      LOG.error(PatientPropertySet.PATIENT_DOB + " is missing.");
      throw new ServiceException(PatientPropertySet.PATIENT_DOB + " is required.");
    }

    GenderEnum gender = GenderEnum.UNKOWN;
    String genderValue = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_GENDER);
    if (genderValue != null) {
      if (StringUtils.isNotBlank(genderValue) && genderValue.equalsIgnoreCase("M")) {
        gender = GenderEnum.MALE;
      } else if (StringUtils.isNotBlank(genderValue) && genderValue.equalsIgnoreCase("F")) {
        gender = GenderEnum.FEMALE;
      }
    } else {
      LOG.error(PatientPropertySet.PATIENT_GENDER + " is missing.");
      throw new ServiceException(PatientPropertySet.PATIENT_GENDER + " is required.");
    }

    val email = getPatientEmail(valueMap);

    val phone = getPatientPhone(valueMap);

    val hin = getPatientHin(valueMap);

    val hinVersion = getPatientHinVersion(valueMap);

    patient.setFirstName(firstName);
    patient.setLastName(lastName);

    String provinceAbrev = ProvinceHelper.getProvinceAbrev(province, postal);
    Address address = new Address("", address1, "", city, provinceAbrev, postal, "");
    if (province.startsWith("US")) {
      address = new Address("", address1, "", city, provinceAbrev, postal, "USA");
    }

    patient.setAddresses(new ArrayList<Address>());
    patient.getAddresses().add(address);
    patient.setRoleId(PartyRoleEnum.PATIENT);
    patient.setRole(PartyRoleEnum.PATIENT.getDisplayName());
    patient.setTypeId(PartyTypeEnum.PATIENT);
    patient.setType(PartyTypeEnum.PATIENT.getDisplayName());

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
    LocalDate dobDay = LocalDate.parse(dobValue, formatter);
    LocalTime midNight = LocalTime.MIDNIGHT;
    LocalDateTime dob = LocalDateTime.of(dobDay, midNight);
    patient.setDateOfBirth(dob);

    patient.setGender(gender.getDisplay());
    patient.setLanguage(LanguageEnum.ENGLISH_CANADA.getDisplayName());
    patient.setEmail(email);
    patient.setEmailAuthorized(false);
    if (phone.matches("\\(?\\d{3}\\)?[\\s.\\-]?\\d{3}[\\s.\\-]?\\d{4}")) {
      patient.setPhoneNumbers(new ArrayList<PhoneNumber>());
      patient.getPhoneNumbers().add(new PhoneNumber(phone, PhoneTypeEnum.HOME));
    }

    if (!province.startsWith("US") || !postal.matches("^[0-9]{5}(?:-[0-9]{4})?$")) {
      patient.setPartyIdentifiers(new ArrayList<PartyIdentifier>());
      IdentifierAbbreviationEnum abrev = getAbbreviationTypeFromProvince(provinceAbrev);
      patient.getPartyIdentifiers().add(new PartyIdentifier(hin, hinVersion, abrev));
    }
    return patient;
  }

  private static String createSystemId(String id) {
    if (id == null || id.isEmpty()) {
      String msg = "Null or empty Id";
      LOG.error(msg);
      throw new ServiceException(msg);
    }
    return DynacareOrderPreferences.getInstanceId() + "-" + id;
  }

  private static IdentifierAbbreviationEnum getAbbreviationTypeFromProvince(
      String provAbreviation) {

    switch (provAbreviation) {
      case "AB":
        return IdentifierAbbreviationEnum.CANAB;
      case "BC":
        return IdentifierAbbreviationEnum.CANBC;
      case "MB":
        return IdentifierAbbreviationEnum.CANMB;
      case "NB":
        return IdentifierAbbreviationEnum.CANNB;
      case "NL":
        return IdentifierAbbreviationEnum.CANNL;
      case "NS":
        return IdentifierAbbreviationEnum.CANNS;
      case "NT":
        return IdentifierAbbreviationEnum.CANNT;
      case "NU":
        return IdentifierAbbreviationEnum.CANNU;
      case "ON":
        return IdentifierAbbreviationEnum.CANON;
      case "PE":
        return IdentifierAbbreviationEnum.CANPE;
      case "QC":
        return IdentifierAbbreviationEnum.CANQC;
      case "SK":
        return IdentifierAbbreviationEnum.CANSK;
      case "YT":
        return IdentifierAbbreviationEnum.CANYT;
      default:
        return null;
    }
  }

  private static String getPatientHin(final Map<String, EFormValue> valueMap) {
    return StringUtils.trimToEmpty(EFormValueHelper.getValue(
        valueMap,
        PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER
    ));
  }

  private static String getPatientHinVersion(final Map<String, EFormValue> valueMap) {
    return StringUtils.trimToEmpty(EFormValueHelper.getValue(
        valueMap,
        PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER_VERSION
    ));
  }

  private static String getPatientPhone(final Map<String, EFormValue> valueMap) {
    return StringUtils.trimToEmpty(EFormValueHelper.getValue(
        valueMap,
        PatientPropertySet.PATIENT_PHONE
    ));
  }

  private static String getPatientEmail(final Map<String, EFormValue> valueMap) {
    return StringUtils.trimToEmpty(EFormValueHelper.getValue(
        valueMap,
        PatientPropertySet.PATIENT_EMAIL
    ));
  }
}
