/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.dynacare.eorder.converter;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PartyIdentifier {
	private String identification;
	private String subIdentification;
	private String identifierAbbreviation;
	private String identifierTypeValue;
	private String identifierType;
	private static String CPSO_TYPE;
	private static String OHIP_BILL_TYPE;
	static final Map<IdentifierAbbreviationEnum, String> IDENTIFIER_TYPES;

	static {
		PartyIdentifier.CPSO_TYPE = "17";
		PartyIdentifier.OHIP_BILL_TYPE = "27";
		IDENTIFIER_TYPES = new HashMap<IdentifierAbbreviationEnum, String>() {
			{
				this.put(IdentifierAbbreviationEnum.CANON, "1");
				this.put(IdentifierAbbreviationEnum.CANAB, "2");
				this.put(IdentifierAbbreviationEnum.CANBC, "3");
				this.put(IdentifierAbbreviationEnum.CANMB, "4");
				this.put(IdentifierAbbreviationEnum.CANNL, "5");
				this.put(IdentifierAbbreviationEnum.CANNS, "6");
				this.put(IdentifierAbbreviationEnum.CANNT, "7");
				this.put(IdentifierAbbreviationEnum.CANNU, "8");
				this.put(IdentifierAbbreviationEnum.CANYT, "9");
				this.put(IdentifierAbbreviationEnum.CANPE, "10");
				this.put(IdentifierAbbreviationEnum.CANQC, "11");
				this.put(IdentifierAbbreviationEnum.CANSK, "12");
				this.put(IdentifierAbbreviationEnum.CSAN, "13");
				this.put(IdentifierAbbreviationEnum.MFHC, "16");
				this.put(IdentifierAbbreviationEnum.CPSO, "17");
				this.put(IdentifierAbbreviationEnum.CANNB, "18");
				this.put(IdentifierAbbreviationEnum.OHIP_BILL, "27");
			}
		};
	}

	public PartyIdentifier(final String identification, final String subIdentification,
			final String identifierAbbreviation, final String identifierType) {
		this.identification = identification;
		this.subIdentification = subIdentification;
		this.identifierAbbreviation = identifierAbbreviation;
		this.identifierType = identifierType;
	}

	public PartyIdentifier(final String identification, final String subIdentification,
			final IdentifierAbbreviationEnum identifierAbbreviation) {
		this.identification = identification;
		this.subIdentification = subIdentification;
		this.identifierAbbreviation = identifierAbbreviation.toString();
		this.identifierTypeValue = identifierAbbreviation.toString();
		this.identifierType = PartyIdentifier.IDENTIFIER_TYPES.get(identifierAbbreviation);
	}

	public static PartyIdentifier createCPSOIdentifier(final String cpso) {
		return new PartyIdentifier(cpso, "", IdentifierAbbreviationEnum.CPSO);
	}

	public static PartyIdentifier createOHIPBillingIdentifier(final String billingNo) {
		return new PartyIdentifier(billingNo, "", IdentifierAbbreviationEnum.OHIP_BILL);
	}
	@AllArgsConstructor 
	public enum IdentifierAbbreviationEnum {
		CANON,
		CANAB,
		CANBC,
		CANMB,
		CANNL, 
		CANNS,
		CANNT, 
		CANNU,
		CANYT, 
		CANPE, 
		CANQC, 
		CANSK, 
		CSAN,
		MFHC, 
		CPSO, 
		CANNB, 
		OHIP_BILL 
	}

}