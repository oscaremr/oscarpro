/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.dynacare.eorder.converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.dynacare.api.AbstractOrderObject;
import com.dynacare.api.Comment;
import com.dynacare.api.OrderStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class RequestEOrder extends AbstractOrderObject {
	private String accession;
	private String systemId;
	private String to;
	private String from;
	private List<OrderAlternateIds> orderAlternateIds;
	private List<Party> parties;
	private List<Test> tests;
	private List<Comment> comment;
	private List<MetaData> metaDatas;
	private OrderStatusEnum statusId;
	public RequestEOrder() {
		this.accession = "";
		this.from = "";
		this.orderAlternateIds = new ArrayList<OrderAlternateIds>();
		this.parties = null;
		this.tests = null;
		this.comment = new ArrayList<Comment>();
		this.metaDatas = Collections.singletonList(new MetaData());
	}
}
