/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.dynacare.eorder;

import static org.oscarehr.integration.dynacare.eorder.DynacareOrderEncryptionUtils.decrypt;

import com.dynacare.api.Order;
import java.nio.charset.StandardCharsets;
import java.util.List;
import lombok.val;
import lombok.var;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.oscarehr.common.dao.DynacareEorderDao;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.dynacare.eorder.converter.OrderConverter;
import org.oscarehr.integration.dynacare.eorder.converter.RequestEOrder;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.util.MiscUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DynacareOrderServiceImpl implements DynacareOrderService {

  private static final Logger LOG = MiscUtils.getLogger();
  private static final String DYNACARE_API_KEY_HEADER = "Ocp-Apim-Subscription-Key";
  private static final String APPLICATION_JSON_FORMAT = "application/json";
  private static final String HTTP_HEADER_ACCEPT = "Accept";
  private static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";

  @Autowired private DynacareEorderDao eOrderDao;
  @Autowired private OrderConverter orderConverter;

  public DynacareOrderServiceImpl() {}

  public DynacareOrderServiceImpl(DynacareEorderDao eOrderDao, OrderConverter orderConverter) {
    this.eOrderDao = eOrderDao;
    this.orderConverter = orderConverter;
  }

  @Override
  public Order createOrder(EFormData eformData, List<EFormValue> eformValues)
      throws ServiceException {
    Order responseOrder = null;
    val valueMap = EFormValueHelper.getValueMap(eformValues);
    val eOrder = eOrderDao.findByExtEFormDataId(eformData.getId());
    val httpPost = new HttpPost(DynacareOrderPreferences.getOrderUrl());
    try (CloseableHttpClient client = HttpClients.createDefault()) {
      val order = orderConverter.toDynacareOrderObject(valueMap, eOrder.getId());
      validatePreferences();
      setHttpPostData(httpPost, order);
      try (CloseableHttpResponse response = client.execute(httpPost)) {
        validateResponse(response);
        responseOrder = convertToResponseOrder(response);
      }
      LOG.info("########### response order:" + responseOrder);
    } catch (Exception e) {
      LOG.error(e.getMessage());
      throw new ServiceException(e.getMessage());
    }
    return responseOrder;
  }

  @Override
  public Order updateOrder(final EFormData eformData, final List<EFormValue> eformValues)
      throws ServiceException {
    val valueMap = EFormValueHelper.getValueMap(eformValues);
    val eorder = eOrderDao.findByExtEFormDataId(eformData.getId());
    var responseOrder = new Order();
    try (val client = HttpClients.createDefault()) {
      validatePreferences();
      val httpPost = new HttpPost(DynacareOrderPreferences.getOrderUrl()
          + eorder.getExternalOrderId());
      val order = orderConverter.toDynacareOrderObject(valueMap, eorder.getId(), true);
      setHttpPostData(httpPost, order);
      try (val response = client.execute(httpPost)) {
        validateResponse(response);
        responseOrder = convertToResponseOrder(response);
      }
    } catch (Exception e) {
      LOG.error(e.getMessage());
      throw new ServiceException(e.getMessage());
    }
    return responseOrder;
  }

  @Override
  public Order getASingleOrder(String orderId) {
    Order responseOrder = null;
    val decryptedKey = decrypt(DynacareOrderPreferences.getApiKey());
    val httpGet = new HttpGet(DynacareOrderPreferences.getOrderUrl() + orderId);
    httpGet.setHeader(HTTP_HEADER_ACCEPT, APPLICATION_JSON_FORMAT);
    httpGet.setHeader(HTTP_HEADER_CONTENT_TYPE, APPLICATION_JSON_FORMAT);
    httpGet.setHeader(DYNACARE_API_KEY_HEADER, decryptedKey);
    try (CloseableHttpClient client = HttpClients.createDefault();
        CloseableHttpResponse response = client.execute(httpGet)) {
      validatePreferences();
      validateResponse(response);
      val result = EntityUtils.toString(response.getEntity(), "UTF-8");
      LOG.info("######## response order: " + result);
      val jsonResponseConverter = new com.dynacare.api.converter.OrderConverter();
      responseOrder = jsonResponseConverter.toOrderObject(parseOrderJson(result));
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    }
    return responseOrder;
  }

  private String parseOrderJson(String orderJson) {
    orderJson = removeContainingBrackets(orderJson);
    return orderJson.contains("OrderedDate")
        ? replaceOrderedDate(orderJson)
        : orderJson;
  }

  private String removeContainingBrackets(final String orderJson) {
    return orderJson.substring(1, orderJson.length() - 1);
  }

  private String replaceOrderedDate(final String orderJson) {
    val jsonObject = new JSONObject(orderJson);
    jsonObject.put("OrderedDate", jsonObject.get("OrderedDate").toString() + ".0000000");
    return jsonObject.toString();
  }

  private Order convertToResponseOrder(CloseableHttpResponse response) throws Exception {
    val result = EntityUtils.toString(response.getEntity(), "UTF-8");
    LOG.info("######## response order: " + result);
    val jsonResponseConverter = new com.dynacare.api.converter.OrderConverter();
    return jsonResponseConverter.toOrderObject(result);
  }

  private void setHttpPostData(HttpPost httpPost, RequestEOrder order) throws Exception {
    val jsonOrder = orderConverter.toJsonOrder(order);
    val decryptedKey = decrypt(DynacareOrderPreferences.getApiKey());
    LOG.info("######## sending order: " + jsonOrder);
    val entity = new StringEntity(jsonOrder, StandardCharsets.UTF_8);
    httpPost.setEntity(entity);
    httpPost.setHeader(HTTP_HEADER_ACCEPT, APPLICATION_JSON_FORMAT);
    httpPost.setHeader(HTTP_HEADER_CONTENT_TYPE, APPLICATION_JSON_FORMAT);
    httpPost.setHeader(DYNACARE_API_KEY_HEADER, decryptedKey);
  }

  private void validatePreferences() {
    for (val preference: DynacareOrderPreferences.values()) {
      if (preference.isValidate()) {
        validatePreference(preference.getName(), preference.getPreferenceValue());
      }
    }
  }

  private void validatePreference(final String preferenceName, final String preferenceValue) {
    if (StringUtils.isBlank(preferenceValue)) {
      val error = String.format(
          "%s is null, this must be defined in the SystemPreferences table", preferenceName);
      LOG.error(error);
      throw new ServiceException(error);
    }
  }

  private void validateResponse(CloseableHttpResponse response) {
    if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
      val msg = "Call to Dynacare API Failed with status "
          + response.getStatusLine().getStatusCode();
      throw new ServiceException(msg);
    }
  }
}