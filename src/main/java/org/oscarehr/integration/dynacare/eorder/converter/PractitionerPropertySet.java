package org.oscarehr.integration.dynacare.eorder.converter;

public class PractitionerPropertySet {

  // The names of the following properties are from apconfig.xml, we must use the exact name
  // on the Lab Requisition form in order to have the fields populated using data from provider
  // related tables.
  public static final String PRACTITIONER_NAME = "doctor";
  public static final String PRACTITIONER_OHIP_NO = "doctor_ohip_no";
  public static final String PRACTITIONER_CPSO = "doctor_cpsid";
  public static final String PRACTITIONER_PHONE = "clinic_phone_hidden";
  public static final String PRACTITIONER_FIRST_NAME = "doctor_first_name";
  public static final String PRACTITIONER_LAST_NAME = "doctor_last_name";
  public static final String PRACTITIONER_ADDRESS = "clinic_addressLine";
  public static final String PRACTITIONER_CITY = "clinic_city";
  public static final String PRACTITIONER_POSTAL = "clinic_postal";
  public static final String PRACTITIONER_PROVINCE = "clinic_province";
  public static final String PRACTITIONER_TITLE = "doctor_title";
  public static final String PRACTITIONER_PROVIDER_NO = "efmprovider_no";

  public static final String COPY_TO_PROVIDER_PREFIX = "copy_to_doctor_";

  public static final String getCopyToIdPropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_id";
  }

  public static final String getCopyToProviderAddressPropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_address1";
  }

  public static final String getCopyToProviderCityPropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_city";
  }

  public static final String getCopyToProviderProvincePropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_province";
  }

  public static final String getCopyToProviderPostalPropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_postal";
  }

  public static final String getCopyToProviderCountryPropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_country";
  }

  public static final String getCopyToProviderFirstNamePropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_first_name";
  }

  public static final String getCopyToProviderLastNamePropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_last_name";
  }

  public static final String getCopyToProviderTitlePropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_title";
  }

  public static final String getCopyToProviderCpsidPropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_cpsid";
  }

  public static final String getCopyToProviderOhipPropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_ohip_no";
  }
}
