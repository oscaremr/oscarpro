/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.dynacare.eorder;

import com.dynacare.api.Order;
import java.util.List;
import lombok.val;
import org.apache.log4j.Logger;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.util.MiscUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DynacareOrderGatewayImpl implements DynacareOrderGateway {

  private static final Logger LOG = MiscUtils.getLogger();

  @Autowired private DynacareOrderService orderService;

  public DynacareOrderGatewayImpl() {}

  @Autowired
  public DynacareOrderGatewayImpl(DynacareOrderService orderService) {
    this.orderService = orderService;
  }

  @Override
  public Order createOrder(EFormData eformData, List<EFormValue> eformValues) {
    LOG.debug("Calling OrderGatewayImpl.createOrder");
    long startTime = System.currentTimeMillis();
    Order order = orderService.createOrder(eformData, eformValues);
    long endTime = System.currentTimeMillis();
    LOG.debug("+++++++++++++++++ Performance - Overall : " + (endTime - startTime));
    return order;
  }

  @Override
  public Order updateOrder(final EFormData eformData, final List<EFormValue> eformValues) {
    LOG.debug("Calling OrderGatewayImpl.updateOrder");
    val startTime = System.currentTimeMillis();
    val order = orderService.updateOrder(eformData, eformValues);
    val endTime = System.currentTimeMillis();
    LOG.debug("+++++++++++++++++ Performance - Overall : " + (endTime - startTime));
    return order;
  }

  @Override
  public Order getASingleOrder(String orderId) {
    LOG.debug("Calling OrderGatewayImpl.getASingleOrder");
    val startTime = System.currentTimeMillis();
    val order = orderService.getASingleOrder(orderId);
    val endTime = System.currentTimeMillis();
    LOG.debug("+++++++++++++++++ Performance - Overall : " + (endTime - startTime));
    return order;
  }
}
