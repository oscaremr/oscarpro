/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.dynacare.eorder.converter;

import org.apache.log4j.Logger;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.util.MiscUtils;

public class ProvinceHelper {

  private static final Logger LOG = MiscUtils.getLogger();

  public static String getProvinceAbrev(String province, String postalCode) {
    if (province == null || province.isEmpty()) {
      if (postalCode == null || postalCode.isEmpty()) {
        String msg = "Province and Postal code are null or empty";
        LOG.error(msg);
        throw new ServiceException(msg);
      } else {
        return getProvinceByPostalCode(postalCode);
      }
    } else if (province.startsWith("US")) {
      return province.replace("US-", "");
    }
    String provinceAbreviation = "";
    switch (province.toUpperCase()) {
      case "BRITISH COLUMBIA":
      case "BC":
      case "B.C.":
        provinceAbreviation = "BC";
        break;
      case "ALBERTA":
      case "AB":
        provinceAbreviation = "AB";
        break;
      case "SASKATCHEWAN":
      case "SASK":
      case "SK":
        provinceAbreviation = "SK";
        break;
      case "MANITOBA":
      case "MAN":
      case "MB":
        provinceAbreviation = "MB";
        break;
      case "ONTARIO":
      case "ONT":
      case "ON":
        provinceAbreviation = "ON";
        break;
      case "QUEBEC":
      case "QUE":
      case "QC":
        provinceAbreviation = "QC";
        break;
      case "NEW BRUNSWICK":
      case "N.B.":
      case "NB":
        provinceAbreviation = "NB";
        break;
      case "NOVA SCOTIA":
      case "N.S.":
      case "NS":
        provinceAbreviation = "NS";
        break;
      case "PRINCE EDWARD ISLAND":
      case "P.E.I.":
      case "PE":
        provinceAbreviation = "PE";
        break;
      case "NEWFOUNDLAND AND LABRADOR":
      case "N.L.":
      case "NL":
        provinceAbreviation = "NL";
        break;
      case "YUKON":
      case "Y.T.":
      case "YT":
        provinceAbreviation = "YT";
        break;
      case "NORTHWEST TERRITORIES":
      case "N.W.T.":
      case "NT":
        provinceAbreviation = "NT";
        break;
      case "NUNAVUT":
      case "NVT":
      case "NU":
        provinceAbreviation = "NU";
        break;
      default:
        provinceAbreviation = getProvinceByPostalCode(postalCode);
    }

    return provinceAbreviation;
  }

  private static String getProvinceByPostalCode(String postalCode) {
    if (postalCode == null || postalCode.isEmpty()) {
      String msg = "Postal Code is null or empty";
      LOG.error(msg);
      throw new ServiceException(msg);
    }
    String abreviation = "";
    switch (postalCode.toUpperCase().substring(0, 1)) {
      case "A":
        abreviation = "NL";
        break;
      case "B":
        abreviation = "NS";
        break;
      case "C":
        abreviation = "PE";
        break;

      case "E":
        abreviation = "NB";
        break;
      case "G":
      case "H":
      case "J":
        abreviation = "QC";
        break;
      case "K":
      case "L":
      case "M":
      case "P":
        abreviation = "ON";
        break;
      case "R":
        abreviation = "MB";
        break;
      case "S":
        abreviation = "SK";
        break;
      case "T":
        abreviation = "AB";
        break;
      case "V":
        abreviation = "BC";
        break;
      case "X":
        //            	Both Nunavut and Northwest Terrtories codes start with X
        String msg = "Province abreviation cannot be determined from postal code: " + postalCode;
        LOG.error(msg);
        throw new ServiceException(msg);
      case "Y":
        abreviation = "YT";
        break;
      default:
        String message =
            "Province abreviation cannot be determined from postal code: " + postalCode;
        LOG.error(message);
        throw new ServiceException(message);
    }

    return abreviation;
  }
}
