/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.dynacare.eorder;

import javax.annotation.Nullable;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.util.PathUtils;
import org.oscarehr.util.SpringUtils;

@Getter
@Slf4j
public enum DynacareOrderPreferences {

  DYNACARE_ORDER_URL (ConfigureConstants.DYNACARE_ORDER_URL, true),
  DYNACARE_API_KEY (ConfigureConstants.DYNACARE_API_KEY, true),
  DYNACARE_CLINIC_EMAIL (ConfigureConstants.DYNACARE_CLINIC_EMAIL, false),
  DYNACARE_INSTANCE_ID (ConfigureConstants.DYNACARE_INSTANCE_ID, false);

  private final String name;
  private final boolean validate;

  private static final SystemPreferencesDao systemPreferencesDao =
      SpringUtils.getBean(SystemPreferencesDao.class);

  DynacareOrderPreferences(final String name, final boolean validate) {
    this.name = name;
    this.validate = validate;
  }

  public String getPreferenceValue() {
    val preference = systemPreferencesDao.findPreferenceByName(this.name);
    return preference != null
        ? preference.getValue()
        : "";
  }

  public static String getOrderUrl() {
    return PathUtils.addTrailingSlash(
        DynacareOrderPreferences.DYNACARE_ORDER_URL.getPreferenceValue());
  }

  public static String getApiKey() {
    return DynacareOrderPreferences.DYNACARE_API_KEY.getPreferenceValue();
  }

  public static String getInstanceId() {
    val instanceId = DynacareOrderPreferences.DYNACARE_INSTANCE_ID.getPreferenceValue();
    if (StringUtils.isNotBlank(instanceId)) {
      return instanceId;
    }
    log.warn("NO INSTANCE ID DEFINED USING DEFAULT");
    return PropertyDefaultConstants.DEFAULT_INSTANCE_ID;
  }

  @Nullable
  public static String getToEmail() {
    val toEmail = DynacareOrderPreferences.DYNACARE_CLINIC_EMAIL.getPreferenceValue();
    if (StringUtils.isNotBlank(toEmail)) {
      return toEmail;
    }
    log.warn(DynacareOrderPreferences.DYNACARE_CLINIC_EMAIL.getName()
        + " system preference is not defined.");
    return null;
  }
}
