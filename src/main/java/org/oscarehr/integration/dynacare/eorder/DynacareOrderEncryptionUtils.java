/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.dynacare.eorder;

import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.annotation.Nullable;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.oscarehr.util.EncryptionUtils;

@Slf4j
public class DynacareOrderEncryptionUtils {

  private static final String PASSWORD_STRING = "ub8Rn6E7xrF3Ia8J";
  private static final char SPLIT = 'i';
  private static final char JOIN = 'K';

  @Nullable
  public static String encrypt(final String plaintext) {
    try {
      return plaintext != null
          ? encrypt(plaintext, PASSWORD_STRING, SPLIT, JOIN)
          : null;
    } catch (GeneralSecurityException e) {
      log.error(ExceptionUtils.getStackTrace(e));
      return null;
    }
  }

  @Nullable
  public static String decrypt(final String encrypted) {
    try {
      return encrypted != null
          ? decrypt(encrypted, PASSWORD_STRING, SPLIT, JOIN)
          : null;
    } catch (GeneralSecurityException e) {
      log.error(ExceptionUtils.getStackTrace(e));
      return null;
    }
  }

  private static String encrypt(
      final String plaintext,
      final String passwordString,
      final char split,
      final char join
  ) throws IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException,
      NoSuchAlgorithmException, InvalidKeyException
  {
    return Base64.encodeBase64String(EncryptionUtils.encrypt(
        generateEncryptionKey(passwordString, split, join),
        plaintext.getBytes()
    ));
  }

  private static String decrypt(
      final String encrypted,
      final String passwordString,
      final char split,
      final char join
  ) throws NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException,
      BadPaddingException, InvalidKeyException {
    return new String(EncryptionUtils.decrypt(
        generateEncryptionKey(passwordString, split, join),
        Base64.decodeBase64(encrypted)
    ));
  }

  private static SecretKeySpec generateEncryptionKey(
      final String pass,
      final char split,
      final char join
  ) {
    return EncryptionUtils.generateEncryptionKey(generateSeed(pass, split, join));
  }

  private static String generateSeed(final String pass, final char split, final char join) {
    val tokens = StringUtils.split(pass, split);
    Arrays.sort(tokens);
    return StringUtils.join(tokens, join);
  }
}
