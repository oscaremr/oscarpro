/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.dynacare.eorder.converter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MetaData {

	private String metaKey;
	private String metaValue;
	private String tableName;

	public MetaData() {
		this.metaKey = "OrderType";
		this.metaValue = "EMR";
		this.tableName = "Order";
	}

	public MetaData(final String metaKey, final String metaValue, final String tableName) {
		this.metaKey = metaKey;
		this.metaValue = metaValue;
		this.tableName = tableName;
	}

}
