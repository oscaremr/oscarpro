/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.dynacare.eorder;

import java.util.List;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import com.dynacare.api.Order;

public interface DynacareOrderGateway {

  Order createOrder(EFormData eformData, List<EFormValue> eformValues);

  Order updateOrder(EFormData eformData, List<EFormValue> eformValues);

  Order getASingleOrder(String orderId);
}
