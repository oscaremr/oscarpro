/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.Map;

import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Location;
import org.oscarehr.common.model.EFormValue;

public class LocationConverter {

  public static Location toFhirObject(Map<String, EFormValue> valueMap) {
    Location location = new Location();
    Identifier identifier = new Identifier();
    identifier.setValue("501");
    location.addIdentifier(identifier);
    return location;
  }

}
