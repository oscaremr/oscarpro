/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.Resource;
import org.hl7.fhir.r4.model.ValueSet;
import org.oscarehr.integration.eOrder.common.ServiceException;

public interface ExcellerisEOrder {


  /**
   * Retrieves the AuthenticationToken from session using the HTTP Servlet Request parameter to get
   * access to the session
   *
   * @param request - authentication tokens are cached in session
   * @return AuthenticationToken object
   */
  AuthenticationToken getAuthenticationToken(HttpServletRequest request);


  /**
   * Authenticates to the Excelleris eOrder Login API, passing in user, password and province
   * in the body of the request
   *
   * @return AuthenticationToken object
   */
  AuthenticationToken authenticate() throws ServiceException;


  /**
   * Returns a list of properties associated with each search hit that was obtained for the provided
   * family name.
   *
   * @param authToken  - Token to use when calling Excelleris eOrder APIs
   * @param givenName  - Given name of the practitioner
   * @param familyName - Family name of the practitioner
   * @return list of practitioner objects
   */
  List<Resource> searchPractitioners(AuthenticationToken authToken, String givenName, String familyName);


  /**
   * Returns a practitioner identified by id
   *
   * @param authToken      authentication token.
   * @param practitionerId Excelleris id of practitioner
   * @return practitioner object
   */
  Practitioner getPractitioner(AuthenticationToken authToken, String practitionerId);


  /**
   * Returns a practitioner identified by id
   *
   * @param authToken           authentication token.
   * @param practitionerId      Excelleris id of practitioner
   * @param isHexRepresentation - True if hex value of excelleris id, false if excelleris value only
   * @return practitioner object
   */
  Practitioner getPractitioner(AuthenticationToken authToken, String practitionerId, boolean isHexRepresentation);


  /**
   * Retrieves patient known to excelleris by id
   *
   * @param authToken auth token used to retrieve patient information
   * @param patientId Excelleris patient id
   * @return Patient structure
   */
  Patient getPatient(AuthenticationToken authToken, String patientId);

  /**
   * Returns the most recent list of test codes
   *
   * @param authToken Auth token to use to retrieve test codes
   * @return list of test objects
   */
  List<ValueSet.ConceptReferenceComponent> retrieveTestCodes(AuthenticationToken authToken);


  /**
   * Validate eOrder
   *
   * @param authToken Auth token to use to perform validation
   * @param bundle    FHIR bundle to validate
   * @return Structure to provide results of validation
   */
  Bundle validatetExcellerisEOrder(AuthenticationToken authToken, Bundle bundle);


  /**
   * Submit eOrder
   *
   * @param authToken Auth token to use for submission
   * @param bundle    FHIR bundle to submit
   * @return Bundle structure to provide results of submission ; may be success, failure or a Questionnaire that needs to be answered
   */
  Bundle submitExcellerisEOrder(AuthenticationToken authToken, Bundle bundle);
}
