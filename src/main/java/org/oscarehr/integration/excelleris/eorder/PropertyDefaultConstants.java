/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;

public class PropertyDefaultConstants {
  public static final String EXCELLERIS_EORDER_BASE_URL_PARAM_NAME = "https://launchpad-on-uat-external.dev.excelleris.com/eOrderREST";
  public static final String DEFAULT_EXCELLERIS_EORDER_KEYSTORE_FILE = "/ExcellerisEorderKeyStore.pkcs12";
  public static final String DEFAULT_EXCELLERIS_EORDER_KEYSTORE_PASSWORD = "PathCert";
  public static final String DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_FILE = "/ExcellerisEorderTrustStore.pkcs12";
  public static final String DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_PASSWORD = "PathCert";
  public static final String DEFAULT_EXCELLERIS_EORDER_API_VERSION = "2.0";
  public static final int DEFAULT_EXCELLERIS_EORDER_CONNETION_TIMEOUT_MILLISECONDS = 60000;

  public static final String DEFAULT_EXCELLERIS_EORDER_V2_CLIENT_SSL_ENABLE = "False";
  public static final String DEFAULT_EXCELLERIS_EORDER_V2_BASE_URL = "https://ca-c-exc-eorder-api-qa.azurewebsites.net/api";
  public static final String DEFAULT_EXCELLERIS_EORDER_V2_LOGIN_URL = "https://ca-c-exc-eorder-api-qa.azurewebsites.net/api/Login";
  public static final String DEFAULT_EXCELLERIS_EORDER_V2_USER = "eordermoa1";
  public static final String DEFAULT_EXCELLERIS_EORDER_V2_USER_PASSWORD = "Password1";
  public static final String DEFAULT_EXCELLERIS_EORDER_V2_USER_PROVINCE = "on";

  public static final String DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_ENABLE = "FALSE";
  public static final String DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_HOST = "127.0.0.1";
  public static final String DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_PORT = "8888";

  public static final String DEFAULT_EXCELLERIS_EORDER_V2_INSTANCE_AFFINITY_DISABLE = "False";
}
