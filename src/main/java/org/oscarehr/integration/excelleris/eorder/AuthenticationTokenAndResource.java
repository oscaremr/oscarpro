/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;

import org.hl7.fhir.r4.model.Resource;

public class AuthenticationTokenAndResource {

  private AuthenticationToken authenticationToken;
  private Resource resource;

  public AuthenticationTokenAndResource() {
    super();
  }

  public AuthenticationTokenAndResource(AuthenticationToken authenticationToken, Resource resource) {
    super();
    this.authenticationToken = authenticationToken;
    this.resource = resource;
  }

  public AuthenticationToken getAuthenticationToken() {
    return authenticationToken;
  }

  public void setAuthenticationToken(AuthenticationToken authenticationToken) {
    this.authenticationToken = authenticationToken;
  }

  public Resource getResource() {
    return resource;
  }

  public void setResource(Resource resource) {
    this.resource = resource;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((authenticationToken == null) ? 0 : authenticationToken.hashCode());
    result = prime * result + ((resource == null) ? 0 : resource.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    AuthenticationTokenAndResource other = (AuthenticationTokenAndResource) obj;
    if (authenticationToken == null) {
      if (other.authenticationToken != null) return false;
    } else if (!authenticationToken.equals(other.authenticationToken)) return false;
    if (resource == null) {
      if (other.resource != null) return false;
    } else if (!resource.equals(other.resource)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "AuthenticationTokenAndResource [authenticationToken=" + authenticationToken + ", resource =" + resource + "]";
  }
}
