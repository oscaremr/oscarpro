/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.oscarehr.common.dao.OrderLabTestCodeCacheDao;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.api.Question;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialBusinessLogic;
import org.oscarehr.util.MiscUtils;

public class QuestionnaireConverter {

  private static final Logger LOG = MiscUtils.getLogger();

  public static org.oscarehr.integration.excelleris.eorder.api.Questionnaire toApiObject(final org.hl7.fhir.r4.model.Questionnaire fhirQuestionnaire,
                                                                                         final ProvincialBusinessLogic provincial,
                                                                                         final OrderLabTestCodeCacheDao orderLabTestCodeCacheDao) {

    if (fhirQuestionnaire == null) {
      LOG.error("Fhir Questionnaire is null");
      throw new ServiceException("Fhir Questionnaire is Null");
    }

    if (provincial == null || provincial.getValueMap() == null) {
      LOG.error("Null value map");
      throw new ServiceException("Cannot convert Fhir Questionnaire");
    }

    final org.oscarehr.integration.excelleris.eorder.api.Questionnaire apiQuestionnaire
        = new org.oscarehr.integration.excelleris.eorder.api.Questionnaire();

    final ArrayList<Question> questions = new ArrayList<Question>();
    for (final org.hl7.fhir.r4.model.Questionnaire.QuestionnaireItemComponent item : fhirQuestionnaire.getItem()) {
      questions.add(QuestionConverter.toApiObject(item, provincial, orderLabTestCodeCacheDao));
    }

    apiQuestionnaire.setQuestions(questions);

    return apiQuestionnaire;
  }
}
