/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.testcodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.ValueSet;
import org.oscarehr.common.dao.OrderLabTestCodeCacheDao;
import org.oscarehr.common.model.OrderLabTestCodeCache;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;

public class LabCodesHelper {

  /**
   * Static helper method to get all of the existing records from the OrderLabTestCodeCache table
   *
   * @return List<OrderLabTestCodeCache>
   */
  public static List<OrderLabTestCodeCache> getExistingTestCodes() {
    final OrderLabTestCodeCacheDao orderLabTestCodeCacheDao = SpringUtils.getBean(org.oscarehr.common.dao.OrderLabTestCodeCacheDao.class);
    final List<OrderLabTestCodeCache> existingTestCodeList = orderLabTestCodeCacheDao.findAll(null, null);
    Collections.sort(existingTestCodeList);

    return existingTestCodeList;
  }
   public static String getTestCodes(String keyName) {
      final OrderLabTestCodeCacheDao orderLabTestCodeCacheDao = SpringUtils.getBean(org.oscarehr.common.dao.OrderLabTestCodeCacheDao.class);
      final OrderLabTestCodeCache orderLabTestCodeCache = orderLabTestCodeCacheDao.findByKeyName(keyName);
      return orderLabTestCodeCache != null ? orderLabTestCodeCache.getLabTestCode() : "<unknown>";
   }

  public static void refreshTestCodes(List<ValueSet.ConceptReferenceComponent> testCodeList) {
    final List<OrderLabTestCodeCache> updatedTestCodes = LabCodesHelper.retrieveUpdatedTestCodes(testCodeList);
    final List<OrderLabTestCodeCache> existingTestCodes = LabCodesHelper.getExistingTestCodes();
    System.out.println("got lists");

    final boolean hasTestCodesChanged = hasOrderLabTestCodeCacheChanged(existingTestCodes, updatedTestCodes);
    if (hasTestCodesChanged) {
      LabCodesHelper.replaceLabCodes(updatedTestCodes);
    }
  }


  /**
   * Static helper method to get all of the most recent test codes from the ValueSet structure recently pulled
   * from the eOrder /Metadata/ValueSet/ordercode?version=2.0 endpoint
   *
   * @return List<OrderLabTestCodeCache>
   */
  public static List<OrderLabTestCodeCache> retrieveUpdatedTestCodes(final List<ValueSet.ConceptReferenceComponent> testCodeList) {
    final List<OrderLabTestCodeCache> currentTestCodeList = new ArrayList<>();
    final Set<String> keyNameSet = new HashSet<>();

    for (final ValueSet.ConceptReferenceComponent component : testCodeList) {
      final OrderLabTestCodeCache item = new OrderLabTestCodeCache();
      if (component.getCode() != null && component.getDisplay() != null) {
        item.setUpdateable(true);
        item.setLabTestCode(component.getCode());
        final String displayValue = getTestDisplayValue(component);

        item.setTestName(displayValue);

        // Derive a unique key name
        //
        // TODO: EORDER - Below is the code from BC/Dynacare eOrder changes on the keyName assignment:
        String keyName = convertKey(displayValue); 
        while (keyNameSet.contains(keyName)) {
          keyName = updateKeyName(keyName);
        }
        keyNameSet.add(keyName);
        item.setKeyName(keyName);
        String province =  OscarProperties.getInstance().getProperty("EXCELLERIS_EORDER_V2_USER_PROVINCE");
        if (province != null) {
          item.setProvince(province.toUpperCase());
        }

        for (final Extension extension : component.getExtension()) {
          if (extension.getUrl().contains("is-hardcoded")) {
            boolean isHardCode = Boolean.valueOf(extension.getValue().primitiveValue());
            item.setTestOnForm(isHardCode);
          }
          if (extension.getUrl().contains("searchable")) {
            boolean searchable = Boolean.valueOf(extension.getValue().primitiveValue());
            item.setSearchable(searchable);
          }
          if (extension.getUrl().contains("patient-pay")) {
            boolean patientPay = Boolean.valueOf(extension.getValue().primitiveValue());
            item.setPatientPay(patientPay);
          }
          if (extension.getUrl().contains("appointment-required")) {
            boolean requireAppointment = Boolean.valueOf(extension.getValue().primitiveValue());
            item.setRequireAppointment(requireAppointment);
          }
        }

        currentTestCodeList.add(item);
      }
    }
    Collections.sort(currentTestCodeList);

    return currentTestCodeList;
  }

  /**
   * Static helper method to replace all records contained in the OrderLabTestCodeCache table
   *
   * @return List<OrderLabTestCodeCache>
   */
  public static void replaceLabCodes(final List<OrderLabTestCodeCache> updatedTestCodes) {
    final OrderLabTestCodeCacheDao orderLabTestCodeCacheDao = SpringUtils.getBean(OrderLabTestCodeCacheDao.class);
    orderLabTestCodeCacheDao.removeAllTestCodes();
    for (final OrderLabTestCodeCache cache : updatedTestCodes) {
      orderLabTestCodeCacheDao.persist(cache);
    }
  }


  /**
   * Static helper method to check if it is necessary to update the OrderLabTestCodeCache table
   *
   * @return List<OrderLabTestCodeCache>
   */
  public static boolean hasOrderLabTestCodeCacheChanged(final List<OrderLabTestCodeCache> existingTestCodes, final List<OrderLabTestCodeCache> updatedTestCodes) {

    if (existingTestCodes.size() != updatedTestCodes.size()) {
      return true;
    }

    for (int i = 0; i < existingTestCodes.size(); i++) {
      if (existingTestCodes.get(i).hashCode() != updatedTestCodes.get(i).hashCode()) {
        return true;
      }
    }

    return false;
  }

  // TODO: EORDER - convertKey and sanatizeDisplayValue are added by BC/Dynacare changes,
  //  update/remove if necessary to bring in line with new ON functionality
  //  getTestDisplayValue is added by BC/Dynacare, ensure it's needed with the new workflow
  /**
   * Creates a key name with the following properties:
   * <p>
   * <key name> = T_<name> where
   * - <name> = <name> with space replaced with _
   * - truncate <name> to 200 characters long (if necessary)
   *
   * @param name the name to modify
   * @return key name the processed name
   */
  public static String convertKey(String name) {
    String result = null;

    if (name != null) {
      result = name;
      result = result.replaceAll(" ", "_");
      result = result.replaceAll("-", "_");
      result = result.replaceAll("/", "_");
      result = result.replaceAll(Pattern.quote("+"), "_");
      result = result.replaceAll(Pattern.quote("&"), "_");
      result = result.replaceAll(Pattern.quote("'"), "_");
      result = result.replaceAll(Pattern.quote("."), "_");
      result = result.replaceAll(Pattern.quote(","), "_");
      result = result.replaceAll(Pattern.quote("("), "_");
      result = result.replaceAll(Pattern.quote(")"), "_");
      result = result.replaceAll(Pattern.quote("["), "_");
      result = result.replaceAll(Pattern.quote("]"), "_");

      result = "T_" + result.toLowerCase();
      result = StringUtils.left(result, 200);
    }
    return result;
  }



  /**
   * Cleans up display value by quoting special characters
   *
   * @param name the name to modify
   * @return sanitized display value
   */
  private static String sanatizeDisplayValue(final String name) {
    String result = null;

    if (name != null) {
      result = name;
      result = result.replaceAll(Pattern.quote("&"), " and ");
      result = result.replaceAll(Pattern.quote("'"), "");
    }

    return result;
  }


   /**
    * Replace the last three characters with 3-digit numeric value
    *
    * @param name the key name to update
    * @return the modified key name
    */
   private static String updateKeyName(String name) {
     if ((name != null) && (name.length() > 3)) {
         String end = name.substring(name.length() - 3);

       int id = 1;
       try {
         id = Integer.parseInt(end);
         id++;
       } catch (Exception ex) {
       }
       end = String.format("%03d", id);
       name = name.substring(0, name.length() - 3) + end;
     }
     return name;
   }


  private static String getTestDisplayValue(final ValueSet.ConceptReferenceComponent component) {
    String displayValue = component.getDisplay();

    String designationValue = null;
    final List<ValueSet.ConceptReferenceDesignationComponent> designationList = component.getDesignation();
    if (designationList != null && designationList.isEmpty() == false) {
      final ValueSet.ConceptReferenceDesignationComponent designationComponent = designationList.get(0);
      designationValue = designationComponent.getValue();
    }

    if (displayValue != null && designationValue != null) {

      displayValue = displayValue.matches("[a-z]*") == false ? displayValue : designationValue;

    } else  {
      return displayValue != null ? displayValue : designationValue;
    }

    return displayValue;
  }
}
