package org.oscarehr.integration.excelleris.eorder.api;

public enum QuestionType {

  BOOLEAN, DECIMAL, INTEGER, DATE, DATETIME, INSTANT, TIME, STRING, TEXT, QUANTITY, CHOICE, OPENCHOICE
}
