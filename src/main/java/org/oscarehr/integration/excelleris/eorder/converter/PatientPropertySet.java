/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

public class PatientPropertySet {

  // The names of the following properties are from apconfig.xml, we must use the exact name
  // on the Lab Requisition form in order to have the fields populated using data from demographic related tables.
  public static final String PATIENT_FIRST_NAME = "patient_nameF";
  public static final String PATIENT_LAST_NAME = "patient_nameL";
  public static final String PATIENT_MIDDLE_NAME = "patient_nameM";
  public static final String PATIENT_GENDER_MALE = "Male";
  public static final String PATIENT_GENDER_FEMALE = "Female";
  public static final String PATIENT_DOB = "dobc2";
  public static final String PATIENT_PHONE = "phone";
  public static final String PATIENT_SECONDARY_PHONE = "phone2";
  public static final String PATIENT_CELL_PHONE = "cell";
  public static final String EXCELLERIS_PATIENT_ID = "excelleris_patient_id";
  public static final String PATIENT_HEALTH_INSURANCE_NUMBER = "hin";
  public static final String PATIENT_HEALTH_INSURANCE_NUMBER_VERSION = "hinversion";
  public static final String PATIENT_ADDRESS_PROVINCE = "province";
  public static final String PATIENT_ADDRESS_CITY = "city";
  public static final String PATIENT_ADDRESS_POSTAL_CODE = "postal";
  public static final String PATIENT_ADDRESS_LINE = "address_line";
  public static final String PATIENT_TITLE = "patient_title";
  public static final String PATIENT_GENDER = "PatientGender";
}
