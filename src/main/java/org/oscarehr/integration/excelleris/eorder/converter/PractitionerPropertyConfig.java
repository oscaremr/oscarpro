/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

public class PractitionerPropertyConfig {

  public static final Integer MAXIMUM_NUM_COPY_TO_PROVIDERS = 5;

  private String firstNamePropertyName;
  private String lastNamePropertyName;
  private String excellerisIdPropertyName;
  private String lifeLabsIdPropertyName;
  private String titlePropertyName;

  public PractitionerPropertyConfig() {
    super();
  }

  public PractitionerPropertyConfig(
      String firstNamePropertyName, String lastNamePropertyName, String excellerisIdPropertyName, String lifeLabsIdPropertyName, String titlePropertyName) {
    super();
    this.firstNamePropertyName = firstNamePropertyName;
    this.lastNamePropertyName = lastNamePropertyName;
    this.excellerisIdPropertyName = excellerisIdPropertyName;
    this.lifeLabsIdPropertyName = lifeLabsIdPropertyName;
    this.titlePropertyName = titlePropertyName;
  }

  public String getFirstNamePropertyName() {
    return firstNamePropertyName;
  }

  public void setFirstNamePropertyName(String firstNamePropertyName) {
    this.firstNamePropertyName = firstNamePropertyName;
  }

  public String getLastNamePropertyName() {
    return lastNamePropertyName;
  }

  public void setLastNamePropertyName(String lastNamePropertyName) {
    this.lastNamePropertyName = lastNamePropertyName;
  }

  public String getExcellerisIdPropertyName() {
    return excellerisIdPropertyName;
  }

  public void setExcellerisIdPropertyName(String excellerisIdPropertyName) {
    this.excellerisIdPropertyName = excellerisIdPropertyName;
  }

  public String getLifeLabsIdPropertyName() {
    return lifeLabsIdPropertyName;
  }

  public void setLifeLabsIdPropertyName(String lifeLabsIdPropertyName) {
    this.lifeLabsIdPropertyName = lifeLabsIdPropertyName;
  }

  public String getTitlePropertyName() {
    return titlePropertyName;
  }

  public void setTitlePropertyName(String titlePropertyName) {
    this.titlePropertyName = titlePropertyName;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((excellerisIdPropertyName == null) ? 0 : excellerisIdPropertyName.hashCode());
    result = prime * result + ((firstNamePropertyName == null) ? 0 : firstNamePropertyName.hashCode());
    result = prime * result + ((lastNamePropertyName == null) ? 0 : lastNamePropertyName.hashCode());
    result = prime * result + ((lifeLabsIdPropertyName == null) ? 0 : lifeLabsIdPropertyName.hashCode());
    result = prime * result + ((titlePropertyName == null) ? 0 : titlePropertyName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    PractitionerPropertyConfig other = (PractitionerPropertyConfig) obj;
    if (excellerisIdPropertyName == null) {
      if (other.excellerisIdPropertyName != null) return false;
    } else if (!excellerisIdPropertyName.equals(other.excellerisIdPropertyName)) return false;
    if (firstNamePropertyName == null) {
      if (other.firstNamePropertyName != null) return false;
    } else if (!firstNamePropertyName.equals(other.firstNamePropertyName)) return false;
    if (lastNamePropertyName == null) {
      if (other.lastNamePropertyName != null) return false;
    } else if (!lastNamePropertyName.equals(other.lastNamePropertyName)) return false;
    if (lifeLabsIdPropertyName == null) {
      if (other.lifeLabsIdPropertyName != null) return false;
    } else if (!lifeLabsIdPropertyName.equals(other.lifeLabsIdPropertyName)) return false;
    if (titlePropertyName == null) {
      if (other.titlePropertyName != null) return false;
    } else if (!titlePropertyName.equals(other.titlePropertyName)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "PractitionerPropertyConfig [firstNamePropertyName=" + firstNamePropertyName + ", lastNamePropertyName=" + lastNamePropertyName + ", excellerisIdPropertyName=" + excellerisIdPropertyName + ", lifeLabsIdPropertyName=" + lifeLabsIdPropertyName + ", titlePropertyName=" + titlePropertyName + "]";
  }
}
