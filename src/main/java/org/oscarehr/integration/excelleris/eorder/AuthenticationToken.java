/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.apache.http.cookie.Cookie;

public class AuthenticationToken implements Serializable {

  private static final long serialVersionUID = 1L;

  /**
   * Creates authentication token with expire in seconds
   *
   * @param token            authentication token
   * @param expiresInSeconds number of seconds the token is valid for
   */
  public AuthenticationToken(final String token, final long expiresInSeconds) {

    this.m_token = token;
    this.m_createdTime = System.currentTimeMillis();

    // We consider this token to be expired 5 seconds sooner than necessary to
    // avoid race conditions.  It takes time to create this class after successfully getting
    // a token, and it takes time to use the token after it is checked to see if it
    // expired or not.  A 5 second fudge factor is used to avoid cases where we check expiry,
    // but then by the time we use it, the token has expired
    //
    this.m_expiryTime = this.m_createdTime + ((expiresInSeconds - 5) * 1000);
  }


  /**
   * Gets the authentication token
   *
   * @return authentication token
   */
  public String getToken() {
    return m_token;
  }

  /**
   * Returns a boolean indicating if the token has expired or not
   *
   * @return if token has expired or not
   */

  public boolean hasExpired() {
    long currentTime = System.currentTimeMillis();
    return currentTime > m_expiryTime;
  }

  /**
   * Write the content of the authentication token structure to a base64 serialized string
   *
   * @param authToken Token to serialize
   * @return Base64 serialized string of the authentication token
   */
  public static String serialized(final AuthenticationToken authToken) {
    String serializeValue = null;
    try {
      final ByteArrayOutputStream baos = new ByteArrayOutputStream();
      final ObjectOutputStream oos = new ObjectOutputStream(baos);
      oos.writeObject(authToken);
      oos.close();

      serializeValue = Base64.getEncoder().encodeToString(baos.toByteArray());
    } catch (final IOException ex) {
      serializeValue = null;
    }

    return serializeValue;
  }

  /**
   * Creates the java object from a base64 representation (deserialization)
   *
   * @param base64 base64 serialized form of authentication token structure
   * @return AuthenticationToken structure
   */
  public static AuthenticationToken deserialize(final String base64) {

    AuthenticationToken authToken = null;
    try {
      final byte[] data = Base64.getDecoder().decode(base64);
      ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
      authToken = (AuthenticationToken) ois.readObject();
      ois.close();

    } catch (Exception e) {
      e.printStackTrace();
    }

    return authToken;
  }

  /**
   * Adds a cookie to the auth token structure
   * NOTE: Likely to be only Azure cookies to enable sticky sessions
   *
   * @param cookie
   */
  public void addCookie(final Cookie cookie) {
    m_cookieList.add(cookie);
  }

  /**
   * Get list of cookies that have been saved to this class.
   * NOTE: Likely to be only Azure cookies to enable sticky sessions
   *
   * @return List of cookies
   */
  public List<Cookie> getCookieList() {
    return m_cookieList;
  }

  @Override
  public String toString() {
    return "AuthToken [token=" + m_token + ", expiryTime" + m_expiryTime + "]";
  }

  private String m_token;
  private long m_expiryTime;
  private long m_createdTime;
  private List<Cookie> m_cookieList = new ArrayList<>();
}
