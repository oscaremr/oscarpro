/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;

import java.util.List;

import org.hl7.fhir.r4.model.Resource;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.eOrder.common.ServiceException;

public interface OrderService {

  /**
   * Submit a diagnostic order in Excelleris eOrder system
   *
   * @param eformData eform data
   * @return newly created diagnostic order
   */
  Resource createOrder(AuthenticationToken authenticationToken,
                       EFormData eformData,
                       List<EFormValue> eformValues) throws ServiceException;

}
