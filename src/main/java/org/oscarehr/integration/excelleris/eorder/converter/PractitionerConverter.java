/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hl7.fhir.r4.model.Address;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.PractitionerRole;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Resource;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.util.MiscUtils;

public class PractitionerConverter {

  public static final String PRACTITIONER_TYPE = "Practitioner";
  public static final String PRACTITIONER_ROLE_TYPE = "PractitionerRole";
  public static final String ROVER_ASSIGNING_AUTHORITY = "2.16.840.1.113883.3.1772.1.0";
  private static final String ORGANIZATION_ROVER = "Rover";
  private static final String ORGANIZATION_ONTLL = "Ontario Lifelabs";
  private static final String ORGANIZATION_ONT_PROV_GOVNT = "Ontario provincial government";

  private static final String ROVER_ID = "rover_id";
  private static final String ONTARIO_LIFELABS_ID = "ontario_lifelabs_id";
  private static final String ONTARIO_PROVINCIAL_GOVERNMENT_ID = "on_provincial_govt_id";

  private static final Logger LOG = MiscUtils.getLogger();


  public static Properties toProperties(final Practitioner practitioner) {
    final Properties practitionerMetadata = new Properties();

    String practitionerId = practitioner.getId();
    final List<Identifier> identifierList = practitioner.getIdentifier();
    for (final Identifier identifier : identifierList) {
      if (ROVER_ASSIGNING_AUTHORITY.equals(identifier.getSystem())) {
        practitionerId = identifier.getValue();
        break;
      }
    }

    practitionerMetadata.put("excellerisId", practitionerId);

    setPlaceholder(practitionerMetadata);
    setName(practitionerMetadata, practitioner);
    setAddress(practitionerMetadata, practitioner);
    setOrganization(practitionerMetadata, practitioner);

    return practitionerMetadata;
  }

  public static Properties toProperties(final PractitionerRole practitionerRole) {
    final Properties practitionerRoleMetadata = new Properties();

    practitionerRoleMetadata.setProperty("specialty", "");
    if (practitionerRole.getSpecialty().isEmpty() == false && practitionerRole.getSpecialty().get(0).getCoding().isEmpty() == false) {
      practitionerRoleMetadata.setProperty("specialty", practitionerRole.getSpecialty().get(0).getCoding().get(0).getCode());
    }

    return practitionerRoleMetadata;
  }

  public static List<Properties> toProperties(final List<Resource> practitionerList) {
    final Map<String, Properties> practitionerMap = new HashMap<>();

    Properties practitionerProperties = null;
    for (int i = 0; i < practitionerList.size(); i++) {
      Resource resource = practitionerList.get(i);
      Practitioner practitioner = null;
      PractitionerRole practitionerRole = null;

      if (resource.getResourceType().toString().equals(PRACTITIONER_TYPE)) {
        practitioner = (Practitioner) resource;
        if (practitioner.hasActive()) {
          practitionerMap.put(practitioner.getId(), PractitionerConverter.toProperties(practitioner));
        }

      } else if (resource.getResourceType().toString().equals(PRACTITIONER_ROLE_TYPE)) {
        practitionerRole = (PractitionerRole) resource;
        practitionerProperties = practitionerMap.get(practitionerRole.getPractitioner().getId());
        if (practitionerProperties != null) {
          practitionerProperties.putAll(PractitionerConverter.toProperties(practitionerRole));
        }
      }
    }

    return new ArrayList<Properties>(practitionerMap.values());
  }


  public static Practitioner toFhirObject(Map<String, EFormValue> valueMap) {

    Practitioner practitioner = new Practitioner();

    // Id
    String excellerisId = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
    if (excellerisId == null) {
      LOG.error(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID + " is missing.");
      throw new ServiceException(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID + " is required.");
    }
    practitioner.setId(excellerisId);

    // Identifier
    // rover id
    Identifier excellerisIdIdentifier = FhirResourceHelper.createIdentifier(
        ResourceUrlHelper.getFhirNameSpaceUrl(), OidConstants.PRACTITIONER_EXCELLERIS_ID_OID, "PRN", excellerisId, "Rover");
    LOG.debug("excellerisId : " + excellerisId);
    practitioner.addIdentifier(excellerisIdIdentifier);

    // LifeLabs id
    String lifeLabsId = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.ORDERING_PROVIDER_LIFELAB_ID);
    if (lifeLabsId == null) {
      LOG.error(PractitionerPropertySet.ORDERING_PROVIDER_LIFELAB_ID + " is missing.");
      throw new ServiceException(PractitionerPropertySet.ORDERING_PROVIDER_LIFELAB_ID + " is required.");
    }
    LOG.debug("lifeLabsId : " + lifeLabsId);
    Identifier lifelabsIdIdentifier = FhirResourceHelper.createIdentifier(
        ResourceUrlHelper.getFhirNameSpaceUrl(), OidConstants.PRACTITIONER_LIFE_LABS_ID_OID, "DN", lifeLabsId, "Ontario Lifelabs");
    practitioner.addIdentifier(lifelabsIdIdentifier);

    // Name
    String lastName = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_LAST_NAME);
    if (lastName == null) {
      LOG.error(PractitionerPropertySet.PRACTITIONER_LAST_NAME + " is missing.");
      throw new ServiceException(PractitionerPropertySet.PRACTITIONER_LAST_NAME + " is required.");
    }
    String firstName = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_FIRST_NAME);
    if (firstName == null) {
      LOG.error(PractitionerPropertySet.PRACTITIONER_FIRST_NAME + " is missing.");
      throw new ServiceException(PractitionerPropertySet.PRACTITIONER_FIRST_NAME + " is required.");
    }

    String middleName = null;

    String title = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.PRACTITIONER_TITLE);
    if (title == null) {
      LOG.warn(PractitionerPropertySet.PRACTITIONER_TITLE + " is missing. Default to DR.");
      title = "DR.";
    }

    HumanName name = FhirResourceHelper.createHumanName(firstName, lastName, middleName, title);
    List<HumanName> nameList = new ArrayList<HumanName>();
    nameList.add(name);
    practitioner.setName(nameList);

    // Telecom (optional)
    // Address (optional)
    // Role (optional)

    return practitioner;
  }

  public static Practitioner toFhirObject(Map<String, EFormValue> valueMap, Integer unknownProviderIndex, PractitionerPropertyConfig propertyConfig) {

    Practitioner practitioner = new Practitioner();

    // Id
    String excellerisId = EFormValueHelper.getValue(valueMap, propertyConfig.getExcellerisIdPropertyName());
    if (excellerisId == null && unknownProviderIndex == null) {
      LOG.error(propertyConfig.getExcellerisIdPropertyName() + " is missing.");
      throw new ServiceException(propertyConfig.getExcellerisIdPropertyName() + " is required.");
    }
    if (excellerisId != null) {
      practitioner.setId(excellerisId);
    } else {
      practitioner.setId(getUnknownProviderId(unknownProviderIndex));
    }

    // Identifier
    // rover id
    if (excellerisId != null) {
      Identifier excellerisIdIdentifier = FhirResourceHelper.createIdentifier(
          ResourceUrlHelper.getFhirNameSpaceUrl(), OidConstants.PRACTITIONER_EXCELLERIS_ID_OID, "PRN", excellerisId, "Rover");
      LOG.debug("excellerisId : " + excellerisId);
      practitioner.addIdentifier(excellerisIdIdentifier);
    }

    // LifeLabs id
    String lifeLabsId = EFormValueHelper.getValue(valueMap, propertyConfig.getLifeLabsIdPropertyName());
    if (lifeLabsId == null && unknownProviderIndex == null) {
      LOG.error(propertyConfig.getLifeLabsIdPropertyName() + " is missing.");
      throw new ServiceException(propertyConfig.getLifeLabsIdPropertyName() + " is required.");
    }
    LOG.debug("lifeLabsId : " + lifeLabsId);
    Identifier lifelabsIdIdentifier = null;
    if (lifeLabsId != null) {
      lifelabsIdIdentifier = FhirResourceHelper.createIdentifier(
          ResourceUrlHelper.getFhirNameSpaceUrl(), OidConstants.PRACTITIONER_LIFE_LABS_ID_OID, "DN", lifeLabsId, "Ontario Lifelabs");
    } else if (unknownProviderIndex != null) {
      lifelabsIdIdentifier = FhirResourceHelper.createIdentifier(
          ResourceUrlHelper.getFhirNameSpaceUrl(), OidConstants.PRACTITIONER_LIFE_LABS_ID_OID, "DN", getUknownProviderCode(unknownProviderIndex), "Ontario Lifelabs");
    }
    practitioner.addIdentifier(lifelabsIdIdentifier);

    // Name
    String lastName = EFormValueHelper.getValue(valueMap, propertyConfig.getLastNamePropertyName());
    if (lastName == null) {
      LOG.error(propertyConfig.getLastNamePropertyName() + " is missing.");
      throw new ServiceException(propertyConfig.getLastNamePropertyName() + " is required.");
    }
    String firstName = EFormValueHelper.getValue(valueMap, propertyConfig.getFirstNamePropertyName());
    if (firstName == null) {
      LOG.warn(propertyConfig.getFirstNamePropertyName() + " is missing. Using default value of UNK");
      firstName = "UNK";
    }

    String middleName = null;

    String title = EFormValueHelper.getValue(valueMap, propertyConfig.getTitlePropertyName());
    if (title == null) {
      LOG.warn(propertyConfig.getTitlePropertyName() + " is missing. Default to DR.");
      title = "DR.";
    }

    HumanName name = FhirResourceHelper.createHumanName(firstName, lastName, middleName, title);
    List<HumanName> nameList = new ArrayList<HumanName>();
    nameList.add(name);
    practitioner.setName(nameList);

    // Telecom (optional)
    // Address (optional)
    // Role (optional)

    return practitioner;
  }

  public static boolean hasCopyToProviderData(Map<String, EFormValue> valueMap, PractitionerPropertyConfig propertyConfig) {

    String excellerisId = EFormValueHelper.getValue(valueMap, propertyConfig.getExcellerisIdPropertyName());
    String lifeLabsId = EFormValueHelper.getValue(valueMap, propertyConfig.getLifeLabsIdPropertyName());
    String lastName = EFormValueHelper.getValue(valueMap, propertyConfig.getLastNamePropertyName());
    String firstName = EFormValueHelper.getValue(valueMap, propertyConfig.getFirstNamePropertyName());
    String title = EFormValueHelper.getValue(valueMap, propertyConfig.getTitlePropertyName());

    if (!StringUtils.isEmpty(excellerisId) || !StringUtils.isEmpty(lifeLabsId) ||
        !StringUtils.isEmpty(lastName) || !StringUtils.isEmpty(firstName) ||
        !StringUtils.isEmpty(title)) {
      return true;
    }

    return false;
  }

  public static String getUnknownProviderId(Integer unknownProviderIndex) {
    return Integer.toString(unknownProviderIndex * 10);
  }

  public static String getUknownProviderCode(Integer unknownProviderIndex) {
    return "UNKNOWN" + (unknownProviderIndex - 1);
  }


  private static void setPlaceholder(final Properties metadata) {
    metadata.put("ontario_lifelabs_id", "");
    metadata.put("rover_id", "");
    metadata.put("on_provincial_govt_id", "");
    metadata.put("pathnet_code", "");
  }

  private static void setName(final Properties metadata, final Practitioner practitioner) {
    if (practitioner.hasName()) {
      final List<HumanName> nameList = practitioner.getName();
      final Set<String> givenName = new HashSet<String>();
      final Set<String> familyName = new HashSet<String>();
      final Set<String> prefixName = new HashSet<String>();
      for (final HumanName humanName : nameList) {
        givenName.add(humanName.getGivenAsSingleString());
        familyName.add(humanName.getFamily());
        prefixName.add(humanName.getPrefixAsSingleString());
      }

      String first_name = "";
      String middle_name = "";
      if (givenName.isEmpty() == false) {
        final String[] practitionerGivenName = givenName.iterator().next().split(" ");
        switch (practitionerGivenName.length) {
          case 2:
            middle_name = practitionerGivenName[1];
          case 1:
            first_name = practitionerGivenName[0];
            break;
        }
      }
      metadata.put("first_name", first_name);
      metadata.put("middle_name", middle_name);
      metadata.put("last_name", !familyName.isEmpty() ? familyName.iterator().next() : "");
      metadata.put("salutation", !prefixName.isEmpty() ? prefixName.iterator().next() : "");
    }
  }

  private static void setAddress(final Properties metadata, final Practitioner practitioner) {
    if (practitioner.hasAddress()) {
      final List<Address> addressList = practitioner.getAddress();
      final Set<String> address1Name = new HashSet<String>();
      final Address address = practitioner.getAddressFirstRep();
      metadata.put("city", filterNull(address.getCity()));
      metadata.put("state_or_province", filterNull(address.getState()));
      metadata.put("country", filterNull(address.getCountry()));
      metadata.put("zip_or_postal", filterNull(address.getPostalCode()));
      metadata.put("address1", "");
      metadata.put("address2", "");

      switch (address.getLine().size()) {
        case 0:
          break;

        case 1:
          metadata.put("address1", filterNull(address.getLine().get(0).toString()));
          break;

        case 2:
          metadata.put("address1", filterNull(address.getLine().get(0).toString()));
          metadata.put("address2", filterNull(address.getLine().get(1).toString()));
          break;

        default:
          metadata.put("address1", filterNull(address.getLine().get(0).toString()));

          String separator = "";
          final StringBuilder lineconcat = new StringBuilder();
          for (int i = 1; i < address.getLine().size(); i++) {
            lineconcat.append(separator).append(address.getLine().get(i).toString());
            separator = ", ";
          }
          metadata.put("address2", lineconcat.toString());
      }
    }
  }


  private static void setOrganization(final Properties metadata, final Practitioner practitioner) {
    for (final Identifier identifier : practitioner.getIdentifier()) {
      final Reference reference = identifier.getAssigner();
      if (reference != null && reference.getIdentifier() != null && reference.getIdentifier().getAssigner() != null) {
        for (final Coding coding : reference.getIdentifier().getType().getCoding()) {
          switch (coding.getCode()) {
            case ORGANIZATION_ROVER :
              metadata.setProperty(ROVER_ID, coding.getCode());
              break;
            case ORGANIZATION_ONTLL:
              metadata.setProperty(ONTARIO_LIFELABS_ID, coding.getCode());
              break;
            case ORGANIZATION_ONT_PROV_GOVNT:
              metadata.setProperty(ONTARIO_PROVINCIAL_GOVERNMENT_ID, coding.getCode());
              break;
          }
        }
      }
    }
  }


  private static String filterNull(final String value) {
    return (value == null ? "" : value);
  }
}
