/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.excelleris.eorder.converter.provincial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.converter.DiagnosticOrderConverter;
import org.oscarehr.integration.excelleris.eorder.converter.FhirResourceHelper;
import org.oscarehr.integration.excelleris.eorder.converter.OidConstants;
import org.oscarehr.integration.excelleris.eorder.converter.PatientPropertySet;
import org.oscarehr.integration.excelleris.eorder.converter.QuestionTextHelper;
import org.oscarehr.integration.excelleris.eorder.converter.ResourceUrlHelper;
import org.oscarehr.integration.excelleris.eorder.testcodes.LabCodesHelper;
import org.oscarehr.util.MiscUtils;
import lombok.extern.slf4j.Slf4j;
import oscar.util.StringUtils;

@Slf4j
public class EOrderOntario extends ProvincialBusinessLogic {

  private static final Logger LOG = MiscUtils.getLogger();

  private static final String THERAPEUTIC_DRUG_MONITORING = "T_therapeutic_drug_monitor";
  private static final String THERAPEUTIC_DRUG_MONITORING_1 = "T_therapeutic_drug_mon_src_1";
  private static final String THERAPEUTIC_DRUG_MONITORING_2 = "T_therapeutic_drug_mon_src_2";

  private static final String NEONATAL_BILIRUBIN_TEST = "T_neonatal_bilirubin";
  private static final String NEONATAL_BILIRUBIN_CHILD_AGE_DAYS = "EO_child_age_days";
  private static final String NEONATAL_BILIRUBIN_CHILD_AGE_HOURS = "EO_child_age_hours";
  private static final String NEONATAL_BILIRUBIN_PRACTITIONER_PHONE = "EO_practitioner_phone";
  private static final String NEONATAL_BILIRUBIN_PATIENT_PHONE = "EO_patient_phone";

  private static final String TDM_TIME_COLLECTED_1 = "EO_time_collected_1";
  private static final String TDM_TIME_OF_LAST_DOSE_1 = "EO_time_of_last_dose_1";
  private static final String TDM_TIME_OF_NEXT_DOSE_1 = "EO_time_of_next_dose_1";
  private static final String TDM_TIME_COLLECTED_2 = "EO_time_collected_2";
  private static final String TDM_TIME_OF_LAST_DOSE_2 = "EO_time_of_last_dose_2";
  private static final String TDM_TIME_OF_NEXT_DOSE_2 = "EO_time_of_next_dose_2";

  private static final String VAGINAL_RECTAL_GROUP_B_STREP = "T_vaginal_rectal_group_b";
  private static final String VAGINAL_RECTAL_GROUP_B_STREP_SRC = "T_vaginal_rectal_group_b_src";

  private static final String CHLAMYDIA = "T_chlamydia";
  private static final String CHLAMYDIA_SRC = "T_chlamydia_src";

  private static final String GC = "T_gc";
  private static final String GC_SRC = "T_gc_src";

  private static final String WOUND = "T_wound";
  private static final String WOUND_SRC = "T_wound_src";

  private static final String OTHER_SWABS_PUS = "T_other_swabs_pus";
  private static final String OTHER_SWABS_PUS_SRC = "T_other_swabs_pus_src";


  public EOrderOntario(final Province province, final Map<String, EFormValue> valueMap) {
    super(province, valueMap);
  }

  @Override
  protected String getMappingFileName() {
    return TEST_NAME_TO_EXCELLERIS_TEST_CODE_MAPPING_FILE.replace("%PROVINCE%", "ON");
  }

  @Override
  public boolean isExtensionQRNeeded(final String key) {
    return false;
  }


  @Override
  public boolean isTest(final String key) {
    return key.startsWith(LAB_TEST_PREFIX) && isRequired(key);
  }

  @Override
  public List<Extension> getOrderExtensionList(final String key) {
    final List<Extension> extensionList = new ArrayList<>();

    // tie breaker
    // TODO: how to set tie breaker code
    String tieBreaker = "0";
    extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemTieBreakerValueSetExtensionUrl(), tieBreaker,
        ResourceUrlHelper.getItemTieBreakerExtensionUrl()));

    extensionList.add(getHardCodingExtension(key));

    // is other microbiology
    String otherMicrobiology = isOtherMicrobiology(key) ? "true" : "false";
    extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemOtherIsOtherMicrobilogyTestValueSetExtensionUrl(), otherMicrobiology,
        ResourceUrlHelper.getItemOtherIsOtherMicrobilogyTestExtensionUrl()));

    return extensionList;
  }


  @Override
  public List<Extension> createTestGlobalExceptions(final Map<String, EFormValue> valueMap) {
    return new ArrayList<Extension>();
  }


  @Override
  public Extension createPayerExtension() {
    String payer = "";
    if (getValueMap().containsKey("EO_OHIP")) {
      payer = "OHIP";
    } else if (getValueMap().containsKey("EO_WSIB")) {
      payer = "WSIB";
    } else {
      // not insured so has to be patient pay
      payer = "Patient";
    }

    final CodeableConcept payerCoding = new CodeableConcept();
    payerCoding.addCoding(new Coding("https://api.excelleris.com/2.0/eorder/ValueSet/payer-type", payer, "Payer for Test"));

    return new Extension("https://api.excelleris.com/2.0/eorder/servicerequest-payertype", payerCoding);
  }

  @Override
  public void processHealthInsuranceLogic(final Patient patient) {
    String payer = null;
    if (getValueMap().containsKey("EO_OHIP")) {
      payer = "OHIP";
    } else if (getValueMap().containsKey("EO_WSIB")) {
      payer = "WSIB";
    } else {
      // not insured so has to be patient pay
      payer = "Patient";
    }

    // Identifier
    // hcn - required if not patient pay
    String healthInsuranceNumber = EFormValueHelper.getValue(getValueMap(), PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER);
    if (healthInsuranceNumber == null && !"Patient".equals(payer)) {
      LOG.error("Missing patient's " + PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER);
      throw new ServiceException("Patient's " + PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER + " is required.");
    }

    // Health insurance number retrieved from database includes space + version number
    // remove spaces from health insurance number
    if (healthInsuranceNumber != null) {
      healthInsuranceNumber = healthInsuranceNumber.replace(" ", "");
    }

    // Excelleris does not accept null property represented as not setting the property as
    // specified by https://www.hl7.org/fhir/DSTU2/json.html
    // Workaround to put a special empty string in there and have the fhir-api-excelleris code
    // to output a ""

    if (healthInsuranceNumber == null) {
      healthInsuranceNumber = "";//JsonParser.OUTPUT_NULL_AS_EMPTY_STRING;
    }

    if (healthInsuranceNumber != null) {
      Identifier identifier = FhirResourceHelper.createPatientHCNIdentifier(
          "hcn", OidConstants.HEALTH_INSURANCE_NUMBER_OID, healthInsuranceNumber, healthInsuranceNumber, "ON");
      patient.addIdentifier(identifier);
    }
  }


  /**
   * Get test code for the given eform value key
   *
   * @param key        eform value key to be mapped to test code
   * @return test code
   */
  @Override
  public String getTestCode(final String key,
                            QuestionnaireResponse questionnaireResponse,
                            final String itemUuid,
                            final Questionnaire questionnaire) {
    // using only 1 questionnaire response to store all questions
    // a null will cause a new one to be created. otherwise will add more info to the existing one

    String testCode = null;
    if (isComplexLabCodeMapping(key)) {
      // complex mapping
      String derivedKey = null;
      // PSA
      if (key.equals("T_total_psa") || (key.equals("T_free_psa"))) {
        // see if corresponding insured/uninsured is selected to determine code
        if (getValueMap().containsKey("T_psa_insured")) {
          derivedKey = key + "_insured";
        } else if (getValueMap().containsKey("T_psa_uninsured")) {
          derivedKey = key + "_uninsured";
        }
        LOG.info("Key : " + derivedKey);
        testCode = DiagnosticOrderConverter.getTestCode(derivedKey, getProvince().name());
      }

      // - depends on source info GC, Chlamydia
      String source = null;
      String sourceKey = null;
      if (key.equals(CHLAMYDIA)) {
        // check the corresponding source
        sourceKey = "T_chlamydia_src";
        if (getValueMap().containsKey(sourceKey)) {
          EFormValue eformValue = getValueMap().get(sourceKey);
          source = eformValue.getVarValue();

          if (source.equals("URINE")) {
            derivedKey = "T_chlamydia_inv_urine";
          } else {
            derivedKey = "T_chlamydia_inv_swab";
          }
          LOG.info("Key : " + derivedKey);
          testCode = DiagnosticOrderConverter.getTestCode(derivedKey, getProvince().name());
        } else {
          // no source value specified. - ERROR
          LOG.error("No source specified for:  " + key);
          throw new ServiceException("No source specified for:  " + key);
        }

        // set source info

        questionnaireResponse = createTestSourceQuestionnaireResponse(getValueMap(), testCode, CHLAMYDIA_SRC, itemUuid, questionnaireResponse);

      }
      if (key.equals(GC)) {
        // check the corresponding source
        sourceKey = "T_gc_src";
        if (getValueMap().containsKey(sourceKey)) {
          EFormValue eformValue = getValueMap().get(sourceKey);
          source = eformValue.getVarValue();
          if (source.equals("URINE")) {
            derivedKey = "T_n__gonorrhoeae_inv_urine";
          } else {
            derivedKey = "T_n__gonorrhoeae_inv_swab";
          }
          LOG.info("Key : " + derivedKey);
          testCode = DiagnosticOrderConverter.getTestCode(derivedKey, getProvince().name());
        } else {
          // no source value specified. - ERROR
          LOG.error("No source specified for:  " + key);
          throw new ServiceException("No source specified for:  " + key);
        }

        // set source info
        questionnaireResponse = createTestSourceQuestionnaireResponse(getValueMap(), testCode, GC_SRC, itemUuid, questionnaireResponse);
      }

      if (key.equals(THERAPEUTIC_DRUG_MONITORING_1)) {
        EFormValue eformValue = getValueMap().get(key);
        source = eformValue.getVarValue();
        derivedKey = LabCodesHelper.convertKey(source);
        LOG.info("Key : " + derivedKey);
        testCode = DiagnosticOrderConverter.getTestCode(derivedKey, getProvince().name());
        // process the source info
        questionnaireResponse = createTDMQuestionnaireResponse(getValueMap(), testCode, questionnaireResponse, TDM_TIME_COLLECTED_1, TDM_TIME_OF_LAST_DOSE_1, TDM_TIME_OF_NEXT_DOSE_1);
      }

      if (key.equals(THERAPEUTIC_DRUG_MONITORING_2)) {
        EFormValue eformValue = getValueMap().get(key);
        source = eformValue.getVarValue();
        derivedKey = LabCodesHelper.convertKey(source);
        LOG.info("Key : " + derivedKey);
        testCode = DiagnosticOrderConverter.getTestCode(derivedKey, getProvince().name());
        // process the source info
        questionnaireResponse = createTDMQuestionnaireResponse(getValueMap(), testCode, questionnaireResponse, TDM_TIME_COLLECTED_2, TDM_TIME_OF_LAST_DOSE_2, TDM_TIME_OF_NEXT_DOSE_2);
      }

      if (NEONATAL_BILIRUBIN_TEST.equals(key)) {
        testCode = DiagnosticOrderConverter.getTestCode(key, getProvince().name());
        questionnaireResponse = createNeonatalBilirubinQuestionnaireResponse(getValueMap(), testCode, questionnaireResponse);
      }

      if (key.equals(VAGINAL_RECTAL_GROUP_B_STREP)) {
        testCode = DiagnosticOrderConverter.getTestCode(key, getProvince().name());
        questionnaireResponse = createTestSourceQuestionnaireResponse(getValueMap(), testCode, VAGINAL_RECTAL_GROUP_B_STREP_SRC, itemUuid, questionnaireResponse);
      }

      if (key.equals(WOUND)) {
        testCode = DiagnosticOrderConverter.getTestCode(key, getProvince().name());
        questionnaireResponse = createTestSourceQuestionnaireResponse(getValueMap(), testCode, WOUND_SRC, itemUuid,
            questionnaireResponse);
      }

      if (key.equals(OTHER_SWABS_PUS)) {
        testCode = DiagnosticOrderConverter.getTestCode(key, getProvince().name());
        questionnaireResponse = createTestSourceQuestionnaireResponse(getValueMap(), testCode, OTHER_SWABS_PUS_SRC, itemUuid, questionnaireResponse);
      }

    } else {
      // simple mapping, just look it up from properties file

      // see if it is one of 'Other Tests'
      if (isOtherTests(key)) {
        // handle other test (find out the name of other tests)
        String derivedKey = null;
        //use naming convention to derive key from names of other tests.
        // for other tests, the test name is stored in the eForm value:value
        EFormValue eformValue = getValueMap().get(key);
        derivedKey = eformValue.getVarValue();
        derivedKey = LabCodesHelper.convertKey(derivedKey);
        log.info("Key : " + derivedKey);
        testCode = DiagnosticOrderConverter.getTestCode(derivedKey, getProvince().name());

      } else {
        log.info("Key : " + key);
        testCode = DiagnosticOrderConverter.getTestCode(key, getProvince().name());
      }
    }

    return testCode;
  }

  @Override
  public boolean isOtherMicrobiology(String key) {
    if (key.equals(OTHER_SWABS_PUS)) {
      return true;
    } else {
      return false;
    }
  }


  //
  // Private Methods
  //

  /**
   * Create questionnaire response for neonatal bilirubin test
   *
   * @param valueMap   eform value hash map
   * @param testCode
   * @param existingQR
   * @return questionnaire response for the specific test
   */
  private QuestionnaireResponse createNeonatalBilirubinQuestionnaireResponse(Map<String, EFormValue> valueMap, String testCode, QuestionnaireResponse existingQR) {

    // need to send child age days, hours, practitioner phone and patient phone
    // as questionnaire response
    // child age days
    List<String> questions = new ArrayList<>();
    List<String> answers = new ArrayList<>();
    String question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_CHILD_AGE_DAYS);
    questions.add(question);
    String answerValue = EFormValueHelper.getValue(valueMap, NEONATAL_BILIRUBIN_CHILD_AGE_DAYS);
    if (StringUtils.empty(answerValue)) {
      answerValue = ANSWER_UNKNOWN;
    }
    answers.add(answerValue);

    // child age hours
    question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_CHILD_AGE_HOURS);
    questions.add(question);
    answerValue = EFormValueHelper.getValue(valueMap, NEONATAL_BILIRUBIN_CHILD_AGE_HOURS);
    if (StringUtils.empty(answerValue)) {
      answerValue = ANSWER_UNKNOWN;
    }
    answers.add(answerValue);

    // practitioner phone
    question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_PRACTITIONER_PHONE_NO);
    questions.add(question);
    answerValue = EFormValueHelper.getValue(valueMap, NEONATAL_BILIRUBIN_PRACTITIONER_PHONE);
    if (StringUtils.empty(answerValue)) {
      answerValue = ANSWER_UNKNOWN;
    }
    answers.add(answerValue);

    // patient phone
    question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_PATIENT_PHONE_NO);
    questions.add(question);
    answerValue = EFormValueHelper.getValue(valueMap, NEONATAL_BILIRUBIN_PATIENT_PHONE);
    if (StringUtils.empty(answerValue)) {
      answerValue = ANSWER_UNKNOWN;
    }
    answers.add(answerValue);

    QuestionnaireResponse questionnaireResponse = FhirResourceHelper.createQuestionnaireResponse(existingQR, questions, answers);

    return questionnaireResponse;
  }

  private boolean isComplexLabCodeMapping(String key) {
    // code to determine if lab code mapping is complex or not
    if (key.equals("T_total_psa") ||
        (key.equals("T_free_psa")) ||
        (key.equals("T_chlamydia")) ||
        (key.equals("T_gc")) ||
        (key.equals(THERAPEUTIC_DRUG_MONITORING_1)) ||
        (key.equals(THERAPEUTIC_DRUG_MONITORING_2)) ||
        (key.equals(NEONATAL_BILIRUBIN_TEST)) ||
        (key.equals(VAGINAL_RECTAL_GROUP_B_STREP)) ||
        (key.equals(WOUND)) ||
        (key.equals(OTHER_SWABS_PUS))) {
      return true;
    } else {
      return false;
    }
  }

  private boolean isRequired(String key) {
    // code to determine if a form field needs to be mapped to a lab test code
    if (key.equals("T_psa_insured") || (key.equals("T_psa_uninsured")) || (key.equals(THERAPEUTIC_DRUG_MONITORING)) || isSource(key)) {
      return false;
    } else {
      return true;
    }
  }


  private boolean isSource(String key) {
    if (key.startsWith("T_") && (key.endsWith("_src") || key.contains("_src_")) && !key.equals(THERAPEUTIC_DRUG_MONITORING_1) && !key.equals(THERAPEUTIC_DRUG_MONITORING_2)) {
      return true;
    }
    return false;
  }

  /**
   * Create questionnaire response for therapeutic drug monitoring test
   *
   * @param valueMap      eform value hash map
   * @param testCode
   * @param existingQR
   * @param timeCollected
   * @param timeLastDose
   * @param timeNextDose
   * @return questionnaire response for the specific test
   */
  private QuestionnaireResponse createTDMQuestionnaireResponse(Map<String, EFormValue> valueMap, String testCode, QuestionnaireResponse existingQR, String timeCollected, String timeLastDose, String timeNextDose) {

    // need to send time collected, time of last dose, time of next dose
    // as questionnaire response
    // time collected
    List<String> questions = new ArrayList<>();
    List<String> answers = new ArrayList<>();
    String question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_TIME_COLLECTED);
    questions.add(question);
    String answerValue = EFormValueHelper.getValue(valueMap, timeCollected);
    if (StringUtils.empty(answerValue)) {
      answerValue = ANSWER_UNKNOWN;
    }
    answers.add(answerValue);

    // time of last dose
    question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_TIME_OF_LAST_DOSE);
    questions.add(question);
    answerValue = EFormValueHelper.getValue(valueMap, timeLastDose);
    if (StringUtils.empty(answerValue)) {
      answerValue = ANSWER_UNKNOWN;
    }
    answers.add(answerValue);

    // practitioner phone
    question = QuestionTextHelper.getQuestionLinkId(testCode, QuestionTextHelper.QUESTION_TIME_OF_NEXT_DOSE);
    questions.add(question);
    answerValue = EFormValueHelper.getValue(valueMap, timeNextDose);
    if (StringUtils.empty(answerValue)) {
      answerValue = ANSWER_UNKNOWN;
    }
    answers.add(answerValue);

    QuestionnaireResponse questionnaireResponse = FhirResourceHelper.createQuestionnaireResponse(existingQR, questions, answers);

    return questionnaireResponse;
  }


  private Extension getHardCodingExtension(final String key) {

    Extension extension = null;

    // is hard coded test
    String hardCoded = isHardCodedTest(key) ? "1" : "0";
    // this value apparently takes on other values based on the test - not described in the api doc
    if (key.equals(THERAPEUTIC_DRUG_MONITORING_1)) {
      hardCoded = "2";
    } else if (key.equals(THERAPEUTIC_DRUG_MONITORING_2)) {
      hardCoded = "3";
    }
    extension = FhirResourceHelper.createCodeableConceptExtension(
        ResourceUrlHelper.getItemIsHardCodedTestValueSetExtensionUrl(), hardCoded, ResourceUrlHelper.getItemIsHardCodedTestExtensionUrl());

    return extension;
  }

}
