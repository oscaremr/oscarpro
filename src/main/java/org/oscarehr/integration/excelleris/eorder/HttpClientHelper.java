/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Map;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.RedirectStrategy;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.cookie.ClientCookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.HttpContext;
import org.apache.log4j.Logger;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.util.MiscUtils;
import oscar.OscarProperties;
import oscar.util.StringUtils;

public class HttpClientHelper {

  private static final Logger LOG = MiscUtils.getLogger();

  private static final String HTTPS_PROTOCOLS = "https.protocols";
  private static final String TLS_VERSION = "TLSv1.2";
  //	private static final String TLS_VERSION = "TLSv1.1";
  public static final String EXCELLERIS_EORDER_AZURE_AFFINITY_COOKIE_NAME = "Arr-Disable-Session-Affinity";

  private static final String LAST_MRH_SESSION_COOKIE_NAME = "LastMRH_Session";
  private static final String MRH_SESSION_COOKIE_NAME = "MRHSession";
  private static final String F5_ST_COOKIE_NAME = "F5_ST";

  // ---------------------------------------------------------------------------- Private Methods

  /**
   * Create a default HttpClient for Excelleris eOrder service (API 2.0)
   *
   * @return HttpClient object
   * @throws Exception
   */
  public static HttpClient createDefaultHttpClient() throws Exception {
    HttpClient client = HttpClients.createDefault();
    return client;
  }

  /**
   * Create a context set up for cookies for the appropriate domain
   *
   * @param domain          Domain to configure
   * @param affinityDisable indication whether or not Azure affinity shoudl be disabled
   * @return HttpContext object
   * @throws Exception
   */
  public static HttpClientContext createHttpContext(final String domain, final String affinityDisable) throws Exception {
    final BasicCookieStore cookieStore = new BasicCookieStore();
    final BasicClientCookie cookie = new BasicClientCookie(EXCELLERIS_EORDER_AZURE_AFFINITY_COOKIE_NAME, affinityDisable);
    cookie.setDomain(domain);
    cookie.setAttribute(ClientCookie.DOMAIN_ATTR, "true");
    cookie.setPath("/");
    cookieStore.addCookie(cookie);

    final HttpClientContext context = HttpClientContext.create();
    context.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);

    return context;
  }

  /**
   * Create a HttpClient with SSLContext initialized for Excelleris eOrder service
   *
   * @return
   * @throws Exception
   */
  public static HttpClient createHttpClient(Map<String, String> cookieMap) throws Exception {

    String keyStoreType = "PKCS12";
    KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");

    KeyStore keyStore = KeyStore.getInstance(keyStoreType);
    InputStream inputStream = getStore("Key Store", ConfigureConstants.EXCELLERIS_EORDER_KEYSTORE_FILE_PARAM_NAME, PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_KEYSTORE_FILE);
    if (inputStream != null) {
      try {
        if (inputStream != null) {
          keyStore.load(inputStream, getKeyStorePassword().toCharArray());
          keyManagerFactory.init(keyStore, getKeyStorePassword().toCharArray());
        }
      } catch (Exception ex) {
        System.out.println("Issue: " + ex.getMessage());
      } finally {
        if (inputStream != null) {
          inputStream.close();
        }
      }
    } else {
      LOG.error("Cannot find resource " + getKeyStorePath());
      throw new ServiceException("Cannot find resource: " + getKeyStorePath());
    }

    KeyStore trustStore = KeyStore.getInstance(keyStoreType);
    inputStream = getStore("Trust Store", ConfigureConstants.EXCELLERIS_EORDER_TRUSTSTORE_FILE_PARAM_NAME, PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_FILE);
    if (inputStream != null) {
      try {
        if (inputStream != null) {
          trustStore.load(inputStream, getTrustStorePassword().toCharArray());
          keyManagerFactory.init(trustStore, getTrustStorePassword().toCharArray());
        }

      } catch (Exception ex) {
        System.out.println("Issue: " + ex.getMessage());


      } finally {
        if (inputStream != null) {
          inputStream.close();
        }
      }
    } else {
      LOG.error("Cannot find resource " + getTrustStorePath());
      throw new ServiceException("Cannot find resource: " + getTrustStorePath());
    }

    SSLContext sslContext = initSSLContext(keyStore, trustStore);

    RequestConfig config = RequestConfig.custom().setConnectTimeout(getConnectionTimeout()).
        setConnectionRequestTimeout(getConnectionTimeout()).
        setSocketTimeout(getConnectionTimeout()).build();

    HttpClient client = HttpClients.custom().setSSLContext(sslContext).setRedirectStrategy(new AuthenticationRedirectStrategy(cookieMap)).setDefaultRequestConfig(config).build();

    return client;
  }

  /**
   * Initialize SSLContext TLS version, keystore and trustore
   *
   * @param keyStore
   * @param trustStore
   * @return
   */
  private static SSLContext initSSLContext(KeyStore keyStore, KeyStore trustStore) {
    try {
      System.setProperty(HTTPS_PROTOCOLS, TLS_VERSION);
      final SSLContext ctx = SSLContext.getInstance(TLS_VERSION);

      ctx.init(getKeyManagers(keyStore, getKeyStorePassword()), getTrustManagers(trustStore), new SecureRandom());
      return ctx;
    } catch (final Exception ex) {
      LOG.error("Error initializing SSL context.", ex);
      throw new ServiceException("Error initializing SSL context");
    }
  }

  /**
   * Create a trustore manager
   *
   * @param trustStore
   * @return
   * @throws NoSuchAlgorithmException
   * @throws KeyStoreException
   */
  private static TrustManager[] getTrustManagers(KeyStore trustStore) throws NoSuchAlgorithmException, KeyStoreException {
    String algorithm = KeyManagerFactory.getDefaultAlgorithm();
    TrustManagerFactory factory = TrustManagerFactory.getInstance(algorithm);
    factory.init(trustStore);
    return factory.getTrustManagers();
  }

  /**
   * Create a key store manager
   *
   * @param keyStore
   * @param keyStorePassword
   * @return
   * @throws GeneralSecurityException
   */
  private static KeyManager[] getKeyManagers(KeyStore keyStore, String keyStorePassword) throws GeneralSecurityException {
    String algorithm = KeyManagerFactory.getDefaultAlgorithm();
    char[] keyPass = keyStorePassword != null ? keyStorePassword.toCharArray() : null;
    KeyManagerFactory factory = KeyManagerFactory.getInstance(algorithm);
    factory.init(keyStore, keyPass);
    return factory.getKeyManagers();
  }

  /**
   * Get keystore path
   *
   * @return
   */
  private static String getKeyStorePath() {

    if (OscarProperties.getInstance() != null) {
      return OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_EORDER_KEYSTORE_FILE_PARAM_NAME);
    } else {
      LOG.warn("Using DEFAULT_EXCELLERIS_EORDER_KEYSTORE_FILE");
      return PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_KEYSTORE_FILE;
    }
  }

  /**
   * Get keystore password
   *
   * @return
   */
  private static String getKeyStorePassword() {
    if (OscarProperties.getInstance() != null) {
      return OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_EORDER_KEYSTORE_PASSWORD_PARAM_NAME);
    } else {
      LOG.warn("Using DEFAULT_EXCELLERIS_EORDER_KEYSTORE_PASSWORD");
      return PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_KEYSTORE_PASSWORD;
    }
  }

  /**
   * Get trust store path
   *
   * @return
   */
  private static String getTrustStorePath() {
    if (OscarProperties.getInstance() != null) {
      return OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_EORDER_TRUSTSTORE_FILE_PARAM_NAME);
    } else {
      LOG.warn("Using DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_FILE");
      return PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_FILE;
    }
  }

  /**
   * Get trust password
   *
   * @return
   */
  private static String getTrustStorePassword() {
    if (OscarProperties.getInstance() != null) {
      return OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_EORDER_TRUSTSTORE_PASSWORD_PARAM_NAME);
    } else {
      LOG.warn("Using DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_PASSWORD");
      return PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_PASSWORD;
    }
  }

  private static InputStream getStore(String type, String propertyName, String defaultPath) {

    InputStream inputStream = null;
    if (OscarProperties.getInstance() != null) {
      String storePath = OscarProperties.getInstance().getProperty(propertyName);
      if (!StringUtils.empty(storePath)) {
        String docDir = OscarProperties.getInstance().getProperty("DOCUMENT_DIR");
        if (docDir == null) {
          LOG.warn("DOCUMENT_DIR is not defined in configuration file.");
          throw new ServiceException("DOCUMENT_DIR is not defined in configuration file.");
        }

        // store path is relative to DOCUMENT_DIR
        String storeFilePath = docDir + storePath;
        LOG.debug("Loading " + type + " store defined in configuration file: " + storeFilePath);

        File file = new File(storeFilePath);
        try {
          inputStream = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
          LOG.debug("File does not exist. " + ex.getMessage());
          LOG.debug("Loading " + type + " store " + storePath + " from classpath.");
          inputStream = HttpClientHelper.class.getResourceAsStream(storePath);
        }
      } else {
        LOG.debug("Loading default " + type + " store : " + defaultPath);
        File file = new File(defaultPath);
        try {
          inputStream = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
          LOG.debug("File does not exist. " + ex.getMessage());
          LOG.debug("Loading default " + type + " store " + defaultPath + " from classpath.");
          inputStream = HttpClientHelper.class.getResourceAsStream(defaultPath);
        }
      }
    } else {
      LOG.debug("Loading default " + type + " store : " + defaultPath);
      File file = new File(defaultPath);
      try {
        inputStream = new FileInputStream(file);
      } catch (FileNotFoundException ex) {
        LOG.debug("File does not exist. " + ex.getMessage());
        LOG.debug("Loading default " + type + " store " + defaultPath + " from classpath.");
        inputStream = HttpClientHelper.class.getResourceAsStream(defaultPath);
      }
    }

    return inputStream;
  }

  /**
   * Get connection timeout in milliseconds
   */
  private static int getConnectionTimeout() {
    String config = null;
    if (OscarProperties.getInstance() != null) {
      config = OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_EORDER_CONNETION_TIMEOUT_MILLISECONDS);
      return Integer.parseInt(config);
    } else {
      LOG.warn("Using DEFAULT_EXCELLERIS_EORDER_CONNETION_TIMEOUT_MILLISECONDS");
      return PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_CONNETION_TIMEOUT_MILLISECONDS;
    }
  }

  /**
   * Authentication redirect strategy class
   */
  private static class AuthenticationRedirectStrategy implements RedirectStrategy {

    private Map<String, String> cookieMap;
    private int redirectCount = 0;

    public AuthenticationRedirectStrategy(Map<String, String> cookieMap) {
      this.cookieMap = cookieMap;
    }

    @Override
    public HttpUriRequest getRedirect(HttpRequest request, HttpResponse response, HttpContext context) throws ProtocolException {
      final Header locationHeader = response.getFirstHeader("location");
      LOG.info("***** getRedirect - redirect count " + redirectCount + ", locationHeader " + locationHeader);

      LOG.info("getRedirect - response " + response);

      Header[] cookies = response.getHeaders("Set-Cookie");

      for (Header cookie : cookies) {
        if (cookie.getValue().startsWith(LAST_MRH_SESSION_COOKIE_NAME)) {
          cookieMap.put(LAST_MRH_SESSION_COOKIE_NAME, cookie.getValue());
        } else if (cookie.getValue().startsWith(MRH_SESSION_COOKIE_NAME)) {
          cookieMap.put(MRH_SESSION_COOKIE_NAME, cookie.getValue());
        } else if (cookie.getValue().startsWith(F5_ST_COOKIE_NAME)) {
          cookieMap.put(F5_ST_COOKIE_NAME, cookie.getValue());
        } else {
          LOG.debug("Ignore cookie " + cookie.getValue());
        }
      }
      LOG.info("cookie map: " + cookieMap);

      redirectCount = redirectCount + 1;
      if (locationHeader != null) {
        URI uri = null;
        try {
          uri = createURI(request, response, context);
          LOG.info("************* uri: " + uri);
        } catch (URISyntaxException e) {
          LOG.error("Error creating uri", e);
          throw new ServiceException("Error creating uri." + e.getMessage());
        }
        final String method = request.getRequestLine().getMethod();
        if (method.equalsIgnoreCase(HttpHead.METHOD_NAME)) {
          return new HttpHead(uri);
        } else if (method.equalsIgnoreCase(HttpGet.METHOD_NAME)) {
          return new HttpGet(uri);
        } else if (method.equalsIgnoreCase(HttpPost.METHOD_NAME)) {
          return RequestBuilder.copy(request).setUri(uri).build(); // Here
        } else {
          final int status = response.getStatusLine().getStatusCode();
          if (status == HttpStatus.SC_TEMPORARY_REDIRECT) {
            return RequestBuilder.copy(request).setUri(uri).build();
          } else {
            return new HttpGet(uri);
          }
        }
      }

      return null;
    }

    @Override
    public boolean isRedirected(HttpRequest request, HttpResponse response, HttpContext context) throws ProtocolException {
      final Header locationHeader = response.getFirstHeader("location");
      LOG.info("isRedirected - locationHeader " + locationHeader);
      LOG.info("isRedirected - response " + response.getAllHeaders());

      final String method = request.getRequestLine().getMethod();
      if (locationHeader != null) {
        if ((method.equalsIgnoreCase(HttpPost.METHOD_NAME)) || (method.equalsIgnoreCase(HttpGet.METHOD_NAME))) {
          return true;
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    private URI createURI(HttpRequest request, HttpResponse response, HttpContext context) throws URISyntaxException {
      final Header locationHeader = response.getFirstHeader("location");
      String baseURL = getBaseUrl();
      if (locationHeader.getValue().startsWith("/")) {
        return new URI(baseURL + locationHeader.getValue());
      } else {
        return new URI(locationHeader.getValue());
      }
    }

    private String getBaseUrl() {
      if (OscarProperties.getInstance() != null) {
        return OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_BASE_URL_PARAM_NAME);
      } else {
        LOG.warn("Using DEFAULT_EXCELLERIS_EORDER_BASE_URL");
        return PropertyDefaultConstants.EXCELLERIS_EORDER_BASE_URL_PARAM_NAME;
      }
    }
  }
}
