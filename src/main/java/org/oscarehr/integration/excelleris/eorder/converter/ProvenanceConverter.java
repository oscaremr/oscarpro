/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import ca.uhn.fhir.model.api.TemporalPrecisionEnum;
import org.apache.commons.codec.binary.Base64;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.InstantType;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.Provenance;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Resource;
import org.hl7.fhir.r4.model.ResourceType;
import org.hl7.fhir.r4.model.ServiceRequest;
import org.hl7.fhir.r4.model.Signature;
import org.oscarehr.common.model.EFormValue;

public class ProvenanceConverter {

  public static final String PROVENANCE_SYSTEM_URL = "urn:iso-astm:E1762-95:2013";
  public static final String PROVENANCE_OID = "1.2.840.10065.1.12.1.1";
  public static final String PROVENANCE_DISPLAY = "Author'sSignature";

  public static Provenance toFhirObject(final ServiceRequest serviceRequest, final Practitioner practitioner, final Map<String, EFormValue> valueMap) {

    final Provenance provenance = new Provenance();
    provenance.setTarget(
        ConverterHelper.createArray(
            new Reference(ResourceUrlHelper.getResourceUrl(ServiceRequest.class.getSimpleName(), null))
        ));
    //	provenance.setRecorded(new Date());   Not needed at this point
    provenance.setAgent(
        ConverterHelper.createArray(
            new Provenance.ProvenanceAgentComponent(
                new Reference(
                    ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), practitioner.getId()))
            )));

    final Signature signature = new Signature();
    Date sigDate;
    try {
      sigDate = new SimpleDateFormat("dd-MMM-yyyy").parse(valueMap.get("today").getVarValue());
    } catch (Exception ex) {
      sigDate = new Date();
    }
    signature.setWhenElement(new InstantType(sigDate, TemporalPrecisionEnum.MILLI, TimeZone.getTimeZone("UTC")));  // Need the date when signature was saved?
    signature.setType(ConverterHelper.createArray(new Coding(PROVENANCE_SYSTEM_URL, PROVENANCE_OID, PROVENANCE_DISPLAY)));
    signature.setWho(new Reference(
        ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), practitioner.getId())));
    signature.setTargetFormat("application/fhir+json");
    signature.setTargetFormat("application/signature+json");

    configureSignature(provenance, valueMap);
    provenance.setSignature(ConverterHelper.createArray(signature));

    return provenance;
  }

  public static void configureSignature(final List<Resource> filteredList, final Map<String, EFormValue> valueMap) {

    for(final Resource resource : filteredList) {
      if (resource.getResourceType() != null && resource.getResourceType().equals(ResourceType.Provenance)) {
        final Provenance provenance = (Provenance) resource;
        configureSignature(provenance, valueMap);
      }
    }
  }

  public static void configureSignature(final Bundle bundle, final Map<String, EFormValue> valueMap) {

    for(final Bundle.BundleEntryComponent bec : bundle.getEntry()) {
      if (bec.getResource() != null 
          && bec.getResource().getResourceType() != null 
          && bec.getResource().getResourceType().equals(ResourceType.Provenance)) {
        final Provenance provenance = (Provenance) bec.getResource();
        configureSignature(provenance, valueMap);
      }
    }
  }


   public static void configureSignature(final Provenance provenance, final Map<String, EFormValue> valueMap) {
    Signature signature = null;
    final EFormValue signatureData = valueMap.get("signatureImageData");
    if (signatureData != null && provenance.getSignature() != null && provenance.getSignature().isEmpty() == false) {
      signature = provenance.getSignature().get(0);
      String imageString = signatureData.getVarValue();
      String[] imageStringArray = imageString.split(",");
      if (imageStringArray != null && imageStringArray.length == 2) {
        Base64 b64 = new Base64();
        byte[] imageByteData = imageStringArray[1].getBytes();
        byte[] imageData = b64.decode(imageByteData);
        signature.setData(imageData);
      }
    }
  }
}
