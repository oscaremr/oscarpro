/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.testcodes;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.oscarehr.integration.excelleris.eorder.ConfigureConstants;
import org.oscarehr.util.MiscUtils;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;
import oscar.OscarProperties;

public class LabTestCodesServlet implements Servlet {

  protected static Logger logger = Logger.getLogger(LabTestCodesServlet.class);
  public static final String SUBMIT_JOB = "labTestCodesJob";
  public static final String STARTUP_SUBMIT_JOB = "startupLabTestCodesJob";
  public static final String SUBMIT_TRIGGER = "labTestCodesTrigger";

  public static void schedule() throws Exception {

    Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

    String time = OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_EORDER_LAB_TEST_CODES_FETCH_TIME);
    String cronText = "0 " + getMinutes(time) + " " + getHour(time) + " * * ?";

    boolean debugTestCodes = Boolean.valueOf(OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_EORDER_LAB_TEST_CODES_FETCH_DEBUG));
    if (debugTestCodes) {
      cronText = "0 * * ? * *"; // This will trigger every minute - use for testing
    }
    logger.info("cron expression [" + cronText + "]");

    scheduler.start();

    JobDetail job = scheduler.getJobDetail(SUBMIT_JOB, Scheduler.DEFAULT_GROUP);
    if (job != null) {
      logger.info("Delete old job.");
      scheduler.deleteJob(SUBMIT_JOB, Scheduler.DEFAULT_GROUP);
    }

    Trigger trigger = new CronTrigger(SUBMIT_TRIGGER, Scheduler.DEFAULT_GROUP, cronText);
    JobDetail jobDetail = new JobDetail(SUBMIT_JOB, Scheduler.DEFAULT_GROUP, LabTestCodesJob.class);
    scheduler.scheduleJob(jobDetail, trigger);


    // When Tomcat first starts, do update the lab test codes regardless of schedule
    //
    if (s_startupLabCodeTestRequired) {
      jobDetail = new JobDetail(STARTUP_SUBMIT_JOB, Scheduler.DEFAULT_GROUP, LabTestCodesJob.class);
      trigger = new SimpleTrigger("StartupLabTestLoad", Scheduler.DEFAULT_GROUP);
      scheduler.scheduleJob(jobDetail, trigger);
      s_startupLabCodeTestRequired = false;
    }
  }

  public ServletConfig getServletConfig() {
    return null;
  }

  public String getServletInfo() {
    return null;
  }

  public void destroy() {
  }

  public void init(ServletConfig arg0) throws ServletException {
    try {
      logger.info("Start lab test codes scheduler...");
      schedule();
    } catch (Exception e) {
      MiscUtils.getLogger().error("Error", e);
    }
  }

  public void service(ServletRequest arg0, ServletResponse arg1)
      throws ServletException, IOException {
  }

  // ---------------------------------------------------------------------- Private Methods

  /**
   * Returns the minutes from a time string (HH:mm)
   *
   * @param time time as string
   * @return the minute portion
   */
  private static String getMinutes(String time) {
    int min = 0;
    try {
      String[] timeParts = time.trim().split(":");
      min = Integer.parseInt(timeParts[1]);
    } catch (Exception ex) {
      logger.info("getMinutes : error", ex);
    }

    return new Integer(min).toString();
  }

  /**
   * Returns the hour from a time string (HH:mm)
   *
   * @param time time as string
   * @return the hour portion
   */
  private static String getHour(String time) {
    int hour = 0;
    try {
      String[] timeParts = time.trim().split(":");
      hour = Integer.parseInt(timeParts[0]);
    } catch (Exception ex) {
      logger.info("getHour : error", ex);
    }

    return new Integer(hour).toString();
  }

  static private boolean s_startupLabCodeTestRequired = true;
}