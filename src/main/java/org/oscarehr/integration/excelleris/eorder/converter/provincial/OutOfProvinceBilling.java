/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.excelleris.eorder.converter.provincial;

public enum OutOfProvinceBilling {
  BC("2.16.840.1.113883.3.1772.1.2"),
  AB("2.16.840.1.113883.3.1772.1.3"),
  MB("2.16.840.1.113883.3.1772.1.4"),
  NB("2.16.840.1.113883.3.1772.1.5"),
  NF("2.16.840.1.113883.3.1772.1.6"),
  NL("2.16.840.1.113883.3.1772.1.6"),
  NS("2.16.840.1.113883.3.1772.1.7"),
  NT("2.16.840.1.113883.3.1772.1.8"),
  NU("2.16.840.1.113883.3.1772.1.9"),
  ON("2.16.840.1.113883.3.1772.1.10"),
  PE("2.16.840.1.113883.3.1772.1.11"),
  QC("2.16.840.1.113883.3.1772.1.12"),
  SK("2.16.840.1.113883.3.1772.1.13"),
  YT("2.16.840.1.113883.3.1772.1.14");

  public String getOid() {
    return m_oid;
  }

  public void setOid(final String oid) {
    m_oid = oid;
  }

  private OutOfProvinceBilling(final String oid) {
    this.m_oid = oid;
  }

  private String m_oid = null;
}
