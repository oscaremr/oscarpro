/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hl7.fhir.dstu3.model.codesystems.IssueSeverity;
import org.hl7.fhir.r4.formats.JsonParser;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Bundle.BundleEntryComponent;
import org.hl7.fhir.r4.model.DocumentReference;
import org.hl7.fhir.r4.model.DocumentReference.DocumentReferenceContentComponent;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.OperationOutcome.OperationOutcomeIssueComponent;
import org.hl7.fhir.r4.model.Parameters;
import org.hl7.fhir.r4.model.Parameters.ParametersParameterComponent;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.Resource;
import org.hl7.fhir.r4.model.StringType;
import org.hl7.fhir.r4.model.Type;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.util.MiscUtils;

public class OrderResponseHelper {

  private static final Logger LOG = MiscUtils.getLogger();

  private static final String VALIDATION_BUNDLE_NAME = "ValidationBundle";
  private static final String OUTPUT_BUNDLE_NAME = "OutputBundle";

  private static final String ORDER_BUNDLE_FULLURL = "https://api.excelleris.com/2.0/eorder/OrderBundle";
  private static final String VALIDATION_BUNDLE_FULLURL = "https://api.excelleris.com/2.0/eorder/ValidationBundle";
  private static final String OUTPUT_BUNDLE_FULLURL = "https://api.excelleris.com/2.0/eorder/OutputBundle";
  private static final String EXTENSION_FULLURL = "https://api.excelleris.com/2.0/eorder/Extension";
  private static final String MHANGO_USER_TYPE_URI = "https://api.excelleris.com/2.0/eorder/MhangoUserType";
  private final Resource resource;
  private String orderId;
  private String patientId;
  /**
   * Base 64 encoded pdf form
   */
  private String pdfForm;
  private List<String> errors = new ArrayList<>();
  private List<String> informationalMessages = new ArrayList<>();
  private boolean initialized = false;
  private Questionnaire questionnaire;
  private boolean patientLifeLabsStatus = false;
  private boolean orderBundleStatus = false;
  private boolean validationBundleStatus = false;

  public OrderResponseHelper(Resource resource) {
    this.resource = resource;
  }

  public List<String> getErrors() {

    if (!initialized) {
      parse();
    }

    return this.errors;
  }

  public List<String> getInformationalMessages() {

    if (!initialized) {
      parse();
    }

    return this.informationalMessages;
  }

  public String getOrderId() {

    if (!initialized) {
      parse();
    }

    return this.orderId;
  }

  public String getPdfLabRequisitionForm() {

    if (!initialized) {
      parse();
    }

    return this.pdfForm;
  }

  public String getPatientId() {

    if (!initialized) {
      parse();
    }

    return this.patientId;
  }

  public Questionnaire getQuestionnaire() {

    if (!initialized) {
      parse();
    }


    return this.questionnaire;
  }

  public boolean getPatientLifeLabsStatus() {

    if (!initialized) {
      parse();
    }

    return this.patientLifeLabsStatus;
  }

  public boolean getValidationBundleStatus() {

    if (!initialized) {
      parse();
    }

    return this.validationBundleStatus;
  }

  // ------------------------------------------------------------------------- Private Methods

  private void parse() {
    if (initialized) {
      return;
    }

    if (resource instanceof Bundle) {
      parseEOrderBundle((Bundle) resource);
    } else if (resource instanceof Parameters) {
      parseParameters((Parameters) resource);
    }

    this.initialized = true;
  }

  private void parseParameters(Parameters parameters) {

    for (ParametersParameterComponent parameter : parameters.getParameter()) {
      String name = parameter.getName();
      LOG.debug("parameter name " + parameter.getName());

      // output bundle
      if (OUTPUT_BUNDLE_NAME.equals(parameter.getName())) {
        Bundle bundle = (Bundle) parameter.getResource();
        for (BundleEntryComponent entry : bundle.getEntry()) {
          Resource entryResource = entry.getResource();
          // LOG.debug("parseParameters: " + entryResource.getClass().getName());
          // Questionnaire
          if (entryResource instanceof Questionnaire) {
            this.questionnaire = (Questionnaire) entryResource;
          }


          // Document Reference
          if (entryResource instanceof DocumentReference) {
            DocumentReference documentReference = (DocumentReference) entryResource;
            List<DocumentReferenceContentComponent> content = documentReference.getContent();
            if (content != null && content.size() == 1) {
              this.pdfForm = content.get(0).getAttachment().getDataElement().getValueAsString();
            } else {
              if (content == null) {
                LOG.error("No pdf lab requisition form.");
                throw new ServiceException("No pdf lab requisition form.");
              }
            }
          }
        }
      } else if (VALIDATION_BUNDLE_NAME.equals(name)) {

        // parameters
        Parameters bundleParameters = (Parameters) parameter.getResource();

        for (ParametersParameterComponent bundleParameter : bundleParameters.getParameter()) {
          Resource bundleResource = bundleParameter.getResource();
          // order bundle
          // LOG.debug("validation bundle - parameter " + bundleResouce.getClass().getName());

          if (bundleResource instanceof Bundle) {
//						parseBundle((Bundle)bundleResouce);
          }

          if (bundleResource instanceof OperationOutcome) {
//						parseOperationOutcome((OperationOutcome)bundleResouce);
          }
          // questionnaire
          if (bundleResource instanceof Questionnaire) {
            this.questionnaire = (Questionnaire) bundleResource;
          }
        }
      }
    }
  }

  private void parseEOrderBundle(Bundle eOrderResponse) {
    if (resource instanceof Bundle) {
      for (BundleEntryComponent entry : eOrderResponse.getEntry()) {
        if (ORDER_BUNDLE_FULLURL.equals(entry.getFullUrl())) {
          // This is a duplicate of what we submitted, so we can ignore it.
          orderBundleStatus = true;

        } else if (VALIDATION_BUNDLE_FULLURL.equals(entry.getFullUrl())) {
          Bundle bundle = (Bundle) entry.getResource();
          parseValidationBundle(bundle);

        } else if (OUTPUT_BUNDLE_FULLURL.equals(entry.getFullUrl())) {
          Bundle bundle = (Bundle) entry.getResource();
          parseOutputBundle(bundle);
        }
      }
    }

  }

  private void parseValidationBundle(Bundle bundle) {

    if (!orderBundleStatus) {
      validationBundleStatus = true;
    }
    for (BundleEntryComponent entry : bundle.getEntry()) {
      Resource validationresource = entry.getResource();

      if (validationresource instanceof Questionnaire) {
        this.questionnaire = (Questionnaire) validationresource;
      }

      // Operation Outcome
      else if (validationresource instanceof OperationOutcome) {
        OperationOutcome operationOutcome = (OperationOutcome) validationresource;
        for (OperationOutcomeIssueComponent issue : operationOutcome.getIssue()) {

          final String severity = issue.getSeverity().toCode();
          if ("information".equals(severity) || "warning".equals(severity)) {
            informationalMessages.add(issue.getDetails().getText());

          } else if ("error".equals(severity)) {
            errors.add(issue.getDetails().getText());
          }
        }
      }
      // Order bundle
    }
  }

  private void parseOutputBundle(Bundle bundle) {

    for (BundleEntryComponent entry : bundle.getEntry()) {
      // LOG.debug("parseBundle: " + resource.getClass().getName());

      if (entry.hasResource()) {
        Resource outputResource = entry.getResource();

        // Operation Outcome
        if (outputResource instanceof OperationOutcome) {
          OperationOutcome operationOutcome = (OperationOutcome) outputResource;
          for (OperationOutcomeIssueComponent issue : operationOutcome.getIssue()) {
            errors.add(issue.getDetails().getText());
          }

        } else if (outputResource instanceof Patient) {
          final Patient patient = (Patient) outputResource;
          final String subjectReference = patient.getId();
          if (subjectReference != null) {
            int index = subjectReference.lastIndexOf("/");
            this.patientId = subjectReference.substring(index + 1);
            LOG.debug("patientId : " + this.patientId);
          }

        } else if (outputResource instanceof DocumentReference) {
          final DocumentReference documentReference = (DocumentReference) entry.getResource();

          final List<DocumentReference.DocumentReferenceContentComponent> contentList = documentReference.getContent();
          if (contentList.isEmpty() == false) {
            final DocumentReference.DocumentReferenceContentComponent component = contentList.get(0);
            this.pdfForm = component.getAttachment().getDataElement().getValueAsString();
          }

          final String[] portions = documentReference.getId().split(("/"));
          if (portions.length > 0) {
            this.orderId = portions[portions.length - 1];
          }
        }
      } else if (EXTENSION_FULLURL.equals(entry.getFullUrl())) {
        final List<Extension> extensions = entry.getExtensionsByUrl(MHANGO_USER_TYPE_URI);
        if (extensions != null && extensions.size() == 1) {
          final Extension extension = extensions.get(0);
          final Type type = extension.getValue();
          if (type instanceof StringType) {
            patientLifeLabsStatus = "yes".equalsIgnoreCase(((StringType) type).asStringValue());
          }
        }
      }
    }
  }

  private void parseOperationOutcome(OperationOutcome opOutcome) {
    // Operation Outcome

    for (OperationOutcomeIssueComponent issue : opOutcome.getIssue()) {
      if (issue.getSeverity().equals(IssueSeverity.INFORMATION)) {
        informationalMessages.add(issue.getDetails().getText());
        LOG.debug("informational msg: " + issue.getDetails().getText());

      } else {
        errors.add(issue.getDetails().getText());
      }

    }
  }


  private String getOrderJsonPayload(final Bundle bundle) {
    ByteArrayOutputStream writer = new ByteArrayOutputStream();
    try {
      final JsonParser parser = new JsonParser();
      parser.compose(writer, bundle);
    } catch (Exception e) {
      LOG.error("Error converting Parameters to JSON.", e);
      throw new ServiceException("Error converting Parameters to JSON.");
    }
    String payload = null;
    try {
      payload = new String(writer.toByteArray(), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return payload;
  }
}
