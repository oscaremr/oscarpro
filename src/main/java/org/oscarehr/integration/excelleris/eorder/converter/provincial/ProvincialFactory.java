/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.excelleris.eorder.converter.provincial;

import java.util.Map;

import org.oscarehr.common.model.EFormValue;

public class ProvincialFactory {

  private static final String FORM_PROVINCE_FIELD = "M_ExcellerisEOrderType";

  public ProvincialFactory(final Map<String, EFormValue> valueMap) {

    m_province = getProvince(valueMap);

    switch (m_province) {
      case ON:
        m_provinceBusinessLogic = new EOrderOntario(m_province, valueMap);
        break;

      case BC:
        m_provinceBusinessLogic = new EOrderBritishColumbia(m_province, valueMap);
        break;
    }
  }


  public ProvincialBusinessLogic getProvincialBusinessLogic() {
    return m_provinceBusinessLogic;
  }


  private Province getProvince(final Map<String, EFormValue> valueMap) {

    final EFormValue provinceValue = valueMap.get(FORM_PROVINCE_FIELD);
    final String provinceName = provinceValue == null ? "ON" : provinceValue.getVarValue();
    return Province.valueOf(provinceName);
  }

  private Province m_province = null;
  private ProvincialBusinessLogic m_provinceBusinessLogic = null;
}
