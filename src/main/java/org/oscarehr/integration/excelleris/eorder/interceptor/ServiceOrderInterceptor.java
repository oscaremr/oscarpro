/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.interceptor;

import ca.uhn.fhir.interceptor.api.Hook;
import ca.uhn.fhir.interceptor.api.Pointcut;
import ca.uhn.fhir.rest.client.api.IHttpRequest;

public class ServiceOrderInterceptor {

  @Hook(Pointcut.CLIENT_REQUEST)
  public void eOrderRequestPreProcessor(final IHttpRequest request) {

    final String[] urlComponents = request.getUri().toString().split("/api/");
    final StringBuilder sb = new StringBuilder();
    sb.append(urlComponents[0]).append("/api/").append("ServiceRequest");
    request.setUri(sb.toString());
  }
}
