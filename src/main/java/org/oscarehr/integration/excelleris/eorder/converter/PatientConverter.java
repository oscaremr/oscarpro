/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hl7.fhir.r4.model.Address;
import org.hl7.fhir.r4.model.ContactPoint;
import org.hl7.fhir.r4.model.DateType;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Patient;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialBusinessLogic;
import org.oscarehr.util.MiscUtils;

public class PatientConverter {

  private static final Logger LOG = MiscUtils.getLogger();

  public static Patient toFhirObject(final ProvincialBusinessLogic provincial) {

    final Map<String, EFormValue> valueMap = provincial.getValueMap();

    final Patient patient = new Patient();

    // Name
    String lastName = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_LAST_NAME);
    if (StringUtils.isEmpty(lastName)) {
      LOG.warn("Missing patient's last name.");
      throw new ServiceException("Patient's last name is required.");
    }
    String firstName = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_FIRST_NAME);
    if (StringUtils.isEmpty(firstName)) {
      LOG.warn("Missing patient's first name.");
      throw new ServiceException("Patient's first name is required.");
    }
    String middleName = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_MIDDLE_NAME);

    // -- title
    String title = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_TITLE);
    if (StringUtils.isEmpty(title)) {
      // eOrder api requires the field even if it is not set, so we have to put UNK there to workaround
      title = "UNK";
    }
    HumanName name = FhirResourceHelper.createHumanName(firstName, lastName, middleName, title);
    patient.addName(name);

    // Gender
    String femaleValue = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_GENDER_FEMALE);
    // female
    if (femaleValue != null) {
      patient.setGender(Enumerations.AdministrativeGender.FEMALE);
    }
    // male
    String maleValue = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_GENDER_MALE);
    if (maleValue != null) {
      patient.setGender(Enumerations.AdministrativeGender.MALE);
    }
    if (StringUtils.isEmpty(femaleValue) && StringUtils.isEmpty(maleValue)) {
      String genderValue = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_GENDER);
      if (StringUtils.isEmpty(genderValue)) {
        LOG.warn("Missing patient's gender, mapped to unknown.");
        patient.setGender(Enumerations.AdministrativeGender.UNKNOWN);
      } else {
        if ("Male".equalsIgnoreCase(genderValue)) {
          patient.setGender(Enumerations.AdministrativeGender.MALE);
        } else if ("Female".equalsIgnoreCase(genderValue)) {
          patient.setGender(Enumerations.AdministrativeGender.FEMALE);
        } else {
          patient.setGender(Enumerations.AdministrativeGender.OTHER);
        }
      }
    }

    // Dob
    String dob = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_DOB);
    if (dob != null) {
      LOG.debug("Patient's DOB: " + dob);
      // dob from eForm is formatted in yyyy/MM/dd

      final String[] dateElements = dob.split("/");
      final DateType dateType = new DateType(Integer.parseInt(dateElements[0]),
                                             Integer.parseInt(dateElements[1]) - 1,
                                             Integer.parseInt(dateElements[2]));

      patient.setBirthDateElement(dateType);
    } else {
      LOG.warn("Missing patient's date of birth.");
      throw new ServiceException("Patient's date of birth is required.");
    }

    // Address
    Address address = FhirResourceHelper.createAddress(valueMap);
    patient.addAddress(address);

    // Telecom
    ContactPoint contactPoint = null;
    String phoneNumber = EFormValueHelper.getValue(valueMap, PatientPropertySet.PATIENT_PHONE);
    if (StringUtils.isEmpty(phoneNumber)) {
      LOG.warn("Missing patient's phone number.");
      throw new ServiceException("Patient's phone number is required.");
    }
    contactPoint = FhirResourceHelper.createContactPoint(valueMap);
    patient.addTelecom(contactPoint);

    // Add work
    contactPoint = FhirResourceHelper.createSecondaryContactPoint(valueMap);
    if (contactPoint != null) {
      patient.addTelecom(contactPoint);
    }

    // Add cell
    contactPoint = FhirResourceHelper.createCellContactPoint(valueMap);
    if (contactPoint != null) {
      patient.addTelecom(contactPoint);
    }


    // extension (optional)

    // Excelleris patient id
    String excellerisPatientId = EFormValueHelper.getValue(valueMap, PatientPropertySet.EXCELLERIS_PATIENT_ID);
    if (excellerisPatientId != null && excellerisPatientId.trim().length() > 0) {
      patient.setId(excellerisPatientId);
    } else {
      // Excelleris does not accept null property represented as not setting the property as
      // specified by https://www.hl7.org/fhir/DSTU2/json.html
      // Workaround to put a special empty string in there and have the fhir-api-excelleris code
      // to output a ""
      patient.setId("");//JsonParser.OUTPUT_NULL_AS_EMPTY_STRING);
    }

    //
    // Process specific provincial insurance business logic
    //
    provincial.processHealthInsuranceLogic(patient);

    return patient;
  }
}
