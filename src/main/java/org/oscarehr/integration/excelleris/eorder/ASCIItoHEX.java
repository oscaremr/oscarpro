/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;

// Java program to convert ASCII
// string to Hexadecimal format string
//
//  This class is to compensate for a defect with the eOrder API where it accepts the hexidecimal values and
//  not the proper identifiers.
//
//     For example, eOrder will find practitioner 59454C353238303641 which is the HEX value of YEL52806A,
//     but it will not find YEL52806A
//
//  The short term goal is to convert to HEX as a short term shim to fix the problem until the eOrder API can be fixed
//

public class ASCIItoHEX {
  // function to convert ASCII to HEX
  public static String toHex(String ascii) {

    // Initialize final String
    String hex = "";

    // Make a loop to iterate through
    // every character of ascii string
    for (int i = 0; i < ascii.length(); i++) {

      // take a char from
      // position i of string
      char ch = ascii.charAt(i);

      // cast char to integer and
      // find its ascii value
      int in = (int) ch;

      // change this ascii value
      // integer to hexadecimal value
      String part = Integer.toHexString(in);

      // add this hexadecimal value
      // to final string.
      hex += part;
    }

    // return the final string hex
    return hex.toUpperCase();
  }

  // Driver Function
  public static void main(String arg[]) {
    // print the Hex String
    System.out.println(toHex("Geek"));
  }
}
