/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

public class PractitionerPropertySet {

  // The names of the following properties are from apconfig.xml, we must use the exact name
  // on the Lab Requisition form in order to have the fields populated using data from provider related tables.
  public static final String PRACTITIONER_NAME = "doctor";
  public static final String PRACTITIONER_OHIP_NO = "doctor_ohip_no";
  public static final String PRACTITIONER_CPSID = "doctor_cpsid";
  public static final String ORDERING_PROVIDER_EXCELLERIS_ID = "doctor_excelleris_id";
  public static final String COPYTO_PROVIDER_EXCELLERIS_ID = "copy_to_doctor_1_excelleris_id";
  public static final String ORDERING_PROVIDER_LIFELAB_ID = "doctor_lifelab_id";
  public static final String PRACTITIONER_PHONE = "doctor_work_phone";
  public static final String PRACTITIONER_FIRST_NAME = "doctor_first_name";
  public static final String PRACTITIONER_LAST_NAME = "doctor_last_name";
  public static final String PRACTITIONER_ADDRESS = "doctor_address";
  public static final String PRACTITIONER_TITLE = "doctor_title";

  public static final String COPY_TO_PROVIDER_PREFIX = "copy_to_doctor_";

  public static final String getCopyToProviderFirstNamePropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_first_name";
  }

  public static final String getCopyToProviderLastNamePropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_last_name";
  }

  public static final String getCopyToProviderExcellerisIdPropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_excelleris_id";
  }

  public static final String getCopyToProviderLifeLabsIdPropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_lifelab_id";
  }

  public static final String getCopyToProviderTitlePropertyNameByIndex(Integer index) {
    return COPY_TO_PROVIDER_PREFIX + index + "_title";
  }
}
