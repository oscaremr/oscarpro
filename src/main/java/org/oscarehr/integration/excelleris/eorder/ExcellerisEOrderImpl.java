/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;

import static org.oscarehr.integration.excelleris.eorder.HttpClientHelper.EXCELLERIS_EORDER_AZURE_AFFINITY_COOKIE_NAME;
import static org.oscarehr.integration.excelleris.eorder.converter.PractitionerConverter.PRACTITIONER_ROLE_TYPE;
import static org.oscarehr.integration.excelleris.eorder.converter.PractitionerConverter.PRACTITIONER_TYPE;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import lombok.val;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.Resource;
import org.hl7.fhir.r4.model.ValueSet;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.interceptor.BundleToPractitionerInterceptor;
import org.oscarehr.integration.excelleris.eorder.interceptor.ServiceOrderInterceptor;
import org.oscarehr.integration.excelleris.eorder.interceptor.ValidateOrderInterceptor;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.SearchStyleEnum;
import ca.uhn.fhir.rest.client.apache.ApacheRestfulClientFactory;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.ServerValidationModeEnum;
import ca.uhn.fhir.rest.client.interceptor.AdditionalRequestHeadersInterceptor;
import ca.uhn.fhir.rest.client.interceptor.BearerTokenAuthInterceptor;
import ca.uhn.fhir.rest.client.interceptor.CookieInterceptor;
import ca.uhn.fhir.rest.server.exceptions.BaseServerResponseException;
import ca.uhn.fhir.rest.server.exceptions.ForbiddenOperationException;
import net.sf.json.JSONObject;
import oscar.OscarProperties;

@Slf4j
@Component
public class ExcellerisEOrderImpl implements ExcellerisEOrder {

  private static final String EXCELLERIS_EORDER_AUTHENTICATION_TOKEN = "EXCELLERIS_EORDER_AUTHENTICATION_TOKEN";

  public ExcellerisEOrderImpl() {
    m_properties = OscarProperties.getInstance();
    initialize();
  }


  public ExcellerisEOrderImpl(final Properties properties) {
    m_properties = properties;
    initialize();
  }

  public void initialize() {
    log.info("Loading FhirContext");
    m_context = FhirContext.forR4();
    m_context.getRestfulClientFactory().setConnectTimeout(3 * 60 * 1000);
    m_context.getRestfulClientFactory().setSocketTimeout(3 * 60 * 1000);

    m_proxyEnabled = new Boolean(m_properties.getProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_HAPI_PROXY_ENABLE, PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_ENABLE));
    m_proxyHost = m_properties.getProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_HAPI_PROXY_HOST, PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_HOST);
    m_proxyPort = new Integer(m_properties.getProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_HAPI_PROXY_PORT, PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_PORT));
    m_eOrderBaseUrl = m_properties.getProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_BASE_URL_PARAM_NAME, PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_BASE_URL);
  }

  /**
   * Get a new Authentication Token (API 2.0) from a session attribute.  If the token in session does not exist
   * or has expired, this method will create (and store) a new Authentication Token
   *
   * @return AuthenticationToken object
   * @throws ServiceException if error occurs
   */
  @Override
  public AuthenticationToken getAuthenticationToken(final HttpServletRequest request) {
    AuthenticationToken authToken = getSessionAuthToken(request);
    if (authToken == null || authToken.hasExpired() || isInvalidateToken()) {
      authToken = this.authenticate();

      if (authToken != null && !authToken.hasExpired()) {
        setSessionAuthToken(request, authToken);
        setInvalidateToken(false);
      }
    }

    return authToken;
  }


  /**
   * Get a new Authentication Token (API 2.0).
   *
   * @return AuthenticationToken object
   * @throws ServiceException if error occurs
   */
  @Override
  public AuthenticationToken authenticate() throws ServiceException {

    final String loginUrl = m_properties.getProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_LOGIN_URL_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_LOGIN_URL);
    final String username = m_properties.getProperty(ConfigureConstants.EXCELLERIS_USER_V2_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_USER);
    final String password = m_properties.getProperty(ConfigureConstants.EXCELLERIS_USER_V2_PASSWORD_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_USER_PASSWORD);
    final String province = m_properties.getProperty(ConfigureConstants.EXCELLERIS_USER_V2_PROVINCE_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_USER_PROVINCE);

    final String isAffinityDisabled = m_properties.getProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_INSTANCE_AFFINITY_DISABLE,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_INSTANCE_AFFINITY_DISABLE);

    AuthenticationToken authenticationToken = null;
    try {
      final HttpClient httpClient = buildHttpClient();
      final String domain = loginUrl.substring("https://".length()).split("/")[0];

      final HttpClientContext httpContext = HttpClientHelper.createHttpContext(domain, isAffinityDisabled);

      log.info("V2 Login URL: |" + loginUrl + "|");
      final URIBuilder uriBuilder = new URIBuilder(loginUrl);
      final List<NameValuePair> params = new ArrayList<NameValuePair>();
      params.add(new BasicNameValuePair("version", "2.0"));
      uriBuilder.addParameters(params);

      final HttpPost httpPost = new HttpPost(uriBuilder.build());

      final UserCredential userCredential = new UserCredential(username, password, province);
      final ObjectMapper mapper = new ObjectMapper();
      final String content = mapper.writeValueAsString(userCredential);

      final StringEntity entity = new StringEntity(content);
      httpPost.setHeader("Content-type", "text/json");
      httpPost.setEntity(entity);

      long startTimeMs = System.currentTimeMillis();

      final HttpResponse response = httpClient.execute(httpPost, httpContext);

      final HttpEntity responseEntity = response.getEntity();
      if (response.getStatusLine().getStatusCode() != 200) {
        log.error(response.getStatusLine().toString());
        throw new ServiceException(response.getStatusLine().toString());
      }
      String token = "";
      long expiresIn = 0;
      if (responseEntity != null) {
        final String responseString = EntityUtils.toString(responseEntity, "UTF-8");
        log.info("V2 responseString: " + responseString);
        final JSONObject tokenObj = JSONObject.fromObject(responseString).getJSONObject("token");
        token = tokenObj.getString("access_token");
        expiresIn = tokenObj.getLong("expires_in");

        authenticationToken = new AuthenticationToken(token, expiresIn);

        final List<Cookie> cookieList = httpContext.getCookieStore().getCookies();
        for (final Cookie cookie : cookieList) {
          if (EXCELLERIS_EORDER_AZURE_AFFINITY_COOKIE_NAME.equals(cookie.getName()) == false) {
            authenticationToken.addCookie(cookie);
          }
        }
      }
      long endTimeMs = System.currentTimeMillis();
      long durationTimeMs = (endTimeMs - startTimeMs);
      log.info("Authentication call duration " + durationTimeMs + " ms");

    } catch (ConnectTimeoutException ex) {
      log.warn("Connection timeout when connecting to Excelleris authentication server.", ex);
      throw new ServiceException("There is a problem communicating with the Excelleris eOrder service. Click the eOrder button to try again. If issue persists, please revert to your usual process of generating lab requisitions outside of the eOrder module and report the error to your system administrator.", ex);
    } catch (Exception ex) {
      log.error("Error login to Excelleris eOrder authentication server.", ex);
      if (ex.getMessage() != null && ex.getMessage().toLowerCase().contains("read timed out")) {
        throw new ServiceException("There is a problem communicating with the Excelleris eOrder service. Click the eOrder button to try again. If issue persists, please revert to your usual process of generating lab requisitions outside of the eOrder module and report the error to your system administrator.", ex);
      }
      throw new ServiceException("Error login to Excelleris eOrder authentication server.", ex);
    }

    return authenticationToken;
  }


  /**
   * Returns a list of properties associated with each search hit that was obtained for the provided
   * family name.
   *
   * @param authToken  - Token to use when calling Excelleris eOrder APIs
   * @param givenName  - Given name of the practitioner
   * @param familyName - Family name of the practitioner
   * @return list of practitioner objects
   */
  @Override
  public List<Resource> searchPractitioners(final AuthenticationToken authToken, final String givenName, final String familyName) {

    final List<Resource> practitionerList = new ArrayList<>();

    final IGenericClient client = configureClient(authToken);
//      client.registerInterceptor(new NoSearchResultsInterceptor()); // Odd....intereceptor has stopped working

    Bundle results = null;
    try {
      results = client.search()
          .forResource(Practitioner.class)
          .usingStyle(SearchStyleEnum.POST)
          .where(Practitioner.FAMILY.matches().values(familyName))
          .and(Practitioner.GIVEN.matches().values(givenName))
          .returnBundle(Bundle.class)
          .execute();

      for (final Bundle.BundleEntryComponent entry : results.getEntry()) {
        if (entry.hasResource()) {
          final Resource resource = entry.getResource();
          if (resource.getResourceType().toString().equals(PRACTITIONER_TYPE)) {
            final val practitioner = (Practitioner) resource;
            final String[] portions = practitioner.getId().split("/");
            if (portions.length == 2) {
              practitioner.setId(portions[1]);
            }
            practitionerList.add(practitioner);

          } else if (resource.getResourceType().toString().equals(PRACTITIONER_ROLE_TYPE)) {
            practitionerList.add(entry.getResource());
          }
        }
      }

    } catch (Exception exception) {
      log.debug("Unable to find practitioner(s)");
    }

    return practitionerList;
  }


  /**
   * Returns a practitioner identified by id
   *
   * @param authToken      authentication token.
   * @param practitionerId Excelleris id of practitioner
   * @return practitioner object
   */
  @Override
  public Practitioner getPractitioner(final AuthenticationToken authToken, final String practitionerId) {
    return getPractitioner(authToken, practitionerId, false);
  }


  /**
   * Returns a practitioner identified by id
   *
   * @param authToken           authentication token.
   * @param practitionerId      Excelleris id of practitioner
   * @param isHexRepresentation - True if hex value of excelleris id, false if excelleris value only
   * @return practitioner object
   */
  @Override
  public Practitioner getPractitioner(final AuthenticationToken authToken, final String practitionerId, boolean isHexRepresentation) {

    String hexPractitionerId = null;
    if (isHexRepresentation) {
      hexPractitionerId = practitionerId;
    } else {
      hexPractitionerId = ASCIItoHEX.toHex(practitionerId);  // excelleris is currently only working for HEX equiavalents of the id
    }

    final IGenericClient client = configureClient(authToken);
    client.registerInterceptor(new BundleToPractitionerInterceptor(m_context));

    Practitioner practitioner = null;
    try {
      practitioner = client.read().resource(Practitioner.class).withId(hexPractitionerId).withAdditionalHeader("Version", "2.0").execute();
      if (practitioner != null) {
        final String[] portions = practitioner.getId().split("/");
        if (portions.length == 2) {
          practitioner.setId(portions[1]);
        }
      }

    } catch (Exception exception) {
      log.debug("Cannot find Practitioner " + practitionerId);
    }

    return practitioner;
  }


  /**
   * Retrieves patient known to excelleris by id
   *
   * @param authToken auth token used to retrieve patient information
   * @param patientId Excelleris patient id
   * @return Patient structure
   */
  @Override
  public Patient getPatient(final AuthenticationToken authToken, final String patientId) {

    final IGenericClient client = configureClient(authToken);
    client.registerInterceptor(new BundleToPractitionerInterceptor(m_context));

    Patient patient = null;
    try {
      patient = client.read().resource(Patient.class).withId(patientId).withAdditionalHeader("Version", "2.0").execute();

    } catch (Exception exception) {
      log.debug("Cannot find Patient " + patientId);
    }

    return patient;
  }

  /**
   * Returns the most recent list of test codes
   *
   * @param authToken Auth token to use to retrieve test codes
   * @return list of test objects
   */
  @Override
  public List<ValueSet.ConceptReferenceComponent> retrieveTestCodes(final AuthenticationToken authToken) {

    final List<ValueSet.ConceptReferenceComponent> testCodes = new ArrayList<>();

    final IGenericClient client = configureClient(authToken);

    final ValueSet results = client.read()
        .resource(ValueSet.class)
        .withUrl(m_eOrderBaseUrl + "/Metadata/ValueSet/ordercode?includeAll=true")
        .execute();

    if (results == null || results.getCompose().isEmpty() || results.getCompose().getInclude().isEmpty()) {
      log.error("Unable to retrieve test codes");

    } else {
      if (results.getCompose().isEmpty() == false && results.getCompose().getInclude().isEmpty() == false) {
        final Iterator<ValueSet.ConceptSetComponent> conceptIterator = results.getCompose().getInclude().iterator();
        while (conceptIterator.hasNext()) {
          for (final ValueSet.ConceptReferenceComponent component : conceptIterator.next().getConcept()) {
            testCodes.add(component);
          }
        }
      }
    }

    return testCodes;
  }


  /**
   * Validate eOrder
   *
   * @param authToken Auth token to use to perform validation
   * @param bundle    FHIR bundle to validate
   * @return Structure to provide results of validation
   */
  @Override
  public Bundle validatetExcellerisEOrder(final AuthenticationToken authToken, final Bundle bundle) {

    Bundle response = null;
    try {
      final IGenericClient client = configureClient(authToken);
      client.registerInterceptor(new ValidateOrderInterceptor());

      response = client.operation()
          .processMessage()
          .setMessageBundle(bundle)
          .synchronous(Bundle.class)
          .execute();

    } catch (Exception exception) {
      exception.printStackTrace();
      throw new ServiceException("Unable to contact server to validate eOrder");
    }

    return response;
  }


  /**
   * Submit eOrder
   *
   * @param authToken Auth token to use for submission
   * @param bundle    FHIR bundle to submit
   * @return Bundle structure to provide results of submission ; may be success, failure or a Questionnaire that needs to be answered
   */
  @Override
  public Bundle submitExcellerisEOrder(final AuthenticationToken authToken, final Bundle bundle) {

    // Enable for debugging
    //
    String requestStringContent = null;
    String responseStringContent = null;

    if (log.isDebugEnabled() && bundle != null) {
      requestStringContent = m_context.newJsonParser().encodeResourceToString(bundle);
      final String filename =
          storeDebugInformation("request.json", jsonPrettyPrint(requestStringContent));
      log.debug("eOrder Request stored at: " + filename);
      log.debug(jsonPrettyPrint(requestStringContent));
    }

    long startTimeMs = 0;
    long endTimeMs = 0;
    long durationTimeMs = 0;
    Bundle response = null;
    try {
      final IGenericClient client = configureClient(authToken);
      client.registerInterceptor(new ServiceOrderInterceptor());

      startTimeMs = System.currentTimeMillis();
      
      // The actual submission of the Excelleris eOrder occurs at this point.
      response = client.operation()
              .processMessage()
              .setMessageBundle(bundle)
              .synchronous(Bundle.class)
              .execute();
      
    } catch (final ForbiddenOperationException forbiddenOperationException) {
      setInvalidateToken(true);
      if (log.isDebugEnabled()) {
        log.debug("eOrder submit failed due to ForbiddenOperation: \n" + forbiddenOperationException.getMessage());
      }

      throw forbiddenOperationException;

    } catch (final BaseServerResponseException baseServerResponseException) {

      log.debug("ResponseBody : " + baseServerResponseException.getResponseBody());
      baseServerResponseException.printStackTrace();
    	
      if (log.isDebugEnabled()) {
        log.debug("eOrder submit failed : \n" + baseServerResponseException.getMessage());
      }

      throw baseServerResponseException;

    } catch (final Exception exception) {
      if (log.isDebugEnabled()) {
        log.debug("eOrder Failure Reason: \n" + exception.getMessage());
      }

      throw new ServiceException("Unable to contact server to submit eOrder");

    } finally {
      endTimeMs = System.currentTimeMillis();
      durationTimeMs = (endTimeMs - startTimeMs);

      // Enable for debugging
      //
      if (log.isDebugEnabled() && response != null) {
        responseStringContent = m_context.newJsonParser().encodeResourceToString(response);
        final String filename =
            storeDebugInformation("response.json", jsonPrettyPrint(responseStringContent));
        log.debug("eOrder Response stored at: " + filename);
        log.debug(jsonPrettyPrint(responseStringContent));
      }

      log.info("eOrder submit call duration " + durationTimeMs + " ms");
    }

    return response;
  }


  //
  // Private Methods
  //

  private HttpClient buildHttpClient() throws Exception {
    HttpClient httpClient = null;

    final String isClientSSLEnabled = m_properties.getProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_CLIENT_SSL_ENABLE,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_CLIENT_SSL_ENABLE);

    if (Boolean.valueOf(isClientSSLEnabled)) {
      final Map<String, String> cookieMap = new HashMap<>();
      httpClient = HttpClientHelper.createHttpClient(cookieMap);

    } else {
      httpClient = HttpClientHelper.createDefaultHttpClient();
    }

    return httpClient;
  }


  private IGenericClient configureClient(final AuthenticationToken authToken) {

    IGenericClient client = null;
    try {
      final ApacheRestfulClientFactory clientFactory = new ApacheRestfulClientFactory(m_context);

      clientFactory.setHttpClient(buildHttpClient());

      m_context.setRestfulClientFactory(clientFactory);
      m_context.getRestfulClientFactory().setServerValidationMode(ServerValidationModeEnum.NEVER);

      // Is debugging via proxy (like Fiddler) enabled?
      // If so, configure host and port for the proxy
      //
      if (m_proxyEnabled) {
        m_context.getRestfulClientFactory().setProxy("127.0.0.1", 8888);
      }

      // Disable validation call to /metadata as eOrder does not implement
      //
      client = m_context.newRestfulGenericClient(m_eOrderBaseUrl);

      // Configure Headers
      //
      final AdditionalRequestHeadersInterceptor interceptor = new AdditionalRequestHeadersInterceptor();
      interceptor.addHeaderValue("Version", "2.0");
      client.registerInterceptor(interceptor);

      final BearerTokenAuthInterceptor authInterceptor = new BearerTokenAuthInterceptor(authToken.getToken());
      client.registerInterceptor(authInterceptor);

      for (final Cookie cookie : authToken.getCookieList()) {
        final CookieInterceptor cookieInterceptor = new CookieInterceptor(cookie.getName() + "=" + cookie.getValue());
        client.registerInterceptor(cookieInterceptor);
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }

    return client;

  }

  private AuthenticationToken getSessionAuthToken(final HttpServletRequest request) {
    AuthenticationToken authToken = null;
    final String serializedToken = (String) request.getSession().getAttribute(EXCELLERIS_EORDER_AUTHENTICATION_TOKEN);
    if (serializedToken != null) {
      authToken = AuthenticationToken.deserialize(serializedToken);
      if (authToken == null || authToken.hasExpired()) {
        request.getSession().removeAttribute(EXCELLERIS_EORDER_AUTHENTICATION_TOKEN);
      }
    }
    return authToken;
  }

  private void setSessionAuthToken(final HttpServletRequest request, final AuthenticationToken authToken) {
    if (authToken.hasExpired() == false) {
      final String serializedToken = AuthenticationToken.serialized(authToken);
      if (serializedToken != null) {
        request.getSession().setAttribute(EXCELLERIS_EORDER_AUTHENTICATION_TOKEN, serializedToken);
      }
    }
  }


  private String jsonPrettyPrint(final String jsnString) {
    if (m_gson == null) {
      m_gson = new GsonBuilder().setPrettyPrinting().create();
    }

    final JsonParser jsonParser = new JsonParser();
    final JsonElement jsonElement = jsonParser.parse(jsnString);

    return m_gson.toJson(jsonElement);
  }

  private String storeDebugInformation(final String filename, final String content) {
    final String documentsFolder = oscarProperties.getProperty("BASE_DOCUMENT_DIR") != null ? oscarProperties.getProperty("BASE_DOCUMENT_DIR") : System.getProperty("java.io.tmpdir");

    final File rootDirectory = new File(documentsFolder, "debug");
    rootDirectory.mkdirs();
    final File contentFilename = new File(rootDirectory, filename);

    try {
      Files.write(Paths.get(contentFilename.getAbsolutePath()), content.getBytes());
    } catch (IOException ex) {
      log.error("Cannot save debug content");
    }

    return contentFilename.getAbsolutePath();
  }

  private synchronized void setInvalidateToken(final boolean status) {
    m_invalidateToken = status;
  }

  private synchronized boolean isInvalidateToken() {
    return m_invalidateToken;
  }

  private boolean m_invalidateToken = false;
  private FhirContext m_context = null;
  private Properties m_properties = null;
  private Boolean m_proxyEnabled = null;
  private String m_proxyHost = null;
  private Integer m_proxyPort = null;
  private String m_eOrderBaseUrl = null;
  private Gson m_gson = null;
  private OscarProperties oscarProperties = OscarProperties.getInstance();
}
