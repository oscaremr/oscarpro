/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.excelleris.eorder.converter.provincial;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.hl7.fhir.r4.model.BooleanType;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.PrimitiveType;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.hl7.fhir.r4.model.ResourceType;
import org.hl7.fhir.r4.model.ServiceRequest;
import org.hl7.fhir.r4.model.StringType;
import org.oscarehr.common.dao.OrderLabTestCodeCacheDao;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.HttpClientHelper;
import org.oscarehr.integration.excelleris.eorder.converter.ConverterHelper;
import org.oscarehr.integration.excelleris.eorder.converter.FhirResourceHelper;
import org.oscarehr.integration.excelleris.eorder.converter.QuestionTextHelper;
import org.oscarehr.integration.excelleris.eorder.converter.ResourceUrlHelper;
import org.oscarehr.util.MiscUtils;
import oscar.util.StringUtils;

abstract public class ProvincialBusinessLogic {

  private static final Logger LOG = MiscUtils.getLogger();

  public static final String LAB_TEST_PREFIX = "T_";
  public static final String OTHER_TESTS = "T_other_tests_";
  protected static final String ANSWER_UNKNOWN = "UNK";
  protected static final String TEST_NAME_TO_EXCELLERIS_TEST_CODE_MAPPING_FILE = "/ExcellerisTestCodeMapping_%PROVINCE%.properties";

  protected static final String QUESTION_RESPONSE_PREFIX = "QR_q";

  protected ProvincialBusinessLogic(final Province province, final Map<String, EFormValue> valueMap) {
    m_province = province;
    m_valueMap = valueMap;
  }

  public void expandTests() {
  }

  abstract public void processHealthInsuranceLogic(Patient patient);

  abstract public Extension createPayerExtension();

  abstract public List<Extension> createTestGlobalExceptions(Map<String, EFormValue> valueMap);

  abstract public boolean isExtensionQRNeeded(String key);

  abstract public boolean isTest(final String key);

  abstract public List<Extension> getOrderExtensionList(final String key);

  abstract public String getTestCode(String key, QuestionnaireResponse questionnaireResponse, String itemUuid, Questionnaire questionnaire);

  abstract public boolean isOtherMicrobiology(String key);

  abstract protected String getMappingFileName();

  public QuestionnaireResponse processExtensionQR(final Map<String, EFormValue> valueMap, final String key, QuestionnaireResponse questionnaireResponse) {
    return questionnaireResponse;
  }

  public Map<String, EFormValue> getValueMap() {
    return m_valueMap;
  }

  public Properties retrieveTestCodeProperties() {

    Properties properties = new Properties();
    ConverterHelper.addLabTestCodesToProperties(properties);

    return properties;
  }

  public Properties retrieveTestCodeProperties(final OrderLabTestCodeCacheDao orderLabTestCodeCacheDao) {

    final String testCodeMappingFile = getMappingFileName();

    // Load test code mapping file
    InputStream inputStream = HttpClientHelper.class.getResourceAsStream(testCodeMappingFile);
    Properties properties = new Properties();
    try {
      properties.load(inputStream);
    } catch (IOException e) {
      LOG.error("Error loading " + testCodeMappingFile, e);
      throw new ServiceException("Error loading " + testCodeMappingFile);
    }

    ConverterHelper.setOrderLabTestCodeCacheDao(orderLabTestCodeCacheDao);
    ConverterHelper.addLabTestCodesToProperties(properties);

    return properties;
  }


  public boolean isOtherTests(String key) {
    // code to determine if this is one of 'Other Tests;
    if (key.contains(OTHER_TESTS)) {
      return true;
    } else {
      return false;
    }
  }

  public boolean isSpecialTests(String key) {
    return false;
  }

  public boolean isHardCodedTest(String key) {
    if (isOtherTests(key)) {
      return false;
    }
    return true;
  }

  public QuestionnaireResponse createQuestionnaireResponse(Map<String, EFormValue> valueMap,
                                                           QuestionnaireResponse existingQR,
                                                           Questionnaire questionnaire,
                                                           Map<String, List<String>> testCodeToKeyAndItemIdMap) {
    if (valueMap == null) {
      LOG.error("Null value map cannot create response");
      throw new ServiceException("Null value map cannot create response");
    }

    if (questionnaire == null) {
      LOG.error("Null questionnaire cannot create response");
      throw new ServiceException("Null questionnaire cannot create response");
    }

    List<String> questions = new ArrayList<String>();
    List<String> answers = new ArrayList<String>();
    List<String> questionTexts = new ArrayList<String>();

    int j = 1;
    for (final Questionnaire.QuestionnaireItemComponent question : questionnaire.getItem()) {
      EFormValue answerValue = valueMap.get(QUESTION_RESPONSE_PREFIX + j);
      String testCode = getTestCodeFromLinkId(question.getLinkId());
      if (testCodeToKeyAndItemIdMap.containsKey(testCode) && testCodeToKeyAndItemIdMap.get(testCode) != null) {
        String key = testCodeToKeyAndItemIdMap.get(testCode).get(0);
        if (answerValue != null) {
          answers.add(answerValue.getVarValue());
          if (isHardCodedTest(key)) {
            String linkId = question.getLinkId();
            linkId = linkId + "|" + testCodeToKeyAndItemIdMap.get(testCode).get(1);
            questions.add(linkId);
          } else {
            questions.add(question.getLinkId());
          }
          questionTexts.add(question.getText());
        }
      }
      j++;
    }
    if (questions.size() > 0 && answers.size() > 0) {
      return FhirResourceHelper.createQuestionnaireResponse(existingQR, questions, answers, questionTexts);
    } else {
      return existingQR;
    }
  }



  public QuestionnaireResponse createQuestionnaireResponse(final Bundle bundle,
                                                           final Questionnaire questionnaire) {
    QuestionnaireResponse questionnaireResponse = new QuestionnaireResponse();

    final Map<String, List<String>> testCodeToKeyAndItemIdMap = new HashMap<>();
    for(final Bundle.BundleEntryComponent bec : bundle.getEntry()) {
      if (bec.getResource() != null 
          && bec.getResource().getResourceType() != null
          && bec.getResource().getResourceType().equals(ResourceType.ServiceRequest)) {
        final ServiceRequest serviceRequest = (ServiceRequest) bec.getResource();
        for (final CodeableConcept codeableConcept : serviceRequest.getOrderDetail()) {
          for (final Coding coding : codeableConcept.getCoding()) {
            final String key = getTestcode(codeableConcept);
            final List<String> keyItemId = new ArrayList<String>();
            keyItemId.add(0, key);
            keyItemId.add(1, codeableConcept.getId());
            testCodeToKeyAndItemIdMap.put(coding.getCode(), keyItemId);
          }
        }
      }
    }

    questionnaireResponse = createQuestionnaireResponse(getValueMap(), questionnaireResponse, questionnaire, testCodeToKeyAndItemIdMap);

    return questionnaireResponse;
  }



  public List<Extension> createProvincialSpecificExtensions() {
    final List<Extension> extensionList = new ArrayList<>();

    return extensionList;
  }


  public Extension createDiagnosisNotesExtension() {

    Extension extension = null;
    // diagnosis/notes
    if (getValueMap().containsKey("EO_Additional_Clinic_Info")) {
      final EFormValue eformValue = getValueMap().get("EO_Additional_Clinic_Info");
      final String diagnosis = eformValue.getVarValue();
      //convert to url format - user URI to convert the content to replace space with %20 etc
      try {
        final URI uri = new URI("dummy", diagnosis, null);
        extension = FhirResourceHelper.createStringExtension(diagnosis, ResourceUrlHelper.getDiagnosisExtensionUrl());

      } catch (URISyntaxException e) {
        LOG.error("Additional Clinical Information");
        throw new ServiceException("Additional Clinical Information");
      }
    }

    return extension;
  }


  public Extension createSubjectNotesExtension() {

    Extension extension = null;
    // diagnosis/notes
    if (getValueMap().containsKey("EO_Subject_Notes_Info")) {
      final EFormValue eformValue = getValueMap().get("EO_Subject_Notes_Info");
      final String subjectNotes = eformValue.getVarValue();
      //convert to url format - user URI to convert the content to replace space with %20 etc
      try {
        final URI uri = new URI("dummy", subjectNotes, null);
        extension = FhirResourceHelper.createStringExtension(subjectNotes, ResourceUrlHelper.getSubjectNotesExtensionUrl());

      } catch (URISyntaxException e) {
        LOG.error("Subject Notes Information");
        throw new ServiceException("Subject Notes Information");
      }
    }

    return extension;
  }

  public Extension createCopyToProviderExtension(final Map<String, EFormValue> valueMap, final String orderingProviderExcellerisId) {

    // copy to provider
    // automatically add ordering provider as copy to provider
    return FhirResourceHelper.createValueReferenceExtension(
        ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), orderingProviderExcellerisId),
        ResourceUrlHelper.getCopyToRecipientExtensionUrl());
  }

  //
  // Protected
  //

  /**
   * Create questionnaire response for test requiring source info
   *
   * @param valueMap         eform value hash map
   * @param testCode
   * @param answerKey
   * @param questionLinkUuid
   * @param existingQR
   * @return questionnaire response for the specific test
   */
  protected static QuestionnaireResponse createTestSourceQuestionnaireResponse(final Map<String, EFormValue> valueMap,
                                                                               final String testCode,
                                                                               final String answerKey,
                                                                               final String questionLinkUuid,
                                                                               final QuestionnaireResponse existingQR) {

    // need to send source information
    // as questionnaire response

    // source
    List<String> questions = new ArrayList<>();
    List<String> answers = new ArrayList<>();
    String question = QuestionTextHelper.getSourceQuestionLinkId(testCode, questionLinkUuid);

    questions.add(question);
    String answerValue = EFormValueHelper.getValue(valueMap, answerKey);
    if (StringUtils.empty(answerValue)) {
      answerValue = ANSWER_UNKNOWN;
    }
    answers.add(answerValue);

    QuestionnaireResponse questionnaireResponse = FhirResourceHelper.createQuestionnaireResponse(existingQR, questions, answers);

    return questionnaireResponse;
  }


  /**
   * Create questionnaire response for global extensions
   *
   * @param valueMap                  eform value hash map
   * @param existingQR                - Existing questionnaire response
   * @param globalPropertyLabel       - Label in QR.  ie for Anticoagulent this would be "OnAnticoagulent:"
   * @param globalProperty            - property to identifify this questionnaire
   * @param globalPropertyDetailLabel - Label in QR.  ie for Anticoagulent this would be "Anticoagulant Name:"
   * @param globalPropertyDetail      - property name from form for the detail. ie for anticoagulent this would be the 'specify' field
   * @return questionnaire response for the global extension
   */
  protected QuestionnaireResponse createGlobalExtensionQuestionnaireResponse(final Map<String, EFormValue> valueMap,
                                                                             final QuestionnaireResponse existingQR,
                                                                             String globalPropertyLabel,
                                                                             String globalProperty,
                                                                             String globalPropertyDetailLabel,
                                                                             String globalPropertyDetail) {

    final List<String> questions = new ArrayList<>();
    final List<PrimitiveType> answers = new ArrayList<>();

    String question = QuestionTextHelper.getGlobalQuestionLinkId(globalPropertyLabel);
    questions.add(question);
    String answerValue = EFormValueHelper.getValue(valueMap, globalProperty);
    answers.add(new BooleanType(StringUtils.empty(answerValue) ? false : true));

    question = QuestionTextHelper.getGlobalQuestionLinkId(globalPropertyDetailLabel);
    questions.add(question);
    answerValue = EFormValueHelper.getValue(valueMap, globalPropertyDetail);
    if (StringUtils.empty(answerValue)) {
      answerValue = ANSWER_UNKNOWN;
    }
    answers.add(new StringType(answerValue));

    final QuestionnaireResponse questionnaireResponse = FhirResourceHelper.createQuestionnairePrimitiveResponse(existingQR, questions, answers);

    return questionnaireResponse;
  }


  protected boolean getTestStatus(final String key) {
    boolean status = false;
    final EFormValue eFormValue = getValueMap().get(key);
    if (eFormValue != null && eFormValue.getVarValue().equals("on")) {
      status = true;
    }

    return status;
  }


  protected String getTestCodeFromLinkId(String linkId) {
    String[] linkIdArray = linkId.split("\\|");
    return linkIdArray[2];
  }

  private String getTestcode(final CodeableConcept codeableConcept) {
    if (codeableConcept.getExtension() != null) {
      for(final Extension extension : codeableConcept.getExtension()) {
        if (ResourceUrlHelper.getTestIdentifierExtensionUrl().equals(extension.getUrl())) {
          final CodeableConcept extensionValue = (CodeableConcept) extension.getValue();
          if (extensionValue != null && extensionValue.getCoding() != null && extensionValue.getCoding().size() > 0) {
            return extensionValue.getCoding().get(0).getCode();
          }
        }
      }
    }

    return "unknown test code";
  }
  
  public Province getProvince() {
    return m_province;
  }

  public void setProvince(Province province) {
    this.m_province = province;
  }

  private Province m_province = null;
  private Map<String, EFormValue> m_valueMap = null;
}
