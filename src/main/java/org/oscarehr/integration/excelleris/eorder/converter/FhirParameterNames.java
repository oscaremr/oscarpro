/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

public class FhirParameterNames {

  public static final String ORDER_BUNDLE_NAME = "OrderBundle";
  public static final String QUESTIONNAIRE_RESOURCE_NAME = "QuestionnaireResponse";

}
