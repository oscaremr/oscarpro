/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.testcodes;

import java.util.List;

import org.apache.log4j.Logger;
import org.hl7.fhir.r4.model.ValueSet;
import org.oscarehr.integration.excelleris.eorder.AuthenticationToken;
import org.oscarehr.integration.excelleris.eorder.ExcellerisEOrder;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class LabTestCodesJob implements Job {

  private static final Logger LOG = MiscUtils.getLogger();

  public void execute(JobExecutionContext ctx) throws JobExecutionException {
    //	RestServicesHelper.getAndSaveTestCodes();

    LOG.info("Loading test codes from REST service...");

    final ExcellerisEOrder excellerisEOrder = (ExcellerisEOrder) SpringUtils.getBean("excellerisEOrder");
    final AuthenticationToken authToken = excellerisEOrder.authenticate();
    if (authToken != null && !authToken.hasExpired()) {
      final List<ValueSet.ConceptReferenceComponent> testCodeList = excellerisEOrder.retrieveTestCodes(authToken);
      LabCodesHelper.refreshTestCodes(testCodeList);
    }

    LOG.info("Check and potential update of test codes complete");
  }
}