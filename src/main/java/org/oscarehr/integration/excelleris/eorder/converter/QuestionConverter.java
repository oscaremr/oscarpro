/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.Questionnaire.QuestionnaireItemComponent;
import org.oscarehr.common.dao.OrderLabTestCodeCacheDao;
import org.oscarehr.common.model.OrderLabTestCodeCache;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.api.Choice;
import org.oscarehr.integration.excelleris.eorder.api.Question;
import org.oscarehr.integration.excelleris.eorder.api.QuestionType;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialBusinessLogic;
import org.oscarehr.util.MiscUtils;
import oscar.util.StringUtils;

public class QuestionConverter {
	
	private static final Logger LOG = MiscUtils.getLogger();

  public static Question toApiObject(final QuestionnaireItemComponent fhirQuestion,
                                     final ProvincialBusinessLogic provincial,
                                     final OrderLabTestCodeCacheDao orderLabTestCodeDao) {

    if (fhirQuestion == null) {
      LOG.error("Null Fhir Question");
      throw new ServiceException("Cannot convert null Fhir Question");
    }

    if (provincial == null || provincial.getValueMap() == null) {
      LOG.error("Null value map");
      throw new ServiceException("Cannot convert Fhir Question");
    }

    Question apiQuestion = new Question();
    apiQuestion.setType(QuestionType.valueOf(fhirQuestion.getType().toString()));

    apiQuestion.setRequired(fhirQuestion.getRequired());

    apiQuestion.setQuestionText(fhirQuestion.getText());
    apiQuestion.setLinkId(fhirQuestion.getLinkId());
    ArrayList<Choice> choices = new ArrayList<Choice>();

    List<Questionnaire.QuestionnaireItemAnswerOptionComponent> answerList = fhirQuestion.getAnswerOption();
    for (Questionnaire.QuestionnaireItemAnswerOptionComponent answer : answerList) {
      Coding coding = (Coding) answer.getValue();
      choices.add(new Choice(coding.getCode(), coding.getDisplay()));
    }

//		for (Coding coding : fhirQuestion.getCode()) {
//			choices.add(new Choice(coding.getCode(), coding.getDisplay()));
//
//		}
    apiQuestion.setChoices(choices);

		// TODO: EORDER - This is what was done for the BC Changes: Implement a friendly solution based
		//  on new workflow
		/*
		// Load test code mapping file
    final Properties properties = provincial.retrieveTestCodeProperties(orderLabTestCodeDao);
    ConverterHelper.setOrderLabTestCodeCacheDao(orderLabTestCodeDao);
    ConverterHelper.addLabTestCodesToProperties(properties);
		 */
		Properties properties = new Properties();
		ConverterHelper.setOrderLabTestCodeCacheDao(orderLabTestCodeDao);
		properties = ConverterHelper.addLabTestCodesToProperties();

    for (Entry<Object, Object> entry : properties.entrySet()) {
      if (apiQuestion.getTestCode().equals(entry.getValue())) {
        String hardCodedTestFieldId = (String) entry.getKey();
        if (provincial.getValueMap().containsKey(hardCodedTestFieldId)) {
          apiQuestion.setHardCodedTestFieldId(hardCodedTestFieldId);
        }
        break;
      }
    }

    String province = provincial.getProvince().name();
    OrderLabTestCodeCache orderLabTestCodeCache =
        orderLabTestCodeDao.findByTestCodeAndProvince(apiQuestion.getTestCode(),
            province);
    if (orderLabTestCodeCache != null) {
      apiQuestion.setTestName(orderLabTestCodeCache.getTestName());
      if (StringUtils.isNullOrEmpty(apiQuestion.getHardCodedTestFieldId())) {
        apiQuestion.setOtherTestFieldId(orderLabTestCodeCache.getTestName());
      }
    }

    return apiQuestion;
  }
}
