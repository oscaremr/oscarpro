/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.interceptor;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.interceptor.api.Hook;
import ca.uhn.fhir.interceptor.api.Interceptor;
import ca.uhn.fhir.interceptor.api.Pointcut;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.client.apache.ApacheHttpResponse;
import ca.uhn.fhir.rest.client.api.IHttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.log4j.Logger;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Resource;
import org.oscarehr.util.MiscUtils;

@Interceptor
public class BundleToPatientInterceptor {
  public static String PATIENT_RESOURCE = "Patient";

  private static final Logger LOG = MiscUtils.getLogger();

  public BundleToPatientInterceptor(final FhirContext fhirContext) {
    m_fhirContext = fhirContext;
  }

  @Hook(Pointcut.CLIENT_RESPONSE)
  public void eOrderResponsePreProcessor(final IHttpResponse resp) {
    LOG.debug("response intercepted");
    final IParser parser = m_fhirContext.newJsonParser();
    Patient patient = null;

    try {
      if (resp.getStatus() == 200) {
        if (ContentType.APPLICATION_JSON.getMimeType().equals(resp.getMimeType())) {
          final InputStream excellerisResponse = ((ApacheHttpResponse) resp).getResponse().getEntity().getContent();

          patient = parseReponse(parser, excellerisResponse);
        }

        String modifiedResponse = null;
        if (patient != null) {
          modifiedResponse = parser.encodeResourceToString(patient);
        } else {
          modifiedResponse = "{}";
          ((ApacheHttpResponse) resp).getResponse().setStatusCode(404);
        }

        ((ApacheHttpResponse) resp).getResponse().setEntity(new StringEntity(modifiedResponse, ContentType.APPLICATION_JSON));
      }
    } catch (IOException e) {
      LOG.debug("Exception finding patient : " + e.getMessage());
    }
  }


  private Patient parseReponse(final IParser parser, final InputStream contentStream) throws IOException {
    final StringBuilder textBuilder = new StringBuilder();
    try (Reader reader = new BufferedReader(new InputStreamReader
        (contentStream, Charset.forName(StandardCharsets.UTF_8.name())))) {
      int c = 0;
      while ((c = reader.read()) != -1) {
        textBuilder.append((char) c);
      }
    }

    Bundle bundle = parser.parseResource(Bundle.class, textBuilder.toString());
    List<Bundle.BundleEntryComponent> entryList = bundle.getEntry();
    Patient patient = null;
    if (entryList.size() > 0) {
      int patientCount = 0;
      for (final Bundle.BundleEntryComponent entry : entryList) {
        if (entry.hasResource()) {
          final Resource resource = entry.getResource();
          if (resource.hasId() && resource.hasType(PATIENT_RESOURCE)) {
            patient = (Patient) entry.getResource();
            patientCount++;
          }
        }
      }

      if (patientCount != 1) {
        patient = null;
      }
    }

    return patient;
  }

  private FhirContext m_fhirContext;
}

