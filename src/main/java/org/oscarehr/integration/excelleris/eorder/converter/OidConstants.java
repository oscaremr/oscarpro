/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

public class OidConstants {

  public static final String HEALTH_INSURANCE_NUMBER_OID = "2.16.840.1.113883.3.1772.1.10";
  public static final String PRACTITIONER_EXCELLERIS_ID_OID = "2.16.840.1.113883.3.1772.1.0";
  public static final String PRACTITIONER_LIFE_LABS_ID_OID = "2.16.840.1.113883.3.1772.1.150";
}
