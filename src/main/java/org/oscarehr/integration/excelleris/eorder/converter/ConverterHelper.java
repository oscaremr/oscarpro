/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import lombok.val;
import org.oscarehr.common.dao.DynacareEorderLabTestCodeDao;
import org.oscarehr.common.dao.OrderLabTestCodeCacheDao;
import org.oscarehr.common.dao.OrderLabTestSourceDao;
import org.oscarehr.common.model.DynacareEorderLabTestCode;
import org.oscarehr.common.model.OrderLabTestCodeCache;
import org.oscarehr.common.model.OrderLabTestCodeSource;
import org.oscarehr.util.SpringUtils;

public class ConverterHelper {

  /**
   * Add persisted lab test codes to Properties object.
   *
   * @param properties the Properties object
   */
  // TODO: EORDER - Function directly below was already in master-all, please update accordingly to meet new workflow
  public static Properties addLabTestCodesToProperties() {
    val properties = new Properties();
    List<OrderLabTestCodeCache> list =
        getOrderLabTestCodeCacheDao().findAll(0, OrderLabTestCodeCacheDao.MAX_LIST_RETURN_SIZE);
    for (OrderLabTestCodeCache code : list) {
      properties.setProperty(code.getKeyName(), code.getLabTestCode());
    }
    return properties;
  }
  // TODO: EORDER - addLabTestCodesToProperties, addLabTestCodesToPropertiesFromLabTestCode, convertKey added via OSCAR-4577, please update accordingly for new workflow/as needed
  public static Properties addLabTestCodesToProperties(Properties properties) {
    if (properties != null) {
      List<OrderLabTestCodeCache> list = getOrderLabTestCodeCacheDao().findAll(0, OrderLabTestCodeCacheDao.MAX_LIST_RETURN_SIZE);
      for (OrderLabTestCodeCache code : list) {
        properties.setProperty(code.getKeyName(), code.getLabTestCode());
      }
    }
    return properties;
  }
  /**
   * Add persisted lab test codes to Properties object.
   *
   * @param properties the Properties object
   */
  public static Properties addLabTestCodesToPropertiesFromLabTestCode(Properties properties,
      DynacareEorderLabTestCodeDao testCodeDao) {
    if (properties != null) {
      List<DynacareEorderLabTestCode> testCodeList = testCodeDao.findByAll();
      for (DynacareEorderLabTestCode code : testCodeList) {
        properties.setProperty(convertKey(code.getLongName()), code.getLabTestCode());
      }
    }
    return properties;
  }
  private static String convertKey(String key) {
    String result = null;
    if (key != null) {
      String invalidCharactersRegex = "[ \\,\\-\\/\\+\\.\\(\\)\\[\\]]";
      result = "T_" + key.replaceAll(invalidCharactersRegex, "_");
      result = org.apache.commons.lang3.StringUtils.left(result, 28);
    }
    return result;
  }


  public static String getTestCodeBySourceName(final String province, final String fieldId, final String sourceName) {
    return getOrderLabTestSourceDao().findBySourceName(province, fieldId, sourceName).getTestCode();
  }


  public static OrderLabTestCodeSource getSourceByFieldId(final String province, final String fieldId) {
    return getOrderLabTestSourceDao().findByFieldId(province, fieldId);
  }

  /**
   * Create a simple typed array
   *
   * @param theObjectList list of objects to add to the list
   */
  public static <T> List<T> createArray(T... theObjectList) {
    final List<T> typeArray = new ArrayList<>();

    for (final T element : theObjectList) {
      if (element != null) {
        typeArray.add(element);
      }
    }

    return typeArray;
  }


  private static OrderLabTestCodeCacheDao getOrderLabTestCodeCacheDao() {
    if (s_orderLabTestCodeCacheDao == null) {
      s_orderLabTestCodeCacheDao = SpringUtils.getBean(OrderLabTestCodeCacheDao.class);
    }

    return s_orderLabTestCodeCacheDao;
  }

  public static void setOrderLabTestCodeCacheDao(final OrderLabTestCodeCacheDao orderLabTestCodeCacheDao) {
    s_orderLabTestCodeCacheDao = orderLabTestCodeCacheDao;
  }

	/**
	   * Creates a key name with the following properties:
	   *
	   *  <key name> = T_<name> where
	   *	- <name> = <name> with space replaced with _
	   *	- truncate <name> to 28 characters long (if necessary)
	   *
	   * @param name the name to modify
	   * @return key name the processed name
	   */

	public static String createKeyName(String name) {
		String result = null;
		if (name != null) {
			String invalidCharactersRegex = "[ \\,\\-\\/\\+\\.\\(\\)\\[\\]]";
			result = "T_" + name.replaceAll(invalidCharactersRegex, "_");
			result = org.apache.commons.lang3.StringUtils.left(result, 28);
		}
		return result;
	}

  private static OrderLabTestSourceDao getOrderLabTestSourceDao() {
    if (s_orderLabTestSourceDao == null) {
      s_orderLabTestSourceDao = SpringUtils.getBean(OrderLabTestSourceDao.class);
    }

    return s_orderLabTestSourceDao;
  }


  static OrderLabTestCodeCacheDao s_orderLabTestCodeCacheDao = null;
  static OrderLabTestSourceDao s_orderLabTestSourceDao = null;
}