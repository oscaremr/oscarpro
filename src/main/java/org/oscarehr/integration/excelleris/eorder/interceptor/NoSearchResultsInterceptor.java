/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.interceptor;

import ca.uhn.fhir.interceptor.api.Hook;
import ca.uhn.fhir.interceptor.api.Interceptor;
import ca.uhn.fhir.interceptor.api.Pointcut;
import ca.uhn.fhir.rest.client.apache.ApacheHttpResponse;
import ca.uhn.fhir.rest.client.api.IHttpResponse;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;

@Interceptor
public class NoSearchResultsInterceptor {

  public static String EMPTY_SEARCH_RESULT = "{\"resourceType\":\"Bundle\",\"type\":\"searchset\",\"total\":0,\"entry\":[]}";

  private static final Logger LOG = MiscUtils.getLogger();

  @Hook(Pointcut.CLIENT_RESPONSE)
  public void eOrderResponsePreProcessor(final IHttpResponse resp) {
    if (resp.getStatus() == 200 && ContentType.TEXT_PLAIN.getMimeType().equals(resp.getMimeType())) {

      String modifiedResponse = "{\"resourceType\":\"Bundle\",\"type\":\"searchset\",\"total\":0,\"entry\":[]}";

      ((ApacheHttpResponse) resp).getResponse().setEntity(new StringEntity(modifiedResponse, ContentType.APPLICATION_JSON));
    }
  }
}
