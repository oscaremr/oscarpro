/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;

import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;
import oscar.OscarProperties;

public class RestServicesHelper {

  private static final Logger LOG = MiscUtils.getLogger();

  // ---------------------------------------------------------------------- Private Methods

  /**
   * Get Excelleris eOrder service url
   *
   * @param endPoint the URL endpoint
   * @return URL as string
   */
  private static String getURL(String endPoint) {
    if (OscarProperties.getInstance() != null) {
      return OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_BASE_URL_PARAM_NAME) + endPoint;
    } else {
      LOG.warn("Using DEFAULT_EXCELLERIS_EORDER_BASE_URL");
      return PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_BASE_URL + endPoint;
    }
  }
}