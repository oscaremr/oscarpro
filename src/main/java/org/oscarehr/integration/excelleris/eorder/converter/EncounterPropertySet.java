/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

public class EncounterPropertySet {

  // Property names used for exchange information in-memory
  public static final String ENCOUNTER_URL = "encounter_url";
}
