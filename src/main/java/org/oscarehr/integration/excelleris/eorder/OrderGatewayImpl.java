/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;


import javax.servlet.http.HttpServletRequest;
import java.util.List;

import org.apache.log4j.Logger;
import org.hl7.fhir.r4.model.Resource;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.util.MiscUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.oscarehr.integration.eOrder.common.ServiceException;

@Component
public class OrderGatewayImpl implements OrderGateway {

  private static final Logger LOG = MiscUtils.getLogger();


  @Autowired
  private OrderService orderService;
  @Autowired
  private ExcellerisEOrder excellerisEOrder;

  public OrderGatewayImpl() {
  }

  @Autowired
  public OrderGatewayImpl(final ExcellerisEOrder authenticationService, final OrderService orderService) {
    super();
    this.excellerisEOrder = authenticationService;
    this.orderService = orderService;
  }

  @Override
  public AuthenticationTokenAndResource createOrder(final HttpServletRequest request, final EFormData eformData,
          final List<EFormValue> eformValues) throws ServiceException {

    LOG.trace("Calling OrderGatewayImpl.createOrder");
    long startTime = System.currentTimeMillis();


    // Do not reuse the token since token expiration interface is not well defined.
    long authenticationStartTime = System.currentTimeMillis();
    final AuthenticationToken authToken = excellerisEOrder.getAuthenticationToken(request);

    long authenticationEndTime = System.currentTimeMillis();

    // Create order
    //
    long createOrderStartTime = System.currentTimeMillis();
    final Resource orderResponse = orderService.createOrder(authToken, eformData, eformValues);


    long endTime = System.currentTimeMillis();

    LOG.info("+++++++++++++++++ Performance - Authentication : " + (authenticationEndTime - authenticationStartTime));
    LOG.info("+++++++++++++++++ Performance - Create Order : " + (endTime - createOrderStartTime));
    LOG.info("+++++++++++++++++ Performance - Overall : " + (endTime - startTime));

    return new AuthenticationTokenAndResource(authToken, orderResponse);
  }
}
