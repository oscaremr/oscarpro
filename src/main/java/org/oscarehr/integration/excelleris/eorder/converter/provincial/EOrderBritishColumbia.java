/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.excelleris.eorder.converter.provincial;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.log4j.Logger;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.OrderLabTestCodeSource;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.converter.ConverterHelper;
import org.oscarehr.integration.excelleris.eorder.converter.DiagnosticOrderConverter;
import org.oscarehr.integration.excelleris.eorder.converter.FhirResourceHelper;
import org.oscarehr.integration.excelleris.eorder.converter.PatientPropertySet;
import org.oscarehr.integration.excelleris.eorder.converter.QuestionTextHelper;
import org.oscarehr.integration.excelleris.eorder.converter.ResourceUrlHelper;
import org.oscarehr.integration.excelleris.eorder.testcodes.LabCodesHelper;
import org.oscarehr.util.MiscUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EOrderBritishColumbia extends ProvincialBusinessLogic {

  private static final Logger LOG = MiscUtils.getLogger();

  private static final String SPECIAL_EXPANSION_TEST_PREFIX = "SET_";

  private static final String ANTIBIOTICS = "T_antibiotics_";
  private static final String ANTIBIOTICS_YES = "T_antibiotics_yes";
  private static final String ANTIBIOTICS_NO = "T_antibiotics_no";
  private static final String ANTIBIOTICS_YES_SOURCE = "T_antibiotics_src";
  private static final String ANTICOAGULANTS = "T_anticoagulant_";
  private static final String ANTICOAGULANT_YES = "T_anticoagulant_yes";
  private static final String ANTICOAGULANT_NO = "T_anticoagulant_no";
  private static final String ANTICOAGULANT_YES_SOURCE = "T_anticoagulant_src";
  private static final String SUPERFICIAL_WOUND = "T_superficial_wound";
  private static final String SUPERFICIAL_WOUND_SOURCE = "T_superficial_wound_src";
  private static final String DEEP_WOUND = "T_deep_wound";
  private static final String DEEP_WOUND_SOURCE = "T_deep_wound_src";
  private static final String ROUTINE_CULTURE_OTHER = "T_rc_other";
  private static final String ROUTINE_CULTURE_OTHER_SOURCE = "T_rc_other_src";
  private static final String TRICHOMONAS = "T_trichomonas_testing";
  private static final String TRICHOMONAS_URINE = "T_trichomonas_urine";
  private static final String BLOODY_STOOL_HISTORY = "T_bloody_stools";
  private static final String NON_NOMINAL_REPORTING = "T_non_nominal";
  private static final String STOOL_OVA = "T_stool_ova";
  private static final String STOOL_OVA_HIGH_RISK = "T_stool_ova_hr";
  private static final String DERM_SPECIMEN = "T_specimen_";
  private static final String DERM_SPECIMEN_SKIN = "T_specimen_skin";
  private static final String DERM_SPECIMEN_NAIL = "T_specimen_nail";
  private static final String DERM_SPECIMEN_HAIR = "T_specimen_hair";
  private static final String DERMATOPHYTES_CULTURE = "T_dermatophyte_culture";
  private static final String DERMATOPHYTES_KOH_PREP = "T_koh_prep";
  private static final String DERMATOPHYTES_SOURCE = "T_dermatophytes_src";
  private static final String MYCOLOGY = "T_mycology_";
  private static final String MYCOLOGY_YEAST = "T_mycology_yeast";
  private static final String MYCOLOGY_FUNGUS = "T_mycology_fungus";
  private static final String MYCOLOGY_SOURCE = "T_mycology_src";
  private static final String CHLAMYDIA_GONORRHEA = "T_ctgc_";
  private static final String CHLAMYDIA_GONORRHEA_OTHER = "T_ctgc_other";
  private static final String CHLAMYDIA_GONORRHEA_OTHER_SOURCE = "T_ctgc_other_src";
  private static final String SPECIAL_EXPANSION_TEST_CHLAMYDIA = "SET_ctgc_special_";
  private static final String GONORRHEA = "T_gc_";
  private static final String GONORRHEA_OTHER = "T_gc_other";
  private static final String GONORRHEA_OTHER_SOURCE = "T_gc_other_src";
  private static final String SPECIAL_EXPANSION_TEST_GONORRHEA = "SET_gc_special_";
  private static final String THYROID = "T_thyroid";
  private static final String HYPER_THYROID = "T_hyperthyroid";
  private static final String HYPO_THYROID = "T_hypothyroid";
  private static final String SPECIAL_EXPANSION_TEST_THYROID_1 = "SET_thyroid_special_1";
  private static final String SPECIAL_EXPANSION_TEST_THYROID_2 = "SET_thyroid_special_2";
  private static final String SPECIAL_EXPANSION_TEST_THYROID_3 = "SET_thyroid_special_3";
  private static final String PSA_KNOWN = "T_psa_billable";
  private static final String PSA_SCREENING = "T_psa_screening";
  private static final String FIT_COPY = "T_fit_copy";
  private static final String FIT_NO_COPY = "T_fit_no_copy";

  // Biller Type OIDs
  public static final String BC_MSP_OID = "2.16.840.1.113883.3.1772.1.2";
  public static final String BC_ICBC_OID = "2.16.840.1.113883.3.1772.1.501";
  public static final String BC_WORKSAFEBC_OID = "2.16.840.1.113883.3.1772.1.503";
  public static final String BC_PATIENT_OID = "2.16.840.1.113883.3.1772.1.502";
  public static final String BC_ACCOUNTS_OID = "2.16.840.1.113883.3.1772.1.504";
  public static final String BC_CORRECTIONS_OID = "2.16.840.1.113883.3.1772.1.505";
  public static final String BC_CANADIANFORCES_OID = "2.16.840.1.113883.3.1772.1.506";
  public static final String BC_DVA_OID = "2.16.840.1.113883.3.1772.1.507";
  public static final String OUT_OF_BC_PROVINCE_OID = "2.16.840.1.113883.3.1772.1.508";
  public static final String BC_RCMP_OID = "2.16.840.1.113883.3.1772.1.509";
  public static final String BC_REFUGEE_OID = "2.16.840.1.113883.3.1772.1.510";

  public static final String OTHER_ACCOUNTS = "Accounts";
  public static final String OTHER_CORRECTIONS = "Corrections";
  public static final String OTHER_CANADIANFORCES = "Canadian Forces";
  public static final String OTHER_DVA = "DVA";
  public static final String OTHER_OUT_OF_PROVINCE = "Out of Province";
  public static final String OTHER_RCMP = "RCMP";
  public static final String OTHER_REFUGEE = "Refugee";
  private static final String COLLECTOR = "T_collector";
  private static final String SELECTED_PROVINCE = "P_billto_other_text_2";
  private static final String OTHER_PAYER_SOURCE = "P_billto_other_text_2";

  private static final String GCO_OTHER_SOURCE = "Other Source";
  private static final String GCO_COLLECTION_DATE_SOURCE = "Collection date (dd-mmm-yyyy):";
  private static final String GCO_COLLECTION_DATE = "EO_gc_col_date";

  public EOrderBritishColumbia(final Province province, final Map<String, EFormValue> valueMap) {
    super(province, valueMap);
  }

  @Override
  protected String getMappingFileName() {
    return TEST_NAME_TO_EXCELLERIS_TEST_CODE_MAPPING_FILE.replace("%PROVINCE%", "BC");
  }

  @Override
  public boolean isExtensionQRNeeded(final String key) {
    return (key.startsWith(ANTICOAGULANTS) ||
        key.startsWith(ANTIBIOTICS)) &&
        key.endsWith("_src") == false;
  }

  @Override
  public boolean isTest(final String key) {
    return (key.startsWith(LAB_TEST_PREFIX) || key.startsWith(SPECIAL_EXPANSION_TEST_PREFIX)) && isRequired(key) && !isExcluded(key);
  }


  @Override
  public List<Extension> getOrderExtensionList(final String key) {
    final List<Extension> extensionList = new ArrayList<>();

    extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getTestIdentifierExtensionUrl(), key,
        ResourceUrlHelper.getTestIdentifierExtensionUrl()));

    // Hard coding extension
    //
    extensionList.add(getHardCodingExtension(key));

    // tie breaker
    String tieBreaker = "0";
    extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemTieBreakerValueSetExtensionUrl(), tieBreaker,
        ResourceUrlHelper.getItemTieBreakerExtensionUrl()));


    // is other microbiology
    String otherMicrobiology = isOtherMicrobiology(key) ? "true" : "false";
    extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemOtherIsOtherMicrobilogyTestValueSetExtensionUrl(), otherMicrobiology,
        ResourceUrlHelper.getItemOtherIsOtherMicrobilogyTestExtensionUrl()));

    if (key.equals(SUPERFICIAL_WOUND)) {
      extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemTestForValueSetExtensionUrl(), "SUPERFICIAL",
          ResourceUrlHelper.getItemTestForExtensionUrl()));
    } else if (key.equals(DEEP_WOUND)) {
      extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemTestForValueSetExtensionUrl(), "DEEP",
          ResourceUrlHelper.getItemTestForExtensionUrl()));
    } else if (key.equals(STOOL_OVA)) {
      extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemTestForValueSetExtensionUrl(), "STOOLOVAANDPARASITE",
          ResourceUrlHelper.getItemTestForExtensionUrl()));
    } else if (key.equals(STOOL_OVA_HIGH_RISK)) {
      extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemTestForValueSetExtensionUrl(), "STOOLOVAANDPARASITEHIGHRISK",
          ResourceUrlHelper.getItemTestForExtensionUrl()));
    } else if (key.equals(MYCOLOGY_YEAST) && getValueMap().containsKey(MYCOLOGY_FUNGUS)) {
      extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemTestForValueSetExtensionUrl(), "YEAST|FUNGUS",
          ResourceUrlHelper.getItemTestForExtensionUrl()));
    } else if (key.equals(MYCOLOGY_YEAST) && getValueMap().containsKey(MYCOLOGY_FUNGUS) == false) {
      extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemTestForValueSetExtensionUrl(), "YEAST",
          ResourceUrlHelper.getItemTestForExtensionUrl()));
    } else if (key.equals(MYCOLOGY_FUNGUS) && getValueMap().containsKey(MYCOLOGY_YEAST) == false) {
      extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemTestForValueSetExtensionUrl(), "FUNGUS",
          ResourceUrlHelper.getItemTestForExtensionUrl()));
    } else if (key.equals(PSA_KNOWN)) {
      extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemTestForValueSetExtensionUrl(), "PSAKNOWN",
          ResourceUrlHelper.getItemTestForExtensionUrl()));
    } else if (key.equals(PSA_SCREENING)) {
      extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemTestForValueSetExtensionUrl(), "PSASCREENING",
          ResourceUrlHelper.getItemTestForExtensionUrl()));
    } else if (key.equals(FIT_COPY)) {
      extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemTestForValueSetExtensionUrl(), "FITWITHCCTOCSP",
          ResourceUrlHelper.getItemTestForExtensionUrl()));
    } else if (key.equals(FIT_NO_COPY)) {
      extensionList.add(FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getItemTestForValueSetExtensionUrl(), "FITWITHNOCCTOCSP",
          ResourceUrlHelper.getItemTestForExtensionUrl()));
    }

    return extensionList;
  }


  @Override
  public List<Extension> createTestGlobalExceptions(final Map<String, EFormValue> valueMap) {
    final List<Extension> extensionList = new ArrayList<>();

    final EFormValue eFormValueAntibioticsYes = getValueMap().get(ANTIBIOTICS_YES);
    final EFormValue eFormValueAntibioticsNo = getValueMap().get(ANTIBIOTICS_NO);
    if (eFormValueAntibioticsYes != null) {
      extensionList.add(FhirResourceHelper.createBooleanExtension(true, ResourceUrlHelper.getOnAntibioticsExtensionUrl()));
    } else if (eFormValueAntibioticsNo != null) {
      extensionList.add(FhirResourceHelper.createBooleanExtension(false, ResourceUrlHelper.getOnAntibioticsExtensionUrl()));
    }
    if (valueMap.get(ANTIBIOTICS_YES_SOURCE) != null) {
      extensionList.add(FhirResourceHelper.createStringExtension(valueMap.get(ANTIBIOTICS_YES_SOURCE).getVarValue(), ResourceUrlHelper.getAntibioticNameExtensionUrl()));
    }

    if (getTestStatus(ANTICOAGULANT_YES) || getTestStatus(ANTICOAGULANT_NO)) {
      extensionList.add(FhirResourceHelper.createBooleanExtension(getTestStatus(ANTICOAGULANT_YES), ResourceUrlHelper.getOnAnticoagulantExtensionUrl()));
    }
    if (valueMap.get(ANTICOAGULANT_YES_SOURCE) != null) {
      extensionList.add(FhirResourceHelper.createStringExtension(valueMap.get(ANTICOAGULANT_YES_SOURCE).getVarValue(), ResourceUrlHelper.getAnticoagulantNameExtensionUrl()));
    }

    extensionList.add(FhirResourceHelper.createBooleanExtension(getTestStatus(BLOODY_STOOL_HISTORY), ResourceUrlHelper.getBloodyStoolsExtensionUrl()));
    extensionList.add(FhirResourceHelper.createBooleanExtension(getTestStatus(NON_NOMINAL_REPORTING), ResourceUrlHelper.getNonNominalReportingExtensionUrl()));

    return extensionList;
  }

  @Override
  public Extension createPayerExtension() {
    final CodeableConcept payerCoding = new CodeableConcept();
    payerCoding.addCoding(new Coding("https://api.excelleris.com/2.0/eorder/ValueSet/payer-type", getPayer(), "Payer for Test"));

    return new Extension("https://api.excelleris.com/2.0/eorder/servicerequest-payertype", payerCoding);
  }


  @Override
  public void processHealthInsuranceLogic(final Patient patient) {
    final String payer = getPayer();

    // Identifier
    String healthInsuranceNumber = EFormValueHelper.getValue(getValueMap(), PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER);

    // Health insurance number retrieved from database includes space + version number
    // remove spaces from health insurance number
    if (healthInsuranceNumber != null) {
      healthInsuranceNumber = healthInsuranceNumber.replace(" ", "");
    }

    // Excelleris does not accept null property represented as not setting the property as
    // specified by https://www.hl7.org/fhir/DSTU2/json.html
    // Workaround to put a special empty string in there and have the fhir-api-excelleris code
    // to output a ""

    if (healthInsuranceNumber == null) {
      healthInsuranceNumber = "";
    }

    if (healthInsuranceNumber != null) {
      OutOfProvinceBilling outOfProvinceBilling = null;
      if (OTHER_OUT_OF_PROVINCE.equals(payer)) {
        final String payerDetail = getValueMap().get(SELECTED_PROVINCE).getVarValue();
        outOfProvinceBilling = getOutOfProvinceBilling(payerDetail);
        if (outOfProvinceBilling == null) {
          throw new ServiceException(payerDetail + " is an invalid province abbreviation");
        }

      } else {
        outOfProvinceBilling = getOutOfProvinceBilling("BC");
      }

      patient.addIdentifier(FhirResourceHelper.createPatientHCNIdentifier("hc", outOfProvinceBilling.getOid(), healthInsuranceNumber, healthInsuranceNumber, outOfProvinceBilling.name()));
    }

    if (getValueMap().containsKey(HEALTH_INSURANCE_MSP)) {
      patient.addIdentifier(FhirResourceHelper.createPatientHCNIdentifier("MSP", BC_MSP_OID, healthInsuranceNumber, healthInsuranceNumber, "BC"));

    } else if (getValueMap().containsKey(HEALTH_INSURANCE_ICBC)) {
      final String payerDetail = (getValueMap().get("P_icbc_ws_hno") != null) ? getValueMap().get("P_icbc_ws_hno").getVarValue() : "";
      patient.addIdentifier(FhirResourceHelper.createPatientHCNIdentifier("ICBC", BC_ICBC_OID, payerDetail, payerDetail, "BC"));

    } else if (getValueMap().containsKey(HEALTH_INSURANCE_WORKSAFE)) {
      final String payerDetail = (getValueMap().get("P_icbc_ws_hno") != null) ? getValueMap().get("P_icbc_ws_hno").getVarValue() : "";
      patient.addIdentifier(FhirResourceHelper.createPatientHCNIdentifier("WorkSafeBC", BC_WORKSAFEBC_OID, payerDetail, payerDetail, "BC"));

    } else if (getValueMap().containsKey(HEALTH_INSURANCE_OTHER)) {
      final String payerDetail = (getValueMap().get(OTHER_PAYER_SOURCE) != null) ? getValueMap().get(OTHER_PAYER_SOURCE).getVarValue() : "";

      if (OTHER_ACCOUNTS.equals(payer)) {
        patient.addIdentifier(FhirResourceHelper.createIdentifier(payer, BC_ACCOUNTS_OID, payerDetail, payerDetail, null));

      } else if (OTHER_CORRECTIONS.equals(payer)) {
        patient.addIdentifier(FhirResourceHelper.createIdentifier(payer, BC_CORRECTIONS_OID, payerDetail, payerDetail, null));

      } else if (OTHER_CANADIANFORCES.equals(payer)) {
        patient.addIdentifier(FhirResourceHelper.createIdentifier(payer, BC_CANADIANFORCES_OID, payerDetail, payerDetail, null));

      } else if (OTHER_DVA.equals(payer)) {
        patient.addIdentifier(FhirResourceHelper.createIdentifier(payer, BC_DVA_OID, payerDetail, payerDetail, null));

      } else if (OTHER_OUT_OF_PROVINCE.equals(payer)) {
        patient.addIdentifier(FhirResourceHelper.createIdentifier(payer, OUT_OF_BC_PROVINCE_OID, payerDetail, payerDetail, payerDetail));

      } else if (OTHER_RCMP.equals(payer)) {
        patient.addIdentifier(FhirResourceHelper.createIdentifier(payer, BC_RCMP_OID, payerDetail, payerDetail, null));

      } else if (OTHER_REFUGEE.equals(payer)) {
        patient.addIdentifier(FhirResourceHelper.createIdentifier(payer, BC_REFUGEE_OID, payerDetail, payerDetail, null));
      }

    } else {
      patient.addIdentifier(FhirResourceHelper.createIdentifier("Patient", BC_PATIENT_OID, null, null, null));
    }
  }

  @Override
  public String getTestCode(final String key, QuestionnaireResponse questionnaireResponse, final String itemUuid, final Questionnaire questionnaire) {
    String testCode = null;
    if (isComplexLabCodeMapping(key)) {

      if (key.equals(SUPERFICIAL_WOUND)) {
        final String source = getValueMap().get(SUPERFICIAL_WOUND_SOURCE).getVarValue();
        testCode = ConverterHelper.getTestCodeBySourceName("BC", SUPERFICIAL_WOUND_SOURCE, source);

        questionnaireResponse = createTestSourceQuestionnaireResponse(getValueMap(), testCode, SUPERFICIAL_WOUND_SOURCE, itemUuid, questionnaireResponse);

      } else if (key.equals(DEEP_WOUND)) {
        final String source = getValueMap().get(DEEP_WOUND_SOURCE).getVarValue();
        testCode = ConverterHelper.getTestCodeBySourceName("BC", DEEP_WOUND_SOURCE, source);

        questionnaireResponse = createTestSourceQuestionnaireResponse(getValueMap(), testCode, DEEP_WOUND_SOURCE, itemUuid, questionnaireResponse);

      } else if (key.equals(ROUTINE_CULTURE_OTHER)) {
        final String source = getValueMap().get(ROUTINE_CULTURE_OTHER_SOURCE).getVarValue();
        testCode = ConverterHelper.getTestCodeBySourceName("BC", ROUTINE_CULTURE_OTHER_SOURCE, source);

        questionnaireResponse = createTestSourceQuestionnaireResponse(getValueMap(), testCode, ROUTINE_CULTURE_OTHER_SOURCE, itemUuid, questionnaireResponse);

      } else if (key.equals(DERMATOPHYTES_CULTURE) || key.equals(DERMATOPHYTES_KOH_PREP)) {
        testCode = getDermatophyteTestCode(key);

        questionnaireResponse = createTestSourceQuestionnaireResponse(getValueMap(), testCode, DERMATOPHYTES_SOURCE, itemUuid, questionnaireResponse);

      } else if (key.equals(MYCOLOGY_YEAST) || key.equals(MYCOLOGY_FUNGUS)) {
        testCode = DiagnosticOrderConverter.getTestCode(key, getProvince().name());

        questionnaireResponse = createTestSourceQuestionnaireResponse(getValueMap(), testCode, MYCOLOGY_SOURCE, itemUuid, questionnaireResponse);

      } else if (key.startsWith(CHLAMYDIA_GONORRHEA) || key.startsWith(SPECIAL_EXPANSION_TEST_CHLAMYDIA)) {
        final OrderLabTestCodeSource testCodeSource = ConverterHelper.getSourceByFieldId("BC", key);
        if (testCodeSource != null) {
          questionnaireResponse = createTestSourceQuestionnaireResponse(testCodeSource.getTestCode(), testCodeSource.getSourceName(), itemUuid, questionnaireResponse);
          testCode = testCodeSource.getTestCode();
        }
      } else if (key.startsWith(GONORRHEA) || key.startsWith(SPECIAL_EXPANSION_TEST_GONORRHEA)) {
        final OrderLabTestCodeSource testCodeSource = (key.startsWith(SPECIAL_EXPANSION_TEST_GONORRHEA) ? null : ConverterHelper.getSourceByFieldId("BC", key));
        if (testCodeSource == null) {
          testCode = "GCO";
          questionnaireResponse = createTestSourceQuestionnaireResponse(testCode, "Other", itemUuid, questionnaireResponse);
          String otherSource = (getValueMap().get(GONORRHEA_OTHER_SOURCE) != null) ? getValueMap().get(GONORRHEA_OTHER_SOURCE).getVarValue() : "";
          questionnaireResponse = createTestSourceQuestionnaireResponse(testCode, otherSource, GCO_OTHER_SOURCE, itemUuid, questionnaireResponse);
          questionnaireResponse = createGCOCollectionDateQuestionnaireResponse(getValueMap(), testCode, itemUuid, questionnaireResponse);

        } else {
          questionnaireResponse = createTestSourceQuestionnaireResponse(testCodeSource.getTestCode(), testCodeSource.getSourceName(), itemUuid, questionnaireResponse);
          questionnaireResponse = createGCOCollectionDateQuestionnaireResponse(getValueMap(), testCodeSource.getTestCode(), itemUuid, questionnaireResponse);
          testCode = testCodeSource.getTestCode();
        }
      }

    } else {
      // simple mapping, just look it up from properties file

      // see if it is one of 'Other Tests'
      if (getValueMap().containsKey(THYROID)
          || getValueMap().containsKey(HYPO_THYROID)
          || getValueMap().containsKey(HYPER_THYROID)) {
        OrderLabTestCodeSource orderLabTestCodeSource =
            ConverterHelper.getSourceByFieldId("BC", key);
        testCode = orderLabTestCodeSource.getTestCode();
        log.info("Key : " + key);
      } else if (isSpecialTests(key)) {
        log.info("Key : " + key);
        testCode = DiagnosticOrderConverter.getTestCode(key, getProvince().name());

      } else if (isOtherTests(key)) {
        // handle other test (find out the name of other tests)
        String derivedKey = null;
        //use naming convention to derive key from names of other tests.
        // for other tests, the test name is stored in the eForm value:value
        EFormValue eformValue = getValueMap().get(key);
        derivedKey = eformValue.getVarValue();
        derivedKey = LabCodesHelper.convertKey(derivedKey);
        log.info("Key : " + derivedKey);
        testCode = DiagnosticOrderConverter.getTestCode(derivedKey, getProvince().name());

      } else {
        log.info("Key : " + key);
        testCode = DiagnosticOrderConverter.getTestCode(key, getProvince().name());
      }
    }

    return testCode;
  }

  @Override
  public Extension createCopyToProviderExtension(final Map<String, EFormValue> valueMap, final String orderingProviderExcellerisId) {
    return null;
  }

  @Override
  public boolean isOtherMicrobiology(final String key) {
    return false;
  }


  @Override
  public boolean isOtherTests(String key) {
    // code to determine if this is one of 'Other Tests;
    if (key.contains(OTHER_TESTS) ||
        TRICHOMONAS_URINE.equals(key)) {
      return true;
    } else {
      return false;
    }
  }


  @Override
  public boolean isSpecialTests(String key) {
    // code to determine if this is one of 'Other Tests;
    if (TRICHOMONAS_URINE.equals(key)) {
      return true;
    } else {
      return false;
    }
  }


  @Override
  public void expandTests() {
    EFormValue value = null;

    final Set<String> uniqueAdditionalTests = new HashSet<>();
    if (getValueMap().containsKey(CHLAMYDIA_GONORRHEA_OTHER)) {

      final Set<String> sourceSet = breakoutSources(CHLAMYDIA_GONORRHEA_OTHER_SOURCE);
      for (String source : sourceSet) {
        uniqueAdditionalTests.add(SPECIAL_EXPANSION_TEST_CHLAMYDIA + source.toLowerCase());
      }
    }

    if (getValueMap().containsKey(GONORRHEA_OTHER)) {

      final Set<String> sourceSet = breakoutSources(GONORRHEA_OTHER_SOURCE);
      for (String source : sourceSet) {
        uniqueAdditionalTests.add(SPECIAL_EXPANSION_TEST_GONORRHEA + source.toLowerCase());
      }
      if (sourceSet.size() == 0) {
        uniqueAdditionalTests.add(SPECIAL_EXPANSION_TEST_GONORRHEA + "");
      }
    }

    if (getValueMap().containsKey(THYROID)) {
      uniqueAdditionalTests.add(SPECIAL_EXPANSION_TEST_THYROID_1);
    }

    if (getValueMap().containsKey(HYPO_THYROID)) {
      uniqueAdditionalTests.add(SPECIAL_EXPANSION_TEST_THYROID_1);
      uniqueAdditionalTests.add(SPECIAL_EXPANSION_TEST_THYROID_2);
    }

    if (getValueMap().containsKey(HYPER_THYROID)) {
      uniqueAdditionalTests.add(SPECIAL_EXPANSION_TEST_THYROID_1);
      uniqueAdditionalTests.add(SPECIAL_EXPANSION_TEST_THYROID_2);
      uniqueAdditionalTests.add(SPECIAL_EXPANSION_TEST_THYROID_3);
    }

    for (String testCode : uniqueAdditionalTests) {
      value = new EFormValue(0, 0, 0, testCode, "on");
      getValueMap().put(testCode, value);
    }
  }

  @Override
  public List<Extension> createProvincialSpecificExtensions() {
    final List<Extension> extensionList = new ArrayList<>();

    if (getValueMap().containsKey("P_fasting") && getValueMap().get("P_fasting").getVarValue().equals("on")) {
      extensionList.add(FhirResourceHelper.createBooleanExtension(getValueMap().containsKey("P_fasting"), ResourceUrlHelper.getFastingExtensionUrl()));
    }

    if (getValueMap().containsKey("P_hpc")) {
      extensionList.add(FhirResourceHelper.createIntegerExtension(Integer.valueOf(getValueMap().get("P_hpc").getVarValue()), ResourceUrlHelper.getFastingDurationExtensionUrl()));
    }

    if (getValueMap().containsKey("P_chart_num")) {
      extensionList.add(FhirResourceHelper.createStringExtension(getValueMap().get("P_chart_num").getVarValue(), ResourceUrlHelper.getChartNumberExtensionUrl()));
    }

    if (getValueMap().containsKey("P_room_num")) {
      extensionList.add(FhirResourceHelper.createStringExtension(getValueMap().get("P_room_num").getVarValue(), ResourceUrlHelper.getRoomNumberExtensionUrl()));
    }

    if (getValueMap().containsKey("P_current_med")) {
      extensionList.add(FhirResourceHelper.createStringExtension(getValueMap().get("P_current_med").getVarValue(), ResourceUrlHelper.getCurrentMedicationsExtensionUrl()));
    }

    return extensionList;
  }


  //
  // Private
  //
  private Set<String> breakoutSources(final String sourceName) {
    final Set<String> sourceSet = new HashSet<>();
    final String source = (getValueMap().get(sourceName) != null) ? getValueMap().get(sourceName).getVarValue() : "";

    final String[] items = source.split(",");
    for (String item : items) {
      if (item != null && item.trim().isEmpty() == false) {
        sourceSet.add(item.trim());
      }
    }

    return sourceSet;
  }


  private boolean isExcluded(final String key) {
    return s_testCodeExclusions.contains(key) ||
        key.startsWith(DERM_SPECIMEN) ||
        key.endsWith("_src") ||
        (key.equals(MYCOLOGY_FUNGUS) && getValueMap().containsKey(MYCOLOGY_YEAST));
  }


  private boolean isRequired(final String key) {
    return true;
  }

  private boolean isComplexLabCodeMapping(final String key) {
    // code to determine if lab code mapping is complex or not

    if (key == null) {
      LOG.error("Test Key was null - unable to process");
    }

    if (s_testCodeComplexTestCode.contains(key) ||
        key.startsWith(MYCOLOGY) ||
        key.startsWith(CHLAMYDIA_GONORRHEA) ||
        key.startsWith(GONORRHEA) ||
        key.startsWith(SPECIAL_EXPANSION_TEST_CHLAMYDIA) ||
        key.startsWith(SPECIAL_EXPANSION_TEST_GONORRHEA)) {
      return true;

    } else {
      return false;
    }
  }


  private String getPayer() {
    String payer = null;
    if (getValueMap().containsKey(HEALTH_INSURANCE_MSP)) {
      payer = "MSP";

    } else if (getValueMap().containsKey(HEALTH_INSURANCE_ICBC)) {
      payer = "ICBC";

    } else if (getValueMap().containsKey(HEALTH_INSURANCE_WORKSAFE)) {
      payer = "WorkSafeBC";

    } else if (getValueMap().containsKey(HEALTH_INSURANCE_NONE)) {
      // not insured so has to be patient pay
      payer = "Patient";

    } else if (getValueMap().containsKey(HEALTH_INSURANCE_OTHER)) {
      payer = getValueMap().get("P_billto_other_text_1").getVarValue();
    }

    return payer.trim();
  }


  private String getDermatophyteTestCode(final String key) {
    String testCode = null;
    final String dermatophyteCulture = DERMATOPHYTES_CULTURE.equals(key) ? key : null;
    final String kohPrep = DERMATOPHYTES_KOH_PREP.equals(key) ? key : null;

    if (getValueMap().get(DERM_SPECIMEN_SKIN) != null) {
      testCode = dermatophyteCulture != null ? "DERS" : "KOHS";

    } else if (getValueMap().get(DERM_SPECIMEN_NAIL) != null) {
      testCode = dermatophyteCulture != null ? "DERN" : "KOHN";

    } else if (getValueMap().get(DERM_SPECIMEN_HAIR) != null) {
      testCode = dermatophyteCulture != null ? "DERH" : "KOHH";
    }

    return testCode;
  }

  private Extension getHardCodingExtension(final String key) {

    Extension extension = null;

    // is hard coded test
    String hardCoded = isHardCodedTest(key) ? "1" : "0";
    extension = FhirResourceHelper.createCodeableConceptExtension(
        ResourceUrlHelper.getItemIsHardCodedTestValueSetExtensionUrl(), hardCoded, ResourceUrlHelper.getItemIsHardCodedTestExtensionUrl());

    return extension;
  }


  /**
   * Create questionnaire response for test requiring source info
   *
   * @param testCode
   * @param testSource
   * @param questionLinkUuid
   * @param existingQR
   * @return questionnaire response for the specific test
   */
  private static QuestionnaireResponse createTestSourceQuestionnaireResponse(final String testCode,
                                                                             final String testSource,
                                                                             final String questionLinkUuid,
                                                                             final QuestionnaireResponse existingQR) {

    // need to send source information
    // as questionnaire response

    // source
    List<String> questions = new ArrayList<>();
    List<String> answers = new ArrayList<>();
    String question = QuestionTextHelper.getSourceQuestionLinkId(testCode, questionLinkUuid);

    questions.add(question);
    answers.add(testSource);

    QuestionnaireResponse questionnaireResponse = FhirResourceHelper.createQuestionnaireResponse(existingQR, questions, answers);

    return questionnaireResponse;
  }


  /**
   * Create questionnaire response for test requiring source info
   *
   * @param testCode
   * @param testSource
   * @param questionLinkUuid
   * @param existingQR
   * @return questionnaire response for the specific test
   */
  private static QuestionnaireResponse createTestSourceQuestionnaireResponse(final String testCode,
                                                                             final String testSource,
                                                                             final String testSourceName,
                                                                             final String questionLinkUuid,
                                                                             final QuestionnaireResponse existingQR) {

    // need to send source information
    // as questionnaire response

    // source
    List<String> questions = new ArrayList<>();
    List<String> answers = new ArrayList<>();
    String question = QuestionTextHelper.getSourceQuestionLinkId(testCode, testSourceName, questionLinkUuid);

    questions.add(question);
    answers.add(testSource);

    QuestionnaireResponse questionnaireResponse = FhirResourceHelper.createQuestionnaireResponse(existingQR, questions, answers);

    return questionnaireResponse;
  }

  /**
   * Create questionnaire response for GCO collection date
   *
   * @param valueMap
   * @param questionLinkUuid
   * @param existingQR
   * @return questionnaire response for GCO collection date
   */
  private static QuestionnaireResponse createGCOCollectionDateQuestionnaireResponse(Map<String, EFormValue> valueMap,
                                                                                    final String testCode,
                                                                                    final String questionLinkUuid,
                                                                                    final QuestionnaireResponse existingQR) {
    List<String> questions = new ArrayList<>();
    List<String> questionTexts = new ArrayList<>();
    List<String> answers = new ArrayList<>();
    String question = QuestionTextHelper.getSourceQuestionLinkId(testCode, GCO_COLLECTION_DATE_SOURCE, questionLinkUuid);

    questions.add(question);
    questionTexts.add(GCO_COLLECTION_DATE_SOURCE);

    try {
      final String collectionTime = valueMap.containsKey(GCO_COLLECTION_DATE) ? valueMap.get(GCO_COLLECTION_DATE).getVarValue() : "00:00";
      answers.add(collectionTime);
    } catch (Exception e) {
      LOG.error("Could not parse GCO collection date: '" + valueMap.get(GCO_COLLECTION_DATE).getVarValue() + "'");
    }
    QuestionnaireResponse questionnaireResponse = FhirResourceHelper.createQuestionnaireResponse(existingQR, questions, answers, questionTexts);

    return questionnaireResponse;
  }

  @Override
  public QuestionnaireResponse createQuestionnaireResponse(final Map<String, EFormValue> valueMap,
                                                           final QuestionnaireResponse existingQR,
                                                           final Questionnaire questionnaire,
                                                           final Map<String, List<String>> testCodeToKeyAndItemIdMap) {
    if (valueMap == null) {
      LOG.error("Null value map cannot create response");
      throw new ServiceException("Null value map cannot create response");
    }

    if (questionnaire == null) {
      LOG.error("Null questionnaire cannot create response");
      throw new ServiceException("Null questionnaire cannot create response");
    }

    List<String> questions = new ArrayList<String>();
    List<String> answers = new ArrayList<String>();
    List<String> questionTexts = new ArrayList<String>();

    int j = 1;
    for (final Questionnaire.QuestionnaireItemComponent question : questionnaire.getItem()) {
      EFormValue answerValue = valueMap.get(ProvincialBusinessLogic.QUESTION_RESPONSE_PREFIX + j);
      String testCode = getTestCodeFromLinkId(question.getLinkId());
      if (testCodeToKeyAndItemIdMap.containsKey(testCode) && testCodeToKeyAndItemIdMap.get(testCode) != null) {
        String key = testCodeToKeyAndItemIdMap.get(testCode).get(0);
        answers.add((answerValue != null) ? answerValue.getVarValue() : "");
        questions.add(question.getLinkId() + "|" + testCodeToKeyAndItemIdMap.get(testCode).get(1));
        questionTexts.add(question.getText());
      }
      j++;
    }
    if (questions.size() > 0 && answers.size() > 0) {
      return FhirResourceHelper.createQuestionnaireResponse(existingQR, questions, answers, questionTexts);
    } else {
      return existingQR;
    }
  }

  private OutOfProvinceBilling getOutOfProvinceBilling(final String province) {
    OutOfProvinceBilling outOfProvinceBilling = null;
    try {
      outOfProvinceBilling = OutOfProvinceBilling.valueOf(province);
    } catch (IllegalArgumentException e) {
      LOG.error("Invalid province abbreviation entered for out of province billing");
    }

    return outOfProvinceBilling;
  }

  private Map<String, EFormValue> m_valueMap = null;

  private static Set<String> s_testCodeComplexTestCode = new HashSet(
      Arrays.asList(SUPERFICIAL_WOUND, DEEP_WOUND, ROUTINE_CULTURE_OTHER, DERMATOPHYTES_CULTURE, DERMATOPHYTES_KOH_PREP));

  private static Set<String> s_testCodeExclusions = new HashSet(
      Arrays.asList(ANTICOAGULANT_YES, ANTIBIOTICS_YES, BLOODY_STOOL_HISTORY, NON_NOMINAL_REPORTING, CHLAMYDIA_GONORRHEA_OTHER, GONORRHEA_OTHER, THYROID, HYPO_THYROID, HYPER_THYROID, TRICHOMONAS, COLLECTOR));

  private static Set<String> s_anticoagulentTestCodes = new HashSet(
      Arrays.asList("PT", "PTT", "APTMX", "FIB", "DIM", "PRC*", "PRS*", "AT*", "LAPTT*", "DRVV*"));


  private static final String HEALTH_INSURANCE_MSP = "P_msp";
  private static final String HEALTH_INSURANCE_ICBC = "P_icbc";
  private static final String HEALTH_INSURANCE_WORKSAFE = "P_workSafeBC";
  private static final String HEALTH_INSURANCE_OTHER = "P_billto_other";
  private static final String HEALTH_INSURANCE_NONE = "P_patient";
}
