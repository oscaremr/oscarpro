/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.Map;

import org.apache.log4j.Logger;
import org.hl7.fhir.r4.model.Encounter;
import org.hl7.fhir.r4.model.Reference;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.util.MiscUtils;

public class EncounterConverter {

  private static final Logger LOG = MiscUtils.getLogger();

  public static Encounter toFhirObject(Map<String, EFormValue> valueMap) {

    Encounter encounter = new Encounter();
    Encounter.EncounterLocationComponent location = new Encounter.EncounterLocationComponent();
    Reference locationReference = new Reference();
    String locationUrl = EFormValueHelper.getValue(valueMap, LocationPropertySet.LOCATION_URL);
    if (locationUrl == null) {
      LOG.error("Missing " + LocationPropertySet.LOCATION_URL + " eform value");
      throw new ServiceException("Missing " + LocationPropertySet.LOCATION_URL + " eform value");
    }
    locationReference.setReference(locationUrl);
    location.setLocation(locationReference);
    encounter.addLocation(location);
    encounter.setStatus(Encounter.EncounterStatus.PLANNED);

    return encounter;
  }
}
