/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.UUID;
import ca.uhn.fhir.model.api.TemporalPrecisionEnum;
import cdsrourke.PatientDocument.Patient;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.hl7.fhir.r4.formats.JsonParser;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Encounter;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.InstantType;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Resource;
import org.hl7.fhir.r4.model.ServiceRequest;
import org.oscarehr.common.dao.OrderLabTestCodeCacheDao;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.OrderLabTestCodeCache;
import org.oscarehr.integration.excelleris.eorder.ASCIItoHEX;
import org.oscarehr.integration.excelleris.eorder.ConfigureConstants;
import org.oscarehr.integration.excelleris.eorder.PropertyDefaultConstants;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialBusinessLogic;
import org.oscarehr.integration.excelleris.eorder.testcodes.LabCodesHelper;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import oscar.util.StringUtils;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;

@Slf4j
public class DiagnosticOrderConverter {

  private static final Logger LOG = MiscUtils.getLogger();
  private static final String COLLECTOR = "T_collector";
  private static final String COLLECTION_DATE = "EO_specimen_col_date";
  private static final String COLLECTOR_TIME = "EO_specimen_col_time";
  private static final String TIMEZONE_OFFSET = "EO_timezone_offset";

  /**
   * Convert eForm values to a Diagnostic Order
   *
   * @param provincial    provincial business logic with embedded valuemap
   * @param practitioner  practitioner requesting eOrder
   * @param questionnaire outstanding questionnairre
   * @return converted Diagnostic Order
   */
  public static List<Resource> toFhirObject(final ProvincialBusinessLogic provincial,
                                            final Practitioner practitioner,
                                            final Questionnaire questionnaire) throws ServiceException {

    LOG.trace("Calling ServiceRequestConverter.toFhirObject");

    final Map<String, EFormValue> valueMap = provincial.getValueMap();
    final List<Resource> resourceList = new ArrayList<>();
    final ServiceRequest serviceRequest = new ServiceRequest();
    serviceRequest.setId(UUID.randomUUID().toString());
    resourceList.add(serviceRequest);

    processLocation(valueMap);
    processEncounter(valueMap);

    // Subject (reference)
    //
    String patientExcellerisId = EFormValueHelper.getValue(valueMap, PatientPropertySet.EXCELLERIS_PATIENT_ID);
    Reference reference = new Reference();
    if (patientExcellerisId == null) {
      // new patient
      reference.setReference(ResourceUrlHelper.getResourceUrl(Patient.class.getSimpleName(), null));
      serviceRequest.setSubject(reference);
    } else {
      // existing patient in Excelleris
      reference.setReference(ResourceUrlHelper.getResourceUrl(Patient.class.getSimpleName(), patientExcellerisId));
      serviceRequest.setSubject(reference);
    }

    // Encounter (reference)
    //
    String encounterUrl = EFormValueHelper.getValue(valueMap, EncounterPropertySet.ENCOUNTER_URL);
    if (encounterUrl == null) {
      LOG.error("Missing " + EncounterPropertySet.ENCOUNTER_URL + " eform value.");
      throw new ServiceException("Missing " + EncounterPropertySet.ENCOUNTER_URL + " eform value.");
    } else {
      reference = new Reference();
      reference.setReference(encounterUrl);
      serviceRequest.setEncounter(reference);
    }

    // Orderer (reference)
    //
    String orderingProviderExcellerisId = EFormValueHelper.getValue(valueMap, PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
    if (orderingProviderExcellerisId == null) {
      LOG.error("Orderring provider Excelleris Id");
      throw new ServiceException("This lab requisition will not be submitted as an eOrder because the orderring provider does not have Lifelabs/Excelleris Ids.");
    } else {
      reference = new Reference();
      reference.setReference(ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), ASCIItoHEX.toHex(orderingProviderExcellerisId)));
      serviceRequest.setRequester(reference);
    }

    // Set up the list of extensions
    //
    final Extension pregancy = createPregnancyExtension(valueMap);
    final Extension diagnosis = provincial.createDiagnosisNotesExtension();
    final Extension subjectNotes = provincial.createSubjectNotesExtension();
    final Extension payerExtension = provincial.createPayerExtension();
    final Extension diagnosticCode = createDiagnosticCodeExtension(valueMap);
    final Extension signatureDate = createSignatureDateExtension(valueMap);
    final Extension copyToProvider = provincial.createCopyToProviderExtension(valueMap, practitioner.getId());
    final List<Extension> extensionList = ConverterHelper.createArray(pregancy, diagnosis, subjectNotes, payerExtension, diagnosticCode, signatureDate, copyToProvider);

    extensionList.addAll(createCopyToOtherProvidersExtension(valueMap));
    extensionList.addAll(createSpecimenExtension(valueMap));
    extensionList.addAll(provincial.createTestGlobalExceptions(valueMap));
    extensionList.addAll(provincial.createProvincialSpecificExtensions());
    for (final Extension extension : extensionList) {
      serviceRequest.addExtension(extension);
    }

    // Store questionnaire and questionnaire response objects
    //
    resourceList.add(questionnaire);
    QuestionnaireResponse questionnaireResponse = new QuestionnaireResponse();
    resourceList.add(questionnaireResponse);

    // Identifier

    //  testCodeToKeyMap is used to create the questionnarie response
    //  the list has the test key at index 0, and test itemId at index 1
    //
    // Item walk through all values with T_ prefix
    Map<String, List<String>> testCodeToKeyAndItemIdMap = new HashMap<String, List<String>>();

    // valueMap contains all eForm Value data belonging to the current eForm Data instance
    for (String key : valueMap.keySet()) {
      Extension extension = null;
      OrderLabTestCodeCache labTestDetail = null;
      if (provincial.isExtensionQRNeeded(key)) {
        questionnaireResponse = provincial.processExtensionQR(valueMap, key, questionnaireResponse);
      } else if (provincial.isTest(key)) {
        EFormValue value = valueMap.get(key);
        //  the test has been removed from the order
        if (StringUtils.isNullOrEmpty(value.getVarValue())) {
          continue;
        }

        String testCodePK = "";
        log.info("Key : " + key);
        testCodePK = provincial.getTestCode(key, questionnaireResponse,
                orderingProviderExcellerisId, questionnaire);

        CodeableConcept labTest = new CodeableConcept();

        String itemUuid = UUID.randomUUID().toString();
        labTest.setId(itemUuid);

        final List<Extension> orderExtensionList = provincial.getOrderExtensionList(key);
        for (Extension ext : orderExtensionList) {
          labTest.addExtension(ext);
        }


        extension = FhirResourceHelper.createCodeableConceptExtension(
            ResourceUrlHelper.getItemOtherTestValueSetExtensionUrl(), provincial.isOtherTests(key) ? "1" : "0", ResourceUrlHelper.getItemOtherTestExtensionUrl());
        labTest.addExtension(extension);


        // code
        CodeableConcept code = new CodeableConcept();
        Coding coding = new Coding(ResourceUrlHelper.getOrderCodeValueSetSystemUrl(), key,
            testCodePK);
        labTest.setText(labTestDetail != null ? labTestDetail.getTestName() : "");

        ArrayList<String> keyItemId = new ArrayList<String>();
        keyItemId.add(0, key);
        keyItemId.add(1, itemUuid);
        testCodeToKeyAndItemIdMap.put(testCodePK, keyItemId);
        log.info("Test code: " + testCodePK);
        coding.setCode(testCodePK);
        labTest.addCoding(coding);
        serviceRequest.addOrderDetail(labTest);
      }
    }

    if (questionnaire != null) {
      questionnaireResponse = provincial.createQuestionnaireResponse(valueMap, questionnaireResponse, questionnaire, testCodeToKeyAndItemIdMap);
    }

    // make sure that there is at least one test in the order
    if (serviceRequest.getOrderDetail() == null || serviceRequest.getOrderDetail().isEmpty()) {
      LOG.warn("There is no test in the lab order.");
      throw new ServiceException("Your lab order is empty. You need to select one or more tests before submitting the order.");
    }

    return resourceList;
  }



  //
  // Private Methods
  //

  private static OrderLabTestCodeCache findLabTest(final String testCode) {
    OrderLabTestCodeCache testDetails = null;
    try {
      OrderLabTestCodeCacheDao orderLabTestCodeCacheDao = getOrderLabTestCodeCacheDao();

      testDetails = orderLabTestCodeCacheDao.findByTestCode(testCode);

    } catch (Exception exception) {
      exception.printStackTrace();
    }

    return testDetails;
  }

  private static Properties findAllLabTest(final List<OrderLabTestCodeCache> testCodeList) {

    Properties properties = new Properties();
    try {
      for (OrderLabTestCodeCache testCodeCache : testCodeList) {
        if (testCodeCache != null
            && testCodeCache.getKeyName() != null
            && testCodeCache.getKeyName().length() > 2) {
          String convertedKey = LabCodesHelper.convertKey(testCodeCache.getKeyName().substring(2));
          properties.setProperty(convertedKey, testCodeCache.getLabTestCode());
        }
      }
    } catch (Exception exception) {
      exception.printStackTrace();
    }

    return properties;
  }

  private static Extension createPregnancyExtension(final Map<String, EFormValue> valueMap) {

    // Extension
    // ordering subject pregnancy
    Boolean pregnant = null;
    if (valueMap.containsKey("EO_pregnant")) {
      pregnant = true;
    } else if (valueMap.containsKey("P_pregnant_no")) {
      pregnant = false;
    }

    return (pregnant == null ?
      null :
      FhirResourceHelper.createBooleanExtension(pregnant, ResourceUrlHelper.getSubjectPregancyExtensionUrl()));
  }

  private static List<Extension> createSpecimenExtension(final Map<String, EFormValue> valueMap) {

    List<Extension> extensionList = new ArrayList<>();

    // Specimen Collection
    //
    if (valueMap.containsKey(COLLECTOR)) {
      extensionList.add(FhirResourceHelper.createStringExtension(valueMap.get(COLLECTOR).getVarValue(), ResourceUrlHelper.getCollectorExtensionUrl()));
    }

    if (valueMap.containsKey(COLLECTION_DATE)) {
      final String collectionTime = valueMap.containsKey(COLLECTOR_TIME) ? valueMap.get(COLLECTOR_TIME).getVarValue() : "00:00";
      final String parseDate = valueMap.get(COLLECTION_DATE).getVarValue() + " " + collectionTime;

      final Date collectionDate = getSpecimenExtensionDate(parseDate);
      if (collectionDate != null) {
        final InstantType collectionDateTime = new InstantType(collectionDate, TemporalPrecisionEnum.MILLI, TimeZone.getTimeZone("UTC"));
        extensionList.add(FhirResourceHelper.createDateTimeExtension(collectionDateTime.asStringValue(), ResourceUrlHelper.getCollectionDateTimeExtensionUrl()));
      } else {
        LOG.error("Could not parse collection date/time: '" + parseDate + "'");
      }
    }

    return extensionList;
  }

  private static Date getSpecimenExtensionDate(String parseDate) {
    Date date = null;
    String[] dateFormats = { "dd-MMM-yyyy HH:mm", "yyyy/MM/dd HH:mm" };
    for (String dateFormat : dateFormats) {
      try {
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        date = format.parse(parseDate);
      } catch (ParseException e) {}
      if (date != null) {
        break;
      }
    }
    return date;
  }

  private static Extension createDiagnosticCodeExtension(final Map<String, EFormValue> valueMap) {

    // diagnostic code
    return FhirResourceHelper.createCodeableConceptExtension(ResourceUrlHelper.getDiagnosticCodeExtensionUrl(),
        "Diagnosis", ResourceUrlHelper.getClassificationExtensionUrl());
  }

  private static Extension createCopyToProviderExtension(final Map<String, EFormValue> valueMap, final String orderingProviderExcellerisId) {

    // copy to provider
    // automatically add ordering provider as copy to provider
    return FhirResourceHelper.createValueReferenceExtension(
        ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), orderingProviderExcellerisId),
        ResourceUrlHelper.getCopyToRecipientExtensionUrl());
  }

  private static Extension createSignatureDateExtension(final Map<String, EFormValue> valueMap) {

    Date sigDate;
    try {
      sigDate = new SimpleDateFormat("dd-MMM-yyyy").parse(valueMap.get("today").getVarValue());
    } catch (Exception ex) {
      sigDate = new Date();
    }
    // Signature
    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    return FhirResourceHelper.createDateExtension(formatter.format(sigDate), ResourceUrlHelper.getSignatureDateExtensionUrl());
  }

  private static List<Extension> createCopyToOtherProvidersExtension(final Map<String, EFormValue> valueMap) {

    final List<Extension> providerList = new ArrayList<>();
    Extension extension = null;

    // other copy to providers
    Integer unknownCopyToProvider = 1;
    for (int ccProviderIndex = 1; ccProviderIndex <= PractitionerPropertyConfig.MAXIMUM_NUM_COPY_TO_PROVIDERS; ccProviderIndex++) {
      PractitionerPropertyConfig propertyConfig = new PractitionerPropertyConfig(
          PractitionerPropertySet.getCopyToProviderFirstNamePropertyNameByIndex(ccProviderIndex),
          PractitionerPropertySet.getCopyToProviderLastNamePropertyNameByIndex(ccProviderIndex),
          PractitionerPropertySet.getCopyToProviderExcellerisIdPropertyNameByIndex(ccProviderIndex),
          PractitionerPropertySet.getCopyToProviderLifeLabsIdPropertyNameByIndex(ccProviderIndex),
          PractitionerPropertySet.getCopyToProviderTitlePropertyNameByIndex(ccProviderIndex));
      if (PractitionerConverter.hasCopyToProviderData(valueMap, propertyConfig)) {
        String copyToProviderExcellerisId = EFormValueHelper.getValue(valueMap,
            PractitionerPropertySet.getCopyToProviderExcellerisIdPropertyNameByIndex(ccProviderIndex));
        if (!StringUtils.isNullOrEmpty(copyToProviderExcellerisId)) {

          if (copyToProviderExcellerisId.startsWith("https://")) {
            extension = FhirResourceHelper.createValueReferenceExtension(
                ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(),
                    copyToProviderExcellerisId.substring(copyToProviderExcellerisId.lastIndexOf("/") + 1)),
                ResourceUrlHelper.getCopyToRecipientExtensionUrl());

          } else {
            extension = FhirResourceHelper.createValueReferenceExtension(
                ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(),
                    ASCIItoHEX.toHex(copyToProviderExcellerisId)),
                ResourceUrlHelper.getCopyToRecipientExtensionUrl());
          }

          providerList.add(extension);

        } else {
          extension = FhirResourceHelper.createValueReferenceExtension(
              ResourceUrlHelper.getResourceUrl(Practitioner.class.getSimpleName(), PractitionerConverter.getUnknownProviderId(unknownCopyToProvider)),
              ResourceUrlHelper.getCopyToRecipientExtensionUrl());

          unknownCopyToProvider++;
          providerList.add(extension);
        }
      }
    }

    return providerList;
  }


  private static void processLocation(final Map<String, EFormValue> valueMap) {
    // location entry
    // Remove randomly generated location url
    final String locationUrl = ResourceUrlHelper.getGuidResourceUrl();

    // inject location url into in memory EFormValue
    final EFormValue urlEformValue = EFormValueHelper.createEFormValue(LocationPropertySet.LOCATION_URL, locationUrl);
    // add location_url to in-memory copy
    valueMap.put(LocationPropertySet.LOCATION_URL, urlEformValue);
  }


  private static void processEncounter(final Map<String, EFormValue> valueMap) {
    // encounter entry
    Encounter encounter = EncounterConverter.toFhirObject(valueMap);
    String encounterUrl = ResourceUrlHelper.getGuidResourceUrl();
    // inject location url into in memory EFormValue
    final EFormValue encounterEformValue = EFormValueHelper.createEFormValue(EncounterPropertySet.ENCOUNTER_URL, encounterUrl);
    // add encounter_url to in-memory copy
    valueMap.put(EncounterPropertySet.ENCOUNTER_URL, encounterEformValue);
  }

  private static String getServiceRequestJsonPayload(final ServiceRequest serviceRequest) {
    // convert parameters to JSON
    ByteArrayOutputStream writer = new ByteArrayOutputStream();
    try {
      final JsonParser parser = new JsonParser();
      parser.compose(writer, serviceRequest);
    } catch (Exception e) {
      LOG.error("Error converting Parameters to JSON.", e);
      throw new ServiceException("Error converting Parameters to JSON.");
    }

    String payload = null;
    try {
      payload = new String(writer.toByteArray(), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }

    return payload;
  }

  private static OrderLabTestCodeCacheDao getOrderLabTestCodeCacheDao() {
    if (s_orderLabTestCodeCacheDao == null) {
      s_orderLabTestCodeCacheDao = SpringUtils.getBean(OrderLabTestCodeCacheDao.class);
    }

    return s_orderLabTestCodeCacheDao;
  }

  public static void setOrderLabTestCodeCacheDao(final OrderLabTestCodeCacheDao orderLabTestCodeCacheDao) {
    s_orderLabTestCodeCacheDao = orderLabTestCodeCacheDao;
  }

  public static String getTestCode(String key, String province) {
    List<OrderLabTestCodeCache> testCodeList = getOrderLabTestCodeCacheDao().findTestCodeByProvince(key, province);
    if (testCodeList == null || testCodeList.isEmpty()) {
      testCodeList = s_orderLabTestCodeCacheDao.findTestCodeByProvinceNull(key);
    }
    Properties properties = findAllLabTest(testCodeList);
    return properties.getProperty(key);
  }

  static OrderLabTestCodeCacheDao s_orderLabTestCodeCacheDao = null;
}
