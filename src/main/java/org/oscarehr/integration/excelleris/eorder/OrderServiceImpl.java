/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.val;
import org.apache.log4j.Logger;
import org.hl7.fhir.r4.formats.JsonParser;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.Provenance;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.hl7.fhir.r4.model.Resource;
import org.hl7.fhir.r4.model.ResourceType;
import org.hl7.fhir.r4.model.ServiceRequest;
import org.oscarehr.common.dao.EOrderDao;
import org.oscarehr.common.dao.OrderLabTestCodeCacheDao;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.ExcellerisEorder;
import org.oscarehr.integration.excelleris.eorder.converter.ConverterHelper;
import org.oscarehr.integration.excelleris.eorder.converter.DiagnosticOrderConverter;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.converter.PatientConverter;
import org.oscarehr.integration.excelleris.eorder.converter.PractitionerPropertyConfig;
import org.oscarehr.integration.excelleris.eorder.converter.PractitionerPropertySet;
import org.oscarehr.integration.excelleris.eorder.converter.ProvenanceConverter;
import org.oscarehr.integration.excelleris.eorder.converter.ResourceUrlHelper;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialBusinessLogic;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialFactory;
import org.oscarehr.util.MiscUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderServiceImpl implements OrderService {

  private static final Logger LOG = MiscUtils.getLogger();
  private static final String AUTHORIZATION_HEADER = "Authorization";
  private static final String HTTP_HEADER_EXCELLERIS_SUBMIT_METHOD = "X-Excelleris-Submit-Method";
  private static final String ORDER_NOW = "ordernow";
  private static final String JSON_FHIR_FORMAT = "application/json+fhir";
  private static final String JSON_FHIR_FORMAT_UTF_8 = "application/json+fhir; charset=utf-8";
  private static final String HTTP_HEADER_COOKIE = "Cookie";
  private static final String HTTP_HEADER_ACCEPT = "Accept";
  private static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";

  @Autowired
  private EOrderDao eOrderDao;
  @Autowired
  private ExcellerisEOrder excellerisEOrder;

  public OrderServiceImpl() {

  }

  public OrderServiceImpl(EOrderDao eOrderDao, ExcellerisEOrder excellerisEOrder, final OrderLabTestCodeCacheDao orderLabTestCodeCacheDao) {
    this.eOrderDao = eOrderDao;
    this.excellerisEOrder = excellerisEOrder;
    DiagnosticOrderConverter.setOrderLabTestCodeCacheDao(orderLabTestCodeCacheDao);
    ConverterHelper.setOrderLabTestCodeCacheDao(orderLabTestCodeCacheDao);
  }

  @Override
  public Resource createOrder(final AuthenticationToken authenticationToken,
                              final EFormData eformData,
                              final List<EFormValue> eformValues) throws ServiceException {

    Resource orderResponse = null;
    Bundle bundle = null;
    ServiceRequest serviceRequest = null;
    Practitioner requestingPractitioner = null;
    Practitioner copyToPractitioner = null;
    Patient patient = null;
    Provenance provenance = null;

    Bundle.BundleEntryComponent questionnaireEntry = null;

    final Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    final ProvincialBusinessLogic provincial = new ProvincialFactory(valueMap).getProvincialBusinessLogic();
    provincial.expandTests();


    ExcellerisEorder eOrder = (ExcellerisEorder) eOrderDao.findByExtEFormDataId(eformData.getId());

    JsonParser parser = new JsonParser();
    Questionnaire questionnaire = null;
    if (eOrder != null && eOrder.getQuestionnaire() != null) {
      try {
        questionnaire = (Questionnaire) parser.parse(eOrder.getQuestionnaire());
      } catch (Exception e) {
        LOG.error("Error converting Parameters to JSON.", e);
        throw new ServiceException("Error converting Parameters to JSON.");
      }
    }

    if (eOrder != null && eOrder.getServiceRequest() != null) {
      try {
        bundle = (Bundle) parser.parse(eOrder.getServiceRequest());

        if (questionnaire != null) {
          bundle = bundleAddResource(bundle, questionnaire);
          final QuestionnaireResponse questionnaireResponse = provincial.createQuestionnaireResponse(bundle, questionnaire);
          bundle = insertQuestionnaireResponse(bundle, questionnaireResponse);
          ProvenanceConverter.configureSignature(bundle, valueMap);
        }

      } catch (Exception e) {
        LOG.error("Error converting Service Request Parameters to JSON.", e);
        throw new ServiceException("Error converting Service Request Parameters to JSON.");
      }
    } else {

      // convert practitioner to JSON
      //
      copyToPractitioner = buildCopyToPractitioner(authenticationToken, valueMap);
      requestingPractitioner = buildRequestingPractitioner(authenticationToken, valueMap);
      if (requestingPractitioner == null) {
        throw new ServiceException("Practitioner is not registered with Excelleris for eOrder submission");
      }
		
      patient = PatientConverter.toFhirObject(provincial);
      final List<Resource> filteredList = new ArrayList<>();
      final List<Resource> resourceList = DiagnosticOrderConverter.toFhirObject(provincial, requestingPractitioner, questionnaire);
      for (Resource resource : resourceList) {
        if (resource == null) {
          continue;
        }
        if (ResourceType.ServiceRequest.equals(resource.getResourceType())) {
          serviceRequest = (ServiceRequest) resource;
          filteredList.add(serviceRequest);
        } else if (ResourceType.QuestionnaireResponse.equals(resource.getResourceType())) {
          final QuestionnaireResponse questionnaireResponse = (QuestionnaireResponse) resource;
          if (questionnaireResponse.getItem() != null && questionnaireResponse.getItem().size() > 0) {
            filteredList.add(questionnaireResponse);
          }
        } else {
          filteredList.add(resource);
        }
      }

      // Add in CCed practitioners
      //
      filteredList.addAll(buildCCPractitionerList(authenticationToken, valueMap));

      provenance = ProvenanceConverter.toFhirObject(serviceRequest, requestingPractitioner, valueMap);
      filteredList.addAll(ConverterHelper.createArray(requestingPractitioner, copyToPractitioner, patient, provenance));
      ProvenanceConverter.configureSignature(filteredList, valueMap);
      bundle = createBundle(filteredList);
    }

    orderResponse = excellerisEOrder.submitExcellerisEOrder(authenticationToken, bundle);

    // Save the submitted request
    //
    try {
      eOrder = (ExcellerisEorder) eOrderDao.findByExtEFormDataId(eformData.getId());
      sanitizeProvenance(bundle);
      eOrder.setServiceRequest(parser.composeString(bundle));
      eOrderDao.merge(eOrder);
    } catch (IOException e) {
      LOG.error("Unable to save the recently submitted service request bundle");
    }

    return orderResponse;
  }


  // ---------------------------------------------------------------------- Private Methods


  //
  // Useful method for debugging to convert a JSON payload to a string
  //
  private String getOrderJsonPayload(final Bundle bundle) {
    ByteArrayOutputStream writer = new ByteArrayOutputStream();
    try {
      final JsonParser parser = new JsonParser();
      parser.compose(writer, bundle);
    } catch (Exception e) {
      LOG.error("Error converting Parameters to JSON.", e);
      throw new ServiceException("Error converting Parameters to JSON.");
    }
    String payload = null;
    try {
      payload = new String(writer.toByteArray(), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return payload;
  }


  private Practitioner buildRequestingPractitioner(final AuthenticationToken authenticationToken, final Map<String, EFormValue> valueMap) {
    final EFormValue eformValue = valueMap.get(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
    if (eformValue == null) {
      LOG.error(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID + " is missing from the form.");
      throw new ServiceException(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID + " is missing from the form.");
    }
    final String practitionerId = eformValue.getVarValue();
    val hackedPractitionerId = ASCIItoHEX.toHex(
        practitionerId);  // excelleris is currently only working for HEX equiavalents of the id
    val requestingPractitioner = excellerisEOrder.getPractitioner(authenticationToken,
        hackedPractitionerId, true);
		return requestingPractitioner;
	}

	private Practitioner buildCopyToPractitioner(final AuthenticationToken authenticationToken, final Map<String, EFormValue> valueMap) {
		Optional<EFormValue> eformValue = Optional.ofNullable(valueMap.get(PractitionerPropertySet.COPYTO_PROVIDER_EXCELLERIS_ID));
		Practitioner requestingPractitioner = new Practitioner();
		if (eformValue.isPresent()) {
			val practitionerId = eformValue.get().getVarValue();
			requestingPractitioner = excellerisEOrder.getPractitioner(authenticationToken, practitionerId,
			    false);
		}
    return requestingPractitioner;
  }


  private List<Practitioner> buildCCPractitionerList(final AuthenticationToken authenticationToken, final Map<String, EFormValue> valueMap) {
    final List<Practitioner> ccList = new ArrayList<>();

    // other copy to providers
    Integer unknownCopyToProvider = 1;
    for (int ccProviderIndex = 1; ccProviderIndex <= PractitionerPropertyConfig.MAXIMUM_NUM_COPY_TO_PROVIDERS; ccProviderIndex++) {

      final String copyToProviderExcellerisId = EFormValueHelper.getValue(valueMap,
          PractitionerPropertySet.getCopyToProviderExcellerisIdPropertyNameByIndex(ccProviderIndex));

      if (copyToProviderExcellerisId != null) {
        if (copyToProviderExcellerisId.startsWith("https://")) {
          ccList.add(excellerisEOrder.getPractitioner(authenticationToken,
              copyToProviderExcellerisId.substring(copyToProviderExcellerisId.lastIndexOf("/") + 1),
              true));
        } else {
          ccList.add(excellerisEOrder.getPractitioner(authenticationToken, copyToProviderExcellerisId, false));
        }
      }
    }

    return ccList;
  }


  private Bundle createBundle(final List<Resource> resourceList) {
    final Bundle bundle = new Bundle();
    bundle.setType(Bundle.BundleType.DOCUMENT);
    Bundle.BundleEntryComponent entry = null;
    for (final Resource resource : resourceList) {
      if (resource != null) {
        bundleAddResource(bundle, resource);
      }
    }
    return bundle;
  }


  private Bundle bundleAddResource(final Bundle bundle, final Resource resource) {
    bundle.setType(Bundle.BundleType.DOCUMENT);
    Bundle.BundleEntryComponent entry = null;

    if (resource != null) {
      entry = new Bundle.BundleEntryComponent();
      if (resource.getResourceType() != null && resource.getResourceType().equals(ResourceType.ServiceRequest)) {
        entry.setFullUrl(ResourceUrlHelper.getResourceUrl(resource.fhirType(), null));
      } else {
        entry.setFullUrl(ResourceUrlHelper.getResourceUrl(resource.fhirType(), resource.getId()));
      }
      entry.setResource(resource);
      bundle.addEntry(entry);
    }

    return bundle;
  }


  private Bundle insertQuestionnaireResponse(final Bundle bundle, final QuestionnaireResponse newQuestionnaireResponse) {
    Bundle resultBundle = null;
    boolean questionnaireResponseFound = false;
    for (final Bundle.BundleEntryComponent bec : bundle.getEntry()) {
      if (bec.getResource() != null
          && bec.getResource().getResourceType() != null
          && bec.getResource().getResourceType().equals(ResourceType.QuestionnaireResponse)) {
        final QuestionnaireResponse existingQuestionnaireResponse = (QuestionnaireResponse) bec.getResource();
        mergeQuestionnaireResponse(newQuestionnaireResponse, existingQuestionnaireResponse);
        resultBundle = bundle;
        questionnaireResponseFound = true;
      }
    }

    if (questionnaireResponseFound == false) {
      resultBundle = bundleAddResource(bundle, newQuestionnaireResponse);
    }

    return resultBundle;
  }

  private QuestionnaireResponse mergeQuestionnaireResponse(final QuestionnaireResponse newQR, final QuestionnaireResponse targetQR) {
    QuestionnaireResponse resultQR = null;

    if (targetQR.getItem() == null) {
      resultQR = newQR;

    } else {
      targetQR.getItem().addAll(newQR.getItem());
    }

    return resultQR;
  }


  private static void sanitizeProvenance(final Bundle bundle) {

    for (final Bundle.BundleEntryComponent bec : bundle.getEntry()) {
      if (bec.getResource() != null
          && bec.getResource().getResourceType() != null
          && bec.getResource().getResourceType().equals(ResourceType.Provenance)) {
        final Provenance provenance = (Provenance) bec.getResource();
        provenance.getSignature().get(0).setData(null);
      }
    }
  }
}
