/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;

public class UserCredential {

  private String userName;
  private String password;
  private String province;

  public UserCredential() {
    super();
  }

  public UserCredential(final String userName, final String password, final String province) {
    super();
    this.userName = userName;
    this.password = password;
    this.province = province;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(final String userName) {
    this.userName = userName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(final String password) {
    this.password = password;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(final String province) {
    this.province = province;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((password == null) ? 0 : password.hashCode());
    result = prime * result + ((userName == null) ? 0 : userName.hashCode());
    result = prime * result + ((province == null) ? 0 : province.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    UserCredential other = (UserCredential) obj;
    if (password == null) {
      if (other.password != null) return false;
    } else if (!password.equals(other.password)) return false;
    if (userName == null) {
      if (other.userName != null) return false;
    } else if (!userName.equals(other.userName)) return false;
    if (province == null) {
      if (other.province != null) return false;
    } else if (!province.equals(other.province)) return false;
    return true;
  }

  @Override
  public String toString() {
    return "UserCredential [userName=" + userName + ", password=" + password + ", province=" + province + "]";
  }
}
