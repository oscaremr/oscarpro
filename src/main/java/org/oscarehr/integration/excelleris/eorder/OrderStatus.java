/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder;

public class OrderStatus {

  public enum StatusType {
    OK("OK"),
    TOKEN_EXPIRED("TOKEN_EXPIRED");

    String code;

    StatusType(String code) {
      this.code = code;
    }

    public String getCode() {
      return this.code;
    }
  }

  private StatusType statusType;

  public OrderStatus() {
    super();
    this.statusType = StatusType.OK;
  }

  public StatusType getStatusType() {
    return statusType;
  }

  public void setStatusType(StatusType statusType) {
    this.statusType = statusType;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((statusType == null) ? 0 : statusType.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    OrderStatus other = (OrderStatus) obj;
    if (statusType != other.statusType) return false;
    return true;
  }

  @Override
  public String toString() {
    return "OrderStatus [statusType=" + statusType + "]";
  }
}