/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import java.util.UUID;

import org.apache.log4j.Logger;
import org.oscarehr.integration.excelleris.eorder.ConfigureConstants;
import org.oscarehr.integration.excelleris.eorder.PropertyDefaultConstants;
import org.oscarehr.util.MiscUtils;
import oscar.OscarProperties;
import oscar.util.StringUtils;

public class ResourceUrlHelper {

  private static final Logger LOG = MiscUtils.getLogger();

  private static final String FHIR_NAMESPACE_URL = "http://hl7.org/fhir/v2/0203";
  private static final String RESOURCE_BASE_URL = "https://api.excelleris.com/";
  private static final String EORDER_PATH = "/eorder/";

  public static final String getApiVersion() {
    String version = null;
    if (OscarProperties.getInstance() != null) {
      version = OscarProperties.getInstance().getProperty(ConfigureConstants.EXCELLERIS_EORDER_API_VERSION_PARAM_NAME);
    }

    if (StringUtils.empty(version)) {
      version = PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_API_VERSION;
    }

    return version;
  }

  public static final String getFhirNameSpaceUrl() {
    return FHIR_NAMESPACE_URL;
  }

  public static String getOrderCodeValueSetSystemUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/ordercode";
  }

  public static String getResourceUrl(String resourceType, String resourceId) {
    String url = RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH;

    url = url + resourceType;
    if (resourceId != null) {
      url = url + "/" + resourceId;
    }

    return url;
  }


  public static final String getOnAntibioticsExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-onantibiotic";
  }

  public static final String getAntibioticNameExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-antibioticname";
  }

  public static final String getOnAnticoagulantExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-onanticoagulant";
  }

  public static final String getAnticoagulantNameExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-anticoagulant";
  }

  public static final String getNonNominalReportingExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-nonnominalreporting";
  }

  public static final String getBloodyStoolsExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-hashistoryofbloodystool";
  }

  public static final String getItemTestForExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-item-test-for";
  }

  public static final String getItemTestForValueSetExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/servicerequest-item-test-for";
  }

  public static final String getSubjectPregancyExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-subjectpregnancy";
  }

  public static final String getSubjectNotesExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-subjectnotes";
  }

  public static final String getDiagnosisExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-subjectdiagnosis";
  }

  public static final String getDiagnosticCodeExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/diagnosis-code";
  }

  public static final String getClassificationExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-classification";
  }

  public static final String getItemPayerTypeValueSetExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/payer-type";
  }

  public static final String getTestIdentifierExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-test-identifier";

  }

  public static final String getItemPayerTypeExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-item-payertype";

  }

  public static final String getItemTieBreakerValueSetExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/servicerequest-item-tiebreaker";
  }

  public static final String getItemTieBreakerExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-item-tiebreaker";
  }

  public static final String getItemIsHardCodedTestValueSetExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/servicerequest-item-ishardcodedtest";
  }

  public static final String getItemIsHardCodedTestExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-item-ishardcodedtest";
  }

  public static final String getItemOtherTestValueSetExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/servicerequest-item-othertestfield";
  }

  public static final String getItemOtherTestExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-item-othertestfield";
  }

  public static final String getItemOtherIsOtherMicrobilogyTestValueSetExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "ValueSet/servicerequest-item-isothermicrobiology";
  }

  public static final String getItemOtherIsOtherMicrobilogyTestExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-item-isothermicrobiology";
  }

  public static final String getCopyToRecipientExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-copytorecipient";
  }

  public static final String getCollectionDateTimeExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-collectiondatetime";
  }

  public static final String getCollectorExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-collector-name";
  }

  public static final String getSignatureDateExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-signaturedate";
  }


  public static final String getFastingExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-isfasting";
  }

  public static final String getFastingDurationExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-fastingduration";
  }

  public static final String getChartNumberExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-chartnumber";
  }

  public static final String getRoomNumberExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-room";
  }

  public static final String getCurrentMedicationsExtensionUrl() {
    return RESOURCE_BASE_URL + getApiVersion() + EORDER_PATH + "servicerequest-currentmedicationsnote";
  }

  public static String getGuidResourceUrl() {
    return "urn:uuid:" + UUID.randomUUID();
  }
}
