package org.oscarehr.integration.excelleris.eorder.api;

import java.util.List;

public class Question {

  private QuestionType type;
  private String linkId;
  private String testCode;
  private String questionText;
  private String itemId;
  private String hardCodedTestFieldId;
  private String otherTestFieldId;
  private String testName;
  private List<Choice> choices;
  private Boolean required;

  public Question() {

  }

  public Question(QuestionType type, String linkId, String testCode, String questionText, String itemId,
                  String hardCodedTestFieldId, String otherTestFieldId, List<Choice> choices, String testName,
                  Boolean required) {
    super();
    this.type = type;
    this.linkId = linkId;
    this.testCode = testCode;
    this.questionText = questionText;
    this.itemId = itemId;
    this.hardCodedTestFieldId = hardCodedTestFieldId;
    this.otherTestFieldId = otherTestFieldId;
    this.choices = choices;
    this.testName = testName;
    this.required = required;
  }

  public QuestionType getType() {
    return type;
  }

  public void setType(QuestionType type) {
    this.type = type;
  }

  public String getLinkId() {
    return linkId;
  }

  public void setLinkId(String linkId) {
    this.linkId = linkId;
    String[] linkIdArray = this.linkId.split("\\|");
    this.itemId = linkIdArray[linkIdArray.length - 1];
    this.testCode = linkIdArray[2];
  }

  public String getTestCode() {
    return this.testCode;
  }
// test code is derived from linkid
//	public void setTestCode(String testCode) {
//		this.testCode = testCode;
//	}

  public String getQuestionText() {
    return questionText;
  }

  public void setQuestionText(String questionText) {
    this.questionText = questionText;
  }

  public String getItemId() {
    return this.itemId;
  }
// itemId is derived from the linkId
//	public void setItemId(String itemId) {
//		this.itemId = itemId;
//	}

  public String getHardCodedTestFieldId() {
    return hardCodedTestFieldId;
  }

  public void setHardCodedTestFieldId(String hardCodedTestFieldId) {
    this.hardCodedTestFieldId = hardCodedTestFieldId;
  }

  public String getOtherTestFieldId() {
    return otherTestFieldId;
  }

  public void setOtherTestFieldId(String otherTestFieldId) {
    this.otherTestFieldId = otherTestFieldId;
  }

  public List<Choice> getChoices() {
    return choices;
  }

  public void setChoices(List<Choice> choices) {
    this.choices = choices;
  }

  public String getTestName() {
    return testName;
  }

  public void setTestName(String testName) {
    this.testName = testName;
  }

  public Boolean getRequired() {
    return required;
  }

  public void setRequired(Boolean required) {
    this.required = required;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((choices == null) ? 0 : choices.hashCode());
    result = prime * result + ((hardCodedTestFieldId == null) ? 0 : hardCodedTestFieldId.hashCode());
    result = prime * result + ((itemId == null) ? 0 : itemId.hashCode());
    result = prime * result + ((linkId == null) ? 0 : linkId.hashCode());
    result = prime * result + ((otherTestFieldId == null) ? 0 : otherTestFieldId.hashCode());
    result = prime * result + ((questionText == null) ? 0 : questionText.hashCode());
    result = prime * result + ((testCode == null) ? 0 : testCode.hashCode());
    result = prime * result + ((testName == null) ? 0 : testName.hashCode());
    result = prime * result + ((type == null) ? 0 : type.hashCode());
    result = prime * result + ((required == null) ? 0 : required.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Question other = (Question) obj;
    if (choices == null) {
      if (other.choices != null)
        return false;
    } else if (!choices.equals(other.choices))
      return false;
    if (hardCodedTestFieldId == null) {
      if (other.hardCodedTestFieldId != null)
        return false;
    } else if (!hardCodedTestFieldId.equals(other.hardCodedTestFieldId))
      return false;
    if (itemId == null) {
      if (other.itemId != null)
        return false;
    } else if (!itemId.equals(other.itemId))
      return false;
    if (linkId == null) {
      if (other.linkId != null)
        return false;
    } else if (!linkId.equals(other.linkId))
      return false;
    if (otherTestFieldId == null) {
      if (other.otherTestFieldId != null)
        return false;
    } else if (!otherTestFieldId.equals(other.otherTestFieldId))
      return false;
    if (questionText == null) {
      if (other.questionText != null)
        return false;
    } else if (!questionText.equals(other.questionText))
      return false;
    if (testCode == null) {
      if (other.testCode != null)
        return false;
    } else if (!testCode.equals(other.testCode))
      return false;
    if (testName == null) {
      if (other.testName != null)
        return false;
    } else if (!testName.equals(other.testName))
      return false;
    if (type != other.type)
      return false;
    if (required == null) {
      if (other.required != null) {
        return false;
      }
    } else if (!required.equals(other.required)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "Question [type=" + type + ", linkId=" + linkId + ", testCode=" + testCode + ", questionText="
        + questionText + ", itemId=" + itemId + ", hardCodedTestFieldId=" + hardCodedTestFieldId
        + ", otherTestFieldId=" + otherTestFieldId + ", testName=" + testName + ", choices=" + choices
        + ", required=" + required + "]";
  }

}
