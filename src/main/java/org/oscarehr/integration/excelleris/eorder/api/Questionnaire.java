package org.oscarehr.integration.excelleris.eorder.api;

import java.util.List;

public class Questionnaire {

  private List<Question> questions;


  public Questionnaire() {
  }

  public Questionnaire(List<Question> questions) {
    this.questions = questions;
  }

  public List<Question> getQuestions() {
    return questions;
  }

  public void setQuestions(List<Question> questions) {
    this.questions = questions;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((questions == null) ? 0 : questions.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Questionnaire other = (Questionnaire) obj;
    if (questions == null) {
      if (other.questions != null)
        return false;
    } else if (!questions.equals(other.questions))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Questionnaire [questions=" + questions + "]";
  }

}
