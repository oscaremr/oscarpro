package org.oscarehr.integration.dhdr;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
@AllArgsConstructor
@Getter @Setter
public class AuditInfo {
  public static final String DHDR = "DHDR";
  public static final String DHIR = "DHIR";
  public static final String SEARCH = "SEARCH";
  public static final String RETRIEVAL = "RETRIEVAL";
  public static final String SUBMISSION = "SUBMISSION";
  private String externalSystem = null;
  private String transactionType = null;
  private Integer demographicNo = null;
}
