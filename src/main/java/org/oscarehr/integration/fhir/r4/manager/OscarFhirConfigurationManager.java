/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
package org.oscarehr.integration.fhir.r4.manager;

import org.apache.log4j.Logger;
import org.oscarehr.integration.fhir.r4.builder.DestinationFactory;
import org.oscarehr.integration.fhir.r4.builder.ResourceAttributeFilterFactory;
import org.oscarehr.integration.fhir.r4.builder.SenderFactory;
import org.oscarehr.integration.fhir.r4.model.Sender;
import org.oscarehr.integration.fhir.r4.resources.ResourceAttributeFilter;
import org.oscarehr.integration.fhir.r4.resources.Settings;
import org.oscarehr.integration.fhir.r4.model.Destination;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;

public final class OscarFhirConfigurationManager {

  private static final Logger logger = MiscUtils.getLogger();

  private final Destination destination;
  private final Sender sender;
  private final LoggedInInfo loggedInInfo;
  private final ResourceAttributeFilter resourceAttributeFilter;
  private Settings settings;

  /**
   * Inject a Settings Object and all the configuration Objects will be instantiated automatically.
   * Including the Sender and Destination Objects.
   */
  public OscarFhirConfigurationManager(LoggedInInfo loggedInInfo, Settings settings) {
    logger.debug("Setting Oscar FHIR Configuration Manager with settings file: " + settings);
    this.loggedInInfo = loggedInInfo;
    this.destination = DestinationFactory.getDestination(settings);
    logger.debug("Destination settings: " + this.destination);
    this.sender = SenderFactory.getSender(settings);
    logger.debug("Sender settings: " + this.sender);
    this.resourceAttributeFilter
        = ResourceAttributeFilterFactory.getFilter(settings.getFhirDestination());
    logger.debug("FHIR Resource Attribute Filter: " + this.resourceAttributeFilter);
  }

  public Destination getDestination() {
    return destination;
  }

  public Sender getSender() {
    return sender;
  }

  public LoggedInInfo getLoggedInInfo() {
    return loggedInInfo;
  }

  public ResourceAttributeFilter getResourceAttributeFilter(Class<?> targetResource) {
    if (resourceAttributeFilter == null) {
      return null;
    }
    return resourceAttributeFilter.getFilter(targetResource);
  }

  public Settings getSettings() {
    return settings;
  }

  public void setSettings(Settings settings) {
    this.settings = settings;
  }
}
