/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 *
 * <p>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * <p>You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * <p>This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
package org.oscarehr.integration.mcedt;

import ca.ontario.health.ebs.EbsFault;
import ca.ontario.health.edt.CommonResult;
import ca.ontario.health.edt.DownloadData;
import ca.ontario.health.edt.DownloadResult;
import ca.ontario.health.edt.Faultexception;
import ca.ontario.health.edt.ResourceResult;
import ca.ontario.health.edt.ResponseResult;
import java.util.List;
import lombok.val;
import oscar.util.Appender;

public class McedtMessageCreator {

  private static final String EHCAU0014_INPUT_MESSAGE =
      "Authentication failed; contact your technical support or software vendor.";

  private static final String EHCAU0014_OUTPUT_MESSAGE =
      "Authentication failed. Please modify your GoSecure password in "
          + "MCEDT and try again or contact support for assistance.";

  public static String responseResultToString(final ResponseResult result) {
    val appender = new Appender();
    if (result.getResourceID() != null) {
      appender.append(result.getResourceID().toString());
    } else {
      appender.append("Failure:");
    }
    val commonResult = result.getResult();
    if (commonResult != null) {
      appender.append("-");
      appender.append(commonResult.getCode());
      appender.append("-");
      appender.append(commonResult.getMsg());
    }
    appender.appendNonEmpty(result.getDescription());
    return appender.toString();
  }

  public static String exceptionToString(final Exception e) {
    if (e == null) {
      return null;
    }
    if (e instanceof Faultexception) {
      return faultToString(((Faultexception) e).getFaultInfo());
    }
    if (e instanceof ca.ontario.health.hcv.Faultexception) {
      return faultToString(((ca.ontario.health.hcv.Faultexception) e).getFaultInfo());
    }
    if (e.getMessage() == null) {
      return "";
    }
    return e.getMessage();
  }

  public static String downloadDataToString(final DownloadData result) {
    val appender = new Appender();
    if ((result != null && result.getResourceID() != null)) {
      appender.append(result.getResourceID().toString());
    } else {
      appender.append("Failure:");
    }
    assert result != null;
    val commonResult = result.getResult();
    if (commonResult != null) {
      appender.append("-");
      appender.append(commonResult.getCode());
      appender.append("-");
      appender.append(commonResult.getMsg());
    }
    appender.appendNonEmpty(result.getDescription());
    return appender.toString();
  }

  private static String faultToString(final EbsFault fault) {
    if (fault == null) {
      return null;
    }
    return fault.getCode() + " " + processEhcau0014Message(fault.getMessage());
  }

  /**
   * this function will modify the message when the message String is equal to "Authentication
   * failed; contact your technical support or software vendor."
   * <p>
   * Instead of directly pass the original message the function will modify the message to
   * "Authentication failed. Please modify your GoSecure password in MCEDT and try again or contact
   * support for assistance.".
   *
   * @param message message from an EbsFault
   * @return original message or modified message if the message is equal to EHCAU0014_INPUT_MESSAGE
   */
  private static String processEhcau0014Message(final String message) {
    return EHCAU0014_INPUT_MESSAGE.equals(message) ? EHCAU0014_OUTPUT_MESSAGE : message;
  }

  public static String resourceResultToString(final ResourceResult result) {
    val app = new Appender("<br/>");
    for (ResponseResult r : result.getResponse()) {
      app.append(responseResultToString(r));
    }
    return app.toString();
  }

  public static String downloadResultToString(final DownloadResult result) {
    val app = new Appender("<br/>");
    for (DownloadData r : result.getData()) {
      app.append(downloadDataToString(r));
    }
    return app.toString();
  }

  public static String stringListToString(final List<String> list) {
    val app = new Appender("<br/>");
    for (String r : list) {
      app.append(r);
    }
    return app.toString();
  }
}