/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration;

import ca.kai.util.MapUtils;
import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.gson.Gson;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.DatatypeConverter;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.oscarehr.common.model.AbstractModel;

@Entity
@Getter @Setter
@NoArgsConstructor
@Slf4j
public class OneIdSession extends AbstractModel<Object> {

  @Id @Setter(AccessLevel.NONE) private String providerNo;
  @Setter(AccessLevel.NONE) private String accessToken;
  private String refreshToken;
  private String idToken;
  private String subject;
  private String email;
  private String serviceEntitlements;
  private String hubTopic;
  private String uaoUpi;
  private String uaoName;
  private String authorizationId;
  private String toolbar;
  private long timestamp;
  private Date lastKeptActive;
  private boolean sso;

  private static final int MILLISECONDS = 1000;
  @Override
  public Object getId() {
    return null;
  }
  public boolean isExpired() {
    return this.accessToken == null || isTokenExpired();
  }

  private boolean isTokenExpired() {
    val accessToken = JWT.decode(this.accessToken);
    val issueDate = getIssueDate(accessToken);
    val expiresDate = getExpirationDate(accessToken, issueDate);
    val futureTime = getFutureTime();
    val expired = expiresDate.before(futureTime);
    if (log.isDebugEnabled()) {
      log.debug(String.format(
          "isExpired() - %s - Issued: %s, Expires: %s, Future: %s",
          expired, issueDate, expiresDate, futureTime
      ));
    }
    return expired;
  }
  private Date getIssueDate(final DecodedJWT accessToken) {
    long iat = accessToken.getClaim("iat").asLong();
    return new Date(iat * MILLISECONDS);
  }
  private Date getExpirationDate(final DecodedJWT accessToken, final Date issueDate) {
    val expires = accessToken.getClaim("expires_in").asInt();
    val calendar = Calendar.getInstance();
    calendar.setTime(issueDate);
    calendar.add(Calendar.SECOND, expires);
    return calendar.getTime();
  }

  private Date getFutureTime() {
    val calendar = Calendar.getInstance();
    calendar.add(Calendar.SECOND, 15);
    return calendar.getTime();
  }

  /**
   * This method will decode the toolbar JSON string and return the URL for the given key.
   * @param key The configurable key to get the URL from the toolbar.
   * @return A URL or an empty string if the key is not found.
   */
  @SuppressWarnings("unchecked")
  public String getUrlFromToolbar(final String key) {
    val toolbarMap =
        new Gson()
            .fromJson(
                new String(DatatypeConverter.parseBase64Binary(toolbar), StandardCharsets.UTF_8),
                Map.class);
    return MapUtils.getOrDefault(toolbarMap, key, "").toString();
  }
}
