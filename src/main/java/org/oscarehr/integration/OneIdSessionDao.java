package org.oscarehr.integration;

import org.oscarehr.common.dao.AbstractDao;
import org.springframework.stereotype.Repository;

@Repository
public class OneIdSessionDao extends AbstractDao<OneIdSession> {
  public OneIdSessionDao() {
    super(OneIdSession.class);
  }
}
