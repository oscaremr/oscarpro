/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.yourcare;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.oscarehr.common.dao.DataSharingSettingsDao;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.dao.DrugDao;
import org.oscarehr.common.dao.Hl7TextInfoDao;
import org.oscarehr.common.dao.PatientLabRoutingDao;
import org.oscarehr.common.model.DataSharingSettings;
import org.oscarehr.common.model.Drug;
import org.oscarehr.common.model.GracePeriodUnit;
import org.oscarehr.common.model.Hl7TextInfo;
import org.oscarehr.common.model.PatientLabRouting;
import org.oscarehr.common.model.ResourceSyncSettings;
import org.oscarehr.common.model.ResourceTypeEnum;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.OscarAuditLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import oscar.log.LogConst;

@Component
public class DataSyncService  {

  public static final String PORTAL_REGISTERED_DATE_TIME = "patient_portal_registered_date_time";
  @Autowired
  private DataSharingSettingsDao dataSharingSettingsDao;
  @Autowired
  private DemographicExtDao demographicExtDao;
  @Autowired
  private Hl7TextInfoDao hl7TextInfoDao;
  @Autowired
  private PatientLabRoutingDao patientLabRoutingDao;
  @Autowired
  private DrugDao drugDao;

  private static final Logger LOG = MiscUtils.getLogger();

  public DataSyncService() {
  }

  @Autowired
  public DataSyncService(DataSharingSettingsDao dataSharingSettingsDao, DemographicExtDao demographicExtDao,
          Hl7TextInfoDao hl7TextInfoDao, PatientLabRoutingDao patientLabRoutingDao, DrugDao drugDao) {
    this.dataSharingSettingsDao = dataSharingSettingsDao;
    this.demographicExtDao = demographicExtDao;
    this.hl7TextInfoDao = hl7TextInfoDao;
    this.patientLabRoutingDao = patientLabRoutingDao;
    this.drugDao = drugDao;

  }

  public Date getAutoSyncDate(Date gracePeriodStart, ResourceTypeEnum resourceType, int demographicNo) {
    if (gracePeriodStart == null) {
      return null;
    }
    DataSharingSettings settings = dataSharingSettingsDao.findByDemographicNo(demographicNo);
    if (settings == null) {
      settings = dataSharingSettingsDao.getOrganizationSettings();
      if (settings == null) {
        LOG.warn("No default settings for organization. Ask admin to enable data sharing");
        return null;
      }
    }

    for (ResourceSyncSettings resourceSetting : settings.getResourceSyncSettings()) {
      if (resourceSetting.getResourceType() == resourceType) {
        if (!resourceSetting.isAutoSync()) {
          return null;
        } else {
          break;
        }
      }
    }

    if (!settings.isAllHistory()) {
      String registrationDateStr = demographicExtDao.getValueForDemoKey(demographicNo, PORTAL_REGISTERED_DATE_TIME);

      if (StringUtils.isBlank(registrationDateStr)) {
        LOG.warn("Patient is not registered as a portal user: " + demographicNo);
        return null;
      }
      Date registrationDate = null;
      try {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        registrationDate = df.parse(registrationDateStr);
      } catch (ParseException e) {
        LOG.warn("Portal registration date parse error for demographic id: " + demographicNo);
        return null;
      }

      if (registrationDate == null || gracePeriodStart.before(registrationDate)) {
        return null;
      }
    }
    Calendar cal = Calendar.getInstance();
    cal.setTime(gracePeriodStart);
    if (settings.getGracePeriodUnit() == GracePeriodUnit.HOURS) {
      cal.add(Calendar.HOUR, settings.getGracePeriod());
    } else if (settings.getGracePeriodUnit() == GracePeriodUnit.DAYS) {
      cal.add(Calendar.DATE, settings.getGracePeriod());
    } else if (settings.getGracePeriodUnit() == GracePeriodUnit.WEEKS) {
      cal.add(Calendar.DATE, settings.getGracePeriod() * 7);
    }
    return cal.getTime();

  }

  public void logUpdateAutoSyncDate(LoggedInInfo loggedInInfo, ResourceTypeEnum type, Integer resourceId,
          int demographicNo,
          Date autoSyncDate) {
    if(autoSyncDate != null) {
      OscarAuditLogger.getInstance().log(loggedInInfo, LogConst.WRITE_AUTO_SYNC_DATE, type.toString(),
          resourceId.toString(), null, demographicNo, "autoSyncDate: " + autoSyncDate);
    }
  }

  public void logUpdateManualSyncDate(LoggedInInfo loggedInInfo, ResourceTypeEnum type, Integer resourceId,
          int demographicNo) {
    OscarAuditLogger.getInstance().log(loggedInInfo, LogConst.PUSH_TO_PORTAL, type.toString(), resourceId.toString(),
            null, demographicNo,
            resourceId.toString());
  }

  public Date setHL7TextInfoAutoSyncDate(int labNo, int demographicNo) {
    Hl7TextInfo lab = hl7TextInfoDao.findLabId(labNo);
    if (lab != null) {
      return getAutoSyncDate(lab.getAcknowledgedDate(), ResourceTypeEnum.LAB_RESULTS,
              demographicNo);
    }
    return null;
  }

  public void manuallySyncLab(int segmentId, int demographicNo, LoggedInInfo loggedInInfo) {
    List<PatientLabRouting> routings = patientLabRoutingDao.findByLabNoAndLabType(segmentId, "HL7");
    if (routings == null) {
      return;
    }

    for (PatientLabRouting routing : routings) {
      if (demographicNo != routing.getDemographicNo()) {
        continue;
      }
      routing.setAvailable(true);
      patientLabRoutingDao.saveEntity(routing);
      this.logUpdateManualSyncDate(loggedInInfo, ResourceTypeEnum.LAB_RESULTS, segmentId, demographicNo);
    }

  }

  public void manuallySyncMedication(int drugId, int demographicNo, LoggedInInfo loggedInInfo) {
    Drug drug = drugDao.find(drugId);
    if (drug != null) {
      drug.setAvailable(true);
      drugDao.saveEntity(drug);
      this.logUpdateManualSyncDate(loggedInInfo, ResourceTypeEnum.MEDICATION, drugId, demographicNo);
    }

  }


}
