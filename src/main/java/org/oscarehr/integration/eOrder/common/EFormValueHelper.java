/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.eOrder.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.oscarehr.common.model.EFormValue;

public class EFormValueHelper {

  public static Map<String, EFormValue> getValueMap(List<EFormValue> eformValues) {

    Map<String, EFormValue> valueMap = new HashMap<String, EFormValue>();

    if (eformValues == null || eformValues.isEmpty()) {
      return valueMap;
    }

    for (EFormValue value : eformValues) {
      valueMap.put(value.getVarName(), value);
    }

    return valueMap;
  }

  public static String getValue(Map<String, EFormValue> valueMap, String varName) {
    if (valueMap == null) {
      return null;
    }

    EFormValue eFormValue = valueMap.get(varName);
    if (eFormValue == null || eFormValue.getVarValue() == null) {
      return null;
    }

    return eFormValue.getVarValue().trim();
  }

  public static EFormValue createEFormValue(String varName, String varValue) {
    EFormValue eformValue = new EFormValue();
    eformValue.setVarName(varName);
    eformValue.setVarValue(varValue);
    return eformValue;
  }
}
