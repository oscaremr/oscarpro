/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.eOrder.common;

public class ServiceException extends RuntimeException {

  public ServiceException() {
    super();
  }

  public ServiceException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
    super(arg0, arg1, arg2, arg3);
  }

  public ServiceException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }

  public ServiceException(String arg0) {
    super(arg0);
  }

  public ServiceException(Throwable arg0) {
    super(arg0);
  }
}
