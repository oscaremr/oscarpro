/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
package org.oscarehr.integration;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.util.Map.Entry;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.oscarehr.integration.dhdr.OmdGateway;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;

public class OneIDTokenUtils {

  static Logger logger = MiscUtils.getLogger();

  public static String urlEncode(String toEncode) {
    if (toEncode == null) {
      return "";
    }
    String encoded = toEncode.replace("%", "%25");
    encoded = encoded.replace(" ", "%20");
    encoded = encoded.replace("!", "%21");
    encoded = encoded.replace("#", "%23");
    encoded = encoded.replace("$", "%24");
    encoded = encoded.replace("&", "%26");
    encoded = encoded.replace("'", "%27");
    encoded = encoded.replace("(", "%28");
    encoded = encoded.replace(")", "%29");
    encoded = encoded.replace("*", "%2A");
    encoded = encoded.replace("+", "%2B");
    encoded = encoded.replace(",", "%2C");
    encoded = encoded.replace("/", "%2F");
    encoded = encoded.replace(":", "%3A");
    encoded = encoded.replace(";", "%3B");
    encoded = encoded.replace("=", "%3D");
    encoded = encoded.replace("?", "%3F");
    encoded = encoded.replace("@", "%40");
    encoded = encoded.replace("[", "%5B");
    encoded = encoded.replace("]", "%5D");
    return encoded;
  }

  public static String debugTokens(HttpSession session) {
    String tokenAttr = (String) session.getAttribute("oneid_token");

    if (tokenAttr == null) {
      logger.warn("tokenAttr is null");
      return "ERROR no token";
    }
    StringBuilder sb = new StringBuilder(
        "===============================\nDEBUG ONEID TOKEN\n=======================\n");
    try {
      JSONObject tokens = JSONObject.fromObject(tokenAttr);
      sb.append("\n" + tokens.toString(3));

      String accessToken = tokens.getString("access_token");

      if (accessToken == null) {
        logger.warn("accessToken is null");
        return "ERROR no access token";
      }
      sb.append("\n\nACCESS TOKEN\n");
      DecodedJWT decodedJWT = JWT.decode(accessToken);
      for (Entry<String, Claim> entry : decodedJWT.getClaims().entrySet()) {
        sb.append("\t entry:" + entry.getKey() + "  " + entry.getValue().asString() + "\n");
      }

      decodedJWT = JWT.decode(tokens.getString("refresh_token"));
      sb.append("\n\nRefresh TOKEN\n");
      for (Entry<String, Claim> entry : decodedJWT.getClaims().entrySet()) {
        sb.append("\t entry:" + entry.getKey() + "  " + entry.getValue().asString() + "\n");

      }

      decodedJWT = JWT.decode(tokens.getString("id_token"));
      sb.append("\n\nID TOKEN\n");
      for (Entry<String, Claim> entry : decodedJWT.getClaims().entrySet()) {
        sb.append("\t entry:" + entry.getKey() + "  " + entry.getValue().asString() + "\n");
      }
    } catch (Exception e) {
      sb.append("Error parsing Token " + tokenAttr);
    }
    sb.append("\n=================================\n");

    return sb.toString();
  }

  public static void verifyAccessTokenIsValid(LoggedInInfo loggedInInfo,
      OneIdGatewayData oneIdGatewayData) throws TokenExpiredException {
    if (oneIdGatewayData.isAccessTokenExpired()) {
      refreshToken(loggedInInfo, oneIdGatewayData);
    }
  }

  private static void refreshToken(LoggedInInfo loggedInInfo, OneIdGatewayData oneIdGatewayData)
      throws TokenExpiredException {

    if (oneIdGatewayData.isRefreshTokenExpired()) {
      logger.info("Token was expired" + oneIdGatewayData);
      throw new TokenExpiredException();
    }
    OmdGateway omdGateway = new OmdGateway();
    omdGateway.refreshToken(loggedInInfo, oneIdGatewayData);
  }

  public static String getCompleteURL(HttpServletRequest request) {
    StringBuffer requestURL = request.getRequestURL();
    if (request.getQueryString() != null) {
      requestURL.append("?").append(request.getQueryString());
    }
    return requestURL.toString();
  }
}
