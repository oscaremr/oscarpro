/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.quatro.util;

import javax.servlet.ServletRequest;

public class ServletRequestUtil {

  public static int getIntParameterFromRequest(ServletRequest request, String parameter) {
    return request.getParameter(parameter) == null
        ? 0
        : Integer.parseInt(request.getParameter(parameter));
  }
}
