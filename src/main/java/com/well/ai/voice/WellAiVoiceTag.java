/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.ai.voice;

import lombok.extern.slf4j.Slf4j;
import org.oscarehr.managers.OktaManager;
import org.oscarehr.util.SpringUtils;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;


/**
 * Tag to support addition WellAiVoice virtual assistant in OSCAR
 * Functionality is provider a Tag with user information for wellAiVoice
 */
@Slf4j
public class WellAiVoiceTag extends TagSupport {
  private WellAiVoiceService wellAiVoiceService;
  private OktaManager oktaManager;
  protected static final String FORMAT = "<script id=\"tali-ai\" async src=\"%s\" client-id=\"%s\"></script>";

  public int doStartTag() throws JspException {
    try {
      super.pageContext.getOut().print(getIdentifier());
    } catch (IOException e) {
      log.error(e.getMessage());
    }
    return SKIP_BODY;
  }

  public int doEndTag() {
    return EVAL_PAGE;
  }

  /**
   * Return a string with wellAiVoice tag to full in the start tag
   * @return String with start of wellAiVoice tag, okta client id and end of wellAiVoice tag
   */
  public String getIdentifier() {
    return getWellAiVoiceService().isWellAiVoiceEnabled() ?
        String.format(FORMAT, wellAiVoiceService.getWellAiVoiceUrl(),
            getOktaManager().isOktaEnabled() ? getOktaManager().getClientId() : "")
        : "";
  }

  public WellAiVoiceService getWellAiVoiceService() {
    if (wellAiVoiceService == null) {
      wellAiVoiceService = SpringUtils.getBean(WellAiVoiceService.class);
    }
    return wellAiVoiceService;
  }

  public OktaManager getOktaManager() {
    if (oktaManager == null) {
      oktaManager = SpringUtils.getBean(OktaManager.class);
    }
    return oktaManager;
  }
}