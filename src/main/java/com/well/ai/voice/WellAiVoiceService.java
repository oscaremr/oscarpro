/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.ai.voice;

import lombok.RequiredArgsConstructor;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to support addition of WellAiVoice virtual assistant in OSCAR.
 * Functionality is enabled via the System Preference "well_ai_voice.enabled" set as "true".
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class WellAiVoiceService {
  private final SystemPreferencesDao systemPreferencesDao;
  protected static final String WELL_AI_VOICE_ENABLED = "well_ai_voice.enabled";

  protected static final String WELL_AI_VOICE_URL = "well.ai.voice_url";

  protected static final String DEFAULT_URL = "https://tali.ai/tali.js";

  /**
   * Check for system preferences table for "well_ai_voice.enabled"
   * @return boolean "well_ai_voice.enabled" from system preferences
   */
  public boolean isWellAiVoiceEnabled() {
    return systemPreferencesDao.isReadBooleanPreferenceWithDefault(WELL_AI_VOICE_ENABLED, false);
  }

  public String getWellAiVoiceUrl() {
    return systemPreferencesDao.getPreferenceValueByName(WELL_AI_VOICE_URL, DEFAULT_URL);
  }
}