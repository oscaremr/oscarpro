/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.hcv;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.MyGroupDao;
import org.oscarehr.common.dao.OscarAppointmentDao;
import org.oscarehr.common.dao.ProviderSiteDao;
import org.oscarehr.common.dao.SiteDao;
import org.oscarehr.common.model.Appointment;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.MyGroup;
import org.oscarehr.common.model.ProviderPreference;
import org.oscarehr.common.model.Site;
import org.oscarehr.util.SessionConstants;
import org.oscarehr.util.SpringUtils;
import oscar.util.ConversionUtils;

public class HCVBulkFactory {
    private HttpSession session;
    private HttpServletRequest request;
    private Properties providerProperties;
    private List<Demographic> demographicsWithValidHin;
    private List<Demographic> demographicsWithInvalidHin;
    private List<Demographic> demographicsWithOutOfOnHin;
    
    public HCVBulkFactory(HttpSession session, HttpServletRequest request, Properties providerProperties) {
        this.session = session;
        this.request = request;
        this.providerProperties = providerProperties;
    }

    public List<Demographic> getDemographicsWithValidHin() {
        return demographicsWithValidHin;
    }

    public List<Demographic> getDemographicsWithInvalidHin() {
        return demographicsWithInvalidHin;
    }

    public List<Demographic> getDemographicsWithOutOfOnHin() {
        return demographicsWithOutOfOnHin;
    }

    public HCVBulkFactory invoke() {
        demographicsWithValidHin = new ArrayList<>();
        demographicsWithInvalidHin = new ArrayList<>();
        demographicsWithOutOfOnHin = new ArrayList<>();
        String curUser_no = (String) session.getAttribute("user");
        SiteDao siteDao = SpringUtils.getBean(SiteDao.class);
        MyGroupDao myGroupDao = SpringUtils.getBean(MyGroupDao.class);
        ProviderSiteDao providerSiteDao = SpringUtils.getBean(ProviderSiteDao.class);
        OscarAppointmentDao appointmentDao = SpringUtils.getBean(OscarAppointmentDao.class);
        DemographicDao demographicDao = (DemographicDao) SpringUtils.getBean("demographicDao");
        //multisite starts =====================
        boolean isMultisite = org.oscarehr.common.IsPropertiesOn.isMultisitesEnable();
        List<Site> sites = new ArrayList<Site>();
        String selectedSite = null;
        if (isMultisite) {
            sites = siteDao.getAllActiveSites();
            selectedSite = (String) request.getSession().getAttribute("site_selected");
        }
        ProviderPreference providerPreference2 = (ProviderPreference) session.getAttribute(SessionConstants.LOGGED_IN_PROVIDER_PREFERENCE);
        String mygroupno = providerPreference2.getMyGroupNo();
        if (mygroupno == null) {
            mygroupno = ".default";
        }
        String caisiView = null;
        caisiView = request.getParameter("GoToCaisiViewFromOscarView");
        boolean notOscarView = "false".equals(session.getAttribute("infirmaryView_isOscar"));
        if ((caisiView != null && "true".equals(caisiView)) || notOscarView) {
            mygroupno = ".default";
        }
        int year = Integer.parseInt(request.getParameter("year"));
        int month = Integer.parseInt(request.getParameter("month"));
        int day = Integer.parseInt(request.getParameter("day"));
        boolean isWeekView = false;
        String provNum = request.getParameter("provider_no");
        if (provNum != null) {
            isWeekView = true;
        }
        if (caisiView != null && "true".equals(caisiView)) {
            isWeekView = false;
        }
        GregorianCalendar cal = new GregorianCalendar();
        //verify the input date is really existed
        cal = new GregorianCalendar(year, (month - 1), day);
        if (isWeekView) {
            cal.add(Calendar.DATE, -(cal.get(Calendar.DAY_OF_WEEK) - 1)); // change the day to the current weeks initial sunday
        }
        year = cal.get(Calendar.YEAR);
        month = (cal.get(Calendar.MONTH) + 1);
        day = cal.get(Calendar.DAY_OF_MONTH);
        int view = request.getParameter("view") != null ? Integer.parseInt(request.getParameter("view")) : 0; //0-multiple views, 1-single view
        int numProvider = 0;
        String[] curProvider_no;
        String programId_oscarView = "0";
        if (mygroupno != null && providerProperties.get(mygroupno) != null) { //single appointed provider view
            numProvider = 1;
            curProvider_no = new String[numProvider];
            curProvider_no[0] = mygroupno;
        } else {
            if (view == 0) { //multiple views
                if (selectedSite != null) {
                    numProvider = siteDao.site_searchmygroupcount(mygroupno, selectedSite).intValue();
                } else {
                    numProvider = myGroupDao.getGroupByGroupNo(mygroupno).size();
                }
                if (numProvider == 0) {
                    numProvider = 1;
                    curProvider_no = new String[]{curUser_no};  //[numProvider];
                } else {
                    curProvider_no = new String[numProvider];
                    int iTemp = 0;
                    if (selectedSite != null) {
                        List<String> siteProviders = providerSiteDao.findByProviderNoBySiteName(selectedSite);
                        List<MyGroup> results = myGroupDao.getGroupByGroupNo(mygroupno);
                        for (MyGroup result : results) {
                            if (siteProviders.contains(result.getId().getProviderNo())) {
                                curProvider_no[iTemp] = String.valueOf(result.getId().getProviderNo());
                                iTemp++;
                            }
                        }
                    } else {
                        List<MyGroup> results = myGroupDao.getGroupByGroupNo(mygroupno);
                        Collections.sort(results, MyGroup.MyGroupNoViewOrderComparator);
                        for (MyGroup result : results) {
                            curProvider_no[iTemp] = String.valueOf(result.getId().getProviderNo());
                            iTemp++;
                        }
                    }
                }
            } else {//single view
                numProvider = 1;
                curProvider_no = new String[numProvider];
                curProvider_no[0] = request.getParameter("curProvider");
            }
        }

        for (int i = 0; i < numProvider; i++) {
            if (isWeekView) {
                year = cal.get(Calendar.YEAR);
                month = (cal.get(Calendar.MONTH) + 1);
                day = cal.get(Calendar.DAY_OF_MONTH);
            }
            
            List<Appointment> appointments = new ArrayList<Appointment>();
            if (isMultisite) {
                appointments = appointmentDao.searchAppointmentDaySite(curProvider_no[i], ConversionUtils.fromDateString(year + "-" + month + "-" + day), ConversionUtils.fromIntString(programId_oscarView), (String) request.getSession().getAttribute("site_selected"));
            } else {
                appointments = appointmentDao.searchappointmentday(curProvider_no[i], ConversionUtils.fromDateString(year + "-" + month + "-" + day), ConversionUtils.fromIntString(programId_oscarView));
            }
            
            for (int j = 0; j < appointments.size(); j++) {
                Appointment appointment = appointments.get(j);
                if (appointment.getStatus().equals("C")) {
                    continue;
                }
                
                int demographic_no = appointment.getDemographicNo();
                if (demographic_no < 1) {
                    continue;
                }
                
                Demographic demographic = demographicDao.getDemographic(String.valueOf(demographic_no));

                if (!demographic.getHcType().equals("ON")) {
                    demographicsWithOutOfOnHin.add(demographic);
                } else {
                    if (StringUtils.isBlank(demographic.getHin())
                        || demographic.getHin().length() != 10
                        || demographic.getVer().length() == 1
                        || demographic.getVer().length() > 2
                        || !demographic.getVer().matches("^[a-zA-Z]*$")) {
                		    demographicsWithInvalidHin.add(demographic);
                	  } else {
                		    demographicsWithValidHin.add(demographic);
                	  }
                }
            }
        }
        return this;
    }

    public static class HCVDemographic {        
    	private Integer demographicNo;
        private String hin;
        private String ver;
        
        public HCVDemographic(Integer demographicNo, String hin, String ver) {
            this.demographicNo = demographicNo;
            this.hin = hin;
            this.ver = ver;
        }    

        public Integer getDemographicNo() {
            return demographicNo;
        }

        public void setDemographicNo(Integer demographicNo) {
            this.demographicNo = demographicNo;
        }

        public String getHin() {
            return hin;
        }

        public void setHin(String hin) {
            this.hin = hin;
        }

        public String getVer() {
            return ver;
        }

        public void setVer(String ver) {
            this.ver = ver;
        }
    }
}
