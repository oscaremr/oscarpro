/*
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package com.well;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import oscar.OscarProperties;

/**
 * Servlet used to serve css files from {DOCUMENT_DIR}/css under the path
 * /{context}/well/css/{filename.css}
 */
public final class CssServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String documentDir = getDocumentDir();
    String reqPath = getPath(request);
    Path cssRootPath = getCssRootPath(documentDir);
    Path cssFilePath = getCssFilePath(cssRootPath, reqPath);
    File cssFile = cssFilePath.toFile();
    if (hasError(response, reqPath, cssRootPath, cssFilePath, cssFile)) {
      return;
    }
    setHeaders(response, cssFile.getName());
    InputStream cssStream = getCssStream(cssFile);
    IOUtils.copy(cssStream, getOutputStream(response));
  }

  protected OutputStream getOutputStream(HttpServletResponse response) throws IOException {
    return response.getOutputStream();
  }

  protected InputStream getCssStream(File cssFile) throws FileNotFoundException {
    return new FileInputStream(cssFile);
  }

  protected String getDocumentDir() {
    OscarProperties instance = OscarProperties.getInstance();
    return instance == null ? "" : instance.getProperty("BASE_DOCUMENT_DIR");
  }

  protected String getPath(HttpServletRequest request) {
    String reqPath = request.getPathInfo();
    // Trim leading '/'
    if (reqPath.startsWith("/")) {
      reqPath = reqPath.substring(1);
    }
    return reqPath;
  }

  protected Path getCssRootPath(String documentDir) {
    return Paths.get(documentDir, "css");
  }

  protected Path getCssFilePath(Path cssRootPath, String reqPath) {
    return Paths.get(cssRootPath.toString(), reqPath).toAbsolutePath().normalize();
  }

  protected boolean hasError(
      HttpServletResponse response,
      String reqPath,
      Path cssRootPath,
      Path cssFilePath,
      File cssFile) throws IOException {
    // Handle missing file parameter.
    if (reqPath.isEmpty()) {
      response.sendError(HttpServletResponse.SC_BAD_REQUEST);
      return true;
    }
    // Handle directory traversal attacks.
    if (!cssFilePath.startsWith(cssRootPath)) {
      response.sendError(HttpServletResponse.SC_BAD_REQUEST);
      return true;
    }
    // Handle invalid file extensions.
    if (!cssFile.getName().endsWith(".css")) {
      response.sendError(HttpServletResponse.SC_BAD_REQUEST);
      return true;
    }
    // Handle missing file on filesystem.
    if (!cssFile.exists()) {
      response.sendError(HttpServletResponse.SC_NOT_FOUND);
      return true;
    }
    return false;
  }

  protected void setHeaders(HttpServletResponse response, String name) {
    response.setContentType("text/css");
    response.setHeader(
        "Content-Disposition",
        "filename=\"" + name.replaceAll("\"", "") + "\"");
  }
}