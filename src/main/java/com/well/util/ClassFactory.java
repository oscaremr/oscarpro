/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.util;


import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;

public class ClassFactory<T> {

    private final Class<? extends T> classDefault;
    private final String classPackage;
    private final String classSuffix;

    private Logger logger = MiscUtils.getLogger();

    protected ClassFactory(String classPackage, String classSuffix, Class<? extends T> classDefault) {
        this.classPackage = classPackage;
        this.classDefault = classDefault;
        this.classSuffix = classSuffix;
    }

    public T newInstance(String className) {
        try {
            return getInstanceClass(className).newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            // Fall back to default
            logger.error("Unable to instantiate class: " + className, e);
            return getDefaultInstance();
        }
    }

    public Class<? extends T> getInstanceClass(String className) {
        try {
            return (Class<? extends T>) Class.forName(classPackage + className + classSuffix);
        } catch (ClassNotFoundException e) {
            return classDefault;
        }
    }

    public T getDefaultInstance() {
        try {
            return  classDefault.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            logger.error("Unable to instantiate default.", e);
            return null;
        }
    }
}
