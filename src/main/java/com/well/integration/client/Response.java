/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.client;

public class Response {
    protected int status;

    private String message;

    public Response(int status) {
        this.status = status;
    }

    public Response(int status, String message) {
        this(status);
        this.message = message;
    }

    public boolean isSuccess() { return status > 199 && status < 300; }

    public boolean hasError() { return !isSuccess(); }

    public String getMessage() { return message; }

    public int getStatus() { return status; }
}
