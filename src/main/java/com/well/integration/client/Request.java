/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.client;

import com.well.integration.Integration;
import com.well.integration.IntegrationException;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.springframework.http.HttpMethod;

import java.io.IOException;
import java.util.List;

public abstract class Request<T extends Response> {
    public static class Route {

        private final String path;

        public Route(String path) {
            this.path = path;
        }

        public String getPath() {
            return path;
        }
    }

    public Client client;

    protected HttpMethod method = HttpMethod.GET;

    protected Route route;

    private Logger logger = MiscUtils.getLogger();

    public Request(Integration integration, Route route) {
        ClientPool clientPool = SpringUtils.getBean(ClientPool.class);
        this.client = clientPool.getClient(integration);
        this.route = route;
    }
    
    public Request() { }

    public T execute() throws IntegrationException {
        return this.execute(client);
    }

    public T execute(Client client) throws IntegrationException {
        // Fall back to local client if none is provided
        if (client == null) { client = this.client; }
        try (CloseableHttpResponse response = client.execute(this)) {
            // Response handler is responsible for consuming resource
            // before they are released
            return this.getResponse(response);
        } catch (IOException e) {
            logger.error("Unable to connect to integration", e);
            throw new IntegrationException("Unable to connect to integration.", e);
        }
    }

    public HttpMethod getMethod() { return method; }

    public void setMethod(HttpMethod method) {
        this.method = method;
    }

    public Route getRoute() { return route; }

    public abstract String getBody();

    public abstract List<NameValuePair> getParameters();

    protected abstract T getResponse(CloseableHttpResponse response) throws IntegrationException;
}
