/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.client;

import com.well.integration.Integration;
import com.well.integration.IntegrationException;
import com.well.integration.config.ConfigPropertyKey;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.client.methods.*;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oscar.OscarProperties;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Integration client that supplies API KEY and CLIENT ID with requests.
 */
public class Client {

    private final Logger logger = LoggerFactory.getLogger(Client.class);

    protected CloseableHttpClient client;
    protected HttpHost httpHost;
    protected HttpClientConnectionManager connectionManager;

    protected String apiKey;
    protected String apiKeyName;
    protected String clientId;
    protected String clientIdName;
    protected String basePath;
    protected Integration integration;

    public Client(Integration integration) {
        URL baseUrl;
        ConfigPropertyKey keys = integration.getKeys();
        this.integration = integration;

        OscarProperties oscarProperties = OscarProperties.getInstance();
        apiKeyName = oscarProperties.getProperty(keys.API_KEY_NAME);
        apiKey = oscarProperties.getProperty(keys.API_KEY);
        clientId = oscarProperties.getProperty(keys.CLIENT_ID);
        clientIdName = oscarProperties.getProperty(keys.CLIENT_ID_NAME);
        try {
            baseUrl = new URL(oscarProperties.getProperty(keys.BASE_URL));
            basePath = baseUrl.getPath();
        } catch (MalformedURLException e) {
            logger.error("Malformed url: " + oscarProperties.getProperty(keys.BASE_URL));
            return;
        }
        httpHost = new HttpHost(baseUrl.getHost(), baseUrl.getPort(), baseUrl.getProtocol());
        PoolingHttpClientConnectionManager clientConnectionManager = new PoolingHttpClientConnectionManager();
        clientConnectionManager.setMaxTotal(20);
        clientConnectionManager.setDefaultMaxPerRoute(20);
        connectionManager = clientConnectionManager;
        client = HttpClientBuilder
                .create()
                .setConnectionManager(connectionManager)
                .build();
    }

    protected CloseableHttpResponse execute(Request request)
            throws IntegrationException {

        try {
            checkConfig();

            HttpRequestBase httpRequest = getRequestBase(request);
            setCredentials(httpRequest);
            httpRequest.addHeader("Content-Type", "application/json");
            httpRequest.setURI(getURI(request));
            if (httpRequest instanceof HttpEntityEnclosingRequestBase) {
                ((HttpEntityEnclosingRequestBase) httpRequest)
                        .setEntity(new StringEntity(request.getBody(), Consts.UTF_8));
            }

            HttpContext httpContext = new BasicHttpContext();

            return client.execute(httpHost, httpRequest, httpContext);
        } catch (IOException | URISyntaxException e) {
            logger.error("Unable to execute integration client request.", e);
            throw new IntegrationException("Unable to execute request.");
        }
    }

    public void checkConfig() throws IntegrationException {
        if (StringUtils.isBlank(apiKey)) {
            throw new IntegrationException("Missing API key.");
        }
        if (StringUtils.isBlank(clientId)) {
            throw new IntegrationException("Missing client ID.");
        }
        if (httpHost == null) {
            throw new IntegrationException("Invalid client configuration.");
        }
        if (client == null) {
            throw new IntegrationException("Http client is unavailable.");
        }
        if (connectionManager == null) {
            throw new IntegrationException("Client connection manager is unavailable.");
        }
    }

    protected HttpRequestBase getRequestBase(Request request) {
        switch(request.getMethod()){
            case DELETE:
                return new HttpDelete();
            case GET:
                return new HttpGet();
            case POST:
                return new HttpPost();
            case PUT:
                return new HttpPut();
            default:
                throw new IllegalArgumentException("Invalid or null HttpMethod: " + request.getMethod());
        }
    }

    protected URI getURI(Request request) throws URISyntaxException {
        URIBuilder uriBuilder = new URIBuilder()
                .setScheme(httpHost.getSchemeName())
                .setHost(httpHost.getHostName())
                .setPort(httpHost.getPort())
                .setPath(basePath + "/" + request.getRoute().getPath());
        uriBuilder.addParameters(request.getParameters());
        return uriBuilder.build().normalize();
    }

    protected void setCredentials(HttpRequest httpRequest) {
        httpRequest.setHeaders(getHeaders());
    }

    public Header[] getHeaders() {
        return new Header[] {
                new BasicHeader(apiKeyName, apiKey),
                new BasicHeader(clientIdName, clientId)
        };
    }

    public Integration getIntegration() {
        return integration;
    }
}
