package com.well.integration.client;

import com.well.integration.Integration;
import com.well.integration.IntegrationDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class ClientPool {

    protected final HashMap<Integration, Client> clients;

    @Autowired
    public IntegrationDao integrationDao;

    public ClientPool() {
        this.clients = new HashMap<>();
    }

    public Client getClient(Integration integration) {
        if (!clients.containsKey(integration)) {
            createClient(integration);
        }
        return clients.get(integration);
    }

    private void createClient(Integration integration) {
        this.clients.put(integration, new Client(integration));
    }
}
