/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ConfigPropertyKey extends Properties {
    public enum Keys {
        API_KEY(".integration.api_key"),
        API_KEY_NAME(".integration.api_key_name"),
        BASE_URL(".integration.base_url"),
        CLIENT_ID(".integration.client_id"),
        CLIENT_ID_NAME(".integration.client_id_name"),
        CONFIGURED(".integration.configured"),
        DISPLAY_NAME(".integration.display_name"),
        ENABLED(".integration.enabled"),
        REST_API_KEY(".integration.rest_api_key"),
        REST_CLIENT_ID(".integration.rest_client_id"),
        SOAP_PROVIDER_NO(".integration.soap_provider_no"),
        SUBSCRIPTIONS_FLUSHED(".integration.subscriptions.flushed"),
        SUBSCRIPTIONS_STATUS(".integration.subscriptions.status"),
        SUBSCRIPTIONS_PG_SIZE(".integration.subscriptions.pg_size"),
        SUBSCRIPTIONS_CAPACITY(".integration.subscriptions.capacity");

        private String key;

        Keys(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }
    }

    private String prefix;
    private List<String> keys;

    public final String API_KEY;
    public final String API_KEY_NAME;
    public final String BASE_URL;
    public final String CLIENT_ID;
    public final String CLIENT_ID_NAME;
    public final String DISPLAY_NAME;
    public final String ENABLED;
    public final String CONFIGURED;
    public final String REST_API_KEY;
    public final String REST_CLIENT_ID;
    public final String SOAP_PROVIDER_NO;
    public final String SUBSCRIPTIONS_FLUSHED;
    public final String SUBSCRIPTIONS_STATUS;
    public final String SUBSCRIPTIONS_PG_SIZE;
    public final String SUBSCRIPTIONS_CAPACITY;

    public ConfigPropertyKey(String prefix) {
        this.prefix = prefix;
        keys = new ArrayList<>();
        for (Keys key : Keys.values()) {
            String value = this.prefix + key.getKey();
            keys.add(value);
            setProperty(key.name(), value);
        }

        API_KEY = getKey(Keys.API_KEY);
        API_KEY_NAME = getKey(Keys.API_KEY_NAME);
        BASE_URL = getKey(Keys.BASE_URL);
        CLIENT_ID = getKey(Keys.CLIENT_ID);
        CLIENT_ID_NAME = getKey(Keys.CLIENT_ID_NAME);
        DISPLAY_NAME = getKey(Keys.DISPLAY_NAME);
        ENABLED = getKey(Keys.ENABLED);
        CONFIGURED = getKey(Keys.CONFIGURED);
        REST_API_KEY = getKey(Keys.REST_API_KEY);
        REST_CLIENT_ID = getKey(Keys.REST_CLIENT_ID);
        SOAP_PROVIDER_NO = getKey(Keys.SOAP_PROVIDER_NO);
        SUBSCRIPTIONS_FLUSHED = getKey(Keys.SUBSCRIPTIONS_FLUSHED);
        SUBSCRIPTIONS_STATUS = getKey(Keys.SUBSCRIPTIONS_STATUS);
        SUBSCRIPTIONS_PG_SIZE = getKey(Keys.SUBSCRIPTIONS_PG_SIZE);
        SUBSCRIPTIONS_CAPACITY = getKey(Keys.SUBSCRIPTIONS_CAPACITY);
    }

    public String getKey(Keys key) {
        return getProperty(key.name());
    }

    public List<String> getKeys() {
        return this.keys;
    }

    public String getPrefix() {
        return prefix;
    }
}
