/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config.parameters;

import com.well.integration.Integration;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class InsigConfigParams extends ConfigParams {

    private static final long serialVersionUID = 1L;

    private Boolean tpLinkEnabled;
    private String tpLinkType;
    private String tpLinkDisplay;

    public InsigConfigParams() {}

    public InsigConfigParams(Integration integration) {
        super(integration);
    }

    public Boolean isTpLinkEnabled() {
        return tpLinkEnabled != null && tpLinkEnabled;
    }

    public void setTpLinkEnabled(Boolean tpLinkEnabled) {
        this.tpLinkEnabled = tpLinkEnabled;
    }

    public String getTpLinkType() {
        return tpLinkType;
    }

    public void setTpLinkType(String tpLinkType) {
        this.tpLinkType = tpLinkType;
    }

    public String getTpLinkDisplay() {
        return tpLinkDisplay;
    }

    public void setTpLinkDisplay(String tpLinkDisplay) {
        this.tpLinkDisplay = tpLinkDisplay;
    }
}
