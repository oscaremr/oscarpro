/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config.parameters;

import com.well.integration.Integration;

import java.io.Serializable;

public class ConfigParams implements Serializable {
    private Integration integration;
    private String restClientId;
    private String restClientName;
    private String soapProviderNo;
    private String soapProviderFirstName;
    private String soapProviderLastName;
    private String soapProviderPassword;

    public ConfigParams() { }
    public ConfigParams(Integration integration) {
        this.integration = integration;
    }

    public Integration getIntegration() {
        return integration;
    }

    public void setIntegration(Integration integration) {
        this.integration = integration;
    }

    public String getRestClientId() {
        return restClientId;
    }

    public void setRestClientId(String restClientId) {
        this.restClientId = restClientId;
    }

    public String getRestClientName() {
        return restClientName;
    }

    public void setRestClientName(String restClientName) {
        this.restClientName = restClientName;
    }

    public String getSoapProviderNo() {
        return soapProviderNo;
    }

    public void setSoapProviderNo(String soapProviderNo) {
        this.soapProviderNo = soapProviderNo;
    }

    public String getSoapProviderFirstName() {
        return soapProviderFirstName;
    }

    public void setSoapProviderFirstName(String soapProviderFirstName) {
        this.soapProviderFirstName = soapProviderFirstName;
    }

    public String getSoapProviderLastName() {
        return soapProviderLastName;
    }

    public void setSoapProviderLastName(String soapProviderLastName) {
        this.soapProviderLastName = soapProviderLastName;
    }

    public String getSoapProviderPassword() {
        return soapProviderPassword;
    }

    public void setSoapProviderPassword(String soapProviderPassword) {
        this.soapProviderPassword = soapProviderPassword;
    }
}
