/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config;


import org.apache.commons.lang3.StringUtils;


public class ConfigProperty {

    /** API key used to authenticate with integration. */
    private String apiKey;

    /** Identifier used when communicating with integration. */
    private String clientId;

    /** When true integration schedule link, admin UI and REST endpoints are enabled */
    private Boolean enabled;

    /** Indicates whether an integration connection has been configured locally. */
    private Boolean configured;

    /** Id for REST client that integration will use. */
    private Integer restClientId;

    /** The provider integration will use for SOAP */
    private String soapProviderNo;

    /** API key used by integration to access privileged rest end points */
    private String restApiKey;

    /** Name displayed to users in the front end */
    private String displayName;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public boolean hasApiKey() {
        return StringUtils.isNotBlank(apiKey);
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public boolean hasClientId() {
        return StringUtils.isNotBlank(clientId);
    }

    public Boolean isEnabled() { return enabled != null && enabled; }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean isConfigured() {
        return configured != null && configured;
    }

    public void setConfigured(Boolean configured) {
        this.configured = configured;
    }

    public String getSoapProviderNo() {
        return soapProviderNo;
    }

    public void setSoapProviderNo(String soapProviderNo) {
        this.soapProviderNo = soapProviderNo;
    }

    public Integer getRestClientId() {
        return restClientId;
    }

    public void setRestClientId(Integer restClientId) {
        this.restClientId = restClientId;
    }

    public String getRestApiKey() {
        return restApiKey;
    }

    public void setRestApiKey(String restApiKey) {
        this.restApiKey = restApiKey;
    }

    public boolean hasRestApiKey() {
        return StringUtils.isNotBlank(restApiKey);
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
