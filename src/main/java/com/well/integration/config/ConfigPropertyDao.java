/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config;

import static org.oscarehr.common.model.SystemPreferences.asBoolean;
import static org.oscarehr.common.model.SystemPreferences.asInteger;
import static org.oscarehr.common.model.SystemPreferences.asString;

import com.well.integration.Integration;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.SystemPreferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import oscar.OscarProperties;

@Slf4j
@Repository
public class ConfigPropertyDao {

  private final SystemPreferencesDao systemPreferencesDao;

  @Autowired
  public ConfigPropertyDao(SystemPreferencesDao systemPreferencesDao) {
		this.systemPreferencesDao = systemPreferencesDao;
	}

  private final OscarProperties oscarProperties = OscarProperties.getInstance();

  public ConfigProperty getConfig(Integration integration) {
    return getConfig(integration.getKeys());
  }

  public ConfigProperty getConfig(ConfigPropertyKey keys) {
    Map<String, SystemPreferences> prefs = getSystemPreferences(keys);
    ConfigProperty config = new ConfigProperty();

    config.setApiKey(oscarProperties.getProperty(keys.API_KEY));
    config.setClientId(oscarProperties.getProperty(keys.CLIENT_ID));

    config.setConfigured(asBoolean(prefs.get(keys.CONFIGURED)));
    config.setEnabled(asBoolean(prefs.get(keys.ENABLED)));
    config.setSoapProviderNo(asString(prefs.get(keys.SOAP_PROVIDER_NO)));
    config.setRestApiKey(asString(prefs.get(keys.REST_API_KEY)));
    config.setDisplayName(asString(prefs.get(keys.DISPLAY_NAME)));

    try {
      config.setRestClientId(asInteger(prefs.get(keys.REST_CLIENT_ID)));
    } catch (NumberFormatException nfe) {
      log.warn("Unable to parse integration REST client id.", nfe);
      config.setRestClientId(null);
    }
    return config;
  }

  public Map<String, SystemPreferences> getSystemPreferences(ConfigPropertyKey properties) {
    return systemPreferencesDao.findByKeysAsPreferenceMap(properties.getKeys());
  }
}
