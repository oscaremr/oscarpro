/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config.generators;

import com.well.integration.Integration;
import com.well.integration.IntegrationManager;
import com.well.integration.config.parameters.ConfigParams;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.ServiceClient;

import java.util.Arrays;
import java.util.List;

public class ConfigGenerator implements Generator {

    @Override
    public ConfigParams generateConfig(IntegrationManager manager, Integration integration) {
        ConfigParams config = new ConfigParams(integration);

        List<String> names = Arrays.asList(integration.getDisplayName() + " Integration", integration.getDisplayName());
        populateCredentials(manager, names, config);
        return config;
    }

    protected void populateCredentials(IntegrationManager manager, List<String> names, ConfigParams config) {
        // populate soap provider or create
        List<Provider> providers = manager.providerDao.findByNamesLike(names);
        if (providers.size() > 0) {
            Provider provider = providers.get(0);
            config.setSoapProviderNo(provider.getProviderNo());
            config.setSoapProviderFirstName(provider.getFirstName());
            config.setSoapProviderLastName(provider.getLastName());
        } else {
            config.setSoapProviderNo("create");
            config.setSoapProviderFirstName("Integration");
            config.setSoapProviderLastName(config.getIntegration().getDisplayName());
        }

        // populate rest client id or create
        List<ServiceClient> serviceClients = manager.serviceClientDao.findByNamesLike(names);
        if (serviceClients.size() > 0) {
            ServiceClient serviceClient = serviceClients.get(0);
            config.setRestClientId(serviceClient.getId().toString());
            config.setRestClientName(serviceClient.getName());
        } else {
            config.setRestClientId("create");
            config.setRestClientName(config.getIntegration().getDisplayName() + " Integration");
        }
    }
}
