/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config.generators;

import com.well.integration.Integration;
import com.well.integration.IntegrationManager;
import com.well.integration.config.parameters.ConfigParams;
import com.well.integration.config.parameters.InsigConfigParams;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.SpringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.oscarehr.common.model.SystemPreferences.asBoolean;
import static org.oscarehr.common.model.SystemPreferences.asString;

public class InsigGenerator extends ConfigGenerator {
    public final static List<String> INSIG_NAMES = Arrays.asList("insig", "virtual");

    @Override
    public ConfigParams generateConfig(IntegrationManager manager, Integration integration) {
        InsigConfigParams config = new InsigConfigParams(integration);
        super.populateCredentials(manager, INSIG_NAMES, config);
        if ("create".equals(config.getSoapProviderNo())) {
            config.setSoapProviderFirstName("Care");
            config.setSoapProviderLastName("Virtual");
        }
        populateThirdPartyLinkPreferences(config);
        return config;
    }

    private void populateThirdPartyLinkPreferences(InsigConfigParams config) {
        // populate third party preferences
        SystemPreferencesDao preferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
        Map<String, SystemPreferences> prefs = preferencesDao.findByKeysAsPreferenceMap(SystemPreferences.SCHEDULE_PREFERENCE_KEYS);
        config.setTpLinkEnabled(asBoolean(prefs.get("schedule_tp_link_enabled")));
        if (config.isTpLinkEnabled()) {
            config.setTpLinkType(asString(prefs.get("schedule_tp_link_type")));
            config.setTpLinkDisplay(asString(prefs.get("schedule_tp_link_display")));
        } else {
            config.setTpLinkEnabled(true);
            config.setTpLinkType("Virtual");
            config.setTpLinkDisplay("VC+");
        }
    }
}
