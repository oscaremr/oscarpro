/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config.generators;

import com.well.integration.Integration;
import com.well.integration.IntegrationManager;
import com.well.integration.config.parameters.ConfigParams;

public interface Generator {
    ConfigParams generateConfig(IntegrationManager manager, Integration integration);
}
