/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config;

import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.ws.rest.conversion.AbstractConverter;
import org.oscarehr.ws.rest.conversion.ConversionException;


public class ConfigPropertyConverter extends AbstractConverter<ConfigProperty, ConfigPropertyTo> {
		
		@Override
		public ConfigProperty getAsDomainObject(LoggedInInfo loggedInInfo, ConfigPropertyTo t) throws ConversionException {
			throw new UnsupportedOperationException("Config should be loaded from DAO.");
		}
		
		@Override
		public ConfigPropertyTo getAsTransferObject(LoggedInInfo loggedInInfo, ConfigProperty a) throws ConversionException {
			ConfigPropertyTo t = new ConfigPropertyTo();
			t.setEnabled(a.isEnabled());
			t.setConfigured(a.isConfigured());
			t.setSoapProviderNo(a.getSoapProviderNo());
			t.setRestClientId(a.getRestClientId());
			t.setHasApiKey(a.hasApiKey() && a.hasClientId()); // We don't want to expose credentials
			return t;
		}
}

