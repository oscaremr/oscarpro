/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.well.util.ClassFactory;
import com.well.integration.Integration;
import com.well.integration.config.parameters.ConfigParams;
import org.apache.commons.lang3.StringUtils;

public class ParameterFactory extends ClassFactory<ConfigParams> {

    private static ParameterFactory instance;

    public static ParameterFactory getInstance() {
        if (instance == null) { instance = new ParameterFactory(); }
        return instance;
    }

    private ParameterFactory() {
        super("com.well.integration.config.parameters.",
              "ConfigParams",
              ConfigParams.class);
    }

    public static ConfigParams getParams(Integration integration, String json) {
        if (integration == null) { return getInstance().getDefaultInstance(); }
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(json, getInstance().getInstanceClass(StringUtils.capitalize(integration.getName())));
    }
}
