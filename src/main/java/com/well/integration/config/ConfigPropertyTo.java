/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config;

import java.io.Serializable;

public class ConfigPropertyTo implements Serializable {

	/** When true integration is enabled in EMR */
	private Boolean enabled;

	/** Indicates whether an integration connection has been configured locally. */
	private Boolean configured;

	/** The provider integration will use for SOAP */
	private String soapProviderNo;

	/** Id for REST client that integration will use. */
	private Integer restClientId;

	/** System is configured with API key used to authenticate with integration */
	private Boolean hasApiKey;

	public Boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Boolean isConfigured() {
		return configured;
	}

	public void setConfigured(Boolean configured) {
		this.configured = configured;
	}

	public String getSoapProviderNo() {
		return soapProviderNo;
	}

	public void setSoapProviderNo(String soapProviderNo) {
		this.soapProviderNo = soapProviderNo;
	}

	public Integer getRestClientId() {
		return restClientId;
	}

	public void setRestClientId(Integer restClientId) {
		this.restClientId = restClientId;
	}

	public Boolean getHasApiKey() {
		return hasApiKey;
	}

	public void setHasApiKey(Boolean hasApiKey) {
		this.hasApiKey = hasApiKey;
	}
}
