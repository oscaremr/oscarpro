/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config;

import com.well.util.ClassFactory;
import com.well.integration.Integration;
import com.well.integration.config.generators.ConfigGenerator;
import com.well.integration.config.generators.Generator;
import org.apache.commons.lang3.StringUtils;

public class GeneratorFactory extends ClassFactory<Generator> {

    private static GeneratorFactory instance;

    public static GeneratorFactory getInstance() {
        if (instance == null) { instance = new GeneratorFactory(); }
        return instance;
    }

    private GeneratorFactory() {
        super("com.well.integration.config.generators.",
              "Generator",
              ConfigGenerator.class);
    }

    public static Generator getGenerator(Integration integration) {
        if (integration == null) { return getInstance().getDefaultInstance(); }
        return getInstance().newInstance(StringUtils.capitalize(integration.getName()));
    }
}
