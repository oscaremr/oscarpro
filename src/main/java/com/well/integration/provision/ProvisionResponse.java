/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.provision;

import com.well.integration.client.Response;

public class ProvisionResponse extends Response {

    private Boolean providerRegistered;
    private Boolean provisioned;

    public ProvisionResponse(int status) {
        super(status);
    }

    public ProvisionResponse(int status, String message) {
        super(status, message);
    }

    @Override
    public boolean isSuccess() {
        // during connection check 404 indicates the system is not yet provisioned
        return (provisioned != null && status == 404) || super.isSuccess();
    }

    public Boolean isProviderRegistered() {
        return providerRegistered;
    }

    public void setProviderRegistered(Boolean providerRegistered) {
        this.providerRegistered = providerRegistered;
    }

    public Boolean isProvisioned() {
        return provisioned;
    }

    public void setProvisioned(Boolean provisioned) {
        this.provisioned = provisioned;
    }
}
