/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.provision;

import com.google.gson.GsonBuilder;
import com.well.integration.Integration;
import com.well.integration.IntegrationException;
import com.well.integration.client.Request;
import com.well.integration.provision.handlers.Handler;
import com.well.integration.provision.handlers.ProvisionHandler;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.http.HttpMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProvisionRequest extends Request<ProvisionResponse> {

    private final static Route PROVISION_ROUTE = new Route("/provision");

    public static ProvisionRequest checkConnection(Integration integration) {
        return new ProvisionRequest(integration);
    }

    public static ProvisionRequest checkHealth(Integration integration) {
        return new ProvisionRequest(integration, true, null);
    }

    public static ProvisionRequest checkProvider(Integration integration, String providerNo) {
        ProvisionRequest request = new ProvisionRequest(integration);
        request.providerNo = providerNo;
        return request;
    }

    public static ProvisionRequest sendConfiguration(Handler handler) {
        return new ProvisionRequest(handler);
    }

    // Connection validation parameters.
    private Boolean healthCheck;
    private String providerNo;

    /** Configures credentials for provisioning. */
    private ProvisionHandler handler;

    private ProvisionRequest(Integration integration) {
        super(integration, PROVISION_ROUTE);
    }

    private ProvisionRequest(Integration integration, Boolean healthCheck, String providerNo) {
        this(integration);
        this.healthCheck = healthCheck;
        this.providerNo = providerNo;
    }

    private ProvisionRequest(Handler handler) {
        super(handler.getParameters().getIntegration(), PROVISION_ROUTE);
        method = HttpMethod.POST;
        this.handler = (ProvisionHandler) handler;
    }

    @Override
    public String getBody() {
        if (handler == null) {
            throw new IllegalArgumentException("Missing config.");
        }
        Map<String, Object> entity = new HashMap<>();
        entity.put("config", handler.getProvisionRequest());
        return new GsonBuilder().create().toJson(entity);
    }

    @Override
    public List<NameValuePair> getParameters() {
        ArrayList<NameValuePair> parameters = new ArrayList<>();

        if (handler != null) { return parameters; }
        if (healthCheck != null) {
            parameters.add(new BasicNameValuePair("healthCheck", healthCheck.toString()));
        }
        if (StringUtils.isNotBlank(providerNo)) {
            parameters.add(new BasicNameValuePair("providerNo", providerNo));
        }
        return parameters;
    }

    @Override
    public ProvisionResponse getResponse(CloseableHttpResponse response) throws IntegrationException {
        switch (method) {
            case GET:
                return this.handleConnectionResponse(response);
            case POST:
                return this.handleProvisionResponse(response);
            default:
                return new ProvisionResponse(400, "Unsupported method.");
        }
    }

    public ProvisionResponse handleConnectionResponse(CloseableHttpResponse response) throws IntegrationException {
        int status = response.getStatusLine().getStatusCode();
        switch (status) {
            case 200:
            case 206:
            case 404:
                ProvisionResponse provisionResponse = new ProvisionResponse(status);
                if (method == HttpMethod.GET) {
                    provisionResponse.setProvisioned(status != 404);
                    if (this.providerNo != null) {
                        provisionResponse.setProviderRegistered(status == 200);
                    }
                }
                return provisionResponse;
            case 401:
                return new ProvisionResponse(status, "Integration rejected system credentials.");
            default:
                return new ProvisionResponse(status, "Unexpected error encountered.");
        }
    }

    public ProvisionResponse handleProvisionResponse(CloseableHttpResponse response) throws IntegrationException {
        int status = response.getStatusLine().getStatusCode();
        switch (status) {
            case 200:
            case 201:
                ProvisionResponse provisionResponse = new ProvisionResponse(status);
                provisionResponse.setProvisioned(true);
                return provisionResponse;
            case 401:
                return new ProvisionResponse(status, "Integration rejected system credentials.");
            default:
                return new ProvisionResponse(status, "Unexpected error encountered.");
        }
    }

    public Boolean getHealthCheck() {
        return healthCheck;
    }

    public ProvisionHandler getHandler() {
        return handler;
    }

    public String getProviderNo() {
        return providerNo;
    }
}
