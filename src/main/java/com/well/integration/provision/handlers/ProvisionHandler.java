/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.provision.handlers;

import ca.oscarpro.security.EncryptionException;
import com.quatro.model.security.Secuserrole;
import com.well.integration.IntegrationException;
import com.well.integration.IntegrationManager;
import com.well.integration.config.ConfigPropertyKey;
import com.well.integration.config.GeneratorFactory;
import com.well.integration.config.parameters.ConfigParams;
import ca.oscarpro.security.OscarPasswordService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.text.RandomStringGenerator;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.Security;
import org.oscarehr.common.model.ServiceAccessToken;
import org.oscarehr.common.model.ServiceClient;
import org.oscarehr.phr.RegistrationHelper;

import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.UUID;


/**
 * Configures an integration connection and encapsulates
 * the parameters that must be communicated to integration to provision the connection.
 */
public class ProvisionHandler implements Handler {
    private ConfigPropertyKey keys;
    private String apiKey;
    private ConfigParams parameters;
    private ConfigParams generated;
    private IntegrationManager manager;
    private String password;
    private Provider provider;
    private Security security;
    private ServiceAccessToken serviceAccessToken;
    private ServiceClient client;
    private RandomStringGenerator randomStringGenerator;

    public ProvisionHandler() {
        randomStringGenerator = new RandomStringGenerator
                .Builder()
                .selectFrom("abcdefghijklmnopqrstuvwxyz0123456789".toCharArray())
                .build();
    }

    public ProvisionHandler(IntegrationManager manager, ConfigParams parameters) {
        this();
        this.initialize(manager, parameters);
    }

    @Override
    public void initialize(IntegrationManager manager, ConfigParams parameters) {
        this.manager = manager;
        this.parameters = parameters;
        if (parameters != null) {
            this.keys = parameters.getIntegration().getKeys();
        }
    }

    @Override
    public ProvisionHandler invoke() throws IntegrationException {
        if (parameters == null) {
            throw new IntegrationException("Missing config request.");
        }
        if (parameters.getSoapProviderNo() == null) {
            throw new IntegrationException("Null SOAP provider.");
        }
        password = StringUtils.trimToNull(parameters.getSoapProviderPassword()) == null
                ? RegistrationHelper.getNewRandomPassword() : parameters.getSoapProviderPassword();
        if ("create".equals(parameters.getSoapProviderNo())) {
            provider = createProvider();
            security = createSecurity(provider, password);
        } else {
            provider = manager.providerDao.getProvider(parameters.getSoapProviderNo());
            if (provider == null) {
                throw new IntegrationException("SOAP provider not found.");
            }
            security = updateSecurity(provider, password);
        }
        if ("create".equals(parameters.getRestClientId())) {
            client = createServiceClient();
        } else {
            client = manager.serviceClientDao.find(Integer.valueOf(parameters.getRestClientId()));
            if (client == null) {
                throw new IntegrationException("Missing REST client.");
            }
        }
        serviceAccessToken = createAccessToken(provider, client);
        createApiKey();
        savePreferences();
        return this;
    }

    private ConfigParams getGenerated() {
        if (generated == null) {
            generated = GeneratorFactory
                    .getGenerator(parameters.getIntegration())
                    .generateConfig(manager, parameters.getIntegration());
        }
        return generated;
    }

    protected Provider createProvider() {
        Provider p = new Provider();
        p.setProviderNo(manager.providerDao.suggestProviderNo());
        p.setLastName(getGenerated().getSoapProviderLastName());
        p.setFirstName(getGenerated().getSoapProviderFirstName());
        p.setProviderType("doctor");
        p.setSex("F");
        p.setSpecialty("Integration");
        p.setStatus("1");
        p.setPrescribeItType("doctor");
        manager.providerDao.saveProvider(p);
        return p;
    }

    protected Security createSecurity(Provider provider, String password) throws IntegrationException {
        Security s = new Security();
        s.setProviderNo(provider.getProviderNo());
        s.setUserName(RegistrationHelper.getNewRandomPassword());
        setSecurityPassword(s, password);
        setSecurityDefaults(s);
        manager.securityDao.persist(s);

        Secuserrole secUserRole = new Secuserrole();
        secUserRole.setProviderNo(provider.getProviderNo());
        secUserRole.setRoleName("doctor");
        secUserRole.setActiveyn(1);
        manager.secUserRoleDao.save(secUserRole);
        return s;
    }

    protected Security updateSecurity(Provider provider, String password) throws IntegrationException {
        Security s = manager.securityDao.getByProviderNo(provider.getProviderNo());
        // Create record if one does not exist.
        if (s == null) { return createSecurity(provider, password); }
        setSecurityPassword(s, password);
        // Should clear any lock, expiration or reset present.
        setSecurityDefaults(s);
        manager.securityDao.merge(s);
        return s;
    }

    private void setSecurityDefaults(Security security) {
        security.setPin(null);
        security.setBExpireset(0);
        security.setBLocallockset(0);
        security.setBRemotelockset(0);
        security.setDateExpiredate(null);
        security.setForcePasswordReset(false);
    }

    private void setSecurityPassword(Security security, String password) throws IntegrationException {
        try {
            security.setPassword(OscarPasswordService.encodePassword(password));
            OscarPasswordService.setPasswordVersion(security);
        } catch (NoSuchAlgorithmException | EncryptionException e) {
            throw new IntegrationException("Failed encoding password", e);
        }
    }

    protected ServiceClient createServiceClient() {
        ServiceClient client = new ServiceClient();
        client.setName(getGenerated().getRestClientName());
        client.setKey(randomStringGenerator.generate(16));
        client.setSecret(randomStringGenerator.generate(16));
        client.setUri("-1");
        client.setLifetime(-1);
        manager.serviceClientDao.persist(client);
        return client;
    }

    protected ServiceAccessToken createAccessToken(Provider provider, ServiceClient sc) {

        String tokenId = UUID.randomUUID().toString();
        String tokenSecret = UUID.randomUUID().toString();
        long issuedAt = System.currentTimeMillis() / 1000;

        ServiceAccessToken sat = new ServiceAccessToken();
        sat.setClientId(sc.getId());
        sat.setDateCreated(new Date());
        sat.setIssued(issuedAt);
        sat.setLifetime(sc.getLifetime());
        sat.setTokenId(tokenId);
        sat.setTokenSecret(tokenSecret);
        sat.setProviderNo(provider.getProviderNo());
        sat.setScopes("");
        manager.serviceAccessTokenDao.persist(sat);
        return sat;
    }

    protected void createApiKey() {
        apiKey = UUID.randomUUID().toString();
    }

    protected void savePreferences() {
        // Integration provisioning related preferences
        manager.systemPreferencesDao.mergeOrPersist(keys.CONFIGURED, "true");
        manager.systemPreferencesDao.mergeOrPersist(keys.REST_API_KEY, apiKey);
        manager.systemPreferencesDao.mergeOrPersist(keys.REST_CLIENT_ID, client.getId().toString());
        manager.systemPreferencesDao.mergeOrPersist(keys.SOAP_PROVIDER_NO, provider.getProviderNo());
    }

    @Override
    public ProvisionRequest getProvisionRequest() {
        return new ProvisionRequest(this);
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Provider getProvider() {
        return provider;
    }

    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    public Security getSecurity() {
        return security;
    }

    public void setSecurity(Security security) {
        this.security = security;
    }

    public ServiceAccessToken getServiceAccessToken() {
        return serviceAccessToken;
    }

    public void setServiceAccessToken(ServiceAccessToken serviceAccessToken) {
        this.serviceAccessToken = serviceAccessToken;
    }

    public ServiceClient getClient() {
        return client;
    }

    public void setClient(ServiceClient client) {
        this.client = client;
    }

    public ConfigParams getParameters() {
        return parameters;
    }

    public void setParameters(ConfigParams parameters) {
        this.parameters = parameters;
    }

    public IntegrationManager getManager() {
        return manager;
    }

    public void setManager(IntegrationManager manager) {
        this.manager = manager;
    }

    public ConfigPropertyKey getKeys() {
        return keys;
    }

    public void setKeys(ConfigPropertyKey keys) {
        this.keys = keys;
    }

    public RandomStringGenerator getRandomStringGenerator() {
        return randomStringGenerator;
    }

    public void setRandomStringGenerator(RandomStringGenerator randomStringGenerator) {
        this.randomStringGenerator = randomStringGenerator;
    }
}
