/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.provision.handlers;

import com.well.integration.IntegrationManager;
import com.well.integration.config.parameters.ConfigParams;
import com.well.integration.config.parameters.InsigConfigParams;

public class InsigHandler extends ProvisionHandler {

    public InsigHandler() { super(); }

    public InsigHandler(IntegrationManager manager, ConfigParams parameters) {
        this.initialize(manager, parameters);
    }

    @Override
    protected void savePreferences() {
        super.savePreferences();
        saveInsigPreferences();
    }

    private void saveInsigPreferences() {
        // Third party link preferences
        InsigConfigParams parameters = (InsigConfigParams) getParameters();
        getManager().systemPreferencesDao.mergeOrPersist(
                "schedule_tp_link_enabled", String.valueOf(parameters.isTpLinkEnabled()));
        getManager().systemPreferencesDao.mergeOrPersist(
                "schedule_tp_link_type", parameters.isTpLinkEnabled() ? parameters.getTpLinkType() : "");
        getManager().systemPreferencesDao.mergeOrPersist(
                "schedule_tp_link_display", parameters.isTpLinkEnabled() ? parameters.getTpLinkDisplay() : "");
    }
}

