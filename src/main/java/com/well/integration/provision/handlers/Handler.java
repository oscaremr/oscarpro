/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.provision.handlers;

import com.well.integration.IntegrationException;
import com.well.integration.IntegrationManager;
import com.well.integration.config.parameters.ConfigParams;

public interface Handler {
    void initialize(IntegrationManager manager, ConfigParams parameters);

    ProvisionHandler invoke() throws IntegrationException;

    ConfigParams getParameters();

    ProvisionRequest getProvisionRequest();

    class ProvisionRequest {
        private String consumerKey;
        private String consumerSecret;
        private String oauthToken;
        private String oauthSecret;
        private String providerUsername;
        private String providerPassword;
        private String apiKey;

        public ProvisionRequest(ProvisionHandler config) {
            this.consumerKey = config.getClient().getKey();
            this.consumerSecret = config.getClient().getSecret();
            this.oauthToken = config.getServiceAccessToken().getTokenId();
            this.oauthSecret = config.getServiceAccessToken().getTokenSecret();
            this.providerUsername = config.getSecurity().getUserName();
            this.providerPassword = config.getPassword();
            this.apiKey = config.getApiKey();
        }

        public String getConsumerKey() {
            return consumerKey;
        }

        public String getConsumerSecret() {
            return consumerSecret;
        }

        public String getOauthToken() {
            return oauthToken;
        }

        public String getOauthSecret() {
            return oauthSecret;
        }

        public String getProviderUsername() {
            return providerUsername;
        }

        public String getProviderPassword() {
            return providerPassword;
        }

        public String getApiKey() {
            return apiKey;
        }
    }
}
