/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.provision;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.well.integration.Integration;
import com.well.integration.IntegrationManager;
import com.well.integration.IntegrationException;
import com.well.integration.client.Client;
import com.well.integration.config.ConfigProperty;
import com.well.integration.config.GeneratorFactory;
import com.well.integration.config.parameters.ConfigParams;
import org.oscarehr.util.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ProvisionServlet extends HttpServlet {
    private final Logger logger = LoggerFactory.getLogger(ProvisionServlet.class);

    private final List<String> LOCAL_IPS = Arrays.asList("127.0.0.1", "::1", "0:0:0:0:0:0:0:1");

    protected IntegrationManager manager = SpringUtils.getBean(IntegrationManager.class);

    private Integration integration;

    @Override
    protected void doGet(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
            throws ServletException, IOException {
        setIntegration(httpRequest.getParameter("integration"));
        ServletResponse configResponse = getResponse(httpRequest.getRemoteAddr());
        configResponse.write(httpResponse);
    }

    protected ServletResponse getResponse(String address) {
        // Use default integration client.
        return getResponse(address, null);
    }

    public ServletResponse getResponse(String address, Client client) {
        try {
            if (!LOCAL_IPS.contains(address)) {
                return ServletResponse.error(Response.Status.FORBIDDEN);
            }
            if (integration == null) {
                return new ServletResponse(Response.Status.BAD_REQUEST.getStatusCode(),
                        "Integration not found.");
            }
            ConfigProperty config = manager.configPropertyDao.getConfig(integration);
            ProvisionResponse healthCheck = ProvisionRequest.checkHealth(integration).execute(client);
            if (healthCheck.hasError()) {
                return new ServletResponse(healthCheck.getStatus(), healthCheck.getMessage());
            } else if (healthCheck.isProvisioned() && config.isConfigured()) {
                return new ServletResponse(Response.Status.NO_CONTENT.getStatusCode(),
                        "Connection already active.");
            }

            manager.saveConfig("-1", "localhost", getParameters(), true, client);
            return ServletResponse.ok();
        }
        catch (IntegrationException e) {
            logger.warn("WELL integration error", e);
            return ServletResponse.error(Response.Status.BAD_REQUEST, e);
        } catch (Throwable t) {
            logger.error("Unexpected error encountered", t);
            // This response should strictly return JSON and not the internal error page.
            return ServletResponse.error(Response.Status.INTERNAL_SERVER_ERROR, t);
        }
    }

    ConfigParams getParameters() {
        return GeneratorFactory
                .getGenerator(integration)
                .generateConfig(manager, integration);
    }

    public void setIntegration(String integration) {
        this.integration = manager.integrationDao.getIntegration(integration);
    }

    protected static class ServletResponse {

        Gson gson = new GsonBuilder()
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation()
                .create();

        @Expose
        protected int status;
        @Expose
        protected String message;

        public ServletResponse(int status, String message) {
            this.status = status;
            this.message = message;
        }

        protected static ServletResponse ok() {
            return new ServletResponse(200, "OK");
        }

        protected static ServletResponse error(Response.Status status) {
            return new ServletResponse(status.getStatusCode(), status.getReasonPhrase());
        }

        protected static ServletResponse error(Response.Status status, Throwable e) {
            return new ServletResponse(status.getStatusCode(), e.getMessage());
        }

        protected void write(HttpServletResponse response) throws IOException {
            setContentType(response);
            gson.toJson(this, response.getWriter());
        }

        protected void setContentType(HttpServletResponse response) {
            response.setContentType(MediaType.APPLICATION_JSON);
        }
    }
}