/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.provision;


import com.well.util.ClassFactory;
import com.well.integration.Integration;
import com.well.integration.provision.handlers.Handler;
import com.well.integration.provision.handlers.ProvisionHandler;
import org.apache.commons.lang3.StringUtils;

public class ProvisionFactory extends ClassFactory<Handler> {

    private static ProvisionFactory instance;

    public static ProvisionFactory getInstance() {
        if (instance == null) { instance = new ProvisionFactory(); }
        return instance;
    }

    public ProvisionFactory() {
        super("com.well.integration.provision.handlers.", "Handler", ProvisionHandler.class);
    }

    public static Handler getHandler(Integration integration) {
        if (integration == null) { return getInstance().getDefaultInstance(); }
        return getInstance().newInstance(StringUtils.capitalize(integration.getName()));
    }
}
