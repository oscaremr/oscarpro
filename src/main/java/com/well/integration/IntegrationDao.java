/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration;

import com.well.integration.config.ConfigProperty;
import com.well.integration.config.ConfigPropertyDao;
import com.well.integration.config.ConfigPropertyKey;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.StringUtils;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.SystemPreferences;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class IntegrationDao {
	public final static String INTEGRATION_MODULES = "integration.modules";

	@Autowired
	public SystemPreferencesDao systemPreferencesDao;

	@Autowired
	public ConfigPropertyDao configPropertyDao;

	private Map<String, Integration> integrationMap;
	private List<Integration> integrations;

	@PostConstruct
	public Map<String, Integration> initializeIntegrations() {
		integrationMap = new HashMap<>();
		integrations = new ArrayList<>();

		String integrationModules = getIntegrationModules();
		if (StringUtils.isBlank(integrationModules)) { return integrationMap; }
		String[] modules = integrationModules.trim().split(",");

		for (String module : modules) {
			if (StringUtils.isBlank(module)) { continue; }
			ConfigPropertyKey keys = new ConfigPropertyKey(module);
			ConfigProperty config = configPropertyDao.getConfig(keys);
			Integration integration = new Integration(module, config.getDisplayName(), keys);
			integrations.add(integration);
			integrationMap.put(module.toLowerCase(), integration);
		}
		return integrationMap;
	}

	public List<Integration> getEnabled() {
		ArrayList<Integration> integrations = new ArrayList<>();
		for (Map.Entry<String, Integration> entry : integrationMap.entrySet()) {
			if (systemPreferencesDao.findPreferenceByName(entry.getValue().getKeys().ENABLED).getValueAsBoolean()) {
				integrations.add(entry.getValue());
			}
		}
		Collections.sort(integrations);
		return integrations;
	}

	public List<Integration> getIntegrations() {
		return new ArrayList<>(integrationMap.values());
	}

	public Integration getIntegration(String integration) {
		return integration == null ? null : integrationMap.get(integration.toLowerCase());
	}

	public String getIntegrationModules() {
		SystemPreferences integrations = systemPreferencesDao.findPreferenceByName(INTEGRATION_MODULES);
		return integrations == null ?  null : integrations.getValue();
	}

	public void setIntegrationModules(String modules) {
		systemPreferencesDao.mergeOrPersist(IntegrationDao.INTEGRATION_MODULES, modules);
		initializeIntegrations();
	}

	// Currently only insig requires support for third party link options during integration.
	public boolean hasScheduleOptions(Integration integration) {
		return isInsig(integration);
	}

	// Currently only insig has support for subscriptions.
	public boolean hasSubscriptions(Integration integration) {
		return isInsig(integration);
	}

	private boolean isInsig(Integration integration) {
		return "insig".equalsIgnoreCase(integration.getName());
	}

	public String getIntegrationAdminUrl(Integration integration) {
		StringBuilder builder = new StringBuilder("/well/admin/integration/#!/");
		builder.append(integration.getName());
		builder.append('?');
		boolean hasParameter = false;
		if (hasScheduleOptions(integration)) {
			builder.append("schedule=true");
			hasParameter = true;
		}
		if (hasSubscriptions(integration)) {
			if (hasParameter) { builder.append('&'); }
			builder.append("subscription=true");
		}
		return builder.toString();
	}
}
