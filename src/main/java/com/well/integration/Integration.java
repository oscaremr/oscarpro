/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration;

import com.well.integration.config.ConfigPropertyKey;

import java.util.Objects;

public class Integration implements Comparable<Integration> {
    private final String name;
    private final String displayName;
    private transient final ConfigPropertyKey keys;

    public Integration(String name, String displayName, ConfigPropertyKey keys) {
        this.name = name;
        this.displayName = displayName;
        this.keys = keys;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public ConfigPropertyKey getKeys() {
        return keys;
    }

    @Override
    public int compareTo(Integration o) {
        return this.getDisplayName().compareTo(o.getDisplayName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Integration that = (Integration) o;
        return getName().equals(that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}