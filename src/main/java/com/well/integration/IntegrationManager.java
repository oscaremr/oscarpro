/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration;

import com.quatro.dao.security.SecuserroleDao;
import com.well.integration.client.Client;
import com.well.integration.client.Response;
import com.well.integration.config.ConfigProperty;
import com.well.integration.config.ConfigPropertyDao;
import com.well.integration.config.parameters.ConfigParams;
import com.well.integration.provision.ProvisionFactory;
import com.well.integration.provision.ProvisionRequest;
import com.well.integration.provision.ProvisionResponse;
import com.well.integration.provision.handlers.Handler;
import org.apache.log4j.Logger;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.SecurityDao;
import org.oscarehr.common.dao.ServiceAccessTokenDao;
import org.oscarehr.common.dao.ServiceClientDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.util.MiscUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import oscar.log.LogAction;

@Service
public class IntegrationManager {

	@Autowired
	public ConfigPropertyDao configPropertyDao;

	@Autowired
	public IntegrationDao integrationDao;

	@Autowired
	public ProviderDao providerDao;

	@Autowired
	public SecurityDao securityDao;

	@Autowired
	public SecuserroleDao secUserRoleDao;

	@Autowired
	public ServiceAccessTokenDao serviceAccessTokenDao;

	@Autowired
	public ServiceClientDao serviceClientDao;

	@Autowired
	public SystemPreferencesDao systemPreferencesDao;
	
	private Logger logger = MiscUtils.getLogger();

	public IntegrationManager() { }

	public ProvisionResponse saveConfig(String providerNo, String ip, ConfigParams configParams) throws IntegrationException {
		return this.saveConfig(providerNo, ip, configParams, false, null);
	}

	public ProvisionResponse saveConfig(String providerNo,
										String ip,
										ConfigParams configParams,
										boolean skipConnectionCheck,
										Client client) throws IntegrationException {
		try {
			if (configParams == null) {
				throw new IntegrationException("Missing config request.");
			}
			Integration integration = configParams.getIntegration();
			ConfigProperty config = configPropertyDao.getConfig(integration);
			if (!config.isEnabled()) {
				throw new IntegrationException("Integration is not enabled.");
			}
			if (!config.hasApiKey()) {
				throw new IntegrationException("Integration API key is not configured in properties file.");
			}
			if (!config.hasClientId()) {
				throw new IntegrationException("Integration client ID is not configured in properties file.");
			}

			// Confirm availability of integration before attempting to configure credentials.
			if (!skipConnectionCheck) {
				checkResponse(ProvisionRequest.checkConnection(integration).execute(client));
			}

			// Perform local setup for integration provisioning.
			Handler handler = ProvisionFactory.getHandler(integration);
			handler.initialize(this, configParams);
			handler.invoke();

			// Post credentials
			checkResponse(ProvisionRequest.sendConfiguration(handler).execute(client));

			// Confirm success
			ProvisionResponse response = ProvisionRequest.checkHealth(integration).execute(client);

			// A provisioned system should not return 404
			if (response.hasError() || response.getStatus() == 404) {
				throw new IntegrationException("Failed to configure credentials.");
			}

			log(providerNo, ip, "Successfully configured.");
			return response;
		}
		catch (IntegrationException e) {
			log(providerNo, ip, e.getMessage());
			logger.warn("Unable to save config", e);
			throw e;
		}
	}

	public void checkResponse(Response response) throws IntegrationException {
		if (response.hasError()) { throw new IntegrationException(response.getMessage()); }
	}

	public void log(String providerNo, String ip, String message) {
		LogAction.addLog(providerNo,"Integration Admin", message, "", ip);
	}
}
