package com.indivica.olis.queries;

public interface RequestingHicQuery {
    String getRequestingHicId();
}
