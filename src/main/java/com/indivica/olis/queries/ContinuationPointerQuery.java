package com.indivica.olis.queries;

public interface ContinuationPointerQuery {

    String getContinuationPointer();

    void setContinuationPointer(String continuationPointer);
}
