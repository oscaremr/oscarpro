/**
 * Copyright (c) 2024 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.model.OscarLog;
import org.oscarehr.util.DeamonThreadFactory;
import org.oscarehr.util.LoggedInInfo;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class LogService {
  private final ExecutorService executorService = Executors.newCachedThreadPool(
      new DeamonThreadFactory(LogService.class.getSimpleName() + ".executorService",
          Thread.MAX_PRIORITY));

  public void log(final LoggedInInfo loggedInInfo, final String action, final String content,
      final String contentId, final String demographicId, final String data) {
    val oscarLog = new OscarLog();
    if (loggedInInfo.getLoggedInSecurity() != null) {
      oscarLog.setSecurityId(loggedInInfo.getLoggedInSecurity().getSecurityNo());
    }

    if (loggedInInfo.getLoggedInProvider() != null) {
      oscarLog.setProviderNo(loggedInInfo.getLoggedInProviderNo());
    }
    oscarLog.setAction(action);
    oscarLog.setContent(content);
    oscarLog.setContentId(contentId);
    oscarLog.setIp(loggedInInfo.getIp());

    try {
      val trimmedDemographicId = StringUtils.trimToNull(demographicId);
      if (trimmedDemographicId != null) {
        oscarLog.setDemographicId(Integer.parseInt(trimmedDemographicId));
      }
    } catch (Exception ex) {
      log.error("Unexpected error while audit logging " + action, ex);
    }
    oscarLog.setData(data);
    executorService.execute(new AddLogExecutorTask(oscarLog));
  }
}
