/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarMessenger.tld;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.model.UserProperty;
import org.oscarehr.util.DbConnectionFilter;
import org.oscarehr.util.MiscUtils;

import org.oscarehr.util.SpringUtils;
import oscar.util.SqlUtils;

public class MsgNewMessageTag extends TagSupport {
    UserPropertyDAO propDao = SpringUtils.getBean(UserPropertyDAO.class);


    public MsgNewMessageTag()    {
        numNewMessages = 0;
        numNewDemographicMessages = 0;
    }

    public void setProviderNo(String providerNo1)    {
       providerNo = providerNo1;
    }

    public String getProviderNo()    {
        return providerNo;
    }

    public int doStartTag() throws JspException    {
       
            Connection c = null;
            PreparedStatement ps = null;
            ResultSet rs = null;
            UserProperty enhancedOrClassic = propDao.getProp(providerNo, UserProperty.ENHANCED_OR_CLASSIC);
            UserProperty erxEnabled = propDao.getProp("erx.enabled");
            boolean isEnhancedAndErxEnabled = (enhancedOrClassic != null && StringUtils.trimToEmpty(enhancedOrClassic.getValue()).equals("E")) 
                    && (erxEnabled != null && Boolean.parseBoolean(erxEnabled.getValue()));
            
        try {
                c = DbConnectionFilter.getThreadLocalDbConnection();
                						//String sql = new String("select count(*) from messagelisttbl where provider_no ='"+ providerNo +"' and status = 'new' ");
                String sql = "select (select count(*) from messagelisttbl m " +
                        "LEFT JOIN oscarcommlocations o ON m.remoteLocation = o.locationId " +
                        "where m.provider_no = ? " +
                        "and (m.status = 'new' or m.status = 'unread') " +
                        "and o.current1=1) ";
                if (isEnhancedAndErxEnabled) {
                    sql += "+" +
                            "(select count(*) from clinician_communication_provider_routing ccpr " +
                            "where ccpr.provider_no = ? " +
                            "and ccpr.status = 'NEW')" +
                            "+" +
                            "(select count(*) from erx_task et " +
                            "where et.provider_no = ? " +
                            "and et.type='p160-m' " +
                            "and et.archived = 0);";
                }

                ps = c.prepareStatement(sql);
                ps.setString(1,providerNo);
                if (isEnhancedAndErxEnabled) {
                    ps.setString(2, providerNo);
                    ps.setString(3, providerNo);
                }
                rs = ps.executeQuery();                
                while (rs.next()) {
                   numNewMessages = (rs.getInt(1));
                }
                
                String sqlCommand="select count(*) from messagelisttbl,msgDemoMap where provider_no =? and (status = 'new' or status = 'unread') and messageID = message" ;
                ps = c.prepareStatement(sqlCommand);
                ps.setString(1,providerNo);
                rs = ps.executeQuery();
                while (rs.next()) {
                	numNewDemographicMessages = (rs.getInt(1));
                }
            }
            catch (SQLException e) {
                throw (new HibernateException(e));
            }
            finally {
                SqlUtils.closeResources(c, ps, rs);
            }
        
        try        {
            JspWriter out = super.pageContext.getOut();
            if(numNewMessages > 0)
                out.print("<span class='tabalert'>");
            else
                out.print("<span>");
        } catch(Exception p) {MiscUtils.getLogger().error("Error",p);
        }
        return(EVAL_BODY_INCLUDE);
    }

    public int doEndTag()        throws JspException    {
     //ronnie 2007-4-26
       try{
          JspWriter out = super.pageContext.getOut();
          if (numNewMessages > 0)
              out.print("<sup>"+numNewDemographicMessages+"/"+numNewMessages+"</sup></span>  ");
          else
              out.print("</span>  ");
       }catch(Exception p) {MiscUtils.getLogger().error("Error",p);
       }
       return EVAL_PAGE;
    }

    private String providerNo;
    private int numNewMessages;
    private int numNewDemographicMessages;
}
