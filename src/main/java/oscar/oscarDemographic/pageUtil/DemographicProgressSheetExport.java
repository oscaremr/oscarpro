/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.oscarDemographic.pageUtil;

import cds.NewCategoryDocument;
import cdsDt.ResidualInformation;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.OscarAppointmentDao;
import org.oscarehr.common.model.Appointment;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.Provider;
import org.oscarehr.util.SpringUtils;
import org.progressSheet.dao.BillingSheetDao;
import org.progressSheet.dao.DrugRecordDao;
import org.progressSheet.dao.ProgressSheetDao;
import org.progressSheet.model.BillingFormField;
import org.progressSheet.model.BillingSheet;
import org.progressSheet.model.DrugRecord;
import org.progressSheet.model.FormField;
import org.progressSheet.model.ProgressSheet;
import oscar.util.StringUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DemographicProgressSheetExport {
	private static final String SIGNING_PROVIDER_PREFIX = "signing";
	private static final String APPOINTMENT_PROVIDER_PREFIX = "appointment";
	private static final String BILLING_PROVIDER_PREFIX = "billing";

	public void createXmlFromDemographicProgressSheets(Demographic demographic, NewCategoryDocument.NewCategory omdCdsNewCategory, ArrayList<String> exportError) {
		omdCdsNewCategory.setCategoryName("PCC Progress Sheets");
		omdCdsNewCategory.setCategoryDescription("Form data from the PCC Progress Sheet created by Well Health Technologies Corp");

		ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
		ProgressSheetDao progressSheetDao = SpringUtils.getBean(ProgressSheetDao.class);
		BillingSheetDao billingSheetDao = SpringUtils.getBean(BillingSheetDao.class);
		DrugRecordDao drugRecordDao = SpringUtils.getBean(DrugRecordDao.class);
		OscarAppointmentDao appointmentDao = SpringUtils.getBean(OscarAppointmentDao.class);
		List<ProgressSheet> progressSheetsToExport = progressSheetDao.getProgressSheetsByDemographic(demographic.getDemographicNo());

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

		for (int i = 0; i < progressSheetsToExport.size(); i++) {
			ProgressSheet ps = progressSheetsToExport.get(i);
			Appointment appointment = appointmentDao.find(ps.getAppointmentId());
			BillingSheet bs = billingSheetDao.getBillingSheetByProgressSheetId(ps.getFormId());
			if (bs == null) {
				exportError.add("Warning! Billing sheet entry for progress sheet id " + ps.getFormId() + " does not exist");
			}

			ResidualInformation residualInformation = omdCdsNewCategory.addNewResidualInfo();
			
			// add demographic info
			ResidualInformation.DataElement demographicName = residualInformation.addNewDataElement();
			demographicName.setName("demographicName");
			demographicName.setDataType("String");
			demographicName.setContent(ps.getDemographicName());
			ResidualInformation.DataElement demographicAddress = residualInformation.addNewDataElement();
			demographicAddress.setName("demographicAddress");
			demographicAddress.setDataType("String");
			demographicAddress.setContent(ps.getDemographicAddress());
			ResidualInformation.DataElement demographicDob = residualInformation.addNewDataElement();
			demographicDob.setName("demographicDob");
			demographicDob.setDataType("String");
			demographicDob.setContent(ps.getDemographicDob());
			// add encounter date
			ResidualInformation.DataElement encounterDate = residualInformation.addNewDataElement();
			encounterDate.setName("encounterDate");
			encounterDate.setDataType("Date");
			encounterDate.setContent(sdf.format(ps.getEncounterDate()));
			// add appointment date
			ResidualInformation.DataElement appointmentDate = residualInformation.addNewDataElement();
			appointmentDate.setName("appointmentDate");
			appointmentDate.setDataType("Date");
			appointmentDate.setContent(sdf.format(appointment.getStartTimeAsFullDate()));

			// add appointment provider and ohip id
			Provider appointmentProvider = providerDao.getProvider(ps.getAppointmentProviderId());
			if (appointmentProvider != null) {
				addProvider(residualInformation, appointmentProvider, APPOINTMENT_PROVIDER_PREFIX);
			} else {
				exportError.add("Warning! Appointment Provider for progress sheet \""+ps.getFormId()+"\" does not exist!");
			}

			// add form fields
			for (FormField formField : ps.getFormFields()) {
				ResidualInformation.DataElement formFieldResidualInfo = residualInformation.addNewDataElement();
				formFieldResidualInfo.setName("formField_" + formField.getFieldKey());
				formFieldResidualInfo.setDataType("String");
				formFieldResidualInfo.setContent(formField.getFieldValue());
			}
			
			// add diagram
			if (ps.getPainDiagram() != null) {
				ResidualInformation.DataElement painDiagramJsonResidualInfo = residualInformation.addNewDataElement();
				painDiagramJsonResidualInfo.setName("painDiagram");
				painDiagramJsonResidualInfo.setDataType("String");
				painDiagramJsonResidualInfo.setContent(ps.getPainDiagram().getDiagram());
				ResidualInformation.DataElement painDiagramScaleResidualInfo = residualInformation.addNewDataElement();
				painDiagramScaleResidualInfo.setName("painDiagramScale");
				painDiagramScaleResidualInfo.setDataType("String");
				painDiagramScaleResidualInfo.setContent(ps.getPainDiagram().getScaledPercent());
			} else {
				exportError.add("Warning! Pain Diagram entry for progress sheet id " + ps.getFormId() + " does not exist");
			}
			
			// add signing provider and ohip id
			Provider signingProvider = providerDao.getProvider(ps.getProviderId());
			if (signingProvider != null) {
				addProvider(residualInformation, signingProvider, SIGNING_PROVIDER_PREFIX);
			} else {
				exportError.add("Warning! Signing Provider for progress sheet \""+ps.getFormId()+"\" does not exist!");
			}
			if (ps.getSignatureFile() != null) {
				ResidualInformation.DataElement providerSig = residualInformation.addNewDataElement();
				providerSig.setName(SIGNING_PROVIDER_PREFIX + "ProviderSignature");
				providerSig.setDataType("String");
				providerSig.setContent(ps.getSignatureFile());
			}


			if (bs != null) {
				Map<String, BillingFormField> billingFormFields = bs.getBillingSheetFieldsMap();
				// add billing provider
				if (billingFormFields.get("billing_provider") != null) {
					String billingProviderId = billingFormFields.get("billing_provider").getFieldValue();
					Provider billingProvider = providerDao.getProvider(billingProviderId);
					if (billingProvider != null) {
						addProvider(residualInformation, billingProvider, BILLING_PROVIDER_PREFIX);
						billingFormFields.remove("billing_provider");
					} else {
						exportError.add("Warning! Billing Provider for progress sheet \""+ps.getFormId()+"\" does not exist!");
					}
				}

				// add form fields
				for (BillingFormField formField : billingFormFields.values()) {
					ResidualInformation.DataElement billingFormFieldResidualInfo = residualInformation.addNewDataElement();
					billingFormFieldResidualInfo.setName("billingFormField_" + formField.getFieldKey());
					billingFormFieldResidualInfo.setDataType("String");
					billingFormFieldResidualInfo.setContent(formField.getFieldValue());
				}
			}
			
			// add all drug record entries
			List<DrugRecord> psDrugRecords = drugRecordDao.findAllByProgressSheet(ps.getFormId());
			for (DrugRecord psDrugRecord : psDrugRecords) {
				ResidualInformation.DataElement drugRecordResidualInfo = residualInformation.addNewDataElement();
				drugRecordResidualInfo.setName("drugRecord");
				drugRecordResidualInfo.setDataType("String");
				drugRecordResidualInfo.setContent(psDrugRecord.getOutline());
			}
		}
	}
	
	
	private void addProvider(ResidualInformation residualInformation, Provider provider, String providerRole) {
		if (StringUtils.filled(provider.getFirstName()) || StringUtils.filled(provider.getLastName())) {
			ResidualInformation.DataElement providerLastName = residualInformation.addNewDataElement();
			providerLastName.setName(providerRole + "ProviderLastName");
			providerLastName.setDataType("String");
			providerLastName.setContent(provider.getLastName());
			ResidualInformation.DataElement providerFirstName = residualInformation.addNewDataElement();
			providerFirstName.setName(providerRole + "ProviderFirstName");
			providerFirstName.setDataType("String");
			providerFirstName.setContent(provider.getFirstName());
			if (StringUtils.noNull(provider.getOhipNo()).length() <= 6) {
				ResidualInformation.DataElement providerOhip = residualInformation.addNewDataElement();
				providerOhip.setName(providerRole + "ProviderOhip");
				providerOhip.setDataType("String");
				providerOhip.setContent(provider.getOhipNo());
			}
		}
	}
}
