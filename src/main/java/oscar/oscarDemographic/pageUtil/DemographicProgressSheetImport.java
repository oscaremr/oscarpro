/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.oscarDemographic.pageUtil;

import cds.NewCategoryDocument;
import cdsDt.ResidualInformation;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.util.SpringUtils;
import org.progressSheet.dao.DrugRecordDao;
import org.progressSheet.dao.ProgressSheetService;
import org.progressSheet.model.BillingFormField;
import org.progressSheet.model.BillingSheet;
import org.progressSheet.model.DrugRecord;
import org.progressSheet.model.FormField;
import org.progressSheet.model.PainDiagram;
import org.progressSheet.model.ProgressSheet;
import oscar.util.StringUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DemographicProgressSheetImport {
	
	boolean matchProviderNames;
	List<String> importErrors;
	
	public DemographicProgressSheetImport(boolean matchProviderNames, ArrayList<String> importErrors) {
		this.matchProviderNames = matchProviderNames;
		this.importErrors = importErrors;
	}

	public void parseXmlFromDemographicProgressSheets(Demographic demographic, NewCategoryDocument.NewCategory progressSheets, Map<Date, Integer> appointmentDateIdMap) {
		ProgressSheetService progressSheetService = SpringUtils.getBean(ProgressSheetService.class);
		DrugRecordDao drugRecordDao = SpringUtils.getBean(DrugRecordDao.class);
		ImportDemographicDataUtils importDataUtils = ImportDemographicDataUtils.getInstance(matchProviderNames, importErrors);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");


		for (int i = 0; i < progressSheets.sizeOfResidualInfoArray(); i++) {
			cdsDt.ResidualInformation progressSheetInfoArray = progressSheets.getResidualInfoArray(i);
			
			Map<String, String> progressSheetStringDataMap = new HashMap<String, String>();
			Map<String, Date> progressSheetDateDataMap = new HashMap<String, Date>();
			List<FormField> formFields = new ArrayList<FormField>();
			Map<String, BillingFormField> billingFormFieldsDataMap = new HashMap<String, BillingFormField>();
			List<String> drugRecordOutline = new ArrayList<String>();
			cdsDt.ResidualInformation.DataElement[] progressSheetData = progressSheetInfoArray.getDataElementArray();
			String fieldName;
			String fieldValue;
			for (ResidualInformation.DataElement progressSheetDatum : progressSheetData) {
				if (progressSheetDatum.getName().startsWith("formField_")) { // add standard formfield data to list of FormFields
					fieldName = progressSheetDatum.getName().replaceFirst("formField_", "");
					fieldValue = progressSheetDatum.getContent();
					formFields.add(new FormField(fieldName, fieldValue));
				} else if (progressSheetDatum.getName().startsWith("billingFormField_")) { // add billing formfield data to map of BillingFormFields
					fieldName = progressSheetDatum.getName().replaceFirst("billingFormField_", "");
					fieldValue = progressSheetDatum.getContent();
					billingFormFieldsDataMap.put(fieldName, new BillingFormField(fieldName, fieldValue));
				} else if (progressSheetDatum.getName().startsWith("drugRecord")) { // add drug records
					drugRecordOutline.add(progressSheetDatum.getContent());
				} else if (progressSheetDatum.getDataType().equals("String")) { // add drug records
					progressSheetStringDataMap.put(progressSheetDatum.getName(), progressSheetDatum.getContent());
				} else if (progressSheetDatum.getDataType().equals("Date")) {
					try {
						progressSheetDateDataMap.put(progressSheetDatum.getName(), sdf.parse(progressSheetDatum.getContent()));
					} catch (ParseException e) {
						importErrors.add("Cannot parse progress sheet date " + progressSheetDatum.getName() + " for demographic " + demographic.getDemographicNo() + ": " + progressSheetDatum.getContent());
					}
				}
			}
			
			ProgressSheet ps = new ProgressSheet();
			ps.setDemographicId(demographic.getDemographicNo());
			ps.setDemographicName(progressSheetStringDataMap.get("demographicName"));
			ps.setDemographicAddress(progressSheetStringDataMap.get("demographicAddress"));
			ps.setDemographicDob(progressSheetStringDataMap.get("demographicDob"));
			ps.setEncounterDate(progressSheetDateDataMap.get("encounterDate"));
			if (progressSheetStringDataMap.get("signingProviderSignature") != null) {
				ps.setSignatureFile(progressSheetStringDataMap.get("signingProviderSignature"));
			}

			ps.setFormFields(formFields);
			if (progressSheetStringDataMap.get("painDiagram") != null) {
				PainDiagram painDiagram = new PainDiagram();
				painDiagram.setDiagram(progressSheetStringDataMap.get("painDiagram"));
				painDiagram.setScaledPercent(progressSheetStringDataMap.get("painDiagramScale"));
				ps.setPainDiagram(painDiagram);
			} else {
				importErrors.add("Progress Sheet has no Pain Diagram ("+(i+1)+")");
			}
			
			if (appointmentDateIdMap.get(progressSheetDateDataMap.get("appointmentDate")) != null) {
				ps.setAppointmentId(appointmentDateIdMap.get(progressSheetDateDataMap.get("appointmentDate")));
			} else {
				importErrors.add("Progress Sheet has no appointment id for date " + progressSheetDateDataMap.get("appointmentDate") + " ("+(i+1)+")");
			}

			// Appointment provider
			String appointmentProviderNo = null;
			String appointmentProviderLastName = progressSheetStringDataMap.get("appointmentProviderLastName");
			String appointmentProviderFirstName = progressSheetStringDataMap.get("appointmentProviderFirstName");
			if (appointmentProviderLastName != null || appointmentProviderFirstName != null) {
				String appointmentProviderOhipId = progressSheetStringDataMap.get("appointmentProviderOhip");
				appointmentProviderNo = importDataUtils.writeProviderData(appointmentProviderFirstName, appointmentProviderLastName, appointmentProviderOhipId, null, "0");
				if (StringUtils.empty(appointmentProviderNo)) {
					appointmentProviderNo = importDataUtils.defaultProviderNo();
					importErrors.add("Progress Sheet has no appointment provider; assigned to \"doctor oscardoc\" ("+(i+1)+")");
				}
			}
			ps.setAppointmentProviderId(appointmentProviderNo);

			// Signing provider
			String signingProviderId = null;
			String signingProviderLastName = progressSheetStringDataMap.get("signingProviderLastName");
			String signingProviderFirstName = progressSheetStringDataMap.get("signingProviderFirstName");
			if (signingProviderLastName != null || signingProviderFirstName != null) {
				String signingProviderOhipId = progressSheetStringDataMap.get("signingProviderOhip");
				signingProviderId = importDataUtils.writeProviderData(signingProviderLastName, signingProviderFirstName, signingProviderOhipId, null, "0");
				if (StringUtils.empty(signingProviderId)) {
					signingProviderId = importDataUtils.defaultProviderNo();
					importErrors.add("Progress Sheet has no signing provider; assigned to \"doctor oscardoc\" ("+(i+1)+")");
				}
			}
			
			// Billing provider
			String billingProviderId = null;
			String billingProviderLastName = progressSheetStringDataMap.get("billingProviderLastName");
			String billingProviderFirstName = progressSheetStringDataMap.get("billingProviderFirstName");
			if (billingProviderLastName != null || billingProviderFirstName != null) {
				String billingProviderOhipId = progressSheetStringDataMap.get("billingProviderOhip");
				billingProviderId = importDataUtils.writeProviderData(billingProviderLastName, billingProviderFirstName, billingProviderOhipId, null, "0");
				if (StringUtils.empty(signingProviderId)) {
					billingProviderId = importDataUtils.defaultProviderNo();
					importErrors.add("AProgress Sheet has no billing provider; assigned to \"doctor oscardoc\" ("+(i+1)+")");
				}
			}
			billingFormFieldsDataMap.put("billing_provider", new BillingFormField("billing_provider", billingProviderId));
			
			ps.setProviderId(signingProviderId);
			progressSheetService.addProgressSheet(ps);
			
			BillingSheet bs = new BillingSheet();
			bs.setProgressSheetId(ps.getFormId());
			bs.setDemographicId(ps.getDemographicId());
			bs.setProviderId(ps.getProviderId());
			bs.setAppointmentId(ps.getAppointmentId());
			bs.setEncounterDate(ps.getEncounterDate());
			
			bs.setFormFields(new ArrayList<BillingFormField>(billingFormFieldsDataMap.values()));
			progressSheetService.addBillingBillingSheetDaoSheet(bs);


			List<DrugRecord> psDrugRecords = new ArrayList<DrugRecord>();
			for (String drugRecord : drugRecordOutline) {
				psDrugRecords.add(new DrugRecord(ps.getFormId(), drugRecord));
			}
			drugRecordDao.addDrugRecords(psDrugRecords);
		}
	}
}
