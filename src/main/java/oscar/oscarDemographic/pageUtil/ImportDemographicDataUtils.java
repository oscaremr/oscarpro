/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.oscarDemographic.pageUtil;

import org.oscarehr.common.dao.ProviderDataDao;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.oscarProvider.data.ProviderData;
import oscar.util.StringUtils;
import java.util.List;

public class ImportDemographicDataUtils {

  private boolean matchProviderNames;
  private List<String> importErrors;

  public static ImportDemographicDataUtils getInstance(boolean matchProviderNames, List<String> importErrors) {
    ImportDemographicDataUtils dataUtils = new ImportDemographicDataUtils();
    dataUtils.setMatchProviderNames(matchProviderNames);
    dataUtils.setImportErrors(importErrors);
    return dataUtils;
  }

  public String writeProviderData(String firstName, String lastName, String ohipNo) {
    return writeProviderData(firstName, lastName, ohipNo, null, null);
  }

  public String writeProviderData(String firstName, String lastName, String ohipNo, String cpsoNo, String status) {
    ProviderData pd = getProviderByOhip(ohipNo);

    if (pd==null) {
      pd = getProviderByNames(firstName, lastName, matchProviderNames);
    }

    if (pd!=null) {
      return updateExternalProvider(firstName, lastName, ohipNo, cpsoNo, pd);
    }

    //Write as a new provider
    if (StringUtils.empty(firstName) && StringUtils.empty(lastName) && StringUtils.empty(ohipNo)) {
      return ""; //no information at all!
    }
    pd = new ProviderData();
    MiscUtils.getLogger().info("ADD EXTERNAL");

    try {
      pd.addExternalProvider(firstName, lastName, ohipNo, cpsoNo, status);
    } catch (Exception e) {
      e.printStackTrace();
      importErrors.add("Error adding external provider " + lastName + ", " + firstName);
      return null;
    }

    return pd.getProviderNo();
  }

  public ProviderData getProviderByOhip(String OhipNo) {
    ProviderData pd = new ProviderData();
    pd.getProviderWithOHIP(OhipNo);
    if (StringUtils.filled(pd.getProviderNo())) {
      pd.getProvider(pd.getProviderNo());
      return pd;
    } else {
      return null;
    }
  }

  public ProviderData getProviderByNames(String firstName, String lastName, boolean matchProviderNames) {
    ProviderData pd = new ProviderData();
    if (matchProviderNames) {
      pd.getProviderWithNames(firstName, lastName);
    } else {
      pd.getExternalProviderWithNames(firstName, lastName);
    }
    if (StringUtils.filled(pd.getProviderNo())) {
      pd.getProvider(pd.getProviderNo());
      return pd;
    }
    else return null;
  }

  public String updateExternalProvider(String firstName, String lastName, String ohipNo, String cpsoNo, ProviderData pd) {
    ProviderDataDao providerDataDao = (ProviderDataDao) SpringUtils.getBean("providerDataDao");
    // For external provider only
    if (pd==null) {
      return null;
    }
    if ( pd.getProviderNo().charAt(0)!='-') {
      return pd.getProviderNo();
    }

    org.oscarehr.common.model.ProviderData newpd = providerDataDao.findByProviderNo(pd.getProviderNo());
    if (StringUtils.empty(pd.getFirst_name())) {
      newpd.setFirstName(StringUtils.noNull(firstName));
    }
    if (StringUtils.empty(pd.getLast_name())) {
      newpd.setLastName(StringUtils.noNull(lastName));
    }
    if (StringUtils.empty(pd.getOhip_no())) {
      newpd.setOhipNo(ohipNo);
    }
    if (StringUtils.empty(pd.getPractitionerNo())) {
      newpd.setPractitionerNo(cpsoNo);
    }

    providerDataDao.merge(newpd);
    return newpd.getId();
  }

  public String defaultProviderNo() {
    ProviderData pd = getProviderByNames("doctor", "oscardoc", true);
    if (pd != null) {
      return pd.getProviderNo();
    }

    return writeProviderData("doctor", "oscardoc", "");
  }

  
  // accessor and set methods
  public boolean isMatchProviderNames() {
    return matchProviderNames;
  }

  public void setMatchProviderNames(boolean matchProviderNames) {
    this.matchProviderNames = matchProviderNames;
  }

  public List<String> getImportErrors() {
    return importErrors;
  }

  public void setImportErrors(List<String> importErrors) {
    this.importErrors = importErrors;
  }
}