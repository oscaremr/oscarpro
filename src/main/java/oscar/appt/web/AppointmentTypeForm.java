/**
 *
 * Copyright (c) 2005-2012. Centre for Research on Inner City Health, St. Michael's Hospital, Toronto. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for
 * Centre for Research on Inner City Health, St. Michael's Hospital,
 * Toronto, Ontario, Canada
 */

package oscar.appt.web;
import org.apache.struts.action.ActionForm;

public class AppointmentTypeForm  extends ActionForm {
    private int typeNo;
    private Integer id;
    private String name;
    private String notes;
	private String newReasonCode;
	private Integer reasonCode = 0;
    private String reason;
    private String location;
    private String resources;    
    private int duration;
    private String providerNo;
    private Integer templateId;
    private String enabled;

    private int auto_bill;
    private String rule_list;
    private String rule_delete_list;
        
    public int getTypeNo() {
		return typeNo;
	}
	public void setTypeNo(int typeNo) {
		this.typeNo = typeNo;
	}
	
	public Integer getId() {
    	return id;
    }
	public void setId(Integer id) {
    	this.id = id;
    }
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getNewReasonCode() {
		return newReasonCode;
	}
	public void setNewReasonCode(String newReasonCode) {
		this.newReasonCode = newReasonCode;
	}

	public Integer getReasonCode() {
		return reasonCode;
	}
	public void setReasonCode(Integer reasonCode) {
		this.reasonCode = reasonCode;
	}

	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}

	public String getResources() {
		return resources;
	}
	public void setResources(String resources) {
		this.resources = resources;
	}

	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}

    public String getProviderNo() {
        return providerNo;
    }
    public void setProviderNo(String providerNo) {
        if (providerNo != null && "".equals(providerNo)) {
            providerNo = null;
        }
        this.providerNo = providerNo;
    }

    public Integer getTemplateId() {
        return templateId;
    }
    public void setTemplateId(Integer templateId) {
        if (templateId != null && templateId == 0) {
            templateId = null;
        }
        this.templateId = templateId;
    }
    
    public String getEnabled() {
        return enabled;
    }
    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

	public int getAutoBill() {
		return auto_bill;
	}
	public void setAutoBill(int auto_bill) {
		this.auto_bill = auto_bill;
	}
	
	public String getRuleList() {
		return rule_list;
	}
	public void setRuleList(String rule_list) {
		this.rule_list = rule_list;
	}
	
	public String getRuleDeleteList() {
		return rule_delete_list;
	}
	public void setRuleDeleteList(String rule_delete_list) {
		this.rule_delete_list = rule_delete_list;
	}
	
}
