package oscar.eform.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.oscarehr.common.dao.EformDocsDao;
import org.oscarehr.common.model.EformDocs;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

//import oscar.OscarProperties;
import oscar.dms.EDocUtil;

public class EformAttachEForm {

	private static EformDocsDao eformDocsDao = (EformDocsDao)SpringUtils.getBean(EformDocsDao.class);

    public final static boolean ATTACHED = true;
    public final static boolean UNATTACHED = false;
    private String providerNo;
    private String demoNo;
    private String reqId;
    private ArrayList<String> docs;

    /** Creates a new instance of ConsultationAttachLabs */
    public EformAttachEForm(String provNo, String demo, String req, String[] d) {
        providerNo = provNo;
        demoNo = demo;
        reqId = req;
        docs = new ArrayList<String>(d.length);
// rich text editor does not use eform_indivica_attachment_enabled
        /*
        if (!OscarProperties.getInstance().isPropertyActive("eform_indivica_attachment_enabled")) {
	        for(int idx = 0; idx < d.length; ++idx ) {
	            docs.add(d[idx]);
	        }
        }
        else { */
        if(d.length > 0) {
	        //if dummy entry skip
	        if( !d[0].equals("0") ) {
	            for(int idx = 0; idx < d.length; ++idx ) {
	                if( d[idx].charAt(0) == 'E')
	                    docs.add(d[idx].substring(1));
	            }
	        }
        }
        //}
    }

    public void attach(LoggedInInfo loggedInInfo) {

    	//first we get a list of currently attached docs
		List<EformDocs> oldlist = EDocUtil.findEforms(loggedInInfo, demoNo, reqId, EDocUtil.ATTACHED);
				//.listDocs(loggedInInfo, demoNo, reqId, EDocUtil.ATTACHED);
		ArrayList<String> newlist = new ArrayList<String>();
		ArrayList<EformDocs> keeplist = new ArrayList<EformDocs>();
		boolean alreadyAttached;
		//add new documents to list and get ids of docs to keep attached
		for (int i = 0; i < docs.size(); ++i) {
			alreadyAttached = false;
			for (int j = 0; j < oldlist.size(); ++j) {
				if (((oldlist.get(j)).getDocumentNo()+"").equals(docs.get(i))) {
					alreadyAttached = true;
					keeplist.add(oldlist.get(j));
					break;
				}
			}
			if (!alreadyAttached) newlist.add(docs.get(i));
		}

		//now compare what we need to keep with what we have and remove association
		for (int i = 0; i < oldlist.size(); ++i) {
			if (keeplist.contains(oldlist.get(i))) continue;

			detachEformConsult(oldlist.get(i).getDocumentNo()+"", reqId);
		}

		//now we can add association to new list
		for (int i = 0; i < newlist.size(); ++i) {
			attachEformConsult(providerNo, newlist.get(i), reqId);
		}
    }

    public static void detachEformConsult(String LabNo, String consultId) {
    	List<EformDocs> eformDocs = eformDocsDao.findByRequestIdDocNoDocType(Integer.parseInt(consultId), Integer.parseInt(LabNo), EformDocs.DOCTYPE_EFORM);
    	for(EformDocs eformDoc:eformDocs) {
    		eformDoc.setDeleted("Y");
    		eformDocsDao.merge(eformDoc);
    	}
    }

    public static void attachEformConsult(String providerNo, String LabNo, String consultId) {
    	EformDocs eformDoc = new EformDocs();
    	eformDoc.setRequestId(Integer.parseInt(consultId));
    	eformDoc.setDocumentNo(Integer.parseInt(LabNo));
    	eformDoc.setDocType(EformDocs.DOCTYPE_EFORM);
    	eformDoc.setAttachDate(new Date());
    	eformDoc.setProviderNo(providerNo);
    	eformDocsDao.persist(eformDoc);
    }
}
