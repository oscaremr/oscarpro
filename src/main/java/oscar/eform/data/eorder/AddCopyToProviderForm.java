/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.eform.data.eorder;

import javax.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

@Getter
@Setter
@NoArgsConstructor
public final class AddCopyToProviderForm extends ActionForm {
  private Integer id;
  private String ontarioLifeLabsId;
  private String roverId;
  private String onProvincialGovtId;
  private String pathnetCode;
  private String salutation;
  private String lastName;
  private String firstName;
  private String middleName;
  private String address1;
  private String address2;
  private String city;
  private String stateOrProvince;
  private String zipOrPostal;
  private String country;
  private String region;
  private String specialty;
  private boolean deleted;
  private boolean visible;
  private String type;

  private String cpso;

  private String billingNumber;


  public AddCopyToProviderForm(
      final String firstName,
      final String lastName,
      final String address1,
      final String city,
      final String stateOrProvince,
      final String country,
      final String zipOrPostal,
      final String type,
      final Integer id,
      final String cpso,
      final String billingNumber
  ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.address1 = address1;
    this.city = city;
    this.stateOrProvince = stateOrProvince;
    this.country = country;
    this.zipOrPostal = zipOrPostal;
    this.type = type;
    this.id = id;
    this.cpso = cpso;
    this.billingNumber = billingNumber;
  }
  @Override
  public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
    ActionErrors errors = new ActionErrors();
    if (firstName == null || firstName.length() == 0)
      errors.add("firstName", new ActionMessage("Errors.Firstname"));
    if (lastName == null || lastName.length() == 0)
      errors.add("lastName", new ActionMessage("Errors.Lastname"));
    if (city == null || city.length() == 0) errors.add("city", new ActionMessage("Errors.City"));
    if (address1 == null || address1.length() == 0)
      errors.add("address", new ActionMessage("Errors.Address"));
    if (stateOrProvince == null || stateOrProvince.length() == 0)
      errors.add("stateOrProvince", new ActionMessage("Errors.StateOrProvince"));
    if (zipOrPostal == null || zipOrPostal.length() == 0)
      errors.add("zipOrPostal", new ActionMessage("Errors.ZipOrPostal"));

    return errors;
  }

  @Override
  public void reset(ActionMapping mapping, HttpServletRequest request) {
    resetForm();
  }

  public void resetForm() {

    id = null;
    firstName = null;
    lastName = null;
    address1 = null;
    city = null;
    stateOrProvince = null;
    country = null;
    stateOrProvince = null;
    ontarioLifeLabsId = null;
    roverId = null;
    onProvincialGovtId = null;
    pathnetCode = null;
    salutation = null;
    middleName = null;
    address2 = null;
    region = null;
    specialty = null;
    deleted = false;
    visible = true;
    type = null;
  }
}
