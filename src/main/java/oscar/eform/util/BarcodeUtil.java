package oscar.eform.util;

import java.lang.reflect.Field;
import net.sourceforge.barbecue.linear.code39.Code39Barcode;

public class BarcodeUtil {
  public static String addCheckDigit(String barcode) {
    if (barcode != null) {
      final String charSet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%";
      int total = 0;
      for (char c : barcode.trim().toUpperCase().toCharArray()) {
        total += charSet.indexOf(c);
      }
      return barcode + charSet.charAt(total % 43);
    } else {
      return null;
    }
  }
  public static void changeBarcodeLabel(Code39Barcode barcode, String label) {
    try {
      Field field = barcode.getClass().getDeclaredField("label");
      field.setAccessible(true);
      field.set(barcode, label);
    } catch (NoSuchFieldException | IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }
}
