/**
 * Copyright (c) 2008-2012 Indivica Inc.
 * 
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "indivica.ca/gplv2"
 * and "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.eform.util;

import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.oscarehr.common.dao.EFormValueDao;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.linear.code39.Code39Barcode;
import oscar.OscarProperties;
import oscar.eform.data.EForm;

/**
 * The purpose of this servlet is to allow a local process to convert an eform html page into a pdf file.
 */
public final class EFormViewForPdfGenerationServlet extends HttpServlet {

	private static final Logger logger = MiscUtils.getLogger();

	@Override
	public final void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// ensure it's a local machine request... no one else should be calling this servlet.
		String remoteAddress = request.getRemoteAddr();
		logger.debug("EformPdfServlet request from : " + remoteAddress);
		if (!"127.0.0.1".equals(remoteAddress)) {
			logger.warn("Unauthorised request made to EFormViewForPdfGenerationServlet from address : " + remoteAddress);
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
		}

		boolean blankForm =  "true".equalsIgnoreCase(request.getParameter("blankForm"));
		boolean populateValues = "true".equalsIgnoreCase(request.getParameter("populateValues"));
		boolean prepareForFax = "true".equals(request.getParameter("prepareForFax")); 
		String id = request.getParameter("fdid");
		String providerId = request.getParameter("providerId");
		String extId = request.getParameter("ext_order_id") != null
				? "Dynacare order ID: " + request.getParameter("ext_order_id")
				: "";

		String barcodeId = request.getParameter("barcode");
		String barcodeHtml = "";
		if (barcodeId != null && !barcodeId.isEmpty()) {
			try {
				Code39Barcode barcode = (Code39Barcode) BarcodeFactory.createCode39(
						BarcodeUtil.addCheckDigit(barcodeId), false);
				BarcodeUtil.changeBarcodeLabel(barcode, barcodeId);
				barcode.setBarHeight(40);
				barcode.setBarWidth(2);
				barcode.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
				BufferedImage bufferedImage = BarcodeImageHandler.getImage(barcode);
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				ImageIO.write(bufferedImage, "png", output);
				String encodedBarcode = Base64.getEncoder().encodeToString(output.toByteArray());
				barcodeHtml = "<img src='data:image/png;base64," + encodedBarcode + "' />";
			} catch (BarcodeException e) {
				logger.error("Error generating barcode: " + e.getMessage(), e);
			}
		}
		EForm eForm = new EForm(id, blankForm);
		eForm.setSignatureCode(request.getContextPath(), request.getHeader("User-Agent"), eForm.getDemographicNo(), providerId);
		eForm.setContextPath(request.getContextPath());
		String projectHome = OscarProperties.getInstance().getProperty("project_home");

		boolean isDynacareEOrder = "true".equals(request.getParameter("M_IsDynacareEOrder"));
		EFormValueDao efvDao = (EFormValueDao) SpringUtils.getBean("EFormValueDao");
		List<EFormValue> eFormValues = efvDao.findByFormDataId(Integer.parseInt(id));
		for (EFormValue value : eFormValues) {
			if (value.getVarName().equals("Letter")) {
				String html = value.getVarValue();
				html = html.replace("/imageRenderingServlet", "/EFormSignatureViewForPdfGenerationServlet");
				if (prepareForFax) {
					html = "<div style=\"position:relative\"><div style=\"position:absolute; margin-top:35px;\">" + html + "</div></div>";
				}
				html = "<html><body style='width:640px;'>" + html + "</body></html>";
				eForm.setFormHtml(html);
			}
			if (value.getVarName().equals("signatureValue")) {
				
				// Checking to see if there are any parameters for the signature in the html.
				String html = eForm.getFormHtml();
				String signatureInit = "signatureControl.initialize\\s*\\(\\s*\\{\\s*eform:true,\\s+height:(\\d+),\\s+width:(\\d+),\\s+top:(\\d+),\\s+left:(\\d+)\\s*\\}\\s*\\)";
				Pattern pattern = Pattern.compile(signatureInit);
				Matcher matcher = pattern.matcher(html);
				boolean matchFound = matcher.find();
				String sign = value.getVarValue();
				sign = sign.replace("/imageRenderingServlet", "/EFormSignatureViewForPdfGenerationServlet");
				if (matchFound && matcher.groupCount() == 4) {
					String left = matcher.group(4), top = matcher.group(3), width = matcher.group(2), height = matcher.group(1);
					eForm.setFormHtml(html.replace("<div id=\"signatureDisplay\"></div>", String.format("<div id=\"signatureDisplay\"><img src=\"%s\" style=\"position:absolute;left:%s;top:%s;width:%s;height:%s;\" /> </div>", sign, left, top, width, height)));
				} else if (isDynacareEOrder) {
					eForm.setFormHtml(eForm.getFormHtml().replace("<div id=\"signatureDisplay\" style=\"display:none;\">", "<img style='position:absolute;left:30;top:900;width:150;height:20;' src='" + sign + "' alt='signature'>"));
				}
			}
		}

		eForm.setFormHtml(eForm.getFormHtml().replace("../eform/displayImage.do",  "/" + projectHome + "/EFormImageViewForPdfGenerationServlet"));
		eForm.setFormHtml(eForm.getFormHtml().replace("${oscar_image_path}", "/" + projectHome + "/EFormImageViewForPdfGenerationServlet?imagefile="));
		eForm.setFormHtml(eForm.getFormHtml().replace("$%7Boscar_image_path%7D", "/" + projectHome + "/EFormImageViewForPdfGenerationServlet?imagefile="));
		eForm.setFormHtml(eForm.getFormHtml().replace("<div class=\"DoNotPrint\" style=\"", "<div class=\"DoNotPrint\" style=\"display:none;"));
		eForm.setFormHtml(eForm.getFormHtml().replace("${barcode}", barcodeHtml));
		eForm.setFormHtml(eForm.getFormHtml().replace("${extOrderId}", extId));
		eForm.setFormHtml(eForm.getFormHtml().replaceAll("(&quot;)(?=[^<]*>)", "\""));
		eForm.setImagePath();
		eForm.setNowDateTime();
		if (populateValues) {
			eForm.setContextPath(request.getContextPath());
			eForm.setupInputFields();
			eForm.setOscarOPEN(request.getRequestURI());
			eForm.setImagePath();
		}
		response.setContentType("text/html");
		response.getOutputStream().write(eForm.getFormHtml().getBytes(Charset.forName("UTF-8")));		
	}
}
