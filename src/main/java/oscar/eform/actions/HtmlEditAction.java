/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.eform.actions;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.EFormConstants;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.util.WebUtils;

import oscar.eform.EFormUtil;
import oscar.eform.data.EFormBase;
import oscar.eform.data.HtmlEditForm;


public class HtmlEditAction extends Action {
	
	private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
	
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
    	
        HtmlEditForm fm = (HtmlEditForm) form;
       
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_eform", "w", null)) {
			throw new SecurityException("missing required security object (_eform)");
		}
        boolean dynamicContent = Boolean.parseBoolean(request.getParameter("dynamicContent"));
        MessageResources messages = MessageResources.getMessageResources("oscarResources");
        try {
            String fid = fm.getFid();
            String formName = fm.getFormName();
            String formSubject = fm.getFormSubject();
            String formFileName = fm.getFormFileName();
            
            //Matches if an input tag has the 'checked' attribute and adds 'data-prechecked="true"' just before it so that the checked status can be removed 
            // when the eform is submitted so that it doesn't default to being checked when the eform is opened after the first time. If the 'data-prechecked="true"' 
            // attribute is already there it won't add it again.
            fm.setFormHtml(fm.getFormHtml().replaceAll("(?i)((\\s+data-prechecked=\"true\")?(\\s+checked(\\s*=\\s*\"\\s*(true|checked)\\s*\")?(?<=<\\s{0,10000}input[^<]{0,10000})(?=[^<]{0,10000}/?>)))", " data-prechecked=\"true\" checked=\"checked\""));
            
            //Same as with the checked status above but for Xboxes. Note that it identifies Xboxes with the class attribute and only if that attribute precedes the value attribute.
            fm.setFormHtml(fm.getFormHtml().replaceAll("(?i)((\\s+data-prechecked=\"true\")?(\\s+value=\"X\")(?<=<\\s{0,10000}input[^<]{0,10000}class=\"Xbox\"[^<]{0,10000})(?=[^<]{0,10000}/?>))", " data-prechecked=\"true\" value=\"X\""));
            
            String formHtml = fm.getFormHtml();
            boolean showLatestFormOnly = WebUtils.isChecked(request, "showLatestFormOnly");
            boolean patientIndependent = WebUtils.isChecked(request, "patientIndependent");
            boolean isAttachmentsEnabled = WebUtils.isChecked(request, EFormConstants.IS_ATTACHMENTS_ENABLED);
            String roleType = fm.getRoleType();
            
            HashMap<String, String> errors = new HashMap<String, String>();

            // property container (bean)
            EFormBase updatedform = new EFormBase(fid, formName, formSubject, formFileName,
                formHtml, showLatestFormOnly, patientIndependent, isAttachmentsEnabled, roleType);

            // validation
            if ((formName == null) || (formName.length() == 0)) {
                errors.put("formNameMissing", messages.getMessage(request.getLocale(), "eform.errors.form_name.missing.regular"));
            }
            if ((fid.length() > 0) && (EFormUtil.formExistsInDBn(formName, fid) > 0)) {
                errors.put("formNameExists", messages.getMessage(request.getLocale(), "eform.errors.form_name.exists.regular"));
            }
            if ((fid.length() == 0) && (errors.size() == 0)) {
                fid = EFormUtil.saveEForm(formName, formSubject, formFileName, formHtml,
                    showLatestFormOnly, patientIndependent, isAttachmentsEnabled, roleType);
                request.setAttribute("success", "true");
            } else if (errors.size() == 0) {
                EFormUtil.updateEForm(updatedform);
                request.setAttribute("success", "true");
            }
            
            HashMap<String, Object> curht = createHashMap(fid, formName, formSubject, formFileName, formHtml, showLatestFormOnly, patientIndependent, roleType);
            request.setAttribute("submitted", curht);
            
            request.setAttribute("errors", errors);

            if (dynamicContent) {
                JSONObject json = new JSONObject();
                json.put("success", (errors.size() == 0));
                json.put("errors", errors);
                json.put("formId", fid);
                json.put("formName", formName);
                json.put("formFileName", formFileName);
                json.put("formDate", curht.get("formDate"));
                json.put("formTime", curht.get("formTime"));
                json.put("formSubject", formSubject);
                response.getWriter().print(json.toString());
            }
        } catch (Exception e) {
            MiscUtils.getLogger().error("Error", e);
        }

        if (dynamicContent) {
            return null;
        }
        return(mapping.findForward("success"));
    }
    
    private HashMap<String, Object> createHashMap(String fid, String formName, String formSubject, String formFileName, String formHtml, boolean showLatestFormOnly, boolean patientIndependent, String roleType) {
    	HashMap<String, Object> curht = new HashMap<String, Object>();
        curht.put("fid", fid);  
        curht.put("formName", formName);
        curht.put("formSubject", formSubject);
        curht.put("formFileName", formFileName);
        curht.put("formHtml", formHtml);
        curht.put("showLatestFormOnly", showLatestFormOnly);
        curht.put("patientIndependent", patientIndependent);
        curht.put("roleType", roleType);
        
        if (fid.length() == 0) {
            curht.put("formDate", "--");
            curht.put("formTime", "--");
        } else {
            curht.put("formDate", EFormUtil.getEFormParameter(fid, "formDate"));
            curht.put("formTime", EFormUtil.getEFormParameter(fid, "formTime"));
        }
        return curht;
    }
    
}
