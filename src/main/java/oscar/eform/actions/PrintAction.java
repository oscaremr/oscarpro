/**
 * Copyright (c) 2008-2012 Indivica Inc.
 *
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "indivica.ca/gplv2"
 * and "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.eform.actions;

import ca.kai.printable.Printable;
import ca.kai.printable.PrintableAttachmentType;
import ca.oscarpro.common.http.OscarProHttpService;
import ca.oscarpro.common.util.ResponseUtils;
import com.google.gson.Gson;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lowagie.text.DocumentException;

import lombok.val;
import lombok.var;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.bouncycastle.util.encoders.Base64;
import org.oscarehr.common.dao.DynacareEorderDao;
import org.oscarehr.common.dao.EFormDataDao;
import org.oscarehr.common.dao.EOrderDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EOrder;
import org.oscarehr.common.printing.HtmlToPdfServlet;
import org.oscarehr.common.dao.PatientLabRoutingDao;
import org.oscarehr.common.model.PatientLabRouting;
import org.oscarehr.hospitalReportManager.HRMPDFCreator;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentDao;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentToDemographicDao;
import org.oscarehr.hospitalReportManager.model.HRMDocument;
import org.oscarehr.hospitalReportManager.model.HRMDocumentToDemographic;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.util.WKHtmlToPdfUtils;

import oscar.OscarProperties;
import oscar.dms.EDoc;
import oscar.dms.EDocUtil;
import oscar.oscarLab.ca.all.pageUtil.CMLPdfCreator;
import oscar.oscarLab.ca.all.pageUtil.LabPDFCreator;
import oscar.oscarLab.ca.all.parsers.Factory;
import oscar.oscarLab.ca.all.parsers.MessageHandler;
import oscar.oscarLab.ca.on.CML.CMLLabTest;
import oscar.util.ConcatPDF;
import oscar.util.ConversionUtils;
import oscar.eform.EFormUtil;
import oscar.util.UtilDateUtilities;

import com.itextpdf.text.Document;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sun.xml.messaging.saaj.util.ByteOutputStream;

public class PrintAction extends Action {

	private static final Logger logger = MiscUtils.getLogger();

	private String localUri = null;
	
	private boolean skipSave = false;
	
	private HttpServletResponse response;
	private HttpServletRequest request;

	private final SecurityInfoManager securityInfoManager;
	private final SystemPreferencesDao systemPreferencesDao;
	private final EFormDataDao eFormDataDao;
  private final OscarProHttpService oscarProHttpService;

	private static final String ATTACHMENT_MANAGER_EFORM_ENABLED = "attachment_manager.eform.enabled";
  private static final String EFORM_PRINT_URL =
      "%s/api/v1/printables/print";

  private static final String RESPONSE_CONTENT_DISPOSITION = "attachment; filename=\"EForm-%s-%s.pdf\"";
	private static final String TEMP_FILE_DATE_FORMAT = "yyyy-mm-dd.hh.mm.ss";

  public PrintAction() {
    this(
        SpringUtils.getBean(SecurityInfoManager.class),
        SpringUtils.getBean(SystemPreferencesDao.class),
        SpringUtils.getBean(EFormDataDao.class),
        SpringUtils.getBean(OscarProHttpService.class));
  }

  public PrintAction(
      final SecurityInfoManager securityInfoManager,
      final SystemPreferencesDao systemPreferencesDao,
      final EFormDataDao eFormDataDao,
      final OscarProHttpService oscarProHttpService) {
    this.securityInfoManager = securityInfoManager;
    this.systemPreferencesDao = systemPreferencesDao;
    this.eFormDataDao = eFormDataDao;
    this.oscarProHttpService = oscarProHttpService;
  }

  public ActionForward execute(
      ActionMapping mapping,
      ActionForm form,
      HttpServletRequest request,
      HttpServletResponse response) {

    if (!securityInfoManager.hasPrivilege(
        LoggedInInfo.getLoggedInInfoFromSession(request), "_eform", "r", null)) {
      throw new SecurityException("missing required security object (_eform)");
    }

    boolean attachmentManagerEFormEnabled =
        systemPreferencesDao.isReadBooleanPreferenceWithDefault(
            ATTACHMENT_MANAGER_EFORM_ENABLED, false);

    boolean getGeneratedPDF = "true".equals(request.getParameter("getGeneratedPDF"));
    boolean isExcellerisEorder = "true".equals(request.getParameter("M_IsExcellerisEOrder"));
    boolean isDynacareEorder = "true".equals(request.getParameter("M_IsDynacareEOrder"));
    logger.info("isExcellerisEOrder : " + isExcellerisEorder);
    logger.info("isDynacareEOrder : " + isDynacareEorder);
    if (getGeneratedPDF) {
      String id = (String) request.getAttribute("fdid");
      if (id == null) {
        id = request.getParameter("fdid");
      }
      sendGeneratedPDF(response, id, isDynacareEorder);
      return mapping.findForward("close");
    }

    localUri = getEformRequestUrl(request);
    this.response = response;
    this.request = request;
    String id = (String) request.getAttribute("fdid");
    if (id == null) {
      id = request.getParameter("fdid");
    }
    String providerId = request.getParameter("providerId");
    skipSave = "true".equals(request.getParameter("skipSave"));
    try {
      if (attachmentManagerEFormEnabled) {
        printFormAttachmentManager(id, request);
      } else {
        printFormLegacy(id, providerId);
      }
    } catch (Exception e) {
      MiscUtils.getLogger().error("", e);
      return mapping.findForward("error");
    }

    if (skipSave) {
      // Removing the eform
      val eFormData = eFormDataDao.find(Integer.parseInt(id));
      eFormData.setCurrent(false);
      eFormDataDao.merge(eFormData);
      return mapping.findForward("success");
    } else {
      return mapping.findForward("close");
    }
  }

	/**
	 * This method is a copy of Apache Tomcat's ApplicationHttpRequest getRequestURL method with the exception that the uri is removed and replaced with our eform viewing uri. Note that this requires that the remote url is valid for local access. i.e. the
	 * host name from outside needs to resolve inside as well. The result needs to look something like this : https://127.0.0.1:8443/oscar/eformViewForPdfGenerationServlet?fdid=2&parentAjaxId=eforms
	 */
    public static String getEformRequestUrl(HttpServletRequest request) {
		StringBuilder url = new StringBuilder();
		String scheme = "http";
		Integer port = 80;
		if (request.getServerPort() != 80) {
			port = 8080;
		}

		var providerNumber = StringUtils.isNotEmpty(request.getParameter("providerId"))
				? request.getParameter("providerId")
				: request.getParameter("providerNumber");

		url.append(scheme);
		url.append("://");
		//url.append(request.getServerName());
		url.append("127.0.0.1");
		
		url.append(':');
		url.append(port);
		url.append(request.getContextPath());
		url.append("/EFormViewForPdfGenerationServlet?parentAjaxId=eforms&providerId=");
		url.append(StringUtils.trimToEmpty(providerNumber));
		url.append("&fdid=");

		return (url.toString());
	}
    
    /**
     * Sends generated pdf in response. Sends empty string if pdf doesn't exist or
     * an error occurs.
     * 
     * @param response the HttpServletResponse object
     * @param formId id of the eorder
     */
    public void sendGeneratedPDF(HttpServletResponse response, String formId, boolean isDynacareEorder) {
    	try {

				EOrder eOrder = null;
				if (isDynacareEorder) {
					DynacareEorderDao dynacareEOrderDao = SpringUtils.getBean(DynacareEorderDao.class);
					eOrder = dynacareEOrderDao.findByExtEFormDataId(Integer.parseInt(formId));
				} else {
					EOrderDao eOrderDao = SpringUtils.getBean(EOrderDao.class);
					eOrder = eOrderDao.findByExtEFormDataId(Integer.parseInt(formId));
				}

			String generatedPDF = "";
			if (eOrder != null) {
				if (!StringUtils.isEmpty(eOrder.getGeneratedPDF())) {
					generatedPDF = eOrder.getGeneratedPDF();
				}
			}
			
	    	response.setContentType("text/plain");  
	    	response.setContentLength(generatedPDF.length());
	    	response.setCharacterEncoding("UTF-8");
	    	
	    	OutputStream ros = null;
			try {
				ros = response.getOutputStream();
				ros.write(generatedPDF.getBytes());
				ros.flush();
			} finally {
				if (ros != null) {
					IOUtils.closeQuietly(ros);
				}
			}
    	
    	} catch (Exception e) {
			MiscUtils.getLogger().error("", e);
		}
    }

	public void printFormAttachmentManager(
			final String formId,
			final HttpServletRequest request)
			throws IOException {
    val requestUrl = String.format(EFORM_PRINT_URL, OscarProperties.getOscarProBaseUrl());
    val requestData =
        new Gson().toJson(new Printable(formId, PrintableAttachmentType.Eforms), Printable.class);

    val pdfPrintResponse =
        oscarProHttpService.makeLoggedInPostRequestToPro(requestUrl, requestData, request);
    if (pdfPrintResponse != null) {
      response.setContentType("application/pdf");
      response.setHeader(
          "Content-Disposition",
					String.format(RESPONSE_CONTENT_DISPOSITION,
							formId,
							UtilDateUtilities.getToday(TEMP_FILE_DATE_FORMAT)));
      ResponseUtils.writeFileDataToResponseOutputStream(response,
					ResponseUtils.readStringDataFromResponseEntityContent(pdfPrintResponse));
		}
	}

	/**
	 * This method will take eforms and send them to a PHR.
	 */
	public void printFormLegacy(String formId, String providerId) {
		boolean addDocumentToEchart = Boolean.parseBoolean(request.getParameter("printDocumentToEchartOnPdfFax"));
		File tempFile = null;

		try {
			logger.info("Generating PDF for eform with fdid = " + formId);
			
			boolean isExcellerisEorder = "true".equals(request.getParameter("M_IsExcellerisEOrder"));
			boolean isDynacareEorder = "true".equals(request.getParameter("M_IsDynacareEOrder"));
			logger.info("isExcellerisEorder : " + isExcellerisEorder);
			logger.info("isDynacareEorder : " + isDynacareEorder);
			
			if (isExcellerisEorder || isDynacareEorder) {
				tempFile = createTemporaryPdfFileForEOrder(formId, isDynacareEorder);
				if (tempFile == null) {
					tempFile = createTemporaryPdfFileForEform(formId);
				}
			} else {
				tempFile = createTemporaryPdfFileForEform(formId);
			}

			EFormData eFormData = eFormDataDao.find(Integer.parseInt(formId));
			boolean isRichTextLetter = eFormData.getFormId().toString().equals(OscarProperties.getInstance().getProperty("rtl_template_id", ""));
			
			if (addDocumentToEchart) {
				LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
				// Saves the eform to the patient's chart
				EDocUtil.saveDocumentToPatient(loggedInInfo, loggedInInfo.getLoggedInProvider(), eFormData.getDemographicId(), tempFile, -1, "", eFormData.getFormName());
			}
			
			InputStream is = new BufferedInputStream(new FileInputStream(tempFile));
			ByteOutputStream bos = new ByteOutputStream();
			byte buffer[] = new byte[1024];
			int read;
			while (is.available() != 0) {
				read = is.read(buffer,0,1024);
				bos.write(buffer,0, read);
			}
			
			bos.flush();
			// byte[] pdf = HtmlToPdfServlet.appendFooter(bos.getBytes());
			byte[] pdf;
            try {
            	if (isRichTextLetter) {
            		pdf = bos.getBytes();
				} else {
					pdf = HtmlToPdfServlet.stamp(bos.getBytes());
				}
            } catch (Exception e) {
            	throw new RuntimeException(e);
            }
			
			//while (fos.read() != -1)
			response.setContentType("application/pdf");  //octet-stream
      response.setHeader(
          "Content-Disposition",
          String.format(
              RESPONSE_CONTENT_DISPOSITION,
              formId,
              UtilDateUtilities.getToday(TEMP_FILE_DATE_FORMAT)));
      combinePDFs(
					request,
					LoggedInInfo.getLoggedInInfoFromSession(request),
					tempFile.getCanonicalPath(),
					"EForm-" + formId + "-" + UtilDateUtilities.getToday(TEMP_FILE_DATE_FORMAT) + ".pdf");

			// Removing the consulation pdf.
			tempFile.delete();
		} catch (IOException | RuntimeException | DocumentException e) {
			MiscUtils.getLogger().error("", e);
		}
	}
	private void combinePDFs(HttpServletRequest request, LoggedInInfo loggedInInfo, String getFileName, String currentFileName) throws IOException{
        String reqId= (String)request.getAttribute("fdid");
		String demoNo=request.getParameter("efmdemographic_no");
		String pacsImageList = request.getParameter("pacsImageList");
		String efname  = (String)request.getAttribute("efname");
        ArrayList<EDoc> eformdocs = EDocUtil.listEformDocs(loggedInInfo, demoNo, reqId, EDocUtil.ATTACHED);
        ArrayList<Object> pdfDocs = new ArrayList<Object>();
        File file2 = null;
        pdfDocs.add(getFileName);
        for (int i=0; i < eformdocs.size(); i++){
            EDoc curDoc =  eformdocs.get(i);
            if ( curDoc.isPDF() )
            	try {
                	pdfDocs.add(curDoc.getFilePath());
                } catch(Exception ex) {	logger.error("File not found: "+curDoc.getFileName()); }
        }
        PatientLabRoutingDao dao = SpringUtils.getBean(PatientLabRoutingDao.class);
        try {
            for(Object[] i : dao.findRoutingsAndEformDocsByEfromId(ConversionUtils.fromIntString(reqId), "L")) {
            	PatientLabRouting p = (PatientLabRouting) i[0];
            	if(p.getLabType().equals("HL7")){
	                String segmentId = "" + p.getLabNo();
	                request.setAttribute("segmentID", segmentId);
	                MessageHandler handler = Factory.getHandler(segmentId);
	                String fileName = OscarProperties.getInstance().getProperty("DOCUMENT_DIR")+"//"+handler.getPatientName().replaceAll("\\s", "_")+"_"+handler.getMsgDate()+"_LabReport.pdf";
	                OutputStream os = new FileOutputStream(fileName);
	                LabPDFCreator pdf = new LabPDFCreator(request, os);
	                pdf.printPdf();
	                pdfDocs.add(fileName);
            	}else if(p.getLabType().equals("CML")){
            		String segmentId = "" + p.getLabNo();
	                request.setAttribute("segmentID", segmentId);
	                CMLLabTest lab = new CMLLabTest();
	                lab.populateLab(segmentId);
	                String fileName = OscarProperties.getInstance().getProperty("DOCUMENT_DIR")+"//"+(lab.pLastName + " " + lab.pFirstName).replaceAll("\\s", "_")+"_"+ lab.printDate +"_LabReport.pdf";
	                file2 = new File(fileName);
	                OutputStream os = new FileOutputStream(file2);
	                CMLPdfCreator cml = new CMLPdfCreator(request, os);
	                cml.printPdf();
	                pdfDocs.add(fileName);
            	}
            }
            HRMDocumentDao hrmDao = (HRMDocumentDao)SpringUtils.getBean("HRMDocumentDao");
 			HRMDocumentToDemographicDao hrmToDemoDao = (HRMDocumentToDemographicDao)SpringUtils.getBean("HRMDocumentToDemographicDao");
 			List<HRMDocumentToDemographic> hList = hrmToDemoDao.findByDemographicNoAndEformID(demoNo, reqId, true);
            for (HRMDocumentToDemographic htd : hList) {
                HRMDocument document = hrmDao.findById(htd.getHrmDocumentId()).get(0);
                OutputStream os;
                String fileName = document.getReportFile() +"_LabReport.pdf";
                try {
                	os = new FileOutputStream(fileName); //For THT: reportFile has full folders
                } catch (Exception e) {
                	fileName = OscarProperties.getInstance().getProperty("DOCUMENT_DIR")+fileName;  //Oscar pro: reportFile only has file name without folder
                	os = new FileOutputStream(fileName);
                }
                HRMPDFCreator hrmpdfc = new HRMPDFCreator(os, htd.getHrmDocumentId(), loggedInInfo);
                hrmpdfc.printPdf();
                pdfDocs.add(fileName);
            }
            int demono=Integer.parseInt(demoNo);
         	List<EFormData> eList = eFormDataDao.getSavedEfromDataByDemoId(demono,ConversionUtils.fromIntString(reqId));
         	for (EFormData eFormData : eList) {
         		if(eFormData.getFormData().indexOf("id=\"root\"") > -1){
         			continue;
         		}
         		String formId=eFormData.getId().toString();
         		    File tempFile = null;
        			logger.info("Generating PDF for eform with fdid = " + formId);
        			tempFile = File.createTempFile("EFormPrint." + formId, ".pdf");
        		    String	localUri = getEformRequestUrl(request);
        			String viewUri = localUri + formId;
        			WKHtmlToPdfUtils.convertToPdf(viewUri, tempFile);
        			logger.info("Writing pdf to : "+tempFile.getCanonicalPath());
        			pdfDocs.add(tempFile.getAbsolutePath());
			}
        }catch(DocumentException de) {
            request.setAttribute("printError", new Boolean(true));
        }catch(IOException ioe) {
            request.setAttribute("printError", new Boolean(true));
        }catch(Exception e){
            request.setAttribute("printError", new Boolean(true));
        }
        File imagePdf = null;
        if(pacsImageList != null && pacsImageList.length() > 0){
        	Document document = new Document();
        	PdfPTable table = new PdfPTable(1);
        	imagePdf = File.createTempFile(
							"pacs-" + UtilDateUtilities.getToday(TEMP_FILE_DATE_FORMAT), ".pdf");
        	String home_dir = OscarProperties.getInstance().getProperty("pacs_image_folder");
        	try {
				PdfWriter.getInstance(document,new FileOutputStream(imagePdf));
				document.open();
	        	String[] imageList = pacsImageList.split(",");
	        	for(int i = 0;i < imageList.length;i ++){
	        		if(imageList[i].length() > 0){
	        			String imageName = "pacs-" + imageList[i] + ".png";
	        			File imageFile = new File(home_dir, imageName);
	        			InputStream in = new FileInputStream(imageFile);
	        			byte[] imageByte = null;
						try {
							imageByte = readInputStream(in);
						} catch (Exception e) {
							MiscUtils.getLogger().error("",e);
						}
	        			Image image = Image.getInstance(imageByte);
	        			table.addCell(image);
	        		}
	        	}
	        	document.add(table);
	        	document.close();
			} catch (com.itextpdf.text.DocumentException e) {
				MiscUtils.getLogger().error("",e);
			}
        	pdfDocs.add(imagePdf.getAbsolutePath());
        }
        if (efname!=null) {
        	String[] allname = efname.split(",");
        	for (String filename : allname) {
        		if (filename==null||filename.trim().length()==0) {
					continue;
				}
        		String folder=System.getProperty("java.io.tmpdir");
            	pdfDocs.add(folder+File.separator+filename);
			}
		}
        response.setContentType("application/pdf");  //octet-stream
        try {
        	response.setHeader("Content-Disposition", "attachment; filename=\"" + currentFileName + "\"");
        } catch (Exception e) {
        	response.setHeader("Content-Disposition", "attachment; filename=\"" + getFileName + "\""); //for eform print&pdf 
        }
        ConcatPDF.concat(pdfDocs,response.getOutputStream());
        if(null != imagePdf){
        	imagePdf.delete();
        }
        if(null != file2){
        	file2.delete();
        }
        if (efname!=null) {
          	String[] allname = efname.split(",");
          	for (String filename : allname) {
          		if (filename==null||filename.trim().length()==0) {
  					continue;
  				}
          		String folder=System.getProperty("java.io.tmpdir");
          		String path = folder+File.separator+filename;
          		File file = new File(path);
          		if (file.exists()) {
  					file.delete();
  				}
  			}
  		} 
    }
    public static byte[] readInputStream(InputStream inStream) throws Exception{  
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();  
        byte[] buffer = new byte[1024];  
        int len = 0;  
        while( (len=inStream.read(buffer)) != -1 ){  
            outStream.write(buffer, 0, len);  
        }  
        inStream.close();  
        return outStream.toByteArray();  
    }

	// ------------------------------------------------------------------------ Private Methods
	
	/**
	 * Create temporary pdf file for eForm
	 * 
	 * @throws IOException 
	 * @throws RuntimeException 
	 * @throws DocumentException 
	 */
	private File createTemporaryPdfFileForEform(String formId) throws IOException, DocumentException, RuntimeException {
		
		File tempFile = null;
		EFormData eFormData = eFormDataDao.find(Integer.parseInt(formId));

		// convert to PDF
		String viewUri = localUri + formId;
		boolean isRichTextLetter = eFormData.getFormId().toString().equals(OscarProperties.getInstance().getProperty("rtl_template_id", ""));
		if (isRichTextLetter) {
			tempFile = File.createTempFile( "templatedRtl."  + formId + "-", ".pdf");
			EFormUtil.printRtlWithTemplate(eFormData, tempFile, skipSave, request);
		} else {
			tempFile = File.createTempFile("EFormPrint." + formId, ".pdf");
			WKHtmlToPdfUtils.convertToPdf(viewUri, tempFile);
		}
		logger.info("Writing pdf to : "+tempFile.getCanonicalPath());
		
		return tempFile;
	}
	
	/**
	 * Create temporary pdf file for eOrder
	 * @throws IOException 
	 */
	private File createTemporaryPdfFileForEOrder(String formId, boolean isDynacareEorder) throws IOException {
		File tempFile = null;
		EOrder eOrder = null;
		if (isDynacareEorder) {
			DynacareEorderDao dynacareEOrderDao = SpringUtils.getBean(DynacareEorderDao.class);
			eOrder = dynacareEOrderDao.findByExtEFormDataId(Integer.parseInt(formId));
		} else {
			EOrderDao eOrderDao = SpringUtils.getBean(EOrderDao.class);
			eOrder = eOrderDao.findByExtEFormDataId(Integer.parseInt(formId));
		}
		if (eOrder != null) {
			if (!StringUtils.isEmpty(eOrder.getGeneratedPDF())) {
				tempFile = File.createTempFile("EFormPrint." + formId, ".pdf");
				FileUtils.writeByteArrayToFile(tempFile, Base64.decode(eOrder.getGeneratedPDF()));
			}
		}
		
		return tempFile;
	}
}
