/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * <p>
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.eform.actions;

import static org.oscarehr.integration.dynacare.eorder.DynacareOrderStatusHelper.convertStatusToString;
import static org.oscarehr.integration.dynacare.eorder.DynacareOrderStatusHelper.getDescription;

import com.dynacare.api.Order;
import com.dynacare.api.OrderStatusEnum;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Base64;
import javax.imageio.ImageIO;
import lombok.val;
import lombok.extern.slf4j.Slf4j;
import org.apache.struts.Globals;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.dao.DynacareEorderDao;
import org.oscarehr.common.dao.EFormDataDao;
import org.oscarehr.common.dao.EFormValueDao;
import org.oscarehr.common.dao.EOrderDao;
import org.oscarehr.common.dao.OrderLabTestCodeCacheDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.DynacareEorder;
import org.oscarehr.common.model.ExcellerisEorder;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.EOrder.OrderState;
import org.oscarehr.common.model.SMTPConfig;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.integration.dynacare.eorder.ConfigureConstants;
import org.oscarehr.integration.dynacare.eorder.DynacareOrderPreferences;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.AuthenticationTokenAndResource;
import org.oscarehr.integration.excelleris.eorder.OrderGateway;
import org.oscarehr.integration.excelleris.eorder.OrderGatewayImpl;
import org.oscarehr.integration.excelleris.eorder.OrderResponseHelper;
import org.oscarehr.integration.excelleris.eorder.api.Questionnaire;
import org.oscarehr.integration.excelleris.eorder.converter.QuestionnaireConverter;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialBusinessLogic;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialFactory;
import org.oscarehr.managers.DemographicManager;
import org.oscarehr.managers.EmailManager;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.match.IMatchManager;
import org.oscarehr.match.MatchManager;
import org.oscarehr.match.MatchManagerException;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.util.WKHtmlToPdfUtils;
import ca.uhn.fhir.rest.server.exceptions.BaseServerResponseException;
import org.hl7.fhir.r4.formats.JsonParser;
import net.sourceforge.barbecue.BarcodeFactory;
import net.sourceforge.barbecue.BarcodeImageHandler;
import net.sourceforge.barbecue.linear.code39.Code39Barcode;
import oscar.dms.EDocUtil;
import oscar.eform.EFormLoader;
import oscar.eform.EFormUtil;
import oscar.eform.data.DatabaseAP;
import oscar.eform.data.EForm;
import oscar.eform.data.EformAttachEForm;
import oscar.eform.data.EformAttachHrms;
import oscar.eform.data.EformAttachLabs;
import oscar.eform.util.BarcodeUtil;
import oscar.oscarEncounter.data.EctProgram;
import oscar.util.StringUtils;

@Slf4j
public class AddEFormAction extends Action {

  private static final String EXCELLERIS_EORDER_AUTHENTICATION_TOKEN = "EXCELLERIS_EORDER_AUTHENTICATION_TOKEN";
  private static final String EXCELLERIS_PATIENT_ID = "Excelleris Patient Id";
  private static final String SUCCESS_MESSAGES = "Success messages";
  private static final String INFORMATIONAL_MESSAGES = "Informational messages";
  private static final String ERROR_MESSAGES = "Error messages";

  private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
  private EmailManager emailManager = SpringUtils.getBean(EmailManager.class);

  public ActionForward execute(ActionMapping mapping, ActionForm form,
                               HttpServletRequest request, HttpServletResponse response) {

    if (!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_eform", "w", null)) {
      throw new SecurityException("missing required security object (_eform)");
    }

    log.debug("==================SAVING ==============");
    HttpSession se = request.getSession();

    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    String providerNo = loggedInInfo.getLoggedInProviderNo();

    boolean fax = "true".equals(request.getParameter("faxEForm"));
    boolean print = "true".equals(request.getParameter("print"));
    boolean isExcellerisEOrder = "true".equals(request.getParameter("M_IsExcellerisEOrder"));
    boolean isDynacareEOrder = "true".equals(request.getParameter("M_IsDynacareEOrder"));

    log.debug("isExcellerisEOrder : " + isExcellerisEOrder);
    log.debug("isDynacareEOrder : " + isDynacareEOrder);

    @SuppressWarnings("unchecked")
    Enumeration<String> paramNamesE = request.getParameterNames();
    ArrayList<String> paramNames = new ArrayList<String>();  //holds "fieldname, ...."
    ArrayList<String> paramValues = new ArrayList<String>(); //holds "myval, ...."
    String fid = request.getParameter("efmfid");
    String demographic_no = request.getParameter("efmdemographic_no");
    String eform_link = request.getParameter("eform_link");
    String subject = request.getParameter("subject");
    boolean addDocumentToEchart = Boolean.parseBoolean(request.getParameter("printDocumentToEchart"));

    boolean doDatabaseUpdate = false;

    List<String> oscarUpdateFields = new ArrayList<String>();

    if (request.getParameter("_oscardodatabaseupdate") != null && request.getParameter("_oscardodatabaseupdate").equalsIgnoreCase("on")) {
      doDatabaseUpdate = true;
    }

    ActionMessages updateErrors = new ActionMessages();

    // The fields in the _oscarupdatefields parameter are separated by %s.
    if (!print && !fax && doDatabaseUpdate && request.getParameter("_oscarupdatefields") != null) {

      oscarUpdateFields = Arrays.asList(request.getParameter("_oscarupdatefields").split("%"));

      boolean validationError = false;

      for (String field : oscarUpdateFields) {
        EFormLoader.getInstance();
        // Check for existence of appropriate databaseap
        DatabaseAP currentAP = EFormLoader.getAP(field);
        if (currentAP != null) {
          if (!currentAP.isInputField()) {
            // Abort! This field can't be updated
            updateErrors.add(field, new ActionMessage("errors.richeForms.noInputMethodError", field));
            validationError = true;
          }
        } else {
          // Field doesn't exit
          updateErrors.add(field, new ActionMessage("errors.richeForms.noSuchFieldError", field));
          validationError = true;
        }
      }

      if (!validationError) {
        for (String field : oscarUpdateFields) {
          EFormLoader.getInstance();
          DatabaseAP currentAP = EFormLoader.getAP(field);
          // We can add more of these later...
          if (currentAP != null) {
            String inSQL = currentAP.getApInSQL();

            inSQL = DatabaseAP.parserReplace("demographic", demographic_no, inSQL);
            inSQL = DatabaseAP.parserReplace("provider", providerNo, inSQL);
            inSQL = DatabaseAP.parserReplace("fid", fid, inSQL);

            inSQL = DatabaseAP.parserReplace("value", request.getParameter(field), inSQL);

            //if(currentAP.getArchive() != null && currentAP.getArchive().equals("demographic")) {
            //	demographicArchiveDao.archiveRecord(demographicManager.getDemographic(loggedInInfo,demographic_no));
            //}

            // Run the SQL query against the database
            //TODO: do this a different way.
            MiscUtils.getLogger().error("Error", new Exception("EForm is using disabled functionality for updating fields..update not performed"));
          }
        }
      }
    }
    EForm curForm = new EForm(fid, demographic_no, providerNo);

    if (subject == null) {
      subject = "";
    }
    String curField = "";
    // if form contains default values and value is blanked out allow it to save as blank
    HashMap<String, String> defaultValueFields = curForm.getFieldsWithDefaultValue();
    while (paramNamesE.hasMoreElements()) {
      curField = paramNamesE.nextElement();
      if (curField.equalsIgnoreCase("parentAjaxId")) {
        continue;
      }
      String parameterValue = request.getParameter(curField);
      if (!org.apache.commons.lang.StringUtils.isBlank(parameterValue) ||
          (!org.apache.commons.lang.StringUtils.isBlank(defaultValueFields.get(curField)) &&
          org.apache.commons.lang.StringUtils.isBlank(parameterValue))) {
        if (curField.equals("faxRecipients") || curField.equals("faxRecipientsName")) {
          // faxRecipients may have multiple values so save them all
          String[] values = request.getParameterValues(curField);
          for (String value : values) {
            paramNames.add(curField);
            paramValues.add(value);
          }
        } else {
          if (curField.equals("attachments")) {
            paramNames.add("attachment-control-printables");
          } else {
            paramNames.add(curField);
          }
          paramValues.add(request.getParameter(curField));
        }
      }
    }


    //add eform_link value from session attribute
    ArrayList<String> openerNames = curForm.getOpenerNames();
    ArrayList<String> openerValues = new ArrayList<String>();
    for (String name : openerNames) {
      String lnk = providerNo + "_" + demographic_no + "_" + fid + "_" + name;
      String val = (String) se.getAttribute(lnk);
      openerValues.add(val);
      if (val != null) {
        se.removeAttribute(lnk);
      }
    }

    //----names parsed
    ActionMessages errors = curForm.setMeasurements(paramNames, paramValues);
    curForm.setFormSubject(subject);
    curForm.setValues(paramNames, paramValues);
    if (!openerNames.isEmpty()) {
      curForm.setOpenerValues(openerNames, openerValues);
    }
    if (eform_link != null) {
      curForm.setEformLink(eform_link);
    }
    curForm.setImagePath();
    curForm.setAction();
    curForm.setNowDateTime();
    if (!errors.isEmpty()) {
      saveErrors(request, errors);
      request.setAttribute("curform", curForm);
      request.setAttribute("page_errors", "true");
      return mapping.getInputForward();
    }

    //Check if eform same as previous, if same -> not saved
    String prev_fdid = (String) se.getAttribute("eform_data_id");

    se.removeAttribute("eform_data_id");
    boolean sameform = false;

    if (isExcellerisEOrder || isDynacareEOrder) {
      if (StringUtils.isNumeric(request.getParameter("M_existingFdid"))) {
        prev_fdid = request.getParameter("M_existingFdid");
        sameform = true;
      }
    } else if (StringUtils.filled(prev_fdid)) {
      EForm prevForm = new EForm(prev_fdid);
      if (prevForm != null) {
        sameform = curForm.getFormHtml().equals(prevForm.getFormHtml());
      }

    }

    log.info("sameform : " + sameform);

    String fdid = null;
    if (!sameform) { //save eform data
      EFormDataDao eFormDataDao = (EFormDataDao) SpringUtils.getBean("EFormDataDao");
      EFormData eFormData = toEFormData(curForm);

      String appointmentNo = request.getParameter("appointment_no");
      // Checks if the appointment number parameter either doesn't exist or is passed as a string of null
      if (appointmentNo != null && !appointmentNo.equals("null")) {
        try {
          // Sets the appointment number to the eform data after parsing it
          eFormData.setAppointmentNo(Integer.parseInt(appointmentNo));
        } catch (NumberFormatException e) {
          log.error("Could not convert appointment number to an integer. Appointment number: " + appointmentNo);
        }
      }

      eFormDataDao.persist(eFormData);


      fdid = eFormData.getId().toString();

      EFormUtil.addEFormValues(paramNames, paramValues, new Integer(fdid), new Integer(fid), new Integer(demographic_no)); //adds parsed values

      if (request.getParameter("documents") != null && request.getParameter("documents").length() > 0) {
        String[] docs = request.getParameter("documents").split("\\|");
        for (int idx = 0; idx < docs.length; ++idx) {
          if (docs[idx].length() > 0) {
            if (docs[idx].charAt(0) == 'D') {
              EDocUtil.attachDocEform(providerNo, docs[idx].substring(1), fdid);
            } else if (docs[idx].charAt(0) == 'L') {
              EformAttachLabs.attachLabConsult(providerNo, docs[idx].substring(1), fdid);
            } else if (docs[idx].charAt(0) == 'H') {
              EformAttachHrms.attachHrmConsult(providerNo, docs[idx].substring(1), fdid);
            } else if (docs[idx].charAt(0) == 'E') {
              EformAttachEForm.attachEformConsult(providerNo, docs[idx].substring(1), fdid);
            } else if (docs[idx].charAt(0) == 'C') {
              EformAttachLabs.attachLabConsult(providerNo, docs[idx].substring(1), fdid);
            }
          }
        }
      }
      //post fdid to {eform_link} attribute
      if (eform_link != null) {
        se.setAttribute(eform_link, fdid);
      }

      if (addDocumentToEchart) {
        String eformPrintUrl = PrintAction.getEformRequestUrl(request) + fdid;
        EFormUtil.printEformToEchart(loggedInInfo, eFormData, eformPrintUrl);
      }

      SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
      SystemPreferences systemPreferences = systemPreferencesDao.findPreferenceByName("patient_intake_eform");
      if (systemPreferences != null && systemPreferences.getValue() != null && systemPreferences.getValue().equals(fid)) {
        EFormUtil.createPatientIntakeLetter(eFormData);
      }

      // Handle Excelleris eOrder
      if (isExcellerisEOrder) {
        ActionForward actionForward = handleExcellerisEOrder(
            mapping, form, request, response, curForm, fdid, providerNo, demographic_no, eFormData);
        if (actionForward != null) {
          return actionForward;
        }
      }
      // Handle Dynacare eOrder
      if (isDynacareEOrder) {
        ActionForward actionForward = handleDynacareEOrder(mapping, form, request, response, curForm, fdid, providerNo,
                demographic_no, eFormData);
        if (actionForward != null) {
          return actionForward;
        }
      }

      if (fax) {
        request.setAttribute("fdid", fdid);
        return (mapping.findForward("fax"));
      } else if (print) {
        request.setAttribute("fdid", fdid);
        return (mapping.findForward("print"));
      } else {
        //write template message to echart
        String program_no = new EctProgram(se).getProgram(providerNo);
        String path = request.getRequestURL().toString();
        String uri = request.getRequestURI();
        path = path.substring(0, path.indexOf(uri));
        path += request.getContextPath();

        EFormUtil.writeEformTemplate(LoggedInInfo.getLoggedInInfoFromSession(request), paramNames, paramValues, curForm, fdid, program_no, path);
      }

    } else {

      if (isExcellerisEOrder && !StringUtils.isNullOrEmpty(request.getParameter("M_existingFdid"))) {
        EFormDataDao eFormDataDao = (EFormDataDao) SpringUtils.getBean("EFormDataDao");
        Integer existingFdid = Integer.parseInt(request.getParameter("M_existingFdid"));
        EFormData existingEformData = eFormDataDao.findByFormDataId(existingFdid);

        EFormData savedEformData = updateEFormData(curForm, existingEformData, eFormDataDao, existingFdid, fid,
            demographic_no, paramNames, paramValues);
        request.setAttribute("fdid", savedEformData.getId().toString());

        ActionForward actionForward = handleExcellerisEOrder(mapping, form, request, response, curForm,
            request.getParameter("M_existingFdid"), providerNo, demographic_no, savedEformData);
        if (actionForward != null) {
          return actionForward;
        }
      }

      if (isDynacareEOrder && StringUtils.isNumeric(request.getParameter("M_existingFdid"))) {
        EFormDataDao eFormDataDao = (EFormDataDao) SpringUtils.getBean("EFormDataDao");
        Integer existingFdid = Integer.parseInt(request.getParameter("M_existingFdid"));
        EFormData existingEformData = eFormDataDao.findByFormDataId(existingFdid);

        EFormData savedEformData = updateEFormData(curForm, existingEformData, eFormDataDao, existingFdid, fid,
                demographic_no, paramNames, paramValues);
        request.setAttribute("fdid", savedEformData.getId().toString());

        ActionForward actionForward = handleDynacareEOrder(mapping, form, request, response, curForm,
                request.getParameter("M_existingFdid"), providerNo, demographic_no, savedEformData);
        if (actionForward != null) {
          return actionForward;
        }
      }

      log.debug("Warning! Form HTML exactly the same, new form data not saved.");
      if (fax) {
        request.setAttribute("fdid", prev_fdid);
        return (mapping.findForward("fax"));
      } else if (print) {
        log.debug("Print.....");
        request.setAttribute("fdid", prev_fdid);
        return (mapping.findForward("print"));
      }
    }

    if (demographic_no != null) {
      IMatchManager matchManager = new MatchManager();
      DemographicManager demographicManager = SpringUtils.getBean(DemographicManager.class);
      Demographic client = demographicManager.getDemographic(loggedInInfo, demographic_no);
      try {
        matchManager.<Demographic>processEvent(client, IMatchManager.Event.CLIENT_CREATED);
      } catch (MatchManagerException e) {
        MiscUtils.getLogger().error("Error while processing MatchManager.processEvent(Client)", e);
      }
    }

    if (isExcellerisEOrder) {
      request.setAttribute("curform", curForm);
      request.setAttribute("page_messages", "true");
      request.setAttribute("fdid", (fdid == null) ? prev_fdid : fdid);

      String str = Globals.MESSAGE_KEY;
      return mapping.getInputForward();
    }

    if (isDynacareEOrder) {
      ActionMessages messages = getMessages(request);
      if (messages == null) {
        messages = new ActionMessages();
      }
      ActionMessage message = new ActionMessage("eform.errors.submit_eorder.success");
      messages.add(ActionMessages.GLOBAL_MESSAGE, message);
      saveMessages(request, messages);
      request.setAttribute("curform", curForm);
      request.setAttribute("page_messages", "true");
      request.setAttribute("fdid", (fdid == null) ? prev_fdid : fdid);

      String str = Globals.MESSAGE_KEY;
      return mapping.getInputForward();
    }

    return (mapping.findForward("close"));
  }

  private ActionForward handleDynacareEOrder(ActionMapping mapping, ActionForm form,
      HttpServletRequest request, HttpServletResponse response, EForm curForm, String fdid,
      String providerNo, String demographic_no, EFormData eFormData) {
    org.oscarehr.integration.dynacare.eorder.DynacareOrderGateway orderGateway = SpringUtils
            .getBean(org.oscarehr.integration.dynacare.eorder.DynacareOrderGatewayImpl.class);
    DynacareEorderDao eOrderDao = SpringUtils.getBean(DynacareEorderDao.class);
    String existingFdid = request.getParameter("M_existingFdid");
    DynacareEorder eOrder;
    Integer fdidInteger;
    if (StringUtils.isNumeric(existingFdid)) {
      eOrder =  eOrderDao.findByExtEFormDataId(Integer.parseInt(existingFdid));
      fdidInteger = new Integer(existingFdid);

    } else {
      eOrder = new DynacareEorder();
      fdidInteger = new Integer(fdid);
      eOrder.setEFormDataId(new Integer(fdidInteger));
      eOrder.setState(OrderState.NEW);

      // Save order so that the id gets generated
      eOrder = eOrderDao.saveEntity(eOrder);
    }
    DynacareEorder savedEorder;
    Order dynacareOrder;
    boolean processed = false;
    boolean updated = false;
    try {
      EFormValueDao eformValueDao = (EFormValueDao) SpringUtils.getBean("EFormValueDao");
      List<EFormValue> eformValues = eformValueDao.findByFormDataId(fdidInteger);
      savedEorder = eOrderDao.findByExtEFormDataId(fdidInteger);
      //eOrder exists in Dynacare's system
      if (savedEorder.getExternalOrderId() != null) {
        dynacareOrder = orderGateway.getASingleOrder(savedEorder.getExternalOrderId());
        if (dynacareOrder.getStatus() == OrderStatusEnum.NOT_PROCESSED) {
          dynacareOrder = orderGateway.updateOrder(eFormData, eformValues);
          updated = true;
        } else if (dynacareOrder.getStatus() == OrderStatusEnum.PROCESSED) {
          processed = true;
        }
      } else {
        dynacareOrder = orderGateway.createOrder(eFormData, eformValues);
      }
      String oscarInstanceId = DynacareOrderPreferences.getInstanceId();
      savedEorder.setExternalOrderId(Integer.toString(dynacareOrder.getId()));
      savedEorder.setState(OrderState.SUBMITTED);
      savedEorder.setBarcode((oscarInstanceId + "-" + savedEorder.getId()).toUpperCase());
      savedEorder = eOrderDao.saveEntity(savedEorder);
    } catch (Exception e) {
      log.error("Error adding eOrder: " + e.getMessage(), e);
      ActionMessage message = new ActionMessage("eform.errors.submit_eorder.failed", e.getMessage());
      ActionMessages submitOrderErrors = new ActionMessages();
      submitOrderErrors.add(ActionMessages.GLOBAL_MESSAGE, message);
      saveErrors(request, submitOrderErrors);
      request.setAttribute("curform", curForm);
      request.setAttribute("fdid", fdid);
      request.setAttribute("M_existingFdid", fdid);
      request.setAttribute("page_errors", "true");
      return mapping.getInputForward();
    }
    Code39Barcode barcode;
    String encodedBarcode = "";
    try {
      barcode = (Code39Barcode) BarcodeFactory.createCode39(
          BarcodeUtil.addCheckDigit(savedEorder.getBarcode()), false);
      BarcodeUtil.changeBarcodeLabel(barcode, savedEorder.getBarcode());
      barcode.setBarHeight(40);
      barcode.setBarWidth(2);
      barcode.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
      BufferedImage bufferedImage = BarcodeImageHandler.getImage(barcode);
      ByteArrayOutputStream output = new ByteArrayOutputStream();
      ImageIO.write(bufferedImage, "png", output);
      encodedBarcode = Base64.getEncoder().encodeToString(output.toByteArray());
      request.setAttribute("barcode_img", encodedBarcode);
    } catch (Exception e) {
      log.error("There was an error generating the barcode. The order has been placed.", e);
      ActionMessages submitOrderErrors = new ActionMessages();
      ActionMessage message = new ActionMessage("eform.errors.generate_eorder_barcode.failed");
      submitOrderErrors.add(ActionMessages.GLOBAL_MESSAGE, message);
      saveErrors(request, submitOrderErrors);
      request.setAttribute("page_errors", "true");
    }

    ActionMessages messages = getMessages(request);
    if (messages == null) {
      messages = new ActionMessages();
    }
    val message = getDynacareMessage(processed, updated);
    messages.add(SUCCESS_MESSAGES, message);
    saveMessages(request, messages);
    request.setAttribute("page_messages", "true");
    request.setAttribute("fdid", fdid);
    request.setAttribute("M_existingFdid", fdid);
    request.setAttribute("ext_order_id", savedEorder.getExternalOrderId());
    request.setAttribute("curform", curForm);
    request.setAttribute("status", convertStatusToString(dynacareOrder.getStatus()));
    request.setAttribute("status_description", getDescription(dynacareOrder.getStatus()));
    String eformPrintUrl = "";
    try {
      eformPrintUrl = getUrlWithoutParameters(PrintAction.getEformRequestUrl(request));

      String params = "?parentAjaxId=eforms&providerId=" + providerNo + "&fdid=" + fdid
              + "&populateValues=true&skipSave=false&M_IsDynacareEOrder=true&ext_order_id="
              + savedEorder.getExternalOrderId() + "&barcode=" + savedEorder.getBarcode();


      eformPrintUrl = eformPrintUrl + params;
      byte[] pdfLabReq = WKHtmlToPdfUtils.convertToPdf(eformPrintUrl);
      String encodedLabReq = Base64.getEncoder().encodeToString(pdfLabReq);
      savedEorder.setGeneratedPDF(encodedLabReq);
      savedEorder = eOrderDao.saveEntity(savedEorder);

    } catch (IOException | URISyntaxException e) {
      log.error("There was an error saving the lab req pdf. The order has been placed.", e);
      ActionMessages submitOrderErrors = new ActionMessages();
      ActionMessage errMsg = new ActionMessage("eform.errors.generate_eorder_pdf.failed");
      submitOrderErrors.add(ActionMessages.GLOBAL_MESSAGE, errMsg);
      saveErrors(request, submitOrderErrors);
      request.setAttribute("page_errors", "true");
    }

    return mapping.getInputForward();
  }

  private EFormData toEFormData(EForm eForm) {
    EFormData eFormData = new EFormData();
    eFormData.setFormId(Integer.parseInt(eForm.getFid()));
    eFormData.setFormName(eForm.getFormName());
    eFormData.setSubject(eForm.getFormSubject());
    eFormData.setDemographicId(Integer.parseInt(eForm.getDemographicNo()));
    eFormData.setCurrent(true);
    eFormData.setFormDate(new Date());
    eFormData.setFormTime(eFormData.getFormDate());
    eFormData.setProviderNo(eForm.getProviderNo());
    eForm.setFormHtml(eForm.getFormHtml().replaceAll("data-prechecked=\"true\" checked=\"checked\"", ""));
    eForm.setFormHtml(eForm.getFormHtml().replaceAll("data-prechecked=\"true\" value=\"X\"", ""));
    eFormData.setFormData(eForm.getFormHtml());
    eFormData.setShowLatestFormOnly(eForm.isShowLatestFormOnly());
    eFormData.setPatientIndependent(eForm.isPatientIndependent());
    eFormData.setRoleType(eForm.getRoleType());

    return (eFormData);
  }

  private EFormData updateEFormData(EForm eForm, EFormData eFormData, EFormDataDao eFormDataDao, Integer fdid,
                                    String fid, String demographic_no, ArrayList<String> paramNames, ArrayList<String> paramValues) {
//		EFormData eFormData=new EFormData();
    eFormData.setFormId(Integer.parseInt(eForm.getFid()));
    eFormData.setFormName(eForm.getFormName());
    eFormData.setSubject(eForm.getFormSubject());
    eFormData.setDemographicId(Integer.parseInt(eForm.getDemographicNo()));
    eFormData.setCurrent(true);
    eFormData.setFormDate(new Date());
    eFormData.setFormTime(eFormData.getFormDate());
    eFormData.setProviderNo(eForm.getProviderNo());
    eForm.setFormHtml(eForm.getFormHtml().replaceAll("data-prechecked=\"true\" checked=\"checked\"", ""));
    eForm.setFormHtml(eForm.getFormHtml().replaceAll("data-prechecked=\"true\" value=\"X\"", ""));
    eForm.setFormHtml(eForm.getFormHtml().replace("${last_update_date}",
        getDateString(eFormData.getFormDate(), "yyyy-MM-dd hh:mm:ss")));
    eFormData.setFormData(eForm.getFormHtml());
    eFormData.setShowLatestFormOnly(eForm.isShowLatestFormOnly());
    eFormData.setPatientIndependent(eForm.isPatientIndependent());
    eFormData.setRoleType(eForm.getRoleType());

    EFormData savedEformData = eFormDataDao.saveEntity(eFormData);

    EFormValueDao eFormValueDao = (EFormValueDao) SpringUtils.getBean("EFormValueDao");

    List<EFormValue> oldEformValues = eFormValueDao.findByFormDataId(fdid);
    Map<String, EFormValue> oldValuesMap = EFormValueHelper.getValueMap(oldEformValues);

    // adds parsed values

    ArrayList<String> paramNamesToAdd = new ArrayList<String>();
    ArrayList<String> paramValuesToAdd = new ArrayList<String>();
    for (int i = 0; i <= paramNames.size() - 1; i++) {
      EFormValue value = oldValuesMap.get(paramNames.get(i));
      if (value != null) {
        value.setVarValue(paramValues.get(i));
        value.setDemographicId(new Integer(demographic_no));
        eFormValueDao.merge(value);
      } else {
        paramNamesToAdd.add(paramNames.get(i));
        paramValuesToAdd.add(paramValues.get(i));
      }

    }

    EFormUtil.addEFormValues(paramNamesToAdd, paramValuesToAdd, fdid, new Integer(fid),
        new Integer(demographic_no));

    for (int i = 0; i <= oldEformValues.size() - 1; i++) {
      EFormValue oldValue = oldEformValues.get(i);
      if (!paramNames.contains(oldValue.getVarName())) {
        oldValue.setVarValue(null);
        oldValue.setDemographicId(null);
        eFormValueDao.merge(oldValue);
      }
    }

    return savedEformData;
  }

  /**
   * Handle Excelleris EOrder
   *
   * @param mapping  action mapping
   * @param form  action form
   * @param request  servlet request
   * @param response servlet response
   * @param curForm  current eForm
   * @param fdid eform data id
   * @param providerNo provider id
   * @param demographicNo patient id
   * @param eFormData eform data
   *
   * @return action forward or null
   */
  private ActionForward handleExcellerisEOrder(ActionMapping mapping, ActionForm form,
                                               HttpServletRequest request, HttpServletResponse response, EForm curForm, String fdid, String providerNo, String demographicNo, EFormData eFormData) {
    log.debug("Excelleris eOrder enabled eform.");
    try {

      String existingFdid = (String) request.getParameter("M_existingFdid");
      EOrderDao eOrderDao = (EOrderDao) SpringUtils.getBean("EOrderDao");
      ExcellerisEorder eOrder;
      Integer fdidInteger;
      if (!StringUtils.isNullOrEmpty(existingFdid)) {
        eOrder = (ExcellerisEorder) eOrderDao.findByExtEFormDataId(Integer.parseInt(existingFdid));
        fdidInteger = new Integer(existingFdid);
      } else {
        // create eOrder in OSCAR database
        eOrder = new ExcellerisEorder();
        fdidInteger = new Integer(fdid);
        eOrder.setEFormDataId(new Integer(fdidInteger));
        eOrder.setState(OrderState.NEW);
        eOrderDao.persist(eOrder);
      }
      // get eform values for the current eform data
      EFormValueDao eformValueDao = (EFormValueDao) SpringUtils.getBean("EFormValueDao");
      List<EFormValue> eformValues = eformValueDao.findByFormDataId(fdidInteger);

      boolean isOffLine = false;
      for (EFormValue val : eformValues) {
        if (val.getVarName().equals("M_IsOffLine")) {
          isOffLine = "true".equalsIgnoreCase(val.getVarValue());
        }
      }
      log.info("isOffLine :  " + isOffLine);

      // Set flag here before calling createOrder() to indicate eorder has been
      // saved and we shouldn't create a new one.
      request.setAttribute("M_existingFdid", fdid);
      // Also disable the eOrder button
      request.setAttribute("disable_order_button", "true");

      DemographicManager demographicManager = SpringUtils.getBean(DemographicManager.class);
      Demographic client = demographicManager.getDemographic(LoggedInInfo.getLoggedInInfoFromSession(request), demographicNo);

      // Setup patient consent for email notifications for eorders
      SMTPConfig smtpConfig = emailManager.getEmailConfig(LoggedInInfo.getLoggedInInfoFromSession(request));
      String enableEmailNotification = ((smtpConfig != null) && smtpConfig.getEnableEmail()) ? "true" : "false";
      if ("true".equals(enableEmailNotification)) {
        Boolean consentToUseEmailForEOrder = client.getConsentToUseEmailForEOrder();
        String requestedConsent = "";
        for (EFormValue val : eformValues) {
          if (val.getVarName().equals("EO_consent_eorder_email")) {
            requestedConsent = val.getVarValue();
          }
        }
        if (consentToUseEmailForEOrder == null) {
          consentToUseEmailForEOrder = ("yes".equals(requestedConsent)) ? Boolean.TRUE : Boolean.FALSE;
          client.setConsentToUseEmailForEOrder(consentToUseEmailForEOrder);
          demographicManager.updateDemographic(LoggedInInfo.getLoggedInInfoFromSession(request), client);
        }
      }

      // invoke service to submit order to Excelleris eOrder service
      OrderGateway orderGateway = (OrderGateway) SpringUtils.getBean(OrderGatewayImpl.class);
      AuthenticationTokenAndResource authenticationTokenAndParams = null;
      if (!isOffLine) {
        authenticationTokenAndParams = orderGateway.createOrder(request, eFormData, eformValues);

      } else {
        ActionMessages messages = new ActionMessages();
        ActionMessage message = new ActionMessage("eform.errors.submit_eorder.saved");
        messages.add(SUCCESS_MESSAGES, message);
        saveMessages(request, messages);
        request.setAttribute("curform", curForm);
        request.setAttribute("page_messages", "true");
        return mapping.getInputForward();
      }

      log.debug("authenticationTokenAndParams: {} " + authenticationTokenAndParams);

      if (authenticationTokenAndParams == null) {
        log.debug("authenticationTokenAndParams is null.");
        ActionMessages submitOrderErrors = new ActionMessages();
        ActionMessage message = new ActionMessage("eform.errors.submit_eorder.failed", "Received null response when submittion eorder.");
        submitOrderErrors.add(ActionMessages.GLOBAL_MESSAGE, message);
        saveErrors(request, submitOrderErrors);
        request.setAttribute("curform", curForm);
        request.setAttribute("page_errors", "true");
        return mapping.getInputForward();
      }

      // handle error messages from Excelleris eOrder
      ActionMessages submitOrderMsgs = new ActionMessages();
      OrderResponseHelper orHelper = new OrderResponseHelper(authenticationTokenAndParams.getResource());
      if (orHelper.getErrors() != null && !orHelper.getErrors().isEmpty()) {
        log.debug("Response has error(s).");
        for (String errorText : orHelper.getErrors()) {
          ActionMessage message = new ActionMessage("eform.errors.submit_eorder.failed", errorText);
          submitOrderMsgs.add(ERROR_MESSAGES, message);
        }
        request.setAttribute("curform", curForm);
      }

      log.info("ValidationBundleStatus : " + orHelper.getValidationBundleStatus());
      request.setAttribute("vb_stat", orHelper.getValidationBundleStatus() ? "true" : "false");

      // handle informational messages from Excelleris eOrder
      if (orHelper.getInformationalMessages() != null && !orHelper.getInformationalMessages().isEmpty()) {
        log.debug("Response has informational message(s).");
        for (String msgText : orHelper.getInformationalMessages()) {
          ActionMessage message = new ActionMessage("eform.errors.submit_eorder.info_msg", msgText);
          submitOrderMsgs.add(INFORMATIONAL_MESSAGES, message);
        }
      }
      if (!submitOrderMsgs.isEmpty()) {
        saveMessages(request, submitOrderMsgs);
      }

      // TODO: add logic to handle questionnaire, for now, just display an error
      if (orHelper.getQuestionnaire() != null) {
        log.debug("Response has Questionnaire.");

        // save the questionnaire as part of the eOrder
        JsonParser parser = new JsonParser();
        try {
          ExcellerisEorder savedEorder = (ExcellerisEorder) eOrderDao.findByExtEFormDataId(new Integer(fdidInteger));
          savedEorder.setQuestionnaire(parser.composeString(orHelper.getQuestionnaire()));
          eOrderDao.saveEntity(savedEorder);
        } catch (Exception e) {
          log.error("Error converting Parameters to JSON.", e);
          throw new ServiceException("Error converting Parameters to JSON.");
        }

        // Convert to api questionnaire
        OrderLabTestCodeCacheDao orderLabTestCodeCacheDao = (OrderLabTestCodeCacheDao) SpringUtils
            .getBean(OrderLabTestCodeCacheDao.class);
        Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

        final ProvincialBusinessLogic provincial = new ProvincialFactory(valueMap).getProvincialBusinessLogic();

        Questionnaire questionnaire = QuestionnaireConverter.toApiObject(orHelper.getQuestionnaire(), provincial, orderLabTestCodeCacheDao);

        // We need to enable the eOrder button for questionnaires
        request.setAttribute("disable_order_button", "false");
        request.setAttribute("questionnaire", questionnaire);
        request.setAttribute("is_new_question_set", "true");
        // set eorder id
        request.setAttribute("curform", curForm);
        return mapping.getInputForward();
      }

      // Check if patient is registered on LifeLabs portal
      ActionMessages messages = new ActionMessages();
      boolean patientLifeLabsStatus = orHelper.getPatientLifeLabsStatus();
      boolean isValidationBundle = orHelper.getValidationBundleStatus();
      // Only print success messages for OrderBundles
      if (!isValidationBundle) {
        if (patientLifeLabsStatus) {
          ActionMessage message = new ActionMessage("eform.email.requisition.inform");
          messages.add(SUCCESS_MESSAGES, message);
        } else {
          String patientEmail = client.getEmail();
          Boolean patientEmailConsent = client.getConsentToUseEmailForEOrder();
          if (patientEmail != null && patientEmail.length() > 0 && Boolean.TRUE.equals(patientEmailConsent) &&
                  "true".equals(enableEmailNotification)) {
            String firstName = client.getFirstName();
            String lastName = client.getLastName();

            log.info("Sending Lab Requisition email ...");
            boolean result = emailManager.sendLabRequisitionEmail(client, orHelper.getOrderId());
            if (result) {
              log.info("Lab Requisition email sent.");
              ActionMessage message = new ActionMessage("eform.email.requisition.do_not_fax_email");
              messages.add(SUCCESS_MESSAGES, message);
              message = new ActionMessage("eform.email.requisition.in_person_visit", firstName, lastName, orHelper.getOrderId());
              messages.add(SUCCESS_MESSAGES, message);
              message = new ActionMessage("eform.email.requisition.virtual_visit", firstName, lastName, orHelper.getOrderId());
              messages.add(SUCCESS_MESSAGES, message);
            } else {
              log.info("Couldn't send Lab Requisition email.");
              ActionMessage message = new ActionMessage("eform.email.requisition.do_not_fax_email");
              messages.add(SUCCESS_MESSAGES, message);
              message = new ActionMessage("eform.email.requisition.no_email.in_person_visit");
              messages.add(SUCCESS_MESSAGES, message);
              message = new ActionMessage("eform.email.requisition.no_email.virtual_visit");
              messages.add(SUCCESS_MESSAGES, message);
            }
          } else {
            ActionMessage message = new ActionMessage("eform.email.requisition.do_not_fax_email");
            messages.add(SUCCESS_MESSAGES, message);
            message = new ActionMessage("eform.email.requisition.no_email.in_person_visit");
            messages.add(SUCCESS_MESSAGES, message);
            message = new ActionMessage("eform.email.requisition.no_email.virtual_visit");
            messages.add(SUCCESS_MESSAGES, message);
          }
        }
      }

      ActionMessages existingMessages = getMessages(request);
      if (existingMessages != null && existingMessages.isEmpty() == false) {
        messages.add(existingMessages);
      }
      if (!isValidationBundle) {
        ActionMessage message = new ActionMessage("eform.errors.submit_eorder.success");
        messages.add(SUCCESS_MESSAGES, message);
      }
      saveMessages(request, messages);

      String pdfForm = orHelper.getPdfLabRequisitionForm();
      String orderId = orHelper.getOrderId();

      log.debug("pdfForm : " + pdfForm);
      log.debug("orderId : " + orderId);

      // update eorder with pdf lab requisition and external order id
      ExcellerisEorder savedEOrder = (ExcellerisEorder) eOrderDao.findByExtEFormDataId(new Integer(fdid));
      log.debug("Saved EOrder id : " + savedEOrder.getId());
      savedEOrder.setGeneratedPDF(pdfForm);
      savedEOrder.setExternalOrderId(orderId);
      savedEOrder.setState(OrderState.SUBMITTED);
      eOrderDao.merge(savedEOrder);

      // save excelleris patient id
      String patientId = orHelper.getPatientId();
      if (patientId != null) {
        final DemographicExtDao demographicExtDao = (DemographicExtDao) SpringUtils.getBean(DemographicExtDao.class);
        String excellerisPatientIdInDB = demographicExtDao.getValueForDemoKey(new Integer(demographicNo), EXCELLERIS_PATIENT_ID);
        if (excellerisPatientIdInDB == null) {
          DemographicExt demographicExt = new DemographicExt(
              providerNo, new Integer(demographicNo), EXCELLERIS_PATIENT_ID, patientId);
          demographicExtDao.saveDemographicExt(demographicExt);
        }
      }


    } catch (final BaseServerResponseException ex) {
      String errorMsg = null;
      ActionMessage message = null;
      ActionMessages submitOrderErrors = new ActionMessages();

      log.error("Error submitting error", ex);


      if (ex.getStatusCode() == 400) {
        errorMsg = "Status Code = " + ex.getStatusCode() + " - Invalid input - EMR troubleshooting required.  ";

      } else if (ex.getStatusCode() == 401 || ex.getStatusCode() == 403) {
        errorMsg = "Status Code = " + ex.getStatusCode() +
            " - Try to login again.  If the problem persists, contact Oscar Service Desk.  ";

      } else {
        errorMsg = "Status Code = " + ex.getStatusCode() +
            " - Contact Oscar Service Desk.  ";
      }
      message = new ActionMessage("eform.errors.submit_eorder.failed", errorMsg);
      submitOrderErrors.add(ActionMessages.GLOBAL_MESSAGE, message);

      saveErrors(request, submitOrderErrors);
      request.setAttribute("curform", curForm);
      request.setAttribute("page_errors", "true");
      return mapping.getInputForward();

    } catch (final Exception ex) {
      log.error("Error submitting error", ex);
      ActionMessages submitOrderErrors = new ActionMessages();
      String errorMsg = ex.getMessage();
      ActionMessage message = new ActionMessage("eform.errors.submit_eorder.failed", errorMsg);
      submitOrderErrors.add(ActionMessages.GLOBAL_MESSAGE, message);
      saveErrors(request, submitOrderErrors);
      request.setAttribute("curform", curForm);
      request.setAttribute("page_errors", "true");
      return mapping.getInputForward();
    }

    return null;
  }

  private String getUrlWithoutParameters(String url) throws URISyntaxException {
    URI uri = new URI(url);
    return new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), null, // Ignore the query part of the input
            uri.getFragment()).toString();
  }

  private ActionMessage getDynacareMessage(boolean processed, boolean updated) {
    return processed
        ? new ActionMessage("eform.errors.update_eorder.processed")
        : updated
            ? new ActionMessage("eform.errors.update_eorder.notprocessed")
            : new ActionMessage("eform.errors.submit_eorder.success");
  }

  private String getDateString(Date date, String format) {
    return new SimpleDateFormat(format).format(date);
  }
}
