/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.eform.actions.eorder;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.DynacareCopyToProviderDao;
import org.oscarehr.common.dao.ExcellerisCopyToProviderDao;
import org.oscarehr.common.model.DynacareCopyToProvider;
import org.oscarehr.common.model.EorderCopyToProvider;
import org.oscarehr.common.model.ExcellerisCopyToProvider;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.owasp.encoder.Encode;
import oscar.eform.data.eorder.AddCopyToProviderForm;

public class AddCopyToProviderAction extends Action {

  private ExcellerisCopyToProviderDao excellerisCopyToProviderDao =
      (ExcellerisCopyToProviderDao) SpringUtils.getBean("excellerisCopyToProviderDao");

  private DynacareCopyToProviderDao dynacareCopyToProviderDao =
      (DynacareCopyToProviderDao) SpringUtils.getBean(DynacareCopyToProviderDao.class);

  private static final Logger logger = MiscUtils.getLogger();

  @Override
  public ActionForward execute(
      ActionMapping mapping,
      ActionForm form,
      HttpServletRequest request,
      HttpServletResponse response)
      throws ServletException, IOException {

    EorderCopyToProvider copyToProvider = null;
    AddCopyToProviderForm addCopyToProviderForm = (AddCopyToProviderForm) form;

    if (addCopyToProviderForm.getType().equals(DynacareCopyToProvider.class.getSimpleName())) {
      copyToProvider = new DynacareCopyToProvider();
      populateFields((DynacareCopyToProvider) copyToProvider, addCopyToProviderForm);
      copyToProvider =
          dynacareCopyToProviderDao.saveEntity((DynacareCopyToProvider) copyToProvider);
    }
    String added = copyToProvider.getFirstName() + " " + copyToProvider.getLastName();
    request.setAttribute("Added", added);
    request.setAttribute("copyToProvider", copyToProvider);
    return mapping.getInputForward();
  }

  private void populateFields(
      ExcellerisCopyToProvider copyToProvider, AddCopyToProviderForm addCopyToProviderForm) {
    copyToProvider.setFirstName(addCopyToProviderForm.getFirstName());
    copyToProvider.setLastName(addCopyToProviderForm.getLastName());
    copyToProvider.setAddress1(addCopyToProviderForm.getAddress1());
    copyToProvider.setCity(addCopyToProviderForm.getCity());
    String stateOrProvince = addCopyToProviderForm.getStateOrProvince();
    copyToProvider.setStateOrProvince(stateOrProvince);
    String country = stateOrProvince.startsWith("US") ? "US" : "CA";
    copyToProvider.setCountry(country);
    copyToProvider.setZipOrPostal(addCopyToProviderForm.getZipOrPostal());
    copyToProvider.setDeleted(false);
    copyToProvider.setVisible(true);
  }

  private void populateFields(
      DynacareCopyToProvider copyToProvider, AddCopyToProviderForm addCopyToProviderForm) {
    copyToProvider.setId(addCopyToProviderForm.getId());
    copyToProvider.setFirstName(Encode.forHtml(addCopyToProviderForm.getFirstName()));
    copyToProvider.setLastName(Encode.forHtml(addCopyToProviderForm.getLastName()));
    copyToProvider.setAddress1(Encode.forHtml(addCopyToProviderForm.getAddress1()));
    copyToProvider.setCity(Encode.forHtml(addCopyToProviderForm.getCity()));
    String stateOrProvince = addCopyToProviderForm.getStateOrProvince();
    copyToProvider.setStateOrProvince(stateOrProvince);
    String country = stateOrProvince.startsWith("US") ? "US" : "CA";
    copyToProvider.setCountry(country);
    copyToProvider.setZipOrPostal(Encode.forHtml(addCopyToProviderForm.getZipOrPostal()));
    copyToProvider.setCpso(Encode.forHtml(addCopyToProviderForm.getCpso()));
    copyToProvider.setBillingNumber(Encode.forHtml(addCopyToProviderForm.getBillingNumber()));
  }
}
