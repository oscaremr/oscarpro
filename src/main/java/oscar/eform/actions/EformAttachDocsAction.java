package oscar.eform.actions;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.common.dao.EformDocsDao;
import org.oscarehr.common.model.EformDocs;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

public class EformAttachDocsAction extends DispatchAction{
	private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
	private static EformDocsDao eformDocsDao = (EformDocsDao) SpringUtils.getBean(EformDocsDao.class);
	
	  public ActionForward attachDoc(ActionMapping mapping, ActionForm form,
	                               HttpServletRequest request,
	                               HttpServletResponse response)

	      throws ServletException, IOException {    

		  	if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_eform", "r", null)) {
				throw new SecurityException("missing required security object (_eform)");
			}
		  
	        DynaActionForm frm = (DynaActionForm)form;
	        LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
	        
	        String requestId = frm.getString("requestId");
	        String demoNo = frm.getString("demoNo");
	        String provNo = frm.getString("providerNo");
	        
	        return mapping.findForward("success");
	    }
	  
	  public ActionForward attachPacs(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
  		String requestId = request.getParameter("requestId");
		List<EformDocs> eDocs =  eformDocsDao.findByRequestIdAndDocType(Integer.parseInt(requestId), "P");
		List<String> uid_list = new ArrayList<String>();
		for(int i = 0;i < eDocs.size();i ++){
			uid_list.add(eDocs.get(i).getDocumentNo().toString());
		}
		
		JSONObject ret = new JSONObject();
		ret.put("uidList", uid_list);
		try {
			PrintWriter out = response.getWriter();
			out.print(ret.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			MiscUtils.getLogger().info(e.toString());
		}
  		return null;
	  }
}
