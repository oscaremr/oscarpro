/**
 * Copyright (c) 2008-2012 Indivica Inc.
 *
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "indivica.ca/gplv2"
 * and "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.eform.actions;

import ca.kai.printable.Printable;
import ca.kai.printable.PrintableAttachmentType;
import ca.oscarpro.common.http.OscarProHttpService;
import ca.oscarpro.fax.FaxException;
import ca.oscarpro.fax.FaxFacade;
import ca.oscarpro.fax.FaxFileType;
import ca.oscarpro.fax.SendFaxData;
import ca.oscarpro.service.OscarProConnectorService.OscarProConnectorServiceException;
import com.google.gson.Gson;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.lowagie.text.DocumentException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import lombok.val;
import lombok.var;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.ClinicDAO;
import org.oscarehr.common.dao.EFormDataDao;
import org.oscarehr.common.dao.FaxConfigDao;
import org.oscarehr.common.dao.PatientLabRoutingDao;
import org.oscarehr.common.dao.SiteDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.FaxConfig;
import org.oscarehr.common.model.PatientLabRouting;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.Site;
import org.oscarehr.hospitalReportManager.HRMPDFCreator;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentDao;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentToDemographicDao;
import org.oscarehr.hospitalReportManager.model.HRMDocument;
import org.oscarehr.hospitalReportManager.model.HRMDocumentToDemographic;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.util.WKHtmlToPdfUtils;
import oscar.OscarProperties;
import oscar.SxmlMisc;
import oscar.dms.EDoc;
import oscar.dms.EDocUtil;
import oscar.eform.EFormUtil;
import oscar.oscarLab.ca.all.pageUtil.CMLPdfCreator;
import oscar.oscarLab.ca.all.pageUtil.LabPDFCreator;
import oscar.oscarLab.ca.all.parsers.Factory;
import oscar.oscarLab.ca.all.parsers.MessageHandler;
import oscar.oscarLab.ca.on.CML.CMLLabTest;
import oscar.util.ConcatPDF;
import oscar.util.ConversionUtils;
import oscar.util.SmartFaxUtil;
import oscar.util.UtilDateUtilities;


public final class FaxAction {

	private static final String ATTACHMENT_MANAGER_EFORM_ENABLED =
			"attachment_manager.eform.enabled";
	private static final String EFORM_PRINT_URL =
			"%s/api/v1/printables/print";

	private static final Logger logger = MiscUtils.getLogger();

	private String localUri = null;
	
	private boolean skipSave = false;
	private static final FaxFacade faxFacade = SpringUtils.getBean(FaxFacade.class);

	public FaxAction(HttpServletRequest request) {
		localUri = getEformRequestUrl(request);
		skipSave = "true".equals(request.getParameter("skipSave"));
	}

	/**
	 * This method is a copy of Apache Tomcat's ApplicationHttpRequest getRequestURL method with the exception that the uri is removed and replaced with our eform viewing uri. Note that this requires that the remote url is valid for local access. i.e. the
	 * host name from outside needs to resolve inside as well. The result needs to look something like this : https://127.0.0.1:8443/oscar/eformViewForPdfGenerationServlet?fdid=2&parentAjaxId=eforms
	 */
	private String getEformRequestUrl(HttpServletRequest request) {
		StringBuilder url = new StringBuilder();
		String scheme = "http";
		Integer port = 80;
		if (request.getServerPort() != 80) {
			port = 8080;
		}

		url.append(scheme);
		url.append("://");
		//url.append(request.getServerName());
		url.append("127.0.0.1");
		url.append(':');
		url.append(port);

		url.append(request.getContextPath());
		url.append("/EFormViewForPdfGenerationServlet?parentAjaxId=eforms&prepareForFax=true&providerId=");
		url.append(request.getParameter("providerId"));
		url.append("&fdid=");

		return (url.toString());
	}

	/**
	 * This method will take eforms and send them to a PHR.
	 * @throws DocumentException 
	 */
	public void faxForms(String[] numbers, String formId, String providerId,
			String demographicNo, HttpServletRequest request)
			throws DocumentException, FaxException, OscarProConnectorServiceException {
		boolean addDocumentToEchart = Boolean.parseBoolean(request.getParameter("printDocumentToEchartOnPdfFax"));
		File tempFile = null;

		try {
			EFormDataDao eFormDataDao = SpringUtils.getBean(EFormDataDao.class);
			EFormData eFormData = eFormDataDao.find(Integer.parseInt(formId));
			Integer demographicNumber = 0;
			if (eFormData!=null){
				demographicNumber = eFormData.getDemographicId();
			}

			logger.info("Generating PDF for eform with fdid = " + formId);

			// convert to PDF
			String viewUri = localUri + formId;
			boolean isRichTextLetter = eFormData.getFormId().toString().equals(OscarProperties.getInstance().getProperty("rtl_template_id", ""));
			if (isRichTextLetter) {
				tempFile = File.createTempFile("generatedEform." + formId + "-", ".pdf");
				EFormUtil.printRtlWithTemplate(eFormData, tempFile, skipSave, request);
			} else {
				tempFile = File.createTempFile("EForm." + formId, ".pdf");
				convertToPdf(formId, viewUri, tempFile, request);
			}
			
			if (addDocumentToEchart) {
				LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
				// Saves the eform to the patient's chart
				EDocUtil.saveDocumentToPatient(loggedInInfo, loggedInInfo.getLoggedInProvider(), eFormData.getDemographicId(), tempFile, -1, "", eFormData.getFormName());
			}
			
			logger.info("Writing pdf to : "+tempFile.getCanonicalPath());
			String eform_name = UtilDateUtilities.getToday("yyyy-mm-dd.hh.mm.ss");
			File eform_fax_pdf = File.createTempFile("EForm." + eform_name, ".pdf");
			LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
			combinePDFs(request, loggedInInfo, tempFile.getCanonicalPath(), eform_fax_pdf.getCanonicalPath());
			
			// Removing all non digit characters from fax numbers.
			for (int i = 0; i < numbers.length; i++) { 
				numbers[i] = numbers[i].trim().replaceAll("\\D", "");
			}
			ArrayList<String> recipients = new ArrayList<String>(Arrays.asList(numbers));
			// Removing duplicate phone numbers.
			recipients = new ArrayList<String>(new HashSet<String>(recipients));

			val pdfBytes = Files.readAllBytes(eform_fax_pdf.toPath());
			String faxClinicId = OscarProperties.getInstance().getProperty("fax_clinic_id", "1234");
			String faxNumber = "";
			ClinicDAO clinicDAO = SpringUtils.getBean(ClinicDAO.class);
			if (!faxClinicId.isEmpty() && clinicDAO.find(Integer.parseInt(faxClinicId)) != null) {
				faxNumber = clinicDAO.find(Integer.parseInt(faxClinicId)).getClinicFax();
				faxNumber = faxNumber.replaceAll("[^0-9]", "");
			}

			val sendFaxData = SendFaxData.builder()
					.providerNumber(providerId)
					.demographicNumber(Integer.parseInt(demographicNo))
					.faxFileType(FaxFileType.FORM)
					.cookies(request.getCookies())
					.faxLine(faxNumber)
					.pdfDocumentBytes(pdfBytes)
					.loggedInInfo(loggedInInfo)
					.build();

			for (int i = 0; i < recipients.size(); i++) {
				String faxNo = recipients.get(i);
				if (faxNo.length() < 7) {
					throw new DocumentException("Document target fax number '" + faxNo + "' is invalid.");
				}
				String tempName = "EForm-" + formId + "." + i + "." + System.currentTimeMillis();
				sendFaxData.setDestinationFaxNumber(faxNo);
				sendFaxData.setFileName(tempName);
				faxFacade.sendFax(sendFaxData);

				if (skipSave) {
					 eFormData.setCurrent(false);
					 eFormDataDao.merge(eFormData);
				}
			}

			FaxConfigDao faxConfigDao = SpringUtils.getBean(FaxConfigDao.class);
			val validFaxNumber = !faxConfigDao.findAll(null, null).isEmpty();
			if (!validFaxNumber) {
				logger.error("PROBLEM CREATING FAX JOB",
						new DocumentException("There are no fax configurations setup for this clinic."));
			}

			// Removing the consulation pdf.
			tempFile.delete();
			eform_fax_pdf.delete();
						
		} catch (IOException e) {
			MiscUtils.getLogger().error("Error converting and sending eform. id="+formId, e);
		} 
	}

	void convertToPdf(final String formId, final String viewUri, final File tempFile,
			final HttpServletRequest request) throws IOException {
		val systemPreferencesDao = (SystemPreferencesDao) SpringUtils.getBean(
				SystemPreferencesDao.class);
		val attachmentManagerEFormEnabled = systemPreferencesDao.isReadBooleanPreferenceWithDefault(
				ATTACHMENT_MANAGER_EFORM_ENABLED, false);
		if (attachmentManagerEFormEnabled) {
			printEFormAndSaveToTempFile(formId, tempFile, request);
		} else {
			legacyConvertToPdf(viewUri, tempFile);
		}
	}

	void legacyConvertToPdf(final String viewUri, final File tempFile) throws IOException {
		WKHtmlToPdfUtils.convertToPdf(viewUri, tempFile);
	}

	private void printEFormAndSaveToTempFile(final String formId, final File tempFile,
			final HttpServletRequest request) {
		val requestUrl = String.format(EFORM_PRINT_URL, OscarProperties.getOscarProBaseUrl());
		val requestData =
				new Gson().toJson(new Printable(formId, PrintableAttachmentType.Eforms), Printable.class);

		val oscarProHttpService = (OscarProHttpService) SpringUtils.getBean(OscarProHttpService.class);
		val pdfPrintResponse = oscarProHttpService.makeLoggedInPostRequestToPro(
				requestUrl, requestData, request);

		if (pdfPrintResponse == null || pdfPrintResponse.getEntity() == null) {
			logger.error("Error getting eForm from Oscar Pro. id=" + formId);
			return;
		}

		try {
			saveEFormToTempFile(formId, tempFile, EntityUtils.toString(pdfPrintResponse.getEntity()));
		} catch (IOException e) {
			logger.error("Error getting eForm file name from response. id=" + formId, e);
		}
	}

	void saveEFormToTempFile(final String formId, final File tempFile, final String fileName) {
		try {
			val pdfReader = new PdfReader(fileName);
			val pdfStamper = new PdfStamper(pdfReader, Files.newOutputStream(tempFile.toPath()));
			pdfStamper.close();
		} catch (IOException | com.itextpdf.text.DocumentException e) {
			logger.error("Error saving eForm to temp file. id=" + formId, e);
		}
	}

	public void attachForms(String formId) throws DocumentException {
		File tempFile = null;
		String tempPath = System.getProperty("java.io.tmpdir");
		String tempName = "EForm-" + formId + "." + System.currentTimeMillis();

		try {
			// Create temp file
			tempFile = File.createTempFile(tempName, ".pdf");

			// Convert to PDF
			String viewUri = localUri + formId;
			WKHtmlToPdfUtils.convertToPdf(viewUri, tempFile);
			logger.info("Writing pdf to : "+tempFile.getCanonicalPath());
			String tempPdf = String.format("%s%s%s.pdf", tempPath, File.separator, tempName);
			FileUtils.copyFile(tempFile, new File(tempPdf));

			// Removing the temp consultation pdf.
			tempFile.delete();

		} catch (IOException e) {
			MiscUtils.getLogger().error("Error converting and sending eform. id="+formId, e);
		}

	}
	
	private static String getUserFax(String userId){
    	String fax = "";
    	
    	ProviderDao providerDao = (ProviderDao) SpringUtils.getBean("providerDao");
    	Provider provider = providerDao.getProvider(userId);
    	if (provider!=null && provider.getComments()!=null) {
    		fax = SxmlMisc.getXmlContent(provider.getComments(),"xml_p_fax");
    	}
    	
    	return fax;
    }

	public static boolean saveFaxJob(String destFaxNo, String curUserNo, String providerNo,
			String faxNumber, String currentUserFaxNo, int demoNo, String pdfFile, 
			String defaultFaxModem, List<FaxConfig> faxConfigs, HttpServletRequest request)
			throws FaxException, IOException, OscarProConnectorServiceException {
		
		if (faxConfigs == null) {
			faxConfigs = new ArrayList<FaxConfig>();
		}
		boolean validFaxNumber = false;

		var destinationFax = destFaxNo;
		if (defaultFaxModem != null && !defaultFaxModem.isEmpty()) {
			destinationFax = defaultFaxModem;
		}

		if (faxNumber == null || faxNumber.trim().isEmpty()) {
			faxNumber = currentUserFaxNo;
			if (faxNumber.isEmpty() && !faxConfigs.isEmpty()) {
				faxNumber = faxConfigs.get(0).getFaxNumber();
			}
		}

		val sendFaxData = SendFaxData.builder()
				.destinationFaxNumber(destinationFax)
				.providerNumber(providerNo)
				.demographicNumber(demoNo)
				.faxFileType(FaxFileType.CONSULTATION)
				.fileName(FilenameUtils.getBaseName(new File(pdfFile).getName()))
				.cookies(request.getCookies())
				.faxLine(faxNumber.replace("-", "").replace(" ", ""))
				.pdfDocumentBytes(Files.readAllBytes(Paths.get(pdfFile)))
				.loggedInInfo(LoggedInInfo.getLoggedInInfoFromSession(request))
				.build();

		faxFacade.sendFax(sendFaxData);

		for (FaxConfig faxConfig : faxConfigs) {
			if (StringUtils.isBlank(faxConfig.getFaxNumber()) || faxConfig.getFaxNumber().equals(faxNumber)) {
				validFaxNumber = true;
				break;
			}
		}
		if( !validFaxNumber ) {
			logger.error("PROBLEM CREATING FAX JOB", new DocumentException("Document outgoing fax number '" + faxNumber + "' is not the same number in faxNumber of the table fax_config."));
		}

		return validFaxNumber;
	}
	
	public static String[] getCurFaxNoDefaultModem(String siteName, String curUserNo) {
		String[] defaultFaxModem = {"",""};
		
		String currentUserFaxNo = "";
		String smartfaxSiteBind = OscarProperties.getInstance().getProperty("smartfax_site_bind");
		if (smartfaxSiteBind!=null) {
			smartfaxSiteBind = smartfaxSiteBind.trim();
		}
		if(org.oscarehr.common.IsPropertiesOn.isMultisitesEnable() && smartfaxSiteBind!=null && 
				smartfaxSiteBind.equalsIgnoreCase("true")) {
			//get site fax number
			if (siteName !=null && !siteName.trim().isEmpty()) {
				SiteDao siteDao = (SiteDao) SpringUtils.getBean("siteDao");
				Site site = siteDao.getByLocation(siteName.trim());
				if(site!=null && site.getFax()!=null && site.getFax().length()>0) {
					currentUserFaxNo = site.getFax();
				}
			}
			
		} else {
			if (curUserNo!=null) {
				currentUserFaxNo = getUserFax(curUserNo);	
			}
		}
		defaultFaxModem[0] = currentUserFaxNo;
		defaultFaxModem[1] = SmartFaxUtil.getDefaultFaxModem(currentUserFaxNo);
		
		return defaultFaxModem;
	}

	private void combinePDFs(HttpServletRequest request, LoggedInInfo loggedInInfo, String getFileName, String currentFileName) throws IOException{
        String reqId = (String)request.getAttribute("fdid");
		String demoNo = request.getParameter("efmdemographic_no");
		String efname  = (String)request.getAttribute("efname");
        ArrayList<EDoc> eformdocs = EDocUtil.listEformDocs(loggedInInfo, demoNo, reqId, EDocUtil.ATTACHED);
        ArrayList<Object> pdfDocs = new ArrayList<Object>();
        File file2 = null;
       
        // add recently created pdf to the list
        pdfDocs.add(getFileName);

        for (int i=0; i < eformdocs.size(); i++){
            EDoc curDoc =  eformdocs.get(i);
            if ( curDoc.isPDF() )
            	try {
                	pdfDocs.add(curDoc.getFilePath());
                } catch(Exception ex) {	logger.error("File not found: "+curDoc.getFileName()); }
        }
        // create pdfs from attached labs
        PatientLabRoutingDao dao = SpringUtils.getBean(PatientLabRoutingDao.class);
        
        try {
            for(Object[] i : dao.findRoutingsAndEformDocsByEfromId(ConversionUtils.fromIntString(reqId), "L")) {
            	PatientLabRouting p = (PatientLabRouting) i[0];

            	if(p.getLabType().equals("HL7")){
	                String segmentId = "" + p.getLabNo();
	                request.setAttribute("segmentID", segmentId);
	                MessageHandler handler = Factory.getHandler(segmentId);
	                String fileName = OscarProperties.getInstance().getProperty("DOCUMENT_DIR")+"//"+handler.getPatientName().replaceAll("\\s", "_")+"_"+handler.getMsgDate()+"_LabReport.pdf";
	                OutputStream os = new FileOutputStream(fileName);
	                LabPDFCreator pdf = new LabPDFCreator(request, os);
	                pdf.printPdf();
	                pdfDocs.add(fileName);
            	}else if(p.getLabType().equals("CML")){
                	String segmentId = "" + p.getLabNo();
	                request.setAttribute("segmentID", segmentId);
	                CMLLabTest lab = new CMLLabTest();
	                lab.populateLab(segmentId);
	                String fileName = OscarProperties.getInstance().getProperty("DOCUMENT_DIR")+"//"+(lab.pLastName + " " + lab.pFirstName).replaceAll("\\s", "_")+"_"+ lab.printDate +"_LabReport.pdf";
	                file2 = new File(fileName);
	                OutputStream os = new FileOutputStream(file2);
	                CMLPdfCreator cml = new CMLPdfCreator(request, os);
	                cml.printPdf();
	                pdfDocs.add(fileName);
                }
            }
            
            HRMDocumentDao hrmDao = (HRMDocumentDao)SpringUtils.getBean("HRMDocumentDao");
         // add hrm document
 			HRMDocumentToDemographicDao hrmToDemoDao = (HRMDocumentToDemographicDao)SpringUtils.getBean("HRMDocumentToDemographicDao");
 			List<HRMDocumentToDemographic> hList = hrmToDemoDao.findByDemographicNoAndEformID(demoNo, reqId, true);
            for (HRMDocumentToDemographic htd : hList) {
                HRMDocument document = hrmDao.findById(htd.getHrmDocumentId()).get(0);
                OutputStream os;
                String fileName = document.getReportFile() +"_LabReport.pdf";
                try {
                	os = new FileOutputStream(fileName); //For THT: reportFile has full folders
                } catch (Exception e) {
                	fileName = OscarProperties.getInstance().getProperty("DOCUMENT_DIR")+fileName;  //Oscar pro: reportFile only has file name without folder
                	os = new FileOutputStream(fileName);
                }
                
                //HrmPDFCreator hrmpdfc = new HrmPDFCreator(os, htd.getHrmDocumentId());
    			//hrmpdfc.printPdf(loggedInInfo);
                HRMPDFCreator hrmpdfc = new HRMPDFCreator(os, htd.getHrmDocumentId(), loggedInInfo);
                hrmpdfc.printPdf();
                pdfDocs.add(fileName);
            }
            
          //add eform list
            EFormDataDao efromDocs=(EFormDataDao)SpringUtils.getBean("EFormDataDao");
            int demono=Integer.parseInt(demoNo);
             
         	List<EFormData> eList = efromDocs.getSavedEfromDataByDemoId(demono,ConversionUtils.fromIntString(reqId));
         	for (EFormData eFormData : eList) {
         		if(eFormData.getFormData().indexOf("id=\"root\"") > -1){
         			continue;
         		}
         		String formId=eFormData.getId().toString();
				//eform pdf
         		    File tempFile = null;
        			logger.info("Generating PDF for eform with fdid = " + formId);
        			tempFile = File.createTempFile("EFormPrint." + formId, ".pdf");
        			//tempFile.deleteOnExit();
        		    String	localUri = getEformRequestUrl(request);
        			// convert to PDF
        			String viewUri = localUri + formId;
        			WKHtmlToPdfUtils.convertToPdf(viewUri, tempFile);
        			logger.info("Writing pdf to : "+tempFile.getCanonicalPath());
        			pdfDocs.add(tempFile.getAbsolutePath());
			}
        }catch(DocumentException de) {
            request.setAttribute("printError", new Boolean(true));
        }catch(IOException ioe) {
            request.setAttribute("printError", new Boolean(true));
        }catch(Exception e){
            request.setAttribute("printError", new Boolean(true));
        }

//        response.setContentType("application/pdf");  //octet-stream
//        response.setHeader("Content-Disposition", "attachment; filename=\"" + currentFileName + "\"");
        if (efname!=null) {
        	String[] allname = efname.split(",");
        	for (String filename : allname) {
        		if (filename==null||filename.trim().length()==0) {
					continue;
				}
        		String folder=System.getProperty("java.io.tmpdir");
            	pdfDocs.add(folder+File.separator+filename);
			}
		}
        
        OutputStream out = new FileOutputStream(new File(currentFileName));
        ConcatPDF.concat(pdfDocs, out);
        
        if(file2 != null){
        	file2.delete();
        }
        if (efname!=null) {
        	String[] allname = efname.split(",");
        	for (String filename : allname) {
        		if (filename==null||filename.trim().length()==0) {
					continue;
				}
        		String folder=System.getProperty("java.io.tmpdir");
        		String path = folder+File.separator+filename;
        		File file = new File(path);
        		if (file.exists()) {
					file.delete();
				}
			}
		}
    }
}
