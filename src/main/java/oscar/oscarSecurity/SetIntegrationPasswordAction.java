/**
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.oscarSecurity;

import ca.oscarpro.security.EncryptionException;
import ca.oscarpro.security.OscarPasswordService;
import ca.oscarpro.security.PasswordValidationException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.SecurityDao;
import org.oscarehr.util.SpringUtils;
import oscar.Misc;
import oscar.OscarProperties;

public class SetIntegrationPasswordAction extends Action {

    private static SecurityDao securityDao = SpringUtils.getBean(SecurityDao.class);

    public ActionForward execute(
        final ActionMapping mapping,
        final ActionForm form,
        final HttpServletRequest request,
        final HttpServletResponse response
    ) throws ServletException, IOException, NoSuchAlgorithmException, EncryptionException {
        val username = request.getParameter("username");
        try {
            OscarPasswordService.validatePassword(request.getParameter("password"));
        } catch (PasswordValidationException e) {
            request.setAttribute("errorMessage", e.getMessage());
            return mapping.findForward("error");
        }
        val password = OscarPasswordService.encodePassword(request.getParameter("password"));
        val pin = processPin(request);
        if (isFormNotValid(username, password, pin)) {
            request.setAttribute("errorMessage", "Input data is invalid. Security record Integration password was not updated.");
            return mapping.findForward("error");
        }
        val securityRecords = securityDao.findByUserName(username);
        if (!securityRecords.isEmpty()) {
            val securityRecord = securityRecords.get(0);
            securityRecord.setPassword(password);
            OscarPasswordService.setPasswordVersion(securityRecord);
            securityRecord.setPin(pin);
            securityDao.merge(securityRecord);
        }
        request.setAttribute("username", username);
        return mapping.findForward("success");
    }

    private boolean isFormNotValid(final String username, final String password, final String pin) {
        return username.equals("-1") || password == null || password.isEmpty() || pin == null || pin.isEmpty();
    }

    private String processPin(final HttpServletRequest request) {
        val pin = request.getParameter("pin");
        return OscarProperties.getInstance().isPINEncripted() ? Misc.encryptPIN(pin) : pin;
    }
}
