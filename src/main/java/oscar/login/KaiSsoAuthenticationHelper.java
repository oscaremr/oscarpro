/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.login;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import java.util.Date;
import java.util.List;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.PMmodule.dao.SecUserRoleDao;
import org.oscarehr.PMmodule.model.SecUserRole;
import org.oscarehr.common.dao.FacilityDao;
import org.oscarehr.common.dao.ProviderPreferenceDao;
import org.oscarehr.common.dao.SecurityDao;
import org.oscarehr.common.model.Facility;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.ProviderPreference;
import org.oscarehr.common.model.Security;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SessionConstants;
import org.oscarehr.util.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.WebUtils;

public class KaiSsoAuthenticationHelper extends HandlerInterceptorAdapter {

  private SigningKeyResolverService signingKeyResolverService;
  private ProviderDao providerDao;
  private ProviderPreferenceDao providerPreferenceDao;
  private FacilityDao facilityDao;

  private static final org.apache.log4j.Logger logger = MiscUtils.getLogger();

  public KaiSsoAuthenticationHelper(SigningKeyResolverService signingKeyResolverService, ProviderDao providerDao,
      ProviderPreferenceDao providerPreferenceDao, FacilityDao facilityDao) {
    this.signingKeyResolverService = signingKeyResolverService;
    this.providerDao = providerDao;
    this.providerPreferenceDao = providerPreferenceDao;
    this.facilityDao = facilityDao;
  }

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object) throws Exception {

    logger.debug("KaiSsoAuthenticationHelper.preHandle()");
    String referrer = request.getHeader("referer");
    String requestInfo = request.getRequestURI() + " from " + request.getRemoteAddr();
    if (request.getSession().getAttribute("loggedInProvider") == null || referrer == null) {

      logger.debug("loggedInProvider is null or referrer is null");

      Cookie cookie = WebUtils.getCookie(request, "kai-sso");

      if (cookie != null) {
        logger.debug("kai-sso is not null");

        String jws = cookie.getValue();

        try {
          Jws<Claims> claims = Jwts.parser().setSigningKeyResolver(signingKeyResolverService).parseClaimsJws(jws);

          Provider provider = providerDao.getProvider(claims.getHeader().getKeyId());
          String uuid = claims.getBody().getId();

          if (uuid.equals(provider.getLastUsedUuid())) {

            HttpSession session = request.getSession(false);

            session.setAttribute(SessionConstants.LOGGED_IN_PROVIDER, provider);

            SecurityDao securityDao = (SecurityDao) SpringUtils.getBean("securityDao");
            Security security = securityDao.getByProviderNo(claims.getHeader().getKeyId());

            // retrieve the oscar roles for this Provider as a comma separated list
            String rolename = null;
            SecUserRoleDao secUserRoleDao = (SecUserRoleDao) SpringUtils.getBean("secUserRoleDao");
            List<SecUserRole> roles = secUserRoleDao.getUserRoles(security.getProviderNo());
            for (SecUserRole role : roles) {
              if (rolename == null) {
                rolename = role.getRoleName();
              } else {
                rolename += "," + role.getRoleName();
              }
            }

            session.setAttribute("user", security.getProviderNo());
            session.setAttribute("userfirstname", provider.getFirstName());
            session.setAttribute("userlastname", provider.getLastName());
            session.setAttribute("userrole", rolename);
            session.setAttribute("oscar_context_path", request.getContextPath());

            String expired_days = "";
            if (security.getBExpireset() != null && security.getBExpireset().intValue() == 1) {
              // Give warning if the password will be expired in 10 days.

              long date_expireDate = security.getDateExpiredate().getTime();
              long date_now = new Date().getTime();
              long date_diff = (date_expireDate - date_now) / (24 * 3600 * 1000);

              if (security.getBExpireset().intValue() == 1 && date_diff < 11) {
                expired_days = String.valueOf(date_diff);
              }
            }

            session.setAttribute("expired_days", expired_days);

            // get preferences from preference table
            ProviderPreference providerPreference = providerPreferenceDao.find(security.getProviderNo());
            if (providerPreference == null) {
              providerPreference = new ProviderPreference();
            }
            session.setAttribute(SessionConstants.LOGGED_IN_PROVIDER_PREFERENCE, providerPreference);
            session.setAttribute(SessionConstants.LOGGED_IN_SECURITY, security);

            // current facility
            Integer facilityId = null;
            List<Integer> facilityIds = providerDao.getFacilityIds(security.getProviderNo());
            if (facilityIds.size() == 0) {
              List<Facility> facility = facilityDao.findAll(null);
              facilityId = facility.get(0).getId();
            } else {
              facilityId = facilityIds.get(0);
            }

            Facility facility = facilityDao.find(facilityId);
            session.setAttribute(SessionConstants.CURRENT_FACILITY, facility);

            return true;
          } else {
            cookie.setMaxAge(0);
            response.addCookie(cookie);

            logger.warn("The UUID in the JWT does not match any providers. ({})" + requestInfo);
            return false;
          }
        } catch (UnsupportedJwtException | MalformedJwtException | SignatureException e) {
          logger.warn("The security JWT could not be decrypted. ({})" + requestInfo);
          return false;
        }
      }

      logger.info("UNAUTHORIZED REQUEST - No logged in provider: " + requestInfo + ": ");
      return false;
    }
    return true;
  }
}
