/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.login;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.model.UserProperty;
import org.oscarehr.util.SpringUtils;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AcknowledgeLoginSwitchNotification extends Action {
  private UserPropertyDAO userPropertyDAO = SpringUtils.getBean(UserPropertyDAO.class);

  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    String providerNo = request.getParameter("providerNo") != null ? request.getParameter("providerNo") : "";

    UserProperty acknowledgedProperty = userPropertyDAO.getProp(providerNo, UserProperty.ACKNOWLEDGED_LOGIN_SWITCH_NOTIFICATION);

    if (acknowledgedProperty != null) {
      acknowledgedProperty.setValue("true");
      userPropertyDAO.saveProp(acknowledgedProperty);
    } else {
      UserProperty property = new UserProperty();
      property.setName(UserProperty.ACKNOWLEDGED_LOGIN_SWITCH_NOTIFICATION);
      property.setValue("true");
      property.setProviderNo(providerNo);
      userPropertyDAO.saveProp(property);
    }
    return (mapping.findForward("success"));
  }
}
