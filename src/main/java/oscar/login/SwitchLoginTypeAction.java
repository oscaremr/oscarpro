/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.login;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.UserProperty;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SessionConstants;
import org.oscarehr.util.SpringUtils;

public final class SwitchLoginTypeAction extends DispatchAction {

  private UserPropertyDAO propDao = (UserPropertyDAO) SpringUtils.getBean("UserPropertyDAO");
  
  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String loginType = request.getParameter("loginType");
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    Provider provider = loggedInInfo.getLoggedInProvider();
    UserProperty property = propDao.getProp(provider.getProviderNo(), UserProperty.ENHANCED_OR_CLASSIC);
    if (property == null) {
      property = new UserProperty();
      property.setName(UserProperty.ENHANCED_OR_CLASSIC);
      property.setProviderNo(provider.getProviderNo());
    }
    property.setValue(loginType);
    propDao.saveProp(property);

    loginType = StringUtils.isBlank(loginType) ? "E" : loginType;
    request.getSession().setAttribute(SessionConstants.LOGIN_TYPE, loginType);
    return (mapping.findForward("success"));
  }
}
