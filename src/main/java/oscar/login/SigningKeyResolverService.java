/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.login;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.SigningKeyResolverAdapter;
import io.jsonwebtoken.security.Keys;
import java.security.Key;
import java.util.Base64;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.model.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SigningKeyResolverService extends SigningKeyResolverAdapter {
    private final ProviderDao providerRepository;
    
    @Autowired
    public SigningKeyResolverService(ProviderDao providerRepository) {
        this.providerRepository = providerRepository;
    }
    
    @Override
    public Key resolveSigningKey(JwsHeader header, Claims claims) {
        String providerNo = header.getKeyId();

        Provider provider = providerRepository.getProvider(providerNo);

        String signingKey = provider.getSigningKey();
        return Keys.hmacShaKeyFor(Base64.getDecoder().decode(signingKey));
    }
}
