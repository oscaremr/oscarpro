/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.login;

import ca.oscarpro.common.http.OscarClassicHttpClient;
import ca.oscarpro.common.util.CertificateUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.oauth.OAuthProblemException;
import org.apache.cxf.jaxrs.ext.MessageContext;
import org.apache.cxf.jaxrs.ext.RequestHandler;
import org.apache.cxf.jaxrs.model.ClassResourceInfo;
import org.apache.cxf.message.Message;
import org.apache.cxf.rs.security.oauth.data.OAuthContext;
import org.apache.cxf.rs.security.oauth.filters.AbstractAuthFilter;
import org.apache.cxf.rs.security.oauth.filters.OAuthInfo;
import org.apache.cxf.security.SecurityContext;

@Provider
@Slf4j
public class OscarOAuthRequestFilter extends AbstractAuthFilter implements RequestHandler {

	@Context
	private MessageContext mc;

	@Context
  HttpServletRequest servletRequest;

  private static final Set<String> PRO_CLASSIC_OR_SHADOW_TRAFFIC_ENDPOINTS =
      new HashSet<>(
          Arrays.asList(
              "api/ehr",
              // Endpoint for Rx Instruction parsing comparison between
              //Oscar Classic and Oscar Pro for consistency check.
              "/rxlookup"
          )
      );
	private final List<String> LOCAL_IPS = Arrays.asList("127.0.0.1", "::1", "0:0:0:0:0:0:0:1");

	public Response handleRequest(Message m, ClassResourceInfo resourceClass) {
		try {
			OAuthInfo info = handleOAuthRequest(mc.getHttpServletRequest());
			setSecurityContext(m, info);
		} catch (OAuthProblemException e) {
			if (isRequestFromOscarPro()) {
				return null;
			}
			log.error("An error occurred while authenticating the oAuth token", e);
			return Response.status(401).header("WWW-Authenticate", "OAuth").build();
		} catch (Exception e) {
			log.error("An unexpected error occurred while authenticating the oAuth token", e);
			return Response.status(500).header("WWW-Authenticate", "OAuth").build();
		}
		return null;
	}

	/**
	 * Check if the incoming request is from OscarPro and is part of shadow traffic endpoints.
	 * This method is used to determine the origin of the request and its purpose.
	 *
	 * @return true if the request is from OscarPro and part of shadow traffic endpoints, false otherwise.
	 */
	protected boolean isRequestFromOscarPro() {
		if (!hasEhrPrefix()) {
			return false;
		}
		// Return true if the incoming request is from a local address
		if (LOCAL_IPS.contains(servletRequest.getRemoteAddr())) {
			return true;
		}
		try {
			val certificates = CertificateUtils.decodeUrlEncodedCertificate(
					servletRequest.getHeader(OscarClassicHttpClient.EHR_CERTIFICATE_HEADER_NAME)
			);
			if (certificates == null || certificates.length == 0) {
				log.error("Certificate not found in request.");
				return false;
			}
			CertificateUtils.validateCertificate(certificates[0]);
			return true;
		} catch (UnsupportedEncodingException e) {
			log.error("An error occurred while decoding the certificate", e);
		} catch (CertificateException | KeyStoreException | IOException | NoSuchAlgorithmException
						 | SignatureException | InvalidKeyException | NoSuchProviderException e) {
			log.error("An error occurred while validating the certificate", e);
		}
		return false;
	}

	/**
	 * Check if the request URI is part of shadow traffic endpoints.
	 * This method is used to determine if the request is related to shadow traffic.
	 *
	 * @return true if the request URI is part of shadow traffic endpoints, false otherwise.
	 */
  boolean hasEhrPrefix() {
    val endpoint = servletRequest.getRequestURI();
    for (String trafficEndpoint : PRO_CLASSIC_OR_SHADOW_TRAFFIC_ENDPOINTS) {
      if (endpoint.contains(trafficEndpoint)) {
        return true;
      }
    }
    return false;
  }


  private void setSecurityContext(Message m, OAuthInfo info) {
		SecurityContext sc = createSecurityContext(mc.getHttpServletRequest(), info);
		m.setContent(SecurityContext.class, sc);
		m.setContent(OAuthContext.class, createOAuthContext(info));
	}
}
