/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.login;

import ca.kai.util.SecurityUtils;
import ca.oscarpro.security.OscarPasswordService;
import ca.oscarpro.security.PasswordValidationException;
import com.quatro.model.security.LdapSecurity;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.UUID;
import javax.crypto.SecretKey;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.PMmodule.service.ProviderManager;
import org.oscarehr.PMmodule.web.OcanForm;
import org.oscarehr.PMmodule.web.utils.UserRoleUtils;
import org.oscarehr.common.dao.FacilityDao;
import org.oscarehr.common.dao.ProviderPreferenceDao;
import org.oscarehr.common.dao.SecObjPrivilegeDao;
import org.oscarehr.common.dao.SecurityDao;
import org.oscarehr.common.dao.ServiceRequestTokenDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.model.Facility;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.ProviderPreference;
import org.oscarehr.common.model.SecObjPrivilege;
import org.oscarehr.common.model.Security;
import org.oscarehr.common.model.ServiceRequestToken;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.common.model.UserProperty;
import org.oscarehr.decisionSupport.service.DSService;
import org.oscarehr.managers.AppManager;
import org.oscarehr.managers.OktaManager;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.myoscar.utils.MyOscarLoggedInInfo;
import org.oscarehr.phr.util.MyOscarUtils;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.LoggedInUserFilter;
import org.oscarehr.util.SessionConstants;
import org.oscarehr.util.SpringUtils;
import org.owasp.encoder.Encode;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import oscar.OscarProperties;
import oscar.log.LogAction;
import oscar.log.LogConst;
import oscar.oscarSecurity.CRHelper;
import oscar.util.AlertTimer;
import oscar.util.CBIUtil;
import oscar.util.ParameterActionForward;

/**
 * This class is the action class for the login page. It is responsible for authenticating the user
 */
@Slf4j
public final class LoginAction extends DispatchAction {

  /**
   * This enum represents the different types of login failures that can occur. primarily used for
   * error display purposes.
   */
  public enum LoginFailureType {
    BLOCKED,
    EXPIRED_LOGIN,
    UNKNOWN;

    public static Optional<LoginFailureType> fromString(String enumString) {
      if (EnumUtils.isValidEnumIgnoreCase(LoginFailureType.class, enumString)) {
        return Optional.of(LoginFailureType.valueOf(enumString.toUpperCase()));
      }
      return Optional.empty();
    }
  }

  /**
   * This variable is only intended to be used by this class and the jsp which sets the selected
   * facility. This variable represents the queryString key used to pass the facility ID to this
   * class.
   */
  public static final String SELECTED_FACILITY_ID = "selectedFacilityId";

  public static final String ERR_PW_ATTEMPTS =
      "Oops! Your account is now locked due to incorrect password attempts!";
  public static final String ERR_PW_EXPIRED =
      "Your account is expired. Please contact your administrator.";
  private static final String LOG_PRE = "Login!@#$: ";

    private ProviderManager providerManager = (ProviderManager) SpringUtils.getBean("providerManager");
    private AppManager appManager = SpringUtils.getBean(AppManager.class);
    private FacilityDao facilityDao = (FacilityDao) SpringUtils.getBean("facilityDao");
    private ProviderPreferenceDao providerPreferenceDao = (ProviderPreferenceDao) SpringUtils.getBean("providerPreferenceDao");
    private ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
    private UserPropertyDAO propDao =(UserPropertyDAO)SpringUtils.getBean("UserPropertyDAO");
	  private SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
	  private OktaManager oktaManager = SpringUtils.getBean(OktaManager.class);
		private ServiceRequestTokenDao serviceRequestTokenDao = SpringUtils.getBean(ServiceRequestTokenDao.class);

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	boolean ajaxResponse = request.getParameter("ajaxResponse") != null?Boolean.valueOf(request.getParameter("ajaxResponse")):false;
    	
    	String ip = LoggedInInfo.obtainClientIpAddress(request);
      Boolean isMobileOptimized = request.getSession().getAttribute("mobileOptimized") != null;
      String submitType = "";
      if(request.getParameter("submit")!=null){
			  submitType = String.valueOf(request.getParameter("submit"));
		  }

      String loginType = "E";

      LoginCheckLogin cl = new LoginCheckLogin();
      String oneIdEmail = request.getParameter("email");
	  String oneIdKey = request.getParameter("nameId");
	  String userName = "";
      String password = "";
      String pin = "";
      String nextPage= "";
      boolean forcedpasswordchange = true;
      String where = "failure";
	  boolean oneIdRecordUpdated = false;
        
    	if (request.getParameter("forcedpasswordchange") != null && request.getParameter("forcedpasswordchange").equalsIgnoreCase("true")) {
    		//Coming back from force password change.
    	    userName = (String) request.getSession().getAttribute("userName");
    	    password = (String) request.getSession().getAttribute("password");
    	    pin = (String) request.getSession().getAttribute("pin");
    	    nextPage = (String) request.getSession().getAttribute("nextPage");
    	    
    	    String newPassword = ((LoginForm) form).getNewPassword();
    	    String confirmPassword = ((LoginForm) form).getConfirmPassword();
    	    String oldPassword = ((LoginForm) form).getOldPassword();
    	    
    	    try{
        	    String errorStr = errorHandling(
                  password,
                  newPassword,
                  confirmPassword,
                  OscarPasswordService.encodePassword(oldPassword),
                  oldPassword);
        	    
        	    //Error Handling
        	    if (errorStr != null && !errorStr.isEmpty()) {
    	        	String newURL = mapping.findForward("forcepasswordreset").getPath();
    	        	newURL = newURL + errorStr;  	        	
    	            return(new ActionForward(newURL));  
        	    }
        	   
        	    persistNewPassword(userName, newPassword);
        	            	    
        	    password = newPassword;
        	            	    
        	    //Remove the attributes from session
        	    removeAttributesFromSession(request);
         	}  
         	catch (Exception e) {
         		log.error("Error", e);
            String newURL = mapping.findForward("error").getPath();
            newURL = newURL + "?errormsg=Setting values to the session.";

            //Remove the attributes from session
            removeAttributesFromSession(request);

            return(new ActionForward(newURL));
         	}

    	    //make sure this checking doesn't happen again
    	    forcedpasswordchange = false;
    	    
    	} else {
    		userName = ((LoginForm) form).getUsername();
    	    password = ((LoginForm) form).getPassword();
    	    pin = ((LoginForm) form).getPin();
    	    nextPage=request.getParameter("nextPage");

	        log.debug("nextPage: "+nextPage);
	        if (nextPage!=null) {
	        	// set current facility
	            String facilityIdString=request.getParameter(SELECTED_FACILITY_ID);
	            Facility facility=facilityDao.find(Integer.parseInt(facilityIdString));
	            request.getSession().setAttribute(SessionConstants.CURRENT_FACILITY, facility);
	            String username=(String)request.getSession().getAttribute("user");
	            LogAction.addLog(username, classicOrPro(loginType), LogConst.CON_LOGIN, "facilityId="+facilityIdString, ip);
	            if(facility.isEnableOcanForms()) {
	            	request.getSession().setAttribute("ocanWarningWindow", OcanForm.getOcanWarningMessage(facility.getId()));
	            }
	            return mapping.findForward(nextPage);
	        }
	        
	        if (cl.isBlock(ip, userName)) {
	        	log.info(LOG_PRE + " Blocked: " + userName);
	            // return mapping.findForward(where); //go to block page
	            // change to block page
	            String newURL = mapping.findForward("error").getPath();
	            newURL = newURL + "?errormsg=" + ERR_PW_ATTEMPTS +
                  "&type=" + LoginFailureType.BLOCKED;
	            
	            if(ajaxResponse) {
	            	JSONObject json = new JSONObject();
	            	json.put("success", false);
	            	json.put("error", ERR_PW_ATTEMPTS);
	            	response.setContentType("text/x-json");
	            	json.write(response.getWriter());
	            	return null;
	            }
	            
	            return(new ActionForward(newURL));
	        }
	                
	        log.debug("ip was not blocked: "+ip);
        
    	}
			ServiceRequestToken serviceRequestToken = null;
			if (request.getParameter("oauth_token") != null) {
				serviceRequestToken = 
						serviceRequestTokenDao.findByTokenId(request.getParameter("oauth_token"));
			}
			// If the login request isn't from valid REST authorization, check if they are allowed to 
			// log in through classic
			if (serviceRequestToken == null) {
				ActionForward errorAction = checkSupportUserLogin(userName, mapping);
				if (errorAction != null) {
					return errorAction;
				}
			}

				String[] strAuth;
        try {

            strAuth = cl.auth(userName, password, pin, ip);
        }
        catch (Exception e) {
        	log.error("Error", e);
            String newURL = mapping.findForward("error").getPath();
            if (e.getMessage() != null && e.getMessage().startsWith("java.lang.ClassNotFoundException")) {
                newURL = newURL + "?errormsg=Database driver " + e.getMessage().substring(e.getMessage().indexOf(':') + 2) + " not found.";
            }
            else {
                newURL = newURL + "?errormsg=Database connection error: " + e.getMessage() + ".";
            }
            
            if(ajaxResponse) {
            	JSONObject json = new JSONObject();
            	json.put("success", false);
            	json.put("error", "Database connection error:"+e.getMessage() + ".");
            	response.setContentType("text/x-json");
            	json.write(response.getWriter());
            	return null;
            }
            
            return(new ActionForward(newURL));
        }
        log.debug("strAuth : "+Arrays.toString(strAuth));
        if (strAuth != null && strAuth.length != 1) { // login successfully
        	
        	
        	
        	//is the provider record inactive?
        	ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
            Provider p = providerDao.getProvider(strAuth[0]);
            if(p == null || (p.getStatus() != null && p.getStatus().equals("0"))) {
            	log.info(LOG_PRE + " Inactive: " + userName);
            	LogAction.addLog(strAuth[0], "login", "failed", "inactive");
            	
                String newURL = mapping.findForward("error").getPath();
                newURL = newURL + "?errormsg=Your account is inactive. Please contact your administrator to activate.";
                return(new ActionForward(newURL));
            }
            
            /* 
             * This section is added for forcing the initial password change.
             */
            Security security = getSecurity(userName);
						val isPasswordValid = isPasswordValid(password);
            if ((!OscarProperties.getInstance().getBooleanProperty("mandatory_password_reset", "false")
								&& security.isForcePasswordReset() != null && security.isForcePasswordReset()
								&& forcedpasswordchange)
								|| !isPasswordValid) {
            	
            	String newURL = mapping.findForward("forcepasswordreset").getPath();
							if (!isPasswordValid) {
								request.setAttribute("errormsg", "Your password does not meet the minimum security requirements. Please update your password.");
							}
            	try{
            	   setUserInfoToSession( request, userName,  password,  pin, nextPage);
            	}  
            	catch (Exception e) {
            		log.error("Error", e);
                    newURL = mapping.findForward("error").getPath();
                    newURL = newURL + "?errormsg=Setting values to the session.";            		
            	}

                return(new ActionForward(newURL));            	
            }

            // invalidate the existing session
            HttpSession session = request.getSession(false);
            if (session != null) {
            	if(request.getParameter("invalidate_session") != null && request.getParameter("invalidate_session").equals("false")) {
            		//don't invalidate in this case..messes up authenticity of OAUTH
            	} else {
            		session.invalidate();
            	}
            }
            String logData = null;
            log.debug("checking oauth_token");
            if (serviceRequestToken != null) {
              logData = "oauth_token used"
                  + "\najaxResponse = " + ajaxResponse;
              serviceRequestToken.setProviderNo(p.getProviderNo());
              serviceRequestTokenDao.merge(serviceRequestToken);
            }
            session = request.getSession(); // Create a new session for this user
            val secObjPrivilegeDao  = (SecObjPrivilegeDao) SpringUtils.getBean(SecObjPrivilegeDao.class);
            var privileges = new ArrayList<SecObjPrivilege>();
            if (strAuth[4] != null) {
              val userRoles = strAuth[4].split(",");
              privileges = new ArrayList<>(secObjPrivilegeDao .findByRoleUserGroups(Arrays.asList(userRoles)));
            }
            if (privileges.isEmpty()) {
              session.invalidate();
              return new ActionRedirect(
                  mapping.findForward("error").getPath()
                      + "?errormsg=This user has no privileges");
            }
            // set session max interval from system prference
            SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
            SystemPreferences forceLogoutInactivePref = systemPreferencesDao.findPreferenceByName("force_logout_when_inactive");
            SystemPreferences forceLogoutInactiveTimePref = systemPreferencesDao.findPreferenceByName("force_logout_when_inactive_time");
            session.setMaxInactiveInterval((forceLogoutInactivePref != null && forceLogoutInactivePref.getValueAsBoolean() && forceLogoutInactiveTimePref != null ? Integer.parseInt(forceLogoutInactiveTimePref.getValue()) : 120) * 60);

          //If the ondIdKey parameter is not null and is not an empty string
        	if (oneIdKey != null && !oneIdKey.equals("")) {
        		String providerNumber = strAuth[0];
        		SecurityDao securityDao = (SecurityDao) SpringUtils.getBean(SecurityDao.class);
        		Security securityRecord = securityDao.getByProviderNo(providerNumber);
        		
        		if (securityRecord.getOneIdKey() == null || securityRecord.getOneIdKey().equals("")) {
        			securityRecord.setOneIdKey(oneIdKey);
        			securityRecord.setOneIdEmail(oneIdEmail);
        			securityDao.updateOneIdKey(securityRecord);
        			session.setAttribute("oneIdEmail", oneIdEmail);
					oneIdRecordUpdated = true;
        		}
        		else {
        			log.error("The account for provider number " + providerNumber + " already has a ONE ID key associated with it");
        			return mapping.findForward("error");
        		}
        	}
            
            log.debug("Assigned new session for: " + strAuth[0] + " : " + strAuth[3] + " : " + strAuth[4]);
            LogAction.addLog(strAuth[0],
                classicOrPro(loginType),
                LogConst.CON_LOGIN,
                "",
                ip,
                null,
                logData);

            // initial db setting
            Properties pvar = OscarProperties.getInstance();
            MyOscarUtils.setDeterministicallyMangledPasswordSecretKeyIntoSession(session, password);
            

            String providerNo = strAuth[0];
            session.setAttribute("userName", userName);
            session.setAttribute("user", strAuth[0]);
            session.setAttribute("userfirstname", strAuth[1]);
            session.setAttribute("userlastname", strAuth[2]);
            session.setAttribute("userrole", strAuth[4]);
            session.setAttribute("oscar_context_path", request.getContextPath());
            session.setAttribute("expired_days", strAuth[5]);
            // If a new session has been created, we must set the mobile attribute again
            if (isMobileOptimized){
            	if (submitType.equalsIgnoreCase("Sign in using Full Site")){
					session.setAttribute("fullSite","true");
				}
				else{
					session.setAttribute("mobileOptimized","true");
				}
			}

            // initiate security manager
            String default_pmm = null;
            
            
            
            // get preferences from preference table
        	ProviderPreference providerPreference=providerPreferenceDao.find(providerNo);
        	
            
                
        	if (providerPreference==null) providerPreference=new ProviderPreference();
         	
        	session.setAttribute(SessionConstants.LOGGED_IN_PROVIDER_PREFERENCE, providerPreference);
        	
            if (org.oscarehr.common.IsPropertiesOn.isCaisiEnable()) {
            	String tklerProviderNo = null;
            	UserProperty prop = propDao.getProp(providerNo, UserProperty.PROVIDER_FOR_TICKLER_WARNING);
        		if (prop == null) {
        			tklerProviderNo = providerNo;
        		} else {
        			tklerProviderNo = prop.getValue();
        		}
            	session.setAttribute("tklerProviderNo",tklerProviderNo);
            	
                session.setAttribute("newticklerwarningwindow", providerPreference.getNewTicklerWarningWindow());
                session.setAttribute("default_pmm", providerPreference.getDefaultCaisiPmm());
                session.setAttribute("caisiBillingPreferenceNotDelete", String.valueOf(providerPreference.getDefaultDoNotDeleteBilling()));
                
                default_pmm = providerPreference.getDefaultCaisiPmm();
                @SuppressWarnings("unchecked")
                ArrayList<String> newDocArr = (ArrayList<String>)request.getSession().getServletContext().getAttribute("CaseMgmtUsers");    
                if("enabled".equals(providerPreference.getDefaultNewOscarCme())) {
                	newDocArr.add(providerNo);
                	session.setAttribute("CaseMgmtUsers", newDocArr);
                }
            }
            session.setAttribute("starthour", providerPreference.getStartHour().toString());
            session.setAttribute("endhour", providerPreference.getEndHour().toString());
            session.setAttribute("everymin", providerPreference.getEveryMin().toString());
            session.setAttribute("groupno", providerPreference.getMyGroupNo());
                
            where = "provider";

            if (where.equals("provider") && default_pmm != null && "enabled".equals(default_pmm)) {
                where = "caisiPMM";
            }

            boolean useProgramLocation = systemPreferencesDao
						    .isReadBooleanPreferenceWithDefault("use_program_location_enabled", false);
            if (where.equals("provider") && useProgramLocation) {
                where = "programLocation";
            }

            String quatroShelter = OscarProperties.getInstance().getProperty("QUATRO_SHELTER");
            if(quatroShelter!= null && quatroShelter.equals("on")) {
            	where = "shelterSelection";
            }
        
            /*
             * if (OscarProperties.getInstance().isTorontoRFQ()) { where = "caisiPMM"; }
             */
            // Lazy Loads AlertTimer instance only once, will run as daemon for duration of server runtime
            if (pvar.getProperty("billregion").equals("BC")) {
                String alertFreq = pvar.getProperty("ALERT_POLL_FREQUENCY");
                if (alertFreq != null) {
                    Long longFreq = new Long(alertFreq);
                    String[] alertCodes = OscarProperties.getInstance().getProperty("CDM_ALERTS").split(",");
                    AlertTimer.getInstance(alertCodes, longFreq.longValue());
                }
            }
            CRHelper.recordLoginSuccess(userName, strAuth[0], request);

            String username = (String) session.getAttribute("user");
            Provider provider = providerManager.getProvider(username);
            session.setAttribute(SessionConstants.LOGGED_IN_PROVIDER, provider);
            session.setAttribute(SessionConstants.LOGGED_IN_SECURITY, cl.getSecurity());

            LoggedInInfo loggedInInfo = LoggedInUserFilter.generateLoggedInInfoFromSession(request);
            
            if (where.equals("provider")) {
                UserProperty drugrefProperty = propDao.getProp(UserProperty.MYDRUGREF_ID);
                if (drugrefProperty != null || appManager.isK2AUser(loggedInInfo)) {
                    DSService service =   SpringUtils.getBean(DSService.class);  
                    service.fetchGuidelinesFromServiceInBackground(loggedInInfo);
                }
            }
            
            if (MyOscarUtils.pingURL(MyOscarLoggedInInfo.getMyOscarServerBaseUrl(), 5000)) {
                MyOscarUtils.attemptMyOscarAutoLoginIfNotAlreadyLoggedIn(loggedInInfo, true);
            } else {
                log.warn("Unable to reach MyOscar server, aborting login");
            }
            
            List<Integer> facilityIds = providerDao.getFacilityIds(provider.getProviderNo());
            if (facilityIds.size() > 1) {
                return(new ActionForward("/select_facility.jsp?nextPage=" + where));
            }
            else if (facilityIds.size() == 1) {
                // set current facility
                Facility facility=facilityDao.find(facilityIds.get(0));
                request.getSession().setAttribute("currentFacility", facility);
                LogAction.addLog(strAuth[0], classicOrPro(loginType), LogConst.CON_LOGIN, "facilityId="+facilityIds.get(0), ip);
                if(facility.isEnableOcanForms()) {
                	request.getSession().setAttribute("ocanWarningWindow", OcanForm.getOcanWarningMessage(facility.getId()));
                }
                if(facility.isEnableCbiForm()) {
                	request.getSession().setAttribute("cbiReminderWindow", CBIUtil.getCbiSubmissionFailureWarningMessage(facility.getId(),provider.getProviderNo() ));
                }
            }
            else {
        		List<Facility> facilities = facilityDao.findAll(true);
        		if(facilities!=null && facilities.size()>=1) {
        			Facility fac = facilities.get(0);
        			int first_id = fac.getId();
        			ProviderDao.addProviderToFacility(providerNo, first_id);
        			Facility facility=facilityDao.find(first_id);
        			request.getSession().setAttribute("currentFacility", facility);
        			LogAction.addLog(strAuth[0], classicOrPro(loginType), LogConst.CON_LOGIN, "facilityId="+first_id, ip);
            	}
            }

			SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
			if (UserRoleUtils.hasRole(request, "Patient Intake")) {
				return mapping.findForward("patientIntake");
			}
            
            //are they using the new UI?
            UserProperty prop = propDao.getProp(provider.getProviderNo(), UserProperty.COBALT);
            if(prop != null && prop.getValue() != null && prop.getValue().equals("yes")) {
            	where="cobalt";
            }

			prop = propDao.getProp(provider.getProviderNo(), UserProperty.ENHANCED_OR_CLASSIC);
			if (prop != null) {
				loginType = prop.getValue();
			} else {
				UserProperty property = new UserProperty();
				property.setName(UserProperty.ENHANCED_OR_CLASSIC);
				property.setProviderNo(provider.getProviderNo());
				property.setValue(loginType);
				propDao.saveProp(property);
			}
			loginType = StringUtils.isBlank(loginType) ? "C" : loginType;
			session.setAttribute(SessionConstants.LOGIN_TYPE, loginType);

			// Sets up the cookie for securely accessing OSCAR Pro pages
			String uuid;
			SecretKey secretKey;
			if (provider.getSigningKey() != null) {
				uuid = provider.getLastUsedUuid();
				String encodedSecretKey = provider.getSigningKey();
				secretKey = Keys.hmacShaKeyFor(Base64.getDecoder().decode(encodedSecretKey));
			} else {
				uuid = UUID.randomUUID().toString();
				secretKey = Keys.secretKeyFor(SignatureAlgorithm.HS256);
				String encodedSecretKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());

				provider.setSigningKey(encodedSecretKey);
				provider.setLastUsedUuid(uuid);
				providerDao.updateProvider(provider);
			}

			String jws = Jwts.builder()
					.setHeaderParam(JwsHeader.KEY_ID, provider.getProviderNo())
					.setId(uuid)
					.signWith(secretKey)
					.compact();

			response.addCookie(SecurityUtils.createKaiSsoCookie(jws));

			if( pvar.getProperty("LOGINTEST","").equalsIgnoreCase("yes")) {
				String proceedURL = mapping.findForward(where).getPath();
				request.getSession().setAttribute("proceedURL", proceedURL);
				return mapping.findForward("LoginTest");
			}
        }
        // expired password
        else if (strAuth != null && strAuth.length == 1 && strAuth[0].equals("expired")) {
        	log.warn("Expired password");
            cl.updateLoginList(ip, userName);
            String newURL = mapping.findForward("error").getPath();
            newURL +=
                "?errormsg=" + ERR_PW_EXPIRED +
                "&type=" + LoginFailureType.EXPIRED_LOGIN;

            if(ajaxResponse) {
            	JSONObject json = new JSONObject();
            	json.put("success", false);
            	json.put("error", ERR_PW_EXPIRED);
            	response.setContentType("text/x-json");
            	json.write(response.getWriter());
            	return null;
            }
            
            return(new ActionForward(newURL));
        }
        else { 
        	log.debug("go to normal directory");

        	// go to normal directory
            // request.setAttribute("login", "failed");
            // LogAction.addLog(userName, "failed", LogConst.CON_LOGIN, "", ip);
            cl.updateLoginList(ip, userName);
            CRHelper.recordLoginFailure(userName, request);
            
            if(ajaxResponse) {
            	JSONObject json = new JSONObject();
            	json.put("success", false);
            	response.setContentType("text/x-json");
            	json.put("error", "Invalid Credentials");
            	json.write(response.getWriter());
            	return null;
            }
            
            ParameterActionForward forward = new ParameterActionForward(mapping.findForward(where));
            forward.addParameter("login", "failed");
            if (oneIdKey != null && !oneIdKey.equals("")) {
            	forward.addParameter("nameId", oneIdKey);
            }
            
            return forward;
        }
        
        if(ajaxResponse) {
        	log.debug("rendering ajax response");
        	Provider prov = providerDao.getProvider((String)request.getSession().getAttribute("user"));
        	JSONObject json = new JSONObject();
        	json.put("success", true);
        	json.put("providerName", Encode.forJavaScript(prov.getFormattedName()));
        	json.put("providerNo", prov.getProviderNo());
        	response.setContentType("text/x-json");
        	json.write(response.getWriter());
        	return null;
        }

		if (oneIdRecordUpdated) {
			response.sendRedirect("/" + OscarProperties.getKaiemrDeployedContext()
					+ "/#/one-id/uao/selector?force=true&forward=" + request.getContextPath() + "/provider/providercontrol.jsp");
			return null;
		}
        
    	log.debug("rendering standard response : "+where);
        return mapping.findForward(where);
    }

	private ActionForward checkSupportUserLogin(String userName, ActionMapping mapping) {
		String kaiSupportUser = systemPreferencesDao.getPreferenceValueByName("kai_username",null);

		boolean isKaiSupport = kaiSupportUser != null && kaiSupportUser.equalsIgnoreCase(userName);
		if (oktaManager.isOktaEnabled() && !isKaiSupport ) {
			String msg = "Only Support User can login with Classic.";
			log.warn(msg);
			String newURL = mapping.findForward("error").getPath();
			newURL = newURL + "?errormsg=" + msg;
			return(new ActionForward(newURL));
		}
		return null;
	}

	private String classicOrPro(String loginType) {
    	return "E".equals(loginType) ? LogConst.LOGIN_PRO : LogConst.LOGIN;
	}

	/**
     * Removes attributes from session
     * @param request
     */
    private void removeAttributesFromSession(HttpServletRequest request) {
	    request.getSession().removeAttribute("userName");
	    request.getSession().removeAttribute("password");
	    request.getSession().removeAttribute("pin");
	    request.getSession().removeAttribute("nextPage");
    }
    
    /**
     * Set user info to session
     * @param request
     * @param userName
     * @param password
     * @param pin
     * @param nextPage
     */
    private void setUserInfoToSession(
				HttpServletRequest request,
				String userName,
				String password,
				String pin,
				String nextPage) throws Exception{
    	request.getSession().setAttribute("userName", userName);
    	request.getSession().setAttribute("password",	OscarPasswordService.encodePassword(password));
    	request.getSession().setAttribute("pin", pin);
    	request.getSession().setAttribute("nextPage", nextPage);

    }
    
     /**
      * Performs the error handling
     * @param password
     * @param newPassword
     * @param confirmPassword
     * @param oldPassword
     * @return
     */
    private String errorHandling(String password, String  newPassword, String  confirmPassword, String  encodedOldPassword, String  oldPassword){
	    
    	String newURL = "";

	    if (!encodedOldPassword.equals(password)) {
     	   newURL = newURL + "?errormsg=Your old password, does NOT match the password in the system. Please enter your old password.";
     	} else if (!newPassword.equals(confirmPassword)) {
      	 newURL = newURL + "?errormsg=Your new password, does NOT match the confirmed password. Please try again.";
      } else if (!Boolean.parseBoolean(OscarProperties.getInstance().getProperty("IGNORE_PASSWORD_REQUIREMENTS")) && newPassword.equals(oldPassword)) {
      	 newURL = newURL + "?errormsg=Your new password, is the same as your old password. Please choose a new password.";
     	} else {
				try {
					OscarPasswordService.validatePassword(newPassword);
				} catch (PasswordValidationException e) {
					newURL = newURL + "?errormsg=" + e.getMessage();
				}
			}
    	    
	    return newURL;
     }

    /**
     * get the security record based on the username
     * @param username
     * @return
     */
    private Security getSecurity(String username) {

		SecurityDao securityDao = (SecurityDao) SpringUtils.getBean("securityDao");
		List<Security> results = securityDao.findByUserName(username);
		Security security = null;
		if (results.size() > 0) security = results.get(0);

		if (security == null) {
			return null;
		} else if (OscarProperties.isLdapAuthenticationEnabled()) {
			security = new LdapSecurity(security);
		}
		
		return security;
    }	
    
    
    /**
     * Persists the new password
     * @param userName
     * @param newPassword
     * @return
     */
    private void  persistNewPassword(String userName, String newPassword) throws Exception{
    
	    Security security = getSecurity(userName);
	    security.setPassword(OscarPasswordService.encodePassword(newPassword));
			OscarPasswordService.setPasswordVersion(security);
			security.setForcePasswordReset(Boolean.FALSE);
	    SecurityDao securityDao = (SecurityDao) SpringUtils.getBean("securityDao");	    
	    securityDao.saveEntity(security); 
		
    }

		private boolean isPasswordValid(final String password) {
			try {
				OscarPasswordService.validatePassword(password);
				return true;
			} catch (PasswordValidationException e) {
				return false;
			}
		}
         
	public ApplicationContext getAppContext() {
		return WebApplicationContextUtils.getWebApplicationContext(getServlet().getServletContext());
	}
}
