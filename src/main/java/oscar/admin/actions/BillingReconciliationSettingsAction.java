/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.admin.actions;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.common.model.UserProperty;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BillingReconciliationSettingsAction extends DispatchAction {

    private final String KEY_SEE_ALL = "see_all_billing_reconciliation";
    private final String KEY_SHOW_ALL = "show_all_billing_reconciliation";

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        String operation = request.getParameter("oper");
        try {
            if (operation.equalsIgnoreCase("toggleProvider")) {
                String providerNo = request.getParameter("providerNo");
                toggleProvider(providerNo);
            } else if (operation.equalsIgnoreCase("toggleShowAll")) {
                String value = request.getParameter("show");
                toggleShowAll(value);
            }
            request.setAttribute("success", true);
        } catch (Exception e) {
            MiscUtils.getLogger().error("Changing Preferences failed", e);
            request.setAttribute("success", false);
        }
        return mapping.findForward("success");
    }

    void toggleShowAll(String value) {
        SystemPreferencesDao spd = SpringUtils.getBean(SystemPreferencesDao.class);
        SystemPreferences systemPreference = spd.findPreferenceByName(KEY_SHOW_ALL);
        if (systemPreference == null) {
            SystemPreferences preferences = new SystemPreferences();
            preferences.setName(KEY_SHOW_ALL);
            preferences.setValue(value);
            spd.saveEntity(preferences);
        } else {
            systemPreference.setValue(value);
            spd.saveEntity(systemPreference);
        }
    }

    void toggleProvider(String providerNo) {
        UserPropertyDAO dao = SpringUtils.getBean(UserPropertyDAO.class);
        UserProperty userProperty = dao.getProp(providerNo, KEY_SEE_ALL);
        if (userProperty == null) {
            userProperty = new UserProperty();
            userProperty.setProviderNo(providerNo);
            userProperty.setName(KEY_SEE_ALL);
            userProperty.setValue("true");
            dao.saveProp(userProperty);
        } else {
            userProperty.setValue(userProperty.getValue().equalsIgnoreCase("true") ? "false" : "true");
            dao.saveProp(userProperty);
        }
    }
}
