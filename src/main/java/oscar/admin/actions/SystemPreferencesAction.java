/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.admin.actions;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.OscarLogDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.OscarLog;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class SystemPreferencesAction extends Action {

    Logger logger = Logger.getLogger(SystemPreferencesAction.class);
    private SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
    private OscarLogDao logDao = SpringUtils.getBean(OscarLogDao.class);
    
    public SystemPreferencesAction() { }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String providerNo = request.getParameter("providerNo");
        String systemPreferenceName = request.getParameter("systemPreferenceName");
        String systemPreferenceNewValue = request.getParameter("systemPreferenceNewValue");
        
        if (systemPreferenceName == null || systemPreferenceNewValue == null) {
            logger.warn("Attempted to create/update systemPreference, but provided preference name or value was null");
            mapping.findForward("failure");
        }
        
        SystemPreferences preference = systemPreferencesDao.findPreferenceByName(systemPreferenceName);
        if (preference == null) {
            preference = new SystemPreferences();
            preference.setName(systemPreferenceName);
        }
        String oldPreferenceValue = preference.getValue();

        preference.setValue(systemPreferenceNewValue);
        preference.setUpdateDate(new Date());
        systemPreferencesDao.saveEntity(preference);


        OscarLog log = new OscarLog();
        log.setProviderNo(providerNo);
        log.setAction("update");
        log.setContent("systemPreference");
        log.setContentId(preference.getId().toString());
        log.setIp(LoggedInInfo.obtainClientIpAddress(request));
        log.setDemographicId(null);
        log.setData("Updated system preference \"" + systemPreferenceName + "\". Old value: " + oldPreferenceValue + ", new value: " + preference.getValue());
        logDao.persist(log);

        return mapping.findForward("success");
    }
}
