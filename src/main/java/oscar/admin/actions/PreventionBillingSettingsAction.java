/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.admin.actions;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.var;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.SpringUtils;

public class PreventionBillingSettingsAction extends Action {

    private final String KEY_PREVENTION_BILLING = "prevention_billing_settings";
    SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);

    public final ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
            HttpServletResponse response) throws IOException,ServletException {
        if ("toggleAutoOpen".equalsIgnoreCase(request.getParameter("oper"))) {
                toggleShowAll(request.getParameter("show"));
        }
        request.setAttribute("success", true);
        return mapping.findForward("success");
    }

    public final void toggleShowAll(final String value) {
        var systemPreference = getOrCreateSystemPreference(KEY_PREVENTION_BILLING);
        systemPreference.setValue(value);
        systemPreferencesDao.mergeOrPersist(systemPreference);
    }

    private SystemPreferences getOrCreateSystemPreference(final String name) {
        var systemPreference = systemPreferencesDao.findPreferenceByName(name);
        return systemPreference != null ? systemPreference : new SystemPreferences(name);
    }
}
