/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
package oscar.oscarResearch.oscarDxResearch.bean;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.oscarehr.common.dao.PartialDateDao;
import org.oscarehr.common.model.PartialDate;
import org.oscarehr.util.SpringUtils;

@AllArgsConstructor
@NoArgsConstructor
public class dxResearchBean {

  private static final PartialDateDao partialDateDao =
      (PartialDateDao) SpringUtils.getBean("partialDateDao");

  @Getter @Setter Date autoSyncDate;
  @Getter @Setter boolean available;
  @Getter @Setter String description;
  @Getter @Setter String dxResearchNo;
  @Getter @Setter String dxSearchCode;
  @Getter @Setter String end_date;
  @Getter @Setter Date lastSyncedDate;
  @Getter @Setter String providerNo;
  @Getter @Setter String status;
  @Setter String start_date;                // has a custom Getter
  @Getter @Setter String type;

  public String getStart_date() {
    return partialDateDao.getDatePartial(
        start_date,
        PartialDate.DXRESEARCH,
        Integer.valueOf(dxResearchNo),
        PartialDate.DXRESEARCH_STARTDATE
    );
  }

  public boolean equals(Object o) {
    if (o instanceof dxCodeSearchBean) {
      dxCodeSearchBean bean = (dxCodeSearchBean) o;
      return (dxSearchCode.equals(bean.getDxSearchCode()) && type.equals(bean.getType()));
    } else {
      return super.equals(o);
    }
  }
}
