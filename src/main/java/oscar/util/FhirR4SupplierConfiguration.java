package oscar.util;

import health.apps.gateway.service.FhirContextSupplierForR4;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FhirR4SupplierConfiguration {
  @Bean
  public FhirContextSupplierForR4 fhirContextForR4() {
    return new FhirContextSupplierForR4();
  }
}
