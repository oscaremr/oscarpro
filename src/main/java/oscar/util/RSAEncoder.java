/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.util;

import org.apache.commons.codec.binary.Base64;
import oscar.OscarProperties;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import lombok.val;
import lombok.var;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RSAEncoder {

  public static String encryptAndEncodeUrl(String urlParams)
      throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException,
          InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
          UnsupportedEncodingException {
    log.debug("encryptAndEncodeUrl - Params: " + urlParams);
    String urlEncrypted = null;

    try {
      val encrypt = encrypt(urlParams);
      urlEncrypted = Base64.encodeBase64String(encrypt);
    } catch (Exception e) {
      log.error(e.getLocalizedMessage());
    }
    if (urlEncrypted!=null) {
      val encoded = URLEncoder.encode(urlEncrypted, "UTF-8");
      log.debug("EncodedEncryptedParams: " + encoded);
      return encoded;
    } else {
      log.debug("WithoutEncryptedParams: " + urlParams);
      return URLEncoder.encode(urlParams, "UTF-8");
    }
  }

  private static byte[] encrypt(String input)
      throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeySpecException,
      IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
    val cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
    cipher.init(Cipher.ENCRYPT_MODE, getPublicKey());
    return cipher.doFinal(input.getBytes(StandardCharsets.UTF_8));
  }

  private static PublicKey getPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
    var modulus = BigInteger.ZERO;
    var exponent = BigInteger.ZERO;
    val factory = KeyFactory.getInstance("RSA");

    if (!StringUtils.isNullOrEmpty(OscarProperties.getInstance().getProperty("harmonyRSAMod"))) {
      val mod = OscarProperties.getInstance().getProperty("harmonyRSAMod").trim();
      modulus = new BigInteger(1, Base64.decodeBase64(mod));
    }
    if (!StringUtils.isNullOrEmpty(OscarProperties.getInstance().getProperty("harmonyRSAExp"))) {
      val exp = OscarProperties.getInstance().getProperty("harmonyRSAExp").trim();
      exponent = new BigInteger(1, Base64.decodeBase64(exp));
    }
    if (modulus.toString().length() >= 512) {
      return factory.generatePublic(new RSAPublicKeySpec(modulus, exponent));
    } else {
      return null;
    }
  }
}
