/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via 
 * "GNU General Public License v2.0 - GNU Project - Free Software Foundation ".
 */

package oscar.util;
 
import java.util.List;
import java.util.Map;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.SpringUtils;

public class SystemPreferencesUtils {
  private static final SystemPreferencesDao systemPreferencesDao =
      SpringUtils.getBean(SystemPreferencesDao.class);

  public static boolean isReadBooleanPreferenceWithDefault(
          String name, boolean defaultIfNotFound) {
    return systemPreferencesDao.isReadBooleanPreferenceWithDefault(
        name, defaultIfNotFound);
  }

  public static List<SystemPreferences> findPreferencesByNames(
          List<String> names) {
    return systemPreferencesDao.findPreferencesByNames(names);
  }
  
  public static Map<String, Boolean> findByKeysAsMap(List<String> keys) {
    return systemPreferencesDao.findByKeysAsMap(keys);
  }
  
  public static SystemPreferences findPreferenceByName(String name) {
    return systemPreferencesDao.findPreferenceByName(name);
  }

  public static Map<String, SystemPreferences> findByKeysAsPreferenceMap(
      List<String> keys) {
    return systemPreferencesDao.findByKeysAsPreferenceMap(keys);
  }
  
  public static boolean isPreferenceValueEquals(
      String preferenceName, String trueValueStr) {
    return systemPreferencesDao.isPreferenceValueEquals(
        preferenceName, trueValueStr);
  }
  
  public static boolean isReadBooleanPreference(String preferenceName) {
    return systemPreferencesDao.isReadBooleanPreference(preferenceName);
  }
  
  public static void saveEntity(SystemPreferences preference) {
    systemPreferencesDao.saveEntity(preference);
  }
  
  public static void merge(SystemPreferences preference) {
    systemPreferencesDao.merge(preference);
  }
  
  public static void persist(SystemPreferences preference) {
    systemPreferencesDao.persist(preference);
  }
  
  public static void mergeOrPersist(SystemPreferences preference) {
    systemPreferencesDao.mergeOrPersist(preference);
  }
  
  public static void mergeOrPersist(String name, String value) {
    systemPreferencesDao.mergeOrPersist(name, value);
  }
  
  public static boolean isAppointmentRemindersEnabled() {
    return systemPreferencesDao.isAppointmentRemindersEnabled();
  }

  public static String getPreferenceValueByName(
      String name, String defaultValue) {
    return systemPreferencesDao.getPreferenceValueByName(name, defaultValue);
  }

  public static boolean isOneIdEnabled() {
    return systemPreferencesDao.isOneIdEnabled();
  }
  
  public static boolean isAddNewDocumentTypeShown() {
    return systemPreferencesDao.isAddNewDocumentTypeShown();
  }
  
  public static boolean isValidationOnSpecialistEnabled() {
    return systemPreferencesDao.isValidationOnSpecialistEnabled();
  }

  public static boolean isClinicalConnectEnabled() {
    return systemPreferencesDao.isClinicalConnectEnabled();
  }
  
  public static boolean isDhirEnabled() {
    return systemPreferencesDao.isDhirEnabled();
  }
  
  public static boolean isDhdrEnabled() {
    return systemPreferencesDao.isDhdrEnabled();
  }
  
  public static boolean isKioskEnabled() {
    return systemPreferencesDao.isKioskEnabled();
  }
}