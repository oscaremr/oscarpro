package oscar.util;

import java.util.List;
import org.oscarehr.common.dao.PatientLabRoutingDao;
import org.oscarehr.common.model.PatientLabRouting;

public class PatientLabRoutingUtil {
  
  private final PatientLabRoutingDao patientLabRoutingDao;
  
  public PatientLabRoutingUtil(PatientLabRoutingDao patientLabRoutingDao) {
    this.patientLabRoutingDao = patientLabRoutingDao;
  }
  
  public void routeToPatient(
      final String documentNumber,
      final String documentType,
      final String demographicNumber
  ) {
    List<PatientLabRouting> patientLabRoutingList = patientLabRoutingDao.findByLabNoAndLabType(
        Integer.parseInt(documentNumber), documentType);
    if (patientLabRoutingList == null || patientLabRoutingList.isEmpty()) {
      PatientLabRouting patientLabRouting = new PatientLabRouting();
      patientLabRouting.setDemographicNo(Integer.parseInt(demographicNumber));
      patientLabRouting.setLabNo(Integer.parseInt(documentNumber));
      patientLabRouting.setLabType("DOC");
      patientLabRoutingDao.persist(patientLabRouting);
    }
  }
}
