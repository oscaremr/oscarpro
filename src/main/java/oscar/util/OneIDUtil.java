/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.util;

import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.util.SpringUtils;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.owasp.encoder.Encode;

public class OneIDUtil {
	public static String getLoginRedirectUrl(ServletRequest request) {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request; 
		StringBuffer oscarUrl = httpServletRequest.getRequestURL();
		//Sets the length of the URL, found by subtracting the length of the servlet path from the length of the full URL, that way it only gets up to the context path
		oscarUrl.setLength(oscarUrl.length() - httpServletRequest.getServletPath().length());
		oscarUrl.append("/ssoLogin.do");
		return oscarUrl.toString();
	}
	
	public static String getLoginMessage(ServletRequest request) {
		String email = request.getParameter("email");
		
		if (email == null) {
			return Encode.forHtmlContent(StringUtils.trimToEmpty(request.getParameter("errorMessage")));
		}
		
		return "Hello " + Encode.forHtmlContent(email) + "<br>"
			+ "Please login with your OSCAR credentials to link your accounts.";
	}

	/**
	 * This function will take the oneid.idp_session_refresh_period system preference and calculate the time the
	 * session must be refreshed. If it is within 3 minutes, it will be need re-authentication.
	 *
	 * @param lastKeptActive - Last time the session was kept active
	 * @return Whether the session is within 3 minutes of expiry
	 */
	public static boolean isSessionNearExpiry(Date lastKeptActive) {
		SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
		int idpSessionRefreshPeriod;
		try {
			idpSessionRefreshPeriod =
					Integer.parseInt(
							systemPreferencesDao.getPreferenceValueByName("oneid.idp_session_refresh_period", "55"));
		} catch (NumberFormatException ignored) {
			idpSessionRefreshPeriod = 55;
		}

		Date currentDate =  new Date();
		Date refreshTime = new Date(lastKeptActive.getTime() + (TimeUnit.MINUTES.toMillis(idpSessionRefreshPeriod)));

		return currentDate.getTime() > (refreshTime.getTime() - TimeUnit.MINUTES.toMillis(3));
	}
}
