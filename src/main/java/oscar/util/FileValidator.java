/**
 * Copyright (c) 2023 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.util;

import java.io.File;
import java.io.IOException;
import lombok.val;

public class FileValidator {
    private final File baseDirectory;

    public FileValidator(String baseDirectory) throws IOException {
        this.baseDirectory = new File(baseDirectory).getCanonicalFile();
    }

    public File validateAndReturnFile(String filename) throws IOException, SecurityException {
        if (filename.contains("..") || filename.contains("/") || filename.contains("\\")) {
            throw new SecurityException("Filename contains restricted characters or sequences.");
        }

        val currentFile = new File(baseDirectory, filename).getCanonicalFile();

        if (!currentFile.getPath().startsWith(baseDirectory.getPath())) {
            throw new SecurityException("Access to file denied.");
        }

        return currentFile;
    }
}
