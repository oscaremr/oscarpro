/**
 * Copyright (c) 2006-. OSCARservice, OpenSoft System. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

package oscar.oscarBilling.ca.on.data;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.apache.struts.util.LabelValueBean;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.billing.CA.ON.dao.BillingPercLimitDao;
import org.oscarehr.billing.CA.ON.model.BillingPercLimit;
import org.oscarehr.common.dao.BillingONCHeader1Dao;
import org.oscarehr.common.dao.BillingONExtDao;
import org.oscarehr.common.dao.BillingONItemDao;
import org.oscarehr.common.dao.BillingONPaymentDao;
import org.oscarehr.common.dao.BillingOnItemPaymentDao;
import org.oscarehr.common.dao.BillingPaymentTypeDao;
import org.oscarehr.common.dao.BillingPermissionDao;
import org.oscarehr.common.dao.BillingServiceDao;
import org.oscarehr.common.dao.ClinicLocationDao;
import org.oscarehr.common.dao.CtlBillingServiceDao;
import org.oscarehr.common.model.BillingONCHeader1;
import org.oscarehr.common.model.BillingONExt;
import org.oscarehr.common.model.BillingONItem;
import org.oscarehr.common.model.BillingONPayment;
import org.oscarehr.common.model.BillingOnItemPayment;
import org.oscarehr.common.model.BillingPaymentType;
import org.oscarehr.common.model.BillingPermission;
import org.oscarehr.common.model.BillingService;
import org.oscarehr.common.model.InvoiceReportItem;
import org.oscarehr.common.model.Provider;
import org.oscarehr.util.DateRange;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.util.ConversionUtils;
import oscar.util.StringUtils;

public class JdbcBillingReviewImpl {
	private static final Logger _logger = Logger.getLogger(JdbcBillingReviewImpl.class);

	private ClinicLocationDao clinicLocationDao = (ClinicLocationDao) SpringUtils.getBean("clinicLocationDao");
	private BillingONCHeader1Dao dao = SpringUtils.getBean(BillingONCHeader1Dao.class);
	private BillingONExtDao extDao = SpringUtils.getBean(BillingONExtDao.class);
	private BillingONPaymentDao payDao = SpringUtils.getBean(BillingONPaymentDao.class);
	private BillingServiceDao serviceDao = SpringUtils.getBean(BillingServiceDao.class);
	private BillingPermissionDao billingPermissionDao = SpringUtils.getBean(BillingPermissionDao.class);
	BillingOnItemPaymentDao billOnItemPaymentDao = (BillingOnItemPaymentDao)SpringUtils.getBean(BillingOnItemPaymentDao.class);
	
	public String getCodeFee(String val, String billReferalDate) {
		String retval = null;
		BillingServiceDao dao = SpringUtils.getBean(BillingServiceDao.class);

		try {
			for (BillingService bs : dao.findByServiceCodeAndLatestDate(val, ConversionUtils.fromDateString(billReferalDate))) {
				retval = bs.getValue();

				DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
				Date serviceDate = df.parse(billReferalDate);
				if (bs.getTerminationDate().before(serviceDate)) {
					retval = "defunct";
				}
			}

		} catch (Exception e) {
			_logger.error("error", e);
		}

		return retval;
	}

	public String getPercFee(String val, String billReferalDate) {
		String retval = null;
		BillingServiceDao dao = SpringUtils.getBean(BillingServiceDao.class);
		try {
			for (BillingService bs : dao.findByServiceCodeAndLatestDate(val, ConversionUtils.fromDateString(billReferalDate))) {
				retval = bs.getPercentage();
			}
		} catch (Exception e) {
			_logger.error("error", e);
		}
		return retval;
	}

	public String[] getPercMinMaxFee(String val, String billReferalDate) {
		String[] retval = { "", "" };

		BillingPercLimitDao dao = SpringUtils.getBean(BillingPercLimitDao.class);
		try {
			for (BillingPercLimit b : dao.findByServiceCodeAndLatestDate(val, ConversionUtils.fromDateString(billReferalDate))) {
				retval[0] = b.getMin();
				retval[1] = b.getMax();
			}
		} catch (Exception e) {
			_logger.error("error", e);
		}
		return retval;
	}

	// invoice report
	public List getBill(String billType, String statusType, String providerNo,
			String startDate, String endDate, String demoNo) {
		
		return getBill(billType, statusType, providerNo, startDate, endDate, demoNo, "", "", "");	
		
	}

	// invoice report
	public List getBill(String billType, String statusType, String providerNo,
			String startDate, String endDate, String demoNo,
			String serviceCodes, String dx, String visitType){
		return getBill(null, null, billType, statusType, providerNo, startDate, endDate, demoNo, serviceCodes, dx, visitType);
	}

	public List getBill(String billingPermission, LoggedInInfo loggedInInfo, String billType, String statusType, String providerNo,
			String startDate, String endDate, String demoNo, String serviceCodes, String dx, String visitType) {
		
		List<BillingClaimHeader1Data> retval = new ArrayList<BillingClaimHeader1Data>();
		BillingClaimHeader1Data ch1Obj = null ;
		
		// For filtering invoice report based on dx code
		String temp = demoNo + " " + providerNo + " " + statusType + " "
				+ startDate + " " + endDate + " " + billType + " " + dx + " "
				+ visitType + " " + serviceCodes;
		temp = temp.trim().startsWith("and") ? temp.trim().substring(3) : temp;
		
		/*String sql = "SELECT ch1.id,ch1.pay_program,ch1.demographic_no,ch1.demographic_name,ch1.billing_date,ch1.billing_time,"
		+ "ch1.status,ch1.provider_no,ch1.provider_ohip_no,ch1.apptProvider_no,ch1.timestamp1,ch1.total,ch1.paid,ch1.clinic,"
		+ "bi.fee, bi.service_code, bi.ser_num, bi.dx, bi.id as billing_on_item_id "
		+ "FROM billing_on_item bi LEFT JOIN billing_on_cheader1 ch1 ON ch1.id=bi.ch1_id "
		+ "WHERE "
		+ temp				
		+ " ORDER BY ch1.billing_date, ch1.billing_time";
		 */		
		List<String[]> bills = dao.findBillingData(temp);
		if(bills!=null) {
			for(String[] b : bills) {
				if(loggedInInfo != null && billingPermission != null){
					String curUser_providerno = loggedInInfo.getLoggedInProviderNo();
					if(!billingPermissionDao.hasPermission(b[7], curUser_providerno, billingPermission)){
						continue;
					}
				}
				
				String prevId = null;
				String prevPaid = null;
				
				boolean bSameBillCh1 = false;
				ch1Obj = new BillingClaimHeader1Data();
				ch1Obj.setId(b[0]);
				ch1Obj.setPay_program(b[1]);
				ch1Obj.setDemographic_no(b[2]);
				ch1Obj.setDemographic_name(b[3]);
				ch1Obj.setBilling_date(b[4]);
				ch1Obj.setBilling_time(b[5]);
				ch1Obj.setStatus(b[6]);
				ch1Obj.setProvider_no(b[7]);
				ch1Obj.setProvider_ohip_no(b[8]);
				ch1Obj.setUpdate_datetime(b[9]);
				ch1Obj.setTotal(b[10]);
				//ch1Obj.setPaid(b[11]);
				ch1Obj.setClinic(b[12]);
				//ch1Obj.setTotal(b[13]);//fee is not total?
				ch1Obj.setSer_num(b[15]); //15 is service code
				ch1Obj.setService_date(b[19]); //19 is service date
				ch1Obj.setBilling_on_item_id(b[17]); //17 is dx
				
				List<BillingONExt> exts = extDao.findByBillingNoAndKey(Integer.parseInt(b[0]), "payDate");
				for(BillingONExt e : exts ) {
					if(e.getStatus()=='1') {
						ch1Obj.setSettle_date(e.getValue());
					}
				}
				
				if("PAT".equals(ch1Obj.getPay_program())){
					BigDecimal amountPaid = billOnItemPaymentDao.getAmountPaidByItemId(Integer.parseInt(b[17]));
					ch1Obj.setPaid(amountPaid.toString());					
				} else {
					if( prevId==null && prevPaid==null) {
						ch1Obj.setPaid(b[11]);
					} else if(prevId!=null && prevPaid!=null && !ch1Obj.getId().equals(prevId) ) {
						ch1Obj.setPaid(b[11]);
					} else {
						ch1Obj.setPaid("0.00");			
					}
				}
				retval.add(ch1Obj);
				
				prevId = ch1Obj.getId();
				prevPaid = b[11];
			}

		}
				
		return retval;
	}


	public List<BillingClaimHeader1Data> getBill(String[] billType, String statusType, String providerNo, String startDate, String endDate, String demoNo, String visitLocation, String paymentStartDate, String paymentEndDate, int pageNumber, int resultsPerPage) {
		return getBillWithSorting(null, null, billType,statusType,providerNo,startDate,endDate,demoNo,visitLocation,null,null, paymentStartDate,paymentEndDate,pageNumber, resultsPerPage);
	}

	public List<BillingClaimHeader1Data> getBill(String billingPermission, LoggedInInfo loggedInInfo, String[] billType, String statusType, String providerNo, String startDate, String endDate, String demoNo, String visitLocation, String paymentStartDate, String paymentEndDate, int pageNumber, int resultsPerPage) {
		return getBillWithSorting(billingPermission, loggedInInfo,billType,statusType,providerNo,startDate,endDate,demoNo,visitLocation,null,null, paymentStartDate,paymentEndDate, pageNumber, resultsPerPage);
	}
	
	// invoice report
	public List<BillingClaimHeader1Data> getBillWithSorting(String[] billType, String statusType, String providerNo, String startDate, String endDate, String demoNo, String visitLocation, String sortName, String sortOrder,  String paymentStartDate, String paymentEndDate, int pageNumber, int resultsPerPage) {
		return getBillWithSorting(null, null, billType, statusType, providerNo, startDate, endDate, demoNo, visitLocation, sortName, sortOrder, paymentStartDate, paymentEndDate, pageNumber, resultsPerPage);
	}
	
	public List<BillingClaimHeader1Data> getBillWithSorting(String billingPermission, LoggedInInfo loggedInInfo, String[] billType, String statusType, String providerNo, String startDate, String endDate, String demoNo, String visitLocation, String sortName, String sortOrder,  String paymentStartDate, String paymentEndDate, int pageNumber, int resultsPerPage) {
		List<BillingClaimHeader1Data> retval = new ArrayList<BillingClaimHeader1Data>();		
		try {
			for (BillingONCHeader1 h : dao.findByMagic(Arrays.asList(billType), statusType, providerNo, ConversionUtils.fromDateString(startDate), ConversionUtils.fromDateString(endDate), ConversionUtils.fromIntString(demoNo),visitLocation, ConversionUtils.fromDateString(paymentStartDate), ConversionUtils.fromDateString(paymentEndDate),pageNumber,resultsPerPage)) {
				if(loggedInInfo != null && billingPermission != null){
					String curUser_providerno = loggedInInfo.getLoggedInProviderNo();
					if(!billingPermissionDao.hasPermission(h.getProviderNo(), curUser_providerno, billingPermission)){
						continue;
					}
				}
				
				String prevId = null;
				String prevPaid = null;
				
				BillingClaimHeader1Data ch1Obj = new BillingClaimHeader1Data();
				ch1Obj.setId("" + h.getId());
				ch1Obj.setDemographic_no("" + h.getDemographicNo());
				ch1Obj.setDemographic_name(h.getDemographicName());
				ch1Obj.setBilling_date(ConversionUtils.toDateString(h.getBillingDate()));
				ch1Obj.setBilling_time(ConversionUtils.toDateString(h.getBillingTime()));
				ch1Obj.setStatus(h.getStatus());
				ch1Obj.setProviderNo(h.getProviderNo());
				ch1Obj.setProvider_ohip_no(h.getProviderOhipNo());
				ch1Obj.setApptProvider_no(h.getApptProviderNo());
				ch1Obj.setUpdate_datetime(ConversionUtils.toDateString(h.getTimestamp()));
				ch1Obj.setTotal(String.valueOf(h.getTotal().doubleValue()));
				ch1Obj.setTotalDeleted(h.getTotalDeleted().toString());
				ch1Obj.setPay_program(h.getPayProgram());
				ch1Obj.setPaid(String.valueOf(h.getPaid().doubleValue()));
				ch1Obj.setClinic(h.getClinic());
				for (BillingONExt b : extDao.findByBillingNoAndKey(h.getId(), "payDate")) {
					ch1Obj.setSettle_date(b.getValue());
				}
				
				ch1Obj.setFacilty_num(clinicLocationDao.searchVisitLocation(h.getFaciltyNum()));

				retval.add(ch1Obj);
			}
		} catch (Exception e) {
			_logger.error("error", e);
		}
		
		applySort(retval,sortName,sortOrder);
		return retval;
	}
	
	private void applySort(List<BillingClaimHeader1Data> retval, String sortName, String sortOrder) {
		if(sortOrder == null) {
			sortOrder = "asc";
		}
		
		if(sortName != null && "ServiceDate".equals(sortName)) {
			Collections.sort(retval, SERVICE_DATE_COMPARATOR);
		}
		if(sortName != null && "DemographicNo".equals(sortName)) {
			Collections.sort(retval, DEMOGRAPHIC_NO_COMPARATOR);
		}
		if(sortName != null && "VisitLocation".equals(sortName)) {
			Collections.sort(retval, VISIT_LOCATION_COMPARATOR);
		}
		if(sortOrder.equals("desc")) {
			Collections.reverse(retval);
		}
	}
	
	public static final Comparator<BillingClaimHeader1Data> SERVICE_DATE_COMPARATOR =new Comparator<BillingClaimHeader1Data>() {
		public int compare(BillingClaimHeader1Data arg0, BillingClaimHeader1Data arg1) {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date0=null,date1=null;
			try {
				date0 = formatter.parse(arg0.getBilling_date());
				date1 = formatter.parse(arg1.getBilling_date());
			}catch(ParseException e) {
				return 0;
			}
			return(date0.compareTo(date1));
			
		}
	};
	
	public static final Comparator<BillingClaimHeader1Data> DEMOGRAPHIC_NO_COMPARATOR =new Comparator<BillingClaimHeader1Data>() {
		public int compare(BillingClaimHeader1Data arg0, BillingClaimHeader1Data arg1) {
			Integer d0,d1;
			try {
				d0 = Integer.parseInt(arg0.getDemographic_no());
				d1 = Integer.parseInt(arg1.getDemographic_no());
			}catch(Exception e) {
				return 0;
			}
			return(d0.compareTo(d1));
			
		}
	};
	
	public static final Comparator<BillingClaimHeader1Data> VISIT_LOCATION_COMPARATOR =new Comparator<BillingClaimHeader1Data>() {
		public int compare(BillingClaimHeader1Data arg0, BillingClaimHeader1Data arg1) {
			return arg0.getFacilty_num().compareTo(arg1.getFacilty_num());
		}
	};
	

	
	public List<BillingClaimHeader1Data> getBillWithSorting(String billingPermission, LoggedInInfo loggedInInfo, String[] billType, String statusType, String providerNo, String startDate, String endDate, String demoNo, String demoName, String demoHin, List<String> serviceCodes, String accountingNumber, String claimNumber, String dx, String raCode, String visitType, String visitLocation, String sortName, String sortOrder, String paymentStartDate, String paymentEndDate, int pageNumber, int resultsPerPage, String selectedSite) {
		List<BillingClaimHeader1Data> retval = new ArrayList<BillingClaimHeader1Data>();

		try {
			String prevId = null;
			String prevPaid = null;

			BillingONCHeader1Dao dao = SpringUtils.getBean(BillingONCHeader1Dao.class);
			BillingONPaymentDao billingOnPaymentDao = SpringUtils.getBean(BillingONPaymentDao.class);
			BillingONExtDao billingOnExtDao = SpringUtils.getBean(BillingONExtDao.class);
			BillingPaymentTypeDao billingPaymentTypeDao = SpringUtils.getBean(BillingPaymentTypeDao.class);
			ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
			
			List<BillingPaymentType> allPaymentTypes = billingPaymentTypeDao.findAll();
			
			List<InvoiceReportItem> invoiceReportItems = dao.findByMagic2(Arrays.asList(billType), statusType, providerNo, ConversionUtils.fromDateString(startDate), ConversionUtils.fromDateString(endDate), ConversionUtils.fromIntString(demoNo), demoName, demoHin, serviceCodes, accountingNumber, claimNumber, dx, raCode, visitType, visitLocation, ConversionUtils.fromDateString(paymentStartDate),  ConversionUtils.fromDateString(paymentEndDate), pageNumber, resultsPerPage, selectedSite);
			BigDecimal currentRemainingPaidAmount = BigDecimal.ZERO;
			
			List<Integer> ch1IdList = new ArrayList<Integer>();
			for (InvoiceReportItem invoiceReportItem : invoiceReportItems) {
				ch1IdList.add(invoiceReportItem.getCh1Id());
			}
			
			List<BillingONPayment> billingONPaymentList = billingOnPaymentDao.find3rdPartyPayments(ch1IdList);

			List<Integer> invoiceReportItemBiIdList = new ArrayList<>();
			
			for (InvoiceReportItem invoiceReportItem : invoiceReportItems) {
				invoiceReportItemBiIdList.add(invoiceReportItem.getBiId());
			}
			
			List<BillingOnItemPayment> boipList = billOnItemPaymentDao.getAllByItemIdList(invoiceReportItemBiIdList);

            RAData raData = new RAData();
			
			for (int billingIndex = 0; billingIndex < invoiceReportItems.size(); billingIndex++) {
				InvoiceReportItem invoiceReportItem = invoiceReportItems.get(billingIndex);
				

				// If bopIds is empty then the invoice was migrated from 12
				List<BillingONPayment> bopIds = new ArrayList<BillingONPayment>();
				
				for (BillingONPayment payment : billingONPaymentList) {
					if (payment.getBillingNo().equals(invoiceReportItem.getCh1Id())) {
						bopIds.add(payment);
					}
				}

                if(loggedInInfo != null && billingPermission != null){
                    String curUser_providerno = loggedInInfo.getLoggedInProviderNo();
                    if(!billingPermissionDao.hasPermission(invoiceReportItem.getProviderNo(), curUser_providerno, billingPermission)){
                        continue;
                    }
                }
				
				BillingClaimHeader1Data ch1Obj = new BillingClaimHeader1Data();
				ch1Obj.setId(String.valueOf(invoiceReportItem.getCh1Id()));
				ch1Obj.setDemographic_no(String.valueOf(invoiceReportItem.getDemographicNo()));
				ch1Obj.setDemographic_name(invoiceReportItem.getDemographicName());
				ch1Obj.setBilling_date(ConversionUtils.toDateString(invoiceReportItem.getBillingDate()));
				ch1Obj.setBilling_time(ConversionUtils.toTimeString(invoiceReportItem.getBillingTime()));
				ch1Obj.setStatus(invoiceReportItem.getStatus());
				ch1Obj.setProviderNo(invoiceReportItem.getProviderNo());
				ch1Obj.setProvider_ohip_no(invoiceReportItem.getProviderOhipNo());
				ch1Obj.setApptProvider_no(invoiceReportItem.getApptProviderNo());
				ch1Obj.setUpdate_datetime(ConversionUtils.toTimestampString(invoiceReportItem.getTimestamp()));
				ch1Obj.setService_date(ConversionUtils.toDateString(invoiceReportItem.getServiceDate()));
				ch1Obj.setClinic(invoiceReportItem.getClinic());
				ch1Obj.setPay_program(invoiceReportItem.getPayProgram());
				ch1Obj.setTransc_id(invoiceReportItem.getServiceCode());
				
				if(invoiceReportItem.getPayProgram() != null
						&& invoiceReportItem.getPayProgram().matches("PAT|IFH")){ 
					BigDecimal amountPaid = billOnItemPaymentDao.getAmountPaidByItemId(invoiceReportItem.getBiId());
					BigDecimal amountDeleted = billOnItemPaymentDao.getAmountDeletedByItemId(invoiceReportItem.getBiId());
					ch1Obj.setPaid(amountPaid != null ? amountPaid.toString() : "0.00");
					ch1Obj.setTotalDeleted(amountDeleted != null ? amountDeleted.toString() : "0.00");
					ch1Obj.setBilling_on_item_id(invoiceReportItem.getBiId().toString());
				} else {
                  ArrayList<Hashtable<String, String>> ra =
                      new ArrayList<Hashtable<String, String>>();
                  ra = raData.getRAData(String.valueOf(invoiceReportItem.getCh1Id()));
                  if (ra.size() > 0) {
                    ch1Obj.setPaid(raData.getAmountPaid(ra, ch1Obj.getId(), ch1Obj.getTransc_id()));
                  } else {
					if( prevId==null && prevPaid==null) {
						ch1Obj.setPaid(invoiceReportItem.getPaid().toString());
					} else if(prevId!=null && prevPaid!=null && !ch1Obj.getId().equals(prevId) ) {
						ch1Obj.setPaid(invoiceReportItem.getPaid().toString());
					} else
                      ch1Obj.setPaid("0.00");
                  }
				}
				ch1Obj.setTotal(invoiceReportItem.getFee());
				ch1Obj.setRec_id(invoiceReportItem.getDx());

				retval.add(ch1Obj);
				prevId = ch1Obj.getId();
				prevPaid = invoiceReportItem.getPaid().toString();
				
				ch1Obj.setFacilty_num(clinicLocationDao.searchVisitLocation(invoiceReportItem.getFacilityNum()));
				
				Map<Integer,BigDecimal> paymentTypeTotals = new HashMap<Integer, BigDecimal>();
				for (BillingPaymentType type: allPaymentTypes){
					paymentTypeTotals.put(type.getId(), null);
				}

                if (bopIds.isEmpty() && "PAT".equals(invoiceReportItem.getPayProgram())) {
					if (billingIndex + 1 < invoiceReportItems.size()) {
						InvoiceReportItem nextCh1 = invoiceReportItems.get(billingIndex + 1);
						if (currentRemainingPaidAmount.equals(BigDecimal.ZERO)) { currentRemainingPaidAmount = invoiceReportItem.getPaid(); }
						// multiply fee by service count to fill out the paid amount
						BigDecimal fee = new BigDecimal(invoiceReportItem.getFee());
						BigDecimal serviceCount = new BigDecimal(invoiceReportItem.getServiceCount());
						BigDecimal itemTotalFee = fee.multiply(serviceCount);
						if (nextCh1.getId().equals(invoiceReportItem.getCh1Id()) && currentRemainingPaidAmount.compareTo(itemTotalFee) > 0) {
							ch1Obj.setPaid(itemTotalFee.toString());
							currentRemainingPaidAmount = currentRemainingPaidAmount.subtract(itemTotalFee);
						} else {
							ch1Obj.setPaid(currentRemainingPaidAmount.toString());
							currentRemainingPaidAmount = BigDecimal.ZERO;
						}
					} else {
						ch1Obj.setPaid(currentRemainingPaidAmount.toString());
						currentRemainingPaidAmount = BigDecimal.ZERO;
					}
					// Billing headers migrated from 12 do not have billing payments, so add paid from header to item here
					paymentTypeTotals.put(1, new BigDecimal(ch1Obj.getPaid()));
				} else { currentRemainingPaidAmount = BigDecimal.ZERO; }
				ch1Obj.setNumItems(Float.parseFloat(invoiceReportItem.getServiceCount()));
				
				for(BillingONPayment paymentObj : bopIds) {
					//because private billing changed, we'll check via paymentTypeId in billing_on_payment

					BillingOnItemPayment boip = null;
					for (BillingOnItemPayment billingOnItemPayment : boipList) {
						if (billingOnItemPayment.getBillingOnItemId() == invoiceReportItem.getBiId() && billingOnItemPayment.getBillingOnPaymentId() == paymentObj.getId()) {
							boip = billingOnItemPayment;
							break;
						}
					}
					
					if(boip == null) {
						MiscUtils.getLogger().warn("boip is null - " + paymentObj.getId() + "," + invoiceReportItem.getBiId());
						//probably means that no payment was applied to this item.
						continue;
					}

					if (paymentTypeTotals.get(paymentObj.getPaymentTypeId()) == null)
					{
						paymentTypeTotals.put(paymentObj.getPaymentTypeId(), boip.getPaid());
					}
					else
					{
						paymentTypeTotals.put(paymentObj.getPaymentTypeId(), paymentTypeTotals.get(paymentObj.getPaymentTypeId()).add(boip.getPaid()));
					}

				}

				String settledDate = null;
				if (ch1Obj.getPay_program().matches("PAT|OCF|ODS|CPP|STD|IFH")){
					for (BillingONExt b : extDao.findByBillingNoAndKey(invoiceReportItem.getCh1Id(), "payDate")) {
						if (b.getValue()!=null){
							settledDate = b.getValue();
						}
					}
					if (settledDate==null){
						for (BillingONExt b : extDao.findByBillingNoAndKey(invoiceReportItem.getCh1Id(), "payment")) {
							if (b.getValue()!=null){
								BigDecimal value = new BigDecimal(b.getValue());
								if (invoiceReportItem.getTotal().compareTo(value) < 0 || invoiceReportItem.getTotal().equals(value)){
									settledDate = b.getDateTime().toString();
								}
							}
						}
					}
				}else {
					//HCP
					settledDate = ch1Obj.getUpdate_datetime();
				}
				ch1Obj.setSettle_date(settledDate);
				
				
				ch1Obj.setPaymentTotals(paymentTypeTotals);
				
				Provider provider = providerDao.getProvider(ch1Obj.getProvider_no());
				if(provider!=null) {
					ch1Obj.setProviderName(provider.getFormattedName());
				}

			}
		} catch (Exception e) {
			_logger.error("error", e);
		}

		applySort(retval,sortName,sortOrder);
		
		return retval;
	}

	public Double getInvoiceSummary(String specialSelect, String billingPermission, LoggedInInfo loggedInInfo, String[] billType, String statusType, String providerNo, String startDate, 
										 String endDate, String demoNo, String demoName, String demoHin, List<String> serviceCodes, String accountingNumber, String claimNumber, 
										 String dx, String raCode, String visitType, String visitLocation, String sortName, String sortOrder, String paymentStartDate, String paymentEndDate, String selectedSite) {
		BillingONCHeader1Dao dao = SpringUtils.getBean(BillingONCHeader1Dao.class);
		return dao.getInvoiceSummary(specialSelect, Arrays.asList(billType), statusType, providerNo, ConversionUtils.fromDateString(startDate), ConversionUtils.fromDateString(endDate), ConversionUtils.fromIntString(demoNo), demoName, demoHin, serviceCodes, accountingNumber, claimNumber, dx, raCode, visitType, visitLocation, ConversionUtils.fromDateString(paymentStartDate),  ConversionUtils.fromDateString(paymentEndDate), selectedSite);
	}
	
	// billing page
	public List<Object> getBillingHist(String demoNo, int iPageSize, int iOffSet, DateRange dateRange)  {
		return getBillingHist(null, demoNo, iPageSize, iOffSet, dateRange, false);
	}
	public List<Object> getBillingHist(LoggedInInfo loggedInInfo, String demoNo, int iPageSize, int iOffSet, DateRange dateRange, Boolean showDeleted) {
		return getBillingHist(loggedInInfo, demoNo, iPageSize, iOffSet, null, null, dateRange, showDeleted);
	}
    
	public List<Object> getBillingHist(LoggedInInfo loggedInInfo, String demoNo, Integer iPageSize, Integer iOffSet, String providerNo, String serviceCode, DateRange dateRange, Boolean showDeleted)  {
		List<Object> retval = new ArrayList<Object>();
		int iRow = 0;

		BillingClaimHeader1Data ch1Obj = null;
		ProviderDao providerdao = (ProviderDao)SpringUtils.getBean(ProviderDao.class);

		BillingONCHeader1Dao dao = SpringUtils.getBean(BillingONCHeader1Dao.class);
		BillingONItemDao itemDao = SpringUtils.getBean(BillingONItemDao.class);

		List<BillingONCHeader1> hs = null;
		if (dateRange == null) {
			hs = dao.findByDemoNo(ConversionUtils.fromIntString(demoNo), iOffSet, iPageSize, providerNo, serviceCode, showDeleted);
		} else {
			hs = dao.findByDemoNoAndDates(ConversionUtils.fromIntString(demoNo), dateRange, iOffSet, iPageSize, providerNo, serviceCode, showDeleted);
		}

		try {
			for (BillingONCHeader1 h : hs) {
				if (loggedInInfo != null) {
					String curUser_providerno = loggedInInfo.getLoggedInProviderNo();
					if ("HCP".equals(h.getPayProgram())) {
						if (!billingPermissionDao.hasPermission(h.getProviderNo(), curUser_providerno, BillingPermission.OHIP_INVOICES)) {
							continue;
						}
					} else {
						if (!billingPermissionDao.hasPermission(h.getProviderNo(), curUser_providerno, BillingPermission.THIRD_PARTY_INVOICES)) {
							continue;
						}
					}
				}
				
				iRow++;
				if (iRow > iPageSize) {
					break;
				}
				ch1Obj = new BillingClaimHeader1Data();
				ch1Obj.setId("" + h.getId());
				ch1Obj.setBilling_date(ConversionUtils.toDateString(h.getBillingDate()));
				ch1Obj.setBilling_time(ConversionUtils.toDateString(h.getBillingTime()));
				ch1Obj.setStatus(h.getStatus());
				ch1Obj.setProviderNo(h.getProviderNo());
				ch1Obj.setProvider_ohip_no(h.getProviderOhipNo());
				ch1Obj.setApptProvider_no(h.getApptProviderNo());
				ch1Obj.setUpdate_datetime(ConversionUtils.toDateString(h.getTimestamp()));

				ch1Obj.setClinic(h.getClinic());
				ch1Obj.setAppointment_no("" + h.getAppointmentNo());
				ch1Obj.setPay_program(h.getPayProgram());
				ch1Obj.setVisittype(h.getVisitType());
				ch1Obj.setAdmission_date(ConversionUtils.toDateString(h.getAdmissionDate()));
				ch1Obj.setFacilty_num(h.getFaciltyNum());
				ch1Obj.setTotal(h.getTotal().toString());
				ch1Obj.setTotalDeleted(h.getTotalDeleted().toString());

				Provider provider = providerdao.getProvider(h.getProviderNo());
				ch1Obj.setLast_name(provider!=null?provider.getLastName():"");
				ch1Obj.setFirst_name(provider!=null?provider.getFirstName():"");


				retval.add(ch1Obj);

				List<String> dx = new ArrayList<>();
				Set<String> serviceCodeSet = new HashSet<String>();

				String strServiceDate = "";
				BigDecimal paid = new BigDecimal("0.00");
				BigDecimal refund = new BigDecimal("0.00");
				BigDecimal discount = new BigDecimal("0.00");


				for (BillingONItem i : itemDao.findByCh1IdAndStatusNotEqual(h.getId(), "D")) {
					String strService = i.getServiceCode() + " x " + i.getServiceCount() + ", ";
					if (!i.getDx().equals("") && !dx.contains(i.getDx())) {
						dx.add(i.getDx());
					}
					strServiceDate = ConversionUtils.toDateString(i.getServiceDate());

					serviceCodeSet.add(strService);
				}

				BillingItemData itObj = new BillingItemData();
				StringBuffer codeBuf = new StringBuffer();
				for (String codeStr : serviceCodeSet) {
					codeBuf.append(codeStr + ",");
				}
				if (codeBuf.length() > 0) {
					codeBuf.deleteCharAt(codeBuf.length() - 1);
				}
				itObj.setService_code(codeBuf.toString());
				
				itObj.setDx(StringUtils.join(dx, ", "));
				
				String dxCodeToBill = dx.size() == 1 ? dx.get(0) : "";
				itObj.setDxToBill(dxCodeToBill);
				itObj.setService_date(strServiceDate);

				if(h.getPayProgram().equals("PAT") || h.getPayProgram().equals("IFH")) {  //This is for 3rd party billings
					List<BillingONPayment> payment = payDao.find3rdPartyPaymentsByBillingNo(h.getId());
					itObj.setPaid(payDao.getTotalSumByBillingNoWeb(h.getId().toString()));
					itObj.setRefund(payDao.getPaymentsRefundByBillingNoWeb(h.getId().toString()));
					BigDecimal discount_total = payDao.getPaymentsDiscountByBillingNo(h.getId());
					if(discount_total == null) {
						discount_total = new BigDecimal(0);
					}
					NumberFormat currency = NumberFormat.getCurrencyInstance(Locale.US);
					itObj.setDiscount(currency.format(discount_total));
					
				} else { //This is for OHIP billings
					//itObj.setPaid(h.getPaid().toString());
					RAData raData = new RAData();
				    ArrayList raList = raData.getRADataIntern(ch1Obj.getId(), ch1Obj.getBilling_date().replaceAll("\\D", ""), ch1Obj.getProvider_ohip_no());
				    if(raList.size() > 0){
				    	String amountPaid = raData.getAmountPaid(raList);
				    	itObj.setPaid(amountPaid);
				    }
				}
				retval.add(itObj);
			}
		} catch (Exception e) {
			_logger.error("error", e);
		}

		return retval;
	}

	public List<Object> getFirstBill(String demoNo)  {
		List<Object> retval = new ArrayList<Object>();
		int iRow = 0;

		BillingClaimHeader1Data ch1Obj = null;
		ProviderDao providerdao = (ProviderDao)SpringUtils.getBean(ProviderDao.class);

		BillingONCHeader1Dao dao = SpringUtils.getBean(BillingONCHeader1Dao.class);
		BillingONItemDao itemDao = SpringUtils.getBean(BillingONItemDao.class);

		BillingONCHeader1 h = null;
		h = dao.getFirstInvoice(ConversionUtils.fromIntString(demoNo));

		try {
			if(h != null){
				ch1Obj = new BillingClaimHeader1Data();
				ch1Obj.setId("" + h.getId());
				ch1Obj.setBilling_date(ConversionUtils.toDateString(h.getBillingDate()));
				ch1Obj.setBilling_time(ConversionUtils.toDateString(h.getBillingTime()));
				ch1Obj.setStatus(h.getStatus());
				ch1Obj.setProviderNo(h.getProviderNo());
				ch1Obj.setApptProvider_no(h.getApptProviderNo());
				ch1Obj.setUpdate_datetime(ConversionUtils.toDateString(h.getTimestamp()));

				ch1Obj.setClinic(h.getClinic());
				ch1Obj.setAppointment_no("" + h.getAppointmentNo());
				ch1Obj.setPay_program(h.getPayProgram());
				ch1Obj.setVisittype(h.getVisitType());
				ch1Obj.setAdmission_date(ConversionUtils.toDateString(h.getAdmissionDate()));
				ch1Obj.setFacilty_num(h.getFaciltyNum());
				ch1Obj.setTotal(h.getTotal().toString());
				ch1Obj.setTotalDeleted(h.getTotalDeleted().toString());
				
				Provider provider = providerdao.getProvider(h.getProviderNo());
				ch1Obj.setLast_name(provider.getLastName());
				ch1Obj.setFirst_name(provider.getFirstName());

				retval.add(ch1Obj);

				String dx = "";
				Set<String> serviceCodeSet = new HashSet<String>();
			
				String strServiceDate = "";
				BigDecimal paid = new BigDecimal("0.00");
				BigDecimal refund = new BigDecimal("0.00");
				BigDecimal discount = new BigDecimal("0.00");

				for (BillingONItem i : itemDao.findByCh1IdAndStatusNotEqual(h.getId(), "D")) {
					String strService = i.getServiceCode() + " x " + i.getServiceCount() + ", ";
					dx = i.getDx();
					strServiceDate = ConversionUtils.toDateString(i.getServiceDate());
					
					serviceCodeSet.add(strService);
				}
								
				BillingItemData itObj = new BillingItemData();
				StringBuffer codeBuf = new StringBuffer();
				for (String codeStr : serviceCodeSet) {
					codeBuf.append(codeStr + ",");
				}
				if (codeBuf.length() > 0) {
					codeBuf.deleteCharAt(codeBuf.length() - 1);
				}
				itObj.setService_code(codeBuf.toString());
				itObj.setDx(dx);
				itObj.setService_date(strServiceDate);
				
				List<BillingONPayment> payment = payDao.find3rdPartyPaymentsByBillingNo(h.getId());
				itObj.setPaid(payDao.getTotalSumByBillingNoWeb(h.getId().toString()));
				itObj.setRefund(payDao.getPaymentsRefundByBillingNoWeb(h.getId().toString()));
				BigDecimal discount_total = payDao.getPaymentsDiscountByBillingNo(h.getId());
				if(discount_total == null) {
					discount_total = new BigDecimal(0);
				}
				NumberFormat currency = NumberFormat.getCurrencyInstance(Locale.US);		        
				itObj.setDiscount(currency.format(discount_total));
				
				retval.add(itObj);
			}
		} catch (Exception e) {
			_logger.error("error", e);
		}

		return retval;
	}

	public List<LabelValueBean> listBillingForms() {
		List<LabelValueBean> res = new ArrayList<LabelValueBean>();

		CtlBillingServiceDao dao = SpringUtils.getBean(CtlBillingServiceDao.class);
		try {
			for (Object[] o : dao.findServiceTypes()) {
				String servicetype = String.valueOf(o[0]);
				String servicetypeName = String.valueOf(o[1]);
				res.add(new LabelValueBean(servicetypeName, servicetype));
			}
		} catch (Exception ex) {
			_logger.error("Error getting billing forms list", ex);
		}
		return res;
	}

	public List<String> mergeServiceCodes(String serviceCodes, String billingForm) {
		
		List<String> serviceCodeList = null;		
		if( (serviceCodes != null && serviceCodes.length() > 0) ||  (billingForm != null && billingForm.length() > 0)){
			serviceCodeList = new ArrayList<String>();
		}
		
		if (serviceCodes != null && serviceCodes.length() > 0) {
			String[] serviceArray = serviceCodes.split(",");
			for (int i = 0; i < serviceArray.length; i++) {
				serviceCodeList.add(serviceArray[i].trim());
			}
		}
		
		if (billingForm != null && billingForm.length() > 0) {
			CtlBillingServiceDao dao = SpringUtils.getBean(CtlBillingServiceDao.class);
			for(Object code : dao.findServiceCodesByType(billingForm)) {
					serviceCodeList.add(code.toString());
			}		
		}
		
		return serviceCodeList;
	}

	// billing edit page
	public List<Object> getBillingByApptNo(String apptNo)  {
		List<Object> retval = new ArrayList<Object>();
		
		BillingClaimHeader1Data ch1Obj = null;

		BillingONCHeader1Dao dao = SpringUtils.getBean(BillingONCHeader1.class);
		BillingONItemDao itemDao = SpringUtils.getBean(BillingONItemDao.class);
		
		try {
			for(BillingONCHeader1 h : dao.findByAppointmentNo(ConversionUtils.fromIntString(apptNo))) {
				ch1Obj = new BillingClaimHeader1Data();
				ch1Obj.setId("" + h.getId());
				ch1Obj.setBilling_date(ConversionUtils.toDateString(h.getBillingDate()));
				ch1Obj.setBilling_time(ConversionUtils.toTimeString(h.getBillingTime()));
				ch1Obj.setStatus(h.getStatus());
				ch1Obj.setProviderNo(h.getProviderNo());
				ch1Obj.setAppointment_no("" + h.getAppointmentNo());
				ch1Obj.setApptProvider_no(h.getApptProviderNo());
				ch1Obj.setAsstProvider_no(h.getAsstProviderNo());
				ch1Obj.setMan_review(h.getManReview());
				ch1Obj.setUpdate_datetime(ConversionUtils.toTimestampString(h.getTimestamp()));
				ch1Obj.setClinic(h.getClinic());
				ch1Obj.setPay_program(h.getPayProgram());
				ch1Obj.setVisittype(h.getVisitType());
				ch1Obj.setAdmission_date(ConversionUtils.toDateString(h.getAdmissionDate()));
				ch1Obj.setFacilty_num(h.getFaciltyNum());
				ch1Obj.setHin(h.getHin());
				ch1Obj.setVer(h.getVer());
				ch1Obj.setProvince(h.getProvince());
				ch1Obj.setDob(h.getDob());
				ch1Obj.setDemographic_name(h.getDemographicName());
				ch1Obj.setDemographic_no("" + h.getDemographicNo());
				ch1Obj.setTotal(String.valueOf(h.getTotal().doubleValue()));
				ch1Obj.setTotalDeleted(h.getTotalDeleted().toString());
				retval.add(ch1Obj);
				
				String dx = null;
				String dx1 = null;
				String dx2 = null;
				String strService = null;
				String strServiceDate = null;

				for(BillingONItem i : itemDao.findByCh1Id(h.getId())) {
					strService += i.getServiceCode() + " x " + i.getServiceCount() + ", ";
					dx = i.getDx();
					strServiceDate = ConversionUtils.toDateString(i.getServiceDate());
					dx1 = i.getDx1();
					dx2 = i.getDx2();
				}
				
				BillingItemData itObj = new BillingItemData();
				itObj.setService_code(strService);
				itObj.setDx(dx);
				itObj.setDx1(dx1);
				itObj.setDx2(dx2);
				itObj.setService_date(strServiceDate);
				retval.add(itObj);

			}
		} catch (Exception e) {
			_logger.error("error", e);
		}

		return retval;
	}
	
	
	public String getCodeDescription(String val, String billReferalDate){
		return serviceDao.getCodeDescription(val, billReferalDate);
		
	}

}
