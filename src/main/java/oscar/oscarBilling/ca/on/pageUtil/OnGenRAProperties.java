/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.oscarBilling.ca.on.pageUtil;

import org.oscarehr.common.dao.PropertyDao;
import org.oscarehr.common.model.Property;
import org.oscarehr.util.SpringUtils;
import oscar.oscarBilling.ca.on.data.JdbcBillingRAImpl;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class OnGenRAProperties {

    private OnGenRAProperties(){};

    public static List<Properties> preparePropertiesForEnabledProviders(JdbcBillingRAImpl dbObj,
            List<Properties> propertiesList, String currentProvider) {

        String key_see_all = "see_all_billing_reconciliation";

        PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
        List<Property> providerProperty = propertyDao.findByNameAndProvider(key_see_all, currentProvider);

        Property providerSeeAll = new Property();
        if (providerProperty.size() != 0) {
            for (Property seeAll : providerProperty) {
                providerSeeAll = seeAll;
                break;
            }
            if (providerSeeAll.getValue().equals("true")) {
                propertiesList = dbObj.getAllRahd("D");
            } else {
                propertiesList = dbObj.getCurrentUserRahd("D", Arrays.asList(currentProvider));
            }
        } else {
            propertiesList = dbObj.getCurrentUserRahd("D", Arrays.asList(currentProvider));
        }

        return propertiesList;
    }
}
