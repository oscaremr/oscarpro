package oscar.oscarBilling.ca.bc.pageUtil;

import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.dao.PropertyDao;
import org.oscarehr.common.model.Property;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;

import java.util.List;

public class VisitTypeWrapper {
    
    public static String getEffectiveVisitType(String providerNo) {
        PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);

        String defaultServiceLocation = "";
        List<Property> defaultServiceLocationPropertyList = propertyDao.findByNameAndProvider("bc_default_service_location", providerNo);
        // Check the per-provider default service location first and if its set, use that
        if (!defaultServiceLocationPropertyList.isEmpty()) {
            defaultServiceLocation = StringUtils.isBlank(defaultServiceLocationPropertyList.get(0).getValue()) ? "" : defaultServiceLocationPropertyList.get(0).getValue();
        }
        
        if (StringUtils.isBlank(defaultServiceLocation) || defaultServiceLocation.equalsIgnoreCase("clinicdefault")) {
            // If no per-provider default service location (or using the clinic default), check global setting
            List<Property> globalDefaultServiceLocation = propertyDao.findGlobalByName("bc_default_service_location");
            if (!globalDefaultServiceLocation.isEmpty()) {
                for (Property p : globalDefaultServiceLocation) {
                    if (p.getValue() != null && p.getValue() != "") {
                        defaultServiceLocation = p.getValue();
                    }
                }
            } else {
                // If no global setting, check the oscar.properties
                String visitType = OscarProperties.getInstance().getProperty("visittype");
                // If the visittype contains a pipe separator, get the value before the pipe separator Eg. A|Practitioner's Office - In Community
                defaultServiceLocation = visitType != null && visitType.contains("|") ? visitType.split("\\|")[0] : visitType;
            }
        }
        
        return defaultServiceLocation;
    }
}
