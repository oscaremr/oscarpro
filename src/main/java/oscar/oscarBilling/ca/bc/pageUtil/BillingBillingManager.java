/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package oscar.oscarBilling.ca.bc.pageUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.oscarehr.common.dao.BillingDao;
import org.oscarehr.common.dao.BillingServiceDao;
import org.oscarehr.common.model.Billing;
import org.oscarehr.common.model.BillingService;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

import oscar.entities.Billingmaster;
import oscar.entities.WCB;
import oscar.oscarBilling.ca.bc.data.BillingmasterDAO;
import oscar.oscarBilling.ca.shared.administration.GstControlAction;
import oscar.util.ConversionUtils;
import oscar.util.StringUtils;


public class BillingBillingManager {
	private String billTtype;
	private String gstPercent = (new GstControlAction()).readDatabase().getProperty("gstPercent", "");

	public ArrayList<BillingItem> getBillView(String billing_no) {
		ArrayList<BillingItem> billingItemsArray = new ArrayList<BillingItem>();

		BillingDao dao = SpringUtils.getBean(BillingDao.class);

		///is msp or wcb?
		String billingType = "";
		Billing billing = dao.find(ConversionUtils.fromIntString(billing_no));
		if (billing != null) {
			billingType = billing.getBillingtype();
		}

		BillingmasterDAO masterDao = SpringUtils.getBean(BillingmasterDAO.class);
		List<Billingmaster> masters = masterDao.getBillingmasterByBillingNo(ConversionUtils.fromIntString(billing_no));

		String billingCode = "";
		String billingUnit = "";

		for (Billingmaster m : masters) {

			if (billingType.equals("WCB")) {
				WCB wcb = masterDao.getWcbByBillingNo(ConversionUtils.fromIntString(billing_no));

				if (wcb != null) {
					billingCode = wcb.getW_feeitem();
					billingUnit = "1";
				}
			} 
			if (billingCode.equals("")) {
				billingCode = m.getBillingCode();
				billingUnit = m.getBillingUnit();
			}
			String dx1 = m.getDxCode1();
			String dx2 = m.getDxCode2();
			String dx3 = m.getDxCode3();

			BillingItem billingItem = new BillingItem(billingCode, billingUnit, dx1, dx2, dx3);
			double gst = m.getGstAmountAsDouble();
			billingItem.fill(this.billTtype, gst/(m.getBillAmountAsDouble() - gst) * 100);
			billingItem.lineTotal = m.getBillAmountAsDouble();
			billingItem.gstFlag = m.getGstAmountAsDouble() > 0;
			if (billingItem.units != 0) {
				billingItem.price = ((billingItem.lineTotal - gst) / billingItem.units);
			} else
				billingItem.price = 0.00;
			billingItem.setLineNo(m.getBillingmasterNo());

			billingItemsArray.add(billingItem);
		}

		return billingItemsArray;
	}

	private BillingItem isBilled(ArrayList<BillingItem> ar, String code) {
		for (int i = 0; i < ar.size(); i++) {
			BillingItem bi = ar.get(i);
			if (bi.service_code.equals(code)) {
				return bi;
			}
		}
		return null;
	}

	// This method replaces getDups2 and generates the list of BillingItems from the billingRows represented in a HashMap for easy lookup
	public ArrayList<BillingItem> getBillingItemListFromHashMap(HashMap billingRequestList, int billingCount) {
		ArrayList<BillingItem> billingItemsArray = new ArrayList<>();
		
		String defaultDx = "";
		String lastKnownDx = "";
		for (int i = 1; i <= billingCount; i++){
			String feeCode = (String)billingRequestList.get("billing_" + i + "_fee");
			String unit = (String)billingRequestList.get("billing_" + i + "_fee_unit");
			String dx1 = (String)billingRequestList.get("billing_" + i + "_fee_dx1");
			String dx2 = (String)billingRequestList.get("billing_" + i + "_fee_dx2");
			String dx3 = (String)billingRequestList.get("billing_" + i + "_fee_dx3");

			feeCode = feeCode == null ? "" : feeCode;
			dx1 = dx1 == null ? "" : dx1;
			dx2 = dx2 == null ? "" : dx2;
			dx3 = dx3 == null ? "" : dx3;

			// Get the first good dx1 for use as a default
			if (defaultDx.isEmpty() && !dx1.isEmpty()) {
				defaultDx = dx1;
			}
			// Get the most recent non-blank dx1 in the list and use it for the next blank dx1
			if (!feeCode.isEmpty() && !dx1.isEmpty()) {
				lastKnownDx = dx1;
			} else if (dx1.isEmpty() && !lastKnownDx.isEmpty()) {
				dx1 = lastKnownDx;
			}
			
			// Default unit to 1 if nothing set
			unit = StringUtils.isNullOrEmpty(unit) ? "1" : unit;

			if (feeCode != "") {
				addBillItem(billingItemsArray, feeCode, unit, dx1, dx2, dx3);
			}
		}
		
		// Any other blank dx1's set to the defaultDx
		if (!defaultDx.isEmpty()) {
			for (BillingItem billingItem : billingItemsArray) {
				if (billingItem.getDx1().isEmpty()) {
					billingItem.dx1 = defaultDx;
				}
			}
		}
		
		return billingItemsArray;
	}
	
	public ArrayList<BillingItem> getBillingItemListSingle(String feeCode, String unit, String dx1, String dx2, String dx3) {
		ArrayList<BillingItem> billingItemsArray = new ArrayList<>();
		
		if (feeCode != null && feeCode != "") {
			dx1 = dx1 == null ? "" : dx1;
			dx2 = dx2 == null ? "" : dx2;
			dx3 = dx3 == null ? "" : dx3;

			// Default unit to 1 if nothing set
			unit = StringUtils.isNullOrEmpty(unit) ? "1" : unit;

			addBillItem(billingItemsArray, feeCode, unit, dx1, dx2, dx3);
		}
		return billingItemsArray;
	}

	private BillingItem addBillItem(ArrayList<BillingItem> ar, String code, String serviceUnits, String dx1, String dx2, String dx3) {
		boolean newCode = true;
		BillingItem bi = null;
		for (int i = 0; i < ar.size(); i++) {
			bi = ar.get(i);
			if (bi.service_code.equals(code)) {
				newCode = false;
				bi.addUnits(serviceUnits);
				i = ar.size();
			}
		}
		if (newCode) {
			bi = new BillingItem(code, serviceUnits, dx1, dx2, dx3);
			bi.fill(this.billTtype);
			ar.add(bi);
		}
		return bi;
	}

	public String getGrandTotal(ArrayList<BillingItem> ar) {
		double grandtotal = 0.00;
		for (int i = 0; i < ar.size(); i++) {
			BillingItem bi = ar.get(i);
			grandtotal += bi.getLineTotal();
			MiscUtils.getLogger().debug("total:" + grandtotal);
		}
		BigDecimal bdFee = new BigDecimal(grandtotal).setScale(2, BigDecimal.ROUND_HALF_UP);
		return bdFee.toString();

	}

	public class BillingItem {

		String service_code;
		String description;
		double price;
		double percentage;
		double units;
		double lineTotal;
		boolean gstFlag = false;
		double gstTotal = 0;
		int lineNo;
		String dx1;
		String dx2;
		String dx3;

		public BillingItem(String service_code, String description, String price1, String percentage1, double units1, String dx1, String dx2, String dx3) {
			this.service_code = service_code;
			this.description = description;
			this.price = Double.parseDouble(price1);
			this.percentage = Double.parseDouble(percentage1);
			this.units = units1;
			this.dx1 = dx1;
			this.dx2 = dx2;
			this.dx3 = dx3;
		}

		public BillingItem(String service_code, String units1, String dx1, String dx2, String dx3) {
			this.service_code = service_code;
			this.units = Double.parseDouble(units1);
			this.dx1 = dx1;
			this.dx2 = dx2;
			this.dx3 = dx3;
		}
		public BillingItem(String service_code, String units1, double gst, String dx1, String dx2, String dx3) {
			this.service_code = service_code;
			this.units = Double.parseDouble(units1);
			this.gstTotal = gst;
			this.dx1 = dx1;
			this.dx2 = dx2;
			this.dx3 = dx3;
		}

		public BillingItem(String service_code, int units1, String dx1, String dx2, String dx3) {
			this.service_code = service_code;
			this.units = units1;
			this.dx1 = dx1;
			this.dx2 = dx2;
			this.dx3 = dx3;
		}

		public void addUnits(int num) {
			units += num;
		}

		public void addUnits(String num) {
			units += (num.compareTo("") != 0) ? Double.parseDouble(num) : 0;
		}

		public String getServiceCode() {
			return service_code;
		}

		public String getDescription() {
			return description;
		}

		public double getUnit() {
			return units;
		}

		public String getDispPrice() {
			BigDecimal bdFee = new BigDecimal(price).setScale(2,
			//BigDecimal.ROUND_HALF_UP);
			        RoundingMode.HALF_UP);
			MiscUtils.getLogger().debug("price" + price + " fee" + bdFee.toString());
			return bdFee.toString();
		}

		public double getPrice() {
			return price;
		}

		public double getPercentage() {
			return percentage;
		}

		public void setLineNo(int lineNo) {
			this.lineNo = lineNo;
		}

		public int getLineNo() {
			return this.lineNo;
		}

		public void fill(String billType) {
			fill(billType, null);
		}
		
		public void fill(String billType, Double percentage) {
			BillingServiceDao dao = SpringUtils.getBean(BillingServiceDao.class);
			List<BillingService> bss = null;
			//make sure to load private fee if required,but default to MSP fee if Private fee unavailable
			if ("pri".equalsIgnoreCase(billType)) {
				bss = dao.findByServiceCodes(Arrays.asList(new String[] { service_code, "A" + service_code }));
			} else {
				bss = dao.findByServiceCode(service_code);
			}

			if(bss.size()> 0) {
				BillingService bs  = bss.get(0);
				this.description = bs.getDescription();
				this.price = Double.parseDouble(bs.getValue());

				try {
					this.gstFlag = bs.getGstFlag() && (StringUtils.filled(gstPercent) || percentage != null);
					if (this.gstFlag) {
						this.percentage = percentage != null? percentage : Double.parseDouble(gstPercent);
					} else if (StringUtils.filled(bs.getPercentage())) {
						this.percentage = Double.parseDouble(bs.getPercentage());
					} else {
						this.percentage = 100.00;
					}
				} catch (NumberFormatException eNum) {
					this.percentage = 100;
				}
			}
		}

		public double getLineTotal() {

			this.lineTotal = price * units;
			if (percentage < 100 && this.gstFlag) {
				this.gstTotal = (this.lineTotal * percentage/100);
				this.lineTotal += this.gstTotal;
			}
			return lineTotal;
		}
		
		public boolean isGstFlag() {
			return gstFlag;
		}

		public double getGstTotal() {
			return gstTotal;
		}
		
		public String getDispGstTotal() {
			BigDecimal bdFee = new BigDecimal("" + gstTotal).setScale(2, RoundingMode.HALF_UP);
			return bdFee.toString();
		}

		public String getDispLineTotal() {
			BigDecimal bdFee = new BigDecimal("" + lineTotal).setScale(2, RoundingMode.HALF_UP);
			return bdFee.toString();
		}

		public String getDx1() {
			return dx1;
		}

		public String getDx2() {
			return dx2;
		}

		public String getDx3() {
			return dx3;
		}
	}

	public void setBillTtype(String billTtype) {
		this.billTtype = billTtype;
	}

	public String getBillTtype() {
		return billTtype;
	}
}
