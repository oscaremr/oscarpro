/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarBilling.ca.bc.Teleplan;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.oscarehr.billing.CA.BC.dao.TeleplanC12Dao;
import org.oscarehr.billing.CA.BC.dao.TeleplanS00Dao;
import org.oscarehr.billing.CA.BC.dao.TeleplanS21Dao;
import org.oscarehr.billing.CA.BC.dao.TeleplanS22Dao;
import org.oscarehr.billing.CA.BC.dao.TeleplanS23Dao;
import org.oscarehr.billing.CA.BC.dao.TeleplanS25Dao;
import org.oscarehr.billing.CA.BC.dao.TeleplanVRCDao;
import org.oscarehr.billing.CA.BC.model.TeleplanC12;
import org.oscarehr.billing.CA.BC.model.TeleplanS00;
import org.oscarehr.billing.CA.BC.model.TeleplanS21;
import org.oscarehr.billing.CA.BC.model.TeleplanS22;
import org.oscarehr.billing.CA.BC.model.TeleplanS23;
import org.oscarehr.billing.CA.BC.model.TeleplanS25;
import org.oscarehr.billing.CA.BC.model.TeleplanVRC;
import org.oscarehr.util.PathUtils;
import org.oscarehr.util.SpringUtils;
import org.springframework.stereotype.Service;
import oscar.OscarProperties;
import oscar.oscarBilling.ca.bc.MSP.MSPReconcile;

/**
 *
 * @author jay
 */
@Service
@Slf4j
public class TeleplanService {
  private TeleplanS00Dao s00Dao;
  private TeleplanS21Dao s21Dao;
  private TeleplanS22Dao s22Dao;
  private TeleplanS23Dao s23Dao;
  private TeleplanS25Dao s25Dao;
  private TeleplanC12Dao c12Dao;
  private TeleplanVRCDao vrcDao;

  /** Creates a new instance of TeleplanService */
  public TeleplanService() {
      s00Dao = SpringUtils.getBean(TeleplanS00Dao.class);
      s21Dao = SpringUtils.getBean(TeleplanS21Dao.class);
      s22Dao = SpringUtils.getBean(TeleplanS22Dao.class);
      s23Dao = SpringUtils.getBean(TeleplanS23Dao.class);
      s25Dao = SpringUtils.getBean(TeleplanS25Dao.class);
      c12Dao = SpringUtils.getBean(TeleplanC12Dao.class);
      vrcDao = SpringUtils.getBean(TeleplanVRCDao.class);
  }

  /** This constructor is for testing purposes only */
  public TeleplanService(
      final TeleplanS00Dao s00Dao,
      final TeleplanS21Dao s21Dao,
      final TeleplanS22Dao s22Dao,
      final TeleplanS23Dao s23Dao,
      final TeleplanS25Dao s25Dao,
      final TeleplanC12Dao c12Dao,
      final TeleplanVRCDao vrcDao) {
    this.s00Dao = s00Dao;
    this.s21Dao = s21Dao;
    this.s22Dao = s22Dao;
    this.s23Dao = s23Dao;
    this.s25Dao = s25Dao;
    this.c12Dao = c12Dao;
    this.vrcDao = vrcDao;
  }

  public TeleplanAPI getTeleplanAPI(final String username, final String password) throws Exception {
    TeleplanAPI tAPI = new TeleplanAPI();
    TeleplanResponse tr = tAPI.login(username, password);

    if (tr != null && tr.getResult().equals("SUCCESS")) {
      return tAPI;
    }
    //TODO: ALSO RESULT COULD BE   EXPIRED.PASSWORD   need some kind of trigger that will propmt user to change password

    throw new Exception(tr.getMsgs());
  }

  public static int findExpectedSequenceNumber(final String errormsg) throws Exception {
    //TETA-022 SEQ NUMBER ERROR. EXPECTED: 0262113 LAST COMMITTED:0262112
    log.debug("WORKING FROM ERROR MSG " + errormsg);
    int i = errormsg.lastIndexOf("COMMITTED:");
    if (i == -1) {
      throw new Exception("Unexpected message " + errormsg);
    }
    String numStr = errormsg.substring(i + 10);
    return Integer.parseInt(numStr);
  }



  //ATTEMPT to get the latest sequence number from teleplan.
  //Creates a one-line (just the header )submission file with the last possible sequence number in it.
  //If it submits successfully the next sequence # is 1 (return 0 so that the incrementing program will roll to one)
  //More than likely though this will be a failure and it will parse out the last committed number
  public int getSequenceNumber(final TeleplanAPI tAPI, final String datacenter) throws Exception {

    String e = "VS1" + datacenter + "9999999V6242OSCAR_MCMASTER           V1.1      20030930OSCAR MCMASTER                          (905) 575-1300                                                                                   ";

    File getSequenceFile =  File.createTempFile("oscarseq", "fil");
    BufferedWriter out = new BufferedWriter(new FileWriter(getSequenceFile));
    out.write(e);
    out.close();

    TeleplanResponse tr = tAPI.putMSPFile(getSequenceFile);
    getSequenceFile.delete();
    log.debug(tr.toString());

    if (tr.isSuccess()) {
      return 0;  // what are the chances!
    }
    return findExpectedSequenceNumber(tr.getMsgs());
  }

  public void changePassword(final TeleplanAPI tAPI, final String oldpassword, final String password) {
    TeleplanResponse chgPasswordResp = tAPI.changePassword(
        "ttuv6242",
        "prkjg07",
        "jgprk07",
        "jgprk07");

    log.debug("RESULT " + chgPasswordResp.getResult());
    log.debug(chgPasswordResp.toString());
    tAPI.logoff();
  }

  /**
   * Method to be called by the TeleplanService to parse the remittance file. Pre-parses a list of VRC entries to split
   * the S21 lines in the remittance file.
   * @param filename The name of the remittance file
   * @return The remittance parser data
   * @throws IOException
   */
  public TeleplanRemittanceParserData parseRemittanceFile(final String filename) throws IOException {
    String filepath = OscarProperties.getInstance().getProperty("DOCUMENT_DIR");
    if (!filepath.endsWith("/") && !filename.startsWith("/")) {
      filepath = PathUtils.addTrailingSlash(filepath);
    }
    FileInputStream file = new FileInputStream(filepath + filename);
    BufferedReader input = new BufferedReader(new InputStreamReader(file));

    // Parse out VRC entries, then go back to the start of the file
    List<TeleplanVRC> parsedVrcList = preParseVrcEntries(input, filename);
    file.getChannel().position(0);
    input = new BufferedReader(new InputStreamReader(file));

    return parseEntriesForRemittance(input, filename, parsedVrcList);
  }

  /**
   * Main logic for parsing remittance files. Checks the line header and calls a parsing method accordingly.
   * @param input The buffered reader for the remittance file
   * @param filename The name of the remittance file
   * @param parsedVrcList The list of parsed VRC entries
   * @return The remittance parser data
   * @throws IOException
   */
  protected TeleplanRemittanceParserData parseEntriesForRemittance(
      final BufferedReader input,
      final String filename,
      final List<TeleplanVRC> parsedVrcList) throws IOException {
    int currentVrcIndex = 0;
    String header;
    String nextline;
    TeleplanRemittanceParserData parserData = createParserData(filename);
    Integer currentVrcId =
        parsedVrcList.isEmpty() ? null : parsedVrcList.get(currentVrcIndex).getId();

    while ((nextline = input.readLine()) != null) {
      header = nextline.substring(0, 3);
      if (header.equals("VRC") && currentVrcIndex < parsedVrcList.size() - 1) {
        currentVrcIndex++;
        currentVrcId = parsedVrcList.get(currentVrcIndex).getId();
      } else if (header.equals("S21")) {
        parserData = parseFromS21Line(currentVrcId, nextline, parserData);
      } else if (header.equals("S01")) {
        parserData = parseFromS01Line(nextline, parserData);
      } else if (header.equals("S02") || header.equals("S00") || header.equals("S03")) {
        parserData = parseFromS02Line(header, nextline, parserData);
      } else if (header.equals("S04")) {
        parserData = parseFromS04Line(nextline, parserData);
      } else if (header.equals("S23") || header.equals("S24")) {
        parseFromS23Line(nextline, parserData);
      } else if (header.equals("S25")) {
        parseFromS25Line(nextline, parserData);
      } else if (header.equals("S22")) {
        parseFromS22Line(nextline, parserData);
      } else if (header.equals("C12")) {
        parserData = parseFromC12Line(nextline, parserData);
      }
    }
    return parserData;
  }

  /**
   * Parses the VRC entries from the remittance file.
   * @param input The buffered reader for the remittance file
   * @param filename The name of the remittance file
   * @return The list of parsed VRC entries
   * @throws IOException
   */
  protected List<TeleplanVRC> preParseVrcEntries(
      final BufferedReader input, final String filename) throws IOException {
    List<TeleplanVRC> vrcList = new ArrayList<>();
    String header;
    String nextline;
    while ((nextline = input.readLine()) != null) {
      header = nextline.substring(0, 3);
      if (header.equals("VRC")) {
        TeleplanVRC vrc = parseVrcEntry(nextline, filename);
        vrcList.add(vrc);
      }
    }
    return vrcList;
  }

  /** Parses the VRC line from the remittance file. Check for duplicates, and persists the VRC entry if none are found.
   * @param nextline The next line in the remittance file to parse
   * @param filename The name of the remittance file
   * @return The parsed VRC entry
   */
  protected TeleplanVRC parseVrcEntry(final String nextline, final String filename) {
    TeleplanVRC t = new TeleplanVRC();
    t.parse(nextline);

    TeleplanVRC existingVRC = vrcDao.findByFilenameDataCentreDateCount(
        filename, t.getDataCentre(), t.getPaymentDate(), t.getRecordCount());
    if (existingVRC == null) {
      t.setFilename(filename);
      vrcDao.persist(t);
    } else {
      t = existingVRC;
    }
    return t;
  }

  /** Parses the S21 line from the remittance file. Check for duplicates, and persists the S21 entry if none are found.
   * Otherwise, update the existing S21 entry with the VRC id if the current vrc entry is null for that record.
   * @param currentVrcId The id of the current VRC section of the remittance file
   * @param nextline The next line in the remittance file to parse
   * @param parserData remittance parser data
   * @return The updated remittance parser data
   */
  protected TeleplanRemittanceParserData parseFromS21Line(
      final Integer currentVrcId,
      final String nextline,
      TeleplanRemittanceParserData parserData) {
    TeleplanS21 parsedS21Entry = parseS21(nextline, parserData);
    parserData.setRaNo("");

    // Check if any VRC entries exist for the S21 record.
    List<TeleplanS21> duplicateS21Entries = s21Dao.findDuplicateS21Entries(parsedS21Entry,
				currentVrcId);
    if (duplicateS21Entries != null && !duplicateS21Entries.isEmpty()) {
      log.info("S21 record already exists in the database. Skipping record.");
      TeleplanS21 duplicateEntry = duplicateS21Entries.get(0);
      parserData.setRaNo(duplicateEntry.getId().toString());

      // Set the VRC id if the file has been previously parsed before the VRC changes.
      if (duplicateEntry.getVrcId() == null) {
        duplicateEntry.setVrcId(currentVrcId);
        s21Dao.merge(duplicateEntry);
      }
      return parserData;
    }

    parserData.setRecFlag(1);
    parsedS21Entry.setVrcId(currentVrcId);
    s21Dao.persist(parsedS21Entry);
    parserData.setRaNo(parsedS21Entry.getId().toString());
    return parserData;
  }

  /** Parses the S01 line from the remittance file and persists the S00 entry if the rec flag is greater than 0.
   * Update the MSP reconcile stat to "Settled".
   * @param nextline The next line in the remittance file to parse
   * @param parserData remittance parser data
   * @return The updated remittance parser data
   */
  protected TeleplanRemittanceParserData parseFromS01Line(
      final String nextline, TeleplanRemittanceParserData parserData) {

    if (parserData.getRecFlag() > 0) {
      TeleplanS00 t = parseS00(nextline, parserData);
      s00Dao.persist(t);
      parserData.updateMspReconcileStat(MSPReconcile.SETTLED, t.getBillingMasterNo());
    }
    return parserData;
  }

  /** Parses the S02, S03, or S00 line from the remittance file and persists the S00 entry if the rec flag is
   * greater than 0. Update the MSP reconcile stat based on the header.
   * @param header The first 3 characters of the line from the remittance file
   * @param nextline The next line in the remittance file to parse
   * @param parserData remittance parser data
   * @return The updated remittance parser data
   */
  protected TeleplanRemittanceParserData parseFromS02Line(
      final String header,
      final String nextline,
      TeleplanRemittanceParserData parserData) {
    int recFlag = parserData.getRecFlag();
    if (recFlag > 0) {
      parserData.setRecFlag(recFlag + 1);
      TeleplanS00 t = parseS02(nextline, parserData);
      s00Dao.persist(t);

      if (header.equals("S02")) {
        parserData.updateMspReconcileStat(MSPReconcile.PAIDWITHEXP, t.getBillingMasterNo());
      } else if (header.equals("S03")) {
        parserData.updateMspReconcileStat(MSPReconcile.REFUSED, t.getBillingMasterNo());
      } else if (header.equals("S00")) {
        parserData.updateMspReconcileStat(MSPReconcile.DATACENTERCHANGED, t.getBillingMasterNo());
      }
    }
    return parserData;
  }

  /** Parses the S04 line from the remittance file, persists the S00 entry, and updates the MSP reconcile stat to
   * "Held" if the rec flag is greater than 0.
   * @param nextline The next line in the remittance file to parse
   * @param parserData remittance parser data
   * @return The updated remittance parser data
   */
  protected TeleplanRemittanceParserData parseFromS04Line(
      final String nextline, TeleplanRemittanceParserData parserData) {
    if (parserData.getRecFlag() > 0) {
      TeleplanS00 t = parseS04(nextline, parserData);
      s00Dao.persist(t);
      parserData.updateMspReconcileStat(MSPReconcile.HELD, t.getBillingMasterNo());
    }
    return parserData;
  }

  /** Parses the S22 line from the remittance file and persists the S22 entry if the rec flag is greater than 0.
   * @param nextline The next line in the remittance file to parse
   * @param parserData remittance parser data
   */
  protected void parseFromS22Line(final String nextline, final TeleplanRemittanceParserData parserData) {
    if (parserData.getRecFlag() > 0) {
      TeleplanS22 t = parseS22(nextline, parserData);
      s22Dao.persist(t);
    }
  }

  /** Parses the S23 line from the remittance file and persists the S23 entry if the rec flag is greater than 0.
   * @param nextline The next line in the remittance file to parse
   * @param parserData remittance parser data
   */
  protected void parseFromS23Line(final String nextline, final TeleplanRemittanceParserData parserData) {
    if (parserData.getRecFlag() > 0) {
      TeleplanS23 t = parseS23(nextline, parserData);
      s23Dao.persist(t);
    }
  }

  /** Parses the S25 line from the remittance file and persists the S25 entry if the rec flag is greater than 0.
   * @param nextline The next line in the remittance file to parse
   * @param parserData remittance parser data
   */
  protected void parseFromS25Line(final String nextline, final TeleplanRemittanceParserData parserData) {
    if (parserData.getRecFlag() > 0) {
      TeleplanS25 t = parseS25(nextline, parserData);
      s25Dao.persist(t);
    }
  }

  /** Parses the C12 line from the remittance file and sets the raNo to an existing S21 record's id,
   * or creates a new S21 entry. Persist the C12 entry.
   * @param nextline The next line in the remittance file to parse
   * @param parserData remittance parser data
   * @return The updated remittance parser data
   */
  protected TeleplanRemittanceParserData parseFromC12Line(
      final String nextline, TeleplanRemittanceParserData parserData) {
    TeleplanC12 c12 = parseC12(nextline, parserData);
    if (StringUtils.isEmpty(parserData.getRaNo())) {
      List<TeleplanS21> rs = s21Dao.findByFilenamePaymentPayeeNo(parserData.getFilename(), "", "");
      for (TeleplanS21 r : rs) {
        parserData.setRaNo(String.valueOf(r.getId()));
      }

      if (StringUtils.isEmpty(parserData.getRaNo())) {
        parserData.setRecFlag(1);

        TeleplanS21 t = new TeleplanS21();
        t.setFileName(parserData.getFilename());
        t.setDataCentre(c12.getDataCentre());
        t.setDataSeq(c12.getDataSeq());
        t.setPayment("");
        t.setPayeeNo(c12.getPayeeNo());
        t.setMspCtlNo("");
        t.setPayeeName("");
        t.setAmountBilled("");
        t.setAmountPaid("");
        t.setBalanceForward("");
        t.setCheque("");
        t.setNewBalance("");
        t.setFiller("");
        t.setStatus('D');
        s21Dao.persist(t);
        parserData.setRaNo(t.getId().toString());
      }
    }
    // This will be +1 if the records are at the bottom
    if (parserData.getRecFlag() > 0) {
      c12.setS21Id(Integer.parseInt(parserData.getRaNo()));
      c12Dao.persist(c12);
      parserData.updateMspReconcileStat(MSPReconcile.REJECTED, c12.getBillingMasterNo());
    }
    parserData.setForwardPage("C12");
    return parserData;
  }

  protected TeleplanS21 parseS21(final String line, final TeleplanRemittanceParserData parserData) {
    TeleplanS21 s21 = new TeleplanS21();
    s21.setFileName(parserData.getFilename());
    s21.setDataCentre(line.substring(3, 8));
    s21.setDataSeq(line.substring(8, 15));
    s21.setPayment(line.substring(15, 23));
    s21.setLineCode(line.substring(23, 24).toCharArray()[0]);
    s21.setPayeeNo(line.substring(24, 29));
    s21.setMspCtlNo(line.substring(29, 35));
    s21.setPayeeName(line.substring(35, 60));
    s21.setAmountBilled(line.substring(60, 69));
    s21.setAmountPaid(line.substring(69, 78));
    s21.setBalanceForward(line.substring(78, 87));
    s21.setCheque(line.substring(87, 96));
    s21.setNewBalance(line.substring(96, 105));
    s21.setFiller(line.substring(105, 165));
    s21.setStatus('N');
    return s21;
  }

  protected TeleplanS00 parseS00(final String line, final TeleplanRemittanceParserData parserData) {
    TeleplanS00 s00 = new TeleplanS00();
    if (NumberUtils.isNumber(parserData.getRaNo())) {
      s00.setS21Id(Integer.parseInt(parserData.getRaNo()));
    }
    s00.setFileName(parserData.getFilename());
    s00.setS00Type(line.substring(0, 3));
    s00.setDataCentre(line.substring(3, 8));
    s00.setDataSeq(line.substring(8, 15));
    s00.setPayment(line.substring(15, 23));
    s00.setLineCode(line.substring(23, 24).toCharArray()[0]);
    s00.setPayeeNo(line.substring(24, 29));
    s00.setMspCtlNo(line.substring(29, 35));
    s00.setPractitionerNo(line.substring(35, 40));
    s00.setMspRcdDate(line.substring(117, 125));
    s00.setInitial("");
    s00.setSurname("");
    s00.setPhn("");
    s00.setPhnDepNo("");
    s00.setServiceDate("");
    s00.setToday("");
    s00.setBillNoServices("");
    s00.setBillClafCode("");
    s00.setBillFeeSchedule("");
    s00.setBillAmount("");
    s00.setPaidNoServices("");
    s00.setPaidClafCode("");
    s00.setPaidFeeSchedule("");
    s00.setPaidAmount(line.substring(110, 117));
    s00.setOfficeNo(line.substring(103, 110));
    s00.setExp1("");
    s00.setExp2("");
    s00.setExp3("");
    s00.setExp4("");
    s00.setExp5("");
    s00.setExp6("");
    s00.setExp7("");
    s00.setAjc1(line.substring(40, 42));
    s00.setAja1(line.substring(42, 49));
    s00.setAjc2(line.substring(49, 51));
    s00.setAja2(line.substring(51, 58));
    s00.setAjc3(line.substring(58, 60));
    s00.setAja3(line.substring(60, 67));
    s00.setAjc4(line.substring(67, 69));
    s00.setAja4(line.substring(69, 76));
    s00.setAjc5(line.substring(76, 78));
    s00.setAja5(line.substring(78, 85));
    s00.setAjc6(line.substring(85, 87));
    s00.setAja6(line.substring(87, 94));
    s00.setAjc7(line.substring(94, 96));
    s00.setAja7(line.substring(96, 103));
    s00.setPaidRate(line.substring(125, 127));
    s00.setPlanRefNo("");
    s00.setClaimSource("");
    s00.setPreviousPaidDate("");
    s00.setIcBcWcb(line.substring(127, 135));
    s00.setInsurerCode(line.substring(135, 137));
    s00.setFiller(line.substring(137, 165));
    return s00;
  }
  
  protected TeleplanS00 parseS02(final String line, final TeleplanRemittanceParserData parserData) {
    TeleplanS00 s00 = new TeleplanS00();
    if (NumberUtils.isNumber(parserData.getRaNo())) {
      s00.setS21Id(Integer.parseInt(parserData.getRaNo()));
    }
    s00.setFileName(parserData.getFilename());
    s00.setS00Type(line.substring(0, 3));
    s00.setDataCentre(line.substring(3, 8));
    s00.setDataSeq(line.substring(8, 15));
    s00.setPayment(line.substring(15, 23));
    s00.setLineCode(line.substring(23, 24).toCharArray()[0]);
    s00.setPayeeNo(line.substring(24, 29));
    s00.setMspCtlNo(line.substring(29, 35));
    s00.setPractitionerNo(line.substring(35, 40));
    s00.setMspRcdDate(line.substring(40, 48));
    s00.setInitial(line.substring(48, 50));
    s00.setSurname(line.substring(50, 68));
    s00.setPhn(line.substring(68, 78));
    s00.setPhnDepNo(line.substring(78, 80));
    s00.setServiceDate(line.substring(80, 88));
    s00.setToday(line.substring(88, 90));
    s00.setBillNoServices(line.substring(90, 93));
    s00.setBillClafCode(line.substring(93, 95));
    s00.setBillFeeSchedule(line.substring(95, 100));
    s00.setBillAmount(line.substring(100, 107));
    s00.setPaidNoServices(line.substring(107, 110));
    s00.setPaidClafCode(line.substring(110, 112));
    s00.setPaidFeeSchedule(line.substring(112, 117));
    s00.setPaidAmount(line.substring(117, 124));
    s00.setOfficeNo(line.substring(124, 131));
    s00.setExp1(line.substring(131, 133));
    s00.setExp2(line.substring(133, 135));
    s00.setExp3(line.substring(135, 137));
    s00.setExp4(line.substring(137, 139));
    s00.setExp5(line.substring(139, 141));
    s00.setExp6(line.substring(141, 143));
    s00.setExp7(line.substring(143, 145));
    s00.setAjc1(line.substring(145, 147));
    s00.setAja1(line.substring(147, 154));
    s00.setAjc2(line.substring(154, 156));
    s00.setAja2(line.substring(156, 163));
    s00.setAjc3(line.substring(163, 165));
    s00.setAja3(line.substring(165, 172));
    s00.setAjc4(line.substring(172, 174));
    s00.setAja4(line.substring(174, 181));
    s00.setAjc5(line.substring(181, 183));
    s00.setAja5(line.substring(183, 190));
    s00.setAjc6(line.substring(190, 192));
    s00.setAja6(line.substring(192, 199));
    s00.setAjc7(line.substring(199, 201));
    s00.setAja7(line.substring(201, 208));
    s00.setPaidRate("");
    s00.setPlanRefNo(line.substring(208, 218));
    s00.setClaimSource(line.substring(218, 219));
    s00.setPreviousPaidDate(line.substring(219, 227));
    s00.setInsurerCode(line.substring(227, 229));
    s00.setIcBcWcb(line.substring(229, 237));
    s00.setFiller(line.substring(237, 267));
    return s00;
  }

  protected TeleplanS00 parseS04(final String line, final TeleplanRemittanceParserData parserData) {
    TeleplanS00 s00 = new TeleplanS00();
    if (NumberUtils.isNumber(parserData.getRaNo())) {
      s00.setS21Id(Integer.parseInt(parserData.getRaNo()));
    }
    s00.setFileName(parserData.getFilename());
    s00.setS00Type(line.substring(0, 3));
    s00.setDataCentre(line.substring(3, 8));
    s00.setDataSeq(line.substring(8, 15));
    s00.setPayment(line.substring(15, 23));
    s00.setLineCode(line.substring(23, 24).toCharArray()[0]);
    s00.setPayeeNo(line.substring(24, 29));
    s00.setMspCtlNo(line.substring(29, 35));
    try {
      s00.setPractitionerNo(line.substring(35, 40));
      s00.setMspRcdDate(line.substring(40, 48));
    } catch (Exception e) {
      s00.setPractitionerNo("");
      s00.setMspRcdDate("");
      log.error("Failed to parse practitioner number and MSP received date: ", e);
    }
    s00.setInitial("");
    s00.setSurname("");
    s00.setPhn("");
    s00.setPhnDepNo("");
    s00.setServiceDate("");
    s00.setToday("");
    s00.setBillNoServices("");
    s00.setBillClafCode("");
    s00.setBillFeeSchedule("");
    s00.setBillAmount("");
    s00.setPaidNoServices("");
    s00.setPaidClafCode("");
    s00.setPaidFeeSchedule("");
    s00.setPaidAmount("");
    s00.setOfficeNo(line.substring(48, 55));
    s00.setExp1(line.substring(55, 57));
    s00.setExp2(line.substring(57, 59));
    s00.setExp3(line.substring(59, 61));
    s00.setExp4(line.substring(61, 63));
    s00.setExp5(line.substring(63, 65));
    s00.setExp6(line.substring(65, 67));
    s00.setExp7(line.substring(67, 69));
    s00.setAjc1("");
    s00.setAja1("");
    s00.setAjc2("");
    s00.setAja2("");
    s00.setAjc3("");
    s00.setAja3("");
    s00.setAjc4("");
    s00.setAja4("");
    s00.setAjc5("");
    s00.setAja5("");
    s00.setAjc6("");
    s00.setAja6("");
    s00.setAjc7("");
    s00.setAja7("");
    s00.setPaidRate("");
    s00.setPlanRefNo("");
    s00.setClaimSource("");
    s00.setPreviousPaidDate("");
    s00.setIcBcWcb(line.substring(69, 77));
    s00.setInsurerCode(line.substring(77, 79));
    s00.setFiller(line.substring(79, 165));
    return s00;
  }

  protected TeleplanS22 parseS22(final String line, final TeleplanRemittanceParserData parserData) {
    TeleplanS22 s22 = new TeleplanS22();
    if (NumberUtils.isNumber(parserData.getRaNo())) {
      s22.setS21Id(Integer.parseInt(parserData.getRaNo()));
    }
    s22.setFileName(parserData.getFilename());
    s22.setS22Type(line.substring(0, 3));
    s22.setDataCentre(line.substring(3, 8));
    s22.setDataSeq(line.substring(8, 15));
    s22.setPayment(line.substring(15, 23));
    s22.setLineCode(line.substring(23, 24).toCharArray()[0]);
    s22.setPayeeNo(line.substring(24, 29));
    s22.setMspCtlNo(line.substring(29, 35));
    s22.setPractitionerNo(line.substring(35, 40));
    s22.setPractitionerName(line.substring(40, 65));
    s22.setAmountBilled(line.substring(65, 74));
    s22.setAmountPaid(line.substring(74, 83));
    s22.setFiller(line.substring(83, 165));
    return s22;
  }

  protected TeleplanS23 parseS23(final String line, final TeleplanRemittanceParserData parserData) {
    TeleplanS23 s23 = new TeleplanS23();
    if (NumberUtils.isNumber(parserData.getRaNo())) {
      s23.setS21Id(Integer.parseInt(parserData.getRaNo()));
    }
    s23.setFileName(parserData.getFilename());
    s23.setS23Type(line.substring(0, 3));
    s23.setDataCentre(line.substring(3, 8));
    s23.setDataSeq(line.substring(8, 15));
    s23.setPayment(line.substring(15, 23));
    s23.setLineCode(line.substring(23, 24).toCharArray()[0]);
    s23.setPayeeNo(line.substring(24,29));
    s23.setMspCtlNo(line.substring(29, 35));
    s23.setAjc(line.substring(35, 37));
    s23.setAji(line.substring(37, 49));
    s23.setAjm(line.substring(49, 69));
    s23.setCalcMethod(line.substring(69, 70));
    s23.setRPercent(line.substring(70, 75));
    s23.setOPercent(line.substring(75, 80));
    s23.setGAmount(line.substring(80, 89));
    s23.setRAmount(line.substring(89, 98));
    s23.setOAmount(line.substring(98, 107));
    s23.setBalanceForward(line.substring(107, 116));
    s23.setAdjMade(line.substring(116, 125));
    s23.setAdjOutstanding(line.substring(125, 134));
    s23.setFiller(line.substring(134, 165));
    return s23;
  }

  protected TeleplanS25 parseS25(final String line, final TeleplanRemittanceParserData parserData) {
    TeleplanS25 s25 = new TeleplanS25();
    if (NumberUtils.isNumber(parserData.getRaNo())) {
      s25.setS21Id(Integer.parseInt(parserData.getRaNo()));
    }
    s25.setFileName(parserData.getFilename());
    s25.setS25Type(line.substring(0, 3));
    s25.setDataCentre(line.substring(3, 8));
    s25.setDataSeq(line.substring(8, 15));
    s25.setPayment(line.substring(15, 23));
    s25.setLineCode(line.substring(23, 24).toCharArray()[0]);
    s25.setPayeeNo(line.substring(24, 29));
    s25.setMspCtlNo(line.substring(29, 35));
    s25.setPractitionerNo(line.substring(35, 40));
    s25.setMessage(line.substring(40, 120));
    s25.setFiller(line.substring(120, 165));
    return s25;
  }

  protected TeleplanC12 parseC12(final String line, final TeleplanRemittanceParserData parserData) {
    TeleplanC12 c12 = new TeleplanC12();
    if (NumberUtils.isNumber(parserData.getRaNo())) {
      c12.setS21Id(Integer.parseInt(parserData.getRaNo()));
    }
    c12.setFileName(parserData.getFilename());
    c12.setDataCentre(line.substring(3, 8));
    c12.setDataSeq(line.substring(8, 15));
    c12.setPayeeNo(line.substring(15, 20));
    c12.setPractitionerNo(line.substring(20, 25));
    c12.setExp1(line.substring(25, 27));
    c12.setExp2(line.substring(27, 29));
    c12.setExp3(line.substring(29, 31));
    c12.setExp4(line.substring(31, 33));
    c12.setExp5(line.substring(33, 35));
    c12.setExp6(line.substring(35, 37));
    c12.setExp7(line.substring(37, 39));
    c12.setOfficeFolioClaimNo(line.substring(39, 46));
    c12.setFiller(line.substring(46, 70));
    return c12;
  }

  protected TeleplanRemittanceParserData createParserData(final String filename) {
    return new TeleplanRemittanceParserData(filename);
  }
}
