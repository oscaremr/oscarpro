package oscar.oscarBilling.ca.bc.Teleplan;

import lombok.Getter;
import lombok.Setter;
import oscar.oscarBilling.ca.bc.MSP.MSPReconcile;

@Getter
@Setter
public class TeleplanRemittanceParserData {
    private String filename;
    private String forwardPage = "S21";
    private String raNo = "";
    private int recFlag = 0;
    private MSPReconcile mspReconcile = new MSPReconcile();

    public TeleplanRemittanceParserData(final String filename) {
        this.filename = filename;
    }

    public void updateMspReconcileStat(final String reconcileStatus, final String billingMasterNo) {
        mspReconcile.updateStat(reconcileStatus, billingMasterNo);
    }
}
