//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.valueObject;

import java.util.Date;

public class OnAccountValueObject implements Comparable {
    private int invoiceNo;
    private String date;
    private String txnType;
    private String voidStatus;
    private String pendingStatus;
    private double totalAmount;
    private double amountCharged;
    private double balanceOwing;
    private Date creationDate;

    public OnAccountValueObject() {
    }

    public int getInvoiceNo() {
        return this.invoiceNo;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setInvoiceNo(int invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTxnType() {
        return this.txnType;
    }

    public void setTxnType(String txnType) {
        this.txnType = txnType;
    }

    public String getVoidStatus() {
        return this.voidStatus;
    }

    public void setVoidStatus(String voidStatus) {
        this.voidStatus = voidStatus;
    }

    public String getPendingStatus() {
        return this.pendingStatus;
    }

    public void setPendingStatus(String pendingStatus) {
        this.pendingStatus = pendingStatus;
    }

    public double getTotalAmount() {
        return this.totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getAmountCharged() {
        return this.amountCharged;
    }

    public void setAmountCharged(double amountCharged) {
        this.amountCharged = amountCharged;
    }

    public double getBalanceOwing() {
        return this.balanceOwing;
    }

    public void setBalanceOwing(double balanceOwing) {
        this.balanceOwing = balanceOwing;
    }

    public int compareTo(Object o) {
        OnAccountValueObject obj = (OnAccountValueObject)o;
        return obj.getCreationDate().compareTo(this.creationDate);
    }
}
