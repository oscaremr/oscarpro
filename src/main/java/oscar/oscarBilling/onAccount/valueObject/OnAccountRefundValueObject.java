//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.valueObject;

public class OnAccountRefundValueObject {
    private int refundID;
    private String date;
    private String refundDetails;
    private double refundAmount;

    public OnAccountRefundValueObject() {
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getRefundID() {
        return this.refundID;
    }

    public void setRefundID(int refundID) {
        this.refundID = refundID;
    }

    public String getRefundDetails() {
        return this.refundDetails;
    }

    public void setRefundDetails(String refundDetails) {
        this.refundDetails = refundDetails;
    }

    public double getRefundAmount() {
        return this.refundAmount;
    }

    public void setRefundAmount(double refundAmount) {
        this.refundAmount = refundAmount;
    }
}
