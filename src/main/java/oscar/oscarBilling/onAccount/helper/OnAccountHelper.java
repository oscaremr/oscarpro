//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;

import ca.kai.util.DateUtils;
import lombok.val;
import lombok.var;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.ClinicDAO;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.OnAccountBillDao;
import org.oscarehr.common.dao.OnAccountDepositDao;
import org.oscarehr.common.dao.OnAccountDepositServiceDao;
import org.oscarehr.common.dao.OnAccountPaymentsDao;
import org.oscarehr.common.dao.OnAccountRefundDao;
import org.oscarehr.common.dao.OnAccountRefundDetailDao;
import org.oscarehr.common.dao.OnAccountServiceDao;
import org.oscarehr.common.model.OnAccountDeposit;
import org.oscarehr.common.model.OnAccountDepositService;
import org.oscarehr.common.model.OnAccountPayment;
import org.oscarehr.common.model.OnAccountRefund;
import org.oscarehr.common.model.OnAccountRefundDetail;
import org.oscarehr.common.model.OnAccountBill;
import org.oscarehr.common.model.OnAccountService;
import org.oscarehr.util.SpringUtils;
import oscar.oscarBilling.onAccount.formBean.OnAccountBillFormBean;
import oscar.oscarBilling.onAccount.formBean.OnAccountDepositFormBean;
import oscar.oscarBilling.onAccount.formBean.OnAccountRefundFormBean;
import oscar.oscarBilling.onAccount.formBean.SharedOnAccountFormBean;
import oscar.oscarBilling.onAccount.valueObject.OnAccountPaymentValueObject;
import oscar.oscarBilling.onAccount.valueObject.OnAccountRefundValueObject;
import oscar.oscarBilling.onAccount.valueObject.OnAccountServiceValueObject;
import oscar.oscarBilling.onAccount.valueObject.OnAccountValueObject;

public class OnAccountHelper {
    public OnAccountHelper() {
    }

    public static double getTotalBills(int demographicNo) {
        OnAccountBillDao onAccountBillDao = SpringUtils.getBean(OnAccountBillDao.class);
        return onAccountBillDao.getTotalBillsByDemographicNumber(demographicNo);
    }

    public static double getTotalBalanceBills(int demographicNo) {
        OnAccountBillDao onAccountBillDao = SpringUtils.getBean(OnAccountBillDao.class);
        return onAccountBillDao.getTotalBalanceByDemographicNumber(demographicNo);
    }

    public static double getDepositPaymentOwing(int demographicNo) {
        OnAccountDepositDao onAccountDepositDao = SpringUtils.getBean(OnAccountDepositDao.class);
        return onAccountDepositDao.getTotalBalanceByDemographicNumber(demographicNo);
    }

    public static double getTotalDeposits(int demographicNo) {
        OnAccountDepositDao onAccountDepositDao = SpringUtils.getBean(OnAccountDepositDao.class);
        return onAccountDepositDao.getTotalDepositsByDemographicNumber(demographicNo);
    }

    public static double getTotalRefunds(int demographicNo) {
        OnAccountRefundDao onAccountRefundDao = SpringUtils.getBean(OnAccountRefundDao.class);
        return onAccountRefundDao.getTotalRefundsByDemographicNumber(demographicNo);
    }

    public static double getTotalPendingRefunds(int demographicNo) {
        OnAccountRefundDao onAccountRefundDao = SpringUtils.getBean(OnAccountRefundDao.class);
        return onAccountRefundDao.getTotalRefundsPendingByDemographicNumber(demographicNo);
    }

    public static double getTotalAccountBalance(int demographicNo) {
        return getTotalDeposits(demographicNo) - getTotalBills(demographicNo) - getTotalRefunds(demographicNo);
    }

    public static void getBillDetails(int demographicNo, ArrayList transactions) {
        OnAccountBillDao onAccountBillDao = SpringUtils.getBean(OnAccountBillDao.class);
        val results = onAccountBillDao.findByDemographicNumber(demographicNo);

        for (var result : results) {
            OnAccountValueObject vo = new OnAccountValueObject();
            vo.setInvoiceNo(result.getId());
            vo.setDate(result.getDate());
            vo.setTxnType("Bill");
            if ("Void".equalsIgnoreCase(result.getBillStatus())) {
                vo.setVoidStatus("Void");
            }

            vo.setPendingStatus("");
            vo.setTotalAmount(result.getTotalServices());
            vo.setAmountCharged(result.getAmountCharged());
            vo.setBalanceOwing(result.getBalance());
            vo.setCreationDate(DateUtils.getDateByString("yyyy-MM-dd HH:mm:ss", result.getCreationDate()));
            transactions.add(vo);
        }
    }

    public static void getDepositDetails(int demographicNo, ArrayList transactions) {
        OnAccountDepositDao onAccountDepositDao = SpringUtils.getBean(OnAccountDepositDao.class);
        val results = onAccountDepositDao.findByDemographicNumber(demographicNo);

        for (var result : results) {
            OnAccountValueObject vo = new OnAccountValueObject();
            vo.setInvoiceNo(result.getId());
            vo.setDate(result.getDate());
            vo.setTxnType("Deposit");
            if ("Void".equalsIgnoreCase(result.getDepositStatus())) {
                vo.setVoidStatus("Void");
            }

            vo.setPendingStatus("");
            vo.setTotalAmount(result.getTotalServices());
            vo.setAmountCharged(result.getTotalDeposits());
            vo.setBalanceOwing(result.getBalance());
            vo.setCreationDate(DateUtils.getDateByString("yyyy-MM-dd HH:mm:ss", result.getCreationDate()));
            transactions.add(vo);
        }

    }

    public static void getRefundDetails(int demographicNo, ArrayList transactions) {

        OnAccountRefundDao onAccountRefundDao = SpringUtils.getBean(OnAccountRefundDao.class);
        val results = onAccountRefundDao.findByDemographicNumber(demographicNo);

        for (var result : results) {
            OnAccountValueObject vo = new OnAccountValueObject();
            vo.setInvoiceNo(result.getId());
            vo.setDate(result.getDate());
            vo.setTxnType("Refund");
            if ("Pending".equalsIgnoreCase(result.getRefundStatus())) {
                vo.setPendingStatus("Pending");
            }

            if ("Void".equalsIgnoreCase(result.getRefundStatus())) {
                vo.setVoidStatus("Void");
            }

            vo.setTotalAmount(result.getTotalRefund());
            vo.setAmountCharged(0.0);
            vo.setBalanceOwing(0.0);
            vo.setCreationDate(DateUtils.getDateByString("yyyy-MM-dd HH:mm:ss", result.getCreationDate()));
            transactions.add(vo);
        }

    }

    public static void saveRefund(OnAccountRefundFormBean bean, HttpServletRequest request) {
        OnAccountRefundDao onAccountRefundDao = SpringUtils.getBean(OnAccountRefundDao.class);
        OnAccountRefundDetailDao onAccountRefundDetailDao = SpringUtils.getBean(OnAccountRefundDetailDao.class);

        val results = onAccountRefundDao.findByDemographicAndInvoiceNumber(
                (Integer) request.getSession().getAttribute("demoNoForOnaccountrefunds"),
                bean.getInvoiceNo());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (results.isEmpty()) {
            val onAccountRefund = new OnAccountRefund();
            onAccountRefund.setDate(bean.getDate());
            onAccountRefund.setDxCode(bean.getDxCode());
            onAccountRefund.setPrimaryPhysician(bean.getPrimaryPhysician());
            onAccountRefund.setBillingPhysician(bean.getBillingPhysician());
            onAccountRefund.setReferringPhysician(bean.getReferringPhysician());
            onAccountRefund.setTotalRefund(bean.getTotalAmount());
            onAccountRefund.setRefundStatus(bean.getRefundStatus());
            onAccountRefund.setNotes(bean.getNotes());
            onAccountRefund.setDemographicNo((Integer) request.getSession().getAttribute("demoNoForOnaccountrefunds"));
            onAccountRefund.setCreationDate(sdf.format(Calendar.getInstance().getTime()));
            onAccountRefundDao.persist(onAccountRefund);
            bean.setInvoiceNo(onAccountRefund.getId());
        } else {
            for (OnAccountRefund result : results) {
                result.setDate(bean.getDate());
                result.setDxCode(bean.getDxCode());
                result.setPrimaryPhysician(bean.getPrimaryPhysician());
                result.setBillingPhysician(bean.getBillingPhysician());
                result.setReferringPhysician(bean.getReferringPhysician());
                result.setTotalRefund(bean.getTotalAmount());
                result.setRefundStatus(bean.getRefundStatus());
                result.setNotes(bean.getNotes());
                onAccountRefundDao.merge(result);
            }
        }

        onAccountRefundDetailDao.deleteByInvoiceNumber(bean.getInvoiceNo());

        for (OnAccountRefundValueObject onAccountRefundValueObject : bean.getRefundList()) {
            val onAccountRefundDetail = new OnAccountRefundDetail();
            onAccountRefundDetail.setInvoiceNumber(bean.getInvoiceNo());
            onAccountRefundDetail.setAmount(onAccountRefundValueObject.getRefundAmount());
            onAccountRefundDetail.setDetails(onAccountRefundValueObject.getRefundDetails());
            onAccountRefundDetail.setDate(onAccountRefundValueObject.getDate());
            onAccountRefundDetail.setStatus("");
            onAccountRefundDetailDao.persist(onAccountRefundDetail);
        }
    }

    public static void saveBill(OnAccountBillFormBean bean, HttpServletRequest request) {
        OnAccountBillDao onAccountBillDao = SpringUtils.getBean(OnAccountBillDao.class);
        OnAccountServiceDao onAccountServiceDao = SpringUtils.getBean(OnAccountServiceDao.class);
        val result = onAccountBillDao.findByInvoiceAndDemographicNumber(
                bean.getInvoiceNo(),
                (Integer) request.getSession().getAttribute("demoNoForOnaccountbills"));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");

        if (result == null) {
            val onAccountBill = new OnAccountBill();
            onAccountBill.setDate(bean.getDate());
            onAccountBill.setDxCode(bean.getDxCode());
            onAccountBill.setPrimaryPhysician(bean.getPrimaryPhysician());
            onAccountBill.setBillingPhysician(bean.getBillingPhysician());
            onAccountBill.setReferringPhysician(bean.getReferringPhysician());
            onAccountBill.setTotalServices(bean.getTotalCharges());
            onAccountBill.setHst(bean.getHst());
            onAccountBill.setAmountCharged(bean.getAmountCharged());
            onAccountBill.setBalance(bean.getBalanceOwing());
            onAccountBill.setNotes(bean.getNotes());
            onAccountBill.setBillStatus("");
            onAccountBill.setDemographicNo((Integer) request.getSession().getAttribute("demoNoForOnaccountbills"));
            onAccountBill.setCreationDate(sdf.format(Calendar.getInstance().getTime()));
            onAccountBillDao.persist(onAccountBill);
            bean.setInvoiceNo(onAccountBill.getId());
        } else {
            result.setDate(bean.getDate());
            result.setDate(bean.getDate());
            result.setDxCode(bean.getDxCode());
            result.setPrimaryPhysician(bean.getPrimaryPhysician());
            result.setBillingPhysician(bean.getBillingPhysician());
            result.setReferringPhysician(bean.getReferringPhysician());
            result.setTotalServices(bean.getTotalCharges());
            result.setHst(bean.getHst());
            result.setAmountCharged(bean.getAmountCharged());
            result.setBalance(bean.getBalanceOwing());
            result.setNotes(bean.getNotes());
            result.setDemographicNo((Integer) request.getSession().getAttribute("demoNoForOnaccountbills"));
            result.setCreationDate(sdf.format(Calendar.getInstance().getTime()));
            onAccountBillDao.merge(result);
        }
        onAccountServiceDao.deleteByInvoiceNumber(bean.getInvoiceNo());
        for (OnAccountServiceValueObject onAccountServiceValueObject : bean.getChargeList()) {
            val onAccountService = new OnAccountService();
            onAccountService.setInvoiceNo(bean.getInvoiceNo());
            onAccountService.setServiceCode(onAccountServiceValueObject.getServiceCode());
            onAccountService.setAmount(onAccountServiceValueObject.getAmount());
            onAccountService.setDate(sdf2.format(Calendar.getInstance().getTime()));
            onAccountService.setStatus("");
            onAccountServiceDao.persist(onAccountService);
        }

    }

    public static void saveDeposit(OnAccountDepositFormBean bean, HttpServletRequest request) {
        OnAccountDepositDao onAccountDepositDao = SpringUtils.getBean(OnAccountDepositDao.class);
        OnAccountDepositServiceDao onAccountDepositServiceDao = SpringUtils.getBean(OnAccountDepositServiceDao.class);
        OnAccountPaymentsDao onAccountPaymentsDao = SpringUtils.getBean(OnAccountPaymentsDao.class);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int demoNo = (Integer)request.getSession().getAttribute("demoNoForOnaccountdeposits");

        val result = onAccountDepositDao.findByDemographicAndInvoiceNumber(
                (Integer) request.getSession().getAttribute("demoNoForOnaccountdeposits"),
                bean.getInvoiceNo());
        if (result == null) {
            val onAccountDeposit = new OnAccountDeposit();
            onAccountDeposit.setDate(bean.getDate());
            onAccountDeposit.setDxCode(bean.getDxCode());
            onAccountDeposit.setPrimaryPhysician(bean.getPrimaryPhysician());
            onAccountDeposit.setBillingPhysician(bean.getBillingPhysician());
            onAccountDeposit.setReferringPhysician(bean.getReferringPhysician());
            onAccountDeposit.setTotalServices(bean.getDepositTotal());
            onAccountDeposit.setTotalDeposits(bean.getPaymentTotal());
            onAccountDeposit.setBalance(bean.getBalanceOwing());
            onAccountDeposit.setNotes(bean.getNotes());
            onAccountDeposit.setDemographicNo(demoNo);
            onAccountDeposit.setCreationDate(sdf.format(Calendar.getInstance().getTime()));
            onAccountDeposit.setDepositStatus("");
            onAccountDepositDao.persist(onAccountDeposit);
            bean.setInvoiceNo(onAccountDeposit.getId());
        } else {
            result.setDate(bean.getDate());
            result.setDxCode(bean.getDxCode());
            result.setPrimaryPhysician(bean.getPrimaryPhysician());
            result.setBillingPhysician(bean.getBillingPhysician());
            result.setReferringPhysician(bean.getReferringPhysician());
            result.setTotalServices(bean.getDepositTotal());
            result.setTotalDeposits(bean.getPaymentTotal());
            result.setBalance(bean.getBalanceOwing());
            result.setNotes(bean.getNotes());
            result.setDemographicNo(demoNo);
            result.setCreationDate(sdf.format(Calendar.getInstance().getTime()));
            onAccountDepositDao.merge(result);
        }
        onAccountDepositServiceDao.deleteByInvoiceNumber(bean.getInvoiceNo());
        for (OnAccountServiceValueObject onAccountServiceValueObject : bean.getServiceList()) {
            val onAccountDepositService = new OnAccountDepositService();
            onAccountDepositService.setInvoiceNo(bean.getInvoiceNo());
            onAccountDepositService.setServiceCode(onAccountServiceValueObject.getServiceCode());
            onAccountDepositService.setAmount(onAccountServiceValueObject.getAmount());
            onAccountDepositService.setDate(sdf.format(Calendar.getInstance().getTime()));
            onAccountDepositService.setStatus("");
            onAccountDepositServiceDao.persist(onAccountDepositService);
        }

        onAccountPaymentsDao.deleteByInvoiceNumber(bean.getInvoiceNo());
        for (OnAccountPaymentValueObject onAccountPaymentValueObject : bean.getPaymentList()) {
            val onAccountPayment = new OnAccountPayment();
            onAccountPayment.setInvoiceNumber(bean.getInvoiceNo());
            onAccountPayment.setDetails(onAccountPaymentValueObject.getPaymentDetails());
            onAccountPayment.setAmount(onAccountPaymentValueObject.getPaymentAmount());
            onAccountPayment.setDate(bean.getPaymentDate());
            onAccountPayment.setStatus("");
            onAccountPaymentsDao.persist(onAccountPayment);
        }

    }

    public static void calculateServiceDeposits(OnAccountDepositFormBean bean, ArrayList serviceList) {
        try {
            double totalServices = 0.0;

            OnAccountServiceValueObject serviceVO;
            for(Iterator it = serviceList.iterator(); it.hasNext(); totalServices += serviceVO.getAmount()) {
                serviceVO = (OnAccountServiceValueObject)it.next();
            }

            bean.setDepositTotal(totalServices);
            bean.setBalanceOwing(totalServices - bean.getPaymentTotal());
        } catch (Exception var6) {
            var6.printStackTrace();
        }

    }

    public static void calculatePayments(OnAccountDepositFormBean bean, ArrayList paymentList) {
        try {
            double totalPayments = 0.0;

            OnAccountPaymentValueObject paymentVO;
            for(Iterator it = paymentList.iterator(); it.hasNext(); totalPayments += paymentVO.getPaymentAmount()) {
                paymentVO = (OnAccountPaymentValueObject)it.next();
            }

            bean.setPaymentTotal(totalPayments);
            bean.setBalanceOwing(bean.getDepositTotal() - totalPayments);
        } catch (Exception var6) {
            var6.printStackTrace();
        }

    }

    public static void calculateDeleteBillCharges(OnAccountBillFormBean bean, ArrayList chargeList, double deletedAmount) {
        try {
            double accountBalance = getTotalAccountBalance(bean.getDemographic_No()) + deletedAmount;
            accountBalance = accountBalance < 0.0 ? 0.0 : accountBalance;
            double totalCharges = 0.0;

            OnAccountServiceValueObject serviceVO;
            for(Iterator it = chargeList.iterator(); it.hasNext(); totalCharges += serviceVO.getAmount()) {
                serviceVO = (OnAccountServiceValueObject)it.next();
            }

            double amountCharged = accountBalance > totalCharges ? totalCharges : accountBalance;
            double balanceOwing = totalCharges - amountCharged;
            bean.setSubTotal(totalCharges);
            bean.setHst(0.0);
            bean.setTotalCharges(totalCharges);
            bean.setAmountCharged(amountCharged);
            bean.setBalanceOwing(balanceOwing);
        } catch (Exception var15) {
            var15.printStackTrace();
        }

    }

    public static void calculateBillCharges(OnAccountBillFormBean bean, ArrayList chargeList) {
        try {
            double accountBalance = getTotalAccountBalance(bean.getDemographic_No());
            accountBalance = accountBalance < 0.0 ? 0.0 : accountBalance;
            double totalCharges = 0.0;

            OnAccountServiceValueObject serviceVO;
            for(Iterator it = chargeList.iterator(); it.hasNext(); totalCharges += serviceVO.getAmount()) {
                serviceVO = (OnAccountServiceValueObject)it.next();
            }

            double amountCharged = accountBalance > totalCharges ? totalCharges : accountBalance;
            double balanceOwing = totalCharges - amountCharged;
            bean.setSubTotal(totalCharges);
            bean.setHst(0.0);
            bean.setTotalCharges(totalCharges);
            bean.setAmountCharged(amountCharged);
            bean.setBalanceOwing(balanceOwing);
        } catch (Exception var13) {
        }

    }

    public static void setClinicFormData(final SharedOnAccountFormBean formBean) {
        ClinicDAO clinicDao = SpringUtils.getBean(ClinicDAO.class);
        val clinic = clinicDao.getClinic();
        if (clinic != null) {
            formBean.setRemitToName(clinic.getClinicName());
            formBean.setRemitToAddress(clinic.getClinicAddress());
            formBean.setRemitToCity(clinic.getClinicCity());
            formBean.setRemitToProvince(clinic.getClinicProvince());
            formBean.setRemitToPostal(clinic.getClinicPostal());
            formBean.setRemitToPhone(clinic.getClinicPhone());
            formBean.setRemitToFax(clinic.getClinicFax());
            formBean.setBillingPhysician(clinic.getClinicName());
        }
    }

    public static void setDemographicFormData(final SharedOnAccountFormBean formBean, final Integer demographicNumber) {
        DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
        ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);

        String requestingPhysician = null;
        val demographic = demographicDao.getDemographic(demographicNumber);
        if (demographic != null) {
            formBean.setPatientName(demographic.getLastName() + ", " + demographic.getFirstName());
            formBean.setPatientAddress(demographic.getAddress());
            formBean.setPatientCity(demographic.getCity());
            formBean.setPatientProvince(demographic.getProvince());
            formBean.setPatientPostal(demographic.getPostal());
            formBean.setReferringPhysicianHidden(demographic.getReferralPhysicianName());
            requestingPhysician = demographic.getProviderNo();
        }

        val provider = providerDao.getProvider(requestingPhysician);
        if (provider != null) {
            formBean.setPrimaryPhysicianHidden(provider.getLastName() + ", " + provider.getFirstName());
        }
    }
}
