//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.action;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.LookupDispatchAction;
import org.apache.struts.util.LabelValueBean;
import oscar.oscarBilling.onAccount.formBean.OnAccountServiceFormBean;
import oscar.oscarDB.DBHandler;

public class OnAccountSelectServiceAction extends LookupDispatchAction {
    public OnAccountSelectServiceAction() {
    }

    protected Map getKeyMethodMap() {
        HashMap map = new HashMap();
        map.put("demographic.onAccount.bill.loadServices", "loadServices");
        return map;
    }

    public ActionForward loadServices(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountServiceFormBean formBean = new OnAccountServiceFormBean();
        formBean.setFromPage(request.getParameter("fromPage"));
        String value = request.getParameter("p1");
        ArrayList serviceList = new ArrayList();

        try {
            ResultSet rs = null;
            String sql;
            if (value.equals("true")) {
                sql = "SELECT bs.service_code service_code,bs.description description,bs.value value " +
                        "FROM ctl_billingservice codes, defaultonaccountbillingform defaultbill,billingservice bs " +
                        "where  ((defaultbill.servicetype1 = '-All-') OR (codes.servicetype = defaultbill.servicetype1 )) " +
                        "and bs.service_code=codes.service_code";
                rs = DBHandler.GetSQL(sql);
            } else {
                sql = "SELECT bs.service_code service_code,bs.description description,bs.value value " +
                        "FROM ctl_billingservice codes, defaultonaccountbillingform defaultbill,billingservice bs " +
                        "where  ((defaultbill.servicetype2 = '-All-') OR (codes.servicetype = defaultbill.servicetype2 )) " +
                        "and bs.service_code=codes.service_code";
                rs = DBHandler.GetSQL(sql);
            }

            while(rs.next()) {
                serviceList.add(new LabelValueBean(rs.getString("service_code") + " (" + rs.getString("description") + ")", rs.getString("value") + "__" + rs.getString("service_code")));
            }

            rs.close();
            formBean.setServiceCodeDetailList(serviceList);
        } catch (Exception var11) {
            var11.printStackTrace();
        }

        request.setAttribute("serviceList", serviceList);
        return mapping.findForward("success");
    }
}
