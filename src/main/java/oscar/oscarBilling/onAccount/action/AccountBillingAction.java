//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import oscar.oscarBilling.onAccount.formBean.OnAccountFormBean;
import oscar.oscarBilling.onAccount.helper.OnAccountHelper;
import oscar.oscarBilling.onAccount.valueObject.OnAccountValueObject;

public class AccountBillingAction extends Action {
    public AccountBillingAction() {
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        OnAccountFormBean bean = (OnAccountFormBean)form;
        int demographicNo = Integer.parseInt(request.getParameter("demographic_no"));
        String demographic_name = request.getParameter("demographic_name");

        bean.setBalance(OnAccountHelper.getTotalAccountBalance(demographicNo));
        bean.setPendingRefund(OnAccountHelper.getTotalPendingRefunds(demographicNo));
        bean.setBalanceBill(OnAccountHelper.getTotalBalanceBills(demographicNo));
        bean.setDepositPaymentOwing(OnAccountHelper.getDepositPaymentOwing(demographicNo));
        ArrayList transactions = new ArrayList();
        OnAccountHelper.getBillDetails(demographicNo, transactions);
        OnAccountHelper.getDepositDetails(demographicNo, transactions);
        OnAccountHelper.getRefundDetails(demographicNo, transactions);
        Collections.sort(transactions, new Comparator() {
            public int compare(Object o1, Object o2) {
                int val = 0;
                if (o1 != null && o2 != null) {
                    OnAccountValueObject obj1 = (OnAccountValueObject)o1;
                    OnAccountValueObject obj2 = (OnAccountValueObject)o2;
                    if (obj1.getDate() != null && obj2.getDate() != null) {
                        val = obj2.getDate().compareTo(obj1.getDate());
                    }
                }

                return val;
            }
        });
        bean.setTransactions(transactions);

        bean.setDemoName(demographic_name);
        bean.setDemographic_No(demographicNo);
        bean.setType(new String[]{"All"});
        bean.setStatus(new String[]{"All"});
        bean.setDateAll(new String[]{"dateAll"});
        bean.setDays(new String[0]);
        bean.setFromDate("");
        bean.setToDate("");
        return mapping.findForward("success");
    }
}
