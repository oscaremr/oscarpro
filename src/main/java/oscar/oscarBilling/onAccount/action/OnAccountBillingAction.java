//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.action;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.LookupDispatchAction;
import oscar.oscarBilling.onAccount.formBean.OnAccountBillFormBean;
import oscar.oscarBilling.onAccount.formBean.OnAccountDepositFormBean;
import oscar.oscarBilling.onAccount.formBean.OnAccountFormBean;
import oscar.oscarBilling.onAccount.formBean.OnAccountPrintFormBean;
import oscar.oscarBilling.onAccount.formBean.OnAccountRefundFormBean;
import oscar.oscarBilling.onAccount.helper.OnAccountHelper;
import oscar.oscarBilling.onAccount.valueObject.OnAccountValueObject;

public class OnAccountBillingAction extends LookupDispatchAction {
    public OnAccountBillingAction() {
    }

    protected Map getKeyMethodMap() {
        HashMap map = new HashMap();
        map.put("demographic.onAccount.onAccountBilling.addDeposit", "addDeposit");
        map.put("demographic.onAccount.onAccountBilling.createBill", "createBill");
        map.put("demographic.onAccount.onAccountBilling.createRefund", "createRefund");
        map.put("demographic.onAccount.onAccountBilling.printHistory", "printHistory");
        map.put("demographic.onAccount.onAccountBilling.applyFilter", "applyFilter");
        map.put("demographic.onAccount.onAccountBilling.resetFilter", "resetFilter");
        return map;
    }

    public ActionForward addDeposit(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {

        OnAccountFormBean bean = (OnAccountFormBean)form;
        OnAccountDepositFormBean depositForm = new OnAccountDepositFormBean();
        depositForm.setDemographic_No(bean.getDemographic_No());
        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        request.getSession().setAttribute("demoNoForOnaccountdeposits", depositForm.getDemographic_No());
        OnAccountHelper.setClinicFormData(depositForm);
        depositForm.setBillingPhysician("n/a");
        OnAccountHelper.setDemographicFormData(depositForm, ((OnAccountFormBean) form).getDemographic_No());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        depositForm.setDate(sdf.format(Calendar.getInstance().getTime()));
        depositForm.setServiceList(new ArrayList());
        depositForm.setPaymentList(new ArrayList());
        request.getSession().setAttribute("onAccountDepositFormBean", depositForm);
        return mapping.findForward("deposit");
    }

    public ActionForward createBill(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {
        OnAccountFormBean bean = (OnAccountFormBean)form;
        OnAccountBillFormBean billForm = new OnAccountBillFormBean();
        billForm.setDemographic_No(bean.getDemographic_No());
        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


        request.getSession().setAttribute("demoNoForOnaccountbills", billForm.getDemographic_No());
        OnAccountHelper.setClinicFormData(billForm);
        OnAccountHelper.setDemographicFormData(billForm, ((OnAccountFormBean) form).getDemographic_No());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        billForm.setDate(sdf.format(Calendar.getInstance().getTime()));
        billForm.setChargeList(new ArrayList());
        request.getSession().setAttribute("onAccountBillFormBean", billForm);
        return mapping.findForward("bill");
    }

    public ActionForward createRefund(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {
        OnAccountFormBean bean = (OnAccountFormBean)form;
        OnAccountRefundFormBean refundForm = new OnAccountRefundFormBean();
        refundForm.setDemographic_No(bean.getDemographic_No());
        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        request.getSession().setAttribute("demoNoForOnaccountrefunds", refundForm.getDemographic_No());
        OnAccountHelper.setClinicFormData(refundForm);
        refundForm.setBillingPhysician("n/a");
        OnAccountHelper.setDemographicFormData(refundForm, bean.getDemographic_No());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        refundForm.setDate(sdf.format(Calendar.getInstance().getTime()));
        refundForm.setRefundList(new ArrayList());
        request.getSession().setAttribute("onAccountRefundFormBean", refundForm);
        return mapping.findForward("refund");
    }

    public ActionForward printHistory(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {
        OnAccountFormBean bean = (OnAccountFormBean)form;
        OnAccountPrintFormBean printForm = new OnAccountPrintFormBean();
        printForm.setDemographic_No(bean.getDemographic_No());

        try {
            OnAccountHelper.setClinicFormData(printForm);
            printForm.setBillingPhysician("n/a");
            OnAccountHelper.setDemographicFormData(printForm, bean.getDemographic_No());

            ArrayList transactions = new ArrayList();
            OnAccountHelper.getBillDetails(bean.getDemographic_No(), transactions);
            OnAccountHelper.getDepositDetails(bean.getDemographic_No(), transactions);
            OnAccountHelper.getRefundDetails(bean.getDemographic_No(), transactions);
            printForm.setTransactions(transactions);
            printForm.setAccountBalance(OnAccountHelper.getTotalAccountBalance(bean.getDemographic_No()));
            printForm.setPendingRefunds(OnAccountHelper.getTotalPendingRefunds(bean.getDemographic_No()));
        } catch (Exception var12) {
            var12.printStackTrace();
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        printForm.setDate(sdf.format(Calendar.getInstance().getTime()));
        request.getSession().setAttribute("onAccountPrintFormBean", printForm);
        return mapping.findForward("print");
    }

    public ActionForward resetFilter(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                     HttpServletResponse response) throws Exception {
        OnAccountFormBean bean = (OnAccountFormBean)form;
        int demographicNo = bean.getDemographic_No();

        try {
            ArrayList transactions = new ArrayList();
            OnAccountHelper.getBillDetails(demographicNo, transactions);
            OnAccountHelper.getDepositDetails(demographicNo, transactions);
            OnAccountHelper.getRefundDetails(demographicNo, transactions);
            Collections.sort(transactions);
            bean.setTransactions(transactions);
        } catch (Exception var9) {
            var9.printStackTrace();
        }

        bean.setType(new String[]{"All"});
        bean.setStatus(new String[]{"All"});
        bean.setDateAll(new String[]{"dateAll"});
        bean.setDays(new String[0]);
        bean.setFromDate("");
        bean.setToDate("");
        return mapping.findForward("filter");
    }

    public ActionForward applyFilter(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                     HttpServletResponse response) throws Exception {
        OnAccountFormBean bean = (OnAccountFormBean)form;

        try {
            ArrayList transactionMaster = new ArrayList();
            OnAccountHelper.getBillDetails(bean.getDemographic_No(), transactionMaster);
            OnAccountHelper.getDepositDetails(bean.getDemographic_No(), transactionMaster);
            OnAccountHelper.getRefundDetails(bean.getDemographic_No(), transactionMaster);
            ArrayList transactionsType = new ArrayList();
            if (bean.getType().length == 1) {
                bean.setTransactions(new ArrayList());
                return mapping.findForward("filter");
            }

            Iterator it;
            OnAccountValueObject vo;
            if (bean.getType().length != 2 || !"All".equalsIgnoreCase(bean.getType()[0])
                    && !"All".equalsIgnoreCase(bean.getType()[1])) {
                for(int i = 0; i < bean.getType().length; ++i) {
                    it = transactionMaster.iterator();

                    while(it.hasNext()) {
                        vo = (OnAccountValueObject)it.next();
                        if (bean.getType()[i].equalsIgnoreCase(vo.getTxnType())) {
                            transactionsType.add(vo);
                        }
                    }
                }
            } else {
                transactionsType.addAll(transactionMaster);
            }

            ArrayList transactionStatus = new ArrayList();
            if (bean.getType().length == 1) {
                bean.setTransactions(new ArrayList());
                return mapping.findForward("filter");
            }

            if (bean.getStatus().length != 2 || !"All".equalsIgnoreCase(bean.getStatus()[0])
                    && !"All".equalsIgnoreCase(bean.getStatus()[1])) {
                for(int i = 0; i < bean.getStatus().length; ++i) {
                    it = transactionsType.iterator();

                    while(it.hasNext()) {
                        vo = (OnAccountValueObject)it.next();
                        if ("Refund Pending".equalsIgnoreCase(bean.getStatus()[i])
                                && "Pending".equalsIgnoreCase(vo.getPendingStatus())) {
                            transactionStatus.add(vo);
                        } else if ("Balance Owing".equalsIgnoreCase(bean.getStatus()[i]) && vo.getBalanceOwing() != 0.0) {
                            transactionStatus.add(vo);
                        }

                        if ("Void".equalsIgnoreCase(bean.getStatus()[i]) && "Void".equalsIgnoreCase(vo.getVoidStatus())) {
                            transactionStatus.add(vo);
                        }
                    }
                }
            } else {
                transactionStatus.addAll(transactionsType);
            }

            ArrayList transactionFinal = new ArrayList();
            if (bean.getDateAll().length == 2) {
                transactionFinal = transactionStatus;
            } else if (!StringUtils.isEmpty(bean.getFromDate()) && !StringUtils.isEmpty(bean.getToDate())) {
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date fromDate = formatter.parse(bean.getFromDate());
                Date toDate = formatter.parse(bean.getToDate());
                it = transactionStatus.iterator();

                label109:
                while(true) {
                    Date voDate;
                    do {
                        do {
                            if (!it.hasNext()) {
                                break label109;
                            }

                            vo = (OnAccountValueObject)it.next();
                            voDate = formatter.parse(vo.getDate());
                        } while(voDate.compareTo(fromDate) != 0 && voDate.compareTo(fromDate) <= 0);
                    } while(voDate.compareTo(toDate) != 0 && voDate.compareTo(toDate) >= 0);

                    transactionFinal.add(vo);
                }
            } else if (bean.getDays().length > 1) {
                Calendar cal30 = null;
                Calendar cal60 = null;
                Calendar cal90 = null;

                for(int i = 0; i < bean.getDays().length; ++i) {
                    if ("30".equalsIgnoreCase(bean.getDays()[i])) {
                        cal30 = Calendar.getInstance();
                        cal30.add(5, -30);
                    } else if ("60".equalsIgnoreCase(bean.getDays()[i])) {
                        cal60 = Calendar.getInstance();
                        cal60.add(5, -60);
                    } else if ("90".equalsIgnoreCase(bean.getDays()[i])) {
                        cal90 = Calendar.getInstance();
                        cal90.add(5, -90);
                    }
                }

                it = transactionStatus.iterator();
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

                label128:
                while(true) {
                    Date voDate;
                    do {
                        if (!it.hasNext()) {
                            break label128;
                        }

                        vo = (OnAccountValueObject)it.next();
                        voDate = formatter.parse(vo.getDate());
                    } while((cal30 == null || voDate.compareTo(cal30.getTime()) <= 0) &&
                            (cal60 == null || voDate.compareTo(cal60.getTime()) <= 0) &&
                            (cal90 == null || voDate.compareTo(cal90.getTime()) <= 0));

                    transactionFinal.add(vo);
                }
            }

            bean.setTransactions(transactionFinal);
        } catch (Exception var18) {
            var18.printStackTrace();
        }

        return mapping.findForward("filter");
    }

    private void setCommonFormAttributes(OnAccountFormBean bean) {
        bean.setType(new String[]{"All"});
        bean.setStatus(new String[]{"All"});
        bean.setDateAll(new String[]{"dateAll"});
        bean.setDays(new String[0]);
        bean.setFromDate("");
        bean.setToDate("");
    }
}
