//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.val;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.LookupDispatchAction;
import org.oscarehr.common.dao.BillingServiceDao;
import org.oscarehr.common.dao.OnAccountDepositDao;
import org.oscarehr.common.dao.OnAccountDepositServiceDao;
import org.oscarehr.common.dao.OnAccountPaymentsDao;
import org.oscarehr.common.model.OnAccountDepositService;
import org.oscarehr.common.model.OnAccountPayment;
import org.oscarehr.util.SpringUtils;
import oscar.oscarBilling.onAccount.formBean.OnAccountDepositFormBean;
import oscar.oscarBilling.onAccount.helper.OnAccountHelper;
import oscar.oscarBilling.onAccount.valueObject.OnAccountPaymentValueObject;
import oscar.oscarBilling.onAccount.valueObject.OnAccountServiceValueObject;

public class OnAccountDepositAction extends LookupDispatchAction {
    public OnAccountDepositAction() {
    }

    protected Map getKeyMethodMap() {
        HashMap map = new HashMap();
        map.put("demographic.onAccount.bill.save", "save");
        map.put("demographic.onAccount.bill.saveClose", "saveAndClose");
        map.put("demographic.onAccount.bill.savePrintPreview", "saveAndPrintPreview");
        map.put("demographic.onAccount.bill.cancel", "cancelDeposit");
        map.put("demographic.onAccount.bill.void", "voidDeposit");
        map.put("demographic.onAccount.deposit.addPaymentDetails", "addPaymentDetails");
        map.put("demographic.onAccount.onAccountBilling.addCharge", "addServiceDeposit");
        map.put("demographic.onAccount.deposit.deleteService", "deleteServiceDeposit");
        map.put("demographic.onAccount.deposit.deletePayment", "deletePaymentDetails");
        map.put("demographic.onAccount.onAccountBilling.open", "openDeposit");
        return map;
    }

    public ActionForward openDeposit(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountDepositDao onAccountDepositDao = SpringUtils.getBean(OnAccountDepositDao.class);
        OnAccountDepositServiceDao onAccountDepositServiceDao = SpringUtils.getBean(OnAccountDepositServiceDao.class);
        BillingServiceDao billingServiceDao = SpringUtils.getBean(BillingServiceDao.class);
        OnAccountPaymentsDao onAccountPaymentsDao = SpringUtils.getBean(OnAccountPaymentsDao.class);
        OnAccountDepositFormBean depositForm = (OnAccountDepositFormBean)form;
        int demographicNo = Integer.parseInt(request.getParameter("demographic_no"));
        int invoiceNo = Integer.parseInt(request.getParameter("invoiceNo"));
        depositForm.setDemographic_No(demographicNo);
        request.getSession().setAttribute("demoNoForOnaccountdeposits", demographicNo);

        try {
            OnAccountHelper.setClinicFormData(depositForm);
            depositForm.setBillingPhysician("n/a");
            OnAccountHelper.setDemographicFormData(depositForm, demographicNo);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");

            val onAccountDeposit = onAccountDepositDao.find(invoiceNo);
            if (onAccountDeposit != null) {
                depositForm.setDate(onAccountDeposit.getDate());
                depositForm.setDxCode(onAccountDeposit.getDxCode());
                depositForm.setPrimaryPhysician(onAccountDeposit.getPrimaryPhysician());
                depositForm.setReferringPhysician(onAccountDeposit.getReferringPhysician());
                depositForm.setDepositTotal(onAccountDeposit.getTotalServices());
                depositForm.setPaymentTotal(onAccountDeposit.getTotalDeposits());
                depositForm.setBalanceOwing(onAccountDeposit.getBalance());
                depositForm.setNotes(onAccountDeposit.getNotes());
                depositForm.setDepositStatus(onAccountDeposit.getDepositStatus());
            }


            ArrayList serviceList = new ArrayList();
            val depositServices = onAccountDepositServiceDao.findByInvoiceNo(invoiceNo);
            for (OnAccountDepositService depositService : depositServices) {
                val serviceVo = new OnAccountServiceValueObject();
                serviceVo.setServiceCode(depositService.getServiceCode());
                serviceVo.setAmount(depositService.getAmount());

                val service = billingServiceDao.findByServiceCode(depositService.getServiceCode());
                if (!service.isEmpty()) {
                    serviceVo.setDescription(service.get(0).getDescription());
                }
                serviceList.add(serviceVo);
            }

            depositForm.setServiceList(serviceList);
            ArrayList paymentList = new ArrayList();

            val payments = onAccountPaymentsDao.findByInvoiceNo(invoiceNo);
            for (OnAccountPayment payment : payments) {
                OnAccountPaymentValueObject paymentVO = new OnAccountPaymentValueObject();
                paymentVO.setPaymentAmount(payment.getAmount());

                try {
                    paymentVO.setPaymentDate(sdf1.format(sdf.parse(payment.getDate())));
                } catch (Exception var20) {
                    paymentVO.setPaymentDate(sdf1.format(sdf1.parse(payment.getDate())));
                }

                paymentVO.setPaymentDetails(payment.getDetails());
                paymentVO.setPaymentID(payment.getPaymentId());
                paymentList.add(paymentVO);
            }

            depositForm.setPaymentList(paymentList);
            depositForm.setButtonStatus("");
        } catch (Exception var21) {
            var21.printStackTrace();
        }

        return mapping.findForward("success");
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountDepositFormBean bean = (OnAccountDepositFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getDepositStatus())) {
            bean.setButtonStatus("Voided");
            return mapping.findForward("success");
        } else {
            OnAccountHelper.saveDeposit(bean, request);
            bean.setButtonStatus("Saved");
            return mapping.findForward("success");
        }
    }

    public ActionForward saveAndClose(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountDepositFormBean bean = (OnAccountDepositFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getDepositStatus())) {
            bean.setButtonStatus("Voided");
            return mapping.findForward("success");
        } else {
            OnAccountHelper.saveDeposit(bean, request);
            bean.setButtonStatus("Close");
            return mapping.findForward("success");
        }
    }

    public ActionForward saveAndPrintPreview(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountDepositFormBean bean = (OnAccountDepositFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getDepositStatus())) {
            bean.setButtonStatus("Voided");
            return mapping.findForward("success");
        } else {
            OnAccountHelper.saveDeposit(bean, request);
            bean.setButtonStatus("Print");
            return mapping.findForward("success");
        }
    }

    public ActionForward cancelDeposit(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountPaymentsDao onAccountPaymentsDao = SpringUtils.getBean(OnAccountPaymentsDao.class);
        OnAccountDepositDao onAccountDepositDao = SpringUtils.getBean(OnAccountDepositDao.class);
        OnAccountDepositFormBean bean = (OnAccountDepositFormBean)form;

        try {
            onAccountPaymentsDao.deleteTempPaymentByInvoiceNumber(bean.getInvoiceNo());
            onAccountPaymentsDao.updateStatusByInvoiceNumberAndStatusIn(bean.getInvoiceNo(),
                    Arrays.asList("deleteSave", "deletetemp"),
                    "Saved");
            onAccountDepositDao.deleteTempDepositByInvoiceNumber(bean.getInvoiceNo());

            bean.setButtonStatus("Cancel");
        } catch (Exception var7) {
            var7.printStackTrace();
        }

        return mapping.findForward("success");
    }

    public ActionForward voidDeposit(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountPaymentsDao onAccountPaymentsDao = SpringUtils.getBean(OnAccountPaymentsDao.class);
        OnAccountDepositDao onAccountDepositDao = SpringUtils.getBean(OnAccountDepositDao.class);
        OnAccountDepositServiceDao onAccountDepositServiceDao = SpringUtils.getBean(OnAccountDepositServiceDao.class);
        OnAccountDepositFormBean bean = (OnAccountDepositFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getDepositStatus())) {
            bean.setButtonStatus("VoidedAlready");
            return mapping.findForward("success");
        } else {
            try {
                onAccountDepositDao.setDepositVoidByInvoiceNumber(bean.getInvoiceNo());
                onAccountPaymentsDao.deleteTempPaymentByInvoiceNumber(bean.getInvoiceNo());
                onAccountPaymentsDao.updateStatusByInvoiceNumber(bean.getInvoiceNo(), "Void");
                onAccountDepositServiceDao.updateStatusByInvoiceNumber(bean.getInvoiceNo(), "Void");

                bean.setDepositStatus("Void");
                bean.setButtonStatus("Void");
            } catch (Exception var7) {
                var7.printStackTrace();
            }

            return mapping.findForward("success");
        }
    }

    public ActionForward addServiceDeposit(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        BillingServiceDao billingServiceDao = SpringUtils.getBean(BillingServiceDao.class);
        OnAccountDepositFormBean bean = (OnAccountDepositFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getDepositStatus())) {
            bean.setButtonStatus("VoidedService");
            return mapping.findForward("success");
        } else {
            try {
                new SimpleDateFormat("yyyy-MM-dd");
                ArrayList serviceList = bean.getServiceList();
                OnAccountServiceValueObject serviceVo = new OnAccountServiceValueObject();
                serviceVo.setServiceCode(bean.getServiceCode());
                serviceVo.setAmount(bean.getAmount());
                val service = billingServiceDao.findByServiceCode(serviceVo.getServiceCode());
                if (!service.isEmpty()) {
                    serviceVo.setDescription(service.get(0).getDescription());
                }
                serviceList.add(serviceVo);
                bean.setServiceList(serviceList);
                OnAccountHelper.calculateServiceDeposits(bean, serviceList);
                bean.setButtonStatus("");
            } catch (Exception var12) {
                var12.printStackTrace();
            }

            return mapping.findForward("success");
        }
    }

    public ActionForward addPaymentDetails(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountDepositFormBean bean = (OnAccountDepositFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getDepositStatus())) {
            bean.setButtonStatus("VoidedPayment");
            return mapping.findForward("success");
        } else {
            try {
                ArrayList paymentList = bean.getPaymentList();
                OnAccountPaymentValueObject paymentVO = new OnAccountPaymentValueObject();
                paymentVO.setPaymentID((int)(Math.random() * 1000.0));
                paymentVO.setPaymentDetails(bean.getPaymentType());
                paymentVO.setPaymentAmount(bean.getPaymentAmount());
                paymentVO.setPaymentDate(bean.getPaymentDate());
                paymentList.add(paymentVO);
                bean.setPaymentList(paymentList);
                OnAccountHelper.calculatePayments(bean, paymentList);
                bean.setButtonStatus("");
            } catch (Exception var8) {
                var8.printStackTrace();
            }

            return mapping.findForward("success");
        }
    }

    public ActionForward deleteServiceDeposit(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountDepositFormBean bean = (OnAccountDepositFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getDepositStatus())) {
            bean.setButtonStatus("VoidedDeleteService");
            return mapping.findForward("success");
        } else {
            try {
                ArrayList serviceList = bean.getServiceList();
                Iterator it = serviceList.iterator();

                while(it.hasNext()) {
                    OnAccountServiceValueObject serviceVO = (OnAccountServiceValueObject)it.next();
                    if (bean.getServiceCode().equalsIgnoreCase(serviceVO.getServiceCode())) {
                        serviceList.remove(serviceVO);
                        break;
                    }
                }

                bean.setServiceList(serviceList);
                OnAccountHelper.calculateServiceDeposits(bean, serviceList);
                bean.setButtonStatus("");
            } catch (Exception var10) {
                var10.printStackTrace();
            }

            return mapping.findForward("success");
        }
    }

    public ActionForward deletePaymentDetails(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountDepositFormBean bean = (OnAccountDepositFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getDepositStatus())) {
            bean.setButtonStatus("VoidedDeletePayment");
            return mapping.findForward("success");
        } else {
            try {
                ArrayList paymentList = bean.getPaymentList();
                Iterator it = paymentList.iterator();

                while(it.hasNext()) {
                    OnAccountPaymentValueObject paymentVO = (OnAccountPaymentValueObject)it.next();
                    if (bean.getPaymentID() == paymentVO.getPaymentID()) {
                        paymentList.remove(paymentVO);
                        break;
                    }
                }

                bean.setPaymentList(paymentList);
                OnAccountHelper.calculatePayments(bean, paymentList);
                bean.setButtonStatus("");
            } catch (Exception var9) {
                var9.printStackTrace();
            }

            return mapping.findForward("success");
        }
    }
}
