//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.val;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.LookupDispatchAction;
import org.oscarehr.common.dao.OnAccountRefundDao;
import org.oscarehr.common.dao.OnAccountRefundDetailDao;
import org.oscarehr.common.model.OnAccountRefundDetail;
import org.oscarehr.util.SpringUtils;
import oscar.oscarBilling.onAccount.formBean.OnAccountRefundFormBean;
import oscar.oscarBilling.onAccount.helper.OnAccountHelper;
import oscar.oscarBilling.onAccount.valueObject.OnAccountRefundValueObject;

public class OnAccountRefundAction extends LookupDispatchAction {
    public OnAccountRefundAction() {
    }

    protected Map getKeyMethodMap() {
        HashMap map = new HashMap();
        map.put("demographic.onAccount.bill.save", "save");
        map.put("demographic.onAccount.bill.saveClose", "saveAndClose");
        map.put("demographic.onAccount.bill.savePrintPreview", "saveAndPrintPreview");
        map.put("demographic.onAccount.bill.cancel", "cancelRefund");
        map.put("demographic.onAccount.bill.void", "voidRefund");
        map.put("demographic.onAccount.refund.addRefund", "addRefund");
        map.put("demographic.onAccount.bill.delete", "deleteRefund");
        map.put("demographic.onAccount.onAccountBilling.open", "openRefund");
        return map;
    }

    public ActionForward openRefund(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountRefundDao onAccountRefundDao = SpringUtils.getBean(OnAccountRefundDao.class);
        OnAccountRefundDetailDao onAccountRefundDetailDao = SpringUtils.getBean(OnAccountRefundDetailDao.class);
        OnAccountRefundFormBean refundForm = (OnAccountRefundFormBean)form;
        int demographicNo = Integer.parseInt(request.getParameter("demographic_no"));
        int invoiceNo = Integer.parseInt(request.getParameter("invoiceNo"));
        refundForm.setDemographic_No(demographicNo);
        request.getSession().setAttribute("demoNoForOnaccountrefunds", demographicNo);

        try {
            OnAccountHelper.setClinicFormData(refundForm);
            OnAccountHelper.setDemographicFormData(refundForm, demographicNo);


            val onAccountRefund = onAccountRefundDao.find(invoiceNo);
            refundForm.setDate(onAccountRefund.getDate());
            refundForm.setDxCode(onAccountRefund.getDxCode());
            refundForm.setPrimaryPhysician(onAccountRefund.getPrimaryPhysician());
            refundForm.setReferringPhysician(onAccountRefund.getReferringPhysician());
            refundForm.setTotalAmount(onAccountRefund.getTotalRefund());
            refundForm.setRefundStatus(onAccountRefund.getRefundStatus());
            refundForm.setBillingPhysician(onAccountRefund.getBillingPhysician());
            refundForm.setNotes(onAccountRefund.getNotes());

            if ("Pending".equalsIgnoreCase(refundForm.getRefundStatus())) {
                refundForm.setRefundPending("on");
            }

            ArrayList refundList = new ArrayList();
            val refundDetails = onAccountRefundDetailDao.findByInvoiceNumber(invoiceNo);
            for (OnAccountRefundDetail refundDetail : refundDetails) {
                OnAccountRefundValueObject refundVO = new OnAccountRefundValueObject();
                refundVO.setDate(refundDetail.getDate());
                refundVO.setRefundAmount(refundDetail.getAmount());
                refundVO.setRefundDetails(refundDetail.getDetails());
                refundVO.setRefundID(refundDetail.getId());
                refundList.add(refundVO);
            }

            refundForm.setRefundList(refundList);
            refundForm.setButtonStatus("");
        } catch (Exception var14) {
            var14.printStackTrace();
        }

        return mapping.findForward("success");
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountRefundFormBean bean = (OnAccountRefundFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getRefundStatus())) {
            bean.setButtonStatus("Voided");
            return mapping.findForward("success");
        } else {
            if ("on".equalsIgnoreCase(request.getParameter("refundPending"))) {
                bean.setRefundPending("on");
                bean.setRefundStatus("Pending");
            } else {
                bean.setRefundPending("");
                bean.setRefundStatus("Saved");
            }

            OnAccountHelper.saveRefund(bean, request);
            bean.setButtonStatus("Saved");
            return mapping.findForward("success");
        }
    }

    public ActionForward saveAndClose(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountRefundFormBean bean = (OnAccountRefundFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getRefundStatus())) {
            bean.setButtonStatus("Voided");
            return mapping.findForward("success");
        } else {
            if ("on".equalsIgnoreCase(request.getParameter("refundPending"))) {
                bean.setRefundPending("on");
                bean.setRefundStatus("Pending");
            } else {
                bean.setRefundPending("");
                bean.setRefundStatus("Saved");
            }

            OnAccountHelper.saveRefund(bean, request);
            bean.setButtonStatus("Close");
            return mapping.findForward("success");
        }
    }

    public ActionForward saveAndPrintPreview(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountRefundFormBean bean = (OnAccountRefundFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getRefundStatus())) {
            bean.setButtonStatus("Voided");
            return mapping.findForward("success");
        } else {
            if ("on".equalsIgnoreCase(request.getParameter("refundPending"))) {
                bean.setRefundPending("on");
                bean.setRefundStatus("Pending");
            } else {
                bean.setRefundPending("");
                bean.setRefundStatus("Saved");
            }

            OnAccountHelper.saveRefund(bean, request);
            bean.setButtonStatus("Print");
            return mapping.findForward("success");
        }
    }

    public ActionForward cancelRefund(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountRefundDetailDao onAccountRefundDetailDao = SpringUtils.getBean(OnAccountRefundDetailDao.class);
        OnAccountRefundFormBean bean = (OnAccountRefundFormBean)form;

        try {
            onAccountRefundDetailDao.deleteTempStatusByInvoiceNumber(bean.getInvoiceNo());
            onAccountRefundDetailDao.updateStatusByInvoiceNumberAndStatusIn(bean.getInvoiceNo(), Arrays.asList("deleteSave", "deletetemp"));
            bean.setButtonStatus("Cancel");
        } catch (Exception var7) {
            var7.printStackTrace();
        }

        return mapping.findForward("success");
    }

    public ActionForward voidRefund(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountRefundDao onAccountRefundDao = SpringUtils.getBean(OnAccountRefundDao.class);
        OnAccountRefundDetailDao onAccountRefundDetailDao = SpringUtils.getBean(OnAccountRefundDetailDao.class);
        OnAccountRefundFormBean bean = (OnAccountRefundFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getRefundStatus())) {
            bean.setButtonStatus("VoidedAlready");
            return mapping.findForward("success");
        } else {
            try {
                onAccountRefundDao.setRefundVoidByInvoiceNumber(bean.getInvoiceNo());
                onAccountRefundDetailDao.deleteTempByInvoiceNumber(bean.getInvoiceNo());
                onAccountRefundDetailDao.setStatusByInvoiceNumber(bean.getInvoiceNo(), "Void");

                bean.setRefundStatus("Void");
                bean.setButtonStatus("Void");
            } catch (Exception var7) {
                var7.printStackTrace();
            }

            return mapping.findForward("success");
        }
    }

    public ActionForward addRefund(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountRefundFormBean bean = (OnAccountRefundFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getRefundStatus())) {
            bean.setButtonStatus("VoidedCharge");
            return mapping.findForward("success");
        } else {
            try {
                new SimpleDateFormat("yyyy-MM-dd");
                ArrayList refundList = bean.getRefundList();
                OnAccountRefundValueObject refundVO = new OnAccountRefundValueObject();
                refundVO.setRefundID((int)(Math.random() * 1000.0));
                refundVO.setRefundAmount(bean.getRefundAmount());
                refundVO.setRefundDetails(bean.getRefundDetails());
                refundVO.setDate(bean.getRefundDate());
                refundList.add(refundVO);
                bean.setRefundList(refundList);
                bean.setTotalAmount(bean.getTotalAmount() + bean.getRefundAmount());
                bean.setButtonStatus("");
            } catch (Exception var10) {
                var10.printStackTrace();
            }

            return mapping.findForward("success");
        }
    }

    public ActionForward deleteRefund(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountRefundFormBean bean = (OnAccountRefundFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getRefundStatus())) {
            bean.setButtonStatus("VoidedDelete");
            return mapping.findForward("success");
        } else {
            try {
                ArrayList refundList = bean.getRefundList();
                Iterator it = refundList.iterator();
                double refundAmount = 0.0;

                while(it.hasNext()) {
                    OnAccountRefundValueObject refundVO = (OnAccountRefundValueObject)it.next();
                    if (bean.getRefundID() == refundVO.getRefundID()) {
                        refundAmount = refundVO.getRefundAmount();
                        refundList.remove(refundVO);
                        break;
                    }
                }

                bean.setRefundList(refundList);
                bean.setTotalAmount(bean.getTotalAmount() - refundAmount);
                bean.setButtonStatus("");
            } catch (Exception var11) {
                var11.printStackTrace();
            }

            return mapping.findForward("success");
        }
    }
}
