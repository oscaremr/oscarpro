//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.val;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.LookupDispatchAction;
import org.oscarehr.common.dao.BillingServiceDao;
import org.oscarehr.common.dao.OnAccountBillDao;
import org.oscarehr.common.dao.OnAccountServiceDao;
import org.oscarehr.common.model.OnAccountService;
import org.oscarehr.util.SpringUtils;
import oscar.oscarBilling.onAccount.formBean.OnAccountBillFormBean;
import oscar.oscarBilling.onAccount.helper.OnAccountHelper;
import oscar.oscarBilling.onAccount.valueObject.OnAccountServiceValueObject;

public class OnAccountBillAction extends LookupDispatchAction {
    public OnAccountBillAction() {
    }

    protected Map getKeyMethodMap() {
        HashMap map = new HashMap();
        map.put("demographic.onAccount.bill.save", "save");
        map.put("demographic.onAccount.bill.saveClose", "saveAndClose");
        map.put("demographic.onAccount.bill.savePrintPreview", "saveAndPrintPreview");
        map.put("demographic.onAccount.bill.cancel", "cancelBill");
        map.put("demographic.onAccount.bill.void", "voidBill");
        map.put("demographic.onAccount.onAccountBilling.addCharge", "addCharge");
        map.put("demographic.onAccount.bill.delete", "deleteService");
        map.put("demographic.onAccount.onAccountBilling.open", "openBill");
        return map;
    }

    public ActionForward openBill(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        OnAccountBillDao onAccountBillDao = SpringUtils.getBean(OnAccountBillDao.class);
        OnAccountServiceDao onAccountServiceDao = SpringUtils.getBean(OnAccountServiceDao.class);
        BillingServiceDao billingServiceDao = SpringUtils.getBean(BillingServiceDao.class);

        OnAccountBillFormBean billForm = (OnAccountBillFormBean)form;
        billForm.setSubTotal(0.0);
        int demographicNo = Integer.parseInt(request.getParameter("demographic_no"));
        int invoiceNo = Integer.parseInt(request.getParameter("invoiceNo"));
        billForm.setDemographic_No(demographicNo);
        request.getSession().setAttribute("demoNoForOnaccountbills", demographicNo);

        OnAccountHelper.setClinicFormData(billForm);
        OnAccountHelper.setDemographicFormData(billForm, demographicNo);

        val onAccountBill = onAccountBillDao.find(invoiceNo);
        if (onAccountBill != null) {
            billForm.setDate(onAccountBill.getDate());
            billForm.setDxCode(onAccountBill.getDxCode());
            billForm.setPrimaryPhysician(onAccountBill.getPrimaryPhysician());
            billForm.setReferringPhysician(onAccountBill.getReferringPhysician());
            billForm.setTotalCharges(onAccountBill.getTotalServices());
            billForm.setSubTotal(onAccountBill.getTotalServices());
            billForm.setHst(onAccountBill.getHst());
            billForm.setAmountCharged(onAccountBill.getAmountCharged());
            billForm.setBalanceOwing(onAccountBill.getBalance());
            billForm.setNotes(onAccountBill.getNotes());
            billForm.setBillStatus(onAccountBill.getBillStatus());
        }

        val chargeList = new ArrayList<OnAccountServiceValueObject>();
        val onAccountServices = onAccountServiceDao.findByInvoiceNo(invoiceNo);
        for (OnAccountService onAccountService : onAccountServices) {
            OnAccountServiceValueObject serviceVo = new OnAccountServiceValueObject();
            serviceVo.setServiceCode(onAccountService.getServiceCode());
            serviceVo.setAmount(onAccountService.getAmount());
            val service = billingServiceDao.findByServiceCode(onAccountService.getServiceCode());
            if (!service.isEmpty()) {
                serviceVo.setDescription(service.get(0).getDescription());
            }
            chargeList.add(serviceVo);
        }
        billForm.setChargeList(chargeList);
        billForm.setButtonStatus("");

        return mapping.findForward("success");
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
        OnAccountBillFormBean bean = (OnAccountBillFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getBillStatus())) {
            bean.setButtonStatus("Voided");
        } else {
            OnAccountHelper.saveBill(bean, request);
            bean.setButtonStatus("Saved");
            bean.setBillStatus("Saved");
        }
        return mapping.findForward("success");
    }

    public ActionForward saveAndClose(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                      HttpServletResponse response) throws Exception {
        OnAccountBillFormBean bean = (OnAccountBillFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getBillStatus())) {
            bean.setButtonStatus("Voided");
            return mapping.findForward("success");
        } else {
            OnAccountHelper.saveBill(bean, request);
            bean.setButtonStatus("Close");
            bean.setBillStatus("Saved");
            return mapping.findForward("success");
        }
    }

    public ActionForward saveAndPrintPreview(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                             HttpServletResponse response) throws Exception {
        OnAccountBillFormBean bean = (OnAccountBillFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getBillStatus())) {
            bean.setButtonStatus("Voided");
            return mapping.findForward("success");
        } else {
            OnAccountHelper.saveBill(bean, request);
            bean.setButtonStatus("Print");
            bean.setBillStatus("Saved");
            return mapping.findForward("success");
        }
    }

    public ActionForward cancelBill(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                    HttpServletResponse response) throws Exception {
        OnAccountBillFormBean bean = (OnAccountBillFormBean)form;

        try {
            bean.setButtonStatus("Cancel");
        } catch (Exception var7) {
            var7.printStackTrace();
        }

        return mapping.findForward("success");
    }

    public ActionForward voidBill(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                  HttpServletResponse response) throws Exception {
        OnAccountBillDao onAccountBillDao = SpringUtils.getBean(OnAccountBillDao.class);
        OnAccountServiceDao onAccountServiceDao = SpringUtils.getBean(OnAccountServiceDao.class);
        OnAccountBillFormBean bean = (OnAccountBillFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getBillStatus())) {
            bean.setButtonStatus("VoidedAlready");
            return mapping.findForward("success");
        } else {
            try {
                onAccountBillDao.voidBillByInvoiceNumber(bean.getInvoiceNo());
                onAccountServiceDao.setBillVoidByInvoiceNumber(bean.getInvoiceNo());
                bean.setBillStatus("Void");
                bean.setButtonStatus("Void");
            } catch (Exception var7) {
                var7.printStackTrace();
            }

            return mapping.findForward("success");
        }
    }

    public ActionForward addCharge(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                   HttpServletResponse response) throws Exception {
        BillingServiceDao billingServiceDao = SpringUtils.getBean(BillingServiceDao.class);
        OnAccountBillFormBean bean = (OnAccountBillFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getBillStatus())) {
            bean.setButtonStatus("VoidedCharge");
            return mapping.findForward("success");
        } else {
            try {
                new SimpleDateFormat("yyyy-MM-dd");
                ArrayList chargeList = bean.getChargeList();
                val serviceVo = new OnAccountServiceValueObject();
                serviceVo.setServiceCode(bean.getServiceCode());
                serviceVo.setAmount(bean.getAmount());

                val service = billingServiceDao.findByServiceCode(serviceVo.getServiceCode());
                if (!service.isEmpty()) {
                    serviceVo.setDescription(service.get(0).getDescription());
                }

                chargeList.add(serviceVo);
                bean.setChargeList(chargeList);
                OnAccountHelper.calculateBillCharges(bean, chargeList);
                bean.setButtonStatus("");
            } catch (Exception var12) {
                var12.printStackTrace();
            }

            return mapping.findForward("success");
        }
    }

    public ActionForward deleteService(ActionMapping mapping, ActionForm form, HttpServletRequest request,
                                       HttpServletResponse response) throws Exception {
        OnAccountBillFormBean bean = (OnAccountBillFormBean)form;
        if ("Void".equalsIgnoreCase(bean.getBillStatus())) {
            bean.setButtonStatus("VoidedDelete");
            return mapping.findForward("success");
        } else {
            try {
                ArrayList chargeList = bean.getChargeList();
                Iterator it = chargeList.iterator();
                Double deletedAmount = 0.0;

                while(it.hasNext()) {
                    OnAccountServiceValueObject serviceVO = (OnAccountServiceValueObject)it.next();
                    if (bean.getServiceCode().equalsIgnoreCase(serviceVO.getServiceCode())) {
                        deletedAmount = serviceVO.getAmount();
                        chargeList.remove(serviceVO);
                        break;
                    }
                }

                bean.setChargeList(chargeList);
                OnAccountHelper.calculateDeleteBillCharges(bean, chargeList, deletedAmount);
                bean.setButtonStatus("");
            } catch (Exception var11) {
                var11.printStackTrace();
            }

            return mapping.findForward("success");
        }
    }
}
