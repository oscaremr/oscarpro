//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.formBean;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;

public class OnAccountFormBean extends ActionForm {
    private static final long serialVersionUID = 1L;
    private int demographic_No;
    private double balance;
    private double pendingRefund;
    private double balanceBill;
    private double depositPaymentOwing;
    private String[] type;
    private String[] status;
    private String[] dateAll;
    private String fromDate;
    private String toDate;
    private String[] days;
    private ArrayList transactions;
    private String demoName;

    public OnAccountFormBean() {
    }

    public double getBalanceBill() {
        return this.balanceBill;
    }

    public void setBalanceBill(double balanceBill) {
        this.balanceBill = balanceBill;
    }

    public ArrayList getTransactions() {
        return this.transactions;
    }

    public void setTransactions(ArrayList transactions) {
        this.transactions = transactions;
    }

    public int getDemographic_No() {
        return this.demographic_No;
    }

    public void setDemographic_No(int demographic_No) {
        this.demographic_No = demographic_No;
    }

    public double getBalance() {
        return this.balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getPendingRefund() {
        return this.pendingRefund;
    }

    public void setPendingRefund(double pendingRefund) {
        this.pendingRefund = pendingRefund;
    }

    public String[] getType() {
        return this.type;
    }

    public void setType(String[] type) {
        this.type = type;
    }

    public String[] getStatus() {
        return this.status;
    }

    public void setStatus(String[] status) {
        this.status = status;
    }

    public String[] getDateAll() {
        return this.dateAll;
    }

    public void setDateAll(String[] dateAll) {
        this.dateAll = dateAll;
    }

    public String getFromDate() {
        return this.fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return this.toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String[] getDays() {
        return this.days;
    }

    public void setDays(String[] days) {
        this.days = days;
    }

    public String getDemoName() {
        return this.demoName;
    }

    public void setDemoName(String demoName) {
        this.demoName = demoName;
    }

    public double getDepositPaymentOwing() {
        return this.depositPaymentOwing;
    }

    public void setDepositPaymentOwing(double depositPaymentOwing) {
        this.depositPaymentOwing = depositPaymentOwing;
    }
}
