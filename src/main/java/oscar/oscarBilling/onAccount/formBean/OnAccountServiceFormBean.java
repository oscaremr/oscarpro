//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.formBean;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;

public class OnAccountServiceFormBean extends ActionForm {
    private static final long serialVersionUID = 1L;
    private String serviceCode;
    private String serviceCode1;
    private ArrayList serviceCodeList;
    private ArrayList serviceCodeDetailList;
    private String fromPage;
    private double amount;

    public OnAccountServiceFormBean() {
    }

    public ArrayList getServiceCodeDetailList() {
        return this.serviceCodeDetailList;
    }

    public void setServiceCodeDetailList(ArrayList serviceCodeDetailList) {
        this.serviceCodeDetailList = serviceCodeDetailList;
    }

    public String getFromPage() {
        return this.fromPage;
    }

    public void setFromPage(String fromPage) {
        this.fromPage = fromPage;
    }

    public String getServiceCode() {
        return this.serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public ArrayList getServiceCodeList() {
        return this.serviceCodeList;
    }

    public void setServiceCodeList(ArrayList serviceCodeList) {
        this.serviceCodeList = serviceCodeList;
    }

    public double getAmount() {
        return this.amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getServiceCode1() {
        return this.serviceCode1;
    }

    public void setServiceCode1(String serviceCode1) {
        this.serviceCode1 = serviceCode1;
    }
}
