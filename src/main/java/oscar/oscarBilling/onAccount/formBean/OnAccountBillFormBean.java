//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.formBean;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;
import oscar.oscarBilling.onAccount.valueObject.OnAccountServiceValueObject;

public class OnAccountBillFormBean extends SharedOnAccountFormBean {
    private static final long serialVersionUID = 1L;
    private int invoiceNo;
    private String date;
    private String remitToName;
    private String remitToAddress;
    private String remitToCity;
    private String remitToProvince;
    private String remitToPostal;
    private String remitToPhone;
    private String remitToFax;
    private String patientName;
    private String patientAddress;
    private String patientCity;
    private String patientProvince;
    private String patientPostal;
    private String dxCode;
    private String primaryPhysician;
    private String primaryPhysicianHidden;
    private String billingPhysician;
    private String referringPhysician;
    private String referringPhysicianHidden;
    private double subTotal;
    private double hst;
    private double totalCharges;
    private double amountCharged;
    private double balanceOwing;
    private String notes;
    private int demographic_No;
    private double accountBalance;
    private String serviceCode;
    private double amount;
    private ArrayList<OnAccountServiceValueObject> chargeList;
    private String buttonStatus;
    private String billStatus;

    public OnAccountBillFormBean() {
    }

    public String getBillStatus() {
        return this.billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public String getButtonStatus() {
        return this.buttonStatus;
    }

    public void setButtonStatus(String buttonStatus) {
        this.buttonStatus = buttonStatus;
    }

    public String getPrimaryPhysicianHidden() {
        return this.primaryPhysicianHidden;
    }

    public void setPrimaryPhysicianHidden(String primaryPhysicianHidden) {
        this.primaryPhysicianHidden = primaryPhysicianHidden;
    }

    public String getReferringPhysicianHidden() {
        return this.referringPhysicianHidden;
    }

    public void setReferringPhysicianHidden(String referringPhysicianHidden) {
        this.referringPhysicianHidden = referringPhysicianHidden;
    }

    public ArrayList<OnAccountServiceValueObject> getChargeList() {
        return this.chargeList;
    }

    public void setChargeList(ArrayList chargeList) {
        this.chargeList = chargeList;
    }

    public String getServiceCode() {
        return this.serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public double getAmount() {
        return this.amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPatientCity() {
        return this.patientCity;
    }

    public void setPatientCity(String patientCity) {
        this.patientCity = patientCity;
    }

    public String getPatientProvince() {
        return this.patientProvince;
    }

    public void setPatientProvince(String patientProvince) {
        this.patientProvince = patientProvince;
    }

    public String getPatientPostal() {
        return this.patientPostal;
    }

    public void setPatientPostal(String patientPostal) {
        this.patientPostal = patientPostal;
    }

    public double getAccountBalance() {
        return this.accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public int getDemographic_No() {
        return this.demographic_No;
    }

    public void setDemographic_No(int demographic_No) {
        this.demographic_No = demographic_No;
    }

    public int getInvoiceNo() {
        return this.invoiceNo;
    }

    public void setInvoiceNo(int invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRemitToName() {
        return this.remitToName;
    }

    public void setRemitToName(String remitToName) {
        this.remitToName = remitToName;
    }

    public String getRemitToAddress() {
        return this.remitToAddress;
    }

    public void setRemitToAddress(String remitToAddress) {
        this.remitToAddress = remitToAddress;
    }

    public String getRemitToCity() {
        return this.remitToCity;
    }

    public void setRemitToCity(String remitToCity) {
        this.remitToCity = remitToCity;
    }

    public String getRemitToProvince() {
        return this.remitToProvince;
    }

    public void setRemitToProvince(String remitToProvince) {
        this.remitToProvince = remitToProvince;
    }

    public String getRemitToPostal() {
        return this.remitToPostal;
    }

    public void setRemitToPostal(String remitToPostal) {
        this.remitToPostal = remitToPostal;
    }

    public String getRemitToPhone() {
        return this.remitToPhone;
    }

    public void setRemitToPhone(String remitToPhone) {
        this.remitToPhone = remitToPhone;
    }

    public String getRemitToFax() {
        return this.remitToFax;
    }

    public void setRemitToFax(String remitToFax) {
        this.remitToFax = remitToFax;
    }

    public String getPatientName() {
        return this.patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientAddress() {
        return this.patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getDxCode() {
        return this.dxCode;
    }

    public void setDxCode(String dxCode) {
        this.dxCode = dxCode;
    }

    public String getPrimaryPhysician() {
        return this.primaryPhysician;
    }

    public void setPrimaryPhysician(String primaryPhysician) {
        this.primaryPhysician = primaryPhysician;
    }

    public String getBillingPhysician() {
        return this.billingPhysician;
    }

    public void setBillingPhysician(String billingPhysician) {
        this.billingPhysician = billingPhysician;
    }

    public String getReferringPhysician() {
        return this.referringPhysician;
    }

    public void setReferringPhysician(String referringPhysician) {
        this.referringPhysician = referringPhysician;
    }

    public double getSubTotal() {
        return this.subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public double getHst() {
        return this.hst;
    }

    public void setHst(double hst) {
        this.hst = hst;
    }

    public double getTotalCharges() {
        return this.totalCharges;
    }

    public void setTotalCharges(double totalCharges) {
        this.totalCharges = totalCharges;
    }

    public double getAmountCharged() {
        return this.amountCharged;
    }

    public void setAmountCharged(double amountCharged) {
        this.amountCharged = amountCharged;
    }

    public double getBalanceOwing() {
        return this.balanceOwing;
    }

    public void setBalanceOwing(double balanceOwing) {
        this.balanceOwing = balanceOwing;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
