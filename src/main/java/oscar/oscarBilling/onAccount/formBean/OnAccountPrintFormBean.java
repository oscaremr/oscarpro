//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.formBean;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;

public class OnAccountPrintFormBean extends SharedOnAccountFormBean {
    private static final long serialVersionUID = 1L;
    private String date;
    private String remitToName;
    private String remitToAddress;
    private String remitToCity;
    private String remitToProvince;
    private String remitToPostal;
    private String remitToPhone;
    private String remitToFax;
    private String patientName;
    private String patientAddress;
    private String patientCity;
    private String patientProvince;
    private String patientPostal;
    private String dxCode;
    private String primaryPhysician;
    private String primaryPhysicianHidden;
    private String billingPhysician;
    private String referringPhysician;
    private String referringPhysicianHidden;
    private int demographic_No;
    private ArrayList transactions;
    private double accountBalance;
    private double pendingRefunds;

    public OnAccountPrintFormBean() {
    }

    public String getPrimaryPhysicianHidden() {
        return this.primaryPhysicianHidden;
    }

    public void setPrimaryPhysicianHidden(String primaryPhysicianHidden) {
        this.primaryPhysicianHidden = primaryPhysicianHidden;
    }

    public String getReferringPhysicianHidden() {
        return this.referringPhysicianHidden;
    }

    public void setReferringPhysicianHidden(String referringPhysicianHidden) {
        this.referringPhysicianHidden = referringPhysicianHidden;
    }

    public String getPatientCity() {
        return this.patientCity;
    }

    public void setPatientCity(String patientCity) {
        this.patientCity = patientCity;
    }

    public String getPatientProvince() {
        return this.patientProvince;
    }

    public void setPatientProvince(String patientProvince) {
        this.patientProvince = patientProvince;
    }

    public String getPatientPostal() {
        return this.patientPostal;
    }

    public void setPatientPostal(String patientPostal) {
        this.patientPostal = patientPostal;
    }

    public double getAccountBalance() {
        return this.accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public int getDemographic_No() {
        return this.demographic_No;
    }

    public void setDemographic_No(int demographic_No) {
        this.demographic_No = demographic_No;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRemitToName() {
        return this.remitToName;
    }

    public void setRemitToName(String remitToName) {
        this.remitToName = remitToName;
    }

    public String getRemitToAddress() {
        return this.remitToAddress;
    }

    public void setRemitToAddress(String remitToAddress) {
        this.remitToAddress = remitToAddress;
    }

    public String getRemitToCity() {
        return this.remitToCity;
    }

    public void setRemitToCity(String remitToCity) {
        this.remitToCity = remitToCity;
    }

    public String getRemitToProvince() {
        return this.remitToProvince;
    }

    public void setRemitToProvince(String remitToProvince) {
        this.remitToProvince = remitToProvince;
    }

    public String getRemitToPostal() {
        return this.remitToPostal;
    }

    public void setRemitToPostal(String remitToPostal) {
        this.remitToPostal = remitToPostal;
    }

    public String getRemitToPhone() {
        return this.remitToPhone;
    }

    public void setRemitToPhone(String remitToPhone) {
        this.remitToPhone = remitToPhone;
    }

    public String getRemitToFax() {
        return this.remitToFax;
    }

    public void setRemitToFax(String remitToFax) {
        this.remitToFax = remitToFax;
    }

    public String getPatientName() {
        return this.patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientAddress() {
        return this.patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getDxCode() {
        return this.dxCode;
    }

    public void setDxCode(String dxCode) {
        this.dxCode = dxCode;
    }

    public String getPrimaryPhysician() {
        return this.primaryPhysician;
    }

    public void setPrimaryPhysician(String primaryPhysician) {
        this.primaryPhysician = primaryPhysician;
    }

    public String getBillingPhysician() {
        return this.billingPhysician;
    }

    public void setBillingPhysician(String billingPhysician) {
        this.billingPhysician = billingPhysician;
    }

    public String getReferringPhysician() {
        return this.referringPhysician;
    }

    public void setReferringPhysician(String referringPhysician) {
        this.referringPhysician = referringPhysician;
    }

    public ArrayList getTransactions() {
        return this.transactions;
    }

    public void setTransactions(ArrayList transactions) {
        this.transactions = transactions;
    }

    public double getPendingRefunds() {
        return this.pendingRefunds;
    }

    public void setPendingRefunds(double pendingRefunds) {
        this.pendingRefunds = pendingRefunds;
    }
}
