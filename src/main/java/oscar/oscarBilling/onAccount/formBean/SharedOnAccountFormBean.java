package oscar.oscarBilling.onAccount.formBean;

import lombok.Getter;
import lombok.Setter;
import org.apache.struts.action.ActionForm;

@Getter
@Setter
public abstract class SharedOnAccountFormBean extends ActionForm {
    private String remitToName;
    private String remitToAddress;
    private String remitToCity;
    private String remitToProvince;
    private String remitToPostal;
    private String remitToPhone;
    private String remitToFax;
    private String patientName;
    private String patientAddress;
    private String patientCity;
    private String patientProvince;
    private String patientPostal;
    private String primaryPhysicianHidden;
    private String referringPhysicianHidden;
    private String billingPhysician;
}
