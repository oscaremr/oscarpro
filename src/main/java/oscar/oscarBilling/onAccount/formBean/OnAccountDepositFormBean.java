//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package oscar.oscarBilling.onAccount.formBean;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;
import oscar.oscarBilling.onAccount.valueObject.OnAccountPaymentValueObject;
import oscar.oscarBilling.onAccount.valueObject.OnAccountServiceValueObject;

public class OnAccountDepositFormBean extends SharedOnAccountFormBean {
    private static final long serialVersionUID = 1L;
    private int invoiceNo;
    private String date;
    private String remitToName;
    private String remitToAddress;
    private String remitToCity;
    private String remitToProvince;
    private String remitToPostal;
    private String remitToPhone;
    private String remitToFax;
    private String patientName;
    private String patientAddress;
    private String patientCity;
    private String patientProvince;
    private String patientPostal;
    private String dxCode;
    private String primaryPhysician;
    private String primaryPhysicianHidden;
    private String billingPhysician;
    private String referringPhysician;
    private String referringPhysicianHidden;
    private double depositTotal;
    private double paymentTotal;
    private double balanceOwing;
    private String notes;
    private int demographic_No;
    private String serviceCode;
    private double amount;
    private ArrayList<OnAccountServiceValueObject> serviceList;
    private ArrayList<OnAccountPaymentValueObject> paymentList;
    private String buttonStatus;
    private String depositStatus;
    private String paymentDate;
    private String paymentType;
    private double paymentAmount;
    private int paymentID;
    private String ccDigits;

    public OnAccountDepositFormBean() {
    }

    public String getCcDigits() {
        return this.ccDigits;
    }

    public void setCcDigits(String ccDigits) {
        this.ccDigits = ccDigits;
    }

    public int getPaymentID() {
        return this.paymentID;
    }

    public void setPaymentID(int paymentID) {
        this.paymentID = paymentID;
    }

    public String getButtonStatus() {
        return this.buttonStatus;
    }

    public void setButtonStatus(String buttonStatus) {
        this.buttonStatus = buttonStatus;
    }

    public String getPrimaryPhysicianHidden() {
        return this.primaryPhysicianHidden;
    }

    public void setPrimaryPhysicianHidden(String primaryPhysicianHidden) {
        this.primaryPhysicianHidden = primaryPhysicianHidden;
    }

    public String getReferringPhysicianHidden() {
        return this.referringPhysicianHidden;
    }

    public void setReferringPhysicianHidden(String referringPhysicianHidden) {
        this.referringPhysicianHidden = referringPhysicianHidden;
    }

    public String getServiceCode() {
        return this.serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public double getAmount() {
        return this.amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPatientCity() {
        return this.patientCity;
    }

    public void setPatientCity(String patientCity) {
        this.patientCity = patientCity;
    }

    public String getPatientProvince() {
        return this.patientProvince;
    }

    public void setPatientProvince(String patientProvince) {
        this.patientProvince = patientProvince;
    }

    public String getPatientPostal() {
        return this.patientPostal;
    }

    public void setPatientPostal(String patientPostal) {
        this.patientPostal = patientPostal;
    }

    public int getDemographic_No() {
        return this.demographic_No;
    }

    public void setDemographic_No(int demographic_No) {
        this.demographic_No = demographic_No;
    }

    public int getInvoiceNo() {
        return this.invoiceNo;
    }

    public void setInvoiceNo(int invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getDate() {
        return this.date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRemitToName() {
        return this.remitToName;
    }

    public void setRemitToName(String remitToName) {
        this.remitToName = remitToName;
    }

    public String getRemitToAddress() {
        return this.remitToAddress;
    }

    public void setRemitToAddress(String remitToAddress) {
        this.remitToAddress = remitToAddress;
    }

    public String getRemitToCity() {
        return this.remitToCity;
    }

    public void setRemitToCity(String remitToCity) {
        this.remitToCity = remitToCity;
    }

    public String getRemitToProvince() {
        return this.remitToProvince;
    }

    public void setRemitToProvince(String remitToProvince) {
        this.remitToProvince = remitToProvince;
    }

    public String getRemitToPostal() {
        return this.remitToPostal;
    }

    public void setRemitToPostal(String remitToPostal) {
        this.remitToPostal = remitToPostal;
    }

    public String getRemitToPhone() {
        return this.remitToPhone;
    }

    public void setRemitToPhone(String remitToPhone) {
        this.remitToPhone = remitToPhone;
    }

    public String getRemitToFax() {
        return this.remitToFax;
    }

    public void setRemitToFax(String remitToFax) {
        this.remitToFax = remitToFax;
    }

    public String getPatientName() {
        return this.patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientAddress() {
        return this.patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getDxCode() {
        return this.dxCode;
    }

    public void setDxCode(String dxCode) {
        this.dxCode = dxCode;
    }

    public String getPrimaryPhysician() {
        return this.primaryPhysician;
    }

    public void setPrimaryPhysician(String primaryPhysician) {
        this.primaryPhysician = primaryPhysician;
    }

    public String getBillingPhysician() {
        return this.billingPhysician;
    }

    public void setBillingPhysician(String billingPhysician) {
        this.billingPhysician = billingPhysician;
    }

    public String getReferringPhysician() {
        return this.referringPhysician;
    }

    public void setReferringPhysician(String referringPhysician) {
        this.referringPhysician = referringPhysician;
    }

    public double getBalanceOwing() {
        return this.balanceOwing;
    }

    public void setBalanceOwing(double balanceOwing) {
        this.balanceOwing = balanceOwing;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public double getDepositTotal() {
        return this.depositTotal;
    }

    public void setDepositTotal(double depositTotal) {
        this.depositTotal = depositTotal;
    }

    public double getPaymentTotal() {
        return this.paymentTotal;
    }

    public void setPaymentTotal(double paymentTotal) {
        this.paymentTotal = paymentTotal;
    }

    public ArrayList<OnAccountServiceValueObject> getServiceList() {
        return this.serviceList;
    }

    public void setServiceList(ArrayList<OnAccountServiceValueObject> serviceList) {
        this.serviceList = serviceList;
    }

    public ArrayList<OnAccountPaymentValueObject> getPaymentList() {
        return this.paymentList;
    }

    public void setPaymentList(ArrayList<OnAccountPaymentValueObject> paymentList) {
        this.paymentList = paymentList;
    }

    public String getDepositStatus() {
        return this.depositStatus;
    }

    public void setDepositStatus(String depositStatus) {
        this.depositStatus = depositStatus;
    }

    public String getPaymentDate() {
        return this.paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentType() {
        return this.paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public double getPaymentAmount() {
        return this.paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }
}
