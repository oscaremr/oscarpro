/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.form.pageUtil;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import oscar.OscarProperties;

public class SmartEncounterUtil {

  static final String INVALID_MIME_TYPE = "invalidMimeType";
  static final String INVALID_FILE_PATH = "invalidFilePath";
  static final String INVALID_MIME_TYPE_MESSAGE = "Some images were not attached successfully due to an invalid mime type.";
  static final String INVALID_FILE_PATH_MESSAGE = "Some images were not attached successfully due to an invalid file path.";

  /**
   * Validates that the filepath is within the allowed directory
   * @param filepath The filepath to validate
   * @return         True if the filepath is valid, false otherwise
   */
  static boolean isValidFilePath(String filepath) {
    Path path = Paths.get(filepath).normalize();
    return path.startsWith(
        OscarProperties.getInstance().getProperty("form_smart_encounter_images_path"));
  }

  /**
   * Validates that the file extension is of the format "image/*"
   * @param mimeType The mimetype of the file
   * @return         True if the file extension is valid, false otherwise
   */
  static boolean isValidMimeType(String mimeType) {
    // define the regex pattern to match "image/*" format
    String regexPattern = "^image/.*$";
    Pattern pattern = Pattern.compile(regexPattern);
    Matcher matcher = pattern.matcher(mimeType);
    // check if the file extension matches the pattern
    return matcher.matches();
  }

  /**
   * Creates an error message based on the request attributes or parameters
   * @param request The request to create the error message from
   * @return        The error message
   */
  public static String createErrorMessage(HttpServletRequest request) {
    StringBuilder stringBuilder = new StringBuilder();
    if (request.getAttribute(INVALID_MIME_TYPE) != null || "true".equals(request.getParameter(INVALID_MIME_TYPE))) {
      stringBuilder.append(INVALID_MIME_TYPE_MESSAGE + " ");
    }
    if (request.getAttribute(INVALID_FILE_PATH) != null || "true".equals(request.getParameter(INVALID_FILE_PATH))) {
      stringBuilder.append(INVALID_FILE_PATH_MESSAGE);
    }
    return stringBuilder.toString();
  }
}
