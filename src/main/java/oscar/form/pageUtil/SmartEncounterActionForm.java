/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.form.pageUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import lombok.val;
import lombok.var;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.util.SpringUtils;
import oscar.form.dao.FormStringValueDao;
import oscar.form.model.FormSmartEncounter;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import oscar.form.model.FormStringValue;
import oscar.form.model.StringValueForm;

@Log4j
public class SmartEncounterActionForm extends ActionForm {
    private Integer formId;
    private Integer demographicNo;
    private String providerNo;
    private Integer appointmentNo;
    private String documentName;
    private Date formCreated;
    private Timestamp formEdited;
    private String deltaText;
    private String htmlText;
    private String plainText;
    private String headerText;
    private String footerText;
    private Integer templateUsed;
    private String fieldsToPullListString;
    private Map<String, String> placeholderValueMap;
    private String[] images = new String[0];
    private Integer headerId;
    private Integer footerId;
    @Getter
    @Setter
    private String attachments;

    public Integer getHeaderId() {
        return headerId;
    }

    public void setHeaderId(Integer headerId) {
        this.headerId = headerId;
    }

    public Integer getFooterId() {
        return footerId;
    }

    public void setFooterId(Integer footerId) {
        this.footerId = footerId;
    }

    public String getHeaderText() {
        return headerText;
    }

    public String getFooterText() {
        return footerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    public void setFooterText(String footerText) {
        this.footerText = footerText;
    }

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public Integer getDemographicNo() {
        return demographicNo;
    }

    public void setDemographicNo(Integer demographicNo) {
        this.demographicNo = demographicNo;
    }

    public String getProviderNo() {
        return providerNo;
    }

    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }

    public Integer getAppointmentNo() {
        return appointmentNo;
    }

    public void setAppointmentNo(Integer appointmentNo) {
        this.appointmentNo = appointmentNo;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public Date getFormCreated() {
        return formCreated;
    }

    public void setFormCreated(Date formCreated) {
        this.formCreated = formCreated;
    }

    public Timestamp getFormEdited() {
        return formEdited;
    }

    public void setFormEdited(Timestamp formEdited) {
        this.formEdited = formEdited;
    }

    public String getDeltaText() {
        return deltaText;
    }

    public void setDeltaText(String deltaText) {
        this.deltaText = deltaText;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public String getPlainText() {
        return plainText;
    }

    public void setPlainText(String plainText) {
        this.plainText = plainText;
    }

    public Integer getTemplateUsed() {
        return templateUsed;
    }

    public void setTemplateUsed(Integer templateUsed) {
        this.templateUsed = templateUsed;
    }

    public String[] getImages() {
        return images;
    }

    public void setImages(String[] images) {
        this.images = images;
    }

    public String getFieldsToPullListString() {
        return fieldsToPullListString;
    }
    public List<String> getFieldsToPullList() {
        List<String> fieldsToPullList = new ArrayList<>();
        if (!StringUtils.isEmpty(fieldsToPullListString)) {
            fieldsToPullList.addAll(Arrays.asList(fieldsToPullListString.split(";")));
        }
        return fieldsToPullList;
    }

    public void setFieldsToPullListString(String fieldsToPullListString) {
        this.fieldsToPullListString = fieldsToPullListString;
    }

    public Map<String, String> getPlaceholderValueMap() {
        return placeholderValueMap;
    }

    public void setPlaceholderValueMap(Object placeholderValueMap) {
        log.info("Object Type:");
        log.info(placeholderValueMap);
        this.placeholderValueMap = (HashMap)placeholderValueMap;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        val placeholderValueMap = new HashMap<String, String>();
        for (var set : request.getParameterMap().entrySet()) {
            if (set.getKey().startsWith("placeholderValueMap")
                    && set.getValue().length == 1) {
                var key = set.getKey();
                var placeholderName = key.substring(key.indexOf('[') + 1, key.length() - 1);
                var placeholderValue = set.getValue()[0];
                placeholderValueMap.put(placeholderName, placeholderValue);
            }
        }
        this.placeholderValueMap = placeholderValueMap;
    }

    public FormSmartEncounter toFormSmartEncounter() {
        FormSmartEncounter newForm = new FormSmartEncounter();
        newForm.setId(null);
        newForm.setDemographicNo(this.getDemographicNo());
        newForm.setProviderNo(this.getProviderNo());
        newForm.setAppointmentNo(this.getAppointmentNo());
        newForm.setDocumentName(this.getDocumentName());
        newForm.setFormCreated(this.getFormCreated() != null ? this.getFormCreated() : new Date());
        newForm.setFormEdited(this.getFormEdited());
        String htmlText = this.getHtmlText()
                .replaceAll("<br>", "<br/>"); // add closing tag to br tags
        newForm.setHtmlText(htmlText);
        newForm.setDeltaText(this.getDeltaText());
        newForm.setPlainText(this.getPlainText());
        newForm.setTemplateUsed(this.getTemplateUsed());
        newForm.setHeaderText(replaceAll(this.getHeaderText()));
        newForm.setFooterText(replaceAll(this.getFooterText()));
        newForm.setHeaderId(this.getHeaderId());
        newForm.setFooterId(this.getFooterId());
        newForm.setAttachments(this.clearGetAttachments(this.getAttachments()));
        return newForm;
    }

    private String clearGetAttachments(final String attachments){
        if (StringUtils.isEmpty(attachments) || attachments.equals("[]")) {
            return null;
        } else{
            return attachments;
        }
    }

    protected Map<String, FormStringValue> getExistingPlaceholderValueMap(FormSmartEncounter form) {
        val existingPlaceholderValueMap = new HashMap<String, FormStringValue>();
        for (Map.Entry<String, String> entry : this.getPlaceholderValueMap().entrySet()) {
            val formStringValue = new FormStringValue(form.getFormTable(), form.getId(), entry.getKey(), entry.getValue());
            existingPlaceholderValueMap.put(entry.getKey(), formStringValue);
        }
        existingPlaceholderValueMap.putAll(getExistingAndUpdatedHeaderAndFooterValueMap(form, this.getPlaceholderValueMap().keySet()));

        return existingPlaceholderValueMap;
    }

    protected Map<String, FormStringValue> getExistingAndUpdatedHeaderAndFooterValueMap(
            FormSmartEncounter form, Set<String> mainFormFields) {
        val headerAndFooterFields = getHeaderAndFooterFields();
        val headerAndFooterValueMap = new HashMap<String, FormStringValue>();
        val fieldsToGetNewValues = new ArrayList<String>();
        FormStringValueDao formStringValueDao = SpringUtils.getBean(FormStringValueDao.class);
        Map<String, FormStringValue> previousFormStringValues = formStringValueDao.findAllForForm(form);

        for (String field : headerAndFooterFields) {
            if (!mainFormFields.contains(field)) {
                if (previousFormStringValues.containsKey(field)) {
                    headerAndFooterValueMap.put(field, previousFormStringValues.get(field));
                } else {
                    fieldsToGetNewValues.add(field);
                }
            }
        }

        Map<String, FormStringValue> newValueMap =
            SmartEncounterPlaceholderResolver.resolveForm(form, fieldsToGetNewValues);
        headerAndFooterValueMap.putAll(newValueMap);
        return headerAndFooterValueMap;
    }

    protected List<String> getHeaderAndFooterFields() {
        List<String> headerAndFooterFields = new ArrayList<>();
        String totalS = headerText + footerText;
        String[] params = totalS.split("\\$");
        for (String para : params) {
            if (para.contains("{")) {
                headerAndFooterFields.add(para.substring(para.indexOf("{") + 1, para.indexOf("}")));
            }
        }
        return headerAndFooterFields;
    }

    private String replaceAll(String value) {
        if (value == null) {
            return "";
        }
        return value;
    }
}
