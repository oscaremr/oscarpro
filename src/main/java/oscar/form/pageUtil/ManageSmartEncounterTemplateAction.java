/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.form.pageUtil;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.apache.tika.Tika;
import org.oscarehr.common.dao.PropertyDao;
import org.oscarehr.common.model.Property;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oscar.OscarProperties;
import oscar.form.dao.SmartEncounterFooterDao;
import oscar.form.dao.SmartEncounterHeaderDao;
import oscar.form.dao.SmartEncounterShortCodeDao;
import oscar.form.dao.SmartEncounterTemplateDao;
import oscar.form.model.SmartEncounterTemplateImage;
import oscar.form.model.SmartEncounterTemplate;
import oscar.form.model.SmartEncounterShortCode;
import oscar.form.model.SmartEncounterHeader;
import oscar.form.model.SmartEncounterFooter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ManageSmartEncounterTemplateAction extends DispatchAction {

    private static Logger logger = LoggerFactory.getLogger(ManageSmartEncounterTemplateAction.class);
    private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
    private SmartEncounterTemplateDao smartEncounterTemplateDao = SpringUtils.getBean(SmartEncounterTemplateDao.class);
    private SmartEncounterShortCodeDao smartEncounterShortCodeDao = SpringUtils.getBean(SmartEncounterShortCodeDao.class);
    private SmartEncounterHeaderDao smartEncounterHeaderDao
            = SpringUtils.getBean(SmartEncounterHeaderDao.class);
    private SmartEncounterFooterDao smartEncounterFooterDao
            = SpringUtils.getBean(SmartEncounterFooterDao.class);
    
    public ActionForward view(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "r", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        
        return mapping.findForward("success");
    }
    
    public ActionForward saveTemplate(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        ManageSmartEncounterTemplateForm formDto = (ManageSmartEncounterTemplateForm) actionForm;
        SmartEncounterTemplate template = formDto.toSmartEncounterTemplate();
        template.setEditedDate(new Date());
        template.setPlaceholderImageList(saveImagesAndPopulateTemplateImages(formDto, request));
        smartEncounterTemplateDao.saveEntity(template);

        return mapping.findForward("success");
    }

    public ActionForward deleteTemplate(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        ManageSmartEncounterTemplateForm form = (ManageSmartEncounterTemplateForm) actionForm;
        Integer templateId = form.getId();
        if (templateId != null) {
            SmartEncounterTemplate template = smartEncounterTemplateDao.find(templateId);
            if (template != null) {
                template.setArchived(true);
                smartEncounterTemplateDao.saveEntity(template);
            }
        }

        return mapping.findForward("success");
    }

    public ActionForward saveShortCode(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        ManageSmartEncounterTemplateForm form = (ManageSmartEncounterTemplateForm) actionForm;
        SmartEncounterShortCode shortCode = form.toSmartEncounterShortCode();
        shortCode.setEditedDate(new Date());
        smartEncounterShortCodeDao.saveEntity(shortCode);

        return mapping.findForward("success");
    }

    public ActionForward saveHeader(ActionMapping mapping, ActionForm actionForm,
            HttpServletRequest request, HttpServletResponse response) {
        if (!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request),
                "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        ManageSmartEncounterTemplateForm form = (ManageSmartEncounterTemplateForm) actionForm;
        SmartEncounterHeader header = form.toSmartEncounterHeader();
        header.setEditedDate(new Date());
        smartEncounterHeaderDao.saveEntity(header);

        return mapping.findForward("success");
    }

    public ActionForward saveFooter(ActionMapping mapping, ActionForm actionForm,
            HttpServletRequest request, HttpServletResponse response) {
        if (!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request),
                "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        ManageSmartEncounterTemplateForm form = (ManageSmartEncounterTemplateForm) actionForm;
        SmartEncounterFooter footer = form.toSmartEncounterFooter();
        footer.setEditedDate(new Date());
        smartEncounterFooterDao.saveEntity(footer);

        return mapping.findForward("success");
    }

    public ActionForward deleteShortCode(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        ManageSmartEncounterTemplateForm form = (ManageSmartEncounterTemplateForm) actionForm;
        Integer shortCodeId = form.getId();
        if (shortCodeId != null) {
            smartEncounterShortCodeDao.remove(shortCodeId);
        }

        return mapping.findForward("success");
    }

    public ActionForward deleteHeader(ActionMapping mapping, ActionForm actionForm,
            HttpServletRequest request, HttpServletResponse response) {
        if (!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request),
                "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        ManageSmartEncounterTemplateForm form = (ManageSmartEncounterTemplateForm) actionForm;
        Integer headerId = form.getId();
        if (headerId != null) {
            smartEncounterHeaderDao.remove(headerId);
        }

        return mapping.findForward("success");
    }

    public ActionForward deleteFooter(ActionMapping mapping, ActionForm actionForm,
            HttpServletRequest request, HttpServletResponse response) {
        if (!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request),
                "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        ManageSmartEncounterTemplateForm form = (ManageSmartEncounterTemplateForm) actionForm;
        Integer footerId = form.getId();
        if (footerId != null) {
            smartEncounterFooterDao.remove(footerId);
        }

        return mapping.findForward("success");
    }

    public ActionForward updateDefaultTemplate(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        String newDefaultTemplate = request.getParameter("defaultTemplate");
        PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
        Property defaultSmartEncounterTemplate = propertyDao.checkByName("default_smart_encounter_template");
        if (defaultSmartEncounterTemplate != null) {
            defaultSmartEncounterTemplate.setValue(newDefaultTemplate);
            propertyDao.merge(defaultSmartEncounterTemplate);
        } else {
            defaultSmartEncounterTemplate = new Property();
            defaultSmartEncounterTemplate.setName("default_smart_encounter_template");
            defaultSmartEncounterTemplate.setValue(newDefaultTemplate);
            propertyDao.persist(defaultSmartEncounterTemplate);
        }
        return mapping.findForward("success");
    }

    public ActionForward updateDefaultHeader(ActionMapping mapping, ActionForm actionForm,
            HttpServletRequest request, HttpServletResponse response) {
        String newDefaultHeader = request.getParameter("defaultHeader");
        PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
        Property defaultSmartEncounterHeader = propertyDao.checkByName("default_smart_encounter_header");
        if (defaultSmartEncounterHeader != null) {
            defaultSmartEncounterHeader.setValue(newDefaultHeader);
            propertyDao.merge(defaultSmartEncounterHeader);
        } else {
            defaultSmartEncounterHeader = new Property();
            defaultSmartEncounterHeader.setName("default_smart_encounter_header");
            defaultSmartEncounterHeader.setValue(newDefaultHeader);
            propertyDao.persist(defaultSmartEncounterHeader);
        }
        return mapping.findForward("success");
    }

    public ActionForward updateDefaultFooter(ActionMapping mapping, ActionForm actionForm,
            HttpServletRequest request, HttpServletResponse response) {
        String newDefaultFooter = request.getParameter("defaultFooter");
        PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
        Property defaultSmartEncounterFooter = propertyDao.checkByName("default_smart_encounter_footer");
        if (defaultSmartEncounterFooter != null) {
            defaultSmartEncounterFooter.setValue(newDefaultFooter);
            propertyDao.merge(defaultSmartEncounterFooter);
        } else {
            defaultSmartEncounterFooter = new Property();
            defaultSmartEncounterFooter.setName("default_smart_encounter_footer");
            defaultSmartEncounterFooter.setValue(newDefaultFooter);
            propertyDao.persist(defaultSmartEncounterFooter);
        }
        return mapping.findForward("success");
    }

    private List<SmartEncounterTemplateImage> saveImagesAndPopulateTemplateImages(
        ManageSmartEncounterTemplateForm formDto,
        HttpServletRequest request
    ) {
        List<SmartEncounterTemplateImage> placeholderImageList = new ArrayList<SmartEncounterTemplateImage>();
        for (String imageData : formDto.getImages()) {
            String[] imageDataArray = imageData.split(";");
            String identifier = imageDataArray[0];

            // already uploaded images have a url instead of data:image/FORMAT;data. skip saving
            if (imageDataArray.length == 3) {
                String base64String = imageDataArray[2];
                // Save and create accessible url for image

                base64String = base64String.replace("base64,", "");
                byte[] decodedImageData = DatatypeConverter.parseBase64Binary(base64String);
                String fileExtension = new Tika().detect(decodedImageData);

                if (!SmartEncounterUtil.isValidMimeType(fileExtension)) {
                    logger.error("Invalid file extension for image");
                    request.setAttribute(SmartEncounterUtil.INVALID_MIME_TYPE, true);
                    continue;
                }

                fileExtension = fileExtension.substring(fileExtension.indexOf('/') + 1);

                String imageFileName = identifier + "." + fileExtension;

                String filepath = OscarProperties.getInstance().getProperty("form_smart_encounter_images_path") + "/" + imageFileName;

                if (!SmartEncounterUtil.isValidFilePath(filepath)) {
                    logger.error("Invalid file path for image");
                    request.setAttribute(SmartEncounterUtil.INVALID_FILE_PATH, true);
                    continue;
                }

                try (FileOutputStream fos = new FileOutputStream(new File(filepath))) {
                    fos.write(decodedImageData);
                } catch (IOException e) {
                    logger.error("Failed to save smart encounter form image", e);
                }

                placeholderImageList.add(new SmartEncounterTemplateImage(identifier, imageFileName));
            }
        }
        return placeholderImageList;
    }
}
