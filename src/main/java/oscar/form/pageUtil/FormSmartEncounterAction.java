/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.form.pageUtil;

import static org.apache.commons.lang.StringUtils.isEmpty;

import ca.oscarpro.common.http.OscarProHttpService;
import ca.oscarpro.common.util.ResponseUtils;
import ca.oscarpro.fax.FaxException;
import ca.oscarpro.fax.FaxFacade;
import ca.oscarpro.fax.FaxFileType;
import ca.oscarpro.fax.SendFaxData;
import ca.oscarpro.service.OscarProConnectorService.OscarProConnectorServiceException;
import com.google.gson.Gson;
import com.lowagie.text.DocumentException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import javax.annotation.Nullable;
import lombok.val;
import net.sf.json.JSONObject;
import net.sf.json.JSONArray;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.apache.struts.actions.DispatchAction;
import org.apache.tika.Tika;
import org.oscarehr.common.dao.ClinicDAO;
import org.oscarehr.common.dao.FaxConfigDao;
import org.oscarehr.common.dao.FaxJobDao;
import org.oscarehr.common.model.FaxConfig;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.DbConnectionFilter;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xhtmlrenderer.layout.SharedContext;
import org.xhtmlrenderer.pdf.ITextRenderer;
import oscar.OscarDocumentCreator;
import oscar.OscarProperties;
import oscar.form.dao.FormSmartEncounterDao;
import oscar.form.dao.SmartEncounterProviderPreferenceDao;
import oscar.form.model.FormSmartEncounter;
import oscar.form.model.SmartEncounterProviderPreference;
import oscar.form.model.FormStringValue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import oscar.form.pageUtil.tagLib.FormSmartEncounterActionHelper;
import oscar.util.ConcatPDF;

public class FormSmartEncounterAction extends DispatchAction {

    private static Logger logger = LoggerFactory.getLogger(FormSmartEncounterAction.class);
    private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
    private FormSmartEncounterDao formSmartEncounterDao = SpringUtils.getBean(FormSmartEncounterDao.class);
    private final OscarProHttpService oscarProHttpService = SpringUtils.getBean(OscarProHttpService.class);
    private static final String PRO_PRINTABLE_PRINT_LIST_URL = "/api/v1/printables/printList";
    private final FormSmartEncounterActionHelper formSmartEncounterActionHelper = new FormSmartEncounterActionHelper();

    private final FaxFacade faxFacade = SpringUtils.getBean(FaxFacade.class);

    public ActionForward view(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "r", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        return mapping.findForward("success");
    }
    
    public ActionForward save(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        SmartEncounterActionForm formDto = (SmartEncounterActionForm) actionForm;
        formDto.setProviderNo(LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo());
        formDto.setFormEdited(new Timestamp(new Date().getTime()));

        return findForward(mapping, "success", saveForm(formDto, request), request);
    }

    public ActionForward print(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request),
            "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        val formDto = (SmartEncounterActionForm) actionForm;
        val encounterFormToPrint = formSmartEncounterDao.find(formDto.getFormId());
        ActionForward forward;
        if (isEmpty(encounterFormToPrint.getAttachments())){
            forward = printForm(mapping, request, response, encounterFormToPrint);
        } else {
            forward = printFormWithAttachments(mapping, request, response, encounterFormToPrint);
        }
        if (forward != null) {
            return forward;
        }
        return mapping.findForward("success");
    }

    public ActionForward saveAndPrint(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request),
                "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        val formDto = (SmartEncounterActionForm) actionForm;
        formDto.setProviderNo(LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo());
        formDto.setFormEdited(new Timestamp(new Date().getTime()));
        return findForward(mapping, "print", saveForm(formDto, request), request);
    }

  public ActionForward fax(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request,
      HttpServletResponse response
  ) throws DocumentException, OscarProConnectorServiceException, FaxException {
        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_admin.fax", "w", null)) {
            throw new SecurityException("missing required security object (_admin.fax)");
        }
        SmartEncounterActionForm formDto = (SmartEncounterActionForm) actionForm;
        formDto.setProviderNo(LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo());
        formDto.setFormEdited(new Timestamp(new Date().getTime()));

        FormSmartEncounter newForm = saveForm(formDto, request);
        if (newForm == null) {
            return formSmartEncounterActionHelper.handleExceptionAndForwardMessageToUser(mapping,
                "Error faxing smart template form, no form exists", newForm);
        }
        String[] destinationFaxNos = request.getParameterValues("faxRecipient");

        ActionForward forward = sendFaxes(newForm, destinationFaxNos, loggedInInfo, mapping, request);
        if (forward != null) {
            return forward;
        }
        return findForward(mapping, "faxSuccess", newForm, request);
    }

  public ActionForward faxAndPrint(ActionMapping mapping, ActionForm actionForm,
      HttpServletRequest request, HttpServletResponse response
  ) throws DocumentException, OscarProConnectorServiceException, FaxException {
        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_admin.fax", "w", null)) {
            throw new SecurityException("missing required security object (_admin.fax)");
        }

        val formDto = (SmartEncounterActionForm) actionForm;
        formDto.setProviderNo(loggedInInfo.getLoggedInProviderNo());
        formDto.setFormEdited(new Timestamp(new Date().getTime()));
        val form = saveForm(formDto, request);
        if (form == null) {
            return formSmartEncounterActionHelper.handleExceptionAndForwardMessageToUser(mapping, "Error faxing smart template form, no form exists", form);
        }

        val destinationFaxNumbers = request.getParameterValues("faxRecipient");
        val byteArrayOutputStream = new ByteArrayOutputStream();
        val forward = sendFaxes(form, destinationFaxNumbers, loggedInInfo, mapping,
            byteArrayOutputStream, request);
        if (forward != null) {
            return forward;
        }
        return findForward(mapping, "print", form, request);
    }
    
    public void getTemplatePlaceHolderValues(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }

        SmartEncounterActionForm formDto = (SmartEncounterActionForm) actionForm;
        formDto.setProviderNo(loggedInInfo.getLoggedInProviderNo());
        
        Map<String, ResolverPlaceholderValue> stringValueMap = SmartEncounterPlaceholderResolver.resolve(formDto.getFieldsToPullList(), formDto.getDemographicNo(), formDto.getProviderNo(), formDto.getAppointmentNo(), formDto.getFormId());
        try {
            JSONArray resultArray = new JSONArray();
            for (Map.Entry<String, ResolverPlaceholderValue> stringEntry : stringValueMap.entrySet()) {
                JSONObject result = new JSONObject();
                result.put("marker", stringEntry.getKey());
                result.put("value", Encode.forHtml(stringEntry.getValue().getValue()));
                result.put("isBlock", stringEntry.getValue().isBlock());
                result.put("noValue", stringEntry.getValue().isNoValue());
                resultArray.add(result);
            }
            JSONObject jsonResponse = new JSONObject();
            jsonResponse.put("resolvedPlaceholders", resultArray);
            jsonResponse.put("success", true);

            response.getOutputStream().write(jsonResponse.toString().getBytes());
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
        } catch (IOException e) { logger.error("Exception when loading placeholder values", e); }
    }

    public ActionForward saveProviderHeaderFooter(ActionMapping mapping, ActionForm actionForm, HttpServletRequest request, HttpServletResponse response) {
        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        
        String headerHtmlText = request.getParameter("headerHtmlText");
        String headerDeltaText = request.getParameter("headerDeltaText");
        String footerHtmlText = request.getParameter("footerHtmlText");
        String footerDeltaText = request.getParameter("footerDeltaText");

        SmartEncounterProviderPreferenceDao smartEncounterProviderPreferenceDao = SpringUtils.getBean(SmartEncounterProviderPreferenceDao.class);
        SmartEncounterProviderPreference smartEncounterProviderPreference = smartEncounterProviderPreferenceDao.findByProviderNo(loggedInInfo.getLoggedInProviderNo());
        if (smartEncounterProviderPreference == null) {
            smartEncounterProviderPreference = new SmartEncounterProviderPreference();
            smartEncounterProviderPreference.setProviderNo(loggedInInfo.getLoggedInProviderNo());
        }
        smartEncounterProviderPreference.setHeaderHtmlText(headerHtmlText);
        smartEncounterProviderPreference.setHeaderDeltaText(headerDeltaText);
        smartEncounterProviderPreference.setFooterHtmlText(footerHtmlText);
        smartEncounterProviderPreference.setFooterDeltaText(footerDeltaText);
        smartEncounterProviderPreferenceDao.saveEntity(smartEncounterProviderPreference);
        
        return mapping.findForward("preferenceSuccess");
    }
    
    protected ActionForward findForward(ActionMapping mapping, String forwardName, FormSmartEncounter form, HttpServletRequest request) {
        ActionRedirect redirect = new ActionRedirect(mapping.findForward(forwardName));
        redirect.addParameter("formId", form.getId());
        redirect.addParameter("demographic_no", form.getDemographicNo());
        redirect.addParameter("appointmentNo", StringUtils.trimToEmpty(String.valueOf(form.getAppointmentNo())));
        String[] destinationFaxNos = request.getParameterValues("faxRecipient");
        if (destinationFaxNos != null && destinationFaxNos.length > 0) {
          redirect.addParameter("faxRecipients", Arrays.asList(destinationFaxNos));
        }
        redirect.addParameter(SmartEncounterUtil.INVALID_MIME_TYPE, request.getAttribute(SmartEncounterUtil.INVALID_MIME_TYPE));
        redirect.addParameter(SmartEncounterUtil.INVALID_FILE_PATH, request.getAttribute(SmartEncounterUtil.INVALID_FILE_PATH));

        if (forwardName.equals("faxSuccess")) {
            redirect.addParameter("faxSuccess", true);
        } else if (forwardName.equals("print")) {
            redirect.addParameter("print", true);
        }
        return redirect;
    }
    
    protected FormSmartEncounter saveForm(SmartEncounterActionForm formDto, HttpServletRequest request) {
        FormSmartEncounter newForm = formDto.toFormSmartEncounter();
        
        // If updating a form, sets date created to original date
        if (formDto.getFormId() != null && formDto.getFormId() > 0) {
            FormSmartEncounter oldForm = formSmartEncounterDao.find(formDto.getFormId());
            newForm.setFormCreated(oldForm.getFormCreated());
        }
        
        // Save images locally
        newForm.setPlaceholderImageMap(saveImagesAndPopulatePlaceholderImageUrlMap(formDto, newForm, request));
        FormSmartEncounter savedSmartEncounterForm = formSmartEncounterDao.saveEntity(newForm);

        // resolve the placeholder
        val fieldsList = formDto.getFieldsToPullList();
        if (formDto.getFormId() != 0) {
            savedSmartEncounterForm.setPlaceholderValueMap(formDto.getExistingPlaceholderValueMap(savedSmartEncounterForm));
        } else {
            fieldsList.addAll(formDto.getHeaderAndFooterFields());
            savedSmartEncounterForm.setPlaceholderValueMap(
                SmartEncounterPlaceholderResolver.resolveForm(savedSmartEncounterForm, fieldsList));
        }
        formSmartEncounterDao.saveEntity(savedSmartEncounterForm);
        return savedSmartEncounterForm;
    }
    
    private Map<String, FormStringValue> saveImagesAndPopulatePlaceholderImageUrlMap(SmartEncounterActionForm formDto, FormSmartEncounter newForm, HttpServletRequest request) {
        Map<String, FormStringValue> placeholderImageUrlMap = new HashMap<String, FormStringValue>();
        for (String imageData : formDto.getImages()) {
            String[] imageDataArray = imageData.split(";");
            String identifier = imageDataArray[0];

            // already uploaded images have a url instead of data:image/FORMAT;data. skip saving
            if (imageDataArray.length == 3) {
                String base64String = imageDataArray[2];
                // Save and create accessible url for image

                base64String = base64String.replace("base64,", "");
                byte[] decodedImageData = DatatypeConverter.parseBase64Binary(base64String);
                String fileExtension = new Tika().detect(decodedImageData);

                if (!SmartEncounterUtil.isValidMimeType(fileExtension)) {
                    logger.error("Invalid file extension for image");
                    request.setAttribute(SmartEncounterUtil.INVALID_MIME_TYPE, true);
                    continue;
                }

                fileExtension = fileExtension.substring(fileExtension.indexOf('/') + 1);

                String imageFileName = identifier + "." + fileExtension;

                String filepath = OscarProperties.getInstance().getProperty("form_smart_encounter_images_path") + "/" + imageFileName;

                if (!SmartEncounterUtil.isValidFilePath(filepath)) {
                    logger.error("Invalid file path for image");
                    request.setAttribute(SmartEncounterUtil.INVALID_FILE_PATH, true);
                    continue;
                }

                try (FileOutputStream fos = new FileOutputStream(new File(filepath))) {
                    fos.write(decodedImageData);
                } catch (IOException e) {
                    logger.error("Failed to save smart encounter form image", e);
                }

                placeholderImageUrlMap.put(identifier, new FormStringValue(newForm.getFormTable(), newForm.getId(), identifier, imageFileName));
            } else if (imageDataArray.length == 2) {
                String imageFileName = imageDataArray[1].substring(imageDataArray[1].indexOf("imagefile=") + 10); // TODO: better way of this
                placeholderImageUrlMap.put(identifier, new FormStringValue(newForm.getFormTable(), newForm.getId(), identifier, imageFileName));
            }
        }
        return placeholderImageUrlMap;
    }

    private ActionForward sendFaxes(FormSmartEncounter form, String[] destinationFaxNos,
        LoggedInInfo loggedInInfo, ActionMapping mapping, final HttpServletRequest request
    ) throws DocumentException, OscarProConnectorServiceException, FaxException {
        return sendFaxes(form, destinationFaxNos, loggedInInfo, mapping, null, request);
    }
    private ActionForward sendFaxes(FormSmartEncounter form, String[] destinationFaxNos,
        LoggedInInfo loggedInInfo, ActionMapping mapping, ByteArrayOutputStream pdfBaos,
        final HttpServletRequest request
    ) throws DocumentException {
        if (destinationFaxNos == null || destinationFaxNos.length == 0) {
            return formSmartEncounterActionHelper.handleExceptionAndForwardMessageToUser(mapping,
                "Error generating smart encounter PDF, no fax recipients provided", form);
        }

        for (int i = 0; i < destinationFaxNos.length; i++) {
            destinationFaxNos[i] = destinationFaxNos[i].trim().replaceAll("[^0-9]", "");
            if (destinationFaxNos[i].length() < 7) {
                return formSmartEncounterActionHelper.handleExceptionAndForwardMessageToUser(
                    mapping,
                    "Document target fax number '" + destinationFaxNos[i] + "' is invalid.", form);
            }
        }
        // Convert to list and remove duplicate phone numbers.
        ArrayList<String> recipients = new ArrayList<String>(new HashSet<String>(Arrays.asList(destinationFaxNos)));

        String tempPath = System.getProperty("java.io.tmpdir");
        String faxClinicId = OscarProperties.getInstance().getProperty("fax_clinic_id", "1234");
        String faxNumber = "";
        ClinicDAO clinicDAO = SpringUtils.getBean(ClinicDAO.class);
        if (!faxClinicId.equals("") && clinicDAO.find(Integer.parseInt(faxClinicId)) != null) {
            faxNumber = clinicDAO.find(Integer.parseInt(faxClinicId)).getClinicFax();
            faxNumber = faxNumber.replaceAll("[^0-9]", "");
        }

        // Generate PDF
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        generatePdf(form, bos);

        FaxJobDao faxJobDao = SpringUtils.getBean(FaxJobDao.class);
        FaxConfigDao faxConfigDao = SpringUtils.getBean(FaxConfigDao.class);
        List<FaxConfig> faxConfigs = faxConfigDao.findAll(null, null);

        val sendFaxData = SendFaxData.builder()
            .providerNumber(loggedInInfo.getLoggedInProviderNo())
            .demographicNumber(form.getDemographicNo())
            .faxFileType(FaxFileType.FORM)
            .cookies(request.getCookies())
            .faxLine(faxNumber)
            .pdfDocumentBytes((pdfBaos == null || pdfBaos.toString().equals(""))
                ? bos.toByteArray() : pdfBaos.toByteArray())
            .loggedInInfo(loggedInInfo)
            .build();

        // Create files in temp directory and create fax entries
        for (int i = 0; i < recipients.size(); i++) {
          String recipientFaxNo = recipients.get(i);
          String tempName = "DOC-" + faxClinicId + form.getId() + "." + i + "." + System.currentTimeMillis();
          sendFaxData.setDestinationFaxNumber(recipientFaxNo);
          sendFaxData.setFileName(tempName);
          try {
            faxFacade.sendFax(sendFaxData);
          } catch (Exception ex) {
            return formSmartEncounterActionHelper.handleExceptionAndForwardMessageToUser(mapping,
                "Exception encountered when sending fax, please contact support", form);
          }
        }
        return null;
    }

@Nullable
    protected ActionForward printForm(ActionMapping mapping, HttpServletRequest request, HttpServletResponse response, FormSmartEncounter smartEncounterForm) {
        val exportPdfJavascript = "this.print()";
        val parameters = new HashMap<String, String>();
        parameters.put("demo", request.getParameter("demographic_no"));

        try(val inputStream =
            getClass().getResourceAsStream("/oscar/form/smartEncounter/smart-encounter-html.jrxml");
            val servletOutputStream = response.getOutputStream()
        ) {
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition",
                formSmartEncounterActionHelper.getHeader(response).toString());
            val osc = new OscarDocumentCreator();
            osc.fillDocumentStream(parameters, servletOutputStream, "pdf", inputStream,
                DbConnectionFilter.getThreadLocalDbConnection(), exportPdfJavascript);
            generatePdf(smartEncounterForm, servletOutputStream);
        } catch (DocumentException | IOException | SQLException e) {
            return formSmartEncounterActionHelper.handleExceptionAndForwardMessageToUser(mapping,
                "Error generating smart encounter PDF", smartEncounterForm);
        }
        return null;
    }

    /**
     * Generates a PDF based on the smart encounter form object given, writing it
     * to the output stream given.
     *
     * @param form A smart encounter form object
     * @param os An output stream
     * @throws DocumentException
     */
    public static void generatePdf(FormSmartEncounter form, OutputStream os)
        throws DocumentException {
        String htmlData = form.getHtmlTextWithValuesFilled();
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(htmlData);
        renderer.layout();
        renderer.createPDF(os);
    }

    @Nullable
    protected ActionForward printFormWithAttachments(final ActionMapping mapping,
        final HttpServletRequest request, final HttpServletResponse response,
        final FormSmartEncounter smartEncounterForm) {
        try(val servletOutputStream = response.getOutputStream()) {
            log.info("Printing smart encounter with attachment");
            response.setContentType("application/pdf");
            response.setHeader("Content-disposition",
                formSmartEncounterActionHelper.getHeader(response).toString());

            String pdfPathFromHtml = File.createTempFile("smartEncounter", ".pdf").getPath();
            generatePdfFromHtml(smartEncounterForm.getHtmlTextWithValuesFilled(), pdfPathFromHtml);

            val filePaths = printAttachmentsAndGetFilePaths(smartEncounterForm.getAttachments(), request);

            // Add pdf form at the top and move others one position
            filePaths.add(0, pdfPathFromHtml);

            val pdfOutputStream = new ByteArrayOutputStream();
            ConcatPDF.concat(filePaths, pdfOutputStream);

            servletOutputStream.write(pdfOutputStream.toByteArray());
        } catch (DocumentException | IOException e) {
            return formSmartEncounterActionHelper.handleExceptionAndForwardMessageToUser(mapping,
                "Error generating smart encounter PDF", smartEncounterForm);
        }
        return null;
    }
    
    void generatePdfFromHtml(final String inputHTML, final String outputPdfPath)
        throws DocumentException, IOException{

        try (OutputStream outputStream = new FileOutputStream(outputPdfPath)) {
            ITextRenderer renderer = new ITextRenderer();
            SharedContext sharedContext = renderer.getSharedContext();
            sharedContext.setPrint(true);
            sharedContext.setInteractive(false);
            renderer.setDocumentFromString(inputHTML);
            renderer.layout();
            renderer.createPDF(outputStream);
        }
    }

    private List printAttachmentsAndGetFilePaths(final String printableJson,
        final HttpServletRequest request)
        throws IOException {
        val responseFromPro = oscarProHttpService.makeLoggedInPostRequestToPro(
            OscarProperties.getOscarProApiUrl() + PRO_PRINTABLE_PRINT_LIST_URL, printableJson, request);
        if (responseFromPro == null || responseFromPro.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            return new ArrayList<>();
        }
        String filePaths = ResponseUtils.readStringDataFromResponseEntityContent(responseFromPro);

        return new Gson().fromJson(filePaths, ArrayList.class);
    }
}
