/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.form.pageUtil;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

import lombok.val;
import lombok.var;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.dao.OscarAppointmentDao;
import org.oscarehr.common.dao.ProfessionalSpecialistDao;
import org.oscarehr.common.model.Appointment;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.ProfessionalSpecialist;
import org.oscarehr.common.model.Provider;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.owasp.encoder.Encode;
import oscar.eform.APExecute;
import oscar.eform.EFormLoader;
import oscar.eform.EFormUtil;
import oscar.eform.data.DatabaseAP;
import oscar.form.model.FormSmartEncounter;
import oscar.form.model.FormStringValue;
import oscar.form.model.SmartEncounterTemplatePlaceholder;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class for accepting SmartEncounterForms and resolves the template values with the relevant demographic, provider, and specialist values
 */
public class SmartEncounterPlaceholderResolver {
    
    private static Logger logger = MiscUtils.getLogger();
    private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final String CONSTANT_EFORM_PREFIX = "eformAp.";
    
    public static Map<String, FormStringValue> resolveForm(FormSmartEncounter form, List<String> fieldsToPullList) {
        Map<String, ResolverPlaceholderValue> stringValueMap = resolve(fieldsToPullList, form.getDemographicNo(), form.getProviderNo(), form.getAppointmentNo(), form.getId());
        HashMap<String, FormStringValue> formStringValueMap = new HashMap<String, FormStringValue>();
        for (Map.Entry<String, ResolverPlaceholderValue> entry : stringValueMap.entrySet()) {
            if (!entry.getValue().isNoValue()) {
                formStringValueMap.put(entry.getKey(), new FormStringValue(form.getFormTable(), form.getId(), entry.getKey(), entry.getValue().getValue()));
            }
        }
        return formStringValueMap;
    }
    
    public static Map<String, ResolverPlaceholderValue> resolve(List<String> fieldsToPullList, Integer demographicNo, String providerNo, Integer appointmentNo, Integer formId) {
        ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
        DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
        OscarAppointmentDao appointmentDao = SpringUtils.getBean(OscarAppointmentDao.class);
        Provider formProvider = providerDao.getProvider(providerNo);
        Provider currentProvider = providerDao.getProvider(providerNo);
        Demographic formDemographic = demographicDao.getDemographic(demographicNo);
        ProfessionalSpecialist familyDoctor = getDemographicLinkedSpecialist(formDemographic, ProfessionalSpecialist.DemographicRelationship.FAMILY_DOCTOR);
        ProfessionalSpecialist referralDoctor = getDemographicLinkedSpecialist(formDemographic, ProfessionalSpecialist.DemographicRelationship.REFERRAL_DOCTOR);
        ArrayList<String> referralProviderPlaceholders = SmartEncounterTemplatePlaceholder.getReferralProviderPlaceholders();
        boolean addReferralFaxData = false;
        Appointment appointment = null;
        if (appointmentNo != null && appointmentNo != 0) {
            appointment = appointmentDao.find(appointmentNo);
        }
        
        HashMap<String, ResolverPlaceholderValue> stringValueMap = new HashMap<String, ResolverPlaceholderValue>();
        for (String subjectAndField : fieldsToPullList) {
            if (referralProviderPlaceholders.contains(subjectAndField)) {
                addReferralFaxData = true;
            }
            if (!stringValueMap.containsKey(subjectAndField)) {
                ResolverPlaceholderValue placeholderValue = new ResolverPlaceholderValue();
                SmartEncounterTemplatePlaceholder placeholder = SmartEncounterTemplatePlaceholder.getPlaceholderSubjectAndField(subjectAndField);
                Method methodToInvoke = SmartEncounterTemplatePlaceholder.getMethodBySubjectAndField(subjectAndField);
                processCustomTagPlaceholderValue(
                    placeholder,
                    subjectAndField,
                    demographicNo,
                    providerNo,
                    appointmentNo,
                    stringValueMap,
                    placeholderValue);
                if (placeholder != null && methodToInvoke != null) {
                    String value = null;
                    try {
                        ProfessionalSpecialist specialist = getRelevantSpecialist(subjectAndField, familyDoctor, referralDoctor);
                        if (methodToInvoke.getDeclaringClass() == APExecute.class) {
                            value = invokeEformApMethod(methodToInvoke, subjectAndField, formDemographic, currentProvider, formId);
                        } else {
                            value = invokeClassMethod(methodToInvoke, formDemographic, formProvider, specialist, appointment);
                        }
                    } catch (InvocationTargetException | IllegalAccessException e) {
                        logger.error("Exception encountered when attempting to resolve smart template placeholder method: " + methodToInvoke.toGenericString());
                    }

                    placeholderValue.setBlock(placeholder.isTextBlock());
                    if (value != null) {
                        value = value.replaceAll("\r", "");
                        
                        if (placeholder.isTextBlock()) {
                            value += "\n";
                        }
                        if (value.length() > 255) {
                            value = value.substring(0, 255);
                        }
                        placeholderValue.setValue(value);
                    } else {
                        placeholderValue.setNoValue(true);
                    }
                    stringValueMap.put(subjectAndField, placeholderValue);
                }
            }
        }
        
        if (referralDoctor != null && addReferralFaxData) { // if referral doc exist add specId and faxNo to values
            stringValueMap.put("demographic.referringPhysicianFullName", new ResolverPlaceholderValue(
                Encode.forHtmlContent(referralDoctor.getFormattedName()), false, false));
            stringValueMap.put("demographic.referringPhysicianSpecId", new ResolverPlaceholderValue(
                Encode.forHtmlContent(String.valueOf(referralDoctor.getId())), false, false));
            if (!StringUtils.isEmpty(referralDoctor.getFaxNumber())) {
                stringValueMap.put("demographic.referringPhysicianFaxNo", new ResolverPlaceholderValue(
                    Encode.forHtmlContent(String.valueOf(referralDoctor.getFaxNumber())), false, false));
            }
        }
        return stringValueMap;
    }

  /** Processes the custom tag placeholder value. */
  private static void processCustomTagPlaceholderValue(
      final SmartEncounterTemplatePlaceholder placeholder,
      final String subjectAndField,
      final Integer demographicNo,
      final String providerNo,
      final Integer appointmentNo,
      final HashMap<String, ResolverPlaceholderValue> stringValueMap,
      final ResolverPlaceholderValue placeholderValue) {
    if (placeholder == null) {
      val customValue =
          getCustomTagValue(subjectAndField, demographicNo, providerNo, appointmentNo);
      if (StringUtils.isNotBlank(customValue)) {
        val isCustomTagTextBlock =
            EFormLoader.getAP(subjectAndField.replace(CONSTANT_EFORM_PREFIX, "")).
                isMultilineResult();
        placeholderValue.setBlock(isCustomTagTextBlock);
        placeholderValue.setValue(customValue);
        stringValueMap.put(subjectAndField, placeholderValue);
      }
    }
  }

  /**
   * Returns the value of the custom tag
   *
   * @param subjectAndField The subject and field mapped string to use
   * @param demographicNumber The demographic number to use
   * @param providerNumber The provider number to use
   * @param appointmentNumber The appointment number to use
   * @return The value of the custom tag
   */
  public static String getCustomTagValue(
      final String subjectAndField,
      final Integer demographicNumber,
      final String providerNumber,
      final Integer appointmentNumber) {
    val regexForCustomTags = "^[^.]*\\.";
    var field = "";
    if (subjectAndField.contains(".")) {
      field = subjectAndField.replaceFirst(regexForCustomTags, "");
    } else {
      field = subjectAndField;
    }
    DatabaseAP ap = EFormLoader.getInstance().getAP(field);
    // Check for a valid custom user tag.
    if (ap == null) {
      return "";
    }
    var sql = ap.getApSQL();
    var output = ap.getApOutput();
    // Check if sql is null
    if (sql == null) {
      return "";
    }
    // Replace placeholders in SQL query
    sql =
        EFormUtil.replaceAllFields(
            sql,
            String.valueOf(demographicNumber),
            providerNumber,
            String.valueOf(appointmentNumber));
    ArrayList<String> names = DatabaseAP.parserGetNames(output);
    sql = DatabaseAP.parserClean(sql);
    ArrayList<String> values = EFormUtil.getValues(names, sql);
    if (values == null || values.size() != names.size()) {
      return "";
    }
    for (int i = 0; i < names.size(); i++) {
      output = DatabaseAP.parserReplace(names.get(i), escapeHtml(values.get(i)), output);
    }
    return output;
  }

    /**
     * Invokes the provided method with the corresponding provided demographic/provider/specialist object
     */
    private static String invokeClassMethod(Method methodToInvoke, Demographic demographic, Provider provider, 
                                      ProfessionalSpecialist professionalSpecialist, Appointment appointment) throws InvocationTargetException, IllegalAccessException {
        if (methodToInvoke.getDeclaringClass() == Demographic.class && demographic != null) {
            return String.valueOf(methodToInvoke.invoke(demographic));
        } else if (methodToInvoke.getDeclaringClass() == Provider.class && provider != null) {
            return String.valueOf(methodToInvoke.invoke(provider));
        } else if (methodToInvoke.getDeclaringClass() == ProfessionalSpecialist.class && professionalSpecialist != null) {
            return String.valueOf(methodToInvoke.invoke(professionalSpecialist));
        } else if (methodToInvoke.getDeclaringClass() == DateFormat.class) {
            return String.valueOf(methodToInvoke.invoke(dateFormat, new Date()));
        } else if (methodToInvoke.getDeclaringClass() == Appointment.class && appointment != null) {
            return String.valueOf(methodToInvoke.invoke(appointment));
        }
        return null;
    }
    /**
     * Invokes the provided method with the eform AP service
     */
    static String invokeEformApMethod(Method methodToInvoke, String subjectAndField, Demographic demographic, Provider currentProvider, Integer formId) throws InvocationTargetException, IllegalAccessException {
        APExecute apExecute = new APExecute();
        String field = subjectAndField.replace("eformAp.", "");
        if (isCurrentFormField(field)) {
            return formId != 0 ? formId.toString() : field + " (populates after save)";
        }
        if (isCurrentUserField(field)) {
            return String.valueOf(methodToInvoke.invoke(apExecute, field, currentProvider.getProviderNo()));
        }
        if (demographic != null) {
            return String.valueOf(methodToInvoke.invoke(apExecute, field, demographic.getDemographicNo().toString()));
        }
        return null;
    }

    static boolean isCurrentFormField(String field) {
        Set<SmartEncounterTemplatePlaceholder> currentFormPlaceholders = SmartEncounterTemplatePlaceholder.getCurrentFormPlaceholders();
        for (SmartEncounterTemplatePlaceholder placeholder : currentFormPlaceholders) {
            if (field.equals(placeholder.getField())) {
                return true;
            }
        }
        return false;
    }

    static boolean isCurrentUserField(String field) {
        Set<SmartEncounterTemplatePlaceholder> currentUserPlaceholders = SmartEncounterTemplatePlaceholder.getCurrentUserPlaceholders();
        for (SmartEncounterTemplatePlaceholder placeholder : currentUserPlaceholders) {
            if (field.equals(placeholder.getField())) {
                return true;
            }
        }
        return false;
    }

    private static ProfessionalSpecialist getDemographicLinkedSpecialist(Demographic demographic, ProfessionalSpecialist.DemographicRelationship demographicRelationship) {
        ProfessionalSpecialistDao professionalSpecialistDao = SpringUtils.getBean(ProfessionalSpecialistDao.class);
        DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
        DemographicExt demographicExt = demographicExtDao.getDemographicExt(demographic.getDemographicNo(), demographicRelationship.getDemographicExtKeyVal());
        ProfessionalSpecialist professionalSpecialist = null;
        if (demographicExt != null) {
            try {
                professionalSpecialist = professionalSpecialistDao.find(Integer.valueOf(demographicExt.getValue()));
            } catch (NumberFormatException e) { logger.warn("Invalid professional specialist id: " + demographicExt.getValue()); }
        }
        return professionalSpecialist;
    }

    /**
     * Determines what professional specialist object should be passed to the invokeMethod() function
     * @param subjectAndField The subject and field mapped string to use
     * @param familyDoctor The demographic's family doctor
     * @param referralDoctor The demographic's referral doctor
     * @return
     */
    private static ProfessionalSpecialist getRelevantSpecialist(String subjectAndField, ProfessionalSpecialist familyDoctor, ProfessionalSpecialist referralDoctor) {
        if (subjectAndField.startsWith("demographic.familyDoctor")) {
            return familyDoctor;
        } else if (subjectAndField.startsWith("demographic.referringPhysician")) {
            return referralDoctor;
        }
        return null;
    }
}
