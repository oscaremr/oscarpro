/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.form.pageUtil;

import lombok.Data;
import org.apache.struts.action.ActionForm;
import oscar.form.model.SmartEncounterFooter;
import oscar.form.util.DateUtil;
import java.util.Date;

@Data
public class SmartFormHeaderFooterForm extends ActionForm {
    private Integer id;
    private String name;
    private Long createDateLong;
    private Date editedDate;
    private String template;
    private String htmlPreview;
    private String smartFormId;

    public SmartEncounterFooter toSmartEncounterFooter() {
        SmartEncounterFooter newFooter = new SmartEncounterFooter();
        newFooter.setId(this.getId() != null && this.getId() != 0 ? this.getId() : null);
        newFooter.setName(this.getName());
        newFooter.setCreateDate(DateUtil.initDate(this.getCreateDateLong()));
        newFooter.setEditedDate(this.getEditedDate());
        newFooter.setText(this.getTemplate());
        newFooter.setHtmlPreview(this.getHtmlPreview());
        return newFooter;
    }
}
