/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.form.pageUtil;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oscar.form.dao.SmartEncounterFooterDao;
import oscar.form.dao.SmartEncounterHeaderDao;
import oscar.form.model.SmartEncounterFooter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

public class SmartFormHeaderFooterAction extends DispatchAction {

    private static Logger logger = LoggerFactory.getLogger(ManageSmartEncounterTemplateAction.class);
    private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
    private SmartEncounterHeaderDao smartEncounterHeaderDao
            = SpringUtils.getBean(SmartEncounterHeaderDao.class);
    private SmartEncounterFooterDao smartEncounterFooterDao
            = SpringUtils.getBean(SmartEncounterFooterDao.class);

    public ActionForward saveFooter(ActionMapping mapping, ActionForm actionForm,
            HttpServletRequest request, HttpServletResponse response) {
        if (!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request),
                "_demographic", "w", null)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        SmartFormHeaderFooterForm form = (SmartFormHeaderFooterForm) actionForm;
        SmartEncounterFooter footer = form.toSmartEncounterFooter();
        footer.setEditedDate(new Date());
        smartEncounterFooterDao.saveEntity(footer);
        return mapping.findForward("success");
    }
}
