/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.form.pageUtil;

import lombok.Data;
import org.apache.struts.action.ActionForm;
import oscar.form.model.SmartEncounterFooter;
import oscar.form.model.SmartEncounterHeader;
import oscar.form.model.SmartEncounterShortCode;
import oscar.form.model.SmartEncounterTemplate;
import oscar.form.util.DateUtil;

import java.util.Date;

@Data
public class ManageSmartEncounterTemplateForm extends ActionForm {

    private Integer id;
    private String name;
    private Long createDateLong;
    private Date editedDate;
    private String template;
    private String htmlPreview;
    private String[] images = new String[0];
    private String defaultTemplateFooter;
    private String defaultTemplateHeader;

    public SmartEncounterTemplate toSmartEncounterTemplate() {
        SmartEncounterTemplate newTemplate = new SmartEncounterTemplate();
        newTemplate.setId(this.getId() != null && this.getId() != 0 ? this.getId() : null);
        newTemplate.setName(this.getName());
        newTemplate.setCreateDate(DateUtil.initDate(this.getCreateDateLong()));
        newTemplate.setEditedDate(this.getEditedDate());
        newTemplate.setTemplate(this.getTemplate());
        newTemplate.setArchived(false);
        newTemplate.setHeaderId(this.getDefaultTemplateHeader());
        newTemplate.setFooterId(this.getDefaultTemplateFooter());
        return newTemplate;
    }

    public SmartEncounterShortCode toSmartEncounterShortCode() {
        SmartEncounterShortCode newShortCode = new SmartEncounterShortCode();
        newShortCode.setId(this.getId() != null && this.getId() != 0 ? this.getId() : null);
        newShortCode.setName(this.getName());
        newShortCode.setCreateDate(DateUtil.initDate(this.getCreateDateLong()));
        newShortCode.setEditedDate(this.getEditedDate());
        newShortCode.setText(this.getTemplate());
        newShortCode.setHtmlPreview(this.getHtmlPreview());
        return newShortCode;
    }

    public SmartEncounterHeader toSmartEncounterHeader() {
        SmartEncounterHeader newHeader = new SmartEncounterHeader();
        newHeader.setId(this.getId() != null && this.getId() != 0 ? this.getId() : null);
        newHeader.setName(this.getName());
        newHeader.setCreateDate(DateUtil.initDate(this.getCreateDateLong()));
        newHeader.setEditedDate(this.getEditedDate());
        newHeader.setText(this.getTemplate());
        newHeader.setHtmlPreview(this.getHtmlPreview());
        return newHeader;
    }

    public SmartEncounterFooter toSmartEncounterFooter() {
        SmartEncounterFooter newFooter = new SmartEncounterFooter();
        newFooter.setId(this.getId() != null && this.getId() != 0 ? this.getId() : null);
        newFooter.setName(this.getName());
        newFooter.setCreateDate(DateUtil.initDate(this.getCreateDateLong()));
        newFooter.setEditedDate(this.getEditedDate());
        newFooter.setText(this.getTemplate());
        newFooter.setHtmlPreview(this.getHtmlPreview());
        return newFooter;
    }
}
