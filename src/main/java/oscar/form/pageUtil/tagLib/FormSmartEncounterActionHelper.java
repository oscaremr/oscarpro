/**
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.form.pageUtil.tagLib;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import oscar.form.model.FormSmartEncounter;

public class FormSmartEncounterActionHelper {

  public StringBuilder getHeader(final HttpServletResponse response) {
    val stringHeader = "SmartEncounter.pdf";
    response.setHeader("Cache-Control", "max-age=0");
    response.setDateHeader("Expires", 0);
    response.setContentType("application/pdf");
    val contentDisplayValue = new StringBuilder();
    contentDisplayValue.append("inline; filename="); //inline - display
    contentDisplayValue.append(stringHeader);
    return contentDisplayValue;
  }

  public void setResponseHeaders(
      final HttpServletResponse response
  ) {
    response.setHeader("Content-disposition", getHeader(response).toString());
  }

  public ActionRedirect generatePdfAndWriteToResponse(
      final HttpServletResponse response,
      final ActionMapping mapping,
      final FormSmartEncounter form,
      final ByteArrayOutputStream byteArrayOutputStream
  ) {
    try {
      setResponseHeaders(response);
      val outputStream = response.getOutputStream();
      byteArrayOutputStream.writeTo(outputStream);
    } catch (IOException e) {
      return handleExceptionAndForwardMessageToUser(mapping,
          "Exception encountered when generating PDF, please contact support", form);
    }
    return null;
  }

  public ActionRedirect handleExceptionAndForwardMessageToUser(
      final ActionMapping mapping,
      final String message,
      final FormSmartEncounter form
  ) {
    val redirect = new ActionRedirect(mapping.findForward("success"));
    redirect.addParameter("formId", form.getId());
    redirect.addParameter("demographic_no", form.getDemographicNo());
    redirect.addParameter("appointment_no", StringUtils.trimToEmpty(String.valueOf(form.getAppointmentNo())));
    redirect.addParameter("error_message", message);
    return redirect;
  }
}
