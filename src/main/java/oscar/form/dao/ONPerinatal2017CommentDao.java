package oscar.form.dao;

import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.dao.AbstractDao;
import org.springframework.stereotype.Repository;
import oscar.form.model.FormONPerinatal2017Comment;

import javax.persistence.Query;
import java.util.List;

@Repository
public class ONPerinatal2017CommentDao extends AbstractDao<FormONPerinatal2017Comment> {
    public ONPerinatal2017CommentDao() {
        super(FormONPerinatal2017Comment.class);
    }

    public List<FormONPerinatal2017Comment> findComments(Integer formId) {
        return findComments(formId, null);
    }

    public List<FormONPerinatal2017Comment> findComments(Integer formId, String fieldNamePrefix) {
        fieldNamePrefix = StringUtils.trimToNull(fieldNamePrefix);
        
        String sql = "select f from FormONPerinatal2017Comment f " +
                "where f.formId = :formId ";
        
        if (fieldNamePrefix != null) {
            sql += "and f.field like :fieldName ";
        }
        
        Query query = entityManager.createQuery(sql);
        query = query.setParameter("formId", formId);
        if (fieldNamePrefix != null) {
            query = query.setParameter("fieldName", fieldNamePrefix + "%");
        }
        return query.getResultList();
    }
}
