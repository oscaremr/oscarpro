package oscar.form.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.Query;
import lombok.val;
import org.oscarehr.common.dao.AbstractDao;
import org.oscarehr.util.LoggedInInfo;
import org.springframework.stereotype.Repository;
import oscar.form.FormAttachmentObject;
import oscar.form.model.FormONPerinatal2017;
import oscar.oscarEncounter.data.EctFormData;

@Repository
public class ONPerinatal2017Dao extends AbstractDao<FormONPerinatal2017> {
    public ONPerinatal2017Dao() {
        super(FormONPerinatal2017.class);
    }
    
    public List<FormONPerinatal2017> findAllDistinctForms(Integer demographicNo) {
        String sql = "select frm from FormONPerinatal2017 frm where frm.demographicNo = :demographicNo group by frm.formId order by frm.formId DESC";
        Query query = entityManager.createQuery(sql);
        query = query.setParameter("demographicNo", demographicNo);
        return query.getResultList();
    }

    public FormONPerinatal2017 findField(Integer formId, String fieldName) {
        String sql = "select f from FormONPerinatal2017 f " +
                "where f.formId = :formId and f.field = :fieldName and f.active = true";
        Query query = entityManager.createQuery(sql);
        query = query.setParameter("formId", formId);
        query = query.setParameter("fieldName", fieldName);

        FormONPerinatal2017 record = null;
        Object result = query.getSingleResult();
        
        if (result != null) {
            record = (FormONPerinatal2017) result;
        }
        
        return record;
    }

    public FormONPerinatal2017 findFieldForPage(Integer formId, Integer pageNo, String fieldName) {
        String sql = "SELECT f FROM FormONPerinatal2017 f " +
                "WHERE f.formId = :formId AND f.pageNo = :pageNo AND f.field = :fieldName AND f.active = TRUE";
        Query query = entityManager.createQuery(sql);
        query = query.setParameter("formId", formId);
        query = query.setParameter("pageNo", pageNo);
        query = query.setParameter("fieldName", fieldName);

        FormONPerinatal2017 record = null;

        List<FormONPerinatal2017> results = query.getResultList();
        if (!results.isEmpty()) {
            record = results.get(0);
        }

        return record;
    }
    
    public List<FormONPerinatal2017> findRecords(Integer formId) {
        String sql = "select f from FormONPerinatal2017 f " +
                "where f.formId = :formId and f.active = true";
        Query query = entityManager.createQuery(sql);
        query = query.setParameter("formId", formId);
        return query.getResultList();
    }

    public List<FormONPerinatal2017> findRecordsByPage(Integer formId, Integer pageNo) {
        String sql = "select f from FormONPerinatal2017 f " +
                "where f.formId = :formId and f.pageNo = :pageNo and f.active = true";
        Query query = entityManager.createQuery(sql);
        query = query.setParameter("formId", formId);
        query = query.setParameter("pageNo", pageNo);
        return query.getResultList();
    }

    /**
     * Checks other pages excluding the pageNo passed for fields.
     * For retrieving sections of a given pageNo, use {@link #findSectionRecords(Integer, Integer, String)}
     * @param formId
     * @param pageNo
     * @param fieldNamePrefix
     * @return 
     */
    public List<FormONPerinatal2017> findSectionRecordsNotForPage(Integer formId, Integer pageNo, String fieldNamePrefix) {
        String sql = "select f from FormONPerinatal2017 f " +
                "where f.formId = :formId and f.pageNo <> :pageNo and f.field like :fieldName and f.active = true";
        Query query = entityManager.createQuery(sql);
        query = query.setParameter("formId", formId);
        query = query.setParameter("pageNo", pageNo);
        query = query.setParameter("fieldName", fieldNamePrefix + "%");
        return query.getResultList();
    }

    /**
     * Get form section records for pageNo.
     * For retrieving sections excluding a given pageNo, use {@link #findSectionRecordsNotForPage(Integer, Integer, String)}
     * @param formId
     * @param pageNo
     * @param fieldNamePrefix
     * @return
     */
    public List<FormONPerinatal2017> findSectionRecords(Integer formId, Integer pageNo, String fieldNamePrefix) {
        String sql = "select f from FormONPerinatal2017 f " +
                "where f.formId = :formId and f.pageNo = :pageNo and f.field like :fieldName and f.active = true";
        Query query = entityManager.createQuery(sql);
        query = query.setParameter("formId", formId);
        query = query.setParameter("pageNo", pageNo);
        query = query.setParameter("fieldName", fieldNamePrefix + "%");
        return query.getResultList();
    }

    public Boolean hasRecordsForPage(Integer formId, Integer pageNo) {
        Boolean hasRecords = false;
        String sql = "select count(f) from FormONPerinatal2017 f " +
                "where f.formId = :formId and f.pageNo = :pageNo and f.active = true";
        Query query = entityManager.createQuery(sql);
        query = query.setParameter("formId", formId);
        query = query.setParameter("pageNo", pageNo);

        List<Long> result = query.getResultList();

        if(result.size()>0 && result.get(0).intValue() > 0) {
            hasRecords = true;
        }
        
        return hasRecords;
    }

    public Integer getNewFormId() {
        Integer id = 1;
        String sql = "select max(frm.formId) from FormONPerinatal2017 frm ";
        Query query = entityManager.createQuery(sql);
        Object result = query.getSingleResult();
        
       
        if (result instanceof Integer) {
            id = ((Integer) result) + 1;
        }
        return id;
    }

    /**
     * Gets the latest form id by the provided demographic number
     * @param demographicNo The demographic number to get the latest form id for
     * @return The latest form id
     */
    public Integer getLatestFormIdByDemographic(Integer demographicNo) {
        Integer latestFormId = null;
        String sql = "select max(frm.formId) from FormONPerinatal2017 frm WHERE frm.demographicNo = :demographicNo";
        Query query = entityManager.createQuery(sql);
        query.setParameter("demographicNo", demographicNo);
        Object result = query.getSingleResult();
        if (result instanceof Integer) {
            latestFormId = (Integer)result;
        }
        
        return latestFormId;
    }

    public List<FormAttachmentObject> findAllFormsAttachedToConsultation (String consultationId) {
        String sql = "select frm.form_id, frm.create_date from form_on_perinatal_2017 as frm left join consultdocs as cd" +
                " on frm.form_id = cd.document_no" +
                " where cd.requestId = ?1 and cd.doctype = 'FP' and cd.deleted IS NULL" +
                " group by frm.form_id";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, consultationId);
        
        List<FormAttachmentObject> formAttachmentObjectList = new ArrayList<FormAttachmentObject>();
        try {
            List<Object[]> resultList = query.getResultList();
            for (Object[] result : resultList) {
                formAttachmentObjectList.add(new FormAttachmentObject((int)result[0], "Perinatal", (Date)result[1]));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formAttachmentObjectList;
    }

  /**
   * 
   * Gets the formID for an older ON AR form, but first checks if a newer ONPerinatal form exists
   * 
   * @param loggedInInfo
   * @param demographicID
   * @return ON AR formId if one exists and no newer Perinatal form exists, else "0"
   */
  public String getArFormIdIfNoPrForm(LoggedInInfo loggedInInfo, String demographicID) {
    if (getLatestFormIdByDemographic(Integer.parseInt(demographicID)) != null) {
      return "0";
    }
    List<EctFormData.PatientForm> formsONAREnhanced = 
      Arrays.asList(EctFormData
        .getPatientFormsFromLocalAndRemote(
          loggedInInfo,
          demographicID,
          "formONAREnhancedRecord",
          true));
    
    return !formsONAREnhanced.isEmpty() ?  formsONAREnhanced.get(0).getFormId() : "0";
  }

  /**
   * Gets the demographic number of a Perinatal form record based on form ID
   *
   * @param formId ID of the form
   * @return Demographic number
   */
  public Integer getDemographicNumberByFormId(final Integer formId) {
      int demographicNumber = 0;
      val sql = "SELECT DISTINCT frm.demographicNo "
          + "FROM FormONPerinatal2017 frm "
          + "WHERE frm.formId = :formId";
      val query = entityManager.createQuery(sql);
      query.setParameter("formId", formId);
      val result = query.getSingleResult();
      if (result instanceof Integer) {
          demographicNumber = (Integer) result;
      }
      return demographicNumber;
  }
}
