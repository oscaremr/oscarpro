package oscar.form.dao;

import org.oscarehr.common.dao.AbstractDao;
import oscar.form.model.FormBCAR2020Text;

import javax.persistence.Query;
import java.util.List;

public class FormBCAR2020TextDao extends AbstractDao<FormBCAR2020Text> {
    public FormBCAR2020TextDao() { super(FormBCAR2020Text.class); }

    public List<FormBCAR2020Text> findFields(Integer formId) {
        String sql = "select f from FormBCAR2020Text f " +
                "where f.formId = :formId";
        Query query = entityManager.createQuery(sql);
        query = query.setParameter("formId", formId);
        return query.getResultList();
    }

    public List<FormBCAR2020Text> findFieldsForPage(Integer formId, Integer pageNo) {
        String sql = "select f from FormBCAR2020Text f " +
                "where f.formId = :formId and (f.pageNo = :pageNo OR f.pageNo = 0)";
        Query query = entityManager.createQuery(sql);
        query = query.setParameter("formId", formId);
        query = query.setParameter("pageNo", pageNo);
        return query.getResultList();
    }
    
    public FormBCAR2020Text findFieldForPage(Integer formId, Integer pageNo, String fieldName) {
        String sql = "SELECT f FROM FormBCAR2020Text f " +
                "WHERE f.formId = :formId AND (f.pageNo = :pageNo OR f.pageNo = 0) AND f.field = :fieldName";
        Query query = entityManager.createQuery(sql);
        query = query.setParameter("formId", formId);
        query = query.setParameter("pageNo", pageNo);
        query = query.setParameter("fieldName", fieldName);

        FormBCAR2020Text record = null;

        List<FormBCAR2020Text> results = query.getResultList();
        if (!results.isEmpty()) {
            record = results.get(0);
        }

        return record;
    }
}
