package oscar.form.dao;

import org.oscarehr.common.dao.AbstractDao;
import org.springframework.stereotype.Repository;
import oscar.form.model.SmartEncounterTemplateImage;

import javax.persistence.Query;
import java.util.List;

@Repository
public class SmartEncounterTemplateImagesDao extends AbstractDao<SmartEncounterTemplateImage> {
    
    public SmartEncounterTemplateImagesDao() {
        super(SmartEncounterTemplateImage.class);
    }

    public List<SmartEncounterTemplateImage> findAllForTemplate(Integer templateId) {
        String sql = "SELECT image FROM SmartEncounterTemplateImage image WHERE image.templateId = :templateId";
        Query query = entityManager.createQuery(sql);
        query.setParameter("templateId", templateId);

        return query.getResultList();
    }
}
