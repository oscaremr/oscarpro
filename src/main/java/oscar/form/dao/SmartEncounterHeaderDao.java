/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.form.dao;

import org.oscarehr.common.dao.AbstractDao;
import org.springframework.stereotype.Repository;
import oscar.form.model.SmartEncounterHeader;
import javax.persistence.Query;
import java.util.List;

@Repository
public class SmartEncounterHeaderDao extends AbstractDao<SmartEncounterHeader> {

    public SmartEncounterHeaderDao() {
        super(SmartEncounterHeader.class);
    }

    public SmartEncounterHeader find(Integer id) {
        return super.find(id);
    }

    public List<SmartEncounterHeader> findAllOrderByName() {
        String sql = "SELECT header FROM SmartEncounterHeader header ORDER BY header.name";
        Query query = entityManager.createQuery(sql);
        return query.getResultList();
    }
}
