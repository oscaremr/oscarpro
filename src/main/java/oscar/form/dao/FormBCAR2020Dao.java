package oscar.form.dao;

import org.oscarehr.common.dao.AbstractDao;
import oscar.form.model.FormBCAR2020;

import javax.persistence.Query;

public class FormBCAR2020Dao extends AbstractDao<FormBCAR2020> {
    public FormBCAR2020Dao() { super(FormBCAR2020.class); }

    public Integer getLatestActiveFormIdByDemographic(Integer demographicNo) {
        Integer latestFormId = null;
        String sql = "select max(frm.formId) from FormBCAR2020 frm WHERE frm.demographicNo = :demographicNo and frm.active = true";
        Query query = entityManager.createQuery(sql);
        query.setParameter("demographicNo", demographicNo);
        Object result = query.getSingleResult();
        if (result instanceof Integer) {
            latestFormId = (Integer)result;
        }

        return latestFormId;
    }
}
