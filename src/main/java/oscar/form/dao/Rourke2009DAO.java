/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */


package oscar.form.dao;

import java.sql.SQLException;
import java.util.List;
import java.util.Properties;
import javax.persistence.Query;
import lombok.val;
import lombok.var;
import org.oscarehr.common.dao.AbstractDao;
import org.springframework.stereotype.Repository;
import oscar.form.FrmRecordHelp;
import oscar.form.model.FormRourke2009;

/**
 *
 * @author rjonasz
 */
@Repository
public class Rourke2009DAO extends AbstractDao<FormRourke2009> {

  public Rourke2009DAO() {
    super(FormRourke2009.class);
  }

  @SuppressWarnings("unchecked")
  public List<FormRourke2009> findAllDistinctForms(Integer demographicNo) {
    String sql = "select frm from FormRourke2009 frm where frm.demographicNo = :demo and frm.id = (select max(frm2.id) from FormRourke2009 frm2 where frm2.formCreated = frm.formCreated and frm2.demographicNo = frm.demographicNo)";
    Query query = entityManager.createQuery(sql);
    query = query.setParameter("demo", demographicNo);
    return query.getResultList();
  }

  /**
   * Count unique records across multiple Rourke2009 form tables using native SQL.
   *
   * @return The count of unique records across the specified tables.
   */
  public long countRecordsInDatabase() {
    String nativeSql = "SELECT (" +
        "SELECT COUNT(DISTINCT ID) FROM formRourke2009_base" +
        ") + (" +
        "SELECT COUNT(DISTINCT ID) FROM formRourke2009_page1" +
        ") + (" +
        "SELECT COUNT(DISTINCT ID) FROM formRourke2009_page2" +
        ") + (" +
        "SELECT COUNT(DISTINCT ID) FROM formRourke2009_page3" +
        ") + (" +
        "SELECT COUNT(DISTINCT ID) FROM formRourke2009_page4" +
        ") AS total_count";
    Object result = entityManager.createNativeQuery(nativeSql).getSingleResult();
    return result != null ? ((Number) result).longValue() : 0;
  }

  public int saveWithTransaction(
      final Properties props,
      final String demographic_no,
      final FrmRecordHelp frmRec
  ) throws SQLException {
    var id = 0;
    try {
      frmRec.setDateFormat("dd/MM/yyyy");
      val baseProps = splitPropertiesByPrefix(props, "");
      val page1Props = splitPropertiesByPrefix(props, "p1_");
      val page2Props = splitPropertiesByPrefix(props, "p2_");
      val page3Props = splitPropertiesByPrefix(props, "p3_");
      val page4Props = splitPropertiesByPrefix(props, "p4_");
      // Save base record
      val baseSql = "SELECT * FROM formRourke2009_base " +
          "WHERE demographic_no=" + demographic_no + " AND ID=0";
      id = frmRec.saveFormRecord(baseProps, baseSql, true);
      // Set ID for each page properties
      setPropertyForPages(id, page1Props, page2Props, page3Props, page4Props);
      // Save each page
      frmRec.saveFormRecord(page1Props, "SELECT * FROM formRourke2009_page1 WHERE ID=" + id, false);
      frmRec.saveFormRecord(page2Props, "SELECT * FROM formRourke2009_page2 WHERE ID=" + id, false);
      frmRec.saveFormRecord(page3Props, "SELECT * FROM formRourke2009_page3 WHERE ID=" + id, false);
      frmRec.saveFormRecord(page4Props, "SELECT * FROM formRourke2009_page4 WHERE ID=" + id, false);
    } catch (Exception ex) {
      if(id != 0) {
        deleteInsertedRecords(id);
      }
      throw ex;
    }
    return id;
  }

  private Properties splitPropertiesByPrefix(Properties props, String prefix) {
    val filteredProps = new Properties();
    for (var e = props.propertyNames(); e.hasMoreElements(); ) {
      val name = (String) e.nextElement();
      if (name.startsWith(prefix)) {
        filteredProps.setProperty(name, props.getProperty(name));
      }
    }
    return filteredProps;
  }

  private void setPropertyForPages(int id, Properties... pages) {
    for (var pageProps : pages) {
      pageProps.setProperty("ID", String.valueOf(id));
    }
  }

  private void deleteInsertedRecords(final int id) {
    entityManager.createNativeQuery("DELETE FROM formRourke2009_page1 WHERE ID = :id")
        .setParameter("id", id)
        .executeUpdate();
    entityManager.createNativeQuery("DELETE FROM formRourke2009_page2 WHERE ID = :id")
        .setParameter("id", id)
        .executeUpdate();
    entityManager.createNativeQuery("DELETE FROM formRourke2009_page3 WHERE ID = :id")
        .setParameter("id", id)
        .executeUpdate();
    entityManager.createNativeQuery("DELETE FROM formRourke2009_page4 WHERE ID = :id")
        .setParameter("id", id)
        .executeUpdate();
    entityManager.createNativeQuery("DELETE FROM formRourke2009_base WHERE ID = :id")
        .setParameter("id", id)
        .executeUpdate();
  }
}
