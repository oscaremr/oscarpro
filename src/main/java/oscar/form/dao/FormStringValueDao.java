/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.form.dao;

import org.oscarehr.common.dao.AbstractDao;
import org.springframework.stereotype.Repository;
import oscar.form.model.FormStringValue;
import oscar.form.model.StringValueForm;

import javax.persistence.Query;
import java.util.HashMap;

@Repository
public class FormStringValueDao extends AbstractDao<FormStringValue> {
    
    public FormStringValueDao() {
        super(FormStringValue.class);
    }
    
    public HashMap<String, FormStringValue> findAllForForm(StringValueForm form) {
        String sql = "SELECT value FROM FormStringValue value WHERE value.id.formName = :formName AND value.id.formId = :formId";
        Query query = entityManager.createQuery(sql);
        query.setParameter("formName", form.getFormTable());
        query.setParameter("formId", form.getId());

        HashMap<String, FormStringValue> results = new HashMap<String, FormStringValue>();
        for (Object o : query.getResultList()) {
            FormStringValue booleanValue = (FormStringValue) o;
            results.put(booleanValue.getId().getFieldName(), booleanValue);
        }
        
        return results;
    } 
}
