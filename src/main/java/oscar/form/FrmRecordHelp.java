/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.form;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.dao.FormDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import oscar.oscarDB.DBHandler;
import oscar.util.JDBCUtil;
import oscar.util.UtilDateUtilities;

@Slf4j
public class FrmRecordHelp {
    private String _dateFormat = "yyyy/MM/dd";
    private String _newDateFormat = "yyyy-MM-dd"; //handles both date formats, but yyyy/MM/dd is displayed to avoid deprecation
    FormDao formDao = SpringUtils.getBean(FormDao.class);
    private final SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);

    private static final HashSet VALID_ACTION_VALUES = new HashSet < String > () {
        {
            add("print");
            add("save");
            add("exit");
            add("graph");
            add("printAll");
            add("printLabReq");
            add("printConsultLetter");
            add("printNewOBConsult");
            add("printMaleConsultLetter");
            add("printIUDTemplate");
            add("printAllJasperReport");
            add("formEpistaxisLetter");
            add("formOtologicLetter");
            add("formSinusLetter");
            add("followUpLetter");
        }
    };

    public void setDateFormat(String s) {
        _dateFormat = s;
    }

    public Properties getFormRecord(String sql) throws SQLException {
        Properties props = new Properties();
        try (val rs = DBHandler.GetSQL(sql)) {
            if (!rs.next()) {
                return props;
            }
            ResultSetMetaData md = rs.getMetaData();
            for (int i = 1; i <= md.getColumnCount(); i++) {
                String name = md.getColumnName(i);
                String value;

                if (md.getColumnTypeName(i).toUpperCase().startsWith("TINYINT") || md.getColumnTypeName(i).equalsIgnoreCase("bit")) {
                    if (rs.getInt(i) == 1)
                        value = "checked='checked'";
                    else
                        value = "";
                } else if (md.getColumnTypeName(i).equalsIgnoreCase("date"))
                    value = UtilDateUtilities.DateToString(rs.getDate(i), _dateFormat);
                else if (md.getColumnTypeName(i).equalsIgnoreCase("timestamp"))
                    value = UtilDateUtilities.DateToString(rs.getTimestamp(i), "yyyy/MM/dd HH:mm:ss");
                else
                    value = oscar.Misc.getString(rs, i);

              props.setProperty(name, value);
            }
        } catch (SQLException e) {
            log.error("Error while retrieving form record, SQL QUERY: {}", sql, e);
            throw e;
        }
        return props;
    }

  public synchronized int saveFormRecord(Properties props, String sql) throws SQLException {
    return saveFormRecord(props, sql, true);
  }

  public synchronized int saveFormRecord(
      final Properties props,
      String sql,
      final Boolean bInsert
  ) throws SQLException {
    var result = 0;
    try (var resultSet = DBHandler.GetSQL(sql, true)) {
      resultSet.moveToInsertRow();
      val updatedResultSet = updateResultSet(props, resultSet, bInsert);
      updatedResultSet.insertRow();
      boolean saveAsXml =
          systemPreferencesDao.isReadBooleanPreferenceWithDefault("enable_save_as_xml", false);
      if (saveAsXml) {
        saveResultSetAsXml(props, sql, updatedResultSet);
      }
    } catch (SQLException e) {
      log.error("Error while saving form record as XML, SQL QUERY: {}", sql, e);
      throw e;
    }
      /*
       * if dataBaseType = mysql return LAST_INSERT_ID() but if dataBaseType = postgresql, return a prepared
       * statement, since here we dont know which sequence will be used
       */
      val dataBaseType =
          OscarProperties.getInstance() != null
              ? OscarProperties.getInstance().getProperty("db_type", "")
              : "";
      if (dataBaseType.isEmpty() || "mysql".equalsIgnoreCase(dataBaseType)) {
        sql = "SELECT LAST_INSERT_ID()";
      } else if ("postgresql".equalsIgnoreCase(dataBaseType)) {
        sql = "SELECT CURRVAL('?')";
      } else {
        throw new SQLException("ERROR: Database " + dataBaseType + " unrecognized.");
      }
      try (val resultSet = DBHandler.GetSQL(sql)) {
      if (!resultSet.next()) {
          return result;
      }
      result = resultSet.getInt(1);
    } catch (SQLException e) {
      log.error("Error while saving form record, SQL QUERY: {}", sql, e);
      throw e;
    }
    return result;
  }

  private void saveResultSetAsXml(final Properties props, final String sql, final ResultSet rs) {
    val index = sql.indexOf("form");
    val now = UtilDateUtilities.DateToString(new Date(), "yyyyMMddHHmmss");
    var formRecordPath = OscarProperties
        .getInstance()
        .getProperty("form_record_path", "/root");
    if (!formRecordPath.endsWith(System.getProperty("file.separator"))) {
      formRecordPath = formRecordPath + System.getProperty("file.separator");
    }
    val formClass = sql.substring(index, sql.indexOf(" ", index));
    val fileName =
        formRecordPath + formClass + "_" + props.getProperty("demographic_no") + "_" + now + ".xml";

    try {
      val doc = JDBCUtil.toDocument(rs);
      JDBCUtil.saveAsXML(doc, fileName);
    } catch (Exception e) {
      log.error("Error while saving form record as XML, FILE NAME: {}", fileName, e);
    }
  }

    /**
     * archived form record by id, using the formTable name to determine form table to update
     *
     * @param tableName
     * @param formId
     */
    public void archiveFormRecord(final String tableName, final String formId) {
        changeArchiveFormRecord(tableName, formId, 1);
    }

    /**
     * unarchived form record by id, using the formTable name to determine form table to update
     *
     * @param tableName
     * @param formId
     */
    public void unarchiveFormRecord(final String tableName, final String formId) {
        changeArchiveFormRecord(tableName, formId, 0);
    }

    /**
     * change archived status form record by id, using the formTable name to determine form table to update
     *
     * @param tableName
     * @param formId
     * @param archiveStatus
     */
    public void changeArchiveFormRecord(final String tableName, final String formId,
        final int archiveStatus) {
      formDao.changeArchiveFormRecord(tableName, formId, archiveStatus);
    }

    public ResultSet updateResultSet(Properties props, ResultSet rs, boolean bInsert) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();

        for (int i = 1; i <= md.getColumnCount(); i++) {
            String name = md.getColumnName(i);
            if (name.equalsIgnoreCase("ID")) {
                rs.updateInt(name, bInsert ? 0 : Integer.parseInt(props.getProperty(name)));
                continue;
            }

            String value = props.getProperty(name, null);

            if (md.getColumnTypeName(i).toUpperCase().startsWith("TINYINT") || md.getColumnTypeName(i).equalsIgnoreCase("bit")) {
                if (value != null) {
                    if (value.equalsIgnoreCase("on") || value.equalsIgnoreCase("checked='checked'")) {
                        rs.updateInt(name, 1);

                    } else {
                        rs.updateInt(name, 0);
                    }
                } else {
                    rs.updateInt(name, 0);
                }
                continue;
            }

            if (md.getColumnTypeName(i).equalsIgnoreCase("date")) {
            java.util.Date d;
                if (md.getColumnName(i).equalsIgnoreCase("formEdited")) {
                    d = new Date();
                } else {
                    if ((value == null) || (value.indexOf('/') != -1))
                        d = UtilDateUtilities.StringToDate(value, _dateFormat);
                    else
                        d = UtilDateUtilities.StringToDate(value, _newDateFormat);
                }
                if (d == null)
                    rs.updateNull(name);
                else
                    rs.updateDate(name, new java.sql.Date(d.getTime()));
                continue;
            }

            if (md.getColumnTypeName(i).equalsIgnoreCase("timestamp")) {
                Date d;
                if (md.getColumnName(i).equalsIgnoreCase("formEdited")) {
                    d = new Date();
                } else {
                    d = UtilDateUtilities.StringToDate(value, "yyyyMMddHHmmss");
                }
                if (d == null)
                    rs.updateNull(name);
                else
                    rs.updateTimestamp(name, new java.sql.Timestamp(d.getTime()));
                continue;
            }

            if (value == null)
                rs.updateNull(name);
            else
                rs.updateString(name, value);
        }

        return rs;
    }

  public Properties getPrintRecord(final String sql) throws SQLException {
    var properties = new Properties();
    try (val resultSet = DBHandler.GetSQL(sql)) {
      if (!resultSet.next()) {
        return properties;
      }
      properties = getResultsAsProperties(resultSet);
    } catch (SQLException e) {
      log.error("Error while retrieving print record, SQL QUERY: {}", sql, e);
      throw e;
    }
    return properties;
  }

  public List<Properties> getPrintRecords(final String sql) throws SQLException {
    val results = new ArrayList<Properties>();
    try (val rs = DBHandler.GetSQL(sql)) {
      while (rs.next()) {
        Properties p = getResultsAsProperties(rs);
        results.add(p);
      }
    } catch (SQLException e) {
      log.error("Error while retrieving print records, SQL QUERY: {}", sql, e);
      throw e;
    }
    return results;
  }

    private Properties getResultsAsProperties(ResultSet rs) throws SQLException
    {
    	Properties p=new Properties();
        ResultSetMetaData md = rs.getMetaData();
        for (int i = 1; i <= md.getColumnCount(); i++) {
            String name = md.getColumnName(i);
            String value;

            if ((md.getColumnTypeName(i).toUpperCase().startsWith("TINYINT") || md.getColumnTypeName(i).equalsIgnoreCase("bit")) && md.getScale(i) == 1) {
                if (rs.getInt(i) == 1)
                    value = "on";
                else
                    value = "off";
            } else if (md.getColumnTypeName(i).equalsIgnoreCase("date"))
                value = UtilDateUtilities.DateToString(rs.getDate(i), _dateFormat);
            else
                value = oscar.Misc.getString(rs, i);

            if (value != null)
                p.setProperty(name, value);
        }

        return(p);
    }

  public List<Integer> getDemographicIds(final String sql) throws SQLException {
    val results = new ArrayList<Integer>();
    try (val rs = DBHandler.GetSQL(sql)) {
      while (rs.next()) {
        results.add(rs.getInt("demographic_no"));
      }
    } catch (SQLException e) {
      log.error("Error while retrieving demographic ids, SQL QUERY: {}", sql, e);
      throw e;
    }
    return results;
  }

    public String findActionValue(String submit)  {
        return VALID_ACTION_VALUES.contains(submit) ? submit : "failure";
    }

    public String createActionURL(String where, String action, String demoId, String formId)  {
        String temp = null;

        if (action.equalsIgnoreCase("print")) {
            temp = where + "?demoNo=" + demoId + "&formId=" + formId; // + "&study_no=" + studyId +
            // "&study_link" + studyLink;
        } else if (action.equalsIgnoreCase("save")) {
            temp = where + "?demographic_no=" + demoId + "&formId=" + formId; // "&study_no=" +
            // studyId +
            // "&study_link" +
            // studyLink; //+
        } else if (action.equalsIgnoreCase("exit")) {
            temp = where;
        } else if (action.equals("printAll")) {
            temp = where + "?demographic_no=" + demoId + "&formId=" + formId;
        }  else if (action.equalsIgnoreCase("printConsultLetter")) {
        	temp = where +  "?formId=" + formId;
        } else if (action.equalsIgnoreCase("printMaleConsultLetter")) {
        	temp = where +  "?formId=" + formId;
        } else if (isOpenHealthCustomForm(action)) {
            temp = where + "?demographic_no=" + demoId + "&formId=" + formId;
        } else {
            temp = where;
        }

        return temp;
    }

    private boolean isOpenHealthCustomForm(String action) {
        return "formEpistaxisLetter".equalsIgnoreCase(action)
                || "formOtologicLetter".equalsIgnoreCase(action)
                || "followUpLetter".equalsIgnoreCase(action)
                || "formSinusLetter".equalsIgnoreCase(action) ;
    }

    public static void convertBooleanToChecked(Properties p)
    {
    	HashSet<Object> keySet=new HashSet<Object>();
    	keySet.addAll(p.keySet());

    	for (Object key : keySet)
    	{
    		String keyName=(String) key;
    		if (keyName.startsWith("b_"))
    		{
    			String value=StringUtils.trimToNull(p.getProperty(keyName));

    			if ("1".equals(value))
    			{
    				p.setProperty(keyName, "checked='checked'");
    			}
    			else
    			{
    				p.setProperty(keyName, "");
    			}
    		}
    	}
    }

    public Date getDateFieldOrNull(Properties props, String fieldName) {
        String value = props.getProperty(fieldName);
        Date result = null;
        if (value != null) {
            try {
                if (value.contains("/")) {
                    result = new SimpleDateFormat(_dateFormat).parse(value);
                } else {
                    result = new SimpleDateFormat(_newDateFormat).parse(value);
                }
            } catch (ParseException e) { /* do nothing, keep result == null */ }
        }
        return result;
    }
    public String parseDateFieldOrNull(Date date) {
        String result = null;
        if (date != null) {
            result = new SimpleDateFormat(_dateFormat).format(date);
        }
        return result;
    }
}
