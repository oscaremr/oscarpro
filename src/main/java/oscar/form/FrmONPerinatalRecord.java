/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.form;

import java.sql.SQLException;
import java.util.*;

import lombok.val;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.util.LoggedInInfo;

import org.oscarehr.util.SpringUtils;
import oscar.form.dao.ONPerinatal2017Dao;
import oscar.form.model.FormONPerinatal2017;
import oscar.util.UtilDateUtilities;

public class FrmONPerinatalRecord extends FrmRecord {
    
    protected String _newDateFormat = "yyyy-MM-dd"; //handles both date formats, but yyyy/MM/dd is displayed to avoid deprecation
    protected List<String> initialLabKeys = Arrays.asList("lab_Hb", "lab_MCV", "lab_ABO", "lab_rh", "lab_antiscr", "lab_rubella", "lab_Hbsag", "lab_syphilis", "lab_hiv", "lab_gc", "lab_chlamydia", "lab_urine");

    public FrmONPerinatalRecord() {
        this.dateFormat = "yyyy/MM/dd";
    }
    
    public Properties getFormRecord(LoggedInInfo loggedInInfo, int demographicNo, int existingID) throws SQLException {
        return getFormRecord(loggedInInfo, demographicNo, existingID, 1);
    }
    
    public Properties getFormRecord(LoggedInInfo loggedInInfo, int demographicNo, int existingID, int pageNo) throws SQLException {
        Properties props = new Properties();
        ONPerinatal2017Dao perinatalDao = SpringUtils.getBean(ONPerinatal2017Dao.class);
        List<FormONPerinatal2017> records = new ArrayList<FormONPerinatal2017>();

        if (existingID <= 0) { 
            this.setDemoProperties(loggedInInfo, demographicNo, props);
            props.setProperty("c_fileNo", StringUtils.trimToEmpty(demographic.getChartNo()));
            props.setProperty("c_lastName", StringUtils.trimToEmpty(demographic.getLastName()));
            props.setProperty("c_firstName", StringUtils.trimToEmpty(demographic.getFirstName()));
            props.setProperty("c_hin", demographic.getHin());
            props.setProperty("c_dateOfBirth", demographic.getFormattedDob());
            props.setProperty("c_hinVer", demographic.getVer());

            if ("ON".equals(demographic.getHcType())) {
                props.setProperty("c_hinType", "OHIP");
            } else if ("QC".equals(demographic.getHcType())) {
                props.setProperty("c_hinType", "RAMQ");
            } else {
                props.setProperty("c_hinType", "OTHER");
            }
            
        } else {
            records.addAll(perinatalDao.findSectionRecordsNotForPage(existingID, pageNo, "c_"));
            records.addAll(perinatalDao.findSectionRecordsNotForPage(existingID, pageNo, "ps_edb_"));
            records.addAll(perinatalDao.findRecordsByPage(existingID, pageNo));
            records.addAll(FrmONPerinatalAction.getCommentsAsRecords(existingID));

            // If demographic info is updated after previous record is saved, update it.
            this.setDemographic(loggedInInfo, demographicNo);
            for (val record : records) {
                setDemographicRecordValues(record);
            }
            
            props = getRecordValuesAsProperties(records);
            
            // If the page is 3 and there isn't an exam weight saved for the page, attempts to get the exam weight from page 2
            if (pageNo == 3) {
                if (!props.containsKey("pe_wt")) {
                    FormONPerinatal2017 weightPg2 = perinatalDao.findFieldForPage(existingID, 2, "pe_wt");
                    if (weightPg2 != null) {
                        props.put(weightPg2.getField(), weightPg2.getValue());
                    }
                }
                
                Properties pg2Labs = getRecordValuesAsProperties(perinatalDao.findSectionRecordsNotForPage(existingID, pageNo, "lab_"));

                Properties rhLabProps = getRecordValuesAsProperties(perinatalDao.findSectionRecordsNotForPage(existingID, pageNo, "lab_rh"));
                if (!rhLabProps.isEmpty() && ("NEG".equals(rhLabProps.getProperty("lab_rh")) || "NEG".equals(rhLabProps.getProperty("lab_rh2")))) {
                    props.put("ri_rhNeg", "checked");
                } else {
                    props.put("ri_rhNeg", "");
                }
                
                if (!props.containsKey("ri_nn_hb")) {
                    Properties hbLabProps = getRecordValuesAsProperties(perinatalDao.findSectionRecordsNotForPage(existingID, pageNo, "lab_Hbsag"));
                    if (!hbLabProps.isEmpty() && "POS".equals(hbLabProps.getProperty("lab_Hbsag"))) {
                        props.put("ri_nn_hb", "checked");
                    }
                }

                if (!props.containsKey("ri_nn_other")) {
                    Properties hbLabProps = getRecordValuesAsProperties(perinatalDao.findSectionRecordsNotForPage(existingID, pageNo, "lab_hiv"));
                    if (!hbLabProps.isEmpty() && "POS".equals(hbLabProps.getProperty("lab_hiv"))) {
                        props.put("ri_nn_other", "checked");
                    }
                }

                boolean initialLabsComplete = true;
                int labIdx = 0;
                while (initialLabsComplete && labIdx < initialLabKeys.size()) {
                    String labKey = initialLabKeys.get(labIdx);
                    initialLabsComplete = pg2Labs.containsKey(labKey) && !"NDONE".equals(pg2Labs.getProperty(labKey));
                    labIdx++;
                }

                props.put("initial_labs_complete", String.valueOf(initialLabsComplete));
            }
        }
        
        if (props.getProperty("pg" + pageNo + "_formDate") == null) {
            props.setProperty("pg" + pageNo + "_formDate", UtilDateUtilities.DateToString(new Date(), dateFormat));
        }
        
        return props;
    }

    /**
     * Sets the demographic record sourced data for the form
     * @param record FormONPerinatal2017 form to set demographic values to
     */
    private void setDemographicRecordValues(FormONPerinatal2017 record) {
        Date formEdited = record.getFormEdited();
        if (formEdited == null || !demographic.getLastUpdateDate().after(formEdited)) {
            return;
        }
        if ("c_address".equals(record.getField())) {
            record.setValue(StringUtils.trimToEmpty(demographic.getAddress()));
        } else if ("c_phone".equals(record.getField())) {
            record.setValue(StringUtils.trimToEmpty(demographic.getPhone()));
        } else if ("c_phoneAlt1".equals(record.getField())) {
            record.setValue(StringUtils.trimToEmpty(demographic.getPhone2()));
        } else if ("c_city".equals(record.getField())) {
            record.setValue(StringUtils.trimToEmpty(demographic.getCity()));
        } else if ("c_province".equals(record.getField())) {
            record.setValue(StringUtils.trimToEmpty(demographic.getProvince()));
        } else if ("c_postal".equals(record.getField())) {
            record.setValue(StringUtils.trimToEmpty(demographic.getPostal()));
        }
    }

    @Deprecated
    /**
     * Use FrmONPerinatalAction instead
     * This is just required to extend FrmRecord 
     */
    public int saveFormRecord(Properties props) throws SQLException {
        
        int formId = StringUtils.isNotEmpty(props.getProperty("formId")) ? Integer.parseInt(props.getProperty("formId")) : 0;
        int demographicNo = StringUtils.isNotEmpty(props.getProperty("demographic_no")) ? Integer.parseInt(props.getProperty("demographic_no")) : 0;
        String providerNo = StringUtils.isNotEmpty(props.getProperty("provNo")) ? props.getProperty("provNo") : "0";
        Set<String> keys = props.stringPropertyNames();
        for (String key : keys) {
            System.out.println(key + " \t\t\t : \t " + props.getProperty(key));
        }

        return formId;
    }

    public String findActionValue(String submit) throws SQLException {
        return ((new FrmRecordHelp()).findActionValue(submit));
    }

    public String createActionURL(String where, String action, String demoId, String formId) throws SQLException {
        return ((new FrmRecordHelp()).createActionURL(where, action, demoId, formId));
    }
    
    public static Integer getLatestFormIdByDemographic(Integer demographicNo) {
        ONPerinatal2017Dao perinatalDao = SpringUtils.getBean(ONPerinatal2017Dao.class);
        Integer formId = perinatalDao.getLatestFormIdByDemographic(demographicNo);
        
        return formId;
    }
    
    private List<FormONPerinatal2017> getExistingPerinatalRecordValues(Integer formId) {
        ONPerinatal2017Dao perinatalDao = SpringUtils.getBean(ONPerinatal2017Dao.class);
        List<FormONPerinatal2017> records = new ArrayList<FormONPerinatal2017>();
        formId = formId == null ? 0 : formId;
        
        if (formId > 0) {
           records = perinatalDao.findRecords(formId);
        }

        return records;
    }



    private Properties getRecordValuesAsProperties(List<FormONPerinatal2017> records) {
        Properties properties = new Properties();
        
        if (records != null) {
            for (FormONPerinatal2017 record : records) {
                properties.setProperty(record.getField(), record.getValue());
            }
        }
      

        return properties;
    }

}

