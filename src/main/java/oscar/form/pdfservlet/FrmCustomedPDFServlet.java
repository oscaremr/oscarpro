/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.form.pdfservlet;

import apps.health.pillway.PillwayLog.Method;
import apps.health.pillway.PillwayManager;
import ca.oscarpro.fax.FaxFacade;
import ca.oscarpro.fax.FaxFileType;
import ca.oscarpro.fax.SendFaxData;
import com.lowagie.text.DocumentException;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.dao.PrescriptionFaxDao;
import org.oscarehr.common.model.PrescriptionFax;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import oscar.log.LogAction;
import oscar.log.LogConst;

public class FrmCustomedPDFServlet extends HttpServlet {

	public static final String HSFO_RX_DATA_KEY = "hsfo.rx.data";

	private final FaxFacade faxFacade = SpringUtils.getBean(FaxFacade.class);

	@Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws java.io.IOException {

		ByteArrayOutputStream baosPDF = null;

		try {
			String method = req.getParameter("__method");
			boolean isFax = method.equals("oscarRxFax");
            FrmCustomedPDFParameters pdfParameters = new FrmCustomedPDFParameters(req);

            if (HSFO_RX_DATA_KEY.equals(pdfParameters.get__title())) {
                baosPDF = generateHsfoRxPDF(req);
            } else {
                FrmCustomedPDFGenerator pdfGenerator = new FrmCustomedPDFGenerator();
                baosPDF = pdfGenerator.generatePDFDocumentBytes(pdfParameters, LoggedInInfo.getLoggedInInfoFromSession(req), req.getLocale());
            }
			if (isFax) {
				res.setContentType("text/html");
				val providerNumber = LoggedInInfo.getLoggedInInfoFromSession(req).getLoggedInProviderNo();
				val demographicNumber = Integer.parseInt(req.getParameter("demographic_no"));
				val pdfFileName = "prescription_" + req.getParameter("pdfId");
				val sendFaxData = SendFaxData.builder()
						.destinationFaxNumber(req.getParameter("pharmaFax").trim().replaceAll("\\D", ""))
						.providerNumber(providerNumber)
						.demographicNumber(demographicNumber)
						.faxFileType(FaxFileType.PRESCRIPTION)
						.fileName(pdfFileName)
						.cookies(req.getCookies())
						.faxLine(req.getParameter("clinicFax"))
						.pdfDocumentBytes(baosPDF.toByteArray())
						.loggedInInfo(LoggedInInfo.getLoggedInInfoFromSession(req))
						.build();
				try {
					faxFacade.sendFax(sendFaxData);
				} catch (Exception ex) {
					throw new RuntimeException("Error sending fax", ex);
				} finally {
					PrescriptionFaxDao prescriptionFaxDao = SpringUtils.getBean(PrescriptionFaxDao.class);
					Integer pharmacyId = Integer.valueOf(req.getParameter("pharmacyId"));
					String scriptId = StringUtils.trimToNull(req.getParameter("scriptId"));
					String providerNo = StringUtils.trimToNull(providerNumber);
					prescriptionFaxDao.persist(new PrescriptionFax(scriptId, pharmacyId, providerNo));
					LogAction.addLog(
							providerNumber, LogConst.SENT, LogConst.CON_FAX, "PRESCRIPTION " + pdfFileName);
					PrintWriter writer = res.getWriter();
					writer.println(
							"<script>alert('Fax sent to: "
									+ req.getParameter("pharmaName").replaceAll("\\'", "\\\\'")
									+ " ("
									+ req.getParameter("pharmaFax")
									+ ")');window.parent.clearPendingFax();</script>");
					PillwayManager manager = SpringUtils.getBean(PillwayManager.class);
					manager.log(pharmacyId, scriptId, providerNo, demographicNumber, Method.FAX);
				}
			} else {
				StringBuilder sbFilename = new StringBuilder();
				sbFilename.append("filename_");
				sbFilename.append(".pdf");

				// set the Cache-Control header
				res.setHeader("Cache-Control", "max-age=0");
				res.setDateHeader("Expires", 0);

				res.setContentType("application/pdf");

				// The Content-disposition value will be inline
				StringBuilder sbContentDispValue = new StringBuilder();
				sbContentDispValue.append("inline; filename="); // inline - display
				// the pdf file
				// directly rather
				// than open/save
				// selection
				// sbContentDispValue.append("; filename=");
				sbContentDispValue.append(sbFilename);

				res.setHeader("Content-disposition", sbContentDispValue.toString());

				res.setContentLength(baosPDF.size());

				ServletOutputStream sos;

				sos = res.getOutputStream();

				baosPDF.writeTo(sos);

				sos.flush();
			}
		} catch (DocumentException dex) {
			res.setContentType("text/html");
			PrintWriter writer = res.getWriter();
			writer.println("Exception from: " + this.getClass().getName() + " " + dex.getClass().getName() + "<br>");
			writer.println("<pre>");
			writer.println(dex.getMessage());
			writer.println("</pre>");
		} catch (java.io.FileNotFoundException dex) {
		    res.setContentType("text/html");
		    PrintWriter writer = res.getWriter();
		    writer.println("<script>alert('Signature not found. Please sign the prescription.');</script>");
	    } finally {
			if (baosPDF != null) {
				baosPDF.reset();
			}
		}

	}

	// added by vic, hsfo
	private ByteArrayOutputStream generateHsfoRxPDF(HttpServletRequest req) {

		HsfoRxDataHolder rx = (HsfoRxDataHolder) req.getSession().getAttribute(HSFO_RX_DATA_KEY);

		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(rx.getOutlines());
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("/oscar/form/prop/Hsfo_Rx.jasper");

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			JasperRunManager.runReportToPdfStream(is, baos, rx.getParams(), ds);
		} catch (JRException e) {
			throw new RuntimeException(e);
		}
		return baos;
	}
}
