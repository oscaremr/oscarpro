/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package oscar.form.pharmaForms.formBPMH.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections.Factory;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.model.Allergy;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.Provider;

/*
 * Author: Dennis Warren 
 * Company: Colcamex Resources
 * Date: November 2014
 * For: UBC Pharmacy Clinic and McMaster Department of Family Medicine
 */

@XmlRootElement(name="xml")
@XmlAccessorType(XmlAccessType.FIELD)
public class BpmhFormBean extends ActionForm implements Serializable {
	
	private final SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
	
	@Getter @Setter private boolean confirm;
	@Getter @Setter private Date editDate;
	@Getter @Setter private String familyDrContactId;
	@Getter @Setter private String familyDrFax;
	@Getter @Setter private String familyDrName;
	@Getter @Setter private String familyDrPhone;
	@Getter @Setter private Date formDate;
	@Getter @Setter private String formId;
	@Getter @Setter private String allergiesString;

	@Setter private String demographicNo;
	@Setter private String note;

	// nested beans
	@Getter @Setter private Demographic demographic;
	@Getter @Setter private Provider provider;
	
	// lazy collections
	@Getter private List<Allergy> allergies;
	@Getter @Setter private List<BpmhDrug> drugs;

	@Override
	@SuppressWarnings("unchecked")
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		super.reset(mapping, request);
		this.drugs = ListUtils.lazyList(new ArrayList<BpmhDrug>(), new Factory() {
			public BpmhDrug create() {
				return new BpmhDrug();
			}
		});
		setConfirm(false);
	}

	public String getFormDateFormatted() {
		return formDate != null
				? formatter.format(formDate)
				: "";
	}

	public String getEditDateFormatted() {
		return editDate != null
				? formatter.format(editDate)
				: "";
	}

	public String getNote() {
		return note != null
				? note
				: "";
	}

	public String getDemographicNo() {
		return demographicNo != null
				? demographicNo
				: "";
	}

	public void setAllergies(List<Allergy> allergies) {
		this.allergies = allergies;
		if( this.allergies != null ) {
			StringBuilder stringBuilder = new StringBuilder("");
			Allergy allergy;
			String reaction;
			// concat description and reaction only.
			for(int i = 0; i < this.allergies.size(); i++) {
				allergy = this.allergies.get(i);
				reaction = allergy.getReaction();
				stringBuilder.append(i + 1).append(". ");
				stringBuilder.append(allergy.getDescription()).append(" ");
				if(reaction != null) {
					stringBuilder.append(", RX: ").append(reaction).append(" ");
				}
			}
			setAllergiesString( stringBuilder.toString() );
		}
	}
	
	public BpmhDrug getDrug(int index) {
		return getDrugs().get(index);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
