package oscar.form;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.PrintResourceLogDao;
import org.oscarehr.common.model.AbstractModel;
import org.oscarehr.common.model.Demographic;

import org.oscarehr.common.model.PrintResourceLog;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import oscar.form.dao.ONPerinatal2017CommentDao;
import oscar.form.dao.ONPerinatal2017Dao;
import oscar.form.model.FormONPerinatal2017;
import oscar.form.model.FormONPerinatal2017Comment;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class FrmONPerinatalAction extends DispatchAction {
    private DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
    private ONPerinatal2017Dao recordDao = SpringUtils.getBean(ONPerinatal2017Dao.class);
    private ONPerinatal2017CommentDao commentDao = SpringUtils.getBean(ONPerinatal2017CommentDao.class);
    
    private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
    private final Logger logger = LoggerFactory.getLogger(FrmONPerinatalRecord.class);
    
    private final String RECORD_NAME = "ONPerinatal";
    private final Integer RF_PER_PAGE = 6;  // max Risk Factors per page
    private final Integer SV_PER_PAGE = 16; // max Subsequent Visits per page
   
    private final Map<Integer, List<String>> checkRadioList = new HashMap<Integer, List<String>>() {{
        put(1, Arrays.asList(new String[] {"c_contactLeaveMessage", "c_interpreter", "c_disability", "c_baObs", "c_baFP", "c_baMidwife", "ps_certain", "ps_regular", "ps_planned", "ps_concep_assist", "ps_edbByT1", "ps_edbByT2", "ps_edbByLMP", "ps_edbByIUI", "ps_edbByEt", "ps_edbByOther", "oh_birth_type1", "mh_1", "mh_2", "mh_3", "mh_4", "mh_5", "mh_6", "mh_7", "mh_8", "mh_9", "mh_10", "mh_11", "mh_12", "mh_13", "mh_14", "mh_15", "mh_16", "mh_17", "mh_18", "mh_19", "mh_20", "mh_21", "mh_22", "mh_23", "mh_24", "mh_25", "mh_27", "mh_27_2", "mh_27_3", "mh_27_4", "mh_28", "mh_28_2", "mh_28_3", "mh_29", "mh_30", "mh_31", "mh_32", "mh_32_2", "mh_33", "mh_34", "mh_35", "mh_36", "mh_36_2", "mh_37", "mh_37_2", "mh_38", "mh_39", "mh_40", "mh_41", "mh_42", "mh_43", "mh_44", "mh_45", "mh_46", "mh_47", "mh_48", "mh_49", "mh_50", "mh_51", "mh_52", "mh_53"}));
        put(2, Arrays.asList(new String[]{"pe_head_neck", "pe_msk", "pe_breast", "pe_pelvic", "pe_heart_lungs", "pe_other", "pe_abdomen", "pgi_screening", "pgi_fts", "pgi_cvs", "pgi_ips1", "pgi_ips2", "pgi_other", "pgi_mss", "pgi_afp", "pgi_nipt", "pgi_declined", "pgi_presentation", "pgi_nipt_off", "us_screenReview", "us_hospitalCopy", "us_clientCopy"}));
        put(3, Arrays.asList(new String[]{"sc_lowASA", "sc_ptb", "sc_hsv", "gbi_swab", "gbi_other", "ri_rhNeg", "ri_flu_discussed", "ri_flu", "ri_per_discussed", "ri_per_utd", "ri_per", "ri_ppv_rub", "ri_ppv_other", "ri_nn_hb", "ri_nn_other", "trim1_nausea", "trim2_classes", "trim3_fetalMove", "trim3_workPlan", "trim1_routineCare", "trim2_preterm", "trim3_birthPlan", "trim1_safety", "trim2_prom", "trim3_tob", "trim1_weightGain", "trim1_bf", "trim2_bleed", "trim3_admission", "trim3_mental", "trim1_activity", "trim1_travel", "trim2_fetalMove", "trim3_bf", "trim3_contra", "trim1_seatbelt", "trim1_qis", "trim2_mental", "trim3_care", "trim1_sexualActi", "trim1_vbac", "trim2_vbac", "trim3_planning", "trim3_postpartum", "pg3_hospitalCopy", "pg3_ptCopy"}));
    }};
    
    private SimpleDateFormat formDateFormat = new SimpleDateFormat( "yyyy/MM/dd");

    public FrmONPerinatalAction() { }

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        return mapping.findForward("pg1");
    }
    public ActionForward loadPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        Integer formId = StringUtils.isNotEmpty(request.getParameter("formId")) ? Integer.parseInt(request.getParameter("formId")) : 0;
        Integer pageNo = StringUtils.isNotEmpty(request.getParameter("page_no")) ? Integer.parseInt(request.getParameter("page_no")) : 1;
        Integer demographicNo = Integer.parseInt(request.getParameter("demographic_no"));

        if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_demographic", "r", demographicNo)) {
            throw new SecurityException("missing required security object (_demographic)");
        }
        
        Demographic demographic = demographicDao.getDemographicById(demographicNo);
        JSONObject jsonRecord = new JSONObject();
        List <FormONPerinatal2017> records = new ArrayList<FormONPerinatal2017>();

        if (formId <= 0) {
            jsonRecord.put("c_lastName", StringUtils.trimToEmpty(demographic.getLastName()));
            jsonRecord.put("c_firstName", StringUtils.trimToEmpty(demographic.getFirstName()));
            jsonRecord.put("c_hin", demographic.getHin());
            jsonRecord.put("c_hinVer", demographic.getVer());

            if ("ON".equals(demographic.getHcType())) {
                jsonRecord.put("c_hinType", "OHIP");
            } else if ("QC".equals(demographic.getHcType())) {
                jsonRecord.put("c_hinType", "RAMQ");
            } else {
                jsonRecord.put("c_hinType", "OTHER");
            }
            jsonRecord.put("c_fileNo", StringUtils.trimToEmpty(demographic.getChartNo()));

            jsonRecord.put("formCreated", formDateFormat.format(new Date()));
            
        }
        else {
            // get common fields from other pages
            records.addAll(recordDao.findSectionRecordsNotForPage(formId, pageNo, "c_"));
            records.addAll(recordDao.findRecordsByPage(formId, pageNo));
        }
        
        for (FormONPerinatal2017 record : records) {
            jsonRecord.put(record.getField(), record.getValue());
        }
        
        if (!jsonRecord.has("pg"+pageNo+"_formDate")) {
            jsonRecord.put("pg"+pageNo+"_formDate", formDateFormat.format(new Date()));
        }
        
        Hashtable results = new Hashtable();
        results.put("results", jsonRecord);

        response.setContentType("text/x-json");
        JSONObject jsonArray=(JSONObject) JSONSerializer.toJSON(results);
        jsonArray.write(response.getWriter());
        
        return null;
    }
    
    public ActionForward saveAndExit (ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        save (mapping, form, request, response);
        return mapping.findForward("exit");
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        int formId = StringUtils.isNotEmpty(request.getParameter("formId")) ? Integer.parseInt(request.getParameter("formId")) : 0;
        int demographicNo = StringUtils.isNotEmpty(request.getParameter("demographicNo")) ? Integer.parseInt(request.getParameter("demographicNo")) : 0;
        String providerNo = StringUtils.isNotEmpty(request.getParameter("provNo")) ? request.getParameter("provNo") : "0";
        Integer page = StringUtils.isNotEmpty(request.getParameter("pageNo")) ? Integer.parseInt(request.getParameter("pageNo")) : 1;
        Integer forwardTo = StringUtils.isNotEmpty(request.getParameter("forwardTo")) ? Integer.parseInt(request.getParameter("forwardTo")) : page;
        String section = request.getParameter("section");
        Boolean update = Boolean.valueOf(request.getParameter("update"));

        List<FormONPerinatal2017> currentRecords = new ArrayList<FormONPerinatal2017>();
        List<FormONPerinatal2017Comment> currentComments = new ArrayList<FormONPerinatal2017Comment>();
        if (update) {
            if (section != null) {
                // section update
                currentRecords = recordDao.findSectionRecords(formId, page, section);
            } else {
                // full page update
                // get current page records and common values saved on other page
                currentRecords = recordDao.findRecordsByPage(formId, page);
                currentRecords.addAll(recordDao.findSectionRecordsNotForPage(formId, page, "c_"));
                currentRecords.addAll(recordDao.findSectionRecordsNotForPage(formId, page, "ps_edb_"));
                currentRecords.addAll(recordDao.findSectionRecordsNotForPage(formId, page, "provider_no"));
                currentRecords.addAll(
                    recordDao.findSectionRecordsNotForPage(formId, page, "pe_wt")
                );
            }
            currentComments = commentDao.findComments(formId, section);
        } else {
            currentRecords = recordDao.findRecords(formId);
            currentComments = commentDao.findComments(formId);
        }
        currentRecords.addAll(getCommentsAsRecords(currentComments));
        
        List<FormONPerinatal2017> addRecords = new ArrayList<>();
        Map<String, String> currentValues = getMappedRecords(currentRecords, section);
        List<AbstractModel<?>> updateRecords = new ArrayList<>();
        List<AbstractModel<?>> updateComments = new ArrayList<>();
        List<AbstractModel<?>> persistRecords = new ArrayList<>();
        List<AbstractModel<?>> persistComments = new ArrayList<>();
        
        Set<String> keys = request.getParameterMap().keySet();
        if (update) {
            List<String> pageCheckRadNames = checkRadioList.get(page) != null ? checkRadioList.get(page) : new ArrayList<String>();
            // look for checkboxes/radios that have been unchecked
            for (String currentValKey : currentValues.keySet()) {
                if (!keys.contains(currentValKey) && pageCheckRadNames.contains(currentValKey)) {
                    FormONPerinatal2017 rec = getRecordByKey(currentRecords, currentValKey);
                    if (rec != null && rec.getId() != null) {
                        rec.setValue("");
                        updateRecords.add(rec);
                    }
                }
            }
        } else {
            formId = recordDao.getNewFormId();

            // copy all current values to new form
            for (FormONPerinatal2017 rec : currentRecords) {
                if ((rec.getPageNo() == null || !rec.getPageNo().equals(page)) || StringUtils.trimToNull(request.getParameter(rec.getField())) != null) {
                    rec.setId(null);
                    rec.setFormId(formId);
                    addRecords.add(rec);
                }
            }
        }
        
        for (String key : keys) {
            if (key.contains("_")) {
                String value = StringUtils.trimToNull(request.getParameter(key));

                if (currentValues.get(key) != null) {
                    FormONPerinatal2017 record = getRecordByKey(addRecords, key);

                    if (record != null) {
                        // new record to be added
                        if (value != null && !value.equals(currentValues.get(key))) {
                            value = value.equals("on") ? "checked" : value;
                            record.setValue(value);
                        } else if (value == null) {
                            record.setValue("");
                        }
                    } else if (update) {
                        // update enabled and not a new record
                        value = StringUtils.trimToEmpty(value);
                        value = value.equals("on") ? "checked" : value;
                        
                        if (!value.equals(currentValues.get(key))) {
                            FormONPerinatal2017 rec = getRecordByKey(currentRecords, key);

                            if (rec.getField().contains("comment")) {
                                FormONPerinatal2017Comment commentRec = getCommentByKey(currentComments, key);
                                if (commentRec != null && commentRec.getId() != null) {
                                    commentRec.setValue(value);
                                    updateComments.add(commentRec);
                                }
                            } else if (rec != null && rec.getId() != null) {
                                rec.setValue(value);
                                updateRecords.add(rec);
                            }
                        }
                    }

                } else if (currentValues.get(key) == null && value != null) {
                    value = value.equals("on") ? "checked" : value;
                    addRecords.add(new FormONPerinatal2017(key, value));
                }
            }
        }
        
        for (FormONPerinatal2017 rec : addRecords) {
            Boolean isComment = rec.getField().contains("comment");
            
            if (rec.getDemographicNo() == null) {
                rec.setDemographicNo(demographicNo);
            }
            
            if (rec.getFormId() == null) {
                rec.setFormId(formId);
            }

            if (rec.getPageNo() == null) {
                rec.setPageNo(page);
            }

            if (rec.getProviderNo() == null) {
                rec.setProviderNo(providerNo);
            }
            
            if (isComment) {
                persistComments.add(new FormONPerinatal2017Comment(rec));
            } else {
                persistRecords.add(rec);
            }
        }
        
        recordDao.batchMerge(updateRecords);
        commentDao.batchMerge(updateComments);
        
        recordDao.batchPersist(persistRecords, 50);
        commentDao.batchPersist(persistComments, 50);
        
        return new ActionForward(mapping.findForward("pg"+forwardTo).getPath()+"?demographic_no=" + demographicNo + "&formId="+formId+"&provNo="+providerNo+"&view=0", true);
    }

    public ActionForward print(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)  {
        Integer demographicNo = Integer.parseInt(request.getParameter("demographicNo"));
        Integer formId = Integer.parseInt(request.getParameter("formId"));
        LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
        
        List<Integer> pagesToPrint = new ArrayList<>();
        // Loops through checking which pages were selected for printing
        for (int page = 1; page <= 5; page++) {
            // If a page was selected to print, adds it to the list to print
            if (Boolean.parseBoolean(request.getParameter("printPg" + page))) {
                pagesToPrint.add(page);
            }
        }
        
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=\"Perinatal_" + formId + ".pdf\"");
        
        try {
            printPdf(response.getOutputStream(), loggedInInfo, demographicNo, formId, pagesToPrint);
        } catch (IOException e) {
            logger.error("Could not retrieve OutputStream from the response to print the perinatal form", e);
        }
        
        return null;
    }

    public void printPdf (OutputStream os, LoggedInInfo loggedInInfo, Integer demographicNo, Integer formId, List<Integer> pagesToPrint)  {
        
        // Creates a new print resource log item so we can track who has printed the perinatal form
        PrintResourceLog item = new PrintResourceLog();
        item.setDateTime(new Date());
        item.setExternalLocation("None");
        item.setExternalMethod("None");
        item.setProviderNo(loggedInInfo.getLoggedInProviderNo());
        item.setResourceId(demographicNo.toString());
        item.setResourceName("ONPREnhanced");
        
        PrintResourceLogDao printLogDao = SpringUtils.getBean(PrintResourceLogDao.class);
        printLogDao.persist(item);
        
        
        final String RESOURCE_PATH = "/oscar/form/perinatal/page";
        ClassLoader cl = getClass().getClassLoader();
        
        try {
            FrmONPerinatalRecord perinatalRecord = (FrmONPerinatalRecord)(new FrmRecordFactory()).factory(RECORD_NAME);

            try {
                List<JasperPrint> pages = new ArrayList<>();
                for (Integer pageNumber : pagesToPrint) {
                    String pageImage = RESOURCE_PATH + pageNumber + ".png";
                    String reportUri = RESOURCE_PATH + pageNumber + ".jrxml";
                    
                    Properties recordData = perinatalRecord.getFormRecord(loggedInInfo, demographicNo, formId, pageNumber);
                    String eggEthnicity = recordData.getProperty("mh_26_egg");
                    if("UN".equals(eggEthnicity)){
                        recordData.setProperty("mh_26_egg", "-");
                    } else if("ANC001".equals(eggEthnicity)){
                        recordData.setProperty("mh_26_egg", "Aboriginal");
                    } else if("ANC002".equals(eggEthnicity)){
                        recordData.setProperty("mh_26_egg", "Asian");
                    } else if("ANC005".equals(eggEthnicity)){
                        recordData.setProperty("mh_26_egg", "Black");
                    } else if("ANC007".equals(eggEthnicity)){
                        recordData.setProperty("mh_26_egg", "Caucasian");
                    } else if("OTHER".equals(eggEthnicity)){
                        recordData.setProperty("mh_26_egg", "Other");
                    }
                    String spermEthnicity = recordData.getProperty("mh_26_sperm");
                    if("UN".equals(spermEthnicity)){
                        recordData.setProperty("mh_26_sperm", "-");
                    } else if("ANC001".equals(spermEthnicity)){
                        recordData.setProperty("mh_26_sperm", "Aboriginal");
                    } else if("ANC002".equals(spermEthnicity)){
                        recordData.setProperty("mh_26_sperm", "Asian");
                    } else if("ANC005".equals(spermEthnicity)){
                        recordData.setProperty("mh_26_sperm", "Black");
                    } else if("ANC007".equals(spermEthnicity)){
                        recordData.setProperty("mh_26_sperm", "Caucasian");
                    } else if("OTHER".equals(spermEthnicity)){
                        recordData.setProperty("mh_26_sperm", "Other");
                    }
                    recordData.setProperty("background_image", cl.getResource(pageImage).toString());
                    
                    JasperReport report = JasperCompileManager.compileReport(cl.getResource(reportUri).toURI().getPath());
                    JasperPrint jasperPrint = JasperFillManager.fillReport(report, (Map) recordData, new JREmptyDataSource());
                    
                    pages.add(jasperPrint);
                    
                    if (pageNumber == 3) {
                        int rfNum = Integer.parseInt(recordData.getProperty("rf_num", "0"));
                        int svNum = Integer.parseInt(recordData.getProperty("sv_num", "0"));
                        
                        if (rfNum > RF_PER_PAGE || svNum > SV_PER_PAGE) {
                            // if the records for Risk Factors or Subsequent Visits is greater than what is allowed on one page
                            // compute additional pages needed

                            // find the additional pages needed to satisfy additional records for Risk Factors and Subsequent Visits
                            int additionalPagesForRF = (rfNum % RF_PER_PAGE == 0) ? ((rfNum / RF_PER_PAGE) - 1) : (rfNum / RF_PER_PAGE);
                            int additionalPagesForSV = (svNum % SV_PER_PAGE == 0) ? ((svNum / SV_PER_PAGE) - 1) : (svNum / SV_PER_PAGE);
                            
                            // get the highest number needed
                            int additionalPage3s = Math.max(additionalPagesForRF, additionalPagesForSV);

                            // add additional page 3s
                            for (int i = 1; i <= additionalPage3s; i++) {
                                reportUri = RESOURCE_PATH + pageNumber + "-" + (i + 1) + ".jrxml";
                                report = JasperCompileManager.compileReport(cl.getResource(reportUri).toURI().getPath());
                                jasperPrint = JasperFillManager.fillReport(report, (Map) recordData, new JREmptyDataSource());
                                pages.add(jasperPrint);
                            }
                        }
                    }
                }

                JRPdfExporter exporter = new JRPdfExporter();
                exporter.setExporterInput(SimpleExporterInput.getInstance(pages));
                exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(os));
                exporter.exportReport();
                
            } catch (URISyntaxException e) {
                logger.error("Could not get URI of the perinatal pages for " + pagesToPrint.toString(), e);
            }
        } catch (NumberFormatException e) {
            logger.error("Could not parse formId for " + formId);
        } catch (JRException e) {
            MiscUtils.getLogger().error("Could not parse Report Template for the perinatal form", e);
        } catch (SQLException e) {
            logger.error("Could not retrieve record for Perinatal form", e);
        }
    }
    
    public ActionForward getPrintData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        PrintResourceLogDao printLogDao = SpringUtils.getBean(PrintResourceLogDao.class);
        ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
        String demographicNo = StringUtils.trimToEmpty(request.getParameter("resourceId"));
        // Gets the print logs for the given demographic
        List<PrintResourceLog> printLogs = printLogDao.findByResource("ONPREnhanced", demographicNo);
        
        if (!printLogs.isEmpty()) {
            List<String> providerNumbers = new ArrayList<>();
            // Creates a list of provider numbers to get the provider names for
            for (PrintResourceLog log : printLogs) {
                providerNumbers.add(log.getProviderNo());
            }
            // Gets a map of the provider names for the related provider numbers
            Map<String, String> providerNameMap = providerDao.getProviderNamesByIdsAsMap(providerNumbers);
            // Updates the resource log object wiht the provider's name for display purposes
            for (PrintResourceLog log : printLogs) {
                log.setProviderName(providerNameMap.getOrDefault(log.getProviderNo(), ""));
            }
        }

        try {
            JSONArray json = JSONArray.fromObject(printLogs);
            response.getWriter().print(json.toString());
        } catch (IOException e) {
            logger.warn("Could not print Perinatal printing log", e);
        }
        
        return null;
    }
    
    public static List<FormONPerinatal2017> getCommentsAsRecords(Integer formId) {
        ONPerinatal2017CommentDao commentDao = SpringUtils.getBean(ONPerinatal2017CommentDao.class);
        List<FormONPerinatal2017> commentsAsRecords = new ArrayList<FormONPerinatal2017>();
        List<FormONPerinatal2017Comment> comments = commentDao.findComments(formId);
        
        for (FormONPerinatal2017Comment c : comments) {
            commentsAsRecords.add(new FormONPerinatal2017(c));
        }
        
        return commentsAsRecords;
    }

    public static List<FormONPerinatal2017> getCommentsAsRecords(List<FormONPerinatal2017Comment> comments) {
        List<FormONPerinatal2017> commentsAsRecords = new ArrayList<FormONPerinatal2017>();

        for (FormONPerinatal2017Comment c : comments) {
            commentsAsRecords.add(new FormONPerinatal2017(c));
        }

        return commentsAsRecords;
    }

    public ActionForward getLatestFormIdByDemographic(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        String demographicNo = StringUtils.trimToEmpty(request.getParameter("demographicNo"));
        Integer latestFormId = null;
        // If the demographic number is all digits, it should be parseable to an integer
        if (NumberUtils.isDigits(demographicNo)) {
            // Gets the latest form id for the demographic
            latestFormId = FrmONPerinatalRecord.getLatestFormIdByDemographic(Integer.valueOf(demographicNo));
        }

        try {
            JSONObject json = new JSONObject();
            json.accumulate("formId", String.valueOf(latestFormId));
            response.getWriter().println(json);
        } catch (IOException e) {
            logger.warn("Could not retrieve the lastest Perinatal form id", e); 
        }
        
        return null;
    }

    private FormONPerinatal2017Comment getCommentByKey(List<FormONPerinatal2017Comment> comments, String key) {
        for (FormONPerinatal2017Comment rec : comments) {
            if (rec.getField().equals(key)) { 
                return rec;
            }
        }

        return null;
    }
    
    private FormONPerinatal2017 getCommentRecordByKey(List<FormONPerinatal2017Comment> comments, String key) {
        FormONPerinatal2017 record = null;

        for (FormONPerinatal2017Comment rec : comments) {
            if (rec.getField().equals(key)) {
                record = new FormONPerinatal2017(rec);
            }
        }

        return record;
    }
    
    private FormONPerinatal2017 getRecordByKey(List<FormONPerinatal2017> records, String key) {
        FormONPerinatal2017 record = null;

        for (FormONPerinatal2017 rec : records) {
            if (rec.getField().equals(key)) {
                record = rec;
            }
        }

        return record;
    }

    private FormONPerinatal2017 getRecordByKeyAbstract(List<AbstractModel<?>> records, String key) {
        FormONPerinatal2017 record = null;

       
        for (AbstractModel<?> rec : records) {
            if (rec instanceof FormONPerinatal2017) {
                if (((FormONPerinatal2017)rec).getField().equals(key)) {
                    record = (FormONPerinatal2017) rec;
                }
            }
        }

        return record;
    }
    
    private Map<String, String> getMappedRecords(List<FormONPerinatal2017> records, String prefix) {
        Map<String, String> recordMap = new HashMap<String, String>();
        prefix = StringUtils.trimToNull(prefix);
        if (records != null) {
            for (FormONPerinatal2017 record : records) {
                if (prefix == null || (record.getField() != null && record.getField().startsWith(prefix))) {
                    recordMap.put(record.getField(), record.getValue());
                }
            }
        }
        
        return recordMap;
    }
}
