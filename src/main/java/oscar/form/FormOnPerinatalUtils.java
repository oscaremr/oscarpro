package oscar.form;

import java.util.Properties;
import lombok.val;
import org.owasp.encoder.Encode;

public class FormOnPerinatalUtils {
  public static String getFormAttribute(final Properties properties, final String atttributeName) {
    if (properties == null) {
      return "";
    }
    return Encode.forHtmlAttribute(properties.getProperty(atttributeName, ""));
  }

  public static String setIfChecked(
      final Properties properties, 
      final String attributeName, 
      final String expectedValue
  ) {
    if (properties == null) {
      return "";
    }
    val property =  properties.getProperty(attributeName);
    if (property != null && property.equals(expectedValue)) {
      return "checked";
    }
    return "";
  }
}
