package oscar.form.util;

import org.oscarehr.common.model.Appointment;
import oscar.util.ConversionUtils;

public class UrlUtil {

  public static String getProBillingUrl(
      final String baseUrl,
      final Appointment appointment,
      final String signedUrlParam
  ) {
    return "<a href=# onClick='popupPage(755, 1200, "
        + "\"/" + baseUrl + "/#/billing/"
        + "?demographicNo=" + appointment.getDemographicNo()
        + "&appointmentNo=" + appointment.getId()
        + signedUrlParam
        + "\"); return false;'>Bill";
  }

  public static String getClassicBillingUrl(
      final String defaultView,
      final String providerView,
      final Appointment appointment
  ) {
    return "<a href=# onClick='popupPage(700, 1000, "
        + "\"billingOB.jsp"
        + "?billForm=" + defaultView
        + "&hotclick="
        + "&appointment_no=" + appointment.getId()
        + "&demographic_name="
        + "&demographic_no=" + appointment.getDemographicNo()
        + "&user_no=" + appointment.getProviderNo()
        + "&apptProvider_no=" + providerView
        + "&appointment_date=" + ConversionUtils.toDateString(appointment.getAppointmentDate())
        + "&start_time=" + ConversionUtils.toTimeString(appointment.getStartTime())
        + "&bNewForm=1"
        + "\"); return false;'>Bill ";
  }
}
