/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.form.util;

import java.util.Date;

public class DateUtil {

    public static Date initDate(Long time) {
        return (time != null && 0L != time) ? new Date(time) : new Date();
    }
}
