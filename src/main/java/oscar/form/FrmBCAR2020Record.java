package oscar.form;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import lombok.val;
import lombok.var;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.common.model.Provider;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.owasp.encoder.Encode;
import oscar.SxmlMisc;
import oscar.form.dao.FormBCAR2020DataDao;
import oscar.form.dao.FormBCAR2020TextDao;
import oscar.form.model.FormBCAR2020Data;
import oscar.form.model.FormBCAR2020Text;
import oscar.util.UtilDateUtilities;
import oscar.util.UtilMisc;

public class FrmBCAR2020Record extends FrmRecord {

    public FrmBCAR2020Record() {
        this.dateFormat = "yyyy/MM/dd";
    }

    //List of global fields that exist on all pages
    private static List<String> globalFields =
        Arrays.asList(
            "t_patientSurname",
            "t_patientGivenName",
            "t_patientAddress",
            "t_patientCity",
            "t_patientProvince",
            "t_patientPostal",
            "t_patientPhone",
            "t_patientHIN",
            "t_patientPhoneWork",
            "t_patientPhoneCell",
            "d_confirmedEDD"
        );

  public Properties getFormRecord(LoggedInInfo loggedInInfo, int demographicNo, int existingID)
      throws SQLException {
    return getFormRecord(loggedInInfo, demographicNo, existingID, 1);
  }

    public Properties getFormRecord(LoggedInInfo loggedInInfo, int demographicNo, int existingID,
        int pageNo) throws SQLException {
        var property = new Properties();
        FormBCAR2020DataDao bcar2020DataDao = SpringUtils.getBean(FormBCAR2020DataDao.class);
        FormBCAR2020TextDao bcar2020TextDao = SpringUtils.getBean(FormBCAR2020TextDao.class);
        if (existingID <= 0) {
            //Set default prop values
            this.setDemoProperties(loggedInInfo, demographicNo, property);
            //Set custom prop values
            updateFieldsFromDemographicRecord(property);
            Provider p = demographic.getProvider();
            String providerName = "";
            if (p != null) {
                providerName = p.getFormattedName();
            }
            property.setProperty("t_primaryCareProvider", providerName);
            String familyPhysician = demographic.getFamilyPhysicianName();
            property.setProperty("t_familyPhysician", familyPhysician);
        } else {
            // Regular data elements are stored as FormBCAR2020Data
            // Large text blobs are stored as FormBCAR2020Text
            val records = new ArrayList<>(bcar2020DataDao.findFieldsForPage(existingID, pageNo));
            val textRecords = new ArrayList<>(bcar2020TextDao.findFieldsForPage(existingID, pageNo));
            property = getRecordValuesAsProperties(records, textRecords);
            // Reset certain fields every time, specifically the Patient Surname/GivenName/HIN and DOB
            // PHSA did not want these to be "editable" and should stay in sync with the master demographic
            this.setDemoProperties(loggedInInfo, demographicNo, property);
            updateFieldsFromDemographicRecord(property);
        }
        if (property.getProperty("pg" + pageNo + "_formDate") == null) {
            property.setProperty(
                "pg" + pageNo + "_formDate",
                UtilDateUtilities.DateToString(new Date(), dateFormat)
            );
        }
        return property;
    }

    private void updateFieldsFromDemographicRecord(final Properties properties) {
        properties.setProperty("t_patientSurname", StringUtils.trimToEmpty(demographic.getLastName()));
        properties.setProperty("t_patientGivenName", StringUtils.trimToEmpty(demographic.getFirstName()));
        properties.setProperty("t_patientHIN", demographic.getHin());
        properties.setProperty("t_patientDOB", demographic.getDemographicDateOfBirth());
        properties.setProperty("t_patientAddress", demographic.getAddress());
        properties.setProperty("t_patientCity", demographic.getCity());
        properties.setProperty("t_patientProvince", demographic.getProvince());
        properties.setProperty("t_patientPostal", demographic.getPostal());
        properties.setProperty("t_patientPhone", demographic.getPhone());
        properties.setProperty("t_patientPhoneWork", demographic.getPhone2());
        val cell = demographicExtDao.getValueForDemoKey(demographic.getDemographicNo(), "demo_cell");
        if (cell != null) {
            properties.setProperty("t_patientPhoneCell", cell);
        }
    }

    @Deprecated
    /**
     * Use FrmBCAR2020Action instead
     * This is just required to extend FrmRecord
     */
    public int saveFormRecord(Properties props) throws SQLException {
      int formId = StringUtils.isNotEmpty(props.getProperty("formId"))
          ? Integer.parseInt(props.getProperty("formId"))
          : 0;
      Set<String> keys = props.stringPropertyNames();
      for (String key : keys) {
        System.out.println(key + " \t\t\t : \t " + props.getProperty(key));
      }
      return formId;
    }

    public String findActionValue(String submit) throws SQLException {
        return ((new FrmRecordHelp()).findActionValue(submit));
    }

  public String createActionURL(String where, String action, String demoId, String formId)
      throws SQLException {
    return ((new FrmRecordHelp()).createActionURL(where, action, demoId, formId));
  }

  private Properties getRecordValuesAsProperties(
      List<FormBCAR2020Data> records,
      List<FormBCAR2020Text> textRecords
  ) {
    Properties properties = new Properties();
    if (records != null) {
      for (FormBCAR2020Data record : records) {
        properties.setProperty(record.getField(), record.getValue());
      }
    }
    if (textRecords != null) {
      for (FormBCAR2020Text textRecord : textRecords) {
        properties.setProperty(textRecord.getField(), textRecord.getValue());
      }
    }
    return properties;
  }

    public Boolean isGlobalField(String fieldName) {
        return globalFields.contains(fieldName);
    }

    public String createToggleOption(Properties props, String fieldName, String description) {
        return "<tr><td>\n" +
            "<input type=\"checkbox\" name=\"c_" + fieldName + "No\" " + Encode.forHtmlAttribute(
            props.getProperty("c_" + fieldName + "No", "").equals("X") ? "checked" : "") + " />\n" +
            "</td><td>\n" +
            "<input type=\"checkbox\" name=\"c_" + fieldName + "Yes\" " + Encode.forHtmlAttribute(
            props.getProperty("c_" + fieldName + "Yes", "").equals("X") ? "checked" : "") + " />\n" +
            "</td><td>\n" +
            "<div class=\"divFlex\">\n" +
            "" + description + "\n" +
            "<input type=\"text\" name=\"t_" + fieldName
            + "Details\" size=\"10\" maxlength=\"150\" value=\"" + UtilMisc.htmlEscape(
            props.getProperty("t_" + fieldName + "Details", "")) + "\" />\n" +
            "</div></td></tr>";
    }

    public String createPrenatalVisitRow(Properties props, String rowCount) {
        return
            "<tr>\n" +
            "<td>\n" +
            "    <div class=\"div-center\">\n" +
            "        <input type=\"text\" id=\"d_prenatalVisitDate" + rowCount
            + "\" name=\"d_prenatalVisitDate" + rowCount + "\" title=\"17. Prenatal Visit Line "
            + rowCount + " - Visit Date\" size=\"9\" maxlength=\"150\" value=\""
            + UtilMisc.htmlEscape(props.getProperty("d_prenatalVisitDate" + rowCount, ""))
            + "\" />\n" +
            "        <img src=\"../images/cal.gif\" id=\"d_prenatalVisitDate" + rowCount
            + "_cal\">\n" +
            "    </div>\n" +
            "</td>\n" +
            "<td>\n" +
            "    <input type=\"text\" name=\"t_prenatalVisitGA" + rowCount
            + "\" title=\"17. Prenatal Visit Line " + rowCount
            + " - GA\" class=\"calcField\" size=\"8\" maxlength=\"150\" value=\""
            + UtilMisc.htmlEscape(props.getProperty("t_prenatalVisitGA" + rowCount, ""))
            + "\" onDblClick=\"getGAByFieldDate('t_prenatalVisitGA" + rowCount
            + "', 'd_confirmedEDD', 'd_prenatalVisitDate" + rowCount + "')\" />\n" +
            "</td>\n" +
            "<td>\n" +
            "    <input type=\"text\" name=\"t_prenatalVisitBP" + rowCount
            + "\" class=\"text-style\" size=\"10\" maxlength=\"150\" value=\""
            + UtilMisc.htmlEscape(props.getProperty("t_prenatalVisitBP" + rowCount, "")) + "\" />\n"
            +
            "</td>\n" +
            "<td>\n" +
            "    <input type=\"text\" name=\"t_prenatalVisitUrine" + rowCount
            + "\" class=\"text-style\" size=\"10\" maxlength=\"150\" value=\""
            + UtilMisc.htmlEscape(props.getProperty("t_prenatalVisitUrine" + rowCount, ""))
            + "\" />\n" +
            "</td>\n" +
            "<td>\n" +
            "    <input type=\"text\" name=\"t_prenatalVisitWt" + rowCount
            + "\" class=\"text-style\" size=\"10\" maxlength=\"150\" value=\""
            + UtilMisc.htmlEscape(props.getProperty("t_prenatalVisitWt" + rowCount, ""))
            + "\" onDblClick=\"wtPoundsToKg(this)\" />\n" +
            "</td>\n" +
            "<td>\n" +
            "    <input type=\"text\" name=\"t_prenatalVisitFundus" + rowCount
            + "\" class=\"text-style\" size=\"10\" maxlength=\"150\" value=\""
            + UtilMisc.htmlEscape(props.getProperty("t_prenatalVisitFundus" + rowCount, ""))
            + "\" />\n" +
            "</td>\n" +
            "<td>\n" +
            "    <input type=\"text\" name=\"t_prenatalVisitFHR" + rowCount
            + "\" class=\"text-style\" size=\"10\" maxlength=\"150\" value=\""
            + UtilMisc.htmlEscape(props.getProperty("t_prenatalVisitFHR" + rowCount, ""))
            + "\" />\n" +
            "</td>\n" +
            "<td>\n" +
            "    <input type=\"text\" name=\"t_prenatalVisitFM" + rowCount
            + "\" class=\"text-style\" size=\"10\" maxlength=\"150\" value=\""
            + UtilMisc.htmlEscape(props.getProperty("t_prenatalVisitFM" + rowCount, "")) + "\" />\n"
            +
            "</td>\n" +
            "<td>\n" +
            "    <input type=\"text\" name=\"t_prenatalVisitPosition" + rowCount
            + "\" class=\"text-style\" size=\"10\" maxlength=\"150\" value=\""
            + UtilMisc.htmlEscape(props.getProperty("t_prenatalVisitPosition" + rowCount, ""))
            + "\" />\n" +
            "</td>\n" +
            "<td>\n" +
            "    <input type=\"text\" name=\"t_prenatalVisitComments" + rowCount
            + "\" class=\"text-style\" size=\"30\" maxlength=\"150\" value=\""
            + UtilMisc.htmlEscape(props.getProperty("t_prenatalVisitComments" + rowCount, ""))
            + "\" />\n" +
            "</td>\n" +
            "<td>\n" +
            "    <input type=\"text\" name=\"t_prenatalVisitNextVisit" + rowCount
            + "\" class=\"text-style\" size=\"10\" maxlength=\"150\" value=\""
            + UtilMisc.htmlEscape(props.getProperty("t_prenatalVisitNextVisit" + rowCount, ""))
            + "\" />\n" +
            "</td>\n" +
            "<td>\n" +
            "    <input type=\"text\" name=\"t_prenatalVisitInitials" + rowCount
            + "\" class=\"text-style\" size=\"3\" maxlength=\"150\" value=\"" + UtilMisc.htmlEscape(
            props.getProperty("t_prenatalVisitInitials" + rowCount, "")) + "\" />\n" +
            "</td>\n" +
            "</tr>";
    }
}
