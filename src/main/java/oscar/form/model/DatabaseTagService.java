/**
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.form.model;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.val;
import org.oscarehr.common.model.Appointment;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.ProfessionalSpecialist;
import org.oscarehr.common.model.Provider;
import org.oscarehr.util.MiscUtils;
import oscar.eform.EFormLoader;
import oscar.eform.data.DatabaseAP;

public class DatabaseTagService {

    @Getter private Map<DatabaseTag, Method> databaseTagMethodMap = new LinkedHashMap<>();

    public void addDefaultDatabaseTags() {
        try {
            databaseTagMethodMap.put(new DatabaseTag("global", "todaysDate",
                    "today&#39;s date", false), SimpleDateFormat.class.getMethod("format", Date.class));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "fullName",
                    "demographic full name", false), Demographic.class.getDeclaredMethod("getFullName"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "firstName",
                    "demographic first name", false), Demographic.class.getDeclaredMethod("getFirstName"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "lastName",
                    "demographic last name", false), Demographic.class.getDeclaredMethod("getLastName"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "label",
                    "demographic label", true), Demographic.class.getDeclaredMethod("getLabel"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "age",
                    "demographic age", false), Demographic.class.getDeclaredMethod("getAge"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "gender",
                    "demographic gender", false), Demographic.class.getDeclaredMethod("getSexDesc"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "subjectPronoun",
                    "he/she/they", false), Demographic.class.getDeclaredMethod("getSubjectPronoun"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "possessivePronoun",
                    "his/hers/their", false), Demographic.class.getDeclaredMethod("getPossessivePronoun"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "referringPhysicianLabel",
                    "referring physician label", true), ProfessionalSpecialist.class.getDeclaredMethod("createContactString"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "referringPhysicianFullName",
                    "referring physician full name", false), ProfessionalSpecialist.class.getDeclaredMethod("getFormattedName"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "referringPhysicianFirstName",
                "referring physician first name", false), ProfessionalSpecialist.class.getDeclaredMethod("getFirstName"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "referringPhysicianLastName",
                "referring physician last name", false), ProfessionalSpecialist.class.getDeclaredMethod("getLastName"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "familyDoctorLabel",
                    "family doctor label", true),ProfessionalSpecialist.class.getDeclaredMethod("createContactString"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "familyDoctorFullName",
                    "family doctor full name", false), ProfessionalSpecialist.class.getDeclaredMethod("getFormattedName"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "familyDoctorFirstName",
                "family doctor first name", true),ProfessionalSpecialist.class.getDeclaredMethod("getFirstName"));
            databaseTagMethodMap.put(new DatabaseTag("demographic", "familyDoctorLastName",
                "family doctor last name", false), ProfessionalSpecialist.class.getDeclaredMethod("getLastName"));
            databaseTagMethodMap.put(new DatabaseTag("provider", "fullName",
                    "provider full name", false), Provider.class.getDeclaredMethod("getFullName"));
            databaseTagMethodMap.put(new DatabaseTag("provider", "firstName",
                    "provider first name", false), Provider.class.getDeclaredMethod("getFullName"));
            databaseTagMethodMap.put(new DatabaseTag("provider", "lastName",
                    "provider last name", false), Provider.class.getDeclaredMethod("getFullName"));
            databaseTagMethodMap.put(new DatabaseTag("appointment", "no",
                    "appointment no", false), Appointment.class.getDeclaredMethod("getId"));
            databaseTagMethodMap.put(new DatabaseTag("appointment", "date",
                    "appointment date", false), Appointment.class.getDeclaredMethod("getAppointmentDate"));
            databaseTagMethodMap.put(new DatabaseTag("appointment", "providerId",
                    "appointment provider id", false), Appointment.class.getDeclaredMethod("getProviderNo"));
            databaseTagMethodMap.put(new DatabaseTag("appointment", "providerName",
                    "appointment provider name", false), Appointment.class.getDeclaredMethod("getProviderName"));
        } catch (NoSuchMethodException e) {
            MiscUtils.getLogger().error("Error on resolving smart encounter placeholder", e);
        }
    }

    public void addCustomTagToMap(final String subject, final String field, final String title, final Boolean textBlock) {
        val databaseTag = new DatabaseTag(subject, field, title, textBlock);
        try {
            databaseTagMethodMap.put(databaseTag, databaseTag.getCustomTagMethod());
        } catch (NoSuchMethodException e) {
            MiscUtils.getLogger().error("Error on resolving smart encounter placeholder", e);
        }
    }

    public void addCustomDatabaseTags() {
        val apNames = EFormLoader.getInstance().getNames();

        for (String apName : apNames) {
            DatabaseAP databaseAP = EFormLoader.getAP(apName);
            String title = apName.replace("_", " ");
            this.addCustomTagToMap("eformAp", apName, title, databaseAP.isMultilineResult());

        }
    }
}
