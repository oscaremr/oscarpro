/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.form.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;
import org.oscarehr.common.model.AbstractModel;

@Entity
@Data
public class SmartEncounterHeader extends AbstractModel<Integer> implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String text;
    private String htmlPreview;
    private Date createDate;
    private Date editedDate;
}
