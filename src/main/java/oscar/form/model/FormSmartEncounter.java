/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.form.model;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import lombok.var;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.model.AbstractModel;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.Provider;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "form_smart_encounter")
public class FormSmartEncounter extends AbstractModel<Integer> implements Serializable, StringValueForm {
    public static final String FORM_TABLE = "formSmartEncounter";

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ID")
    private Integer id;
    @Column(name="demographic_no")
    private Integer demographicNo;
    @Column(name="provider_no")
    private String providerNo;
    @Column(name="appointment_no")
    private Integer appointmentNo;
    @Column(name="document_name")
    private String documentName;
    @Temporal(TemporalType.DATE)
    @Column(name="formCreated")
    private Date formCreated;
    @Column(name="formEdited")
    private Timestamp formEdited;
    /**
     * Canonical note written and exported from quilljs as a delta, with placeholder elements included 
     */
    @Column(name="delta_text", columnDefinition = "TEXT")
    private String deltaText;
    /**
     * Mote exported from quilljs as a html, with placeholder ( ${placeholderName} ) text included 
     */
    @Column(name="html_text", columnDefinition = "TEXT")
    private String htmlText;
    /**
     * Plain, no markup note exported from quilljs as a html, with placeholder ( ${placeholderName} ) text included 
     */
    @Column(name="plain_text", columnDefinition = "TEXT")
    private String plainText;
    @Column(name="template_used")
    private Integer templateUsed;

    @Column(name="headerText", columnDefinition = "TEXT")
    private String headerText;

    @Column(name="footerText", columnDefinition = "TEXT")
    private String footerText;

    @Column(name = "footerId")
    private Integer footerId;

    @Column(name = "headerId")
    private Integer headerId;

    @Transient
    private static BaseFont baseFont;

    @Transient
    private Map<String, FormStringValue> placeholderValueMap = new HashMap<String, FormStringValue>();
    @Transient
    private Map<String, FormStringValue> placeholderImageMap = new HashMap<String, FormStringValue>();

    @Getter
    @Setter
    @Column(name = "attachments")
    private String attachments;

    public FormSmartEncounter() { }

    @Override
    public String getFormTable() {
        return FORM_TABLE;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public Map<String, FormStringValue> getStringValueMap() { 
        HashMap<String, FormStringValue> map = new HashMap<String, FormStringValue>();
        for (Map.Entry<String, FormStringValue> entry : placeholderValueMap.entrySet()) {
            map.put(entry.getKey(), entry.getValue());
        }
        for (Map.Entry<String, FormStringValue> entry : placeholderImageMap.entrySet()) {
            map.put(entry.getKey(), entry.getValue());
        }
        return map;
    }

    @Override
    public void setStringValueMap(Map<String, FormStringValue> stringValueMap) {
        this.placeholderValueMap = stringValueMap;
    }

    public Map<String, FormStringValue> getPlaceholderValueMap() {
        return placeholderValueMap;
    }

    public void setPlaceholderValueMap(Map<String, FormStringValue> placeholderValueMap) {
        this.placeholderValueMap = placeholderValueMap;
    }

    public Map<String, FormStringValue> getPlaceholderImageMap() {
        return placeholderImageMap;
    }

    public void setPlaceholderImageMap(Map<String, FormStringValue> placeholderImageMap) {
        this.placeholderImageMap = placeholderImageMap;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProviderNo() {
        return providerNo;
    }

    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }

    public Integer getAppointmentNo() {
        return appointmentNo;
    }

    public void setAppointmentNo(Integer appointmentNo) {
        this.appointmentNo = appointmentNo;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }

    public Integer getDemographicNo() {
        return demographicNo;
    }

    public void setDemographicNo(Integer demographicNo) {
        this.demographicNo = demographicNo;
    }

    public Date getFormCreated() {
        return formCreated;
    }

    public void setFormCreated(Date formCreated) {
        this.formCreated = formCreated;
    }

    public Timestamp getFormEdited() {
        return formEdited;
    }

    public void setFormEdited(Timestamp formEdited) {
        this.formEdited = formEdited;
    }

    public String getDeltaText() {
        return deltaText;
    }

    public void setDeltaText(String deltaText) {
        this.deltaText = deltaText;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public String getFooterText() {
        return footerText;
    }

    public void setFooterText(String footerText) {
        this.footerText = footerText;
    }
    
    public Integer getFooterId() {
        return footerId;
    }

    public void setFooterId(Integer footerId) {
        this.footerId = footerId;
    }

    public Integer getHeaderId() {
        return headerId;
    }

    public void setHeaderId(Integer headerId) {
        this.headerId = headerId;
    }

    public String getHtmlTextWithValuesFilled() {
        String html = getHtmlText();
        //replace header

        if (headerText == null) {
            headerText = "";
        }
        if (footerText == null) {
            footerText = "";
        }

        // replace string values 
        for (Map.Entry<String, FormStringValue> stringFormStringValueEntry : getPlaceholderValueMap().entrySet()) {
            String key = stringFormStringValueEntry.getKey();
            String value = stringFormStringValueEntry.getValue().getValue();
            if (SmartEncounterTemplatePlaceholder.isBlockPlaceholder(key)) {
              value = value.replaceAll("\n", "<br/>");
            }
            value = value.replace("&", "&amp;");
            html = html.replaceAll("\\$\\{" + key + "}", value);
            headerText = headerText.replaceAll("\\$\\{" + key + "}", value);
            footerText = footerText.replaceAll("\\$\\{" + key + "}", value);
        }

        try {
            headerText = new String(headerText.getBytes("UTF-8"), "UTF-8");
            footerText = new String(footerText.getBytes("UTF-8"), "UTF-8");
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        html = html.replaceFirst("_header", replaceAll(headerText));
        html = html.replaceFirst("_footer", replaceAll(footerText));

        val imageDirectory = OscarProperties.getInstance()
            .getProperty("form_smart_encounter_images_path");

        // replace image 
        for (Map.Entry<String, FormStringValue> imageEntry : getPlaceholderImageMap().entrySet()) {
            String key = imageEntry.getKey();
            String value = imageEntry.getValue().getValue();
            File imageFile = new File(imageDirectory + File.separator + value);
            if (imageFile.exists()) {
                try {
                    byte[] fileData = FileUtils.readFileToByteArray(imageFile);
                    String base64Image = DatatypeConverter.printBase64Binary(fileData);
                    html = html.replaceAll("\\$\\{" + key + "}",
                        "<img src=\"data:image/png;base64," + base64Image + "\"/>");
                } catch (IOException e) {
                    e.printStackTrace(); // todo: this
                }
            }
        }

        html = html.replaceAll("\t", "<span class=\"tab\">&nbsp;</span>");
        return html;
    }

    public String getPlainText() {
        return plainText;
    }

    public void setPlainText(String plainText) {
        this.plainText = plainText;
    }

    public Integer getTemplateUsed() {
        return templateUsed;
    }

    public void setTemplateUsed(Integer templateUsed) {
        this.templateUsed = templateUsed;

    }

    public String getHeaderText() {
        return headerText;
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }

    private String replaceAll(String value) {
        if (value == null) {
            return "";
        }
        value = value.replaceAll("\"", "\\\\\\\\\"");
        value = value.replaceFirst("</p><p>", "\\\\\\\\a ");
        value = value.replaceAll("</p><p>", " ");
        value = value.replaceAll("\n","");
        while (value.contains("<")) {
            value = value.substring(0, value.indexOf("<")) + value.substring(value.indexOf(">") + 1);
        }

        while (value.contains("${")) {
            int firstIndex = value.indexOf("${");
            int lastIndex = value.indexOf("}", firstIndex);
            value = value.substring(0, firstIndex) + value.substring(lastIndex + 1);
        }

        return getFormatedString(value);
    }

    private String getFormatedString(String value) {
        float maxWidth = 1050;
        String[] lines = value.split("\\\\\\\\a");
        if (lines.length == 1) {
            if (getStringWidth(value) > maxWidth) {
                //split into two lines
                int wordIndex = getLastWordIndex(value);
                var line1 = value.substring(0, wordIndex).trim();
                var line2 = value.substring(wordIndex).trim();
                if (getStringWidth(line2) > maxWidth) {
                    line2 = line2.substring(0, getLastWordIndex(line2));
                }
                value = line1 + "\\\\\\\\a " + line2.trim();
            }
        } else {
            if (getStringWidth(lines[0]) <= maxWidth) {
                if (getStringWidth(lines[1]) > maxWidth) {
                    lines[1] = lines[1].substring(0, getLastWordIndex(lines[1])).trim();
                }
                value = lines[0] + "\\\\\\\\a " + lines[1].trim();
            } else {
                var line1 = lines[0].substring(0, getLastWordIndex(lines[0]));
                var line2 = lines[0].substring(getLastWordIndex(lines[0])) + lines[1];
                if (getStringWidth(line2) > maxWidth) {
                    line2 = line2.substring(0, getLastWordIndex(line2));
                }
                value = line1 + "\\\\\\\\a " + line2.trim();
            }

        }
        return value;
    }

    private float getStringWidth(String value) {
        if (baseFont == null) {
            try {
                baseFont = BaseFont.createFont();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return baseFont.getWidthPoint(value, 16);

    }

    private int getLastWordIndex(String value) {
        float maxWidth = 1050;
        //firstly try to find space or comma
        int wordIndex = 0;
        for(int i = 1; i < value.length(); i++) {
            var currentString = value.substring(0, i);
            if (getStringWidth(currentString) >= maxWidth) {
                //width full
                if (wordIndex != 0) {
                    return wordIndex;
                } else {
                    return i;
                }
            }
            if (value.charAt(i) == ' ' || value.charAt(i) == ',') {
                wordIndex = i;
            }
        }
        return value.length();
    }

    public static void main(String[] args) {
        val formSmartEncounter = new FormSmartEncounter();
    }
}
