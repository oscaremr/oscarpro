package oscar.form.model;

import javax.persistence.Column;
import java.io.Serializable;

public class CompositeFormBCAR2020DataId implements Serializable {
    @Column(name = "form_id")
    Integer formId;
    @Column(name = "page_no")
    Integer pageNo;
    @Column(name = "field")
    String field;
    
    public CompositeFormBCAR2020DataId() {
    }
    
    public CompositeFormBCAR2020DataId(Integer formId, Integer pageNo, String field) {
        this.formId = formId;
        this.pageNo = pageNo;
        this.field = field;
    }

    public Integer getFormId() {
        return formId;
    }

    public void setFormId(Integer formId) {
        this.formId = formId;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (formId == null ? 0 : formId.hashCode());
        result = prime * result + (pageNo == null ? 0 : pageNo.hashCode());
        result = prime * result + (field == null ? 0 : field.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (getClass() != obj.getClass()) {
            return false;
        }
        CompositeFormBCAR2020DataId other = (CompositeFormBCAR2020DataId) obj;
        if (formId == null && other.getFormId() != null) {
            return false;
        } else if (!formId.equals(other.getFormId())) {
            return false;
        }

        if (pageNo == null && other.getPageNo() != null) {
            return false;
        } else if (!pageNo.equals(other.getPageNo())) {
            return false;
        }

        if (field == null && other.getField() != null) {
            return false;
        } else if (!field.equals(other.getField())) {
            return false;
        }

        return true;
    }
}
