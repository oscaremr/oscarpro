/**
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.form.model;

import java.lang.reflect.Method;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.var;
import org.owasp.encoder.Encode;
import oscar.eform.APExecute;

@Getter @Setter
@AllArgsConstructor
public class DatabaseTag {

    private String subject;
    private String field;
    private String title;
    private boolean textBlock;

    public String getSubjectAndField() {
        return subject + "." + field;
    }

    public Method getCustomTagMethod() throws NoSuchMethodException {
        return APExecute.class.getMethod("execute", String.class, String.class);
    }

    public String toJavascriptCreateMethod() {
        var createMethodString = "";
        if (textBlock) {
            createMethodString += "TemplateBlockPlaceholder.create({";
        } else {
            createMethodString += "TemplatePlaceholder.create({";
        }

        createMethodString += "subject: '" + Encode.forJavaScriptBlock(subject) + "', ";
        createMethodString += "marker: '" + Encode.forJavaScriptBlock(getSubjectAndField()) + "', ";
        createMethodString += "title: '" + Encode.forJavaScript(title) + "'})";

        return createMethodString;
    }
}
