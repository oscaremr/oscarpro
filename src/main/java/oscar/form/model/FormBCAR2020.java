package oscar.form.model;

import com.sun.istack.NotNull;
import org.oscarehr.common.model.AbstractModel;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "formBCAR2020")
public class FormBCAR2020 extends AbstractModel<Integer> implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer formId;

    @NotNull
    @Column(name = "demographic_no")
    Integer demographicNo;

    @NotNull
    @Column(name = "provider_no")
    String providerNo;

    @Column(name = "formCreated")
    Date formCreated;

    public FormBCAR2020() {
    }

    public FormBCAR2020(Integer demographicNo, String providerNo) {
        this.demographicNo = demographicNo;
        this.providerNo = providerNo;
    }

    @Override
    public Integer getId() {
        return formId;
    }

    @PrePersist
    public void prePersist() {
        if (formCreated == null) {
            setFormCreated(new Date());
        }
    }

    public Integer getDemographicNo() {
        return demographicNo;
    }

    public void setDemographicNo(Integer demographicNo) {
        this.demographicNo = demographicNo;
    }

    public String getProviderNo() {
        return providerNo;
    }

    public void setProviderNo(String providerNo) {
        this.providerNo = providerNo;
    }

    public Date getFormCreated() {
        return formCreated;
    }

    public void setFormCreated(Date formCreated) {
        this.formCreated = formCreated;
    }
    
}
