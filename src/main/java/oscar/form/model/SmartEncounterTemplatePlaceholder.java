/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.form.model;

import com.google.common.collect.Maps;
import java.util.EnumSet;
import java.util.Set;
import org.oscarehr.common.model.Appointment;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.ProfessionalSpecialist;
import org.oscarehr.common.model.Provider;
import org.oscarehr.util.MiscUtils;
import org.owasp.encoder.Encode;
import oscar.eform.APExecute;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

public enum SmartEncounterTemplatePlaceholder {
    GLOBAL_TODAYS_DATE("global", "todaysDate", "today&#39;s date", false),
    DEMOGRAPHIC_FULL_NAME("demographic", "fullName", "demographic full name", false),
    DEMOGRAPHIC_FIRST_NAME("demographic", "firstName", "demographic first name", false),
    DEMOGRAPHIC_LAST_NAME("demographic", "lastName", "demographic last name", false),
    DEMOGRAPHIC_LABEL("demographic", "label", "demographic label", true),
    DEMOGRAPHIC_AGE("demographic", "age", "demographic age", false),
    DEMOGRAPHIC_GENDER("demographic", "gender", "demographic gender", false),
    DEMOGRAPHIC_SUBJECT_PRONOUN("demographic", "subjectPronoun", "he/she/they", false),
    DEMOGRAPHIC_POSSESSIVE_PRONOUN("demographic", "possessivePronoun", "his/hers/their", false),
    DEMOGRAPHIC_REFERRING_PHYSICIAN_LABEL("demographic", "referringPhysicianLabel", "referring physician label", true),
    DEMOGRAPHIC_REFERRING_PHYSICIAN_FULLNAME("demographic", "referringPhysicianFullName", "referring physician full name", false),
    DEMOGRAPHIC_REFERRING_PHYSICIAN_FIRSTNAME("demographic", "referringPhysicianFirstName", "referring physician first name", false),
    DEMOGRAPHIC_REFERRING_PHYSICIAN_LASTNAME("demographic", "referringPhysicianLastName", "referring physician last name", false),
    DEMOGRAPHIC_FAMILY_DOCTOR_LABEL("demographic", "familyDoctorLabel", "family doctor label", true),
    DEMOGRAPHIC_FAMILY_DOCTOR_FULLNAME("demographic", "familyDoctorFullName", "family doctor full name", false),
    DEMOGRAPHIC_FAMILY_DOCTOR_FIRSTNAME("demographic", "familyDoctorFirstName", "family doctor first name", false),
    DEMOGRAPHIC_FAMILY_DOCTOR_LASTNAME("demographic", "familyDoctorLastName", "family doctor last name", false),
    PROVIDER_FULLNAME("provider", "fullName", "provider full name", false),
    PROVIDER_FIRST_NAME("provider", "firstName", "provider first name", false),
    PROVIDER_LAST_NAME("provider", "lastName", "provider last name", false),
    APPOINTMENT_NO("appointment", "no", "appointment no", false),
    APPOINTMENT_DATE("appointment", "date", "appointment date", false),
    APPOINTMENT_PROVIDER_ID("appointment", "providerId", "appointment provider id", false),
    APPOINTMENT_PROVIDER_NAME("appointment", "providerName", "appointment provider name", false),
    // Eform AP placeholders
    TODAY("eformAp", "today", "today", false),
    TIME("eformAp", "time", "time", false),
    CURRENT_FORM_ID("eformAp", "current_form_id", "current form id", false),
    CURRENT_FORM_DATA_ID("eformAp", "current_form_data_id", "current form data id", false),
    CURRENT_USER("eformAp", "current_user", "current user", false),
    CURRENT_USER_FNAME_LNAME("eformAp", "current_user_fname_lname", "current user fname lname", false),
    CURRENT_USER_FNAME_LNAME_CREDENTIALS("eformAp", "current_user_fname_lname_credentials", "current user fname lname credentials", false),
    CURRENT_USER_OHIP_NO("eformAp", "current_user_ohip_no", "current user ohip no", false),
    CURRENT_USER_SPECIALTY_CODE("eformAp", "current_user_specialty_code", "current user specialty code", false),
    CURRENT_USER_CPSID("eformAp", "current_user_cpsid", "current user cpsid", false),
    CURRENT_USER_ID("eformAp", "current_user_id", "current user id", false),
    CURRENT_USER_SIGNATURE("eformAp", "current_user_signature", "current user signature", false),
    PATIENT_NAME("eformAp", "patient_name", "patient name", false),
    FIRST_LAST_NAME("eformAp", "first_last_name", "first last name", false),
    PATIENT_NAMEL("eformAp", "patient_namel", "patient namel", false),
    PATIENT_NAMEF("eformAp", "patient_namef", "patient namef", false),
    PATIENT_ALIAS("eformAp", "patient_alias", "patient alias", false),
    PATIENT_ID("eformAp", "patient_id", "patient id", false),
    LABEL("eformAp", "label", "label", true),
    ADDRESS("eformAp", "address", "address", false),
    ADDRESSLINE("eformAp", "addressline", "addressline", false),
    ADDRESS_STREET_NUMBER_AND_NAME("eformAp", "address_street_number_and_name", "address street number and name", false),
    PROVINCE("eformAp", "province", "province", false),
    CITY("eformAp", "city", "city", false),
    POSTAL("eformAp", "postal", "postal", false),
    DOB("eformAp", "dob", "dob", false),
    DOBC("eformAp", "dobc", "dobc", false),
    DOBC2("eformAp", "dobc2", "dobc2", false),
    DOBC3("eformAp", "dobc3", "dobc3", false),
    DOB_YEAR("eformAp", "dob_year", "dob year", false),
    DOB_MONTH("eformAp", "dob_month", "dob month", false),
    DOB_DAY("eformAp", "dob_day", "dob day", false),
    NAMEADDRESS("eformAp", "nameaddress", "nameaddress", false),
    HIN("eformAp", "hin", "hin", false),
    HINC("eformAp", "hinc", "hinc", false),
    HINVERSION("eformAp", "hinversion", "hinversion", false),
    HC_RENEW_DATE("eformAp", "hc_renew_date", "hc renew date", false),
    CHARTNO("eformAp", "chartno", "chartno", false),
    PHONE("eformAp", "phone", "phone", false),
    PHONE2("eformAp", "phone2", "phone2", false),
    CELL("eformAp", "cell", "cell", false),
    PHONE_EXTENSION("eformAp", "phone_extension", "phone extension", false),
    PHONE2_EXTENSION("eformAp", "phone2_extension", "phone2 extension", false),
    AGE("eformAp", "age", "age", false),
    AGE_IN_MONTHS("eformAp", "age_in_months", "age in months", false),
    AGECOMPLEX("eformAp", "agecomplex", "agecomplex", false),
    SEX("eformAp", "sex", "sex", false),
    SIN("eformAp", "sin", "sin", false),
    PATIENT_TITLE("eformAp", "patient_title", "patient title", false),
    MEDICAL_HISTORY("eformAp", "medical_history", "medical history", false),
    OTHER_MEDICATIONS_HISTORY("eformAp", "other_medications_history", "other medications history", false),
    SOCIAL_FAMILY_HISTORYC("eformAp", "social_family_historyc", "social family historyc", false),
    ONGOINGCONCERNS("eformAp", "ongoingconcerns", "ongoingconcerns", false),
    REMINDERS("eformAp", "reminders", "reminders", false),
    RISK_FACTORS_JSON("eformAp", "risk_factors_json", "risk factors json", false),
    FAMILY_HISTORY_JSON("eformAp", "family_history_json", "family history json", false),
    RECENT_NOTE("eformAp", "recent_note", "recent note", false),
    DXREGISTRY("eformAp", "dxregistry", "dxregistry", false),
    OHIPDXCODE("eformAp", "ohipdxcode", "ohipdxcode", false),
    ALLERGIES_DES("eformAp", "allergies_des", "allergies des", false),
    ALLERGIES_DES_NO_ARCHIVED("eformAp", "allergies_des_no_archived", "allergies des no archived", false),
    RECENT_RX("eformAp", "recent_rx", "recent rx", true),
    TODAY_RX("eformAp", "today_rx", "today rx", true),
    DRUGLIST_GENERIC("eformAp", "druglist_generic", "druglist generic", true),
    DRUGLIST_TRADE("eformAp", "druglist_trade", "druglist trade", true),
    DRUGLIST_LINE("eformAp", "druglist_line", "druglist line", true),
    ONGTPAL("eformAp", "ongtpal", "ongtpal", false),
    ONEDB("eformAp", "onedb", "onedb", false),
    BCGTPAL("eformAp", "bcgtpal", "bcgtpal", false),
    BCEDD("eformAp", "bcedd", "bcedd", false),
    DOCTOR("eformAp", "doctor", "doctor", false),
    DOCTOR_PROVIDER_NO("eformAp", "doctor_provider_no", "doctor provider no", false),
    DOCTOR_OHIP_NO("eformAp", "doctor_ohip_no", "doctor ohip no", false),
    DOCTOR_SPECIALTY_CODE("eformAp", "doctor_specialty_code", "doctor specialty code", false),
    DOCTOR_CPSID("eformAp", "doctor_cpsid", "doctor cpsid", false),
    DOCTOR_TITLE("eformAp", "doctor_title", "doctor title", false),
    PROVIDER_NAME("eformAp", "provider_name", "provider name", false),
    PROVIDER_NAME_FIRST_INIT("eformAp", "provider_name_first_init", "provider name first init", false),
    DOCTOR_WORK_PHONE("eformAp", "doctor_work_phone", "doctor work phone", false),
    DOCTOR_SIGNATURE("eformAp", "doctor_signature", "doctor signature", false),
    APPT_NO("eformAp", "appt_no", "appt no", false),
    REFERRAL_NAME("eformAp", "referral_name", "referral name", false),
    REFERRAL_ADDRESS("eformAp", "referral_address", "referral address", false),
    REFERRAL_PHONE("eformAp", "referral_phone", "referral phone", false),
    REFERRAL_FAX("eformAp", "referral_fax", "referral fax", false),
    BC_REFERRAL_NAME("eformAp", "bc_referral_name", "bc referral name", false),
    BC_REFERRAL_ADDRESS("eformAp", "bc_referral_address", "bc referral address", false),
    BC_REFERRAL_PHONE("eformAp", "bc_referral_phone", "bc referral phone", false),
    BC_REFERRAL_FAX("eformAp", "bc_referral_fax", "bc referral fax", false),
    BC_REFERRAL_NO("eformAp", "bc_referral_no", "bc referral no", false),
    CLINIC_NAME("eformAp", "clinic_name", "clinic name", false),
    CLINIC_PHONE("eformAp", "clinic_phone", "clinic phone", false),
    CLINIC_FAX("eformAp", "clinic_fax", "clinic fax", false),
    CLINIC_LABEL("eformAp", "clinic_label", "clinic label", true),
    CLINIC_ADDRESSLINE("eformAp", "clinic_addressline", "clinic addressline", false),
    CLINIC_ADDRESSLINEFULL("eformAp", "clinic_addresslinefull", "clinic addresslinefull", false),
    FORMATTED_CLINIC_ADDRESS("eformAp", "formatted_clinic_address", "formatted clinic address", false),
    CLINIC_ADDRESS("eformAp", "clinic_address", "clinic address", false),
    CLINIC_CITY("eformAp", "clinic_city", "clinic city", false),
    CLINIC_PROVINCE("eformAp", "clinic_province", "clinic province", false),
    CLINIC_POSTAL("eformAp", "clinic_postal", "clinic postal", false),
    _OTHER_ID("eformAp", "_other_id", " other id", false),
    DTAP_IMMUNIZATION_DATE("eformAp", "dtap_immunization_date", "dtap immunization date", false),
    FLU_IMMUNIZATION_DATE("eformAp", "flu_immunization_date", "flu immunization date", false),
    FOBT_IMMUNIZATION_DATE("eformAp", "fobt_immunization_date", "fobt immunization date", false),
    MAMMOGRAM_IMMUNIZATION_DATE("eformAp", "mammogram_immunization_date", "mammogram immunization date", false),
    PAP_IMMUNIZATION_DATE("eformAp", "pap_immunization_date", "pap immunization date", false),
    CYTOLOGY_NO("eformAp", "cytology_no", "cytology no", false),
    GUARDIAN_LABEL("eformAp", "guardian_label", "guardian label", true),
    GUARDIAN_LABEL2("eformAp", "guardian_label2", "guardian label2", true),
    EMAIL("eformAp", "email", "email", false),
    SERVICE_DATE("eformAp", "service_date", "service date", false),
    PRACTITIONER("eformAp", "practitioner", "practitioner", false),
    REF_DOCTOR("eformAp", "ref_doctor", "ref doctor", false),
    FEE_TOTAL("eformAp", "fee_total", "fee total", false),
    PAYMENT_TOTAL("eformAp", "payment_total", "payment total", false),
    REFUND_TOTAL("eformAp", "refund_total", "refund total", false),
    BALANCE_OWING("eformAp", "balance_owing", "balance owing", false),
    BILL_ITEM_NUMBER("eformAp", "bill_item_number", "bill item number", false),
    BILL_ITEM_DESCRIPTION("eformAp", "bill_item_description", "bill item description", false),
    BILL_ITEM_SERVICE_CODE("eformAp", "bill_item_service_code", "bill item service code", false),
    BILL_ITEM_QTY("eformAp", "bill_item_qty", "bill item qty", false),
    BILL_ITEM_DX("eformAp", "bill_item_dx", "bill item dx", false),
    BILL_ITEM_AMOUNT("eformAp", "bill_item_amount", "bill item amount", false),
    URINE_TOX_TEST_JSON("eformAp", "urine_tox_test_json", "urine tox test json", false),
    METHADONE_INDUCTION_ASSESSMENT_JSON("eformAp", "methadone_induction_assessment_json", "methadone induction assessment json", false),
    CURRENT_USER_FNAME("eformAp", "current_user_fname", "current user fname", false),
    CURRENT_USER_LNAME("eformAp", "current_user_lname", "current user lname", false),
    HAS_SIGNATURE_STAMP("eformAp", "has_signature_stamp", "has signature stamp", false),
    LAB_TEST_CODE("eformAp", "lab_test_code", "lab test code", false),
    TEST_NAME("eformAp", "test_name", "test name", false),
    EXCELLERIS_PATIENT_ID("eformAp", "excelleris_patient_id", "excelleris patient id", false),
    DOCTOR_EXCELLERIS_ID("eformAp", "doctor_excelleris_id", "doctor excelleris id", false),
    DOCTOR_LIFELABS_ID("eformAp", "doctor_lifelabs_id", "doctor lifelabs id", false),
    DOCTOR_FIRST_NAME("eformAp", "doctor_first_name", "doctor first name", false),
    DOCTOR_LAST_NAME("eformAp", "doctor_last_name", "doctor last name", false),
    DOCTOR_ADDRESS("eformAp", "doctor_address", "doctor address", false),
    T_VAGINAL_RECTAL_GROUP_B_SOURCE_NAMES("eformAp", "t_vaginal_rectal_group_b_source_names", "t vaginal rectal group b source names", false),
    T_CHLAMYDIA_SOURCE_NAMES("eformAp", "t_chlamydia_source_names", "t chlamydia source names", false),
    T_GC_SOURCE_NAMES("eformAp", "t_gc_source_names", "t gc source names", false),
    T_WOUND_SOURCE_NAMES("eformAp", "t_wound_source_names", "t wound source names", false),
    T_OTHER_SWABS_PUS_SOURCE_NAMES("eformAp", "t_other_swabs_pus_source_names", "t other swabs pus source names", false),
    T_THERAPEUTIC_DRUG_MONITOR_SOURCE_NAMES("eformAp", "t_therapeutic_drug_monitor_source_names", "t therapeutic drug monitor source names", false);
    
    // Map subject and field to enums
    private static final Map<String, SmartEncounterTemplatePlaceholder> templatePlaceholderSubjectMap = Maps.newHashMapWithExpectedSize(SmartEncounterTemplatePlaceholder.values().length);
    static {
        for (SmartEncounterTemplatePlaceholder value : SmartEncounterTemplatePlaceholder.values()) {
            templatePlaceholderSubjectMap.put(value.getSubjectAndField(), value);
        }
    }

    // Map enums and methods
    private static EnumMap<SmartEncounterTemplatePlaceholder, Method> placeholderMethodMap = new EnumMap<SmartEncounterTemplatePlaceholder, Method>(SmartEncounterTemplatePlaceholder.class);
    static {
        try {
            placeholderMethodMap.put(GLOBAL_TODAYS_DATE, SimpleDateFormat.class.getMethod("format", Date.class));
            placeholderMethodMap.put(DEMOGRAPHIC_FULL_NAME, Demographic.class.getDeclaredMethod("getFullName"));
            placeholderMethodMap.put(DEMOGRAPHIC_FIRST_NAME, Demographic.class.getDeclaredMethod("getFirstName"));
            placeholderMethodMap.put(DEMOGRAPHIC_LAST_NAME, Demographic.class.getDeclaredMethod("getLastName"));
            placeholderMethodMap.put(DEMOGRAPHIC_LABEL, Demographic.class.getDeclaredMethod("getLabel"));
            placeholderMethodMap.put(DEMOGRAPHIC_AGE, Demographic.class.getDeclaredMethod("getAge"));
            placeholderMethodMap.put(DEMOGRAPHIC_GENDER, Demographic.class.getDeclaredMethod("getSexDesc"));
            placeholderMethodMap.put(DEMOGRAPHIC_SUBJECT_PRONOUN, Demographic.class.getDeclaredMethod("getSubjectPronoun"));
            placeholderMethodMap.put(DEMOGRAPHIC_POSSESSIVE_PRONOUN, Demographic.class.getDeclaredMethod("getPossessivePronoun"));
            placeholderMethodMap.put(DEMOGRAPHIC_REFERRING_PHYSICIAN_LABEL, ProfessionalSpecialist.class.getDeclaredMethod("createContactString"));
            placeholderMethodMap.put(DEMOGRAPHIC_REFERRING_PHYSICIAN_FULLNAME, ProfessionalSpecialist.class.getDeclaredMethod("getFormattedName"));
            placeholderMethodMap.put(DEMOGRAPHIC_REFERRING_PHYSICIAN_FIRSTNAME, ProfessionalSpecialist.class.getDeclaredMethod("getFirstName"));
            placeholderMethodMap.put(DEMOGRAPHIC_REFERRING_PHYSICIAN_LASTNAME, ProfessionalSpecialist.class.getDeclaredMethod("getLastName"));
            placeholderMethodMap.put(DEMOGRAPHIC_FAMILY_DOCTOR_LABEL, ProfessionalSpecialist.class.getDeclaredMethod("createContactString"));
            placeholderMethodMap.put(DEMOGRAPHIC_FAMILY_DOCTOR_FULLNAME, ProfessionalSpecialist.class.getDeclaredMethod("getFormattedName"));
            placeholderMethodMap.put(DEMOGRAPHIC_FAMILY_DOCTOR_FIRSTNAME,
                ProfessionalSpecialist.class.getDeclaredMethod("getFirstName"));
            placeholderMethodMap.put(DEMOGRAPHIC_FAMILY_DOCTOR_LASTNAME,
                ProfessionalSpecialist.class.getDeclaredMethod("getLastName"));
            placeholderMethodMap.put(PROVIDER_FULLNAME, Provider.class.getDeclaredMethod("getFullName"));
            placeholderMethodMap.put(PROVIDER_FIRST_NAME, Provider.class.getDeclaredMethod("getFirstName"));
            placeholderMethodMap.put(PROVIDER_LAST_NAME, Provider.class.getDeclaredMethod("getLastName"));
            placeholderMethodMap.put(APPOINTMENT_NO, Appointment.class.getDeclaredMethod("getId"));
            placeholderMethodMap.put(APPOINTMENT_DATE, Appointment.class.getDeclaredMethod("getAppointmentDate"));
            placeholderMethodMap.put(APPOINTMENT_PROVIDER_ID, Appointment.class.getDeclaredMethod("getProviderNo"));
            placeholderMethodMap.put(APPOINTMENT_PROVIDER_NAME, Appointment.class.getDeclaredMethod("getProviderName"));

            placeholderMethodMap.put(TODAY, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(TIME, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CURRENT_FORM_ID, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CURRENT_FORM_DATA_ID, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CURRENT_USER, APExecute.class.getMethod("currentUser", String.class, String.class));
            placeholderMethodMap.put(CURRENT_USER_FNAME_LNAME, APExecute.class.getMethod("currentUser", String.class, String.class));
            placeholderMethodMap.put(CURRENT_USER_FNAME_LNAME_CREDENTIALS, APExecute.class.getMethod("currentUser", String.class, String.class));
            placeholderMethodMap.put(CURRENT_USER_OHIP_NO, APExecute.class.getMethod("currentUser", String.class, String.class));
            placeholderMethodMap.put(CURRENT_USER_SPECIALTY_CODE, APExecute.class.getMethod("currentUser", String.class, String.class));
            placeholderMethodMap.put(CURRENT_USER_CPSID, APExecute.class.getMethod("currentUser", String.class, String.class));
            placeholderMethodMap.put(CURRENT_USER_ID, APExecute.class.getMethod("currentUser", String.class, String.class));
            placeholderMethodMap.put(CURRENT_USER_SIGNATURE, APExecute.class.getMethod("currentUser", String.class, String.class));
            placeholderMethodMap.put(PATIENT_NAME, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(FIRST_LAST_NAME, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PATIENT_NAMEL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PATIENT_NAMEF, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PATIENT_ALIAS, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PATIENT_ID, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(LABEL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(ADDRESS, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(ADDRESSLINE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(ADDRESS_STREET_NUMBER_AND_NAME, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PROVINCE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CITY, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(POSTAL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOB, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOBC, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOBC2, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOBC3, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOB_YEAR, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOB_MONTH, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOB_DAY, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(NAMEADDRESS, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(HIN, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(HINC, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(HINVERSION, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(HC_RENEW_DATE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CHARTNO, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PHONE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PHONE2, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CELL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PHONE_EXTENSION, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PHONE2_EXTENSION, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(AGE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(AGE_IN_MONTHS, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(AGECOMPLEX, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(SEX, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(SIN, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PATIENT_TITLE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(MEDICAL_HISTORY, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(OTHER_MEDICATIONS_HISTORY, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(SOCIAL_FAMILY_HISTORYC, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(ONGOINGCONCERNS, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(REMINDERS, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(RISK_FACTORS_JSON, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(FAMILY_HISTORY_JSON, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(RECENT_NOTE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DXREGISTRY, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(OHIPDXCODE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(ALLERGIES_DES, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(ALLERGIES_DES_NO_ARCHIVED, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(RECENT_RX, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(TODAY_RX, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DRUGLIST_GENERIC, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DRUGLIST_TRADE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DRUGLIST_LINE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(ONGTPAL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(ONEDB, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BCGTPAL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BCEDD, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOCTOR, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOCTOR_PROVIDER_NO, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOCTOR_OHIP_NO, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOCTOR_SPECIALTY_CODE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOCTOR_CPSID, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOCTOR_TITLE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PROVIDER_NAME, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PROVIDER_NAME_FIRST_INIT, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOCTOR_WORK_PHONE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOCTOR_SIGNATURE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(APPT_NO, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(REFERRAL_NAME, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(REFERRAL_ADDRESS, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(REFERRAL_PHONE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(REFERRAL_FAX, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BC_REFERRAL_NAME, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BC_REFERRAL_ADDRESS, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BC_REFERRAL_PHONE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BC_REFERRAL_FAX, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BC_REFERRAL_NO, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CLINIC_NAME, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CLINIC_PHONE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CLINIC_FAX, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CLINIC_LABEL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CLINIC_ADDRESSLINE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CLINIC_ADDRESSLINEFULL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(FORMATTED_CLINIC_ADDRESS, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CLINIC_ADDRESS, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CLINIC_CITY, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CLINIC_PROVINCE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CLINIC_POSTAL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(_OTHER_ID, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DTAP_IMMUNIZATION_DATE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(FLU_IMMUNIZATION_DATE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(FOBT_IMMUNIZATION_DATE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(MAMMOGRAM_IMMUNIZATION_DATE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PAP_IMMUNIZATION_DATE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CYTOLOGY_NO, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(GUARDIAN_LABEL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(GUARDIAN_LABEL2, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(EMAIL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(SERVICE_DATE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PRACTITIONER, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(REF_DOCTOR, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(FEE_TOTAL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(PAYMENT_TOTAL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(REFUND_TOTAL, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BALANCE_OWING, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BILL_ITEM_NUMBER, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BILL_ITEM_DESCRIPTION, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BILL_ITEM_SERVICE_CODE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BILL_ITEM_QTY, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BILL_ITEM_DX, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(BILL_ITEM_AMOUNT, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(URINE_TOX_TEST_JSON, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(METHADONE_INDUCTION_ASSESSMENT_JSON, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(CURRENT_USER_FNAME, APExecute.class.getMethod("currentUser", String.class, String.class));
            placeholderMethodMap.put(CURRENT_USER_LNAME, APExecute.class.getMethod("currentUser", String.class, String.class));
            placeholderMethodMap.put(HAS_SIGNATURE_STAMP, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(LAB_TEST_CODE, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(TEST_NAME, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(EXCELLERIS_PATIENT_ID, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOCTOR_EXCELLERIS_ID, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOCTOR_LIFELABS_ID, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOCTOR_FIRST_NAME, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOCTOR_LAST_NAME, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(DOCTOR_ADDRESS, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(T_VAGINAL_RECTAL_GROUP_B_SOURCE_NAMES, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(T_CHLAMYDIA_SOURCE_NAMES, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(T_GC_SOURCE_NAMES, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(T_WOUND_SOURCE_NAMES, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(T_OTHER_SWABS_PUS_SOURCE_NAMES, APExecute.class.getMethod("execute", String.class, String.class));
            placeholderMethodMap.put(T_THERAPEUTIC_DRUG_MONITOR_SOURCE_NAMES, APExecute.class.getMethod("execute", String.class, String.class));
        } catch (NoSuchMethodException e) {
            MiscUtils.getLogger().error("Error on resolving smart template placeholder", e);
        }
    }
    
    private static ArrayList<String> referralProviderPlaceholders = new ArrayList<String>();
    static {
        referralProviderPlaceholders.add(DEMOGRAPHIC_REFERRING_PHYSICIAN_LABEL.getSubjectAndField());
        referralProviderPlaceholders.add(DEMOGRAPHIC_REFERRING_PHYSICIAN_FULLNAME.getSubjectAndField());
        referralProviderPlaceholders.add(DEMOGRAPHIC_REFERRING_PHYSICIAN_FIRSTNAME.getSubjectAndField());
        referralProviderPlaceholders.add(DEMOGRAPHIC_REFERRING_PHYSICIAN_LASTNAME.getSubjectAndField());
        referralProviderPlaceholders.add(REFERRAL_NAME.getSubjectAndField());
        referralProviderPlaceholders.add(REFERRAL_ADDRESS.getSubjectAndField());
        referralProviderPlaceholders.add(REFERRAL_PHONE.getSubjectAndField());
        referralProviderPlaceholders.add(REFERRAL_FAX.getSubjectAndField());
        referralProviderPlaceholders.add(BC_REFERRAL_NAME.getSubjectAndField());
        referralProviderPlaceholders.add(BC_REFERRAL_ADDRESS.getSubjectAndField());
        referralProviderPlaceholders.add(BC_REFERRAL_PHONE.getSubjectAndField());
        referralProviderPlaceholders.add(BC_REFERRAL_FAX.getSubjectAndField());
        referralProviderPlaceholders.add(BC_REFERRAL_NO.getSubjectAndField());
    }

    private String subject;
    private String field;
    private String title;
    private boolean isTextBlock;

    SmartEncounterTemplatePlaceholder(String subject, String field, String title, boolean isTextBlock) {
        this.subject = subject;
        this.field = field;
        this.title = title;
        this.isTextBlock = isTextBlock;
    }

    public String getSubject() {
        return subject;
    }

    public String getField() {
        return field;
    }

    public String getTitle() {
        return title;
    }

    public String getSubjectAndField() {
        return subject + "." + field;
    }

    public boolean isTextBlock() {
        return isTextBlock;
    }
    
    public String toJavascriptCreateMethod() {
        String createMethodString = "";
        if (isTextBlock) {
            createMethodString += "TemplateBlockPlaceholder.create({";
        } else {
            createMethodString += "TemplatePlaceholder.create({";
        }
        
        createMethodString += "subject: '" + Encode.forJavaScriptBlock(subject) + "', ";
        createMethodString += "marker: '" + Encode.forJavaScriptBlock(getSubjectAndField()) + "', ";
        createMethodString += "title: '" + Encode.forJavaScript(title) + "'})";
        
        return createMethodString;
    }

    public static Method getMethodBySubjectAndField(String subjectAndField) {
        SmartEncounterTemplatePlaceholder templatePlaceholder = templatePlaceholderSubjectMap.get(subjectAndField);
        return templatePlaceholder != null ? placeholderMethodMap.get(templatePlaceholder) : null;
    }
    public static SmartEncounterTemplatePlaceholder getPlaceholderSubjectAndField(String subjectAndField) {
        return templatePlaceholderSubjectMap.get(subjectAndField);
    }
    
    public static boolean isBlockPlaceholder(String subjectAndField) {
        SmartEncounterTemplatePlaceholder placeholder = templatePlaceholderSubjectMap.get(subjectAndField);
        return placeholder != null && placeholder.isTextBlock();
    }
    
    public static ArrayList<String> getReferralProviderPlaceholders() {
        return referralProviderPlaceholders;
    }
    
    public static List<String> getDatePlaceholderNames() {
        List<String> datePlaceholderNames = new ArrayList<String>();
        datePlaceholderNames.add(GLOBAL_TODAYS_DATE.getSubjectAndField());
        datePlaceholderNames.add(APPOINTMENT_DATE.getSubjectAndField());
        datePlaceholderNames.add(HC_RENEW_DATE.getSubjectAndField());
        datePlaceholderNames.add(DTAP_IMMUNIZATION_DATE.getSubjectAndField());
        datePlaceholderNames.add(FLU_IMMUNIZATION_DATE.getSubjectAndField());
        datePlaceholderNames.add(FOBT_IMMUNIZATION_DATE.getSubjectAndField());
        datePlaceholderNames.add(MAMMOGRAM_IMMUNIZATION_DATE.getSubjectAndField());
        datePlaceholderNames.add(PAP_IMMUNIZATION_DATE.getSubjectAndField());
        datePlaceholderNames.add(SERVICE_DATE.getSubjectAndField());
        return datePlaceholderNames;
    }

  public static Set<SmartEncounterTemplatePlaceholder> getCurrentUserPlaceholders() {
    return EnumSet.of(
        CURRENT_USER,
        CURRENT_USER_FNAME_LNAME,
        CURRENT_USER_FNAME_LNAME_CREDENTIALS,
        CURRENT_USER_OHIP_NO,
        CURRENT_USER_SPECIALTY_CODE,
        CURRENT_USER_CPSID,
        CURRENT_USER_ID,
        CURRENT_USER_SIGNATURE,
        CURRENT_USER_FNAME,
        CURRENT_USER_LNAME);
  }

  public static Set<SmartEncounterTemplatePlaceholder> getCurrentFormPlaceholders() {
    return EnumSet.of(
        CURRENT_FORM_ID,
        CURRENT_FORM_DATA_ID);
  }
}