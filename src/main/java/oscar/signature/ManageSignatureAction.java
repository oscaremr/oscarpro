/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.signature;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.codehaus.jackson.map.ObjectMapper;
import org.oscarehr.common.dao.DigitalSignatureDao;
import org.oscarehr.common.dao.DigitalSignatureFavouriteDao;
import org.oscarehr.common.model.DigitalSignature;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

public class ManageSignatureAction extends DispatchAction {

  public ActionForward updateSignature(
      final ActionMapping mapping,
      final ActionForm form,
      final HttpServletRequest request,
      final HttpServletResponse response) throws IOException {
    // grab id, label, favourite, and deleted from request
    int id = Integer.parseInt(request.getParameter("id"));
    String label = request.getParameter("label");
    val archived = Boolean.parseBoolean(request.getParameter("isArchived"));

    // get signature from database using dao
    DigitalSignatureDao digitalSignatureDao = SpringUtils.getBean(DigitalSignatureDao.class);
    DigitalSignature signature = digitalSignatureDao.find(id);
    // update label
    signature.setLabel(label);

    // get signature favourite from database using dao
    val digitalSignatureFavouriteDao = (DigitalSignatureFavouriteDao) SpringUtils.getBean(
        DigitalSignatureFavouriteDao.class);
    val digitalSignatureFavourite = digitalSignatureFavouriteDao.findFavouriteSignatureById(id);
    digitalSignatureFavourite.setArchived(archived);

    // save signature and signature favourite
    digitalSignatureDao.merge(signature);
    digitalSignatureFavouriteDao.merge(digitalSignatureFavourite);

    // convert the signature to JSON
    ObjectMapper objectMapper = new ObjectMapper();
    String jsonSignature = objectMapper.writeValueAsString(signature);
    // set the JSON response
    response.setContentType("application/json");
    response.getWriter().write(jsonSignature);
    // return null as we're using AJAX
    return null;
  }

  public ActionForward getSignatures(
      final ActionMapping mapping,
      final ActionForm form,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws IOException {
    String parameter = request.getParameter("archived");
    boolean archived = Boolean.parseBoolean(parameter);
    DigitalSignatureDao digitalSignatureDao = SpringUtils.getBean(DigitalSignatureDao.class);
    response.setContentType("application/json");
    response.getWriter().write(new ObjectMapper().writeValueAsString(
        digitalSignatureDao.getLatestSignatures(archived)));
    // return null as we're using AJAX
    return null;
  }

  public ActionForward getProviderSignatures(
      final ActionMapping mapping,
      final ActionForm form,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws IOException {
    String parameter = request.getParameter("archived");
    boolean archived = Boolean.parseBoolean(parameter);
    String providerNo = LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo();
    DigitalSignatureDao digitalSignatureDao = SpringUtils.getBean(DigitalSignatureDao.class);
    response.setContentType("application/json");
    response.getWriter().write(new ObjectMapper().writeValueAsString(
        digitalSignatureDao.getProviderSignatures(providerNo, archived)));
    // return null as we're using AJAX
    return null;
  }
}