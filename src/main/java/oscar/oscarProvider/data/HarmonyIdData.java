/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.oscarProvider.data;

import org.oscarehr.common.dao.PropertyDao;
import org.oscarehr.common.model.Property;
import org.oscarehr.util.SpringUtils;
import java.util.List;

public final class HarmonyIdData {

  private static final String PROPERTY_KEY = "harmonyUser";

  private static PropertyDao dao = SpringUtils.getBean(PropertyDao.class);

  private HarmonyIdData() {}

  public static String getHarmonyId(String providerNo) {
    List<Property> props = dao.findByNameAndProvider(PROPERTY_KEY, providerNo);
    if (props.size() > 0) {
      return props.get(0).getValue();
    }
    return "";
  }

  public static boolean setId(String providerId, String id) {
    boolean ret = true;

    List<Property> props = dao.findByNameAndProvider(PROPERTY_KEY, providerId);
    Property p = new Property();
    if(props.size()>0) {
      p = props.get(0);
      p.setValue(id);
      dao.merge(p);
    } else {
      p.setName(PROPERTY_KEY);
      p.setValue(id);
      p.setProviderNo(providerId);
      dao.persist(p);
    }
    return ret;
  }

}
