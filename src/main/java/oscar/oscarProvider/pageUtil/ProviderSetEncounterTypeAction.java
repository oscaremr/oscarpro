/**
 * Copyright (c) 2022 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.oscarProvider.pageUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

public class ProviderSetEncounterTypeAction extends Action {

  public ActionForward execute(
      ActionMapping mapping,
      ActionForm form,
      HttpServletRequest request,
      HttpServletResponse response)
      throws Exception {
    val providerNumber =
        LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo();
    if (providerNumber == null) {
        return mapping.findForward("eject");
    }
    val encounterTypeActionForm = (DynaActionForm) form;
    val propertyDao = (UserPropertyDAO) SpringUtils.getBean(UserPropertyDAO.class);
    propertyDao.saveProp(
        providerNumber,
        "default_encounter_type",
        (String) encounterTypeActionForm.get("encounterType")
    );
    request.setAttribute("status", "complete");

    return mapping.findForward("success");
  }
}