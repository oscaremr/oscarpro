/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.oscarProvider.pageUtil;

import org.apache.struts.action.*;
import org.oscarehr.util.LoggedInInfo;
import oscar.oscarProvider.data.HarmonyIdData;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ProEditHarmonyIdAction extends Action {

  @Override
  public ActionForward execute(
      ActionMapping mapping,
      ActionForm form,
      HttpServletRequest request,
      HttpServletResponse response)
      throws Exception {

    String forward;
    String providerNo = LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo();

    if (providerNo == null) {
        return mapping.findForward("eject");
    }

    DynaActionForm frm = (DynaActionForm) form;
    String harmonyId = (String) frm.get("harmonyId");

    if (HarmonyIdData.setId(providerNo, harmonyId)) {
      request.setAttribute("status", "complete");
      forward = "success";
    } else {
      forward = "error";
    }

    return mapping.findForward(forward);
  }
}
