/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.oscarLab.ca.all.parsers;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

@Slf4j
public class MessageHandlerUtils {

  protected static String handleException(Exception exception) {
    log.error(ExceptionUtils.getStackTrace(exception));
    return "";
  }
}
