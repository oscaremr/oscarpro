/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.oscarLab.ca.all.parsers;

/**
 * Interface for MessageHandlers that include a health number version
 */
public interface HinVersionMessageHandler {

    public String getHealthNumVersion();
}
