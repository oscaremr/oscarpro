package oscar.oscarLab.ca.all.pageUtil;

import org.apache.commons.lang.StringUtils;

public class LabUtils {
  public static String getHl7OrderStatusString(final String msgType, final String orderStatus) {
    final String trimmedMsgType = StringUtils.trimToEmpty(msgType);
    final String trimmedOrderStatus = StringUtils.trimToEmpty(orderStatus);
    switch (trimmedOrderStatus) {
      case "I":
        return "Pending";
      case "P":
        return StringUtils.equals(trimmedMsgType, "PATHL7") ? "Preliminary" : trimmedOrderStatus;
      case "A":
        return "Partial results";
      case "F":
        return StringUtils.equals(trimmedMsgType, "PATHL7") ? "Complete" : "Final";
      case "R":
        return "Retransmitted";
      case "C":
        return "Corrected";
      case "X":
        return "Deleted";
      default:
        return StringUtils.isBlank(trimmedOrderStatus) ? "Unknown" : trimmedOrderStatus;
    }
  }
}
