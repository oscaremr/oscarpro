package oscar.oscarLab.ca.all.pageUtil;

import java.awt.Color;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.oscarehr.casemgmt.service.CaseManagementManager;
import org.oscarehr.common.printing.FontSettings;
import org.oscarehr.common.printing.PdfWriterFactory;
import org.oscarehr.util.SpringUtils;

import oscar.oscarLab.ca.on.CML.CMLLabTest;
import oscar.oscarLab.ca.on.CML.CMLLabTest.GroupResults;
import oscar.oscarLab.ca.on.CML.CMLLabTest.LabResult;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

public class CMLPdfCreator extends PdfPageEventHelper{

		private OutputStream os;

	    private CMLLabTest lab = new CMLLabTest();
	    
	    private int versionNum;
	    private String[] multiID;
	    private String id;

	    private Document document;
	    private BaseFont bf;
	    private Font font;
	    private Font boldFont;
	    private int linenum=0;
	    private String dateLabReceived;
	    
	    CaseManagementManager caseManagementManager = (CaseManagementManager) SpringUtils.getBean("caseManagementManager");
	    
	    public CMLPdfCreator(HttpServletRequest request, OutputStream os){
	    	this(os, (request.getParameter("segmentID")!=null?request.getParameter("segmentID"):(String)request.getAttribute("segmentID")), (request.getParameter("providerNo")!=null?request.getParameter("providerNo"):(String)request.getAttribute("providerNo")));
	    }
	    
	    public CMLPdfCreator(OutputStream os, String segmentId, String providerNo){
	    	this.os = os;
	        this.id = segmentId;
	        
	        // find cml lab
	        lab.populateLab(segmentId);
	        
	        dateLabReceived = lab.printDate + " " + lab.printTime;
	        
	        // determine lab version
	        this.multiID = lab.multiLabId.split(",");

	        int i=0;
	        while (!multiID[i].equals(id)){
	            i++;
	        }
	        this.versionNum = i+1;
	    }
	    
	    public void printPdf() throws IOException, DocumentException{
	    	
	    	document = new Document();
	    	PdfWriter writer = PdfWriterFactory.newInstance(document, os, FontSettings.HELVETICA_10PT);
	    	
	    	//Set page event, function onEndPage will execute each time a page is finished being created
	        writer.setPageEvent(this);

	        document.setPageSize(PageSize.LETTER);
	        document.addTitle("Title of the Document");
	        document.addCreator("OSCAR");
	        document.open();
	        
	        //Create the fonts that we are going to use
	        bf = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
	        font = new Font(bf, 9, Font.NORMAL);
	        boldFont = new Font(bf, 10, Font.BOLD);
	        //  redFont = new Font(bf, 9, Font.NORMAL, Color.RED);
	        
	        // add the header table containing the patient and lab info to the document
	        createInfoTable();
	        
	        // add the tests and test info for each header
	        ArrayList<GroupResults> groupLabs = lab.getGroupResults(lab.labResults);
	        
	        for(int i=0; i<groupLabs.size(); i++){
	        	linenum=0;
                CMLLabTest.GroupResults gResults = (CMLLabTest.GroupResults) groupLabs.get(i);
	        	addLabCategory(gResults, this.id);
	        }
	        
	        // add end of report table
	        PdfPTable table = new PdfPTable(1);
	        table.setWidthPercentage(100);
	        PdfPCell cell = new PdfPCell();
	        cell.setBorder(0);
	        cell.setPhrase(new Phrase("  "));
	        table.addCell(cell);
	        cell.setBorder(15);
	        cell.setBackgroundColor(new Color(210, 212, 255));
	        cell.setPhrase(new Phrase("END OF REPORT", boldFont));
	        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
	        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
	        table.addCell(cell);
	        document.add(table);

	        document.close();

	        os.flush();
	    }
	    
	    /*
		 * Given the name of a lab category this method will add the category
		 * header, the test result headers and the test results for that category.
		 */
		private void addLabCategory(GroupResults gResults, String segmentId) throws DocumentException {
			float[] mainTableWidths = new float[] {5f, 2f, 1f, 3f, 2f, 4f, 2f, 2f };
			PdfPTable table = new PdfPTable(mainTableWidths);
			table.setHeaderRows(2);
			table.setWidthPercentage(100);

			PdfPCell cell = new PdfPCell();
			// category name
			cell.setPadding(3);
			cell.setPhrase(new Phrase("  "));
			cell.setBorder(0);
			cell.setColspan(8);
			table.addCell(cell);
			
			cell.setBorder(15);
			cell.setPadding(3);
			cell.setColspan(2);
			cell.setPhrase(new Phrase(gResults.groupName.replaceAll("<br\\s*/*>", "\n").replaceAll("\\\\.br\\\\", "\n").replaceAll("&nbsp;", " "),
					new Font(bf, 12, Font.BOLD)));
			table.addCell(cell);
			cell.setPhrase(new Phrase("  "));
			cell.setBorder(0);
			cell.setColspan(6);
			table.addCell(cell);

			// table headers
			cell.setColspan(1);
			cell.setBorder(15);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setBackgroundColor(new Color(210, 212, 255));
			cell.setPhrase(new Phrase("Test Name(s)", boldFont));
			table.addCell(cell);
			cell.setPhrase(new Phrase("Result", boldFont));
			table.addCell(cell);
			cell.setPhrase(new Phrase("Abn", boldFont));
			table.addCell(cell);
			cell.setPhrase(new Phrase("Reference Range", boldFont));
			table.addCell(cell);
			cell.setPhrase(new Phrase("Units", boldFont));
			table.addCell(cell);
			cell.setPhrase(new Phrase("Date/Time Completed", boldFont));
			table.addCell(cell);
			cell.setPhrase(new Phrase("Test Location", boldFont));
			table.addCell(cell);
			cell.setPhrase(new Phrase("Status", boldFont));
			table.addCell(cell); 

			// add test results
			ArrayList<LabResult> labResults = gResults.getLabResults();
			cell.setBorder(12);
			cell.setBorderColor(Color.BLACK); // cell.setBorderColor(Color.WHITE);
			cell.setBackgroundColor(new Color(255, 255, 255));

			for (int j = 0; j < labResults.size(); j++) {
                 CMLLabTest.LabResult thisResult = (CMLLabTest.LabResult) labResults.get(j);
                
                 //set value
                 if (thisResult.isLabResult()){
					cell.setColspan(1);
					cell.setBorder(15);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					if(linenum % 2 == 1){
						cell.setBackgroundColor(new Color(224, 224, 255));
					}
					
					if(null != thisResult.testName){
						cell.setPhrase(new Phrase(thisResult.testName, boldFont));
					}else{
						cell.setPhrase(new Phrase(" ", boldFont));
					}
					table.addCell(cell);
					if(null != thisResult.result){
						cell.setPhrase(new Phrase(thisResult.result, boldFont));
					}else{
						cell.setPhrase(new Phrase(" ", boldFont));
					}
					table.addCell(cell);
					if(null != thisResult.abn){
						cell.setPhrase(new Phrase(thisResult.abn, boldFont));
					}else{
						cell.setPhrase(new Phrase(" ", boldFont));
					}
					table.addCell(cell);
					cell.setPhrase(new Phrase(thisResult.getReferenceRange(), boldFont));
					table.addCell(cell);
					if(null != thisResult.units){
						cell.setPhrase(new Phrase(thisResult.units, boldFont));
					}else{
						cell.setPhrase(new Phrase(" ", boldFont));
					}
					table.addCell(cell);
					cell.setPhrase(new Phrase(lab.collectionDate, boldFont));
					table.addCell(cell);
					if(null != thisResult.locationId){
						cell.setPhrase(new Phrase(thisResult.locationId, boldFont));
					}else{
						cell.setPhrase(new Phrase(" ", boldFont));
					}
					table.addCell(cell);
					cell.setPhrase(new Phrase(" ", boldFont));
					table.addCell(cell);
                 }else{
                	 cell.setColspan(8);
					 cell.setBorder(15);
					 cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					 if(linenum % 2 == 1){
						 cell.setBackgroundColor(new Color(224, 224, 255));
					 }
					 cell.setPhrase(new Phrase(thisResult.description, boldFont));
					 table.addCell(cell);
                 }
			}

			document.add(table);
		}
	    
	    /*
	     *  createInfoTable creates and adds the table at the top of the document
	     *  which contains the patient and lab information
	     */
	    private void createInfoTable() throws DocumentException{

	        //Create patient info table
	        PdfPCell cell = new PdfPCell();
	        cell.setBorder(0);
	        float[] pInfoWidths = {2f, 4f, 3f, 2f};
	        PdfPTable pInfoTable = new PdfPTable(pInfoWidths);
	        cell.setPhrase(new Phrase("Patient Name: ", boldFont));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase(lab.pLastName + ", " + lab.pFirstName, font));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase("Home Phone: ", boldFont));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase(lab.pPhone, font));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase("Date of Birth: ", boldFont));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase(lab.pDOB, font));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase("Work Phone: ", boldFont));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase("", font));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase("Age: ", boldFont));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase(lab.getAge(), font));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase("Sex: ", boldFont));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase(lab.pSex, font));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase("Health #: ", boldFont));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase(lab.pHealthNum, font));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase("Patient Location: ", boldFont));
	        pInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase("", font));
	        pInfoTable.addCell(cell);

	        //Create results info table
	        PdfPTable rInfoTable = new PdfPTable(2);
	        cell.setPhrase(new Phrase("Date of Service: ", boldFont));
	        rInfoTable.addCell(cell);
	        if(null != lab.serviceDate){
	        	cell.setPhrase(new Phrase(lab.serviceDate, font));
	        }else{
	        	cell.setPhrase(new Phrase("", font));
	        }
	        rInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase("Date Received: ", boldFont));
	        rInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase(dateLabReceived, font));
	        rInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase("Report Status: ", boldFont));
	        rInfoTable.addCell(cell);
	        if(null != lab.status){
	        	cell.setPhrase(new Phrase(lab.status.equals("F") ? "Final" :"Partial", font));
	        }else{
	        	cell.setPhrase(new Phrase("Partial", font));
	        }
	        rInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase("Client Ref. #: ", boldFont));
	        rInfoTable.addCell(cell);
	        if(null != lab.docNum){
	        	cell.setPhrase(new Phrase(lab.docNum, font));
	        }else{
	        	cell.setPhrase(new Phrase(lab.docNum, font));
	        }
	        rInfoTable.addCell(cell);
	        cell.setPhrase(new Phrase("Accession #: ", boldFont));
	        rInfoTable.addCell(cell);
	        if(null != lab.accessionNum){
	        	cell.setPhrase(new Phrase(lab.accessionNum, font));
	        }else{
	        	cell.setPhrase(new Phrase("", font));
	        }
	        rInfoTable.addCell(cell);

	        //Create client table
	        float[] clientWidths = {2f, 3f};
	        Phrase clientPhrase = new Phrase();
	        PdfPTable clientTable = new PdfPTable(clientWidths);
	        clientPhrase.add(new Chunk("Requesting Client:  ", boldFont));
	        if(null != lab.docName){
	        	clientPhrase.add(new Chunk(lab.docName, font));
	        }else{
	        	clientPhrase.add(new Chunk("", font));
	        }
	        cell.setPhrase(clientPhrase);
	        clientTable.addCell(cell);

	        clientPhrase = new Phrase();
	        clientPhrase.add(new Chunk("cc: Client:  ", boldFont));
	        String ccDocs = "";
	        clientPhrase.add(new Chunk(ccDocs, font));
	        cell.setPhrase(clientPhrase);
	        clientTable.addCell(cell);

	        //Create header info table
	        float[] tableWidths = {2f, 1f};
	        PdfPTable table = new PdfPTable(tableWidths);
	        if (multiID.length > 1){
	            cell = new PdfPCell(new Phrase("Version: "+versionNum+" of "+multiID.length, boldFont));
	            cell.setBackgroundColor(new Color(210, 212, 255));
	            cell.setPadding(3);
	            cell.setColspan(2);
	            table.addCell(cell);
	        }
	        cell = new PdfPCell(new Phrase("Detail Results: Patient Info", boldFont));
	        cell.setBackgroundColor(new Color(210, 212, 255));
	        cell.setPadding(3);
	        table.addCell(cell);
	        cell.setPhrase(new Phrase("Results Info", boldFont));
	        table.addCell(cell);

	        // add the created tables to the document
	        table = addTableToTable(table, pInfoTable, 1);
	        table = addTableToTable(table, rInfoTable, 1);
	        table = addTableToTable(table, clientTable, 2);

	        table.setWidthPercentage(100);

	        document.add(table);
	    }
	    
	    /*
	     *  addTableToTable(PdfPTable main, PdfPTable add) adds the table 'add' as
	     *  a cell spanning 'colspan' columns to the table main.
	     */
	    private PdfPTable addTableToTable(PdfPTable main, PdfPTable add, int colspan){
	        PdfPCell cell = new PdfPCell(add);
	        cell.setPadding(3);
	        cell.setColspan(colspan);
	        main.addCell(cell);
	        return main;
	    }
}
