/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.oscarLab.ca.all.pageUtil;

import com.lowagie.text.Paragraph;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import lombok.val;
import lombok.var;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.jpedal.PdfDecoder;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.hospitalReportManager.HRMReport;
import org.oscarehr.hospitalReportManager.HRMReportParser;
import org.oscarehr.hospitalReportManager.dao.HRMCategoryDao;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentCommentDao;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentDao;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentSubClassDao;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentToDemographicDao;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentToProviderDao;
import org.oscarehr.hospitalReportManager.dao.HRMProviderConfidentialityStatementDao;
import org.oscarehr.hospitalReportManager.model.HRMCategory;
import org.oscarehr.hospitalReportManager.model.HRMDocument;
import org.oscarehr.hospitalReportManager.model.HRMDocumentComment;
import org.oscarehr.hospitalReportManager.model.HRMDocumentSubClass;
import org.oscarehr.hospitalReportManager.model.HRMDocumentToDemographic;
import org.oscarehr.hospitalReportManager.model.HRMDocumentToProvider;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

import oscar.oscarDemographic.data.DemographicNameAgeString;

import com.itextpdf.text.Element;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.LineSeparator;
import oscar.util.UtilMisc;

public class HrmPDFCreator extends PdfPageEventHelper{
	private OutputStream os = null;
	private String hrmDocumentId = null;
	private BaseFont bf = null;
	private Font font = null, boldFont = null, italicFont = null;
	private String confidentialityStatement = null;
	
	private HRMDocumentDao hrmDocumentDao = (HRMDocumentDao) SpringUtils.getBean("HRMDocumentDao");
	private HRMDocumentToDemographicDao hrmDocumentToDemographicDao = (HRMDocumentToDemographicDao) SpringUtils.getBean("HRMDocumentToDemographicDao");
	private HRMDocumentToProviderDao hrmDocumentToProviderDao = (HRMDocumentToProviderDao) SpringUtils.getBean("HRMDocumentToProviderDao");
	private HRMDocumentSubClassDao hrmDocumentSubClassDao = (HRMDocumentSubClassDao) SpringUtils.getBean("HRMDocumentSubClassDao");
	private HRMCategoryDao hrmCategoryDao = (HRMCategoryDao) SpringUtils.getBean("HRMCategoryDao");
	private HRMDocumentCommentDao hrmDocumentCommentDao = (HRMDocumentCommentDao) SpringUtils.getBean("HRMDocumentCommentDao");
	private HRMProviderConfidentialityStatementDao hrmProviderConfidentialityStatementDao = (HRMProviderConfidentialityStatementDao) SpringUtils.getBean("HRMProviderConfidentialityStatementDao");
	private ProviderDao providerDao = (ProviderDao) SpringUtils.getBean("providerDao");
	
	public HrmPDFCreator(OutputStream os, String hrmDocumentId) {
		this.os = os;
		this.hrmDocumentId = hrmDocumentId;
 	}
	
	public void printPdf(LoggedInInfo loggedInInfo) throws IOException, DocumentException{
		// get hrm document
		if (hrmDocumentId == null) {
			return;
		}
		if (hrmDocumentId.startsWith("H")) {
			hrmDocumentId = hrmDocumentId.replace("H", "");
			if (!StringUtils.isNumeric(hrmDocumentId)) {
				return;
			}
		}
		HRMDocument hrmDoc = hrmDocumentDao.findById(Integer.parseInt(hrmDocumentId)).get(0);
		if (hrmDoc == null) {
			return;
		}
		MiscUtils.getLogger().debug("reading repotFile : "+hrmDoc.getReportFile());
		HRMReport report = HRMReportParser.parseReport(loggedInInfo, hrmDoc.getReportFile());
		if(report == null) {
			return ;
		}
		
		String loggedInProviderNo = loggedInInfo.getLoggedInProviderNo();
		confidentialityStatement = hrmProviderConfidentialityStatementDao.getConfidentialityStatementForProvider(loggedInProviderNo);
		List<HRMDocumentToDemographic> demographicLinkList = 
				hrmDocumentToDemographicDao.findByHrmDocumentId(hrmDoc.getId().toString());
        HRMDocumentToDemographic demographicLink = (demographicLinkList.size() > 0 ? demographicLinkList.get(0) 
        		: null);
        List<HRMDocumentToProvider> providerLinkList = hrmDocumentToProviderDao.findByHrmDocumentIdNoSystemUser(hrmDoc.getId());
        List<HRMDocumentSubClass> subClassList = hrmDocumentSubClassDao.getSubClassesByDocumentId(hrmDoc.getId());
        HRMDocumentSubClass hrmDocumentSubClass=null;
        if (subClassList!= null)
        {
        	for (HRMDocumentSubClass temp : subClassList)
        	{
        		if (temp.isActive())
        		{
        			hrmDocumentSubClass=temp;
        			break;
        		}
        	}
        }
        HRMCategory category = null;
        if (hrmDocumentSubClass != null) {
            category = hrmCategoryDao.findBySubClassNameMnemonic(hrmDocumentSubClass.getSendingFacilityId(),hrmDocumentSubClass.getSubClass()+':'+hrmDocumentSubClass.getSubClassMnemonic());
        }
        else
        {
        	category=hrmCategoryDao.findBySubClassNameMnemonic("DEFAULT");
        }
        List<HRMDocumentComment> documentComments = hrmDocumentCommentDao.getCommentsForDocument(Integer.parseInt(hrmDocumentId));
		
		Document document = new Document();
		PdfWriter writer = PdfWriter.getInstance(document, os);
		writer.setPageEvent(this);
		document.setPageSize(PageSize.LETTER);
        document.addTitle("Title of the Document");
        document.addCreator("OSCAR");
        document.open();
        
        // Create the fonts that we are going to use
        bf = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        font = new Font(bf, 9, Font.NORMAL);
        boldFont = new Font(bf, 10, Font.BOLD);
        italicFont = new Font(bf, 9, Font.ITALIC);
        
        // 1. first table
        PdfPTable table1 = new PdfPTable(1);
       // PdfPTable borderTbl = new PdfPTable();
        table1.setWidthPercentage(100);
        
        Phrase reportPhr = new Phrase();
        reportPhr.add(new Chunk("This report was received from the Hospital"
        		+" Report Manager (HRM) at "+hrmDoc.getTimeReceived().toString()
        		+".\nOSCAR has received "+hrmDoc.getNumDuplicatesReceived()
        		+" duplicates of this report.\n\n", italicFont));
        reportPhr.add(new Chunk(new LineSeparator()));
        reportPhr.add(new Chunk("\n\n"));
        if (!report.isBinary()) {
					val rptContent = report
							.getFirstReportTextContent()
							.replaceAll("\\\\.br\\\\", "\n");
					reportPhr.add(new Chunk(rptContent, font));
				} else if (report.isBinary() && report.isHtml()){
					val rptContent = UtilMisc.decode64(report.getFirstReportTextContent());
					val strReader = new StringReader(rptContent);
					ArrayList<com.lowagie.text.Element> elementList =
							HTMLWorker.parseToList(strReader, null);
					for (var element : elementList) {
						List<Chunk> chunks = element.getChunks();
						for (var chunk : chunks) {
							chunk.setFont(font);
						}
						if (element.getClass() == Paragraph.class) {
							val paragraph = (Paragraph) element;
							if (" ".equals(paragraph.getContent())) {
								reportPhr.add(new Paragraph("\n"));
							}
						}
						reportPhr.add(element);
						reportPhr.add(new Paragraph("\n"));
					}
				}
        PdfPCell cell = new PdfPCell();
				cell.setPhrase(reportPhr);
        //cell.setBorderWidth(2f);
        cell.setPaddingLeft(5f);
        cell.setPaddingRight(5f);
        cell.setPaddingTop(10f);
        cell.setPaddingBottom(10f);
        table1.addCell(cell);
        
        List<String> imglist = new ArrayList<String>();
        if (report.isBinary()) {
        	if (report.getFileExtension() != null && report.isImage()) {
        		Image img = Image.getInstance(report.getBinaryContent());
        		cell = new PdfPCell(img);
        		cell.setPhrase(reportPhr);
                //cell.setBorderWidth(2f);
                cell.setPaddingLeft(5f);
                cell.setPaddingRight(5f);
                cell.setPaddingTop(10f);
                table1.addCell(cell);
        	}else if (report.getFileExtension() != null && ".pdf".equals(report.getFileExtension())) {
  	           File temp = File.createTempFile("HRMDownloadFile", report.getFileExtension()); 
               temp.deleteOnExit();
               
               if(report.getBinaryContent() != null) {
               FileUtils.writeByteArrayToFile(temp, report.getBinaryContent());
               PdfDecoder decode_pdf = new PdfDecoder(true);
               try {
					decode_pdf.openPdfFile(temp.getAbsolutePath());
					int start = 1, end = decode_pdf.getPageCount();
					for (int i = start; i < end+1; i++) {
						BufferedImage image = decode_pdf.getPageAsImage(i);
						String folder=System.getProperty("java.io.tmpdir");
						String imgpath = folder + "/HRMDownloadFile"+i+".png";
						imglist.add(imgpath);
						ImageIO.write(image, "png", new File(imgpath));
						Image imge = Image.getInstance(imgpath);
			    		cell = new PdfPCell(imge,true);
			            table1.addCell(cell);
					}
					decode_pdf.closePdfFile();
				} catch (Exception e) {
					throw new ExceptionConverter(e);
				}
               }
               
            }
        }
        table1.setSpacingAfter(10f);
        document.add(table1);
        
        // 2. second table ( info box )
        PdfPTable table2 = new PdfPTable(new float[] {1f, 4f});
        table2.setWidthPercentage(100);
        table2.setSpacingAfter(10f);
        PdfPCell cellRAgn = new PdfPCell();
        cellRAgn.setBorderWidthLeft(1f);
        cellRAgn.setBorderWidthTop(1f);
        cellRAgn.setBorderWidthRight(0);
        cellRAgn.setBorderWidthBottom(0);
        cellRAgn.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cellRAgn.setPhrase(new Phrase("Report Date", boldFont));
        table2.addCell(cellRAgn);
        cellRAgn.setBorderWidthTop(0);
        
        PdfPCell cellLAgn = new PdfPCell();
        cellLAgn.setBorderWidthBottom(0);
        cellLAgn.setBorderWidthLeft(0);
        cellLAgn.setBorderWidthRight(1f);
        cellLAgn.setBorderWidthTop(1f);
        cellLAgn.setHorizontalAlignment(Element.ALIGN_LEFT);
        String reportDate = "";
        if (report.getFirstReportEventTime() != null) {
        	reportDate = report.getFirstReportEventTime().getTime().toString();
        } else if (report.getFirstAccompanyingSubClassDateTime() != null) {
        	reportDate = report.getFirstAccompanyingSubClassDateTime();
        }
        cellLAgn.setPhrase(new Phrase(reportDate, font));
        table2.addCell(cellLAgn);
        cellLAgn.setBorderWidthTop(0);
        
        cellRAgn.setPhrase(new Phrase("Demographic Info", boldFont));
        table2.addCell(cellRAgn);
        
        String demotmp = report.getLegalName() + "\n"
        		+ report.getAddressLine1() + "\n"
        		+ (report.getAddressLine2() != null? report.getAddressLine2() : "") + "\n"
        		+ report.getAddressCity();
        cellLAgn.setPhrase(new Phrase(demotmp, font));
        table2.addCell(cellLAgn);
        
        cellRAgn.setPhrase(new Phrase("Linked with Demographic", boldFont));
        table2.addCell(cellRAgn);
        
        if (demographicLink != null) {
        	DemographicNameAgeString demoNameAge = DemographicNameAgeString.getInstance();       
            String nameage = demoNameAge.getNameAgeString(loggedInInfo, Integer.valueOf(demographicLink.getDemographicNo()));
            cellLAgn.setPhrase(new Phrase(nameage, font));
        } else {
        	cellLAgn.setPhrase(new Phrase("Not currently linked", font));
        }
        table2.addCell(cellLAgn);
        cellRAgn.setPhrase(new Phrase("Assigned Providers", boldFont));
        table2.addCell(cellRAgn);
        
        Phrase providers = new Phrase();
        if (providerLinkList != null || providerLinkList.size() >= 1) {
			for (HRMDocumentToProvider p : providerLinkList) { 
				if (p.getSignedOff() == 1 && p.getProviderNo() != null && p.getProviderNo().length()>0) {
					demotmp = providerDao.getProviderName(p.getProviderNo());
					providers.add(new Chunk(demotmp + " ", font));
					if (p.getSignedOff() !=null && p.getSignedOff()  == 1) {
						providers.add(new Chunk("(Signed-Off)\n"));
					}
				}
			}
        } else {
        	providers.add(new Chunk("No providers currently assigned\n", italicFont));
        }
        if (hrmDoc.getUnmatchedProviders() != null && hrmDoc.getUnmatchedProviders().trim().length() >= 1) {
			String[] unmatchedProviders = hrmDoc.getUnmatchedProviders().substring(1).split("\\|");
			for (String unmatchedProvider : unmatchedProviders) {
				providers.add(new Chunk(unmatchedProvider+"\n", italicFont));
			}
        }
        cellLAgn.setPhrase(providers);
        table2.addCell(cellLAgn);
        cell = new PdfPCell();
        cell.setColspan(2);
        cell.setBorderWidthBottom(0);
        cell.setBorderWidthTop(0);
        cell.setBorderWidthLeft(1f);
        cell.setBorderWidthRight(1f);
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setPhrase(new Phrase(new Chunk(new LineSeparator())));
        table2.addCell(cell);
        cellRAgn.setPhrase(new Phrase("Report Class", boldFont));
        table2.addCell(cellRAgn);
        cellLAgn.setPhrase(new Phrase(report.getFirstReportClass(), font));
        table2.addCell(cellLAgn);
        if (report.getFirstReportClass().equalsIgnoreCase("Diagnostic Imaging Report") 
        		|| report.getFirstReportClass().equalsIgnoreCase("Cardio Respiratory Report")) { 
        	cellRAgn.setPhrase(new Phrase("Accompanying Subclass", boldFont));
            table2.addCell(cellRAgn);
            List<List<Object>> subClassListFromReport = report.getAccompanyingSubclassList();
			Phrase subclsPhr = new Phrase();
			if (subClassListFromReport.size() > 0) {
				subclsPhr.add(new Chunk("From the Report\n", italicFont));
				for (List<Object> subClass : subClassListFromReport) {
					String scls = "(" + subClass.get(1) + ") " + subClass.get(2);
					subclsPhr.add(new Chunk(scls+"\n", font));
				}
				subclsPhr.add(new Chunk("\n"));
				if (subClassList != null && subClassList.size() > 0) { 
					subclsPhr.add(new Chunk("Description\n", italicFont));
					for (HRMDocumentSubClass subClass : subClassList) {
						String scls = "(" + subClass.getSubClassMnemonic() + ") " + subClass.getSubClassDescription();
						subclsPhr.add(new Chunk(scls+"\n", font));
					}
				}
			}
			cellLAgn.setPhrase(subclsPhr);
			table2.addCell(cellLAgn);
        } else {
        	cellRAgn.setPhrase(new Phrase("Subclass", boldFont));
        	table2.addCell(cellRAgn);
        	String[] subClassFromReport = report.getFirstReportSubClass().split("\\^");
        	if (subClassFromReport.length == 2) {
        		cellLAgn.setPhrase(new Phrase(new Chunk(subClassFromReport[1], font)));
        	} else {
        		cellLAgn.setPhrase(new Phrase(""));
        	}
        	table2.addCell(cellLAgn);
        	if (report.getFirstReportClass().equalsIgnoreCase("Medical Records Report")) {
				if (subClassList != null && subClassList.size() > 0) {
					cellRAgn.setPhrase(new Phrase("Description", boldFont));
					table2.addCell(cellRAgn);
					Phrase desc = new Phrase();
					for (HRMDocumentSubClass subClass : subClassList) {
						desc.add(new Chunk(subClass.getSubClassDescription()+"\n", font));
					}
					cellLAgn.setPhrase(desc);
					table2.addCell(cellLAgn);
				}
        	}
        }
        cellRAgn.setBorderWidthBottom(1f);
        cellRAgn.setPhrase(new Phrase("Categorization", boldFont));
        table2.addCell(cellRAgn);
        cellLAgn.setBorderWidthBottom(1f);
		if (category != null) {
			cellLAgn.setPhrase(new Phrase(StringEscapeUtils.escapeHtml(category.getCategoryName()), font));
		} else {
			cellLAgn.setPhrase(new Phrase(""));
		}
        table2.addCell(cellLAgn);
        document.add(table2);
        
        // 3. report comment
        PdfPTable table3 = new PdfPTable(1);
        table3.setWidthPercentage(100);
        table3.setSpacingAfter(10f);
        
        if (documentComments != null) {
        	cell = new PdfPCell();
        	cell.setBorderWidthTop(1f);
        	cell.setBorderWidthLeft(1f);
        	cell.setBorderWidthRight(1f);
        	String temp = "Displaying " + documentComments.size() 
        	+ " comment" + ((documentComments.size() != 1) ? "s" : "");
        	cell.setPhrase(new Phrase(new Chunk(temp+"\n", font)));
        	table3.addCell(cell);
        	for (int i=0; i<documentComments.size(); i++) {
        		if (i != 0) {
        			cell.setBorderWidthTop(0);
        		}
        		if (i == (documentComments.size() - 1)) {
        			cell.setBorderWidthBottom(1f);
        		}
        		HRMDocumentComment comment = documentComments.get(i);
        		Phrase cmt = new Phrase();
        		cmt.add(new Chunk(providerDao.getProviderName(comment.getProviderNo())+" on "
        				+comment.getCommentTime().toString()+" wrote...\n", boldFont));
        		cmt.add(new Chunk(comment.getComment()+"\n", font));
        		cell.setPhrase(cmt);
        		table3.addCell(cell);
        	}
        }
        document.add(table3);
       
        // 4. summary of report
        PdfPTable table4 = new PdfPTable(new float[]{1f, 4f});
        table4.setWidthPercentage(100);
        table4.setSpacingAfter(10f);
        cellRAgn.setBorderWidthRight(0f);
        cellRAgn.setBorderWidthBottom(0);
        cellLAgn.setBorderWidthBottom(0);
        cellLAgn.setBorderWidthLeft(0);
        cellRAgn.setBorderWidthTop(1f);
        
        cellRAgn.setPhrase(new Phrase("Message Unique ID", boldFont));
        table4.addCell(cellRAgn);
        cellRAgn.setBorderWidthTop(0);
        
        cellLAgn.setBorderWidthTop(1f);
        cellLAgn.setPhrase(new Phrase(report.getMessageUniqueId(), font));
        table4.addCell(cellLAgn);
        cellLAgn.setBorderWidthTop(0);
        
        cellRAgn.setPhrase(new Phrase("Sending Facility ID", boldFont));
        table4.addCell(cellRAgn);
        cellLAgn.setPhrase(new Phrase(report.getSendingFacilityId(), font));
        table4.addCell(cellLAgn);
        
        cellRAgn.setPhrase(new Phrase("Sending Facility Report No.", boldFont));
        table4.addCell(cellRAgn);
        cellLAgn.setPhrase(new Phrase(report.getSendingFacilityReportNo(), font));
        table4.addCell(cellLAgn);
        
        cellRAgn.setPhrase(new Phrase("Date and Time of Report", boldFont));
        table4.addCell(cellRAgn);
        cellLAgn.setPhrase(new Phrase(HRMReportParser.getAppropriateDateFromReport(report).toString(), font));
        table4.addCell(cellLAgn);
        
        cellRAgn.setBorderWidthBottom(1f);
        cellRAgn.setPhrase(new Phrase("Result Status", boldFont));
        table4.addCell(cellRAgn);
        cellLAgn.setBorderWidthBottom(1f);
        cellLAgn.setPhrase(new Phrase((report.getResultStatus() != null && report.getResultStatus().equalsIgnoreCase("C")) 
        		? "Cancelled" : "Signed by the responsible author and Released by health records", font));
        table4.addCell(cellLAgn);
        document.add(table4);
        
        document.close();
        os.flush();
        
        for (String imgepath : imglist) {
			if (imgepath.length()>0) {
				File file = new File(imgepath);
				if (file.exists()) {
					file.delete();
				}
			}
		}
	}
	
	public void onEndPage(PdfWriter writer, Document document){
        try {

            PdfContentByte cb = writer.getDirectContent();

            //add footer for every page
            cb.beginText();
            cb.setFontAndSize(bf, 9);
            cb.showTextAligned(PdfContentByte.ALIGN_LEFT, "Provider Confidentiality Statement", 37, 13, 0);
            cb.showTextAligned(PdfContentByte.ALIGN_LEFT, confidentialityStatement, 37, 7, 0);
            cb.endText();

        // throw any exceptions
        } catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
}
