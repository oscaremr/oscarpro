/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

/*
 * ProviderLabRouting.java
 *
 * Created on July 17, 2007, 9:43 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package oscar.oscarLab.ca.all.upload;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.oscarehr.common.dao.Hl7TextInfoDao;
import org.oscarehr.common.dao.Hl7TextMessageDao;
import org.oscarehr.common.dao.PatientLabRoutingDao;
import org.oscarehr.common.dao.ProviderLabRoutingDao;
import org.oscarehr.common.model.Hl7TextInfo;
import org.oscarehr.common.model.Hl7TextMessage;
import org.oscarehr.common.model.IncomingLabRules;
import org.oscarehr.common.model.PatientLabRouting;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.ProviderLabRoutingModel;
import org.oscarehr.common.model.ResourceTypeEnum;
import org.oscarehr.integration.yourcare.DataSyncService;
import org.oscarehr.util.SpringUtils;

import oscar.OscarProperties;
import oscar.oscarLab.ForwardingRules;
import oscar.util.ConversionUtils;

/**
 *
 * @author wrighd
 */
public class ProviderLabRouting {

	Logger logger = Logger.getLogger(ProviderLabRouting.class);
	private ProviderLabRoutingDao providerLabRoutingDao = SpringUtils.getBean(ProviderLabRoutingDao.class);
  private PatientLabRoutingDao patientLabRoutingDao = SpringUtils.getBean(PatientLabRoutingDao.class);
	private Hl7TextMessageDao hl7TextMessageDao = (Hl7TextMessageDao) SpringUtils.getBean("hl7TextMessageDao");
  private Hl7TextInfoDao hl7TextInfoDao = (Hl7TextInfoDao) SpringUtils.getBean("hl7TextInfoDao");
  private DataSyncService dataSyncService = (DataSyncService) SpringUtils.getBean("dataSyncService");
	
	public ProviderLabRouting() {
	}

	public void route(String labId, String provider_no, Connection conn, String labType) throws SQLException {
		 route(Integer.parseInt(labId), provider_no, conn, labType);
	}

	public void route(int labId, String provider_no, String labType) {
		routeMagic(labId, provider_no, labType);
	}

	/**
	 * @deprecated Use {@link #routeMagic(int, String, String)} instead
	 */
	@Deprecated
	@SuppressWarnings("unused")
	public void route(int labId, String provider_no, Connection conn, String labType) throws SQLException {
		// hey, Eclipse now shows no errors for this method!
		if (false) {
			throw new SQLException("" + conn);
		}

		routeMagic(labId, provider_no, labType);
	}

	public void routeMagic(int labId, String provider_no, String labType) {
		routeMagic(labId, provider_no, labType, null);
	}
	public void routeMagic(int labId, String provider_no, String labType, String routeWithStatus) {
		ForwardingRules fr = new ForwardingRules();
		OscarProperties props = OscarProperties.getInstance();
		String autoFileLabs = props.getProperty("AUTO_FILE_LABS");

		ProviderLabRoutingDao dao = SpringUtils.getBean(ProviderLabRoutingDao.class);
		List<ProviderLabRoutingModel> routings = dao.findByLabNoAndLabTypeAndProviderNo(labId, labType, provider_no);

		if (routings.isEmpty()) {
			String status;
			if (routeWithStatus != null) {
				status = routeWithStatus;
			} else {
				status = fr.getStatus(provider_no);
			}
			List<Object[]> forwardProviders = fr.getForwardRulesAndProviders(provider_no);

			ProviderLabRoutingModel p = new ProviderLabRoutingModel();
			p.setProviderNo(provider_no);
			p.setLabNo(labId);
			p.setStatus(status);
			p.setLabType(labType);
			providerLabRoutingDao.persist(p);
      setHL7TextInfoAcknowlededDate(p);
			String providers_want_chartech_lab_inbox = props.getProperty("providers_want_chartech_lab_inbox", "");
			String[] providersWantLabs = providers_want_chartech_lab_inbox.split(",");
			Hl7TextMessage lab = hl7TextMessageDao.find(labId);
			
			//forward lab to specified providers
			for (int j = 0; j < forwardProviders.size(); j++) {
                IncomingLabRules forwardRules = (IncomingLabRules) forwardProviders.get(j)[0];
                Provider provider = (Provider) forwardProviders.get(j)[1];
                if (forwardRules.getForwardTypeStrings().contains(labType)) {
                	if(!"HL7".equalsIgnoreCase(labType) || !lab.getType().equalsIgnoreCase("CHARTECH") || (lab.getType().equalsIgnoreCase("CHARTECH") && !StringUtils.isBlank(providers_want_chartech_lab_inbox) && Arrays.asList(providersWantLabs).contains(forwardProviders.get(j)[0])) ) {
                		logger.info("FORWARDING PROVIDER: " + (provider.getProviderNo()));
                    	routeMagic(labId, provider.getProviderNo(), labType, forwardRules.getForwardWithStatus());
                	}else{
                		//CHARTECH labs do not go to some doctors' inbox.
    					continue;
                	}
                }
			}

			// If the lab has already been sent to this provider check to make sure that
			// it is set as a new lab for at least one provider if AUTO_FILE_LABS=yes is not
			// set in the oscar.properties file
		} else if (autoFileLabs == null || !autoFileLabs.equalsIgnoreCase("yes")) {
			List<ProviderLabRoutingModel> moreRoutings = dao.findByLabNoTypeAndStatus(labId, labType, "N");
			if (!moreRoutings.isEmpty()) {
				ProviderLabRoutingModel plr = providerLabRoutingDao.findByLabNoAndLabType(labId, labType);
				if (plr != null) {
					plr.setStatus("N");
					providerLabRoutingDao.merge(plr);
				}
			}
		}

	}

	public static Hashtable<String, Object> getInfo(String lab_no) {
		Hashtable<String, Object> info = new Hashtable<String, Object>();
		ProviderLabRoutingDao dao = SpringUtils.getBean(ProviderLabRoutingDao.class);
		ProviderLabRoutingModel r = dao.findByLabNo(ConversionUtils.fromIntString(lab_no));
		if (r != null) {
			info.put("lab_no", lab_no);
			info.put("provider_no", r.getProviderNo());
			info.put("status", r.getStatus());
			info.put("comment", StringUtils.trimToEmpty(r.getComment()));
			info.put("timestamp", r.getTimestamp());
			info.put("lab_type", r.getLabType());
			info.put("id", r.getId());
		}
		return info;
	}

	public void route(String labId, String provider_no, String labType) throws SQLException {
		ForwardingRules fr = new ForwardingRules();
		OscarProperties props = OscarProperties.getInstance();
		String autoFileLabs = props.getProperty("AUTO_FILE_LABS");

		ProviderLabRoutingDao providerLabRoutingDao = SpringUtils.getBean(ProviderLabRoutingDao.class);
		List<ProviderLabRoutingModel> rs = providerLabRoutingDao.getProviderLabRoutingForLabProviderType(Integer.parseInt(labId), provider_no, labType);

		if (!rs.isEmpty()) {
			String status = fr.getStatus(provider_no);
			ArrayList<ArrayList<String>> forwardProviders = fr.getProviders(provider_no);

			ProviderLabRoutingModel newRouted = new ProviderLabRoutingModel();
			newRouted.setProviderNo(provider_no);
			newRouted.setLabNo(Integer.parseInt(labId));
			newRouted.setLabType(labType);
			newRouted.setStatus(status);

			providerLabRoutingDao.persist(newRouted);
      setHL7TextInfoAcknowlededDate(newRouted);
			//forward lab to specified providers
			for (int j = 0; j < forwardProviders.size(); j++) {
				logger.info("FORWARDING PROVIDER: " + ((forwardProviders.get(j)).get(0)));
				route(labId, ((forwardProviders.get(j)).get(0)), labType);
			}

			// If the lab has already been sent to this provider check to make sure that
			// it is set as a new lab for at least one provider if AUTO_FILE_LABS=yes is not
			// set in the oscar.properties file
		} else if (autoFileLabs == null || !autoFileLabs.equalsIgnoreCase("yes")) {
			rs = providerLabRoutingDao.getProviderLabRoutingForLabAndType(Integer.parseInt(labId), labType);
			if (rs.isEmpty()) {
				providerLabRoutingDao.updateStatus(Integer.parseInt(labId), labType);
			}
		}

	}

	public static HashMap<String, Object> getInfo(String lab_no, String lab_type) {
		HashMap<String, Object> info = new HashMap<String, Object>();
		ProviderLabRoutingDao dao = SpringUtils.getBean(ProviderLabRoutingDao.class);
		ProviderLabRoutingModel r = dao.findByLabNoAndLabType(ConversionUtils.fromIntString(lab_no), lab_type, true);

		if (r != null) {
			info.put("lab_no", lab_no);
			info.put("provider_no", r.getProviderNo());
			info.put("status", r.getStatus());
			info.put("comment", r.getComment());
			info.put("timestamp", r.getTimestamp());
			info.put("lab_type", r.getLabType());
			info.put("id", r.getId());
		}
		return info;
	}

  public void setHL7TextInfoAcknowlededDate(ProviderLabRoutingModel routing) {
    if (routing.getLabType().equals("HL7") && routing.getStatus().equals("A")) {
      Hl7TextInfo lab = hl7TextInfoDao.findLabId(routing.getLabNo());
      if (lab != null && lab.getAcknowledgedDate() == null) {
        lab.setAcknowledgedDate(new Date());
        PatientLabRouting patientLabRouting = patientLabRoutingDao.findDemographicByLabId(routing.getLabNo());
        if (patientLabRouting != null && patientLabRouting.getDemographicNo() != null) {
          Date autoSyncDate = dataSyncService.getAutoSyncDate(lab.getAcknowledgedDate(), ResourceTypeEnum.LAB_RESULTS,
                  patientLabRouting.getDemographicNo());
          patientLabRouting.setAutoSyncDate(autoSyncDate);
          patientLabRoutingDao.saveEntity(patientLabRouting);
          hl7TextInfoDao.saveEntity(lab);
          dataSyncService.logUpdateAutoSyncDate(null, ResourceTypeEnum.LAB_RESULTS, lab.getLabNumber(),
                  patientLabRouting.getDemographicNo(), autoSyncDate);
        }
      }
    }
  }

}
