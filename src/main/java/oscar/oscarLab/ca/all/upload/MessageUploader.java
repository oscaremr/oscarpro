/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


/*
 * MessageUploader.java
 *
 * Created on June 18, 2007, 1:53 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package oscar.oscarLab.ca.all.upload;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import lombok.val;
import lombok.var;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.oscarehr.PMmodule.dao.ProgramDao;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.PMmodule.exception.AdmissionException;
import org.oscarehr.PMmodule.exception.ProgramFullException;
import org.oscarehr.PMmodule.exception.ServiceRestrictionException;
import org.oscarehr.PMmodule.service.AdmissionManager;
import org.oscarehr.PMmodule.service.ProgramManager;
import org.oscarehr.PMmodule.web.GenericIntakeEditAction;
import org.oscarehr.common.OtherIdManager;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.dao.FileUploadCheckDao;
import org.oscarehr.common.dao.Hl7TextInfoDao;
import org.oscarehr.common.dao.Hl7TextMessageDao;
import org.oscarehr.common.dao.MeasurementDao;
import org.oscarehr.common.dao.MeasurementsExtDao;
import org.oscarehr.common.dao.PatientLabRoutingDao;
import org.oscarehr.common.dao.ProviderLabRoutingDao;
import org.oscarehr.common.dao.RecycleBinDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.TriggerDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.DemographicExtKey;
import org.oscarehr.common.model.FileUploadCheck;
import org.oscarehr.common.model.Hl7TextInfo;
import org.oscarehr.common.model.Hl7TextMessage;
import org.oscarehr.common.model.Measurement;
import org.oscarehr.common.model.MeasurementsExt;
import org.oscarehr.common.model.OtherId;
import org.oscarehr.common.model.PatientLabRouting;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.ProviderLabRoutingModel;
import org.oscarehr.common.model.RecycleBin;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.common.model.Trigger;
import org.oscarehr.common.model.TriggerType;
import org.oscarehr.managers.DemographicManager;
import org.oscarehr.olis.dao.OLISSystemPreferencesDao;
import org.oscarehr.olis.model.OLISSystemPreferences;
import org.oscarehr.util.DbConnectionFilter;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import oscar.log.LogAction;
import oscar.oscarDemographic.data.DemographicMerged;
import oscar.oscarDemographic.data.ProvinceNames;
import oscar.oscarLab.ForwardingRules;
import oscar.oscarLab.ca.all.Hl7textResultsData;
import oscar.oscarLab.ca.all.parsers.Factory;
import oscar.oscarLab.ca.all.parsers.HHSEmrDownloadHandler;
import oscar.oscarLab.ca.all.parsers.HinVersionMessageHandler;
import oscar.oscarLab.ca.all.parsers.MEDITECHHandler;
import oscar.oscarLab.ca.all.parsers.MessageHandler;
import oscar.oscarLab.ca.all.parsers.OLISHL7Handler;
import oscar.oscarLab.ca.all.parsers.SpireHandler;
import oscar.util.UtilDateUtilities;

public final class MessageUploader {

	private static final Logger logger = MiscUtils.getLogger();
	private static PatientLabRoutingDao patientLabRoutingDao = SpringUtils.getBean(PatientLabRoutingDao.class);
	private static ProviderLabRoutingDao providerLabRoutingDao = SpringUtils.getBean(ProviderLabRoutingDao.class);
	private static RecycleBinDao recycleBinDao = SpringUtils.getBean(RecycleBinDao.class);
	private static Hl7TextInfoDao hl7TextInfoDao = (Hl7TextInfoDao) SpringUtils.getBean("hl7TextInfoDao");
	private static Hl7TextMessageDao hl7TextMessageDao = (Hl7TextMessageDao) SpringUtils.getBean("hl7TextMessageDao");
	private static MeasurementsExtDao measurementsExtDao = SpringUtils.getBean(MeasurementsExtDao.class);
	private static MeasurementDao measurementDao = SpringUtils.getBean(MeasurementDao.class);
	private static FileUploadCheckDao fileUploadCheckDao = SpringUtils.getBean(FileUploadCheckDao.class);
	private static DemographicManager demographicManager = SpringUtils.getBean(DemographicManager.class);
  private static DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
	private static DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);
	private static ProgramDao programDao = SpringUtils.getBean(ProgramDao.class);
	private static ProgramManager programManager = SpringUtils.getBean(ProgramManager.class);
	private static AdmissionManager admissionManager = SpringUtils.getBean(AdmissionManager.class);
	private static SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
	private static final String CHARTECH = "CHARTECH";
	private static final String HL7 = "HL7";
	private static final String PROVIDER = "provider";
	private static final String OHIP_NO = "ohip_no";
	private static final ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
	private static final String providersWantChartechLabInbox = OscarProperties.getInstance().getProperty("providers_want_chartech_lab_inbox", "");

	private MessageUploader() {
		// there's no reason to instantiate a class with no fields.
	}

	public static String routeReport(LoggedInInfo loggedInInfo, String serviceName, String type, String hl7Body, int fileId) throws Exception {
		return routeReport(loggedInInfo, serviceName, type, hl7Body, fileId, null);
	}

	public static String routeReport(LoggedInInfo loggedInInfo, String serviceName, String type, String hl7Body, int fileId, RouteReportResults results) throws Exception {
		return routeReport(loggedInInfo, serviceName, type, hl7Body, fileId, results, true, null);
	}
	public static String routeReport(LoggedInInfo loggedInInfo, String serviceName, String type, String hl7Body, int fileId, RouteReportResults results, Integer alreadyMatchedDemoNo) throws Exception {
		return routeReport(loggedInInfo, serviceName, type, hl7Body, fileId, results, true, alreadyMatchedDemoNo);
	}
	public static String routeReport(LoggedInInfo loggedInInfo, String serviceName, String type, String hl7Body, int fileId, RouteReportResults results, Boolean autoRoute) throws Exception {
		return routeReport(loggedInInfo, serviceName, type, hl7Body, fileId, results, autoRoute, null);
	}

	/**
	 * Insert the lab into the proper tables of the database
	 * alreadyMatchedDemoNo - field used exclusively for OLIS lab saving where cases where lab 
	 * HIN is blank but because OLIS results can return result only for a demographic 
	 */
	public static String routeReport(LoggedInInfo loggedInInfo, String serviceName, String type, String hl7Body, int fileId, RouteReportResults results, Boolean autoRoute, Integer alreadyMatchedDemoNo) throws Exception {

		
		String retVal = "";
		// Get the system preference to determine if a label should be applied to a version of a lab even if the labs inside the version change, defaulting to true to maintain current expected behavior 
		Boolean applyLabelDifferentLabs = true;
		SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
		SystemPreferences applyLabelDifferentLabsPreference = systemPreferencesDao.findPreferenceByName("sticky_label_different_labs");
		if (applyLabelDifferentLabsPreference != null) {
			applyLabelDifferentLabs = Boolean.parseBoolean(applyLabelDifferentLabsPreference.getValue());
		}
		try {
            MessageHandler h = Factory.getHandler(type, hl7Body);

            String firstName = h.getFirstName();
            String lastName = h.getLastName();
            String dob = h.getDOB();
            String sex = h.getSex();
            String hin = h.getHealthNum();
            String hinVersion = "";
            if (h instanceof HinVersionMessageHandler) {
              hinVersion = ((HinVersionMessageHandler) h).getHealthNumVersion();
            }
            String homePhone = h.getHomePhone();
            String workPhone = "N/A".equals(h.getWorkPhone()) ? "" : h.getWorkPhone();
            String resultStatus = "";
            String priority = h.getMsgPriority();
            String requestingClient = h.getDocName();
            if (h instanceof OLISHL7Handler) {
                requestingClient = ((OLISHL7Handler) h).getShortDocName();
            }
            String reportStatus = h.getOrderStatus();
			String accessionNumber = h.getAccessionNum();
            String fillerOrderNum = h.getFillerOrderNumber();
            String sendingFacility = h.getPatientLocation();
			ArrayList<String> docNums = h.getDocNums();
            int finalResultCount = h.getOBXFinalResultCount();
            String obrDate = h.getMsgDate();
            String collectionDate = "";
            String lastUpdateInOLIS = "";
            if (h instanceof OLISHL7Handler) {
                collectionDate = ((OLISHL7Handler) h).getCollectionDateTime(0);
                lastUpdateInOLIS = ((OLISHL7Handler) h).getLastUpdateInOLIS();
			}

            if (h instanceof HHSEmrDownloadHandler) {
                try {
                    String chartNo = ((HHSEmrDownloadHandler) h).getPatientIdByType("MR");
                    if (chartNo != null) {
                        //let's get the hin
                        List<Demographic> clients = demographicManager.getDemosByChartNo(loggedInInfo, chartNo);
                        if (clients != null && clients.size() > 0) {
                            hin = clients.get(0).getHin();
                        }
                    }
                } catch (Exception e) {
                    logger.error("HHS ERROR", e);
                }
            }

            // get actual ohip numbers based on doctor first and last name for spire lab
            if (h instanceof SpireHandler) {
                List<String> docNames = ((SpireHandler) h).getDocNames();
                //logger.debug("docNames:");
                for (int i = 0; i < docNames.size(); i++) {
                    logger.info(i + " " + docNames.get(i));
                }
                if (docNames != null) {
                    docNums = findProvidersForSpireLab(docNames);
                }
            }
            logger.info("docNums:");
            for (int i=0; i < docNums.size(); i++) {
				logger.info(i + " " + docNums.get(i));
			}

            try {
                // reformat date
                // use OBR date, if blank use specimen received date
                String date = obrDate.trim() != "" ? obrDate.substring(0, obrDate.length() ) : collectionDate.substring(0, collectionDate.trim().length());
                String format = "yyyy-MM-dd HH:mm:ss".substring(0, date.length() );
                obrDate = UtilDateUtilities.DateToString(UtilDateUtilities.StringToDate(date, format), "yyyy-MM-dd HH:mm:ss");
            } catch (Exception e) {
                logger.error("Error parsing obr date : ", e);
                throw e;
            }

            int i = 0;
            int j = 0;
            while (resultStatus.equals("") && i < h.getOBRCount()) {
                j = 0;
                while (resultStatus.equals("") && j < h.getOBXCount(i)) {
                    if (h.isOBXAbnormal(i, j)) resultStatus = "A";
                    j++;
                }
                i++;
            }

            ArrayList<String> disciplineArray = h.getHeaders();
            String next = "";

            if (disciplineArray != null && disciplineArray.size() > 0) {
                next = disciplineArray.get(0);
            }

            int sepMark;
            if ((sepMark = next.indexOf("<br />")) < 0) {
                if ((sepMark = next.indexOf(" ")) < 0) sepMark = next.length();
            }

            String discipline = next.substring(0, sepMark).trim();

            for (i = 1; i < disciplineArray.size(); i++) {

                next = disciplineArray.get(i);
                if ((sepMark = next.indexOf("<br />")) < 0) {
                    if ((sepMark = next.indexOf(" ")) < 0) sepMark = next.length();
                }

                if (!next.trim().equals("")) {
                    discipline = discipline + "/" + next.substring(0, sepMark);
                }
            }

            boolean isTDIS = type.equals("TDIS");
            boolean hasBeenUpdated = false;
            Hl7TextMessage hl7TextMessage = new Hl7TextMessage();
            Hl7TextInfo hl7TextInfo = new Hl7TextInfo();


            if (h instanceof MEDITECHHandler) {
                discipline = ((MEDITECHHandler) h).getDiscipline();
            }

            if (isTDIS) {
                List<Hl7TextInfo> matchingTdisLab = hl7TextInfoDao.searchByFillerOrderNumber(fillerOrderNum, sendingFacility);
                if (matchingTdisLab.size() > 0) {
                    hl7TextMessageDao.updateIfFillerOrderNumberMatches(new String(Base64.encodeBase64(hl7Body.getBytes(MiscUtils.DEFAULT_UTF8_ENCODING)), MiscUtils.DEFAULT_UTF8_ENCODING), fileId, matchingTdisLab.get(0).getLabNumber());
                    hl7TextInfoDao.updateReportStatusByLabId(reportStatus, matchingTdisLab.get(0).getLabNumber());
                    hasBeenUpdated = true;
                }
            }
            int insertID = 0;
            if (!isTDIS || !hasBeenUpdated) {
                hl7TextMessage.setFileUploadCheckId(fileId);
                hl7TextMessage.setType(type);
                hl7TextMessage.setBase64EncodedeMessage(new String(Base64.encodeBase64(hl7Body.getBytes(MiscUtils.DEFAULT_UTF8_ENCODING)), MiscUtils.DEFAULT_UTF8_ENCODING));
                hl7TextMessage.setServiceName(serviceName);
                hl7TextMessageDao.persist(hl7TextMessage);

                insertID = hl7TextMessage.getId();
                hl7TextInfo.setLabNumber(insertID);
                hl7TextInfo.setLastName(lastName);
                hl7TextInfo.setFirstName(firstName);
                hl7TextInfo.setSex(sex);
                hl7TextInfo.setHealthNumber(hin);
                hl7TextInfo.setResultStatus(resultStatus);
                hl7TextInfo.setFinalResultCount(finalResultCount);
                hl7TextInfo.setObrDate(obrDate);
                hl7TextInfo.setPriority(priority);
                hl7TextInfo.setRequestingProvider(requestingClient);
                hl7TextInfo.setDiscipline(discipline);
                hl7TextInfo.setReportStatus(reportStatus);
				hl7TextInfo.setAccessionNumber(accessionNumber);
				hl7TextInfo.setSendingFacility(sendingFacility);
                hl7TextInfo.setFillerOrderNum(fillerOrderNum);
				hl7TextInfo.setLastUpdatedInOLIS(lastUpdateInOLIS);
				
				// If a past lab with the same AccessionNumber exist carry over the label
				List<Hl7TextInfo> matchingLabs = hl7TextInfoDao.searchByAccessionNumberOrderByObrDate(accessionNumber);
				for (Hl7TextInfo matchingLab : matchingLabs) {
					// if the lab has an entered label to carry over and the disciplines match (same lab types... for example CHEM1 & CHEM1 and not CHEM1/CHEM4 & CHEM1)
					if (!StringUtils.isBlank(matchingLab.getLabel()) && (applyLabelDifferentLabs || discipline.equals(matchingLab.getDiscipline()))) {
						// if the matched lab exist and a demographic is matched to it, make sure the HIN is the same 
						// before carrying over the label. If there is no demographic linked, just carry over the label
						PatientLabRouting matchedPatientLabRouting = patientLabRoutingDao.findDemographicByLabId(matchingLab.getLabNumber());
						if (matchedPatientLabRouting != null) {
							Demographic matchedDemographic = demographicDao.getDemographic(matchedPatientLabRouting.getDemographicNo());
							if (matchedDemographic != null && !StringUtils.isBlank(hin) && hin.equals(matchedDemographic.getHin())) {
								hl7TextInfo.setLabel(matchingLab.getLabel());
								break;
							}
						} else {
							hl7TextInfo.setLabel(matchingLab.getLabel());
							break;
						}

                    }
                }
                hl7TextInfoDao.persist(hl7TextInfo);
            }

			if("true".equals(OscarProperties.getInstance().getProperty("inbox.labels.sticky","false"))) {
				String latestLabel = "";
				String multiID = Hl7textResultsData.getMatchingLabs(String.valueOf(hl7TextMessage.getId()));
				for(String id: multiID.split(",")) {
					if(!id.equals(String.valueOf(hl7TextMessage.getId()))) {
						List<Hl7TextInfo> infos = hl7TextInfoDao.findByLabId(Integer.parseInt(id));
						for(Hl7TextInfo info:infos) {
							if(!StringUtils.isEmpty(info.getLabel()) && (applyLabelDifferentLabs || discipline.equals(info.getDiscipline()))) {
								latestLabel = info.getLabel();
							}
						}
					}
				}
				if(!StringUtils.isEmpty(latestLabel)) {
					hl7TextInfo.setLabel(latestLabel);
					hl7TextInfoDao.merge(hl7TextInfo);
				}
			}
			
			if (autoRoute) {
                String demProviderNo = null;
                Connection c = null;
                try {
                    c = DbConnectionFilter.getThreadLocalDbConnection();
                    demProviderNo = patientRouteReport(loggedInInfo, hl7TextInfo, lastName, firstName, sex, dob, hin, hinVersion, homePhone, workPhone, c, alreadyMatchedDemoNo);
                } finally {
                    try {
                        c.close();
                    }catch(SQLException e) {

                    }
                }
                if (type.equals("OLIS_HL7") && demProviderNo.equals("0")) {
                    OLISSystemPreferencesDao olisPrefDao = (OLISSystemPreferencesDao) SpringUtils.getBean("OLISSystemPreferencesDao");
                    OLISSystemPreferences olisPreferences = olisPrefDao.getPreferences();
                    c = DbConnectionFilter.getThreadLocalDbConnection();
                    try {
                        if(olisPreferences.isFilterPatients()) {
                            //set as unclaimed
                            providerRouteReport(String.valueOf(insertID), null, c, String.valueOf(0), type);
                        } else {
                            providerRouteReport(String.valueOf(insertID), docNums, DbConnectionFilter.getThreadLocalDbConnection(), demProviderNo, type);
                        }
                    } finally {
                        try {
                            c.close();
                        }catch(SQLException e) {

                        }
                    }
                } else {
                    Integer limit = null;
                    boolean orderByLength = false;
                    String search = null;
                    if (type.equals("Spire")) {
                        limit = new Integer(1);
                        orderByLength = true;
                        search = "provider_no";
                    }

                    if ("MEDITECH".equals(type) || "ExcellerisON".equals(type)) {
                        search = "practitionerNo";
                    }

                    if( "IHAPOI".equals(type) ) {
                        search = "hso_no";
                    }

                    c = DbConnectionFilter.getThreadLocalDbConnection();
                    try {
                        providerRouteReport(String.valueOf(insertID), docNums, c, demProviderNo, type, search);
                    } finally {
                        try {
                            c.close();
                        }catch(SQLException e) {

                        }
                    }
                }
            }
			
			retVal = h.audit();
			if(results != null) {
				results.segmentId = insertID;
			}

            TriggerDao triggerDao = SpringUtils.getBean(TriggerDao.class);
            List<Trigger> triggerList = triggerDao.findAllActiveByType(TriggerType.INCOMING_LAB.getKey());

            for (Trigger trigger : triggerList) {
                trigger.parseIncomingLabTrigger(hl7TextInfo, h);
            }
		} catch (Exception e) {
			logger.error("Error uploading lab to database");
			throw e;
		}

		return (retVal);

	}
	
	/**
	 * Method findProvidersForSpireLab
	 * Finds the providers that are associated with a spire lab.  (need to do this using doctor names, as
	 * spire labs don't have a valid ohip number associated with them).
	 */ 
	private static ArrayList<String> findProvidersForSpireLab(List<String> docNames) {
		List<String> docNums = new ArrayList<String>();
		ProviderDao providerDao = (ProviderDao)SpringUtils.getBean("providerDao");

		for (int i=0; i < docNames.size(); i++) {
			String[] firstLastName = docNames.get(i).split("\\s");
			if (firstLastName != null && firstLastName.length >= 2) {
				//logger.debug("Searching for provider with first and last name: " + firstLastName[0] + " " + firstLastName[firstLastName.length-1]);
				List<Provider> provList = providerDao.getProviderLikeFirstLastName("%"+firstLastName[0]+"%", firstLastName[firstLastName.length-1]);
				if (provList != null) {
					int provIndex = findProviderWithShortestFirstName(provList);
					if (provIndex != -1 && provList.size() >= 1 && !provList.get(provIndex).getProviderNo().equals("0")) {
						docNums.add( provList.get(provIndex).getProviderNo() );
						//logger.debug("ADDED1: " + provList.get(provIndex).getProviderNo());
					} else {
						// prepend 'dr ' to first name and try again
						provList = providerDao.getProviderLikeFirstLastName("dr " + firstLastName[0], firstLastName[1]);
						if (provList != null) {
							provIndex = findProviderWithShortestFirstName(provList);
							if (provIndex != -1 && provList.size() == 1 && !provList.get(provIndex).getProviderNo().equals("0")) {
								//logger.debug("ADDED2: " + provList.get(provIndex).getProviderNo());
								docNums.add( provList.get(provIndex).getProviderNo() );
							}
						}
					}
				}
			}
		}
		
		return (ArrayList<String>)docNums;
	}
	
	/**
	 * Method findProviderWithShortestFirstName
	 * Finds the provider with the shortest first name in a list of providers.
	 */ 
	private static int findProviderWithShortestFirstName(List<Provider> provList) {
		if (provList == null || provList.isEmpty())
			return -1;
			
		int index = 0;
		int shortestLength = provList.get(0).getFirstName().length();
		for (int i=1; i < provList.size(); i++) {
			int curLength = provList.get(i).getFirstName().length();
			if (curLength < shortestLength) {
				index = i;
				shortestLength = curLength;
			}
		}
		
		return index;
	}

  /**
   * Routes the providers report based on given parameters.
   *
   * @param labId          the lab ID
   * @param docNums        the document numbers
   * @param conn           the connection
   * @param altProviderNo  the alternative provider number
   * @param labType        the type of the lab
   * @param searchOn       the search criteria
   * @throws Exception     if an error occurs during routing
   */
  public static void providerRouteReport(
      final String labId,
      final ArrayList<String> docNums,
      final Connection conn,
      final String altProviderNo,
      final String labType,
      final String searchOn
  ) throws Exception {
    val providerNums = getProviderNumbers(docNums, searchOn);
    if (shouldSendToMRP(altProviderNo)) {
      providerNums.add(altProviderNo);
    }
    routeProviders(labId, conn, labType, providerNums, altProviderNo);
  }

  /**
   * Retrieves provider numbers based on given document numbers and search criteria.
   *
   * @param docNums   the document numbers
   * @param searchOn  the search criteria
   * @return a set of provider numbers
   */
  private static Set<String> getProviderNumbers(final ArrayList<String> docNums, final String searchOn) {
    val providerNums = new LinkedHashSet<String>();
    val sqlSearchOn = (searchOn != null && !searchOn.isEmpty()) ? searchOn : OHIP_NO;
    if (docNums != null) {
      for (var providerNumber : docNums) {
        if (providerNumber != null && !providerNumber.trim().isEmpty()) {
          providerNums.addAll(getProvidersMatchingNumber(providerNumber, sqlSearchOn));
        }
      }
    }
    return providerNums;
  }

  /**
   * Retrieves providers matching given provider number and search criteria.
   *
   * @param providerNumber the provider number
   * @param searchOn       the search criteria
   * @return a list of provider numbers
   */
  private static List<String> getProvidersMatchingNumber(final String providerNumber, final String searchOn) {
    val providerNumbers = new ArrayList<String>();
    var matchedProviders = new ArrayList<Provider>();
    if (OHIP_NO.equals(searchOn)) {
      // Retrieving providers who have a matching OHIP number.
      matchedProviders = (ArrayList<Provider>) providerDao.getBillableProvidersByOHIPNo(providerNumber);
      if ((matchedProviders == null || matchedProviders.isEmpty()) && providerNumber.length() == 5) {
        matchedProviders = (ArrayList<Provider>) providerDao.getBillableProvidersByOHIPNo("0" + providerNumber);
      }
    }
    else if ("practitionerNo".equals(searchOn)) {
      matchedProviders = (ArrayList<Provider>) providerDao.getProvidersByPractitionerNo(providerNumber);
    }
    else if ("hso_no".equals(searchOn)) {
      matchedProviders = (ArrayList<Provider>) providerDao.getProvidersByHsoNo(providerNumber);
    }
    if (matchedProviders != null) {
      for (var provider : matchedProviders) {
        providerNumbers.add(provider.getProviderNo());
      }
    }
    // Handling the case where other IDs (not OHIP, practitioner, or HSO numbers) are used for matching.
    // The key for the other ID is retrieved from the properties.
    val otherIdMatchKey = OscarProperties.getInstance().getProperty("lab.other_id_matching", "");
    if (otherIdMatchKey.length() > 0) {
      OtherId otherId = OtherIdManager.searchTable(Integer.valueOf(PROVIDER), otherIdMatchKey, providerNumber);
      if (otherId != null) {
        providerNumbers.add(otherId.getTableId());
      }
    }
    return providerNumbers;
  }

  /**
   * Determines if the document should be sent to MRP based on the alternative provider number.
   *
   * @param altProviderNo  the alternative provider number
   * @return true if should send to MRP, false otherwise
   */
  private static boolean shouldSendToMRP(final String altProviderNo) {
		return new ForwardingRules().getAlwaysSendToMRP() && altProviderNo != null && !altProviderNo.equals("0");
	}

  /**
   * Routes providers based on given parameters.
   *
   * @param labId         the lab ID
   * @param conn          the connection
   * @param labType       the type of the lab
   * @param providerNums  the provider numbers
   * @param altProviderNo the alternative provider number
   * @throws Exception    if an error occurs during routing
   */
  private static void routeProviders(
      final String labId,
      final Connection conn,
      final String labType,
      final Set<String> providerNums,
      final String altProviderNo
  ) throws Exception {
    val routing = new ProviderLabRouting();
    // Splitting the providers who want Chartech lab in the inbox into an array.
    val providersWantLabs = providersWantChartechLabInbox.split(",");
    if (!providerNums.isEmpty()) {
      for (var providerNo : providerNums) {
        // Checking the conditions - if the lab type is not CHARTECH or if it is CHARTECH and the provider is in the list of those who want Chartech labs.
        if (!CHARTECH.equalsIgnoreCase(labType)
            || (CHARTECH.equalsIgnoreCase(labType)
                    && !StringUtils.isBlank(providersWantChartechLabInbox)
                    && Arrays.asList(providersWantLabs).contains(providerNo))
        ) {
          // Routing the lab report to the specific provider.
          routing.route(labId, providerNo, conn, HL7);
        } else {
          // If conditions are not met, routing to a general inbox with provider number 0.
          routing.route(labId, "0", conn, HL7);
        }
      }
    } else {
      // If no provider numbers are provided, routing by demographic or to general inbox.
      routeByDemographicProviderOrGeneralInbox(labId, conn, labType, altProviderNo, providersWantLabs, routing);
    }
  }

  /**
   * Routes by demographic provider or to the general inbox based on given parameters.
   *
   * @param labId              the lab ID
   * @param conn               the connection
   * @param labType            the type of the lab
   * @param altProviderNo      the alternative provider number
   * @param providersWantLabs  the providers who want labs
   * @param routing            the provider lab routing
   * @throws Exception         if an error occurs during routing
   */
  private static void routeByDemographicProviderOrGeneralInbox(
      final String labId,
      final Connection conn,
      final String labType,
      final String altProviderNo,
      final String[] providersWantLabs,
      final ProviderLabRouting routing
  ) throws Exception {
    // Creating a list to store demographic providers associated with the alternative provider number.
    var demographicProviders = new ArrayList<Provider>();
    if (altProviderNo != null && !altProviderNo.trim().isEmpty()) {
      demographicProviders = (ArrayList<Provider>) providerDao.getProvidersByIds(Collections.singletonList(altProviderNo));
    }

    if (demographicProviders != null && !demographicProviders.isEmpty()) {
      // If the lab type is not CHARTECH and forwarding to MRP is allowed, or
      // if the lab type is CHARTECH, and the provider wants Chartech labs in the inbox.
      if (!CHARTECH.equalsIgnoreCase(labType) && new ForwardingRules().getAlwaysSendToMRP()
          || (CHARTECH.equalsIgnoreCase(labType) && !StringUtils.isBlank(providersWantChartechLabInbox) && Arrays.asList(providersWantLabs).contains(altProviderNo))) {
        // Routing the lab report to the specific demographic provider.
        routing.route(labId, altProviderNo, conn, HL7);
      } else {
        // Otherwise, routing to the general inbox.
        routing.route(labId, "0", conn, HL7);
      }
    } else {
      // If no demographic providers are found, routing to the general inbox.
      routing.route(labId, "0", conn, HL7);
    }
  }

	/**
	 * Attempt to match the doctors from the lab to a provider
	 */
	private static void providerRouteReport(String labId, ArrayList docNums, Connection conn, String altProviderNo, String labType) throws Exception {
		providerRouteReport(labId, docNums, conn, altProviderNo, labType, null);
	}

	/**
	 * Attempt to match the patient from the lab to a demographic, return the patients provider which is to be used when no other provider can be found to match the patient to.
	 */
	private static String patientRouteReport(LoggedInInfo loggedInInfo, Hl7TextInfo labInfo, String lastName, String firstName, String sex, String dob, String hin, String hinVersion, String homePhone, String workPhone, Connection conn, Integer alreadyMatchedDemoNo) throws SQLException {
		PatientLabRoutingResult result = null;
		
			String sql = null;
			String demo = "0";
			String provider_no = "0";
			// 19481015
			String dobYear = "%";
			String dobMonth = "%";
			String dobDay = "%";
			String hinMod = null;
			
			try {
				if (alreadyMatchedDemoNo != null && alreadyMatchedDemoNo > 0 && StringUtils.isBlank(hin)) {
					// if demographicNo was used in search (as in the case of some OLIS results 
					// where hin can be null), query for that record without HIN
					sql = "select demographic_no, provider_no from demographic where demographic_no = " + alreadyMatchedDemoNo;
				} else {
					if (hin != null) {
						hinMod = new String(hin);
						if (hinMod.length() == 12) {
							hinMod = hinMod.substring(0, 10);
						}
					}
		
					if (dob != null && !dob.equals("")) {
						String[] dobArray = dob.trim().split("-");
						dobYear = dobArray[0];
						dobMonth = dobArray[1];
						dobDay = dobArray[2];
					}

					String firstNameFirstLetter = firstName;
					String lastNameFirstLetter = lastName;
					// only the first letter of names
					if (!firstName.equals("")) {
						firstNameFirstLetter = firstName.substring(0, 1);
					}
					if (!lastName.equals("")) {
						lastNameFirstLetter = lastName.substring(0, 1);
					}
					// there are too many wild cards for this query to work with any amount of accuracy.
	//				if (hinMod.equals("%")) {
	//					sql = "select demographic_no, provider_no from demographic where" + " last_name like '" + lastName + "%' and " + " first_name like '" + firstName + "%' and " + " year_of_birth like '" + dobYear + "' and " + " month_of_birth like '" + dobMonth + "' and " + " date_of_birth like '" + dobDay + "' and " + " sex like '" + sex + "%' ";
	//				} 
					
					// HIN is ALWAYS required for lab matching. Please do not revert this code. Previous iterations have caused fatal patient miss-matches.				
					if( !StringUtils.isBlank(hinMod) ) {
						val labNoMatchNamesEnabled = systemPreferencesDao
								.isReadBooleanPreferenceWithDefault("lab_nomatch_names_enabled", true);
						if (labNoMatchNamesEnabled) {
							sql = "select demographic_no, provider_no from demographic where hin='" + hinMod + "' and " + " year_of_birth like '" + dobYear + "' and " + " month_of_birth like '" + dobMonth + "' and " + " date_of_birth like '" + dobDay + "' and " + " sex like '" + sex + "%' ";
						} else {
							sql = "select demographic_no, provider_no from demographic where hin='" + hinMod + "' and " + " last_name like '" + lastNameFirstLetter + "%' and " + " first_name like '" + firstNameFirstLetter + "%' and " + " year_of_birth like '" + dobYear + "' and " + " month_of_birth like '" + dobMonth + "' and " + " date_of_birth like '" + dobDay + "' and " + " sex like '" + sex + "%' ";
						}
					}
				}
				
				if( sql != null ) {
					logger.debug(sql);
					PreparedStatement pstmt = conn.prepareStatement(sql);
					ResultSet rs = pstmt.executeQuery();
					int count = 0;
					
					while (rs.next()) {
						result = new PatientLabRoutingResult();
						demo = oscar.Misc.getString(rs, "demographic_no");
						provider_no = oscar.Misc.getString(rs, "provider_no");
						result.setDemographicNo(Integer.parseInt(demo));
						result.setProviderNo(provider_no);
						count++;
					}
					rs.close();
					pstmt.close();
					if(count > 1) {
						result = null;
					}
				}
			} catch (SQLException sqlE) {
				throw sqlE;
			}

		SystemPreferences createDemographicOnUnmatchedLab = systemPreferencesDao.findPreferenceByName("createDemographicOnUnmatchedLab");
		if (result == null && createDemographicOnUnmatchedLab != null 
						&& ("always".equals(createDemographicOnUnmatchedLab.getValue()) 
						|| ("onlyOnBlankHin".equals(createDemographicOnUnmatchedLab.getValue()) && StringUtils.isEmpty(hinMod)))) {
			// no results found, attempt to create demographic and create link to that
			Demographic newDemographic = createDemographicRecordUsingLabData(loggedInInfo, labInfo, lastName, firstName, sex, dob, hin, hinVersion, homePhone, workPhone);
			result = new PatientLabRoutingResult();
			result.setDemographicNo(newDemographic.getDemographicNo());
			result.setProviderNo(provider_no);
		}
		
		try {
			//did this link a merged patient? if so, we need to make sure we are the head record, or update
			//result to be the head record.
			if(result != null) {
				DemographicMerged dm = new DemographicMerged();
				Integer headDemo = dm.getHead(result.getDemographicNo());
				if(headDemo != null && headDemo.intValue() != result.getDemographicNo()) {
					Demographic demoTmp = demographicManager.getDemographic(loggedInInfo, headDemo);
					if(demoTmp != null) {
						result.setDemographicNo(demoTmp.getDemographicNo());
						result.setProviderNo(demoTmp.getProviderNo());
					} else {
						logger.info("Unable to load the head record of this patient record. (" + result.getDemographicNo()  + ")");
						result = null;
					}
				}
				
				Hl7textResultsData.populateMeasurementsTable("" + labInfo.getLabNumber(), result.getDemographicNo().toString());

                sql = "insert into patientLabRouting (demographic_no, lab_no,lab_type,dateModified,created) values ('" + ((result != null && result.getDemographicNo()!=null)?result.getDemographicNo().toString():"0") + "', '" + labInfo.getLabNumber() + "','HL7',now(),now())";
                Connection c = null;
                PreparedStatement pstmt = null;
                try {
                    c = DbConnectionFilter.getThreadLocalDbConnection();
                    pstmt = c.prepareStatement(sql);
                    pstmt.executeUpdate();

                } finally {
                    try {
                        pstmt.close();
                        c.close();
                    }catch(SQLException e) {

                    }
                }
				if (OscarProperties.getInstance().isPropertyActive("queens_resident_tagging")) {
					val residentIds = new ArrayList<String>();
					val midwife = demographicExtDao.getValueForDemoKey(headDemo, DemographicExtKey.MIDWIFE);
					if (StringUtils.isNotEmpty(midwife)) {
						residentIds.add(midwife);
					}
					val nurse = demographicExtDao.getValueForDemoKey(headDemo, DemographicExtKey.NURSE);
					if (StringUtils.isNotEmpty(nurse)) {
						residentIds.add(nurse);
					}
					val resident = demographicExtDao.getValueForDemoKey(headDemo, DemographicExtKey.RESIDENT);
					if (StringUtils.isNotEmpty(resident)) {
						residentIds.add(resident);
					}
					for (String residentId : residentIds) {
						if (residentId != null && !residentId.equals("")) {
							ProviderLabRouting p = new ProviderLabRouting();
							p.routeMagic(labInfo.getLabNumber(), residentId, "HL7");
						}
					}
				}
			} else {
				logger.info("Could not find patient for lab: " + labInfo.getLabNumber());
			}
		} catch (SQLException sqlE) {
			logger.info("NO MATCHING PATIENT FOR LAB id =" + labInfo.getLabNumber());
			throw sqlE;
		}

		return (result != null)?result.getProviderNo():"0";
	}

	/**
	 * Used when errors occur to clean the database of labs that have not been inserted into all of the necessary tables
	 */
	public static void clean(int fileId) {
		
		List<Hl7TextMessage> results = hl7TextMessageDao.findByFileUploadCheckId(fileId);
		

		for (Hl7TextMessage result:results) {
			int lab_id = result.getId();
			
			Hl7TextInfo hti = hl7TextInfoDao.findLabId(lab_id);
			if(hti != null) {
				RecycleBin rb = new RecycleBin();
				rb.setProviderNo("0");
				rb.setUpdateDateTime(new Date());
				rb.setTableName("hl7TextInfo");
				rb.setKeyword(String.valueOf(lab_id));
				rb.setTableContent("<id>" + hti.getId() + "</id>" + "<lab_no>" + lab_id + "</lab_no>" + "<sex>" + hti.getSex() + "</sex>" + "<health_no>" +hti.getHealthNumber() + "</health_no>" + "<result_status>"
				        + hti.getResultStatus() + "</result_status>" + "<final_result_count>" + hti.getFinalResultCount() + "</final_result_count>" + "<obr_date>" + hti.getObrDate() + "</obr_date>" + "<priority>" + hti.getPriority() + "</priority>" + "<requesting_client>" + hti.getRequestingProvider() + "</requesting_client>" + "<discipline>" + hti.getDiscipline() + "</discipline>"
				        + "<last_name>" + hti.getLastName() + "</last_name>" + "<first_name>" + hti.getFirstName() + "</first_name>" + "<report_status>" + hti.getReportStatus() + "</report_status>" + "<accessionNum>" + hti.getAccessionNumber() + "</accessionNum>'");
				recycleBinDao.persist(rb);
				
				hl7TextInfoDao.remove(hl7TextInfoDao.findLabId(lab_id).getId());
			}

			Hl7TextMessage htm = hl7TextMessageDao.find(lab_id);
			if(htm != null) {
				RecycleBin rb = new RecycleBin();
				rb.setProviderNo("0");
				rb.setUpdateDateTime(new Date());
				rb.setTableName("hl7TextMessage");
				rb.setKeyword(String.valueOf(lab_id));
				rb.setTableContent("<lab_id>" + htm.getId() + "</lab_id>" + "<message>" + htm.getBase64EncodedeMessage() + "</message>" + "<type>" + htm.getType() + "</type>" + "<fileUploadCheck_id>" + htm.getFileUploadCheckId() + "</fileUploadCheck_id>");
				recycleBinDao.persist(rb);
				
				hl7TextMessageDao.remove(lab_id);
			}

			ProviderLabRoutingModel plr = providerLabRoutingDao.findByLabNo(lab_id);
			if(plr != null) {
				RecycleBin rb = new RecycleBin();
				rb.setProviderNo("0");
				rb.setUpdateDateTime(new Date());
				rb.setTableName("providerLabRouting");
				rb.setKeyword(String.valueOf(lab_id));
				rb.setTableContent("<provider_no>" + plr.getProviderNo() + "</provider_no>" + "<lab_no>" + plr.getLabNo() + "</lab_no>" + "<status>" +plr.getStatus() + "</status>" + "<comment>" + plr.getComment()
				        + "</comment>" + "<timestamp>" + plr.getTimestamp() + "</timestamp>" + "<lab_type>" + plr.getLabType() + "</lab_type>" + "<id>" + plr.getId() + "</id>");
				recycleBinDao.persist(rb);
				
				providerLabRoutingDao.remove(plr.getId());
			}
				
			PatientLabRouting lr = patientLabRoutingDao.findByLabNo(lab_id);
			if(lr != null) {
				RecycleBin rb = new RecycleBin();
				rb.setProviderNo("0");
				rb.setUpdateDateTime(new Date());
				rb.setTableName("patientLabRouting");
				rb.setKeyword(String.valueOf(lab_id));
				rb.setTableContent("<demographic_no>" + lr.getDemographicNo() + "</demographic_no>" + "<lab_no>" + lr.getLabNo() + "</lab_no>" + "<lab_type>" + lr.getLabType() + "</lab_type>" + "<id>" + lr.getId() + "</id>");
				recycleBinDao.persist(rb);

				patientLabRoutingDao.remove(lr.getId());
			}

			List<MeasurementsExt> measurementExts = measurementsExtDao.findByKeyValue("lab_no", String.valueOf(lab_id));
			for(MeasurementsExt me:measurementExts) {
				Measurement m = measurementDao.find(me.getMeasurementId());
				if(m != null) {
					RecycleBin rb = new RecycleBin();
					rb.setProviderNo("0");
					rb.setUpdateDateTime(new Date());
					rb.setTableName("measurements");
					rb.setKeyword(String.valueOf(me.getMeasurementId()));
					rb.setTableContent("<id>" +m.getId() + "</id>" + "<type>" + m.getType() + "</type>" + "<demographicNo>" +m.getDemographicId() + "</demographicNo>" + "<providerNo>" + m.getProviderNo() + "</providerNo>" + "<dataField>"
						        + m.getDataField() + "</dataField>" + "<measuringInstruction>" + m.getMeasuringInstruction() + "</measuringInstruction>" + "<comments>" + m.getComments() + "</comments>" + "<dateObserved>" + m.getDateObserved() + "</dateObserved>" + "<dateEntered>" + m.getCreateDate()+ "</dateEntered>");
					recycleBinDao.persist(rb);
					
					measurementDao.remove(m.getId());
				}
				
				List<MeasurementsExt> mes = measurementsExtDao.getMeasurementsExtByMeasurementId(me.getMeasurementId());
				for(MeasurementsExt me1:mes) {
					RecycleBin rb = new RecycleBin();
					rb.setProviderNo("0");
					rb.setUpdateDateTime(new Date());
					rb.setTableName("measurementsExt");
					rb.setKeyword(String.valueOf(me.getMeasurementId()));
					rb.setTableContent("<id>" + me1.getId() + "</id>" + "<measurement_id>" + me1.getMeasurementId() + "</measurement_id>" + "<keyval>" + me1.getKeyVal() + "</keyval>" + "<val>" + me1.getVal() + "</val>");
					recycleBinDao.persist(rb);
					
					measurementsExtDao.remove(me1.getId());
				}
			}
		}

		FileUploadCheck fuc = fileUploadCheckDao.find(fileId);
		if(fuc != null) {
			RecycleBin rb = new RecycleBin();
			rb.setProviderNo("0");
			rb.setUpdateDateTime(new Date());
			rb.setTableName("fileUploadCheck");
			rb.setKeyword(String.valueOf(fileId));
			rb.setTableContent("<id>" + fuc.getId() + "</id>" + "<provider_no>" + fuc.getProviderNo() + "</provider_no>" + "<filename>" + fuc.getFilename() + "</filename>" + "<md5sum>" + fuc.getMd5sum() + "</md5sum>" + "<datetime>" +fuc.getDateTime() + "</datetime>");
			recycleBinDao.persist(rb);	
			
			fileUploadCheckDao.remove(fuc.getId());
		}
	}
	
	private static Demographic createDemographicRecordUsingLabData(LoggedInInfo loggedInInfo, Hl7TextInfo 
				labInfo, String lastName, String firstName, String sex, String dob, String hin, String hinVersion, String homePhone, String workPhone) {
		OscarProperties oscarProperties = OscarProperties.getInstance();
		String creatingProviderNo = "-1";
		if (loggedInInfo != null && loggedInInfo.getLoggedInProviderNo() != null) {
			creatingProviderNo = loggedInInfo.getLoggedInProviderNo();
		}
		// no results found, attempt to create demographic and create link to that
		Demographic demographic = new Demographic();
		demographic.setTitle("");
		demographic.setLastName(lastName);
		demographic.setFirstName(firstName);
		demographic.setAddress("");
		demographic.setCity("");
		demographic.setPostal("");
		demographic.setSex(sex);
		demographic.setPhone(StringUtils.trimToEmpty(homePhone));
		demographic.setPhone2(StringUtils.trimToEmpty(workPhone));
		demographic.setEmail("");
		demographic.setSex(sex);
		demographic.setPatientStatus("AC");
		demographic.setChartNo("");
		demographic.setHin(hin);
		demographic.setVer(hinVersion);
		// attempt to use billregion for healthcard type
		String province = "OT";
		String hcType = "OT";
		if (ProvinceNames.getDefaultProvinces().containsKey(oscarProperties.getProperty("billregion", ""))) {
			hcType = oscarProperties.getProperty("billregion");
			province = oscarProperties.getProperty("billregion");
		}
		demographic.setHcType(hcType);
		demographic.setProvince(province);
		demographic.setRosterStatus("");
		demographic.setSpokenLanguage("");
		demographic.setOfficialLanguage("");
		demographic.setProviderNo("");
		demographic.setSin("");
		demographic.setCountryOfOrigin("-1");
		demographic.setNewsletter("Unknown");
		demographic.setPatientType("");
		demographic.setPatientId("");
		if (dob != null && !dob.isEmpty()) {
			String[] dobArray = dob.trim().split("-");
			demographic.setYearOfBirth(dobArray[0]);
			demographic.setMonthOfBirth(dobArray[1]);
			demographic.setDateOfBirth(dobArray[2]);
		}
		demographic.setLastUpdateDate(new Date());
		demographic.setLastUpdateUser(creatingProviderNo);
		demographic.setPatientStatusDate(new Date());
		demographic.setDateJoined(new Date());
		demographic.setPreferredName(StringUtils.EMPTY);

		var createdByLab = new DemographicExt();
		createdByLab.setDemographicNo(demographic.getDemographicNo());
		createdByLab.setProviderNo(creatingProviderNo);
		createdByLab.setKey("demographicCreatedByIncomingLab");
		createdByLab.setValue(String.valueOf(labInfo.getLabNumber()));
		createdByLab.setDateCreated(new Date());

		DemographicManager.createDemographic(
				demographic,
				Collections.singletonList(createdByLab)
		);

		GenericIntakeEditAction genericIntakeManager = new GenericIntakeEditAction();
		genericIntakeManager.setAdmissionManager(admissionManager);
		genericIntakeManager.setProgramManager(programManager);
		int bedProgramId = programDao.getProgramByName("OSCAR").getId();
		try {
			genericIntakeManager.admitBedCommunityProgram(demographic.getDemographicNo(),
					creatingProviderNo, bedProgramId, "", "", null);
		} catch (ProgramFullException | AdmissionException | ServiceRestrictionException e) {
			logger.error("Exception creating demographic from lab's admission data", e);
		}

		LogAction.addLog(creatingProviderNo, "add", "demographic", "", null,
				String.valueOf(demographic.getDemographicNo()), "added by lab with accession=" + labInfo.getAccessionNumber() + " , segmentId=" + labInfo.getLabNumber());
		return demographic;
	}
}
