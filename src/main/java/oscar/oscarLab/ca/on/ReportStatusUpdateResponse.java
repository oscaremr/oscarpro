package oscar.oscarLab.ca.on;

public class ReportStatusUpdateResponse {
    private boolean success = false;
    private Integer newCommentId;

    public ReportStatusUpdateResponse(boolean success, Integer newCommentId) {
        this.success = success;
        this.newCommentId = newCommentId;
    }
    public ReportStatusUpdateResponse(boolean success) {
        this(success, null);
    }

    public boolean isSuccess() {
        return success;
    }
    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Integer getNewCommentId() {
        return newCommentId;
    }
    public void setNewCommentId(Integer newCommentId) {
        this.newCommentId = newCommentId;
    }
}
