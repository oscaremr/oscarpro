/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package oscar.oscarLab.ca.on;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.PMmodule.caisi_integrator.CaisiIntegratorManager;
import org.oscarehr.PMmodule.caisi_integrator.IntegratorFallBackManager;
import org.oscarehr.billing.CA.BC.dao.Hl7MshDao;
import org.oscarehr.caisi_integrator.ws.CachedDemographicLabResult;
import org.oscarehr.common.dao.CtlDocumentDao;
import org.oscarehr.common.dao.DocumentCommentDao;
import org.oscarehr.common.dao.DocumentResultsDao;
import org.oscarehr.common.dao.Hl7TextMessageDao;
import org.oscarehr.common.dao.IncomingLabRulesDao;
import org.oscarehr.common.dao.LabPatientPhysicianInfoDao;
import org.oscarehr.common.dao.MdsMSHDao;
import org.oscarehr.common.dao.PatientLabRoutingDao;
import org.oscarehr.common.dao.ProviderLabRoutingDao;
import org.oscarehr.common.dao.QueueDocumentLinkDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.DocumentComment;
import org.oscarehr.common.model.IncomingLabRules;
import org.oscarehr.common.model.PatientLabRouting;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.ProviderLabRoutingModel;
import org.oscarehr.common.model.QueueDocumentLink;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentToDemographicDao;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentToProviderDao;
import org.oscarehr.labs.LabIdAndType;
import org.oscarehr.managers.DemographicManager;
import org.oscarehr.util.DbConnectionFilter;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.util.XmlUtils;
import org.xml.sax.SAXException;
import oscar.OscarProperties;
import oscar.oscarDB.ArchiveDeletedRecords;
import oscar.oscarLab.ca.all.Hl7textResultsData;
import oscar.oscarLab.ca.all.upload.ProviderLabRouting;
import oscar.oscarLab.ca.bc.PathNet.PathnetResultsData;
import oscar.oscarMDS.data.MDSResultsData;
import oscar.oscarMDS.data.ReportStatus;
import oscar.util.ConversionUtils;

@NoArgsConstructor
@Slf4j
public class CommonLabResultData {

	private static final CtlDocumentDao CTL_DOCUMENT_DAO
			= SpringUtils.getBean(CtlDocumentDao.class);
	private static final DemographicManager DEMOGRAPHIC_MANAGER
			= SpringUtils.getBean(DemographicManager.class);
	private static final DocumentCommentDao DOCUMENT_COMMENT_DAO
			= SpringUtils.getBean(DocumentCommentDao.class);
	private static final DocumentResultsDao DOCUMENT_RESULTS_DAO
			= SpringUtils.getBean(DocumentResultsDao.class);
	private static final Hl7MshDao HL7_MSH_DAO
			= SpringUtils.getBean(Hl7MshDao.class);
	private static final Hl7TextMessageDao HL7_TEXT_MESSAGE_DAO
			= SpringUtils.getBean(Hl7TextMessageDao.class);
	private static final HRMDocumentToDemographicDao HRM_DOCUMENT_TO_DEMOGRAPHIC_DAO
			= SpringUtils.getBean(HRMDocumentToDemographicDao.class);
	private static final HRMDocumentToProviderDao HRM_DOCUMENT_TO_PROVIDER_DAO
			= SpringUtils.getBean(HRMDocumentToProviderDao.class);
	private static final IncomingLabRulesDao INCOMING_LAB_RULES_DAO
			= SpringUtils.getBean(IncomingLabRulesDao.class);
	private static final LabPatientPhysicianInfoDao LAB_PATIENT_PHYSICIAN_INFO_DAO
			= SpringUtils.getBean(LabPatientPhysicianInfoDao.class);
	private static final MdsMSHDao MDS_MSH_DAO
			= SpringUtils.getBean(MdsMSHDao.class);
	private static final OscarProperties OSCAR_PROPERTIES
			= OscarProperties.getInstance();
	private static final PatientLabRoutingDao PATIENT_LAB_ROUTING_DAO
			= SpringUtils.getBean(PatientLabRoutingDao.class);
	private static final ProviderLabRoutingDao PROVIDER_LAB_ROUTING_DAO
			= SpringUtils.getBean(ProviderLabRoutingDao.class);
	private static final QueueDocumentLinkDao QUEUE_DOCUMENT_LINK_DAO
			= SpringUtils.getBean(QueueDocumentLinkDao.class);
	private static final SystemPreferencesDao SYSTEM_PREFERENCES_DAO
			= SpringUtils.getBean(SystemPreferencesDao.class);


	public static final boolean ATTACHED = true;
	public static final boolean UNATTACHED = false;

	public static final String NOT_ASSIGNED_PROVIDER_NO = "0";
	public static final String SPIRE = OSCAR_PROPERTIES.getProperty("Spire_LABS");
	public static final boolean EPSILON_LABS_ENABLED = SYSTEM_PREFERENCES_DAO
			.isReadBooleanPreferenceWithDefault("enable_epsilon_labs", false);
	public static final boolean PATHNET_LABS_ENABLED = SYSTEM_PREFERENCES_DAO
			.isReadBooleanPreferenceWithDefault("enable_pathnet_labs", false);
	public static final boolean HL7TEXT_LABS_ENABLED = SYSTEM_PREFERENCES_DAO
			.isReadBooleanPreferenceWithDefault("enable_hl7text_labs", true);
	public static final boolean MDS_LABS_ENABLED = SYSTEM_PREFERENCES_DAO
			.isReadBooleanPreferenceWithDefault("enable_mds_labs", false);
	public static final boolean CML_LABS_ENABLED = SYSTEM_PREFERENCES_DAO
			.isReadBooleanPreferenceWithDefault("enable_cml_labs", false);

	public static String[] getLabTypes() {
		return new String[] { "MDS", "CML", "BCP", "HL7", "DOC", "Epsilon", "HRM"};
	}

	public static boolean existsLabWithProviderNumberAndLabNumber(final String providerNumber, final String labNumber) {
		var labNumberInteger = 0;
		try {
			labNumberInteger = Integer.parseInt(labNumber);
		} catch (NumberFormatException | NullPointerException e) {
			// value already set; just continue
		}
		return existsLabWithProviderNumberAndLabNumber(providerNumber, labNumberInteger);
	}

	public static boolean existsLabWithProviderNumberAndLabNumber(final String providerNumber, final int labNumber) {
		return PROVIDER_LAB_ROUTING_DAO.existsProviderLabRoutingModelWithProviderNumberAndLabNumber(providerNumber, labNumber);
	}

	//Populate Lab data for Eform
	public ArrayList<LabResultData> populateLabEformData(
			final LoggedInInfo loggedInInfo,
			final String demographicNo,
			final String reqId,
			final boolean attach
	) {
		val labs = new ArrayList<LabResultData>();
	  val mDSData = new oscar.oscarMDS.data.MDSResultsData();
		if (CML_LABS_ENABLED) {
			labs.addAll(mDSData.populateCMLEformData(demographicNo, reqId, attach));
		}
		if (EPSILON_LABS_ENABLED) {
			labs.addAll(mDSData.populateEpsilonEformData(demographicNo, reqId, attach));
		}
		if (HL7TEXT_LABS_ENABLED) {
			labs.addAll(Hl7textResultsData.populateHL7EformData(demographicNo, reqId, attach));
		}
		if (MDS_LABS_ENABLED) {
			labs.addAll(mDSData.populateMDSEformData(demographicNo, reqId, attach));
		}
		if (PATHNET_LABS_ENABLED) {
			labs.addAll(new PathnetResultsData().populatePathnetEformData(demographicNo, reqId, attach));
		}
		return labs;
	}

	//Populate Lab data for consultation request
	public ArrayList<LabResultData> populateLabResultsData(LoggedInInfo loggedInInfo, String demographicNo, String reqId, boolean attach) {
		return populateLabResultsData(loggedInInfo, demographicNo, reqId, attach, false);
	}
	
	//Populate Lab data for consultation response
	public ArrayList<LabResultData> populateLabResultsDataConsultResponse(LoggedInInfo loggedInInfo, String demographicNo, String respId, boolean attach) {
		return populateLabResultsData(loggedInInfo, demographicNo, respId, attach, true);
	}
	
	//Populate Lab data for consultation (private shared method)
	private ArrayList<LabResultData> populateLabResultsData(LoggedInInfo loggedInInfo, String demographicNo, String consultId, boolean attach, boolean isConsultResponse) {
		val labs = new ArrayList<LabResultData>();
		val mDSData = new oscar.oscarMDS.data.MDSResultsData();
		if (CML_LABS_ENABLED) {
			val cmlLabs = isConsultResponse
					? mDSData.populateCMLResultsDataConsultResponse(demographicNo, consultId, attach)
					: mDSData.populateCMLResultsData(demographicNo, consultId, attach);
			labs.addAll(cmlLabs);
		}
		if (EPSILON_LABS_ENABLED) {
			val cmlLabs = isConsultResponse
					? mDSData.populateCMLResultsDataConsultResponse(demographicNo, consultId, attach)
					: mDSData.populateCMLResultsData(demographicNo, consultId, attach);
			labs.addAll(cmlLabs);
		}
		if (MDS_LABS_ENABLED) {
			val mdsLabs = isConsultResponse
					? mDSData.populateMDSResultsDataConsultResponse(demographicNo, consultId, attach)
					: mDSData.populateMDSResultsData(demographicNo, consultId, attach);
			labs.addAll(mdsLabs);
		}
		if (PATHNET_LABS_ENABLED) {
			val pathData = new PathnetResultsData();
			val pathLabs = isConsultResponse
					? pathData.populatePathnetResultsDataConsultResponse(demographicNo, consultId, attach)
					: pathData.populatePathnetResultsData(demographicNo, consultId, attach);
			labs.addAll(pathLabs);
		}
		if (HL7TEXT_LABS_ENABLED) {
			val hl7Labs = isConsultResponse
					? Hl7textResultsData.populateHL7ResultsDataConsultResponse(demographicNo, consultId, attach)
					: Hl7textResultsData.populateHL7ResultsData(demographicNo, consultId, attach);
			labs.addAll(hl7Labs);
		}
		return labs;
	}

	public ArrayList<LabResultData> populateLabResultsData(
			final LoggedInInfo loggedInInfo,
			final String providerNo,
			final String demographicNo,
			final String patientFirstName,
			final String patientLastName,
			final String patientHealthNumber,
			final String status,
			final boolean isPaged,
			final Integer page,
			final Integer pageSize,
			final boolean mixLabsAndDocs,
			final Boolean isAbnormal,
			final Date startDate,
			final Date endDate
	) {
		val labs = new ArrayList<LabResultData>();
		val mDSData = new oscar.oscarMDS.data.MDSResultsData();
		if (!isPaged && CML_LABS_ENABLED) {
			labs.addAll(mDSData.populateCMLResultsData(
					providerNo,
					demographicNo,
					patientFirstName,
					patientLastName,
					patientHealthNumber,
					status, null
			));
		}
		if (!isPaged && MDS_LABS_ENABLED) {
			labs.addAll(mDSData.populateMDSResultsData2(
					providerNo,
					demographicNo,
					patientFirstName,
					patientLastName,
					patientHealthNumber,
					status,
					null
			));
		}
		if (!isPaged && PATHNET_LABS_ENABLED) {
			val pathData = new PathnetResultsData();
			val pathLabs = pathData.populatePathnetResultsData(
					providerNo,
					demographicNo,
					patientFirstName,
					patientLastName,
					patientHealthNumber,
					status,
					null
			);
			labs.addAll(pathLabs);
		}
		if (HL7TEXT_LABS_ENABLED) {
			ArrayList<LabResultData> hl7Labs;
			if (isPaged) {
				hl7Labs = Hl7textResultsData.populateHl7ResultsData(
						providerNo,
						demographicNo,
						patientFirstName,
						patientLastName,
						patientHealthNumber,
						status,
						true,
						page,
						pageSize,
						mixLabsAndDocs,
						isAbnormal,
						startDate,
						endDate
				);
			} else {
				hl7Labs = Hl7textResultsData.populateHl7ResultsData(
						providerNo,
						demographicNo,
						patientFirstName,
						patientLastName,
						patientHealthNumber,
						status,
						null
				);
			}
			labs.addAll(hl7Labs);
		}
		return labs;
	}

	public ArrayList<LabResultData> populateLabResultsData(
			final LoggedInInfo loggedInInfo,
			final String providerNo,
			final String demographicNo,
			final String patientFirstName,
			final String patientLastName,
			final String patientHealthNumber,
			final String ackStatus,
			final String docScanStatus,
			final boolean isPaged,
			final Integer page,
			final Integer pageSize,
			final boolean mixLabsAndDocs,
			final Boolean isAbnormal
	) {
			return populateLabResultsData(
					loggedInInfo,
					providerNo,
					demographicNo,
					patientFirstName,
					patientLastName, patientHealthNumber,
					ackStatus,
					isPaged,
					page,
					pageSize,
					mixLabsAndDocs,
					isAbnormal,
					null,
					null
			);
	}

	public ArrayList<LabResultData> populateLabResultsData(LoggedInInfo loggedInInfo, String providerNo, String demographicNo, String patientFirstName, String patientLastName, String patientHealthNumber, String status) {
		return populateLabResultsData(loggedInInfo, providerNo, demographicNo, patientFirstName, patientLastName, patientHealthNumber, status, "I");
	}

	public ArrayList<LabResultData> populateLabResultsDataInboxIndexPage(LoggedInInfo loggedInInfo, String providerNo, String demographicNo, String patientFirstName, String patientLastName, String patientHealthNumber, String status, String scannedDocStatus) {
		val labs = new ArrayList<LabResultData>();
		if (scannedDocStatus != null
				&& (scannedDocStatus.equals("N")
						|| scannedDocStatus.equals("I")
						|| scannedDocStatus.equals(""))
		) {
			populateLabResultData(
					labs,
					providerNo,
					demographicNo,
					patientFirstName,
					patientLastName,
					patientHealthNumber,
					status
			);
		}
		if (scannedDocStatus != null
				&& (scannedDocStatus.equals("O")
						|| scannedDocStatus.equals("I")
						|| scannedDocStatus.equals(""))
		) {
			labs.addAll(DOCUMENT_RESULTS_DAO.populateDocumentResultsDataOfAllProviders(
							providerNo, demographicNo, status
			));
		}
		return labs;
	}

	// get documents that are specific provider to show in that provider's inbox
	public ArrayList<LabResultData> populateLabResultsData2(LoggedInInfo loggedInInfo, String providerNo, String demographicNo, String patientFirstName, String patientLastName, String patientHealthNumber, String status, String scannedDocStatus) {
		val labs = populateLabsData(
				loggedInInfo,
				providerNo,
				demographicNo,
				patientFirstName,
				patientLastName,
				patientHealthNumber,
				status,
				scannedDocStatus
		);
		labs.addAll(populateDocumentDataSpecificProvider(
				providerNo,
				demographicNo,
				patientFirstName,
				patientLastName,
				patientHealthNumber,
				status,
				scannedDocStatus
		));
		return labs;
	}

	// return documents specific to this provider only, doesn't include documents that are not linked to any provider
	public ArrayList<LabResultData> populateDocumentDataSpecificProvider(String providerNo, String demographicNo, String patientFirstName, String patientLastName, String patientHealthNumber, String status, String scannedDocStatus) {
		if (scannedDocStatus != null
				&& (scannedDocStatus.equals("O")
						|| scannedDocStatus.equals("I")
						|| scannedDocStatus.equals(""))) {
			return DOCUMENT_RESULTS_DAO.populateDocumentResultsDataLinkToProvider(
					providerNo, demographicNo, status
			);
		}
		return new ArrayList<>();
	}

	public ArrayList<LabResultData> populateDocumentData(String providerNo, String demographicNo, String patientFirstName, String patientLastName, String patientHealthNumber, String status, String scannedDocStatus) {
		if (scannedDocStatus != null
				&& (scannedDocStatus.equals("O")
						|| scannedDocStatus.equals("I")
						|| scannedDocStatus.equals(""))) {
			return DOCUMENT_RESULTS_DAO.populateDocumentResultsData(providerNo, demographicNo, status);
		}
		return new ArrayList<>();
	}

	public ArrayList<LabResultData> populateLabsData(LoggedInInfo loggedInInfo, String providerNo, String demographicNo, String patientFirstName, String patientLastName, String patientHealthNumber, String status, String scannedDocStatus) {
		val result = new ArrayList<LabResultData>();
		val ids = new ArrayList<Integer>();
		val parentId = ConversionUtils.fromIntString(demographicNo);
		ids.add(parentId);
		ids.addAll(DEMOGRAPHIC_MANAGER.getMergedDemographicIds(loggedInInfo, parentId));
		for(Integer id : ids) {
			result.addAll(generateLabResultData(
					providerNo,
					id.toString(),
					patientFirstName,
					patientLastName,
					patientHealthNumber,
					status,
					scannedDocStatus
			));
		}
		return result;
	}

	public ArrayList<LabResultData> populateLabResultsData(LoggedInInfo loggedInInfo, String providerNo, String demographicNo, String patientFirstName, String patientLastName, String patientHealthNumber, String status, String scannedDocStatus) {
		return populateLabsData(loggedInInfo, providerNo, demographicNo, patientFirstName, patientLastName, patientHealthNumber, status, scannedDocStatus);
	}

	public static ReportStatusUpdateResponse updateReportStatus(int labNo, String providerNo, char status, String comment, String labType) {
		return updateReportStatus(labNo, providerNo, status, comment, labType, null);
	}

	public static ReportStatusUpdateResponse updateReportStatus(int labNo, String providerNo, char status, String comment, String labType, String notStatus) {
		try {
			val providerLabRoutings
					= PROVIDER_LAB_ROUTING_DAO.findByLabNoAndLabTypeAndProviderNo(labNo, labType, providerNo);
			// handles the case where this provider/lab combination is not already in providerLabRouting table
			DocumentComment newComment = null;
			val timeStampDate = new Date();
			notStatus = StringUtils.trimToEmpty(notStatus);
			for (ProviderLabRoutingModel plr : providerLabRoutings) {
				if (plr != null && (StringUtils.isBlank(notStatus) || !notStatus.equals(plr.getStatus()))) {
					plr.setStatus(""+status);
					if ("DOC".equals(labType)
							&& StringUtils.isNotBlank(comment)
							&& !StringUtils.trimToEmpty(comment).equals(StringUtils.trimToEmpty(plr.getComment()))
					) {
						newComment = new DocumentComment(providerNo, labNo, comment);
						DOCUMENT_COMMENT_DAO.persist(newComment);
					}
					//we don't want to clobber existing comments when filing labs
					if (status != 'F' && StringUtils.isNotBlank(comment)) {
						plr.setComment(comment);
					}
					plr.setTimestamp(timeStampDate);
					PROVIDER_LAB_ROUTING_DAO.merge(plr);
          val providerLabRouting = new ProviderLabRouting();
          providerLabRouting.setHL7TextInfoAcknowlededDate(plr);
					// check for and update forwarding provider status if changed
					if (status != 'N') {
						val incomingLabRules = INCOMING_LAB_RULES_DAO.findRulesForForwardedToProviderAndTypeOf(providerNo, labType);
						for (IncomingLabRules incomingLabRule : incomingLabRules) {
							// if routed using incoming lab rules then check if update forwarding provider status is set
							val updateStatus = incomingLabRule.getUpdateForwardingProviderStatus();
							if (StringUtils.isNotEmpty(updateStatus) &&
									(IncomingLabRules.FORWARDING_STATUS_UPDATE.equals(updateStatus) ||
											(IncomingLabRules.FORWARDING_STATUS_FILE_ONLY.equals(updateStatus) && status == 'F'))) {
								val forwardingProviderRoutings = PROVIDER_LAB_ROUTING_DAO
										.findByLabNoAndLabTypeAndProviderNo(
												labNo, labType, incomingLabRule.getProviderNo()
										);
								for (ProviderLabRoutingModel forwardingProviderRouting : forwardingProviderRoutings) {
									forwardingProviderRouting.setStatus(String.valueOf(status));
									forwardingProviderRouting.setTimestamp(timeStampDate);
									PROVIDER_LAB_ROUTING_DAO.saveEntity(forwardingProviderRouting);
								}
							}
						}
					}
				}
			}
			if (providerLabRoutings.isEmpty()) {
				val p = new ProviderLabRoutingModel();
				p.setProviderNo(providerNo);
				p.setLabNo(labNo);
				p.setStatus(String.valueOf(status));
				p.setComment(comment);
				p.setLabType(labType);
				p.setTimestamp(new Date());
				PROVIDER_LAB_ROUTING_DAO.persist(p);
				val helper = new ProviderLabRouting();
        helper.setHL7TextInfoAcknowlededDate(p);
				if ("DOC".equals(labType) && StringUtils.isNotBlank(comment)) {
					newComment = new DocumentComment(providerNo, labNo, comment);
					DOCUMENT_COMMENT_DAO.persist(newComment);
				}
			}
			if (!"0".equals(providerNo)) {
				val modelRecords = PROVIDER_LAB_ROUTING_DAO
						.findByLabNoAndLabTypeAndProviderNo(labNo, labType, providerNo);
				val adr = new ArchiveDeletedRecords();
				adr.recordRowsToBeDeleted(modelRecords, "" + providerNo, "providerLabRouting");
				for (ProviderLabRoutingModel plr :
						PROVIDER_LAB_ROUTING_DAO.findByLabNoAndLabTypeAndProviderNo(labNo, labType, "0")
				) {
					PROVIDER_LAB_ROUTING_DAO.remove(plr.getId());
				}
			}
			return new ReportStatusUpdateResponse(true, (newComment != null ? newComment.getId() : null));
		} catch (Exception e) {
			log.error("exception in MDSResultsData.updateReportStatus()", e);
			return new ReportStatusUpdateResponse(false, null);
		} finally {
			DbConnectionFilter.releaseThreadLocalDbConnection();
		}
	}

	public ArrayList<ReportStatus> getStatusArray(String labId, String labType) {
		val statusArray = new ArrayList<ReportStatus>();
		for (Object[] i : PROVIDER_LAB_ROUTING_DAO
				.getProviderLabRoutings(ConversionUtils.fromIntString(labId), labType)
		) {
			val p = (Provider) i[0];
			val m = (ProviderLabRoutingModel) i[1];
			statusArray.add(new ReportStatus(p.getFullName(),
					p.getProviderNo(),
					descriptiveStatus(m.getStatus()),
					m.getComment(),
					ConversionUtils.toTimestampString(m.getTimestamp()),
					labId));
		}
		return statusArray;
	}

	public String descriptiveStatus(String status) {
		switch (status.charAt(0)) {
		case 'A':
			return "Acknowledged";
		case 'F':
			return "Filed but not acknowledged";
		case 'U':
			return "N/A";
		default:
			return "Not Acknowledged";
		}
	}
	public static String matchProviderWithLab(
			final String providerNumber,
			final String labNumber,
			final String labType,
			final String comment
	) {
		Integer labNumberInteger = null;
		try {
			labNumberInteger = Integer.parseInt(labNumber);
		} catch (NumberFormatException | NullPointerException e) {
			labNumberInteger = 0;
		}
		return matchProviderWithLab(providerNumber, labNumberInteger, labType, comment);
	}
	public static String matchProviderWithLab(
			final String providerNumber,
			final Integer labNumber,
			final String labType,
			final String comment
	) {

		return PROVIDER_LAB_ROUTING_DAO.addProviderLabRouting(providerNumber, labNumber, "N", comment, labType);
	}
	public static String searchPatient(String labNo, String labType) {
		val routings = PATIENT_LAB_ROUTING_DAO.findByLabNoAndLabType(
				ConversionUtils.fromIntString(labNo), labType
		);
		if (routings.isEmpty()) {
			return "0";
		}
		return routings.get(0).getDemographicNo().toString();
	}

	public static boolean updatePatientLabRouting(String labNo, String demographicNo, String labType) {
		boolean result = false;
		try {
			// update patientLabRouting for labs with the same accession number
			val data = new CommonLabResultData();
			val labArray = data.getMatchingLabs(labNo, labType).split(",");
			for (String s : labArray) {
				// delete old entries
				for (PatientLabRouting p :
						PATIENT_LAB_ROUTING_DAO.findByLabNoAndLabType(Integer.parseInt(s), labType)) {
					PATIENT_LAB_ROUTING_DAO.remove(p.getId());
				}
				// add new entries
				val plr = new PatientLabRouting();
				plr.setLabNo(Integer.parseInt(s));
				plr.setDemographicNo(Integer.parseInt(demographicNo));
				plr.setLabType(labType);
				PATIENT_LAB_ROUTING_DAO.persist(plr);
				if (OscarProperties.getInstance().isPropertyActive("queens_resident_tagging")) {
					val residentIds = DEMOGRAPHIC_MANAGER.getQueensResidentProviderNumbers(demographicNo);
					for (String residentId : residentIds) {
						if (residentId != null && !residentId.equals("")) {
							val p = new ProviderLabRouting();
							p.routeMagic(Integer.parseInt(s), residentId, labType);
						}
					}
				}
				// add labs to measurements table
				populateMeasurementsTable(s, demographicNo, labType);
			}
			return result;
		} catch (Exception e) {
			log.error("exception in CommonLabResultData.updateLabRouting()", e);
			return false;
		}
	}

	public static boolean updateLabRouting(ArrayList<LabIdAndType> flaggedLabs, String selectedProviders) {
		try {
			val providersArray = selectedProviders.split(",");
			val data = new CommonLabResultData();
			val plr = new ProviderLabRouting();
			for (LabIdAndType flaggedLab : flaggedLabs) {
				val lab = String.valueOf(flaggedLab.getLabId());
				val labType = flaggedLab.getLabType();
				// Forward all versions of the lab
				val matchingLabs = data.getMatchingLabs(lab, labType);
				val labIds = matchingLabs.split(",");
				for (String labId : labIds) {
					for (String s : providersArray) {
						plr.route(labId, s, DbConnectionFilter.getThreadLocalDbConnection(), labType);
					}
					// delete old entries
					for (ProviderLabRoutingModel p :
							PROVIDER_LAB_ROUTING_DAO.findByLabNoAndLabTypeAndProviderNo(
									Integer.parseInt(labId), labType, "0")
					) {
						PROVIDER_LAB_ROUTING_DAO.remove(p.getId());
					}
				}
			}
			return true;
		} catch (Exception e) {
			log.error("exception in CommonLabResultData.updateLabRouting()", e);
			return false;
		}
	}

	public static boolean fileLabs(ArrayList<String[]> flaggedLabs, String provider) {
		val data = new CommonLabResultData();
		for (String[] strarr : flaggedLabs) {
			val lab = strarr[0];
			val labType = strarr[1];
			val labs = data.getMatchingLabs(lab, labType);
			if (labs != null && !labs.equals("")) {
				val labArray = labs.split(",");
				for (String s : labArray) {
					updateReportStatus(Integer.parseInt(s), provider, 'F', "", labType);
					removeFromQueue(Integer.parseInt(s));
				}
			} else {
				updateReportStatus(Integer.parseInt(lab), provider, 'F', "", labType);
				removeFromQueue(Integer.parseInt(lab));
			}
		}
		return true;
	}

	public String getMatchingLabs(String lab_no, String lab_type) {
		String labs = null;
		if (lab_type.equals(LabResultData.HL7TEXT)) {
			labs = Hl7textResultsData.getMatchingLabs(lab_no);
		} else if (lab_type.equals(LabResultData.MDS)) {
			val data = new MDSResultsData();
			labs = data.getMatchingLabs(lab_no);
		} else if (lab_type.equals((LabResultData.EXCELLERIS))) {
			val data = new PathnetResultsData();
			labs = data.getMatchingLabs(lab_no);
		} else if (lab_type.equals(LabResultData.CML)) {
			val data = new MDSResultsData();
			labs = data.getMatchingCMLLabs(lab_no);
		} else if (lab_type.equals(LabResultData.DOCUMENT)) {
			labs = lab_no;// one document is only linked to one patient.
		} else if (lab_type.equals(LabResultData.HRM)){
			labs = lab_no;
		}
		return labs;
	}

	public String getDemographicNo(String labId, String labType) {
		return searchPatient(labId, labType);
	}

	public boolean isDocLinkedWithPatient(String labId, String labType) {
		val docList = CTL_DOCUMENT_DAO.findByDocumentNoAndModule(
				ConversionUtils.fromIntString(labId), "demographic"
		);
		if (docList.isEmpty()) {
			return false;
		}
		val mi = ConversionUtils.toIntString(docList.get(0).getId().getModuleId());
		return mi != null && !mi.trim().equals("-1");
	}

	public boolean isLabLinkedWithPatient(String labId, String labType) {
		val routing = PATIENT_LAB_ROUTING_DAO.findDemographics(
				labType, ConversionUtils.fromIntString(labId)
		);
		if (routing == null)
			return false;
		val demo = ConversionUtils.toIntString(routing.getDemographicNo());
		return demo != null && !demo.trim().equals("0");
	}

	public boolean isHRMLinkedWithPatient(String labId, String labType) {
		boolean ret = false;
		try {
			val docToDemo
					= HRM_DOCUMENT_TO_DEMOGRAPHIC_DAO.findByHrmDocumentId(Integer.parseInt(labId));
			if (docToDemo != null && docToDemo.size() > 0) {
				ret = true;
			}
		} catch (Exception e) {
			log.error("exception in isLabLinkedWithPatient", e);
		}
		return ret;
	}

	public int getAckCount(String labId, String labType) {
		if (labType.equals(LabResultData.HRM)) {
			val labIdInteger = Integer.parseInt(labId);
			return HRM_DOCUMENT_TO_PROVIDER_DAO.findSignedByHrmDocumentId(labIdInteger).size();
		} else {
			return PROVIDER_LAB_ROUTING_DAO.findByStatusANDLabNoType(
					ConversionUtils.fromIntString(labId), labType, "A"
			).size();
		}
	}

	public static void populateMeasurementsTable(String labId, String demographicNo, String labType) {
		if (labType.equals(LabResultData.HL7TEXT)) {
			Hl7textResultsData.populateMeasurementsTable(labId, demographicNo);
		}
	}

	public static ArrayList<LabResultData> getRemoteLabs(LoggedInInfo loggedInInfo,Integer demographicId) {
		val results = new ArrayList<LabResultData>();
		try {
			List<CachedDemographicLabResult> labResults  = null;
			try {
				if (!CaisiIntegratorManager.isIntegratorOffline(loggedInInfo.getSession())){
					labResults = CaisiIntegratorManager.getDemographicWs(loggedInInfo, loggedInInfo.getCurrentFacility()).getLinkedCachedDemographicLabResults(demographicId);
				}
			} catch (Exception e) {
				log.error("Unexpected error.", e);
				CaisiIntegratorManager.checkForConnectionError(loggedInInfo.getSession(),e);
			}
			if (CaisiIntegratorManager.isIntegratorOffline(loggedInInfo.getSession())) {
				labResults = IntegratorFallBackManager.getLabResults(loggedInInfo,demographicId);
			}
			for (CachedDemographicLabResult cachedDemographicLabResult : labResults) {
				results.add(toLabResultData(cachedDemographicLabResult));
			}
		} catch (Exception e) {
			log.error("Error retrieving remote labs", e);
		}
		return (results);
	}

	public List<LabIdAndType> getCmlAndEpsilonLabResultsSince(Integer demographicNo, Date updateDate) {
		//This case handles Epsilon and the old CML data
		val ids = LAB_PATIENT_PHYSICIAN_INFO_DAO.getLabResultsSince(demographicNo, updateDate);
		val results = new ArrayList<LabIdAndType>();
		for (Integer id:ids) {
			results.add(new LabIdAndType(id,"CML"));
		}
		return results;
	}

	public List<LabIdAndType> getMdsLabResultsSince(Integer demographicNo, Date updateDate) {
		//This case handles old MDS data
		val ids = MDS_MSH_DAO.getLabResultsSince(demographicNo, updateDate);
		val results = new ArrayList<LabIdAndType>();
		for (Integer id:ids) {
			results.add(new LabIdAndType(id,"MDS"));
		}
		return results;
	}

	public List<LabIdAndType> getPathnetResultsSince(Integer demographicNo, Date updateDate) {
		val ids = HL7_MSH_DAO.getLabResultsSince(demographicNo, updateDate);
		val results = new ArrayList<LabIdAndType>();
		for (Integer id:ids) {
			results.add(new LabIdAndType(id,"BCP"));
		}
		return results;
	}

	public List<LabIdAndType> getHl7ResultsSince(Integer demographicNo, Date updateDate) {
		val ids = HL7_TEXT_MESSAGE_DAO.getLabResultsSince(demographicNo, updateDate);
		val results = new ArrayList<LabIdAndType>();
		for (Integer id:ids) {
			results.add(new LabIdAndType(id,"HL7"));
		}
		return results;
	}

	public LabResultData getLab(LabIdAndType labIdAndType) {
		val mDSData = new oscar.oscarMDS.data.MDSResultsData();
		val pathData = new PathnetResultsData();
		val resultsList = new ArrayList<LabResultData>();
		if("Epsilon".equals(labIdAndType.getLabType())) {
			resultsList.addAll(mDSData.populateEpsilonResultsData(null, null, null, null, null, null,labIdAndType.getLabId()));
		} else if("CML".equals(labIdAndType.getLabType())) {
			resultsList.addAll(mDSData.populateCMLResultsData(null, null, null, null, null, null,labIdAndType.getLabId()));
		} else if("BCP".equals(labIdAndType.getLabType())) {
			resultsList.addAll(pathData.populatePathnetResultsData(null, null, null, null, null, null, labIdAndType.getLabId()));
		} else if("MDS".equals(labIdAndType.getLabType())) {
			resultsList.addAll(mDSData.populateMDSResultsData2(null, null, null, null, null, null,labIdAndType.getLabId()));
		} else if("HL7".equals(labIdAndType.getLabType())) {
			resultsList.addAll(Hl7textResultsData.populateHl7ResultsData(null, null, null, null, null, null,labIdAndType.getLabId()));
		}
		if(!resultsList.isEmpty()) {
			return resultsList.get(0);
		}
		return null;
	}

	/* PRIVATE HELPER METHODS */

	private static LabResultData toLabResultData(
			CachedDemographicLabResult cachedDemographicLabResult
	) throws IOException, SAXException, ParserConfigurationException {
		val result = new LabResultData();
		result.setRemoteFacilityId(
				cachedDemographicLabResult.getFacilityIdLabResultCompositePk().getIntegratorFacilityId()
		);
		result.labType = cachedDemographicLabResult.getType();
		val doc = XmlUtils.toDocument(cachedDemographicLabResult.getData());
		val root = doc.getFirstChild();
		result.acknowledgedStatus = XmlUtils.getChildNodeTextContents(root, "acknowledgedStatus");
		result.accessionNumber = XmlUtils.getChildNodeTextContents(root, "accessionNumber");
		result.dateTime = XmlUtils.getChildNodeTextContents(root, "dateTime");
		result.discipline = XmlUtils.getChildNodeTextContents(root, "discipline");
		result.healthNumber = XmlUtils.getChildNodeTextContents(root, "healthNumber");
		result.labPatientId = XmlUtils.getChildNodeTextContents(root, "labPatientId");
		result.patientName = XmlUtils.getChildNodeTextContents(root, "patientName");
		result.priority = XmlUtils.getChildNodeTextContents(root, "priority");
		result.reportStatus = XmlUtils.getChildNodeTextContents(root, "reportStatus");
		result.requestingClient = XmlUtils.getChildNodeTextContents(root, "requestingClient");
		result.segmentID = XmlUtils.getChildNodeTextContents(root, "segmentID");
		result.sex = XmlUtils.getChildNodeTextContents(root, "sex");
		result.setAckCount(Integer.parseInt(XmlUtils.getChildNodeTextContents(root, "ackCount")));
		result.setMultipleAckCount(Integer.parseInt(XmlUtils.getChildNodeTextContents(root, "multipleAckCount")));
		return result;
	}

	private static void removeFromQueue(Integer lab_no) {
		val queues = QUEUE_DOCUMENT_LINK_DAO.getQueueFromDocument(lab_no);
		for( QueueDocumentLink queue : queues ) {
			QUEUE_DOCUMENT_LINK_DAO.remove(queue.getId());
		}
	}

	private ArrayList<LabResultData> generateLabResultData(
			final String providerNo,
			final String demographicNo,
			final String patientFirstName,
			final String patientLastName,
			final String  patientHealthNumber,
			final String  status,
			final String scannedDocStatus
	) {
		val labs = new ArrayList<LabResultData>();
		if (scannedDocStatus != null
				&& (scannedDocStatus.equals("N")
						|| scannedDocStatus.equals("I")
						|| scannedDocStatus.equals(""))
		) {
			populateLabResultData(
					labs,
					providerNo,
					demographicNo,
					patientFirstName,
					patientLastName,
					patientHealthNumber,
					status
			);
		}
		return labs;
	}

	private void populateLabResultData(
			final ArrayList<LabResultData> labs,
			final String providerNo,
			final String demographicNo,
			final String patientFirstName,
			final String patientLastName,
			final String patientHealthNumber,
			final String status
	) {
		val mDSData = new oscar.oscarMDS.data.MDSResultsData();
		if (CML_LABS_ENABLED) {
			labs.addAll(mDSData.populateCMLResultsData(
					providerNo,
					demographicNo,
					patientFirstName,
					patientLastName,
					patientHealthNumber,
					status,
					null
			));
		}
		if (EPSILON_LABS_ENABLED) {
			labs.addAll(mDSData.populateEpsilonResultsData(
					providerNo,
					demographicNo,
					patientFirstName,
					patientLastName,
					patientHealthNumber,
					status,
					null
			));
		}
		if (HL7TEXT_LABS_ENABLED) {
			labs.addAll(Hl7textResultsData.populateHl7ResultsData(
					providerNo,
					demographicNo,
					patientFirstName,
					patientLastName,
					patientHealthNumber,
					status,
					null
			));
		}
		if (MDS_LABS_ENABLED) {
			labs.addAll(mDSData.populateMDSResultsData2(
					providerNo,
					demographicNo,
					patientFirstName,
					patientLastName,
					patientHealthNumber,
					status,
					null
			));
		}
		if (PATHNET_LABS_ENABLED) {
			labs.addAll(new PathnetResultsData().populatePathnetResultsData(
					providerNo,
					demographicNo,
					patientFirstName,
					patientLastName,
					patientHealthNumber,
					status,
					null
			));
		}

		if (SPIRE != null && SPIRE.trim().equals("yes")) {
			// no-op
			// SpireResultsData spireData = new SpireResultsData();
			// ArrayList<LabResultData> spireLabs = spireData.populateSpireResultsData(providerNo, demographicNo, patientFirstName, patientLastName, patientHealthNumber, status);
			// labs.addAll(spireLabs);
		}

		Collections.sort(labs);
	}
}
