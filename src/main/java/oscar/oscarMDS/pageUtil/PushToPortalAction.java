/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.oscarMDS.pageUtil;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.common.dao.PatientLabRoutingDao;
import org.oscarehr.common.model.PatientLabRouting;
import org.oscarehr.integration.yourcare.DataSyncService;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

public class PushToPortalAction extends DispatchAction {

  private static Logger logger = MiscUtils.getLogger();

  public PushToPortalAction() {
  }

  public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request,
          HttpServletResponse response) throws ServletException, IOException {
    return executemain(mapping, form, request, response);
  }

  public ActionForward executemain(ActionMapping mapping, ActionForm form, HttpServletRequest request,
          HttpServletResponse response) {
    try {
      int labNo = Integer.parseInt(request.getParameter("segmentID"));
      String lab_type = request.getParameter("labType");

      String demographicID = getDemographicIdFromLab(lab_type, labNo);
      DataSyncService dataSyncService = SpringUtils.getBean(DataSyncService.class);
      dataSyncService.manuallySyncLab(labNo, Integer.parseInt(demographicID),
              LoggedInInfo.getLoggedInInfoFromSession(request));
      return mapping.findForward("success");
    } catch (Exception e) {
      logger.error("exception in ReportStatusUpdateAction", e);
      return mapping.findForward("failure");
    }
  }


  private static String getDemographicIdFromLab(String labType, int labNo) {
    String demographicID = "";
    PatientLabRoutingDao dao = SpringUtils.getBean(PatientLabRoutingDao.class);
    for (PatientLabRouting r : dao.findByLabNoAndLabType(labNo, labType)) {
      demographicID = "" + r.getDemographicNo();
    }
    return demographicID;
  }
}
