/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarMDS.pageUtil;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.common.dao.ProviderLabRoutingDao;
import org.oscarehr.common.model.IncomingLabRules;
import org.oscarehr.managers.DemographicManager;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

import oscar.oscarLab.ForwardingRules;
import oscar.oscarLab.ca.on.CommonLabResultData;

@Slf4j
public class PatientMatchAction extends DispatchAction {

  private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
  private DemographicManager demographicManager = SpringUtils.getBean(DemographicManager.class);
  private ProviderLabRoutingDao providerLabRoutingDao = SpringUtils.getBean(ProviderLabRoutingDao.class);

  public PatientMatchAction() {
   }

  public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {

	   if (!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_lab", "w", null)) {
	     throw new SecurityException("missing required security object (_lab)");
		 }
	   
	   String demographicNo = request.getParameter("demographicNo");
	   String labNo = request.getParameter("labNo");
	   String labType = request.getParameter("labType");
	   String providerNo = request.getParameter("providerNo");

	   String newURL = "";
	   val demographic = demographicManager.getDemographic(LoggedInInfo.getLoggedInInfoFromSession(request), Integer.parseInt(demographicNo));
	   val id =
         CommonLabResultData.matchProviderWithLab(demographic.getProviderNo(), labNo, labType, "");
	   log.debug("Object matched :" + id);

	   if (demographic.getProviderNo() != null) {
	     this.forwardLabToProviders(demographic.getProviderNo(), labType, labNo);
	   }
	   CommonLabResultData.updatePatientLabRouting(labNo, demographicNo,labType);
	   newURL = mapping.findForward("success").getPath();
	   newURL = newURL + "?demographicNo=" + demographicNo + "&labNo=" + labNo + "&providerNo=" + providerNo;

	   return (new ActionForward(newURL));
  }

  private void forwardLabToProviders(String providerNo, String labType, String labNo) {
    val forwardingRules = new ForwardingRules();

    val forwardingRulesAndProviders = forwardingRules.getForwardRulesAndProviders(providerNo);
    for (val object : forwardingRulesAndProviders) {
      val forwardRules = (IncomingLabRules) object[0];
      val routings = providerLabRoutingDao.findByLabNoAndLabTypeAndProviderNo(Integer.parseInt(labNo), labType, forwardRules.getFrwdProviderNo());
      if (forwardRules.getForwardTypeStrings().contains(labType)
          && "HL7".equalsIgnoreCase(labType)) {
        if (routings.isEmpty()) {
          CommonLabResultData.matchProviderWithLab(forwardRules.getFrwdProviderNo(), labNo, labType, "");
          log.info("FORWARDING PROVIDER: " + (forwardRules.getFrwdProviderNo()));
          this.forwardLabToProviders(forwardRules.getFrwdProviderNo(), labType, labNo);
        }
      }
    }
  }
}
