/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarRx.pageUtil;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.integration.yourcare.DataSyncService;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

public final class RxRePushToPortalAction extends DispatchAction {
	
	private static final String PRIVILEGE_READ = "r"; 
	private static final String PRIVILEGE_WRITE = "w";

	private static final Logger logger = MiscUtils.getLogger();
	private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
  private DataSyncService dataSyncService = SpringUtils.getBean(DataSyncService.class);


  public ActionForward pushToPortal(ActionMapping mapping, ActionForm form, HttpServletRequest request,
          HttpServletResponse response) throws IOException {
    MiscUtils.getLogger().debug("================in pushToPortal of RxRePushToPortalAction.java=================");
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    checkPrivilege(loggedInInfo, PRIVILEGE_WRITE);
    oscar.oscarRx.pageUtil.RxSessionBean bean = (oscar.oscarRx.pageUtil.RxSessionBean) request.getSession()
            .getAttribute("RxSessionBean");
    String drugIds = request.getParameter("drugIds");
    for (String drugId : drugIds.split(",")) {
      if (StringUtils.isNotBlank(drugId)) {
        dataSyncService.manuallySyncMedication(Integer.parseInt(drugId), bean.getDemographicNo(), loggedInInfo);
      }
    }

    MiscUtils.getLogger().debug("================END pushToPortal of RxRePushToPortalAction.java=================");
    return (mapping.findForward("success"));
  }

	public void p(String s) {
		MiscUtils.getLogger().debug(s);
	}

	public void p(String s, String s1) {
		MiscUtils.getLogger().debug(s + "=" + s1);
	}

	
	private void checkPrivilege(LoggedInInfo loggedInInfo, String privilege) {
		if (!securityInfoManager.hasPrivilege(loggedInInfo, "_rx", privilege, null)) {
			throw new RuntimeException("missing required security object (_rx)");
		}
	}
}
