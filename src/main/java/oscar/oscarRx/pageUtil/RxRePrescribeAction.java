/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarRx.pageUtil;

import java.io.IOException;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lowagie.text.DocumentException;
import lombok.val;
import lombok.var;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.oscarehr.casemgmt.service.CaseManagementManager;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DemographicPharmacyDao;
import org.oscarehr.common.dao.PharmacyInfoDao;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.dao.PrescriptionDao;
import org.oscarehr.common.dao.PrescriptionFaxDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.DrugDao;
import org.oscarehr.common.model.Drug;
import org.oscarehr.common.model.UserProperty;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.DemographicPharmacy;
import org.oscarehr.common.model.PharmacyInfo;
import org.oscarehr.common.model.PrescriptionFax;
import org.oscarehr.managers.DemographicManager;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.ui.servlet.ImageRenderingServlet;
import org.oscarehr.util.DigitalSignatureUtils;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.owasp.encoder.Encode;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import oscar.OscarProperties;
import oscar.form.pdfservlet.FrmCustomedPDFGenerator;
import oscar.form.pdfservlet.FrmCustomedPDFParameters;

import oscar.log.LogAction;
import oscar.log.LogConst;
import oscar.oscarEncounter.pageUtil.EctSessionBean;
import oscar.oscarProvider.data.ProSignatureData;
import oscar.oscarRx.data.RxPatientData;
import oscar.oscarRx.data.RxPatientData.Patient;
import oscar.oscarRx.data.RxPrescriptionData;
import oscar.oscarRx.data.RxPrescriptionData.Prescription;
import oscar.oscarRx.data.RxProviderData;
import oscar.oscarRx.util.RxUtil;
import static oscar.util.StringUtils.nullSafeEquals;

public final class RxRePrescribeAction extends DispatchAction {
	
	private static final String PRIVILEGE_READ = "r"; 
	private static final String PRIVILEGE_WRITE = "w";

	private static final Logger logger = MiscUtils.getLogger();
	private SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);

	public ActionForward reprint(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		checkPrivilege(loggedInInfo, PRIVILEGE_READ);
		
		oscar.oscarRx.pageUtil.RxSessionBean sessionBeanRX = (oscar.oscarRx.pageUtil.RxSessionBean) request.getSession().getAttribute("RxSessionBean");
		if (sessionBeanRX == null) {
			response.sendRedirect("error.html");
			return null;
		}

		oscar.oscarRx.pageUtil.RxSessionBean beanRX = new oscar.oscarRx.pageUtil.RxSessionBean();
		beanRX.setDemographicNo(sessionBeanRX.getDemographicNo());
		beanRX.setProviderNo(sessionBeanRX.getProviderNo());

		RxDrugListForm frm = (RxDrugListForm) form;
		String script_no = frm.getDrugList();

		String ip = LoggedInInfo.obtainClientIpAddress(request);

		RxPrescriptionData rxData = new RxPrescriptionData();
		List<Prescription> list = rxData.getPrescriptionsByScriptNo(Integer.parseInt(script_no), sessionBeanRX.getDemographicNo());
		RxPrescriptionData.Prescription p = null;
		StringBuilder auditStr = new StringBuilder();
		for (int idx = 0; idx < list.size(); ++idx) {
			p = list.get(idx);
			beanRX.setStashIndex(beanRX.addStashItem(loggedInInfo, p));
			auditStr.append(p.getAuditString() + "\n");
		}

		// save print date/time to prescription table
		if (p != null) {
			p.Print(loggedInInfo);
		}

		String comment = rxData.getScriptComment(script_no);

		request.getSession().setAttribute("tmpBeanRX", beanRX);
		request.setAttribute("rePrint", "true");
		request.setAttribute("comment", comment);

		LogAction.addLog((String) request.getSession().getAttribute("user"), LogConst.REPRINT, LogConst.CON_PRESCRIPTION, script_no, ip, "" + beanRX.getDemographicNo(), auditStr.toString());

		return mapping.findForward("reprint");
	}

	public ActionForward reprint2(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {

		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		checkPrivilege(loggedInInfo, PRIVILEGE_READ);

		oscar.oscarRx.pageUtil.RxSessionBean sessionBeanRX = (oscar.oscarRx.pageUtil.RxSessionBean) request.getSession().getAttribute("RxSessionBean");
		if (sessionBeanRX == null) {
			response.sendRedirect("error.html");
			return null;
		}

		oscar.oscarRx.pageUtil.RxSessionBean beanRX = new oscar.oscarRx.pageUtil.RxSessionBean();
		beanRX.setDemographicNo(sessionBeanRX.getDemographicNo());
		beanRX.setProviderNo(sessionBeanRX.getProviderNo());

		// RxDrugListForm frm = (RxDrugListForm) form;
		String script_no = request.getParameter("scriptNo");
		String ip = LoggedInInfo.obtainClientIpAddress(request);
		RxPrescriptionData rxData = new RxPrescriptionData();
		List<Prescription> list = rxData.getPrescriptionsByScriptNo(Integer.parseInt(script_no), sessionBeanRX.getDemographicNo());
		RxPrescriptionData.Prescription p = null;
		StringBuilder auditStr = new StringBuilder();
		for (int idx = 0; idx < list.size(); ++idx) {
			p = list.get(idx);
			beanRX.setStashIndex(beanRX.addStashItem(loggedInInfo, p));
			auditStr.append(p.getAuditString() + "\n");
		}
		// p("auditStr "+auditStr.toString());
		// save print date/time
		if (p != null) {
			p.Print(loggedInInfo);
		}

		String comment = rxData.getScriptComment(script_no);
		request.getSession().setAttribute("tmpBeanRX", beanRX);
		request.getSession().setAttribute("rePrint", "true");
		request.getSession().setAttribute("comment", comment);
		LogAction.addLog((String) request.getSession().getAttribute("user"), LogConst.REPRINT, LogConst.CON_PRESCRIPTION, script_no, ip, "" + beanRX.getDemographicNo(), auditStr.toString());

		return mapping.findForward(null);
	}

	public ActionForward represcribe(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		checkPrivilege(loggedInInfo, PRIVILEGE_WRITE);
		
		oscar.oscarRx.pageUtil.RxSessionBean beanRX = (oscar.oscarRx.pageUtil.RxSessionBean) request.getSession().getAttribute("RxSessionBean");
		if (beanRX == null) {
			response.sendRedirect("error.html");
			return null;
		}
		RxDrugListForm frm = (RxDrugListForm) form;
		StringBuilder auditStr = new StringBuilder();
		try {
			RxPrescriptionData rxData = new RxPrescriptionData();

			String drugList = frm.getDrugList();

			String[] drugArr = drugList.split(",");

			int drugId;
			int i;

			for (i = 0; i < drugArr.length; i++) {
				try {
					drugId = Integer.parseInt(drugArr[i]);
				} catch (Exception e) {
					logger.error("Unexpected error.", e);
					break;
				}

				// get original drug
				RxPrescriptionData.Prescription oldRx = rxData.getPrescription(drugId);

				// create copy of Prescription
				RxPrescriptionData.Prescription rx = rxData.newPrescription(beanRX.getProviderNo(), beanRX.getDemographicNo(), oldRx);

				beanRX.setStashIndex(beanRX.addStashItem(loggedInInfo, rx));
				auditStr.append(rx.getAuditString() + "\n");

				// allocate space for annotation
				beanRX.addAttributeName(rx.getAtcCode() + "-" + String.valueOf(beanRX.getStashIndex()));
				// p("beanRX.getStashIndex() in represcribe after", "" + beanRX.getStashIndex());
				request.setAttribute("BoxNoFillFirstLoad", "true");
			}
		} catch (Exception e) {
			logger.error("Unexpected error occurred.", e);
		}

		return (mapping.findForward("success"));
	}

	public ActionForward saveReRxDrugIdToStash(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
		MiscUtils.getLogger().debug("================in saveReRxDrugIdToStash  of RxRePrescribeAction.java=================");
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		
		oscar.oscarRx.pageUtil.RxSessionBean bean = (oscar.oscarRx.pageUtil.RxSessionBean) request.getSession().getAttribute("RxSessionBean");
		if (bean == null) {
			response.sendRedirect("error.html");
			return null;
		}
		StringBuilder auditStr = new StringBuilder();

		RxPrescriptionData rxData = new RxPrescriptionData();

		// String strId = (request.getParameter("drugId").split("_"))[1];
		String strId = request.getParameter("drugId");
		try {
			int drugId = Integer.parseInt(strId);
			// get original drug
			RxPrescriptionData.Prescription oldRx = rxData.getPrescription(drugId);
			// create copy of Prescription
			RxPrescriptionData.Prescription rx = rxData.newPrescription(bean.getProviderNo(), bean.getDemographicNo(), oldRx); // set writtendate, rxdate ,enddate=null.
			Long rand = Math.round(Math.random() * 1000000);
			rx.setRandomId(rand);

			request.setAttribute("BoxNoFillFirstLoad", "true");
			String qText = rx.getQuantity();
			MiscUtils.getLogger().debug("qText in represcribe2=" + qText);
			if (qText != null && RxUtil.isStringToNumber(qText)) {
			} else {
				rx.setQuantity(RxUtil.getQuantityFromQuantityText(qText));
				rx.setUnitName(RxUtil.getUnitNameFromQuantityText(qText));
			}
			MiscUtils.getLogger().debug("quantity, unitName represcribe2=" + rx.getQuantity() + "; " + rx.getUnitName());
			// trim Special
			String spec = RxUtil.trimSpecial(rx);
			rx.setSpecial(spec);

			List<RxPrescriptionData.Prescription> listReRx = new ArrayList<Prescription>();
			rx.setDiscontinuedLatest(RxUtil.checkDiscontinuedBefore(rx));
			// add rx to rx list
			if (RxUtil.isRxUniqueInStash(bean, rx)) {
				listReRx.add(rx);
			}
			// save rx to stash
			int rxStashIndex = bean.addStashItem(loggedInInfo, rx);
			bean.setStashIndex(rxStashIndex);

			auditStr.append(rx.getAuditString() + "\n");
			bean.addAttributeName(rx.getAtcCode() + "-" + String.valueOf(bean.getStashIndex()));		
			// p("brandName saved in stash", rx.getBrandName());
			// p("stashIndex becomes", "" + beanRX.getStashIndex());
			
			// RxUtil.printStashContent(beanRX);
		} catch (Exception e) {
			MiscUtils.getLogger().error("Error", e);
		}
		MiscUtils.getLogger().debug("================end saveReRxDrugIdToStash of RxRePrescribeAction.java=================");
		return null;
	}

	public ActionForward represcribe2(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
		MiscUtils.getLogger().debug("================in represcribe2 of RxRePrescribeAction.java=================");
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		checkPrivilege(loggedInInfo, PRIVILEGE_WRITE);
		
		oscar.oscarRx.pageUtil.RxSessionBean beanRX = (oscar.oscarRx.pageUtil.RxSessionBean) request.getSession().getAttribute("RxSessionBean");
		if (beanRX == null) {
			response.sendRedirect("error.html");
			return null;
		}
		StringBuilder auditStr = new StringBuilder();

		RxPrescriptionData rxData = new RxPrescriptionData();

		// String strId = (request.getParameter("drugId").split("_"))[1];
		String strId = request.getParameter("drugId");
		// p("!!!!!!!!!s", strId);
		try {

			int drugId = Integer.parseInt(strId);
			// get original drug
			RxPrescriptionData.Prescription oldRx = rxData.getPrescription(drugId);
			// create copy of Prescription
			RxPrescriptionData.Prescription rx = rxData.newPrescription(beanRX.getProviderNo(), beanRX.getDemographicNo(), oldRx); // set writtendate, rxdate ,enddate=null.
			Long rand = Math.round(Math.random() * 1000000);
			rx.setRandomId(rand);

			request.setAttribute("BoxNoFillFirstLoad", "true");
			String qText = rx.getQuantity();
			MiscUtils.getLogger().debug("qText in represcribe2=" + qText);
			if (qText != null && RxUtil.isStringToNumber(qText)) {
			} else {
				rx.setQuantity(RxUtil.getQuantityFromQuantityText(qText));
				rx.setUnitName(RxUtil.getUnitNameFromQuantityText(qText));
			}
			MiscUtils.getLogger().debug("quantity, unitName represcribe2=" + rx.getQuantity() + "; " + rx.getUnitName());
			// trim Special
			String spec = RxUtil.trimSpecial(rx);
			rx.setSpecial(spec);

			List<RxPrescriptionData.Prescription> listReRx = new ArrayList<Prescription>();
			rx.setDiscontinuedLatest(RxUtil.checkDiscontinuedBefore(rx));
			// add rx to rx list
			if (RxUtil.isRxUniqueInStash(beanRX, rx)) {
				listReRx.add(rx);
			}
			// save rx to stash
			int rxStashIndex = beanRX.addStashItem(loggedInInfo, rx);
			beanRX.setStashIndex(rxStashIndex);

			auditStr.append(rx.getAuditString() + "\n");
			beanRX.addAttributeName(rx.getAtcCode() + "-" + String.valueOf(beanRX.getStashIndex()));	
			// p("brandName saved in stash", rx.getBrandName());
			// p("stashIndex becomes", "" + beanRX.getStashIndex());

			// RxUtil.printStashContent(beanRX);
			request.setAttribute("listRxDrugs", listReRx);
		} catch (Exception e) {
			MiscUtils.getLogger().error("Error", e);
		}

		return (mapping.findForward("represcribe"));
	}

	public ActionForward repcbAllLongTerm(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		checkPrivilege(loggedInInfo, PRIVILEGE_WRITE);
		CaseManagementManager caseManagementManager = SpringUtils.getBean(CaseManagementManager.class);
		
		oscar.oscarRx.pageUtil.RxSessionBean beanRX = (oscar.oscarRx.pageUtil.RxSessionBean) request.getSession().getAttribute("RxSessionBean");
		if (beanRX == null) {
			response.sendRedirect("error.html");
			return null;
		}
		StringBuilder auditStr = new StringBuilder();
		// String idList = request.getParameter("drugIdList");

		Integer demoNo = Integer.parseInt(request.getParameter("demoNo"));
		String strShow = request.getParameter("showall");

		boolean showall = false;
		if (strShow.equalsIgnoreCase("true")) {
			showall = true;
		}
		// get a list of long term meds
		List<Drug> prescriptDrugs;
		if(showall) { prescriptDrugs = caseManagementManager.getPrescriptions(loggedInInfo, demoNo, true);
		} else { prescriptDrugs = caseManagementManager.getCurrentPrescriptions(demoNo); }
		List<Integer> listLongTermMed = new ArrayList<Integer>();
		for (Drug prescriptDrug : prescriptDrugs) {
			// add all long term med drugIds to an array.
			if (prescriptDrug.isLongTerm()) {
				listLongTermMed.add(prescriptDrug.getId());
			}
		}


        oscar.oscarRx.pageUtil.RxSessionBean bean = (oscar.oscarRx.pageUtil.RxSessionBean) request.getSession().getAttribute("RxSessionBean");
        
        List<String> reRxDrugIdList=bean.getReRxDrugIdList();
        
		List<RxPrescriptionData.Prescription> listLongTerm = new ArrayList<Prescription>();
		for (int i = 0; i < listLongTermMed.size(); i++) {
			Long rand = Math.round(Math.random() * 1000000);

			// loop this
			int drugId = listLongTermMed.get(i);
			
            //add drug to re-prescribe drug list
            reRxDrugIdList.add(Integer.toString(drugId));
			
			// get original drug
			RxPrescriptionData rxData = new RxPrescriptionData();
			RxPrescriptionData.Prescription oldRx = rxData.getPrescription(drugId);

			// create copy of Prescription
			RxPrescriptionData.Prescription rx = rxData.newPrescription(beanRX.getProviderNo(), beanRX.getDemographicNo(), oldRx);

			request.setAttribute("BoxNoFillFirstLoad", "true");

			// give rx a random id.
			rx.setRandomId(rand);
			String qText = rx.getQuantity();
			MiscUtils.getLogger().debug("qText in represcribe2=" + qText);
			if (qText != null && RxUtil.isStringToNumber(qText)) {
			} else {
				rx.setQuantity(RxUtil.getQuantityFromQuantityText(qText));
				rx.setUnitName(RxUtil.getUnitNameFromQuantityText(qText));
			}
			MiscUtils.getLogger().debug("quantity, unitName represcribe2=" + rx.getQuantity() + "; " + rx.getUnitName());
			String spec = RxUtil.trimSpecial(rx);
			rx.setSpecial(spec);

			if (RxUtil.isRxUniqueInStash(beanRX, rx)) {
				listLongTerm.add(rx);
			}
			int rxStashIndex = beanRX.addStashItem(loggedInInfo, rx);
			beanRX.setStashIndex(rxStashIndex);
			auditStr.append(rx.getAuditString() + "\n");

			// allocate space for annotation
			beanRX.addAttributeName(rx.getAtcCode() + "-" + String.valueOf(beanRX.getStashIndex()));
		}
		// RxUtil.printStashContent(beanRX);
		request.setAttribute("listRxDrugs", listLongTerm);

		return (mapping.findForward("repcbLongTerm"));
	}

	public ActionForward represcribeInEncounter(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		logger.debug("================in represcribeInEncounter of RxRePrescribeAction.java=================");
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		checkPrivilege(loggedInInfo, PRIVILEGE_WRITE);

		EctSessionBean bean = (EctSessionBean) request.getSession().getAttribute("EctSessionBean");
		CopyOnWriteArrayList<String> reRxDrugList = new CopyOnWriteArrayList<String>();
		oscar.oscarRx.data.RxPrescriptionData prescriptData = new oscar.oscarRx.data.RxPrescriptionData();
		String drugId = request.getParameter("drugId");

		reRxDrugList.add(drugId);

		logger.debug(reRxDrugList);
		CopyOnWriteArrayList<Prescription> listReRxDrug = new CopyOnWriteArrayList<Prescription>();

		Long rand = Math.round(Math.random() * 1000000);
		RxPrescriptionData rxData = new RxPrescriptionData();
		Prescription oldRx = rxData.getPrescription(Integer.parseInt(drugId));
		Prescription rx = rxData.newPrescription(bean.providerNo, Integer.parseInt(bean.demographicNo), oldRx);
		rx.setRandomId(rand);
		String qText = rx.getQuantity();
		logger.debug("qText in represcribe2=" + qText);
		if (qText == null || !RxUtil.isStringToNumber(qText)) {
			rx.setQuantity(RxUtil.getQuantityFromQuantityText(qText));
			rx.setUnitName(RxUtil.getUnitNameFromQuantityText(qText));
		}
		logger.debug("quantity, unitName represcribe2=" + rx.getQuantity() + "; " + rx.getUnitName());
		listReRxDrug.add(rx);
		logger.debug(listReRxDrug);
		String scriptId = saveDrug(rx, request,oldRx.getDrugId());
		request.setAttribute("listRxDrugs", listReRxDrug);
		generateFax(request, scriptId, rx);
		logger.debug("================END represcribeInEncounter of RxRePrescribeAction.java=================");
		return (mapping.findForward("represcribe"));
	}

	private void generateFax(HttpServletRequest request, String scriptId, Prescription rx) {
		EctSessionBean bean = (EctSessionBean) request.getSession().getAttribute("EctSessionBean");
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		RxProviderData.Provider provider = new oscar.oscarRx.data.RxProviderData().getProvider(bean.providerNo);
		Patient patient = RxPatientData.getPatient(loggedInInfo, bean.getDemographicNo());
		RxProviderData.Provider docProvider = new oscar.oscarRx.data.RxProviderData().getProvider(bean.providerNo);
		String docPracNo = docProvider.getPractitionerNo();
		ProSignatureData sig = new ProSignatureData();
		int demographicNo = Integer.parseInt(bean.demographicNo);
		FrmCustomedPDFParameters frmCustomedPDFParameters = new FrmCustomedPDFParameters();
		frmCustomedPDFParameters.set__method("oscarRxFax");
		frmCustomedPDFParameters.set__title("Rx&");
		frmCustomedPDFParameters.setDemographicNo(bean.demographicNo);
		frmCustomedPDFParameters.setScriptId(scriptId);
		frmCustomedPDFParameters.setClinicName(provider.getClinicName() + "\n" + provider.getClinicAddress()
				+ "\n" + provider.getClinicCity() + "  " + provider.getClinicPostal());
		frmCustomedPDFParameters.setClinicFax(provider.getClinicFax());
		frmCustomedPDFParameters.setClinicPhone(provider.getClinicPhone());
		frmCustomedPDFParameters.setPatientPhone(patient.getPhone());
		frmCustomedPDFParameters.setPatientAddress(patient.getAddress());
		frmCustomedPDFParameters.setPatientChartNo(patient.getChartNo());
		frmCustomedPDFParameters.setPatientCityPostal(patient.getPostal());
		frmCustomedPDFParameters.setPatientDOB(RxUtil.DateToString(patient.getDOB(), "MMM d, yyyy"));
		frmCustomedPDFParameters.setPatientHIN(patient.getHin());
		frmCustomedPDFParameters.setPatientName(patient.getFirstName() + " " + patient.getSurname());


		val doctorName = sig.hasSignature(bean.providerNo)
				? sig.getSignature(bean.providerNo)
				: (provider.getFirstName() + " " + provider.getSurname());

		val p = (Properties) request.getSession().getAttribute("providerBean");
		val demographicDao = (DemographicDao)SpringUtils.getBean("demographicDao");
		val demographic = demographicDao.getDemographic(bean.demographicNo);
		val mrp = new oscar.oscarRx.data.RxProviderData().getProvider(demographic.getProviderNo());
		val mrpPracNo = mrp.getPractitionerNo();

		if (docPracNo != null && docPracNo.trim().length() > 0) {
			frmCustomedPDFParameters.setSigDoctorName(doctorName + " (" + docPracNo + ")");
		} else {
			frmCustomedPDFParameters.setSigDoctorName(doctorName);
		}

		if (mrpPracNo != null && mrpPracNo.trim().length() > 0) {
			frmCustomedPDFParameters.setMRP(p.getProperty(demographic.getProviderNo(), "") + " (" + mrpPracNo + ")");
		} else {
			frmCustomedPDFParameters.setMRP(p.getProperty(demographic.getProviderNo(), ""));
		}
		frmCustomedPDFParameters.isReRx = true;

		frmCustomedPDFParameters.setPromoText(OscarProperties.getInstance().getProperty("FORMS_PROMOTEXT"));
		if (frmCustomedPDFParameters.getPromoText() == null) {
			frmCustomedPDFParameters.setPromoText(null);
		}
		frmCustomedPDFParameters.setRxDate(RxUtil.DateToString(RxUtil.Today(), "MMM d, yyyy"));

		boolean showPatientDOB = false;
		//check if user prefer to show dob in print
		WebApplicationContext ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getSession().getServletContext());
		UserPropertyDAO userPropertyDAO = (UserPropertyDAO) ctx.getBean("UserPropertyDAO");
		UserProperty prop = userPropertyDAO.getProp(bean.providerNo, UserProperty.RX_SHOW_PATIENT_DOB);
		if (prop != null && prop.getValue().equalsIgnoreCase("yes")) {
			showPatientDOB = true;
		}
		frmCustomedPDFParameters.setShowPatientDOB(showPatientDOB);

		DemographicManager demographicManager = SpringUtils.getBean(DemographicManager.class);
		DemographicExt demographicExtStatusNum = demographicManager.getDemographicExt(loggedInInfo, demographicNo, "statusNum");
		DemographicExt demographicExtBandName = null;
		DemographicExt demographicExtBandFamily = null;
		DemographicExt demographicExtBandFamilyPosition = null;
		String bandNumber = "";
		String bandName = "";
		String bandFamily = "";
		String bandFamilyPosition = "";
		if (demographicExtStatusNum != null) {
			bandNumber = StringUtils.trimToEmpty(demographicExtStatusNum.getValue());
		}
		if (bandNumber == null) {
			bandNumber = "";
		}
		// if band number is empty try the alternate composite.
		if (bandNumber.isEmpty()) {

			demographicExtBandName = demographicManager.getDemographicExt(loggedInInfo, demographicNo, "fNationCom");
			demographicExtBandFamily = demographicManager.getDemographicExt(loggedInInfo, demographicNo, "fNationFamilyNumber");
			demographicExtBandFamilyPosition = demographicManager.getDemographicExt(loggedInInfo, demographicNo, "fNationFamilyPosition");

			if (demographicExtBandName != null) {
				bandName = StringUtils.trimToEmpty(demographicExtBandName.getValue());
			}

			if (demographicExtBandFamily != null) {
				bandFamily = StringUtils.trimToEmpty(demographicExtBandFamily.getValue());
			}

			if (demographicExtBandFamilyPosition != null) {
				bandFamilyPosition = StringUtils.trimToEmpty(demographicExtBandFamilyPosition.getValue());
			}

			StringBuilder bandNumberString = new StringBuilder();

			if (!bandName.isEmpty()) {
				bandNumberString.append(bandName);
			}

			if (!bandFamily.isEmpty()) {
				bandNumberString.append("-" + bandFamily);
			}

			if (!bandFamilyPosition.isEmpty()) {
				bandNumberString.append("-" + bandFamilyPosition);
			}

			bandNumber = bandNumberString.toString();
		}
		frmCustomedPDFParameters.setBandNumber(bandNumber);
		frmCustomedPDFParameters.setPracNo(provider.getPractitionerNo());

		DemographicPharmacyDao demographicPharmacyDao = SpringUtils.getBean(DemographicPharmacyDao.class);
		PharmacyInfoDao pharmacyInfoDao = SpringUtils.getBean(PharmacyInfoDao.class);
		List<DemographicPharmacy> demographicPharmacies = demographicPharmacyDao.findByDemographicId(demographicNo);
		PharmacyInfo pharmacy = pharmacyInfoDao.getPharmacy(demographicPharmacies.get(0).getPharmacyId());
		frmCustomedPDFParameters.setPharmacyId(pharmacy.getId().toString());
		frmCustomedPDFParameters.setPharmaName(pharmacy.getName());
		frmCustomedPDFParameters.setPharmaAddress1(pharmacy.getAddress());
		frmCustomedPDFParameters.setPharmaAddress2(pharmacy.getCity() + ", " + pharmacy.getProvince() + " " + pharmacy.getPostalCode());
		frmCustomedPDFParameters.setPharmaTel(pharmacy.getPhone1() + ((pharmacy.getPhone2() != null
				&& !pharmacy.getPhone2().isEmpty()) ? "," + pharmacy.getPhone2() : ""));
		frmCustomedPDFParameters.setPharmaFax(pharmacy.getFax());
		frmCustomedPDFParameters.setPharmaEmail(pharmacy.getEmail());
		frmCustomedPDFParameters.setPharmaNote(pharmacy.getNotes());
		frmCustomedPDFParameters.setPharmaShow(false);
		frmCustomedPDFParameters.setRxPageSize("PageSize.A4");

		OscarProperties props = OscarProperties.getInstance();
		UserProperty signatureProperty = userPropertyDAO.getProp(bean.providerNo, UserProperty.PROVIDER_CONSULT_SIGNATURE);
		boolean stampSignature = OscarProperties.getInstance().isPropertyActive("consult_auto_load_signature")
				&& signatureProperty != null && signatureProperty.getValue() != null
				&& !signatureProperty.getValue().trim().isEmpty();
		if (props.getBooleanProperty("rx_electronic_signing", "true")) {
			SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd-yyyy 'at' HH:mm");
			String getFirstName = provider.getFirstName().replace("Dr. ", "");
			String signatureMessage = "Electronically Authorized by "
					+ Encode.forHtmlAttribute(provider.getSurname()) + ", \n" + Encode.forHtmlAttribute(getFirstName)
					+ " (" + provider.getPractitionerNo() + ") at " + sdf.format(new Date());
			frmCustomedPDFParameters.setElectronicSignature(signatureMessage);
		} else {
			String imgFile = "";
			String signatureRequestId = null;
			String imageUrl = null;
			String startimageUrl = null;
			String statusUrl = null;
			if (stampSignature) {
				signatureRequestId = loggedInInfo.getLoggedInProviderNo();
				imageUrl = request.getContextPath() + "/eform/displayImage.do?imagefile=" + signatureProperty.getValue();
				startimageUrl = imageUrl;
				statusUrl = request.getContextPath() + "/PMmodule/ClientManager/check_signature_status.jsp?"
						+ DigitalSignatureUtils.SIGNATURE_REQUEST_ID_KEY + "=" + signatureRequestId;
				String eFormImagePath = OscarProperties.getInstance().getProperty("eform_image");
				eFormImagePath = eFormImagePath.endsWith("/") ? eFormImagePath : eFormImagePath + "/";
				imgFile = eFormImagePath + signatureProperty.getValue();
			} else {
				signatureRequestId = loggedInInfo.getLoggedInProviderNo();
				imageUrl = request.getContextPath() + "/imageRenderingServlet?source="
						+ ImageRenderingServlet.Source.signature_preview.name()
						+ "&" + DigitalSignatureUtils.SIGNATURE_REQUEST_ID_KEY + "=" + signatureRequestId;
				startimageUrl = request.getContextPath() + "/images/1x1.gif";
				statusUrl = request.getContextPath() + "/PMmodule/ClientManager/check_signature_status.jsp?"
						+ DigitalSignatureUtils.SIGNATURE_REQUEST_ID_KEY + "=" + signatureRequestId;
			}

			String signatureImagePath = "";
			PrescriptionDao prescriptionDao = SpringUtils.getBean(PrescriptionDao.class);
			org.oscarehr.common.model.Prescription prescription = prescriptionDao.find(Integer.parseInt(scriptId));
			DrugDao drugDao = SpringUtils.getBean(DrugDao.class);
			List<Drug> drugs = drugDao.findByPrescriptionId(Integer.parseInt(scriptId));
			for (var drug : drugs) {
				if (drug.getWrittenDate() != null) {
					frmCustomedPDFParameters.writtenDate = RxUtil.DateToString(drug.getWrittenDate(), "MMM d, yyyy");
					break;
				}
			}
			if (prescription != null && prescription.getSignatureId() != null) {
				signatureImagePath = request.getContextPath() + "/imageRenderingServlet?source="
						+ ImageRenderingServlet.Source.signature_stored_and_temp.name() + "&digitalSignatureId="
						+ prescription.getSignatureId();
				imgFile = DigitalSignatureUtils.getTempFilePath(prescription.getSignatureId().toString());
			}
			frmCustomedPDFParameters.setImgFile(imgFile);

			Integer storedSignatureId = null;
			storedSignatureId = prescription.getSignatureId();
			if (storedSignatureId != null) {
				signatureRequestId = storedSignatureId.toString();
			} else {
				signatureRequestId = DigitalSignatureUtils.generateSignatureRequestId(loggedInInfo.getLoggedInProviderNo());
			}
			frmCustomedPDFParameters.setPdfId(signatureRequestId.toString());

			String strRx = "";
			StringBuffer strRxNoNewLines = new StringBuffer();

			String fullOutLine = rx.getFullOutLine().replaceAll(";", "\n");

			if (fullOutLine == null || fullOutLine.length() <= 6) {
				Logger.getLogger("preview_jsp").error("drug full outline was null");
				fullOutLine = 
						"<span style=\"color:red;font-size:16;font-weight:bold\">An error occurred, please write a new prescription.</span><br />"
						+ fullOutLine;
			}
			strRx += fullOutLine;
			SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
			boolean rxShowStartDatesPref = systemPreferencesDao.isReadBooleanPreference("rx_show_start_dates");
			if (rx.getRxDate() != null && rxShowStartDatesPref) {
				strRx += "; Start Date: " + RxUtil.DateToString(rx.getRxDate(), "MMMM d, yyyy", request.getLocale()) + ";;";
			}
			boolean rxShowEndDatesPref = systemPreferencesDao.isReadBooleanPreference("rx_show_end_dates");
			if (rxShowEndDatesPref) {
				strRx += "; End Date: " + RxUtil.DateToString(rx.getEndDate(), "MMMM d, yyyy", request.getLocale());
			}
			frmCustomedPDFParameters.setRx(strRx);

			log.debug("frmCustomedPDFParameters:" + frmCustomedPDFParameters);

			ByteArrayOutputStream baosPDF = null;

			FrmCustomedPDFGenerator pdfGenerator = new FrmCustomedPDFGenerator();
			try {
				baosPDF = pdfGenerator.generatePDFDocumentBytes(frmCustomedPDFParameters,
						LoggedInInfo.getLoggedInInfoFromSession(request), request.getLocale());
				// write to file
				String pdfFile = "prescription_" + frmCustomedPDFParameters.getPdfId() + ".pdf";
				String path = OscarProperties.getInstance().getProperty("DOCUMENT_DIR") + "/";
				FileOutputStream fos = new FileOutputStream(path + pdfFile);
				baosPDF.writeTo(fos);
				fos.close();

				// write to file
				String tempPdf = System.getProperty("java.io.tmpdir") + "/prescription_" + frmCustomedPDFParameters.getPdfId() + ".pdf";
				// Copying the fax pdf.
				FileUtils.copyFile(new File(path + pdfFile), new File(tempPdf));

				String txtFile = System.getProperty("java.io.tmpdir") + "/prescription_" + frmCustomedPDFParameters.getPdfId() + ".txt";
				FileWriter fstream = new FileWriter(txtFile);
				BufferedWriter out = new BufferedWriter(fstream);
				logger.info("save fax to :" + txtFile);
				try {
					String faxNo = frmCustomedPDFParameters.getPharmaFax().trim().replaceAll("\\D", "");
					out.write(faxNo);
				} finally {
					if (out != null) {
						out.close();
					}
				}
			} catch (DocumentException | FileNotFoundException de) {
				logger.error("exception in document generateion:" + de);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (baosPDF != null) {
					baosPDF.reset();
				}
			}

		}
	}

	public ActionForward represcribeMultiple(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.debug("================in represcribeMultiple of RxRePrescribeAction.java=================");
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		checkPrivilege(loggedInInfo, PRIVILEGE_WRITE);
		
		oscar.oscarRx.pageUtil.RxSessionBean bean = (oscar.oscarRx.pageUtil.RxSessionBean) request.getSession().getAttribute("RxSessionBean");
		if (bean == null) {
			response.sendRedirect("error.html");
			return null;
		}
		CopyOnWriteArrayList<String> reRxDrugList = new CopyOnWriteArrayList<String>();
		reRxDrugList = bean.getReRxDrugIdList();
		logger.debug(reRxDrugList);
		CopyOnWriteArrayList<Prescription> listReRxDrug = new CopyOnWriteArrayList<Prescription>();
		for (String drugId : reRxDrugList) {
			Long rand = Math.round(Math.random() * 1000000);
			RxPrescriptionData rxData = new RxPrescriptionData();
			Prescription oldRx = rxData.getPrescription(Integer.parseInt(drugId));
			Prescription rx = rxData.newPrescription(bean.getProviderNo(), bean.getDemographicNo(), oldRx);
			rx.setRandomId(rand);
			String qText = rx.getQuantity();
			MiscUtils.getLogger().debug("qText in represcribe2=" + qText);
			logger.debug("qText in represcribe2=" + qText);
			if (qText == null || !RxUtil.isStringToNumber(qText)) {
				rx.setQuantity(RxUtil.getQuantityFromQuantityText(qText));
				rx.setUnitName(RxUtil.getUnitNameFromQuantityText(qText));
			}
			logger.debug("quantity, unitName represcribe2=" + rx.getQuantity() + "; " + rx.getUnitName());
			String spec = RxUtil.trimSpecial(rx);
			rx.setSpecial(spec);
			if (RxUtil.isRxUniqueInStash(bean, rx)) {
				listReRxDrug.add(rx);
			}
			int rxStashIndex = bean.addStashItem(loggedInInfo, rx);
			bean.setStashIndex(rxStashIndex);
			bean.addAttributeName(rx.getAtcCode() + "-" + String.valueOf(bean.getStashIndex()));
		}
		logger.debug(listReRxDrug);
		request.setAttribute("listRxDrugs", listReRxDrug);
		logger.debug("================END represcribeMultiple of RxRePrescribeAction.java=================");
		return (mapping.findForward("represcribe"));
	}

	public void p(String s) {
		logger.debug(s);
	}

	public void p(String s, String s1) {
		logger.debug(s + "=" + s1);
	}

	
	private void checkPrivilege(LoggedInInfo loggedInInfo, String privilege) {
		if (!securityInfoManager.hasPrivilege(loggedInInfo, "_rx", privilege, null)) {
			throw new RuntimeException("missing required security object (_rx)");
		}
	}

	public String saveDrug(Prescription rx, HttpServletRequest request,
			int oldDrugId) throws Exception {

		RxPrescriptionData prescription = new RxPrescriptionData();
		Date today = RxUtil.Today();

		DrugDao drugDao = (DrugDao) SpringUtils.getBean("drugDao");

		int item  = oldDrugId;

		Drug drug = drugDao.find(item);
		if (drug.isArchived()) {
			//incorrect drug id, need to find the latest drug for represcription
			oscar.oscarRx.data.RxPrescriptionData prescriptData = new oscar.oscarRx.data.RxPrescriptionData();
			Prescription[] arr = prescriptData.getUniquePrescriptionsByPatient(rx.getDemographicNo());
			for (Prescription predrug : arr) {
				Drug currentDrug = drugDao.find(predrug.getDrugId());
				if (!currentDrug.isArchived() && nullSafeEquals(currentDrug.getCustomName(), drug.getCustomName())
						&& currentDrug.getTakeMin() == drug.getTakeMin() && currentDrug.getTakeMax() == drug.getTakeMax()
						&& nullSafeEquals(currentDrug.getSpecial(), drug.getSpecial()))	{
					//this is the lastest drug
					currentDrug.setArchived(true);
					currentDrug.setArchivedDate(new Date());
					currentDrug.setArchivedReason(Drug.REPRESCRIBED);
					drugDao.merge(currentDrug);
					oldDrugId = currentDrug.getId();
					item = oldDrugId;
					drug = currentDrug;
					break;
				}
			}
		} else {
			drug.setArchived(true);
			drug.setArchivedDate(new Date());
			drug.setArchivedReason(Drug.REPRESCRIBED);
			drugDao.merge(drug);
		}

		org.oscarehr.common.model.Prescription rx_new = new org.oscarehr.common.model.Prescription();
		rx_new.setProviderNo(rx.getProviderNo());
		rx_new.setDemographicId(rx.getDemographicNo());
		rx_new.setDatePrescribed(today);
		rx_new.setDatePrinted(today);
		rx.setWrittenDate(drug.getWrittenDate());

		PrescriptionDao dao = SpringUtils.getBean(PrescriptionDao.class);
		dao.persist(rx_new);
		String scriptId = rx_new.getId().toString();

		DemographicPharmacyDao demographicPharmacyDao = SpringUtils.getBean(DemographicPharmacyDao.class);
		PharmacyInfoDao pharmacyInfoDao = SpringUtils.getBean(PharmacyInfoDao.class);
		var demographicPharmacies
				= demographicPharmacyDao.findByDemographicId(rx.getDemographicNo());
		if (demographicPharmacies.size() > 0) {
			PharmacyInfo pharmacy = pharmacyInfoDao.getPharmacy(demographicPharmacies.get(0).getPharmacyId());

			PrescriptionFaxDao prescriptionFaxDao = SpringUtils.getBean(PrescriptionFaxDao.class);
			prescriptionFaxDao.persist(new PrescriptionFax(scriptId, pharmacy.getId(), rx.getProviderNo()));
			List<PrescriptionFax> prescriptionFaxes
					= prescriptionFaxDao.findFaxedByPrescriptionIdentifier(rx.getScript_no());

			for (PrescriptionFax prescriptionFax : prescriptionFaxes) {
				prescriptionFax.setRxId(scriptId);
				prescriptionFaxDao.merge(prescriptionFax);
			}

		}

		StringBuilder auditStr = new StringBuilder();

		try {
			rx.Save(scriptId);// new drug id available after this line

			auditStr.append(rx.getAuditString());
			auditStr.append("\n");

		} catch (Exception e) {
			logger.error("Error", e);
		}

		String ip = LoggedInInfo.obtainClientIpAddress(request);;
		request.setAttribute("scriptId", scriptId);

		LogAction.addLog((String) request.getSession().getAttribute("user"), LogConst.REPRESCRIBE,
				LogConst.CON_MEDICATION, "drugid="+item, ip, "" + rx.getDemographicNo(), auditStr.toString());

		LogAction.addLog("-1", LogConst.DISCONTINUE, LogConst.CON_MEDICATION, "drugid="+item, "",
				"" + rx.getDemographicNo(), auditStr.toString());

		LogAction.addLog((String) request.getSession().getAttribute("user"), LogConst.ADD,
				LogConst.CON_PRESCRIPTION, scriptId, ip, "" + rx.getDemographicNo(), auditStr.toString());

		return scriptId;
	}
}
