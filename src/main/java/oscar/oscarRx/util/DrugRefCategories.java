package oscar.oscarRx.util;

public class DrugRefCategories {
    public static final int ANATOMICAL_CLASS = 8;
    public static final int CHEMICAL_CLASS = 9;
    public static final int THERAPEUTIC_CLASS = 10;
    public static final int GENERIC = 11;
    public static final int COMPOSITE_GENERIC = 12;
    public static final int BRANDED_PRODUCT = 13;
    public static final int INGREDIENT = 14;
}
