package oscar;

public class PreferencesDefaultConstants {

  public static final boolean DEFAULT_FALSE_IF_NOT_FOUND = false;
  public static final boolean DEFAULT_TRUE_IF_NOT_FOUND = true;
  public static final boolean ADMIN_USER_BILLING_CONTROL_DEFAULT = false;
}
