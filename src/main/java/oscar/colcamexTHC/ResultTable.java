/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */

package oscar.colcamexTHC;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;


/**
 * @author Dennis Warren
 */
public class ResultTable extends BodyTagSupport {

  private static final long serialVersionUID = 1L;

  private static final Logger logger = MiscUtils.getLogger();

  @Setter private String resultSet;
  @Setter private int columnCount;
  @Setter private String editLink;
  @Setter private String styleId;
  @Setter private String styleClass;
  private ResultSet results;

  @Override
  public int doStartTag() {
    JspWriter out = pageContext.getOut();

    try {
      out.append("<table ");
      if (this.styleId != null) {
        out.append("id=\"" + this.styleId + "\" ");
      }
      if (this.styleClass != null) {
        out.append("class=\"" + this.styleClass + "\"");
      }
      out.append(">");
    } catch (IOException e) {
      logger.error(this.getClass().getName().toString(), e);
    }
    return (EVAL_BODY_INCLUDE);
  }

  public int doAfterBody() {
    JspWriter out = pageContext.getOut();
    // get the column which contains the id values.
    if (this.resultSet != null) {
      this.results = (ResultSet) this.pageContext.getAttribute(this.resultSet);

      try {
        String rowClass = null;
        while (results.next()) {
          rowClass = "even";
          if ((results.getRow() % 2) != 0) {
            rowClass = "odd";
          }
          out.append("<tr class=");
          out.append('"' + rowClass + '"');
          out.append(">");
          for (int i = 0; i < columnCount; i++) {
            out.append("<td>");
            out.append(results.getString(i + 1));
            out.append("</td>");
          }
          //add modify and delete links
          if (this.editLink != null) {
            out.append("<td><a href=");
            out.append(this.editLink);
            //out.append(results.getString(Columns.ID.getDatabase()));
            out.append(" >modify</a></td>");
            out.append("<td><a href=");
            out.append(this.editLink);
            //out.append(results.getString(Columns.ID.getDatabase()));
            out.append(" >delete</a></td>");
            out.append("</tr>\n");
          }
        }
      } catch (IOException e) {
        logger.error(this.getClass().getName().toString(), e);
      } catch (SQLException e) {
        logger.error(this.getClass().getName().toString(), e);
      }
    }
    return (SKIP_BODY);
  }


  public int doEndTag() {
    JspWriter out = pageContext.getOut();

    try {
      out.print("</table>");
    } catch (IOException e) {
      logger.error(this.getClass().getName().toString(), e);
    }
    return (EVAL_PAGE);
  }

}
