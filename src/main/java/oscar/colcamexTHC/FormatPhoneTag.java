package oscar.colcamexTHC;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import lombok.Setter;
import lombok.val;
import lombok.var;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.oscarehr.util.MiscUtils;

/**
 * @author Dennis Warren @ OSCARprn by Treatment www.oscarprn.com
 * @version May 2012
 * <p>
 * A class written to compliment OSCAR functionality.
 */
public class FormatPhoneTag extends BodyTagSupport {

  private static final long serialVersionUID = 1L;
  @Setter private String phoneNumber;
  @Setter private String label;
  private static final String LONG_PHONE = "^(?:\\([2-9]\\d{2}\\)\\ ?|(?:[2-9]\\d{2}\\-))[2-9]\\d{2}\\-\\d{4}$";
  private static final String SHORT_PHONE = "^([0-9]{3})[-. ]?([0-9]{4})$";
  private static final String STRIP_CHARS = "[^\\p{L}\\p{N}]";
  private static final Logger logger = MiscUtils.getLogger();

  public FormatPhoneTag() {
    super();
  }

  private String getAreaCode() {

    val areaCode = oscar.OscarProperties.getInstance().getProperty("phoneprefix");
    if (!areaCode.isEmpty()) {
      return areaCode.trim();
    }
    return " ";
  }

  private boolean isValid(String phoneNumber) {
    var valid = false;
    Matcher matcher = Pattern.compile(LONG_PHONE).matcher(phoneNumber);
    if (matcher.matches()) {
      valid = true;
    }
    return valid;
  }

  private boolean hasAreaCode(String phoneNumber) {

    var valid = true;
    Matcher matcher = Pattern.compile(SHORT_PHONE).matcher(phoneNumber);
    // if it matches then this means that there is not
    // an area code prepended to the phone number.
    if (matcher.matches()) {
      valid = false;
    }
    return valid;
  }


  @Override
  public int doStartTag() {
    JspWriter out = pageContext.getOut();
    var number = "";
    if (this.phoneNumber != null) {
      String[] attribute = this.phoneNumber.split("\\.");
      if (attribute.length > 0) {
        Object bean = pageContext.findAttribute(attribute[0]);
        if (attribute.length > 1) {
          String property = attribute[1];
          try {
            number = (String) PropertyUtils.getProperty(bean, property);
            if (this.label != null) {
              out.append("<span class=\"label\" ");
              out.append("id=\"");
              out.append(this.label);
              out.append("\" ");
              out.append(">");
              out.append(this.label + " ");
              out.append("</span>");
            }

          } catch (IllegalAccessException e) {
            logger.error(this.getClass().getName().toString(), e);
          } catch (InvocationTargetException e) {
            logger.error(this.getClass().getName().toString(), e);
          } catch (NoSuchMethodException e) {
            logger.error(this.getClass().getName().toString(), e);
          } catch (IOException e) {
            logger.error(this.getClass().getName().toString(), e);
          }
        } else {
          number = bean.toString();
        }
      }
    }

    // if not a valid format then do your thing
    if (!isValid(number)) {

      // strip down the number for reformat
      var rawNumber = number.replaceAll(STRIP_CHARS, "");
      // first check for areacode.
      // add it if missing
      if (!hasAreaCode(number)) {
        rawNumber = getAreaCode().replaceAll(STRIP_CHARS, "") + rawNumber;
      }

      // now we have a raw 10 dig phone number to format the way we want.
      if (rawNumber.length() == 10) {
        val areaCode = rawNumber.substring(0, 3);
        val exchange = rawNumber.substring(3, 6);
        val suffix = rawNumber.substring(6, 10);
        try {
          out.append("(");
          out.append(areaCode);
          out.append(") ");
          out.append(exchange);
          out.append("-");
          out.append(suffix);
        } catch (IOException e) {
          logger.error(this.getClass().getName().toString(), e);
        }
      }
    } else {

      try {
        out.append(number);
      } catch (IOException e) {
        logger.error(this.getClass().getName().toString(), e);
      }
    }

    return (EVAL_BODY_INCLUDE);
  }
}
