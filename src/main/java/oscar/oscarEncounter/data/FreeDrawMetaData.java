/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.oscarEncounter.data;

import java.util.Date;

public class FreeDrawMetaData {
  private int id;
  private String providerNo;
  private String name;
  private Date updateTime;
  private String type;
  private String thumbnailId;

  public FreeDrawMetaData(int id, String providerNo, String name, Date updateTime, String type,
      String thumbnailId) {
    this.id = id;
    this.providerNo = providerNo;
    this.name = name;
    this.updateTime = updateTime;
    this.type = type;
    this.thumbnailId = thumbnailId;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getProviderNo() {
    return providerNo;
  }

  public void setProviderNo(String providerNo) {
    this.providerNo = providerNo;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(Date updateTime) {
    this.updateTime = updateTime;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getThumbnailId() {
    return thumbnailId;
  }

  public void setThumbnailId(String thumbnailId) {
    this.thumbnailId = thumbnailId;
  }
}
