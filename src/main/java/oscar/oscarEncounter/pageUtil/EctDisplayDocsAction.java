/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarEncounter.pageUtil;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;

import org.oscarehr.util.SessionConstants;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import oscar.dms.EDoc;
import oscar.dms.EDocUtil;
import oscar.dms.EDocUtil.EDocSort;
import oscar.util.DateUtils;
import oscar.util.StringUtils;

public class EctDisplayDocsAction extends EctDisplayAction {
	private static Logger logger = MiscUtils.getLogger();

	private static final String cmd = "docs";

	public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {
    
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		
    	if (!securityInfoManager.hasPrivilege(loggedInInfo, "_edoc", "r", null)) {
    		return true; // documents link won't show up on new CME screen.
    	} else {
				try {
    		//Group documents by doctype
    		SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
				SystemPreferences preference =
						systemPreferencesDao.findPreferenceByName("echart_show_group_document_by_type");
				boolean groupByType = preference != null && Boolean.parseBoolean(preference.getValue());
    		
    		String omitTypeStr = request.getParameter("omit");
    		String[] omitTypes = new String[0];
    		if (omitTypeStr != null) {
    			omitTypes = omitTypeStr.split(",");
    		}
    		// add for inbox manager
			boolean isConsent = request.getAttribute("consent")!=null?(Boolean) request.getAttribute("consent"):false;
    		boolean inboxflag = oscar.util.plugin.IsPropertiesOn.propertiesOn("inboxmnger");
    		// set lefthand module heading and link
    		String winName = "docs" + bean.demographicNo;
    		String url = "popupPage(500,1115,'" + winName + "', '" + request.getContextPath() + "/dms/documentReport.jsp?" + "function=demographic&doctype=lab&functionid=" + bean.demographicNo + "&curUser=" + bean.providerNo + "')";
    
    		Dao.setLeftHeading(messages.getMessage(request.getLocale(), "oscarEncounter.Index.msgDocuments"));
    		if (isConsent)
			{
				Dao.setLeftHeading(messages.getMessage(request.getLocale(), "oscarEncounter.Index.msgConsentDocuments"));
				url = "popupPage(500,1115,'" + winName + "', '" + request.getContextPath() + "/dms/documentReport.jsp?" + "function=demographic&doctype=consent&functionid="
				+ bean.demographicNo + "&curUser=" + bean.providerNo + "')";
			}
    		if (inboxflag) {
    			url = "popupPage(600,1024,'" + winName + "', '" + request.getContextPath() + "/mod/docmgmtComp/DocList.do?method=list&&demographic_no=" + bean.demographicNo + "');";
    			Dao.setLeftHeading(messages.getMessage("oscarEncounter.Index.inboxManager"));
    		}
    		Dao.setLeftURL(url);
    
    		// set the right hand heading link to call addDocument in index jsp
    		winName = "addDoc" + bean.demographicNo;
    		url = "popupPage(500,1115,'" + winName + "','" + request.getContextPath() + "/dms/documentReport.jsp?" + "function=demographic&doctype=lab&functionid=" + bean.demographicNo + "&curUser=" + bean.providerNo + "&mode=add" + "&parentAjaxId=" + cmd + "');return false;";
    
    		if (inboxflag) {
    			url = "popupPage(300,600,'" + winName + "','" + request.getContextPath() + "/mod/docmgmtComp/FileUpload.do?method=newupload&demographic_no=" + bean.demographicNo + "');return false;";
    		}
    		Dao.setRightURL(url);
    		Dao.setRightHeadingID(cmd); // no menu so set div id to unique id for this action
    
    		StringBuilder javascript = new StringBuilder("<script type=\"text/javascript\">");
    		String js = "";
    		ArrayList<EDoc> docList = EDocUtil.listDocs(loggedInInfo, "demographic", bean.demographicNo, null, EDocUtil.PRIVATE, EDocSort.OBSERVATIONDATE, "active");
    		if (isConsent)
			{
				docList = EDocUtil.listDocs(loggedInInfo, "demographic", bean.demographicNo, "CONSENT / LEGAL", EDocUtil.PRIVATE, EDocSort.OBSERVATIONDATE, "active");
			}
    		String dbFormat = "yyyy-MM-dd";
    		String serviceDateStr = "";
    		String key;
    		String title;
    		int hash;
    		String BGCOLOUR = request.getParameter("hC");
    		Date date;

            String user = (String) request.getSession().getAttribute("user");
            HttpSession session = request.getSession();
            boolean enhancedEnabled = "E".equals(session.getAttribute(SessionConstants.LOGIN_TYPE));
    
    		// --- add remote documents ---
    		
    		if (loggedInInfo.getCurrentFacility().isIntegratorEnabled()) {
    			try {
    				ArrayList<EDoc> remoteDocuments = EDocUtil.getRemoteDocuments(loggedInInfo, Integer.parseInt(bean.demographicNo));
    				docList.addAll(remoteDocuments);
    			} catch (Exception e) {
    				logger.error("error getting remote documents", e);
    			}
    		}
    
    		boolean isURLjavaScript;
    		
    		String docType = "";
    		for (int i = 0; i < docList.size(); i++) {
    			isURLjavaScript = false;
    			EDoc curDoc = docList.get(i);
    			
    			if (groupByType && !docType.equals(curDoc.getType())) {
    				docType = curDoc.getType();
    				key = StringEscapeUtils.escapeJavaScript(docType);
    				NavBarDisplayDAO.Item item = NavBarDisplayDAO.Item();
    				url = "popupPage(500,1115,'" + winName + "','" + request.getContextPath() 
    						+ "/dms/documentReport.jsp?function=demographic&doctype=lab&defaultDocType=" 
    						+ docType + "&functionid=" + bean.demographicNo + "&curUser=" + bean.providerNo 
    						+ "&mode=add&parentAjaxId=" + cmd + "');return false;";
    				
    				item.setBgColour("#90a6d1");
    				item.setColour("#ffffff");
    				if (StringUtils.isNullOrEmpty(docType)) {
    					item.setLinkTitle("&nbsp;");
    				}
    				item.setTitle(docType + "  +");
    				item.setURL(url);
    				item.setURLJavaScript(true);
    				
    				js = "itemColours['" + key + "'] = 'EE0000'; autoCompleted['" + key + "'] = \"" + url 
    						+ "\"; autoCompList.push('" + key + "');";
    				javascript.append(js);
    				Dao.addItem(item);
    			}
    			
    			String dispFilename = org.apache.commons.lang.StringUtils.trimToEmpty(curDoc.getFileName());
    			String dispStatus = String.valueOf(curDoc.getStatus());
    
    			boolean skip = false;
    			for (int x = 0; x < omitTypes.length; x++) {
    				if (omitTypes[x].equals(curDoc.getType())) {
    					skip = true;
    					break;
    				}
    			}
    			if (skip) continue;
    
    			if (dispStatus.equals("A")) dispStatus = "active";
    			else if (dispStatus.equals("H")) dispStatus = "html";
    
    			String dispDocNo = curDoc.getDocId();
    			title = StringUtils.maxLenString(curDoc.getDescription(), MAX_LEN_TITLE, CROP_LEN_TITLE, ELLIPSES);
    
    			if (EDocUtil.getDocUrgentFlag(dispDocNo)) title = StringUtils.maxLenString("!" + curDoc.getDescription(), MAX_LEN_TITLE, CROP_LEN_TITLE, ELLIPSES);
    
    			DateFormat formatter = new SimpleDateFormat(dbFormat);
    			String dateStr = curDoc.getObservationDate();
    			NavBarDisplayDAO.Item item = NavBarDisplayDAO.Item();
    			try {
    				date = formatter.parse(dateStr);
    				serviceDateStr = DateUtils.formatDate(date, request.getLocale());
    			} catch (ParseException ex) {
    				MiscUtils.getLogger().debug("EctDisplayDocsAction: Error creating date " + ex.getMessage());
    				serviceDateStr = "Error";
    				date = null;
    			}
    
    			if (!groupByType) {
    				item.setDate(date);
    			}
    			hash = Math.abs(winName.hashCode());
    			
    			if (inboxflag) {
    				String path = oscar.util.plugin.IsPropertiesOn.getProperty("DOCUMENT_DIR");
    				url = "popupPage(700,800,'" + hash + "', '" + request.getContextPath() + "/mod/docmgmtComp/FillARForm.do?method=showInboxDocDetails&path=" + path + "&demoNo=" + bean.demographicNo + "&name=" + StringEscapeUtils.escapeHtml(dispFilename) + "'); return false;";
    				isURLjavaScript = true;
    			} else if (enhancedEnabled) {
    			    url = "popupPage(900, 1420, 'Document " + dispDocNo + "', '/"+ OscarProperties.getKaiemrDeployedContext() +"/#/document/?providerNo=" + user + "&documentNo=" + dispDocNo + "&view=eChart', 10); return false;";
    			    isURLjavaScript = true;
                }
    			else if( curDoc.getRemoteFacilityId()==null && curDoc.isPDF() ) {
    				url = "popupPage(900, 1680,'" + hash + "','" + request.getContextPath() + "/dms/MultiPageDocDisplay.jsp?segmentID=" + dispDocNo + "&providerNo=" + user + "&searchProviderNo=" + user + "&status=A'); return false;";
    				isURLjavaScript = true;
    			}
    			else if (curDoc.isText() || curDoc.isImage()) {
    				url = "popupPage(700,800,'" + hash + "', '" +  request.getContextPath() + "/dms/ManageDocument.do?method=display&doc_no=" + dispDocNo + "&providerNo=" + user + (curDoc.getRemoteFacilityId()!=null?"&remoteFacilityId="+curDoc.getRemoteFacilityId():"") + "'); return false;";
    			}
    			else {
					url = "window.location.href = '"+request.getContextPath() + "/dms/ManageDocument.do?method=display&doc_no=" + dispDocNo + "&demoNo=" + bean.demographicNo + "&providerNo=" + user + (curDoc.getRemoteFacilityId()!=null?"&remoteFacilityId="+curDoc.getRemoteFacilityId():"") + "';return false;";
				}
    			
    			item.setLinkTitle(title + serviceDateStr);
    			if (groupByType) {
    				item.setTitle(title + "  " + serviceDateStr);
    			} else {
    				item.setTitle(title);
    			}
    			key = StringUtils.maxLenString(curDoc.getDescription(), MAX_LEN_KEY, CROP_LEN_KEY, ELLIPSES) + "(" + serviceDateStr + ")";
    			key = StringEscapeUtils.escapeJavaScript(key);
    
    			if (inboxflag) {
    				if (!EDocUtil.getDocReviewFlag(dispDocNo)) item.setColour("FF0000");
    			}
    			js = "itemColours['" + key + "'] = '" + BGCOLOUR + "'; autoCompleted['" + key + "'] = \"" + url + "\"; autoCompList.push('" + key + "');";
    			javascript.append(js);				
    			item.setURL(url);
    			item.setURLJavaScript(true);
    			
    			if(curDoc.isAbnormal()){
					item.setColour("red");
				}
    			
    			Dao.addItem(item);
    
    		}
    		javascript.append("</script>");
    
    		Dao.setJavaScript(javascript.toString());
			} catch (Exception e) {
			logger.error("OSCARPRO-5468: Error in EctDisplayDocsAction", e);
			return false;
		}
    		return true;
    	}
    }

	public String getCmd() {
		return cmd;
	}
}
