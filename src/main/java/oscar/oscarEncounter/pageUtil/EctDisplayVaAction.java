package oscar.oscarEncounter.pageUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;
import org.oscarehr.common.dao.MeasurementDao;
import org.oscarehr.common.dao.OscarAppointmentDao;
import org.oscarehr.common.model.Appointment;
import org.oscarehr.common.model.Measurement;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

public class EctDisplayVaAction extends EctDisplayAction{

	private static Logger logger = MiscUtils.getLogger();
	
	private String cmd = "VA";
	
	@SuppressWarnings("deprecation")
	public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {
		try{
			//set lefthand module heading and link
//			String winName = "freeDrawing" + bean.demographicNo;
//	        String url = "popupPage(500,950,'" + winName + "', '" + request.getContextPath() + "/casemgmt/freeDrawList.jsp?demographic_no="+bean.demographicNo+"&provNo="+bean.providerNo+"&appointment="+bean.appointmentNo+"&parentAjaxId=freeDrawing2')";
	        Dao.setLeftHeading(messages.getMessage(request.getLocale(), "global.VaAndIop"));
	        Dao.setLeftURL("javascript:void(0)");
	        
	        //set the right hand heading link
//	        url = "popupPage(500,950,'" + winName + "', '" + request.getContextPath() + "/casemgmt/freeDrawPage2.jsp?demographic_no="+bean.demographicNo+"&provNo="+bean.providerNo+"&appointment="+bean.appointmentNo+"&parentAjaxId=freeDrawing2'); return false;";
	        Dao.setRightURL("javascript:void(0)");        
	        Dao.setRightHeadingID(cmd);  //no menu so set div id to unique id for this action
	        
	        OscarAppointmentDao appointmentDao = (OscarAppointmentDao)SpringUtils.getBean("oscarAppointmentDao");
	        MeasurementDao measurementsDao = SpringUtils.getBean(MeasurementDao.class);
	        
	        List<Appointment> appList = appointmentDao.getAppointmentHistory(Integer.parseInt(bean.demographicNo));
	        List<Map<String, String>> vm_data = new ArrayList<Map<String, String>>();
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        for(Appointment appt:appList) {
	        	Map<String, String> vm_map = new HashMap<String, String>();
	        	Measurement m = null;
	        	//VM
	        	m = measurementsDao.findLatestByAppointmentNoAndType(appt.getId(),"v_rdsc");
	        	if(m != null){
	        		vm_map.put("vm_rdsc", m.getDataField());
	        	}
	        	m = measurementsDao.findLatestByAppointmentNoAndType(appt.getId(),"v_rdcc");
	        	if(m != null){
	        		vm_map.put("vm_rdcc", m.getDataField());
	        	}
	        	m = measurementsDao.findLatestByAppointmentNoAndType(appt.getId(),"v_ldsc");
	        	if(m != null){
	        		vm_map.put("vm_ldsc", m.getDataField());
	        	}
	        	m = measurementsDao.findLatestByAppointmentNoAndType(appt.getId(),"v_ldcc");
	        	if(m != null){
	        		vm_map.put("vm_ldcc", m.getDataField());
	        	}
	        	
	        	if(!vm_map.isEmpty()){
	        		vm_map.put("date", sdf.format(appt.getAppointmentDate()));
	        		
	        		vm_data.add(vm_map);
	        	}
	        }
	        
	        if(vm_data.size() > 0){
	        	for(int i = 0;i < vm_data.size();i ++){
	        		Map<String, String> map = vm_data.get(i);
	        		
	        		if(i == 0){
	        			String vm_title = "<div style='width: 100%;text-align: center'>";
	        			vm_title += "<span style='width: 2%;display: inline-block'>[VA]</span>";
	        			vm_title += "<span style='width: 39%;display: inline-block'>OD</span>";
	        			vm_title += "<span style='width: 39%;display: inline-block'>OS</span>";
	        			vm_title += "<span style='width: 20%;display: inline-block'>DATE</span>";
	        			vm_title += "</div>";
	        			NavBarDisplayDAO.Item item = NavBarDisplayDAO.Item();
	        			item.setTitle(vm_title);
	        			item.setURL("return false;");
	        			item.setNeedEncode(false);
		        		Dao.addItem(item);
		        		
		        		String vm_title2 = "<div style='width: 100%;text-align: center'>";
	        			vm_title2 += "<span style='width: 2%;display: inline-block'></span>";
	        			vm_title2 += "<span style='width: 19.5%;display: inline-block'>sc</span>";
	        			vm_title2 += "<span style='width: 19.5%;display: inline-block'>cc</span>";
	        			vm_title2 += "<span style='width: 19.5%;display: inline-block'>sc</span>";
	        			vm_title2 += "<span style='width: 19.5%;display: inline-block'>cc</span>";
	        			vm_title2 += "<span style='width: 20%;display: inline-block'></span>";
	        			vm_title += "</div>";
	        			NavBarDisplayDAO.Item item2 = NavBarDisplayDAO.Item();
	        			item2.setTitle(vm_title2);
	        			item2.setURL("return false;");
	        			item2.setNeedEncode(false);
		        		Dao.addItem(item2);
		        		
//		        		String vm_line = "<div style='background-color:#b0e2ff;height: 1px'></div>";
//		        		NavBarDisplayDAO.Item item_line = NavBarDisplayDAO.Item();
//		        		item_line.setTitle(vm_line);
//		        		Dao.addItem(item_line);
	        		}	
		        		
        			String vm = "<div style='width: 100%;text-align: center'>"; 
        			vm += "<span style='width: 2%;display: inline-block'></span>";
        			vm += "<span style='width: 19.5%;display: inline-block'>" + (map.get("vm_rdsc") == null ? "":map.get("vm_rdsc")) + "</span>";
        			vm += "<span style='width: 19.5%;display: inline-block'>" + (map.get("vm_rdcc") == null ? "":map.get("vm_rdcc")) + "</span>";
        			vm += "<span style='width: 19.5%;display: inline-block'>" + (map.get("vm_ldsc") == null ? "":map.get("vm_ldsc")) + "</span>";
        			vm += "<span style='width: 19.5%;display: inline-block'>" + (map.get("vm_ldcc") == null ? "":map.get("vm_ldcc")) + "</span>";
        			vm += "<span style='width: 20%;display: inline-block'>" + map.get("date") + "</span>";
        			vm += "</div>";
        			
        			NavBarDisplayDAO.Item item = NavBarDisplayDAO.Item();
        			item.setTitle(vm);
        			item.setURL("return false;");
        			item.setNeedEncode(false);
        			Dao.addItem(item);
        			
        			if(i == vm_data.size() - 1){
        				String vm_line = "<div style='background-color:#b0e2ff;height: 1px'></div>";
		        		NavBarDisplayDAO.Item item_line = NavBarDisplayDAO.Item();
		        		item_line.setTitle(vm_line);
		        		item_line.setURL("return false;");
		        		item_line.setNeedEncode(false);
		        		Dao.addItem(item_line);
        			}
	        	}
	        }
	        
			return true;
		}catch (Exception e){
			logger.error("OSCARPRO-5468 - Error in EctDisplayVaAction: ", e);
			return false;
		}
	}
	
	public String getCmd() {
		return cmd;
	}
}
