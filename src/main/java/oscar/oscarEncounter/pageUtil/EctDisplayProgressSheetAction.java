/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarEncounter.pageUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import oscar.OscarProperties;
import oscar.util.DateUtils;

public class EctDisplayProgressSheetAction extends EctDisplayAction {
	private static Logger logger = MiscUtils.getLogger();

	private static final String cmd = "progressSheet";

	public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {
    
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		
    	if (!securityInfoManager.hasPrivilege(loggedInInfo, "_edoc", "r", null)) {
    		return true; // documents link won't show up on new CME screen.
    	} else if (!OscarProperties.getInstance().getBooleanProperty("echart_show_progress_sheet", "true")) {
    		return true;
    	} else {
				try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    

			String providerId = (String) request.getSession().getAttribute("user");
    		String url = "return false;";
    		String progressSheetHost = request.getRequestURL().substring(0, request.getRequestURL().indexOf(request.getContextPath())); // assume progress sheet is on same system
    		
    		Dao.setLeftHeading(messages.getMessage(request.getLocale(), "oscarEncounter.LeftNavBar.ProgressSheet"));

    		String winName = "addProgressSheets" + bean.demographicNo;
    		Dao.setRightHeadingID("blank"); // no menu so set div id to unique id for this action
    
    		// Add menu dropdown
    		String menuId = "ProgressSheet";
    		Dao.setRightHeadingID(menuId);
    		Dao.setRightURL("return !showMenu('" + menuId + "', event);");
    		
    		// Add list progress sheets for demographic link
    		String startAndEndDateParamJs = "&startDate=' + document.getElementById('progressSheetStartDate').value + '&endDate=' + document.getElementById('progressSheetEndDate').value";
    		url = "popupPage(700,1000, '" + winName + "','" + progressSheetHost + "/progresssheet/listForDemographic?demographicNo=" + bean.getDemographicNo() + startAndEndDateParamJs + ")"; 
    		Dao.addPopUpUrl(url);
    		Dao.addPopUpText(messages.getMessage("oscarEncounter.LeftNavBar.ProgressSheetPrint"));
    		// Add list billing sheets for demographic link
    		url = "popupPage(700,1000, '" + winName + "','" + progressSheetHost + "/progresssheet/billing/listForDemographic?demographicNo=" + bean.getDemographicNo() + startAndEndDateParamJs + ")";
    		Dao.addPopUpUrl(url);
    		Dao.addPopUpText(messages.getMessage("oscarEncounter.LeftNavBar.BillingSheetPrint"));
    		
    		// default dates to now and a year ago
    		Calendar calendar = Calendar.getInstance();
    		calendar.add(Calendar.YEAR, -1);
    		Dao.addPopUpUrl("return false;");
    		Dao.addPopUpText("Start Date: <input id=\"progressSheetStartDate\" type=\"date\" value=\"" + sdf.format(calendar.getTime()) + "\">");
    		// default dates to now and a year ago
    		calendar.add(Calendar.YEAR, 1);
    		Dao.addPopUpUrl("return false;");
    		Dao.addPopUpText("End Date: <input id=\"progressSheetEndDate\" type=\"date\" value=\"" + sdf.format(calendar.getTime()) + "\">");
    		
    		StringBuilder javascript = new StringBuilder("<script type=\"text/javascript\">");
    		String serviceDateStr;
    		int hash;
    		Date date;
    		
    		String serviceUrl = progressSheetHost + "/progresssheet/demographic/getProgressSheets/?demographicId=" + bean.getDemographicNo();
    		if (OscarProperties.getInstance().getProperty("progress_sheet_url") != null) {
    			serviceUrl = OscarProperties.getInstance().getProperty("progress_sheet_url") + "demographic/getProgressSheets/?demographicId=" + bean.getDemographicNo();
    		}
    		JSONArray progressSheetArray = new JSONArray();
			try {

				HttpClient httpClient = new DefaultHttpClient();
				httpClient.getParams().setParameter("http.connection.timeout", 10000);
				HttpGet httpGet = new HttpGet(serviceUrl);
				HttpResponse httpResponse = httpClient.execute(httpGet);
				int statusCode = httpResponse.getStatusLine().getStatusCode();
				if (statusCode == 200) {
					progressSheetArray = JSONArray.fromObject(EntityUtils.toString(httpResponse.getEntity()));
				}
				else if (statusCode == 404) {
					throw new Exception("Service " + serviceUrl + " not found.");
				}
				logger.debug("StatusCode:" + statusCode);
			} catch (Exception ex) {
				MiscUtils.getLogger().debug("EctDisplayProgressSheetAction: Error connecting to progresssheet: " + ex.getMessage());
				NavBarDisplayDAO.Item item = NavBarDisplayDAO.Item();
				item.setTitle("404 error");
				item.setLinkTitle("Service " + serviceUrl + " not found");
				Dao.addItem(item);
			}
			
    		for (int i = 0; i < progressSheetArray.size(); i++) {
    			JSONObject progressSheet = (JSONObject) progressSheetArray.get(i);
    			NavBarDisplayDAO.Item item = NavBarDisplayDAO.Item();
    			
        		String title = "Progress Sheet";
    			String demographicId = progressSheet.getString("demographicId");
    			String appointmentId = progressSheet.getString("appointmentId");
    			String encounterDate = progressSheet.getString("encounterDate");
    			
    			try {
    				date = sdf.parse(encounterDate);
    				serviceDateStr = DateUtils.formatDate(date, request.getLocale());
    			} catch (ParseException ex) {
    				MiscUtils.getLogger().debug("EctDisplayProgressSheetAction: Error creating date " + ex.getMessage());
    				serviceDateStr = "Error";
    				date = null;
    			}
    
    			item.setDate(date);
    			hash = Math.abs(winName.hashCode());
    			

    			url = "popupPage(700,800,'" + hash + "', '/progresssheet/?demographicId=" + demographicId + "&appointmentId=" + appointmentId + "&providerId=" + providerId+ "'); return false;";
    			
    			item.setLinkTitle(title + " " + serviceDateStr);// + "\nLast updated:" + "");
    			item.setTitle(title);			
    			item.setURL(url);
    			
    			Dao.addItem(item);
    		}
    		javascript.append("</script>");
    
    		Dao.setJavaScript(javascript.toString());
			} catch (Exception e) {
			logger.error("OSCARPRO-5468 - Error in EctDisplayProgressSheetAction: ", e);
			return false;
		}
    		return true;
    	}
    }

	public String getCmd() {
		return cmd;
	}
}
