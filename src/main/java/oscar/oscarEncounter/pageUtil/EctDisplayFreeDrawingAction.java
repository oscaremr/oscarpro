/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.oscarEncounter.pageUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;
import org.oscarehr.common.dao.FreeDrawDataDao;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.oscarEncounter.data.FreeDrawMetaData;

public class EctDisplayFreeDrawingAction extends EctDisplayAction {
  private static Logger logger = MiscUtils.getLogger();
  private String cmd = "freeDrawing";

  @SuppressWarnings("deprecation")
  public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao,
      MessageResources messages) {

    String appointmentNo = bean.appointmentNo;
    if (appointmentNo == null) {
      appointmentNo = request.getParameter("appointmentNo");
    }

    try {
      // set lefthand module heading and link
      String winName = "freeDrawing" + bean.demographicNo;
      String url = "popupPage(500,950,'" + winName + "', '" + request.getContextPath()
          + "/casemgmt/freeDrawList.jsp?demographic_no=" + bean.demographicNo + "&provNo="
          + bean.providerNo + "&appointment=" + appointmentNo + "&parentAjaxId=freeDrawing')";
      Dao.setLeftHeading(messages.getMessage(request.getLocale(), "global.freeDrawing"));
      Dao.setLeftURL(url);

      // set the right hand heading link
      url = "popupPage(500,950,'" + winName + "', '" + request.getContextPath()
          + "/casemgmt/freeDrawPage.jsp?demographic_no=" + bean.demographicNo + "&provNo="
          + bean.providerNo + "&appointment=" + appointmentNo
          + "&parentAjaxId=freeDrawing'); return false;";
      Dao.setRightURL(url);
      Dao.setRightHeadingID(cmd); // no menu so set div id to unique id for this action

      FreeDrawDataDao freeDao = (FreeDrawDataDao) SpringUtils.getBean(FreeDrawDataDao.class);
      List<FreeDrawMetaData> dataList =
          freeDao.getDrawingListByDemoNo(Integer.parseInt(bean.demographicNo));

      DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
      int num = 3;
      if (dataList.size() <= 3) {
        num = dataList.size();
      }

      for (int i = 0; i < num; i++) {
        NavBarDisplayDAO.Item item = NavBarDisplayDAO.Item();

        url = "popupPage(700,960,'" + winName + "','" + request.getContextPath()
            + "/casemgmt/showDrawPage.jsp?id=" + dataList.get(i).getId() + "'); return false;";
        String date = formatter.format(dataList.get(i).getUpdateTime());
        item.setLinkTitle("Free Drawing");
        item.setTitle("<img style='height: 100%;width: 100px;' name='thumbnailFREEDRAW' src='"
            + request.getContextPath()
            + "/imageRenderingServlet?source=freedraw_thumbnail_stored&thumbnailId="
            + dataList.get(i).getThumbnailId() + "&demoNo=" + bean.demographicNo
            + "' id='thumbnailFREEDRAW" + dataList.get(i).getId() + "' />");
        item.setURL(url);
        item.setDate(formatter.parse(date));
        Dao.addItem(item);
      }

      StringBuilder javascript = new StringBuilder("<script type=\"text/javascript\">");
      javascript.append("jQuery(\"img[name='thumbnailFREEDRAW']\").each(function () {"
          + "var liStyle = jQuery(this).parent().parent().parent().attr('style');"
          + "liStyle += 'height: 3.2em;';"
          + "jQuery(this).parent().parent().parent().attr('style', liStyle);" +

          "var spanStyle = jQuery(this).parent().parent().attr('style');"
          + "spanStyle += 'height: 3.2em;';"
          + "jQuery(this).parent().parent().attr('style', spanStyle);" +

          "var nextSpanStyle = jQuery(this).parent().parent().next().attr('style');"
          + "nextSpanStyle += 'margin-top: 13px;';"
          + "jQuery(this).parent().parent().next().attr('style', nextSpanStyle);" + "})");
      javascript.append("</script>");
      Dao.setJavaScript(javascript.toString());
      return true;
    } catch (Exception e) {
      logger.error("OSCARPRO-5468 - Error in EctDisplayFreeDrawingAction: ", e);
      return false;
    }

  }

  public String getCmd() {
    return cmd;
  }
}
