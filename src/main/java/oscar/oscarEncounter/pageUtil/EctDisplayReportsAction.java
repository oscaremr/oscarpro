package oscar.oscarEncounter.pageUtil;

import org.apache.log4j.Logger;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.util.MessageResources;
import oscar.oscarReport.reportByTemplate.ReportFactory;
import oscar.oscarReport.reportByTemplate.ReportManager;
import oscar.oscarReport.reportByTemplate.ReportObjectGeneric;

import java.util.ArrayList;

public class EctDisplayReportsAction extends EctDisplayAction {
    private static Logger logger = MiscUtils.getLogger();
    private String cmd = "reports";

    public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {
        LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
        
        Dao.setLeftHeading(messages.getMessage(request.getLocale(), "oscarEncounter.NavBar.Reports"));
        Dao.setRightHeadingID(cmd); // no menu so set div id to unique id for this action
        
        // Check for privileges
        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_newCasemgmt.reports", "r", null)) {
            return true;
        }
        try {
        
        // Check to see if there is anything to display and if not there is no point displaying the E-Chart modules
        ReportManager reportManager = new ReportManager();
        ArrayList<ReportObjectGeneric> reportList = reportManager.getReportTemplatesByType(ReportFactory.EChart);
        //But at least display the title label there
        if (reportList.size() == 0) {
            return true;
        }
        
        String winName = "reports" + bean.demographicNo;
        String url = "popupPage(800,800,'" + Math.abs(winName.hashCode()) + "', '../oscarReport/reportByTemplate/homePage.jsp'); return false;";
        
        Dao.setLeftURL(url);
        
        Dao.setRightURL(url);
        
        String demographicNo = bean.getDemographicNo();
        String providerNo = loggedInInfo.getLoggedInProviderNo();
        
        for (ReportObjectGeneric report : reportList) {
            NavBarDisplayDAO.Item item = NavBarDisplayDAO.Item();
            item.setTitle(report.getTitle());
            String templateId = report.getTemplateId();
            int hash = Math.abs(winName.hashCode());

            url = "popupPage(800,800,'" + hash + "', '../oscarReport/reportByTemplate/reportConfiguration.jsp?templateid=" + templateId + "&demographicNo=" + demographicNo + "&providerNo=" + providerNo+ "&autoRun=true'); return false;";
            item.setURL(url);
            Dao.addItem(item);
        }

        // --- sort all results ---
        Dao.sortItems(NavBarDisplayDAO.DATESORT_ASC);
    } catch (Exception e) {
        logger.error("OSCARPRO-5468 - Error in EctDisplayReportsAction: ", e);
        return false;
    }
        return true;
    }

    public String getCmd() {
        return cmd;
    }
}
