/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarEncounter.pageUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;
import org.oscarehr.common.dao.EncounterFormDao;
import org.oscarehr.common.dao.OscarAppointmentDao;
import org.oscarehr.common.dao.SiteDao;
import org.oscarehr.common.model.Appointment;
import org.oscarehr.common.model.Site;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

import oscar.OscarProperties;
import oscar.util.StringUtils;

/**
 * retrieves info to display Disease entries for demographic
 */
@Slf4j
public class EctDisplayParcsAction extends EctDisplayAction {

	private static Logger logger = MiscUtils.getLogger();	
	
	private String cmd = "parcs";
	
	public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {
		
		Dao.setLeftHeading(messages.getMessage(request.getLocale(), "global.parcs"));
		Dao.setRightHeadingID(cmd);
		
		LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
		
		if (!securityInfoManager.hasPrivilege(loggedInInfo, "_parcs", "r", null)) {
			return true; // PARCS link won't show up on new CME screen.
    } else {
      try {
			if (org.oscarehr.common.IsPropertiesOn.isMultisitesEnable()) {
				SiteDao siteDao=(SiteDao)SpringUtils.getBean("siteDao");
				List<Site> sites = siteDao.getAllActiveSites();
				OscarAppointmentDao apptDao = SpringUtils.getBean(OscarAppointmentDao.class);
				
				String appointmentNo = request.getParameter("appointmentNo");
				
				Integer default_site_id = null;
				
				if(appointmentNo != null) {
					Appointment appt = apptDao.find(Integer.valueOf(appointmentNo));
					if(appt != null) {
						String site_name = appt.getLocation();					
						if(!org.apache.commons.lang.StringUtils.isBlank(site_name)) {
							Site st = siteDao.findByName(site_name);
							if(st != null) {
								default_site_id = st.getId();
							}
						} 
					}
				}
				
				if(default_site_id == null) {
					if(sites.size() > 0) {
						default_site_id = sites.get(0).getId();
					}
				}
				// set lefthand module heading and link
				String winName = "PARCS" + bean.demographicNo;
				String url = "popupPage(580,900,'" + winName + "','" + request.getContextPath() + "/parcs/ExternalController/LimitedExternalControllerDemo.jsp?site=" + default_site_id + "&demographicNo=" + bean.demographicNo + "')";
				Dao.setLeftHeading(messages.getMessage(request.getLocale(), "global.parcs"));
				Dao.setLeftURL(url);

				// set righthand link to same as left so we have visual consistency with other modules
				url += "; return false;";
				Dao.setRightURL(url);
				Dao.setRightHeadingID(cmd); // no menu so set div id to unique id for this action
				
				//Add multiple site drop down list				
				StringBuilder javascript = new StringBuilder("<script type=\"text/javascript\">");	
				String BGCOLOUR = request.getParameter("hC");
				for(Site site : sites) {
					Integer site_id = site.getSiteId();
					winName = winName.concat(site.getShortName());
										
					url = "popupPage(580,900,'" + winName + "','" + request.getContextPath() + "/parcs/ExternalController/LimitedExternalControllerDemo.jsp?site=" + site.getId().toString() + "&demographicNo=" + bean.demographicNo + "')";
					
					Dao.addPopUpUrl(url);
					String key = StringUtils.maxLenString(site.getName(), MAX_LEN_KEY, CROP_LEN_KEY, ELLIPSES) + " (new)";
					Dao.addPopUpText(site.getName());
					key = StringEscapeUtils.escapeJavaScript(key);

					// auto completion arrays and colour code are set
					String js = "itemColours['" + key + "'] = '" + BGCOLOUR + "'; autoCompList.push('" + key + "'); autoCompleted['" + key + "'] = \"" + url + ";\";";
					javascript.append(js);
				}
				
				Dao.setRightURL("return !showMenu('parcs', event);");

				javascript.append("</script>");
				Dao.setJavaScript(javascript.toString());
				
			} else {
			
			// set lefthand module heading and link
			String winName = "PARCS" + bean.demographicNo;
			String url = "popupPage(580,900,'" + winName + "','" + request.getContextPath() + "/parcs/ExternalController/LimitedExternalControllerDemo.jsp?site=0&demographicNo=" + bean.demographicNo + "')";
			Dao.setLeftHeading(messages.getMessage(request.getLocale(), "global.parcs"));
			Dao.setLeftURL(url);

			// set righthand link to same as left so we have visual consistency with other modules
			url += "; return false;";
			Dao.setRightURL(url);
			Dao.setRightHeadingID(cmd); // no menu so set div id to unique id for this action	
			}
      } catch (Exception e) {
        log.error("OSCARPRO-5468 - Error in EctDisplayParcsAction: ", e);
        return false;
      }
		}
		return true;
	}

	public String getCmd() {
		return cmd;
	}
}
