/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.oscarEncounter.pageUtil;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;
import org.oscarehr.PMmodule.caisi_integrator.CaisiIntegratorManager;
import org.oscarehr.PMmodule.caisi_integrator.IntegratorFallBackManager;
import org.oscarehr.caisi_integrator.ws.CachedDemographicDrug;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SessionConstants;
import oscar.OscarProperties;
import oscar.oscarRx.data.RxPrescriptionData;
import oscar.util.DateUtils;
import oscar.util.StringUtils;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EctDisplayReprescribeAction extends EctDisplayAction {

    private String cmd = "Represcribe";
    private static final Logger logger= MiscUtils.getLogger();

    public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {

        LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);

        if (!securityInfoManager.hasPrivilege(loggedInInfo, "_rx", "w", null)) {
            return true; //check privilege
        }
        try {

            //set lefthand module heading and link

            //get drug name
            oscar.oscarRx.data.RxPrescriptionData prescriptData = new oscar.oscarRx.data.RxPrescriptionData();
            EctSessionBean ectSessionBeanbean = (EctSessionBean) request.getSession().getAttribute("EctSessionBean");
            RxPrescriptionData.Prescription[] arr = prescriptData.getUniquePrescriptionsByPatient(Integer.parseInt(ectSessionBeanbean.demographicNo));
            String drugName = "";
            for (RxPrescriptionData.Prescription prescription : arr) {
                drugName += prescription.getDrugName();
            }

            String leftUrl = request.getContextPath() + "/oscarRx/rePrescribe2.do?method=represcribeInEncounter&rand="+Math.floor(Math.random()*10001);
            String url = "new Ajax.Request('\" + leftUrl + \"', {method: 'get'});writeNewNoteSimple('Re-prescribe: " + StringEscapeUtils.escapeJavaScript(drugName) + "');";
            Dao.setLeftHeading("RePrescribe");

            //set righthand link to same as left so we have visual consistency with other modules
            url += "; return false;";
            Dao.setRightURL(url);
            Dao.setLeftURL(url);
            Dao.setRightHeadingID(cmd);  //no menu so set div id to unique id for this action
    } catch (Exception e) {
        logger.error("OSCARPRO-5468 - Error in EctDisplayReprescribeAction: ", e);
        return false;
    }
            return true;
    }

    public String getCmd() {
        return cmd;
    }
}
