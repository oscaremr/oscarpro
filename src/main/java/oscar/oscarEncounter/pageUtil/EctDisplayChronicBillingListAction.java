/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarEncounter.pageUtil;

import java.net.URLEncoder;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;

import oscar.OscarProperties;
import oscar.entities.Billingmaster;
import oscar.oscarRx.data.RxPatientData;
import oscar.util.DateUtils;
import oscar.util.OscarRoleObjectPrivilege;
import oscar.util.StringUtils;

/**
 * retrieves info to display Disease entries for demographic
 */
public class EctDisplayChronicBillingListAction extends EctDisplayAction {

	private static Logger logger = MiscUtils.getLogger();

	private String cmd = "chronicbilling";

	public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {
		Dao.setLeftHeading(messages.getMessage(request.getLocale(), "oscarEncounter.NavBar.ChronicBilling"));
		Dao.setRightHeadingID(cmd); // no menu so set div id to unique id for this action
		
		boolean a = true;
		Vector v = OscarRoleObjectPrivilege.getPrivilegeProp("_newCasemgmt.chronicbilling");
		String curProvider_no = (String) request.getSession().getAttribute("user");
		String roleName = (String) request.getSession().getAttribute("userrole") + "," + curProvider_no;
		a = OscarRoleObjectPrivilege.checkPrivilege(roleName, (Properties) v.get(0), (Vector) v.get(1));
		if (!a) {
			return true; // Chronic Billing link won't show up on new CME screen.
		} else {
			try {
			GregorianCalendar now=new GregorianCalendar();
			int curYear = now.get(Calendar.YEAR);
			int curMonth = (now.get(Calendar.MONTH)+1);
			int curDay = now.get(Calendar.DAY_OF_MONTH);
			String dateString = curYear+"-"+curMonth+"-"+curDay;

			String selectedSite = request.getParameter("site");

			// set lefthand module heading and link
			String winName = "Billing" + bean.demographicNo;
			String demographicName = URLEncoder.encode(bean.getPatientLastName()) + "," + URLEncoder.encode(bean.getPatientFirstName());
			
			OscarProperties props = OscarProperties.getInstance();
			String billRegion = props.getProperty("billregion", "");
			String billForm = props.getProperty("default_view", "");
			
			String url = "popupPage(755,1200,'" + winName + "','" + request.getContextPath() + "/billing.do?billRegion=" + URLEncoder.encode(billRegion) + "&billForm=" + URLEncoder.encode(billForm) + "&hotclick=&appointment_no=0&demographic_name="+ URLEncoder.encode(demographicName) + "&demographic_no=" + bean.demographicNo + "&providerview=" + bean.providerNo + "&user_no=" + curProvider_no + "&apptProvider_no=none&appointment_date=" + dateString + "&start_time=00:00:00&bNewForm=1&status=t" + (selectedSite != null ? "&site=" + selectedSite : "") + "');";
			
			Dao.setLeftURL(url);

			// set righthand link to same as left so we have visual consistency with other modules
			url += "return false;";
			Dao.setRightURL(url);			

			// grab all of the diseases associated with patient and add a list item for each

			Billingmaster[] billings;

			Integer demographicId = Integer.parseInt(bean.demographicNo);
			Locale locale=request.getLocale();

			billings = RxPatientData.getPatient(LoggedInInfo.getLoggedInInfoFromSession(request), demographicId).getChronicBillings();

			for (int idx = 0; idx < billings.length; ++idx) {
				Date date = billings[idx].getServiceDateAsDate();
				NavBarDisplayDAO.Item item = makeItem(date, billings[idx].getBillingCode(), billings[idx].getBillingstatus(), locale);
				Dao.addItem(item);
			}

			// --- sort all results ---
			Dao.sortItems(NavBarDisplayDAO.DATESORT_ASC);
		} catch (Exception e) {
			logger.error("OSCARPRO-5468 - Error in EctDisplayChronicBillingListAction", e);
			return false;
		}
			return true;
		}
	}

	private static NavBarDisplayDAO.Item makeItem(Date entryDate, String code, String status, Locale locale)
	{
		NavBarDisplayDAO.Item item = NavBarDisplayDAO.Item();

		item.setDate(entryDate);
		
		String title = code + " - " + status;
		
		item.setTitle(StringUtils.maxLenString(title, MAX_LEN_TITLE, CROP_LEN_TITLE, ELLIPSES));
		item.setLinkTitle(title + " " + DateUtils.formatDate(entryDate, locale));

		item.setURL("return false;");

		return(item);
	}

	public String getCmd() {
		return cmd;
	}
}
