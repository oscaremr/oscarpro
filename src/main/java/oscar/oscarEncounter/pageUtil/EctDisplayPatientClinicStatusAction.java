package oscar.oscarEncounter.pageUtil;

import org.apache.struts.util.MessageResources;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import javax.servlet.http.HttpServletRequest;

public class EctDisplayPatientClinicStatusAction extends EctDisplayAction {
    private static final String cmd = "patientClinicStatus";

    public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {
        try {
        LoggedInInfo loggedInInfo=LoggedInInfo.getLoggedInInfoFromSession(request);
        String widget = request.getParameter("widget");

        DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
        Demographic demographic = demographicDao.getDemographic(bean.demographicNo);
        String displayName = "";
        switch (widget) {
            case "fDoc":
                displayName = demographic.getFamilyPhysicianName();
                break;
            case "rDoc":
                displayName = demographic.getReferralPhysicianName();
                break;
            default:
                // invalid value, do not display link
                return false;
        }
        
        String heading = messages.getMessage(request.getLocale(), "oscarEncounter.LeftNavBar." + widget);
        
        String winName = "patientClinicStatus" + bean.demographicNo;
        String url = "popupPage(700,1024,'" + winName + "', '" + request.getContextPath() + "/demographic/demographiccontrol.jsp?demographic_no=" + bean.demographicNo + "&displaymode=edit&dboperation=search_detail');return false;";
        Dao.setLeftHeading(heading);
        Dao.setLeftURL(url);
        
        NavBarDisplayDAO.Item item = NavBarDisplayDAO.Item();
        item.setLinkTitle(displayName);
        displayName = oscar.util.StringUtils.maxLenString(displayName, MAX_LEN_TITLE, CROP_LEN_TITLE, ELLIPSES);
        item.setTitle(displayName);
        item.setURL(url);
        Dao.addItem(item);

        //set righthand link to same as left so we have visual consistency with other modules
        Dao.setRightURL(url);
        Dao.setRightHeadingID(cmd);  //no menu so set div id to unique id for this action
    } catch (Exception e) {
        MiscUtils.getLogger().error("OSCARPRO-5468 - Error in EctDisplayPatientClinicStatusAction: ", e);
        return false;
    }
        return true; 
    }

    public String getCmd() {
        return cmd;
    }
}
