/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package oscar.oscarEncounter.pageUtil;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.struts.util.MessageResources;
import org.codehaus.jettison.json.JSONObject;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.integration.OneIdSession;
import org.oscarehr.integration.OneIdSessionDao;
import org.oscarehr.integration.OneIdViewlet;
import org.oscarehr.integration.OneIdViewletDao;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;

public class EctDisplayEHRAction extends EctDisplayAction {

	private static final String cmd = "ehr";

	OneIdViewletDao oneIdViewletDao = SpringUtils.getBean(OneIdViewletDao.class);
	OneIdSessionDao oneIdSessionDao = SpringUtils.getBean(OneIdSessionDao.class);
	SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);

	public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {
		LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
		OneIdSession oneIdSession = oneIdSessionDao.find(loggedInInfo.getLoggedInProviderNo());
		List<OneIdViewlet> oneIdViewlets = oneIdViewletDao.findAllActiveAndShowInEchartTrue();

		Dao.setLeftHeading("Provincial EHR Services");
		Dao.setRightHeadingID(cmd); //no menu so set div id to unique id for this action		
		
		if (!securityInfoManager.hasPrivilege(loggedInInfo, "_ehr", "r", null)) {
			return true;
		} else {
			try {
			String winName = "ehr" + bean.demographicNo;
			String url = "javascript:void(0)";
			
			Dao.setLeftURL(url);

			url += ";return false;";
			Dao.setRightURL(url);

			DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
			Demographic demographic = demographicDao.getDemographic(bean.getDemographicNo());
			String demographicCmsErrors = isValidCmsDemographic(demographic);

			for (OneIdViewlet oneIdViewlet : oneIdViewlets) {
				if ("eforms_url".equals(oneIdViewlet.getKeyValue())
						&& !systemPreferencesDao.isEformsEnabled()) {
					continue;
				}
				NavBarDisplayDAO.Item item = new NavBarDisplayDAO.Item();
				item.setTitle(oneIdViewlet.getName());
				item.setLinkTitle("Open " + oneIdViewlet.getName());
				if (demographicCmsErrors == null) {
					item.setURL(
							"popupEHRService('/"
									+ OscarProperties.getKaiemrDeployedContext()
									+ "/api/v1/open-viewlet/launch/"
									+ bean.demographicNo + "?key=" + oneIdViewlet.getKeyValue()
									+ "', '"
									+ bean.demographicNo
									+ "'); return false;");
				} else {
					item.setURL("alert('"
							+ StringEscapeUtils.escapeJavaScript(demographicCmsErrors) + "'); return false");
				}
				Dao.addItem(item);
			}

			if (systemPreferencesDao.isClinicalConnectEnabled()) {
				if (oneIdSession != null && !oneIdSession.isExpired()) {
					NavBarDisplayDAO.Item item = new NavBarDisplayDAO.Item();
					item.setTitle("Clinical Connect Viewer");
					item.setLinkTitle("Open the Clinical Connect EHR Viewer");
					if (demographicCmsErrors == null) {
						item.setURL(
								"popupEHRService('/"
										+ OscarProperties.getKaiemrDeployedContext()
										+ "/api/v1/clinical-connect/launch/"
										+ bean.demographicNo
										+ "', '"
										+ bean.demographicNo
										+ "'); return false;");
					} else {
						item.setURL("alert('"
								+ StringEscapeUtils.escapeJavaScript(demographicCmsErrors) + "'); return false");
					}
					Dao.addItem(item);
				} else {
					NavBarDisplayDAO.Item item = new NavBarDisplayDAO.Item();
					item.setTitle("Clinical Connect Viewer");
					item.setLinkTitle("Sign in to OneID required");
					item.setURL("popupEHRService('/" + OscarProperties.getKaiemrDeployedContext()
							+ "/#/one-id/login?forward=/" + OscarProperties.getKaiemrDeployedContext()
							+ "/api/v1/clinical-connect/launch/" + bean.demographicNo + "', '" + bean.demographicNo
							+ "'); return false;");

					Dao.addItem(item);
				}
			}
		} catch (Exception e) {
			MiscUtils.getLogger().error("OSCARPRO-5468 - Error in EctDisplayEHRAction: ", e);
			return false;
		}
			return true;
		}
	}

  private String isValidCmsDemographic(Demographic demographic) {
    List<String> errors = new ArrayList<>();
		if (StringUtils.isBlank(demographic.getHin())) {
			errors.add("- health card number");
		}
		if (StringUtils.isBlank(demographic.getFirstName())) {
			errors.add("- first name");
		}
		if (StringUtils.isBlank(demographic.getLastName())) {
			errors.add("- last name");
		}
		if (StringUtils.isBlank(demographic.getSex())) {
			errors.add("- gender");
		}
		if (StringUtils.isBlank(demographic.getFormattedDob())) {
			errors.add("- DOB");
		}
    if (errors.size() == 0) {
      return null;
    }
    StringBuilder sb = new StringBuilder("Unable to launch Context-Aware EHR service due to missing data in demographic record.\n");
    for (String error : errors) {
      sb.append("\n");
      sb.append(error);
    }
    return sb.toString();
  }

	public static String getBaseUrl(HttpServletRequest request) {
	    String scheme = request.getScheme() + "://";
	    String serverName = request.getServerName();
	    String serverPort = (request.getServerPort() == 80) ? "" : ":" + request.getServerPort();
	    String contextPath = request.getContextPath();
	    return scheme + serverName + serverPort + contextPath;
	  }
	
	private boolean tokenValid(String token) {
		Date d = this.retrieveSessionExpiration(token);
		if(d == null) {
			return false;
		}
			
		if(d.after(new Date())) {
			return true;
		}
		return false;
	}

	protected Date retrieveSessionExpiration(String oneIdToken) {
		String sessionExpiry = null;
		
		
		try {
			//get the context session id
			String url = OscarProperties.getInstance().getProperty("backendEconsultUrl") + "/api/getTokenExpiry";
			HttpGet httpGet = new HttpGet(url);
			httpGet.addHeader("x-access-token", oneIdToken);
			HttpClient httpClient = getHttpClient2();
			HttpResponse httpResponse = httpClient.execute(httpGet);
	
			if (httpResponse.getStatusLine().getStatusCode() == 200) {
				String entity = EntityUtils.toString(httpResponse.getEntity());
				JSONObject obj = new JSONObject(entity);
				sessionExpiry = (String) obj.get("sessionExpiration");
				MiscUtils.getLogger().debug("sessionExpiry = " + sessionExpiry);
				
				SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
				Date d  = fmt.parse(sessionExpiry + "+0000");
				
				return d;
			}
		}catch(Exception e) {
			MiscUtils.getLogger().error("Error",e);
		}
		
		return null;
	}
	
	protected HttpClient getHttpClient2() throws Exception {

		String cmsKeystoreFile = OscarProperties.getInstance().getProperty("clinicalConnect.CMS.keystore");
		String cmsKeystorePassword = OscarProperties.getInstance().getProperty("clinicalConnect.CMS.keystore.password");

		KeyStore ks = KeyStore.getInstance("JKS");
		ks.load(new FileInputStream(cmsKeystoreFile), cmsKeystorePassword.toCharArray());

		//setup SSL
		SSLContext sslcontext = SSLContexts.custom().loadKeyMaterial(ks, cmsKeystorePassword.toCharArray()).build();
		sslcontext.getDefaultSSLParameters().setNeedClientAuth(true);
		sslcontext.getDefaultSSLParameters().setWantClientAuth(true);
		SSLConnectionSocketFactory sf = new SSLConnectionSocketFactory(sslcontext);

		//setup timeouts
		int timeout = Integer.parseInt(OscarProperties.getInstance().getProperty("clinicalConnect.CMS.timeout", "60"));
		RequestConfig config = RequestConfig.custom().setSocketTimeout(timeout * 1000).setConnectTimeout(timeout * 1000).build();

		CloseableHttpClient httpclient3 = HttpClients.custom().setDefaultRequestConfig(config).setSSLSocketFactory(sf).build();

		return httpclient3;

	}
	public String getCmd() {
		return cmd;
	}
}
