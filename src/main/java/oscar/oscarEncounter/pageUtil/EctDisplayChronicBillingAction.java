/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package oscar.oscarEncounter.pageUtil;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.util.MessageResources;

public class EctDisplayChronicBillingAction extends EctDisplayAction {

	private final String COMMAND = "chronicBilling";
	
	protected EctDisplayChronicBillingAction() {
		// default constructor.
		
	}
	
	@Override
    public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {
		
		String demographicNo = bean.getDemographicNo();
		
		// with proper encapsulation security can be handled in the abstract bean.

    	/*Vector v = OscarRoleObjectPrivilege.getPrivilegeProp("_newCasemgmt.chronicBilling");
    	String roleName = (String)request.getSession().getAttribute("userrole") + "," + (String) request.getSession().getAttribute("user");
    	boolean a = OscarRoleObjectPrivilege.checkPrivilege(roleName, (Properties) v.get(0), (Vector) v.get(1));

    	if(!a) {
    		return a;
    	} else {
    		return true;
    	}*/
		System.out.println("CHRONIC BILLING RUNNING...");
		
		Dao.setLeftURL("#");
		Dao.setLeftHeading(messages.getMessage(request.getLocale(), "oscarEncounter.Index.chronicBilling"));
		Dao.setRightHeadingID("3");
		Dao.setMenuHeader(messages.getMessage("oscarEncounter.LeftNavBar.InputGrps"));
		Dao.setRightURL("return !showMenu('" + 3 + "', event);");

		
		Dao.setLeftHeading(messages.getMessage(request.getLocale(), "oscarEncounter.Index.chronicBilling"));
		NavBarDisplayDAO.Item displayItem = NavBarDisplayDAO.Item();
		displayItem.setBgColour("green");
		displayItem.setURL("#");
		displayItem.setLinkTitle("this is link");
		displayItem.setValue("$100");
		displayItem.setTitle("Test Entry");
		Dao.addItem(displayItem);
		Dao.sortItems(NavBarDisplayDAO.DATESORT_ASC);
		
		return true;
    	
    }

	@Override
    public String getCmd() {
	    return COMMAND;
    }
    

}
