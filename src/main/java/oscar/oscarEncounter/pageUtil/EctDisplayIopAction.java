package oscar.oscarEncounter.pageUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.util.MessageResources;
import org.oscarehr.common.dao.MeasurementDao;
import org.oscarehr.common.dao.OscarAppointmentDao;
import org.oscarehr.common.model.Appointment;
import org.oscarehr.common.model.Measurement;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

public class EctDisplayIopAction extends EctDisplayAction {

private static Logger logger = MiscUtils.getLogger();
	
	private String cmd = "IOP";
	
	@SuppressWarnings("deprecation")
	public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {
		try{
			//set lefthand module heading and link
//			String winName = "freeDrawing" + bean.demographicNo;
//	        String url = "popupPage(500,950,'" + winName + "', '" + request.getContextPath() + "/casemgmt/freeDrawList.jsp?demographic_no="+bean.demographicNo+"&provNo="+bean.providerNo+"&appointment="+bean.appointmentNo+"&parentAjaxId=freeDrawing2')";
//	        Dao.setLeftHeading(messages.getMessage(request.getLocale(), "global.IopData"));
//	        Dao.setLeftURL(url);
	        
	        //set the right hand heading link
//	        url = "popupPage(500,950,'" + winName + "', '" + request.getContextPath() + "/casemgmt/freeDrawPage2.jsp?demographic_no="+bean.demographicNo+"&provNo="+bean.providerNo+"&appointment="+bean.appointmentNo+"&parentAjaxId=freeDrawing2'); return false;";
//	        Dao.setRightURL(url);        
//	        Dao.setRightHeadingID(cmd);  //no menu so set div id to unique id for this action
	        
	        OscarAppointmentDao appointmentDao = (OscarAppointmentDao)SpringUtils.getBean("oscarAppointmentDao");
	        MeasurementDao measurementsDao = SpringUtils.getBean(MeasurementDao.class);
	        
	        List<Appointment> appList = appointmentDao.getAppointmentHistory(Integer.parseInt(bean.demographicNo));
	        List<Map<String, String>> iop_data = new ArrayList<Map<String, String>>();
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	        for(Appointment appt:appList) {
	        	Map<String, String> iop_map = new HashMap<String, String>();
	        	Measurement m = null;
	        	
	        	//IOP
	        	m = measurementsDao.findLatestByAppointmentNoAndType(appt.getId(),"iop_rn");
	        	if(m != null){
	        		iop_map.put("iop_rn", m.getDataField());
	        	}
	        	m = measurementsDao.findLatestByAppointmentNoAndType(appt.getId(),"iop_ra");
	        	if(m != null){
	        		iop_map.put("iop_ra", m.getDataField());
	        	}
	        	m = measurementsDao.findLatestByAppointmentNoAndType(appt.getId(),"iop_ln");
	        	if(m != null){
	        		iop_map.put("iop_ln", m.getDataField());
	        	}
	        	m = measurementsDao.findLatestByAppointmentNoAndType(appt.getId(),"iop_la");
	        	if(m != null){
	        		iop_map.put("iop_la", m.getDataField());
	        	}
	        	
	        	if(!iop_map.isEmpty()){
	        		iop_map.put("date", sdf.format(appt.getAppointmentDate()));
	        		
	        		iop_data.add(iop_map);
	        	}
	        }
	        
	        if(iop_data.size() > 0){
	        	for(int i = 0;i < iop_data.size();i ++){
	        		Map<String, String> map = iop_data.get(i);
	        		
	        		if(i == 0){
	        			String iop_title = "<div style='width: 100%'>";
	        			iop_title += "<span style='width: 2%; text-align: left; display: inline-block'>[IOP]</span>";
	        			iop_title += "<span style='width: 39%; text-align: center; display: inline-block'>OD</span>";
	        			iop_title += "<span style='width: 39%; text-align: center; display: inline-block'>OS</span>";
	        			iop_title += "<span style='width: 20%; text-align: center; display: inline-block'>DATE</span>";
	        			iop_title += "</div>";
	        			
	        			NavBarDisplayDAO.Item item = NavBarDisplayDAO.Item();
	        			item.setTitle(iop_title);
	        			item.setNeedEncode(false);
	        			item.setURL("return false;");
	        			Dao.addItem(item);
	        			
	        			String iop_title2 = "<div style='width: 100%'>";
	        			iop_title2 += "<span style='width: 2%; text-align: left; display: inline-block'></span>";
	        			iop_title2 += "<span style='width: 19.5%; text-align: center; display: inline-block'>NCT</span>";
	        			iop_title2 += "<span style='width: 19.5%; text-align: center; display: inline-block'>App</span>";
	        			iop_title2 += "<span style='width: 19.5%; text-align: center; display: inline-block'>NCT</span>";
	        			iop_title2 += "<span style='width: 19.5%; text-align: center; display: inline-block'>App</span>";
	        			iop_title2 += "<span style='width: 20%; text-align: center; display: inline-block'></span>";
	        			iop_title2 += "</div>";
	        			NavBarDisplayDAO.Item item2 = NavBarDisplayDAO.Item();
	        			item2.setTitle(iop_title2);
	        			item2.setURL("return false;");
	        			item2.setNeedEncode(false);
	        			Dao.addItem(item2);
	        			
//	        			String iop_line = "<div style='background-color:#b0e2ff;height: 1px'></div>";
//		        		NavBarDisplayDAO.Item item_line = NavBarDisplayDAO.Item();
//		        		item_line.setTitle(iop_line);
//		        		Dao.addItem(item_line);
	        		}
	        		
        			String iop = "<div style='width: 100%;text-align: center'>"; 
        			iop += "<span style='width: 2%; text-align: left; display: inline-block'></span>";
        			iop += "<span style='width: 19.5%; text-align: center; display: inline-block'>" + (map.get("iop_rn") == null ? "":map.get("iop_rn"))  + "</span>";
        			iop += "<span style='width: 19.5%; text-align: center; display: inline-block'>" + (map.get("iop_ra") == null ? "":map.get("iop_ra")) + "</span>";
        			iop += "<span style='width: 19.5%; text-align: center; display: inline-block'>" + (map.get("iop_ln") == null ? "":map.get("iop_ln")) + "</span>";
        			iop += "<span style='width: 19.5%; text-align: center; display: inline-block'>" + (map.get("iop_la") == null ? "":map.get("iop_la")) + "</span>";
        			iop += "<span style='width: 20%; text-align: center; display: inline-block'>" + map.get("date") + "</span>";
        			iop += "</div>";
        			
        			NavBarDisplayDAO.Item item = NavBarDisplayDAO.Item();
        			item.setTitle(iop);
        			item.setURL("return false;");
        			item.setNeedEncode(false);
        			Dao.addItem(item);
        			
        			if(i == iop_data.size() - 1){
        				String iop_line = "<div style='background-color:#b0e2ff;height: 1px'></div>";
		        		NavBarDisplayDAO.Item item_line = NavBarDisplayDAO.Item();
		        		item_line.setTitle(iop_line);
		        		item_line.setURL("return false;");
		        		item_line.setNeedEncode(false);
		        		Dao.addItem(item_line);
        			}
	        	}
	        }
	        
			return true;
		}catch (Exception e){
			logger.error("OSCARPRO-5468 - Error in EctDisplayIopAction: ", e);
			return false;
		}
	}
	
	public String getCmd() {
		return cmd;
	}
}
