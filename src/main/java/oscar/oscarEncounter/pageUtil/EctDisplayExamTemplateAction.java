package oscar.oscarEncounter.pageUtil;

import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.util.MessageResources;
import org.oscarehr.eyeform.dao.ExamTemplateDao;
import org.oscarehr.eyeform.model.ExamTemplate;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

import oscar.util.OscarRoleObjectPrivilege;
import oscar.util.StringUtils;

public class EctDisplayExamTemplateAction extends EctDisplayAction {
	private static final String cmd = "examTemplate";
	
	public boolean getInfo(EctSessionBean bean, HttpServletRequest request, NavBarDisplayDAO Dao, MessageResources messages) {
		if(!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_eyeform", "r", null)) {
			throw new SecurityException("missing required security object (_eyeform)");
		}
		
		boolean a = true;
		Vector v = OscarRoleObjectPrivilege.getPrivilegeProp("_newCasemgmt.examTemplate");
		String roleName = (String)request.getSession().getAttribute("userrole") + "," + (String) request.getSession().getAttribute("user");
		a = OscarRoleObjectPrivilege.checkPrivilege(roleName, (Properties) v.get(0), (Vector) v.get(1));
		a=true;
		if(!a) {
	 		return true; //The link of tickler won't show up on new CME screen.
	 	} else {
	 		try {
	 			 String appointmentNo = request.getParameter("appointment_no");
	 			 String cpp =request.getParameter("cpp");
	 			 if(cpp==null) {
	 				 cpp=new String();
	 			 }
	 			
	 			//Set lefthand module heading and link
	 		    String winName = "exam templates" + bean.demographicNo;
	 		    String pathview, pathedit;
	 		   
	 		    pathview = request.getContextPath() + "/eyeform/examTemplate.do?method=list&providerNo=" + bean.providerNo;
	 		    pathedit = request.getContextPath() + "/eyeform/examTemplate.do?method=addExamTemplate&providerNo=" + bean.providerNo;
	 		    
	 		    String url = "popupPage(500,900,'" + winName + "','" + pathview + "')";
	 		    Dao.setLeftHeading(messages.getMessage(request.getLocale(), "global.examTemplate"));
	 		    Dao.setLeftURL(url);
	 		    
	 		    //set right hand heading link
	 		    winName = "AddExamTemplate" + bean.demographicNo;
	 		    url = "popupPage(500,600,'" + winName + "','" + pathedit + "'); return false;";
	 		    Dao.setRightURL(url);
	 		    Dao.setRightHeadingID(cmd); //no menu so set div id to unique id for this action
	 		    
	 		   ExamTemplateDao examTemplateDao = (ExamTemplateDao)SpringUtils.getBean("examTemplateDao");
	 		   List<ExamTemplate> examList = examTemplateDao.getAllMySelfTemplate(bean.providerNo);
	 		   for(ExamTemplate exam : examList){
	 			  NavBarDisplayDAO.Item item = NavBarDisplayDAO.Item();
	 			  String itemHeader = StringUtils.maxLenString(exam.getExamTemplateName(), MAX_LEN_TITLE, CROP_LEN_TITLE, ELLIPSES);                      
	 			  item.setLinkTitle(itemHeader);        
	 			  item.setTitle(itemHeader);
	 			  
	 			  url = "return runExamTemplate("+exam.getId()+",'" + exam.getExamTemplateName() + "',"+appointmentNo+",'"+cpp+"');";
	 			  item.setURL(url);               
	 			  Dao.addItem(item);
	 		   }
	 		    
	 		}catch( Exception e ) {
	 		     MiscUtils.getLogger().error("Error", e);
	 		     return false;
	 		 }
	 		return true;
	 	}
	}
	
	public String getCmd() {
	     return cmd;
	 }
}
