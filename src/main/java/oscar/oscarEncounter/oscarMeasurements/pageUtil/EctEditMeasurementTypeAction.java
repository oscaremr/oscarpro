/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarEncounter.oscarMeasurements.pageUtil;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.MessageResources;
import org.oscarehr.common.dao.MeasurementGroupDao;
import org.oscarehr.common.dao.MeasurementTypeDao;
import org.oscarehr.common.dao.MeasurementTypeDeletedDao;
import org.oscarehr.common.model.MeasurementGroup;
import org.oscarehr.common.model.MeasurementType;
import org.oscarehr.common.model.MeasurementTypeDeleted;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import oscar.oscarEncounter.oscarMeasurements.bean.EctMeasurementTypesBeanHandler;
import oscar.oscarEncounter.oscarMeasurements.data.MeasurementTypes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class EctEditMeasurementTypeAction extends Action {
	
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        MeasurementTypeDao mtDao = SpringUtils.getBean(MeasurementTypeDao.class);
        MeasurementTypeDeletedDao mtdDao = SpringUtils.getBean(MeasurementTypeDeletedDao.class);
        SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);
        MeasurementGroupDao measurementGroupDao = SpringUtils.getBean(MeasurementGroupDao.class);
        
        EctEditMeasurementTypeForm ectEditMeasurementTypeForm = (EctEditMeasurementTypeForm) form;

        if( securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_admin", "w", null) || securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request), "_admin.measurements", "w", null) )  {
       
            request.getSession().setAttribute("EctEditMeasurementTypeForm", ectEditMeasurementTypeForm);
            
            List<String> messages = new LinkedList<String>();
            int id = ectEditMeasurementTypeForm.getTypeId();
            
            //Anything with an id less than 0 is a non-existent record
            if (id < 0) {
                return mapping.findForward("failure");
            }
    
            MeasurementType oldMeasurementType = mtDao.find(id);
    
            //Anything that isn't actually stored in the DB is a new record and therefore cannot be edited
            if (oldMeasurementType == null) {
                return mapping.findForward("failure");
            }
            
            String type = ectEditMeasurementTypeForm.getType();
            String typeDesc = ectEditMeasurementTypeForm.getTypeDesc();
            String typeDisplayName = ectEditMeasurementTypeForm.getTypeDisplayName();
            String measuringInstrc = ectEditMeasurementTypeForm.getMeasuringInstrc();
            String validation = ectEditMeasurementTypeForm.getValidation();
            
            //Backend validation for the data
            if (!allInputIsValid(request, type, typeDesc, typeDisplayName, measuringInstrc)){
                return (new ActionForward(mapping.getInput()));
            }
    
            MeasurementType newMeasurementType = new MeasurementType();
            newMeasurementType.setType(type.toUpperCase());
            newMeasurementType.setTypeDescription(typeDesc);
            newMeasurementType.setTypeDisplayName(typeDisplayName);
            newMeasurementType.setMeasuringInstruction(measuringInstrc);
            newMeasurementType.setValidation(validation);
    
            mtDao.persist(newMeasurementType);
            
            MeasurementTypeDeleted deletedMeasurement = new MeasurementTypeDeleted();
            deletedMeasurement.setType(oldMeasurementType.getType());
            deletedMeasurement.setPrevTypeId(oldMeasurementType.getId());
            deletedMeasurement.setTypeDisplayName(oldMeasurementType.getTypeDisplayName());
            deletedMeasurement.setTypeDescription(oldMeasurementType.getTypeDescription());
            deletedMeasurement.setMeasuringInstruction(oldMeasurementType.getMeasuringInstruction());
            deletedMeasurement.setValidation(oldMeasurementType.getValidation());
            deletedMeasurement.setDateDeleted(new Date());
            
            mtdDao.persist(deletedMeasurement);
            
            //If the measurement changed it's display name, measurementGroups need to be updated with the new name
            //or else they wont link properly.
            if (!newMeasurementType.getTypeDisplayName().equals(deletedMeasurement.getTypeDisplayName())) {
                List<MeasurementGroup> msgList = measurementGroupDao.findByTypeDisplayName(deletedMeasurement.getTypeDisplayName());
    
                for (MeasurementGroup measurementGroup : msgList) {
                    measurementGroup.setArchived(true);
                    measurementGroupDao.merge(measurementGroup);
                    
                    MeasurementGroup newMsg = new MeasurementGroup();
                    newMsg.setName(measurementGroup.getName());
                    newMsg.setTypeDisplayName(newMeasurementType.getTypeDisplayName());
                    measurementGroupDao.persist(newMsg);
                }
            }
            
            MessageResources mr = getResources(request);
            String msg = mr.getMessage("oscarEncounter.oscarMeasurements.EditMeasurementType.successful", "!");
            messages.add(msg);
            request.setAttribute("messages", messages);
            
            //Resets the form
            ectEditMeasurementTypeForm.setTypeId(-1);
            ectEditMeasurementTypeForm.setTypeDisplayName("");
            ectEditMeasurementTypeForm.setTypeDesc("");
            ectEditMeasurementTypeForm.setMeasuringInstrc("");
            
            request.getSession().setAttribute("EctEditMeasurementTypeForm", ectEditMeasurementTypeForm);
            MeasurementTypes mts =  MeasurementTypes.getInstance();
            mts.reInit();

            EctMeasurementTypesBeanHandler hd = new EctMeasurementTypesBeanHandler();
            HttpSession session = request.getSession();
            session.setAttribute( "measurementTypes", hd );
            return mapping.findForward("success");
		} else {
			throw new SecurityException("Access Denied!"); //missing required security object (_admin)
		}

    }
    
    private boolean allInputIsValid(HttpServletRequest request, String type, String typeDesc, String typeDisplayName, String measuringInstrc){
        
        ActionMessages errors = new ActionMessages();  
        EctValidation validate = new EctValidation();
        String regExp = validate.getRegCharacterExp();
        boolean isValid = true;
        
        String errorField = "The type " + type;
        if(!validate.matchRegExp(regExp, type)){
            errors.add(type,
            new ActionMessage("errors.invalid", errorField));
            saveErrors(request, errors);
            isValid = false;
        }
        if(!validate.maxLength(50, type)){
            errors.add(type,
            new ActionMessage("errors.maxlength", errorField, "4"));
            saveErrors(request, errors);
            isValid = false;
        }

        errorField = "The type description " + typeDesc;
        if(!validate.matchRegExp(regExp, typeDesc)){
            errors.add(typeDesc,
            new ActionMessage("errors.invalid", errorField));
            saveErrors(request, errors);
            isValid = false;
        }
        if(!validate.maxLength(255, type)){
            errors.add(type,
            new ActionMessage("errors.maxlength", errorField, "255"));
            saveErrors(request, errors);
            isValid = false;
        }
        
        errorField = "The display name " + typeDisplayName;
        if(!validate.matchRegExp(regExp, typeDisplayName)){
            errors.add(typeDisplayName,
            new ActionMessage("errors.invalid", errorField));
            saveErrors(request, errors);
            isValid = false;
        }
        if(!validate.maxLength(255, type)){
            errors.add(type,
            new ActionMessage("errors.maxlength", errorField, "255"));
            saveErrors(request, errors);
            isValid = false;
        }
        
        errorField = "The measuring instruction " + measuringInstrc;
        if(!validate.matchRegExp(regExp, measuringInstrc)){
            errors.add(measuringInstrc,
            new ActionMessage("errors.invalid", errorField));
            saveErrors(request, errors);
            isValid = false;
        }
        if(!validate.maxLength(255, type)){
            errors.add(type,
            new ActionMessage("errors.maxlength", errorField, "255"));
            saveErrors(request, errors);
            isValid = false;
        }
       
        return isValid;
        
    }
}
