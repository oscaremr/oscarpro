/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.oscarEncounter.oscarMeasurements.util;

import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import oscar.oscarEncounter.oscarMeasurements.bean.EctMeasurementsDataBean;
import oscar.oscarEncounter.oscarMeasurements.bean.EctMeasurementsDataBeanHandler;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KidneyFailureRiskCalculatorDataFetcher {

  @SuppressWarnings("UnstableApiUsage")
  public MeasurementValue fetch(HttpServletRequest request) {
    MeasurementValue measurementValue = new MeasurementValue();
    try {
      Integer demo_no = Integer.valueOf(request.getParameter("demographic_no"));
      LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
      fetchMeasurementValues(demo_no, loggedInInfo, measurementValue);
    } catch (Exception e) {
      MiscUtils.getLogger().error(e.getMessage(), e);
      return measurementValue;
    }
    return measurementValue;
  }

  private void fetchMeasurementValues(Integer demo_no, LoggedInInfo loggedInInfo, MeasurementValue measurementValue) {
    Collection<EctMeasurementsDataBean> mdDataBeans =
            new EctMeasurementsDataBeanHandler(demo_no).getMeasurementsDataVector();

    if (mdDataBeans.isEmpty()) {
      return;
    }

    for (EctMeasurementsDataBean dataBean : mdDataBeans) {
      List<EctMeasurementsDataBean> measurements = getMeasurements(demo_no, loggedInInfo, dataBean.getType());

      if (measurements != null && !measurements.isEmpty()) {
        EctMeasurementsDataBean measurementsDataBean = measurements.get(0);
        String dataField = measurementsDataBean.getDataField();

        if (dataField == null || dataField.trim().length() == 0) {
          continue;
        }

        measurementValue.putComparingDate(measurementsDataBean.getType(), dataField, measurementsDataBean.getDateEnteredAsDate());
      }
    }
  }

  private List<EctMeasurementsDataBean> getMeasurements(Integer demo_no, LoggedInInfo loggedInInfo, String type) {
    List<EctMeasurementsDataBean> measurements =
            (List<EctMeasurementsDataBean>) new EctMeasurementsDataBeanHandler(demo_no, type).getMeasurementsDataVector();

    if (loggedInInfo.getCurrentFacility().isIntegratorEnabled()) {
      EctMeasurementsDataBeanHandler.addRemoteMeasurements(loggedInInfo, measurements, type, demo_no);
    }

    return measurements;
  }

  public static class MeasurementValue {

    Map<String, String> valueMap = new HashMap<>();
    Map<String, Date> dateMap = new HashMap<>();

    public void putComparingDate(String type, String value, Date date) {
      if (type == null || value == null || date == null)
        return;

      if (dateMap.get(type) == null || (dateMap.get(type) != null && dateMap.get(type).compareTo(date) > 0)) {
        dateMap.put(type,date);
        valueMap.put(type, value);
      }
    }

    public String getValueOrDefault(String type) {
      return valueMap.get(type) != null ? valueMap.get(type).trim() : "0";
    }
  }

  public enum KidneyFailureEnum {
    ACR, EGFR
  }
}
