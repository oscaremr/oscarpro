/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */

package oscar.oscarEncounter.oscarMeasurements.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import org.oscarehr.common.dao.MeasurementGroupDao;
import org.oscarehr.common.dao.MeasurementTypeDao;
import org.oscarehr.common.dao.MeasurementTypeDeletedDao;
import org.oscarehr.common.model.MeasurementType;
import org.oscarehr.common.model.MeasurementTypeDeleted;
import org.oscarehr.util.SpringUtils;

public class EctTypeDisplayNameBeanHandler {

	Vector<EctTypeDisplayNameBean> typeDisplayNameVector = new Vector<EctTypeDisplayNameBean>();

	public EctTypeDisplayNameBeanHandler() {
		init();
	}

	public EctTypeDisplayNameBeanHandler(String groupName, boolean excludeGroupName) {
		initGroupTypes(groupName, excludeGroupName);
	}

	public boolean init() {
		MeasurementTypeDao dao = SpringUtils.getBean(MeasurementTypeDao.class);
		for (Object name : dao.findUniqueTypeDisplayNames()) {
			EctTypeDisplayNameBean typeDisplayName = new EctTypeDisplayNameBean(String.valueOf(name));
			typeDisplayNameVector.add(typeDisplayName);
		}
		return true;
	}

	public boolean initGroupTypes(String groupName, boolean excludeGroupName) {
		MeasurementTypeDao tDao = SpringUtils.getBean(MeasurementTypeDao.class);
		MeasurementTypeDeletedDao mtdDao = SpringUtils.getBean(MeasurementTypeDeletedDao.class);
		MeasurementGroupDao gDao = SpringUtils.getBean(MeasurementGroupDao.class);
		
		List<MeasurementType> mTypeList = tDao.findAll();
		List<MeasurementType> mTypeRemoveList = new ArrayList<MeasurementType>();
		List<MeasurementTypeDeleted> mTypeDeletedList = mtdDao.findAll(null,null);
		
		for (MeasurementType measurementType : mTypeList) {
			for (MeasurementTypeDeleted measurementTypeDeleted : mTypeDeletedList) {
				if (measurementType.getId().equals(measurementTypeDeleted.getPrevTypeId())) {
					mTypeRemoveList.add(measurementType);
				}
			}
		}
		
		mTypeList.removeAll(mTypeRemoveList);
		
		List<String> uniqueDisplayNameList = new ArrayList<String>();
		
		for (MeasurementType measurementType : mTypeList) {
			if (!uniqueDisplayNameList.contains(measurementType.getTypeDisplayName())) {
				uniqueDisplayNameList.add(measurementType.getTypeDisplayName());
			}
		}
		
		

		if (excludeGroupName) {
			for (String typeDisplayNameFromMeasurementType : uniqueDisplayNameList) {
				boolean foundInGroup = false;

				for (Object tdnMg : gDao.findUniqueTypeDisplayNamesByGroupName(groupName)) {
					String typeDisplayNameFromMeasurmentGroup = String.valueOf(tdnMg);

					if (typeDisplayNameFromMeasurementType.equals(typeDisplayNameFromMeasurmentGroup)) {
						foundInGroup = true;
						break;
					} else {
						foundInGroup = false;
					}
				}

				if (!foundInGroup) {
					EctTypeDisplayNameBean typeDisplayName = new EctTypeDisplayNameBean(typeDisplayNameFromMeasurementType);
					typeDisplayNameVector.add(typeDisplayName);
				}
			}
		} else {
			for (Object tdnMg : gDao.findUniqueTypeDisplayNamesByGroupName(groupName)) {
				EctTypeDisplayNameBean typeDisplayName = new EctTypeDisplayNameBean(String.valueOf(tdnMg));
				typeDisplayNameVector.add(typeDisplayName);
			}
		}

		return true;
	}

	public Collection<EctTypeDisplayNameBean> getTypeDisplayNameVector() {
		return typeDisplayNameVector;
	}
}
