/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarEncounter.oscarConsultationRequest.pageUtil;

import org.oscarehr.common.dao.ConsultDocsDao;
import org.oscarehr.common.model.ConsultDocs;
import org.oscarehr.util.SpringUtils;
import oscar.form.FormAttachmentObject;
import oscar.form.dao.ONPerinatal2017Dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ConsultationAttachForms {

	private static ConsultDocsDao consultDocsDao = SpringUtils.getBean(ConsultDocsDao.class);
    public final static boolean ATTACHED = true;
    private String providerNumber;
    private String consultationId;
    private String formType;
    private String demographicNo;
    private boolean editOnOcean;
    private ArrayList<String> forms;

    /** Creates a new instance of ConsultationAttachPerinatalForms */
    ConsultationAttachForms(String provNo, String req, String[] formNoList, String frmType, String demoNo, boolean editOcean) {
        providerNumber = provNo;
        consultationId = req;
        formType = frmType;
        demographicNo = demoNo;
        editOnOcean = editOcean;
        forms = new ArrayList<>();

        forms.addAll(Arrays.asList(formNoList));
    }

    public void attach() {
        ONPerinatal2017Dao onPerinatal2017Dao = SpringUtils.getBean(ONPerinatal2017Dao.class);
        
        List<FormAttachmentObject> oldList = new ArrayList<>();
        
        //Add all attached Perinatal Forms
        if (formType.equals(ConsultDocs.DOCTYPE_FORM_PERINATAL)) {
            oldList.addAll(onPerinatal2017Dao.findAllFormsAttachedToConsultation(consultationId));
        }
        
        ArrayList<String> newList = new ArrayList<>();
        ArrayList<FormAttachmentObject> keepList = new ArrayList<>();
        
        boolean alreadyAttached;
        //For each of the forms in the forms array
        for(String formItem : forms) {
            //Sets already attached to false
            alreadyAttached = false;
            //For each item in the oldList, compares it to the current forms and if they match then it gets added to the keepList
            for(FormAttachmentObject formAttachmentObject : oldList) {
                if(formAttachmentObject.getFormId().toString().equals(formItem)) {
                    alreadyAttached = true;
                    keepList.add(formAttachmentObject);
                }
            }
            //If the forms is not already attached then attaches it to the report
            if(!alreadyAttached) {
                newList.add(formItem);
            }
        }

        //Compares the oldList and the keepList to see which one are being kept.
        for(FormAttachmentObject formAttachmentObject : oldList) {
            //If the item from the oldList is not in the keepList, then it sets it as deleted
            if(!keepList.contains(formAttachmentObject)) {
                detachFormConsult(formAttachmentObject.getFormId().toString(), consultationId, formType);
                if (editOnOcean) {
                  OceanEReferralAttachmentUtil.detachOceanEReferralConsult(
                      formAttachmentObject.getFormId().toString(), formType);
                }
            }
        }
        //Attaches all the items in the newList 
        for(String formItem : newList) {
            attachFormConsult(providerNumber, formItem, consultationId, formType);
          if (editOnOcean) {
            OceanEReferralAttachmentUtil.attachOceanEReferralConsult(formItem, demographicNo, formType);
          }
        }
    }

    private static void detachFormConsult(String formNo, String consultId, String formType) {
    	List<ConsultDocs> consultDocs = consultDocsDao.findByRequestIdDocNoDocType(Integer.parseInt(consultId), Integer.parseInt(formNo), formType);
    	for(ConsultDocs consultDoc:consultDocs) {
    		consultDoc.setDeleted("Y");
    		consultDocsDao.merge(consultDoc);
    	}
    }

    static void attachFormConsult(String providerNo, String formNo, String consultId, String formType) {
    	ConsultDocs consultDoc = new ConsultDocs();
    	consultDoc.setRequestId(Integer.parseInt(consultId));
    	consultDoc.setDocumentNo(Integer.parseInt(formNo));
    	consultDoc.setDocType(formType);
    	consultDoc.setAttachDate(new Date());
    	consultDoc.setProviderNo(providerNo);
    	consultDocsDao.persist(consultDoc);
    }


}
