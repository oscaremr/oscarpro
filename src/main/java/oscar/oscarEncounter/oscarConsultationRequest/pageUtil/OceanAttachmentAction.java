/*
 * Copyright (c) 2024 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.oscarEncounter.oscarConsultationRequest.pageUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.EReferAttachmentDao;
import org.oscarehr.common.model.EReferAttachment;
import org.oscarehr.common.model.EReferAttachmentData;
import org.oscarehr.common.model.OceanWorkflowTypeEnum;
import org.oscarehr.util.SpringUtils;

public class OceanAttachmentAction extends Action {

  private final EReferAttachmentDao eReferAttachmentDao;

  public OceanAttachmentAction() {
    eReferAttachmentDao = SpringUtils.getBean(EReferAttachmentDao.class);
  }

  public OceanAttachmentAction(final EReferAttachmentDao eReferAttachmentDao) {
    this.eReferAttachmentDao = eReferAttachmentDao;
  }

  public ActionForward execute(
      final ActionMapping mapping, final ActionForm form, final HttpServletRequest request,
      final HttpServletResponse response) throws IOException {
    String demographicNumberString = StringUtils.trimToEmpty(request.getParameter("demographicNumber"));
    String documents = StringUtils.trimToEmpty(request.getParameter("documents"));
    String oceanWorkflowType = StringUtils.trimToEmpty(request.getParameter("type"));

    try {
      validateData(demographicNumberString, documents, oceanWorkflowType);
    } catch (IllegalArgumentException e) {
      response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
      response.getWriter().write(e.getMessage());
      return null;
    }

    EReferAttachment eReferAttachment = new EReferAttachment(Integer.parseInt(
        demographicNumberString));

    List<EReferAttachmentData> attachments = addAttachments(documents, eReferAttachment);

    eReferAttachment.setAttachments(attachments);
    eReferAttachment.setType(oceanWorkflowType);

    EReferAttachment savedEReferAttachment = eReferAttachmentDao.saveEntity(eReferAttachment);

    response.setStatus(HttpServletResponse.SC_OK);
    response.getWriter().write(savedEReferAttachment.getId().toString());

    return null;
  }

  private void validateData(
      final String demographicNo, final String documents, final String attachmentType) {
    if (StringUtils.isEmpty(demographicNo)
        || !StringUtils.isNumeric(demographicNo)
        || Integer.parseInt(demographicNo) < 0) {
      throw new IllegalArgumentException("Invalid demographic number.");
    }
    if (StringUtils.isEmpty(documents)) {
      throw new IllegalArgumentException("Documents were not provided.");
    }
    if (StringUtils.isEmpty(attachmentType) || !isValidAttachmentType(attachmentType)) {
      throw new IllegalArgumentException("Invalid attachment type.");
    }
  }

  private boolean isValidAttachmentType(final String attachmentType) {
    try {
      OceanWorkflowTypeEnum.valueOf(attachmentType);
      return true;
    } catch (IllegalArgumentException e) {
      return false;
    }
  }

  private List<EReferAttachmentData> addAttachments(final String documents,
      final EReferAttachment eReferAttachment) {
    List<EReferAttachmentData> attachments = new ArrayList<>();
    String[] splitDocuments = documents.split("\\|");

    for (String document : splitDocuments) {
      String type = document.replaceAll("\\d", "");
      Integer id = Integer.parseInt(document.substring(type.length()));
      EReferAttachmentData attachmentData = new EReferAttachmentData(eReferAttachment, id, type);
      attachments.add(attachmentData);
    }

    return attachments;
  }
}
