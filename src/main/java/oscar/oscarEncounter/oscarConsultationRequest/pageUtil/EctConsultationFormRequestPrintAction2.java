/**
 * Copyright (c) 2008-2012 Indivica Inc.
 *
 * <p>This software is made available under the terms of the GNU General Public License, Version 2,
 * 1991 (GPLv2). License details are available via "indivica.ca/gplv2" and
 * "gnu.org/licenses/gpl-2.0.html".
 */

/*
 * EctConsultationFormRequestPrintAction.java
 *
 * Created on November 19, 2007, 4:05 PM
 */

package oscar.oscarEncounter.oscarConsultationRequest.pageUtil;

import ca.kai.printable.Printable;
import ca.kai.printable.PrintableAttachmentType;
import ca.oscarpro.common.http.OscarProHttpService;
import ca.oscarpro.service.OscarProConnectorService.OscarProConnectorServiceException;
import com.google.gson.Gson;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.val;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.ConsultationRequestDao;
import org.oscarehr.common.model.ConsultationRequest;
import org.oscarehr.fax.util.PdfCoverPageCreator;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

import oscar.OscarProperties;
import oscar.util.ConcatPDF;
import oscar.util.UtilDateUtilities;

/** Convert submitted preventions into pdf and return file */
public class EctConsultationFormRequestPrintAction2 extends Action {

  private final Logger logger = MiscUtils.getLogger();
  private final SecurityInfoManager securityInfoManager;
  private final PdfCoverPageCreator pdfCoverPageCreator;
  private final ConsultationRequestDao consultationRequestDao;
  private final OscarProHttpService oscarProHttpService;

  private final String CONSULTATION_PRINT_URL =
      "%s/api/v1/printables/print";

  public EctConsultationFormRequestPrintAction2() {
    this(
        SpringUtils.getBean(SecurityInfoManager.class),
        SpringUtils.getBean(PdfCoverPageCreator.class),
        SpringUtils.getBean(ConsultationRequestDao.class),
        SpringUtils.getBean(OscarProHttpService.class)
    );
  }

  public EctConsultationFormRequestPrintAction2(SecurityInfoManager securityInfoManager,
      final PdfCoverPageCreator pdfCoverPageCreator,
      final ConsultationRequestDao consultationRequestDao,
      final OscarProHttpService oscarProHttpService
  ) {
    this.securityInfoManager = securityInfoManager;
    this.pdfCoverPageCreator = pdfCoverPageCreator;
    this.consultationRequestDao = consultationRequestDao;
    this.oscarProHttpService = oscarProHttpService;
  }

  @Override
  public ActionForward execute(
      final ActionMapping mapping,
      final ActionForm form,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws OscarProConnectorServiceException {
    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);

    if (!securityInfoManager.hasPrivilege(loggedInInfo, "_con", "r", null)) {
      throw new SecurityException("missing required security object (_con)");
    }

    String demoNo = request.getParameter("demographicNo");
    String requestId = (String) request.getAttribute("reqId");
    if (request.getParameter("reqId") != null) {
      requestId = request.getParameter("reqId");
    }

    val pdfsToCombine = new ArrayList<Object>();
    if (request.getParameter("coverpage") != null
        && request.getParameter("coverpage").equalsIgnoreCase("true")) {
      addCoverPage(request, pdfsToCombine);
    }

    val requestUrl =
        String.format(CONSULTATION_PRINT_URL, OscarProperties.getOscarProBaseUrl(), requestId);

    val requestData =
        new Gson()
            .toJson(
                new Printable(requestId, PrintableAttachmentType.Consultations), Printable.class);
    val consultationPdf = oscarProHttpService.makeLoggedInPostRequestToPro(requestUrl, requestData, request);
    if (consultationPdf == null) {
      request.setAttribute("printError", "Error getting consultation pdf");
      request.setAttribute("de", demoNo);
      return mapping.findForward("error");
    }
    try {
      pdfsToCombine.add(IOUtils.toString(
          new InputStreamReader(
              consultationPdf.getEntity().getContent(), StandardCharsets.UTF_8)));
    } catch (IOException e) {
      logger.error("IOException occurred inside ConsultationPrintAction", e);
      request.setAttribute("printError", e.getMessage());
      request.setAttribute("de", demoNo);
      return mapping.findForward("error");
    }

    val pdfOutputStream = new ByteArrayOutputStream();
    ConcatPDF.concat(pdfsToCombine, pdfOutputStream);

    response.setContentType("application/pdf");
    response.setHeader(
        "Content-Disposition",
        "inline; filename=\"combinedPDF-"
            + UtilDateUtilities.getToday("yyyy-mm-dd.hh.mm.ss")
            + ".pdf\"");

    try (val outputStream = response.getOutputStream()) {
      outputStream.write(pdfOutputStream.toByteArray());
    } catch (IOException e) {
      logger.error("IOException occurred writing output pdf", e);
    }
    // Set consultation locked after printing
    ConsultationRequest printedConsultation =
        consultationRequestDao.find(Integer.valueOf(requestId));
    printedConsultation.setLocked(true);
    consultationRequestDao.merge(printedConsultation);

    return null;
  }

  private void addCoverPage(final HttpServletRequest request, final ArrayList<Object> pdfList) {
    String note = request.getParameter("note") == null ? "" : request.getParameter("note");
    val coverPageBytes =
        pdfCoverPageCreator.createStandardCoverPage(note, request.getParameter("specialist"));
    pdfList.add(new ByteArrayInputStream(coverPageBytes));
  }
}
