/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarEncounter.oscarConsultationRequest.pageUtil;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.IsPropertiesOn;
import org.oscarehr.common.dao.ClinicDAO;
import org.oscarehr.common.dao.ConsultationRequestArchiveDao;
import org.oscarehr.common.dao.ConsultationRequestDao;
import org.oscarehr.common.dao.ConsultationRequestExtArchiveDao;
import org.oscarehr.common.dao.ConsultationRequestExtDao;
import org.oscarehr.common.dao.Hl7TextInfoDao;
import org.oscarehr.common.dao.ProfessionalSpecialistDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.hl7.v2.oscar_to_oscar.OruR01;
import org.oscarehr.common.hl7.v2.oscar_to_oscar.OruR01.ObservationData;
import org.oscarehr.common.hl7.v2.oscar_to_oscar.RefI12;
import org.oscarehr.common.hl7.v2.oscar_to_oscar.SendingUtils;
import org.oscarehr.common.model.AbstractModel;
import org.oscarehr.common.model.Clinic;
import org.oscarehr.common.model.ConsultDocs;
import org.oscarehr.common.model.ConsultationRequest;
import org.oscarehr.common.model.ConsultationRequestArchive;
import org.oscarehr.common.model.ConsultationRequestExt;
import org.oscarehr.common.model.ConsultationRequestExtArchive;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DigitalSignature;
import org.oscarehr.common.model.Hl7TextInfo;
import org.oscarehr.common.model.ProfessionalSpecialist;
import org.oscarehr.common.model.Provider;
import org.oscarehr.managers.DemographicManager;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.DigitalSignatureUtils;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;
import org.oscarehr.util.WebUtils;

import oscar.dms.EDoc;
import oscar.dms.EDocUtil;
import oscar.log.LogConst;
import oscar.log.LogService;
import oscar.oscarLab.ca.all.pageUtil.LabPDFCreator;
import oscar.oscarLab.ca.on.CommonLabResultData;
import oscar.oscarLab.ca.on.LabResultData;
import oscar.util.ParameterActionForward;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.v26.message.ORU_R01;
import ca.uhn.hl7v2.model.v26.message.REF_I12;

import com.lowagie.text.DocumentException;

public class EctConsultationFormRequestAction extends Action {

  private static final Logger logger=MiscUtils.getLogger();
  private final LogService logService;
  private final SecurityInfoManager securityInfoManager;
  private final ConsultationRequestDao consultationRequestDao;
  private final SystemPreferencesDao systemPreferencesDao;
  private final ConsultationRequestArchiveDao consultationRequestArchiveDao;
  private final ConsultationRequestExtDao consultationRequestExtDao;
  private final ConsultationRequestExtArchiveDao consultationRequestExtArchiveDao;
  private final ProfessionalSpecialistDao professionalSpecialistDao;

  private final static String[] DATE_FORMATS = new String[] {"yyyy-MM-dd", "yyyy/MM/dd"};
  private final static String SURROUNDING_QUOTES_REGEX = "^\"|\"$";
  private final static String ESCAPED_CHARACTER_REGEX = "\\\\";
  private final static String NEWLINE_REGEX = "\\\\n";
  private final static String NEWLINE_CHARACTER = "&#13;";

  public EctConsultationFormRequestAction() {
    this(
        SpringUtils.getBean(LogService.class),
        SpringUtils.getBean(SecurityInfoManager.class),
        SpringUtils.getBean(ConsultationRequestDao.class),
        SpringUtils.getBean(SystemPreferencesDao.class),
        SpringUtils.getBean(ConsultationRequestArchiveDao.class),
        SpringUtils.getBean(ConsultationRequestExtDao.class),
        SpringUtils.getBean(ConsultationRequestExtArchiveDao.class),
        SpringUtils.getBean(ProfessionalSpecialistDao.class));
  }

  public EctConsultationFormRequestAction(
      final LogService logService,
      final SecurityInfoManager securityInfoManager,
      final ConsultationRequestDao consultationRequestDao,
      final SystemPreferencesDao systemPreferencesDao,
      final ConsultationRequestArchiveDao consultationRequestArchiveDao,
      final ConsultationRequestExtDao consultationRequestExtDao,
      final ConsultationRequestExtArchiveDao consultationRequestExtArchiveDao,
      final ProfessionalSpecialistDao professionalSpecialistDao) {
    this.logService = logService;
    this.securityInfoManager = securityInfoManager;
    this.consultationRequestDao = consultationRequestDao;
    this.systemPreferencesDao = systemPreferencesDao;
    this.consultationRequestArchiveDao = consultationRequestArchiveDao;
    this.consultationRequestExtDao = consultationRequestExtDao;
    this.consultationRequestExtArchiveDao = consultationRequestExtArchiveDao;
    this.professionalSpecialistDao = professionalSpecialistDao;
  }

  @Override
  public ActionForward execute(
      final ActionMapping mapping,
      final ActionForm form,
      final HttpServletRequest request,
      final HttpServletResponse response)
      throws ServletException, IOException {

    if (!securityInfoManager.hasPrivilege(
        LoggedInInfo.getLoggedInInfoFromSession(request), "_con", "w", null)) {
      throw new SecurityException("missing required security object (_con)");
    }

    LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    EctConsultationFormRequestForm frm = (EctConsultationFormRequestForm) form;

    String sendTo = frm.getSendTo();
    String submission = frm.getSubmission();
    String providerNo = frm.getProviderNo();
    String demographicNo = frm.getDemographicNo();
    String requestId = "";

    boolean newSignature =
        request.getParameter("newSignature") != null
            && request.getParameter("newSignature").equalsIgnoreCase("true");
    String signatureId = null;

    if (submission.startsWith("Submit")) {
      try {
        if (newSignature) {
          signatureId =
              getNewSignatureId(loggedInInfo, request, demographicNo, frm.getSignatureImg());
        }

        ConsultationRequest consult = new ConsultationRequest();
        consult.setSignatureImg(signatureId);
        setSharedSaveAndUpdateData(frm, consult);
        consult.setCreatorName(request.getParameter("creatorName"));
        consultationRequestDao.persist(consult);

        Integer specId = new Integer(frm.getSpecialist());
        ProfessionalSpecialist professionalSpecialist = professionalSpecialistDao.find(specId);
        if (professionalSpecialist != null) {
          consult.setProfessionalSpecialist(professionalSpecialist);
          consultationRequestDao.merge(consult);
        }
        MiscUtils.getLogger().debug("saved new consult id " + consult.getId());
        requestId = String.valueOf(consult.getId());

        Enumeration e = request.getParameterNames();
        while (e.hasMoreElements()) {
          String name = (String) e.nextElement();
          if (name.equals("fromlist2")) {
            if (!request.getParameter(name).isEmpty()) {
              HttpSession session = request.getSession();
              String value = (String) session.getAttribute("examination");
              String str = request.getParameter("specialProblem");
              if (null != str && str.trim().length() > 0) {
                if (value == null || value.trim().length() > 0 && !value.contains(str)) {
                  value = str;
                  value = value.replaceAll("\n", "");
                  value = value.replaceAll("<br>", "\n");
                }
              }
              if (value == null) {
                value = "";
              }
              consultationRequestExtDao.persist(createExtEntry(requestId, "specialProblem", value));
            }
          }

          if (name.startsWith("ext_")) {
            String value = request.getParameter(name);
            consultationRequestExtDao.persist(
                createExtEntry(requestId, name.substring(name.indexOf("_") + 1), value));
          }
        }
        saveAttachments(frm, providerNo, requestId);
      } catch (ParseException e) {
        MiscUtils.getLogger().error("Invalid Date", e);
      }

      request.setAttribute("reqId", requestId);
      request.setAttribute("transType", "2");
    } else if (submission.equals("updateLocked")) {
      try {
        updateLockedConsult(request, frm);
      } catch (ParseException e) {
        MiscUtils.getLogger().error("Invalid Date", e);
      }
      request.setAttribute("transType", "1");
    } else if (submission.startsWith("Update")) {
      requestId = frm.getRequestId();
      try {
        if (newSignature) {
          signatureId =
              getNewSignatureId(loggedInInfo, request, demographicNo, frm.getSignatureImg());
        } else {
          signatureId = frm.getSignatureImg();
        }

        ConsultationRequest consult = consultationRequestDao.find(new Integer(requestId));
        ConsultationRequestArchive consultArchive = new ConsultationRequestArchive(consult);
        consult.setSignatureImg(signatureId);

        /*
         * If Consultant: was changed to "blank/No Consultant Saved" we
         * don't want to try and create an Integer out of the specId as
         * it will throw a NumberForamtException
         */
        String specIdStr = frm.getSpecialist();
        ProfessionalSpecialist professionalSpecialist = null;

        if (StringUtils.isNotEmpty(specIdStr)) {
          Integer specId = new Integer(frm.getSpecialist());
          professionalSpecialist = professionalSpecialistDao.find(specId);
        }
        consult.setProfessionalSpecialist(professionalSpecialist);
        setSharedSaveAndUpdateData(frm, consult);

        consultationRequestDao.merge(consult);

        saveArchive(request, requestId, consult, consultArchive);

        HttpSession session = request.getSession();
        String value = (String) session.getAttribute("examination");
        String specialProblem = request.getParameter("specialProblem");
        if (StringUtils.isEmpty(specialProblem)) {
          specialProblem = (String) session.getAttribute("specialProblem");
        }
        if (value != null) {
          if (StringUtils.isNotBlank(specialProblem) && !value.contains(specialProblem)) {
            value = specialProblem;
          }
        } else if (StringUtils.isNotBlank(specialProblem)) {
          value = specialProblem;
        }
        if (value == null) {
          value = "";
        }
        consultationRequestExtDao.persist(createExtEntry(requestId, "specialProblem", value));
      } catch (ParseException e) {
        MiscUtils.getLogger().error("Error", e);
      }

      request.setAttribute("transType", "1");
    } else if (submission.equalsIgnoreCase("And Print Preview")) {
      requestId = frm.getRequestId();
    }
    frm.setRequestId("");
    request.setAttribute("teamVar", sendTo);

    return getActionForward(mapping, request, loggedInInfo, submission, demographicNo, requestId);
  }

  private ActionForward getActionForward(final ActionMapping mapping, final HttpServletRequest request,
      final LoggedInInfo loggedInInfo, final String submission, final String demographicNo, final String requestId) {
    boolean consultationFaxEnabled = systemPreferencesDao
        .isReadBooleanPreferenceWithDefault("consultation_fax_enabled", true);

    if (submission.endsWith("And Print Preview")) {
      request.setAttribute("reqId", requestId);
      if (consultationFaxEnabled) {
        return mapping.findForward("printIndivica");
      }
      else if (IsPropertiesOn.propertiesOn("CONSULT_PRINT_PDF")) {
        return mapping.findForward("printpdf");
      } else if (IsPropertiesOn.propertiesOn("CONSULT_PRINT_ALT")) {
        return mapping.findForward("printalt");
      } else {
        return mapping.findForward("print");
      }
    } else if (submission.endsWith("Print And Fax")){
      String printType = null;
      request.setAttribute("reqId", requestId);
      if (consultationFaxEnabled) {
        printType = "printIndivica";
      }
      else if (IsPropertiesOn.propertiesOn("CONSULT_PRINT_PDF")) {
        printType ="printpdf";
      } else if (IsPropertiesOn.propertiesOn("CONSULT_PRINT_ALT")) {
        printType ="printalt";
      } else {
        printType = "print";
      }

      request.setAttribute("printType", printType);
      if (consultationFaxEnabled) {
        return mapping.findForward("faxIndivica");
      }
      else {
        return mapping.findForward("fax");
      }
    } else if (submission.endsWith("And Fax")) {
      request.setAttribute("reqId", requestId);
      if (consultationFaxEnabled) {
        return mapping.findForward("faxIndivica");
      } else {
        return mapping.findForward("fax");
      }
    } else if (submission.endsWith("esend")) {
      // upon success continue as normal with success message
      // upon failure, go to consultation update page with message
      try {
              doHl7Send(loggedInInfo, Integer.parseInt(requestId));
              WebUtils.addLocalisedInfoMessage(request, "oscarEncounter.oscarConsultationRequest.ConfirmConsultationRequest.msgCreatedUpdateESent");
            } catch (Exception e) {
              logger.error("Error contacting remote server.", e);

              WebUtils.addLocalisedErrorMessage(request, "oscarEncounter.oscarConsultationRequest.ConfirmConsultationRequest.msgCreatedUpdateESendError");
          ParameterActionForward forward = new ParameterActionForward(mapping.findForward("failESend"));
          forward.addParameter("de", demographicNo);
          forward.addParameter("requestId", requestId);
        return forward;
            }
    }

    logService.log(
        loggedInInfo,
        submission.startsWith("Update") ? LogConst.UPDATE : LogConst.ADD,
        "Consultation Request",
        requestId,
        demographicNo,
        "requestId=" + requestId);
    ParameterActionForward forward = new ParameterActionForward(mapping.findForward("success"));
    forward.addParameter("de", demographicNo);
    return forward;
  }

  private ConsultationRequestExt createExtEntry(String requestId, String name,String value) {
    ConsultationRequestExt obj = new ConsultationRequestExt();
    obj.setDateCreated(new Date());
    obj.setKey(name);
    obj.setValue(value);
    obj.setRequestId(Integer.parseInt(requestId));
    return obj;
  }

  private void doHl7Send(LoggedInInfo loggedInInfo, Integer consultationRequestId) throws InvalidKeyException, SignatureException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException, IOException, HL7Exception, ServletException {

      ProfessionalSpecialistDao professionalSpecialistDao=(ProfessionalSpecialistDao)SpringUtils.getBean("professionalSpecialistDao");
      Hl7TextInfoDao hl7TextInfoDao=(Hl7TextInfoDao)SpringUtils.getBean("hl7TextInfoDao");
      ClinicDAO clinicDAO=(ClinicDAO)SpringUtils.getBean("clinicDAO");

      ConsultationRequest consultationRequest=consultationRequestDao.find(consultationRequestId);
      ProfessionalSpecialist professionalSpecialist=professionalSpecialistDao.find(consultationRequest.getSpecialistId());
      Clinic clinic=clinicDAO.getClinic();

      // set status now so the remote version shows this status
      consultationRequest.setStatus("2");

      REF_I12 refI12=RefI12.makeRefI12(clinic, consultationRequest);
      SendingUtils.send(loggedInInfo, refI12, professionalSpecialist);

      // save after the sending just in case the sending fails.
      consultationRequestDao.merge(consultationRequest);

      //--- send attachments ---
      Provider sendingProvider=loggedInInfo.getLoggedInProvider();
      DemographicManager demographicManager = SpringUtils.getBean(DemographicManager.class);
      Demographic demographic=demographicManager.getDemographic(loggedInInfo, consultationRequest.getDemographicId());

      //--- process all documents ---
      ArrayList<EDoc> attachments=EDocUtil.listDocs(loggedInInfo, demographic.getDemographicNo().toString(), consultationRequest.getId().toString(), EDocUtil.ATTACHED);
      for (EDoc attachment : attachments)
      {
          ObservationData observationData=new ObservationData();
          observationData.subject=attachment.getDescription();
          observationData.textMessage="Attachment for consultation : "+consultationRequestId;
          observationData.binaryDataFileName=attachment.getFileName();
          observationData.binaryData=attachment.getFileBytes();

          ORU_R01 hl7Message=OruR01.makeOruR01(clinic, demographic, observationData, sendingProvider, professionalSpecialist);
          SendingUtils.send(loggedInInfo, hl7Message, professionalSpecialist);
      }

      //--- process all labs ---
        CommonLabResultData labData = new CommonLabResultData();
        ArrayList<LabResultData> labs = labData.populateLabResultsData(loggedInInfo, demographic.getDemographicNo().toString(), consultationRequest.getId().toString(), CommonLabResultData.ATTACHED);
      for (LabResultData attachment : labs)
      {
        try {
              byte[] dataBytes=LabPDFCreator.getPdfBytes(attachment.getSegmentID(), sendingProvider.getProviderNo());
              Hl7TextInfo hl7TextInfo=hl7TextInfoDao.findLabId(Integer.parseInt(attachment.getSegmentID()));

              ObservationData observationData=new ObservationData();
              observationData.subject=hl7TextInfo.getDiscipline();
              observationData.textMessage="Attachment for consultation : "+consultationRequestId;
              observationData.binaryDataFileName=hl7TextInfo.getDiscipline()+".pdf";
              observationData.binaryData=dataBytes;


              ORU_R01 hl7Message=OruR01.makeOruR01(clinic, demographic, observationData, sendingProvider, professionalSpecialist);
              int statusCode=SendingUtils.send(loggedInInfo, hl7Message, professionalSpecialist);
              if (HttpServletResponse.SC_OK!=statusCode) throw(new ServletException("Error, received status code:"+statusCode));
            } catch (DocumentException e) {
              logger.error("Unexpected error.", e);
            }
      }
    }

    private void updateLockedConsult(
        final HttpServletRequest request,
        final EctConsultationFormRequestForm form) throws ParseException {

        String requestId = form.getRequestId();
        ConsultationRequest consult = consultationRequestDao.find(new Integer(requestId));
        ConsultationRequestArchive consultArchive = new ConsultationRequestArchive(consult);


        if (!StringUtils.isBlank(form.getFollowUpDate())) {
      Date appointmentDateTime = DateUtils.parseDate(form.getAppointmentDate(), DATE_FORMATS);
            consult.setAppointmentDate(appointmentDateTime);

            String appointmentHour = form.getAppointmentHour();
            String appointmentPm = form.getAppointmentPm();
            if ("PM".equals(appointmentPm) && Integer.parseInt(appointmentHour) < 12) {
                appointmentHour = Integer.toString(Integer.parseInt(appointmentHour) + 12);
            } else if ("12".equals(appointmentHour) && "AM".equals(appointmentPm)) {
                appointmentHour = "0";
            }
            if(!StringUtils.isBlank(appointmentHour) && !StringUtils.isBlank(form.getAppointmentMinute())) {
                appointmentDateTime = DateUtils.setHours(appointmentDateTime, new Integer(appointmentHour));
                appointmentDateTime = DateUtils.setMinutes(appointmentDateTime, new Integer(form.getAppointmentMinute()));
                appointmentDateTime = DateUtils.setSeconds(appointmentDateTime, 0);
                consult.setAppointmentTime(appointmentDateTime);
            }
        }
        consult.setStatusText(form.getAppointmentNotes());

        if (!StringUtils.isBlank(form.getFollowUpDate())) {
          consult.setFollowUpDate(DateUtils.parseDate(form.getFollowUpDate(), DATE_FORMATS));
        }

        consult.setStatus(form.getStatus());
        consultationRequestDao.saveEntity(consult);

        saveArchive(request, requestId, consult, consultArchive);
    }

  /**
   * Saves existingConsultation and marks it as a consultation request that has
   * been superseded by supersedingConsultation.
   * Used in cases when saving a new request AND an existing request is being replaced due to changed
   * referrer, mark the previous request as superseded
   * @param existingConsultation existing consult to be superseded
   * @param supersedingConsultation new consult with superseding ID
   */
  private void updateExistingConsultationRequestWithSupersededId(
      final ConsultationRequest existingConsultation,
      final ConsultationRequest supersedingConsultation
  ) {
    existingConsultation.setSupersededById(supersedingConsultation.getId());
    consultationRequestDao.merge(existingConsultation);
  }

  private void setAttachments(final ConsultationRequest consult, final String attachments) {
    if (systemPreferencesDao.isAttachmentManagerConsultationEnabled()) {
      consult.setLegacyAttachment(false);
      consult.setAttachments(stripSurroundingAndEncodedQuotes(attachments));
    } else {
      consult.setLegacyAttachment(true);
    }
  }

  private String getNewSignatureId(
      final LoggedInInfo loggedInInfo,
      final HttpServletRequest request,
      final String demographicNo,
      String signatureImg) {
    if (StringUtils.isBlank(signatureImg)) {
      signatureImg = request.getParameter("newSignatureImg");
      if (signatureImg == null) { signatureImg = ""; }
    }

    boolean signedWithStamp = Boolean.parseBoolean(request.getParameter("signedWithStamp"));
    DigitalSignature signature;
    if (!signedWithStamp) {
      signature =
          DigitalSignatureUtils.storeDigitalSignatureFromTempFileToDB(
              loggedInInfo, signatureImg, Integer.parseInt(demographicNo));
    } else {
      signature =
          DigitalSignatureUtils.storeSignatureStamp(loggedInInfo, Integer.parseInt(demographicNo));
    }
    return signature != null ? String.valueOf(signature.getId()) : null;
  }

  private void setSharedSaveAndUpdateData(
      final EctConsultationFormRequestForm form, final ConsultationRequest consult)
      throws ParseException {
    consult.setProviderNo(form.getProviderNo());
    Date date = DateUtils.parseDate(form.getReferalDate(), DATE_FORMATS);
    consult.setReferralDate(date);
    consult.setServiceId(new Integer(form.getService()));

    consult.setLetterheadName(form.getLetterheadName());
    consult.setLetterheadAddress(form.getLetterheadAddress());
    consult.setLetterheadPhone(form.getLetterheadPhone());
    consult.setLetterheadFax(form.getLetterheadFax());
    try {
      date = DateUtils.parseDate(form.getAppointmentDate(), DATE_FORMATS);
      consult.setAppointmentDate(date);
      try {
        String appointmentHour = form.getAppointmentHour();
        String appointmentPm = form.getAppointmentPm();

        if (appointmentPm.equals("PM") && Integer.parseInt(appointmentHour) < 12) {
          appointmentHour = Integer.toString(Integer.parseInt(appointmentHour) + 12);
        } else if (appointmentHour.equals("12") && appointmentPm.equals("AM")) {
          appointmentHour = "0";
        }
        date = DateUtils.setHours(date, new Integer(appointmentHour));
        date = DateUtils.setMinutes(date, new Integer(form.getAppointmentMinute()));
        consult.setAppointmentTime(date);
      } catch (NumberFormatException nfEx) {
        MiscUtils.getLogger().error("Invalid Time", nfEx);
        consult.setAppointmentTime(null);
      }
    } catch (ParseException pe) {
      if (StringUtils.trimToNull(form.getAppointmentDate()) != null) {
        // only throw error if date is invalid format
        // null dates are intentional for when user wants to clear the date
        MiscUtils.getLogger().error("Invalid Date", pe);
      }
      consult.setAppointmentDate(null);
      consult.setAppointmentTime(null);
    }

    consult.setReasonForReferral(form.getReasonForConsultation());
    consult.setClinicalInfo(form.getClinicalInformation());
    consult.setCurrentMeds(form.getCurrentMedications());
    consult.setAllergies(form.getAllergies());

    consult.setDemographicId(new Integer(form.getDemographicNo()));
    consult.setStatus(form.getStatus());
    consult.setStatusText(form.getAppointmentNotes());
    consult.setSendTo(form.getSendTo());
    consult.setConcurrentProblems(form.getConcurrentProblems());
    consult.setUrgency(form.getUrgency());
    consult.setAppointmentInstructions(form.getAppointmentInstructions());
    consult.setSiteName(form.getSiteName());
    Boolean pWillBook = false;
    if (form.getPatientWillBook() != null) {
      pWillBook = form.getPatientWillBook().equals("1");
    }
    consult.setPatientWillBook(pWillBook);

    try {
      date = DateUtils.parseDate(form.getFollowUpDate(), DATE_FORMATS);
      consult.setFollowUpDate(date);
    } catch (ParseException pe) {
      if (StringUtils.trimToNull(form.getFollowUpDate()) != null) {
        // only throw error if date is invalid format
        // null dates are intentional for when user wants to clear the date
        MiscUtils.getLogger().error("Invalid Date", pe);
      }
      consult.setFollowUpDate(null);
    }

    if (form.getSource() != null && !"null".equals(form.getSource())) {
      consult.setSource(form.getSource());
    } else {
      consult.setSource("");
    }
    setAttachments(consult, form.getAttachments());
  }

  private static void saveAttachments(
      EctConsultationFormRequestForm frm, String providerNo, String requestId) {
    // now that we have consultation id we can save any attached docs as well
    // format of input is D2|L2 for doc and lab
    String[] docs = frm.getDocuments().split("\\|");

    for (int idx = 0; idx < docs.length; ++idx) {
      if (docs[idx].length() > 0) {
        if (docs[idx].charAt(0) == 'D') {
          EDocUtil.attachDocConsult(providerNo, docs[idx].substring(1), requestId);
        } else if (docs[idx].charAt(0) == 'L') {
          ConsultationAttachLabs.attachLabConsult(providerNo, docs[idx].substring(1), requestId);
        } else if (docs[idx].charAt(0) == 'H') {
          ConsultationAttachHRMReports.attachHRMReportConsult(
              providerNo, docs[idx].substring(1), requestId);
        } else if (docs[idx].charAt(0) == 'E') {
          ConsultationAttachEForms.attachFormConsult(providerNo, docs[idx].substring(1), requestId);
        } else if (docs[idx].startsWith(ConsultDocs.DOCTYPE_FORM_PERINATAL)) {
          ConsultationAttachForms.attachFormConsult(
              providerNo,
              docs[idx].replace(ConsultDocs.DOCTYPE_FORM_PERINATAL, ""),
              requestId,
              ConsultDocs.DOCTYPE_FORM_PERINATAL);
        }
      }
    }
  }

  private void saveArchive(
      HttpServletRequest request,
      String requestId,
      ConsultationRequest consult,
      ConsultationRequestArchive consultArchive) {
    consultationRequestArchiveDao.saveEntity(consultArchive);

    List<AbstractModel<?>> consultationRequestExtArchiveList = new ArrayList<>();
    List<ConsultationRequestExt> consultationRequestExtList =
        consultationRequestExtDao.getConsultationRequestExts(consult.getId());
    for (ConsultationRequestExt ext : consultationRequestExtList) {
      ConsultationRequestExtArchive archiveExt =
          new ConsultationRequestExtArchive(consultArchive.getId(), ext);
      archiveExt.setArchiveTimestamp(consultArchive.getArchiveTimestamp());
      consultationRequestExtArchiveList.add(archiveExt);
    }

    consultationRequestExtDao.clear(Integer.parseInt(requestId));
    Enumeration e = request.getParameterNames();
    while (e.hasMoreElements()) {
      String name = (String) e.nextElement();
      if (name.startsWith("ext_")) {
        String value = request.getParameter(name);
        consultationRequestExtDao.persist(
            createExtEntry(requestId, name.substring(name.indexOf("_") + 1), value));
      }
    }

    // Save ext archive
    consultationRequestExtArchiveDao.batchPersist(consultationRequestExtArchiveList, 25);
  }

  private String stripSurroundingAndEncodedQuotes(String input) {
    return input.replaceAll(SURROUNDING_QUOTES_REGEX, "").replaceAll(NEWLINE_REGEX, NEWLINE_CHARACTER)
        .replaceAll(ESCAPED_CHARACTER_REGEX, "");
  }
}
