package oscar.oscarEncounter.oscarConsultationRequest.pageUtil;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.JSONArray;
import org.json.JSONObject;
import org.oscarehr.common.dao.EReferAttachmentDao;
import org.oscarehr.common.model.EReferAttachment;
import org.oscarehr.common.model.EReferAttachmentData;
import org.oscarehr.common.model.EReferAttachmentManagerData;
import org.oscarehr.common.model.OceanWorkflowTypeEnum;
import org.oscarehr.util.SpringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class EReferAction extends Action {

	private final EReferAttachmentDao eReferAttachmentDao;

	public EReferAction(EReferAttachmentDao eReferAttachmentDao) {
		this.eReferAttachmentDao = eReferAttachmentDao;
	}
	
	public EReferAction() {
		this.eReferAttachmentDao = SpringUtils.getBean(EReferAttachmentDao.class);
	}

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) {
		Integer demographicNo = Integer.parseInt(request.getParameter("demographicNo"));
		String documents = StringUtils.trimToEmpty(request.getParameter("documents"));
		String attachments = StringUtils.trimToEmpty(request.getParameter("attachments"));

		if (documents.isEmpty() && attachments.isEmpty()) {
			return null;
		}

		EReferAttachment eReferAttachment = new EReferAttachment(demographicNo);

		if (!documents.isEmpty()) {
			handleDocumentData(documents, eReferAttachment);
		} else {
			handleAttachmentManagerData(attachments, eReferAttachment);
		}

		eReferAttachment.setType(OceanWorkflowTypeEnum.EREFERRAL.name());

		EReferAttachment savedEReferAttachment = eReferAttachmentDao.saveEntity(eReferAttachment);
		try {
			response.getWriter().write(savedEReferAttachment.getId().toString());
		} catch (IOException e) {
			log.error("Error writing eReferAttachment ID to response", e);
		}

		return null;
	}

	void handleDocumentData(final String documents, final EReferAttachment eReferAttachment) {
		List<EReferAttachmentData> attachmentDataList = new ArrayList<>();
		String[] splitDocuments = documents.split("\\|");

		for (String document : splitDocuments) {
			String type = document.replaceAll("\\d", "");
			Integer id = Integer.parseInt(document.substring(type.length()));
			EReferAttachmentData attachmentData = new EReferAttachmentData(eReferAttachment, id, type);
			attachmentDataList.add(attachmentData);
		}
		eReferAttachment.setAttachments(attachmentDataList);
	}

	void handleAttachmentManagerData(final String attachments,
			final EReferAttachment eReferAttachment) {
		List<EReferAttachmentManagerData> attachmentManagerDataList = new ArrayList<>();
		JSONArray attachmentsArray = new JSONArray(attachments);

		for (int i = 0; i < attachmentsArray.length(); i++) {
			JSONObject attachmentJson = attachmentsArray.getJSONObject(i);
			EReferAttachmentManagerData attachmentManagerData = new EReferAttachmentManagerData(
					eReferAttachment, attachmentJson.toString());
			attachmentManagerDataList.add(attachmentManagerData);
		}
		eReferAttachment.setAttachmentManagerData(attachmentManagerDataList);
	}
}
