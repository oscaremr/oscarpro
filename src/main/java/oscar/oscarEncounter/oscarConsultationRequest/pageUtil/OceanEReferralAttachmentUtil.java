/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.oscarEncounter.oscarConsultationRequest.pageUtil;

import java.util.Collections;
import java.util.List;
import lombok.var;
import org.oscarehr.common.dao.EReferAttachmentDao;
import org.oscarehr.common.dao.EReferAttachmentDataDao;
import org.oscarehr.common.model.EReferAttachment;
import org.oscarehr.common.model.EReferAttachmentData;
import org.oscarehr.util.SpringUtils;

public class OceanEReferralAttachmentUtil {
  private static EReferAttachmentDataDao eReferAttachmentDataDao =
      SpringUtils.getBean(EReferAttachmentDataDao.class);
  private static EReferAttachmentDao eReferAttachmentDao =
      SpringUtils.getBean(EReferAttachmentDao.class);

  public static void detachOceanEReferralConsult(String docId, String type) {
    EReferAttachmentData eReferAttachmentData =
        eReferAttachmentDataDao.getByDocumentId(Integer.parseInt(docId), type);
    boolean deleteEReferAttachment = false;
    
    if (eReferAttachmentData != null) {
      List<EReferAttachmentData> eReferAttachmentDataList =
          eReferAttachmentData.geteReferAttachment().getAttachments();
      deleteEReferAttachment = eReferAttachmentDataList.size() == 1;

      eReferAttachmentDataDao.remove(eReferAttachmentData.getId());
      if (deleteEReferAttachment) {
        eReferAttachmentDao.remove(eReferAttachmentData.getId().getEReferAttachment().getId());
      }
    }
  }

  public static void attachOceanEReferralConsult(String documentId, String demoNo, String type) {
    var eReferAttachment = 
        eReferAttachmentDao.getLatestCreatedByDemographic(Integer.parseInt(demoNo));
    if (eReferAttachment == null) {
      eReferAttachment = new EReferAttachment(Integer.parseInt(demoNo));
    }
    var attachmentData = 
        new EReferAttachmentData(eReferAttachment, Integer.parseInt(documentId), type);
    
    if (eReferAttachment.getId() != null) {
      // If an eReferAttachment record exists, save the new attachment data only
      eReferAttachmentDataDao.persist(attachmentData);
    } else {
      // If an eReferAttachment doesn't exist, save the attachment data with the new 
      // eReferAttachment
      var attachments = Collections.singletonList(attachmentData);
      eReferAttachment.setAttachments(attachments);
      eReferAttachmentDao.persist(eReferAttachment);
    }
  }
}
