/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. 
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */


package oscar.oscarEncounter.oscarConsultationRequest.config.pageUtil;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import lombok.var;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.oscarehr.common.dao.PropertyDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.Property;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.managers.ConsultationManager;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

public class EctConRequestSettingsAction extends Action {
    private static PropertyDao propertyDao = SpringUtils.getBean(PropertyDao.class);
    private static SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
    private static ConsultationManager manager = SpringUtils.getBean(ConsultationManager.class);
    private static SecurityInfoManager securityInfoManager = SpringUtils.getBean(SecurityInfoManager.class);

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
        HttpServletResponse response)
        throws ServletException, IOException {
      if (!securityInfoManager.hasPrivilege(LoggedInInfo.getLoggedInInfoFromSession(request),
          "_con", "u", null)) {
        throw new SecurityException("missing required security object (_con)");
      }
      val ectConRequestSettingsFormForm = (EctConRequestSettingsForm) form;
      mergeProperty(manager.CON_DEFAULT_INSTRUCTIONS,
          ectConRequestSettingsFormForm.getDefaultAppointmentInstructions());
      updateFilterMeasurementsByAppointmentSystemPreference(request);
      updateReferringPractitionerSelection(request);
      mergeProperty(
          manager.CON_DISPLAY_PROVIDER_ENABLED,
          String.valueOf(ectConRequestSettingsFormForm.isDisplayProviderOnConsultationPdf()));
      request.setAttribute("CONSULTATION_REQUEST_SETTINGS_UPDATED", "updated");
      return mapping.findForward("success");
    }

    private void updateReferringPractitionerSelection(final HttpServletRequest request) {
    	for (var type : ConsultationManager.ConsultationReferringPractitioner.values()) {
            val referringPractitionerSelection = checkboxChecked(request.getParameter(type.getConsultationManagerReferringString()));
            var referringPractitioner = systemPreferencesDao.findPreferenceByName(type.getConsultationManagerReferringString());
            if (referringPractitioner == null) {
                referringPractitioner = new SystemPreferences(type.getConsultationManagerReferringString());
            }
            referringPractitioner.setValue(referringPractitionerSelection);
            systemPreferencesDao.saveEntity(referringPractitioner);
        }
	}

    private void updateFilterMeasurementsByAppointmentSystemPreference(final HttpServletRequest request) {
        SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
        val filterMeasurementByAppointmentParam = request
                .getParameter(ConsultationManager.CON_FILTER_OCULAR_MEASUREMENTS_BY_APPOINTMENT);
        if (filterMeasurementByAppointmentParam != null) {
            var filterMeasurementPreference = systemPreferencesDao
                    .findPreferenceByName(ConsultationManager.CON_FILTER_OCULAR_MEASUREMENTS_BY_APPOINTMENT);
            if (filterMeasurementPreference == null) {
                filterMeasurementPreference =
                        new SystemPreferences(ConsultationManager.CON_FILTER_OCULAR_MEASUREMENTS_BY_APPOINTMENT);
            }
            filterMeasurementPreference.setValue(filterMeasurementByAppointmentParam);
            systemPreferencesDao.saveEntity(filterMeasurementPreference);
        }
    }
    
    private String checkboxChecked(final String valueFromUi) {
    	return "on".equalsIgnoreCase(valueFromUi) ? "true" : "false";
    }

    private void mergeProperty(final String propertyName, final String value) {
        var defaultAppointmentInstructions = new Property(propertyName);
        val results = propertyDao.findByName(propertyName);
        if (!results.isEmpty()) {
            defaultAppointmentInstructions = results.get(0);
            defaultAppointmentInstructions.setValue(value);
            propertyDao.merge(defaultAppointmentInstructions);
        }
    }
}
