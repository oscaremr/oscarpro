/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package apps.health.pillway;

import apps.health.pillway.PillwayLog.Method;
import org.oscarehr.common.dao.DemographicPharmacyDao;
import org.oscarehr.common.dao.PharmacyInfoDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.model.PharmacyInfo;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.common.model.UserProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PillwayManager {

  public static final String PILLWAY_ID = "PILLWAY";

  private final DemographicPharmacyDao demographicPharmacyDao;
  private final PillwayLogDao pillwayLogDao;
  private final PharmacyInfoDao pharmacyInfoDao;
  private final SystemPreferencesDao systemPreferencesDao;
  private final UserPropertyDAO userPropertyDAO;

  @Autowired
  public PillwayManager(
      DemographicPharmacyDao demographicPharmacyDao,
      PharmacyInfoDao pharmacyInfoDao,
      PillwayLogDao pillwayLogDao,
      SystemPreferencesDao systemPreferencesDao,
      UserPropertyDAO userPropertyDAO) {
    this.demographicPharmacyDao = demographicPharmacyDao;
    this.pharmacyInfoDao = pharmacyInfoDao;
    this.pillwayLogDao = pillwayLogDao;
    this.systemPreferencesDao = systemPreferencesDao;
    this.userPropertyDAO = userPropertyDAO;
  }

  /**
   * Check if a pharmacy has the pillway identifier
   *
   * @param pharmacyId the id of the pharmacy to check
   * @return true if the identifier is PILLWAY and false otherwise
   */
  public boolean isPillwayPharmacy(Integer pharmacyId) {
    PharmacyInfo pharmacy = pharmacyInfoDao.getPharmacy(pharmacyId);
    return pharmacy != null && PILLWAY_ID.equals(pharmacy.getServiceLocationIdentifier());
  }

  /**
   * Add entry to the Pillway log.
   *
   * <p>Will not log if pharmacy identifier does not match PILLWAY.
   *
   * @param pharmacyId the id of the pharmacy being subscribed to.
   * @param rxId the id of the prescription that was sent to Pillway
   * @param providerNo the id of the provider who sent the prescription
   * @param demographicNo the demographic the prescription was for
   * @param method how the prescription was communicated to Pillway
   */
  public PillwayLog log(
      Integer pharmacyId, String rxId, String providerNo, Integer demographicNo, Method method) {
    if (isPillwayPharmacy(pharmacyId)) {
      PillwayLog entry = new PillwayLog(rxId, providerNo, demographicNo, method);
      pillwayLogDao.persist(entry);
      return entry;
    }
    return null;
  }

  /**
   * Add Pillway pharmacy to demographic's preferred pharmacies.
   *
   * @param demographicNo the demographic to add the pharmacy to
   */
  public void addToDemographic(Integer demographicNo) {
    if (!isAddToDemographicEnabled()) {
      return;
    }
    PharmacyInfo pillway = pharmacyInfoDao.getPharmacyByIdentifier(PILLWAY_ID);
    if (pillway != null) {
      demographicPharmacyDao.addPharmacyToDemographic(pillway.getId(), demographicNo);
    }
  }

  /**
   * Check if Pillway add pharmacy to demographic is enabled
   *
   * <p>Defaults to true
   *
   * @return true if Pillway is enabled and false otherwise
   */
  public boolean isAddToDemographicEnabled() {
    Boolean enabled =
        SystemPreferences.asBoolean(
            systemPreferencesDao.findPreferenceByName(
                SystemPreferences.PILLWAY_ADD_TO_DEMOGRAPHIC_ENABLED));
    return enabled == null || enabled;
  }

  /**
   * Check if Pillway home delivery button is enabled for a provider.
   *
   * <p> Defaults to true
   *
   * @param providerNo the provider to checkk for
   * @return true if enabled and false otherwise
   */
  public boolean isPillwayButtonEnabled(String providerNo) {
    UserProperty pillwayButtonEnabled =
        userPropertyDAO.getProp(providerNo, UserProperty.PILLWAY_BUTTON_ENABLED);
    return pillwayButtonEnabled == null || Boolean.parseBoolean(pillwayButtonEnabled.getValue());
  }
}
