/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package apps.health.pillway;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.oscarehr.common.model.AbstractModel;

@Entity
public class PillwayLog extends AbstractModel<Object> {

  public enum Method {
    API, FALLBACK, FAX
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Integer id;

  public String rxId;

  public String providerNo;

  public Integer demographicNo;

  @Enumerated(EnumType.ORDINAL)
  public Method method;

  public Date timestamp;

  public PillwayLog() { }

  public PillwayLog(
      String rxId,
      String providerNo,
      Integer demographicNo,
      Method method) {
    this.rxId = rxId;
    this.providerNo = providerNo;
    this.demographicNo = demographicNo;
    this.method = method;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getRxId() {
    return rxId;
  }

  public void setRxId(String rxId) {
    this.rxId = rxId;
  }

  public String getProviderNo() {
    return providerNo;
  }

  public void setProviderNo(String providerNo) {
    this.providerNo = providerNo;
  }

  public Integer getDemographicNo() {
    return demographicNo;
  }

  public void setDemographicNo(Integer demographicNo) {
    this.demographicNo = demographicNo;
  }

  public Method getMethod() {
    return method;
  }

  public void setMethod(Method method) {
    this.method = method;
  }

  public Date getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }
}
