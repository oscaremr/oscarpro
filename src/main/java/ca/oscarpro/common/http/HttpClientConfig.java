package ca.oscarpro.common.http;

import org.apache.http.client.HttpClient;

public interface HttpClientConfig {

  HttpClient getHttpClient();
}
