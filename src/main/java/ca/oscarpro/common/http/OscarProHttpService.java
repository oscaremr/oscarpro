package ca.oscarpro.common.http;

import java.io.IOException;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.util.WebUtils;

@Service
@Slf4j
public class OscarProHttpService {

  @Nullable
  public HttpResponse makeLoggedInPostRequestToPro(
      final @NonNull String url,
      final @NonNull String body,
      final @NonNull HttpServletRequest request) {
    val post = new HttpPost(url);
    post.setEntity(new StringEntity(body, ContentType.APPLICATION_JSON));
    addKaiSsoCookieToRequest(post, request);
    return makeLoggedInRequestToPro(post);
  }

  @Nullable
  public HttpResponse makeLoggedInGetRequestToPro(
      final @NonNull String url,
      final @NonNull HttpServletRequest request) {
    val get = new HttpGet(url);
    addKaiSsoCookieToRequest(get, request);
    return makeLoggedInRequestToPro(get);
  }

  private void addKaiSsoCookieToRequest(
      final @NonNull HttpRequestBase getOrPost,
      final @NonNull HttpServletRequest request) {
    val cookie = WebUtils.getCookie(request, "kai-sso");
    getOrPost.addHeader("Cookie", cookie.getName() + "=" + cookie.getValue());
  }

  @Nullable
  protected HttpResponse makeLoggedInRequestToPro(final HttpRequestBase getOrPost) {
    try {
      return HttpClientBuilder.create().build().execute(getOrPost);
    } catch (IOException e) {
      log.error("Error making request to Pro", e);
    }
    return null;
  }
}
