package ca.oscarpro.common.http;

import ca.oscarpro.common.util.CertificateUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Collections;
import java.util.List;
import javax.net.ssl.SSLContext;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.ssl.SSLContexts;
import org.springframework.stereotype.Component;
import oscar.OscarProperties;

@Slf4j
@Component
public class OscarClassicHttpClient implements HttpClientConfig {

  public final static String KEYSTORE_INSTANCE = "JKS";

  public final static String EHR_KEYSTORE_FILE = "ehr.keystore_file";

  public final static String EHR_KEYSTORE_PASSWORD = "ehr.keystore_password";

  public final static String EHR_CERTIFICATE_ALIAS = "classic-pro-connector";

  public final static String EHR_CERTIFICATE_HEADER_NAME = "X-OSCAR-Pro-Certificate";

  public final static String EHR_KEYSTORE_ENABLED = "ehr.keystore.enabled";

  /**
   * Builds HttpClient to execute requests in Oscar Classic that require the OSCAR certificate for
   * authentication by adding SSLContext with the keystore material loaded to the HttpClient.
   *
   * @return HttpClient
   */
  public HttpClient getHttpClient() {
    val keystorePath = OscarProperties.getInstance().getProperty(EHR_KEYSTORE_FILE);
    try {
      if (OscarProperties.getInstance().getBooleanProperty(EHR_KEYSTORE_ENABLED, "true")) {
        return buildHttpClient(createClassicProCertificateHeaders(keystorePath));
      } else {
        return HttpClientBuilder.create().build();
      }

    } catch (NoSuchAlgorithmException | KeyStoreException | CertificateException | IOException e) {
        log.error("Error creating OSCAR Pro HTTP Client with keystore path: " + keystorePath, e);
    }
    return null;
  }

  private CloseableHttpClient buildHttpClient(final List<BasicHeader> headers)
          throws CertificateException,
          IOException, KeyStoreException, NoSuchAlgorithmException {
    return HttpClientBuilder.create()
            .setDefaultHeaders(headers).build();
  }

  private List<BasicHeader> createClassicProCertificateHeaders(String keystorePath)
          throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {
    return Collections.singletonList(new BasicHeader(
            EHR_CERTIFICATE_HEADER_NAME,
            CertificateUtils.encodeCertificateAsBase64Url(
                    getKeyStore(keystorePath).getCertificate(EHR_CERTIFICATE_ALIAS)
            )
    ));
  }

  private SSLContext createSslContext(final KeyStore keystore)
      throws UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
    try {
      val keyStorePassword = OscarProperties.getInstance()
          .getProperty(EHR_KEYSTORE_PASSWORD)
          .toCharArray();
      return SSLContexts.custom()
          .loadKeyMaterial(keystore, keyStorePassword)
          .build();
    } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException |
             UnrecoverableKeyException e) {
      log.error("createSslContext - unable to create SSLContext.", e);
      throw e;
    }

  }

  private KeyStore getKeyStore(final String keyStorePath)
      throws CertificateException, KeyStoreException, IOException, NoSuchAlgorithmException {
    val keyStoreFile = new File(keyStorePath);
    try (FileInputStream inputStream = new FileInputStream(keyStoreFile)) {
      val keyStore = KeyStore.getInstance(KEYSTORE_INSTANCE);
      val keyStorePassword = OscarProperties.getInstance()
          .getProperty(EHR_KEYSTORE_PASSWORD)
          .toCharArray();
      keyStore.load(inputStream, keyStorePassword);
      return keyStore;
    } catch (KeyStoreException | IOException | CertificateException | NoSuchAlgorithmException e) {
        log.error("getKeyStore - unable to load keystore.", e);
        throw e;
    }
  }
}
