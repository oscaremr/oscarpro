package ca.oscarpro.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class CalendarUtils {

  private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd-MMM-yyyy");

  public static Calendar[] getDateRangeFromDates(final String startDateStr, final String endDateStr) {
    val trimmedStartDate = StringUtils.trimToNull(startDateStr);
    val trimmedEndDate = StringUtils.trimToNull(endDateStr);
    try {
      return new Calendar[]{parseDate(trimmedStartDate), parseDate(trimmedEndDate, true)};
    } catch (IllegalArgumentException e) {
      log.error("Error parsing dates: Start Date - " + startDateStr + ", End Date - " + endDateStr, e);
      return new Calendar[]{null, null};
    }
  }

  private static Calendar parseDate(final String dateStr) throws IllegalArgumentException {
    return parseDate(dateStr, false);
  }

  private static Calendar parseDate(final String dateStr, final boolean setToEndOfDay)
      throws IllegalArgumentException {
    if (StringUtils.isBlank(dateStr)) {
      return null;
    }
    try {
      val date = FORMATTER.parse(dateStr);
      val calendarDate = Calendar.getInstance();
      calendarDate.setTime(date);
      if (setToEndOfDay) {
        setTimeToEndOfDay(calendarDate);
      }
      return calendarDate;
    } catch (ParseException e) {
      throw new IllegalArgumentException("Invalid date format: " + dateStr, e);
    }
  }

  private static void setTimeToEndOfDay(final Calendar calendar) {
    calendar.set(Calendar.HOUR_OF_DAY, 23);
    calendar.set(Calendar.MINUTE, 59);
    calendar.set(Calendar.SECOND, 59);
  }
}
