package ca.oscarpro.common.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;

/** Utility class for accessing and manipulating HTTP response data. */
public class ResponseUtils {

  public static String readStringDataFromResponseEntityContent(final HttpResponse response)
      throws IOException {
    return IOUtils.toString(
        new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8));
  }

  public static void writeFileDataToResponseOutputStream(
      final HttpServletResponse response, final String filePath) throws IOException {
    try (val outputStream = response.getOutputStream()) {
      outputStream.write(FileUtils.readFileToByteArray(new File(filePath)));
    }
  }
}
