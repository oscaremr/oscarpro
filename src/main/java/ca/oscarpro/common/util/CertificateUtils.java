package ca.oscarpro.common.util;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.util.Base64;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import oscar.OscarProperties;

@Slf4j
public class CertificateUtils {

  public final static String EHR_KEYSTORE_FILE = "ehr.keystore_file";

  public final static String EHR_KEYSTORE_PASSWORD = "ehr.keystore_password";

  public static final String X509_CERTIFICATE_HEADER_FORMAT =
          "-----BEGIN CERTIFICATE-----\n%s\n-----END CERTIFICATE-----";

  private static final String CERTIFICATE_INSTANCE = "X.509";

  private static final String ENCODING = "UTF-8";

  private static final String KEYSTORE_TYPE = "JKS";

  /**
   * Returns a X509Certificate array from a URL encoded certificate.
   *
   * @param urlEncodedCert The URL encoded certificate in String format.
   * @return X509Certificate array.
   * @throws UnsupportedEncodingException For unsupported character encoding.
   * @throws CertificateException For parsing errors.
   */
  public static X509Certificate[] decodeUrlEncodedCertificate(final String urlEncodedCert)
      throws UnsupportedEncodingException, CertificateException {
    if (urlEncodedCert == null || urlEncodedCert.isEmpty()) {
      return null;
    }
    val certificateFactory = CertificateFactory.getInstance(CERTIFICATE_INSTANCE);
    val certificateBytes = URLDecoder.decode(urlEncodedCert, ENCODING).getBytes();
    val inputStream = new ByteArrayInputStream(certificateBytes);
    val certificate = (X509Certificate) certificateFactory.generateCertificate(inputStream);
    return new X509Certificate[]{certificate};
  }

  /**
   * Returns a Base64, Url encoded String from a certificate instance.
   *
   * @param certificate The certificate to encode.
   * @return String encoded certificate.
   */
  public static String encodeCertificateAsBase64Url(final Certificate certificate)
          throws CertificateEncodingException,
          UnsupportedEncodingException {
    return URLEncoder.encode(
            String.format(
                    X509_CERTIFICATE_HEADER_FORMAT,
                    Base64.getEncoder().encodeToString(certificate.getEncoded())),
            "UTF-8");
  }

  /**
   * Validates certificate by first checking the date and time of the certificate, then checks if
   * the certificate was signed using the private key to the matching public key of the certificate
   * found in the EHR keystore.
   *
   * @param certificate The certificate to validate.
   * @throws KeyStoreException if cacerts cannot be loaded or if the keystore is not initialized.
   * @throws IOException if the cacerts file cannot be loaded due to bad data or incorrect password.
   * @throws CertificateException for encoding errors.
   * @throws NoSuchAlgorithmException if the algorithm used to check the integrity of the keystore
   * cannot be found.
   */
  public static void validateCertificate(final X509Certificate certificate)
      throws KeyStoreException, IOException, CertificateException,
      NoSuchAlgorithmException, SignatureException, InvalidKeyException,
      NoSuchProviderException {
    try {
      certificate.checkValidity();
    } catch (CertificateExpiredException | CertificateNotYetValidException e) {
      log.error("isValidCertificate - certificate is not valid.", e);
      throw e;
    }
    val keyStore = getVerifyingKeyStore();
    val alias = keyStore.getCertificateAlias(certificate);
    if (alias == null) {
      throw new KeyStoreException("Certificate not found in keystore.");
    }
    val keyStoreCertificate = (X509Certificate) keyStore.getCertificate(alias);
    certificate.verify(keyStoreCertificate.getPublicKey());
  }

  private static KeyStore getVerifyingKeyStore()
      throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {
    val keyStoreFile = OscarProperties.getInstance()
        .getProperty(EHR_KEYSTORE_FILE);
    val inputStream = new FileInputStream(keyStoreFile);
    val keyStore = KeyStore.getInstance(KEYSTORE_TYPE);
    val keyStorePassword = OscarProperties.getInstance()
        .getProperty(EHR_KEYSTORE_PASSWORD)
        .toCharArray();
    keyStore.load(inputStream, keyStorePassword);
    return keyStore;
  }
}