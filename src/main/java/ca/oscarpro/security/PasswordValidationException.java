package ca.oscarpro.security;


// TODO: refactor into ca.oscarpro.common.security after RingCentral work is complete
public class PasswordValidationException extends Exception {
    public PasswordValidationException(String message) {
        super(message);
    }
}
