/**
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.security;

import java.security.NoSuchAlgorithmException;
import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.PageContext;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import lombok.var;
import org.oscarehr.admin.web.OktaUserHelper;
import org.oscarehr.common.dao.SecurityDao;
import org.oscarehr.common.model.Security;
import org.oscarehr.managers.OktaManager;
import org.oscarehr.util.LoggedInInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import oscar.Misc;
import oscar.MyDateFormat;
import oscar.OscarProperties;
import oscar.log.LogAction;
import oscar.log.LogConst;


// TODO: refactor into ca.oscarpro.common.security after RingCentral work is complete
@Slf4j
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Service // Wired as Service to hookup beans, public API utilized via static methods.
public class OscarPasswordService extends PasswordService {
  public static final String ENCRYPTED_PROPERTY = "oscar.auth.encrypted";

  /*
   * Password service is instance based (on version), initializing DAO in a static
   * context allows for less bean management between instances.
   * Also facilitates mocking of DAO/Manager for test cases.
   */
  protected static OktaManager oktaManager;
  @Autowired protected OktaManager tOktaManager;
  protected static SecurityDao securityDao;
  @Autowired protected SecurityDao tSecurityDao;
  static OscarProperties oscarProperties = OscarProperties.getInstance();

  @PostConstruct
  protected void postConstruct() {
    oktaManager = tOktaManager;
    securityDao = tSecurityDao;
  }

  public static void upgradePasswords() {
    if (!OscarPasswordService.isEncryptionEnabled()) {
      log.debug("Skipping password encryption check.");
      return;
    }
    val securities = securityDao.findWithOlderPasswordVersion();
    for (var security : securities) {
      // If Okta is enabled and the password is *
      // then the password is stored solely at Okta
      if (oktaManager.isOktaEnabled() && "*".equals(security.getPassword())) {
        continue;
      }
      log.debug("Encrypting password for security record {} (v{} -> v{})",
          security.getSecurityNo(),
          security.getPasswordVersion(),
          PasswordConfig.VERSION_CURRENT);
      try {
        val password = OscarPasswordService.upgradePassword(
            security.getPassword(), security.getPasswordVersion());
        security.setPassword(password);
        security.setPasswordVersion(PasswordConfig.VERSION_CURRENT);
        securityDao.merge(security);
      } catch (EncryptionException e) {
        // Do not upgrade passwords if an error occurred because of invalid
        // configuration version, parameters or encryption errors.
        log.error("Failed to encrypt password for security record {} (v{} -> v{})",
            security.getSecurityNo(),
            security.getPasswordVersion(),
            PasswordConfig.VERSION_CURRENT);
      }
    }
  }

  /**
   * Compare a password against a security record and see if it matches
   * @param password the password to check
   * @param security the security record to check against
   * @return if the password matches it's encoded form at the password level of the security record.
   */
  public static boolean isMatch(final String password, final Security security) {
    return OscarPasswordService
        .isMatch(password, security.getPassword(), security.getPasswordVersion());
  }

  /**
   * Updates security record to indicate current password version used to encode passwords.
   *
   * @param security the security record to update
   */
  public static void setPasswordVersion(Security security) {
    security.setPasswordVersion(
        PasswordService.isEncryptionEnabled()
            ? PasswordConfig.VERSION_CURRENT
            : PasswordConfig.VERSION_INITIAL);
  }

  public static void validatePassword(final String password) throws PasswordValidationException {
    var minLength = Integer.parseInt(oscarProperties.getProperty("password_min_length"));
    if (password.length() < minLength) {
      throw new PasswordValidationException(
          "Password must be at least " + minLength + " characters long.");
    }

    val minGroups = Integer.parseInt(oscarProperties.getProperty("password_min_groups"));
    val numberOfGroupsUsed = calculateNumberOfValidationGroupsUsed(password);
    if (numberOfGroupsUsed < minGroups) {
      throw new PasswordValidationException(
          "Password must contain at least " + minGroups + " of the following: "
              + "upper case letters, lower case letters, digits, and special characters.");
    }

    if (BannedPasswordService.isPasswordBanned(password)) {
      throw new PasswordValidationException(
          "Your password is insecure. Please choose a different password.");
    }
  }

  /**
   * Updates a user's password using the information provided by the pageContext.
   * This function should only be used in securityupdate.jsp as that is where it originally resided.
   * @param pageContext The page context to get the request from
   * @return The status message to be displayed to the user
   * @throws NoSuchAlgorithmException when encoding the password fails
   * @throws EncryptionException when encoding the password fails
   */
  public static String updatePasswordFromPageContext(final PageContext pageContext)
      throws NoSuchAlgorithmException, EncryptionException {
    val request = (HttpServletRequest) pageContext.getRequest();
    val oktaUserHelper = new OktaUserHelper();
    var hasErrorOccured = false;

    var pin = request.getParameter("pin");
    if (OscarProperties.getInstance().isPINEncripted()) {
      pin = Misc.encryptPIN(request.getParameter("pin"));
    }

    var statusMessage = "admin.securityupdate.msgUpdateFailure";

    val security = securityDao.find(Integer.parseInt(request.getParameter("security_no")));
    if (security != null) {
      val oldUserName = security.getUserName();
      val oktaUserId = security.getOktaUserId();
      security.setUserName(request.getParameter("user_name"));
      val shouldForcePasswordReset = "Force Password Reset".equals(request.getParameter("subbutton"));
      val isUpdateOktaCredentials =
          OscarProperties.getInstance().isPropertyActive("enable_kai_emr")
              && oktaManager.isOktaEnabled()
              && oktaUserId != null;
      log.debug("Updating User security attributes...");

      // Update user credentials in Okta if OSCAR Pro and Okta Auth are enabled
      if (isUpdateOktaCredentials) {
        log.debug("###### securityUpdate:  isOktaOauthEnabled =" + isUpdateOktaCredentials);

        val oktaCredentials = new Security();
        oktaCredentials.setUserName(request.getParameter("user_name"));
        oktaCredentials.setProviderNo(request.getParameter("provider_no"));
        oktaCredentials.setOktaUserId(oktaUserId);
        if (request.getParameter("password") != null && !"*********".equals(request.getParameter("password"))) {
          oktaCredentials.setPassword(request.getParameter("password"));
        }

        if (shouldForcePasswordReset) {
          log.debug(" -- Resetting Okta User password " + oldUserName + ", oktaUserId=" + oktaCredentials.getOktaUserId());
          oktaUserHelper.resetOktaUserPassword(security, request);
        } else {
          log.debug(" -- Updating Okta User " + oldUserName + ", oktaUserId=" + oktaCredentials.getOktaUserId());
          statusMessage = oktaUserHelper.updateOktaUser(pageContext, oldUserName, oktaCredentials);
          log.debug(" -- Okta User update status: " + statusMessage);
          hasErrorOccured =
              statusMessage != null && !statusMessage.equalsIgnoreCase("admin.securityupdate.msgUpdateSuccess");
        }
      }

      // Update Oscar user only if Okta is not enabled or okta enabled and Okta User update was successful
      if (!hasErrorOccured) {
        security.setProviderNo(request.getParameter("provider_no"));
        security.setBExpireset(
            request.getParameter("b_ExpireSet") == null ? 0 : Integer.parseInt(request.getParameter("b_ExpireSet")));
        security.setDateExpiredate(MyDateFormat.getSysDate(request.getParameter("date_ExpireDate")));
        if (!oktaManager.isOktaEnabled()) {
          security.setBLocallockset(request.getParameter("b_LocalLockSet") == null ? 0
              : Integer.parseInt(request.getParameter("b_LocalLockSet")));
          security.setBRemotelockset(request.getParameter("b_RemoteLockSet") == null ? 0
              : Integer.parseInt(request.getParameter("b_RemoteLockSet")));
        }
        // update force password reset status
        security.setForcePasswordReset("1".equals(request.getParameter("forcePasswordReset"))
            || shouldForcePasswordReset);

        // do not update oscar password or pin if Okta is enabled
        if (!isUpdateOktaCredentials) {
          val password = request.getParameter("password");
          if (!"*********".equals(password)) {
            log.debug("###### securityUpdate:  !isOktaOauthEnabled --> setting password in security record");
            try {
              validatePassword(password);
            } catch (PasswordValidationException e) {
              log.error(e.getMessage(), e);
              return e.getMessage();
            }
            security.setPassword(OscarPasswordService.encodePassword(password));
            OscarPasswordService.setPasswordVersion(security);
          }

          if (!oktaManager.isOktaEnabled()
              && (request.getParameter("pin") == null
                  || !"****".equals(request.getParameter("pin")))) {
            security.setPin(pin);
          }
        }

        log.debug(" -- Updating Oscar User: " + oldUserName);
        securityDao.saveEntity(security);
        statusMessage = "admin.securityupdate.msgUpdateSuccess";
        LogAction.addLog((String) request.getSession().getAttribute("user"), LogConst.UPDATE,
            LogConst.CON_SECURITY,
            request.getParameter("security_no") + "->" + request.getParameter("user_name"),
            LoggedInInfo.obtainClientIpAddress(request));
      }
    }

    return statusMessage;
  }

  private static int calculateNumberOfValidationGroupsUsed(final String password) {
    val lowerCharacters = oscarProperties.getProperty("password_group_lower_chars");
    val upperCharacters = oscarProperties.getProperty("password_group_upper_chars");
    val allowedDigits = oscarProperties.getProperty("password_group_digits");
    val specialCharacters = oscarProperties.getProperty("password_group_special");

    var upper = false;
    var lower = false;
    var digits = false;
    var special = false;

    for (Character character : password.toCharArray()) {
      if (lowerCharacters.indexOf(character) != -1) {
        lower = true;
      } else if (upperCharacters.indexOf(character) != -1) {
        upper = true;
      } else if (allowedDigits.indexOf(character) != -1) {
        digits = true;
      } else if (specialCharacters.indexOf(character) != -1) {
        special = true;
      }
    }

    return (upper ? 1 : 0)
        + (lower ? 1 : 0)
        + (digits ? 1 : 0)
        + (special ? 1 : 0);
  }
}
