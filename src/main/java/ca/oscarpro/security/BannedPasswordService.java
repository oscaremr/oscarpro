/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package ca.oscarpro.security;

import java.util.HashSet;
import java.util.Set;
import javax.annotation.PostConstruct;
import lombok.NoArgsConstructor;
import lombok.val;
import org.oscarehr.common.dao.BannedPasswordDao;
import org.oscarehr.common.model.BannedPassword;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// TODO: refactor into ca.oscarpro.common.security after RingCentral work is complete
@Service
public class BannedPasswordService {

  private static Set<String> bannedPasswords;

  @Autowired
  private BannedPasswordDao tBannedPasswordDao;
  private static BannedPasswordDao bannedPasswordDao;

  @PostConstruct
  void postConstruct() {
    // We should not autowire static fields, so we will autowire a non-static field and
    // set it to the static field.
    bannedPasswordDao = tBannedPasswordDao;
    bannedPasswords = new HashSet<>();
  }

  static void initializeBannedPasswords() {
    bannedPasswords = convertBannedPasswordsToStrings(
        bannedPasswordDao.getAllBannedPasswords());
  }

  static Set<String> convertBannedPasswordsToStrings(
      final Set<BannedPassword> bannedPasswordsSet
  ) {
    val bannedPasswordStringsSet = new HashSet<String>();
    for (val bannedPassword : bannedPasswordsSet) {
      bannedPasswordStringsSet.add(bannedPassword.getPassword());
    }
    return bannedPasswordStringsSet;
  }

  /**
   * Checks if password is in the banned passwords stored in the database.
   * @param password Password to check
   * @return True if password is banned, else false
   */
  public static boolean isPasswordBanned(final String password) {
    if (bannedPasswords.isEmpty()) {
      initializeBannedPasswords();
    }
    return bannedPasswords.contains(password);
  }

}
