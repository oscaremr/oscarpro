package ca.oscarpro.ehr.patient.controller;

import ca.oscarpro.ehr.patient.repository.ClassicPatientChartRepository;
import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.FacilityDao;
import org.oscarehr.common.model.Facility;
import org.oscarehr.util.LoggedInInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This class is a REST controller for accepting requests from Oscar Pro in OSCAR Classic.
 * It accepts requests using the base OSCAR Classic URL, ie /oscar/api/ehr/..
 */
@Path("/api/ehr/classic/patient")
@Component("ClassicPatientController")
@Slf4j
public class ClassicPatientController {

  @Autowired private ClassicPatientChartRepository classicPatientChartPrintRepository;
  @Autowired private FacilityDao facilityDao;
  @GET
  @Path("/v1/{uuid}/chart/summary:pdf")
  @Produces("application/pdf")
  public Response printPatientChart(
      final @PathParam("uuid") String uuid,
      final @Context HttpServletRequest request) {
    try {
      // Generate the PDF content
      val pdfStream = new ByteArrayOutputStream();
      classicPatientChartPrintRepository.printChart(
          uuid, request, pdfStream, createLoggedInInfo(request), true);
      // Build the response with PDF content
      val response = Response.status(Response.Status.OK);
      response.header("Content-Disposition",
          "inline; filename=Encounter-"
              + new SimpleDateFormat("yyyy-MM-dd.hh.mm.ss").format(new Date()) + ".pdf"
      );
      response.header("Content-Type", "application/pdf");
      response.entity(pdfStream.toByteArray());
      return response.build();
    } catch (RuntimeException re) {
      log.error("Error printing patient chart for patient {}", uuid, re);
      return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(re.getMessage()).build();
    }
  }

  private LoggedInInfo createLoggedInInfo(final HttpServletRequest request) {
    val loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
    loggedInInfo.setCurrentFacility(getDefaultFacility());
    return loggedInInfo;
  }

  private Facility getDefaultFacility() {
    val facilities = facilityDao.findAll(true);
    return facilities.isEmpty() ? new Facility() : facilities.get(0);
  }
}
