package ca.oscarpro.ehr.patient.repository;

import ca.oscarpro.ehr.patient.projection.ClassicPatientChartProjection;
import com.lowagie.text.DocumentException;
import java.io.IOException;
import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import lombok.val;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.util.LoggedInInfo;
import org.springframework.stereotype.Component;

@Component
public class ClassicPatientChartRepository {

  public void printChart(
      final String demographicNumber,
      final HttpServletRequest request,
      final OutputStream stream,
      final LoggedInInfo loggedInInfo
  ) {
    printChart(demographicNumber, request, stream, loggedInInfo, false);
  }

  public void printChart(
      final String demographicNumber,
      final HttpServletRequest request,
      final OutputStream stream,
      final LoggedInInfo loggedInInfo,
      final boolean printAll
  ) {
    try {
      setAttributeForPrint(request);
      ClassicPatientChartProjection.create(
          request,
          processDemographicNumber(demographicNumber, request),
          stream,
          loggedInInfo,
          printAll).doPrint();
    } catch (BadRequestException | DocumentException | IOException e) {
      throw new RuntimeException("Error generating PDF for patient chart", e);
    }
  }

  private void setAttributeForPrint(final HttpServletRequest request) {
    val selectedSiteIdForPrint =
        StringUtils.defaultString(request.getParameter("selectedSiteIdForPrint"), "0");
    request.setAttribute("selectedSiteIdForPrint", selectedSiteIdForPrint);
  }

  private Integer processDemographicNumber(final String demographicNumber, final HttpServletRequest request) {
    val alternativeDemographicNumber = (String) request.getAttribute("casemgmt_DemoNo");
    if (StringUtils.isBlank(demographicNumber) && StringUtils.isBlank(alternativeDemographicNumber)) {
      throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(
          "Both primary and alternative demographic numbers are missing or empty."
      ).build());
    }
    try {
      return StringUtils.isNotBlank(demographicNumber)
          ? Integer.parseInt(demographicNumber.trim())
          : Integer.parseInt(alternativeDemographicNumber.trim());
    } catch (NumberFormatException e) {
      throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(String.format(
          "Invalid demographic numbers included in request: %s, %s",
          demographicNumber, alternativeDemographicNumber
      )).build());
    }
  }
}
