package ca.oscarpro.ehr.patient.projection;

import ca.oscarpro.common.util.CalendarUtils;
import com.lowagie.text.DocumentException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import javax.servlet.http.HttpServletRequest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.val;
import lombok.var;
import org.oscarehr.casemgmt.dao.CaseManagementNoteDAO;
import org.oscarehr.casemgmt.service.CaseManagementPrint;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

@Getter
@NoArgsConstructor(force = true)
public class ClassicPatientChartProjection {

  private final CaseManagementNoteDAO caseManagementNoteDao
      = SpringUtils.getBean(CaseManagementNoteDAO.class);

  private String[] noteIds;

  private Calendar[] dateRange;

  private Integer demographicNumber;

  private boolean printAllNotes;

  private boolean printNotes;

  private boolean printCPP;

  private boolean printRx;

  private boolean printLabs;

  private boolean printPreventions;

  private boolean printMeasurements;

  private boolean printDocuments;

  private boolean printHrms;

  private HttpServletRequest request;

  private OutputStream stream;

  private LoggedInInfo loggedInInfo;

  @AllArgsConstructor
  @Getter
  public enum ClassicPatientChartProjectionParameters {
    ALL_NOTES("ALL_NOTES"),
    PRINT_NOTES("printNotes"),
    PRINT_CPP("printCPP"),
    PRINT_RX("printRx"),
    PRINT_LABS("printLabs"),
    PRINT_PREVENTIONS("printPreventions"),
    PRINT_MEASUREMENTS("printMeasurements"),
    PRINT_DOCUMENTS("printDocuments"),
    PRINT_HRMS("printHrms");
    private final String parameter;

    public static boolean isAllNotes(String parameter) {
      return ALL_NOTES.getParameter().equals(parameter);
    }

    public static boolean isParameterTrue(
        final HttpServletRequest request,
        final ClassicPatientChartProjectionParameters parameterEnum
    ) {
      return "true".equalsIgnoreCase(request.getParameter(parameterEnum.getParameter()));
    }
  }

  public ClassicPatientChartProjection(
      @NonNull final HttpServletRequest request,
      @NonNull final Integer demographicNumber,
      @NonNull final OutputStream stream,
      @NonNull final LoggedInInfo loggedInInfo
  ) {
    this.request = request;
    this.stream = stream;
    this.loggedInInfo = loggedInInfo;
    this.dateRange = CalendarUtils.getDateRangeFromDates(request.getParameter("pStartDate"), request.getParameter("pEndDate"));
    this.demographicNumber = demographicNumber;
    this.noteIds = getNoteIds(request, dateRange);
    this.printAllNotes = ClassicPatientChartProjectionParameters.isAllNotes(noteIds.length == 1 ? noteIds[0] : "");
    this.printNotes = ClassicPatientChartProjectionParameters.isParameterTrue(request, ClassicPatientChartProjectionParameters.PRINT_NOTES);
    if (!printNotes) {
      this.printAllNotes = false;
      this.noteIds = new String[]{};
    }
    this.printCPP = ClassicPatientChartProjectionParameters.isParameterTrue(request, ClassicPatientChartProjectionParameters.PRINT_CPP);
    this.printRx = ClassicPatientChartProjectionParameters.isParameterTrue(request, ClassicPatientChartProjectionParameters.PRINT_RX);
    this.printLabs = ClassicPatientChartProjectionParameters.isParameterTrue(request, ClassicPatientChartProjectionParameters.PRINT_LABS);
    this.printPreventions = ClassicPatientChartProjectionParameters.isParameterTrue(request, ClassicPatientChartProjectionParameters.PRINT_PREVENTIONS);
    this.printMeasurements = ClassicPatientChartProjectionParameters.isParameterTrue(request, ClassicPatientChartProjectionParameters.PRINT_MEASUREMENTS);
    this.printDocuments = ClassicPatientChartProjectionParameters.isParameterTrue(request, ClassicPatientChartProjectionParameters.PRINT_DOCUMENTS);
    this.printHrms = ClassicPatientChartProjectionParameters.isParameterTrue(request, ClassicPatientChartProjectionParameters.PRINT_HRMS);
    if (!hasDataToPrint(noteIds, printAllNotes, printCPP, printRx, printLabs, printPreventions,
        printMeasurements, printDocuments, printHrms)) {
      throw new RuntimeException("No data to print.");
    }
  }

  public static ClassicPatientChartProjection create(
      @NonNull final HttpServletRequest request,
      @NonNull final Integer demographicNumber,
      @NonNull final OutputStream stream,
      @NonNull final LoggedInInfo loggedInInfo,
      final boolean printAll) {
    if (!printAll) {
      return new ClassicPatientChartProjection(request, demographicNumber, stream, loggedInInfo);
    }
    val projection = new ClassicPatientChartProjection();
    projection.request = request;
    projection.stream = stream;
    projection.loggedInInfo = loggedInInfo;
    projection.dateRange = CalendarUtils.getDateRangeFromDates(
        request.getParameter("pStartDate"), request.getParameter("pEndDate"));
    projection.demographicNumber = demographicNumber;
    projection.noteIds = projection.getNoteIds(request, projection.dateRange);
    projection.printAllNotes = true;
    projection.printNotes = true;
    projection.printCPP = true;
    projection.printRx = true;
    projection.printLabs = true;
    projection.printPreventions = true;
    projection.printMeasurements = true;
    projection.printDocuments = true;
    projection.printHrms = true;
    return projection;
  }

  public void doPrint()
      throws DocumentException, IOException {
    new CaseManagementPrint().doPrint(this);
  }

  private String[] getNoteIds(
      final HttpServletRequest request,
      final Calendar[] dateRange
  ) {
    var ids = new StringBuilder(request.getParameter("notes2print") != null
        ? request.getParameter("notes2print")
        : ClassicPatientChartProjectionParameters.ALL_NOTES.getParameter());
    if (dateRange[0] != null && dateRange[1] != null) {
      ids = new StringBuilder();
      val notes =
          caseManagementNoteDao.getNotesByDemographicDateRange(
              demographicNumber.toString(),
              dateRange[0].getTime(),
              dateRange[1].getTime()
          );
      if (notes == null) {
        throw new NullPointerException("Retrieved notes list is null.");
      }
      for (var note : notes) {
        ids.append(note.getId()).append(",");
      }
    }
    return ids.length() > 0 ? ids.toString().split(",") : new String[]{};
  }

  private boolean hasDataToPrint(final String[] noteIds, final boolean... printFlags) {
    for (var flag : printFlags) {
      if (flag) {
        return true;
      }
    }
    return noteIds.length != 0;
  }

  public Calendar getStartDate() {
    return this.dateRange[0];
  }

  public Calendar getEndDate() {
    return this.dateRange[1];
  }
}
