/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.service;

import ca.oscarpro.utils.CookieUtils;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.Cookie;
import lombok.NonNull;
import lombok.experimental.StandardException;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import oscar.OscarProperties;

// TODO: refactor into ca.oscarpro.common.http after RingCentral work is complete
@Service
public class OscarProConnectorService {
  @Autowired
  private RestTemplate restTemplate;
  private final Set<String> authCookieNames = new HashSet<>(Arrays.asList("kai-sso", "JSESSIONID"));

  @StandardException
  public static class OscarProConnectorServiceException extends Exception {}

  private String getCsrfToken(final @NonNull Cookie[] cookies)
      throws OscarProConnectorServiceException {
    // Create Headers
    val headers = new HttpHeaders();
    configureAuthentication(headers, cookies);

    // Get Response
    val csrfResponseEntity = restTemplate.exchange(OscarProperties.getCsrfUrl(), HttpMethod.GET,
        new HttpEntity<String>(headers), String.class);

    return csrfResponseEntity.getBody();
  }

  private void configureAuthentication(final @NonNull HttpHeaders headers,
      final @NonNull Cookie[] cookies) throws OscarProConnectorServiceException {
    val foundCookies = getAuthenticationCookies(cookies);
    CookieUtils.addCookiesToHeaders(headers, foundCookies);
  }

  private List<Cookie> getAuthenticationCookies(final @NonNull Cookie[] cookies)
      throws OscarProConnectorServiceException {
    val foundCookies = CookieUtils.getCookiesByNames(cookies, authCookieNames);
    if (foundCookies.size() != authCookieNames.size()) {
      throw new OscarProConnectorServiceException("Missing authentication cookies");
    }

    return foundCookies;
  }

  private void configureCsrf(final @NonNull HttpHeaders headers, final @NonNull Cookie[] cookies)
      throws OscarProConnectorServiceException {
    val csrfToken = getCsrfToken(cookies);

    // set up the header
    headers.set("X-XSRF-TOKEN", csrfToken);

    // set up the cookie
    val cookie = new Cookie("XSRF-TOKEN", csrfToken);
    cookie.setPath("/");
    cookie.setDomain(OscarProperties.getClinicUrl());

    // add the cookie to the headers
    CookieUtils.addCookieToHeaders(headers, cookie);
  }

  public void configureHeaders(final @NonNull HttpHeaders headers, final @NonNull Cookie[] cookies)
      throws OscarProConnectorServiceException {
    configureAuthentication(headers, cookies);
    configureCsrf(headers, cookies);
  }
}