/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.fax;

import java.io.File;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.stereotype.Component;

@Component
public class FaxFileReader {
  public int getNumberOfPagesFromPdf(final File pdfFile) throws IOException {
    try (PDDocument document = PDDocument.load(pdfFile)) {
      return document.getNumberOfPages();
    }
  }
}
