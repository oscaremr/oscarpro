/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.fax;

import javax.servlet.http.Cookie;
import lombok.Builder;
import lombok.Data;
import org.oscarehr.util.LoggedInInfo;

@Data
@Builder
public class SendFaxData {
  private String destinationFaxNumber;
  private String providerNumber;
  private Integer demographicNumber;
  private FaxFileType faxFileType;
  private String fileName;
  private Cookie[] cookies;
  private String faxLine;
  private byte[] pdfDocumentBytes;
  private LoggedInInfo loggedInInfo;
  private String existingFilePath;

  public String getLoggedInProviderNumber() {
    return loggedInInfo.getLoggedInProviderNo();
  }
}
