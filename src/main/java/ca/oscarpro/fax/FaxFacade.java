/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.fax;

import ca.oscarpro.service.OscarProConnectorService.OscarProConnectorServiceException;
import ca.oscarpro.utils.FileUtils;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.Nullable;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.oscarehr.common.dao.FaxConfigDao;
import org.oscarehr.common.dao.FaxJobDao;
import org.oscarehr.common.model.FaxConfig;
import org.oscarehr.common.model.FaxJob;
import org.oscarehr.common.model.FaxJob.STATUS;
import org.oscarehr.util.FaxUtils;
import org.oscarehr.util.PathUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import oscar.OscarProperties;
import oscar.log.LogService;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FaxFacade {

  private final String JAVA_TEMP_DIR_PATH = System.getProperty("java.io.tmpdir");
  private final String DOCUMENT_DIR_PATH =
      OscarProperties.getInstance().getProperty("DOCUMENT_DIR");
  private final String BASE_DOCUMENT_DIR_PATH =
      OscarProperties.getInstance().getProperty("BASE_DOCUMENT_DIR");

  private final OutboundFaxApiConnector outboundFaxApiConnector;
  private final FaxJobDao faxJobDao;
  private final FaxConfigDao faxConfigDao;
  private final FaxFileWriter faxFileWriter;
  private final FaxFileReader faxFileReader;
  private final LogService logService;

  /**
   * Sends a fax to the destinationFaxNumber fax number.
   * If fax is not configured, saves a FaxJob to the database.
   *
   * @param sendFaxData Data object with parameters required for sending the fax
   * @throws FaxException if the fax fails to send
   */
  public void sendFax(final SendFaxData sendFaxData)
      throws FaxException, OscarProConnectorServiceException, IOException {
    // If fax is not configured, default to old fax service
    if (outboundFaxApiConnector.isFaxEnabled(sendFaxData.getCookies())) {
      // Create fax directory for current date and save fax pdf to it
      val faxFile = writeFaxPdfToFaxDirectory(getFaxDirectoryPath(), sendFaxData.getFileName(),
          sendFaxData.getPdfDocumentBytes());
      // Send fax through modern fax
      outboundFaxApiConnector.sendFax(sendFaxData, faxFile);
    } else {
      sendFaxOld(sendFaxData);
    }
  }

  /**
   * Sends a fax to the destinationFaxNumber fax number.
   * Saves a FaxJob to the database.
   *
   * @param sendFaxData Data object with parameters required for sending the fax
   * @throws FaxException if the fax fails to send
   */
  protected void sendFaxOld(final SendFaxData sendFaxData) throws FaxException, IOException {
    validateDestinationFaxNumber(sendFaxData.getDestinationFaxNumber());
    val faxPdfFile = writeFaxFilesToDiskAndValidateTheyExist(
        sendFaxData.getDestinationFaxNumber(), sendFaxData.getFileName(),
        sendFaxData.getPdfDocumentBytes(), sendFaxData.getExistingFilePath());
    val faxJobId = createAndSaveFaxJob(
        sendFaxData.getDestinationFaxNumber(), sendFaxData.getLoggedInProviderNumber(),
        sendFaxData.getDemographicNumber(), faxPdfFile.getName(),
        faxFileReader.getNumberOfPagesFromPdf(faxPdfFile), sendFaxData.getFaxLine());
    createLog(sendFaxData, faxJobId);
  }

  private void createLog(final SendFaxData sendFaxData, final Integer faxJobId) {
    logService.log(sendFaxData.getLoggedInInfo(),
        "FaxFacade:sendOldFax",
        sendFaxData.getFaxFileType().toString() + " fax sent",
        "FaxJob ID: " + faxJobId,
        sendFaxData.getDemographicNumber().toString(),
        "");
  }

  /**
   * Validates the destinationFaxNumber fax number.
   *
   * @param destinationFaxNumber the destinationFaxNumber fax number
   * @throws FaxException if the destinationFaxNumber fax number is invalid
   */
  private void validateDestinationFaxNumber(final String destinationFaxNumber) throws FaxException {
    if (!FaxUtils.isValidFaxNumber(destinationFaxNumber)) {
      throw new FaxException("Invalid fax number: " + destinationFaxNumber);
    }
  }

  /**
   * Writes the fax files to disk and validates they exist.
   *
   * @param destinationFaxNumber the destinationFaxNumber fax number
   * @param fileName the name of the file
   * @param pdfDocumentBytes the pdf document bytes
   * @throws FaxException if the files fail to write
   */
  private File writeFaxFilesToDiskAndValidateTheyExist(
      final String destinationFaxNumber, final String fileName, final byte[] pdfDocumentBytes,
      final String existingFilePath)
      throws FaxException {
    writeFaxPdfToJavaTempDir(fileName, pdfDocumentBytes);
    writeDestinationFaxNumberTxtToJavaTempDir(fileName, destinationFaxNumber);
    return existingFilePath == null
        ? writeFaxPdfToOscarDocuments(fileName, pdfDocumentBytes)
        : new File(existingFilePath);
  }

  /**
   * Writes the fax pdf to the oscar documents directory.
   *
   * @param fileName the name of the file
   * @param pdfDocumentBytes the pdf document bytes
   * @throws FaxException if the file fails to write
   */
  private File writeFaxPdfToOscarDocuments(final String fileName, final byte[] pdfDocumentBytes)
      throws FaxException {
    // Write fax pdf to /oscar/documents/ directory
    final File documentDirFile = new File(FileUtils.addFileExtension(
        DOCUMENT_DIR_PATH + File.separator + fileName, ".pdf"));
    faxFileWriter.writeFaxPdfFile(pdfDocumentBytes, documentDirFile);
    validateFileExists(documentDirFile);
    return documentDirFile;
  }

  /**
   * Writes the destination fax number txt file to the java temp directory.
   *
   * @param fileName the name of the file
   * @param destinationFaxNumber the destinationFaxNumber fax number
   * @throws FaxException if the file fails to write
   */
  private void writeDestinationFaxNumberTxtToJavaTempDir(
      final String fileName, final String destinationFaxNumber) throws FaxException {
    // Write destination fax txt file to /Tomcat 7.0/temp/ directory
    final File tempDirTxtFile = new File(FileUtils.addFileExtension(
        JAVA_TEMP_DIR_PATH + File.separator + fileName, ".txt"));
    faxFileWriter.writeDestinationFaxNumberTxtFile(destinationFaxNumber, tempDirTxtFile);
    validateFileExists(tempDirTxtFile);
  }

  /**
   * Writes the fax pdf to the java temp directory.
   *
   * @param fileName the name of the file
   * @param pdfDocumentBytes the pdf document bytes
   * @throws FaxException if the file fails to write
   */
  private void writeFaxPdfToJavaTempDir(final String fileName, final byte[] pdfDocumentBytes)
      throws FaxException {
    // Write fax pdf to /Tomcat 7.0/temp/ directory
    final File tempDirPdfFile = new File(FileUtils.addFileExtension(
        JAVA_TEMP_DIR_PATH + File.separator + fileName, ".pdf"));
    faxFileWriter.writeFaxPdfFile(pdfDocumentBytes, tempDirPdfFile);
    validateFileExists(tempDirPdfFile);
  }

  /**
   * Validates the file exists.
   *
   * @param file the file
   * @throws FaxException if the file does not exist
   */
  protected void validateFileExists(final File file) throws FaxException {
    if (!file.exists()) {
      throw new FaxException("Failed to write fax file: " + file.getPath());
    }
  }

  /**
   * Creates and saves a FaxJob to the database.
   *
   * @param destinationFaxNumber the destinationFaxNumber fax number
   * @param providerNumber the provider number
   * @param demographicNumber the demographic number
   * @param fileName the name of the file
   * @param numberOfPages the number of pages
   * @param faxLine the fax line
   */
  private Integer createAndSaveFaxJob(
      final String destinationFaxNumber,
      final String providerNumber,
      final Integer demographicNumber,
      final String fileName,
      final int numberOfPages,
      final String faxLine) {
    // Create FaxJob
    final FaxJob faxJob = new FaxJob();
    faxJob.setDestination(destinationFaxNumber);
    faxJob.setFile_name(fileName);
    faxJob.setNumPages(numberOfPages);
    faxJob.setFax_line(faxLine);
    faxJob.setStamp(new Date());
    faxJob.setOscarUser(providerNumber);
    faxJob.setDemographicNo(demographicNumber);
    // Set the user and status
    setFaxJobUser(faxJob, faxLine);
    setFaxJobStatus(faxJob);
    // Save FaxJob to database
    faxJobDao.persist(faxJob);
    return faxJob.getId();
  }

  /**
   * Sets the user for the fax job if it exists.
   *
   * @param faxJob the fax job
   * @param faxLine the fax line
   */
  private void setFaxJobUser(final FaxJob faxJob, final String faxLine) {
    final FaxConfig faxConfig = getFaxConfig(faxLine);
    if (faxConfig != null) {
      faxJob.setUser(faxConfig.getFaxUser());
    }
  }

  /**
   * Gets the fax config for the fax line.
   *
   * @param faxLine the fax line
   * @return the matching fax config, or the first fax config if no match exists, or null if no fax
   *     configs exist
   */
  @Nullable
  private FaxConfig getFaxConfig(final String faxLine) {
    // Get all faxConfigs from database
    final List<FaxConfig> faxConfigs = faxConfigDao.findAll(null, null);

    // Return faxConfig if faxLine matches
    for (FaxConfig faxConfig : faxConfigs) {
      if (faxLine.equals(faxConfig.getFaxNumber())) {
        return faxConfig;
      }
    }

    // If no faxConfigs match, return first faxConfig in list
    if (!faxConfigs.isEmpty()) {
      return faxConfigs.get(0);
    }

    // If no faxConfigs exist, return null
    return null;
  }

  /**
   * Sets the status for the fax job SENT if the user exists ERROR if the user does not exist.
   *
   * @param faxJob the fax job
   */
  private void setFaxJobStatus(final FaxJob faxJob) {
    faxJob.setStatus((faxJob.getUser() == null) ? STATUS.ERROR : STATUS.SENT);
  }

  /**
   * Gets the fax directory path for the given date.
   *
   * @return the fax directory path in the format of BASE_DOCUMENT_PATH/fax/yyyy/mm/dd
   * @throws FaxException if the fax directory fails to create
   */
  protected String getFaxDirectoryPath() throws FaxException {
    // Build path to fax directory for current date
    val currentDate = new Date();
    val yearFormat = new SimpleDateFormat("yyyy");
    val monthFormat = new SimpleDateFormat("MM");
    val dateFormat = new SimpleDateFormat("dd");
    val faxDirectoryPath = PathUtils.addTrailingSlash(BASE_DOCUMENT_DIR_PATH)
        + "fax" + File.separator
        + yearFormat.format(currentDate) + File.separator
        + monthFormat.format(currentDate) + File.separator
        + dateFormat.format(currentDate) + File.separator;

    // Ensure the fax directory exists and create it if it doesn't
    createFaxDirectory(new File(faxDirectoryPath));

    return faxDirectoryPath;
  }

  /**
   * Writes the fax pdf to the fax directory.
   *
   * @param faxDirectoryPath the fax directory path
   * @param fileName the name of the file
   * @param pdfDocumentBytes the pdf document bytes
   * @throws FaxException if the file fails to write
   */
  private File writeFaxPdfToFaxDirectory(final String faxDirectoryPath, final String fileName,
      final byte[] pdfDocumentBytes) throws FaxException {
    // Write fax pdf to fax directory
    val faxFile = new File(FileUtils.addFileExtension(
        faxDirectoryPath + fileName, ".pdf"));
    faxFileWriter.writeFaxPdfFile(pdfDocumentBytes, faxFile);
    validateFileExists(faxFile);
    return faxFile;
  }

  /**
   * Creates the fax directory if it does not exist.
   *
   * @param faxDirectory the fax directory
   * @throws FaxException if the fax directory fails to create
   */
  protected void createFaxDirectory(final File faxDirectory) throws FaxException {
    // Create directories if they don't exist
    if (!faxDirectory.exists() && !faxDirectory.mkdirs()) {
      throw new FaxException("Failed to create fax directory: " + faxDirectory.getPath());
    }
  }
}
