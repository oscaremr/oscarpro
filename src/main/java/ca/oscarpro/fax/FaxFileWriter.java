/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.fax;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FaxFileWriter {

  public void writeFaxPdfFile(final byte[] pdfDocumentBytes, final File directoryFile) {
    try (FileOutputStream out = new FileOutputStream(directoryFile)) {
      out.write(pdfDocumentBytes);
    } catch (IOException ex) {
      log.error("Cannot create file for faxing: " + directoryFile.getPath(), ex);
    }
  }

  public void writeDestinationFaxNumberTxtFile(final String destinationFaxNumber, final File directoryFile) {
    try {
      val tempDirFilePath = Paths.get(directoryFile.getPath());
      val lines = Collections.singletonList(destinationFaxNumber);
      Files.write(tempDirFilePath, lines, StandardCharsets.UTF_8);
    } catch (IOException ex) {
      log.error("Cannot create file for faxing: " + directoryFile.getPath(), ex);
    }
  }

}
