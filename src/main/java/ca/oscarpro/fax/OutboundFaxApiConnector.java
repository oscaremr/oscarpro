/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.fax;

import ca.oscarpro.service.OscarProConnectorService;
import ca.oscarpro.service.OscarProConnectorService.OscarProConnectorServiceException;
import java.io.File;
import javax.servlet.http.Cookie;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import oscar.OscarProperties;

@Service
public class OutboundFaxApiConnector {
  @Autowired
  private RestTemplate restTemplate;
  @Autowired
  private OscarProConnectorService oscarProConnectorService;

  private static boolean isActiveFaxAccountPresent = false;

  public void sendFax(final SendFaxData sendFaxData, final File file)
      throws OscarProConnectorServiceException {
    // Create Headers
    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    oscarProConnectorService.configureHeaders(headers, sendFaxData.getCookies());

    // Create Body
    final JSONObject body = new JSONObject();

    body.put("destination", sendFaxData.getDestinationFaxNumber());
    body.put("providerId", sendFaxData.getProviderNumber());
    body.put("demographicId", sendFaxData.getDemographicNumber());
    body.put("fileType", sendFaxData.getFaxFileType());
    body.put("filename", file.getPath());

    final HttpEntity<String> entity = new HttpEntity<>(body.toString(), headers);

    restTemplate.exchange(OscarProperties.getOscarProBaseUrl() + "/api/v1/fax/outbound",
        HttpMethod.POST, entity, null);
  }

  /**
   * Checks if there is an active fax account in the database. If this value is true, all future
   * checks will return true until system restart.
   *
   * @param cookies The cookies to add to the headers
   * @return True if there is an active fax account or is an active fax account was found already, false otherwise
   * @throws OscarProConnectorServiceException If there is an error connecting to the Oscar PRO server
   */
  public boolean isFaxEnabled(final Cookie[] cookies) throws OscarProConnectorServiceException {
    // Return true if an active fax account was found already
    if (isActiveFaxAccountPresent) {
      return true;
    }

    // Create headers
    final HttpHeaders headers = new HttpHeaders();
    oscarProConnectorService.configureHeaders(headers, cookies);
    final HttpEntity<String> entity = new HttpEntity<>(headers);

    // Send api call to OSCAR Pro
    isActiveFaxAccountPresent = restTemplate.exchange(
        OscarProperties.getOscarProBaseUrl() + "/api/fax/account/active", HttpMethod.GET,
        entity, Boolean.class).getBody();
    return isActiveFaxAccountPresent;
  }
}