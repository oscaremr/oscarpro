/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.servlet.http.Cookie;
import lombok.NonNull;
import lombok.val;
import org.springframework.http.HttpHeaders;


// TODO: refactor into ca.oscarpro.common.util after RingCentral work is complete
public class CookieUtils {

  public static String formatCookie(final @NonNull Cookie cookie) {
    return String.format(
        "%s=%s; Expires=%s; Max-Age=%s; Path=/; Domain=%s",
        cookie.getName(),
        cookie.getValue(),
        cookie.getMaxAge(),
        cookie.getMaxAge(),
        cookie.getDomain());
  }

  public static List<Cookie> getCookiesByNames(final @NonNull Cookie[] cookies,
      final @NonNull Set<String> cookieNames) {
    val foundCookies = new ArrayList<Cookie>();
    // making a copy of names, so we can remove elements from it without modifying the original
    val cookiesNameToFind = new HashSet<>(cookieNames);
    for (Cookie cookie : cookies) {
      if (cookiesNameToFind.isEmpty()) {
        break;
      }

      if (cookiesNameToFind.contains(cookie.getName())) {
        foundCookies.add(cookie);
        cookiesNameToFind.remove(cookie.getName());
      }
    }

    return foundCookies;
  }

  public static void addCookieToHeaders(final @NonNull HttpHeaders headers,
      final @NonNull Cookie cookie) {
    headers.add("Cookie", formatCookie(cookie));
  }

  public static void addCookiesToHeaders(final @NonNull HttpHeaders headers,
      final @NonNull List<Cookie> cookies) {
    for (Cookie cookie : cookies) {
      addCookieToHeaders(headers, cookie);
    }
  }
}
