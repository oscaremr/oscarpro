/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.utils;

public class FileUtils {

  public static String addFileExtension(final String filename, final String extension) {
    return filename.endsWith(extension) ? filename : filename + extension;
  }

}
