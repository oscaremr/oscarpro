/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package ca.kai.datasharing;

import ca.kai.datasharing.PatientPortalData.SyncStatus;
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.EnumUtils;
import org.oscarehr.casemgmt.model.CaseManagementNote;
import org.oscarehr.common.dao.DataSharingSettingsDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.model.DataSharingSettings;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.Prevention;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import oscar.OscarProperties;
import oscar.oscarResearch.oscarDxResearch.bean.dxResearchBean;


@Service
@Slf4j
public class DataSharingService {

  private static final String ENABLE_KAI_EMR_KEY = "enable_kai_emr";
  private static final String ENABLE_TIA_HEALTH_KEY = "enable_tia_health_patient_portal";
  private static final String KAI_EMR_URL_KEY = "clinic.url";
  private static final String KAI_DEPLOYED_CONTEXT_KEY = "kaiemr_deployed_context";
  private static final String BUTTON_DISABLED_STRING = "disabled";

  @Autowired private DataSharingSettingsDao dataSharingSettingsDao;
  @Autowired private DemographicDao demographicDao;

  @Getter private static String kaiEmrUrl;

  private final OscarProperties oscarProperties;
  private DataSharingSettings dataSharingSettings;
  private Boolean enableKaiEmr;
  private Boolean enableTiaHealthPatientPortal;

  @Autowired
  public DataSharingService(DataSharingSettingsDao dao, DemographicDao demographicDao) {
    this.dataSharingSettingsDao = dao;
    this.demographicDao = demographicDao;
    this.oscarProperties = OscarProperties.getInstance();
  }

  @PostConstruct
  private void initialize() {
    this.dataSharingSettings = dataSharingSettingsDao.getOrganizationSettings();

    this.enableKaiEmr = oscarProperties.getBooleanProperty(ENABLE_KAI_EMR_KEY, "true");
    this.enableTiaHealthPatientPortal
        = oscarProperties.getBooleanProperty(ENABLE_TIA_HEALTH_KEY, "true");
    DataSharingService.kaiEmrUrl = String.format("%s%s",
        oscarProperties.getProperty(KAI_EMR_URL_KEY, "http://localhost:8080"),
        oscarProperties.getProperty(KAI_DEPLOYED_CONTEXT_KEY, "kaiemr")
    );
  }

  /**
   * Get the clinic's DataSharingSetting from the DB.
   * @return settings or null.
   */
  @Nullable
  public DataSharingSettings getOrganizationSettings() {
    return this.dataSharingSettings;
  }

  public boolean isPortalEnabled() {
    return this.enableKaiEmr && this.enableTiaHealthPatientPortal;
  }
  public boolean isDemographicPortalEnabled(Integer demographicNumber) {
    if (isPortalEnabled()) {
      Demographic demographic = demographicDao.getDemographic(demographicNumber);
      return demographic != null && !StringUtils.isEmpty(demographic.getPortalUserId());
    }
    return false;
  }

  public boolean doDataSharingSettingsExist() {
    return this.dataSharingSettings != null;
  }

  public String getButtonDisabledStatus() {
    return dataSharingSettings != null
        ? StringUtils.EMPTY
        : BUTTON_DISABLED_STRING;
  }

  public PatientPortalData.SyncStatus getPortalStatus(
      PatientPortalData.Type type,
      Object portalDataObject
  ) {
    if (PatientPortalData.Type.CONDITION.equals(type)) {
      return (new PatientPortalData((dxResearchBean) portalDataObject)).getPortalStatus();
    } else if (PatientPortalData.Type.PREVENTION.equals(type)) {
      return (new PatientPortalData((Prevention) portalDataObject)).getPortalStatus();
    } else if (PatientPortalData.Type.PROCEDURE.equals(type)) {
      return (new PatientPortalData((CaseManagementNote) portalDataObject)).getPortalStatus();
    } else {
      return SyncStatus.UNAVAILABLE;
    }
  }

  public String getPortalStatusHtml(PatientPortalData.SyncStatus status, String contextPath) {
    return status.getIconHtml(contextPath);
  }

  public String getPortalStatusHtml(String status, String contextPath) {
    return (EnumUtils.isValidEnum(PatientPortalData.SyncStatus.class, status))
        ? PatientPortalData.SyncStatus.valueOf(status).getIconHtml(contextPath)
        : StringUtils.EMPTY;
  }

  /**
   *  Constructor for unit testing ONLY.
   */
  protected DataSharingService(DataSharingSettingsDao dao, OscarProperties properties) {
    this.dataSharingSettingsDao = dao;
    this.oscarProperties = properties;
    initialize();
  }
}
