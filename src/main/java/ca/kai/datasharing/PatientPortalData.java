/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.kai.datasharing;

import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.casemgmt.model.CaseManagementNote;
import org.oscarehr.common.model.Prevention;
import oscar.oscarResearch.oscarDxResearch.bean.dxResearchBean;

@NoArgsConstructor
@Slf4j
public class PatientPortalData implements Serializable {

  @Getter @Setter private boolean available = false;
  @Getter @Setter private Date autoSyncDate;
  @Getter @Setter private Date lastSyncedDate;

  public PatientPortalData(Prevention prevention) {
    this.available = prevention.isAvailable();
    this.lastSyncedDate = prevention.getLastSyncedDate();
    this.autoSyncDate = prevention.getAutoSyncDate();
  }

  public PatientPortalData(dxResearchBean condition) {
    this.available = condition.isAvailable();
    this.lastSyncedDate = condition.getLastSyncedDate();
    this.autoSyncDate = condition.getAutoSyncDate();
  }

  public PatientPortalData(CaseManagementNote note) {
    this.available = note.isAvailable();
    this.lastSyncedDate = note.getLastSyncedDate();
    this.autoSyncDate = note.getAutoSyncDate();
  }

  public boolean isAvailableForPortalSync() {
    return this.available || this.hasAutoSyncDateBeforeNow();
  }

  public boolean wasPreviouslySynced() {
    return this.lastSyncedDate != null;
  }

  public boolean hasAutoSyncDateBeforeNow() {
    return wasPreviouslySynced()
        && this.autoSyncDate != null
        && this.autoSyncDate.before(new Date());
  }

  public SyncStatus getPortalStatus() {
    if (this.isAvailableForPortalSync() && this.wasPreviouslySynced()) {
      return SyncStatus.SYNCED;
    } else if (this.isAvailableForPortalSync()) {
      return SyncStatus.PENDING;
    } else if (this.wasPreviouslySynced()) {
      return SyncStatus.REMOVED;
    }
    return SyncStatus.UNAVAILABLE;
  }

  /* PORTAL DATA ENUM */

  @AllArgsConstructor
  public enum Type {
    CONDITION(dxResearchBean.class),
    PREVENTION(Prevention.class),
    PROCEDURE(CaseManagementNote.class);
    @Getter private final Class<?> dataClass;
  }

  /* PORTAL SYNC STATUS ENUM */

  @AllArgsConstructor
  public enum SyncStatus {
    SYNCED("check-status"),
    PENDING("pending"),
    REMOVED("x"),
    UNAVAILABLE(null);
    private final String icon;

    public String getIconHtml(String contextPath) {
      if (icon == null) {
        return StringUtils.EMPTY;
      }
      return String.format(
          "<img class=\"syncStatusImg\" style=\"%s\" src=\"%s/images/%s.png\" alt=\"\">",
          "width: 13px; margin-left: 20px;",
          contextPath,
          icon
      );
    }
  }
}
