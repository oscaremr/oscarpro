/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package ca.kai.printable;

import com.lowagie.text.DocumentException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.pdfbox.multipdf.Splitter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.json.JSONObject;

/**
 * This utility class contains static methods
 * that are used (and reused) in each servlet.
 *
 * @author Elijah Tungul
 */
public class PrintableUtils {
  /**
   * This function returns a byte array of the first page of a given document.
   *
   * @param document The given document
   * @param response HttpServletResponse
   * @throws IOException IOException for document functions
   */
  public static void writePreview(final PDDocument document, final HttpServletResponse response)
      throws IOException {
    val page1 = new Splitter().split(document).get(0);
    val byteArray = getByteArray(page1);

    try (OutputStream os = response.getOutputStream()) {
      os.write(byteArray, 0, byteArray.length);
    }
  }

  /**
   * This function saves the PDF document given and saves it to the DOCUMENT_DIR directory.
   * The function returns the JSON response containing the path of the file.
   *
   * @param document The given document
   * @param prefix The prefix name of the temp file
   * @param response HttpServletResponse
   * @throws IOException IOException for document functions
   */
  public static void saveAndWritePdfResponse(final PDDocument document,
      final String prefix, final HttpServletResponse response) throws IOException {
    val tempFile = File.createTempFile(prefix, ".pdf");
    val path = tempFile.getPath();

    document.save(tempFile);
    returnJsonResponse(response, path);
  }

  /**
   * This method will return a JSON object containing an error if an exception is reached.
   *
   * @param response HttpServletResponse
   * @throws IOException IOException for document functions
   */
  public static void returnErrorResponse(final HttpServletResponse response,
      final DocumentException documentException)
      throws IOException {
    documentException.printStackTrace();
    response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
  }

  /**
   * This function returns the byte array of a PDDocument (PDF Document).
   *
   * @param document PDF document to be converted to byte array
   * @return A byte array of the given document
   * @throws IOException IOException for document functions
   */
  private static byte[] getByteArray(final PDDocument document) throws IOException {
    val byteArrayOutputStream = new ByteArrayOutputStream();
    document.save(byteArrayOutputStream);
    document.close();
    return byteArrayOutputStream.toByteArray();
  }

  /**
   * This function returns the JSON object containing the path to the file given.
   *
   * @param response HttpServletResponse
   * @param path The file path to be displayed
   * @throws IOException IOException for document functions
   */
  private static void returnJsonResponse(final HttpServletResponse response, final String path)
      throws IOException {
    response.setContentType("application/json");

    val json = new JSONObject();
    json.put("value", path);

    printJsonResponse(response, json);
  }

  private static void printJsonResponse(final HttpServletResponse response, final JSONObject json)
      throws IOException {
    try (val outputStream = response.getOutputStream();
        val writer = new PrintWriter(outputStream)) {

      writer.write(json.toString());
      writer.flush();
    }
  }
}

