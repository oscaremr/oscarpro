/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package ca.kai.printable;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum AntenatalForm {

  AR2005("AR2005", "ONAR"),
  ONAR_ENHANCED("ON AR Enhanced", "ONAREnhanced"),
  PERINATAL("Perinatal 2017", "Perinatal");

  private final String name;

  private final String type;
}
