package ca.kai.printable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Slf4j
public class Printable {
  @EqualsAndHashCode.Include private String id;
  private String name;
  @EqualsAndHashCode.Include private String type;
  private Map<String, Object> params = new HashMap<>();
  private String previewUrl;
  private Date date;
  private boolean supported = true;

  public Printable(final String id, final PrintableAttachmentType type) {
    this.id = id;
    this.type = type.name();
  }
}
