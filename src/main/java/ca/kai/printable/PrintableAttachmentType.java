package ca.kai.printable;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.val;
import org.oscarehr.common.model.ConsultDocs;

/**
 * The AttachmentType of a printable object, items must match the AttachmentType in the Kaiemr
 * project.
 */
public enum PrintableAttachmentType {
  Antenatal(ConsultDocs.DOCTYPE_FORM_PERINATAL, "Perinatal Form"),
  Document(ConsultDocs.DOCTYPE_DOC, "Document"),
  Eforms(ConsultDocs.DOCTYPE_EFORM, "EForm"),
  Hrm(ConsultDocs.DOCTYPE_HRM, "HRM Lab"),
  Lab(ConsultDocs.DOCTYPE_LAB, "Lab Result"),

  // Below are new attachment types supported by attachment manager (not "legacy")
  Consultations("CR", "Consultation"),
  CoverPage("CP", "CoverPage"),
  SmartEncounter("SE", "Smart Encounter"),
  Medication("MED", "Medication"),
  Prevention("PRV", "Prevention"),
  FamilyHistory("FHS", "Family History"),
  MedicalHistory("MHS", "Medical History"),
  Notes("NOT", "Notes"),
  OngoingConcerns("OC", "Ongoing Concerns"),
  OtherMeds("OM", "Other Medications"),
  Reminders("REM", "Reminders"),
  RiskFactors("RF", "Risk Factors"),
  SocialHistory("SC", "Social History");

  @Getter private final String consultationTo1Type;
  @Getter private final String consultationTo1DisplayName;

  private static final Map<String, PrintableAttachmentType> TYPE_LOOKUP;

  PrintableAttachmentType(
      final String consultationTo1Type, final String consultationTo1DisplayName) {
    this.consultationTo1Type = consultationTo1Type;
    this.consultationTo1DisplayName = consultationTo1DisplayName;
  }

  static {
    val typeMap = new HashMap<String, PrintableAttachmentType>();
    for (PrintableAttachmentType attachmentType : PrintableAttachmentType.values()) {
      typeMap.put(attachmentType.consultationTo1Type, attachmentType);
    }
    TYPE_LOOKUP = Collections.unmodifiableMap(typeMap);
  }

  public static PrintableAttachmentType getByConsultationTo1Type(String consultationTo1Type) {
    return TYPE_LOOKUP.get(consultationTo1Type);
  }
}
