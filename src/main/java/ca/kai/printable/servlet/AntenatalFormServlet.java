/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package ca.kai.printable.servlet;

import static ca.kai.printable.PrintableUtils.returnErrorResponse;
import static ca.kai.printable.PrintableUtils.saveAndWritePdfResponse;
import static ca.kai.printable.PrintableUtils.writePreview;

import ca.kai.printable.AntenatalForm;
import com.lowagie.text.DocumentException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import oscar.form.FrmONPerinatalAction;
import oscar.form.dao.ONPerinatal2017Dao;

@Slf4j
public class AntenatalFormServlet extends PrintableServlet {

  ONPerinatal2017Dao onPerinatal2017Dao = SpringUtils.getBean(ONPerinatal2017Dao.class);

  @Override
  void print(final HttpServletRequest request, final HttpServletResponse response)
      throws IOException, ServletException {
    val id = request.getParameter("id");
    val preview = Boolean.parseBoolean(request.getParameter("preview"));
    if (id == null || id.split("-").length < 2) {
      return;
    }
    val idComponents = id.split("-");
    val formType = idComponents[0];
    val formId = Integer.parseInt(idComponents[1]);
    val supportedTypes = Arrays.asList(
        AntenatalForm.PERINATAL.getType(),
        AntenatalForm.ONAR_ENHANCED.getType(),
        AntenatalForm.AR2005.getType()
    );
    if (!supportedTypes.contains(formType)) {
      log.error("Given type is not a supported type.");
      returnErrorResponse(response, new DocumentException());
      return;
    }
    if (AntenatalForm.PERINATAL.getType().equals(formType)) {
      try (val pdf = requestPerinatalForm(formId, request)) {
        if (preview) {
          writePreview(pdf, response);
        } else {
          saveAndWritePdfResponse(pdf, "Perinatal", response);
        }
      }
    } else {
      request.setAttribute("formId", Integer.toString(formId));
      request.setAttribute("demographic_no", "-1");
      request.setAttribute("form_class", formType);
      val isEnhanced = AntenatalForm.ONAR_ENHANCED.getType().equals(formType);
        if (preview) {
          requestPageOneOnArForm(request, isEnhanced);
        } else {
          requestAllOnArForm(request, isEnhanced);
        }
      }
      val rd = request.getRequestDispatcher("/form/createpdf");
      rd.forward(request, response);
  }

  private PDDocument requestPerinatalForm(final int id, final HttpServletRequest request)
      throws IOException {
    try (val byteArrayOutputStream = new ByteArrayOutputStream()) {
      val frmONPerinatalAction = new FrmONPerinatalAction();
      val loggedInInfo = LoggedInInfo.getLoggedInInfoFromSession(request);
      val demographicNumber = onPerinatal2017Dao.getDemographicNumberByFormId(id);
      frmONPerinatalAction.printPdf(byteArrayOutputStream, loggedInInfo, demographicNumber,
          id, Arrays.asList(1, 2, 3, 4, 5));

      return PDDocument.load(byteArrayOutputStream.toByteArray());
    }
  }

  private void requestPageOneOnArForm(final HttpServletRequest request, final boolean isEnhanced) {
    requestOnArFormPage(request, isEnhanced, 1);
  }

  private void requestAllOnArForm(final HttpServletRequest request, final boolean isEnhanced) {
    request.setAttribute("multiple", "2");
    requestOnArFormPage(request, isEnhanced, 1);
    requestOnArFormPage(request, isEnhanced, 2);
    request.setAttribute("returnJson", true);
  }

  private void requestOnArFormPage(final HttpServletRequest request,
      final boolean isEnhanced, final int pageNumber) {
    val enhanced = (isEnhanced ? "enhanced" : "");
    val configPage = "onar" + pageNumber + enhanced + "PrintCfgPg" + pageNumber;
    val configNumber = (pageNumber == 1 ? "" : "1");
    request.setAttribute("__template" + configNumber, "onar" + pageNumber);
    request.setAttribute("__cfgfile" + configNumber, configPage);
  }
}
