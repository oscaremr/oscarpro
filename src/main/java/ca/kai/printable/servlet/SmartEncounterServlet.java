/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package ca.kai.printable.servlet;

import static ca.kai.printable.PrintableUtils.returnErrorResponse;
import static ca.kai.printable.PrintableUtils.saveAndWritePdfResponse;
import static ca.kai.printable.PrintableUtils.writePreview;

import com.lowagie.text.DocumentException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.oscarehr.util.SpringUtils;
import oscar.form.dao.FormSmartEncounterDao;
import oscar.form.pageUtil.FormSmartEncounterAction;

/**
 * This servlet prints the smart encounter sent by the request.
 * The servlet will be used by the attachment manager.
 *
 * @author Elijah Tungul
 */
@Slf4j
public class SmartEncounterServlet extends PrintableServlet {

  /**
   * Smart Encounter Servlet for Attachment Manager
   *
   * This servlet takes two parameters: id and preview. ID obtains the printable at the given ID,
   * while preview is a boolean that determines whether to show a preview of the form.
   * If the preview parameter is true, then it returns a byte array containing the preview image of
   * the printable. If preview is not true (false), it prints the files, saving them to the temp
   * directory. The browser also returns a JSON response with the attribute
   * "value" as the path to the saved file.
   *
   * @param request HttpServletRequest
   * @param response HttpServletResponse
   * @throws IOException IOException
   * @throws ServletException ServletException
   */
  @Override
  public void print(final HttpServletRequest request, final HttpServletResponse response)
      throws IOException, ServletException {
    val id = Integer.parseInt(request.getParameter("id"));
    val preview = Boolean.parseBoolean(request.getParameter("preview"));

    val formSmartEncounterDao =
        (FormSmartEncounterDao) SpringUtils.getBean(FormSmartEncounterDao.class);
    val smartEncounter = formSmartEncounterDao.find(id);

    val byteArrayOutputStream = new ByteArrayOutputStream();

    try {
      FormSmartEncounterAction.generatePdf(smartEncounter, byteArrayOutputStream);
    } catch (DocumentException documentException) {
      log.error("An error occurred while creating the smart encounter PDF for request [{}]: ", id);
      returnErrorResponse(response, documentException);
      return;
    }

    val pdf = PDDocument.load(byteArrayOutputStream.toByteArray());

    if (preview) {
      writePreview(pdf, response);
    } else {
      saveAndWritePdfResponse(pdf, "SmartEncounter", response);
    }
  }
}
