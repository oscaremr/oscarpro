/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package ca.kai.printable.servlet;

import static ca.kai.printable.PrintableUtils.saveAndWritePdfResponse;
import static ca.kai.printable.PrintableUtils.writePreview;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.oscarehr.fax.util.PdfCoverPageCreator;
import org.oscarehr.util.SpringUtils;

/**
 * This servlet prints the cover page sent by the request.
 * The servlet will be used by the attachment manager.
 *
 * @author Elijah Tungul
 */
public class CoverPageServlet extends PrintableServlet {

  /**
   * Cover Page Servlet for Attachment Manager
   *
   * This servlet takes two parameters: id and preview. ID obtains the printable at the given ID,
   * while preview is a boolean that determines whether to show a preview of the form.
   * If the preview parameter is true, then it returns a byte array containing the preview image of
   * the printable. If preview is not true (false), it prints the files, saving them to the
   * kai_documents directory. The browser also returns a JSON response with the attribute
   * "value" as the path to the saved file.
   *
   * @param request HttpServletRequest
   * @param response HttpServletResponse
   * @throws IOException IOException
   * @throws ServletException ServletException
   */
  @Override
  public void print(final HttpServletRequest request, final HttpServletResponse response)
      throws IOException, ServletException {
    val preview = Boolean.parseBoolean(request.getParameter("preview"));
    String subject = request.getParameter("subject");
    String message = request.getParameter("message");

    val pdfCoverPageCreator = (PdfCoverPageCreator) SpringUtils.getBean("pdfCoverPageCreator");
    val pdf = PDDocument.load(pdfCoverPageCreator.createCoverPage(subject, message));

    if (preview) {
      writePreview(pdf, response);
    } else {
      saveAndWritePdfResponse(pdf, "CoverPage", response);
    }
  }
}

