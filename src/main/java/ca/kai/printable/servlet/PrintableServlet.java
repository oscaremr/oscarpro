/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package ca.kai.printable.servlet;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

/**
 * This servlet acts as a wrapper servlet for each servlet in printable/servlet.
 *
 * @author Elijah Tungul
 */
public abstract class PrintableServlet extends HttpServlet {
  private final List<String> LOCAL_IPS = Arrays.asList("127.0.0.1", "::1", "0:0:0:0:0:0:0:1");

  /**
   * This method will authenticate a request from a user.
   * If the user is accessing this servlet from their local machine,
   * then allow them to use it.
   *
   * @param request HttpServletRequest
   * @param response HttpServletResponse
   * @throws IOException IOException
   * @throws ServletException ServletException
   */
  public void service(final HttpServletRequest request, final HttpServletResponse response)
      throws IOException, ServletException {
    if (!LOCAL_IPS.contains(request.getRemoteAddr())) {
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }

    setLoggedInProvider(request, request.getParameter("providerNumber"));
    print(request, response);
  }

  /**
   * This function sets the logged in provider to the respective provider
   * with the associated provider number.
   *
   * @param request HttpServletRequest
   * @param providerNumber The provider number of the provider to be set as logged in
   */
  private static void setLoggedInProvider(final HttpServletRequest request,
      final String providerNumber) {
    val providerDao = (ProviderDao) SpringUtils.getBean(ProviderDao.class);
    val provider = providerDao.getProvider(providerNumber);

    val loggedInInfo = new LoggedInInfo();
    loggedInInfo.setLoggedInProvider(provider);

    LoggedInInfo.setLoggedInInfoIntoSession(request.getSession(), loggedInInfo);
  }

  abstract void print(final HttpServletRequest request, final HttpServletResponse response)
      throws IOException, ServletException;
}
