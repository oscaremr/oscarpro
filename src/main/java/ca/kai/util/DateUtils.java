/**
 * Copyright (c) 2023 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package ca.kai.util;

import static org.oscarehr.util.OscarTrackingBasicDataSource.logger;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import lombok.val;
import lombok.var;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.validator.routines.DateValidator;

public class DateUtils {

  private static final String TODAY_BACKGROUND_COLOR = "gold";
  private static final String NOT_TODAY_BACKGROUND_COLOR = "#EEEEFF";
  public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd";
  public static final String DEFAULT_TS_PATTERN = "yyyy-MM-dd HH:mm:ss";
  public static final int TIMESTAMP_LENGTH = 19;

  public static String getDateString(final String format, final Date date) {
    var dateFormat = new SimpleDateFormat();
    try {
      dateFormat = new SimpleDateFormat(format);
      return dateFormat.format(date);
    } catch (NullPointerException | IllegalArgumentException e) {
      logger.error("the given pattern is invalid or Null", e);
      return "";
    }
  }

  public static String getDateString(final Date date) {
    return getDateString("yyyy-MM-dd", date);
  }

  public static Date getDateByString(final String format, final String dateString) {
    val dateFormat = new SimpleDateFormat(format);
    try {
      return dateFormat.parse(dateString);
    } catch (ParseException e) {
      throw new IllegalArgumentException("Invalid date format");
    }
  }

  public static Date getDateByString(final String dateString) {
    return getDateByString("yyyy-MM-dd", dateString);
  }

  public static Date stripMilliseconds(final Date date) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      return sdf.parse(sdf.format(date));
    } catch (ParseException e) {
      return date;
    }
  }

  public static String getCurrentDate() {
    return getDateString(new Date());
  }

  public static int getCurrentYear() {
    return new GregorianCalendar().get(Calendar.YEAR);
  }

  public static int getCurrentMonth() {
    return new GregorianCalendar().get(Calendar.MONTH) + 1;
  }

  public static int getCurrentDay() {
    return new GregorianCalendar().get(Calendar.DAY_OF_MONTH);
  }

  public static String getDayCssBackgroundColor(final Integer year, final Integer month, final Integer day) {
    return getCurrentYear() == year && getCurrentMonth() == month && getCurrentDay() == day
        ? TODAY_BACKGROUND_COLOR
        : NOT_TODAY_BACKGROUND_COLOR;
  }

  public static int getCurrentYearFromRequest(final HttpServletRequest request) {
    return request != null && StringUtils.isNumeric(request.getParameter("year"))
        ? Integer.parseInt(request.getParameter("year"))
        : -1;
  }

  public static int getCurrentMonthFromRequest(final HttpServletRequest request) {
    return request != null && StringUtils.isNumeric(request.getParameter("month"))
        ? Integer.parseInt(request.getParameter("month"))
        : -1;
  }

  public static int getDeltaFromRequest(final HttpServletRequest request) {
    return request != null && NumberUtils.isNumber(request.getParameter("delta"))
        ? Integer.parseInt(request.getParameter("delta"))
        : 0;
  }
  public static int adjustYearForCalendarPopup(final int year, final int month) {
    if (month < 1) {
      return year - 1;
    } else if (month > 12) {
      return year + 1;
    }
    return year;
  }
  public static int adjustMonthForCalendarPopup(final int month) {
    if (month < 1) {
      return 12 + month;
    } else if (month > 12) {
      return month - 12;
    }
    return month;
  }

  /**
   * Converts a date string into a Date object using a default format. The default format is
   * determined based on the length of the date string. If the date string is null or empty, an
   * empty Optional is returned.
   *
   * @param dateString the string to be converted into a Date object
   * @return an Optional<Date> object representing the date and time specified by the input string,
   *     or an empty Optional if the input string is null or empty
   */
  public static Optional<Date> convertDateString(final String dateString) {
    if (dateString == null || dateString.isEmpty()) {
      return Optional.empty();
    }
    if (dateString.length() == TIMESTAMP_LENGTH) {
      return convertDateString(dateString, DEFAULT_TS_PATTERN);
    } else {
      return convertDateString(dateString, DEFAULT_DATE_PATTERN);
    }
  }

  /**
   * Converts a date string into a Date object according to the provided format. If the format is
   * null or empty, an IllegalArgumentException is thrown. If the date string is not in the provided
   * format, an empty Optional is returned.
   *
   * @param dateString the string to be converted into a Date object
   * @param format the format of the dateString
   * @return an Optional<Date> object representing the date and time specified by the input string,
   *     or an empty Optional if the input string is not in the expected format
   * @throws IllegalArgumentException if the format is null or empty
   */
  public static Optional<Date> convertDateString(final String dateString, final String format) {
    if (format == null || format.isEmpty()) {
      throw new IllegalArgumentException("Format cannot be null or empty");
    }
    val date = DateValidator.getInstance().validate(dateString, format);
    return Optional.ofNullable(date);
  }
}
