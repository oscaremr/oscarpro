-- MySQL dump 10.13  Distrib 5.5.28, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: oscar_test
-- ------------------------------------------------------
-- Server version	5.5.28-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `billingmaster`
--

LOCK TABLES `billingmaster` WRITE;
/*!40000 ALTER TABLE `billingmaster` DISABLE KEYS */;

INSERT INTO `billingmaster`
(billingmaster_no, billing_no, createdate, billingstatus,
 demographic_no, appointment_no, claimcode, datacenter,
 payee_no, practitioner_no, phn, name_verify, dependent_num,
 billing_unit, clarification_code, anatomical_area, after_hour,
 new_program, billing_code, bill_amount, payment_mode,
 service_date, service_to_day, submission_code,
 extended_submission_code, dx_code1, dx_code2, dx_code3,
 dx_expansion, service_location, referral_flag1, referral_no1,
 referral_flag2, referral_no2, time_call, service_start_time,
 service_end_time, birth_date, office_number,
 correspondence_code, claim_comment, mva_claim_code,
 icbc_claim_no, original_claim, facility_no, facility_sub_no,
 filler_claim, oin_insurer_code, oin_registration_no,
 oin_birthdate, oin_first_name, oin_second_name, oin_surname,
 oin_sex_code, oin_address, oin_address2, oin_address3,
 oin_address4, oin_postalcode, paymentMethod, wcb_id)
VALUES (1, 1, NULL, NULL, 1, 0, 'C02', '00000', '00000', '00000', '0000000000', '', '00', '000',
        '00', NULL, '0', '00', '00000', '000000000', '0', '00000000', '00', '0', '', '', '', '', '',
        '0', '0', '00000', '0', '00000', '0000', '0000', '0000', '00000000', '0000000', '0', NULL,
        'N', '00000000', '00000000000000000000', '00000', '00000', NULL, '', '', '', '', '', '', '',
        '', '', '', '', '', 6, NULL);
/*!40000 ALTER TABLE `billingmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `wcb`
--

LOCK TABLES `wcb` WRITE;
/*!40000 ALTER TABLE `wcb` DISABLE KEYS */;

INSERT INTO `wcb`
(ID, billing_no, demographic_no, provider_no, formCreated, formEdited, w_reporttype, bill_amount,
 w_fname, w_lname, w_mname, w_gender, w_dob, w_address, w_city, w_postal, w_area, w_phone, w_phn,
 w_empname, w_emparea, w_empphone, w_wcbno, w_opaddress, w_opcity, w_rphysician, w_duration,
 w_problem, w_servicedate, w_diagnosis, w_icd9, w_bp, w_side, w_noi, w_work, w_workdate,
 w_clinicinfo, w_capability, w_capreason, w_estimate, w_rehab, w_rehabtype, w_wcbadvisor,
 w_ftreatment, w_estimatedate, w_tofollow, w_payeeno, w_pracno, w_doi, `status`, w_feeitem,
 w_extrafeeitem, w_servicelocation, formNeeded)
VALUES (1, 1, 1, 999998, '2012-12-12 21:39:03', '2012-12-12 21:39:03', 'a', '0.00', NULL, NULL,
        NULL, NULL, '0000-00-00', NULL, '', NULL, NULL, NULL, 'a', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, '2012-12-12', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2012-12-12', 'A', 'a', 'a',
        'O', 1);
/*!40000 ALTER TABLE `wcb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `demographic`
--

LOCK TABLES `demographic` WRITE;
/*!40000 ALTER TABLE `demographic` DISABLE KEYS */;
TRUNCATE TABLE `demographic`;
INSERT INTO `demographic`
(demographic_no, title, last_name, first_name, address, city,
 province, postal, phone, phone2, email, myOscarUserName,
 year_of_birth, month_of_birth, date_of_birth, hin, ver,
 roster_status, roster_date, roster_termination_date,
 roster_termination_reason, patient_status, patient_status_date,
 date_joined, chart_no, official_lang, spoken_lang, provider_no,
 sex, end_date, eff_date, pcn_indicator, hc_type, hc_renew_date,
 family_doctor, family_physician, alias, previousAddress,
 children, sourceOfIncome, citizenship, sin, country_of_origin,
 newsletter, anonymous, lastUpdateUser, lastUpdateDate,
 patient_type, patient_id, pref_name, consentToUseEmailForCare,
 portal_user_id, cached_link_demographic_state, genderId,
 pronounId)
VALUES (2, null, 'last', 'first', null, null, null, null, null, null, null, null, null, null, null, null,
        null, null, null, null, null, null, null, null, null, null, null, null, 'M', null, null,
        null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
        '2022-12-30 18:58:08', null, null, '', null, null, 'UNLINKED', null, null);
/*!40000 ALTER TABLE `demographic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `teleplanC12`
--

LOCK TABLES `teleplanC12` WRITE;
/*!40000 ALTER TABLE `teleplanC12` DISABLE KEYS */;

INSERT INTO `teleplanC12`
(c12_id, s21_id, filename, t_datacenter, t_dataseq, t_payeeno, t_practitioner_no, t_exp1, t_exp2,
 t_exp3, t_exp4, t_exp5, t_exp6, t_exp7, t_officefolioclaimno, t_filler, `status`)
VALUES
(1,1,'a','a','a','a','a','1','2','3','4','5','6','7','1','a','A');
/*!40000 ALTER TABLE `teleplanC12` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2012-12-12 21:40:35
