/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package apps.health.pillway;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

import apps.health.pillway.PillwayLog.Method;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.oscarehr.common.dao.DemographicPharmacyDao;
import org.oscarehr.common.dao.PharmacyInfoDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.model.PharmacyInfo;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.common.model.UserProperty;

public class PillwayManagerTest {

  @Mock private DemographicPharmacyDao demographicPharmacyDao;

  @Mock private PillwayLogDao pillwayLogDao;

  @Mock private PharmacyInfoDao pharmacyInfoDao;

  @Mock private SystemPreferencesDao systemPreferencesDao;

  @Mock private UserPropertyDAO userPropertyDao;

  private PillwayManager pillwayManager;

  @Before
  public void before() throws Exception {
    MockitoAnnotations.openMocks(this);
    pillwayManager =
        new PillwayManager(
            demographicPharmacyDao, pharmacyInfoDao, pillwayLogDao, systemPreferencesDao, userPropertyDao);
  }

  @Test
  public void testIsPillWayPharmacy_notFound() {
    assertNull(pharmacyInfoDao.getPharmacy(99999));
    assertFalse(pillwayManager.isPillwayPharmacy(99999));
  }

  @Test
  public void testIsPillWayPharmacy_notPillway() throws Exception {
    setupPharmacy("TEST");
    assertFalse(pillwayManager.isPillwayPharmacy(1));
  }

  @Test
  public void testIsPillWayPharmacy_noServiceLocationIdentifier() throws Exception {
    setupPharmacy(null);
    assertFalse(pillwayManager.isPillwayPharmacy(1));
    setupPharmacy("");
    assertFalse(pillwayManager.isPillwayPharmacy(1));
  }

  @Test
  public void testIsPillWayPharmacy_pillwayFound() throws Exception {
    setupPharmacy(PillwayManager.PILLWAY_ID);
    assertTrue(pillwayManager.isPillwayPharmacy(1));
  }

  @Test
  public void testLog_nonPillwayPharmacy() throws Exception {
    PillwayLog log = pillwayManager.log(1, "1", "2", 3, Method.FAX);
    assertNull(log);
    Mockito.verify(pillwayLogDao, Mockito.never()).persist(any(PillwayLog.class));
  }

  @Test
  public void testLog_pillwayPharmacy() throws Exception {
    setupPharmacy(PillwayManager.PILLWAY_ID);
    String rxId = "1";
    String providerNo = "2";
    int demographicNo = 3;
    Method method = Method.FAX;
    PillwayLog log = pillwayManager.log(1, rxId, providerNo, demographicNo, method);
    assertEquals(log.getRxId(), rxId);
    assertEquals(log.getProviderNo(), providerNo);
    assertEquals(log.getDemographicNo(), Integer.valueOf(demographicNo));
    assertEquals(log.getMethod(), method);
  }

  @Test
  public void testLog_method() throws Exception {
    setupPharmacy(PillwayManager.PILLWAY_ID);
    PillwayLog log = pillwayManager.log(1, "1", "2", 3, Method.API);
    assertEquals(log.getMethod(), Method.API);
    log = pillwayManager.log(1, "1", "2", 3, Method.FALLBACK);
    assertEquals(log.getMethod(), Method.FALLBACK);
    log = pillwayManager.log(1, "1", "2", 3, Method.FAX);
    assertEquals(log.getMethod(), Method.FAX);
    log = pillwayManager.log(1, "1", "2", 3, null);
    assertNull(log.getMethod());
  }

  @Test
  public void testAddToDemographic_notEnabled() throws Exception {
    setupSystemPreference("false");
    setupPharmacy(PillwayManager.PILLWAY_ID);
    pillwayManager.addToDemographic(1);
    Mockito.verify(demographicPharmacyDao, Mockito.never())
        .addPharmacyToDemographic(anyInt(), anyInt());
  }

  @Test
  public void testAddToDemographic_pharmacyNotFound() {
    setupSystemPreference("true");
    pillwayManager.addToDemographic(1);
    Mockito.verify(demographicPharmacyDao, Mockito.never())
        .addPharmacyToDemographic(anyInt(), anyInt());
  }

  @Test
  public void testAddToDemographic_happyPath() throws Exception {
    setupSystemPreference("true");
    setupPharmacy(PillwayManager.PILLWAY_ID);
    pillwayManager.addToDemographic(2);
    Mockito.verify(demographicPharmacyDao).addPharmacyToDemographic(1, 2);
  }

  @Test
  public void testIsAddToDemographicEnabled_false() {
    setupSystemPreference("false");
    assertFalse(pillwayManager.isAddToDemographicEnabled());
  }

  @Test
  public void testIsAddToDemographicEnabled_true() {
    setupSystemPreference("true");
    assertTrue(pillwayManager.isAddToDemographicEnabled());
  }

  @Test
  public void testIsPillwayButtonEnabled_propertyMissing() throws Exception {
    setupPreference(null);
    assertTrue(pillwayManager.isPillwayButtonEnabled("123456"));
    Mockito.verify(userPropertyDao).getProp("123456", UserProperty.PILLWAY_BUTTON_ENABLED);
  }

  @Test
  public void testIsPillwayButtonEnabled_propertyFalse() throws Exception {
    setupPreference("false");
    assertFalse(pillwayManager.isPillwayButtonEnabled("123456"));
    Mockito.verify(userPropertyDao).getProp("123456", UserProperty.PILLWAY_BUTTON_ENABLED);
  }

  @Test
  public void testIsPillwayButtonEnabled_propertyTrue() throws Exception {
    setupPreference("true");
    assertTrue(pillwayManager.isPillwayButtonEnabled("123456"));
    Mockito.verify(userPropertyDao).getProp("123456", UserProperty.PILLWAY_BUTTON_ENABLED);
  }

  private void setupPreference(String value) {
    UserProperty prop = null;
    if (value != null) {
      prop = new UserProperty();
      prop.setValue(value);
    }
    Mockito.doReturn(prop).when(userPropertyDao).getProp(anyString(), anyString());
  }

  private void setupPharmacy(String identifier) throws Exception {
    PharmacyInfo info = new PharmacyInfo();
    info.setId(1);
    info.setServiceLocationIdentifier(identifier);
    Mockito.doReturn(info).when(pharmacyInfoDao).getPharmacy(anyInt());
    Mockito.doReturn(info).when(pharmacyInfoDao).getPharmacyByIdentifier(anyString());
  }

  private void setupSystemPreference(String value) {
    SystemPreferences pref = new SystemPreferences();
    pref.setValue(value);
    Mockito.doReturn(pref).when(systemPreferencesDao).findPreferenceByName(anyString());
  }
}
