package ca;

import ca.kai.printable.PrintableUtils;
import ca.kai.printable.servlet.CoverPageServlet;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.fax.util.PdfCoverPageCreator;
import org.oscarehr.util.SpringUtils;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CoverPageServletTest {

  private MockedStatic<SpringUtils> springUtils;
  private MockedStatic<PDDocument> pdDocument;
  private MockedStatic<PrintableUtils> mockedPrintableUtils;
  PdfCoverPageCreator pdfCoverPageCreator = Mockito.mock(PdfCoverPageCreator.class);

  HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
  HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

  @BeforeEach
  public void setup() {

    springUtils = mockStatic(SpringUtils.class);
    springUtils.when(() -> SpringUtils.getBean(eq("pdfCoverPageCreator")))
        .thenReturn(pdfCoverPageCreator);
    PDDocument document = Mockito.mock(PDDocument.class);
    pdDocument = mockStatic(PDDocument.class);
    pdDocument.when(() -> PDDocument.load(anyString().getBytes())).thenReturn(document);

    mockedPrintableUtils = mockStatic(PrintableUtils.class);

  }

  @AfterEach
  public void tearDown() {

    springUtils.close();
    pdDocument.close();
    mockedPrintableUtils.close();

  }

  /**
   * Show that when the preview parameter in request is true, then print will call the writePreview
   * method.
   */
  @Test
  public void givenPreview_whenPrint_thenWritePreview()
      throws ServletException, IOException {

    when(request.getParameter("preview")).thenReturn("true");
    CoverPageServlet coverPageServlet = new CoverPageServlet();
    coverPageServlet.print(request, response);

    mockedPrintableUtils.verify(() -> PrintableUtils.writePreview(any(), any()));

  }

  /**
   * Show that when the preview parameter in request is false, then print will call the
   * saveAndWritePdfResponse method.
   */
  @Test
  public void givenNotPreview_whenPrint_thenSaveAndWritePdfResponse()
      throws ServletException, IOException {

    when(request.getParameter("preview")).thenReturn("false");
    CoverPageServlet coverPageServlet = new CoverPageServlet();
    coverPageServlet.print(request, response);

    mockedPrintableUtils.verify(
        () -> PrintableUtils.saveAndWritePdfResponse(any(), anyString(), any()));

  }

  private static Object[] customCoverPageRequestParameters() {
    return new Object[] {
        new Object[] { "subject", "message" },
        new Object[] { "subject", null },
        new Object[] { null, "message" },
        new Object[] { null, null }
    };
  }

  @ParameterizedTest
  @MethodSource("customCoverPageRequestParameters")
  public void givenRequestParameters_whenPrint_thenCreateCoverPageWithNote(final String subject, final String message) throws ServletException, IOException {

    when(request.getParameter("subject")).thenReturn(subject);
    when(request.getParameter("message")).thenReturn(message);

    CoverPageServlet coverPageServlet = new CoverPageServlet();
    coverPageServlet.print(request, response);

    Mockito.verify(pdfCoverPageCreator).createCoverPage(subject, message);

  }

}
