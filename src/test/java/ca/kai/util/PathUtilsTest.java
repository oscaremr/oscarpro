/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package ca.kai.util;

import org.junit.Test;
import org.oscarehr.util.PathUtils;

import static org.junit.Assert.assertEquals;

public class PathUtilsTest {

  @Test
  public void givenEmpty_whenAddLeadingSlash_thenReturnSlash() {
    assertEquals("/", PathUtils.addLeadingSlash(""));
  }

  @Test
  public void givenRoot_whenAddLeadingSlash_thenReturnSlash() {
    assertEquals("/", PathUtils.addLeadingSlash("/"));
  }

  @Test
  public void givenTest_whenAddLeadingSlash_thenReturnTestWithLeadingSlash() {
    assertEquals("/test", PathUtils.addLeadingSlash("test"));
  }

  @Test
  public void givenRootTest_whenAddLeadingSlash_thenReturnRootTest() {
    assertEquals("/test", PathUtils.addLeadingSlash("/test"));
  }

  @Test
  public void givenDoubleRootTest_whenAddLeadingSlash_thenReturnDoubleRootTest() {
    assertEquals("//test", PathUtils.addLeadingSlash("//test"));
  }

  @Test
  public void givenEmpty_whenSurroundWithSlashes_thenReturnSlash() {
    assertEquals("/", PathUtils.surroundWithSlashes(""));
  }

  @Test
  public void givenRoot_whenSurroundWithSlashes_thenReturnSlash() {
    assertEquals("/", PathUtils.surroundWithSlashes("/"));
  }

  @Test
  public void givenTest_whenSurroundWithSlashes_thenReturnTestSurroundedWithSlashes() {
    assertEquals("/test/", PathUtils.surroundWithSlashes("test"));
  }

  @Test
  public void givenTestRoot_whenSurroundWithSlashes_thenReturnTestSurroundedWithSlashes() {
    assertEquals("/test/", PathUtils.surroundWithSlashes("test/"));
  }

  @Test
  public void givenRootTest_whenSurroundWithSlashes_thenReturnTestSurroundedWithSlashes() {
    assertEquals("/test/", PathUtils.surroundWithSlashes("/test"));
  }

  @Test
  public void givenRootTestRoot_whenSurroundWithSlashes_thenReturnTestSurroundedWithSlashes() {
    assertEquals("/test/", PathUtils.surroundWithSlashes("/test/"));
  }

  @Test
  public void givenDoubleRootTestDoubleRoot_whenSurroundWithSlashes_thenReturnDoubleRootTestDoubleRoot() {
    assertEquals("//test//", PathUtils.surroundWithSlashes("//test//"));
  }

  @Test
  public void givenEmpty_whenAddTrailingSlash_thenReturnSlash() {
    assertEquals("/", PathUtils.addTrailingSlash(""));
  }

  @Test
  public void givenRoot_whenAddTrailingSlash_thenReturnSlash() {
    assertEquals("/", PathUtils.addTrailingSlash("/"));
  }

  @Test
  public void givenTest_whenAddTrailingSlash_thenReturnTestWithTrailingSlash() {
    assertEquals("test/", PathUtils.addTrailingSlash("test"));
  }

  @Test
  public void givenTestRoot_whenAddTrailingSlash_thenReturnTestWithTrailingSlash() {
    assertEquals("test/", PathUtils.addTrailingSlash("test/"));
  }

  @Test
  public void givenTestDoubleRoot_whenAddTrailingSlash_thenReturnTestWithDoubleTrailingSlash() {
    assertEquals("test//", PathUtils.addTrailingSlash("test//"));
  }
}