package ca.kai.util;

import static ca.kai.util.DateUtils.DEFAULT_DATE_PATTERN;
import static ca.kai.util.DateUtils.DEFAULT_TS_PATTERN;
import static ca.kai.util.DateUtils.TIMESTAMP_LENGTH;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

public class DateUtilsTest {

  private static final String DATE_FORMAT = "yyyy-MM-dd";
  private static final String ERROR_DATE = "11-2022-16";
  private static final String REQUEST_YEAR = "2022";
  private static final String REQUEST_MONTH = "11";
  private static final String REQUEST_DELTA = "1";
  private static final String SAMPLE_DATE = "2022-11-16";
  private static final int DELTA_YEAR_FORWARD = 12;
  private static final int DELTA_YEAR_BACK = -12;
  private static final int START_YEAR = 2024;
  private static final int START_MONTH = 02;
  private static final int MONTH_JUNE = 06;
  private static final int ZERO_DELTA = 0;

  @Test
  public void testGetCurrentYearFromRequestReturnsYear() {
    val request = Mockito.mock(HttpServletRequest.class);
    Mockito.when(request.getParameter("year")).thenReturn(REQUEST_YEAR);
    Assertions.assertEquals(2022, DateUtils.getCurrentYearFromRequest(request));
  }

  @Test
  public void testGetCurrentYearFromRequestReturnsNegativeOne() {
    val request = Mockito.mock(HttpServletRequest.class);
    Mockito.when(request.getParameter("year")).thenReturn(null);
    Assertions.assertEquals(-1, DateUtils.getCurrentYearFromRequest(request));
  }
  @Test
  public void givenOneYearForward_whenAdjustYearForCalendarPopup_thenReturnIncreaseYearByOne() {
    val adjustedMonth = START_MONTH + DELTA_YEAR_FORWARD;
    Assertions.assertEquals(2025, DateUtils.adjustYearForCalendarPopup(START_YEAR, adjustedMonth));
  }
  @Test
  public void givenOneYearForward_whenAdjustMonthForCalendarPopup_thenReturnMonthUnchanged() {
    val adjustedMonth = START_MONTH + DELTA_YEAR_FORWARD;
    Assertions.assertEquals(02, DateUtils.adjustMonthForCalendarPopup(adjustedMonth));
  }
  @Test
  public void givenOneYearBackwards_whenAdjustYearForCalendarPopup_thenReturnDecreaseYearByOne() {
    val adjustedMonth = START_MONTH + DELTA_YEAR_BACK;
    Assertions.assertEquals(2023, DateUtils.adjustYearForCalendarPopup(START_YEAR, adjustedMonth));
  }
  @Test
  public void givenOneYearBackwards_whenAdjustMonthForCalendarPopup_thenReturnMonthUnchanged() {
    val adjustedMonth = START_MONTH + DELTA_YEAR_BACK;
    Assertions.assertEquals(02, DateUtils.adjustMonthForCalendarPopup(adjustedMonth));
  }
  @Test
  public void givenOneMonthBackwards_whenAdjustYearForCalendarPopup_thenReturnYearUnchanged() {
    val adjustedMonth = START_MONTH + Integer.parseInt(REQUEST_DELTA);
    Assertions.assertEquals(2024, DateUtils.adjustYearForCalendarPopup(START_YEAR, adjustedMonth));
  }
  @Test
  public void givenOneMonthBackwards_whenAdjustMonthForCalendarPopup_thenReturnDecreaseMonthByOne() {
    val adjustedMonth = START_MONTH + (Integer.parseInt(REQUEST_DELTA) * -1);
    Assertions.assertEquals(01, DateUtils.adjustMonthForCalendarPopup(adjustedMonth));
  }
  @Test
  public void givenOneMonthForward_whenAdjustYearForCalendarPopup_thenReturnYearUnchanged() {
    val adjustedMonth = START_MONTH + Integer.parseInt(REQUEST_DELTA);
    Assertions.assertEquals(2024, DateUtils.adjustYearForCalendarPopup(START_YEAR, adjustedMonth));
  }
  @Test
  public void givenOneMonthForward_whenAdjustMonthForCalendarPopup_thenReturnIncreaseMonthByOne() {
    val adjustedMonth = START_MONTH + Integer.parseInt(REQUEST_DELTA);
    Assertions.assertEquals(03, DateUtils.adjustMonthForCalendarPopup(adjustedMonth));
  }
  @Test
  public void givenAnySelectedMonth_whenAdjustMonthForCalendarPopup_thenReturnTheSelectedMonth() {
    val adjustedMonth = MONTH_JUNE + ZERO_DELTA;
    Assertions.assertEquals(06, DateUtils.adjustMonthForCalendarPopup(adjustedMonth));
  }
  @Test
  public void testGetCurrentMonthFromRequestReturnsMonth() {
    val request = Mockito.mock(HttpServletRequest.class);
    Mockito.when(request.getParameter("month")).thenReturn(REQUEST_MONTH);
    Assertions.assertEquals(11, DateUtils.getCurrentMonthFromRequest(request));
  }

  @Test
  public void testGetCurrentMonthFromRequestReturnsNegativeOne() {
    val request = Mockito.mock(HttpServletRequest.class);
    Mockito.when(request.getParameter("month")).thenReturn(null);
    Assertions.assertEquals(-1, DateUtils.getCurrentMonthFromRequest(request));
  }

  @Test
  public void testGetDeltaFromRequestReturnsDelta() {
    val request = Mockito.mock(HttpServletRequest.class);
    Mockito.when(request.getParameter("delta")).thenReturn(REQUEST_DELTA);
    Assertions.assertEquals(1, DateUtils.getDeltaFromRequest(request));
  }

  @Test
  public void testGetDeltaFromRequestReturnsZero() {
    HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
    Mockito.when(request.getParameter("delta")).thenReturn("");
    Assertions.assertEquals(0, DateUtils.getDeltaFromRequest(request));
  }

  @Test
  public void testGetDateStringWithSampleDate() throws ParseException {
    Date date = new SimpleDateFormat(DATE_FORMAT).parse(SAMPLE_DATE);
    String dateString = DateUtils.getDateString(DATE_FORMAT, date);
    Assertions.assertEquals(dateString, SAMPLE_DATE);
  }

  // OSCAR-4817 disabled because the test is invalid.
  @Disabled
  @Test
  public void testGetDateStringWithErrorDate() throws ParseException {
    val date = new SimpleDateFormat(DATE_FORMAT).parse(ERROR_DATE);
    DateUtils.getDateString(DATE_FORMAT, date);
  }

  // OSCAR-4817 disabled because the test is invalid.
  @Disabled
  @Test
  public void testGetDateStringWithNullDate() throws ParseException {
    DateUtils.getDateString(DATE_FORMAT, null);
  }

  @Test
  public void testGetDateByStringWithFormat() {
    val date = DateUtils.getDateByString(DATE_FORMAT, SAMPLE_DATE);
    val sdf = new SimpleDateFormat(DATE_FORMAT);
    val result = sdf.format(date);
    Assertions.assertEquals(SAMPLE_DATE, result);
  }

  @Test
  public void testGetDateByStringWithoutFormat() {
    val date = DateUtils.getDateByString(SAMPLE_DATE);
    val sdf = new SimpleDateFormat(DATE_FORMAT);
    val result = sdf.format(date);
    Assertions.assertEquals(SAMPLE_DATE, result);
  }

  @ParameterizedTest
  @NullAndEmptySource
  public void givenNullOrEmptyDateString_whenConvertDateString_thenReturnsEmptyOptional(
      String dateString) {
    val result = DateUtils.convertDateString(dateString);
    Assertions.assertFalse(result.isPresent());
  }

  @ParameterizedTest
  @ValueSource(strings = {"2022-12-31", "2023-01-01 10:10:10"})
  public void givenValidDateString_whenConvertDateString_thenReturnsOptionalWithDate(
      String dateString) {
    val result = DateUtils.convertDateString(dateString);
    Assertions.assertTrue(result.isPresent());

    val sdf = new SimpleDateFormat(getDateFormat(dateString));
    try {
      val expectedDate = sdf.parse(dateString);
      val actualDate = result.get();
      Assertions.assertEquals(expectedDate, actualDate);
    } catch (ParseException e) {
      Assertions.fail("Could not parse date string");
    }
  }

  @ParameterizedTest
  @ValueSource(strings = {"invalid-date", "2023-12-31 10:10:100", "2023-12-35"})
  public void givenInvalidDateString_whenConvertDateStringWithFormat_thenReturnsEmptyOptional(
      String dateString) {
    val format = getDateFormat(dateString);
    val result = DateUtils.convertDateString(dateString, format);
    Assertions.assertFalse(result.isPresent());
  }

  @ParameterizedTest
  @MethodSource("validDateStringAndFormatProvider")
  public void
      givenValidDateStringAndFormat_whenConvertDateStringWithFormat_thenReturnsOptionalWithDate(
          String dateString, String format) {
    val result = DateUtils.convertDateString(dateString, format);
    Assertions.assertTrue(result.isPresent());
  }

  @Test
  public void
      givenNullOrEmptyFormat_whenConvertDateStringWithFormat_thenThrowsIllegalArgumentException() {
    assertIllegalArgumentException("2022-12-31", null);
    assertIllegalArgumentException("2022-12-31", "");
  }

  private void assertIllegalArgumentException(final String date, final String format) {
    Assertions.assertThrows(
        IllegalArgumentException.class,
        new Executable() {
          @Override
          public void execute() throws Throwable {
            DateUtils.convertDateString(date, format);
          }
        });
  }

  static Stream<Arguments> validDateStringAndFormatProvider() {
    return Stream.of(
        Arguments.of("2022-12-31", DEFAULT_DATE_PATTERN),
        Arguments.of("2023-01-01 10:10:10", DEFAULT_TS_PATTERN));
  }

  private static String getDateFormat(final String dateString) {
    return dateString.length() == TIMESTAMP_LENGTH ? DEFAULT_TS_PATTERN : DEFAULT_DATE_PATTERN;
  }
}
