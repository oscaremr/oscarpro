package ca.kai.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import org.junit.Test;
import org.oscarehr.common.model.DemographicExtKey;

public class MapUtilsTest {

  private static final String DEFAULT = "DEFAULT";
  private final HashMap<Object, Object> map = new HashMap<Object, Object>() {{
    put(DemographicExtKey.NURSE, DemographicExtKey.NURSE.getKey());
    put(DemographicExtKey.NURSE.getKey(), DemographicExtKey.NURSE.getKey());
    put("display_former_name_true", true);
    put("display_former_name_false", false);
    put("null_value", null);
  }};

  @Test
  public void testGetOrDefault_demographicExtKey_returnsValue() {
    assertEquals(
        DemographicExtKey.NURSE.getKey(),
        MapUtils.getOrDefault(map, DemographicExtKey.NURSE, DEFAULT)
    );
  }

  @Test
  public void testGetOrDefault_demographicExtKeyGetKey_returnsValue() {
    assertEquals(
        DEFAULT,
        MapUtils.getOrDefault(map, DemographicExtKey.MIDWIFE.getKey(), DEFAULT)
    );
  }

  @Test
  public void testGetOrDefault_demographicExtKeyNull_returnsDefault() {
    assertEquals(
        DemographicExtKey.NURSE.getKey(),
        MapUtils.getOrDefault(map, DemographicExtKey.NURSE, DEFAULT)
    );
  }

  @Test
  public void testGetOrDefault_stringBooleanTrue_returnsValue() {
    assertEquals(
        true,
        MapUtils.getOrDefault(map, "display_former_name_true", false)
    );
  }

  @Test
  public void testGetOrDefault_stringBooleanFalse_returnsValue() {
    assertEquals(
        false,
        MapUtils.getOrDefault(map, "display_former_name_false", true)
    );
  }

  @Test
  public void testGetOrDefault_stringBooleanNull_returnsDefault() {
    assertEquals(
        false,
        MapUtils.getOrDefault(map, "display_former_name_null", false)
    );
  }
  
  @Test
  public void testGetOrDefault_nullValue_returnsNull() {
    assertNull(MapUtils.getOrDefault(map, "null_value", "bad_value"));
  }
}
