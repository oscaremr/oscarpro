/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.kai.datasharing;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.oscarehr.common.dao.DataSharingSettingsDao;
import org.oscarehr.common.model.DataSharingSettings;
import oscar.OscarProperties;

public class DataSharingServiceTest {

  private static final String ENABLE_KAI_EMR_KEY = "enable_kai_emr";
  private static final boolean ENABLE_KAI_EMR_VALUE = true;
  private static final String ENABLE_TIA_HEALTH_KEY = "enable_tia_health_patient_portal";
  private static final boolean ENABLE_TIA_HEALTH_VALUE = true;
  private static final String KAI_EMR_URL_KEY = "clinic.url";
  private static final String KAI_EMR_URL_VALUE = "https://dev.well.company/";
  private static final String KAI_DEPLOYED_CONTEXT_KEY = "kaiemr_deployed_context";
  private static final String KAI_DEPLOYED_CONTEXT_VALUE = "kaiemr";
  private static final String EMPTY_STRING = "";
  private static final String DISABLED_STRING = "disabled";

  /* TEST DATA */
  @Mock private static DataSharingSettingsDao dataSharingSettingsDao;
  @Mock private static OscarProperties oscarProperties;

  private DataSharingSettings dataSharingSettings;
  private DataSharingService dataSharingService;

  @Before
  public void initialize() {
    dataSharingSettingsDao = Mockito.mock(DataSharingSettingsDao.class);
    oscarProperties = Mockito.mock(OscarProperties.class);

    Mockito.when(oscarProperties.getProperty(
            ArgumentMatchers.eq(KAI_EMR_URL_KEY),
            ArgumentMatchers.anyString()))
        .thenReturn(KAI_EMR_URL_VALUE);
    Mockito.when(oscarProperties.getBooleanProperty(
                ArgumentMatchers.eq(ENABLE_KAI_EMR_KEY),
                ArgumentMatchers.anyString()))
        .thenReturn(ENABLE_KAI_EMR_VALUE);
    Mockito.when(oscarProperties.getBooleanProperty(
                ArgumentMatchers.eq(ENABLE_TIA_HEALTH_KEY),
                ArgumentMatchers.anyString()))
        .thenReturn(ENABLE_TIA_HEALTH_VALUE);
    Mockito.when(oscarProperties.getProperty(
                ArgumentMatchers.eq(KAI_DEPLOYED_CONTEXT_KEY),
                ArgumentMatchers.anyString()))
        .thenReturn(KAI_DEPLOYED_CONTEXT_VALUE);
  }

  @Test
  public void testGetInstance_success_enabled() {
    dataSharingSettings = new DataSharingSettings();
    Mockito.when(dataSharingSettingsDao.getOrganizationSettings())
        .thenReturn(dataSharingSettings);
    dataSharingService = new DataSharingService(dataSharingSettingsDao, oscarProperties);

    Assert.assertEquals(
        String.format("%s%s", KAI_EMR_URL_VALUE, KAI_DEPLOYED_CONTEXT_VALUE),
        DataSharingService.getKaiEmrUrl()
    );
    Assert.assertTrue(dataSharingService.doDataSharingSettingsExist());
    Assert.assertEquals(EMPTY_STRING, dataSharingService.getButtonDisabledStatus());
    Assert.assertTrue(dataSharingService.isPortalEnabled());
  }

  @Test
  public void testGetInstance_success_disabled() {
    Mockito.when(dataSharingSettingsDao.getOrganizationSettings())
        .thenReturn(null);
    dataSharingService = new DataSharingService(dataSharingSettingsDao, oscarProperties);

    Assert.assertEquals(
        String.format("%s%s", KAI_EMR_URL_VALUE, KAI_DEPLOYED_CONTEXT_VALUE),
        DataSharingService.getKaiEmrUrl()
    );
    Assert.assertFalse(dataSharingService.doDataSharingSettingsExist());
    Assert.assertEquals(DISABLED_STRING, dataSharingService.getButtonDisabledStatus());
    Assert.assertTrue(dataSharingService.isPortalEnabled());
  }
}
