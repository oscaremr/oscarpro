/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package ca.kai.datasharing;

import ca.kai.datasharing.PatientPortalData.SyncStatus;
import java.text.SimpleDateFormat;
import lombok.val;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.oscarehr.casemgmt.model.CaseManagementNote;
import org.oscarehr.common.model.Prevention;
import org.oscarehr.util.SpringUtils;
import oscar.oscarResearch.oscarDxResearch.bean.dxResearchBean;

public class PatientPortalDataTest {

  /* TEST DATA */
  private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
  private final String HTML_SYNCED =
      "<img class=\"syncStatusImg\" style=\"width: 13px; margin-left: 20px;\" "
          + "src=\"/images/check-status.png\" alt=\"\">";
  private final String HTML_PENDING =
      "<img class=\"syncStatusImg\" style=\"width: 13px; margin-left: 20px;\" "
          + "src=\"/images/pending.png\" alt=\"\">";
  private final String HTML_REMOVED =
      "<img class=\"syncStatusImg\" style=\"width: 13px; margin-left: 20px;\" "
          + "src=\"/images/x.png\" alt=\"\">";

  @Before
  public void initialize() {
  }

  @Test
  public void testGetPortalStatus_Prevention_Synced() throws Exception {
    val prevention = new Prevention();
    prevention.setAvailable(true);
    prevention.setLastSyncedDate(format.parse("2021-01-01"));
    val data = new PatientPortalData(prevention);

    Assert.assertTrue(data.isAvailable());
    Assert.assertTrue(data.wasPreviouslySynced());
    Assert.assertFalse(data.hasAutoSyncDateBeforeNow());
    Assert.assertEquals(SyncStatus.SYNCED, data.getPortalStatus());
    Assert.assertEquals(HTML_SYNCED, data.getPortalStatus().getIconHtml(""));
  }

  @Test
  public void testGetPortalStatus_Note_Pending() throws Exception {
    // mock SpringUtils because CaseManagementNote requires a call to .getBean
    try (MockedStatic<SpringUtils> utils = Mockito.mockStatic(SpringUtils.class,
        new Answer() {
          @Override
          public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
            if (invocationOnMock.getMethod().getName().equalsIgnoreCase("getbean")) {
              return null;
            }
            return invocationOnMock.callRealMethod();
          }
        })) {
      val note = new CaseManagementNote();
      note.setAvailable(true);
      val data = new PatientPortalData(note);

      Assert.assertTrue(data.isAvailable());
      Assert.assertFalse(data.wasPreviouslySynced());
      Assert.assertFalse(data.hasAutoSyncDateBeforeNow());
      Assert.assertEquals(SyncStatus.PENDING, data.getPortalStatus());
      Assert.assertEquals(HTML_PENDING, data.getPortalStatus().getIconHtml(""));
    }
  }

  @Test
  public void testGetPortalStatus_DxResearch_Removed() throws Exception {
    // mock SpringUtils because dxResearchBean requires a call to .getBean
    try (MockedStatic<SpringUtils> utils = Mockito.mockStatic(SpringUtils.class,
        new Answer() {
          @Override
          public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
            if (invocationOnMock.getMethod().getName().equalsIgnoreCase("getbean")) {
              return null;
            }
            return invocationOnMock.callRealMethod();
          }
        })) {
      val condition = new dxResearchBean();
      condition.setLastSyncedDate(format.parse("2021-01-01"));
      val data = new PatientPortalData(condition);
      Assert.assertFalse(data.isAvailable());
      Assert.assertTrue(data.wasPreviouslySynced());
      Assert.assertFalse(data.hasAutoSyncDateBeforeNow());
      Assert.assertEquals(SyncStatus.REMOVED, data.getPortalStatus());
      Assert.assertEquals(HTML_REMOVED, data.getPortalStatus().getIconHtml(""));
    }
  }

  @Test
  public void testGetPortalStatus_Unavailable() {
    val data = new PatientPortalData();
    Assert.assertFalse(data.isAvailable());
    Assert.assertFalse(data.wasPreviouslySynced());
    Assert.assertFalse(data.hasAutoSyncDateBeforeNow());
    Assert.assertEquals(SyncStatus.UNAVAILABLE, data.getPortalStatus());
    Assert.assertEquals("", data.getPortalStatus().getIconHtml(""));
  }
}
