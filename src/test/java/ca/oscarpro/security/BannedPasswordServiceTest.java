/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package ca.oscarpro.security;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import lombok.val;
import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.BannedPasswordDao;
import org.oscarehr.common.model.BannedPassword;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
public class BannedPasswordServiceTest {

  private final Set<BannedPassword> BANNED_PASSWORDS = new HashSet<>();
  private final Set<String> BANNED_PASSWORD_STRINGS = new HashSet<>(
      Arrays.asList("BannedPassword1!", "BannedPassword2!", "BannedPassword3!"));
  private final String SUCCESS_PASSWORD = "SuccessPassword123!";
  private final String FAIL_PASSWORD = "BannedPassword2!";

  @Mock
  BannedPasswordDao bannedPasswordDao;

  private BannedPasswordService bannedPasswordService;

  @BeforeEach
  public void before() {
    createBannedPasswords();
    bannedPasswordService = new BannedPasswordService();
    ReflectionTestUtils.setField(bannedPasswordService, "bannedPasswords", BANNED_PASSWORD_STRINGS);
    ReflectionTestUtils.setField(bannedPasswordService, "bannedPasswordDao", bannedPasswordDao);
  }

  @Test
  public void initializeBannedPasswords() {
    when(bannedPasswordDao.getAllBannedPasswords()).thenReturn(BANNED_PASSWORDS);
    ReflectionTestUtils.setField(bannedPasswordService, "bannedPasswords", new HashSet<>());
    BannedPasswordService.initializeBannedPasswords();
    val bannedPasswords = getBannedPasswordsFromService();
    Assertions.assertTrue(bannedPasswords.containsAll(BANNED_PASSWORD_STRINGS));
  }

  @Test
  public void convertBannedPasswordsToStrings() {
    val bannedPasswordStringsSet =
        BannedPasswordService.convertBannedPasswordsToStrings(BANNED_PASSWORDS);
    assertBannedPasswordStringsSet(BANNED_PASSWORD_STRINGS, bannedPasswordStringsSet);
  }

  @Test
  public void isBannedPassword_success() {
    Assertions.assertFalse(BannedPasswordService.isPasswordBanned(SUCCESS_PASSWORD));
  }

  @Test
  public void isBannedPassword_failOnBannedPassword() {
    Assertions.assertTrue(BannedPasswordService.isPasswordBanned(FAIL_PASSWORD));
  }

  @Test
  public void isBannedPassword_initializeBannedPasswordsIfEmptySet() {
    when(bannedPasswordDao.getAllBannedPasswords()).thenReturn(BANNED_PASSWORDS);
    ReflectionTestUtils.setField(bannedPasswordService, "bannedPasswords", new HashSet<>());
    Assertions.assertFalse(BannedPasswordService.isPasswordBanned(SUCCESS_PASSWORD));
    verify(bannedPasswordDao, times(1)).getAllBannedPasswords();
    Assertions.assertFalse(BannedPasswordService.isPasswordBanned(SUCCESS_PASSWORD));
    verifyNoMoreInteractions(bannedPasswordDao);
  }

  private Set<String> getBannedPasswordsFromService() {
    return (Set<String>) ReflectionTestUtils.getField(bannedPasswordService, "bannedPasswords");
  }

  private void assertBannedPasswordStringsSet(
      final Set<String> expectedSet,
      final Set<String> actualSet
  ) {
    Assertions.assertEquals(expectedSet.size(), actualSet.size());
    for (val bannedPassword : expectedSet) {
      Assertions.assertTrue(actualSet.contains(bannedPassword));
    }
  }

  private void createBannedPasswords() {
    var i = 1;
    for (val bannedPasswordString : BANNED_PASSWORD_STRINGS) {
      val bannedPassword = new BannedPassword();
      bannedPassword.setId(i);
      bannedPassword.setPassword(bannedPasswordString);
      BANNED_PASSWORDS.add(bannedPassword);
    }
  }

}
