/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.lenient;

import ca.oscarpro.service.OscarProConnectorService.OscarProConnectorServiceException;
import javax.servlet.http.Cookie;
import lombok.val;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;
import oscar.OscarProperties;
import utils.OscarPropertiesUtil;

@ExtendWith(MockitoExtension.class)
@ActiveProfiles(value = "test")
public class OscarProConnectorServiceTest {
  @InjectMocks
  private OscarProConnectorService oscarProConnectorService;
  @Mock
  private RestTemplate restTemplate;

  @BeforeAll
  public static void beforeAll() {
    OscarPropertiesUtil.getOscarProperties();
  }

  @BeforeEach
  public void beforeEach() {
    val responseEntity = new ResponseEntity<>("CSRF_TOKEN", HttpStatus.ACCEPTED);
    lenient().when(restTemplate.exchange(
            ArgumentMatchers.anyString(),
            ArgumentMatchers.any(HttpMethod.class),
            ArgumentMatchers.any(HttpEntity.class),
            ArgumentMatchers.<Class<String>>any()))
        .thenReturn(responseEntity);
  }

  @Test
  public void givenNoData_whenConfigureHeaders_thenThrowAuthenticationException() {
    val headers = new HttpHeaders();
    val cookies = new Cookie[] {};

    assertThrows(OscarProConnectorServiceException.class, new Executable() {
      @Override
      public void execute() throws Throwable {
        oscarProConnectorService.configureHeaders(headers, cookies);
      }
    });
  }

  @Test
  public void givenData_whenConfigureHeaders_thenReturnHeadersWithCookiesAndCsrfToken()
      throws OscarProConnectorServiceException {
    val headers = new HttpHeaders();
    val cookies = new Cookie[] {
        new Cookie("kai-sso", "kai-sso"),
        new Cookie("JSESSIONID", "JSESSIONID")
    };

    // before there should be no cookies
    assertNull(headers.get("Cookie"));
    oscarProConnectorService.configureHeaders(headers, cookies);

    // 3 cookies: kai-sso, JSESSIONID, XSRF-TOKEN
    assertEquals(3, headers.get("Cookie").size());
    // in the method we call, the authentication gets configured first (added to the header)
    // kai-sso is added first because of the order in which we specified the cookies
    assertEquals("kai-sso=kai-sso; Expires=-1; Max-Age=-1; Path=/; Domain=null", headers.get("Cookie").get(0));
    assertEquals("JSESSIONID=JSESSIONID; Expires=-1; Max-Age=-1; Path=/; Domain=null", headers.get("Cookie").get(1));
    // then the csrf token gets configured (added to the header)
    assertEquals("XSRF-TOKEN=CSRF_TOKEN; Expires=-1; Max-Age=-1; Path=/; Domain=" + OscarProperties.getClinicUrl(), headers.get("Cookie").get(2));
  }
}