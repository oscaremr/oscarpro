/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.fax;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.oscarpro.service.OscarProConnectorService;
import ca.oscarpro.service.OscarProConnectorService.OscarProConnectorServiceException;
import java.io.File;
import java.io.IOException;
import javax.servlet.http.Cookie;
import lombok.val;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;
import utils.OscarPropertiesUtil;

@ExtendWith(value = MockitoExtension.class)
@ActiveProfiles(value = "test")
public class OutboundFaxApiConnectorTest {

  private static final String DESTINATION = "test destination";
  private static final String PROVIDER_NUMBER = "test provider number";
  private static final Integer DEMOGRAPHIC_NUMBER = 123;
  private static final FaxFileType FAX_FILE_TYPE = FaxFileType.DOCUMENT;
  private static final String FILE_PATH = "test filepath";
  private static final Cookie[] COOKIES = new Cookie[0];

  @Mock
  private RestTemplate restTemplate;
  @Mock
  private OscarProConnectorService oscarProConnectorService;
  @InjectMocks
  private OutboundFaxApiConnector outboundFaxApiConnector;

  @BeforeAll
  public static void beforeAll() {
    OscarPropertiesUtil.getOscarProperties();
  }

  @Test
  public void givenProviderNumberAndDemographicNumberAndFile_whenSendFax_thenSendFax()
      throws OscarProConnectorServiceException, IOException {
    // Create the Data to Send
    val sendFaxData = SendFaxData.builder()
        .destinationFaxNumber(DESTINATION)
        .providerNumber(PROVIDER_NUMBER)
        .demographicNumber(DEMOGRAPHIC_NUMBER)
        .faxFileType(FAX_FILE_TYPE)
        .cookies(COOKIES)
        .build();

    val file = mock(File.class);
    when(file.getPath()).thenReturn(FILE_PATH);

    // Send the Fax
    outboundFaxApiConnector.sendFax(sendFaxData, file);

    // verify oscarProConnectorService was called once with correct parameters
    val headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    verify(oscarProConnectorService, times(1))
        .configureHeaders(eq(headers), eq(COOKIES));

    // verify restTemplate was called once with correct parameters and capture the HttpEntity
    ArgumentCaptor<HttpEntity<?>> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
    verify((restTemplate), times(1)).exchange(
        anyString(),
        eq(HttpMethod.POST),
        httpEntityCaptor.capture(),
        ArgumentMatchers.<Class<Object>>isNull());

    // retrieve and parse the JSON body from the HttpEntity
    val capturedHttpEntity = (HttpEntity<String>) httpEntityCaptor.getValue();
    val objectMapper = new ObjectMapper();
    val rootNode = objectMapper.readTree(capturedHttpEntity.getBody());

    // verify each parameter
    assertEquals(rootNode.get("destination").asText(), DESTINATION);
    assertEquals(rootNode.get("providerId").asText(), PROVIDER_NUMBER);
    assertEquals(rootNode.get("demographicId").asInt(), DEMOGRAPHIC_NUMBER.intValue());
    assertEquals(rootNode.get("fileType").asText(), FAX_FILE_TYPE.toString());
    assertEquals(rootNode.get("filename").asText(), FILE_PATH);
  }

  @Test
  public void givenNoActiveFaxAccountAndFunctionIsCalledTwice_whenIsFaxEnabled_thenSendApiCallTwice() throws OscarProConnectorServiceException {
    // mock response entity to return false
    final ResponseEntity<Boolean> responseEntity = mock(ResponseEntity.class);
    when(responseEntity.getBody()).thenReturn(false);
    when(restTemplate.exchange(anyString(),
        eq(HttpMethod.GET),
        any(HttpEntity.class),
        eq(Boolean.class))).thenReturn(responseEntity);

    // call isFaxEnabled twice and verify the result is false for both calls
    assertFalse(outboundFaxApiConnector.isFaxEnabled(COOKIES));

    // this call should make a second api call
    assertFalse(outboundFaxApiConnector.isFaxEnabled(COOKIES));

    // verify oscarProConnectorService was called twice with correct parameters
    verify(oscarProConnectorService, times(2))
        .configureHeaders(any(HttpHeaders.class), eq(COOKIES));

    // verify restTemplate was called twice with correct parameters
    final ArgumentCaptor<HttpEntity<?>> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
    verify((restTemplate), times(2)).exchange(
        anyString(),
        eq(HttpMethod.GET),
        httpEntityCaptor.capture(),
        eq(Boolean.class));
  }

  @Test
  public void givenActiveFaxAccountAndFunctionIsCalledTwice_whenIsFaxEnabled_thenSendApiCallOnce()
      throws OscarProConnectorServiceException {
    // mock response entity to return false
    final ResponseEntity<Boolean> responseEntity = mock(ResponseEntity.class);
    when(responseEntity.getBody()).thenReturn(true);
    when(restTemplate.exchange(anyString(),
        eq(HttpMethod.GET),
        any(HttpEntity.class),
        eq(Boolean.class))).thenReturn(responseEntity);

    // call isFaxEnabled twice and verify the result is true for both calls
    assertTrue(outboundFaxApiConnector.isFaxEnabled(COOKIES));

    // this call should return true without making an api call
    assertTrue(outboundFaxApiConnector.isFaxEnabled(COOKIES));

    // verify oscarProConnectorService was called once with correct parameters
    verify(oscarProConnectorService, times(1))
        .configureHeaders(any(HttpHeaders.class), eq(COOKIES));

    // verify restTemplate was called once with correct parameters
    final ArgumentCaptor<HttpEntity<?>> httpEntityCaptor = ArgumentCaptor.forClass(HttpEntity.class);
    verify((restTemplate), times(1)).exchange(
        anyString(),
        eq(HttpMethod.GET),
        httpEntityCaptor.capture(),
        eq(Boolean.class));
  }
}
