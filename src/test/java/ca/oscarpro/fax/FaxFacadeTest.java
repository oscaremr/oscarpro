/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.fax;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.oscarpro.service.OscarProConnectorService.OscarProConnectorServiceException;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.Cookie;
import lombok.val;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.FaxConfigDao;
import org.oscarehr.common.dao.FaxJobDao;
import org.oscarehr.common.model.FaxConfig;
import org.oscarehr.common.model.FaxJob;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.PathUtils;
import org.springframework.test.context.ActiveProfiles;
import oscar.OscarProperties;
import oscar.log.LogService;
import utils.OscarPropertiesUtil;

@ExtendWith(value = MockitoExtension.class)
@ActiveProfiles(value = "test")
public class FaxFacadeTest {

  private final String DESTINATION_FAX_NUMBER = "1112223333";
  private final String INVALID_DESTINATION_FAX_NUMBER = "987987987987987";
  private final String PROVIDER_NUMBER = "123456";
  private final String LOGGED_IN_PROVIDER_NUMBER = "654321";
  private final Integer DEMOGRAPHIC_NUMBER = 1;
  private final FaxFileType FAX_FILE_TYPE = FaxFileType.DOCUMENT;
  private final String FILE_NAME = "test.pdf";
  private final int NUMBER_OF_PAGES = 1;
  private final Cookie[] COOKIES = new Cookie[0];
  private final byte[] PDF_DOCUMENT_BYTES = "test pdf".getBytes();
  private final String FAX_LINE = "222222222222";
  private final String NON_MATCHING_FAX_LINE = "999999999999";
  private final String FAX_CONFIG_FAX_LINE = "111111111111";
  private final String FAX_CONFIG_USER_1 = "111111";
  private final String FAX_CONFIG_FAX_LINE_2 = "222222222222";
  private final String FAX_CONFIG_USER_2 = "222222";
  private final String FAX_CONFIG_FAX_LINE_3 = "333333333333";
  private final String FAX_CONFIG_USER_3 = "333333";

  private final String DOCUMENT_DIRECTORY =
      OscarProperties.getInstance().getProperty("BASE_DOCUMENT_DIR");

  @Mock private OutboundFaxApiConnector outboundFaxApiConnector;
  @Mock private FaxJobDao faxJobDao;
  @Mock private FaxConfigDao faxConfigDao;
  @Mock private FaxFileWriter faxFileWriter;
  @Mock private FaxFileReader faxFileReader;
  @Mock private LoggedInInfo loggedInInfo;
  @Mock private LogService logService;

  @InjectMocks
  @Spy
  private FaxFacade faxFacade;

  @BeforeAll
  public static void beforeAll() {
    OscarPropertiesUtil.getOscarProperties();
    OscarProperties.getInstance().setProperty("BASE_DOCUMENT_DIR", "/oscar-documents/");
  }

  @BeforeEach
  public void beforeEach() throws FaxException, OscarProConnectorServiceException, IOException {
    lenient().doNothing().when(faxJobDao).persist(any(FaxJob.class));

    // Ensure files are not written to disk during tests
    lenient().doNothing().when(faxFileWriter).writeFaxPdfFile(any(byte[].class), any(File.class));
    lenient().doNothing().when(faxFileWriter).writeDestinationFaxNumberTxtFile(
        anyString(), any(File.class));

    // Ensure whatever is read from disk is returned
    lenient().when(faxFileReader.getNumberOfPagesFromPdf(any(File.class))).thenReturn(
        NUMBER_OF_PAGES);

    // Ensure we ignore logging errors
    lenient().doNothing().when(logService).log(any(LoggedInInfo.class), anyString(), anyString(),
        anyString(), anyString(), anyString());

    // Ensure we don't actually send the fax
    lenient().doNothing().when(outboundFaxApiConnector)
        .sendFax(any(SendFaxData.class), any(File.class));
    lenient().when(loggedInInfo.getLoggedInProviderNo()).thenReturn(LOGGED_IN_PROVIDER_NUMBER);
  }

  @Test
  public void givenRingCentralEnabled_whenSendFax_thenWriteFaxFileAndSendFaxWithRingCentral()
      throws FaxException, OscarProConnectorServiceException, IOException {
    // Ensure dirs are not created during tests
    doNothing().when(faxFacade).createFaxDirectory(any(File.class));
    // Files will not be created, so skip validation
    doNothing().when(faxFacade).validateFileExists(any(File.class));
    when(outboundFaxApiConnector.isFaxEnabled(any(Cookie[].class))).thenReturn(true);
    val sendFaxData = SendFaxData.builder()
        .destinationFaxNumber(DESTINATION_FAX_NUMBER)
        .providerNumber(PROVIDER_NUMBER)
        .demographicNumber(DEMOGRAPHIC_NUMBER)
        .faxFileType(FAX_FILE_TYPE)
        .fileName(FILE_NAME)
        .cookies(COOKIES)
        .faxLine(FAX_LINE)
        .pdfDocumentBytes(PDF_DOCUMENT_BYTES)
        .loggedInInfo(loggedInInfo)
        .build();
    faxFacade.sendFax(sendFaxData);
    // verify fax is written to disk
    verify(faxFileWriter, times(1)).writeFaxPdfFile(PDF_DOCUMENT_BYTES,
        new File(getExpectedFaxDirectoryPath() + FILE_NAME));
    // verify fax is sent through modern fax
    verify(outboundFaxApiConnector, times(1))
        .sendFax(eq(sendFaxData), any(File.class));
    // verify fax is not sent through old fax service
    verify(faxFacade, times(0))
        .sendFaxOld(sendFaxData);
  }

  @Test
  public void givenRingCentralDisabled_whenSendFax_thenSendUsingOldFaxServiceWithCorrectParameters()
      throws FaxException, OscarProConnectorServiceException, IOException {
    when(outboundFaxApiConnector.isFaxEnabled(any(Cookie[].class))).thenReturn(false);
    when(loggedInInfo.getLoggedInProviderNo()).thenReturn(LOGGED_IN_PROVIDER_NUMBER);
    // Files will not be created, so skip validation
    doNothing().when(faxFacade).validateFileExists(any(File.class));
    val sendFaxData = SendFaxData.builder()
        .destinationFaxNumber(DESTINATION_FAX_NUMBER)
        .providerNumber(PROVIDER_NUMBER)
        .demographicNumber(DEMOGRAPHIC_NUMBER)
        .faxFileType(FAX_FILE_TYPE)
        .fileName(FILE_NAME)
        .cookies(COOKIES)
        .faxLine(FAX_LINE)
        .pdfDocumentBytes(PDF_DOCUMENT_BYTES)
        .loggedInInfo(loggedInInfo)
        .build();
    faxFacade.sendFax(sendFaxData);
    // verify sendFaxOld is called with correct parameters, notably the logged in provider number
    verify(faxFacade, times(1))
        .sendFaxOld(sendFaxData);
    // verify fax is not sent through modern fax
    verify(outboundFaxApiConnector, times(0))
        .sendFax(eq(sendFaxData), any(File.class));
    verifyFileWriter();
  }

  @Test
  public void givenRingCentralDisabledAndExistingFilePath_whenSendFax_thenSendUsingOldFaxServiceDontSaveNewDocument()
      throws FaxException, OscarProConnectorServiceException, IOException {
    when(outboundFaxApiConnector.isFaxEnabled(any(Cookie[].class))).thenReturn(false);
    when(loggedInInfo.getLoggedInProviderNo()).thenReturn(LOGGED_IN_PROVIDER_NUMBER);
    doNothing().when(faxFacade).validateFileExists(any(File.class));
    val sendFaxData = SendFaxData.builder()
        .destinationFaxNumber(DESTINATION_FAX_NUMBER)
        .providerNumber(PROVIDER_NUMBER)
        .demographicNumber(DEMOGRAPHIC_NUMBER)
        .faxFileType(FAX_FILE_TYPE)
        .fileName(FILE_NAME)
        .cookies(COOKIES)
        .faxLine(FAX_LINE)
        .pdfDocumentBytes(PDF_DOCUMENT_BYTES)
        .loggedInInfo(loggedInInfo)
        .existingFilePath("textExistingFilePath.pdf")
        .build();
    faxFacade.sendFax(sendFaxData);
    // verify sendFaxOld is called with correct parameters, notably the logged in provider number
    verify(faxFacade, times(1))
        .sendFaxOld(sendFaxData);
    // verify fax is not sent through modern fax
    verify(outboundFaxApiConnector, times(0))
        .sendFax(eq(sendFaxData), any(File.class));
    verify(faxFileWriter, times(1)).writeFaxPdfFile(eq(PDF_DOCUMENT_BYTES), any(File.class));
  }

  @Test
  public void givenInvalidDestinationFaxNumber_whenSendFaxOld_thenThrowFaxException()
      throws IOException {
    val sendFaxData = SendFaxData.builder()
        .destinationFaxNumber(INVALID_DESTINATION_FAX_NUMBER)
        .demographicNumber(DEMOGRAPHIC_NUMBER)
        .fileName(FILE_NAME)
        .faxLine(FAX_LINE)
        .pdfDocumentBytes(PDF_DOCUMENT_BYTES)
        .loggedInInfo(loggedInInfo)
        .build();
    try {
      faxFacade.sendFaxOld(sendFaxData);
      fail();
    } catch (FaxException ignored) {
      // Expected exception, ensure no files are written to disk and no FaxJob is saved
      verify(faxFileWriter, never()).writeFaxPdfFile(any(byte[].class), any(File.class));
      verify(faxFileWriter, never()).writeDestinationFaxNumberTxtFile(anyString(), any(File.class));
      verify(faxJobDao, never()).persist(any(FaxJob.class));
    }
  }

  @Test
  public void givenMatchingFaxLineAndFaxConfig_whenSendFaxOld_thenSetMatchingFaxUserAndStatus()
      throws FaxException, IOException {
    when(faxConfigDao.findAll(null, null)).thenReturn(getTestFaxConfigList());
    // Files will not be created, so skip validation
    doNothing().when(faxFacade).validateFileExists(any(File.class));
    val sendFaxData = SendFaxData.builder()
        .destinationFaxNumber(DESTINATION_FAX_NUMBER)
        .demographicNumber(DEMOGRAPHIC_NUMBER)
        .fileName(FILE_NAME)
        .faxLine(FAX_LINE)
        .pdfDocumentBytes(PDF_DOCUMENT_BYTES)
        .faxFileType(FAX_FILE_TYPE)
        .loggedInInfo(loggedInInfo)
        .build();
    faxFacade.sendFaxOld(sendFaxData);
    // verify FaxJob is saved to database
    final ArgumentCaptor<FaxJob> faxJobArgumentCaptor = ArgumentCaptor.forClass(FaxJob.class);
    verify(faxJobDao, times(1)).persist(faxJobArgumentCaptor.capture());
    // verify FaxJob has matching user and status is SENT
    assertFaxUserAndStatus(faxJobArgumentCaptor.getValue(), FAX_CONFIG_USER_2, FaxJob.STATUS.SENT);
  }

  @Test
  public void
      givenNoMatchingFaxLineAndFaxConfig_whenSendFaxOld_thenSetFaxUserOfFirstFaxConfigInList()
          throws FaxException, IOException {
    when(faxConfigDao.findAll(null, null)).thenReturn(getTestFaxConfigList());
    // Files will not be created, so skip validation
    doNothing().when(faxFacade).validateFileExists(any(File.class));
    val sendFaxData = SendFaxData.builder()
        .destinationFaxNumber(DESTINATION_FAX_NUMBER)
        .demographicNumber(DEMOGRAPHIC_NUMBER)
        .fileName(FILE_NAME)
        .faxLine(NON_MATCHING_FAX_LINE)
        .pdfDocumentBytes(PDF_DOCUMENT_BYTES)
        .faxFileType(FAX_FILE_TYPE)
        .loggedInInfo(loggedInInfo)
        .build();
    faxFacade.sendFaxOld(sendFaxData);
    // verify FaxJob is saved to database
    final ArgumentCaptor<FaxJob> faxJobArgumentCaptor = ArgumentCaptor.forClass(FaxJob.class);
    verify(faxJobDao, times(1)).persist(faxJobArgumentCaptor.capture());
    // verify FaxJob has fax user of first fax config in list and status is SENT
    assertFaxUserAndStatus(faxJobArgumentCaptor.getValue(), FAX_CONFIG_USER_1, FaxJob.STATUS.SENT);
  }

  @Test
  public void givenEmptyFaxConfigList_whenSendFaxOld_thenFaxUserIsNullAndStatusIsError()
      throws FaxException, IOException {
    // Empty fax config list
    when(faxConfigDao.findAll(null, null)).thenReturn(new ArrayList<FaxConfig>());
    // Files will not be created, so skip validation
    doNothing().when(faxFacade).validateFileExists(any(File.class));
    val sendFaxData = SendFaxData.builder()
        .destinationFaxNumber(DESTINATION_FAX_NUMBER)
        .demographicNumber(DEMOGRAPHIC_NUMBER)
        .fileName(FILE_NAME)
        .faxLine(FAX_LINE)
        .pdfDocumentBytes(PDF_DOCUMENT_BYTES)
        .faxFileType(FAX_FILE_TYPE)
        .loggedInInfo(loggedInInfo)
        .build();
    faxFacade.sendFaxOld(sendFaxData);
    // verify FaxJob is saved to database
    final ArgumentCaptor<FaxJob> faxJobArgumentCaptor = ArgumentCaptor.forClass(FaxJob.class);
    verify(faxJobDao, times(1)).persist(faxJobArgumentCaptor.capture());
    // verify FaxJob has a null user and status is ERROR
    assertFaxUserAndStatus(faxJobArgumentCaptor.getValue(), null, FaxJob.STATUS.ERROR);
  }

  @Test
  public void givenMinimumRequiredParameters_whenSendFaxOld_thenFaxJobContainsCorrectParameters()
      throws FaxException, IOException {
    // Files will not be created, so skip validation
    doNothing().when(faxFacade).validateFileExists(any(File.class));
    val sendFaxData = SendFaxData.builder()
        .destinationFaxNumber(DESTINATION_FAX_NUMBER)
        .demographicNumber(DEMOGRAPHIC_NUMBER)
        .fileName(FILE_NAME)
        .faxLine(FAX_LINE)
        .pdfDocumentBytes(PDF_DOCUMENT_BYTES)
        .faxFileType(FAX_FILE_TYPE)
        .loggedInInfo(loggedInInfo)
        .build();
    faxFacade.sendFaxOld(sendFaxData);
    // verify FaxJob is saved to database
    final ArgumentCaptor<FaxJob> faxJobArgumentCaptor = ArgumentCaptor.forClass(FaxJob.class);
    verify(faxJobDao, times(1)).persist(faxJobArgumentCaptor.capture());
    // verify FaxJob parameters
    assertFaxJob(faxJobArgumentCaptor.getValue());
    verifyFileWriter();
  }

  @Test
  public void givenFileExists_whenValidateFileExists_thenReturnTrue() {
    final File file = mock(File.class);
    when(file.exists()).thenReturn(true);
    try {
      faxFacade.validateFileExists(file);
    } catch (FaxException ignored) {
      // should not throw exception
      fail();
    }
  }

  @Test
  public void givenFileDoesNotExist_whenValidateFileExists_thenReturnFalse() {
    final File file = mock(File.class);
    when(file.exists()).thenReturn(false);
    try {
      faxFacade.validateFileExists(file);
      fail();
    } catch (FaxException ignored) {
      // should throw exception
    }
  }

  @Test
  public void givenDate_whenGetFaxDirectoryPath_thenReturnCorrectPath() throws FaxException {
    // Prevent creating directories during test
    doNothing().when(faxFacade).createFaxDirectory(any(File.class));
    assertEquals(getExpectedFaxDirectoryPath(), faxFacade.getFaxDirectoryPath());
  }

  private String getExpectedFaxDirectoryPath() {
    final Date currentDate = new Date();
    final SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
    final SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
    final SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
    return PathUtils.addTrailingSlash(DOCUMENT_DIRECTORY) + "fax" + File.separator
        + yearFormat.format(currentDate) + File.separator
        + monthFormat.format(currentDate) + File.separator
        + dateFormat.format(currentDate) + File.separator;
  }

  @Test
  public void givenFaxDirectoryExists_whenCreateFaxDirectory_thenDoNothing() throws FaxException {
    // Mock input file, this will stub the mkdirs() method as well
    final File faxDirectory = mock(File.class);
    when(faxDirectory.exists()).thenReturn(true);

    faxFacade.createFaxDirectory(faxDirectory);
    verify(faxDirectory, never()).mkdirs();
  }

  @Test
  public void givenFaxDirectoryDoesNotExist_whenCreateFaxDirectory_thenCreateTheDirectory()
      throws FaxException {
    // Mock input file, this will stub the mkdirs() method as well
    final File faxDirectory = mock(File.class);
    when(faxDirectory.exists()).thenReturn(false);

    try {
      faxFacade.createFaxDirectory(faxDirectory);
      // fail if no exception is thrown
      fail();
    } catch (FaxException ignored) {
      // should throw exception
      verify(faxDirectory, times(1)).mkdirs();
    }
  }

  private void assertFaxUserAndStatus(
      final FaxJob faxJob, final String faxUser, final FaxJob.STATUS faxStatus) {
    assertEquals(faxUser, faxJob.getUser());
    assertEquals(faxStatus, faxJob.getStatus());
  }

  private List<FaxConfig> getTestFaxConfigList() {
    final ArrayList<FaxConfig> faxConfigList = new ArrayList<>();
    addFaxConfig(FAX_CONFIG_FAX_LINE, FAX_CONFIG_USER_1, faxConfigList);
    addFaxConfig(FAX_CONFIG_FAX_LINE_2, FAX_CONFIG_USER_2, faxConfigList);
    addFaxConfig(FAX_CONFIG_FAX_LINE_3, FAX_CONFIG_USER_3, faxConfigList);
    return faxConfigList;
  }

  private void addFaxConfig(
      final String faxLine, final String faxUser, final List<FaxConfig> faxConfigList) {
    final FaxConfig faxConfig = new FaxConfig();
    faxConfig.setFaxNumber(faxLine);
    faxConfig.setFaxUser(faxUser);
    faxConfigList.add(faxConfig);
  }

  private void assertFaxJob(FaxJob faxJob) {
    assertEquals(DESTINATION_FAX_NUMBER, faxJob.getDestination());
    assertEquals(LOGGED_IN_PROVIDER_NUMBER, faxJob.getOscarUser());
    assertEquals(DEMOGRAPHIC_NUMBER, faxJob.getDemographicNo());
    assertEquals(FILE_NAME, faxJob.getFile_name());
    assertEquals(NUMBER_OF_PAGES, faxJob.getNumPages());
    assertEquals(FAX_LINE, faxJob.getFax_line());
  }

  private void verifyFileWriter() {
    verify(faxFileWriter, times(2)).writeFaxPdfFile(eq(PDF_DOCUMENT_BYTES),
        any(File.class));
    verify(faxFileWriter, times(1)).writeDestinationFaxNumberTxtFile(anyString(), any(File.class));
  }
}
