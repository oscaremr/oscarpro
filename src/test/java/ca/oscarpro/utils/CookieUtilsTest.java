/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import javax.servlet.http.Cookie;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles(value = "test")
public class CookieUtilsTest {

  @Test
  void givenCookie_WhenFormatCookie_ThenReturnFormattedCookie() {
    // given
    val cookie = new Cookie("cookieName", "cookieValue");
    cookie.setMaxAge(100);
    cookie.setDomain("cookieDomain");

    // when
    val formattedCookie = CookieUtils.formatCookie(cookie);

    // then (note: cookiedomain is lowercased in the output)
    assertEquals("cookieName=cookieValue; Expires=100; Max-Age=100; Path=/; Domain=cookiedomain", formattedCookie);
  }

  @Test
  void givenEmptyCookie_WhenFormatCookie_ThenReturnFormattedCookie() {
    // given
    val cookie = new Cookie("cookieName", "");
    cookie.setMaxAge(0);
    cookie.setDomain("");

    // when
    val formattedCookie = CookieUtils.formatCookie(cookie);

    // then
    assertEquals("cookieName=; Expires=0; Max-Age=0; Path=/; Domain=", formattedCookie);
  }

  @Test
  void givenCookies_WhenGetCookiesByNames_ThenReturnCookies() {
    // given
    val cookies = new Cookie[] {
        new Cookie("cookieName1", "cookieValue1"),
        new Cookie("cookieName2", "cookieValue2"),
        new Cookie("cookieName3", "cookieValue3"),
    };

    val cookieNames = new HashSet<>(Arrays.asList("cookieName1", "cookieName3"));

    // when
    val foundCookies = CookieUtils.getCookiesByNames(cookies, cookieNames);

    // then
    assertEquals(2, foundCookies.size());

    // cookie 1
    assertEquals("cookieName1", foundCookies.get(0).getName());
    assertEquals("cookieValue1", foundCookies.get(0).getValue());

    // cookie 3
    assertEquals("cookieName3", foundCookies.get(1).getName());
    assertEquals("cookieValue3", foundCookies.get(1).getValue());
  }

  @Test
  void givenNoCookies_WhenGetCookiesByNames_ThenReturnNoCookies() {
    val cookies = new Cookie[0];

    val cookieNames = new HashSet<>(Collections.singletonList("cookieName1"));

    // when
    val foundCookies = CookieUtils.getCookiesByNames(cookies, cookieNames);

    // then
    assertEquals(0, foundCookies.size());
  }

  @Test
  void givenMissingCookiesNames_WhenGetCookiesByNames_ThenReturnNoCookies() {
    // given
    val cookies = new Cookie[] {
        new Cookie("cookieName1", "cookieValue1"),
        new Cookie("cookieName2", "cookieValue2"),
        new Cookie("cookieName3", "cookieValue3"),
    };

    val cookieNames = new HashSet<>(Arrays.asList("cookieName4", "cookieName5"));

    // when
    val foundCookies = CookieUtils.getCookiesByNames(cookies, cookieNames);

    // then
    assertEquals(0, foundCookies.size());
  }

  @Test
  void givenOrderedCookies_WhenGetCookiesByName_ThenReturnOrderedFoundCookies() {
    val cookies = new Cookie[] {
        new Cookie("cookieName1", "cookieValue1"),
        new Cookie("cookieName2", "cookieValue2"),
        new Cookie("cookieName3", "cookieValue3"),
        new Cookie("cookieName4", "cookieValue4"),
        new Cookie("cookieName5", "cookieValue5"),
        new Cookie("cookieName6", "cookieValue6"),
        new Cookie("cookieName7", "cookieValue7"),
        new Cookie("cookieName8", "cookieValue8"),
        new Cookie("cookieName9", "cookieValue9"),
    };

    val cookieNames = new HashSet<>(Arrays.asList("cookieName2", "cookieName4", "cookieName5", "cookieName1"));

    // when
    val foundCookies = CookieUtils.getCookiesByNames(cookies, cookieNames);

    // then (make sure the order is maintained, from the original list)
    assertEquals(4, foundCookies.size());
    assertEquals("cookieName1", foundCookies.get(0).getName());
    assertEquals("cookieName2", foundCookies.get(1).getName());
    assertEquals("cookieName4", foundCookies.get(2).getName());
    assertEquals("cookieName5", foundCookies.get(3).getName());
  }

  @Test
  void givenEmptyCookieNames_WhenGetCookiesByNames_ThenReturnNoCookies() {
    // given
    val cookies = new Cookie[] {
        new Cookie("cookieName1", "cookieValue1"),
        new Cookie("cookieName2", "cookieValue2"),
        new Cookie("cookieName3", "cookieValue3"),
    };

    val cookieNames = new HashSet<String>();

    // when
    val foundCookies = CookieUtils.getCookiesByNames(cookies, cookieNames);

    // then
    assertEquals(0, foundCookies.size());
  }

  @Test
  void givenCookies_WhenAddCookieToHeaders_ThenAddCookieToHeaders() {
    // given
    val headers = new HttpHeaders();
    val cookie = new Cookie("cookieName", "cookieValue");

    cookie.setMaxAge(100);
    cookie.setDomain("cookieDomain");

    // when
    CookieUtils.addCookieToHeaders(headers, cookie);

    // then
    assertEquals("cookieName=cookieValue; Expires=100; Max-Age=100; Path=/; Domain=cookiedomain", headers.getFirst("Cookie"));
  }

  @Test
  void givenMultipleCookies_WhenAddCookieToHeaders_ThenAddCookieToHeaders() {
    // given
    val headers = new HttpHeaders();
    val cookie1 = new Cookie("cookieName1", "cookieValue1");
    val cookie2 = new Cookie("cookieName2", "cookieValue2");

    cookie1.setMaxAge(100);
    cookie1.setDomain("cookieDomain1");

    cookie2.setMaxAge(200);
    cookie2.setDomain("cookieDomain2");

    // when
    CookieUtils.addCookieToHeaders(headers, cookie1);
    CookieUtils.addCookieToHeaders(headers, cookie2);

    // then
    assertEquals("cookieName1=cookieValue1; Expires=100; Max-Age=100; Path=/; Domain=cookiedomain1", headers.getFirst("Cookie"));
    assertEquals("cookieName2=cookieValue2; Expires=200; Max-Age=200; Path=/; Domain=cookiedomain2", headers.get("Cookie").get(1));
  }

  @Test
  void givenCookies_WhenAddCookieToHeaders_ThenAddCookiesToHeaders() {
    // given
    val headers = new HttpHeaders();
    val cookie = new Cookie("cookieName", "cookieValue");

    cookie.setMaxAge(100);
    cookie.setDomain("cookieDomain");

    // when
    CookieUtils.addCookiesToHeaders(headers, Collections.singletonList(cookie));

    // then
    assertEquals("cookieName=cookieValue; Expires=100; Max-Age=100; Path=/; Domain=cookiedomain", headers.getFirst("Cookie"));
  }

  @Test
  void givenMultipleCookies_WhenAddCookieToHeaders_ThenAddCookiesToHeaders() {
    // given
    val headers = new HttpHeaders();
    val cookie1 = new Cookie("cookieName1", "cookieValue1");
    val cookie2 = new Cookie("cookieName2", "cookieValue2");

    cookie1.setMaxAge(100);
    cookie1.setDomain("cookieDomain1");

    cookie2.setMaxAge(200);
    cookie2.setDomain("cookieDomain2");

    // when
    CookieUtils.addCookiesToHeaders(headers, Arrays.asList(cookie1, cookie2));

    // then
    assertEquals(
        "cookieName1=cookieValue1; Expires=100; Max-Age=100; Path=/; Domain=cookiedomain1",
        headers.getFirst("Cookie"));
    assertEquals(
        "cookieName2=cookieValue2; Expires=200; Max-Age=200; Path=/; Domain=cookiedomain2",
        headers.get("Cookie").get(1));
  }
}