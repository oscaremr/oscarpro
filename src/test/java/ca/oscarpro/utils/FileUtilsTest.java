/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package ca.oscarpro.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles(value = "test")
public class FileUtilsTest {

  private final String FILE_NAME = "test-filename";
  private final String EXTENSION = ".txt";
  private final String FILE_NAME_WITH_EXTENSION = "test-filename.txt";

  @Test
  public void givenFileNameHasNoExtension_whenAddFileExtension_thenAppendExtension() {
    final String fileName = FileUtils.addFileExtension(FILE_NAME, EXTENSION);
    assertEquals(fileName, FILE_NAME_WITH_EXTENSION);
  }

  @Test
  public void givenFileNameHasSameExtension_whenAddFileExtension_thenReturnFileName() {
    final String fileName = FileUtils.addFileExtension(FILE_NAME_WITH_EXTENSION, EXTENSION);
    assertEquals(fileName, FILE_NAME_WITH_EXTENSION);
  }

}
