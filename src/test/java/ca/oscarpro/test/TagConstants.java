package ca.oscarpro.test;

public class TagConstants {
  // for tests that specifically require a BC database configuration
  public static final String BC = "BC";
  // for tests that specifically require a ON database configuration
  public static final String ON = "ON";
}
