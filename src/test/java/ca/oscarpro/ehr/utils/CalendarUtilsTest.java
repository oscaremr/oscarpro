package ca.oscarpro.ehr.utils;
import ca.oscarpro.common.util.CalendarUtils;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
public class CalendarUtilsTest {
  private HttpServletRequest mockRequest;
  @Before
  public void setUp() {
    mockRequest = mock(HttpServletRequest.class);
  }
  @Test
  public void givenValidDates_whenGetDateRangeFromRequest_thenReturnValidCalendarRange() {
    when(mockRequest.getParameter("pStartDate")).thenReturn("01-JAN-2023");
    when(mockRequest.getParameter("pEndDate")).thenReturn("02-JAN-2023");
    val range = CalendarUtils.getDateRangeFromDates(mockRequest.getParameter("pStartDate"), mockRequest.getParameter("pEndDate"));
    val formatter = new SimpleDateFormat("dd-MMM-yyyy");
    assertEquals("01-Jan-2023", formatter.format(range[0].getTime()));
    val expectedEndDate = Calendar.getInstance();
    expectedEndDate.setTime(range[1].getTime());
    expectedEndDate.set(Calendar.HOUR_OF_DAY, 0);
    expectedEndDate.set(Calendar.MINUTE, 0);
    expectedEndDate.set(Calendar.SECOND, 0);
    assertEquals("02-Jan-2023", formatter.format(expectedEndDate.getTime()));
  }
  @Test
  public void givenNullDates_whenGetDateRangeFromRequest_thenReturnNullCalendarRange() {
    when(mockRequest.getParameter("pStartDate")).thenReturn(null);
    when(mockRequest.getParameter("pEndDate")).thenReturn(null);
    val range = CalendarUtils.getDateRangeFromDates(mockRequest.getParameter("pStartDate"), mockRequest.getParameter("pEndDate"));
    assertNull(range[0]);
    assertNull(range[1]);
  }
  @Test
  public void givenEmptyDates_whenGetDateRangeFromRequest_thenReturnNullCalendarRange() {
    when(mockRequest.getParameter("pStartDate")).thenReturn("");
    when(mockRequest.getParameter("pEndDate")).thenReturn("");
    val range = CalendarUtils.getDateRangeFromDates(mockRequest.getParameter("pStartDate"), mockRequest.getParameter("pEndDate"));
    assertNull(range[0]);
    assertNull(range[1]);
  }
  @Test
  public void givenInvalidStartDate_whenGetDateRangeFromRequest_thenThrowIllegalArgumentException() {
    when(mockRequest.getParameter("pStartDate")).thenReturn("invalid-date");
    when(mockRequest.getParameter("pEndDate")).thenReturn("02-JAN-2023");
    val range = CalendarUtils.getDateRangeFromDates(mockRequest.getParameter("pStartDate"), mockRequest.getParameter("pEndDate"));
    assertNull(range[0]);
    assertNull(range[1]);
  }
  @Test
  public void givenInvalidEndDate_whenGetDateRangeFromRequest_thenThrowIllegalArgumentException() {
    when(mockRequest.getParameter("pStartDate")).thenReturn("01-JAN-2023");
    when(mockRequest.getParameter("pEndDate")).thenReturn("invalid-date");
    val range = CalendarUtils.getDateRangeFromDates(mockRequest.getParameter("pStartDate"), mockRequest.getParameter("pEndDate"));
    assertNull(range[0]);
    assertNull(range[1]);
  }
}