package ca.oscarpro.ehr;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

import ca.oscarpro.ehr.patient.repository.ClassicPatientChartRepository;
import java.io.OutputStream;
import javax.servlet.http.HttpServletRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.oscarehr.util.LoggedInInfo;
public class OscarProClassicPatientChartProjectionTest {
  @InjectMocks
  private ClassicPatientChartRepository facade;
  @Mock
  private HttpServletRequest request;
  @Mock
  private OutputStream stream;
  @Mock
  private LoggedInInfo loggedInInfo;
  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);
  }
  @Test(expected = RuntimeException.class)
  public void testPrintChartNoDataToPrint() {
    when(request.getParameter(anyString())).thenReturn(
        "false"); // Assuming no flags are set to true
    when(request.getAttribute("casemgmt_DemoNo")).thenReturn(
        "1234"); // Setting a demographic number
    facade.printChart("", request, stream, loggedInInfo);
  }
  @Test(expected = RuntimeException.class)
  public void testPrintChartInvalidDate() {
    when(request.getParameter("pStartDate")).thenReturn("invalid-date");
    when(request.getAttribute("casemgmt_DemoNo")).thenReturn("1234");
    facade.printChart("", request, stream, loggedInInfo);
  }
}