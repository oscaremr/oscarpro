package ca.oscarpro.common.http;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import lombok.val;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@Execution(ExecutionMode.SAME_THREAD)
public class OscarProHttpServiceTest {

  private static final String URL = "http://testurl:8080";
  private static final String BODY = "testbody";
  private static final String KAI_SSO_COOKIE = "kai-sso";
  private static final String KAI_SSO_COOKIE_VALUE = "test";
  private static final String COOKIE_HEADER = KAI_SSO_COOKIE + "=" + KAI_SSO_COOKIE_VALUE;

  private final ArgumentCaptor<HttpGet> httpGetCaptor = ArgumentCaptor.forClass(HttpGet.class);

  private final ArgumentCaptor<HttpPost> httpPostCaptor = ArgumentCaptor.forClass(HttpPost.class);

  @Mock
  private HttpServletRequest request;

  @Spy
  private OscarProHttpService oscarProHttpService;

  @BeforeEach
  public void setUp() {
    when(request.getCookies()).thenReturn(getCookieArrayWithKaiSsoCookie());
    doReturn(null).when(oscarProHttpService).makeLoggedInRequestToPro(any());
  }

  @Test
  public void givenUrlAndBodyAndRequest_whenMakeLoggedInPostRequestToPro_thenReturnsHttpResponse()
      throws IOException {
    oscarProHttpService.makeLoggedInPostRequestToPro(URL, BODY, request);
    verify(oscarProHttpService, times(1)).makeLoggedInRequestToPro(httpPostCaptor.capture());
    val httpPost = httpPostCaptor.getValue();
    assertNotNull(httpPost);
    assertNotNull(httpPost.getEntity());
    assertEquals(URL, httpPost.getURI().toString());
    assertEquals(BODY, EntityUtils.toString(httpPost.getEntity()));
    assertEquals(COOKIE_HEADER, httpPost.getFirstHeader("Cookie").getValue());
  }

  @Test
  public void givenUrlAndRequest_whenMakeLoggedInGetRequestToPro_thenReturnsHttpResponse() {
    oscarProHttpService.makeLoggedInGetRequestToPro(URL, request);
    verify(oscarProHttpService, times(1)).makeLoggedInRequestToPro(httpGetCaptor.capture());
    val httpGet = httpGetCaptor.getValue();
    assertNotNull(httpGet);
    assertEquals(URL, httpGet.getURI().toString());
    assertEquals(COOKIE_HEADER, httpGet.getFirstHeader("Cookie").getValue());
  }

  private Cookie[] getCookieArrayWithKaiSsoCookie() {
    return new Cookie[]{new Cookie(KAI_SSO_COOKIE, KAI_SSO_COOKIE_VALUE)};
  }

}