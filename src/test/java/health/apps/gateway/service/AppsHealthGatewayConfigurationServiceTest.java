package health.apps.gateway.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import lombok.val;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.oscarehr.common.dao.SystemPreferencesDao;
import oscar.OscarProperties;

@ExtendWith(MockitoExtension.class)
public class AppsHealthGatewayConfigurationServiceTest {

  private MockedStatic<OscarProperties> oscarPropertiesMockedStatic;

  @Mock
  private SystemPreferencesDao systemPreferencesDao;

  @InjectMocks
  private AppsHealthGatewayConfigurationService service;

  @BeforeEach
  public void before(){
    oscarPropertiesMockedStatic = mockStatic(OscarProperties.class);
  }

  @AfterEach
  public void close(){
    oscarPropertiesMockedStatic.close();
  }

  @Test
  public void givenPropertyEnabled_whenIsGatewayEnabledInProperties_thenReturnsTrue() {
    val properties = mock(OscarProperties.class);
    when(OscarProperties.getInstance())
        .thenAnswer((Answer<OscarProperties>) invocation -> properties);
    when(properties.getBooleanProperty("apps_health.gateway.enabled", "true"))
        .thenReturn(true);
    assertTrue(service.isGatewayEnabledInProperties());
  }

  @Test
  public void givenPropertyDisabled_whenIsGatewayEnabledInProperties_thenReturnsFalse() {
    val properties = mock(OscarProperties.class);
    when(OscarProperties.getInstance())
        .thenAnswer((Answer<OscarProperties>) invocation -> properties);
    when(properties.getBooleanProperty("apps_health.gateway.enabled", "true"))
        .thenReturn(false);
    assertFalse(service.isGatewayEnabledInProperties());
  }

  @Test
  public void givenSystemPrefEnabled_whenIsGatewayEnabledInSystemPreferences_thenReturnsTrue() {
    when(systemPreferencesDao.isReadBooleanPreference(
        "apps_health.gateway.enabled")).thenReturn(true);
    assertTrue(service.isGatewayEnabledInSystemPreferences());
  }

  @Test
  public void givenSystemPrefDisabled_whenIsGatewayEnabledInSystemPreferences_thenReturnsFalse() {
    when(systemPreferencesDao.isReadBooleanPreference(
        "apps_health.gateway.enabled")).thenReturn(false);
    assertFalse(service.isGatewayEnabledInSystemPreferences());
  }

  @Test
  public void givenBothEnabled_whenIsGatewayEnabled_thenReturnsTrue() {
    val properties = mock(OscarProperties.class);
    when(systemPreferencesDao.isReadBooleanPreference("apps_health.gateway.enabled"))
        .thenReturn(true);
    when(OscarProperties.getInstance())
        .thenAnswer((Answer<OscarProperties>) invocation -> properties);
    when(properties.getBooleanProperty("apps_health.gateway.enabled", "true"))
        .thenReturn(true);
    assertTrue(service.isGatewayEnabled());
  }

  @Test
  public void givenPropertyEnabledSystemPrefDisabled_whenIsGatewayEnabled_thenReturnsFalse() {
    val properties = mock(OscarProperties.class);
    when(properties.getBooleanProperty("apps_health.gateway.enabled", "true"))
        .thenReturn(true);
    when(systemPreferencesDao.isReadBooleanPreference("apps_health.gateway.enabled"))
        .thenReturn(false);
    when(OscarProperties.getInstance())
        .thenAnswer((Answer<OscarProperties>) invocation -> properties);
    assertFalse(service.isGatewayEnabled());
  }

  @Test
  public void givenPropertyDisabled_whenIsGatewayEnabled_thenReturnsFalse() {
    val properties = mock(OscarProperties.class);
    when(properties.getBooleanProperty("apps_health.gateway.enabled", "true"))
        .thenReturn(false);
    when(OscarProperties.getInstance())
        .thenAnswer((Answer<OscarProperties>) invocation -> properties);
    assertFalse(service.isGatewayEnabled());
  }
}