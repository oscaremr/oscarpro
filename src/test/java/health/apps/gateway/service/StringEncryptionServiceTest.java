/**
 * Copyright (c) 2024 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package health.apps.gateway.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import lombok.val;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.springframework.security.crypto.encrypt.Encryptors;
import oscar.OscarProperties;

public class StringEncryptionServiceTest {

  private static MockedStatic<OscarProperties> mockedOscarProperties;

  @BeforeAll
  public static void beforeAll() {
    mockedOscarProperties = mockStatic(OscarProperties.class);
  }

  @AfterAll
  public static void afterAll() {
    mockedOscarProperties.close();
  }

  @Test
  public void givenValidPasswordAndSalt_whenDecrypt_thenSuccessful() {
    // Given
    configureOscarPropertiesMock("12345678", "abcd");
    val service = new StringEncryptionService();
    val encryptedString = Encryptors.delux("12345678", "abcd").encrypt("testString");

    // When
    val decryptedString = service.decrypt(encryptedString);

    // Then
    assertEquals("testString", decryptedString);
  }

  @Test
  public void givenInValidPassword_whenDecrypt_thenThrowException() {
    // Given
    configureOscarPropertiesMock(null, "abcd");
    val service = new StringEncryptionService();

    // When
    val exception = assertThrows(IllegalStateException.class, () -> {
      service.decrypt("someEncryptedString");
    });

    // Then
    assertEquals("AES password or salt is not set", exception.getMessage());
  }

  @Test
  public void givenInValidSalt_whenDecrypt_thenThrowException() {
    // Given
    configureOscarPropertiesMock("12345678", null);
    val service = new StringEncryptionService();

    // When
    val exception = assertThrows(IllegalStateException.class, () -> {
      service.decrypt("someEncryptedString");
    });

    // Then
    assertEquals("AES password or salt is not set", exception.getMessage());
  }

  private static void configureOscarPropertiesMock(String password, String salt) {
    val mockedProperties = mock(OscarProperties.class);
    when(OscarProperties.getInstance()).thenAnswer(invocation -> mockedProperties);
    when(mockedProperties.getProperty("security.encryption.aes.password")).thenReturn(password);
    when(mockedProperties.getProperty("security.encryption.aes.salt")).thenReturn(salt);
  }
}