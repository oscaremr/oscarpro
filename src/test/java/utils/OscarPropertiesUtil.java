/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package utils;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.ServletContext;
import org.oscarehr.common.dao.utils.ConfigUtils;
import oscar.OscarProperties;

public class OscarPropertiesUtil {

  public static OscarProperties getOscarProperties() {
    if (OscarProperties.getInstance() == null) {
      final String testContext = ConfigUtils.getProperty("test_context");
      final ServletContext context = mock(ServletContext.class);
      when(context.getContextPath()).thenReturn(
          (testContext == null) || testContext.trim().isEmpty()
              ? "oscar" : testContext);
      OscarProperties.initialize(context);
    }
    return OscarProperties.getInstance();
  }
}
