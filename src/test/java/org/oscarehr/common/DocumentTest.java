/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common;

import static org.junit.jupiter.api.Assertions.assertEquals;

import lombok.val;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.model.Document;

public class DocumentTest {

  @Test
  public void givenDocument_whenCreateDocument_thenDefaultValuesUsed() {
    val document = new Document();
    assertEquals(Document.STATUS_ACTIVE, document.getStatus());
  }
}
