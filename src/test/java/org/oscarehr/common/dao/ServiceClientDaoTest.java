/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import static com.well.integration.config.generators.InsigGenerator.INSIG_NAMES;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.ServiceClient;
import org.oscarehr.util.SpringUtils;

public class ServiceClientDaoTest extends DaoTestFixtures {

	protected ServiceClientDao serviceClientDao = SpringUtils.getBean(ServiceClientDao.class);

	@Before
	public void before() throws Exception {
		SchemaUtils.restoreTable("ServiceClient");
	}

	@Test
	public void testServiceClientFindByNamesLikeNotFound() {
		assertListIsEmpty(serviceClientDao.findByNamesLike(null));
		assertListIsEmpty(serviceClientDao.findByNamesLike(new ArrayList<String>()));
		assertListIsEmpty(serviceClientDao.findByNamesLike(INSIG_NAMES));
	}

	@Test
	public void testServiceClientFindByNamesLikeInsig() throws Exception {
		testServiceClientFindByNamesLike("insig integration");
		testServiceClientFindByNamesLike("Insig");
	}

	@Test
	public void testServiceClientFindByNamesLikeVirtual() throws Exception {
		testServiceClientFindByNamesLike("virtual care");
		testServiceClientFindByNamesLike("VirtualCare");
	}

	private void testServiceClientFindByNamesLike(String name) throws Exception {
		ServiceClient client = createTestServiceClient(name);
		List<ServiceClient> clients = serviceClientDao.findByNamesLike(INSIG_NAMES);
		assertEquals(1, clients.size());
		assertEquals(client.getId(), clients.get(0).getId());
	}

	private ServiceClient createTestServiceClient(String name) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		SchemaUtils.restoreTable("ServiceClient");
		ServiceClient client = new ServiceClient();
		client.setName(name);
		client.setKey("");
		client.setSecret("");
		client.setLifetime(-1);
		serviceClientDao.persist(client);
		return client;
	}

	private void assertListIsEmpty(List list) {
		assertNotNull(list);
		assertEquals(0, list.size());
	}
}
