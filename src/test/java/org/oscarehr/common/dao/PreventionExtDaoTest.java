/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
/**
 * @author Shazib
 */
package org.oscarehr.common.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.Prevention;
import org.oscarehr.common.model.PreventionExt;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

public class PreventionExtDaoTest extends DaoTestFixtures {

	protected PreventionDao preventionDao = SpringUtils.getBean(PreventionDao.class);
	protected PreventionExtDao preventionExtDao = SpringUtils.getBean(PreventionExtDao.class);

	@BeforeEach
	public void before() throws Exception {
		SchemaUtils.restoreTable("preventions", "preventionsExt");
	}

	@Test
	public void testFindByPreventionId() throws Exception {
		Prevention p1 = new Prevention();
		EntityDataGenerator.generateTestDataForModelClass(p1);
		p1.setId(null);
		preventionDao.persist(p1);

		Prevention p2 = new Prevention();
		EntityDataGenerator.generateTestDataForModelClass(p2);
		p2.setId(null);
		preventionDao.persist(p2);

		int preventionId1 = p1.getId();
		int preventionId2 = p2.getId();

		PreventionExt prevenExt1 = new PreventionExt();
		EntityDataGenerator.generateTestDataForModelClass(prevenExt1);
		prevenExt1.setPreventionId(preventionId1);
		preventionExtDao.persist(prevenExt1);

		PreventionExt prevenExt2 = new PreventionExt();
		EntityDataGenerator.generateTestDataForModelClass(prevenExt2);
		prevenExt2.setPreventionId(preventionId2);
		preventionExtDao.persist(prevenExt2);

		PreventionExt prevenExt3 = new PreventionExt();
		EntityDataGenerator.generateTestDataForModelClass(prevenExt3);
		prevenExt3.setPreventionId(preventionId1);
		preventionExtDao.persist(prevenExt3);

		List<PreventionExt> expectedResult = new ArrayList<PreventionExt>(
        Arrays.asList(prevenExt1, prevenExt3));
		List<PreventionExt> result = preventionExtDao.findByPreventionId(preventionId1);

		Logger logger = MiscUtils.getLogger();

		if (result.size() != expectedResult.size()) {
			logger.warn("Array sizes do not match.");
			Assertions.fail("Array sizes do not match.");
		}
		for (int i = 0; i < expectedResult.size(); i++) {
			if (!expectedResult.get(i).equals(result.get(i))){
				logger.warn("Items  do not match.");
				Assertions.fail("Items  do not match.");
			}
		}
		Assertions.assertTrue(true);
	}

	@Test
	public void testFindByKeyAndValue() throws Exception {

		Prevention p1 = new Prevention();
		EntityDataGenerator.generateTestDataForModelClass(p1);
		p1.setId(null);
		preventionDao.persist(p1);

		String val1 = "100";
		String val2 = "200";

		String keyVal1 = "alpha";
		String keyVal2 = "bravo";

		PreventionExt prevenExt1 = new PreventionExt();
		EntityDataGenerator.generateTestDataForModelClass(prevenExt1);
		prevenExt1.setVal(val1);
		prevenExt1.setKeyval(keyVal1);
		prevenExt1.setPreventionId(p1.getId());
		preventionExtDao.persist(prevenExt1);

		PreventionExt prevenExt2 = new PreventionExt();
		EntityDataGenerator.generateTestDataForModelClass(prevenExt2);
		prevenExt2.setVal(val2);
		prevenExt2.setKeyval(keyVal2);
		prevenExt2.setPreventionId(p1.getId());
		preventionExtDao.persist(prevenExt2);

		PreventionExt prevenExt3 = new PreventionExt();
		EntityDataGenerator.generateTestDataForModelClass(prevenExt3);
		prevenExt3.setVal(val1);
		prevenExt3.setKeyval(keyVal1);
		prevenExt3.setPreventionId(p1.getId());
		preventionExtDao.persist(prevenExt3);

		List<PreventionExt> expectedResult = new ArrayList<PreventionExt>(Arrays.asList(prevenExt1, prevenExt3));
		List<PreventionExt> result = preventionExtDao.findByKeyAndValue(keyVal1, val1);

		Logger logger = MiscUtils.getLogger();

		if (result.size() != expectedResult.size()) {
			logger.warn("Array sizes do not match.");
			Assertions.fail("Array sizes do not match.");
		}
		for (int i = 0; i < expectedResult.size(); i++) {
			if (!expectedResult.get(i).equals(result.get(i))){
				logger.warn("Items  do not match.");
				Assertions.fail("Items  do not match.");
			}
		}
		Assertions.assertTrue(true);
	}

	@Test
	public void testFindByPreventionIdAndKey() throws Exception {

		Prevention p1 = new Prevention();
		EntityDataGenerator.generateTestDataForModelClass(p1);
		p1.setId(null);
		preventionDao.persist(p1);

		Prevention p2 = new Prevention();
		EntityDataGenerator.generateTestDataForModelClass(p2);
		p2.setId(null);
		preventionDao.persist(p2);

		int preventionId1 = p1.getId();
		int preventionId2 = p2.getId();

		String keyVal1 = "alpha";
		String keyVal2 = "bravo";

		PreventionExt prevenExt1 = new PreventionExt();
		EntityDataGenerator.generateTestDataForModelClass(prevenExt1);
		prevenExt1.setPreventionId(preventionId1);
		prevenExt1.setKeyval(keyVal1);
		preventionExtDao.persist(prevenExt1);

		PreventionExt prevenExt2 = new PreventionExt();
		EntityDataGenerator.generateTestDataForModelClass(prevenExt2);
		prevenExt2.setPreventionId(preventionId2);
		prevenExt2.setKeyval(keyVal2);
		preventionExtDao.persist(prevenExt2);

		PreventionExt prevenExt3 = new PreventionExt();
		EntityDataGenerator.generateTestDataForModelClass(prevenExt3);
		prevenExt3.setPreventionId(preventionId1);
		prevenExt3.setKeyval(keyVal1);
		preventionExtDao.persist(prevenExt3);

		List<PreventionExt> expectedResult = new ArrayList<PreventionExt>(Arrays.asList(prevenExt1, prevenExt3));
		List<PreventionExt> result = preventionExtDao.findByPreventionIdAndKey(preventionId1, keyVal1);

		Logger logger = MiscUtils.getLogger();

		if (result.size() != expectedResult.size()) {
			logger.warn("Array sizes do not match.");
			Assertions.fail("Array sizes do not match.");
		}
		for (int i = 0; i < expectedResult.size(); i++) {
			if (!expectedResult.get(i).equals(result.get(i))){
				logger.warn("Items  do not match.");
				Assertions.fail("Items  do not match.");
			}
		}
		Assertions.assertTrue(true);
	}

	@Test
	public void testGetPreventionExt() throws Exception {

		Prevention p1 = new Prevention();
		EntityDataGenerator.generateTestDataForModelClass(p1);
		p1.setId(null);
		preventionDao.persist(p1);

		Prevention p2 = new Prevention();
		EntityDataGenerator.generateTestDataForModelClass(p2);
		p2.setId(null);
		preventionDao.persist(p2);

		int preventionId1 = p1.getId();
		int preventionId2 = p2.getId();

		String val1 = "100";
		String val2 = "200";

		String keyVal1 = "alpha";
		String keyVal2 = "bravo";

		PreventionExt prevenExt1 = new PreventionExt();
		EntityDataGenerator.generateTestDataForModelClass(prevenExt1);
		prevenExt1.setPreventionId(preventionId1);
		prevenExt1.setVal(val1);
		prevenExt1.setKeyval(keyVal1);
		preventionExtDao.persist(prevenExt1);

		PreventionExt prevenExt2 = new PreventionExt();
		EntityDataGenerator.generateTestDataForModelClass(prevenExt2);
		prevenExt2.setPreventionId(preventionId2);
		prevenExt2.setVal(val2);
		prevenExt2.setKeyval(keyVal2);
		preventionExtDao.persist(prevenExt2);

		HashMap<String, String> expectedResult = new HashMap<String, String>();
		expectedResult.put(keyVal1, val1);
		HashMap<String, String> result = preventionExtDao.getPreventionExt(preventionId1);

		Assertions.assertEquals(expectedResult, result);
	}

}