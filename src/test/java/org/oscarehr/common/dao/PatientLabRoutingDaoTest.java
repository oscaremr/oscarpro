/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import ca.oscarpro.test.TagConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.util.SpringUtils;

@Tag(TagConstants.BC)
public class PatientLabRoutingDaoTest extends DaoTestFixtures {

	protected PatientLabRoutingDao dao = SpringUtils.getBean(PatientLabRoutingDao.class);

	@BeforeEach
	public void before() throws Exception {
		SchemaUtils.restoreTable("patientLabRouting", "labTestResults", "labPatientPhysicianInfo", "mdsOBX", "mdsMSH", "hl7_msh",
				"hl7_pid", "hl7_obr", "hl7_obx", "hl7_orc", "consultdocs", "mdsZRG", "mdsMSH",
				"mdsPID","mdsPV1","mdsZFR","mdsOBR");
	}

	@Test
	public void testFindDemographicByLabId() {
		dao.findDemographicByLabId(1);
	}

	@Test
	public void testFindDemographic() {
		dao.findDemographics("TYPE", 10);
	}

	@Test
	public void testFindUniqueTestNames() {
		Assertions.assertNotNull(dao.findUniqueTestNames(100, "MDS"));
	}

	@Test
	@Disabled // TODO: fix table drop/restore in @Before
	public void testFindUniqueTestNamesForPatientExcelleris() {
		Assertions.assertNotNull(dao.findUniqueTestNamesForPatientExcelleris(100, "MDS"));
	}

	@Test
	public void testFindByDemographicAndLabType() {
		Assertions.assertNotNull(dao.findByDemographicAndLabType(100, "MDS"));
	}

	@Test
	public void testFindRoutingsAndTests() {
		Assertions.assertNotNull(dao.findRoutingsAndTests(100, "MDS"));
		Assertions.assertNotNull(dao.findRoutingsAndTests(100, "MDS", "TEST"));
	}

	@Test
	@Disabled // TODO: fix table drop/restore in @Before
	public void testFindHl7InfoForRoutingsAndTests() {
		Assertions.assertNotNull(dao.findHl7InfoForRoutingsAndTests(100, "MDS", "TEST"));
	}

	@Test
	public void testFindRoutingsAndConsultDocsByRequestId() {
		Assertions.assertNotNull(dao.findRoutingsAndConsultDocsByRequestId(100, "L"));
	}
}
