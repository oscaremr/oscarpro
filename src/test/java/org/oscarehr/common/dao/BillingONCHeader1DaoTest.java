/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import ca.oscarpro.test.TagConstants;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.BillingONCHeader1;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.Provider;
import org.oscarehr.util.DateRange;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import oscar.oscarBilling.ca.on.data.BillingDataHlp;

@Tag(TagConstants.ON)
public class BillingONCHeader1DaoTest extends DaoTestFixtures {

	protected BillingONCHeader1Dao dao = SpringUtils.getBean(BillingONCHeader1Dao.class);

	public BillingONCHeader1DaoTest() {
	}

	@BeforeEach
	public void before() throws Exception {
		SchemaUtils.restoreTable("billing_on_cheader1", "billing_on_item", "gstControl", "billingservice", "provider",
				"demographic", "lst_gender", "admission", "demographic_merged", "program",
				"health_safety", "provider", "providersite", "site", "program_team","log", "Facility","billing_on_payment");
	}

	@Test
	public void testCreate() throws Exception {
		BillingONCHeader1 entity = new BillingONCHeader1();
		EntityDataGenerator.generateTestDataForModelClass(entity);
		dao.persist(entity);
		Assertions.assertNotNull(entity.getId());
	}

    @Test
	public void testCountBillingVisitsByCreator() {
        Assertions.assertNotNull(dao.countBillingVisitsByCreator("100", new Date(), new Date()));
    }

    @Test
	public void testCountBillingVisitsByProvider() {
        Assertions.assertNotNull(dao.countBillingVisitsByProvider("100", new Date(), new Date()));
	}

    @Test
    public void testFindBillingsByManyThings() {
	    Assertions.assertNotNull(dao.findBillingsByManyThings("STAT", null, null, null, null));
	    Assertions.assertNotNull(dao.findBillingsByManyThings("STAT", null, null, null, 100));
	    Assertions.assertNotNull(dao.findBillingsByManyThings("STAT", null , null, new Date(), 100));
	    Assertions.assertNotNull(
					dao.findBillingsByManyThings("STAT", null , new Date(), new Date(), 100));
	    Assertions.assertNotNull(
					dao.findBillingsByManyThings("STAT", "PROV" , new Date(), new Date(), 100));
    }

    @Test
    public void testFindByProviderStatusAndDateRange() {
    	Date date = new Date(0);
    	int demographicNo = -1;
	    Assertions.assertNotNull(dao.findByProviderStatusAndDateRangeAndDemographic("100", Arrays.asList(new String[] {"A"}), new DateRange(date, new Date()), demographicNo, false));
	    Assertions.assertNotNull(dao.findByProviderStatusAndDateRangeAndDemographic("100", Arrays.asList(new String[] {"A"}), new DateRange(null, null), demographicNo, false));
	    Assertions.assertNotNull(dao.findByProviderStatusAndDateRangeAndDemographic("100", Arrays.asList(new String[] {"A"}), new DateRange(null, new Date()), demographicNo, false));
	    Assertions.assertNotNull(dao.findByProviderStatusAndDateRangeAndDemographic("100", Arrays.asList(new String[] {"A"}), new DateRange(new Date(), null), demographicNo, false));

    }

    @Test
    public void testFindBillingsAndDemographicsById() {
	    Assertions.assertNotNull(dao.findBillingsAndDemographicsById(100));
    }

    @Test
    public void testFindByMagic() {
	    Assertions.assertNotNull(
					dao.findByMagic(Arrays.asList(new String[] {"PP"}), "STS_TY", "PROV_NO", new Date(), new Date(), 100,null,null,null, 0, 20));
    }

    @Test
    public void testFindByMagic2() {
	    Assertions.assertNotNull(
					dao.findByMagic2(Arrays.asList(new String[] {"PP"}), "STS_TY", "PROV_NO",
							new Date(), new Date(), 100, "DEMO_NAME", "DEMO_HIN", Arrays.asList(new String[] {"SVN_CODE"}), "100", "CLAIM_NO", "DX", "","VIS_TYPE",null,null,null,0,20, null));
    }

    @Test
    public void testFindBillingsByDemoNoCh1HeaderServiceCodeAndDate() {
	    Assertions.assertNotNull(dao.findBillingsByDemoNoCh1HeaderServiceCodeAndDate(100, Arrays.asList(new String[] {"PP"}), new Date(), new Date()));
    }

  @Test
  public void givenSimpleDemographic_whenAssembleAutoGeneratedHeader_thenCorrectValues() {

    // set up test data
    Date serviceDate = new Date(1000);
    OscarProperties properties = OscarProperties.getInstance();
    properties.setProperty("clinic_no", "location");

    Provider provider = new Provider();
    provider.setProviderNo("99");
    provider.setOhipNo("ohip");
    provider.setRmaNo("rma");

    Demographic demographic = new Demographic();
    demographic.setDemographicNo(1);
    demographic.setFirstName("fname");
    demographic.setLastName("lname");
    demographic.setYearOfBirth("2000");
    demographic.setMonthOfBirth("12");
    demographic.setDateOfBirth("01");
    demographic.setHin("hin");
    demographic.setVer("ver");
    demographic.setHcType("ON");

    // run the method
    BillingONCHeader1 results = dao.assembleAutoGeneratedHeader(
        provider,
        demographic,
        7,
        "clinicRefCode",
        serviceDate,
        "100",
        "creator",
        "status",
        "refNo",
        "visit",
        "site",
        properties);

    //verify provider results
    Assertions.assertEquals("99", results.getProviderNo());
    Assertions.assertEquals("ohip", results.getProviderOhipNo());
    Assertions.assertEquals("rma", results.getProviderRmaNo());
    Assertions.assertEquals("creator", results.getCreator());
    Assertions.assertEquals("refNo", results.getRefNum());
    Assertions.assertEquals("", results.getRefLabNum());
    Assertions.assertEquals("", results.getAsstProviderNo());

    // verify demographic results
    Assertions.assertEquals(1, results.getDemographicNo());
    Assertions.assertEquals("lname,fname", results.getDemographicName());
    Assertions.assertEquals("20001201", results.getDob());
    Assertions.assertEquals("ON", results.getProvince());
    Assertions.assertEquals("hin", results.getHin());
    Assertions.assertEquals("ver", results.getVer());

    // verify appointment results
    Assertions.assertEquals(7, results.getAppointmentNo());
    Assertions.assertEquals("", results.getApptProviderNo());

    // verify payment results
    Assertions.assertEquals(BillingDataHlp.CLAIMHEADER1_PAYEE, results.getPayee());
    Assertions.assertEquals("HCP", results.getPayProgram()); // based on HcType
    Assertions.assertEquals(new BigDecimal(100), results.getTotal());
    Assertions.assertEquals(new BigDecimal("0.00"), results.getPaid());
    //TODO figure out how to test these dates
    //Assertions.assertEquals(serviceDate, results.getBillingDate());
    //Assertions.assertEquals(serviceDate, results.getBillingTime());

    // verify misc results
    Assertions.assertEquals("status", results.getStatus());
    Assertions.assertEquals("visit", results.getVisitType());
    Assertions.assertEquals("site", results.getClinic());
    Assertions.assertEquals("location", results.getLocation());
    Assertions.assertEquals(BillingDataHlp.CLAIMHEADER1_TRANSACTIONIDENTIFIER, results.getTranscId());
    Assertions.assertEquals(BillingDataHlp.CLAIMHEADER1_REORDIDENTIFICATION, results.getRecId());
    Assertions.assertEquals(0, results.getHeaderId());
    Assertions.assertEquals("clinicRefCode", results.getFaciltyNum());
    Assertions.assertEquals("", results.getManReview());
    Assertions.assertEquals("Auto generated billing.", results.getComment());
  }

    @Override
    protected List<String> getSimpleExceptionTestExcludes() {
		List<String> excludes = super.getSimpleExceptionTestExcludes();
		// this is very JSP specific method that includes a mix of SQL fields, we will test it manuall in #findBillingData
		excludes.add("findBillingData");

	    return excludes;
    }
}
