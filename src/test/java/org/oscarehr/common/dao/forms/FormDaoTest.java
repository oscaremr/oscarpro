/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao.forms;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import lombok.val;
import org.apache.commons.lang.StringEscapeUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.FormDao;

@ExtendWith(MockitoExtension.class)
public class FormDaoTest {

  @Mock private EntityManager entityManager;

  @Mock private Query query;

  @InjectMocks private FormDao formDao;

  private static final int ARCHIVE_STATUS = 1;
  private static final int UNARCHIVE_STATUS = 0;
  private static final String PERINATAL_FORM_TABLE_NAME = "form_on_perinatal_2017";
  private static final String FORM_TABLE_NAME = "form_name";
  private static final String PERINATAL_ID_COLUMN = "form_id";
  private static final String FORM_ID_COLUMN = "id";
  private static final String ID_VALUE = "1";

  @BeforeEach
  void setUp() {
    when(entityManager.createNativeQuery(anyString())).thenReturn(query);
  }

  @Test
  public void givenArchivePerinatalForm_whenChangeArchiveFormRecord_thenExecuteSetStatusAsOne() {
    val expectedQuery = String.format("UPDATE %s f SET f.archived = ? " + "WHERE f.%s = ?",
        StringEscapeUtils.escapeSql(PERINATAL_FORM_TABLE_NAME), PERINATAL_ID_COLUMN);

    when(entityManager.createNativeQuery(expectedQuery)).thenReturn(query);
    formDao.changeArchiveFormRecord(PERINATAL_FORM_TABLE_NAME, ID_VALUE, ARCHIVE_STATUS);

    verify(entityManager).createNativeQuery(expectedQuery);
    verify(query).setParameter(1, ARCHIVE_STATUS);
    verify(query).setParameter(2, ID_VALUE);
    verify(query).executeUpdate();
  }

  @Test
  public void givenUnArchivePerinatalForm_whenChangeArchiveFormRecord_thenExecuteSetStatusAsZero() {
    val expectedQuery = String.format("UPDATE %s f SET f.archived = ? " + "WHERE f.%s = ?",
        StringEscapeUtils.escapeSql(PERINATAL_FORM_TABLE_NAME), PERINATAL_ID_COLUMN);

    when(entityManager.createNativeQuery(expectedQuery)).thenReturn(query);
    formDao.changeArchiveFormRecord(PERINATAL_FORM_TABLE_NAME, ID_VALUE, UNARCHIVE_STATUS);

    verify(entityManager).createNativeQuery(expectedQuery);
    verify(query).setParameter(1, UNARCHIVE_STATUS);
    verify(query).setParameter(2, ID_VALUE);
    verify(query).executeUpdate();
  }

  @Test
  public void givenArchiveNotPerinatalForm_whenChangeArchiveFormRecord_thenExecuteSetStatusAsOne() {
    val expectedQuery = String.format("UPDATE %s f SET f.archived = ? " + "WHERE f.%s = ?",
        StringEscapeUtils.escapeSql(FORM_TABLE_NAME), FORM_ID_COLUMN);

    when(entityManager.createNativeQuery(expectedQuery)).thenReturn(query);
    formDao.changeArchiveFormRecord(FORM_TABLE_NAME, ID_VALUE, ARCHIVE_STATUS);

    verify(entityManager).createNativeQuery(expectedQuery);
    verify(query).setParameter(1, ARCHIVE_STATUS);
    verify(query).setParameter(2, ID_VALUE);
    verify(query).executeUpdate();
  }

  @Test
  public void givenUnArchiveNotPerinatalForm_whenChangeArchiveFormRecord_thenExecuteSetStatusAsZero() {
    val expectedQuery = String.format("UPDATE %s f SET f.archived = ? " + "WHERE f.%s = ?",
        StringEscapeUtils.escapeSql(FORM_TABLE_NAME), FORM_ID_COLUMN);

    when(entityManager.createNativeQuery(expectedQuery)).thenReturn(query);
    formDao.changeArchiveFormRecord(FORM_TABLE_NAME, ID_VALUE, UNARCHIVE_STATUS);

    verify(entityManager).createNativeQuery(expectedQuery);
    verify(query).setParameter(1, UNARCHIVE_STATUS);
    verify(query).setParameter(2, ID_VALUE);
    verify(query).executeUpdate();
  }
}
