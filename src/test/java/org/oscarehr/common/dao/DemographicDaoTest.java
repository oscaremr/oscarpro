/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import static org.junit.Assert.assertEquals;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import lombok.val;
import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.Gender;
import org.oscarehr.common.dao.DemographicDao.DemographicCriterion;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicGender;
import org.oscarehr.common.model.DemographicPronoun;
import org.oscarehr.util.SpringUtils;

public class DemographicDaoTest extends DaoTestFixtures {

  protected final DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);

  @BeforeEach
  public void before() throws Exception {
    super.beforeDemographic();
    super.beforeProvider();
  }

  @Test
  public void testCreate() throws Exception {
    val entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    demographicDao.save(entity);
    Assertions.assertNotNull(entity.getDemographicNo());
  }

  @Test
  public void testGetDemographic() throws Exception {
    val entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    demographicDao.save(entity);

    Assertions.assertNotNull(demographicDao.getDemographic(entity.getDemographicNo().toString()));
    Assertions.assertNotNull(demographicDao.getDemographicById(entity.getDemographicNo()));
    Assertions.assertNotNull(demographicDao.getClientByDemographicNo(entity.getDemographicNo()));
  }

  @Test
  public void testGetDemographicByProvider() throws Exception {
    val entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setProviderNo("000001");
    entity.setPatientStatus("AC");
    demographicDao.save(entity);

    Assertions.assertNotNull(demographicDao.getDemographicByProvider(entity.getProviderNo()));
    Assertions.assertNotNull(
        demographicDao.getDemographicByProvider(entity.getProviderNo(), false));

    Assertions.assertEquals(1,
        demographicDao.getDemographicByProvider(entity.getProviderNo()).size());
    Assertions.assertEquals(1,
        demographicDao.getDemographicByProvider(entity.getProviderNo(), false).size());
  }

  @Test
  public void testGetDemographicByMyOscarUserName() throws Exception {
    val entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setMyOscarUserName("marc");
    demographicDao.save(entity);

    Assertions.assertNotNull(demographicDao.getDemographicByMyOscarUserName("marc"));
  }

  @Test
  public void testGetActiveDemosByHealthCardNo() throws Exception {
    val entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setHin("2222222222");
    entity.setHcType("Ontario");
    entity.setPatientStatus("AC");
    demographicDao.save(entity);

    Assertions.assertNotNull(
        demographicDao.getActiveDemosByHealthCardNo(entity.getHin(), entity.getHcType()));
    Assertions.assertEquals(1,
        demographicDao.getActiveDemosByHealthCardNo(entity.getHin(), entity.getHcType()).size());
  }

  @Test
  public void testSearchDemographic() throws Exception {
    val entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setLastName("Smith");
    entity.setFirstName("John");
    demographicDao.save(entity);

    Assertions.assertEquals(1, demographicDao.searchDemographic("Smi").size());
    Assertions.assertEquals(0, demographicDao.searchDemographic("Doe").size());
    Assertions.assertEquals(1, demographicDao.searchDemographic("Smi,Jo").size());
    Assertions.assertEquals(0, demographicDao.searchDemographic("Smi,Ja").size());
  }

  @Test
  public void testGetRosterStatuses() throws Exception {
    var entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setRosterStatus("AB");
    demographicDao.save(entity);

    entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setRosterStatus("AB");
    demographicDao.save(entity);

    entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setRosterStatus("AC");
    demographicDao.save(entity);

    Assertions.assertEquals(2, demographicDao.getRosterStatuses().size());
  }

  @Test
  public void testClientExists() throws Exception {
    val entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    demographicDao.save(entity);

    Assertions.assertNotNull(entity.getDemographicNo());
    Assertions.assertTrue(demographicDao.clientExists(entity.getDemographicNo()));
  }

  @Test
  public void testGetClientsByChartNo() throws Exception {
    val entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setChartNo("000001");
    demographicDao.save(entity);

    Assertions.assertNotNull(demographicDao.getClientsByChartNo(entity.getChartNo()));
    Assertions.assertEquals(1, demographicDao.getClientsByChartNo(entity.getChartNo()).size());
  }

  @Test
  public void testGetClientsByHealthCard() throws Exception {
    val entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setHin("2222222222");
    entity.setHcType("ontario");
    demographicDao.save(entity);

    Assertions.assertNotNull(
        demographicDao.getClientsByHealthCard(entity.getHin(), entity.getHcType()));
    Assertions.assertEquals(1,
        demographicDao.getClientsByHealthCard(entity.getHin(), entity.getHcType()).size());
  }

  @Test
  public void testSearchByHealthCard() throws Exception {
    val entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setHin("2222222222");
    demographicDao.save(entity);

    Assertions.assertNotNull(demographicDao.searchByHealthCard(entity.getHin()));
    Assertions.assertEquals(1, demographicDao.searchByHealthCard(entity.getHin()).size());
  }

  @Test
  public void testGetDemographicByNamePhoneEmail() throws Exception {
    val entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setLastName("Smith");
    entity.setFirstName("John");
    entity.setPhone("444-444-4444");
    entity.setPhone2("555-555-5555");
    entity.setEmail("a@b.com");
    demographicDao.save(entity);

    Assertions.assertNotNull(
        demographicDao.getDemographicByNamePhoneEmail(entity.getFirstName(), entity.getLastName(),
            entity.getPhone(), entity.getPhone2(), entity.getEmail()));
    Assertions.assertEquals(entity.getDemographicNo(),
        demographicDao.getDemographicByNamePhoneEmail(entity.getFirstName(), entity.getLastName(),
            entity.getPhone(), entity.getPhone2(), entity.getEmail()).getDemographicNo());
  }

  @Test
  public void testGetDemographicWithLastFirstDOB() throws Exception {
    Demographic entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setLastName("Smith");
    entity.setFirstName("John");
    entity.setYearOfBirth("1999");
    entity.setMonthOfBirth("12");
    entity.setDateOfBirth("01");
    demographicDao.save(entity);

    Assertions.assertNotNull(
        demographicDao.getDemographicWithLastFirstDOB(entity.getLastName(), entity.getFirstName(),
            entity.getYearOfBirth(), entity.getMonthOfBirth(), entity.getDateOfBirth()));
    Assertions.assertEquals(1,
        demographicDao.getDemographicWithLastFirstDOB(entity.getLastName(), entity.getFirstName(),
            entity.getYearOfBirth(), entity.getMonthOfBirth(), entity.getDateOfBirth()).size());
  }

  @Test
  public void testFindByCriterion() {
    Assertions.assertNotNull(demographicDao.findByCriterion(
        new DemographicCriterion(null, "", "", "", "", "", "", "")));
    Assertions.assertNotNull(demographicDao.findByCriterion(
        new DemographicCriterion("", "", "", "", "", "", "", "")));
  }

  @Test
  public void testGetAllPatientStatuses() {
    Assertions.assertNotNull(demographicDao.getAllPatientStatuses());
  }

  @Test
  public void testGetAllRosterStatuses() {
    Assertions.assertNotNull(demographicDao.getAllRosterStatuses());
  }

  @Test
  public void testGetAllProviderNumbers() {
    Assertions.assertNotNull(demographicDao.getAllProviderNumbers());
  }

  @Test
  public void testFindByField() {
    Assertions.assertNotNull(demographicDao.findByField("DemographicNo", "", "DemographicNo", 0));

    for (String s : new String[]{"LastName", "FirstName", "ChartNo", "Sex", "YearOfBirth",
        "PatientStatus"}) {
      Assertions.assertNotNull(demographicDao.findByField(s, "BLAH", "DemographicNo", 0));
    }
  }

  @Override
  protected List<String> getSimpleExceptionTestExcludes() {
    val result = super.getSimpleExceptionTestExcludes();
    result.add("findByField");
    return result;
  }

  @Test
  public void findByAttributes() throws Exception {
    var entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setHin("7771111");
    entity.setFirstName("bob");
    entity.setLastName("the builder");
    entity.setSex(Gender.M.name());
    entity.setBirthDay(new GregorianCalendar(1990, 1, 4));
    entity.setPhone("5556667777");
    entity.setPhone2("9998884444");
    demographicDao.save(entity);

    entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setHin("7772222");
    entity.setFirstName("bart");
    entity.setLastName("simpson");
    entity.setSex(Gender.M.name());
    entity.setBirthDay(new GregorianCalendar(1980, 2, 5));
    entity.setPhone("2224446666");
    demographicDao.save(entity);

    entity = new Demographic();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(null);
    entity.setHin("7773333");
    entity.setFirstName("lisa");
    entity.setLastName("simpson");
    entity.setSex(Gender.F.name());
    entity.setBirthDay(new GregorianCalendar(1970, 0, 9));
    entity.setPhone2("6665553333");
    demographicDao.save(entity);

    List<Demographic> results = demographicDao.findByAttributes("777", null, null, null, null, null,
        null, null, null, null, 0, 99);
    Assertions.assertEquals(3, results.size());

    results = demographicDao.findByAttributes(null, null, "sim", null, null, null, null, null, null,
        null, 0, 99);
    Assertions.assertEquals(2, results.size());

    results = demographicDao.findByAttributes(null, "bar", "sim", null, null, null, null, null,
        null, null, 0, 99);
    Assertions.assertEquals(1, results.size());

    results = demographicDao.findByAttributes(null, "b", null, null, null, null, null, null, null,
        null, 0, 99);
    Assertions.assertEquals(2, results.size());

    results = demographicDao.findByAttributes(null, "b", null, null,
        new GregorianCalendar(1980, 2, 5), null, null, null, null, null, 0, 99);
    Assertions.assertEquals(1, results.size());

    results = demographicDao.findByAttributes(null, "b", null, null, null, null, null, "6665553333",
        null, null, 0, 99);
    Assertions.assertEquals(0, results.size());

    results = demographicDao.findByAttributes(null, null, null, null, null, null, null,
        "6665553333", null, null, 0, 99);
    Assertions.assertEquals(1, results.size());

    results = demographicDao.findByAttributes(null, "lisa", null, null, null, null, null,
        "66555333", null, null, 0, 99);
    Assertions.assertEquals(1, results.size());

    results = demographicDao.findByAttributes(null, null, null, Gender.F, null, null, null, null,
        null, null, 0, 99);
    Assertions.assertEquals(1, results.size());

    results = demographicDao.findByAttributes(null, null, null, Gender.F, null, null, null,
        "66555333", null, null, 0, 99);
    Assertions.assertEquals(1, results.size());
  }

  @Test
  void testConstructor() {
    val original = new Demographic();
    val demographicGender = new DemographicGender();
    demographicGender.setId(1);
    demographicGender.setValue("Male");
    demographicGender.setDeleted(false);
    demographicGender.setEditable(false);
    val demographicPronoun = new DemographicPronoun();
    demographicPronoun.setId(1);
    demographicPronoun.setValue("She/Her");
    demographicPronoun.setDeleted(false);
    demographicPronoun.setEditable(false);
    original.setDemographicNo(1);
    original.setActiveCount(5);
    original.setAddress("123 Main St");
    original.setAlias("John Doe");
    original.setAnonymous("Yes");
    original.setChartNo("C12345");
    original.setChildren("2");
    original.setCitizenship("US");
    original.setCity("New York");
    original.setConsentToUseEmailForCare(true);
    original.setConsentToUseEmailForEOrder(true);
    original.setCountryOfOrigin("USA");
    original.setDateJoined(new Date());
    original.setDateOfBirth("1980-01-01");
    original.setGenderIdentity(demographicGender);
    original.setPronoun(demographicPronoun);
    original.setGenderId(1);
    original.setPronounId(1);
    original.setEffDate(new Date());
    original.setEmail("john.doe@example.com");
    original.setEndDate(new Date());
    original.setFirstName("John");
    original.setHcRenewDate(new Date());
    original.setHcType("Type A");
    original.setHeadRecord(1);
    original.setHin("H12345");
    original.setHsAlertCount(3);
    original.setLastName("Doe");
    original.setLastUpdateDate(new Date());
    original.setLastUpdateUser("admin");
    original.setLinks("www.example.com");
    original.setMiddleName("Middle");
    original.setMonthOfBirth("January");
    original.setMyOscarUserName("johndoe");
    original.setNewsletter("Yes");
    original.setOfficialLanguage("English");
    original.setPatientId("P12345");
    original.setPatientStatus("Active");
    original.setPatientStatusDate(new Date());
    original.setPatientType("Type 1");
    original.setPcnIndicator("Indicator 1");
    original.setPhone("1234567890");
    original.setPhone2("0987654321");
    original.setPortalUserId("portaluser");
    original.setPostal("12345");
    original.setPreferredName("Johnny");
    original.setPreviousAddress("456 Secondary St");
    original.setProvince("NY");
    original.setRosterDate(new Date());
    original.setRosterStatus("Enrolled");
    original.setRosterTerminationDate(new Date());
    original.setRosterTerminationReason("Reason");
    original.setSex("Male");
    original.setSexDesc("Male");
    original.setSin("S12345");
    original.setSourceOfIncome("Employment");
    original.setSpokenLanguage("English");
    original.setTitle("Mr.");
    original.setVer("1.0");
    original.setYearOfBirth("1980");
    original.setMiddleNames("Middle");
    original.setRosterEnrolledTo("EnrolledTo");

    val copy = new Demographic(original);
    // Verify each field in the copy matches the original
    assertEquals(original.getDemographicNo(), copy.getDemographicNo());
    assertEquals(original.getActiveCount(), copy.getActiveCount());
    assertEquals(original.getAddress(), copy.getAddress());
    assertEquals(original.getAlias(), copy.getAlias());
    assertEquals(original.getAnonymous(), copy.getAnonymous());
    assertEquals(original.getChartNo(), copy.getChartNo());
    assertEquals(original.getChildren(), copy.getChildren());
    assertEquals(original.getCitizenship(), copy.getCitizenship());
    assertEquals(original.getCity(), copy.getCity());
    assertEquals(original.getConsentToUseEmailForCare(), copy.getConsentToUseEmailForCare());
    assertEquals(original.getConsentToUseEmailForEOrder(), copy.getConsentToUseEmailForEOrder());
    assertEquals(original.getCountryOfOrigin(), copy.getCountryOfOrigin());
    assertEquals(original.getDateJoined(), copy.getDateJoined());
    assertEquals(original.getDateOfBirth(), copy.getDateOfBirth());
    assertEquals(original.getGenderIdentity(), copy.getGenderIdentity());
    assertEquals(original.getPronoun(), copy.getPronoun());
    assertEquals(original.getGenderId(), copy.getGenderId());
    assertEquals(original.getPronounId(), copy.getPronounId());
    assertEquals(original.getEffDate(), copy.getEffDate());
    assertEquals(original.getEmail(), copy.getEmail());
    assertEquals(original.getEndDate(), copy.getEndDate());
    assertEquals(original.getFirstName(), copy.getFirstName());
    assertEquals(original.getHcRenewDate(), copy.getHcRenewDate());
    assertEquals(original.getHcType(), copy.getHcType());
    assertEquals(original.getHeadRecord(), copy.getHeadRecord());
    assertEquals(original.getHin(), copy.getHin());
    assertEquals(original.getHsAlertCount(), copy.getHsAlertCount());
    assertEquals(original.getLastName(), copy.getLastName());
    assertEquals(original.getLastUpdateDate(), copy.getLastUpdateDate());
    assertEquals(original.getLastUpdateUser(), copy.getLastUpdateUser());
    assertEquals(original.getLinks(), copy.getLinks());
    assertEquals(original.getMiddleName(), copy.getMiddleName());
    assertEquals(original.getMonthOfBirth(), copy.getMonthOfBirth());
    assertEquals(original.getMyOscarUserName(), copy.getMyOscarUserName());
    assertEquals(original.getNewsletter(), copy.getNewsletter());
    assertEquals(original.getOfficialLanguage(), copy.getOfficialLanguage());
    assertEquals(original.getPatientId(), copy.getPatientId());
    assertEquals(original.getPatientStatus(), copy.getPatientStatus());
    assertEquals(original.getPatientStatusDate(), copy.getPatientStatusDate());
    assertEquals(original.getPatientType(), copy.getPatientType());
    assertEquals(original.getPcnIndicator(), copy.getPcnIndicator());
    assertEquals(original.getPhone(), copy.getPhone());
    assertEquals(original.getPhone2(), copy.getPhone2());
    assertEquals(original.getPortalUserId(), copy.getPortalUserId());
    assertEquals(original.getPostal(), copy.getPostal());
    assertEquals(original.getPreferredName(), copy.getPreferredName());
    assertEquals(original.getPreviousAddress(), copy.getPreviousAddress());
    assertEquals(original.getProvince(), copy.getProvince());
    assertEquals(original.getRosterDate(), copy.getRosterDate());
    assertEquals(original.getRosterStatus(), copy.getRosterStatus());
    assertEquals(original.getRosterTerminationDate(), copy.getRosterTerminationDate());
    assertEquals(original.getRosterTerminationReason(), copy.getRosterTerminationReason());
    assertEquals(original.getSex(), copy.getSex());
    assertEquals(original.getSexDesc(), copy.getSexDesc());
    assertEquals(original.getSin(), copy.getSin());
    assertEquals(original.getSourceOfIncome(), copy.getSourceOfIncome());
    assertEquals(original.getSpokenLanguage(), copy.getSpokenLanguage());
    assertEquals(original.getTitle(), copy.getTitle());
    assertEquals(original.getVer(), copy.getVer());
    assertEquals(original.getYearOfBirth(), copy.getYearOfBirth());
    assertEquals(original.getMiddleNames(), copy.getMiddleNames());
    assertEquals(original.getRosterEnrolledTo(), copy.getRosterEnrolledTo());
  }
}
