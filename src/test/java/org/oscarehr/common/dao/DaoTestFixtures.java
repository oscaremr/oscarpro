/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
// -----------------------------------------------------------------------------------------------------------------------
// *
// *
// * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved. *
// * This software is published under the GPL GNU General Public License.
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the GNU General Public License
// * as published by the Free Software Foundation; either version 2
// * of the License, or (at your option) any later version. *
// * This program is distributed in the hope that it will be useful,
// * but WITHOUT ANY WARRANTY; without even the implied warranty of
// * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// * GNU General Public License for more details. * * You should have received a copy of the GNU General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
// *
// * <OSCAR TEAM>
// * This software was written for the
// * Department of Family Medicine
// * McMaster University
// * Hamilton
// * Ontario, Canada
// *
// -----------------------------------------------------------------------------------------------------------------------
package org.oscarehr.common.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.oscarehr.common.dao.utils.ConfigUtils;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.OscarTrackingBasicDataSource;
import org.oscarehr.util.SpringUtils;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import utils.OscarPropertiesUtil;

@Slf4j
public abstract class DaoTestFixtures {
  @Getter
  private static final LoggedInInfo loggedInInfo =
      LoggedInInfo.getLoggedInInfoAsCurrentClassAndMethod();

  public void beforeForInnoDB() throws Exception {
    SchemaUtils.restoreTable(
        "IntegratorConsent",
        "HnrDataValidation",
        "ClientLink",
        "IntegratorConsentComplexExitInterview",
        "DigitalSignature",
        "appointment",
        "admission",
        "program",
        "demographic");
  }

	public void beforeDemographic() throws Exception {
		// order matters for foreign keys
		SchemaUtils.restoreTable(
				// depend on demographic
				"admission",
				"ClientLink",
				"DemographicGender",
				"DemographicPronoun",
				"demographic_merged",
				"demographicstudy",
				"email_attachment",
				"email_log",
				"HnrDataValidation",
				"IntegratorConsentShareDataMap",
				"IntegratorConsentComplexExitInterview",
				"IntegratorConsent",
				"PeriodDef",
				"tickler_comments",
				"tickler_update",
				"tickler",
				"sharing_document_export",
				// foreign key constraints dropped
				"demographic",
				"demographicArchive",
				"demographicExt",
				"demographicExtArchive"
		);

		SchemaUtils.restoreTable(
				"demographic",
				"demographicArchive",
				"demographicExt",
				"demographicExtArchive",
				// depend on demographic
				"admission",
				"ClientLink",
				"DemographicGender",
				"DemographicPronoun",
				"demographic_merged",
				"demographicstudy",
				"email_log",
				"email_attachment",
				"health_safety",
				"lst_gender",
				"PeriodDef",
				"site",
				"tickler",
				"tickler_comments",
				"tickler_update"
				// causes duplicate foreign key constraint error on restore
				// "sharing_document_export",
		);
	}

	public void beforeProvider() throws Exception {
		SchemaUtils.restoreTable(
				// depend on provider
				"appointment",
				"custom_filter_assignees",
				"ClientLink",
				"DigitalSignature",
				"document_review",
				"IntegratorConsentShareDataMap",
				"IntegratorConsentComplexExitInterview",
				"IntegratorConsent",
				"log",
				"program_client_restriction",
				"program_team",
				"program",
				"Facility", // Facility depends on program
				"providersite",
				"site",
				// provider
				"provider"
		);

		SchemaUtils.restoreTable(
				// provider
				"provider",
				// depend on provider
				"appointment",
				"custom_filter_assignees",
				"DigitalSignature",
				"document_review",
				"IntegratorConsentShareDataMap",
				"IntegratorConsentComplexExitInterview",
				"IntegratorConsent",
				"Facility",
				"ClientLink",
				"log",
				// "program_client_restriction",
				"program_team",
				"program",
				"providersite",
				"site"
		);
	}

	public void beforeConsultations() throws Exception {
		SchemaUtils.restoreTable(
				"consultationRequests",
				"consultationServices",
				"LookupListItem",
				"professionalSpecialists",
				"serviceSpecialists"
		);

		SchemaUtils.restoreTable(
				"consultationRequests",
				"consultationServices",
				"LookupListItem",
				"professionalSpecialists",
				"serviceSpecialists"
		);
	}

	@AfterAll
	@AfterClass
	public static void afterClass() throws Exception {
		OscarTrackingBasicDataSource.releaseThreadConnections();
	}

	@BeforeAll
	@BeforeClass
	public static void classSetUp() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, IOException {

		Logger.getRootLogger().setLevel(Level.WARN);

		long start = System.currentTimeMillis();
		if(SchemaUtils.databaseInitializationComplete()) {
			OscarTrackingBasicDataSource.releaseThreadConnections();
		}
		else if (SchemaUtils.databaseInitializationAttempted()) {
			log.error("Database initialization failed. Assert Failure.");
			assert false;
		}
		else {
			log.info("dropAndRecreateDatabase");
			SchemaUtils.dropAndRecreateDatabase();
		}
		long end = System.currentTimeMillis();
		long secsTaken = (end-start)/1000;
		if(secsTaken > 30) {
      log.info("Setting up db took " + secsTaken + " seconds.");
		}

		start = System.currentTimeMillis();
		if(SpringUtils.beanFactory==null) {
			oscar.OscarProperties p = OscarPropertiesUtil.getOscarProperties();
			p.setProperty("db_name", ConfigUtils.getProperty("db_schema") + ConfigUtils.getProperty("db_schema_properties"));
			p.setProperty("db_username", ConfigUtils.getProperty("db_user"));
			p.setProperty("db_password", ConfigUtils.getProperty("db_password"));
			p.setProperty("db_uri", ConfigUtils.getProperty("db_url_prefix"));
			p.setProperty("db_driver", ConfigUtils.getProperty("db_driver"));
			loadApplicationContext();
		}
		end = System.currentTimeMillis();
		secsTaken = (end-start)/1000;
    log.info("Setting up spring took " + secsTaken + " seconds.");
	}

  protected static void loadApplicationContext()
      throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
    
    // these tables are required during spring bean initialization
    SchemaUtils.restoreTable("DataSharingSettings", "ResourceStorage", "SystemPreferences");
    SpringUtils.beanFactory = new ClassPathXmlApplicationContext("applicationContext.xml");
  }

	/**
	 * Gets list of all methods that should be ignored for simple exception test on the DAO.
	 *
	 * @return
	 * 		Returns a list of methods to be skipped during exception testing.
	 */
	protected List<String> getSimpleExceptionTestExcludes() {
		String[] excludes = {
				"batchMerge",
				"batchPersist",
				"batchRemove",
				"equals",
				"find",
				"findAll",
				"getClass",
				"getCountAll",
				"getModelClass",
				"hashCode",
				"merge",
				"notify",
				"notifyAll",
				"persist",
				"refresh",
				"remove",
				"removeAll",
				"runNativeQuery",
				"save",
				"saveEntity",
				"toString",
				"wait",
		};
		List<String> excludeList = new ArrayList<String>();
		excludeList.addAll(Arrays.asList(excludes));
		return excludeList;
	}
}
