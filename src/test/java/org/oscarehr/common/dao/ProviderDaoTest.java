/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import static com.well.integration.config.generators.InsigGenerator.INSIG_NAMES;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.Provider;
import org.oscarehr.util.SpringUtils;

public class ProviderDaoTest extends DaoTestFixtures {

  protected ProviderDao dao = SpringUtils.getBean(ProviderDao.class);

  @BeforeEach
  public void before() throws Exception {
    SchemaUtils.restoreTable(
        false,
        "provider",
        "providersite",
        "demographic",
        "provider_facility",
        "Facility");
  }

  @Test
  public void testFindOhip() {
    List<Provider> providers = dao.getProvidersWithNonEmptyOhip();
    assertNotNull(providers);
  }

  @Test
  public void testGetCurrentTeamProviders() {
    dao.getCurrentTeamProviders("999998");
  }

  @Test
  public void testActiveProviders() {
    assertNotNull(dao.getActiveProviders());
  }

  @Test
  public void testGetActiveTeamsViaSites() {
    assertNotNull(dao.getActiveTeamsViaSites("100"));
  }

  @Test
  public void testGetProviderByPatientId() {
    assertNotNull(dao.getProviderByPatientId(100));
  }

  @Test
  public void test() {
    assertNotNull(dao.getProvidersByTypePattern("%nurse%"));
  }

  @Test
  public void testGetProviderIds() {
    assertNotNull(dao.getProviderIds(1));
  }

  @Test
  public void testGetFacilityIds() {
    assertNotNull(dao.getFacilityIds("999998"));
  }

  @Test
  public void testDetDoctorsWithNonEmptyCredentials() {
    assertNotNull(dao.getDoctorsWithNonEmptyCredentials());
  }

  @Test
  public void testProviderFindByNamesLikeNotFound() {
    assertListIsEmpty(dao.findByNamesLike(null));
    assertListIsEmpty(dao.findByNamesLike(new ArrayList<String>()));
    assertListIsEmpty(dao.findByNamesLike(INSIG_NAMES));
  }

  @Test
  public void testProviderFindByNamesLikeInsig() throws Exception {
    testProviderFindByNamesLike("Insig", "Integration");
    testProviderFindByNamesLike("InsigIntegration", "Provider");
    testProviderFindByNamesLike("Doctor", "InsigIntegration");
    testProviderFindByNamesLike("integration", "insig");
  }

  @Test
  public void testProviderFindByNamesLikeVirtual() throws Exception {
    testProviderFindByNamesLike("Virtual", "Care");
    testProviderFindByNamesLike("VirtualCare", "Doctor");
    testProviderFindByNamesLike("care", "virtual");
  }

  @Test
  public void givenProviderWithTeam_whenGetBillableProvidersOnTeam_thenReturnEmptyList()
      throws Exception {
    val provider = createTestProvider("Billable", "Provider", null);
    provider.setTeam("Team");

    val providers = dao.getBillableProvidersOnTeam(provider);

    assertNotNull(providers);
  }

  @Test
  public void givenProviderWithTeam_whenGetCurrentInactiveTeamProviders_thenReturnEmptyList()
      throws Exception {
    val provider = createTestProvider("Billable", "Provider", null);
    provider.setTeam("Team");

    val providers = dao.getCurrentInactiveTeamProviders(provider.getProviderNo());

    assertNotNull(providers);
    assertEquals(0, providers.size());
  }

  @Test
  public void givenPractitionerNoWithPractitionerNoFalse_whenGetReferringPractitioner_thenReturnIt()
      throws Exception {
    createTestProvider("Test", "Provider", "99098");
    val referralPractitioners = dao.getReferringPractitioner(false);
    assertEquals(1, referralPractitioners.size());
  }

  @Test
  public void givenPractitionerNoAndWithPractitionerNoTrue_whenGetReferringPractitioner_thenReturnPractitioner()
      throws Exception {
    createTestProvider("Test", "Provider", "99098");
    val referralPractitioners = dao.getReferringPractitioner(true);
    assertEquals(1, referralPractitioners.size());
  }

  @Test
  public void givenNullPractitionerNoAndWithPractitionerNoFalse_whenGetReferringPractitioner_thenReturnPractitioner()
      throws Exception {
    createTestProvider("Test", "Provider", null);
    val referralPractitioners = dao.getReferringPractitioner(false);
    assertEquals(1, referralPractitioners.size());
  }

  @Test
  public void givenNullPractitionerNoAndWithPractitionerNoTrue_whenGetReferringPractitioner_thenReturnNone()
      throws Exception {
    createTestProvider("Test", "Provider", null);
    val referralPractitioners = dao.getReferringPractitioner(true);
    assertEquals(0, referralPractitioners.size());
  }

  @Test
  public void givenEmptyStringPractitionerNoAndWithPractitionerNoFalse_whenGetReferringPractitioner_thenReturnPractitioner()
      throws Exception {
    createTestProvider("Test", "Provider", "");
    val referralPractitioners = dao.getReferringPractitioner(false);
    assertEquals(1, referralPractitioners.size());
  }

  @Test
  public void givenEmptyStringPractitionerNoAndWithPractitionerNoTrue_whenGetReferringPractitioner_thenReturnNone()
      throws Exception {
    createTestProvider("Test", "Provider", "");
    val referralPractitioners = dao.getReferringPractitioner(true);
    assertEquals(0, referralPractitioners.size());
  }

  private void testProviderFindByNamesLike(String firstName, String lastName) throws Exception {
    SchemaUtils.restoreTable("provider");
    Provider p = createTestProvider(firstName, lastName, null);
    List<Provider> providers = dao.findByNamesLike(INSIG_NAMES);
    assertEquals(1, providers.size());
    assertEquals(p.getProviderNo(), providers.get(0).getProviderNo());
  }

  private Provider createTestProvider(String firstName, String lastName, String practitionerNo) {
    Provider p = new Provider();
    p.setProviderNo(dao.suggestProviderNo());
    p.setLastName(lastName);
    p.setFirstName(firstName);
    p.setProviderType("doctor");
    p.setSex("F");
    p.setSpecialty("Integration");
    p.setStatus("1");
    p.setPrescribeItType("doctor");
    p.setPractitionerNo(practitionerNo);
    dao.saveProvider(p);
    return p;
  }

  private void assertListIsEmpty(List list) {
    assertNotNull(list);
    assertEquals(0, list.size());
  }
}
