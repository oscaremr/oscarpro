/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import ca.kai.util.DateUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import lombok.SneakyThrows;
import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.MeasurementDao.SearchCriteria;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.Measurement;
import org.oscarehr.common.model.MeasurementMap;
import org.oscarehr.common.model.MeasurementsExt;
import org.oscarehr.util.SpringUtils;

public class MeasurementDaoTest extends DaoTestFixtures {
	public static final Date BEFORE = getDateTimeFromString("2020-01-01 00:00:00");
	public static final Date JUST_BEFORE = getDateTimeFromString("2020-02-04 00:00:00");
	public static final Date AT = getDateTimeFromString("2020-02-05 00:00:00");
	public static final Date JUST_AFTER = getDateTimeFromString("2020-02-06 00:00:00");
	public static final Date AFTER = getDateTimeFromString("2020-03-01 00:00:00");
	public static final String IDENT_CODE = "IC1";
	protected MeasurementDao dao = SpringUtils.getBean(MeasurementDao.class);
	protected MeasurementMapDao mmDao = SpringUtils.getBean(MeasurementMapDao.class);
	protected MeasurementsExtDao meDao = SpringUtils.getBean(MeasurementsExtDao.class);

	@BeforeEach
	public void before() throws Exception {
		SchemaUtils.restoreTable(
				"measurements",
				"measurementType",
				"measurementsExt",
				"measurementMap",
				"provider"
		);
	}

	@Test
	public void testFind() throws ParseException {
		Measurement m = populate();

		SearchCriteria c = new SearchCriteria();
		c.setComments(m.getComments());
		c.setDataField(m.getDataField());
		val dateObserved = DateUtils.stripMilliseconds(m.getDateObserved());
		c.setDateObserved(dateObserved);
		c.setDemographicNo(m.getDemographicId());
		c.setMeasuringInstrc(m.getMeasuringInstruction());
		c.setType(m.getType());

		List<Measurement> ms = dao.find(c);
		Assertions.assertEquals(1, ms.size());

		c = new SearchCriteria();
		ms = dao.find(c);
		Assertions.assertNotNull(ms);

		c = new SearchCriteria();
		c.setComments(m.getComments());
		ms = dao.find(c);
		Assertions.assertNotNull(ms);

		c = new SearchCriteria();
		c.setDataField(m.getDataField());
		ms = dao.find(c);
		Assertions.assertNotNull(ms);

		c = new SearchCriteria();
		c.setDateObserved(dateObserved);
		ms = dao.find(c);
		Assertions.assertNotNull(ms);

		c = new SearchCriteria();
		c.setDemographicNo(m.getDemographicId());
		ms = dao.find(c);
		Assertions.assertNotNull(ms);

		c = new SearchCriteria();
		c.setMeasuringInstrc(m.getMeasuringInstruction());
		ms = dao.find(c);
		Assertions.assertNotNull(ms);

		c = new SearchCriteria();
		c.setType(m.getType());
		ms = dao.find(c);
		Assertions.assertNotNull(ms);

		c = new SearchCriteria();
		c.setDataField(m.getDataField());
		c.setDateObserved(dateObserved);
		c.setMeasuringInstrc(m.getMeasuringInstruction());
		c.setType(m.getType());
		ms = dao.find(c);
		Assertions.assertNotNull(ms);

		c = new SearchCriteria();
		c.setComments(m.getComments());
		c.setDataField(m.getDataField());
		c.setDemographicNo(m.getDemographicId());
		c.setMeasuringInstrc(m.getMeasuringInstruction());
		ms = dao.find(c);
		Assertions.assertNotNull(ms);
	}

	protected Measurement populate() {
		return populate(999, DateUtils.stripMilliseconds(new Date()), "DTATAHEROVATA");
	}

	protected Measurement populate(int demographicId, Date dateObserved, String dataField) {
		Measurement m = new Measurement();
		m.setDemographicId(demographicId);
		m.setAppointmentNo(100);
		m.setComments("NUIOBLAHA");
		m.setDataField(dataField);
		m.setDateObserved(dateObserved);
		m.setMeasuringInstruction("MSRNIGINSRCTIONS");
		m.setProviderNo("PRVDRE");
		m.setType("TIPPITIP");
		dao.persist(m);
		return m;
	}

	@SneakyThrows
	private static Date getDateTimeFromString(String date) {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date);
	}

	@Test
	public void testFindById() {
		populate();
		populate();

		List<Measurement> m = dao.findByIdTypeAndInstruction(999, "TIPPITIP", "MSRNIGINSRCTIONS");
		Assertions.assertFalse(m.isEmpty());
	}

	@Test
	public void testFindByDemographicIdUpdatedAfterDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -1);

		Measurement m = new Measurement();
		m.setDemographicId(1);
		m.setAppointmentNo(100);
		m.setComments("NUIOBLAHA");
		m.setDataField("DTATAHEROVATA");
		m.setDateObserved(cal.getTime());
		m.setCreateDate(cal.getTime());
		m.setMeasuringInstruction("MSRNIGINSRCTIONS");
		m.setProviderNo("PRVDRE");
		m.setType("TIPPITIP");
		dao.persist(m);

		m = new Measurement();
		m.setDemographicId(1);
		m.setAppointmentNo(100);
		m.setComments("NUIOBLAHA");
		m.setDataField("DTATAHEROVATA");
		m.setDateObserved(new Date());
		m.setMeasuringInstruction("MSRNIGINSRCTIONS");
		m.setProviderNo("PRVDRE");
		m.setType("TIPPITIP");
		dao.persist(m);

		cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);

		Assertions.assertEquals(1, dao.findByDemographicIdUpdatedAfterDate(1, cal.getTime()).size());

		cal=new GregorianCalendar();
		cal.add(Calendar.DAY_OF_YEAR, -1);
		List<Measurement> results=dao.findByCreateDate(cal.getTime(), 99);
		Assertions.assertTrue(results.size()>0);

		cal.add(Calendar.DAY_OF_YEAR, 2);
		results=dao.findByCreateDate(cal.getTime(), 99);
		Assertions.assertEquals(0, results.size());

	}

	@Test
	public void testFindMeasurementsByDemographicIdAndLocationCode() {
		Assertions.assertNotNull(dao.findMeasurementsByDemographicIdAndLocationCode(100, "CDE"));
	}

	@Test
	public void testFindMeasurementsWithIdentifiersByDemographicIdAndLocationCode() {
		Assertions.assertNotNull(
				dao.findMeasurementsWithIdentifiersByDemographicIdAndLocationCode(100, "CDE"));
	}

	@Test
	public void testFindLabNumbers() {
		Assertions.assertNotNull(dao.findLabNumbers(100, "CDE"));
	}

	@Test
	public void testFindLastEntered() {
		dao.findLastEntered(100, "CDE");
	}

	@Test
	public void testFindMeasurementsAndProviders() {
		Assertions.assertNotNull(dao.findMeasurementsAndProviders(100));
	}

	@Test
	public void testFindMeasurementsAndProvidersByType() {
		Assertions.assertNotNull(dao.findMeasurementsAndProvidersByType("TYPE", 100));
	}

	@Test
	public void testFindMeasurementsAndProvidersByDemoAndType() {
		dao.findMeasurementsAndProvidersByDemoAndType(100, "TYPE");
	}

	@Test
	public void testFindByValue() {
		Assertions.assertNotNull(dao.findByValue("ZPA", "ZPA"));
	}

    @Test
    public void testFindObservationDatesByDemographicNoTypeAndMeasuringInstruction() {
	    Assertions.assertNotNull(
					dao.findObservationDatesByDemographicNoTypeAndMeasuringInstruction(100, "TYPE", "INSTR"));
    }

    @Test
    public void testFindByDemographicNoTypeAndDate() {
	    dao.findByDemographicNoTypeAndDate(100, "TUY", new Date());

    }

	@Nested
	class FindMeasurementsByDemographicIdAndIdentCode {

		@Test
		void givenNoData_whenCalled_thenEmpty() {
			val result = dao.findMeasurementsByDemographicIdAndIdentCode(
				7, "identCode1", null, null);
			Assertions.assertNotNull(result);
			Assertions.assertEquals(0, result.size());
		}

		@Test
		void givenData_whenCalled_thenFiltersCorrectly() {
			val item = populate(999, AT, "3.14");
			val other = populate(13, new Date(), "other");
			measurementMapFixture(IDENT_CODE);
			measurementMapFixture("IC2");
			measurementExtFixture(item, "name", "Sodium");
			measurementExtFixture(item, "identifier", IDENT_CODE);
			// act
			val result = dao.findMeasurementsByDemographicIdAndIdentCode(
				999, IDENT_CODE, null, null);
			// assert
			Assertions.assertNotNull(result);
			Assertions.assertEquals(2, result.size());
			Assertions.assertArrayEquals(
				new Object[]{ item.getId(), "3.14", AT, "name", "Sodium" },
				result.get(0)
			);
			Assertions.assertArrayEquals(
				new Object[]{ item.getId(), "3.14", AT, "identifier", IDENT_CODE },
				result.get(1)
			);
			// assert that other.id is not in the result ... assertJ would have been nice here
			for(val row: result) {
				Assertions.assertNotEquals(other.getId(), row[0]);
			}
		}

		@Test
		void givenIC1IsMappedToIC2_whenCalled_thenUsesMeasurementMap() {
			// set up a lab test with IC2 and search for loinc code to see mapping works
			val item = populate();
			measurementMapFixture(IDENT_CODE);
			measurementMapFixture("IC2");
			measurementExtFixture(item, "identifier", "IC2");
			// act
			val result = dao.findMeasurementsByDemographicIdAndIdentCode(
				999, IDENT_CODE, null, null);
			// assert
			Assertions.assertNotNull(result);
			Assertions.assertEquals(1, result.size());
		}

		@Test
		void givenData_whenCalled_thenFiltersFromDate() {
			val itemBefore = populate(999, BEFORE, "2.5");
			val item = populate(999, AT, "3.5");
			val itemAfter = populate(999, AFTER, "4.5");
			measurementMapFixture(IDENT_CODE);
			measurementExtFixture(itemBefore, "identifier", IDENT_CODE);
			measurementExtFixture(item, "identifier", IDENT_CODE);
			measurementExtFixture(itemAfter, "identifier", IDENT_CODE);
			// act
			val result = dao.findMeasurementsByDemographicIdAndIdentCode(
				999, IDENT_CODE, AT, null);
			// assert
			Assertions.assertNotNull(result);
			Assertions.assertEquals(2, result.size());
			Assertions.assertEquals(itemAfter.getId(), result.get(0)[0]);
			Assertions.assertEquals(item.getId(), result.get(1)[0]);
		}

		@Test
		void givenData_whenCalled_thenFiltersToDate() {
			val itemBefore = populate(999, BEFORE, "2.5");
			val item = populate(999, AT, "3.5");
			val itemAfter = populate(999, AFTER, "4.5");
			measurementMapFixture(IDENT_CODE);
			measurementExtFixture(itemBefore, "identifier", IDENT_CODE);
			measurementExtFixture(item, "identifier", IDENT_CODE);
			measurementExtFixture(itemAfter, "identifier", IDENT_CODE);
			// act
			val result = dao.findMeasurementsByDemographicIdAndIdentCode(
				999, IDENT_CODE, null, AT);
			// assert
			Assertions.assertNotNull(result);
			Assertions.assertEquals(2, result.size());
			Assertions.assertEquals(itemBefore.getId(), result.get(1)[0]);
			Assertions.assertEquals(item.getId(), result.get(0)[0]);
		}

		@Test
		void givenData_whenCalled_thenFiltersFromAndToDate() {
			val itemBefore = populate(999, BEFORE, "2.5");
			val item = populate(999, AT, "3.5");
			val itemAfter = populate(999, AFTER, "4.5");
			measurementMapFixture(IDENT_CODE);
			measurementExtFixture(itemBefore, "identifier", IDENT_CODE);
			measurementExtFixture(item, "identifier", IDENT_CODE);
			measurementExtFixture(itemAfter, "identifier", IDENT_CODE);
			// act
			val result = dao.findMeasurementsByDemographicIdAndIdentCode(
				999, IDENT_CODE, JUST_BEFORE, JUST_AFTER);
			// assert
			Assertions.assertNotNull(result);
			Assertions.assertEquals(1, result.size());
			Assertions.assertEquals(item.getId(), result.get(0)[0]);
		}

		@Test
		void givenData_whenCalled_thenSorts() {
			val itemBefore = populate(999, BEFORE, "2.5");
			val itemAfter = populate(999, AFTER, "4.5");
			val item = populate(999, AT, "3.5");
			measurementMapFixture(IDENT_CODE);
			measurementExtFixture(item, "name", "Sodium2");
			measurementExtFixture(itemAfter, "identifier", IDENT_CODE);
			measurementExtFixture(itemBefore, "identifier", IDENT_CODE);
			measurementExtFixture(item, "identifier", IDENT_CODE);
			measurementExtFixture(itemAfter, "name", "Sodium3");
			measurementExtFixture(itemBefore, "name", "Sodium1");
			// act
			val result = dao.findMeasurementsByDemographicIdAndIdentCode(
				999, IDENT_CODE, null, null);
			// assert
			Assertions.assertNotNull(result);
			Assertions.assertEquals(6, result.size());
			assertMeasurement(result.get(0), itemAfter.getId(), "identifier", IDENT_CODE);
			assertMeasurement(result.get(1), itemAfter.getId(), "name", "Sodium3");
			assertMeasurement(result.get(2), item.getId(), "name", "Sodium2");
			assertMeasurement(result.get(3), item.getId(), "identifier", IDENT_CODE);
			assertMeasurement(result.get(4), itemBefore.getId(), "identifier", IDENT_CODE);
			assertMeasurement(result.get(5), itemBefore.getId(), "name", "Sodium1");
			Assertions.assertTrue(((Date) result.get(0)[2]).compareTo((Date) result.get(1)[2]) >= 0);
			Assertions.assertTrue(((Date) result.get(1)[2]).compareTo((Date) result.get(2)[2]) >= 0);
			Assertions.assertTrue(((Date) result.get(2)[2]).compareTo((Date) result.get(3)[2]) >= 0);
			Assertions.assertTrue(((Date) result.get(3)[2]).compareTo((Date) result.get(4)[2]) >= 0);
			Assertions.assertTrue(((Date) result.get(4)[2]).compareTo((Date) result.get(5)[2]) >= 0);
		}

		private void assertMeasurement(
			final Object[] row, final Integer id, final String name, final String value
		) {
			Assertions.assertEquals(id, row[0]);
			Assertions.assertEquals(name, row[3]);
			Assertions.assertEquals(value, row[4]);
		}

		private void measurementExtFixture(
			final Measurement item, final String extKey, final String extValue
		) {
			val measurementsExt = new MeasurementsExt();
			measurementsExt.setMeasurementId(item.getId());
			measurementsExt.setKeyVal(extKey);
			measurementsExt.setVal(extValue);
			meDao.persist(measurementsExt);
		}

		private void measurementMapFixture(
			final String identCode
		) {
			val measurementMap = new MeasurementMap();
			measurementMap.setLoincCode("1234-5");
			measurementMap.setLabType("LT");
			measurementMap.setName("Sodium");
			measurementMap.setIdentCode(identCode);
			mmDao.addMeasurementMap(measurementMap);
		}

	}
}
