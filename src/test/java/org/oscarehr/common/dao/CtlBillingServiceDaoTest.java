/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import ca.oscarpro.test.TagConstants;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.CtlBillingService;
import org.oscarehr.util.SpringUtils;

public class CtlBillingServiceDaoTest extends DaoTestFixtures {

	protected CtlBillingServiceDao dao = SpringUtils.getBean(CtlBillingServiceDao.class);

	@BeforeEach
	public void before() throws Exception {
		SchemaUtils.restoreTable("ctl_billingservice");
	}

	@Test
	@Disabled // TODO: fix failing test case population
	public void testAllServiceTypes() {
		List<Object[]> serviceTypes = dao.getAllServiceTypes();
		Assertions.assertNotNull(serviceTypes);
		Assertions.assertFalse(serviceTypes.isEmpty());
	}

	@Tag(TagConstants.ON)
	@Test
	@Disabled // TODO: fix failing test case population
	public void testFindByServiceGroupAndServiceTypeOn() {
		List<CtlBillingService> services = dao.findByServiceGroupAndServiceType("Group2", null);
		Assertions.assertFalse(services.isEmpty());

		services = dao.findByServiceGroupAndServiceType("Group1", "MFP");
		Assertions.assertFalse(services.isEmpty());
	}

	@Tag(TagConstants.BC)
	@Test
	@Disabled // TODO: fix failing test case population
	public void testFindByServiceGroupAndServiceTypeBc() {
		List<CtlBillingService> services = dao.findByServiceGroupAndServiceType("Group2", null);
		Assertions.assertFalse(services.isEmpty());

		services = dao.findByServiceGroupAndServiceType("Group2", "GP");
		Assertions.assertFalse(services.isEmpty());
	}

    @Test
    public void testFindUniqueServiceTypesByCode() {
	    Assertions.assertNotNull(dao.findUniqueServiceTypesByCode("CODE"));
    }

    @Test
    public void testFindServiceTypes() {
	    Assertions.assertNotNull(dao.findServiceTypes());
    }

    @Test
    public void testFindServiceCodesByType() {
	    Assertions.assertNotNull(dao.findServiceCodesByType("SRV_TY"));
    }

    @Test
    public void testFindServiceTypesByStatus() {
	    Assertions.assertNotNull(dao.findServiceTypesByStatus("A"));
    }
}
