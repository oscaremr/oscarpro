/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
/**
 * @author Shazib
 */
package org.oscarehr.common.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.val;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.ProfessionalSpecialist;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

public class ProfessionalSpecialistDaoTest extends DaoTestFixtures {

	protected ProfessionalSpecialistDao dao = SpringUtils.getBean(ProfessionalSpecialistDao.class);

	@BeforeEach
	public void before() throws Exception {
		SchemaUtils.restoreTable(false, "professionalSpecialists");
	}

	@Test
	public void testFindAll() throws Exception {

		String fName1 = "FirstName1";
		String fName2 = "FirstName2";
		String fName3 = "FirstName3";

		String lName1 = "LastName1";
		String lName2 = "LastName2";
		String lName3 = "LastName3";

		val professionalSpecialist3 = mockSpecialistFullName(fName1, lName1);
		val professionalSpecialist1 = mockSpecialistFullName(fName2, lName2);
		val professionalSpecialist2 = mockSpecialistFullName(fName3, lName3);

		List<ProfessionalSpecialist> expectedResult = new ArrayList<ProfessionalSpecialist>(
        Arrays.asList(professionalSpecialist3, professionalSpecialist1, professionalSpecialist2));
		List<ProfessionalSpecialist> result = dao.findAll();

		Logger logger = MiscUtils.getLogger();

		if (result.size() != expectedResult.size()) {
			logger.warn("Array sizes do not match.");
			Assertions.fail("Array sizes do not match.");
		}
		for (int i = 0; i < expectedResult.size(); i++) {
			if (!expectedResult.get(i).getId().equals(result.get(i).getId())){
				logger.warn("Items  do not match.");
				Assertions.fail("Items  do not match.");
			}
		}
		Assertions.assertTrue(true);
	}

	@Test
	public void testFindByEDataUrlNotNull() throws Exception {

		String fName1 = "FirstName1";
		String fName2 = "FirstName2";
		String fName3 = "FirstName3";
		String fName4 = "FirstName4";

		String lName1 = "LastName1";
		String lName2 = "LastName2";
		String lName3 = "LastName3";
		String lName4 = "LastName4";

		String eDataUrl1 = "eData1";
		String eDataUrl2 = "eData2";
		String eDataUrl3 = "eData3";
		String eDataUrl4 = null;

		val professionalSpecialist1
				= mockSpecialistNameUrl(fName2, lName2, eDataUrl1);

		val professionalSpecialist2
				= mockSpecialistNameUrl(fName3, lName3, eDataUrl2);

		val professionalSpecialist3
				= mockSpecialistNameUrl(fName1, lName1, eDataUrl3);

		mockSpecialistNameUrl(fName4, lName4, eDataUrl4);

		List<ProfessionalSpecialist> expectedResult = new ArrayList<ProfessionalSpecialist>(Arrays.asList(professionalSpecialist3, professionalSpecialist1, professionalSpecialist2));
		List<ProfessionalSpecialist> result = dao.findByEDataUrlNotNull();

		Logger logger = MiscUtils.getLogger();

		if (result.size() != expectedResult.size()) {
			logger.warn("Array sizes do not match. Expected: " + expectedResult.size() + " Result: " + result.size());
			Assertions.fail("Array sizes do not match.");
		}
		for (int i = 0; i < expectedResult.size(); i++) {
			if (!expectedResult.get(i).getId().equals(result.get(i).getId())){
				logger.warn("Items  do not match.");
				Assertions.fail("Items  do not match.");
			}
		}
		Assertions.assertTrue(true);

	}

	@Test
	public void testFindByFullName() throws Exception {

		String fName1 = "FirstName1";
		String fName2 = "FirstName3";

		String lName1 = "LastName1";
		String lName2 = "LastName3";

		ProfessionalSpecialist professionalSpecialist1 = mockSpecialistFullName(fName1, lName1);

		mockSpecialistFullName(fName2, lName2);

		ProfessionalSpecialist professionalSpecialist3 = mockSpecialistFullName(fName1, lName1);

		List<ProfessionalSpecialist> expectedResult = new ArrayList<ProfessionalSpecialist>(Arrays.asList(professionalSpecialist1, professionalSpecialist3));
		List<ProfessionalSpecialist> result = dao.findByFullName(lName1, fName1);

		Logger logger = MiscUtils.getLogger();

		if (result.size() != expectedResult.size()) {
			logger.warn("Array sizes do not match.");
			Assertions.fail("Array sizes do not match.");
		}
		for (int i = 0; i < expectedResult.size(); i++) {
			if (!expectedResult.get(i).getId().equals(result.get(i).getId())){
				logger.warn("Items  do not match.");
				Assertions.fail("Items  do not match.");
			}
		}
		Assertions.assertTrue(true);
	}

	@Test
	public void testFindByLastName() throws Exception {

		String fName1 = "FirstName1";
		String fName2 = "FirstName3";

		String lName1 = "LastName1";
		String lName2 = "LastName3";

		val professionalSpecialist1 = mockSpecialistFullName(fName1, lName1);

		mockSpecialistFullName(fName2, lName2);

		val professionalSpecialist3 = mockSpecialistFullName(fName1, lName1);

		List<ProfessionalSpecialist> expectedResult = new ArrayList<ProfessionalSpecialist>(Arrays.asList(professionalSpecialist1, professionalSpecialist3));
		List<ProfessionalSpecialist> result = dao.findByLastName(lName1);

		Logger logger = MiscUtils.getLogger();

		if (result.size() != expectedResult.size()) {
			logger.warn("Array sizes do not match.");
			Assertions.fail("Array sizes do not match.");
		}
		for (int i = 0; i < expectedResult.size(); i++) {
			if (!expectedResult.get(i).getId().equals(result.get(i).getId())){
				logger.warn("Items  do not match.");
				Assertions.fail("Items  do not match.");
			}
		}
		Assertions.assertTrue(true);
	}

	@Test
	public void testFindBySpecialty() throws Exception {

		String lName1 = "LastName1";
		String lName2 = "LastName2";
		String lName3 = "LastName3";

		String specialty1 = "alpha";
		String specialty2 = "bravo";

		val professionalSpecialist1 = mockSpecialistLastNameType(lName2, specialty1);
		val professionalSpecialist3 = mockSpecialistLastNameType(lName1, specialty1);
		mockSpecialistLastNameType(lName3, specialty2);

		List<ProfessionalSpecialist> expectedResult = new ArrayList<ProfessionalSpecialist>(Arrays.asList(professionalSpecialist3, professionalSpecialist1));
		List<ProfessionalSpecialist> result = dao.findBySpecialty(specialty1);

		Logger logger = MiscUtils.getLogger();

		if (result.size() != expectedResult.size()) {
			logger.warn("Array sizes do not match.");
			Assertions.fail("Array sizes do not match.");
		}
		for (int i = 0; i < expectedResult.size(); i++) {
			if (!expectedResult.get(i).getId().equals(result.get(i).getId())){
				logger.warn("Items  do not match.");
				Assertions.fail("Items  do not match.");
			}
		}
		Assertions.assertTrue(true);
	}

	@Test
	public void testFindByReferralNo() throws Exception {

		String lName1 = "LastName1";
		String lName2 = "LastName2";
		String lName3 = "LastName3";

		String referralNo1 = "alpha";
		String referralNo2 = "bravo";

		val professionalSpecialist1= mockSpecialistLastNameReferral(lName2, referralNo1);
		val professionalSpecialist3= mockSpecialistLastNameReferral(lName1, referralNo1);
		mockSpecialistLastNameReferral(lName3, referralNo2);

		val expectedResult = new ArrayList<>(
				Arrays.asList(professionalSpecialist3, professionalSpecialist1));
		val result = dao.findByReferralNo(referralNo1);

		val logger = MiscUtils.getLogger();

		if (result.size() != expectedResult.size()) {
			logger.warn("Array sizes do not match.");
			Assertions.fail("Array sizes do not match.");
		}
		for (int i = 0; i < expectedResult.size(); i++) {
			if (!expectedResult.get(i).getId().equals(result.get(i).getId())){
				logger.warn("Items  do not match.");
				Assertions.fail("Items  do not match.");
			}
		}
		Assertions.assertTrue(true);
	}

	@Test
	public void testGetByReferralNo() throws Exception {

		String lName1 = "LastName1";
		String lName2 = "LastName2";
		String lName3 = "LastName3";

		String referralNo1 = "alpha";
		String referralNo2 = "bravo";
		String referralNo3 = "charlie";

		val professionalSpecialist1= mockSpecialistLastNameReferral(lName2, referralNo1);
		mockSpecialistLastNameType(lName3, referralNo2);
		mockSpecialistLastNameType(lName1, referralNo3);

		assertEquals(professionalSpecialist1.getReferralNo(), dao.getByReferralNo(referralNo1).getReferralNo());
	}

	private ProfessionalSpecialist mockSpecialistLastNameType(String lName1, String referralNo3) throws Exception {
		val specialist = new ProfessionalSpecialist();
		EntityDataGenerator.generateTestDataForModelClass(specialist);
		specialist.setSpecialtyType(referralNo3);
		specialist.setLastName(lName1);
		specialist.setDeleted(false);
		dao.persist(specialist);
		return specialist;
	}

	private ProfessionalSpecialist mockSpecialistFullName(String fName1, String lName1)
			throws Exception {
		val specialist = new ProfessionalSpecialist();
		EntityDataGenerator.generateTestDataForModelClass(specialist);
		specialist.setFirstName(fName1);
		specialist.setLastName(lName1);
		specialist.setDeleted(false);
		dao.persist(specialist);
		return specialist;
	}

	private ProfessionalSpecialist mockSpecialistNameUrl(String fName2, String lName2,
			String eDataUrl1) throws Exception {
		val specialist = new ProfessionalSpecialist();
		EntityDataGenerator.generateTestDataForModelClass(specialist);
		specialist.setFirstName(fName2);
		specialist.setLastName(lName2);
		specialist.setEdataUrl(eDataUrl1);
		specialist.setDeleted(false);
		dao.persist(specialist);
		return specialist;
	}

	private ProfessionalSpecialist mockSpecialistLastNameReferral(String lName3, String referralNo2) throws Exception {
		val specialist = new ProfessionalSpecialist();
		EntityDataGenerator.generateTestDataForModelClass(specialist);
		specialist.setReferralNo(referralNo2);
		specialist.setLastName(lName3);
		specialist.setDeleted(false);
		dao.persist(specialist);
		return specialist;
	}
}