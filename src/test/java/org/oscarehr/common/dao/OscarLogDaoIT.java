/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Date;
import java.util.List;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.OscarLog;
import org.oscarehr.util.SpringUtils;
import org.springframework.test.util.ReflectionTestUtils;

public class OscarLogDaoIT extends DaoTestFixtures {

  private final OscarLogDao oscarLogDao = SpringUtils.getBean(OscarLogDao.class);

  private static final String PROVIDER_NO = "1";
  private static final Integer DEMOGRAPHIC_ID_A = 10000;
  private static final Integer DEMOGRAPHIC_ID_B = 20000;
  private static final Integer DEMOGRAPHIC_ID_C = 30000;

  private static final int OFFSET = 0;
  private static final int LIMIT = 10;

  @BeforeEach
  public void before() throws Exception {
    SchemaUtils.restoreTable("log");
  }

  @Test
  public void givenNoLogEntries_whenGetRecentDemographicsAccessedByProvider_thenGetEmpty() {
    List<Integer> results =
        oscarLogDao.getRecentDemographicsAccessedByProvider(PROVIDER_NO, OFFSET, LIMIT);
    assertTrue(results.isEmpty());
  }

  @Test
  public void givenRecentDemographicEntries_whenGetRecentDemographicsAccessedByProvider_thenGetResults() {
    // setup
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_A));
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_B));
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_C));
    oscarLogDao.persist(buildOscarLog("00000", DEMOGRAPHIC_ID_C));

    // execute
    List<Integer> results =
        oscarLogDao.getRecentDemographicsAccessedByProvider(PROVIDER_NO, OFFSET, LIMIT);

    // verify
    assertEquals(3, results.size());
  }

  @Test
  public void givenOlderRecentDemographicEntries_whenGetRecentDemographicsAccessedByProvider_thenExcludeOld() {
    // setup
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_A));
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_B));
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_C, "2000-01-01"));

    // execute
    List<Integer> results =
        oscarLogDao.getRecentDemographicsAccessedByProvider(PROVIDER_NO, OFFSET, LIMIT);

    // verify
    assertEquals(2, results.size());
    assertEquals(DEMOGRAPHIC_ID_A, results.get(0));
    assertEquals(DEMOGRAPHIC_ID_B, results.get(1));
  }

  @Test
  public void givenUnorderedEntries_whenGetRecentDemographicsAccessedByProvider_thenNewestFirstOrdering() {
    // setup
    oscarLogDao.persist(
        buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_A, DateUtils.addHours(new Date(), -12)));
    oscarLogDao.persist(
        buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_B, DateUtils.addHours(new Date(), -6)));
    oscarLogDao.persist(
        buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_C, DateUtils.addHours(new Date(), -1)));

    // execute
    List<Integer> results =
        oscarLogDao.getRecentDemographicsAccessedByProvider(PROVIDER_NO, OFFSET, LIMIT);

    // verify
    assertEquals(3, results.size());
    assertEquals(DEMOGRAPHIC_ID_C, results.get(0));
    assertEquals(DEMOGRAPHIC_ID_B, results.get(1));
    assertEquals(DEMOGRAPHIC_ID_A, results.get(2));
  }

  @Test
  public void givenEntriesWithSameDemographic_whenGetRecentDemographicsAccessedByProvider_thenDistinctResults() {
    // setup
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_A));
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_C));
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_A));
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_B));
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_A));
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_C));

    // execute
    List<Integer> results =
        oscarLogDao.getRecentDemographicsAccessedByProvider(PROVIDER_NO, OFFSET, LIMIT);

    // verify
    assertEquals(3, results.size());
  }

  @Test
  public void givenManyEntries_whenGetRecentDemographicsAccessedByProvider_thenLimitObserved() {
    // setup
    for (int i = 0; i < LIMIT; i++) {
      oscarLogDao.persist(buildOscarLog(PROVIDER_NO, (i + 1)));
    }
    for (int i = 0; i < LIMIT; i++) {
      oscarLogDao.persist(buildOscarLog(PROVIDER_NO, (LIMIT + i + 1)));
    }

    // execute
    List<Integer> results =
        oscarLogDao.getRecentDemographicsAccessedByProvider(PROVIDER_NO, OFFSET, LIMIT);

    // verify
    assertEquals(LIMIT, results.size());
  }

  @Test
  public void givenManyEntries_whenGetRecentDemographicsAccessedByProvider_thenOffsetObserved() {
    // setup
    final int offset = LIMIT; // variable for readability
    for (int i = 0; i < offset; i++) {
      oscarLogDao.persist(buildOscarLog(PROVIDER_NO, (i + 1)));
    }
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_B));
    oscarLogDao.persist(buildOscarLog(PROVIDER_NO, DEMOGRAPHIC_ID_C));

    // execute
    List<Integer> results =
        oscarLogDao.getRecentDemographicsAccessedByProvider(PROVIDER_NO, offset, LIMIT);

    // verify
    assertEquals(2, results.size());
  }

  private OscarLog buildOscarLog(String providerNo, Integer demographicNo, String created) {
    return buildOscarLog(providerNo, demographicNo, toDate(created));
  }

  private OscarLog buildOscarLog(String providerNo, Integer demographicNo, Date created) {
    OscarLog log = buildOscarLog(providerNo, demographicNo);
    ReflectionTestUtils.setField(log, "created", created);
    return log;
  }

  private OscarLog buildOscarLog(String providerNo, Integer demographicNo) {
    OscarLog log = new OscarLog();
    log.setProviderNo(providerNo);
    log.setDemographicId(demographicNo);
    return log;
  }

  private Date toDate(String date) {
    try {
      return DateUtils.parseDate(date, "yyyy-MM-dd");
    } catch (Exception e) {
      throw new IllegalArgumentException("Invalid format: " + date, e);
    }
  }
}
