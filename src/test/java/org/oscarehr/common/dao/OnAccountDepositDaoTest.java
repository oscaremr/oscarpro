/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import lombok.val;
import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.OnAccountDeposit;
import org.oscarehr.util.SpringUtils;

public class OnAccountDepositDaoTest extends DaoTestFixtures {

    protected OnAccountDepositDao dao = SpringUtils.getBean(OnAccountDepositDao.class);

    private static final Integer DEMOGRAPHIC_1_NUMBER = 1;
    private static final Double DEMOGRAPHIC_1_DEPOSIT_1 = 1.3d;
    private static final Double DEMOGRAPHIC_1_DEPOSIT_2 = 13.1d;
    private static final Double DEMOGRAPHIC_1_SUM_DEPOSIT = 14.4d;
    private static final Double DEMOGRAPHIC_1_BALANCE_1 = 12.5d;
    private static final Double DEMOGRAPHIC_1_BALANCE_2 = 17.5d;
    private static final Double DEMOGRAPHIC_1_SUM_BALANCE = 30d;
    private static final Integer DEMOGRAPHIC_2_NUMBER = 2;
    private static final Double DEMOGRAPHIC_2_DEPOSIT_1 = 1d;
    private static final Double DEMOGRAPHIC_2_DEPOSIT_2 = 13d;
    private static final Double DEMOGRAPHIC_2_SUM_DEPOSIT = 14d;
    private static final Double DEMOGRAPHIC_2_BALANCE_1 = 15d;
    private static final Double DEMOGRAPHIC_2_BALANCE_2 = 15d;
    private static final Double DEMOGRAPHIC_2_SUM_BALANCE = 30d;

    public OnAccountDepositDaoTest() {
    }

    @BeforeEach
    public void before() throws Exception {
        SchemaUtils.restoreTable("onaccountdeposits");
    }

    @Test
    public void givenNewOnAccountDeposit_whenDaoPersist_thenEntityIdAssigned() throws Exception {
        OnAccountDeposit entity = new OnAccountDeposit();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        dao.persist(entity);
        Assertions.assertNotNull(entity.getId());
    }

    @Test
    public void givenOnAccountDeposit_whenFindByDemographicNumber_thenSuccess() throws Exception {
        createOnAccountDepositEntries();
        var results = dao.findByDemographicNumber(DEMOGRAPHIC_1_NUMBER);
        Assertions.assertEquals(2, results.size());
        for (OnAccountDeposit result : results) {
            Assertions.assertEquals(DEMOGRAPHIC_1_NUMBER, result.getDemographicNo());
        }
    }

    @Test
    public void givenOnAccountDeposit_whenGetTotalDepositsByDemographicNumber_thenReturnDepositSum() throws Exception {
        createOnAccountDepositEntries();
        var result = dao.getTotalDepositsByDemographicNumber(DEMOGRAPHIC_2_NUMBER);
        Assertions.assertEquals(DEMOGRAPHIC_2_SUM_DEPOSIT, result);


        result = dao.getTotalDepositsByDemographicNumber(DEMOGRAPHIC_1_NUMBER);
        Assertions.assertEquals(DEMOGRAPHIC_1_SUM_DEPOSIT, result);
    }

    @Test
    public void givenOnAccountDeposit_whenGetTotalBalanceByDemographicNumber_thenReturnBalanceSum() throws Exception {
        createOnAccountDepositEntries();
        var result = dao.getTotalBalanceByDemographicNumber(DEMOGRAPHIC_2_NUMBER);
        Assertions.assertEquals(DEMOGRAPHIC_2_SUM_BALANCE, result);


        result = dao.getTotalBalanceByDemographicNumber(DEMOGRAPHIC_1_NUMBER);
        Assertions.assertEquals(DEMOGRAPHIC_1_SUM_BALANCE, result);
    }

    @Test
    public void givenInvoiceNumber_whenVoidDepositByInvoiceNumber_thenDepositStatusVoidAndAmountsZero() throws Exception {
        var entity = new OnAccountDeposit();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_1_NUMBER);
        entity.setBalance(DEMOGRAPHIC_1_BALANCE_1);
        entity.setTotalDeposits(DEMOGRAPHIC_1_DEPOSIT_1);
        entity.setTotalServices(100d);
        entity.setDepositStatus("");
        dao.persist(entity);
        dao.setDepositVoidByInvoiceNumber(entity.getId());
        val result = dao.find(entity.getId());
        Assertions.assertEquals("Void", result.getDepositStatus());
        Assertions.assertEquals(0, result.getTotalDeposits());
        Assertions.assertEquals(0, result.getTotalServices());
        Assertions.assertEquals(0, result.getBalance());
    }

    private void createOnAccountDepositEntries() throws Exception {
        var entity = new OnAccountDeposit();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_1_NUMBER);
        entity.setBalance(DEMOGRAPHIC_1_BALANCE_1);
        entity.setTotalDeposits(DEMOGRAPHIC_1_DEPOSIT_1);
        dao.persist(entity);

        entity = new OnAccountDeposit();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_1_NUMBER);
        entity.setBalance(DEMOGRAPHIC_1_BALANCE_2);
        entity.setTotalDeposits(DEMOGRAPHIC_1_DEPOSIT_2);
        dao.persist(entity);

        entity = new OnAccountDeposit();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_2_NUMBER);
        entity.setBalance(DEMOGRAPHIC_2_BALANCE_1);
        entity.setTotalDeposits(DEMOGRAPHIC_2_DEPOSIT_1);
        dao.persist(entity);

        entity = new OnAccountDeposit();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_2_NUMBER);
        entity.setBalance(DEMOGRAPHIC_2_BALANCE_2);
        entity.setTotalDeposits(DEMOGRAPHIC_2_DEPOSIT_2);
        dao.persist(entity);
    }

}