/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.OnAccountDepositService;
import org.oscarehr.util.SpringUtils;

public class OnAccountDepositServiceDaoTest extends DaoTestFixtures {

    protected OnAccountDepositServiceDao dao = SpringUtils.getBean(OnAccountDepositServiceDao.class);


    private static final Integer INVOICE_NO_1 = 1;
    private static final Integer INVOICE_NO_1_COUNT = 2;
    private static final Integer INVOICE_NO_2 = 2;

    public OnAccountDepositServiceDaoTest() {
    }

    @BeforeEach
    public void before() throws Exception {
        SchemaUtils.restoreTable("onaccountdepositservices");
    }

    @Test
    public void givenNewOnAccountDepositService_whenDaoPersist_thenEntityIdAssigned() throws Exception {
        OnAccountDepositService entity = new OnAccountDepositService();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        dao.persist(entity);
        Assertions.assertNotNull(entity.getId());
    }

    @Test
    public void givenOnAccountDepositService_whenFindByInvoiceNumber_thenSuccess() throws Exception {
        createOnAccountDepositServiceEntries();
        var results = dao.findByInvoiceNo(INVOICE_NO_1);
        Assertions.assertEquals(INVOICE_NO_1_COUNT, results.size());
        for (OnAccountDepositService result : results) {
            Assertions.assertEquals(result.getInvoiceNo(), INVOICE_NO_1);
        }
    }

    private void createOnAccountDepositServiceEntries() throws Exception {
        var entity = new OnAccountDepositService();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNo(INVOICE_NO_1);
        dao.persist(entity);
        entity = new OnAccountDepositService();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNo(INVOICE_NO_1);
        dao.persist(entity);
        entity = new OnAccountDepositService();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNo(INVOICE_NO_2);
        dao.persist(entity);
    }
}