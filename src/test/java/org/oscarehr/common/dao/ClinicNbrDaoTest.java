/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.ClinicNbr;
import org.oscarehr.util.SpringUtils;

public class ClinicNbrDaoTest extends DaoTestFixtures {

	protected ClinicNbrDao dao = SpringUtils.getBean(ClinicNbrDao.class);

	@BeforeAll
	public static void setUp() throws Exception {
		SchemaUtils.restoreTable("clinic_nbr");
	}

	@Test
	/**
	 * Ensures that the removeEntry() method deletes records
	 * by setting the status to 'D'
	 * @throws Exception
	 */
	public void testRemoveEntry() throws Exception {
		ClinicNbr nbr1 = new ClinicNbr();
		EntityDataGenerator.generateTestDataForModelClass(nbr1);
		nbr1.setNbrStatus("A");
		dao.persist(nbr1);

		dao.removeEntry(1);
		nbr1 = dao.find(1);
		assertEquals("D", nbr1.getNbrStatus());
	}

	@Test
	/**
	 * Ensures that the addEntry() method persists new
	 * records to the table given the value and string.
	 * @throws Exception
	 */
	public void testAddEntry() throws Exception {
		String nbrValue = "A";
		String nbrString ="RMA";

		ClinicNbr nbr1 = new ClinicNbr();
		EntityDataGenerator.generateTestDataForModelClass(nbr1);
		nbr1.setNbrString(nbrString);
		nbr1.setNbrValue(nbrValue);

		dao.addEntry(nbrValue, nbrString);

		assertEquals("RMA", nbr1.getNbrString());
		assertEquals("A", nbr1.getNbrValue());
	}
}
