/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.util.SpringUtils;

public class DemographicExtDaoTest extends DaoTestFixtures {

  protected DemographicExtDao demographicExtDao = SpringUtils.getBean(DemographicExtDao.class);

  @BeforeEach
  public void before() throws Exception {
    beforeDemographic();
  }

  @Test
  public void testCreate() throws Exception {
    DemographicExt entity = new DemographicExt();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(1);
    demographicExtDao.saveDemographicExt(entity, true);
    DemographicExt foundEntity = demographicExtDao.getDemographicExt(entity.getId());

    Assertions.assertEquals(entity.getKey(), foundEntity.getKey());
    Assertions.assertEquals(entity.getValue(), foundEntity.getValue());
  }

  // Fail
  @Test
  public void testGetDemographicExt() throws Exception {
    DemographicExt entity = new DemographicExt();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setDemographicNo(1);
    demographicExtDao.saveDemographicExt(entity, true);
    DemographicExt foundEntity = demographicExtDao.getDemographicExt(entity.getId());

    Assertions.assertEquals(entity.getKey(), foundEntity.getKey());
    Assertions.assertEquals(entity.getValue(), foundEntity.getValue());
  }

  @Test
  public void getDemographicExtByDemographicNo() throws Exception {
    DemographicExt item1 = new DemographicExt();
    EntityDataGenerator.generateTestDataForModelClass(item1);
    item1.setDemographicNo(20);
    DemographicExt item2 = new DemographicExt();
    EntityDataGenerator.generateTestDataForModelClass(item2);
    item2.setDemographicNo(20);
    DemographicExt item3 = new DemographicExt();
    EntityDataGenerator.generateTestDataForModelClass(item3);
    item3.setDemographicNo(20);
    List<DemographicExt> found = demographicExtDao.getDemographicExtByDemographicNo(20);

    Assertions.assertEquals(0, found.size());

    demographicExtDao.saveDemographicExt(item1, true);
    demographicExtDao.saveDemographicExt(item2, true);
    demographicExtDao.saveDemographicExt(item3, true);
    found = demographicExtDao.getDemographicExtByDemographicNo(20);

    Assertions.assertEquals(3, found.size());
  }

  @Test
  public void testGetDemographicExtWithKeyAndDemographicNo() throws Exception {
    DemographicExt entity = new DemographicExt();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setKey("Test");
    entity.setDemographicNo(20);
    demographicExtDao.saveDemographicExt(entity, true);
    DemographicExt foundEntity = demographicExtDao.getDemographicExt(20, "Test");

    Assertions.assertEquals(entity.getKey(), foundEntity.getKey());
    Assertions.assertEquals(entity.getValue(), foundEntity.getValue());
  }

  @Test
  public void testGetLatestDemographic() throws Exception {
    DemographicExt entity = new DemographicExt();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setKey("Test");
    entity.setDemographicNo(20);
    entity.setDateCreated(new Date());
    demographicExtDao.saveDemographicExt(entity, true);

    DemographicExt newerEntity = new DemographicExt();
    EntityDataGenerator.generateTestDataForModelClass(newerEntity);
    newerEntity.setKey("Test");
    newerEntity.setDemographicNo(20);
    newerEntity.setDateCreated(new Date());
    demographicExtDao.saveDemographicExt(newerEntity, true);
    DemographicExt foundEntity = demographicExtDao.getLatestDemographicExt(20, "Test");

    Assertions.assertEquals(newerEntity.getKey(), foundEntity.getKey());
    Assertions.assertEquals(newerEntity.getValue(), foundEntity.getValue());
  }

  @Test
  public void testSaveDemographicExt() {
    demographicExtDao.saveDemographicExt(20, "20", "Test", "TestVal", true);
    DemographicExt result = demographicExtDao.getDemographicExt(20, "Test");
    assert ("TestVal".equals(result.getValue()));
  }
}
