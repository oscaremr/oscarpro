/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import lombok.val;
import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.OnAccountService;
import org.oscarehr.util.SpringUtils;

public class OnAccountServiceDaoTest extends DaoTestFixtures {

    protected OnAccountServiceDao dao = SpringUtils.getBean(OnAccountServiceDao.class);


    private static final Integer INVOICE_NO_1 = 1;
    private static final Integer INVOICE_NO_1_COUNT = 2;
    private static final Integer INVOICE_NO_2 = 2;

    public OnAccountServiceDaoTest() {
    }

    @BeforeEach
    public void before() throws Exception {
        SchemaUtils.restoreTable("onaccountservices");
    }

    @Test
    public void givenNewOnAccountService_whenDaoPersist_thenEntityIdAssigned() throws Exception {
        OnAccountService entity = new OnAccountService();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        dao.persist(entity);
        Assertions.assertNotNull(entity.getId());
    }

    @Test
    public void givenOnAccountService_whenFindByInvoiceNumber_thenSuccess() throws Exception {
        createOnAccountServiceEntries();
        var results = dao.findByInvoiceNo(INVOICE_NO_1);
        Assertions.assertEquals(INVOICE_NO_1_COUNT, results.size());
        for (OnAccountService result : results) {
            Assertions.assertEquals(result.getInvoiceNo(), INVOICE_NO_1);
        }
    }

    @Test
    public void givenOnAccountService_whenSetBillVoidByInvoiceNumber_thenSuccess() throws Exception {
        var entity = new OnAccountService();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNo(INVOICE_NO_1);
        entity.setStatus("Pending");
        dao.persist(entity);
        entity = new OnAccountService();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNo(INVOICE_NO_1);
        entity.setStatus("Pending");
        dao.persist(entity);
        dao.setBillVoidByInvoiceNumber(INVOICE_NO_1);
        val results = dao.findByInvoiceNo(INVOICE_NO_1);
        for (OnAccountService result : results) {
            Assertions.assertEquals(result.getStatus(), "Void");
        }
    }

    private void createOnAccountServiceEntries() throws Exception {
        var entity = new OnAccountService();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNo(INVOICE_NO_1);
        dao.persist(entity);
        entity = new OnAccountService();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNo(INVOICE_NO_1);
        dao.persist(entity);
        entity = new OnAccountService();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNo(INVOICE_NO_2);
        dao.persist(entity);
    }
}