/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import java.util.Arrays;
import lombok.val;
import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.OnAccountPayment;
import org.oscarehr.util.SpringUtils;

public class OnAccountPaymentDaoTest extends DaoTestFixtures {

    protected OnAccountPaymentsDao dao = SpringUtils.getBean(OnAccountPaymentsDao.class);


    private static final Integer INVOICE_NO_1 = 1;
    private static final Integer INVOICE_NO_1_COUNT = 2;
    private static final Integer INVOICE_NO_2 = 2;

    public OnAccountPaymentDaoTest() {
    }

    @BeforeEach
    public void before() throws Exception {
        SchemaUtils.restoreTable("onaccountpayments");
    }

    @Test
    public void givenNewOnAccountPayments_whenDaoPersist_thenEntityIdAssigned() throws Exception {
        OnAccountPayment entity = new OnAccountPayment();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        dao.persist(entity);
        Assertions.assertNotNull(entity.getInvoiceNumber());
    }

    @Test
    public void givenOnAccountPayments_whenFindByInvoiceNumber_thenSuccess() throws Exception {
        createOnAccountPaymentsEntries();
        var results = dao.findByInvoiceNo(INVOICE_NO_1);
        Assertions.assertEquals(INVOICE_NO_1_COUNT, results.size());
        for (OnAccountPayment result : results) {
            Assertions.assertEquals(result.getInvoiceNumber(), INVOICE_NO_1);
        }
    }

    @Test
    public void givenOnAccountPayments_whenUpdateStatusByInvoiceNumber_thenSuccess() throws Exception {
        createOnAccountPaymentsEntries();
        dao.updateStatusByInvoiceNumber(INVOICE_NO_1, "Void");
        val results = dao.findByInvoiceNo(INVOICE_NO_1);
        Assertions.assertEquals(INVOICE_NO_1_COUNT, results.size());
        for (OnAccountPayment result : results) {
            Assertions.assertEquals(result.getStatus(), "Void");
        }
    }

    @Test
    public void givenOnAccountPayments_whenUpdateStatusByInvoiceNumberAndStatusIn_thenSuccess() throws Exception {
        var entity = new OnAccountPayment();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNumber(INVOICE_NO_1);
        entity.setStatus("deleteSave");
        dao.persist(entity);
        entity = new OnAccountPayment();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNumber(INVOICE_NO_1);
        entity.setStatus("deletetemp");
        dao.persist(entity);
        entity = new OnAccountPayment();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNumber(INVOICE_NO_1);
        entity.setStatus("Void");
        dao.persist(entity);
        entity = new OnAccountPayment();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNumber(INVOICE_NO_2);
        entity.setStatus("deletetemp");
        dao.persist(entity);
        dao.updateStatusByInvoiceNumberAndStatusIn(INVOICE_NO_1,
                Arrays.asList("deleteSave", "deletetemp"), "Void");
        val results = dao.findByInvoiceNo(INVOICE_NO_1);
        Assertions.assertEquals(3, results.size());
        for (OnAccountPayment result : results) {
            Assertions.assertEquals(result.getStatus(), "Void");
        }
    }

    private void createOnAccountPaymentsEntries() throws Exception {
        var entity = new OnAccountPayment();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNumber(INVOICE_NO_1);
        dao.persist(entity);
        entity = new OnAccountPayment();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNumber(INVOICE_NO_1);
        dao.persist(entity);
        entity = new OnAccountPayment();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setInvoiceNumber(INVOICE_NO_2);
        dao.persist(entity);
    }
}