/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License. This program is free
 * software; you can redistribute it and/or modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either version 2 of the License, or (at
 * your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * <p>
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 * <p>
 * This software was written for the Department of Family Medicine McMaster University Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import lombok.val;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicArchive;
import org.oscarehr.common.model.DemographicGender;
import org.oscarehr.common.model.DemographicPronoun;
import org.oscarehr.util.MiscUtils;
import org.oscarehr.util.SpringUtils;

public class DemographicArchiveDaoTest extends DaoTestFixtures {

  protected DemographicArchiveDao dao = SpringUtils.getBean(DemographicArchiveDao.class);

  @BeforeEach
  public void before() throws Exception {
    beforeDemographic();
  }

  @Test
  public void testCreate() throws Exception {
    DemographicArchive entity = new DemographicArchive();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    dao.persist(entity);

    Assertions.assertNotNull(entity.getId());
  }

  @Test
  public void testFindByDemographicNo() throws Exception {
    int demoNo1 = 101;
    int demoNo2 = 202;

    DemographicArchive demographicArchive1 = new DemographicArchive();
    EntityDataGenerator.generateTestDataForModelClass(demographicArchive1);
    demographicArchive1.setDemographicNo(demoNo1);
    dao.persist(demographicArchive1);

    DemographicArchive demographicArchive2 = new DemographicArchive();
    EntityDataGenerator.generateTestDataForModelClass(demographicArchive2);
    demographicArchive2.setDemographicNo(demoNo1);
    dao.persist(demographicArchive2);

    DemographicArchive demographicArchive3 = new DemographicArchive();
    EntityDataGenerator.generateTestDataForModelClass(demographicArchive3);
    demographicArchive3.setDemographicNo(demoNo2);
    dao.persist(demographicArchive3);

    DemographicArchive demographicArchive4 = new DemographicArchive();
    EntityDataGenerator.generateTestDataForModelClass(demographicArchive4);
    demographicArchive4.setDemographicNo(demoNo1);
    dao.persist(demographicArchive4);

    List<DemographicArchive> expectedResult = new ArrayList<DemographicArchive>(
        Arrays.asList(demographicArchive1, demographicArchive2, demographicArchive4));
    List<DemographicArchive> result = dao.findByDemographicNo(demoNo1);

    Logger logger = MiscUtils.getLogger();

    if (result.size() != expectedResult.size()) {
      logger.warn("Array sizes do not match.");
      Assertions.fail("Array sizes do not match.");
    }
    for (int i = 0; i < expectedResult.size(); i++) {
      if (!expectedResult.get(i).equals(result.get(i))) {
        logger.warn("Items  do not match.");
        Assertions.fail("Items  do not match.");
      }
    }
    Assertions.assertTrue(true);
  }

  @Test
  public void testFindRosterStatusHistoryByDemographicNo() throws Exception {
    int demoNo1 = 101;
    int demoNo2 = 202;

    String rosterStatus1 = "alpha";
    String rosterStatus2 = "bravo";
    String rosterStatus3 = "charlie";

    DemographicArchive demographicArchive1 = new DemographicArchive();
    EntityDataGenerator.generateTestDataForModelClass(demographicArchive1);
    demographicArchive1.setDemographicNo(demoNo1);
    demographicArchive1.setRosterStatus(rosterStatus1);
    dao.persist(demographicArchive1);

    DemographicArchive demographicArchive2 = new DemographicArchive();
    EntityDataGenerator.generateTestDataForModelClass(demographicArchive2);
    demographicArchive2.setDemographicNo(demoNo1);
    demographicArchive2.setRosterStatus(rosterStatus2);
    dao.persist(demographicArchive2);

    DemographicArchive demographicArchive3 = new DemographicArchive();
    EntityDataGenerator.generateTestDataForModelClass(demographicArchive3);
    demographicArchive3.setDemographicNo(demoNo2);
    demographicArchive3.setRosterStatus(rosterStatus3);
    dao.persist(demographicArchive3);

    DemographicArchive demographicArchive4 = new DemographicArchive();
    EntityDataGenerator.generateTestDataForModelClass(demographicArchive4);
    demographicArchive4.setDemographicNo(demoNo1);
    demographicArchive4.setRosterStatus(rosterStatus3);
    dao.persist(demographicArchive4);

    DemographicArchive demographicArchive5 = new DemographicArchive();
    EntityDataGenerator.generateTestDataForModelClass(demographicArchive5);
    demographicArchive5.setDemographicNo(demoNo1);
    demographicArchive5.setRosterStatus(rosterStatus3);
    dao.persist(demographicArchive5);

    List<DemographicArchive> expectedResult = new ArrayList<DemographicArchive>(
        Arrays.asList(demographicArchive4, demographicArchive2, demographicArchive1));
    List<DemographicArchive> result = dao.findRosterStatusHistoryByDemographicNo(demoNo1);

    Logger logger = MiscUtils.getLogger();

    if (result.size() != expectedResult.size()) {
      logger.warn("Array sizes do not match. Result: " + result.size());
      Assertions.fail("Array sizes do not match.");
    }
    for (int i = 0; i < expectedResult.size(); i++) {
      if (!expectedResult.get(i).equals(result.get(i))) {
        logger.warn("Items  do not match.");
        Assertions.fail("Items  do not match.");
      }
    }
    Assertions.assertTrue(true);
  }

  @Test
  public void testDemographicArchiveConstructor() {
    // Create a Demographic object with sample data
    val demographicGender = new DemographicGender();
    demographicGender.setId(1);
    demographicGender.setValue("Male");
    demographicGender.setDeleted(false);
    demographicGender.setEditable(false);
    val demographicPronoun = new DemographicPronoun();
    demographicPronoun.setId(1);
    demographicPronoun.setValue("She/Her");
    demographicPronoun.setDeleted(false);
    demographicPronoun.setEditable(false);
    Demographic demographic = new Demographic();
    demographic.setAddress("123 Main St");
    demographic.setAlias("John Doe");
    demographic.setAnonymous("ANONYMOUS");
    demographic.setChartNo("CHART123");
    demographic.setChildren("Child1, Child2");
    demographic.setCitizenship("Canada");
    demographic.setCity("Hamilton");
    demographic.setConsentToUseEmailForCare(true);
    demographic.setConsentToUseEmailForEOrder(false);
    demographic.setCountryOfOrigin("Canada");
    demographic.setDateJoined(new Date());
    demographic.setDateOfBirth("1990-01-01");
    demographic.setDemographicNo(1);
    demographic.setEffDate(new Date());
    demographic.setEmail("john@example.com");
    demographic.setEndDate(new Date());
    demographic.setFirstName("John");
    demographic.setHcRenewDate(new Date());
    demographic.setHcType("ON");
    demographic.setHin("HIN123");
    demographic.setLastName("Doe");
    demographic.setLastUpdateDate(new Date());
    demographic.setLastUpdateUser("admin");
    demographic.setMonthOfBirth("01");
    demographic.setMyOscarUserName("johndoe");
    demographic.setNewsletter("Weekly");
    demographic.setOfficialLanguage("English");
    demographic.setPatientStatus("AC");
    demographic.setPatientStatusDate(new Date());
    demographic.setPcnIndicator("PCN");
    demographic.setPhone("1234567890");
    demographic.setPhone2("9876543210");
    demographic.setPostal("L8S 4K1");
    demographic.setPreviousAddress("456 Elm St");
    demographic.setProviderNo("PROV123");
    demographic.setProvince("Ontario");
    demographic.setRosterDate(new Date());
    demographic.setRosterStatus("ROSTERED");
    demographic.setRosterTerminationDate(new Date());
    demographic.setRosterTerminationReason("Reason");
    demographic.setSex("M");
    demographic.setSin("SIN123");
    demographic.setSourceOfIncome("Employment");
    demographic.setSpokenLanguage("English");
    demographic.setTitle("Mr.");
    demographic.setVer("1.0");
    demographic.setYearOfBirth("1990");
    demographic.setGenderId(1);
    demographic.setPronounId(1);
    demographic.setGenderIdentity(demographicGender);
    demographic.setPronoun(demographicPronoun);
    demographic.setPreferredName("John");
    demographic.setPatientType("Type1");
    demographic.setPatientId("PAT123");
    demographic.setPortalUserId("portaluser");
    demographic.setConsentToUseEmailForCare(true);
    demographic.setConsentToUseEmailForEOrder(false);
    // Create a DemographicArchive object using the constructor
    val demographicArchive = new DemographicArchive(demographic);
    // Assert that the fields in DemographicArchive match the values from the Demographic object
    Assertions.assertEquals(demographic.getAddress(), demographicArchive.getAddress());
    Assertions.assertEquals(demographic.getAlias(), demographicArchive.getAlias());
    Assertions.assertEquals(demographic.getAnonymous(), demographicArchive.getAnonymous());
    Assertions.assertEquals(demographic.getChartNo(), demographicArchive.getChartNo());
    Assertions.assertEquals(demographic.getChildren(), demographicArchive.getChildren());
    Assertions.assertEquals(demographic.getCitizenship(), demographicArchive.getCitizenship());
    Assertions.assertEquals(demographic.getCity(), demographicArchive.getCity());
    Assertions.assertEquals(demographic.getConsentToUseEmailForCare(), demographicArchive.getConsentToUseEmailForCare());
    Assertions.assertEquals(demographic.getConsentToUseEmailForEOrder(), demographicArchive.getConsentToUseEmailForEOrder());
    Assertions.assertEquals(demographic.getCountryOfOrigin(), demographicArchive.getCountryOfOrigin());
    Assertions.assertEquals(demographic.getDateJoined(), demographicArchive.getDateJoined());
    Assertions.assertEquals(demographic.getDateOfBirth(), demographicArchive.getDateOfBirth());
    Assertions.assertEquals(demographic.getDemographicNo(), demographicArchive.getDemographicNo());
    Assertions.assertEquals(demographic.getEffDate(), demographicArchive.getEffDate());
    Assertions.assertEquals(demographic.getEmail(), demographicArchive.getEmail());
    Assertions.assertEquals(demographic.getEndDate(), demographicArchive.getEndDate());
    Assertions.assertEquals(demographic.getFirstName(), demographicArchive.getFirstName());
    Assertions.assertEquals(demographic.getHcRenewDate(), demographicArchive.getHcRenewDate());
    Assertions.assertEquals(demographic.getHcType(), demographicArchive.getHcType());
    Assertions.assertEquals(demographic.getHin(), demographicArchive.getHin());
    Assertions.assertEquals(demographic.getLastName(), demographicArchive.getLastName());
    Assertions.assertTrue(
        demographic.getLastUpdateDate().getTime()
            <= demographicArchive.getLastUpdateDate().getTime());
    Assertions.assertEquals(demographic.getLastUpdateUser(), demographicArchive.getLastUpdateUser());
    Assertions.assertEquals(demographic.getMonthOfBirth(), demographicArchive.getMonthOfBirth());
    Assertions.assertEquals(demographic.getMyOscarUserName(), demographicArchive.getMyOscarUserName());
    Assertions.assertEquals(demographic.getNewsletter(), demographicArchive.getNewsletter());
    Assertions.assertEquals(demographic.getOfficialLanguage(), demographicArchive.getOfficialLanguage());
    Assertions.assertEquals(demographic.getPatientStatus(), demographicArchive.getPatientStatus());
    Assertions.assertEquals(demographic.getPatientStatusDate(), demographicArchive.getPatientStatusDate());
    Assertions.assertEquals(demographic.getPcnIndicator(), demographicArchive.getPcnIndicator());
    Assertions.assertEquals(demographic.getPhone(), demographicArchive.getPhone());
    Assertions.assertEquals(demographic.getPhone2(), demographicArchive.getPhone2());
    Assertions.assertEquals(demographic.getPostal(), demographicArchive.getPostal());
    Assertions.assertEquals(demographic.getPreviousAddress(), demographicArchive.getPreviousAddress());
    Assertions.assertEquals(demographic.getProviderNo(), demographicArchive.getProviderNo());
    Assertions.assertEquals(demographic.getProvince(), demographicArchive.getProvince());
    Assertions.assertEquals(demographic.getRosterDate(), demographicArchive.getRosterDate());
    Assertions.assertEquals(demographic.getRosterStatus(), demographicArchive.getRosterStatus());
    Assertions.assertEquals(demographic.getRosterTerminationDate(), demographicArchive.getRosterTerminationDate());
    Assertions.assertEquals(demographic.getRosterTerminationReason(), demographicArchive.getRosterTerminationReason());
    Assertions.assertEquals(demographic.getSex(), demographicArchive.getSex());
    Assertions.assertEquals(demographic.getSin(), demographicArchive.getSin());
    Assertions.assertEquals(demographic.getSourceOfIncome(), demographicArchive.getSourceOfIncome());
    Assertions.assertEquals(demographic.getSpokenLanguage(), demographicArchive.getSpokenLanguage());
    Assertions.assertEquals(demographic.getTitle(), demographicArchive.getTitle());
    Assertions.assertEquals(demographic.getVer(), demographicArchive.getVer());
    Assertions.assertEquals(demographic.getYearOfBirth(), demographicArchive.getYearOfBirth());
    Assertions.assertEquals(demographic.getGenderId(), demographicArchive.getGenderId());
    Assertions.assertEquals(demographic.getPronounId(), demographicArchive.getPronounId());
    Assertions.assertEquals(demographic.getGenderIdentityValue(), demographicArchive.getGender());
    Assertions.assertEquals(demographic.getPronounValue(), demographicArchive.getPronoun());
    Assertions.assertEquals(demographic.getPreferredName(), demographicArchive.getPreferredName());
    Assertions.assertEquals(demographic.getPatientType(), demographicArchive.getPatientType());
    Assertions.assertEquals(demographic.getPatientId(), demographicArchive.getPatientId());
    Assertions.assertEquals(demographic.getPortalUserId(), demographicArchive.getPortalUserId());
    Assertions.assertEquals(demographic.getConsentToUseEmailForCare(), demographicArchive.getConsentToUseEmailForCare());
    Assertions.assertEquals(demographic.getConsentToUseEmailForEOrder(), demographicArchive.getConsentToUseEmailForEOrder());
  }
}
