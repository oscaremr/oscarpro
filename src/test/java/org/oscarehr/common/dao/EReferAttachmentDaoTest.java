package org.oscarehr.common.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.EReferAttachment;
import org.oscarehr.common.model.OceanWorkflowTypeEnum;
import org.oscarehr.util.SpringUtils;

public class EReferAttachmentDaoTest extends DaoTestFixtures {

  private final Date CURRENT_DATE = new Date();
  private final Date OLD_DATE = getOldDate();

  private final EReferAttachmentDao eReferAttachmentDao = SpringUtils.getBean(EReferAttachmentDao.class);

  @BeforeEach
  public void before() throws Exception {
    SchemaUtils.restoreTable("erefer_attachment");
  }

  @AfterAll
  public static void after() throws Exception {
    SchemaUtils.restoreTable("erefer_attachment");
  }
  
  @Test
  public void givenDemographicNumberAndTypeEReferral_whenGetAttachmentsByType_thenReturnCorrectAttachments() {
    persistEReferAttachments(Arrays.asList(
        new EReferAttachment(1, OceanWorkflowTypeEnum.EREFERRAL, OLD_DATE),
        new EReferAttachment(2, OceanWorkflowTypeEnum.EREFERRAL, OLD_DATE),
        new EReferAttachment(1, OceanWorkflowTypeEnum.MESSENGER, OLD_DATE)
    ));

    List<EReferAttachment> eReferAttachments = eReferAttachmentDao.getAttachmentsByType(1, OceanWorkflowTypeEnum.EREFERRAL);

    assertEquals(1, eReferAttachments.size());
    assertEquals(1, eReferAttachments.get(0).getDemographicNo());
    assertEquals(OceanWorkflowTypeEnum.EREFERRAL.name(), eReferAttachments.get(0).getType());
  }
  
  @Test
  public void givenDemographicNumberAndTypeMessenger_whenGetAttachmentsByType_thenReturnCorrectAttachments() {
    persistEReferAttachments(Arrays.asList(
        new EReferAttachment(1, OceanWorkflowTypeEnum.EREFERRAL, OLD_DATE),
        new EReferAttachment(2, OceanWorkflowTypeEnum.MESSENGER, OLD_DATE),
        new EReferAttachment(1, OceanWorkflowTypeEnum.MESSENGER, OLD_DATE)
    ));
    
    List<EReferAttachment> eReferAttachments = eReferAttachmentDao.getAttachmentsByType(2, OceanWorkflowTypeEnum.MESSENGER);
    
    assertEquals(1, eReferAttachments.size());
    assertEquals(2, eReferAttachments.get(0).getDemographicNo());
    assertEquals(OceanWorkflowTypeEnum.MESSENGER.name(), eReferAttachments.get(0).getType());
  }
  
  @Test
  public void givenDemographicNumberAndTypeNull_whenGetAttachmentsByType_thenReturnCorrectAttachments() {
    persistEReferAttachments(Arrays.asList(
        new EReferAttachment(1, OceanWorkflowTypeEnum.EREFERRAL, OLD_DATE),
        new EReferAttachment(2, OceanWorkflowTypeEnum.EREFERRAL, OLD_DATE),
        new EReferAttachment(1, OceanWorkflowTypeEnum.MESSENGER, OLD_DATE)
    ));
    
    List<EReferAttachment> eReferAttachments = eReferAttachmentDao.getAttachmentsByType(1, null);
    
    assertEquals(2, eReferAttachments.size());
    assertEquals(1, eReferAttachments.get(0).getDemographicNo());
    assertEquals(1, eReferAttachments.get(1).getDemographicNo());
    assertEquals(OceanWorkflowTypeEnum.EREFERRAL.name(), eReferAttachments.get(0).getType());
    assertEquals(OceanWorkflowTypeEnum.MESSENGER.name(), eReferAttachments.get(1).getType());
  }

  @Test
  public void givenDemographicNumberAndType_whenGetRecentByDemographic_thenReturnCorrectAttachment() {
    persistEReferAttachments(Arrays.asList(
        new EReferAttachment(1, OceanWorkflowTypeEnum.EREFERRAL, CURRENT_DATE),
        new EReferAttachment(2, OceanWorkflowTypeEnum.EREFERRAL, OLD_DATE),
        new EReferAttachment(1, OceanWorkflowTypeEnum.MESSENGER, OLD_DATE)
    ));

    EReferAttachment eReferAttachment = eReferAttachmentDao.getLatestCreatedByDemographic(
        1, OceanWorkflowTypeEnum.EREFERRAL, null);

    assertEquals(1, eReferAttachment.getDemographicNo());
    assertEquals(OceanWorkflowTypeEnum.EREFERRAL.name(), eReferAttachment.getType());
    assertDates(CURRENT_DATE, eReferAttachment.getCreated());
  }

  @Test
  public void givenDemographicNumberAndNullType_whenGetLatestCreatedByDemographic_thenReturnCorrectAttachment() {
    persistEReferAttachments(Arrays.asList(
        new EReferAttachment(1, OceanWorkflowTypeEnum.EREFERRAL, OLD_DATE),
        new EReferAttachment(1, OceanWorkflowTypeEnum.MESSENGER, CURRENT_DATE)
    ));

    EReferAttachment eReferAttachment =
        eReferAttachmentDao.getLatestCreatedByDemographic(1, null, null);

    assertEquals(1, eReferAttachment.getDemographicNo());
    assertEquals(OceanWorkflowTypeEnum.MESSENGER.name(), eReferAttachment.getType());
    assertDates(CURRENT_DATE, eReferAttachment.getCreated());
  }

  @Test
  public void givenDemographicNumber_whenGetLatestCreatedByDemographic_thenReturnCorrectAttachment() {
    persistEReferAttachments(Arrays.asList(
        new EReferAttachment(1, OceanWorkflowTypeEnum.EREFERRAL, CURRENT_DATE),
        new EReferAttachment(1, OceanWorkflowTypeEnum.MESSENGER, OLD_DATE)
    ));

    EReferAttachment eReferAttachment = eReferAttachmentDao.getLatestCreatedByDemographic(1);

    assertEquals(1, eReferAttachment.getDemographicNo());
    assertEquals(OceanWorkflowTypeEnum.EREFERRAL.name(), eReferAttachment.getType());
    assertDates(CURRENT_DATE, eReferAttachment.getCreated());
  }

  @Test
  public void givenMultipleResults_whenGetLatestCreatedByDemographic_thenReturnMostRecent() {
    persistEReferAttachments(Arrays.asList(
        new EReferAttachment(1, OceanWorkflowTypeEnum.MESSENGER, OLD_DATE),
        new EReferAttachment(1, OceanWorkflowTypeEnum.MESSENGER, CURRENT_DATE),
        new EReferAttachment(1, OceanWorkflowTypeEnum.MESSENGER, OLD_DATE)
    ));

    EReferAttachment eReferAttachment =
        eReferAttachmentDao.getLatestCreatedByDemographic(1, OceanWorkflowTypeEnum.MESSENGER, null);

    assertEquals(1, eReferAttachment.getDemographicNo());
    assertEquals(OceanWorkflowTypeEnum.MESSENGER.name(), eReferAttachment.getType());
    assertDates(CURRENT_DATE, eReferAttachment.getCreated());
  }

  @Test
  public void givenNoResults_whenGetLatestCreatedByDemographic_thenReturnNull() {
    persistEReferAttachments(Arrays.asList(
        new EReferAttachment(1, OceanWorkflowTypeEnum.MESSENGER, OLD_DATE),
        new EReferAttachment(1, OceanWorkflowTypeEnum.MESSENGER, CURRENT_DATE)
    ));
    EReferAttachment eReferAttachment =
        eReferAttachmentDao.getLatestCreatedByDemographic(1, OceanWorkflowTypeEnum.EREFERRAL, null);
    assertNull(eReferAttachment);
  }

  @Test
  public void givenEmptyTableAndNoResults_whenGetLatestCreatedByDemographic_thenReturnNull() {
    EReferAttachment eReferAttachment = eReferAttachmentDao.getLatestCreatedByDemographic(1);
    assertNull(eReferAttachment);
  }

  private void persistEReferAttachments(final List<EReferAttachment> eReferAttachments) {
    for (EReferAttachment eReferAttachment : eReferAttachments) {
      eReferAttachmentDao.persist(eReferAttachment);
    }
  }

  private Date getOldDate() {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.MINUTE, -30);
    return calendar.getTime();
  }

  private void assertDates(final Date expected, final Date actual) {
    long expectedTimeInSeconds = TimeUnit.MILLISECONDS.toSeconds(expected.getTime());
    long actualTimeInSeconds = TimeUnit.MILLISECONDS.toSeconds(actual.getTime());
    assertTrue(Math.abs(expectedTimeInSeconds - actualTimeInSeconds) <= 1);
  }

}