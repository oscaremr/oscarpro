/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import lombok.val;
import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.OnAccountBill;
import org.oscarehr.util.SpringUtils;

public class OnAccountBillDaoTest extends DaoTestFixtures {

    protected OnAccountBillDao dao = SpringUtils.getBean(OnAccountBillDao.class);

    private static final Integer DEMOGRAPHIC_1_NUMBER = 1;
    private static final Double DEMOGRAPHIC_1_CHARGED_1 = 1d;
    private static final Double DEMOGRAPHIC_1_CHARGED_2 = 13.1d;
    private static final Double DEMOGRAPHIC_1_SUM_CHARGED = 14.1d;
    private static final Double DEMOGRAPHIC_1_BALANCE_1 = 12.5d;
    private static final Double DEMOGRAPHIC_1_BALANCE_2 = 17.5d;
    private static final Double DEMOGRAPHIC_1_SUM_BALANCE = 30d;
    private static final Integer DEMOGRAPHIC_2_NUMBER = 2;
    private static final Double DEMOGRAPHIC_2_CHARGED_1 = 1d;
    private static final Double DEMOGRAPHIC_2_CHARGED_2 = 13d;
    private static final Double DEMOGRAPHIC_2_SUM_CHARGED = 14d;
    private static final Double DEMOGRAPHIC_2_BALANCE_1 = 15d;
    private static final Double DEMOGRAPHIC_2_BALANCE_2 = 15d;
    private static final Double DEMOGRAPHIC_2_SUM_BALANCE = 30d;

    public OnAccountBillDaoTest() {
    }

    @BeforeEach
    public void before() throws Exception {
        SchemaUtils.restoreTable("onaccountbills");
    }

    @Test
    public void givenNewOnAccountBill_whenDaoPersist_thenEntityIdAssigned() throws Exception {
        OnAccountBill entity = new OnAccountBill();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        dao.persist(entity);
        Assertions.assertNotNull(entity.getId());
    }

    @Test
    public void givenOnAccountBill_whenFindByDemographicNumber_thenSuccess() throws Exception {
        createOnAccountBillEntries();
        var results = dao.findByDemographicNumber(DEMOGRAPHIC_1_NUMBER);
        Assertions.assertEquals(2, results.size());
        for (OnAccountBill result : results) {
            Assertions.assertEquals(DEMOGRAPHIC_1_NUMBER, result.getDemographicNo());
        }
    }

    @Test
    public void givenOnAccountBill_whenGetTotalBillsByDemographicNumber_thenReturnDepositSum() throws Exception {
        createOnAccountBillEntries();
        var result = dao.getTotalBillsByDemographicNumber(DEMOGRAPHIC_2_NUMBER);
        Assertions.assertEquals(DEMOGRAPHIC_2_SUM_CHARGED, result);


        result = dao.getTotalBillsByDemographicNumber(DEMOGRAPHIC_1_NUMBER);
        Assertions.assertEquals(DEMOGRAPHIC_1_SUM_CHARGED, result);
    }

    @Test
    public void givenOnAccountBill_whenGetTotalBalanceByDemographicNumber_thenReturnBalanceSum() throws Exception {
        createOnAccountBillEntries();
        var result = dao.getTotalBalanceByDemographicNumber(DEMOGRAPHIC_2_NUMBER);
        Assertions.assertEquals(DEMOGRAPHIC_2_SUM_BALANCE, result);


        result = dao.getTotalBalanceByDemographicNumber(DEMOGRAPHIC_1_NUMBER);
        Assertions.assertEquals(DEMOGRAPHIC_1_SUM_BALANCE, result);
    }

    @Test
    public void givenInvoiceNumber_whenVoidBillByInvoiceNumber_thenBillStatusVoidAndAmountsZero() throws Exception {
        var entity = new OnAccountBill();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_1_NUMBER);
        entity.setBalance(DEMOGRAPHIC_1_BALANCE_1);
        entity.setAmountCharged(DEMOGRAPHIC_1_CHARGED_1);
        entity.setBillStatus("");
        dao.persist(entity);
        dao.voidBillByInvoiceNumber(entity.getId());
        val result = dao.find(entity.getId());
        Assertions.assertEquals("Void", result.getBillStatus());
        Assertions.assertEquals(0, result.getAmountCharged());
        Assertions.assertEquals(0, result.getHst());
        Assertions.assertEquals(0, result.getBalance());
    }

    @Test
    public void givenInvoiceAndDemographicNumber_whenFindBy_thenReturnMatchingResult() throws Exception {
        var entity = new OnAccountBill();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_1_NUMBER);
        entity.setBalance(DEMOGRAPHIC_1_BALANCE_1);
        entity.setAmountCharged(DEMOGRAPHIC_1_CHARGED_1);
        entity.setBillStatus("");
        dao.persist(entity);
        entity = new OnAccountBill();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_1_NUMBER);
        entity.setBalance(DEMOGRAPHIC_1_BALANCE_2);
        entity.setAmountCharged(DEMOGRAPHIC_1_CHARGED_2);
        entity.setBillStatus("");
        dao.persist(entity);
        val result = dao.findByInvoiceAndDemographicNumber(entity.getId(), DEMOGRAPHIC_1_NUMBER);
        Assertions.assertEquals(DEMOGRAPHIC_1_CHARGED_2, result.getAmountCharged());
        Assertions.assertEquals(DEMOGRAPHIC_1_BALANCE_2, result.getBalance());

    }

    private void createOnAccountBillEntries() throws Exception {
        createOnAccountBillEntry(DEMOGRAPHIC_1_NUMBER, DEMOGRAPHIC_1_BALANCE_1, DEMOGRAPHIC_1_CHARGED_1);
        createOnAccountBillEntry(DEMOGRAPHIC_1_NUMBER, DEMOGRAPHIC_1_BALANCE_2, DEMOGRAPHIC_1_CHARGED_2);
        createOnAccountBillEntry(DEMOGRAPHIC_2_NUMBER, DEMOGRAPHIC_2_BALANCE_1, DEMOGRAPHIC_2_CHARGED_1);
        createOnAccountBillEntry(DEMOGRAPHIC_2_NUMBER, DEMOGRAPHIC_2_BALANCE_2, DEMOGRAPHIC_2_CHARGED_2);
    }

    private void createOnAccountBillEntry(final Integer demographicNumber,
                                          final Double balance,
                                          final Double amountCharged) throws Exception {
        var entity = new OnAccountBill();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(demographicNumber);
        entity.setBalance(balance);
        entity.setAmountCharged(amountCharged);
        dao.persist(entity);
    }

}