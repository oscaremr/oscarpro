/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.common.dao.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.commons.lang.StringUtils;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

/**
 * This class is a utility class to create and drop the database schema and all it's bits.
 * This class is highly database specific and alternate versions of this class will have to be
 * written for other databases.
 */
@Slf4j
public class SchemaUtils {

  public static Map<String, String> createTableStatements = new HashMap<>();

  private static final String TABLE_SUFFIX = "_maventest";
	private static final String MYSQL_DEFAULT_PORT = "3306";
  private static boolean initAttempt = false;
  private static boolean initComplete = false;

  private static Connection getConnection()
      throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
    String driver = ConfigUtils.getProperty("db_driver");
    String user = ConfigUtils.getProperty("db_user");
    String password = ConfigUtils.getProperty("db_password");
    String urlPrefix = ConfigUtils.getProperty("db_url_prefix");

    Class.forName(driver).newInstance();
    return (DriverManager.getConnection(urlPrefix, user, password));
  }

	public static void restoreTable(String... tableNames) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		restoreTable(true,tableNames);
	}

	private static boolean isWindows() {
		String osName = System.getProperty("os.name");
		return osName.toLowerCase().contains("windows");
	}

	public static void restoreTable(boolean includeInitData, String... tableNames) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		long start = System.currentTimeMillis();
		String schema=ConfigUtils.getProperty("db_schema");
		try (Connection c = getConnection()) {
			Statement statement=c.createStatement();
			statement.executeUpdate("use "+schema);
			statement.executeUpdate("set foreign_key_checks = 0");
			for (String tableName : tableNames) {
				// We can't initialize tables that don't exist.
				if (StringUtils.isEmpty(tableName)) {
					continue;
				}
				try {
          String sql = "truncate table " + tableName;
          statement.executeUpdate(sql);

					if (includeInitData) {
						statement.executeUpdate("insert into " + tableName + " select * from " + tableName + TABLE_SUFFIX);
					}
				} catch (Exception e) {
					log.warn("Unable to create table for: " + tableName, e);
					// We swallow exceptions since some tables for certain tests are different in ON and BC
					// We will let the tests be responsible for triggering failures themselves.
				}
			}
			statement.executeUpdate("set foreign_key_checks = 1");
			statement.close();
		}
		long end = System.currentTimeMillis();
		long secsTaken = (end-start)/1000;
		if(secsTaken > 30) {
      log.info("Restoring db took " + secsTaken + " seconds.");
		}
	}

	@Deprecated
	public static void restoreAllTables() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		long start = System.currentTimeMillis();
		String schema=ConfigUtils.getProperty("db_schema");

		try (Connection c = getConnection()) {
			Statement s=c.createStatement();
			s.executeUpdate("use "+schema);
			for (String tableName : createTableStatements.keySet()) {
				restoreTable(tableName);
			}
			s.close();
		}
		long end = System.currentTimeMillis();
		long secsTaken = (end-start)/1000;
		if(secsTaken > 30) {
      log.info("Restoring db took " + secsTaken + " seconds.");
		}
	}

  public static void loadFileIntoMySQL(Resource resource)
      throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
    try (Connection c = getConnection()) {
      val statement = c.createStatement();
      statement.executeUpdate("USE " + ConfigUtils.getProperty("db_schema"));
      statement.executeUpdate("SET foreign_key_checks = 0");

      try {
        ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
        populator.addScript(resource);
        populator.populate(c);
      }
      finally {
        statement.executeUpdate("SET foreign_key_checks = 1");
      }
    }
	}

  private static synchronized void initializeTestDatabase(final Connection c)
      throws SQLException,
          ClassNotFoundException,
          InstantiationException,
          IllegalAccessException {
    boolean initializeDatabase = !Boolean.parseBoolean(System.getProperty("oscar.dbinit.skip"));

    // print connection info for dev debugging
    String schema=ConfigUtils.getProperty("db_schema");
    String driver=ConfigUtils.getProperty("db_driver");
    String user=ConfigUtils.getProperty("db_user");
    String urlPrefix=ConfigUtils.getProperty("db_url_prefix");

    log.info("Using Connection [" + driver + "]: " + urlPrefix + schema + " -u " + user);
    Statement statement=c.createStatement();

    // initialize a new test database if needed,
    // otherwise assume the database is already initialized
    if (initializeDatabase) {
      statement.executeUpdate("drop database if exists "+schema);
      statement.executeUpdate("create database "+schema + " DEFAULT CHARACTER SET utf8  DEFAULT COLLATE utf8_general_ci");
      statement.executeUpdate("use "+schema);

      //TODO make database init correctly
      //Might not be possible anymore with split database repos etc.
      String dbFile = ConfigUtils.getProperty("oscar.dbinit.file");
      loadFileIntoMySQL(new FileSystemResource(dbFile));
    }
    else {
      statement.executeUpdate("use "+schema);
    }
    createMavenTableRestorePoints(c, schema);
    initComplete = true;
  }

	private static void populateExistingTableStatements(Connection c) {
		//we're assuming a db ready to go... we just need to build the createTableStatementsMap
		createTableStatements.clear();
		try {
			ResultSet rs = c.getMetaData().getTables(ConfigUtils.getProperty("db_schema"), null, "%", null);
			while(rs.next()) {
				String tableName = rs.getString("TABLE_NAME");
				if(!tableName.endsWith(TABLE_SUFFIX))
					continue;
				Statement stmt2 = c.createStatement();
				ResultSet rs2 = stmt2.executeQuery("show create table " + tableName + ";");
				if(rs2.next()) {
					String tableNamePart = tableName.substring(0, tableName.length() - 10);
					String sql = rs2.getString(2).replaceAll(tableName, tableNamePart);
          sql = renameConstraint(sql);
					createTableStatements.put(tableNamePart, sql);
				}
				rs2.close();
				stmt2.close();
			}
			rs.close();
		} catch(SQLException e) {
      log.error("Error:", e);
		}
	}

  private static void createMavenTableRestorePoints(Connection c, String schema) throws SQLException {
    //we're assuming a db ready to go... we just need to build the createTableStatementsMap
    createTableStatements.clear();
    ResultSet rs = c.getMetaData().getTables(schema, null, "%", null);
    while(rs.next()) {
      String tableName = rs.getString("TABLE_NAME");
      if (tableName.endsWith(TABLE_SUFFIX)) {
        continue;
      }

      Statement stmt2 = c.createStatement();
      ResultSet rs2 = stmt2.executeQuery("SHOW CREATE TABLE " + tableName + ";");
      if(rs2.next()) {
        String sql = rs2.getString(2);
        sql = renameConstraint(sql);
        createTableStatements.put(tableName, sql);

        // Create table restore point
        Statement getTableInfoStmt = c.createStatement();
        ResultSet tableInfo = getTableInfoStmt.executeQuery("SHOW TABLE STATUS WHERE Name='" + tableName + "'");
        if (tableInfo.next())
        {
          String tableEngine = "";
          if (tableInfo.getString(2) != null)
          {
            tableEngine = " ENGINE = " + tableInfo.getString(2);
          }

          try {
            Statement stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS " + tableName + TABLE_SUFFIX + tableEngine + " SELECT * FROM " + tableName);
            stmt.close();
          }
          catch (SQLException e) {
            log.error("Error creating " + TABLE_SUFFIX + " table copy:", e);
          }
        }
        getTableInfoStmt.close();
      }
      rs2.close();
      stmt2.close();
    }
    rs.close();
  }

  /**
   * Replace constraint names with name + "_original".
   * @param sql The input sql to modify
   * @return The updated-constraint sql
   */
  private static String renameConstraint(String sql)
  {
    if(sql == null) {
      return null;
    }

    return sql.replaceAll("CONSTRAINT `(.*?)`", String.format("CONSTRAINT `$1%s`", "_original"));
  }

	/**
	 * Intent is for junit tests.
	 */
	public static void dropAndRecreateDatabase()
			throws SQLException, InstantiationException,
			IllegalAccessException, ClassNotFoundException, IOException {
		initAttempt = true;
		if (Boolean.parseBoolean(ConfigUtils.getProperty("wpde"))) {
			initializeWpdeTestDatabase();
		}
    else {
      try (Connection c = getConnection()) {
        initializeTestDatabase(c);
      }
    }
	}

	private static void initializeWpdeTestDatabase() throws
			ClassNotFoundException,
			IllegalAccessException,
			InstantiationException,
			IOException,
			SQLException
	{
		String command = "bootstrap unit_tests";
		if (Boolean.parseBoolean(ConfigUtils.getProperty("kubernetes"))) {
			command = "kubernetes " + ConfigUtils.getProperty("billregion");
		}
		try (Connection c = getConnection()) {
			runWpdeCommand(command);
			Statement s = c.createStatement();
			s.executeUpdate("use " + ConfigUtils.getProperty("db_schema"));
			populateExistingTableStatements(c);
			initComplete = true;
		} finally {
			getConnection().close();
		}
	}

	private static int runWpdeCommand(String command) throws IOException {
		if (isWindows()) {
			command = "cmd /c run.cmd " + command;
		} else {
			command = "./run.sh " + command;
		}

		log.info("Runtime exec command string : " + command);
		Process process = Runtime.getRuntime().exec(
				command,
				null,
				new File(ConfigUtils.getProperty("wpde.directory"))
		);

		logProcessStream(process.getInputStream());
		logProcessStream(process.getErrorStream());
		int exitValue;
		try {
			exitValue = process.waitFor();
		} catch (InterruptedException e) {
			val error = "error with process \"" + command + "\"";
			log.error(error);
			throw new IOException(error);
		}
		return exitValue;
	}

	private static void logProcessStream(final InputStream stream) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		String s;
		while ((s = reader.readLine()) != null) {
      log.info(s);
		}
		reader.close();
	}

  public static boolean databaseInitializationComplete() {
    return initComplete;
  }

  public static boolean databaseInitializationAttempted() {
    return initAttempt;
  }

  public static void main(String... argv) throws Exception {
    dropAndRecreateDatabase();
  }
}
