/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common.dao;

import lombok.val;
import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.OnAccountRefund;
import org.oscarehr.util.SpringUtils;

public class OnAccountRefundDaoTest extends DaoTestFixtures {

    protected OnAccountRefundDao dao = SpringUtils.getBean(OnAccountRefundDao.class);

    private static final Integer DEMOGRAPHIC_NUMBER = 1;
    private static final Double DEMOGRAPHIC_TOTAL_REFUND_1 = 1.29d;
    private static final String DEMOGRAPHIC_STATUS_1 = "Pending";
    private static final Double DEMOGRAPHIC_TOTAL_REFUND_2 = 1.41d;
    private static final String DEMOGRAPHIC_STATUS_2 = "Pending";
    private static final Double DEMOGRAPHIC_TOTAL_REFUND_3 = 34.5d;
    private static final String DEMOGRAPHIC_STATUS_3 = "Void";
    private static final Double DEMOGRAPHIC_TOTAL_REFUND_4 = 23.7d;
    private static final String DEMOGRAPHIC_STATUS_4 = "Void";
    private static final Double DEMOGRAPHIC_TOTAL_REFUND_5 = 62.13d;
    private static final String DEMOGRAPHIC_STATUS_5 = "Saved";
    private static final Double DEMOGRAPHIC_TOTAL_REFUND_SUM_PENDING = 2.7d;
    private static final Double DEMOGRAPHIC_TOTAL_REFUND_SUM_TOTAL = 123.03d;

    private static final Integer DEMOGRAPHIC_NUMBER_2 = 2;
    private static final Double DEMOGRAPHIC_2_TOTAL_REFUND = 1.23d;
    private static final String DEMOGRAPHIC_2_STATUS = "Saved";

    public OnAccountRefundDaoTest() {
    }

    @BeforeEach
    public void before() throws Exception {
        SchemaUtils.restoreTable("onaccountrefunds");
    }

    @Test
    public void givenNewOnAccountRefund_whenDaoPersist_thenEntityIdAssigned() throws Exception {
        OnAccountRefund entity = new OnAccountRefund();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        dao.persist(entity);
        Assertions.assertNotNull(entity.getId());
    }

    @Test
    public void givenOnAccountRefund_whenFindByDemographicNumber_thenSuccess() throws Exception {
        createOnAccountRefundEntries();
        var results = dao.findByDemographicNumber(DEMOGRAPHIC_NUMBER);
        Assertions.assertEquals(5, results.size());
        for (OnAccountRefund result : results) {
            Assertions.assertEquals(result.getDemographicNo(), DEMOGRAPHIC_NUMBER);
        }
    }

    @Test
    public void givenOnAccountRefund_whenGetTotalDepositsByDemographicNumber_thenReturnDepositSum() throws Exception {
        createOnAccountRefundEntries();
        var result = dao.getTotalRefundsByDemographicNumber(DEMOGRAPHIC_NUMBER);
        Assertions.assertEquals(DEMOGRAPHIC_TOTAL_REFUND_SUM_TOTAL, result);


        result = dao.getTotalRefundsByDemographicNumber(DEMOGRAPHIC_NUMBER_2);
        Assertions.assertEquals(DEMOGRAPHIC_2_TOTAL_REFUND, result);
    }

    @Test
    public void givenOnAccountRefund_whenGetTotalBalanceByDemographicNumber_thenReturnBalanceSum() throws Exception {
        createOnAccountRefundEntries();
        var result = dao.getTotalRefundsPendingByDemographicNumber(DEMOGRAPHIC_NUMBER);
        Assertions.assertEquals(DEMOGRAPHIC_TOTAL_REFUND_SUM_PENDING, result);


        result = dao.getTotalRefundsPendingByDemographicNumber(DEMOGRAPHIC_NUMBER_2);
        Assertions.assertEquals(0, result);
    }

    @Test
    public void givenOnAccountRefunds_whenSetRefundVoidByInvoiceNumber_thenSuccess() throws Exception {
        var entity = new OnAccountRefund();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setRefundStatus("Pending");
        entity.setTotalRefund(100d);
        dao.persist(entity);
        dao.setRefundVoidByInvoiceNumber(entity.getId());
        val result = dao.find(entity.getId());
        Assertions.assertEquals(result.getRefundStatus(), "Void");
        Assertions.assertEquals(result.getTotalRefund(), 0);
    }

    private void createOnAccountRefundEntries() throws Exception {
        var entity = new OnAccountRefund();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_NUMBER);
        entity.setTotalRefund(DEMOGRAPHIC_TOTAL_REFUND_1);
        entity.setRefundStatus(DEMOGRAPHIC_STATUS_1);
        dao.persist(entity);
        entity = new OnAccountRefund();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_NUMBER);
        entity.setTotalRefund(DEMOGRAPHIC_TOTAL_REFUND_2);
        entity.setRefundStatus(DEMOGRAPHIC_STATUS_2);
        dao.persist(entity);
        entity = new OnAccountRefund();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_NUMBER);
        entity.setTotalRefund(DEMOGRAPHIC_TOTAL_REFUND_3);
        entity.setRefundStatus(DEMOGRAPHIC_STATUS_3);
        dao.persist(entity);
        entity = new OnAccountRefund();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_NUMBER);
        entity.setTotalRefund(DEMOGRAPHIC_TOTAL_REFUND_4);
        entity.setRefundStatus(DEMOGRAPHIC_STATUS_4);
        dao.persist(entity);
        entity = new OnAccountRefund();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_NUMBER);
        entity.setTotalRefund(DEMOGRAPHIC_TOTAL_REFUND_5);
        entity.setRefundStatus(DEMOGRAPHIC_STATUS_5);
        dao.persist(entity);

        entity = new OnAccountRefund();
        EntityDataGenerator.generateTestDataForModelClass(entity);
        entity.setDemographicNo(DEMOGRAPHIC_NUMBER_2);
        entity.setTotalRefund(DEMOGRAPHIC_2_TOTAL_REFUND);
        entity.setRefundStatus(DEMOGRAPHIC_2_STATUS);
        dao.persist(entity);
    }

}