package org.oscarehr.common.dao;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.OrderLabTestCodeCache;
import org.oscarehr.util.SpringUtils;

public class OrderLabTestCodeCacheDaoTest extends DaoTestFixtures {

  private OrderLabTestCodeCacheDao dao = SpringUtils.getBean(OrderLabTestCodeCacheDao.class);

  @BeforeEach
  public void before() throws Exception {
    SchemaUtils.restoreTable(false, "OrderLabTestCodeCache");
  }

  @Test
  public void test_FindAllTestCodeByProvince() throws Exception {
    dao.persist(getOrderLabTestCodeCache("ON"));
    dao.persist(getOrderLabTestCodeCache("BC"));
    assertEquals(1, dao.findAllTestCodeByProvince("ON").size());
    assertEquals(1, dao.findAllTestCodeByProvince("BC").size());
  }

  @Test
  public void test_findTestCodeByProvince() throws Exception {
    dao.persist(getOrderLabTestCodeCache("T_hba1c", "TR10228-5B","ON"));
    assertEquals(1, dao.findTestCodeByProvince("T_hba1c","ON").size());
  }

  @Test
  public void test_findTestCodeByProvinceNull() throws Exception {
    dao.persist(getOrderLabTestCodeCache("T_hba1c", "TR10228-5B",null));
    assertEquals(1, dao.findTestCodeByProvinceNull("T_hba1c").size());
    assertEquals(1, dao.findTestCodeByProvinceNull("T_hba1c").size());
  }

  private OrderLabTestCodeCache getOrderLabTestCodeCache(String province) throws Exception {
    OrderLabTestCodeCache orderLabTestCodeCache = new OrderLabTestCodeCache();
    EntityDataGenerator.generateTestDataForModelClass(orderLabTestCodeCache);
    orderLabTestCodeCache.setProvince(province);
    return orderLabTestCodeCache;
  }

  private OrderLabTestCodeCache getOrderLabTestCodeCache(String key, String testCode, String province) throws Exception {
    OrderLabTestCodeCache orderLabTestCodeCache = new OrderLabTestCodeCache();
    orderLabTestCodeCache.setProvince(province);
    orderLabTestCodeCache.setKeyName(key);
    orderLabTestCodeCache.setLabTestCode(testCode);
    return orderLabTestCodeCache;
  }
}
