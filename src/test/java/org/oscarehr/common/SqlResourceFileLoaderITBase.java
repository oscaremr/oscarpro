/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.common;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.springframework.core.io.ClassPathResource;

/**
 * Base class for loading SQL resource files into MySQL for integration tests.
 * Built for compatibility with legacy Oscar code. Avoid use with new code as the sql loader
 * relies on an external mysql command and should be removed in the future.
 * <p>
 * Use: extend this class and implement the abstract methods to load a SQL file into MySQL.
 * Provide a relative path to the SQL resource file in the classpath.
 * Ex: org/oscarehr/my/folder/someFile.sql
 */
public abstract class SqlResourceFileLoaderITBase extends DaoTestFixtures {

  protected abstract String getSqlResourceFile();
  protected abstract String[] getTablesToRestore();

  @BeforeEach
  public void before() throws Exception {
    SchemaUtils.restoreTable(getTablesToRestore());
    String resourceFileName = getSqlResourceFile();
    SchemaUtils.loadFileIntoMySQL(new ClassPathResource(resourceFileName));
  }

  @AfterEach
  public void after() throws Exception {
    SchemaUtils.restoreTable(getTablesToRestore());
  }
}
