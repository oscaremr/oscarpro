package org.oscarehr.demographic.merge;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import lombok.val;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.DemographicExtKey;
import org.oscarehr.managers.adapter.DemographicAdapter;
import org.springframework.test.util.ReflectionTestUtils;

public class DemographicMergeOperationDaoTest {

  public static final String REFERRAL_DOCTOR_OPERATION = "Demographic family_doctor Main";
  public static final String FAMILY_DOCTOR_OPERATION = "Demographic family_physician Main";
  public static final String DOCTOR_NAME = "Test,Doctor";
  public static final String DOCTOR_OHIP = "123456";

  @InjectMocks private DemographicMergeOperationDao demographicMergeOperationDao;
  @Mock private EntityManager entityManager;
  @Mock private Query query;
  @Mock private DemographicDao demographicDao;
  @Mock private DemographicExtDao demographicExtDao;

  private DemographicAdapter demographicAdapter = new DemographicAdapter();
  private Demographic oldDemographic;
  private Demographic mainDemographic;
  private DemographicExt demographicExtName;
  private DemographicExt demographicExtOhip;

  @BeforeEach
  public void setup() {
    MockitoAnnotations.openMocks(this);
    ReflectionTestUtils.setField(demographicAdapter, "demographicExtDao", demographicExtDao);
    oldDemographic = new Demographic();
    oldDemographic.setDemographicNo(1);

    mainDemographic = new Demographic();
    mainDemographic.setDemographicNo(2);

    demographicExtName = new DemographicExt();
    demographicExtName.setDemographicNo(mainDemographic.getDemographicNo());
    demographicExtName.setValue("Test,Doctor");

    demographicExtOhip = new DemographicExt();
    demographicExtOhip.setDemographicNo(mainDemographic.getDemographicNo());
    demographicExtOhip.setValue("123456");

    Mockito.when(entityManager.createQuery(Mockito.anyString())).thenReturn(query);
  }

  @Test
  public void revertUpdateDemographic_revertPhysicianNameAndOhip_revertSuccessful() {
    val referralOperation = createDemographicMergeOperation(REFERRAL_DOCTOR_OPERATION, DOCTOR_NAME + "," + DOCTOR_OHIP);
    val familyOperation = createDemographicMergeOperation(FAMILY_DOCTOR_OPERATION, DOCTOR_NAME + "," + DOCTOR_OHIP);

    Mockito.when(query.getResultList()).thenReturn(Arrays.asList(referralOperation, familyOperation));
    Mockito.when(demographicDao.getDemographic(mainDemographic.getDemographicNo())).thenReturn(mainDemographic);
    Mockito
        .when(demographicExtDao.getDemographicExt(Mockito.anyInt(), Mockito.any(DemographicExtKey.class)))
        .thenReturn(demographicExtName, demographicExtName, demographicExtOhip, demographicExtOhip);
    demographicMergeOperationDao.revertUpdateDemographic(
        oldDemographic.getDemographicNo(),
        mainDemographic.getDemographicNo(),
        "999998"
    );

    assertEquals(DOCTOR_NAME, mainDemographic.getReferralPhysicianName());
    assertEquals(DOCTOR_NAME, mainDemographic.getFamilyPhysicianName());
    assertEquals(DOCTOR_OHIP, mainDemographic.getReferralPhysicianOhip());
    assertEquals(DOCTOR_OHIP, mainDemographic.getFamilyPhysicianOhip());
  }

  @Test
  public void revertUpdateDemographic_revertPhysicianName_revertSuccessful() {
    val referralOperation = createDemographicMergeOperation(REFERRAL_DOCTOR_OPERATION, DOCTOR_NAME);
    val familyOperation = createDemographicMergeOperation(FAMILY_DOCTOR_OPERATION, DOCTOR_NAME);

    Mockito.when(query.getResultList()).thenReturn(Arrays.asList(referralOperation, familyOperation));
    Mockito.when(demographicDao.getDemographic(mainDemographic.getDemographicNo())).thenReturn(mainDemographic);
    Mockito
        .when(demographicExtDao.getDemographicExt(Mockito.anyInt(), Mockito.any(DemographicExtKey.class)))
        .thenReturn(demographicExtName, demographicExtName, null);
    demographicMergeOperationDao.revertUpdateDemographic(
        oldDemographic.getDemographicNo(),
        mainDemographic.getDemographicNo(),
        "999998"
    );

    assertEquals(DOCTOR_NAME, mainDemographic.getReferralPhysicianName());
    assertEquals(DOCTOR_NAME, mainDemographic.getFamilyPhysicianName());
    assertEquals(StringUtils.EMPTY, mainDemographic.getReferralPhysicianOhip());
    assertEquals(StringUtils.EMPTY, mainDemographic.getFamilyPhysicianOhip());
  }

  @Test
  public void revertUpdateDemogrpahic_emptyContent_noRevert() {
    val referralOperation = createDemographicMergeOperation(REFERRAL_DOCTOR_OPERATION, StringUtils.EMPTY);
    val familyOperation = createDemographicMergeOperation(FAMILY_DOCTOR_OPERATION, StringUtils.EMPTY);

    Mockito.when(query.getResultList()).thenReturn(Arrays.asList(referralOperation, familyOperation));
    demographicMergeOperationDao.revertUpdateDemographic(
        oldDemographic.getDemographicNo(),
        mainDemographic.getDemographicNo(),
        "999998"
    );
    // assumes physician extension values are empty strings before revert, and asserts that they are still empty after
    assertEquals(StringUtils.EMPTY, mainDemographic.getReferralPhysicianName());
    assertEquals(StringUtils.EMPTY, mainDemographic.getFamilyPhysicianName());
    assertEquals(StringUtils.EMPTY, mainDemographic.getReferralPhysicianOhip());
    assertEquals(StringUtils.EMPTY, mainDemographic.getFamilyPhysicianOhip());
  }

  @Test
  public void updateDemographicPhysicianExtension_emptyContent_referralPhysicianDataNotReverted() {
    val referralOperation = createDemographicMergeOperation(REFERRAL_DOCTOR_OPERATION, StringUtils.EMPTY);

      demographicMergeOperationDao.updateDemographicPhysicianExtension(
          mainDemographic.getDemographicNo(),
          referralOperation.getContentID(),
          DemographicMergeOperationDao.REFERRAL_PHYSICIAN_TYPE
      );
      assertEquals(StringUtils.EMPTY, mainDemographic.getReferralPhysicianName());
      assertEquals(StringUtils.EMPTY, mainDemographic.getReferralPhysicianOhip());
  }

  @Test
  public void updateDemographicPhysicianExtension_emptyContent_familyPhysicianDataNotReverted() {
    val familyOperation = createDemographicMergeOperation(FAMILY_DOCTOR_OPERATION, StringUtils.EMPTY);

    demographicMergeOperationDao.updateDemographicPhysicianExtension(
        mainDemographic.getDemographicNo(),
        familyOperation.getContentID(),
        DemographicMergeOperationDao.FAMILY_PHYSICIAN_TYPE
    );
    assertEquals(StringUtils.EMPTY, mainDemographic.getFamilyPhysicianName());
    assertEquals(StringUtils.EMPTY, mainDemographic.getFamilyPhysicianOhip());
  }

  @Test
  public void updateDemographicPhysicianExtension_nullDemographic_referralPhysicianNotReverted() {
    val referralOperation = createDemographicMergeOperation(
        REFERRAL_DOCTOR_OPERATION,
        DOCTOR_NAME + "," + DOCTOR_OHIP
    );

    Mockito.when(demographicDao.getDemographic(Mockito.anyInt())).thenReturn(null);
    demographicMergeOperationDao.updateDemographicPhysicianExtension(
        mainDemographic.getDemographicNo(),
        referralOperation.getContentID(),
        DemographicMergeOperationDao.REFERRAL_PHYSICIAN_TYPE
    );

    assertEquals(StringUtils.EMPTY, mainDemographic.getReferralPhysicianName());
    assertEquals(StringUtils.EMPTY, mainDemographic.getReferralPhysicianOhip());
  }

  @Test
  public void updateDemographicPhysicianExtension_nullDemographic_familyPhysicianNotReverted() {
    val familyOperation = createDemographicMergeOperation(
        FAMILY_DOCTOR_OPERATION,
        DOCTOR_NAME + "," + DOCTOR_OHIP
    );

    Mockito.when(demographicDao.getDemographic(Mockito.anyInt())).thenReturn(null);
    demographicMergeOperationDao.updateDemographicPhysicianExtension(
        mainDemographic.getDemographicNo(),
        familyOperation.getContentID(),
        DemographicMergeOperationDao.FAMILY_PHYSICIAN_TYPE
    );

    assertEquals(StringUtils.EMPTY, mainDemographic.getFamilyPhysicianName());
    assertEquals(StringUtils.EMPTY, mainDemographic.getFamilyPhysicianOhip());
  }

  @Test
  public void updateDemographicPhysicianExtension_wrongContentFormat_noRevert() {
    val referralOperation = createDemographicMergeOperation(
        REFERRAL_DOCTOR_OPERATION,
        "Test Doctor" + " " + DOCTOR_OHIP
    );

    Mockito.when(demographicDao.getDemographic(Mockito.anyInt())).thenReturn(mainDemographic);
    demographicMergeOperationDao.updateDemographicPhysicianExtension(
        mainDemographic.getDemographicNo(),
        referralOperation.getContentID(),
        DemographicMergeOperationDao.REFERRAL_PHYSICIAN_TYPE
    );

    assertEquals(StringUtils.EMPTY, mainDemographic.getReferralPhysicianName());
    assertEquals(StringUtils.EMPTY, mainDemographic.getReferralPhysicianOhip());
    assertEquals(StringUtils.EMPTY, mainDemographic.getFamilyPhysicianName());
    assertEquals(StringUtils.EMPTY, mainDemographic.getFamilyPhysicianOhip());
  }

  @Test
  public void updateDemographicPhysicianExtension_correctContentFormat_referralPhysicianReverted() {
    val referralOperation = createDemographicMergeOperation(
        REFERRAL_DOCTOR_OPERATION,
        DOCTOR_NAME + "," + DOCTOR_OHIP
    );

    Mockito.when(demographicDao.getDemographic(Mockito.anyInt())).thenReturn(mainDemographic);
    Mockito.when(demographicExtDao.getDemographicExt(Mockito.anyInt(), Mockito.any(DemographicExtKey.class)))
        .thenReturn(demographicExtName, demographicExtOhip);
    demographicMergeOperationDao.updateDemographicPhysicianExtension(
        mainDemographic.getDemographicNo(),
        referralOperation.getContentID(),
        DemographicMergeOperationDao.REFERRAL_PHYSICIAN_TYPE
    );

    assertEquals(DOCTOR_NAME, mainDemographic.getReferralPhysicianName());
    assertEquals(DOCTOR_OHIP, mainDemographic.getReferralPhysicianOhip());
  }

  @Test
  public void updateDemographicPhysicianExtension_onlyDoctorName_physicianNameRevertedOnly() {
    val referralOperation = createDemographicMergeOperation(
        REFERRAL_DOCTOR_OPERATION,
        DOCTOR_NAME
    );

    Mockito.when(demographicDao.getDemographic(Mockito.anyInt())).thenReturn(mainDemographic);
    Mockito.when(demographicExtDao.getDemographicExt(Mockito.anyInt(), Mockito.any(DemographicExtKey.class)))
        .thenReturn(demographicExtName, null);
    demographicMergeOperationDao.updateDemographicPhysicianExtension(
        mainDemographic.getDemographicNo(),
        referralOperation.getContentID(),
        DemographicMergeOperationDao.REFERRAL_PHYSICIAN_TYPE
    );

    assertEquals(DOCTOR_NAME, mainDemographic.getReferralPhysicianName());
    assertEquals(StringUtils.EMPTY, mainDemographic.getReferralPhysicianOhip());
  }

  private DemographicMergeOperation createDemographicMergeOperation(
      final String operationType,
      final String content
  ) {
    val operation = new DemographicMergeOperation();
    operation.setMainDemographic(mainDemographic.getDemographicNo());
    operation.setOldDemographic(oldDemographic.getDemographicNo());
    operation.setContentType(operationType);
    operation.setContentID(content);
    return operation;
  }
}