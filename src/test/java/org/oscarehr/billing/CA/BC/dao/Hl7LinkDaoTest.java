/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.billing.CA.BC.dao;

import ca.oscarpro.test.TagConstants;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.oscarehr.billing.CA.BC.model.Hl7Link;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.util.SpringUtils;

@Tag(TagConstants.BC)
public class Hl7LinkDaoTest extends DaoTestFixtures {

	public Hl7LinkDao dao = SpringUtils.getBean(Hl7LinkDao.class);

	@BeforeEach
	public void before() throws Exception {
		SchemaUtils.restoreTable("hl7_link", "hl7_pid", "hl7_link", "hl7_obr", "demographic", "lst_gender", "admission","demographic_merged",
				"hl7_message", "program", "health_safety","provider","providersite","site","program_team");
	}

	@Test
	public void testCreate() throws Exception {
		Hl7Link entity = new Hl7Link();
		EntityDataGenerator.generateTestDataForModelClass(entity);
		entity.setId(1);
		dao.persist(entity);
		Assertions.assertNotNull(entity.getId());
	}

	@Test
	public void testFindLabs() {
		Assertions.assertNotNull(dao.findLabs());
	}

	@Test
	public void testFindMagicLinks() {
		Assertions.assertNotNull(dao.findMagicLinks());
	}

    @Test
    public void testFindLinksAndRequestDates() {
	    Assertions.assertNotNull(dao.findLinksAndRequestDates(100));
    }

    @Test
    public void testFindReports() {
	    Assertions.assertNotNull(
					dao.findReports(new Date(), new Date(), "-ULL", "patient_name", "CMD"));
	    Assertions.assertNotNull(
					dao.findReports(new Date(), new Date(), "-APL", "patient_name", "CMD"));
	    Assertions.assertNotNull(
					dao.findReports(new Date(), new Date(), "-UAP", "patient_name", "CMD"));
    }

	@Override
    protected List<String> getSimpleExceptionTestExcludes() {
		List<String> result = super.getSimpleExceptionTestExcludes();
		result.add("findReports");
	    return result;
    }

}
