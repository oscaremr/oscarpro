/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.billing.CA.BC.dao;

import ca.oscarpro.test.TagConstants;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.oscarehr.billing.CA.BC.model.Wcb;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.util.SpringUtils;

@Tag(TagConstants.BC)
public class WcbDaoTest extends DaoTestFixtures {

	public WcbDao dao = SpringUtils.getBean(WcbDao.class);


	@BeforeEach
	public void before() throws Exception {
		SchemaUtils.restoreTable("wcb");
	}

	@Test
	public void testCreate() throws Exception {
		Wcb entity = new Wcb();
		EntityDataGenerator.generateTestDataForModelClass(entity);
		dao.persist(entity);
		Assertions.assertNotNull(entity.getId());
	}

	@Test
	public void testFindByDemographic() {
			Assertions.assertNotNull(dao.findByDemographic(100));
	}
}
