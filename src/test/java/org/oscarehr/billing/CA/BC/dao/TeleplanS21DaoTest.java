/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.billing.CA.BC.dao;

import ca.oscarpro.test.TagConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.oscarehr.billing.CA.BC.model.TeleplanS21;
import org.oscarehr.billing.CA.BC.model.TeleplanVRC;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.util.SpringUtils;

import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

// Needs to be re-enabled after the fix in OSCARPRO-5231
@Disabled
@Tag(TagConstants.BC)
public class TeleplanS21DaoTest extends DaoTestFixtures {
  private TeleplanS21Dao s21Dao = SpringUtils.getBean(TeleplanS21Dao.class);
  private TeleplanVRCDao vrcDao = SpringUtils.getBean(TeleplanVRCDao.class);
  private TeleplanS21 duplicateEntry = new TeleplanS21();
  private TeleplanS21 duplicateEntryWithVrc = new TeleplanS21();
  private TeleplanVRC vrc = new TeleplanVRC();

  @BeforeEach
  public void before() throws Exception {
    SchemaUtils.restoreTable("teleplanS21", "teleplanVRC");

    duplicateEntry.setDataCentre("abc");
    duplicateEntry.setDataSeq("def");
    duplicateEntry.setMspCtlNo("123");
    duplicateEntry.setPayment("456");
    duplicateEntry.setPayeeNo("789");
    duplicateEntry.setAmountBilled("100.0");
    duplicateEntry.setAmountPaid("100.0");
    duplicateEntry.setVrcId(null);
    s21Dao.persist(duplicateEntry);

    vrc.setDataCentre(duplicateEntry.getDataCentre());
    vrc.setFilename("test");
    vrc.setPaymentDate(new Date());
    vrc.setRecordCount(1);
    vrcDao.persist(vrc);

    duplicateEntryWithVrc.setDataCentre("aaa");
    duplicateEntryWithVrc.setDataSeq("bbb");
    duplicateEntryWithVrc.setMspCtlNo("111");
    duplicateEntryWithVrc.setPayment("222");
    duplicateEntryWithVrc.setPayeeNo("333");
    duplicateEntryWithVrc.setAmountBilled("100.0");
    duplicateEntryWithVrc.setAmountPaid("100.0");
    duplicateEntryWithVrc.setVrcId(vrc.getId());
    s21Dao.persist(duplicateEntryWithVrc);
  }

  @Test
  public void testCreate() throws Exception {
    TeleplanS21 entity = new TeleplanS21();
    EntityDataGenerator.generateTestDataForModelClass(entity);
    entity.setVrcId(null);
    s21Dao.persist(entity);
    assertNotNull(entity.getId());
  }

  @Test
  public void givenDuplicateS21Entry_whenFindDuplicateS21Entries_thenReturnDuplicateEntry() {
    // Ensure entry with a null VRC ID is considered a duplicate when all fields match
    List<TeleplanS21> duplicateS21List = s21Dao.findDuplicateS21Entries(duplicateEntry, null);
    assertEquals(1, duplicateS21List.size());
  }

  @Test
  public void givenS21Entry_whenFindDuplicateS21Entries_thenReturnNoDuplicateEntry() {
    duplicateEntry.setPayeeNo("111");
    List<TeleplanS21> duplicateS21List = s21Dao.findDuplicateS21Entries(duplicateEntry, null);
    assertEquals(0, duplicateS21List.size());
    s21Dao.merge(duplicateEntry);
  }

  @Test
  public void givenS21EntryWithVrc_whenFindDuplicateS21Entries_thenReturnDuplicateEntry() {
    List<TeleplanS21> duplicateS21List = s21Dao.findDuplicateS21Entries(duplicateEntryWithVrc, vrc.getId());
    assertEquals(1, duplicateS21List.size());
  }

  @Test
  public void givenS21EntryWithoutMatchingVrc_whenFindDuplicateS21Entries_thenReturnNoDuplicateEntry() {
    List<TeleplanS21> duplicateS21List = s21Dao.findDuplicateS21Entries(duplicateEntryWithVrc, vrc.getId() + 1);
    assertEquals(0, duplicateS21List.size());
  }
}
