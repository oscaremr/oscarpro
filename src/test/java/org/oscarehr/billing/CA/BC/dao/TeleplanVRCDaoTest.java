package org.oscarehr.billing.CA.BC.dao;

import ca.oscarpro.test.TagConstants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.oscarehr.billing.CA.BC.model.TeleplanVRC;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.util.SpringUtils;
import oscar.util.ConversionUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

// Needs to be re-enabled after the fix in OSCARPRO-5231
@Disabled
@Tag(TagConstants.BC)
public class TeleplanVRCDaoTest extends DaoTestFixtures {
  private TeleplanVRCDao dao = SpringUtils.getBean(TeleplanVRCDao.class);

  @BeforeEach
  public void before() throws Exception {
    SchemaUtils.restoreTable("teleplanVRC");
    TeleplanVRC vrcEntry = new TeleplanVRC();
    vrcEntry.setFilename("abc");
    vrcEntry.setDataCentre("def");
    vrcEntry.setPaymentDate(ConversionUtils.fromDateString("20240101", "yyyyMMdd"));
    vrcEntry.setRecordCount(1);
    dao.persist(vrcEntry);
  }

  @Test
  public void givenMatchingFields_whenFindByFilenameDataCentreDateCount_thenReturnVrcEntry() {
    TeleplanVRC vrcEntry = dao.findByFilenameDataCentreDateCount("abc", "def",
        ConversionUtils.fromDateString("20240101", "yyyyMMdd"), 1);
    assertEquals("abc", vrcEntry.getFilename());
    assertEquals("def", vrcEntry.getDataCentre());
    assertEquals(ConversionUtils.fromDateString("20240101", "yyyyMMdd"), vrcEntry.getPaymentDate());
    assertEquals(1, vrcEntry.getRecordCount());
  }

  @Test
  public void givenNoMatchingFields_whenFindByFilenameDataCentreDateCount_thenReturnNull() {
    TeleplanVRC vrcEntry = dao.findByFilenameDataCentreDateCount("abc", "def",
        ConversionUtils.fromDateString("20240101", "yyyyMMdd"), 2);
    assertNull(vrcEntry);
  }
}
