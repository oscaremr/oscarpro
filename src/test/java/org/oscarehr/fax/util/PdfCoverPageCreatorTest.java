package org.oscarehr.fax.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.oscarehr.common.dao.ClinicDAO;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class PdfCoverPageCreatorTest {

  @Mock
  private ClinicDAO clinicDao;

  @InjectMocks
  private PdfCoverPageCreator pdfCoverPageCreator;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.initMocks(this);
    Mockito.when(clinicDao.getClinic()).thenReturn(null);
  }

  private String getPdfTextFromBytes(byte[] result) throws IOException {
    ByteArrayInputStream stream = new ByteArrayInputStream(result);
    PDDocument document = PDDocument.load(stream);

    PDFTextStripper pdfStripper = new PDFTextStripper();
    String text = pdfStripper.getText(document);
    document.close();
    return text;

  }

  @Test
  public void givenNote_whenCreateCoverPage_thenResultHasNote() throws IOException {
    String note = "note";
    Mockito.when(clinicDao.getClinic()).thenReturn(null);

    byte[] result = pdfCoverPageCreator.createCoverPage(note);

    String actualPdfContent = getPdfTextFromBytes(result);
    actualPdfContent = actualPdfContent.replaceAll("\r\n", "\n");
    String expectedPdfContent = "OSCAR\nnote\n";
    System.out.println("Actual: " + actualPdfContent);
    System.out.println("Expected: " + expectedPdfContent);
    assertEquals(actualPdfContent, expectedPdfContent);

  }

  private static Object[] customCoverPageParameters() {
    return new Object[]{
        new Object[]{"positive_control", "subject", "message", "OSCAR\nsubject\nmessage\n"},
        new Object[]{"no_note", "subject", null, "OSCAR\nsubject\n"},
        new Object[]{"no_subject", null, "message", "OSCAR\nmessage\n"},
        new Object[]{"neither_subject_nor_message", null, null, "OSCAR\n"},
        new Object[]{"empty_message", "subject", "", "OSCAR\nsubject\n"},
        new Object[]{"empty_subject", "", "message", "OSCAR\nmessage\n"},
        new Object[]{"empty_subject_and_message", "", "", "OSCAR\n"}
    };
  }

  @ParameterizedTest
  @MethodSource("customCoverPageParameters")
  public void givenSubjectAndMessage_whenCreateCoverPage_thenResultHasSubjectAndMessage(
      String description, String subject, String message, String expectedPdfContent)
      throws IOException {

    byte[] result = pdfCoverPageCreator.createCoverPage(subject, message);

    String actualPdfContent = getPdfTextFromBytes(result);
    actualPdfContent = actualPdfContent.replaceAll("\r\n", "\n");
    assert (actualPdfContent.equals(expectedPdfContent));
  }

}
