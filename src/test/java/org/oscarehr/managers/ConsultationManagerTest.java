package org.oscarehr.managers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.lowagie.text.DocumentException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import lombok.val;
import lombok.var;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.ConsultDocsDao;
import org.oscarehr.common.dao.ConsultRequestDao;
import org.oscarehr.common.dao.EReferAttachmentDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.ConsultDocs;
import org.oscarehr.common.model.ConsultationRequest;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EReferAttachment;
import org.oscarehr.common.model.EReferAttachmentData;
import org.oscarehr.common.model.EReferAttachmentManagerData;
import org.oscarehr.common.model.OceanWorkflowTypeEnum;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.ws.rest.to.model.OceanApiAttachment;
import oscar.dms.EDoc;
import oscar.form.dao.ONPerinatal2017Dao;
import oscar.form.model.FormONPerinatal2017;
import oscar.log.LogAction;
import oscar.log.LogService;
import oscar.oscarLab.ca.on.LabResultData;

@ExtendWith(MockitoExtension.class)
public class ConsultationManagerTest {

  private static final Integer DEMOGRAPHIC_NO = 1;
  private static final String FILE_NAME = "filename.pdf";
  private static final String FILE_PATH = "path/to/" + FILE_NAME;
  private static final byte[] PDF_BYTES = new byte[]{1, 2, 3, 4, 5};
  private static final Integer PRINTABLE_JSON_ID = 52;

  private static final String PRINTABLE_JSON = "{\n"
      + "\"id\":\"" + PRINTABLE_JSON_ID + "\",\n"
      + "\"name\":\"Test Name\",\n"
      + "\"type\":\"Document\",\n"
      + "\"params\":{},\n"
      + "\"previewUrl\":\"/kaiemr/api/document/getPagePreview/" + PRINTABLE_JSON_ID + "?page=1\",\n"
      + "\"date\":null,\n"
      + "\"supported\":true,\n"
      + "\"printableAttachments\":[]\n"
      + "}";

  private static final Integer LEGACY_ID = 1;
  private static final String LEGACY_FILE_NAME = "legacy.file";
  private static final byte[] LEGACY_PDF_BYTES = new byte[]{1, 101, 102, 103};

  @InjectMocks
  @Spy
  private ConsultationManager consultationManager;

  @Mock
  private EReferAttachmentDao eReferAttachmentDao;
  @Mock
  private LogService logService;
  @Mock
  private SystemPreferencesDao systemPreferencesDao;
  @Mock ConsultRequestDao consultationRequestDao;
  @Mock ConsultDocsDao requestDocDao;
  @Mock
  ONPerinatal2017Dao onPerinatal2017Dao;
  @Mock private SecurityInfoManager securityInfoManager;
  private static MockedStatic<LogAction> logActionMock;

  @Mock
  private HttpServletRequest request;
  @Mock
  private LoggedInInfo loggedInInfo;

  @BeforeAll
  public static void beforeAll() {
    logActionMock = mockStatic(LogAction.class);
  }

  @AfterAll
  public static void afterAll() {
    logActionMock.close();
  }

  @Test
  public void givenNoAttachments_whenGetLatestDemographicEReferOceanApiAttachments_thenReturnNoResults()
      throws Exception {
    when(eReferAttachmentDao.getLatestCreatedByDemographic(
        DEMOGRAPHIC_NO, OceanWorkflowTypeEnum.EREFERRAL, null))
        .thenReturn(null);

    val resultList = consultationManager.getLatestDemographicEReferOceanApiAttachments(
        loggedInInfo, request, DEMOGRAPHIC_NO, OceanWorkflowTypeEnum.EREFERRAL);

    verify(consultationManager, never())
        .printAttachmentManagerDataAndAddToOceanApiAttachmentList(
            any(), any(), any(), any());
    verify(consultationManager, never())
        .generateLegacyEReferAttachmentDataFilesAndAddToOceanApiAttachmentList(
            any(), any(), any(), any(), any());

    assertTrue(resultList.isEmpty(), "The result should be empty");
  }

  @Test
  public void givenValidAttachmentAndFeatureFlagEnabled_whenGetLatestDemographicEReferOceanApiAttachments_thenReturnResults()
      throws Exception {
    when(systemPreferencesDao.isAttachmentManagerConsultationEnabled()).thenReturn(true);
    doReturn(FILE_PATH).when(consultationManager).printAndGetFilePath(any(), any());
    doReturn(PDF_BYTES).when(consultationManager).getBytesFromFile(FILE_PATH);
    when(eReferAttachmentDao.getLatestCreatedByDemographic(
        DEMOGRAPHIC_NO, OceanWorkflowTypeEnum.EREFERRAL, null))
        .thenReturn(getTestEReferAttachment());

    val resultList = consultationManager.getLatestDemographicEReferOceanApiAttachments(
        loggedInInfo, request, DEMOGRAPHIC_NO, OceanWorkflowTypeEnum.EREFERRAL);

    verify(consultationManager, times(1))
        .printAttachmentManagerDataAndAddToOceanApiAttachmentList(
            any(), any(), any(), any());
    verify(consultationManager, never())
        .generateLegacyEReferAttachmentDataFilesAndAddToOceanApiAttachmentList(
            any(), any(), any(), any(), any());

    assertEquals(1, resultList.size());
    val oceanApiAttachment = resultList.get(0);
    assertEquals(PRINTABLE_JSON_ID, oceanApiAttachment.getId());
    assertEquals(OceanWorkflowTypeEnum.AM.name(), oceanApiAttachment.getAttachmentType());
    assertEquals(FILE_NAME, oceanApiAttachment.getFileName());
    assertEquals(PDF_BYTES, oceanApiAttachment.getData());
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        ConsultDocs.DOCTYPE_DOC,
        ConsultDocs.DOCTYPE_LAB,
        ConsultDocs.DOCTYPE_HRM,
        ConsultDocs.DOCTYPE_EFORM,
        ConsultDocs.DOCTYPE_FORM_PERINATAL
      })
  public void
      givenLegacyAttachmentAndFeatureFlagDisabled_whenGetLatestDemographicEReferOceanApiAttachments_thenReturnResults(
          final String consultDocsType) throws Exception {
    mockLegacyAttachmentMethod(consultDocsType);
    when(systemPreferencesDao.isAttachmentManagerConsultationEnabled()).thenReturn(false);
    when(eReferAttachmentDao.getLatestCreatedByDemographic(
        DEMOGRAPHIC_NO, OceanWorkflowTypeEnum.EREFERRAL, null))
        .thenReturn(getTestEReferAttachment(consultDocsType));

    val resultList = consultationManager.getLatestDemographicEReferOceanApiAttachments(
        loggedInInfo, request, 1, OceanWorkflowTypeEnum.EREFERRAL);

    verify(consultationManager, never())
        .printAttachmentManagerDataAndAddToOceanApiAttachmentList(
            any(), any(), any(), any());
    verify(consultationManager, times(1))
        .generateLegacyEReferAttachmentDataFilesAndAddToOceanApiAttachmentList(
            any(), any(), any(), any(), any());

    assertEquals(1, resultList.size());
    val oceanApiAttachment = resultList.get(0);
    assertEquals(LEGACY_ID, oceanApiAttachment.getId());
    assertEquals(consultDocsType, oceanApiAttachment.getAttachmentType());
    assertEquals(LEGACY_FILE_NAME, oceanApiAttachment.getFileName());
    assertEquals(LEGACY_PDF_BYTES, oceanApiAttachment.getData());
  }

  private EReferAttachment getTestEReferAttachment() {
    return getTestEReferAttachment(ConsultDocs.DOCTYPE_DOC);
  }

  private EReferAttachment getTestEReferAttachment(final String consultDocsType) {
    val eReferAttachment = new EReferAttachment();
    eReferAttachment.setId(1);
    eReferAttachment.setDemographicNo(1);
    eReferAttachment.setCreated(new Date());
    eReferAttachment.setArchived(false);
    eReferAttachment.setType(OceanWorkflowTypeEnum.EREFERRAL.name());

    // populate attachments list
    val attachment = new EReferAttachmentData(
        eReferAttachment, 1, consultDocsType);
    eReferAttachment.setAttachments(new ArrayList<>(Collections.singletonList(attachment)));

    // populate attachment manager data list
    val eReferAttachmentData = new EReferAttachmentManagerData();
    eReferAttachmentData.setEReferAttachment(eReferAttachment);
    eReferAttachmentData.setPrintable(PRINTABLE_JSON);
    eReferAttachment.setAttachmentManagerData(new ArrayList<>(
        Collections.singletonList(eReferAttachmentData)));

    return eReferAttachment;
  }

  private void mockLegacyAttachmentMethod(final String consultDocsType)
      throws IOException, DocumentException {
    switch (consultDocsType) {
      case ConsultDocs.DOCTYPE_DOC:
        doReturn(new OceanApiAttachment(
            LEGACY_ID, ConsultDocs.DOCTYPE_DOC, LEGACY_FILE_NAME, LEGACY_PDF_BYTES))
            .when(consultationManager).getDocumentAttachment(any(), any());
        break;
      case ConsultDocs.DOCTYPE_LAB:
        doReturn(new OceanApiAttachment(
            LEGACY_ID, ConsultDocs.DOCTYPE_LAB, LEGACY_FILE_NAME, LEGACY_PDF_BYTES))
            .when(consultationManager).getLabAttachment(any(), any());
        break;
      case ConsultDocs.DOCTYPE_HRM:
        doReturn(new OceanApiAttachment(
            LEGACY_ID, ConsultDocs.DOCTYPE_HRM, LEGACY_FILE_NAME, LEGACY_PDF_BYTES))
            .when(consultationManager).getHrmAttachment(any(), any());
        break;
      case ConsultDocs.DOCTYPE_EFORM:
        doReturn(new OceanApiAttachment(
            LEGACY_ID, ConsultDocs.DOCTYPE_EFORM, LEGACY_FILE_NAME, LEGACY_PDF_BYTES))
            .when(consultationManager).getEformAttachment(any(), any(), any());
        break;
      case ConsultDocs.DOCTYPE_FORM_PERINATAL:
        doReturn(
                new OceanApiAttachment(
                    LEGACY_ID,
                    ConsultDocs.DOCTYPE_FORM_PERINATAL,
                    LEGACY_FILE_NAME,
                    LEGACY_PDF_BYTES))
            .when(consultationManager)
            .getPerinatalAttachment(any(), any(), any());
        break;
      default:
        throw new IllegalArgumentException("Invalid consultDocsType: " + consultDocsType);
    }
  }

  @Test
  public void givenLegacyAttachments_whenGetRequestAttachments_thenReturnLegacyResults() {

    initConsultationLegacyAttachmentsMocks();

    val result = consultationManager.getRequestAttachments(loggedInInfo, 1, true, "1");

    assertEquals(8, result.size());
  }

  @Test
  public void givenAttachmentManagerAttachments_whenGetRequestAttachments_thenReturnResults() {

    when(securityInfoManager.hasPrivilege(any(LoggedInInfo.class), eq("_con"), eq("r"), eq(null)))
        .thenReturn(true);

    val consultationRequestMock = new ConsultationRequest();
    consultationRequestMock.setId(1);
    consultationRequestMock.setLegacyAttachment(false);
    consultationRequestMock.setAttachments(
        "[{\"id\":\"Perinatal-8\",\"name\":\"Perinatal 2017\",\"type\":\"Antenatal\",\"params\":{},\"previewUrl\":\"/oscar/printable/antenatal-form?preview=true&id=Perinatal-8&providerNumber=1\",\"date\":null,\"supported\":true},{\"id\":\"83\",\"name\":\"Test PDF\",\"type\":\"Document\",\"params\":{},\"previewUrl\":\"/kaiemr/api/document/getPagePreview/83?page=1\",\"date\":1716955200000,\"supported\":true},{\"id\":\"80\",\"name\":\"annie small\",\"type\":\"Document\",\"params\":{},\"previewUrl\":\"/kaiemr/api/document/getPagePreview/80?page=1\",\"date\":1716523200000,\"supported\":true},{\"id\":\"19\",\"name\":\"Rich Text Letter Best Version\",\"type\":\"Eforms\",\"params\":{},\"previewUrl\":\"/oscar/printable/eform?preview=true&id=19&providerNumber=1\",\"date\":1717012358000,\"supported\":true},{\"id\":\"18\",\"name\":\"Rich Text Letter Best Version\",\"type\":\"Eforms\",\"params\":{},\"previewUrl\":\"/oscar/printable/eform?preview=true&id=18&providerNumber=1\",\"date\":1717012340000,\"supported\":true}]");

    when(consultationRequestDao.find(1)).thenReturn(consultationRequestMock);

    val result = consultationManager.getRequestAttachments(loggedInInfo, 1, true, "1");

    assertEquals(5, result.size());
  }

  @Test
  public void
      givenAttachmentManagerNoAttachments_whenGetRequestAttachments_thenReturnEmptyResults() {
    when(securityInfoManager.hasPrivilege(any(LoggedInInfo.class), eq("_con"), eq("r"), eq(null)))
        .thenReturn(true);

    val consultationRequestMock = new ConsultationRequest();
    consultationRequestMock.setId(1);
    consultationRequestMock.setLegacyAttachment(false);
    consultationRequestMock.setAttachments("");
    when(consultationRequestDao.find(1)).thenReturn(consultationRequestMock);

    val result = consultationManager.getRequestAttachments(loggedInInfo, 1, true, "1");

    assertEquals(0, result.size());
  }
  public void initConsultationLegacyAttachmentsMocks() {
    when(securityInfoManager.hasPrivilege(any(LoggedInInfo.class), eq("_con"), eq("r"), eq(null)))
        .thenReturn(true);
    val consultationRequestMock = new ConsultationRequest();
    consultationRequestMock.setId(1);
    consultationRequestMock.setLegacyAttachment(true);
    when(consultationRequestDao.find(1)).thenReturn(consultationRequestMock);

    val mockEDocs = new ArrayList<EDoc>();
    mockEDocs.add(new EDoc());
    mockEDocs.add(new EDoc());
    doReturn(mockEDocs)
        .when(consultationManager)
        .listLegacyEDocAttachments(eq(loggedInInfo), eq("1"), eq("1"), eq(false), eq(true));

    val mockConsultDocs = new ArrayList<ConsultDocs>();
    var consultDoc = new ConsultDocs();
    consultDoc.setDocType(ConsultDocs.DOCTYPE_EFORM);
    consultDoc.setDocumentNo(1);
    mockConsultDocs.add(consultDoc);
    consultDoc = new ConsultDocs();
    consultDoc.setDocType(ConsultDocs.DOCTYPE_EFORM);
    consultDoc.setDocumentNo(2);
    mockConsultDocs.add(consultDoc);
    when(requestDocDao.findByRequestId(1)).thenReturn(mockConsultDocs);

    val mockEForms = new ArrayList<EFormData>();
    var eform = new EFormData();
    eform.setId(1);
    mockEForms.add(eform);
    eform = new EFormData();
    eform.setId(2);
    mockEForms.add(eform);
    doReturn(mockEForms).when(consultationManager).listLegacyEFormAttachments(eq("1"));

    val mockLabResults = new ArrayList<LabResultData>();
    mockLabResults.add(new LabResultData("HL7"));
    mockLabResults.add(new LabResultData("HL7"));
    doReturn(mockLabResults)
        .when(consultationManager)
        .listLegacyLabResultsAttachments(eq(loggedInInfo), eq("1"), eq("1"), eq(true));

    val perinatalResults = new ArrayList<FormONPerinatal2017>();
    perinatalResults.add(new FormONPerinatal2017());
    perinatalResults.add(new FormONPerinatal2017());
    doReturn(perinatalResults).when(onPerinatal2017Dao).findAllDistinctForms(eq(1));
  }
}
