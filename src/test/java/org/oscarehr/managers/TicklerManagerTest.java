package org.oscarehr.managers;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.model.Tickler;

@ExtendWith(MockitoExtension.class)
public class TicklerManagerTest {

  @InjectMocks
  private TicklerManager ticklerManager;

  @Test
  public void updateNullTicklerCreateDate() {
    val tickler = new Tickler();
    tickler.setCreateDate(null);

    ticklerManager.updateNullTicklerCreationDate(tickler);
    assertEquals(
        "1899-11-30",
        new SimpleDateFormat("yyyy-MM-dd").format(tickler.getCreateDate()));
  }

  @Test
  public void updateNullTicklerCreateDate_nonNull() {
    val tickler = new Tickler();
    tickler.setCreateDate(new Date());

    ticklerManager.updateNullTicklerCreationDate(tickler);
    assertEquals(
        new SimpleDateFormat("yyyy-MM-dd").format(new Date()),
        new SimpleDateFormat("yyyy-MM-dd").format(tickler.getCreateDate()));
  }
}