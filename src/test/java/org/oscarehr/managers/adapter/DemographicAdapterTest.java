/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.managers.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import lombok.val;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.DemographicArchiveDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.DemographicExtArchiveDao;
import org.oscarehr.common.dao.DemographicExtDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.DemographicArchive;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.DemographicExtArchive;
import org.oscarehr.common.model.DemographicExtKey;
import org.oscarehr.managers.FhirSubscriptionManager;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.orm.hibernate3.HibernateTemplate;
import oscar.OscarProperties;

@ExtendWith(MockitoExtension.class)
public class DemographicAdapterTest {

  public static final Integer DEMOGRAPHIC_NO_VALUE = 1;
  public static final String DEMOGRAPHIC_NO_VALUE_STRING = "1";
  public static final String DELETE_USER = "DELETE_USER";
  public static final String CUSTOM_KEY = "CUSTOM_KEY";
  public static final String CUSTOM_VALUE = "CUSTOM_VALUE";
  public static final String REFERRAL_SOURCE_VALUE = "NOT-OTHER";
  public static final String REFERRAL_DATE_VALUE = "2021-01-01";
  public static final String DEMOGRAPHIC_MISC_ID_VALUE = "100001";
  public static final String PROVIDER_NO_VALUE = "999998";
  public static final String HIN = "hin";
  public static final String EMPTY_HIN = "";
  public static final String NON_EMPTY_HIN = "8181818181";
  public static final String PHYSICIAN_ROW_ID = "1";
  public static final String PHYSICIAN_NAME = "Test, Physician";
  public static final String PHYSICIAN_OHIP = "123456";

  private Demographic demographic;
  private List<DemographicExt> extensions;
  Map<String, Boolean> masterFilePreferences;

  @Mock
  HibernateTemplate demographicDaoTemplate;
  @Mock
  DemographicDao demographicDao;
  @Mock
  DemographicArchiveDao demographicArchiveDao;
  @Mock
  DemographicExtDao demographicExtDao;
  @Mock
  DemographicExtArchiveDao demographicExtArchiveDao;
  @Mock
  SystemPreferencesDao systemPreferencesDao;
  @Mock
  ApplicationEventPublisher applicationEventPublisher;
  @Mock
  FhirSubscriptionManager fhirSubscriptionManager;
  @Mock
  OscarProperties oscarProperties;
  @Mock
  HttpServletRequest request;
  @Mock
  OscarProperties properties;

  @BeforeEach
  public void before() throws ParseException {
    demographicDaoTemplate = mock(HibernateTemplate.class);
    demographicDao = mock(DemographicDao.class);
    demographicArchiveDao = mock(DemographicArchiveDao.class);
    demographicExtDao = mock(DemographicExtDao.class);
    demographicExtArchiveDao = mock(DemographicExtArchiveDao.class);
    systemPreferencesDao = mock(SystemPreferencesDao.class);
    applicationEventPublisher = mock(ApplicationEventPublisher.class);
    fhirSubscriptionManager = mock(FhirSubscriptionManager.class);
    oscarProperties = mock(OscarProperties.class);
    request = mock(HttpServletRequest.class);
    demographic = generateDemographic(DEMOGRAPHIC_NO_VALUE);
    extensions = generateExtensions();

    // assign mocks to service
    DemographicAdapter.assignStaticDaos(
        demographicDaoTemplate,
        demographicDao,
        demographicArchiveDao,
        demographicExtDao,
        demographicExtArchiveDao,
        systemPreferencesDao,
        applicationEventPublisher,
        fhirSubscriptionManager,
        oscarProperties
    );
  }

  @ParameterizedTest
  @MethodSource("listDemographicExtKeyValuesParameterizedTest")
  public void givenDemographicExtension_whenGetDemographicExtensionValueAsStringByIntegerValue_thenNonEmptyPhysicianByDemographicExtensionValue
      (DemographicExtKey demographicExtKey, String physicianValue) {
    val extension = generateExtension(demographicExtKey.getKey(), physicianValue);
    when(demographicExtDao.getDemographicExt(anyInt(), (DemographicExtKey) any()))
        .thenReturn(extension);
    val extensionValue = DemographicAdapter.getDemographicExtensionValueAsString(
        DEMOGRAPHIC_NO_VALUE, demographicExtKey);
    assertEquals(extension.getValue(), extensionValue);
  }

  @ParameterizedTest
  @MethodSource("listDemographicExtKeyValuesParameterizedTest")
  public void givenNullDemographicExtension_whenGetDemographicExtensionValueAsStringByIntegerValue_thenEmptyPhysicianByDemographicExtensionValue
      (DemographicExtKey demographicExtKey) {
    when(demographicExtDao.getDemographicExt(anyInt(), (DemographicExtKey) any()))
        .thenReturn(null);
    val extensionValue = DemographicAdapter.getDemographicExtensionValueAsString(
        DEMOGRAPHIC_NO_VALUE, demographicExtKey);
    assertEquals(StringUtils.EMPTY, extensionValue);
  }

  @ParameterizedTest
  @MethodSource("listDemographicExtKeyValuesParameterizedTest")
  public void givenDemographicExtension_whenGetDemographicExtensionValueAsStringByStringValue_thenNonEmptyPhysicianByDemographicExtensionValue
      (DemographicExtKey demographicExtKey, String physicianValue) {
    val extension = generateExtension(demographicExtKey.getKey(), physicianValue);
    when(demographicExtDao.getDemographicExt(anyInt(), (DemographicExtKey) any()))
        .thenReturn(extension);
    val extensionValue = DemographicAdapter.getDemographicExtensionValueAsString(
        DEMOGRAPHIC_NO_VALUE_STRING, demographicExtKey);
    assertEquals(extension.getValue(), extensionValue);
  }

  @ParameterizedTest
  @MethodSource("listDemographicExtKeyValuesParameterizedTest")
  public void givenNullDemographicExtension_whenGetDemographicExtensionValueAsStringByStringValue_thenEmptyPhysicianByDemographicExtensionValue
      (DemographicExtKey demographicExtKey) {
    when(demographicExtDao.getDemographicExt(anyInt(), (DemographicExtKey) any()))
        .thenReturn(null);
    val extensionValue = DemographicAdapter.getDemographicExtensionValueAsString(
        DEMOGRAPHIC_NO_VALUE_STRING, demographicExtKey);
    assertEquals(StringUtils.EMPTY, extensionValue);
  }

  @ParameterizedTest
  @MethodSource("listDemographicExtKeyValuesParameterizedTest")
  public void givenDemographicExtension_whenGetDemographicExtensionValueAsStringByNullValue_thenNonEmptyPhysicianByDemographicExtensionValue
      (DemographicExtKey demographicExtKey, String physicianValue) {
    val extension = generateExtension(demographicExtKey.getKey(), physicianValue);
    val extensionValue = DemographicAdapter.getDemographicExtensionValueAsString(
        (String) null, demographicExtKey);
    assertEquals(StringUtils.EMPTY, extensionValue);
    assertNotEquals(extension.getValue(), extensionValue);
  }

  @ParameterizedTest
  @MethodSource("listDemographicExtKeyValuesParameterizedTest")
  public void givenDemographicExtension_whenGetDemographicExtensionValueAsStringByEmptyValue_thenNonEmptyPhysicianByDemographicExtensionValue
      (DemographicExtKey demographicExtKey, String physicianValue) {
    val extension = generateExtension(demographicExtKey.getKey(), physicianValue);
    val extensionValue = DemographicAdapter.getDemographicExtensionValueAsString(
        StringUtils.EMPTY, demographicExtKey);
    assertEquals(StringUtils.EMPTY, extensionValue);
    assertNotEquals(extension.getValue(), extensionValue);
  }

  @ParameterizedTest
  @MethodSource("listDemographicExtKeyValuesParameterizedTest")
  public void givenWithoutDemographicExtension_whenGetDemographicExtensionValueAsStringByNullValue_thenEmptyPhysicianByDemographicExtensionValue
      (DemographicExtKey demographicExtKey) {
    val extensionValue = DemographicAdapter.getDemographicExtensionValueAsString(
        (Integer) null, demographicExtKey);
    assertEquals("", extensionValue);
  }

  @ParameterizedTest
  @MethodSource("listDemographicExtKeyValuesParameterizedTest")
  public void givenWithoutDemographicExtension_whenUpdateOrCreateDemographicExtensionByNullValue_thenNullPhysicianByDemographicExtensionValue
      (DemographicExtKey demographicExtKey, String physicianValue) {
    val extensionValue = DemographicAdapter.updateOrCreateDemographicExtension(
        null, demographicExtKey, physicianValue);
    assertNull(extensionValue);
  }

  @ParameterizedTest
  @MethodSource("listDemographicExtKeyValuesParameterizedTest")
  public void givenDemographicNullValue_whenUpdateOrCreateDemographicExtension_thenNullPhysicianByDemographicExtensionValue
      (DemographicExtKey demographicExtKey, String physicianValue) {
    val demographic = generateDemographic(null);
    val extensionValue = DemographicAdapter.updateOrCreateDemographicExtension(
        demographic, demographicExtKey, physicianValue);
    assertNull(extensionValue);
  }

  @ParameterizedTest
  @MethodSource("listDemographicExtKeyValuesParameterizedTest")
  public void givenDemographicExtensionAndDemographicIntegerValue_whenUpdateOrCreateDemographicExtension_thenReturnPhysicianByDemographicExtensionValue
      (DemographicExtKey demographicExtKey, String physicianValue) {
    val extension = generateExtension(demographicExtKey.getKey(), physicianValue);
    val demographic = generateDemographic(DEMOGRAPHIC_NO_VALUE);
    when(demographicExtDao.getDemographicExt(anyInt(), anyString()))
        .thenReturn(extension);
    val newExtension = DemographicAdapter.updateOrCreateDemographicExtension(
        demographic, demographicExtKey, physicianValue);
    assertEquals(extension, newExtension);
  }

  @ParameterizedTest
  @MethodSource("listDemographicExtKeyValuesParameterizedTest")
  public void givenDemographicIntegerValueAndDemographicExtensionNull_whenUpdateOrCreateDemographicExtension_thenReturnPhysicianByDemographicExtensionValue
      (DemographicExtKey demographicExtKey, String physicianValue) {
    val demographic = generateDemographic(DEMOGRAPHIC_NO_VALUE);
    when(demographicExtDao.getDemographicExt(anyInt(), anyString()))
        .thenReturn(null);
    val newExtension = DemographicAdapter.updateOrCreateDemographicExtension(
        demographic, demographicExtKey, physicianValue);
    assertNotNull(newExtension);
  }

  @ParameterizedTest
  @MethodSource("listDemographicExtKeyValuesParameterizedTest")
  public void givenDemographicExtensionNullAndDemographicIntegerValue_whenUpdateOrCreateDemographicExtension_thenReturnNotEqualsPhysicianByDemographicExtensionValue
      (DemographicExtKey demographicExtKey, String physicianValue) {
    val extension = generateExtension(demographicExtKey.getKey(), physicianValue);
    extension.setDemographicNo(2);
    val demographic = generateDemographic(DEMOGRAPHIC_NO_VALUE);
    when(demographicExtDao.getDemographicExt(anyInt(), anyString()))
        .thenReturn(null);
    val newExtension = DemographicAdapter.updateOrCreateDemographicExtension(
        demographic, demographicExtKey, physicianValue);
    assertNotEquals(extension, newExtension);
  }

  /* DEMOGRAPHIC ADAPTER */
  @Test
  public void givenDemographicAndDemographicArchive_whenCreateDemographic_thenVerifySaveAndArchive() {
    val demographic = generateDemographic(2);
    val extensions = new ArrayList<DemographicExt>();
    val demographicArchive = new DemographicArchive(demographic);
    demographicArchive.setId(1L);

    when(demographicArchiveDao.archiveRecord(any(Demographic.class)))
        .thenReturn(demographicArchive);
    when(demographicExtDao.getDemographicExtByDemographicNo(anyInt()))
        .thenReturn(new ArrayList<DemographicExt>());
    DemographicAdapter.createDemographic(demographic);
    verifySaveAndArchive(demographic, extensions,
        true,
        true,
        true,
        true
    );
  }

  @Test
  public void givenNothing_whenCreateDemographic_thenVerifySaveAndArchive() {
    DemographicAdapter.createDemographic(demographic, true, true);
    verifySaveAndArchive(demographic, extensions,
        true,
        false,
        false,
        false
    );
  }

  @Test
  public void givenDemographicArchiveDao_whenCreateDemographic_thenVerifySaveAndArchive() {
    val demographicArchive = new DemographicArchive(demographic);
    demographicArchive.setId(1L);
    when(demographicArchiveDao.archiveRecord(any(Demographic.class)))
        .thenReturn(demographicArchive);
    when(demographicExtDao.getDemographicExtByDemographicNo(anyInt()))
        .thenReturn(new ArrayList<DemographicExt>());
    DemographicAdapter.createDemographic(demographic, extensions);
    verifySaveAndArchive(demographic, extensions,
        true,
        true,
        true,
        true
    );
  }

  @Test
  public void givenDemographicArchive_whenCreateDemographic_thenVerifySaveAndArchive() {
    val demographicArchive = new DemographicArchive(demographic);
    demographicArchive.setId(1L);
    DemographicAdapter.createDemographic(demographic, extensions, true, true);
    verifySaveAndArchive(demographic, extensions,
        true,
        true,
        false,
        false
    );
  }

  @Test
  public void givenDemographicArchive_whenDeleteDemographic_thenVerifyDeleteDemographic() {
    val demographicArchive = new DemographicArchive(demographic);
    demographicArchive.setId(1L);

    when(demographicExtDao.getDemographicExtByDemographicNo(anyInt()))
        .thenReturn(new ArrayList<DemographicExt>());
    when(demographicExtArchiveDao.saveEntity(any(DemographicExtArchive.class)))
        .thenReturn(new DemographicExtArchive());
    when(demographicExtDao.getDemographicExtByDemographicNo(
        eq(DEMOGRAPHIC_NO_VALUE)))
        .thenReturn(generateExtensions());
    when(demographicArchiveDao.archiveRecord(any(Demographic.class)))
        .thenReturn(demographicArchive);

    DemographicAdapter.deleteDemographic(demographic, DELETE_USER);

    verify(demographicArchiveDao, times(2))
        .archiveRecord(any(Demographic.class));
    verify(demographicExtDao, times(2))
        .getDemographicExtByDemographicNo(anyInt());
    verify(demographicExtDao, times(extensions.size()))
        .removeDemographicExt(anyInt());
    verify(demographicExtArchiveDao, times(extensions.size()))
        .saveEntity(any(DemographicExtArchive.class));
  }

  @Test
  public void givenDemographicArchive_whenSaveDemographic_thenVerifySaveDemographic() {
    val demographicArchive = new DemographicArchive(demographic);
    demographicArchive.setId(1L);

    when(demographicDao.getDemographic(anyInt()))
        .thenReturn(generateDemographic(DEMOGRAPHIC_NO_VALUE));
    when(demographicExtDao.getDemographicExtByDemographicNo(
        eq(DEMOGRAPHIC_NO_VALUE)))
        .thenReturn(generateExtensions());
    when(demographicArchiveDao.archiveRecord(any(Demographic.class)))
        .thenReturn(demographicArchive);

    DemographicAdapter.saveDemographic(1);

    verify(demographicDao)
        .getDemographic(anyInt());
    verify(demographicDaoTemplate)
        .saveOrUpdate(any(Demographic.class));
    verify(demographicDao, never())
        .save(any(Demographic.class));
    verify(demographicArchiveDao)
        .archiveRecord(any(Demographic.class));
    verify(demographicExtDao)
        .getDemographicExtByDemographicNo(anyInt());
    verify(demographicExtArchiveDao, times(extensions.size()))
        .saveEntity(any(DemographicExtArchive.class));
  }

  @Test
  public void givenDemographicAndDemographicArchive_whenSaveDemographic_thenVerifySaveDemographic() {
    val demographic = mock(Demographic.class);
    val demographicArchive = new DemographicArchive(demographic);
    demographicArchive.setId(1L);

    when(demographic.getDemographicNo())
        .thenReturn(DEMOGRAPHIC_NO_VALUE);
    when(demographicExtDao.getDemographicExtByDemographicNo(
        eq(DEMOGRAPHIC_NO_VALUE)))
        .thenReturn(generateExtensions());
    when(demographicArchiveDao.archiveRecord(any(Demographic.class)))
        .thenReturn(demographicArchive);

    DemographicAdapter.saveDemographic(demographic);

    verify(demographicDaoTemplate)
        .saveOrUpdate(any(Demographic.class));
    verify(demographicDao, never())
        .save(any(Demographic.class));
    verify(demographicArchiveDao)
        .archiveRecord(any(Demographic.class));
    verify(demographicExtDao)
        .getDemographicExtByDemographicNo(anyInt());
    verify(demographicExtArchiveDao, times(extensions.size()))
        .saveEntity(any(DemographicExtArchive.class));
  }

  @Test
  public void givenNothing_whenSaveDemographic_thenVerifySaveDemographic() {
    DemographicAdapter.saveDemographic(demographic, true);

    verify(demographicDaoTemplate)
        .saveOrUpdate(any(Demographic.class));
    verify(demographicDao, never())
        .save(any(Demographic.class));
    verify(demographicArchiveDao, never())
        .archiveRecord(any(Demographic.class));
    verify(demographicExtDao, never())
        .getDemographicExtByDemographicNo(anyInt());
    verify(demographicExtArchiveDao, never())
        .saveEntity(any(DemographicExtArchive.class));
  }

  @Test
  public void givenDemographicAndDemographicArchive_whenSaveDemographic_thenVerifySaveWithExtensions() {
    val demographic = mock(Demographic.class);
    val demographicArchive = new DemographicArchive(demographic);
    demographicArchive.setId(1L);

    when(demographic.getDemographicNo())
        .thenReturn(DEMOGRAPHIC_NO_VALUE);
    when(demographicExtDao.getDemographicExtByDemographicNo(
        eq(DEMOGRAPHIC_NO_VALUE)))
        .thenReturn(generateExtensions());
    when(demographicArchiveDao.archiveRecord(any(Demographic.class)))
        .thenReturn(demographicArchive);

    DemographicAdapter.saveDemographic(demographic, extensions);

    verify(demographicDaoTemplate)
        .saveOrUpdate(any(Demographic.class));
    verify(demographicDao, never())
        .save(any(Demographic.class));
    verify(demographicExtDao)
        .saveDemographicExts(any(List.class), anyBoolean());
    verify(demographicArchiveDao)
        .archiveRecord(any(Demographic.class));
    verify(demographicExtArchiveDao, times(extensions.size()))
        .saveEntity(any(DemographicExtArchive.class));
  }

  @Test
  public void givenDemographicAndDemographicArchive_whenArchiveDemographic_thenVerifySaveDemographic() {
    val demographic = mock(Demographic.class);
    val demographicArchive = new DemographicArchive(demographic);
    demographicArchive.setId(1L);

    when(demographic.getDemographicNo())
        .thenReturn(DEMOGRAPHIC_NO_VALUE);
    when(demographicExtDao.getDemographicExtByDemographicNo(
        eq(DEMOGRAPHIC_NO_VALUE)))
        .thenReturn(generateExtensions());
    when(demographicArchiveDao.archiveRecord(any(Demographic.class)))
        .thenReturn(demographicArchive);

    DemographicAdapter.archiveDemographic(demographic);
    verifySaveAndArchive(demographic, extensions,
        false,
        false,
        true,
        true
    );
  }

  @Test
  public void givenDemographicArchive_whenArchiveDemographicExt_thenVerifySaveDemographic() {
    val demographicArchive = new DemographicArchive(demographic);
    demographicArchive.setId(1L);

    when(demographicDao.getDemographic(anyInt()))
        .thenReturn(generateDemographic(DEMOGRAPHIC_NO_VALUE));
    when(demographicExtDao.getDemographicExtByDemographicNo(
        eq(DEMOGRAPHIC_NO_VALUE)))
        .thenReturn(generateExtensions());
    when(demographicArchiveDao.archiveRecord(any(Demographic.class)))
        .thenReturn(demographicArchive);

    DemographicAdapter.archiveDemographicExt(
        generateExtension("demo_cell", "905-111-2354")
    );

    verify(demographicDao)
        .getDemographic(anyInt());
    verify(demographicDaoTemplate)
        .saveOrUpdate(any(Demographic.class));
    verify(demographicDao, never())
        .save(any(Demographic.class));
    verify(demographicArchiveDao)
        .archiveRecord(any(Demographic.class));
    verify(demographicExtDao)
        .getDemographicExtByDemographicNo(anyInt());
    verify(demographicExtArchiveDao, times(extensions.size()))
        .saveEntity(any(DemographicExtArchive.class));
  }

  /* DEMOGRAPHIC REPOSITORY EXTENSION HELPER */
  @Test
  public void givenDemographicExtKey_whenProcessDemographicExtensions_thenVerifyExtensionsSize() {
    val helper = generateDemographicRepositoryExtensionHelper();

    for(DemographicExtKey key : DemographicExtKey.values()){
      when(request.getParameter(key.getRequestParameter()))
          .thenReturn(null);
      mockDemographicExtKeyValues(key);
    }

    val extensionsFounded = helper.processDemographicExtensions(false);
    // + 3 for the ones added in the test data
    // + 3 because the remider_* are included by default
    // + 1 because 'referral_source' is used as a key twice
    assertEquals(10, extensionsFounded.size());
  }

  @Test
  public void givenDemographicExtensionAdapter_whenProcessDemographicExtensions_thenVerifyExtensionsSize() {
    val helper = generateDemographicRepositoryExtensionHelper();

    when(request.getParameter(CUSTOM_KEY))
        .thenReturn(CUSTOM_VALUE + 2);

    for(DemographicExtKey key : DemographicExtKey.values()){
      when(request.getParameter(key.getRequestParameter()))
          .thenReturn(null);
      mockDemographicExtKeyValues(key);
    }

    val extensionsFounded = helper.processDemographicExtensions(false);
    // + 3 for the ones added in the test data
    // + 3 because the remider_* are included by default
    // + 1 because there is one custom
    // + 1 because 'referral_source' is used as a key twice

    assertEquals(11, extensionsFounded.size());
  }

  @Test
  public void givenEmptyHin_whenProcessUniqueDemographicHin_thenVerifyEmptyHin() {
    when(request.getParameter(HIN))
        .thenReturn(EMPTY_HIN);
    val outputHin = DemographicAdapter.processUniqueDemographicHin(request, 1);
    assertEquals("", outputHin);
  }

  @Test
  public void givenNonEmptyHin_whenProcessUniqueDemographicHin_thenVerifyNonEmptyUniqueHin() {
    when(demographicDao.searchDemographicByHinAndNotVer(anyString(), anyString()))
        .thenReturn(new ArrayList<Demographic>());
    when(request.getParameter(HIN))
        .thenReturn(NON_EMPTY_HIN);
    val outputHin = DemographicAdapter.processUniqueDemographicHin(request, 1);
    assertEquals(NON_EMPTY_HIN, outputHin);
  }

  @Test
  public void givenNonEmptyHin_whenProcessUniqueDemographicHin_thenVerifyNonEmptyNonUniqueHin() {
    when(demographicDao.searchDemographicByHinAndNotVer(anyString(), anyString()))
        .thenReturn(Collections.singletonList(demographic));
    when(request.getParameter(HIN))
        .thenReturn(NON_EMPTY_HIN);
    val outputHin = DemographicAdapter.processUniqueDemographicHin(request, 2);
    assertEquals("", outputHin);
  }

  @Test
  public void givenNonEmptyHin_whenProcessUniqueDemographicHin_thenVerifyNonEmptyUniqueHinNewDemo() {
    when(demographicDao.searchDemographicByHinAndNotVer(anyString(), anyString()))
        .thenReturn(Collections.singletonList(demographic));
    when(request.getParameter(HIN))
        .thenReturn(NON_EMPTY_HIN);
    val outputHin = DemographicAdapter.processUniqueDemographicHin(request, 1);
    assertEquals(NON_EMPTY_HIN, outputHin);
  }

  @Test
  public void givenNonEmptyHin_whenProcessUniqueDemographicHin_thenVerifyNonEmptyHinWithVer66() {
    when(demographicDao.searchDemographicByHinAndVer(anyString(), anyString()))
        .thenReturn(Collections.singletonList(generateDemographic(1)));
    when(request.getParameter(HIN))
        .thenReturn(NON_EMPTY_HIN);
    when(request.getParameter("ver"))
        .thenReturn("66");
    val outputHin = DemographicAdapter.processUniqueDemographicHin(request, 1);
    assertEquals(NON_EMPTY_HIN, outputHin);
  }

  @Test
  public void givenNonEmptyHin_whenProcessUniqueDemographicHin_thenVerifyEmptyHinWithVer66() {
    when(demographicDao.searchDemographicByHinAndVer(anyString(), anyString()))
        .thenReturn(new ArrayList<Demographic>());
    when(request.getParameter(HIN))
        .thenReturn(NON_EMPTY_HIN);
    when(request.getParameter("ver"))
        .thenReturn("66");
    val outputHin = DemographicAdapter.processUniqueDemographicHin(request, 1);
    assertEquals(NON_EMPTY_HIN, outputHin);
  }

  @Test
  public void givenNonEmptyHin_whenProcessUniqueDemographicHin_thenVerifyEmptyHinWithoutVer66() {
    when(demographicDao.searchDemographicByHinAndVer(anyString(), anyString()))
        .thenReturn(null);
    when(demographicDao.searchDemographicByHinAndNotVer(anyString(), anyString()))
        .thenReturn(Collections.singletonList(demographic));
    when(request.getParameter(HIN))
        .thenReturn(NON_EMPTY_HIN);
    when(request.getParameter("ver"))
        .thenReturn("65");
    val outputHin = DemographicAdapter.processUniqueDemographicHin(request, 1);
    assertEquals(NON_EMPTY_HIN, outputHin);
  }

  @Test
  public void givenNonEmptyHin_whenProcessUniqueDemographicHin_thenVerifyEmptyHinWithoutVer() {
    when(demographicDao.searchDemographicByHinAndVer(anyString(), anyString()))
        .thenReturn(null);
    when(demographicDao.searchDemographicByHinAndNotVer(anyString(), anyString()))
        .thenReturn(Collections.singletonList(demographic));
    when(request.getParameter(HIN))
        .thenReturn(NON_EMPTY_HIN);
    when(request.getParameter("ver"))
        .thenReturn("");
    val outputHin = DemographicAdapter.processUniqueDemographicHin(request, 1);
    assertEquals(NON_EMPTY_HIN, outputHin);
  }

  private static Collection<Object> listDemographicExtKeyValuesParameterizedTest() {
    return Arrays.asList(new Object[][]{
        {DemographicExtKey.DOCTOR_ROSTER, PHYSICIAN_ROW_ID},
        {DemographicExtKey.DOCTOR_ROSTER_NAME, PHYSICIAN_NAME},
        {DemographicExtKey.DOCTOR_ROSTER_OHIP, PHYSICIAN_OHIP},
        {DemographicExtKey.DOCTOR_FAMILY, PHYSICIAN_ROW_ID},
        {DemographicExtKey.DOCTOR_FAMILY_NAME, PHYSICIAN_NAME},
        {DemographicExtKey.DOCTOR_FAMILY_OHIP, PHYSICIAN_OHIP}
    });
  }

  /* HELPERS */
  private void verifySaveAndArchive(
      Demographic demographic,
      List<DemographicExt> extensions,
      boolean shouldSaveDemographic,
      boolean shouldSaveDemographicExt,
      boolean shouldArchiveDemographic,
      boolean shouldArchiveDemographicExt
  ) {

    int calls = extensions != null ? extensions.size() : 0;

    if (shouldSaveDemographic) {
      verify(demographicDaoTemplate).saveOrUpdate(demographic);
      verify(demographicDao, never())
          .save(any(Demographic.class));
    } else {
      verify(demographicDaoTemplate, never())
          .saveOrUpdate(any(Demographic.class));
      verify(demographicDao, never())
          .save(any(Demographic.class));
    }

    if (shouldSaveDemographicExt) {
      verify(demographicExtDao, times(calls))
          .saveDemographicExt(any(DemographicExt.class));
    } else {
      verify(demographicExtDao, never())
          .saveDemographicExt(any(DemographicExt.class));
    }

    if (shouldArchiveDemographic) {
      verify(demographicArchiveDao).archiveRecord(demographic);
    } else {
      verify(demographicArchiveDao, never())
          .archiveRecord(any(Demographic.class));
    }

    if (shouldArchiveDemographicExt) {
      verify(demographicExtArchiveDao, times(calls))
          .saveEntity(any(DemographicExtArchive.class));
    } else {
      verify(demographicExtArchiveDao, never())
          .saveEntity(any(DemographicExtArchive.class));
    }
  }

  private Demographic generateDemographic(Integer demographicNo) {
    val demographic = new Demographic();
    demographic.setDemographicNo(demographicNo);
    demographic.setFirstName("TEST");
    demographic.setLastName("PATIENT");
    return demographic;
  }

  private List<DemographicExt> generateExtensions() {
    val extensions = new ArrayList<DemographicExt>();
    extensions.add(generateExtension("demo_cell", "939-556-0001"));
    extensions.add(generateExtension("referralDate", "2021-01-01"));
    return extensions;
  }

  private DemographicExt generateExtension(String key, String value) {
    val extension = new DemographicExt();
    extension.setId(1);
    extension.setDemographicNo(DEMOGRAPHIC_NO_VALUE);
    extension.setKey(key);
    extension.setValue(value);
    return extension;
  }

  private DemographicExtensionAdapter generateDemographicRepositoryExtensionHelper() {
    masterFilePreferences = new HashMap<>();
    properties = mock(OscarProperties.class);
    request = mock(HttpServletRequest.class);

    val helper = new DemographicExtensionAdapter(
        demographicExtDao,
        properties,
        masterFilePreferences,
        request,
        PROVIDER_NO_VALUE,
        DEMOGRAPHIC_NO_VALUE,
        systemPreferencesDao.isAppointmentRemindersEnabled()
    );

    when(properties.getProperty(anyString()))
        .thenReturn(CUSTOM_KEY);
    when(properties.getProperty(anyString(), anyString()))
        .thenReturn(CUSTOM_KEY);

    return helper;
  }

  private void mockDemographicExtKeyValues(DemographicExtKey key) {
    switch (key) {
      case REFERRAL_SOURCE:
      case REFERRAL_SOURCE_CUSTOM:
        when(request.getParameter(key.getKey()))
            .thenReturn(REFERRAL_SOURCE_VALUE);
        break;
      case REFERRAL_DATE:
        when(request.getParameter(key.getKey()))
            .thenReturn(REFERRAL_DATE_VALUE);
        break;
      case DEMOGRAPHIC_MISC_ID:
        when(request.getParameter(key.getKey()))
            .thenReturn(DEMOGRAPHIC_MISC_ID_VALUE);
        break;
      case ENROLLMENT_PROVIDER:
        when(request.getParameter(key.getKey()))
            .thenReturn(PROVIDER_NO_VALUE);
        break;
      default:
        when(request.getParameter(key.getKey()))
            .thenReturn(null);
        break;
    }
  }
}