package org.oscarehr.managers;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.springframework.test.util.ReflectionTestUtils;
import oscar.OscarProperties;

import static junit.framework.Assert.assertEquals;

public class OktaManagerTest {

  @Mock
  OscarProperties oscarProperties;
  @Mock
  SystemPreferencesDao systemPreferencesDao;

  OktaManager oktaManager;
  private AutoCloseable closeable;

  @BeforeEach
  public void before() throws Exception {
    closeable = MockitoAnnotations.openMocks(this);
    oktaManager = new OktaManager(systemPreferencesDao);
    ReflectionTestUtils.setField(oktaManager, "oscarProperties", oscarProperties);
  }

  @AfterEach
  public void after() throws Exception {
    closeable.close();
  }

  private void settingSystemPreference(String preferenceName, String expectCondition) {
    Mockito.doReturn(expectCondition)
        .when(systemPreferencesDao)
        .getPreferenceValueByName(preferenceName, "");
  }

  @Test
  public void testClientIdNull() {
    settingSystemPreference(OktaManager.OSCAR_OKTA_CLIENT_ID, "");
    Assertions.assertEquals(oktaManager.getClientId(), "");
  }

  @Test
  public void testClientIdNormal() {
    settingSystemPreference(OktaManager.OSCAR_OKTA_CLIENT_ID, "1234567890");
    Assertions.assertEquals(oktaManager.getClientId(), "1234567890");
  }
}