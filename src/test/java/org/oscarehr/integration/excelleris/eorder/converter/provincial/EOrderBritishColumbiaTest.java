package org.oscarehr.integration.excelleris.eorder.converter.provincial;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.OrderLabTestSourceDao;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.OrderLabTestCodeSource;
import org.oscarehr.util.SpringUtils;

@ExtendWith(MockitoExtension.class)
public class EOrderBritishColumbiaTest {
  @Mock
  Province province;

  @Mock
  QuestionnaireResponse questionnaireResponse;

  @Mock
  Questionnaire questionnaire;

  @Mock
  OrderLabTestSourceDao orderLabTestSourceDao;

  private AutoCloseable closeable;
  private MockedStatic<SpringUtils> springUtils;


  @BeforeEach
  public void initialize() throws Exception {
    closeable = MockitoAnnotations.openMocks(this);
    mockObjects();
  }

  @AfterEach
  public void closeService() throws Exception {
    closeable.close();
    springUtils.close();
  }

  @Test
  public void test_getTestCode() {
    EOrderBritishColumbia eOrderBritishColumbia =
        new EOrderBritishColumbia(province, getValueMap());
    String testCode = eOrderBritishColumbia.getTestCode("T_superficial_wound",
        questionnaireResponse, null, questionnaire);
    assertEquals("ABD", testCode);

    testCode = eOrderBritishColumbia.getTestCode("T_deep_wound", questionnaireResponse,
        null, questionnaire);
    assertEquals("AXIL", testCode);
  }

  private void mockObjects() {
    springUtils = mockStatic(SpringUtils.class);
    springUtils
        .when(() -> SpringUtils.getBean(OrderLabTestSourceDao.class))
        .thenReturn(orderLabTestSourceDao);

    when(orderLabTestSourceDao.findBySourceName("BC", "T_superficial_wound_src", "ABDOMEN"))
        .thenReturn(getOrderLabTestCodeSource("ABDOMEN", "ABD"));
    when(orderLabTestSourceDao.findBySourceName("BC", "T_deep_wound_src", "AXILLA"))
        .thenReturn(getOrderLabTestCodeSource("AXILLA", "AXIL"));
  }

  private OrderLabTestCodeSource getOrderLabTestCodeSource(String sourceName, String testCode) {
    OrderLabTestCodeSource orderLabTestCodeSource = new OrderLabTestCodeSource();
    orderLabTestCodeSource.setSourceName(sourceName);
    orderLabTestCodeSource.setTestCode(testCode);
    return orderLabTestCodeSource;
  }

  private Map<String, EFormValue> getValueMap() {
    Map<String, EFormValue> valueMap = new HashMap<>();
    valueMap.put("T_superficial_wound_src", getEFormValue("T_superficial_wound_src", "ABDOMEN"));
    valueMap.put("T_deep_wound_src", getEFormValue("T_superficial_wound_src", "AXILLA"));
    return valueMap;
  }

  private EFormValue getEFormValue(String varName, String varValue) {
    EFormValue eFormValue = new EFormValue();
    eFormValue.setVarName(varName);
    eFormValue.setVarValue(varValue);
    return eFormValue;
  }
}
