package org.oscarehr.integration.excelleris.eorder;

import java.util.Properties;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

// Ignored because it requires valid credentials.
@Ignore
public class AuthenticationServiceImplTest {
	
	private ExcellerisEOrder excellerisEOrderImpl;
	
	@Before
    public void setUp() {
		final Properties properties = new Properties();
		properties.setProperty(PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_ENABLE, PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_ENABLE);
		properties.setProperty(PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_HOST,   PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_HOST);
		properties.setProperty(PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_PORT,   PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_PORT);
		properties.setProperty(PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_BASE_URL,          PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_BASE_URL);

		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_CLIENT_SSL_ENABLE,            "False");
		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_LOGIN_URL_PARAM_NAME,         PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_LOGIN_URL );
		properties.setProperty(ConfigureConstants.EXCELLERIS_USER_V2_PROVINCE_PARAM_NAME,            PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_USER_PROVINCE);
		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_INSTANCE_AFFINITY_DISABLE,    PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_INSTANCE_AFFINITY_DISABLE);
		properties.setProperty(ConfigureConstants.EXCELLERIS_USER_V2_PARAM_NAME,                     "ecambian3"); // Previous user had no available practitioners
		properties.setProperty(ConfigureConstants.EXCELLERIS_USER_V2_PASSWORD_PARAM_NAME,            "4400Dominion");
		properties.setProperty(ConfigureConstants.EXCELLERIS_USER_V2_PROVINCE_PARAM_NAME,            PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_USER_PROVINCE);

		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_KEYSTORE_FILE_PARAM_NAME,    PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_KEYSTORE_FILE);
		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_TRUSTSTORE_FILE_PARAM_NAME,    PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_FILE);

		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_KEYSTORE_PASSWORD_PARAM_NAME,    PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_KEYSTORE_PASSWORD);
		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_TRUSTSTORE_PASSWORD_PARAM_NAME,    PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_PASSWORD);
		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_API_VERSION_PARAM_NAME,    PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_API_VERSION);

		this.excellerisEOrderImpl = new ExcellerisEOrderImpl(properties);
    }
	
	@Test
	public void testAuthenticate() {
		
		// execute test
		AuthenticationToken token = excellerisEOrderImpl.authenticate();

		// verify result
		assertNotNull("should not be null", token);	
	}
}
