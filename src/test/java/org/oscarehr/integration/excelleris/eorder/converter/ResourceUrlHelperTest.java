/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.oscarehr.integration.excelleris.eorder.PropertyDefaultConstants;
import org.oscarehr.util.MiscUtils;

public class ResourceUrlHelperTest {
	
	private static final Logger LOG = MiscUtils.getLogger();
	private static final String FHIR_NAMESPACE_URL = "http://hl7.org/fhir/v2/0203";
	private static final String RESOURCE_BASE_URL = "https://api.excelleris.com/";
	private static final String EORDER_PATH = "/eorder/";
	private static final String API_VERSION = PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_API_VERSION;
	

	@Test
	public void testGetApiVersion() {
		
		assertEquals("2.0", ResourceUrlHelper.getApiVersion());
	}

	@Test
	public void testGetFhirNamSpaceUrl() {
		assertEquals(FHIR_NAMESPACE_URL, ResourceUrlHelper.getFhirNameSpaceUrl());
	}

	@Test
	public void testResourceUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "Practitioner/DAV123",
				ResourceUrlHelper.getResourceUrl("Practitioner", "DAV123"));
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "Practitioner",
				ResourceUrlHelper.getResourceUrl("Practitioner", null));
	}

	@Test
	public void testgetSubjectPreganacyExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "servicerequest-subjectpregnancy",
				ResourceUrlHelper.getSubjectPregancyExtensionUrl());

	}

	@Test
	public void testgetDiagnosisExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "servicerequest-subjectdiagnosis",
				ResourceUrlHelper.getDiagnosisExtensionUrl());
	}

	@Test
	public void testgetDiagnosticCodeExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "ValueSet/diagnosis-code",
				ResourceUrlHelper.getDiagnosticCodeExtensionUrl());
	}

	@Test
	public void testGetClassificationExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "servicerequest-classification",
				ResourceUrlHelper.getClassificationExtensionUrl());
	}

	@Test
	public void testgetItemPayerTypeValueSetExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "ValueSet/payer-type",
				ResourceUrlHelper.getItemPayerTypeValueSetExtensionUrl());
	}

	@Test
	public void testGetItemPayerTypeExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "servicerequest-item-payertype",
				ResourceUrlHelper.getItemPayerTypeExtensionUrl());
	}

	@Test
	public void testgetItemTieBreakerValueSetExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "ValueSet/servicerequest-item-tiebreaker",
				ResourceUrlHelper.getItemTieBreakerValueSetExtensionUrl());
	}

	@Test
	public void getItemTieBreakerExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "servicerequest-item-tiebreaker",
				ResourceUrlHelper.getItemTieBreakerExtensionUrl());
	}


	@Test
	public void getItemIsHardCodedTestValueSetExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "ValueSet/servicerequest-item-ishardcodedtest",
				ResourceUrlHelper.getItemIsHardCodedTestValueSetExtensionUrl());
	}

	@Test
	public void testgetItemIsHardCodedTestExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "servicerequest-item-ishardcodedtest",
				ResourceUrlHelper.getItemIsHardCodedTestExtensionUrl());
	}

	@Test
	public void testGetItemOtherTestValueSetExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "ValueSet/servicerequest-item-othertestfield",
				ResourceUrlHelper.getItemOtherTestValueSetExtensionUrl());
	}

	@Test
	public void testgetItemOtherTestExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "servicerequest-item-othertestfield",
				ResourceUrlHelper.getItemOtherTestExtensionUrl());
	}

	@Test
	public void testgetItemOtherIsOtherMicrobilogyTestValueSetExtensionUrl() {
		assertEquals(
				RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "ValueSet/servicerequest-item-isothermicrobiology",
				ResourceUrlHelper.getItemOtherIsOtherMicrobilogyTestValueSetExtensionUrl());
	}

	@Test
	public void testgetItemOtherIsOtherMicrobilogyTestExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "servicerequest-item-isothermicrobiology",
				ResourceUrlHelper.getItemOtherIsOtherMicrobilogyTestExtensionUrl());
	}

	@Test
	public void testgetCopyToRecipientExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "servicerequest-copytorecipient",
				ResourceUrlHelper.getCopyToRecipientExtensionUrl());
	}

	@Test
	public void testgetCollectionDateTimeExtensionUrl() {
		assertEquals(RESOURCE_BASE_URL + API_VERSION + EORDER_PATH + "servicerequest-collectiondatetime",
				ResourceUrlHelper.getCollectionDateTimeExtensionUrl());
	}


}
