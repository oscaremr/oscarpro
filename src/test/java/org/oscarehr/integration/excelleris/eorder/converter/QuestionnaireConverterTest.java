/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hl7.fhir.r4.model.Coding;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.OrderLabTestCodeCacheDao;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.OrderLabTestCodeCache;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.api.Questionnaire;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialBusinessLogic;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialFactory;

@ExtendWith(MockitoExtension.class)
public class QuestionnaireConverterTest {

  @Mock
  private OrderLabTestCodeCacheDao orderLabTestCodeDao;

  @BeforeEach
  public void before() {
    this.orderLabTestCodeDao = new MockOrderLabTestCodeCacheDao();
  }

  @Test
  public void givenNullQuestionnaire_whenToApiObject_thenThrowServiceException() {
    assertThrows(
        ServiceException.class,
        () -> {
          final ProvincialBusinessLogic provincial =
              new ProvincialFactory(new HashMap<>()).getProvincialBusinessLogic();
          QuestionnaireConverter.toApiObject(null, provincial, new MockOrderLabTestCodeCacheDao());
        });
  }

  @Test
  public void givenNullValueMap_whenToApiObject_thenThrowServiceException() {
    assertThrows(
        ServiceException.class,
        () ->
            QuestionnaireConverter.toApiObject(
                new org.hl7.fhir.r4.model.Questionnaire(),
                null,
                new MockOrderLabTestCodeCacheDao()));
  }

	@Test
	public void testToApiObject() {

		org.hl7.fhir.r4.model.Questionnaire fhirQuestionnaire = new org.hl7.fhir.r4.model.Questionnaire();

		org.hl7.fhir.r4.model.Questionnaire.QuestionnaireItemComponent fhirQuestion = new org.hl7.fhir.r4.model.Questionnaire.QuestionnaireItemComponent();
		fhirQuestionnaire.addItem(fhirQuestion);
		fhirQuestion.setType(org.hl7.fhir.r4.model.Questionnaire.QuestionnaireItemType.CHOICE);
		fhirQuestion.setLinkId(
				"GUI|TestCode|TR10259-0U|Global|1|Iron and Ferritin together require one of the following diagnosis:");
		fhirQuestion.setText("Iron and Ferritin together require one of the following diagnosis:");

		org.hl7.fhir.r4.model.Questionnaire.QuestionnaireItemAnswerOptionComponent answerOptionComponent = null;

		Coding coding1 = new Coding();
		coding1.setCode("Chronic kidney failure");
		coding1.setDisplay("Chronic kidney failure");
		answerOptionComponent = new org.hl7.fhir.r4.model.Questionnaire.QuestionnaireItemAnswerOptionComponent();
		answerOptionComponent.setValue(coding1);
		fhirQuestion.addAnswerOption(answerOptionComponent);

		Coding coding2 = new Coding();
		coding2.setCode("Iron overload");
		coding2.setDisplay("Iron overload");
		answerOptionComponent = new org.hl7.fhir.r4.model.Questionnaire.QuestionnaireItemAnswerOptionComponent();
		answerOptionComponent.setValue(coding2);
		fhirQuestion.addAnswerOption(answerOptionComponent);

//		fhirQuestionnaire.getGroup().addQuestion(fhirQuestion);

		List<EFormValue> eformValues = new ArrayList<>();
		EFormValue eformValue = new EFormValue();
		eformValue.setVarName("T_Iron__24h_Urine");
		eformValue.setVarValue("TR10259-0U");
		eformValues.add(eformValue);

		eformValue = new EFormValue();
		eformValue.setVarName("M_ExcellerisEOrderType");
		eformValue.setVarValue("ON");
		eformValues.add(eformValue);

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		final ProvincialBusinessLogic provincial = new ProvincialFactory(valueMap).getProvincialBusinessLogic();
		Questionnaire questionnaire = QuestionnaireConverter.toApiObject(fhirQuestionnaire, provincial,
				this.orderLabTestCodeDao);
		assertEquals(1, questionnaire.getQuestions().size());
	}


	public class MockOrderLabTestCodeCacheDao extends OrderLabTestCodeCacheDao {

		public MockOrderLabTestCodeCacheDao() {
		}

		@Override
		public OrderLabTestCodeCache findByTestCodeAndProvince(
				String testCode, String province) {
			return null;
		}

		@Override
		public List<OrderLabTestCodeCache> findAll(final Integer offset, final Integer limit) {
			return new ArrayList<>();
		}
	}

}
