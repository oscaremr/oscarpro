/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.Questionnaire;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.OrderLabTestCodeCacheDao;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.OrderLabTestCodeCache;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.api.Question;
import org.oscarehr.integration.excelleris.eorder.api.QuestionType;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialBusinessLogic;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialFactory;

public class QuestionConverterTest {

  private OrderLabTestCodeCacheDao orderLabTestCodeDao;

  @BeforeEach
  public void setUp() {
    this.orderLabTestCodeDao = new MockOrderLabTestCodeCacheDao();
  }

  @Test
  public void givenNullQuestion_whenToApiObjectCalled_thenServiceExceptionThrown() {
    final ProvincialBusinessLogic provincial =
        new ProvincialFactory(new HashMap<String, EFormValue>()).getProvincialBusinessLogic();
    assertThrows(
        ServiceException.class,
        () -> {
          QuestionConverter.toApiObject(null, provincial, orderLabTestCodeDao);
        });
  }

  @Test
  public void givenNullValueMap_whenToApiObjectCalled_thenServiceExceptionThrown() {
    Questionnaire.QuestionnaireItemComponent component =
        new Questionnaire.QuestionnaireItemComponent();
    assertThrows(
        ServiceException.class,
        () -> QuestionConverter.toApiObject(component, null, orderLabTestCodeDao));
  }

  @Test
  public void givenHardCodedTest_whenToApiObjectCalled_thenQuestionCreated() {

    Questionnaire.QuestionnaireItemComponent fhirQuestion =
        new Questionnaire.QuestionnaireItemComponent();
    fhirQuestion.setType(Questionnaire.QuestionnaireItemType.CHOICE);
    fhirQuestion.setLinkId(
        "GUI|TestCode|TR10259-0U|Global|1|Iron and Ferritin together require one of the following diagnosis:");
    fhirQuestion.setText("Iron and Ferritin together require one of the following diagnosis:");

    Questionnaire.QuestionnaireItemAnswerOptionComponent answerOptionComponent = null;

    Coding coding1 = new Coding();
    coding1.setCode("Chronic kidney failure");
    coding1.setDisplay("Chronic kidney failure");
    answerOptionComponent = new Questionnaire.QuestionnaireItemAnswerOptionComponent();
    answerOptionComponent.setValue(coding1);
    fhirQuestion.addAnswerOption(answerOptionComponent);

    Coding coding2 = new Coding();
    coding2.setCode("Iron overload");
    coding2.setDisplay("Iron overload");
    answerOptionComponent = new Questionnaire.QuestionnaireItemAnswerOptionComponent();
    answerOptionComponent.setValue(coding2);
    fhirQuestion.addAnswerOption(answerOptionComponent);

    List<Coding> options = new ArrayList<Coding>();
    options.add(coding1);
    options.add(coding2);

    fhirQuestion.addCode(coding1);
    fhirQuestion.addCode(coding2);

    List<EFormValue> eformValues = new ArrayList<>();
    EFormValue eformValue = new EFormValue();
    eformValue.setVarName("T_other_tests_5");
    eformValue.setVarValue("Iron  24h Urine");
    eformValues.add(eformValue);

    eformValue = new EFormValue();
    eformValue.setVarName("M_ExcellerisEOrderType");
    eformValue.setVarValue("ON");
    eformValues.add(eformValue);

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    final ProvincialBusinessLogic provincial =
        new ProvincialFactory(valueMap).getProvincialBusinessLogic();
    Question question =
        QuestionConverter.toApiObject(fhirQuestion, provincial, this.orderLabTestCodeDao);

    Assertions.assertEquals(
        "Iron and Ferritin together require one of the following diagnosis:",
        question.getQuestionText());
    Assertions.assertEquals(QuestionType.CHOICE, question.getType());
    Assertions.assertEquals(
        "GUI|TestCode|TR10259-0U|Global|1|Iron and Ferritin together require one of the following diagnosis:",
        question.getLinkId());
    Assertions.assertTrue(question.getOtherTestFieldId() != null);
    Assertions.assertEquals(null, question.getHardCodedTestFieldId());
    Assertions.assertEquals("Iron 24h Urine", question.getTestName());
    Assertions.assertEquals("TR10259-0U", question.getTestCode());
    Assertions.assertEquals(
        "Iron and Ferritin together require one of the following diagnosis:", question.getItemId());
    Assertions.assertEquals(2, question.getChoices().size());
    Assertions.assertEquals("Iron overload", question.getChoices().get(1).getCode());
    Assertions.assertEquals("Iron overload", question.getChoices().get(1).getText());
    Assertions.assertEquals("Chronic kidney failure", question.getChoices().get(0).getCode());
    Assertions.assertEquals("Chronic kidney failure", question.getChoices().get(0).getText());
  }

  @Test
  public void givenOtherTest_whenToApiObjectCalled_thenQuestionCreated() {

    Questionnaire.QuestionnaireItemComponent fhirQuestion =
        new Questionnaire.QuestionnaireItemComponent();
    fhirQuestion.setType(Questionnaire.QuestionnaireItemType.CHOICE);
    fhirQuestion.setLinkId(
        "GUI|TestCode|TR10259-0|Global|1|Iron and Ferritin together require one of the following diagnosis:");
    fhirQuestion.setText("Iron and Ferritin together require one of the following diagnosis:");

    Questionnaire.QuestionnaireItemAnswerOptionComponent answerOptionComponent = null;

    Coding coding1 = new Coding();
    coding1.setCode("Chronic kidney failure");
    coding1.setDisplay("Chronic kidney failure");
    answerOptionComponent = new Questionnaire.QuestionnaireItemAnswerOptionComponent();
    answerOptionComponent.setValue(coding1);
    fhirQuestion.addAnswerOption(answerOptionComponent);

    Coding coding2 = new Coding();
    coding2.setCode("Iron overload");
    coding2.setDisplay("Iron overload");
    answerOptionComponent = new Questionnaire.QuestionnaireItemAnswerOptionComponent();
    answerOptionComponent.setValue(coding2);
    fhirQuestion.addAnswerOption(answerOptionComponent);

    final ProvincialBusinessLogic provincial =
        new ProvincialFactory(new HashMap<String, EFormValue>()).getProvincialBusinessLogic();
    Question question =
        QuestionConverter.toApiObject(fhirQuestion, provincial, this.orderLabTestCodeDao);

    Assertions.assertEquals(
        "Iron and Ferritin together require one of the following diagnosis:",
        question.getQuestionText());
    Assertions.assertEquals(QuestionType.CHOICE, question.getType());
    Assertions.assertEquals(
        "GUI|TestCode|TR10259-0|Global|1|Iron and Ferritin together require one of the following diagnosis:",
        question.getLinkId());
    Assertions.assertNull(question.getHardCodedTestFieldId());
    Assertions.assertEquals("Iron", question.getOtherTestFieldId());
    Assertions.assertEquals("Iron", question.getTestName());
    Assertions.assertEquals("TR10259-0", question.getTestCode());
    Assertions.assertEquals(
        "Iron and Ferritin together require one of the following diagnosis:", question.getItemId());
    Assertions.assertEquals(2, question.getChoices().size());
    Assertions.assertEquals("Iron overload", question.getChoices().get(1).getCode());
    Assertions.assertEquals("Iron overload", question.getChoices().get(1).getText());
    Assertions.assertEquals("Chronic kidney failure", question.getChoices().get(0).getCode());
    Assertions.assertEquals("Chronic kidney failure", question.getChoices().get(0).getText());
  }

  public static class MockOrderLabTestCodeCacheDao extends OrderLabTestCodeCacheDao {

    List<OrderLabTestCodeCache> orderLabTestCodes;

    public MockOrderLabTestCodeCacheDao() {

      orderLabTestCodes = new ArrayList<OrderLabTestCodeCache>();

      OrderLabTestCodeCache labTestCode = new OrderLabTestCodeCache();
      labTestCode.setId(1);
      labTestCode.setSearchable(true);
      labTestCode.setTestOnForm(false);
      labTestCode.setRequireAppointment(false);
      labTestCode.setPatientPay(false);
      labTestCode.setTestName("Iron");
      labTestCode.setLabTestCode("TR10259-0");
      orderLabTestCodes.add(labTestCode);

      OrderLabTestCodeCache labTestCode2 = new OrderLabTestCodeCache();
      labTestCode2.setId(2);
      labTestCode2.setSearchable(true);
      labTestCode2.setTestOnForm(false);
      labTestCode2.setRequireAppointment(false);
      labTestCode2.setPatientPay(false);
      labTestCode2.setTestName("Blood");
      labTestCode2.setLabTestCode("TR10222-0");
      orderLabTestCodes.add(labTestCode2);

      OrderLabTestCodeCache labTestCode3 = new OrderLabTestCodeCache();
      labTestCode3.setId(3);
      labTestCode3.setSearchable(true);
      labTestCode3.setTestOnForm(false);
      labTestCode3.setRequireAppointment(false);
      labTestCode3.setPatientPay(false);
      labTestCode3.setTestName("Iron 24h Urine");
      labTestCode3.setLabTestCode("TR10259-0U");
      orderLabTestCodes.add(labTestCode3);
    }

    @Override
    public OrderLabTestCodeCache findByTestCodeAndProvince(String testCode, String province) {

      for (OrderLabTestCodeCache orderLabTestCode : this.orderLabTestCodes) {
        if (orderLabTestCode.getLabTestCode().equals(testCode)) {
          return orderLabTestCode;
        }
      }

      return null;
    }

    @Override
    public List<OrderLabTestCodeCache> findAll(final Integer offset, final Integer limit) {
      return new ArrayList<>();
    }
  }
}
