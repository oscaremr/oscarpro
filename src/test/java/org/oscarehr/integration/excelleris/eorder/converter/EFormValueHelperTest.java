/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;

public class EFormValueHelperTest {
	
	
	@Test
	public void testGetValueMap_null() {
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(null);

		assertEquals(new HashMap<String, EFormValue>(), valueMap);

	}
	
	@Test
	public void testGetValueMap_empty() {
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(new ArrayList<EFormValue>());

		assertEquals(new HashMap<String, EFormValue>(), valueMap);

	}

	@Test
	public void testGetValueMap() {

		List<EFormValue> eformValues = new ArrayList<>();
		EFormValue eformValue = new EFormValue();
		eformValue.setVarName(PatientPropertySet.PATIENT_ADDRESS_CITY);
		eformValue.setVarValue("Vancouver");
		eformValues.add(eformValue);
		eformValue = new EFormValue();
		eformValue.setVarName(PatientPropertySet.PATIENT_ADDRESS_PROVINCE);
		eformValue.setVarValue("BC");
		eformValues.add(eformValue);

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
		assertEquals(2, valueMap.size());
		assertEquals("Vancouver", valueMap.get(PatientPropertySet.PATIENT_ADDRESS_CITY).getVarValue());
		assertEquals("BC", valueMap.get(PatientPropertySet.PATIENT_ADDRESS_PROVINCE).getVarValue());

	}

	@Test
	public void testGetValue_nullName() {
		String value = EFormValueHelper.getValue(new HashMap<String, EFormValue>(), null);

		assertTrue(value == null);
	}

	@Test
	public void testGetValue_nullMap() {
		String value = EFormValueHelper.getValue(null, "City");

		assertTrue(value == null);
	}

	@Test
	public void testGetValue_emptyMap() {
		String value = EFormValueHelper.getValue(new HashMap<String, EFormValue>(), "City");

		assertTrue(value == null);
	}

	@Test
	public void testGetValue() {

		List<EFormValue> eformValues = new ArrayList<>();

		EFormValue eformValue = new EFormValue();
		eformValue.setVarName("City");
		eformValue.setVarValue("Vancouver");
		eformValues.add(eformValue);
		eformValue = new EFormValue();
		eformValue.setVarName("Province");
		eformValue.setVarValue("BC");
		eformValues.add(eformValue);

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		String cityValue = EFormValueHelper.getValue(valueMap, "City");
		String streetValue = EFormValueHelper.getValue(valueMap, "Street");

		assertEquals("Vancouver", cityValue);
		assertTrue(streetValue == null);
	}

	@Test
	public void testCreateEFormValue() {
		EFormValue value = EFormValueHelper.createEFormValue("City", "Vancouver");
		assertEquals("City", value.getVarName());
		assertEquals("Vancouver", value.getVarValue());
	}

	@Test
	public void testCreateEFormValue_nullName() {
		EFormValue value = EFormValueHelper.createEFormValue(null, "Vancouver");
		assertTrue(value.getVarName() == null);
		assertEquals("Vancouver", value.getVarValue());
	}

	@Test
	public void testCreateEFormValue_nullValue() {
		EFormValue value = EFormValueHelper.createEFormValue("City", null);
		assertEquals("City", value.getVarName());
		assertTrue(value.getVarValue() == null);
	}

}