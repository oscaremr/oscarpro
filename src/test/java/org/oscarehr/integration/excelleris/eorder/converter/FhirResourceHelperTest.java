/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hl7.fhir.r4.model.Address;
import org.hl7.fhir.r4.model.BooleanType;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.ContactPoint;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.StringType;
import org.junit.Test;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.util.MiscUtils;

public class FhirResourceHelperTest {
	
	private static final Logger LOG = MiscUtils.getLogger();

	@Test
	public void testCreateIdentifier() {
		Identifier identifier = FhirResourceHelper.createIdentifier("Coding System", "Identifier System", "Code",
				"Value", "Assigner Value");

		Coding coding = identifier.getType().getCoding().get(0);
		assertEquals(identifier.getUse(), Identifier.IdentifierUse.OFFICIAL);
		assertEquals(coding.getCode(), "Code");
		assertEquals(coding.getSystem(), "Coding System");
		assertEquals(identifier.getSystem(), "Identifier System");
		assertEquals(identifier.getValue(), "Value");
		assertEquals(identifier.getAssigner().getDisplay(), "Assigner Value");
	}

	@Test
	public void testCreateIdentifier_nullCodingSystem() {
		Identifier identifier = FhirResourceHelper.createIdentifier(null, "Identifier System", "Code",
				"Value", "Assigner Value");

		Coding coding = identifier.getType().getCoding().get(0);
		assertEquals(identifier.getUse(), Identifier.IdentifierUse.OFFICIAL);
		assertEquals(coding.getCode(), "Code");
		assertEquals(coding.getSystem(), null);
		assertEquals(identifier.getSystem(), "Identifier System");
		assertEquals(identifier.getValue(), "Value");
		assertEquals(identifier.getAssigner().getDisplay(), "Assigner Value");
	}

	@Test
	public void testCreateIdentifier_nullIdentifierSystem() {
		Identifier identifier = FhirResourceHelper.createIdentifier("Coding System", null, "Code", "Value",
				"Assigner Value");

		Coding coding = identifier.getType().getCoding().get(0);
		assertEquals(identifier.getUse(), Identifier.IdentifierUse.OFFICIAL);
		assertEquals(coding.getCode(), "Code");
		assertEquals(coding.getSystem(), "Coding System");
		assertEquals(identifier.getSystem(), null);
		assertEquals(identifier.getValue(), "Value");
		assertEquals(identifier.getAssigner().getDisplay(), "Assigner Value");
	}

	@Test
	public void testCreateIdentifier_nullCode() {
		Identifier identifier = FhirResourceHelper.createIdentifier("Coding System", "Identifier System", null, "Value",
				"Assigner Value");

		Coding coding = identifier.getType().getCoding().get(0);
		assertEquals(identifier.getUse(), Identifier.IdentifierUse.OFFICIAL);
		assertEquals(coding.getCode(), null);
		assertEquals(coding.getSystem(), "Coding System");
		assertEquals(identifier.getSystem(), "Identifier System");
		assertEquals(identifier.getValue(), "Value");
		assertEquals(identifier.getAssigner().getDisplay(), "Assigner Value");
	}

	@Test
	public void testCreateIdentifier_nullValue() {
		Identifier identifier = FhirResourceHelper.createIdentifier("Coding System", "Identifier System", "Code", null,
				"Assigner Value");

		Coding coding = identifier.getType().getCoding().get(0);
		assertEquals(identifier.getUse(), Identifier.IdentifierUse.OFFICIAL);
		assertEquals(coding.getCode(), "Code");
		assertEquals(coding.getSystem(), "Coding System");
		assertEquals(identifier.getSystem(), "Identifier System");
		assertEquals(identifier.getValue(), null);
		assertEquals(identifier.getAssigner().getDisplay(), "Assigner Value");
	}

	@Test
	public void testCreateIdentifier_nullAssignerValue() {
		Identifier identifier = FhirResourceHelper.createIdentifier("Coding System", "Identifier System", "Code",
				"Value", null);

		Coding coding = identifier.getType().getCoding().get(0);
		assertEquals(identifier.getUse(), Identifier.IdentifierUse.OFFICIAL);
		assertEquals(coding.getCode(), "Code");
		assertEquals(coding.getSystem(), "Coding System");
		assertEquals(identifier.getSystem(), "Identifier System");
		assertEquals(identifier.getValue(), "Value");
		assertEquals(identifier.getAssigner().getDisplay(), null);
	}

	@Test
	public void testCreateHumanName() {
		HumanName name = FhirResourceHelper.createHumanName("John", "Doe", "L.", "Dr.");
      assertEquals("Doe", name.getFamily());
		assertEquals("John", name.getGiven().get(0).asStringValue());
		assertEquals("Dr.", name.getPrefix().get(0).asStringValue());
		assertEquals(HumanName.NameUse.OFFICIAL, name.getUse());
	}

	@Test
	public void testCreateAddress_CAN() {
		List<EFormValue> eformValues = new ArrayList<>();
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "BC"));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V9D0S3"));
		
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
		
		Address address = FhirResourceHelper.createAddress(valueMap);
		assertEquals(Address.AddressUse.HOME, address.getUse());
		assertEquals(Address.AddressType.BOTH, address.getType());
		assertEquals("22 Main St.", address.getLine().get(0).asStringValue());
		assertEquals("Vancouver", address.getCity());
		assertEquals("CAN", address.getCountry());
		assertEquals("BC", address.getState());
		assertEquals("V9D 0S3", address.getPostalCode());
	}

	@Test(expected = ServiceException.class)
	public void testCreateAddress_CANWrongPCFormat() {
		List<EFormValue> eformValues = new ArrayList<>();
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "BC"));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V9D0S"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
		FhirResourceHelper.createAddress(valueMap);
	}
	
	@Test
	public void testCreateAddress_US() {
		List<EFormValue> eformValues = new ArrayList<>();
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Seattle"));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "US-Washington"));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "12345"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		Address address = FhirResourceHelper.createAddress(valueMap);
		assertEquals(Address.AddressUse.HOME, address.getUse());
		assertEquals(Address.AddressType.BOTH, address.getType());
		assertEquals("22 Main St.", address.getLine().get(0).asStringValue());
		assertEquals("Seattle", address.getCity());
		assertEquals("US", address.getCountry());
		assertEquals("US-Washington", address.getState());
		assertEquals("12345", address.getPostalCode());
	}

	@Test
	public void testCreateAddress_OT() {
		List<EFormValue> eformValues = new ArrayList<>();
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "OT"));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "Mexico"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		Address address = FhirResourceHelper.createAddress(valueMap);
		assertEquals(Address.AddressUse.HOME, address.getUse());
		assertEquals(Address.AddressType.BOTH, address.getType());
		assertEquals("22 Main St.", address.getLine().get(0).asStringValue());
		assertEquals("Vancouver", address.getCity());
		assertEquals("UNK", address.getCountry());
		assertEquals("OT", address.getState());
		assertEquals("Mexico", address.getPostalCode());
	}
	
	@Test(expected = ServiceException.class)
	public void testCreateAddress_noAddressLine() {
		List<EFormValue> eformValues = new ArrayList<>();
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "BC"));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V9D0S"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
		FhirResourceHelper.createAddress(valueMap);
	}
	
	@Test(expected = ServiceException.class)
	public void testCreateAddress_noCity() {
		List<EFormValue> eformValues = new ArrayList<>();
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, ""));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "BC"));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V9D0S"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
		FhirResourceHelper.createAddress(valueMap);
	}
	
	@Test(expected = ServiceException.class)
	public void testCreateAddress_noProvince() {
		List<EFormValue> eformValues = new ArrayList<>();
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V9D0S"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
		FhirResourceHelper.createAddress(valueMap);
	}
	
	@Test(expected = ServiceException.class)
	public void testCreateAddress_noPostalCode() {
		List<EFormValue> eformValues = new ArrayList<>();
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "BC"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
		FhirResourceHelper.createAddress(valueMap);
	}
	
	@Test
	public void testCreateContactPoint() {

		List<EFormValue> eformValues = new ArrayList<>();
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_PHONE, "1-123-123-1234"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
		ContactPoint contactPoint = FhirResourceHelper.createContactPoint(valueMap);

		assertEquals(ContactPoint.ContactPointSystem.PHONE, contactPoint.getSystem());
		assertEquals("1-123-123-1234", contactPoint.getValue());
		assertEquals(ContactPoint.ContactPointUse.HOME, contactPoint.getUse());

	}

	@Test
	public void testCreateContactPoint_noPhone() {

		List<EFormValue> eformValues = new ArrayList<>();
//		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_PHONE, "1-123-123-1234"));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
		ContactPoint contactPoint = FhirResourceHelper.createContactPoint(valueMap);

		assertEquals(ContactPoint.ContactPointSystem.PHONE, contactPoint.getSystem());
		assertTrue(contactPoint.getValue() == null);
		assertEquals(ContactPoint.ContactPointUse.HOME, contactPoint.getUse());

	}

	@Test
	public void testCreateContactPoint_nullPhone() {

		List<EFormValue> eformValues = new ArrayList<>();
		eformValues.add(createEFormValue(PatientPropertySet.PATIENT_PHONE, ""));

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
		ContactPoint contactPoint = FhirResourceHelper.createContactPoint(valueMap);

		assertEquals(ContactPoint.ContactPointSystem.PHONE, contactPoint.getSystem());
		assertTrue(contactPoint.getValue() == null);
		assertEquals(ContactPoint.ContactPointUse.HOME, contactPoint.getUse());

	}

	@Test
	public void testCreateCodeableConcept() {
		CodeableConcept codeableConcept = FhirResourceHelper.createCodeableConcept("/valueset", "Code");
		assertEquals(1, codeableConcept.getCoding().size());
		assertEquals("/valueset", codeableConcept.getCoding().get(0).getSystem());
		assertEquals("Code", codeableConcept.getCoding().get(0).getCode());
	}

	@Test
	public void testCreateCodeableConcept_nullValueSetUrl() {
		CodeableConcept codeableConcept = FhirResourceHelper.createCodeableConcept(null, "Code");
		assertEquals(1, codeableConcept.getCoding().size());
		assertTrue(codeableConcept.getCoding().get(0).getSystem() == null);
		assertEquals("Code", codeableConcept.getCoding().get(0).getCode());
	}

	@Test
	public void testCreateCodeableConcept_nullCode() {
		CodeableConcept codeableConcept = FhirResourceHelper.createCodeableConcept("/valueset", null);
		assertEquals(1, codeableConcept.getCoding().size());
		assertTrue(codeableConcept.getCoding().get(0).getCode() == null);
		assertEquals("/valueset", codeableConcept.getCoding().get(0).getSystem());
	}
	
	@Test
	public void testCreateCodeableConcept_emptyValueSetUrl() {
		CodeableConcept codeableConcept = FhirResourceHelper.createCodeableConcept("", "Code");
		assertEquals(1, codeableConcept.getCoding().size());
		assertTrue(codeableConcept.getCoding().get(0).getSystem() == null);
		assertEquals("Code", codeableConcept.getCoding().get(0).getCode());
	}

	@Test
	public void testCreateCodeableConcept_emptyCode() {
		CodeableConcept codeableConcept = FhirResourceHelper.createCodeableConcept("/valueset", "");
		assertEquals(1, codeableConcept.getCoding().size());
		assertTrue(codeableConcept.getCoding().get(0).getCode() == null);
		assertEquals("/valueset", codeableConcept.getCoding().get(0).getSystem());
	}

	@Test
	public void testCreateCodeableConceptExtension() {
		Extension extension = FhirResourceHelper.createCodeableConceptExtension("/valueset", "Code", "extension");

		CodeableConcept codeableConcept = (CodeableConcept) extension.getValue();

		assertEquals("extension", extension.getUrl());
		assertEquals(1, codeableConcept.getCoding().size());
		assertEquals("Code", codeableConcept.getCoding().get(0).getCode());
		assertEquals("/valueset", codeableConcept.getCoding().get(0).getSystem());
	}

	@Test
	public void testCreateCodeableConceptExtension_nullExtension() {
		Extension extension = FhirResourceHelper.createCodeableConceptExtension("/valueset", "Code", null);

		CodeableConcept codeableConcept = (CodeableConcept) extension.getValue();

		assertTrue(extension.getUrl() == null);
		assertEquals(1, codeableConcept.getCoding().size());
		assertEquals("Code", codeableConcept.getCoding().get(0).getCode());
		assertEquals("/valueset", codeableConcept.getCoding().get(0).getSystem());
	}

	@Test
	public void testCreateCodeableConceptExtension_emptyExtension() {
		Extension extension = FhirResourceHelper.createCodeableConceptExtension("/valueset", "Code", "");

		CodeableConcept codeableConcept = (CodeableConcept) extension.getValue();

		assertEquals("", extension.getUrl());
		assertEquals(1, codeableConcept.getCoding().size());
		assertEquals("Code", codeableConcept.getCoding().get(0).getCode());
		assertEquals("/valueset", codeableConcept.getCoding().get(0).getSystem());
	}

	@Test
	public void testCreateBooleanExtension_true() {
		Extension extension = FhirResourceHelper.createBooleanExtension(true, "/extensionUrl");

		BooleanType booleanValue = (BooleanType) extension.getValue();

		assertEquals("/extensionUrl", extension.getUrl());
		assertEquals(true, booleanValue.getValue());
	}

	@Test
	public void testCreateBooleanExtension_false() {
		Extension extension = FhirResourceHelper.createBooleanExtension(false, "/extensionUrl");

		BooleanType booleanValue = (BooleanType) extension.getValue();

		assertEquals("/extensionUrl", extension.getUrl());
		assertEquals(false, booleanValue.getValue());
	}

	@Test
	public void testCreateBooleanExtension_null() {
		Extension extension = FhirResourceHelper.createBooleanExtension(null, "/extensionUrl");

		BooleanType booleanValue = (BooleanType) extension.getValue();

		assertEquals("/extensionUrl", extension.getUrl());
		assertTrue(booleanValue.getValue() == null);
	}

	@Test
	public void testCreateStringExtension() {
		Extension extension = FhirResourceHelper.createStringExtension("valueString", "/extensionUrl");

		StringType stringValue = (StringType) extension.getValue();

		assertEquals("/extensionUrl", extension.getUrl());
		assertEquals("valueString", stringValue.getValue());
	}

	@Test
	public void testCreateStringExtension_empty() {
		Extension extension = FhirResourceHelper.createStringExtension("", "/extensionUrl");

		StringType stringValue = (StringType) extension.getValue();

		assertEquals("/extensionUrl", extension.getUrl());
		assertEquals("", stringValue.getValue());
	}

	@Test
	public void testCreateStringExtension_null() {
		Extension extension = FhirResourceHelper.createStringExtension(null, "/extensionUrl");

		StringType stringValue = (StringType) extension.getValue();

		assertEquals("/extensionUrl", extension.getUrl());
		assertTrue(stringValue.getValue() == null);
	}

	@Test
	public void testCreateValueReferenceExtension() {
		Extension extension = FhirResourceHelper.createValueReferenceExtension("/referenceUrl", "/extensionUrl");

		Reference reference = (Reference) extension.getValue();

		assertEquals("/extensionUrl", extension.getUrl());
		assertEquals("/referenceUrl", reference.getReference());
	}

	@Test
	public void testCreateValueReferenceExtension_empty() {
		Extension extension = FhirResourceHelper.createValueReferenceExtension("", "/extensionUrl");

		Reference reference = (Reference) extension.getValue();

		assertEquals("/extensionUrl", extension.getUrl());
		assertTrue(reference.getReference() == null);
	}

	@Test
	public void testCreateValueReferenceExtension_null() {
		Extension extension = FhirResourceHelper.createValueReferenceExtension(null, "/extensionUrl");

		Reference reference = (Reference) extension.getValue();

		assertEquals("/extensionUrl", extension.getUrl());
		assertTrue(reference.getReference() == null);
	}

	@Test(expected = ServiceException.class)
	public void testCreateQuestionnaireResponse_nullQuestionsAndAnswers() {
		FhirResourceHelper.createQuestionnaireResponse(null, null, null);
	}

	@Test(expected = ServiceException.class)
	public void testCreateQuestionnaireResponse_emptyQuestionsAndAnswers() {
		FhirResourceHelper.createQuestionnaireResponse(null, new ArrayList<String>(), new ArrayList<String>());
	}

	@Test(expected = ServiceException.class)
	public void testCreateQuestionnaireResponse_nullQuestions() {
		FhirResourceHelper.createQuestionnaireResponse(null, null, new ArrayList<String>());
	}

	@Test(expected = ServiceException.class)
	public void testCreateQuestionnaireResponse_nullAnswers() {
		FhirResourceHelper.createQuestionnaireResponse(null, new ArrayList<String>(), null);
	}

	@Test(expected = ServiceException.class)
	public void testCreateQuestionnaireResponse_differentSizes() {
		List<String> questions = new ArrayList<String>();
		questions.add("Question 1");
		FhirResourceHelper.createQuestionnaireResponse(null, questions, new ArrayList<String>());
	}

//	@Test
//	public void testCreateQuestionnaireResponse_nullExistingQR() {
//		List<String> questions = new ArrayList<String>();
//		questions.add("Question 1");
//		questions.add("Question 2");
//
//		List<String> answers = new ArrayList<String>();
//		answers.add("Answer 1");
//		answers.add("Answer 2");
//		QuestionnaireResponse questionnaireResponse = FhirResourceHelper.createQuestionnaireResponse(null, questions,
//				answers);
//
//		assertEquals(QuestionnaireResponse.QuestionnaireResponseStatus.COMPLETED, questionnaireResponse.getStatus());
//		assertEquals(2, questionnaireResponse.getGroup().getQuestion().size());
//		QuestionComponent question1 = questionnaireResponse.getGroup().getQuestion().get(0);
//		assertEquals("Question 1", question1.getLinkId());
//		assertEquals("Answer 1", question1.getAnswer().get(0).getValue().toString());
//		QuestionComponent question2 = questionnaireResponse.getGroup().getQuestion().get(1);
//		assertEquals("Question 2", question2.getLinkId());
//		assertEquals("Answer 2", question2.getAnswer().get(0).getValue().toString());
//
//	}

//	@Test
//	public void testCreateQuestionnaireResponse() {
//		List<String> questions = new ArrayList<String>();
//		questions.add("Question 1");
//		questions.add("Question 2");
//
//		List<String> answers = new ArrayList<String>();
//		answers.add("Answer 1");
//		answers.add("Answer 2");
//
//		QuestionnaireResponse initialResponse = new QuestionnaireResponse();
//		initialResponse.setGroup(new Coverage.GroupComponent());
//
//		QuestionnaireResponse questionnaireResponse = FhirResourceHelper.createQuestionnaireResponse(initialResponse,
//				questions,
//				answers);
//
//		assertEquals(QuestionnaireResponse.QuestionnaireResponseStatus.COMPLETED, questionnaireResponse.getStatus());
//		assertEquals(2, questionnaireResponse.getGroup().getQuestion().size());
//		QuestionComponent question1 = questionnaireResponse.getGroup().getQuestion().get(0);
//		assertEquals("Question 1", question1.getLinkId());
//		assertEquals("Answer 1", question1.getAnswer().get(0).getValue().toString());
//		QuestionComponent question2 = questionnaireResponse.getGroup().getQuestion().get(1);
//		assertEquals("Question 2", question2.getLinkId());
//		assertEquals("Answer 2", question2.getAnswer().get(0).getValue().toString());
//
//	}

	private EFormValue createEFormValue(String name, String value) {
		EFormValue eformValue = new EFormValue();
		eformValue.setVarName(name);
		eformValue.setVarValue(value);
		return eformValue;
	}


}
