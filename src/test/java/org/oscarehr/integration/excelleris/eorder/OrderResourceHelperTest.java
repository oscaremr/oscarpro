package org.oscarehr.integration.excelleris.eorder;

import java.io.InputStream;

import ca.uhn.fhir.context.FhirContext;
import org.hl7.fhir.r4.formats.JsonParser;
import org.hl7.fhir.r4.model.Bundle;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class OrderResourceHelperTest {


	@Before
	public void setUp() {
		m_context = FhirContext.forR4();
	}
		@Test
	public void questionnaireRequestTest() throws Exception {

		// prepare test data
		InputStream inputStream = OrderResourceHelperTest.class.getResourceAsStream("/org/oscarehr/integration/excelleris/eorder/Parameters-with-questionnaire-request.json");
		Bundle resource = (Bundle) m_context.newJsonParser().parseResource(inputStream);

		// execute test
		OrderResponseHelper helper = new OrderResponseHelper(resource);
		
		assertNotNull(helper.getQuestionnaire());
	}
	
	@Test
	public void orderIdTest() throws Exception {

		// prepare test data
		InputStream inputStream = OrderResourceHelperTest.class.getResourceAsStream("/org/oscarehr/integration/excelleris/eorder/Parameters-with-pdf.json");

		Bundle resource = (Bundle) m_context.newJsonParser().parseResource(inputStream);
				
		// execute test
		OrderResponseHelper helper = new OrderResponseHelper(resource);
		
		assertNotNull(helper.getOrderId());
	}
	
	@Test
	public void pdfLabRequisitionFormTest() throws Exception {

		// prepare test data
		InputStream inputStream = OrderResourceHelperTest.class.getResourceAsStream("/org/oscarehr/integration/excelleris/eorder/Parameters-with-pdf.json");
		Bundle resource = (Bundle) m_context.newJsonParser().parseResource(inputStream);
				
		// execute test
		OrderResponseHelper helper = new OrderResponseHelper(resource);
		
		assertNotNull(helper.getPdfLabRequisitionForm());
	}
	
	@Test
	public void patientIdTest() throws Exception {

		// prepare test data
		JsonParser parser = new JsonParser();		
		InputStream inputStream = OrderResourceHelperTest.class.getResourceAsStream("/org/oscarehr/integration/excelleris/eorder/Parameters-with-pdf.json");
		Bundle resource = (Bundle) m_context.newJsonParser().parseResource(inputStream);
				
		// execute test
		OrderResponseHelper helper = new OrderResponseHelper(resource);
		
		assertNotNull(helper.getPatientId());
	}

	FhirContext m_context = null;

}
