/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import static org.junit.Assert.assertEquals;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.oscarehr.util.MiscUtils;

public class QuestionTextHelperTest {
	
	private static final Logger LOG = MiscUtils.getLogger();
	
	@Test
	public void testGetQuestionLinkId() {
		String linkId = QuestionTextHelper.getQuestionLinkId("Test Code", "Question");
		
		String expectedLinkId = "GUI|TestCode|Test Code|Global|1|Question";
		assertEquals(linkId, expectedLinkId);
	}

	@Test
	public void testGetSourceQuestionLinkId() {
		String qlId = QuestionTextHelper.getSourceQuestionLinkId("Test Code", "QLID");

		String expectedLinkId = "GUI|TestCode|Test Code|Global|1|Source|QLID";
		assertEquals(qlId, expectedLinkId);
	}
}
