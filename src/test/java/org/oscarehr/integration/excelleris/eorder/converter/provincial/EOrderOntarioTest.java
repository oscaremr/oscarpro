package org.oscarehr.integration.excelleris.eorder.converter.provincial;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import java.util.Map;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.OrderLabTestCodeCacheDao;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.excelleris.eorder.converter.DiagnosticOrderConverter;
import org.oscarehr.util.SpringUtils;

@ExtendWith(MockitoExtension.class)
public class EOrderOntarioTest {

  @Mock
  Province province;

  @Mock
  Map<String, EFormValue> valueMap;

  @Mock
  QuestionnaireResponse questionnaireResponse;

  @Mock
  Questionnaire questionnaire;

  @Mock
  EFormValue eformValue;

  @Mock
  OrderLabTestCodeCacheDao orderLabTestCodeCacheDao;

  private AutoCloseable closeable;
  private MockedStatic<SpringUtils> springUtils;
  private MockedStatic<DiagnosticOrderConverter> diagnosticOrderConverter;

  @BeforeEach
  public void initialize() throws Exception {
    closeable = MockitoAnnotations.openMocks(this);
    mockObjects();
  }

  @AfterEach
  public void closeService() throws Exception {
    closeable.close();
    springUtils.close();
    diagnosticOrderConverter.close();
  }

  @Test
  public void test_getTestCode() {
    EOrderOntario eOrderOntario = new EOrderOntario(province, valueMap);
    String testCode = eOrderOntario.getTestCode("T_free_psa", questionnaireResponse,
        null, questionnaire);
    assertEquals("TR10373-9", testCode);

    testCode = eOrderOntario.getTestCode("T_chlamydia", questionnaireResponse, null,
        questionnaire);
    assertEquals("TR10689-8", testCode);

    testCode = eOrderOntario.getTestCode("T_hba1c", questionnaireResponse, null,
        questionnaire);
    assertEquals("TR10228-5", testCode);
  }

  private void mockObjects() {
    springUtils = mockStatic(SpringUtils.class);
    diagnosticOrderConverter = mockStatic(DiagnosticOrderConverter.class);

    springUtils.when(() -> SpringUtils.getBean(OrderLabTestCodeCacheDao.class))
        .thenReturn(orderLabTestCodeCacheDao);

    diagnosticOrderConverter
        .when(() -> DiagnosticOrderConverter.getTestCode(eq("T_free_psa_insured"), eq("ON")))
        .thenReturn("TR10373-9");

    diagnosticOrderConverter
        .when(() -> DiagnosticOrderConverter.getTestCode(eq("T_chlamydia_inv_urine"), eq("ON")))
        .thenReturn("TR10689-8");

    diagnosticOrderConverter
        .when(() -> DiagnosticOrderConverter.getTestCode(eq("T_hba1c"), eq("ON")))
        .thenReturn("TR10228-5");

    when(province.name()).thenReturn("ON");
    when(valueMap.containsKey("T_psa_insured")).thenReturn(true);
    when(valueMap.containsKey("T_chlamydia_src")).thenReturn(true);
    when(valueMap.get("T_chlamydia_src")).thenReturn(eformValue);
    when(eformValue.getVarValue()).thenReturn("URINE");
  }
}
