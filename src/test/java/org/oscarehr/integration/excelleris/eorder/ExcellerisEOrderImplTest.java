package org.oscarehr.integration.excelleris.eorder;

import static org.junit.Assert.assertNotNull;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import org.hl7.fhir.r4.model.Bundle;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.util.ResourceUtils;
import ca.uhn.fhir.context.FhirContext;

@Ignore // OSCAR-3148 Ignored for lack of credentials.
public class ExcellerisEOrderImplTest {

  private ExcellerisEOrderImpl excellerisEOrderImpl;

  @Before
  public void setUp() {
    final Properties properties = new Properties();
    properties.setProperty(PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_ENABLE,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_ENABLE);
    properties.setProperty(PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_HOST,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_HOST);
    properties.setProperty(PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_PORT,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_PORT);
    properties.setProperty(PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_BASE_URL,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_BASE_URL);
    properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_CLIENT_SSL_ENABLE,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_CLIENT_SSL_ENABLE);
    properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_LOGIN_URL_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_LOGIN_URL);
    properties.setProperty(ConfigureConstants.EXCELLERIS_USER_V2_PROVINCE_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_USER_PROVINCE);
    properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_INSTANCE_AFFINITY_DISABLE,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_INSTANCE_AFFINITY_DISABLE);
    properties.setProperty(ConfigureConstants.EXCELLERIS_USER_V2_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_USER);
    properties.setProperty(ConfigureConstants.EXCELLERIS_USER_V2_PASSWORD_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_USER_PASSWORD);
    properties.setProperty(ConfigureConstants.EXCELLERIS_USER_V2_PROVINCE_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_USER_PROVINCE);
    properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_KEYSTORE_FILE_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_KEYSTORE_FILE);
    properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_TRUSTSTORE_FILE_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_FILE);
    properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_KEYSTORE_PASSWORD_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_KEYSTORE_PASSWORD);
    properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_TRUSTSTORE_PASSWORD_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_PASSWORD);
    properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_API_VERSION_PARAM_NAME,
        PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_API_VERSION);

    excellerisEOrderImpl = new ExcellerisEOrderImpl(properties);
  }

  @Test
  public void test_submitExcellerisEOrder() {

    AuthenticationToken token = excellerisEOrderImpl.authenticate();
    Bundle orderResponse = excellerisEOrderImpl.submitExcellerisEOrder(token, getBundle());

    assertNotNull("Should not be null", orderResponse);
  }

  private Bundle getBundle() {
    String eOrderString = "";
    Bundle bundle = null;
    try {
      File file = ResourceUtils.getFile("classpath:eorder_request.json");
      byte[] eOrderData = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
      eOrderString = new String(eOrderData);

      FhirContext fhirContext = FhirContext.forR4();
      bundle = fhirContext.newJsonParser().parseResource(Bundle.class, eOrderString);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return bundle;
  }
}
