package org.oscarehr.integration.excelleris.eorder;

import java.util.HashMap;
import java.util.Map;
import org.apache.http.client.HttpClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Disabled // TODO: Fix missing DOCUMENT_DIR configuration pathing
public class HttpClientHelperTest {
	
	@Test
	public void testCreateHttpClient() throws Exception {
		Map<String, String> cookieMap = new HashMap<>();
		HttpClient httpClient = HttpClientHelper.createHttpClient(cookieMap);
		Assertions.assertNotNull(httpClient, "should not be null");
	}
}
