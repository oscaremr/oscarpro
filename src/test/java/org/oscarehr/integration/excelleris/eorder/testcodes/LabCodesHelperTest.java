package org.oscarehr.integration.excelleris.eorder.testcodes;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class LabCodesHelperTest {

  @Test
  public void test_convertKey() {
    String convertedKey = LabCodesHelper.convertKey("Albumin");
    assertEquals("T_albumin", convertedKey);
  }
}
