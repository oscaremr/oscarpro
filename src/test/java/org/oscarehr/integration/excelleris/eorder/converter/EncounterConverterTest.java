/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hl7.fhir.r4.model.Encounter;
import org.hl7.fhir.r4.model.Encounter.EncounterStatus;
import org.junit.Test;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.util.MiscUtils;

public class EncounterConverterTest {
	
	private static final Logger LOG = MiscUtils.getLogger();
	
	

	@Test(expected = ServiceException.class)
	public void TestToFhirObject_noLocation() {
		// first name
		List<EFormValue> eformValues = new ArrayList<>();
		EFormValue eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_FIRST_NAME);
		eformValue.setVarValue("John");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_LAST_NAME);
		eformValue.setVarValue("Doe");

		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
		eformValue.setVarValue("DAV77092A");

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		Encounter encounter = EncounterConverter.toFhirObject(valueMap);

	}

	
	public void testToFhirObject_nullLastName() {
		// first name
		List<EFormValue> eformValues = new ArrayList<>();
		EFormValue eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_FIRST_NAME);
		eformValue.setVarValue("John");

		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
		eformValue.setVarValue("DAV77092A");
		// LifeLabs id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_LIFELAB_ID);
		eformValue.setVarValue("1526201");

		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(LocationPropertySet.LOCATION_URL);
		eformValue.setVarValue("locationUrl");

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		Encounter encounter = EncounterConverter.toFhirObject(valueMap);

		assertEquals(EncounterStatus.PLANNED, encounter.getStatus());
		assertEquals("locationUrl", encounter.getLocation().get(0).getLocation().getReference());
		assertEquals(0, encounter.getLocation().size());
		
	}

			}