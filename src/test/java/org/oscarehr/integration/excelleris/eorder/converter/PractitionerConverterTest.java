/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.Practitioner;
import org.junit.Test;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.util.MiscUtils;

public class PractitionerConverterTest {
	
	private static final Logger LOG = MiscUtils.getLogger();
	
	@Test(expected = ServiceException.class)
	public void testToFhirObject_nullExcellerisId() {

		Map<String, EFormValue> valueMap = new HashMap<String, EFormValue>();

		PractitionerConverter.toFhirObject(valueMap);

	}

	@Test(expected = ServiceException.class)
	public void TestToFhirObject_nullLifeLabsId() {
		// first name
		List<EFormValue> eformValues = new ArrayList<>();
		EFormValue eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_FIRST_NAME);
		eformValue.setVarValue("John");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_LAST_NAME);
		eformValue.setVarValue("Doe");

		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
		eformValue.setVarValue("DAV77092A");

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		Practitioner practitioner = PractitionerConverter.toFhirObject(valueMap);

	}

	@Test(expected = ServiceException.class)
	public void testToFhirObject_nullLastName() {
		// first name
		List<EFormValue> eformValues = new ArrayList<>();
		EFormValue eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_FIRST_NAME);
		eformValue.setVarValue("John");

		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
		eformValue.setVarValue("DAV77092A");
		// LifeLabs id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_LIFELAB_ID);
		eformValue.setVarValue("1526201");

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerConverter.toFhirObject(valueMap);
	}

	@Test(expected = ServiceException.class)
	public void testToFhirObject_nullFirstName() {
		List<EFormValue> eformValues = new ArrayList<>();
		EFormValue eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_LAST_NAME);
		eformValue.setVarValue("Doe");

		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
		eformValue.setVarValue("DAV77092A");
		// LifeLabs id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_LIFELAB_ID);
		eformValue.setVarValue("1526201");

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerConverter.toFhirObject(valueMap);
	}

	@Test
	public void testToFhirObject() {
		// first name
		List<EFormValue> eformValues = new ArrayList<>();
		EFormValue eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_FIRST_NAME);
		eformValue.setVarValue("John");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_LAST_NAME);
		eformValue.setVarValue("Doe");

		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
		eformValue.setVarValue("DAV77092A");
		// LifeLabs id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_LIFELAB_ID);
		eformValue.setVarValue("1526201");

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		Practitioner practitioner = PractitionerConverter.toFhirObject(valueMap);
		assertEquals(practitioner.getName().get(0).getUse(), HumanName.NameUse.OFFICIAL);
		assertEquals(practitioner.getName().get(0).getFamily(), "Doe");
		assertEquals(practitioner.getName().get(0).getGiven().get(0).asStringValue(), "John");
		assertEquals(practitioner.getName().get(0).getPrefix().get(0).asStringValue(), "DR.");
		assertEquals(practitioner.getId(), "DAV77092A");
		assertEquals(practitioner.getIdentifier().size(), 2);
	}


	@Test
	public void testToFhirObjectFromPropertyConfig() {
		
		List<EFormValue> eformValues = new ArrayList<>();
		
		// Patient
		// first name
		EFormValue eformValue = new EFormValue();
		// Ordering provider
		// first name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("FNAME");
		eformValue.setVarValue("Jane");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("LNAME");
		eformValue.setVarValue("Doe");
		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("EXCELLERIS_ID");
		eformValue.setVarValue("DAV77092A");
		// LifeLabs id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("LL_ID");
		eformValue.setVarValue("1526201");
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerPropertyConfig config = new PractitionerPropertyConfig("FNAME", "LNAME", "EXCELLERIS_ID", "LL_ID",
				"TITLE");
		

		Practitioner practitioner = PractitionerConverter.toFhirObject(valueMap, null, config);

		assertEquals(practitioner.getName().get(0).getUse(), HumanName.NameUse.OFFICIAL);
		assertEquals(practitioner.getName().get(0).getFamily(), "Doe");
		assertEquals(practitioner.getName().get(0).getGiven().get(0).asStringValue(), "Jane");
		assertEquals(practitioner.getName().get(0).getPrefix().get(0).asStringValue(), "DR.");
		assertEquals(practitioner.getId(), "DAV77092A");
		assertEquals(practitioner.getIdentifier().size(), 2);

	}

	@Test
	public void testToFhirObjectFromPropertyConfig_unknownProvider() {

		List<EFormValue> eformValues = new ArrayList<>();

		// Patient
		// first name
		EFormValue eformValue = new EFormValue();
		// Ordering provider
		// first name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("FNAME");
		eformValue.setVarValue("Jane");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("LNAME");
		eformValue.setVarValue("Doe");
		// execelleris id
//		eformValue = new EFormValue();
//		eformValues.add(eformValue);
//		eformValue.setVarName("EXCELLERIS_ID");
//		eformValue.setVarValue("DAV77092A");
		// LifeLabs id
//		eformValue = new EFormValue();
//		eformValues.add(eformValue);
//		eformValue.setVarName("LL_ID");
//		eformValue.setVarValue("1526201");
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerPropertyConfig config = new PractitionerPropertyConfig("FNAME", "LNAME", "EXCELLERIS_ID", "LL_ID",
				"TITLE");

		Practitioner practitioner = PractitionerConverter.toFhirObject(valueMap, 3, config);

		assertEquals(practitioner.getName().get(0).getUse(), HumanName.NameUse.OFFICIAL);
		assertEquals(practitioner.getName().get(0).getFamily(), "Doe");
		assertEquals(practitioner.getName().get(0).getGiven().get(0).asStringValue(), "Jane");
		assertEquals(practitioner.getName().get(0).getPrefix().get(0).asStringValue(), "DR.");
		assertEquals(practitioner.getId(), "30");
		assertEquals(practitioner.getIdentifier().size(), 1);
		assertEquals(practitioner.getIdentifier().get(0).getValue(), "UNKNOWN2");

	}

	@Test(expected = ServiceException.class)
	public void testToFhirObjectFromPropertyConfig_nullExcellerisIdAndUnknownProvider() {

		List<EFormValue> eformValues = new ArrayList<>();

		// Patient
		// first name
		EFormValue eformValue = new EFormValue();
		// Ordering provider
		// first name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("FNAME");
		eformValue.setVarValue("Jane");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("LNAME");
		eformValue.setVarValue("Doe");
		// LifeLabs id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("LL_ID");
		eformValue.setVarValue("1526201");
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerPropertyConfig config = new PractitionerPropertyConfig("FNAME", "LNAME", "EXCELLERIS_ID", "LL_ID",
				"TITLE");

		PractitionerConverter.toFhirObject(valueMap, null, config);

	}

	@Test
	public void testToFhirObjectFromPropertyConfig_nullFirstNameId() {

		List<EFormValue> eformValues = new ArrayList<>();

		// Patient
		// first name
		EFormValue eformValue = new EFormValue();

		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("LNAME");
		eformValue.setVarValue("Doe");
		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("EXCELLERIS_ID");
		eformValue.setVarValue("DAV77092A");
		// LifeLabs id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("LL_ID");
		eformValue.setVarValue("1526201");
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerPropertyConfig config = new PractitionerPropertyConfig("FNAME", "LNAME", "EXCELLERIS_ID", "LL_ID",
				"TITLE");

		Practitioner practitioner = PractitionerConverter.toFhirObject(valueMap, null, config);

		assertEquals(practitioner.getName().get(0).getUse(), HumanName.NameUse.OFFICIAL);
		assertEquals(practitioner.getName().get(0).getFamily(), "Doe");
		assertEquals(practitioner.getName().get(0).getGiven().get(0).asStringValue(), "UNK");
		assertEquals(practitioner.getName().get(0).getPrefix().get(0).asStringValue(), "DR.");
		assertEquals(practitioner.getId(), "DAV77092A");
		assertEquals(practitioner.getIdentifier().size(), 2);
	}

	@Test(expected = ServiceException.class)
	public void testToFhirObjectFromPropertyConfig_nullLastName() {

		List<EFormValue> eformValues = new ArrayList<>();

		EFormValue eformValue = new EFormValue();
		// Ordering provider
		// first name
		eformValues.add(eformValue);
		eformValue.setVarName("FNAME");
		eformValue.setVarValue("Jane");

		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("EXCELLERIS_ID");
		eformValue.setVarValue("DAV77092A");
		// LifeLabs id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("LL_ID");
		eformValue.setVarValue("1526201");
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerPropertyConfig config = new PractitionerPropertyConfig("FNAME", "LNAME", "EXCELLERIS_ID", "LL_ID",
				"TITLE");

		PractitionerConverter.toFhirObject(valueMap, null, config);
	}

	@Test(expected = ServiceException.class)
	public void testToFhirObjectFromPropertyConfig_nullLifeLabsId() {

		List<EFormValue> eformValues = new ArrayList<>();

		// Patient
		// first name
		EFormValue eformValue = new EFormValue();
		// Ordering provider
		// first name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("FNAME");
		eformValue.setVarValue("Jane");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("LNAME");
		eformValue.setVarValue("Doe");
		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("EXCELLERIS_ID");
		eformValue.setVarValue("DAV77092A");

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerPropertyConfig config = new PractitionerPropertyConfig("FNAME", "LNAME", "EXCELLERIS_ID", "LL_ID",
				"TITLE");

		PractitionerConverter.toFhirObject(valueMap, null, config);

	}

	@Test
	public void testHasCopyToProviderData() {

		List<EFormValue> eformValues = new ArrayList<>();

		// Patient
		// first name
		EFormValue eformValue = new EFormValue();
		// Ordering provider
		// first name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("FNAME");
		eformValue.setVarValue("Jane");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("LNAME");
		eformValue.setVarValue("Doe");
		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("EXCELLERIS_ID");
		eformValue.setVarValue("DAV77092A");
		// LifeLabs id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("LL_ID");
		eformValue.setVarValue("1526201");

		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("TITLE");
		eformValue.setVarValue("DR.");
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerPropertyConfig config = new PractitionerPropertyConfig("FNAME", "LNAME", "EXCELLERIS_ID", "LL_ID",
				"TITLE");
		boolean hasCopyToProviderData = PractitionerConverter.hasCopyToProviderData(valueMap, config);
		assertTrue(hasCopyToProviderData);
	}

	@Test
	public void testHasCopyToProviderData_fName() {

		List<EFormValue> eformValues = new ArrayList<>();

		// Patient
		// first name
		EFormValue eformValue = new EFormValue();
		// Ordering provider
		// first name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("FNAME");
		eformValue.setVarValue("Jane");
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerPropertyConfig config = new PractitionerPropertyConfig("FNAME", "LNAME", "EXCELLERIS_ID", "LL_ID",
				"TITLE");
		boolean hasCopyToProviderData = PractitionerConverter.hasCopyToProviderData(valueMap, config);
		assertTrue(hasCopyToProviderData);
	}

	@Test
	public void testHasCopyToProviderData_lname() {

		List<EFormValue> eformValues = new ArrayList<>();

		EFormValue eformValue = new EFormValue();
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("LNAME");
		eformValue.setVarValue("Doe");

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerPropertyConfig config = new PractitionerPropertyConfig("FNAME", "LNAME", "EXCELLERIS_ID", "LL_ID",
				"TITLE");
		boolean hasCopyToProviderData = PractitionerConverter.hasCopyToProviderData(valueMap, config);
		assertTrue(hasCopyToProviderData);
	}

	@Test
	public void testHasCopyToProviderData_excellerisId() {

		List<EFormValue> eformValues = new ArrayList<>();

		// Patient
		// first name
		EFormValue eformValue = new EFormValue();
		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("EXCELLERIS_ID");
		eformValue.setVarValue("DAV77092A");
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerPropertyConfig config = new PractitionerPropertyConfig("FNAME", "LNAME", "EXCELLERIS_ID", "LL_ID",
				"TITLE");
		boolean hasCopyToProviderData = PractitionerConverter.hasCopyToProviderData(valueMap, config);
		assertTrue(hasCopyToProviderData);
	}

	@Test
	public void testHasCopyToProviderData_lifeLabsId() {

		List<EFormValue> eformValues = new ArrayList<>();

		EFormValue eformValue = new EFormValue();
		// LifeLabs id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("LL_ID");
		eformValue.setVarValue("1526201");
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerPropertyConfig config = new PractitionerPropertyConfig("FNAME", "LNAME", "EXCELLERIS_ID", "LL_ID",
				"TITLE");
		boolean hasCopyToProviderData = PractitionerConverter.hasCopyToProviderData(valueMap, config);
		assertTrue(hasCopyToProviderData);
	}

	@Test
	public void testHasCopyToProviderData_title() {

		List<EFormValue> eformValues = new ArrayList<>();

		EFormValue eformValue = new EFormValue();
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("TITLE");
		eformValue.setVarValue("DR.");
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerPropertyConfig config = new PractitionerPropertyConfig("FNAME", "LNAME", "EXCELLERIS_ID", "LL_ID",
				"TITLE");
		boolean hasCopyToProviderData = PractitionerConverter.hasCopyToProviderData(valueMap, config);
		assertTrue(hasCopyToProviderData);
	}

	@Test
	public void testHasCopyToProviderData_false() {

		List<EFormValue> eformValues = new ArrayList<>();

		EFormValue eformValue = new EFormValue();
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("City");
		eformValue.setVarValue("Vancouver");
		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		PractitionerPropertyConfig config = new PractitionerPropertyConfig("FNAME", "LNAME", "EXCELLERIS_ID", "LL_ID",
				"TITLE");
		boolean hasCopyToProviderData = PractitionerConverter.hasCopyToProviderData(valueMap, config);
		assertFalse(hasCopyToProviderData);
	}

	@Test
	public void testGetUnknownProvoderId() {
		assertEquals("0", PractitionerConverter.getUnknownProviderId(0));
		assertEquals("50", PractitionerConverter.getUnknownProviderId(5));
	}

	@Test
	public void testGetUnknownProvoderCode() {
		assertEquals("UNKNOWN-1", PractitionerConverter.getUknownProviderCode(0));
		assertEquals("UNKNOWN4", PractitionerConverter.getUknownProviderCode(5));
	}


}
