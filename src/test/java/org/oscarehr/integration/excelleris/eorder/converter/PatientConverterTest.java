/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.excelleris.eorder.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hl7.fhir.r4.model.Address;
import org.hl7.fhir.r4.model.Address.AddressType;
import org.hl7.fhir.r4.model.Address.AddressUse;
import org.hl7.fhir.r4.model.Coding;
import org.hl7.fhir.r4.model.ContactPoint;
import org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem;
import org.hl7.fhir.r4.model.ContactPoint.ContactPointUse;
import org.hl7.fhir.r4.model.Enumerations.AdministrativeGender;
import org.hl7.fhir.r4.model.HumanName;
import org.hl7.fhir.r4.model.HumanName.NameUse;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Identifier.IdentifierUse;
import org.hl7.fhir.r4.model.Patient;
import org.junit.Test;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialBusinessLogic;
import org.oscarehr.integration.excelleris.eorder.converter.provincial.ProvincialFactory;

public class PatientConverterTest {
	

	@Test
	public void TestToFhirObject() {
		List<EFormValue> eformValues = new ArrayList<>();

		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_MIDDLE_NAME, "C."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Doe"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_TITLE, "Mr."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER_MALE, "Male"));
//		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "Male"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "1991/07/11"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "BC"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V9D0S3"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "1-722-721-1113"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.EXCELLERIS_PATIENT_ID, "PAT1322"));

		EFormValue eformValue = new EFormValue();
		eformValue.setVarName("M_ExcellerisEOrderType");
		eformValue.setVarValue("ON");
		eformValues.add(eformValue);

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		final ProvincialBusinessLogic provincial = new ProvincialFactory(valueMap).getProvincialBusinessLogic();
		Patient patient = PatientConverter.toFhirObject(provincial);
		// health care number identifier
		Identifier identifier = patient.getIdentifier().get(0);
		Coding coding = identifier.getType().getCoding().get(0);
		assertEquals(identifier.getUse(), IdentifierUse.OFFICIAL);
		assertEquals("hcn", coding.getCode());
		assertEquals(coding.getSystem(), OidConstants.HEALTH_INSURANCE_NUMBER_OID);
		assertEquals(identifier.getSystem(), OidConstants.HEALTH_INSURANCE_NUMBER_OID);
		assertEquals(null, identifier.getValue());
		assertEquals(identifier.getAssigner().getDisplay(), "ON");
		// name
		HumanName name = patient.getName().get(0);
		assertEquals("Doe", name.getFamily());
		assertEquals("John", name.getGiven().get(0).asStringValue());
		assertEquals("Mr.", name.getPrefix().get(0).asStringValue());
		assertEquals(NameUse.OFFICIAL, name.getUse());

//		gender
		assertEquals(AdministrativeGender.MALE, patient.getGender());
//		dob
		assertEquals("1991-07-11T00:00:00.000Z", patient.getBirthDateElement().asStringValue()+"T00:00:00.000Z");
//		address
		Address address = FhirResourceHelper.createAddress(valueMap);
		assertEquals(AddressUse.HOME, address.getUse());
		assertEquals(AddressType.BOTH, address.getType());
		assertEquals("22 Main St.", address.getLine().get(0).asStringValue());
		assertEquals("Vancouver", address.getCity());
		assertEquals("CAN", address.getCountry());
		assertEquals("BC", address.getState());
		assertEquals("V9D 0S3", address.getPostalCode());
//		phone
		ContactPoint contactPoint = patient.getTelecom().get(0);
		assertEquals(ContactPointSystem.PHONE, contactPoint.getSystem());
		assertEquals("1-722-721-1113", contactPoint.getValue());
		assertEquals(ContactPointUse.HOME, contactPoint.getUse());
//		excellerisId
		assertEquals("PAT1322", patient.getId());
	}

	@Test
	public void TestToFhirObject_OHIP() {
		List<EFormValue> eformValues = new ArrayList<>();

		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_MIDDLE_NAME, "C."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Doe"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_OHIP", "OHIP"));
		eformValues.add(
				EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER, "1234567890"));
//		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_TITLE, "Mr."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "Male"));
//		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "Male"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "1991/07/11"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "BC"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V9D0S3"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "1-722-721-1113"));
//		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.EXCELLERIS_PATIENT_ID, "PAT1322"));

		EFormValue eformValue = new EFormValue();
		eformValue.setVarName("M_ExcellerisEOrderType");
		eformValue.setVarValue("ON");
		eformValues.add(eformValue);

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		final ProvincialBusinessLogic provincial = new ProvincialFactory(valueMap).getProvincialBusinessLogic();
		Patient patient = PatientConverter.toFhirObject(provincial);
		// health care number identifier
		Identifier identifier = patient.getIdentifier().get(0);
		Coding coding = identifier.getType().getCoding().get(0);
		assertEquals(identifier.getUse(), IdentifierUse.OFFICIAL);
		assertEquals(coding.getCode(), "hcn");
		assertEquals(coding.getSystem(), OidConstants.HEALTH_INSURANCE_NUMBER_OID);
		assertEquals(identifier.getSystem(), OidConstants.HEALTH_INSURANCE_NUMBER_OID);
		assertEquals(identifier.getValue(), "1234567890");
		assertEquals(identifier.getAssigner().getDisplay(), "ON");
//		name
		HumanName name = patient.getName().get(0);
		assertEquals("Doe", name.getFamily());
		assertEquals("John", name.getGiven().get(0).asStringValue());
		assertEquals("UNK", name.getPrefix().get(0).asStringValue());
		assertEquals(NameUse.OFFICIAL, name.getUse());
//		gender
		assertEquals(AdministrativeGender.MALE, patient.getGender());
//		dob
		assertEquals("1991-07-11T00:00:00.000Z", patient.getBirthDateElement().asStringValue()+"T00:00:00.000Z");
//		address
		Address address = FhirResourceHelper.createAddress(valueMap);
		assertEquals(AddressUse.HOME, address.getUse());
		assertEquals(AddressType.BOTH, address.getType());
		assertEquals("22 Main St.", address.getLine().get(0).asStringValue());
		assertEquals("Vancouver", address.getCity());
		assertEquals("CAN", address.getCountry());
		assertEquals("BC", address.getState());
		assertEquals("V9D 0S3", address.getPostalCode());
//		phone
		ContactPoint contactPoint = patient.getTelecom().get(0);
		assertEquals(ContactPointSystem.PHONE, contactPoint.getSystem());
		assertEquals("1-722-721-1113", contactPoint.getValue());
		assertEquals(ContactPointUse.HOME, contactPoint.getUse());
//		excellerisId
		assertNull(patient.getId());
	}
	
	@Test
	public void TestToFhirObject_WSIB() {
		List<EFormValue> eformValues = new ArrayList<>();

		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_MIDDLE_NAME, "C."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Doe"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_WSIB", "WSIB"));
		eformValues.add(
				EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER, "1234567890"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "1991/07/11"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "BC"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V9D0S3"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "1-722-721-1113"));

		EFormValue eformValue = new EFormValue();
		eformValue.setVarName("M_ExcellerisEOrderType");
		eformValue.setVarValue("ON");
		eformValues.add(eformValue);

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		final ProvincialBusinessLogic provincial = new ProvincialFactory(valueMap).getProvincialBusinessLogic();
		Patient patient = PatientConverter.toFhirObject(provincial);
		// health care number identifier
		Identifier identifier = patient.getIdentifier().get(0);
		Coding coding = identifier.getType().getCoding().get(0);
		assertEquals(identifier.getUse(), IdentifierUse.OFFICIAL);
		assertEquals(coding.getCode(), "hcn");
		assertEquals(coding.getSystem(), OidConstants.HEALTH_INSURANCE_NUMBER_OID);
		assertEquals(identifier.getSystem(), OidConstants.HEALTH_INSURANCE_NUMBER_OID);
		assertEquals(identifier.getValue(), "1234567890");
		assertEquals(identifier.getAssigner().getDisplay(), "ON");
//		name
		HumanName name = patient.getName().get(0);
		assertEquals("Doe", name.getFamily());
		assertEquals("John", name.getGiven().get(0).asStringValue());
		assertEquals("UNK", name.getPrefix().get(0).asStringValue());
		assertEquals(NameUse.OFFICIAL, name.getUse());
//		gender
		assertEquals(AdministrativeGender.UNKNOWN, patient.getGender());
//		dob
		assertEquals("1991-07-11T00:00:00.000Z", patient.getBirthDateElement().asStringValue()+"T00:00:00.000Z");
//		address
		Address address = FhirResourceHelper.createAddress(valueMap);
		assertEquals(AddressUse.HOME, address.getUse());
		assertEquals(AddressType.BOTH, address.getType());
		assertEquals("22 Main St.", address.getLine().get(0).asStringValue());
		assertEquals("Vancouver", address.getCity());
		assertEquals("CAN", address.getCountry());
		assertEquals("BC", address.getState());
		assertEquals("V9D 0S3", address.getPostalCode());
//		phone
		ContactPoint contactPoint = patient.getTelecom().get(0);
		assertEquals(ContactPointSystem.PHONE, contactPoint.getSystem());
		assertEquals("1-722-721-1113", contactPoint.getValue());
		assertEquals(ContactPointUse.HOME, contactPoint.getUse());
//		excellerisId
		assertNull(patient.getId());
	}

	@Test(expected = ServiceException.class)
	public void TestToFhirObject_missingHealthInsuranceNumber() {
		List<EFormValue> eformValues = new ArrayList<>();

		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_MIDDLE_NAME, "C."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Doe"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_WSIB", "WSIB"));
//		eformValues.add(
//				EFormValueHelper.createEFormValue(PatientPropertySet.PAITENT_HEALTH_INSURANCE_NUMBER, "1234567890"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "1991/07/11"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "BC"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V9D0S3"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "1-722-721-1113"));

		EFormValue eformValue = new EFormValue();
		eformValue.setVarName("M_ExcellerisEOrderType");
		eformValue.setVarValue("ON");
		eformValues.add(eformValue);

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		final ProvincialBusinessLogic provincial = new ProvincialFactory(valueMap).getProvincialBusinessLogic();
		PatientConverter.toFhirObject(provincial);
	}

	@Test(expected = ServiceException.class)
	public void TestToFhirObject_missingLastName() {
		List<EFormValue> eformValues = new ArrayList<>();

		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_MIDDLE_NAME, "C."));
//		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Doe"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_WSIB", "WSIB"));
		eformValues.add(
				EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER, "1234567890"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "1991/07/11"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "BC"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V9D0S3"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "1-722-721-1113"));

		EFormValue eformValue = new EFormValue();
		eformValue.setVarName("M_ExcellerisEOrderType");
		eformValue.setVarValue("ON");
		eformValues.add(eformValue);

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		final ProvincialBusinessLogic provincial = new ProvincialFactory(valueMap).getProvincialBusinessLogic();
		PatientConverter.toFhirObject(provincial);
	}

	@Test(expected = ServiceException.class)
	public void TestToFhirObject_missingFirstName() {
		List<EFormValue> eformValues = new ArrayList<>();

//		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_MIDDLE_NAME, "C."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Doe"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_WSIB", "WSIB"));
		eformValues.add(
				EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER, "1234567890"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "1991/07/11"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "BC"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V9D0S3"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "1-722-721-1113"));

		EFormValue eformValue = new EFormValue();
		eformValue.setVarName("M_ExcellerisEOrderType");
		eformValue.setVarValue("ON");
		eformValues.add(eformValue);

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		final ProvincialBusinessLogic provincial = new ProvincialFactory(valueMap).getProvincialBusinessLogic();
		PatientConverter.toFhirObject(provincial);
	}

	@Test(expected = ServiceException.class)
	public void TestToFhirObject_missingDOB() {
		List<EFormValue> eformValues = new ArrayList<>();

		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_MIDDLE_NAME, "C."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Doe"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_WSIB", "WSIB"));
		eformValues.add(
				EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER, "1234567890"));
//		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "1991/07/11"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "BC"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V9D0S3"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "1-722-721-1113"));

		EFormValue eformValue = new EFormValue();
		eformValue.setVarName("M_ExcellerisEOrderType");
		eformValue.setVarValue("ON");
		eformValues.add(eformValue);

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		final ProvincialBusinessLogic provincial = new ProvincialFactory(valueMap).getProvincialBusinessLogic();
		PatientConverter.toFhirObject(provincial);
	}

	@Test(expected = ServiceException.class)
	public void TestToFhirObject_missingPhone() {
		List<EFormValue> eformValues = new ArrayList<>();

		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_MIDDLE_NAME, "C."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Doe"));
		eformValues.add(EFormValueHelper.createEFormValue("EO_WSIB", "WSIB"));
		eformValues.add(
				EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER, "1234567890"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "1991/07/11"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "22 Main St."));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "BC"));
		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V9D0S3"));
//		eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "1-722-721-1113"));

		EFormValue eformValue = new EFormValue();
		eformValue.setVarName("M_ExcellerisEOrderType");
		eformValue.setVarValue("ON");
		eformValues.add(eformValue);

		Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

		final ProvincialBusinessLogic provincial = new ProvincialFactory(valueMap).getProvincialBusinessLogic();
		PatientConverter.toFhirObject(provincial);
	}

}