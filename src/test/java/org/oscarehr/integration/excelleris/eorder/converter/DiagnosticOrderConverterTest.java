package org.oscarehr.integration.excelleris.eorder.converter;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.oscarehr.common.dao.OrderLabTestCodeCacheDao;
import org.oscarehr.common.model.OrderLabTestCodeCache;
import org.oscarehr.util.SpringUtils;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class DiagnosticOrderConverterTest {

  @Mock
  private OrderLabTestCodeCacheDao orderLabTestCodeCacheDao;

  private AutoCloseable closeable;
  private MockedStatic<SpringUtils> springUtils;

  @BeforeEach
  public void initialize() throws Exception {
    closeable = MockitoAnnotations.openMocks(this);

    springUtils = mockStatic(SpringUtils.class);
    springUtils.when(() -> SpringUtils.getBean(eq(OrderLabTestCodeCacheDao.class)))
        .thenReturn(orderLabTestCodeCacheDao);
    when(orderLabTestCodeCacheDao.findTestCodeByProvince(Mockito.anyString(), Mockito.anyString()))
        .thenReturn(getTestCodeList());
    when(orderLabTestCodeCacheDao.findTestCodeByProvinceNull(Mockito.anyString()))
        .thenReturn(getTestCodeList());
  }

  @AfterEach
  public void closeService() throws Exception {
    closeable.close();
    springUtils.close();
  }

  @Test
  public void test_findAllLabTest() throws NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    Class<DiagnosticOrderConverter> clazz = DiagnosticOrderConverter.class;
    Method method = clazz.getDeclaredMethod("findAllLabTest", List.class);
    method.setAccessible(true);
    Properties properties = (Properties) method.invoke(null, getTestCodeList());

    Assertions.assertNotNull(properties);
    Assertions.assertEquals("TR10228-5B", properties.getProperty("T_hba1c"));
  }

  @Test
  public void test_getTestCode() throws NoSuchMethodException, SecurityException,
      IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    Class<DiagnosticOrderConverter> clazz = DiagnosticOrderConverter.class;
    Method method = clazz.getDeclaredMethod("getTestCode", String.class, String.class);
    method.setAccessible(true);
    String testCode = (String) method.invoke(null, "T_hba1c", "ON");

    Assertions.assertNotNull(testCode);
    Assertions.assertEquals("TR10228-5B", testCode);
  }

  private List<OrderLabTestCodeCache> getTestCodeList() {
    List<OrderLabTestCodeCache> testCodeList = new ArrayList<OrderLabTestCodeCache>();
    testCodeList.add(getOrderLabTestCodeCache("T_hba1c", "TR10228-5B"));
    return testCodeList;
  }

  private OrderLabTestCodeCache getOrderLabTestCodeCache(String keyName, String labTestCode) {
    OrderLabTestCodeCache orderLabTestCodeCache = new OrderLabTestCodeCache();
    orderLabTestCodeCache.setKeyName(keyName);
    orderLabTestCodeCache.setLabTestCode(labTestCode);
    return orderLabTestCodeCache;
  }
}
