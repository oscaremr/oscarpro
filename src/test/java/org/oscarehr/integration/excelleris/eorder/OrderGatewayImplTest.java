package org.oscarehr.integration.excelleris.eorder;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.oscarehr.common.dao.EOrderDao;
import org.oscarehr.common.dao.OrderLabTestCodeCacheDao;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.ExcellerisEorder;
import org.oscarehr.common.model.OrderLabTestCodeCache;
import org.oscarehr.integration.excelleris.eorder.converter.PatientPropertySet;
import org.oscarehr.integration.excelleris.eorder.converter.PractitionerPropertySet;

@Ignore // OSCAR-4817 Ignored for lack of credentials.
public class OrderGatewayImplTest {
	
	private OrderGatewayImpl orderGatewayImpl;
	private OrderService orderService;
	private ExcellerisEOrderImpl excellerisEOrderImpl;
	
	@Mock 
	private OrderLabTestCodeCacheDao orderLabTestCodeDaoMock;
	
	@Before
    public void setUp() {

		final Properties properties = new Properties();
		properties.setProperty(PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_ENABLE, PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_ENABLE);
		properties.setProperty(PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_HOST,   PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_HOST);
		properties.setProperty(PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_PORT,   PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_HAPI_PROXY_PORT);
		properties.setProperty(PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_BASE_URL,          PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_BASE_URL);

		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_CLIENT_SSL_ENABLE,            "False");
		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_LOGIN_URL_PARAM_NAME,         PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_LOGIN_URL );
		properties.setProperty(ConfigureConstants.EXCELLERIS_USER_V2_PROVINCE_PARAM_NAME,            PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_USER_PROVINCE);
		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_V2_INSTANCE_AFFINITY_DISABLE,    PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_INSTANCE_AFFINITY_DISABLE);
		properties.setProperty(ConfigureConstants.EXCELLERIS_USER_V2_PARAM_NAME,                     "ecambian3"); // Previous user had no available practitioners
		properties.setProperty(ConfigureConstants.EXCELLERIS_USER_V2_PASSWORD_PARAM_NAME,            "4400Dominion");
		properties.setProperty(ConfigureConstants.EXCELLERIS_USER_V2_PROVINCE_PARAM_NAME,            PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_V2_USER_PROVINCE);

		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_KEYSTORE_FILE_PARAM_NAME,    PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_KEYSTORE_FILE);
		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_TRUSTSTORE_FILE_PARAM_NAME,    PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_FILE);

		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_KEYSTORE_PASSWORD_PARAM_NAME,    PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_KEYSTORE_PASSWORD);
		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_TRUSTSTORE_PASSWORD_PARAM_NAME,    PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_TRUSTSTORE_PASSWORD);
		properties.setProperty(ConfigureConstants.EXCELLERIS_EORDER_API_VERSION_PARAM_NAME,    PropertyDefaultConstants.DEFAULT_EXCELLERIS_EORDER_API_VERSION);

		this.excellerisEOrderImpl = new ExcellerisEOrderImpl(properties);

		this.orderService = new OrderServiceImpl(new MockEOrderDao(), excellerisEOrderImpl, new MockOrderLabTestCodeCacheDao());
		this.orderGatewayImpl = new OrderGatewayImpl(excellerisEOrderImpl, orderService);
    }
	
	@Test
	public void testCreateOrder() {

		EFormData eformData = new EFormData();
		eformData.setId(1);
		List<EFormValue> eformValues = new ArrayList<>();
		
		// Patient
		// first name
		EFormValue eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_FIRST_NAME);
		eformValue.setVarValue("John");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_LAST_NAME);
		eformValue.setVarValue("Doe");
		// date of birth
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_DOB);
		eformValue.setVarValue("2000-01-01");
		// address
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_ADDRESS_LINE);
		eformValue.setVarValue("2000-01-01");
		// city
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_ADDRESS_CITY);
		eformValue.setVarValue("Surrey");
		// province
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_ADDRESS_PROVINCE);
		eformValue.setVarValue("BC");
		// postal code
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE);
		eformValue.setVarValue("V1V 1C1");
		// phone
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PatientPropertySet.PATIENT_PHONE);
		eformValue.setVarValue("604-222-2222");
		
		// Ordering provider
		// first name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_FIRST_NAME);
		eformValue.setVarValue("Jane");
		// last name
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.PRACTITIONER_LAST_NAME);
		eformValue.setVarValue("Doe");
		// execelleris id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_EXCELLERIS_ID);
		eformValue.setVarValue("WYM22475A");
		// lifelabs id
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName(PractitionerPropertySet.ORDERING_PROVIDER_LIFELAB_ID);
		eformValue.setVarValue("DAV77092A");

		// Tests
		eformValue = new EFormValue();
		eformValues.add(eformValue);
		eformValue.setVarName("T_hepatitis_a");
		eformValue.setVarValue("T_hepatitis_a");
		
		HttpServletRequest mockReq = mock(HttpServletRequest.class);
		HttpSession mockSession = mock(HttpSession.class);
		when(mockReq.getSession()).thenReturn(mockSession);
		
		AuthenticationTokenAndResource result = orderGatewayImpl.createOrder(mockReq, eformData, eformValues);

		assertNotNull("should not be null", result);	
	}

	public class MockOrderLabTestCodeCacheDao extends OrderLabTestCodeCacheDao {

		public MockOrderLabTestCodeCacheDao() {
		}

		public OrderLabTestCodeCache findByTestCode(String testCode) {
			OrderLabTestCodeCache value = new OrderLabTestCodeCache();
			value.setId(24737);
			value.setSearchable(true);
			value.setTestOnForm(false);
			value.setRequireAppointment(false);
			value.setPatientPay(false);
			value.setTestName("Hepatitis A Immunity");
			value.setKeyName("T_Hepatitis_A_Immunity");
			value.setLabTestCode("TR11453-8V");
			return value;
		}

		public List<OrderLabTestCodeCache>findAll(Integer offset, Integer limit) {
			return Collections.emptyList();
		}
	}

	public class MockEOrderDao extends EOrderDao {

		public MockEOrderDao() {
		}

		public ExcellerisEorder findByExtEFormDataId(Integer fdid) {
			return new ExcellerisEorder();
		}
	}
}
