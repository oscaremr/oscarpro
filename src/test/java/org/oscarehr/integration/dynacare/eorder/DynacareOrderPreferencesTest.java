/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.dynacare.eorder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import lombok.val;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.SpringUtils;

@ExtendWith(MockitoExtension.class)
public class DynacareOrderPreferencesTest {

  private static SystemPreferencesDao systemPreferencesDao;

  private static MockedStatic<SpringUtils> utilsMock;

  @BeforeAll
  public static void setup() {
    utilsMock = mockStatic(SpringUtils.class);
    systemPreferencesDao = mock(SystemPreferencesDao.class);
    when(SpringUtils.getBean(SystemPreferencesDao.class))
        .thenReturn(systemPreferencesDao);
  }

  @Test
  public void testGetOrderUrl() {
    createSystemPreferencesMock(ConfigureConstants.DYNACARE_ORDER_URL, "https://eorder.dynacare.ca");
    val result = DynacareOrderPreferences.getOrderUrl();
    assertEquals("https://eorder.dynacare.ca/", result);
  }

  @Test
  public void testGetApiKey() {
    createSystemPreferencesMock(ConfigureConstants.DYNACARE_API_KEY, "1234567890");
    val result = DynacareOrderPreferences.getApiKey();
    assertNotNull(result);
    assertEquals("1234567890", result);
  }

  @Test
  public void testGetInstanceId() {
    createSystemPreferencesMock(ConfigureConstants.DYNACARE_INSTANCE_ID, "1234567890");
    val result = DynacareOrderPreferences.getInstanceId();
    assertNotNull(result);
    assertEquals("1234567890", result);
  }

  @Test
  public void testGetInstanceId_isBlank() {
    createSystemPreferencesMock(ConfigureConstants.DYNACARE_INSTANCE_ID, "");
    val result = DynacareOrderPreferences.getInstanceId();
    assertNotNull(result);
    assertEquals("OSCAR", result);
  }

  @Test
  public void testGetToEmail() {
    createSystemPreferencesMock(ConfigureConstants.DYNACARE_CLINIC_EMAIL, "test@email.com");
    val result = DynacareOrderPreferences.getToEmail();
    assertNotNull(result);
    assertEquals("test@email.com", result);
  }

  @Test
  public void testGetToEmail_isBlank() {
    createSystemPreferencesMock(ConfigureConstants.DYNACARE_CLINIC_EMAIL, "");
    val result = DynacareOrderPreferences.getToEmail();
    assertNull(result);
  }

  @AfterAll
  public static void close() {
    utilsMock.close();
  }

  private SystemPreferences createSystemPreference(String name, String value) {
    val systemPreference = new SystemPreferences();
    systemPreference.setName(name);
    systemPreference.setValue(value);
    return systemPreference;
  }

  private void createSystemPreferencesMock(String name, String value) {
    val orderUrlPreference = createSystemPreference(name, value);
    when(systemPreferencesDao.findPreferenceByName(name))
        .thenReturn(orderUrlPreference);
  }
}
