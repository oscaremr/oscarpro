/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.dynacare.eorder;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import lombok.val;
import org.junit.Test;

public class DynacareOrderEncryptionUtilsTest {

  private static final String PLAIN_TEXT = "Hello, World!";

  @Test
  public void testEncryptAndDecrypt() {
    // Encrypt the plaintext
    val encrypted = DynacareOrderEncryptionUtils.encrypt(PLAIN_TEXT);
    assertNotNull(encrypted);
    // Decrypt the encrypted text
    val decrypted = DynacareOrderEncryptionUtils.decrypt(encrypted);
    assertNotNull(decrypted);
    assertEquals(PLAIN_TEXT, decrypted);
  }

  @Test
  public void testDecryptWithInvalidText() {
    // Attempt to decrypt invalid text
    val decrypted = DynacareOrderEncryptionUtils.decrypt("InvalidText");
    assertNull(decrypted);
  }

  @Test
  public void testEncryptAndDecryptWithNull() {
    // Encrypt and decrypt null values
    val encrypted = DynacareOrderEncryptionUtils.encrypt(null);
    assertNull(encrypted);
    val decrypted = DynacareOrderEncryptionUtils.decrypt(null);
    assertNull(decrypted);
  }
}
