/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.dynacare.eorder.converter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import com.dynacare.api.Comment;
import com.dynacare.api.OrderStatusEnum;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.val;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.oscarehr.common.dao.DynacareEorderLabTestCodeDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.DynacareEorderLabTestCode;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.util.SpringUtils;

public class OrderConverterTest {

  private static final String DYNACARE_TO = "dynporportal@dynacare.ca";

  private OrderConverter converter;

  private MockedStatic<SpringUtils> utilsMock;

  @Before
  public void setUp() {
    this.converter = new OrderConverter(new MockDynacareLabTestDao());
    createMocksForDynacareToEmail();
  }

  @Test
  public void testToDynacareOrderObject() {
    List<EFormValue> eformValues = new ArrayList<>();
    setUpLabTestData(eformValues);
    setUpPatientTestData(eformValues);
    setUpPractionerTestData(eformValues);
    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    val order = converter.toDynacareOrderObject(valueMap, 22);

    assertEquals("OSCAR-22", order.getSystemId());
    assertEquals(OrderStatusEnum.NOT_PROCESSED, order.getStatusId());

    //		parties are checked in PartyConverterTest
    assertEquals(2, order.getParties().size());
    assertEquals(1, order.getTests().size());
    org.oscarehr.integration.dynacare.eorder.converter.Test cbcTest = new org.oscarehr.integration.dynacare.eorder.converter.Test();
    cbcTest.setSystemId("LOINC");
    cbcTest.setDescription("Complete Blood Count");
    cbcTest.setCode("TR10477-8");
    org.oscarehr.integration.dynacare.eorder.converter.Test ddd = order.getTests().get(0);
    assertEquals(cbcTest, ddd);
    List<Comment> expectedComments = new ArrayList<Comment>();
    Comment comment1 = new Comment();
    comment1.setCommentCode("External.FreeText");
    comment1.setCommentText("The patient is pregnant");
    expectedComments.add(comment1);
    Comment comment2 = new Comment();
    comment2.setCommentCode("External.FreeText");
    comment2.setCommentText("Specimen collection time : 2020-02-11T01:13");
    expectedComments.add(comment2);
    Comment comment3 = new Comment();
    comment3.setCommentCode("External.FreeText");
    comment3.setCommentText("additional info comment");
    expectedComments.add(comment3);
    Comment comment4 = new Comment();
    comment4.setCommentCode("External.FreeText");
    comment4.setCommentText("Payment Type : Patient");
    expectedComments.add(comment4);
    List<Comment> comments = order.getComment();
    assertEquals(expectedComments, comments);
  }

  @Test(expected = ServiceException.class)
  public void testToDynacareOrderObject_noProviderInfo() {
    List<EFormValue> eformValues = new ArrayList<>();
    setUpLabTestData(eformValues);
    setUpPatientTestData(eformValues);
    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    val order = converter.toDynacareOrderObject(valueMap, 22);
  }

  @Test(expected = ServiceException.class)
  public void testToDynacareOrderObject_noPatientInfo() {
    List<EFormValue> eformValues = new ArrayList<>();
    setUpLabTestData(eformValues);
    setUpPractionerTestData(eformValues);
    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);
    val order = converter.toDynacareOrderObject(valueMap, 22);
  }

  @Test(expected = ServiceException.class)
  public void testToDynacareOrderObject_noLabTest() {
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(EFormValueHelper.createEFormValue("EO_pregnant", "true"));
    eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_date", "2020/02/11"));
    eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_time", "01:13"));
    eformValues.add(EFormValueHelper.createEFormValue("EO_Additional_Clinic_Info", "pos"));
    eformValues.add(EFormValueHelper.createEFormValue("EO_timezone_offset", "+01:00"));

    setUpPatientTestData(eformValues);
    setUpPractionerTestData(eformValues);
    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    val order = converter.toDynacareOrderObject(valueMap, 22);
  }

  @After
  public void close() {
    this.utilsMock.close();
  }

  private void setUpLabTestData(List<EFormValue> eformValues) {
    eformValues.add(EFormValueHelper.createEFormValue("EO_pregnant", "true"));
    eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_date", "2020/02/11"));
    eformValues.add(EFormValueHelper.createEFormValue("EO_specimen_col_time", "01:13"));
    eformValues.add(
        EFormValueHelper.createEFormValue("EO_Additional_Clinic_Info", "additional info comment"));
    eformValues.add(EFormValueHelper.createEFormValue("EO_timezone_offset", "+01:00"));
    eformValues.add(EFormValueHelper.createEFormValue("T_cbc", "CBC"));

  }
  private void setUpPractionerTestData(List<EFormValue> eformValues) {
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVIDER_NO, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_ADDRESS, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_POSTAL, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_PHONE, "713-711-2122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_OHIP_NO, "1122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_CPSO, "1111"));
  }

  private void setUpPatientTestData(List<EFormValue> eformValues) {
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID, "21"));
    eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Doe"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "Jane"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "113 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "777-212-7713"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "2011/07/22"));
    eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "F"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_EMAIL, "jane@cambian.com"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER, "123456789"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER_VERSION, "ON"));

  }

  private void createMocksForDynacareToEmail() {
    this.utilsMock = mockStatic(SpringUtils.class);
    val dynacareTo = new SystemPreferences("dynacare.to_email", DYNACARE_TO);
    val systemPreferencesDao = mock(SystemPreferencesDao.class);
    when(SpringUtils.getBean(SystemPreferencesDao.class)).thenReturn(systemPreferencesDao);
    when(systemPreferencesDao.findPreferenceByName("dynacare.to_email")).thenReturn(dynacareTo);
  }

  public class MockDynacareLabTestDao extends DynacareEorderLabTestCodeDao {

    public MockDynacareLabTestDao() {}

    @Override
    public DynacareEorderLabTestCode findByTestCode(String testCode) {

      if (testCode.equals("TR10477-8")) {
        DynacareEorderLabTestCode cbcTest = new DynacareEorderLabTestCode();
        cbcTest.setSystemId("LIONIC");
        cbcTest.setLongName("Complete Blood Count");
        cbcTest.setShortName("CBC");
        cbcTest.setLabTestCode("TR10477-8");
        return cbcTest;
      }

      return null;
    }
    @Override
    public List<DynacareEorderLabTestCode> findByAll() {
    	 List<DynacareEorderLabTestCode> cbcTestList = new ArrayList<>();
    	 DynacareEorderLabTestCode cbcTest = new DynacareEorderLabTestCode();
         cbcTest.setSystemId("LIONIC");
         cbcTest.setLongName("Complete Blood Count");
         cbcTest.setShortName("CBC");
         cbcTest.setLabTestCode("TR10477-8");
         cbcTestList.add(cbcTest);
         return cbcTestList;
    }
  }
}
