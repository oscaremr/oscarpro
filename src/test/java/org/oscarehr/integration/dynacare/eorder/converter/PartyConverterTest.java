/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.dynacare.eorder.converter;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import lombok.val;
import org.junit.Test;
import org.oscarehr.common.model.EFormValue;
import org.oscarehr.integration.eOrder.common.EFormValueHelper;
import org.oscarehr.integration.eOrder.common.ServiceException;
import com.dynacare.api.Address;
import com.dynacare.api.GenderEnum;
import com.dynacare.api.LanguageEnum;
import com.dynacare.api.PartyRoleEnum;
import com.dynacare.api.PartyTypeEnum;
import com.dynacare.api.PhoneNumber;
import com.dynacare.api.PhoneTypeEnum;

public class PartyConverterTest {

  @Test
  public void toDynacarePractitionerPartyObject() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVIDER_NO, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_ADDRESS, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_POSTAL, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_PHONE, "713-711-2122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_OHIP_NO, "1122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_CPSO, "1111"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    Party practitioner = PartyConverter.toDynacareOrderingProvider(valueMap);

    assertEquals("John", practitioner.getFirstName());
    assertEquals("Smith", practitioner.getLastName());
    assertEquals(1, practitioner.getAddresses().size());
    assertEquals(
        new Address("", "123 main st.", "", "Vancouver", "BC", "V84H8U", ""),
        practitioner.getAddresses().get(0));

    assertEquals(PartyRoleEnum.ORDERING, practitioner.getRoleId());
    assertEquals(PartyTypeEnum.PHYSICIAN, practitioner.getTypeId());

    assertEquals(false, practitioner.isEmailAuthorized());
    assertEquals(1, practitioner.getPhoneNumbers().size());

    assertEquals(
        new PhoneNumber("713-711-2122", PhoneTypeEnum.WORK), practitioner.getPhoneNumbers().get(0));
    assertEquals("John", practitioner.getFirstName());
  }

  @Test(expected = ServiceException.class)
  public void toDynacarePractitionerPartyObject_missingProviderNo() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_ADDRESS, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_POSTAL, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_PHONE, "713-711-2122"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toDynacareOrderingProvider(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void toDynacarePractitionerPartyObject_missingFirstName() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVIDER_NO, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_ADDRESS, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_POSTAL, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_PHONE, "713-711-2122"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toDynacareOrderingProvider(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void toDynacarePractitionerPartyObject_missingLastName() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVIDER_NO, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_ADDRESS, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_POSTAL, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_PHONE, "713-711-2122"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toDynacareOrderingProvider(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void toDynacarePractitionerPartyObject_missingAddress() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVIDER_NO, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_POSTAL, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_PHONE, "713-711-2122"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toDynacareOrderingProvider(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void toDynacarePractitionerPartyObject_missingCity() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVIDER_NO, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_ADDRESS, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_POSTAL, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_PHONE, "713-711-2122"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toDynacareOrderingProvider(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void toDynacarePractitionerPartyObject_missingProviderPostalCode() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVIDER_NO, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_ADDRESS, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_PHONE, "713-711-2122"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toDynacareOrderingProvider(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void toDynacarePractitionerPartyObject_missingProviderProvince() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVIDER_NO, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_ADDRESS, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_POSTAL, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_PHONE, "713-711-2122"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toDynacareOrderingProvider(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void toDynacarePractitionerPartyObject_missingProviderPhone() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVIDER_NO, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PractitionerPropertySet.PRACTITIONER_ADDRESS, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_POSTAL, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_OHIP_NO, "1122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PractitionerPropertySet.PRACTITIONER_CPSO, "1111"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toDynacareOrderingProvider(valueMap);
  }

  @Test
  public void testToPatientParty() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "713-711-2122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "2011/07/22"));
    eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "M"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_EMAIL, "john@cambian.com"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER, "123456789"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_HEALTH_INSURANCE_NUMBER_VERSION, "ON"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    Party patient = PartyConverter.toPatientParty(valueMap);

    assertEquals("John", patient.getFirstName());
    assertEquals("Smith", patient.getLastName());
    assertEquals(1, patient.getAddresses().size());
    assertEquals(
        new Address("", "123 main st.", "", "Vancouver", "BC", "V84H8U", ""),
        patient.getAddresses().get(0));

    assertEquals(PartyRoleEnum.PATIENT, patient.getRoleId());
    assertEquals(PartyTypeEnum.PATIENT, patient.getTypeId());
    assertEquals(LocalDateTime.of(2011, 07, 22, 0, 0), patient.getDateOfBirth());
    assertEquals(GenderEnum.MALE.getDisplay(),patient.getGender());
    assertEquals("john@cambian.com", patient.getEmail());
    assertEquals(false, patient.isEmailAuthorized());
    assertEquals(LanguageEnum.ENGLISH_CANADA.getDisplayName(), patient.getLanguage());
    assertEquals(1, patient.getPhoneNumbers().size());
    assertEquals(
        new PhoneNumber("713-711-2122", PhoneTypeEnum.HOME), patient.getPhoneNumbers().get(0));
    assertEquals("John", patient.getFirstName());
  }

  @Test(expected = ServiceException.class)
  public void testToPatientParty_missingId() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "713-711-2122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "2011/07/22"));
    eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "M"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_EMAIL, "john@cambian.com"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toPatientParty(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void testToPatientParty_missingLastName() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "713-711-2122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "2011/07/22"));
    eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "M"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_EMAIL, "john@cambian.com"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toPatientParty(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void testToPatientParty_missingFirstName() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "713-711-2122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "2011/07/22"));
    eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "M"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_EMAIL, "john@cambian.com"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toPatientParty(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void testToPatientParty_missingAddress() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "713-711-2122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "2011/07/22"));
    eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "M"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_EMAIL, "john@cambian.com"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toPatientParty(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void testToPatientParty_missingCity() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "713-711-2122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "2011/07/22"));
    eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "M"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_EMAIL, "john@cambian.com"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toPatientParty(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void testToPatientParty_missingPostal() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "713-711-2122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "2011/07/22"));
    eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "M"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_EMAIL, "john@cambian.com"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toPatientParty(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void testToPatientParty_missingProvince() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "713-711-2122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "2011/07/22"));
    eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "M"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_EMAIL, "john@cambian.com"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toPatientParty(valueMap);
  }

  @Test
  public void testToPatientParty_missingPhone() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "2011/07/22"));
    eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "M"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_EMAIL, "john@cambian.com"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    val party = PartyConverter.toPatientParty(valueMap);
    assertEquals(Collections.emptyList(), party.getPhoneNumbers());
  }

  @Test(expected = ServiceException.class)
  public void testToPatientParty_missingDOB() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "713-711-2122"));
    eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "M"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_EMAIL, "john@cambian.com"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toPatientParty(valueMap);
  }

  @Test(expected = ServiceException.class)
  public void testToPatientParty_missingGender() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "713-711-2122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "2011/07/22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_EMAIL, "john@cambian.com"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    PartyConverter.toPatientParty(valueMap);
  }

  @Test
  public void testToPatientParty_missingEmail() {
    // first name
    List<EFormValue> eformValues = new ArrayList<>();
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DEMOGRAPHIC_ID, "22"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_LAST_NAME, "Smith"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_FIRST_NAME, "John"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_LINE, "123 main st."));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_CITY, "Vancouver"));
    eformValues.add(
        EFormValueHelper.createEFormValue(
            PatientPropertySet.PATIENT_ADDRESS_POSTAL_CODE, "V84H8U"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_ADDRESS_PROVINCE, "bc"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_PHONE, "713-711-2122"));
    eformValues.add(
        EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_DOB, "2011/07/22"));
    eformValues.add(EFormValueHelper.createEFormValue(PatientPropertySet.PATIENT_GENDER, "M"));

    Map<String, EFormValue> valueMap = EFormValueHelper.getValueMap(eformValues);

    val party = PartyConverter.toPatientParty(valueMap);
    assertEquals("", party.getEmail());
  }
}
