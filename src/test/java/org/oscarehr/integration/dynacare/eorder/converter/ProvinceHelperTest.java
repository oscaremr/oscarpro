/*
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.dynacare.eorder.converter;

import static org.junit.Assert.assertEquals;

import lombok.val;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.oscarehr.integration.eOrder.common.ServiceException;
import org.oscarehr.util.MiscUtils;

public class ProvinceHelperTest {

  private static final Logger LOG = MiscUtils.getLogger();

  @Test(expected = ServiceException.class)
  public void testGetProvinceAbrevNull() {
    ProvinceHelper.getProvinceAbrev(null, null);
  }

  @Test(expected = ServiceException.class)
  public void testGetProvinceAbrevEmpty() {
    ProvinceHelper.getProvinceAbrev("", null);
  }

  @Test
  public void testGetProvinceAbrev_bc() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("British Columbia", null);
    String abrev2 = ProvinceHelper.getProvinceAbrev("bc", null);
    String abrev3 = ProvinceHelper.getProvinceAbrev("B.C.", null);

    String expectedAbrev = "BC";
    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_ab() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("alberta", null);
    String abrev2 = ProvinceHelper.getProvinceAbrev("AB", null);

    String expectedAbrev = "AB";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
  }

  @Test
  public void testGetProvinceAbrev_sk() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("saskatchewan", null);
    String abrev2 = ProvinceHelper.getProvinceAbrev("SAsk", null);
    String abrev3 = ProvinceHelper.getProvinceAbrev("SK", null);
    String expectedAbrev = "SK";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_mb() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("Manitoba", null);
    String abrev2 = ProvinceHelper.getProvinceAbrev("MAN", null);
    String abrev3 = ProvinceHelper.getProvinceAbrev("MB", null);
    String expectedAbrev = "MB";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_on() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("Ontario", null);
    String abrev2 = ProvinceHelper.getProvinceAbrev("ont", null);
    String abrev3 = ProvinceHelper.getProvinceAbrev("ON", null);
    String expectedAbrev = "ON";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_qe() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("QUEBEC", null);
    String abrev2 = ProvinceHelper.getProvinceAbrev("que", null);
    String abrev3 = ProvinceHelper.getProvinceAbrev("Qc", null);
    String expectedAbrev = "QC";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_nb() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("NEW BRUNSWICK", null);
    String abrev2 = ProvinceHelper.getProvinceAbrev("n.b.", null);
    String abrev3 = ProvinceHelper.getProvinceAbrev("Nb", null);
    String expectedAbrev = "NB";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_ns() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("Nova Scotia", null);
    String abrev2 = ProvinceHelper.getProvinceAbrev("N.S.", null);
    String abrev3 = ProvinceHelper.getProvinceAbrev("ns", null);
    String expectedAbrev = "NS";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_pe() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("prINCE Edward Island", null);
    String abrev2 = ProvinceHelper.getProvinceAbrev("P.E.I.", null);
    String abrev3 = ProvinceHelper.getProvinceAbrev("PE", null);
    String expectedAbrev = "PE";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_nf() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("NEWFOUNDLAND AND LABRADOR", null);
    String abrev2 = ProvinceHelper.getProvinceAbrev("N.L.", null);
    String abrev3 = ProvinceHelper.getProvinceAbrev("nl", null);
    String expectedAbrev = "NL";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_yt() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("yukon", null);
    String abrev2 = ProvinceHelper.getProvinceAbrev("Y.t.", null);
    String abrev3 = ProvinceHelper.getProvinceAbrev("yt", null);
    String expectedAbrev = "YT";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_nt() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("Northwest Territories", null);
    String abrev2 = ProvinceHelper.getProvinceAbrev("N.W.T.", null);
    String abrev3 = ProvinceHelper.getProvinceAbrev("nt", null);
    String expectedAbrev = "NT";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_nu() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("Nunavut", null);
    String abrev2 = ProvinceHelper.getProvinceAbrev("NVT", null);
    String abrev3 = ProvinceHelper.getProvinceAbrev("nu", null);
    String expectedAbrev = "NU";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_postalA() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("null", "A");
    String abrev2 = ProvinceHelper.getProvinceAbrev("", "a");
    String abrev3 = ProvinceHelper.getProvinceAbrev("abc", "a");
    String expectedAbrev = "NL";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_postalB() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("null", "B");
    String abrev2 = ProvinceHelper.getProvinceAbrev("", "b");
    String abrev3 = ProvinceHelper.getProvinceAbrev("abc", "b");
    String expectedAbrev = "NS";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_postalC() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("null", "c");
    String abrev2 = ProvinceHelper.getProvinceAbrev("", "C");
    String abrev3 = ProvinceHelper.getProvinceAbrev("abc", "c");
    String expectedAbrev = "PE";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_postalE() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("null", "E");
    String abrev2 = ProvinceHelper.getProvinceAbrev("", "e");
    String abrev3 = ProvinceHelper.getProvinceAbrev("abc", "e");
    String expectedAbrev = "NB";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_postalGHJ() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("null", "G");
    String abrev2 = ProvinceHelper.getProvinceAbrev("", "h");
    String abrev3 = ProvinceHelper.getProvinceAbrev("abc", "J");
    String expectedAbrev = "QC";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_postalKLMP() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("null", "K");
    String abrev2 = ProvinceHelper.getProvinceAbrev("", "L");
    String abrev3 = ProvinceHelper.getProvinceAbrev("abc", "m");
    String abrev4 = ProvinceHelper.getProvinceAbrev("abc", "p");
    String expectedAbrev = "ON";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
    assertEquals(expectedAbrev, abrev4);
  }

  @Test
  public void testGetProvinceAbrev_postalR() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("null", "R");
    String abrev2 = ProvinceHelper.getProvinceAbrev("", "r");
    String abrev3 = ProvinceHelper.getProvinceAbrev("abc", "r");
    String expectedAbrev = "MB";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_postalS() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("null", "S");
    String abrev2 = ProvinceHelper.getProvinceAbrev("", "s8j89s");
    String abrev3 = ProvinceHelper.getProvinceAbrev("abc", "s9g7l9");
    String expectedAbrev = "SK";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_postalT() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("null", "T0F7F8");
    String abrev2 = ProvinceHelper.getProvinceAbrev("", "t8b9s9");
    String abrev3 = ProvinceHelper.getProvinceAbrev("abc", "t8d9a5");
    String expectedAbrev = "AB";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test
  public void testGetProvinceAbrev_postalV() {
    String abrev1 = ProvinceHelper.getProvinceAbrev("null", "v");
    String abrev2 = ProvinceHelper.getProvinceAbrev("", "V");
    String abrev3 = ProvinceHelper.getProvinceAbrev("abc", "V");
    String expectedAbrev = "BC";

    assertEquals(expectedAbrev, abrev1);
    assertEquals(expectedAbrev, abrev2);
    assertEquals(expectedAbrev, abrev3);
  }

  @Test(expected = ServiceException.class)
  public void testGetProvinceAbrev_postalX() {
    ProvinceHelper.getProvinceAbrev("null", "X0C4W0");
  }

  @Test(expected = ServiceException.class)
  public void testGetProvinceAbrev_postalInvalid() {
    ProvinceHelper.getProvinceAbrev("null", "F0X2R4");
  }

  @Test
  public void testGetProvinceAbrev_startsWithUs() {
    val abbreviation = "US-AZ";
    val postalCode = "123456";
    assertEquals("AZ", ProvinceHelper.getProvinceAbrev(abbreviation, postalCode));
  }
}
