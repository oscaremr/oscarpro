package org.oscarehr.integration.mcedt;

import ca.ontario.health.ebs.EbsFault;
import lombok.val;
import org.junit.Assert;
import org.junit.Test;

public class McedtMessageCreatorTest {

  // Constant Var
  private static final String EHCAU0014_INPUT_MESSAGE =
      "Authentication failed; contact your technical support or software vendor.";
  private static final String EHCAU0014_OUTPUT_MESSAGE =
      "Authentication failed. Please modify your GoSecure password in "
          + "MCEDT and try again or contact support for assistance.";
  private static final String EBS_FAULT_CODE = "EHCAU0014";
  private static final String EBS_FAULT_MESSAGE = "Authentication failed";
  private static final String TEST_ERROR_MESSAGE = "Output Message not as expected";

  // null input
  @Test
  public void exceptionToString_nullInput_returnNull() {
    Assert.assertNull(
        "Null exception should return null", McedtMessageCreator.exceptionToString(null));
  }

  // e.getFaultInfo() = null
  @Test
  public void exceptionToString_faultInfoIsNull_returnNull() {
    Assert.assertNull(
        "Null FaultInfo should return Null",
        McedtMessageCreator.exceptionToString(new ca.ontario.health.edt.Faultexception()));
  }

  // e.getMessage() = null
  @Test
  public void exceptionToString_getMessageIsNull_returnEmpty() {
    val exception = new Exception();
    Assert.assertEquals(
        "Null message should return empty String",
        "",
        McedtMessageCreator.exceptionToString(exception));
  }

  // edt -> EHCAU0014
  @Test
  public void exceptionToString_edtFaultExceptionInput_returnModifiedString() {
    val ebsFault = new EbsFault();
    ebsFault.setMessage(EHCAU0014_INPUT_MESSAGE);
    ebsFault.setCode(EBS_FAULT_CODE);
    val faultException = new ca.ontario.health.edt.Faultexception("", ebsFault);
    Assert.assertEquals(
        TEST_ERROR_MESSAGE,
        EBS_FAULT_CODE + " " + EHCAU0014_OUTPUT_MESSAGE,
        McedtMessageCreator.exceptionToString(faultException));
  }

  // edt -> Original
  @Test
  public void exceptionToString_edtFaultExceptionInput_returnOriginalString() {
    val ebsFault = new EbsFault();
    ebsFault.setMessage(EBS_FAULT_MESSAGE);
    ebsFault.setCode(EBS_FAULT_CODE);
    val faultException = new ca.ontario.health.edt.Faultexception("", ebsFault);
    Assert.assertEquals(
        TEST_ERROR_MESSAGE,
        EBS_FAULT_CODE + " " + EBS_FAULT_MESSAGE,
        McedtMessageCreator.exceptionToString(faultException));
  }

  // hcv -> Original
  @Test
  public void exceptionToString_hcvFaultExceptionInput_returnOriginalString() {
    val ebsFault = new EbsFault();
    ebsFault.setMessage(EBS_FAULT_MESSAGE);
    ebsFault.setCode(EBS_FAULT_CODE);
    val faultException = new ca.ontario.health.hcv.Faultexception("", ebsFault);
    Assert.assertEquals(
        TEST_ERROR_MESSAGE,
        EBS_FAULT_CODE + " " + EBS_FAULT_MESSAGE,
        McedtMessageCreator.exceptionToString(faultException));
  }

  // hcv -> EHCAU0014
  @Test
  public void exceptionToString_hcvFaultExceptionInput_returnModifiedString() {
    val ebsFault = new EbsFault();
    ebsFault.setMessage(EHCAU0014_INPUT_MESSAGE);
    ebsFault.setCode(EBS_FAULT_CODE);
    val faultException = new ca.ontario.health.hcv.Faultexception("", ebsFault);
    Assert.assertEquals(
        TEST_ERROR_MESSAGE,
        EBS_FAULT_CODE + " " + EHCAU0014_OUTPUT_MESSAGE,
        McedtMessageCreator.exceptionToString(faultException));
  }
}