/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.integration.oneid;

import static org.junit.Assert.assertEquals;

import com.google.gson.JsonSyntaxException;
import java.nio.charset.StandardCharsets;
import javax.xml.bind.DatatypeConverter;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.oscarehr.integration.OneIdSession;

/**
 * Unit tests for {@link OneIdSession} class including
 * various scenarios for the getUrlFromToolbar method.
 */
public class OneIdSessionTest {

  private OneIdSession oneIdSession;
  private final String key = "key";
  private final String value = "value";

  @Before
  public void before() {
    val toolbarJson = "{\"" + key + "\": \"" + value + "\"}";
    val jsonByteArray =
        DatatypeConverter.printBase64Binary(toolbarJson.getBytes(StandardCharsets.UTF_8));
    oneIdSession = new OneIdSession();
    oneIdSession.setToolbar(jsonByteArray);
  }

  @Test
  public void testGetUrlFromToolbar() {
    assertEquals(value, oneIdSession.getUrlFromToolbar(key));
  }

  @Test(expected = NullPointerException.class)
  public void testGetUrlFromToolbar_nullToolbar() {
    oneIdSession.setToolbar(null);
    oneIdSession.getUrlFromToolbar("");
  }

  @Test
  public void testGetUrlFromToolbar_unmatchedKey() {
    assertEquals(oneIdSession.getUrlFromToolbar(null), "");
  }

  @Test(expected = JsonSyntaxException.class)
  public void testGetUrlFromToolbar_invalidToolbar() {
    oneIdSession.setToolbar("test");
    oneIdSession.getUrlFromToolbar("");
  }
}
