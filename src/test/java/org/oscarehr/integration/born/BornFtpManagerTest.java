/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.born;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.InputStream;
import java.nio.file.Files;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.util.SpringUtils;
import org.springframework.integration.file.remote.session.Session;
import org.springframework.integration.sftp.session.DefaultSftpSessionFactory;
import oscar.OscarProperties;

@ExtendWith(MockitoExtension.class)
public class BornFtpManagerTest {

  @Test
  public void givenFile_whenUploadONAREnhancedDataToRepository_thenWriteStreamToSession()
      throws Exception {

    try (MockedStatic<OscarProperties> oscarProps = mockStatic(OscarProperties.class)) {
      try (MockedStatic<SpringUtils> utils = mockStatic(SpringUtils.class)) {
        try (MockedStatic<Files> files = mockStatic(Files.class)) {

          DefaultSftpSessionFactory sessionFactory = mock(DefaultSftpSessionFactory.class);
          Session session = mock(Session.class);
          when(sessionFactory.getSession()).thenReturn(session);
          when(session.isOpen()).thenReturn(true);

          OscarProperties properties = mock(OscarProperties.class);
          when(properties.getProperty(eq("born_sftp_remote_dir"), anyString()))
              .thenReturn("remotePath");

          oscarProps.when(OscarProperties::getInstance).thenReturn(properties);
          utils.when(() -> SpringUtils.getBean(eq("ftpClientFactory"))).thenReturn(sessionFactory);
          files.when(() -> Files.newInputStream(any())).thenReturn(mock(InputStream.class));

          // test
          BornFtpManager.uploadONAREnhancedDataToRepository("/path", "tempfile.txt");

          verify(session).write(any(InputStream.class), anyString());
        }
      }
    }
  }
}
