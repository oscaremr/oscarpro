/**
 * Copyright (c) 2024 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.integration.link;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.SearchStyleEnum;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.client.api.IRestfulClientFactory;
import ca.uhn.fhir.rest.gclient.ICreate;
import ca.uhn.fhir.rest.gclient.ICreateTyped;
import ca.uhn.fhir.rest.gclient.ICriterion;
import ca.uhn.fhir.rest.gclient.IQuery;
import ca.uhn.fhir.rest.gclient.IRead;
import ca.uhn.fhir.rest.gclient.IReadExecutable;
import ca.uhn.fhir.rest.gclient.IReadTyped;
import ca.uhn.fhir.rest.gclient.IUntypedQuery;
import health.apps.gateway.LinkData;
import health.apps.gateway.service.AppsHealthGatewayConfigurationService;
import health.apps.gateway.service.FhirContextSupplierForR4;
import health.apps.gateway.service.GatewayDaoImpl;
import health.apps.gateway.service.StringEncryptionService;
import lombok.val;
import org.hl7.fhir.instance.model.api.IBaseBundle;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.Patient;
import org.junit.Before;
import org.junit.Test;

public class GatewayDaoImplTest {

  private AppsHealthGatewayConfigurationService configurationService;
  private StringEncryptionService stringEncryptionService;
  private FhirContext fhirContext;
  private FhirContextSupplierForR4 fhirContextSupplier;
  private GatewayDaoImpl gatewayDao;
  private IRestfulClientFactory clientFactory;
  private IGenericClient client;
  private IUntypedQuery<IBaseBundle> untypedQuery;
  private IQuery patientQuery;
  private IQuery<Bundle> patientBundle;
  private ICreate iCreate;
  private ICreateTyped iCreateTyped;
  private MethodOutcome methodOutcome;
  private OperationOutcome operationOutcome;
  private final Patient patientToPersist = new Patient();


  @Before
  public void setUp() {
    configurationService = mock(AppsHealthGatewayConfigurationService.class);
    stringEncryptionService = mock(StringEncryptionService.class);
    fhirContext = mock(FhirContext.class);
    fhirContextSupplier = mock(FhirContextSupplierForR4.class);
    clientFactory = mock(IRestfulClientFactory.class);
    client = mock(IGenericClient.class);
    untypedQuery = mock(IUntypedQuery.class);
    patientQuery = mock(IQuery.class);
    patientBundle = mock(IQuery.class);
    iCreate = mock(ICreate.class);
    iCreateTyped = mock(ICreateTyped.class);

    gatewayDao = new GatewayDaoImpl(
        configurationService, stringEncryptionService, fhirContextSupplier);

    when(fhirContextSupplier.get()).thenReturn(fhirContext);
    when(fhirContext.getRestfulClientFactory()).thenReturn(clientFactory);
    when(configurationService.getSystemId()).thenReturn("test-system-id");
    when(configurationService.getSystemKey()).thenReturn("test");
    when(configurationService.getUrl()).thenReturn("http://localhost:8080");
    when(stringEncryptionService.decrypt("test")).thenReturn("test-plain-text");
    when(configurationService.getFhirPath()).thenReturn("/system/:system-id:/fhir");
    when(fhirContext.newRestfulGenericClient(
        "http://localhost:8080/system/remote-system-id/fhir")).thenReturn(client);
    when(client.search()).thenReturn(untypedQuery);
    when(untypedQuery.forResource(Patient.class)).thenReturn(patientQuery);
    when(patientQuery.usingStyle(SearchStyleEnum.GET)).thenReturn(patientQuery);
    when(patientQuery.where(any(ICriterion.class))).thenReturn(patientQuery);
    when(patientQuery.returnBundle(Bundle.class)).thenReturn(patientBundle);
    when(patientBundle.execute()).thenReturn(mock(Bundle.class));
    when(client.create()).thenReturn(iCreate);
  }

  @Test
  public void givenIsLinkedAndGatewayEnabled_whenFindAllRemoteByDemographicId_thenReturnsBundle() {
    // Given
    val resource = Patient.class;
    val parent = mockLinkData(true, "test-guid", "remote-system-id");
    when(configurationService.isGatewayEnabled()).thenReturn(true);

    // When
    Bundle result = gatewayDao.findAllRemoteByDemographicId(resource, parent);

    // Then
    assertNotNull(result);
  }

  @Test
  public void givenParentNull_whenFindAllRemoteByDemographicId_thenReturnsNull() {
    // Given
    val resource = Patient.class;
    LinkData parent = null;

    // When
    Bundle result = gatewayDao.findAllRemoteByDemographicId(resource, parent);

    // Then
    assertNull(result);
    verify(configurationService, never()).isGatewayEnabled();
    verify(fhirContext, never()).getRestfulClientFactory();
  }

  @Test
  public void givenRemoteSystemIdNull_whenFindAllRemoteByDemographicId_thenReturnsNull() {
    // Given
    val resource = Patient.class;
    val parent = mockLinkData(true, "test-guid", null);

    // When
    Bundle result = gatewayDao.findAllRemoteByDemographicId(resource, parent);

    // Then
    assertNull(result);
    verify(configurationService, never()).getSystemId();
    verify(fhirContext, never()).getRestfulClientFactory();
  }

  @Test
  public void givenParentNull_whenFindRemoteById_thenReturnsNull() {
    // Given
    val resource = Patient.class;
    LinkData parent = null;
    String identifier = "test-identifier";

    // When
    val result = gatewayDao.findRemoteById(resource, parent, identifier);

    // Then
    assertNull(result);
    verify(configurationService, never()).isGatewayEnabled();
    verify(fhirContext, never()).getRestfulClientFactory();
  }

  @Test
  public void givenParentNotLinked_whenFindRemoteById_thenReturnsNull() {
    // Given
    val resource = Patient.class;
    LinkData parent = mockLinkData(false, "test-guid", "remote-system-id");
    String identifier = "test-identifier";

    // When
    val result = gatewayDao.findRemoteById(resource, parent, identifier);

    // Then
    assertNull(result);
    verify(configurationService, never()).isGatewayEnabled();
    verify(fhirContext, never()).getRestfulClientFactory();
  }

  @Test
  public void givenRemoteSystemIdNull_whenFindRemoteById_thenReturnsNull() {
    // Given
    val resource = Patient.class;
    val parent = mockLinkData(true, "test-guid", null);
    String identifier = "test-identifier";

    // When
    val result = gatewayDao.findRemoteById(resource, parent, identifier);

    // Then
    assertNull(result);
    verify(configurationService, never()).getSystemId();
    verify(fhirContext, never()).getRestfulClientFactory();
  }

  @Test
  public void givenGatewayDisabled_whenFindRemoteById_thenReturnsNull() {
    // Given
    val resource = Patient.class;
    LinkData parent = mockLinkData(true, "test-guid", "remote-system-id");
    String identifier = null;
    when(configurationService.isGatewayEnabled()).thenReturn(false);

    // When
    val result = gatewayDao.findRemoteById(resource, parent, identifier);

    // Then
    assertNull(result);
    verify(configurationService).isGatewayEnabled();
    verify(parent).isLinked();
    verify(parent, never()).getGuid();
    verify(fhirContext, never()).getRestfulClientFactory();
  }

  @Test
  public void givenIdentifierNull_whenFindRemoteById_thenReturnsNull() {
    // Given
    val resource = Patient.class;
    LinkData parent = mockLinkData(true, "test-guid", "remote-system-id");
    String identifier = null;
    when(configurationService.isGatewayEnabled()).thenReturn(true);

    // When
    val result = gatewayDao.findRemoteById(resource, parent, identifier);

    // Then
    assertNull(result);
    verify(configurationService).isGatewayEnabled();
    verify(parent).isLinked();
    verify(parent).getGuid();
    verify(fhirContext, never()).getRestfulClientFactory();
  }

  @Test
  public void givenParentGuidNull_whenFindRemoteById_thenReturnsNull() {
    // Given
    val resource = Patient.class;
    LinkData parent = mockLinkData(true, null, "remote-system-id");
    String identifier = null;
    when(configurationService.isGatewayEnabled()).thenReturn(true);

    // When
    val result = gatewayDao.findRemoteById(resource, parent, identifier);

    // Then
    assertNull(result);
    verify(configurationService).isGatewayEnabled();
    verify(parent).isLinked();
    verify(parent).getGuid();
    verify(fhirContext, never()).getRestfulClientFactory();
  }

  @Test
  public void givenParentLinkedAndLinkEnabled_whenFindRemoteById_thenReturnsRecord() {
    // Given
    val resource = Patient.class;
    LinkData parent = mockLinkData(true, "guid", "remote-system-id");
    String identifier = "test-identifier";
    val read = mock(IRead.class);
    val readTyped = mock(IReadTyped.class);
    val readExecutable = mock(IReadExecutable.class);

    when(configurationService.isGatewayEnabled()).thenReturn(true);
    when(client.read()).thenReturn(read);
    when(read.resource(resource)).thenReturn(readTyped);
    when(readTyped.withId(identifier)).thenReturn(readExecutable);
    when(readExecutable.execute()).thenReturn(mock(Patient.class));

    // When
    val result = gatewayDao.findRemoteById(resource, parent, identifier);

    // Then
    assertNotNull(result);
    verify(configurationService).isGatewayEnabled();
    verify(parent).isLinked();
    verify(parent).getGuid();
    verify(fhirContext).getRestfulClientFactory();
  }

  @Test
  public void givenParentNull_whenSaveRemote_thenReturnsNull() {
    // Given
    val resource = patientToPersist;
    LinkData parent = null;

    // When
    val result = gatewayDao.saveRemote(parent, resource);

    // Then
    assertNull(result);
    verify(configurationService, never()).isGatewayEnabled();
    verify(fhirContext, never()).getRestfulClientFactory();
  }

  @Test
  public void givenParentNotLinked_whenSaveRemote_thenReturnsNull() {
    // Given
    val resource = patientToPersist;
    val parent = mockLinkData(false, "test-guid", "remote-system-id");

    // When
    val result = gatewayDao.saveRemote(parent, resource);

    // Then
    assertNull(result);
    verify(configurationService, never()).isGatewayEnabled();
    verify(fhirContext, never()).getRestfulClientFactory();
  }

  @Test
  public void givenRemoteSystemIdNull_whenSaveRemote_thenReturnsNull() {
    // Given
    val parent = mockLinkData(true, "test-guid", null);

    // When
    val result = gatewayDao.saveRemote(parent, patientToPersist);

    // Then
    assertNull(result);
    verify(configurationService, never()).getSystemId();
    verify(fhirContext, never()).getRestfulClientFactory();
  }

  @Test
  public void givenGatewayDisabled_whenSaveRemote_thenReturnsNull() {
    // Given
    val parent = mockLinkData(true, "test-guid", "remote-system-id");
    when(configurationService.isGatewayEnabled()).thenReturn(false);

    // When
    val result = gatewayDao.saveRemote(parent, patientToPersist);

    // Then
    assertNull(result);
    verify(configurationService).isGatewayEnabled();
    verify(parent).isLinked();
    verify(parent, never()).getGuid();
    verify(fhirContext, never()).getRestfulClientFactory();
  }

  @Test
  public void givenParentGuidNull_whenSaveRemote_thenReturnsNull() {
    // Given
    val parent = mockLinkData(true, null, "remote-system-id");
    when(configurationService.isGatewayEnabled()).thenReturn(true);

    // When
    val result = gatewayDao.saveRemote(parent, patientToPersist);

    // Then
    assertNull(result);
    verify(configurationService).isGatewayEnabled();
    verify(parent).isLinked();
    verify(parent).getGuid();
    verify(fhirContext, never()).getRestfulClientFactory();
  }

  @Test
  public void givenParentLinkedAndLinkEnabled_whenSaveRemote_thenReturnsRecord() {
    // Given
    val outcomeResource = new Patient();
    val parent = mockLinkData(true, "guid", "remote-system-id");

    when(configurationService.isGatewayEnabled()).thenReturn(true);
    setupCreateMock(patientToPersist, true, outcomeResource);

    // When
    val result = gatewayDao.saveRemote(parent, patientToPersist);

    // Then
    assertNotNull(result);
    verify(configurationService).isGatewayEnabled();
    verify(parent).isLinked();
    verify(parent).getGuid();
    verify(fhirContext).getRestfulClientFactory();
    assertEquals(outcomeResource, result);
  }

  @Test
  public void givenCreationIssues_whenSaveRemote_thenReturnsNull() {
    // Given
    val resource = patientToPersist;
    val parent = mockLinkData(true, "guid", "remote-system-id");

    when(configurationService.isGatewayEnabled()).thenReturn(true);
    setupCreateMock(resource, false, null);

    // When
    val result = gatewayDao.saveRemote(parent, resource);

    // Then
    assertNull(result);
    verify(configurationService).isGatewayEnabled();
    verify(parent).isLinked();
    verify(parent).getGuid();
    verify(fhirContext).getRestfulClientFactory();
    verify(methodOutcome, never()).getResource();
  }

  private void setupCreateMock(Patient resource, boolean created, IBaseResource outcomeResource) {
    methodOutcome = mock(MethodOutcome.class);
    operationOutcome = mock(OperationOutcome.class);

    when(client.create()).thenReturn(iCreate);
    when(iCreate.resource(resource)).thenReturn(iCreateTyped);
    when(iCreateTyped.encodedJson()).thenReturn(iCreateTyped);
    when(iCreateTyped.execute()).thenReturn(methodOutcome);
    when(methodOutcome.getCreated()).thenReturn(created);
    when(methodOutcome.getResource()).thenReturn(outcomeResource);
    when(methodOutcome.getOperationOutcome()).thenReturn(operationOutcome);
  }

  private static LinkData mockLinkData(
      final boolean isLinked,
      final String guid,
      final String remoteSystemId
  ) {
    val parent = mock(LinkData.class);
    when(parent.isLinked()).thenReturn(isLinked);
    when(parent.getGuid()).thenReturn(guid);
    when(parent.getRemoteSystemId()).thenReturn(remoteSystemId);
    return parent;
  }
}