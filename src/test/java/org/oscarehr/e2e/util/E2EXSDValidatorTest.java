/**
 * Copyright (c) 2013-2015. Department of Computer Science, University of Victoria. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Computer Science
 * LeadLab
 * University of Victoria
 * Victoria, Canada
 */
package org.oscarehr.e2e.util;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.InputStream;
import java.util.Scanner;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class E2EXSDValidatorTest {
  private static String xmlTestString;

  @BeforeAll
  public static void beforeClass() {
    InputStream stream =
        E2EXSDValidatorTest.class
            .getClassLoader()
            .getResourceAsStream("org/oscarehr/e2e/validatorTest.xml");
    assertNotNull(stream);
    xmlTestString = new Scanner(stream, "UTF-8").useDelimiter("\\Z").next();
    assertNotNull(xmlTestString);
    assertFalse(xmlTestString.isEmpty());
  }

  @Test
  public void instantiationTest() {
    assertThrows(UnsupportedOperationException.class, E2EXSDValidator::new);
  }

  @Test
  public void isWellFormedXMLTest() {
    assertTrue(E2EXSDValidator.isWellFormedXML(xmlTestString));
  }

  @Test
  public void isWellFormedXMLOnNonWellFormedDocumentTest() {
    assertFalse(
        E2EXSDValidator.isWellFormedXML(
            xmlTestString.replace("</ClinicalDocument>", "</clinicalDocument>")));
  }

  @Test
  public void testIsValidXML() {
    assertTrue(E2EXSDValidator.isValidXML(xmlTestString));
  }

  @Test
  public void testIsValidXMLOnNonValidDocument() {
    assertFalse(E2EXSDValidator.isValidXML(xmlTestString.replace("DOCSECT", "DOXSECT")));
  }
}
