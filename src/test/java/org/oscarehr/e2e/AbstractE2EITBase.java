/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.e2e;

import org.oscarehr.common.SqlResourceFileLoaderITBase;
import org.oscarehr.e2e.constant.Constants.Runtime;

public abstract class AbstractE2EITBase extends SqlResourceFileLoaderITBase {

  @Override
  protected String getSqlResourceFile() {
    return "org/oscarehr/e2e/e2e_setup.sql";
  }

  @Override
  protected String[] getTablesToRestore() {
    return Runtime.TABLES;
  }
}
