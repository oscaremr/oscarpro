/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.hospitalReportManager;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import com.sun.xml.messaging.saaj.util.ByteOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import lombok.val;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.MockedStatic;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentDao;
import org.oscarehr.hospitalReportManager.model.HRMDocument;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.springframework.test.util.ReflectionTestUtils;
import oscar.OscarProperties;

public class HRMPDFCreatorTest {

  private HRMPDFCreator hrmPdfCreator;

  private OscarProperties properties;

  private ByteOutputStream outputStream;

  private LoggedInInfo loggedInInfo;

  private MockedStatic<SpringUtils> utilsMock;

  private MockedStatic<OscarProperties> propertiesMock;

  private final String BASE64_STRING = "PHA+Jm5ic3A7PC9wPg0KPHA+PHN0cm9uZz5EYXRlIG9mIEF"
      + "zc2Vzc21lbnQ6PC9zdHJvbmc+IDIwMjAtTWF5LTA3PC9wPg0KPHA+MS0gdGh5bWljIG5ldXJvZW5kb2NyaW"
      + "5lIGNhcmNpbm9tYSB3YXMgb24gRXZlcm9saW11cyB3aGljaCB3YXMgc3RvcHBlZCBkdWUgdG8gZGlzZWFzZ"
      + "SBwcm9ncmVzc2lvbiB3aXRoIGZ1cnRoZXIgY2hlbW90aGVyYXB5IHRvIGJlIHBsYW5uZWQuPC9wPg0KPHA+"
      + "Jm5ic3A7PC9wPg0KPHA+TWVkaWNhdGlvbnM6PC9wPg0KPHA+MS0gZmx1ZHJvY29ydGlzb25lIGFjZXRhdGU"
      + "gMC41bWcgVGFibGV0IE9uY2UgZGFpbHk8L3A+DQo8cD4yLSBsZXZvdGh5cm94aW5lIHNvZGl1bSAxMDAgbW"
      + "NnIE9uY2UgZGFpbHk8L3A+DQo8cD4zLSBoeWRyb2NvcnRpc29uZSAxNSBtZyBpbiB0aGUgbW9ybmluZyBhb"
      + "mQgMTAgbWcgaW4gdGhlIGV2ZW5pbmcuPC9wPg0KPHA+NC0gcmlzZWRyb25hdGUgc29kaXVtIDM1IG1nIE9y"
      + "YWwgb25jZSB3ZWVrbHkuPC9wPg0KPHA+NS0gQW1sb2RpcGluZSAxMCBtZyBvcmFsIGRhaWx5PC9wPg0KPHA"
      + "+Ni0gTWV0Zm9ybWluIDUwMCBtZyBwbyBCSUQ8L3A+DQo8cD43LSBPbWVnYSAzIHRhYnM8L3A+DQo8cD4mbm"
      + "JzcDs8L3A+DQo8cD5BbGxlcmdpZXM6PC9wPg0KPHA+LSBQZW5pY2lsbGluLjwvcD4NCjxwPkZhbWlseSBoa"
      + "XN0b3J5OjwvcD4NCjxwPi0gTm8gZmFtaWx5IGhpc3Rvcnkgb2YgaGVhcnQgZGlzZWFzZSBpbiB0aGUgZmFt"
      + "aWx5PC9wPg0KPHA+Jm5ic3A7PC9wPg0KPHA+SFBJOjwvcD4NCjxwPi0gTm8gYWJkb21pbmFsIHBhaW4sIG5"
      + "hdXNlYSwgdm9taXRpbmcsIGRpYXJyaGVhLCBmZXZlciwgcnVubnkgbm9zZSwgc29yZSB0aHJvYXQsIGR5c3"
      + "VyaWEsIG9yIGFueSBldmlkZW5jZSBvZiBhY3RpdmUgYmxlZWRpbmcuPC9wPg0KPHA+SW52ZXN0aWdhdGlvb"
      + "nM6PC9wPg0KPHA+TGFzdCBJbnZlc3RpZ2F0aW9ucyBpbiBBUFJJTC8yMDIwOjwvcD4NCjxwPi0gQ2E9Mi40"
      + "NCwgcE80PTEuMzE8L3A+DQo8cD4tIE5hPTEzNywgSz0zLjcsIEFHPTExLCBBTFQ9MTYsIEFMUD01OSxCaWx"
      + "pcnViaW4gPSA1LCBDcmVhdGluaW5lID0gNTg8L3A+DQo8cD4tIENCQzogSEI9MTMzLCBXQkM9NS40LCBQbH"
      + "QgPSAyOTkuPC9wPg0KPHA+LSBDVCBjaGVzdDo8L3A+DQo8dWw+DQo8bGk+RGlzZWFzZSBwcm9ncmVzc2lvb"
      + "iBwYXJ0aWN1bGFybHkgb2YgcHJpbWFyeSBsZXNpb24gaW4gdGhlIGFudGVyaW9yIG1lZGlhc3RpbnVtIGFz"
      + "IHdlbGwgYXMgdXBwZXIgbWVkaWFzdGluYWwgbWV0YXN0YXRpYyBseW1waCBub2RlcyBhcyBpbmRpY2F0ZWQ"
      + "uPC9saT4NCjxsaT5SZWxhdGl2ZWx5IHN0YWJsZSBhcHBlYXJhbmNlIG9mIGJvdGggbHVuZ3MuPC9saT4NCj"
      + "xsaT5JbnRlcnZhbCBpbmNyZWFzZSBpbiBzaXplIG9mIGJpbGF0ZXJhbCBwbGV1cmFsIGVmZnVzaW9ucyBhb"
      + "mQgbm93IHdpdGggbW9kZXJhdGUgdG8gbGFyZ2UgcGVyaWNhcmRpYWwgZWZmdXNpb24uIE5vIGRlZmluaXRl"
      + "IGZlYXR1cmVzIG9mIHBlcmljYXJkaWFsIHRhbXBvbmFkZSBhbHRob3VnaCBzdHVkeSB3YXMgbm90IG9wdGl"
      + "taXplZCB0byBhc3Nlc3MgZm9yIHRoaXMgY29uZGl0aW9uLiAuPC9saT4NCjwvdWw+DQo8cD4tIENUIGFiZG"
      + "9tZW46PC9wPg0KPHVsPg0KPGxpPk11bHRpZm9jYWwgb3NzZW91cyBtZXRhc3RhdGljIGRlcG9zaXRzLCBsY"
      + "XJnZWx5IHVuY2hhbmdlZCB2ZXJzdXMgZGVjcmVhc2VkIGNvbnNwaWN1aXR5IHNpbmNlIHByaW9yLiBObyBu"
      + "ZXcgc2l0ZXMgb2YgZGlzZWFzZSByZWFkaWx5IGV2aWRlbnQsIGFsdGhvdWdoIGRlcG9zaXRzIGFyZSB3aWR"
      + "lc3ByZWFkLiBTdGFibGUgcGF0aG9sb2dpYyBmcmFjdHVyZXMuPC9saT4NCjxsaT5QZXJpcGFuY3JlYXRpYy"
      + "Bzb2Z0IHRpc3N1ZSBpcyBsYXJnZWx5IHVuY2hhbmdlZCBzaW5jZSBwcmlvciwgZXF1aXZvY2FsIGZvciBtZ"
      + "XRhc3RhdGljIGRlcG9zaXQuIEEgc3VidGxlIHBhbmNyZWF0aWMgdGFpbCBsZXNpb24vaHlwb2RlbnNpdHkg"
      + "aXMgbm90ZWQsIHRvbyBzbWFsbCB0byBjaGFyYWN0ZXJpemUuIEdpdmVuIHRoZSB0aG9yYWNpYyBmaW5kaW5"
      + "ncywgdGhlc2UgbWF5IGJlIHJlZXZhbHVhdGVkIGF0IG5leHQgaW50ZXJ2YWwgZm9sbG93LXVwIENULjwvbG"
      + "k+DQo8L3VsPg0KPHA+LSBUcmFuc3ZhZ2luYWwgVVM6PC9wPg0KPHVsPg0KPGxpPk5vIHNvbm9ncmFwaGljI"
      + "GV2aWRlbmNlIG9mIG1ldGFzdGF0aWMgdGh5bWljIG5ldXJvZW5kb2NyaW5lIHR1bW9yIGluIHRoZSBwZWx2"
      + "aXMuIE5vIGFibm9ybWFsIGFkbmV4YWwgbWFzc2VzLiBUaGUgcHJldmlvdXNseSB2aXN1YWxpemVkIGJ1bGt"
      + "5IGFkbmV4YSBvbiBDVCwgbGlrZWx5IHJlbGF0ZSB0byBvdmFyaWFuIHBhcmVuY2h5bWEuIEF0dGVudGlvbi"
      + "BvbiB0aGUgZm9sbG93LXVwIHRvIENUIHRvIGVuc3VyZSBzdGFiaWxpdHkuPC9saT4NCjwvdWw+DQo8cD4tI"
      + "EVjaG9jYXJkaW9ncmFtIHRvZGF5IHNob3dlZDo8L3A+DQo8dWw+DQo8bGk+TWlsZCBnbG9iYWwgTFYgc3lz"
      + "dG9saWMgZHlzZnVuY3Rpb24sIExWRUYgNTAlLiBMYXJnZSwgcHJlZG9taW5hbnRseSByaWdodCBzaWRlZCB"
      + "wZXJpY2FyZGlhbCBlZmZ1c2lvbi4gQWx0aG91Z2ggc29tZSBlYXJseSBzaWducyBvZiBpbmNyZWFzZWQgcG"
      + "VyaWNhcmRpYWwgcHJlc3N1cmUgYXJlIHByZXNlbnQgKHRyYW5zaWVudCBSVk9UL1JWIGludmVyc2lvbiksI"
      + "G5vIHBlcnNpc3RlbnQgY2hhbWJlciBjb2xsYXBzZSBvciBvdGhlciBmaW5kaW5ncyBvZiBmcmFuayBlY2hv"
      + "IHRhbXBvbmFkZS4gQ29ycmVsYXRlIGNsaW5pY2FsbHkuIFRoZSB0cmljdXNwaWQgYW5kIHB1bG1vbmljIHZ"
      + "hbHZlcyBzdHJ1Y3R1cmUgYW5kIGZ1bmN0aW9uIHJlbWFpbiBub3JtYWwuIE5vcm1hbCBSVlNQLjwvbGk+DQ"
      + "o8L3VsPg0KPHA+PGJyIC8+PHN0cm9uZz5Db3JyZWN0ZWQgYW5kIENvLVNpZ25lZCBieTo8L3N0cm9uZz4ge"
      + "Hh4eHh4eHh4eHh4eHh4eHh4eDwvcD4NCjxociAvPg0KPHA+PHN0cm9uZz5Db3BpZXMgdG86PC9zdHJvbmc+"
      + "PGJyIC8+U2VudCB2aWEgSFJNIHRvOiB4eHh4eHh4eHh4eHh4eHh4eHg8YnIgLz48YnIgLz48L3A+DQo8aHI"
      + "gLz4NCjxwPjxiciAvPjxzdHJvbmc+VGhpcyByZXBvcnQgaGFzIGJlZW4gc2lnbmVkIHVzaW5nIGVsZWN0cm"
      + "9uaWMgYXV0aGVudGljYXRpb24uPC9zdHJvbmc+PC9wPg==";

  private final String WK_HTML_TO_PDF_PATH = "wkhtmltopdf";

  private final String WK_HTML_TO_PDF_ARGS = "--print-media-type --javascript-delay 1000 "
      + "--debug-javascript --no-stop-slow-scripts";

  @BeforeEach
  public void setup() throws Exception {
    initProperties();
    initHrmDocumentDao();
    hrmPdfCreator = createHrmPdfCreator();
    ReflectionTestUtils.setField(hrmPdfCreator, "hrmReport", initReport());
  }

  @Test
  @Disabled // missing wkhtmltopdf in test environment
  public void test_printHtmlContent() throws IOException {
    hrmPdfCreator.printHtmlContent();
    assertNotNull(outputStream.getBytes());
  }

  @AfterEach
  public void closeMocks() {
    utilsMock.close();
    propertiesMock.close();
  }


  private void initProperties() throws IOException {
    propertiesMock = mockStatic(OscarProperties.class);
    properties = mock(OscarProperties.class);
    val file = Files.createTempFile("test", ".pdf");
    when(OscarProperties.getInstance()).thenReturn(properties);
    when(properties.getProperty(ArgumentMatchers.eq("WKHTMLTOPDF_COMMAND")))
        .thenReturn(WK_HTML_TO_PDF_PATH);
    when(properties.getProperty(ArgumentMatchers.eq("WKHTMLTOPDF_ARGS")))
        .thenReturn(WK_HTML_TO_PDF_ARGS);
    when(properties.getProperty(ArgumentMatchers.eq("BASE_DOCUMENT_DIR")))
        .thenReturn(file.toString());
    when(properties.getProperty(ArgumentMatchers.eq("WKHTMLTOPDF_COMMAND")))
        .thenReturn(WK_HTML_TO_PDF_PATH);
    when(properties.getProperty(ArgumentMatchers.eq("WKHTMLTOPDF_ARGS")))
        .thenReturn(WK_HTML_TO_PDF_ARGS);
  }

  private HRMReport initReport() {
    val hrmReport = mock(HRMReport.class);
    when(hrmReport.getFirstReportTextContent())
        .thenReturn(BASE64_STRING);
    when(hrmReport.getFileLocation())
        .thenReturn("test.pdf");
    return hrmReport;
  }

  private void initHrmDocumentDao() {
    val hrmDocument = new HRMDocument();
    val hrmDocumentDao = mock(HRMDocumentDao.class);
    utilsMock = mockStatic(SpringUtils.class);
    when(SpringUtils.getBean(HRMDocumentDao.class)).thenReturn(hrmDocumentDao);
    when(hrmDocumentDao.findById(1))
        .thenReturn(Collections.singletonList(hrmDocument));
  }

  private HRMPDFCreator createHrmPdfCreator() {
    outputStream = new ByteOutputStream();
    loggedInInfo = LoggedInInfo.getLoggedInInfoAsCurrentClassAndMethod();
    return new HRMPDFCreator(outputStream, 1, loggedInInfo);
  }
}
