package org.oscarehr.hospitalReportManager.util;

import java.util.Collections;
import lombok.val;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.hospitalReportManager.dao.HRMDocumentToProviderDao;
import org.oscarehr.hospitalReportManager.model.HRMDocumentToProvider;

@ExtendWith(MockitoExtension.class)
public class HrmDocumentUtilTest {

  @Mock HRMDocumentToProviderDao hrmDocumentToProviderDao;

  private final String UNCLAIMED_PROVIDER_NUMBER = "-1";
  private final String PROVIDER_NUMBER = "1";
  private final Integer HRM_DOCUMENT_ID = 123;

  private HrmDocumentUtil hrmDocumentUtil;

  @BeforeEach
  public void before() {
    hrmDocumentUtil = new HrmDocumentUtil(hrmDocumentToProviderDao);
  }

  @Test
  public void checkIfDocumentIsUnclaimed_noActiveLinkedProviders() {
    val hrmDocumentToProvider = new HRMDocumentToProvider(PROVIDER_NUMBER, HRM_DOCUMENT_ID);
    Mockito.when(hrmDocumentToProviderDao.findByHrmDocumentIdNoSystemUser(HRM_DOCUMENT_ID))
        .thenReturn(Collections.<HRMDocumentToProvider>emptyList());

    hrmDocumentUtil.handleUnclaimedDocument(hrmDocumentToProvider);

    val argument = ArgumentCaptor.forClass(HRMDocumentToProvider.class);
    Mockito.verify(hrmDocumentToProviderDao, Mockito.times(1))
        .persist((HRMDocumentToProvider) argument.capture());
    // generated unclaimed entry
    assertProviderHrmRouting(argument.getValue(),
        HRM_DOCUMENT_ID, UNCLAIMED_PROVIDER_NUMBER, false);
  }

  @Test
  public void checkIfDocumentIsUnclaimed_existingLinkedProvider() {
    val hrmDocumentToProvider = new HRMDocumentToProvider(PROVIDER_NUMBER, HRM_DOCUMENT_ID);
    Mockito.when(hrmDocumentToProviderDao.findByHrmDocumentIdNoSystemUser(HRM_DOCUMENT_ID))
        .thenReturn(Collections.singletonList(hrmDocumentToProvider));

    hrmDocumentUtil.handleUnclaimedDocument(hrmDocumentToProvider);

    Mockito.verify(hrmDocumentToProviderDao, Mockito.never())
        .merge(Mockito.any());
  }

  private void assertProviderHrmRouting(
      final HRMDocumentToProvider providerHRMRouting,
      final Integer hrmDocumentId,
      final String providerNo,
      final boolean isDeleted
  ) {
    Assertions.assertEquals(hrmDocumentId, providerHRMRouting.getHrmDocumentId());
    Assertions.assertEquals(providerNo, providerHRMRouting.getProviderNo());
    Assertions.assertEquals(isDeleted, providerHRMRouting.isDeleted());
  }
}
