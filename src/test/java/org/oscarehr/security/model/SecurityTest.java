/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.security.model;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.model.Security;

class SecurityTest {
  Security security;

  @BeforeEach
  public void setUp() {
    security = new Security();
  }

  @Test
  void givenSecurityWithLocalLock_whenIsLocalLockSet_thenReturnTrue() {
    security.setBLocallockset(1);
    assertTrue(security.isLocalLockSet());
  }

  @Test
  void givenSecurityWithoutLocalLock_whenIsLocalLockSet_thenReturnFalse() {
    security.setBLocallockset(0);
    assertFalse(security.isLocalLockSet());
  }

  @Test
  void givenNullLocalLock_whenIsLocalLockSet_thenReturnFalse() {
    security.setBLocallockset(null);
    assertFalse(security.isLocalLockSet());
  }

  @Test
  void givenSecurityWithRemoteLock_whenIsRemoteLockSet_thenReturnTrue() {
    security.setBRemotelockset(1);
    assertTrue(security.isRemoteLockSet());
  }

  @Test
  void givenSecurityWithOutRemoteLock_whenIsRemoteLockSet_thenReturnFalse() {
    security.setBRemotelockset(0);
    assertFalse(security.isRemoteLockSet());
  }

  @Test
  void givenNullRemoteLock_whenIsRemoteLockSet_thenReturnFalse() {
    security.setBRemotelockset(null);
    assertFalse(security.isRemoteLockSet());
  }
}
