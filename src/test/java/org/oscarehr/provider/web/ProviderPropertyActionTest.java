/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.provider.web;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.oscarehr.common.dao.UserPropertyDAO;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.UserProperty;
import org.oscarehr.util.LoggedInInfo;

public class ProviderPropertyActionTest {

  private AutoCloseable closeable;

  @Mock
  ActionMapping actionmapping;

  @Mock
  DynaActionForm actionform;

  @Mock
  HttpServletRequest request;

  @Mock
  HttpServletResponse response;

  @Mock
  UserPropertyDAO userPropertyDAO;

  @Mock
  HttpSession httpSession;

  @InjectMocks
  ProviderPropertyAction providerPropertyAction;

  @Before
  public void initialize() throws Exception {
    closeable = MockitoAnnotations.openMocks(this);
  }

  @After
  public void closeService() throws Exception {
    closeable.close();
  }

  @Test
  public void notNull() {
    assertNotNull(actionmapping);
    assertNotNull(actionform);
    assertNotNull(request);
    assertNotNull(response);
    assertNotNull(userPropertyDAO);
    assertNotNull(httpSession);
  }

  @Test
  public void testSaveHappyPath() {
    when(actionform.get("dateProperty")).thenReturn(getUserDateProperty());
    when(actionform.get("singleViewProperty")).thenReturn(getSingleViewProperty());
    when(actionform.get("staleLineNumberProperty")).thenReturn(getStaleLineNumberProperty());
    doNothing().when(userPropertyDAO).saveProp(any(UserProperty.class));
    providerPropertyAction.save(actionmapping, actionform, request, response);
    verify(userPropertyDAO, times(3)).saveProp(any(UserProperty.class));
  }

  @Test
  public void testSave_staleLineNumberProperty_null() {
    doNothing().when(userPropertyDAO).saveProp(any(UserProperty.class));
    providerPropertyAction.save(actionmapping, actionform, request, response);
    verify(userPropertyDAO, times(0)).saveProp(any(UserProperty.class));
  }

  @Test
  public void testSave_staleLineNumberProperty() {
    when(actionform.get("staleLineNumberProperty")).thenReturn(getStaleLineNumberProperty());
    doNothing().when(userPropertyDAO).saveProp(any(UserProperty.class));
    providerPropertyAction.save(actionmapping, actionform, request, response);
    verify(userPropertyDAO, times(1)).saveProp(any(UserProperty.class));
  }

  @Test
  public void testSave_staleLineNumberProperty_not_int() {
    when(actionform.get("staleLineNumberProperty")).thenReturn(getStaleLineNumberPropertyNotInt());
    doNothing().when(userPropertyDAO).saveProp(any(UserProperty.class));
    providerPropertyAction.save(actionmapping, actionform, request, response);
    verify(userPropertyDAO, times(0)).saveProp(any(UserProperty.class));
  }

  @Test
  public void testRemove() {
    when(actionform.get("dateProperty")).thenReturn(getUserDateProperty());
    when(actionform.get("singleViewProperty")).thenReturn(getSingleViewProperty());
    when(actionform.get("staleLineNumberProperty")).thenReturn(getStaleLineNumberProperty());
    doNothing().when(userPropertyDAO).delete(any(UserProperty.class));
    providerPropertyAction.remove(actionmapping, actionform, request, response);
    verify(userPropertyDAO, times(3)).delete(any(UserProperty.class));
  }

  private UserProperty getUserDateProperty() {
    return new UserProperty("1", UserProperty.STALE_NOTEDATE, "-2");
  }

  private UserProperty getSingleViewProperty() {
    return new UserProperty("2", UserProperty.STALE_FORMAT, "yes");
  }

  private UserProperty getStaleLineNumberProperty() {
    return new UserProperty("2", UserProperty.STALE_LINE, "3");
  }

  private UserProperty getStaleLineNumberPropertyNotInt() {
    return new UserProperty("2", UserProperty.STALE_LINE, "A");
  }

  private LoggedInInfo getLoggedInInfo() {
    LoggedInInfo loggedInInfo = new LoggedInInfo();
    loggedInInfo.setLoggedInProvider(getProvider());
    return loggedInInfo;
  }

  private Provider getProvider() {
    return new Provider("1");
  }

}
