/**
 * Copyright (c) 2013-2015. Department of Computer Science, University of Victoria. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Computer Science
 * LeadLab
 * University of Victoria
 * Victoria, Canada
 */

package org.oscarehr.rx;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.intThat;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;
import static org.mockito.quality.Strictness.LENIENT;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.oscarehr.common.dao.DrugDao;
import org.oscarehr.common.exception.AccessDeniedException;
import org.oscarehr.common.model.Drug;
import org.oscarehr.common.model.Prescription;
import org.oscarehr.managers.PrescriptionManager;
import org.oscarehr.managers.RxManager;
import org.oscarehr.managers.RxManager.PrescriptionDrugs;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import org.springframework.test.context.ActiveProfiles;
import oscar.log.LogAction;

@ExtendWith(value = MockitoExtension.class)
@MockitoSettings(strictness = LENIENT)
@ActiveProfiles(value = "test")
public class RxManagerTest {

  @InjectMocks
  private RxManager rxManager;
  @Mock
  private DrugDao drugDao;
  @Mock
  private SecurityInfoManager securityInfoManager;
  @Mock
  private PrescriptionManager prescriptionManager;

  private static MockedStatic<LogAction> logActionMock;

  // Helper variables for testing.
  private Drug old = null;
  private int daoAddNewDrugCalled = 0;

  @BeforeAll
  public static void beforeAll() {
    logActionMock =  mockStatic(LogAction.class);
  }

  @BeforeEach
  public void before() {
    this.old = null;
    this.daoAddNewDrugCalled = 0;

    mockPrescriptionManager();
    mockSecurityManager();
    mockDrugDao();
  }

  @AfterEach
  public void after() {
    this.drugDao = null;
    this.old = null;
  }

  @AfterAll
  public static void afterAll() {
    logActionMock.close();
  }

  @Test
  public void testReadCheckWithAllowedAccess() {

    LoggedInInfo info = new LoggedInInfo();

    // We know that MockSecurityInfoManager.hasPrivledge()
    // will return true if demographicNo = 1.

    try {
      rxManager.readCheck(info, 1);
    } catch (RuntimeException rte) {
      Assertions.fail();
    }
  }

  @Test
  public void testReadCheckWithDeniedAccess() {
    Assertions.assertThrows(
        AccessDeniedException.class,
        () -> {
          LoggedInInfo info = new LoggedInInfo();

          // We know that MockSecurityInfoManager.hasPrivledge()
          // will return true if demographicNo > 5, false otherwise.

          rxManager.readCheck(info, 10);
        });
  }

  @Test
  public void testGetDrugsWithStatusAll() {

    LoggedInInfo info = new LoggedInInfo();

    List<Drug> drugs = rxManager.getDrugs(info, 1, RxManager.ALL);

    Assertions.assertEquals(drugs.size(), 2);
    Assertions.assertEquals(drugs.get(0).getBrandName(), "Aspirin");
    Assertions.assertEquals(drugs.get(1).getBrandName(), "Tylenol");
  }

  @Test
  public void testGetDrugsWithStatusArchived() {

    LoggedInInfo info = new LoggedInInfo();

    List<Drug> drugs = rxManager.getDrugs(info, 1, RxManager.ARCHIVED);

    Assertions.assertEquals(drugs.size(), 1);
    Assertions.assertEquals(drugs.get(0).getBrandName(), "Aspirin");
  }

  @Test
  public void testGetDrugsWithStatusCurrent() {

    LoggedInInfo info = new LoggedInInfo();

    List<Drug> drugs = rxManager.getDrugs(info, 1, RxManager.CURRENT);

    Assertions.assertEquals(drugs.size(), 1);
    Assertions.assertEquals(drugs.get(0).getBrandName(), "Tylenol");
  }

  @Test
  public void testGetDrugsWithInvalidStatus() {
    Assertions.assertThrows(
        UnsupportedOperationException.class,
        () -> {
          LoggedInInfo info = new LoggedInInfo();

          List<Drug> drugs = rxManager.getDrugs(info, 1, "FOOBAR");
        });
  }

  @Test
  public void testWriteCheckWithInvalidCreds() {
    Assertions.assertThrows(
        AccessDeniedException.class,
        () -> {
          LoggedInInfo info = new LoggedInInfo();

          // The MockSecurityInfoManager.hasPrivledge()
          // will return false for demographicNo > 5

          rxManager.writeCheck(info, 6);
        });
  }

  @Test
  public void testWriteCheckWithValidCred() {

    LoggedInInfo info = new LoggedInInfo();

    // MockSecurityInfoManager.hasPriviledge() will
    // return true for demographicNo <=5

    try {
      rxManager.writeCheck(info, 1);
    } catch (RuntimeException re) {
      Assertions.fail(); // should not throw a RuntimeException!
    }
  }

  @Test
  public void testAddDrugWithValidDrug() {

    LoggedInInfo info = new LoggedInInfo();

    Drug d = new Drug();

    // genericName == ASA will return true from MockDrugDao.addDrug()
    d.setGenericName("ASA");

    // demographicId == 1 will cause writeCheck() to return pass.
    d.setDemographicId(1);

    Drug result = rxManager.addDrug(info, d);

    Assertions.assertNotNull(result);
    Assertions.assertEquals((Integer) 1, result.getId());
  }

  @Test
  public void testAddDrugWithInvalidDrug() {

    LoggedInInfo info = new LoggedInInfo();

    Drug d = new Drug();

    // genericName != ASA will return false from MockDrugDao.addDrug()
    d.setGenericName("Foobar");

    // demographicId == 1 will cause writeCheck() to return pass.
    d.setDemographicId(1);

    Assertions.assertNull(rxManager.addDrug(info, d));
  }

  @Test
  public void testUpdateDrugWithValidInput() {

    LoggedInInfo info = new LoggedInInfo();

    Drug d = new Drug();

    d.setDemographicId(1);
    d.setId(1);
    d.setGenericName("ASA");

    Drug result = rxManager.updateDrug(info, d);

    Assertions.assertNotNull(result);
    Assertions.assertEquals(1, (int) d.getId()); // should take on id assigned by dao.addNewDrug
    Assertions.assertEquals("ASA", d.getGenericName()); // should not change other fields.

    // merge() should have adjusted the this.old variable
    // to have archived status
    Assertions.assertTrue(old.isArchived());
    Assertions.assertEquals("represcribed", old.getArchivedReason());
  }

  @Test
  public void testUpdateDrugWithFailedAddNewDrug() {

    LoggedInInfo info = new LoggedInInfo();

    Drug d = new Drug();

    d.setDemographicId(1);
    d.setId(1);
    d.setGenericName("foobar"); // will fail in MockDrugDao

    Drug result = rxManager.updateDrug(info, d);

    Assertions.assertNull(result);

    // should not have created a new "old" drug.
    Assertions.assertNull(old);
  }

  @Test
  public void testDiscontinueWithInvalidDrugId() {

    LoggedInInfo info = new LoggedInInfo();

    // pass 2nd argument as drug id that is not
    // in the test data
    boolean r = rxManager.discontinue(info, 20, 1, "allergy");

    Assertions.assertFalse(r);
  }

  @Test
  public void testDiscontinueWithDrugIdNotMatchingDemographicId() {

    LoggedInInfo info = new LoggedInInfo();

    // pass 2nd argument as drug id that is not
    // in the test data
    boolean r = rxManager.discontinue(info, 1, 2, "allergy");

    Assertions.assertFalse(r);
  }

  @Test
  public void testDiscontinueWithValidReasonAdverseReaction() {
    executeValidDiscontinueByReason("adverseReaction");
  }

  @Test
  public void testDiscontinueWithValidReasonAllergy() {
    executeValidDiscontinueByReason("allergy");
  }

  @Test
  public void testDiscontinueWithValidReasonAnotherPhysician() {
    executeValidDiscontinueByReason("discontinuedByAnotherPhysician");
  }

  @Test
  public void testDiscontinueWithValidReasonDrugInteraction() {
    executeValidDiscontinueByReason("drugInteraction");
  }

  @Test
  public void testDiscontinueWithValidReasonCost() {
    executeValidDiscontinueByReason("cost");
  }

  @Test
  public void testDiscontinueWithValidReasonDeleted() {
    executeValidDiscontinueByReason("deleted");
  }

  @Test
  public void testDiscontinueWithValidReasonBenefitRisk() {
    executeValidDiscontinueByReason("increasedRiskBenefitRatio");
  }

  @Test
  public void testDiscontinueWithValidReasonEvidence() {
    executeValidDiscontinueByReason("newScientificEvidence");
  }

  @Test
  public void testDiscontinueWithValidReasonTreatment() {
    executeValidDiscontinueByReason("ineffectiveTreatment");
  }

  @Test
  public void testDiscontinueWithValidReasonNecessary() {
    executeValidDiscontinueByReason("noLongerNecessary");
  }

  @Test
  public void testDiscontinueWithValidReasonRequest() {
    executeValidDiscontinueByReason("patientRequest");
  }

  @Test
  public void testDiscontinueWithValidReasonSimplify() {
    executeValidDiscontinueByReason("simplifyingTreatment");
  }

  @Test
  public void testDiscontinueWithValidReasonUnknown() {
    executeValidDiscontinueByReason("unknown");
  }

  @Test
  public void testDiscontinueWithValidReasonOther() {
    executeValidDiscontinueByReason("other");
  }

  @Test
  public void testDiscontinueWithUnsupportedReason() {
    Assertions.assertThrows(
        UnsupportedOperationException.class,
        () -> {
          // should throw exception because reason is not supported.
          executeValidDiscontinueByReason("foobar");
        });
  }

  @Test
  public void testPrescribeBasic() {

    List<Drug> drugs = new ArrayList<Drug>();
    drugs.add(getTestDrug());

    LoggedInInfo info = new LoggedInInfo();

    PrescriptionDrugs pd = rxManager.prescribe(info, drugs, 1);

    Assertions.assertNotNull(pd);
    Assertions.assertEquals(pd.drugs.size(), 1);
    Assertions.assertNotNull(pd.prescription);
  }

  @Test
  public void testPrescribeBasicMultiple() {

    List<Drug> drugs = new ArrayList<Drug>();
    drugs.add(getTestDrug());
    drugs.add(getTestDrug());
    drugs.add(getTestDrug());

    LoggedInInfo info = new LoggedInInfo();

    PrescriptionDrugs pd = rxManager.prescribe(info, drugs, 1);

    Assertions.assertNotNull(pd);
    Assertions.assertEquals(pd.drugs.size(), 3);
    Assertions.assertNotNull(pd.prescription);
  }

  @Test
  public void testShouldReturnNullOnNullInfo() {

    List<Drug> drugs = new ArrayList<Drug>();
    drugs.add(getTestDrug());
    PrescriptionDrugs pd = rxManager.prescribe(null, drugs, 1);
    Assertions.assertNull(pd);
  }

  @Test
  public void testShouldReturnNullOnNullDrugs() {

    LoggedInInfo info = new LoggedInInfo();
    PrescriptionDrugs pd = rxManager.prescribe(info, null, 1);
    Assertions.assertNull(pd);
  }

  @Test
  public void testShouldReturnNullOnEmptyDrugs() {

    List<Drug> l = new ArrayList<Drug>();
    LoggedInInfo info = new LoggedInInfo();
    PrescriptionDrugs pd = rxManager.prescribe(info, l, 1);
    Assertions.assertNull(pd);
  }

  @Test
  public void testShouldReturnNullOnInvalidDemoNo() {

    List<Drug> drugs = new ArrayList<Drug>();
    drugs.add(getTestDrug());
    LoggedInInfo info = new LoggedInInfo();
    PrescriptionDrugs pd = rxManager.prescribe(info, drugs, -1);
    Assertions.assertNull(pd);
  }

  @Test
  public void testShouldReturnNullOnDrugThatCannotBePrescribed() {

    List<Drug> drugs = new ArrayList<Drug>();
    Drug d = getTestDrug();
    d.setProviderNo(""); // will cause check to fail.
    drugs.add(d);

    LoggedInInfo info = new LoggedInInfo();
    PrescriptionDrugs pd = rxManager.prescribe(info, drugs, 1);
    Assertions.assertNull(pd);
  }

  @Test
  public void testShouldDenyAcess() {
    Assertions.assertThrows(
        AccessDeniedException.class,
        () -> {
          List<Drug> drugs = new ArrayList<Drug>();
          Drug d = getTestDrug();
          drugs.add(d);
          LoggedInInfo info = new LoggedInInfo();
          PrescriptionDrugs pd = rxManager.prescribe(info, drugs, 10);
        });
  }

  @Test
  public void testShouldAttemptToAddDrugIfDoesNotExist() {

    List<Drug> drugs = new ArrayList<Drug>();
    Drug d = getTestDrug();
    d.setId(3); // result in MockDrugDao.find() failing.
    d.setGenericName("ASA"); // allowed to add in test MockDrugDao.addNewDrug
    drugs.add(d);
    LoggedInInfo info = new LoggedInInfo();
    PrescriptionDrugs pd = rxManager.prescribe(info, drugs, 1);

    Assertions.assertNotNull(pd);
    Assertions.assertEquals(daoAddNewDrugCalled, 1);
    Assertions.assertEquals(pd.drugs.get(0).getGenericName(), "ASA");
  }

  @Test
  public void testShouldReturnNullIfAddingANewDrugFails() {

    List<Drug> drugs = new ArrayList<Drug>();
    Drug d = getTestDrug();
    d.setId(3); // result in MockDrugDao.find() failing.
    d.setGenericName("NOT ASA"); // fail to add in test MockDrugDao.addNewDrug
    drugs.add(d);
    LoggedInInfo info = new LoggedInInfo();
    PrescriptionDrugs pd = rxManager.prescribe(info, drugs, 1);

    Assertions.assertNull(pd);
    Assertions.assertEquals(daoAddNewDrugCalled, 1);
  }

  @Test
  public void testCanPrescribeBasic() {
    Drug d = getTestDrug();
    Assertions.assertTrue(rxManager.canPrescribe(d));
  }

  @Test
  public void testCanPrescribeIsFalseOnNull() {
    Assertions.assertFalse(rxManager.canPrescribe(null));
  }

  @Test
  public void testCanPrescribeIsFalseNullProvider() {
    Drug d = getTestDrug();
    d.setProviderNo(null);
    Assertions.assertFalse(rxManager.canPrescribe(d));
  }

  @Test
  public void testCanPrescribeIsFalseEmptyStringProvider() {
    Drug d = getTestDrug();
    d.setProviderNo("");
    Assertions.assertFalse(rxManager.canPrescribe(d));
  }

  @Test
  public void testCanPrescribeIsFalseNullDemographic() {
    Drug d = getTestDrug();
    d.setDemographicId(null);
    Assertions.assertFalse(rxManager.canPrescribe(d));
  }

  @Test
  public void testCanPrescribeIsFalseInvalidDemographic() {
    Drug d = getTestDrug();
    d.setDemographicId(-1);
    Assertions.assertFalse(rxManager.canPrescribe(d));
  }

  @Test
  public void testCanPrescribeIsFalseOnNullStartDate() {
    Drug d = getTestDrug();
    d.setRxDate(null);
    Assertions.assertFalse(rxManager.canPrescribe(d));
  }

  @Test
  public void testCanPrescribeIsFalseOnNullEndDate() {
    Drug d = getTestDrug();
    d.setEndDate(null);
    Assertions.assertFalse(rxManager.canPrescribe(d));
  }

  @Test
  public void testCanPrescribeIsFalseOnBadDateSequence() {
    Drug d = getTestDrug();

    // start after end date.
    d.setEndDate(new Date(100000000));
    d.setRxDate(new Date(200000000));

    Assertions.assertFalse(rxManager.canPrescribe(d));
  }

  @Test
  public void testCanPrescribeIsFalseOnNullInstructions() {
    Drug d = getTestDrug();
    d.setSpecial(null);
    Assertions.assertFalse(rxManager.canPrescribe(d));
  }

  @Test
  public void testCanPrescribeIsFalseOnEmptyStringInstructions() {
    Drug d = getTestDrug();
    d.setSpecial("");
    Assertions.assertFalse(rxManager.canPrescribe(d));
  }

  @Test
  public void testGetHistoryBasic() {

    LoggedInInfo info = new LoggedInInfo();

    List<Drug> drugs = rxManager.getHistory(1, info, 1);

    Assertions.assertNotNull(drugs);
    Assertions.assertEquals(drugs.size(), 1);
  }

  @Test
  public void testGetHistoryReturnsEmptyListOnInvalidDrug() {

    LoggedInInfo info = new LoggedInInfo();

    // drug id > 5 results in not finding drug in MockDrugDao.findByDemographicIdAndDrugId()
    List<Drug> drugs = rxManager.getHistory(6, info, 1);

    Assertions.assertNotNull(drugs);
    Assertions.assertEquals(drugs.size(), 0);
  }

  @Test
  public void testGetHistoryCanDenyAccess() {
    Assertions.assertThrows(
        AccessDeniedException.class,
        () -> {
          LoggedInInfo info = new LoggedInInfo();

          // MockSecurityManager will fail the access check for demo > 5
          List<Drug> drugs = rxManager.getHistory(1, info, 6);
        });
  }

  // =========== TEST HELPER METHODS =================

  public Drug getTestDrug() {

    Date startDate = new Date();
    Date endDate = new Date();
    Date archivedDate = new Date();

    Drug d = new Drug();

    d.setId(1);
    d.setDemographicId(1);
    d.setProviderNo("1");
    d.setBrandName("Aspirin");
    d.setGenericName("ASA");
    d.setRegionalIdentifier("12345");
    d.setAtc("abcde");
    d.setTakeMax(2);
    d.setTakeMin(1);
    d.setRxDate((Date) startDate.clone());
    d.setEndDate((Date) endDate.clone());
    d.setFreqCode("BID");
    d.setDuration("28");
    d.setDurUnit("D");
    d.setRoute("PO");
    d.setDrugForm("TAB");
    d.setPrn(true);
    d.setMethod("Take");
    d.setRepeat(5);
    d.setSpecial("some string");
    d.setArchived(false);
    d.setArchivedDate((Date) archivedDate.clone());
    d.setArchivedReason("reason");

    return d;
  }

  public Prescription getTestPrescription() {

    Prescription p = new Prescription();

    p.setDemographicId(1);
    p.setProviderNo("1");
    p.setTextView("PRESCRIPTION TEXT");
    p.setDatePrescribed(new Date());
    p.setComments("COMMENT TEXT");

    return p;
  }

  protected void executeValidDiscontinueByReason(String reason) {

    LoggedInInfo info = new LoggedInInfo();

    boolean r = rxManager.discontinue(info, 1, 1, reason);

    Assertions.assertTrue(r);
    Assertions.assertEquals(reason, old.getArchivedReason());
    Assertions.assertTrue(old.isArchived());
    Assertions.assertNotNull(old.getArchivedDate());
  }

  // =========== TESTING SUPPORT CLASSES =============
  // Uses to mock objects to get around dependancy injection
  // allows for more control over exactly what is tested.

  protected void mockPrescriptionManager() {
    when(prescriptionManager.createNewPrescription(any(LoggedInInfo.class), anyList(),
        intThat(i -> (i <= 10))))
        .thenReturn(getTestPrescription());
    when(prescriptionManager.createNewPrescription(any(LoggedInInfo.class), anyList(),
        intThat(i -> (i > 10))))
        .thenReturn(null);
  }

  protected void mockSecurityManager() {
    when(securityInfoManager.hasPrivilege(any(), any(), any(), anyInt()))
        .thenAnswer(
            invocation -> {
              Object[] args = invocation.getArguments();
              Integer demoNo = (Integer) args[3];
              return demoNo <= 5;
            });
    when(securityInfoManager.hasPrivilege(any(), any(), any(), anyString()))
        .thenAnswer(
            invocation -> {
              Object[] args = invocation.getArguments();
              int demoNo = Integer.parseInt((String) args[3]);
              return demoNo <= 5;
            });
  }

  protected void mockDrugDao() {
    List<Drug> drugList;
    Drug tempDrug;

    drugList = new ArrayList<>();

    tempDrug = new Drug();
    tempDrug.setId(1);
    tempDrug.setDemographicId(1);
    tempDrug.setGenericName("ASA");
    tempDrug.setBrandName("Aspirin");
    tempDrug.setArchived(true);
    tempDrug.setArchivedDate(new Date());
    tempDrug.setArchivedReason("allergy");
    drugList.add(tempDrug);

    tempDrug = new Drug();
    tempDrug.setId(2);
    tempDrug.setDemographicId(1);
    tempDrug.setGenericName("Acetaminophen");
    tempDrug.setBrandName("Tylenol");
    tempDrug.setArchived(false);
    drugList.add(tempDrug);

    when(drugDao.findByDemographicId(anyInt())).thenReturn(drugList);
    when(drugDao.findByDemographicId(anyInt(), anyBoolean()))
        .thenAnswer(
            invocation -> {
              List<Drug> toReturn = new ArrayList<>();
              Object[] args = invocation.getArguments();
              boolean archived = (boolean) args[1];

              for (Drug drug : drugList) {
                if (drug.isArchived() == archived) {
                  toReturn.add(drug);
                }
              }
              return toReturn;
            });
    when(drugDao.findByDemographicIdAndDrugId(anyInt(), anyInt()))
        .thenAnswer(
            invocation -> {
              Object[] args = invocation.getArguments();
              Integer demographicNo = (Integer) args[0];
              Integer drugId = (Integer) args[1];

              if (drugId > 5) {
                return new ArrayList<Drug>();
              }
              else {
                List<Drug> drugs = new ArrayList<>();
                Drug d = getTestDrug();
                d.setId(drugId);
                d.setDemographicId(demographicNo);
                drugs.add(d);
                return drugs;
              }
            });
    when(drugDao.findByAtc(anyString()))
        .thenAnswer(
            invocation -> {
              Object[] args = invocation.getArguments();
              String atc = (String) args[0];

              if (atc.equals("BAD_ATC")) {
                return new ArrayList<Drug>();
              }
              else {
                List<Drug> drugs = new ArrayList<>();
                Drug d = getTestDrug();
                drugs.add(d);
                return drugs;
              }
            });
    when(drugDao.addNewDrug(any()))
        .thenAnswer(
            invocation -> {
              // For testing purposes we only return
              // a drug if the drug matches out ASA drug in the test data.
              // Write tests to take advantage of this fact...
              daoAddNewDrugCalled++;
              Object[] args = invocation.getArguments();
              Drug d = (Drug) args[0];

              if (d.getGenericName().equals("ASA")) {
                d.setId(1);
                return true;
              }
              return false;
            });
    when(drugDao.find(any()))
        .thenAnswer(
            invocation -> {
              Object[] args = invocation.getArguments();
              int j = (Integer) args[0];

              for (Drug d : drugList) {
                if (j == d.getId()) {
                  return d;
                }
              }
              return null;
            });
    doAnswer(
            invocation -> {
              Object[] args = invocation.getArguments();
              // Sets this in the parent class so that we
              // can check it after the test.
              old = (Drug) args[0];
              return null;
            })
        .when(drugDao)
        .merge(any());
  }
}
