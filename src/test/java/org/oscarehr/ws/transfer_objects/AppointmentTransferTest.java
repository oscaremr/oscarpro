package org.oscarehr.ws.transfer_objects;

import org.junit.jupiter.api.Test;
import org.oscarehr.common.model.Appointment;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;

class AppointmentTransferTest {

    @Test
    public void givenStartTimeAndEndTime_whenCopyTo_thenEndTimeShouldBeCorrectlyCopied() {
        // Given
        AppointmentTransfer appointmentTransfer = new AppointmentTransfer();
        Calendar appointmentStartDateTime = new GregorianCalendar(2024, Calendar.MAY, 31, 9, 30, 0);
        Calendar appointmentEndDateTime = new GregorianCalendar(2024, Calendar.MAY, 31, 9, 45, 0);
        Calendar appointmentEndDateTimeCopy = new GregorianCalendar(2024, Calendar.MAY, 31, 9, 45, 0);
        Calendar appointmentEndDateTimeCorrect = new GregorianCalendar(2024, Calendar.MAY, 31, 9, 44, 59);

        appointmentTransfer.setAppointmentStartDateTime(appointmentStartDateTime);
        appointmentTransfer.setAppointmentEndDateTime(appointmentEndDateTime);

        Appointment copy = new Appointment();
        // When
        appointmentTransfer.copyTo(copy);

        // Then
        assertNotEquals(appointmentEndDateTimeCopy.getTimeInMillis(), copy.getEndTime().getTime());
        assertEquals(appointmentEndDateTimeCorrect.getTimeInMillis(), copy.getEndTime().getTime());
        assertTrue(copy.getEndTime().before(appointmentEndDateTimeCopy.getTime()), "Copied appointment end time should be earlier than the original end time.");
    }
}