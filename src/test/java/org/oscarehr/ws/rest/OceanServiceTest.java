package org.oscarehr.ws.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import javax.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.oscarehr.common.model.OceanWorkflowTypeEnum;
import org.oscarehr.managers.ConsultationManager;
import org.oscarehr.util.LoggedInInfo;

public class OceanServiceTest {

  @Mock
  private HttpServletRequest request;
  @Mock
  private ConsultationManager consultationManager;
  @Mock
  private LoggedInInfo loggedInInfo;

  @Spy
  @InjectMocks
  private OceanService oceanService;

  @BeforeEach
  public void setUp() {
    openMocks(this);
  }

  @Test
  public void givenLowercaseType_whenGetAttachments_thenGetCorrectAttachmentType()
      throws Exception {
    mockConsultationManager();
    mockLoggedInInfo();
    oceanService.getAttachments(request, 1, "ereferral");
    assertEquals(OceanWorkflowTypeEnum.EREFERRAL,
        verifyGetLatestAttachmentsAndGetOceanWorkflowTypeEnum());
  }

  @Test
  public void givenUppercaseType_whenGetAttachments_thenGetCorrectAttachmentType()
      throws Exception {
    mockConsultationManager();
    mockLoggedInInfo();
    oceanService.getAttachments(request, 1, "MESSENGER");
    assertEquals(OceanWorkflowTypeEnum.MESSENGER,
        verifyGetLatestAttachmentsAndGetOceanWorkflowTypeEnum());
  }

  @Test
  public void givenMixedCaseType_whenGetAttachments_thenGetCorrectAttachmentType()
      throws Exception {
    mockConsultationManager();
    mockLoggedInInfo();
    oceanService.getAttachments(request, 1, "eReferral");
    assertEquals(OceanWorkflowTypeEnum.EREFERRAL,
        verifyGetLatestAttachmentsAndGetOceanWorkflowTypeEnum());
  }

  @Test
  public void givenNullType_whenGetAttachments_thenGetNullAttachmentType()
      throws Exception {
    mockConsultationManager();
    mockLoggedInInfo();
    oceanService.getAttachments(request, 1, null);
    assertNull(verifyGetLatestAttachmentsAndGetOceanWorkflowTypeEnum());
  }

  @Test
  public void givenEmptyType_whenGetAttachments_thenGetNullAttachmentType()
      throws Exception {
    mockConsultationManager();
    mockLoggedInInfo();
    oceanService.getAttachments(request, 1, "");
    assertNull(verifyGetLatestAttachmentsAndGetOceanWorkflowTypeEnum());
  }

  private void mockConsultationManager() throws Exception {
    when(consultationManager.getLatestDemographicEReferOceanApiAttachments(
        any(LoggedInInfo.class), any(HttpServletRequest.class), anyInt(),
        any(OceanWorkflowTypeEnum.class))).thenReturn(null);
  }

  private void mockLoggedInInfo() {
    doReturn(loggedInInfo).when(oceanService).getLoggedInInfo();
  }

  private OceanWorkflowTypeEnum verifyGetLatestAttachmentsAndGetOceanWorkflowTypeEnum()
      throws Exception {
    ArgumentCaptor<OceanWorkflowTypeEnum> oceanWorkflowTypeEnumArgumentCaptor =
        ArgumentCaptor.forClass(OceanWorkflowTypeEnum.class);

    verify(consultationManager, times(1))
        .getLatestDemographicEReferOceanApiAttachments(
            any(LoggedInInfo.class), any(HttpServletRequest.class), anyInt(),
            oceanWorkflowTypeEnumArgumentCaptor.capture());

    return oceanWorkflowTypeEnumArgumentCaptor.getValue();
  }

}