/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package org.oscarehr.ws.rest.conversion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.oscarehr.common.model.DemographicExt;
import org.oscarehr.common.model.DemographicExtKey;
import org.oscarehr.common.model.ProfessionalSpecialist;
import org.oscarehr.managers.adapter.DemographicAdapter;
import org.oscarehr.ws.rest.to.model.DemographicTo1;

/**
 * Unit tests for {@link DemographicConverter}
 */
public class DemographicConverterPhysicianTest {

  private DemographicConverter demoConverter = new DemographicConverter();
  private DemographicAdapter demographicAdapter = new DemographicAdapter();
  private List<ProfessionalSpecialist> specialists;

  @Before
  public void before() {
    specialists = new ArrayList<>(Arrays.asList(
        createSpecialist("111-111-1111", "123 Main St", "222-222-2222"),
        createSpecialist("333-333-3333", "456 Elm St", "444-444-4444"),
        createSpecialist("555-555-5555", "789 Oak St", "666-666-6666")
    ));
  }

  @Test
  public void findMostSimilarPhysician_oneMatchingField() {
    val mostSimilarPhysician = demoConverter.findMostSimilarPhysician(
        specialists,
        "111-111-1111",
        "999 Park Ave",
        "333-333-3333"
    );
    assertEquals(specialists.get(0), mostSimilarPhysician);
  }

  @Test
  public void findMostSimilarPhysician_twoMatchingFields() {
    val mostSimilarPhysician = demoConverter.findMostSimilarPhysician(
        specialists,
        "555-555-5555",
        "123 Main St",
        "666-666-6666"
    );
    assertEquals(specialists.get(2), mostSimilarPhysician);
  }

  @Test
  public void findMostSimilarPhysician_threeMatchingFields() {
    val mostSimilarPhysician = demoConverter.findMostSimilarPhysician(
        specialists,
        "333-333-3333",
        "456 Elm St",
        "444-444-4444"
    );
    assertEquals(specialists.get(1), mostSimilarPhysician);
  }

  @Test
  public void findMostSimilarPhysician_emptySpecialists() {
    val mostSimilarPhysician = demoConverter.findMostSimilarPhysician(
        Collections.<ProfessionalSpecialist>emptyList(),
        "123-456-7890",
        "Test Address 123",
        "0987-654-321"
    );
    assertNotNull(mostSimilarPhysician);
  }

  @Test
  public void createDemographicExtension() {
    DemographicTo1 demographicTo = new DemographicTo1();
    demographicTo.setDemographicNo(1);
    demographicTo.setProviderNo("123");
    val extension = demoConverter.createDemographicExtension(
        demographicTo,
        DemographicExtKey.DOCTOR_ROSTER.getKey(),
        "11"
    );
    assertEquals((Integer) 1, extension.getDemographicNo());
    assertEquals("123", extension.getProviderNo());
    assertEquals(DemographicExtKey.DOCTOR_ROSTER.getKey(), extension.getKey());
    assertEquals("11", extension.getValue());
  }

  @Test
  public void addExtensionToArray_emptyArray() {
    DemographicExt[] initialExtensions = new DemographicExt[]{};
    val extension = this.createDemographicExtension(
        DemographicExtKey.DOCTOR_ROSTER.getKey(),
        "11"
    );
    val extensions = demoConverter.addExtensionToArray(
        initialExtensions,
        extension
    );
    assertEquals(extension, extensions[0]);
  }

  @Test
  public void addExtensionToArray_nonEmptyArray() {
    DemographicExt[] initialExtensions = {
        this.createDemographicExtension(DemographicExtKey.DOCTOR_ROSTER.getKey(), "11")
    };
    val extension = this.createDemographicExtension(
        DemographicExtKey.DOCTOR_ROSTER_NAME.getKey(),
        "Test,Specialist"
    );
    val extensions = demoConverter.addExtensionToArray(
        initialExtensions,
        extension
    );
    assertEquals(extension, extensions[extensions.length - 1]);
  }

  @Test
  public void addExtensionToArray_nullArray() {
    val extension = this.createDemographicExtension(
        DemographicExtKey.DOCTOR_ROSTER.getKey(),
        "11"
    );
    val extensions = demoConverter.addExtensionToArray(
        null,
        extension
    );
    assertEquals(extension, extensions[extensions.length - 1]);
  }

  private DemographicExt createDemographicExtension(
      final String key,
      final String value
  ) {
    val extension = new DemographicExt();
    extension.setKey(key);
    extension.setValue(value);
    return extension;
  }

  private ProfessionalSpecialist createSpecialist(
      final String phone,
      final String address,
      final String fax
  ) {
    val specialist = new ProfessionalSpecialist();
    specialist.setPhoneNumber(phone);
    specialist.setStreetAddress(address);
    specialist.setFaxNumber(fax);
    return specialist;
  }
}