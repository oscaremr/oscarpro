/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.util;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.apache.commons.lang.StringEscapeUtils;
import org.junit.Test;

public class FaxUtilsTest {
  @Test
  public void isValidFaxNumber_12DigitPass() {
    assertTrue(FaxUtils.isValidFaxNumber("9-9-345-678-9012"));
    assertTrue(FaxUtils.isValidFaxNumber("9-1-345-678-9012"));
  }

  @Test
  public void isValidFaxNumber_12DigitFail() {
    assertFalse(FaxUtils.isValidFaxNumber("123-456789012"));
  }

  @Test
  public void isValidFaxNumber_11DigitPass() {
    assertTrue(FaxUtils.isValidFaxNumber("9-234-567-8901"));
    assertTrue(FaxUtils.isValidFaxNumber("1-234-567-8901"));
  }

  @Test
  public void isValidFaxNumber_11DigitFail() {
    assertFalse(FaxUtils.isValidFaxNumber("02345678901"));
  }

  @Test
  public void isValidFaxNumber_10DigitPass() {
    assertTrue(FaxUtils.isValidFaxNumber("123-456-7890"));
    assertTrue(FaxUtils.isValidFaxNumber("(123) 456-7890"));
  }

  @Test
  public void isValidFaxNumber_9DigitsOrLess() {
    assertFalse(FaxUtils.isValidFaxNumber("123456789"));
  }

  @Test
  public void isValidFaxNumber_13DigitsOrMore() {
    assertFalse(FaxUtils.isValidFaxNumber("1234567891234"));
    assertFalse(FaxUtils.isValidFaxNumber("1991234567890"));
  }

  @Test
  public void trimFaxNumber_whitespace() {
    assertEquals(FaxUtils.trimFaxNumber("123 456 7890"), "1234567890");
  }

  @Test
  public void trimFaxNumber_brackets() {
    assertEquals(FaxUtils.trimFaxNumber("(123)456 7890"), "1234567890");
  }

  @Test
  public void trimFaxNumber_alphabet() {
    assertEquals(FaxUtils.trimFaxNumber("(123)456 789a"), "123456789");
  }

  @Test
  public void convertToRegExp_leading9() {
    assertEquals(
        StringEscapeUtils.unescapeJava(
            "(1|9[19]?)?[()\\-\\s]*(123)[()\\-\\s]*(456)[()\\-\\s]*(7890)"),
        FaxUtils.convertToRegExp("91234567890"));
    assertEquals(
        StringEscapeUtils.unescapeJava(
            "(1|9[19]?)?[()\\-\\s]*(123)[()\\-\\s]*(456)[()\\-\\s]*(7890)"),
        FaxUtils.convertToRegExp("911234567890"));
    assertEquals(
        StringEscapeUtils.unescapeJava(
            "(1|9[19]?)?[()\\-\\s]*(123)[()\\-\\s]*(456)[()\\-\\s]*(7890)"),
        FaxUtils.convertToRegExp("991234567890"));
  }

  @Test
  public void convertToRegExp_leading1() {
    assertEquals(
        StringEscapeUtils.unescapeJava(
            "(1|9[19]?)?[()\\-\\s]*(123)[()\\-\\s]*(456)[()\\-\\s]*(7890)"),
        FaxUtils.convertToRegExp("11234567890"));
  }

  @Test
  public void convertToRegExp_10Digits() {
    assertEquals(
        StringEscapeUtils.unescapeJava(
            "(1|9[19]?)?[()\\-\\s]*(123)[()\\-\\s]*(456)[()\\-\\s]*(7890)"),
        FaxUtils.convertToRegExp("1234567890"));
  }
}
