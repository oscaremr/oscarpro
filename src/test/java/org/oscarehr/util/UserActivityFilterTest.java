/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package org.oscarehr.util;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserActivityFilterTest {

  private static final String AUTO_REFRESH = "autoRefresh";
  private static final String JAVASCRIPT_SERVLET = "/JavaScriptServlet";
  private static final String UPGRADE_INSECURE_REQUESTS = "upgrade-insecure-requests";

  @Mock private HttpServletRequest mockRequest;

  @Test
  public void givenUserRequest_whenAutoRefreshTrue_thenFalseReturned() throws Exception {

    when(mockRequest.getParameter(AUTO_REFRESH)).thenReturn("true");
    StringBuffer url = new StringBuffer();
    when(mockRequest.getRequestURL()).thenReturn(url);
    when(mockRequest.getHeader(UPGRADE_INSECURE_REQUESTS)).thenReturn("");

    boolean result = isUserRequest(mockRequest);
    assertFalse(result);
  }

  @Test
  public void givenUserRequest_whenRequestURLMatchingJavaScriptServlet_thenFalseReturned()
      throws Exception {

    when(mockRequest.getParameter(AUTO_REFRESH)).thenReturn("false");
    StringBuffer url = new StringBuffer(mockRequest.getContextPath() + JAVASCRIPT_SERVLET);
    when(mockRequest.getRequestURL()).thenReturn(url);
    when(mockRequest.getHeader(UPGRADE_INSECURE_REQUESTS)).thenReturn("");

    boolean result = isUserRequest(mockRequest);
    assertFalse(result);
  }

  @Test
  public void givenUserRequest_whenUpgradeInsecureRequestsHeaderNull_thenFalseReturned()
      throws Exception {

    when(mockRequest.getParameter(AUTO_REFRESH)).thenReturn("false");
    StringBuffer url = new StringBuffer();
    when(mockRequest.getRequestURL()).thenReturn(url);
    when(mockRequest.getHeader(UPGRADE_INSECURE_REQUESTS)).thenReturn(null);

    boolean result = isUserRequest(mockRequest);
    assertFalse(result);
  }

  @Test
  public void givenUserRequest_whenAllConditionsFalse_thenTrueReturned() throws Exception {

    when(mockRequest.getParameter(AUTO_REFRESH)).thenReturn("false");
    StringBuffer url = new StringBuffer();
    when(mockRequest.getRequestURL()).thenReturn(url);
    when(mockRequest.getHeader(UPGRADE_INSECURE_REQUESTS)).thenReturn("");

    boolean result = isUserRequest(mockRequest);
    assertTrue(result);
  }

  private boolean isUserRequest(HttpServletRequest mockRequest) {
    UserActivityFilter userActivityFilter = new UserActivityFilter();
    return userActivityFilter.isUserRequest(mockRequest);
  }
}
