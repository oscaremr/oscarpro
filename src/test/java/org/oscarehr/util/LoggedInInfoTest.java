package org.oscarehr.util;

import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import lombok.val;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class LoggedInInfoTest {

  private final String firstTestIp = "127.0.0.1";
  private final String secondTestIp = "10.255.123.5";

  @Mock
  private HttpServletRequest mockRequest;

  @Test
  public void obtainClientIpAddress_whenNoXForwardedForHeader_thenRemoteAddressIsReturned() {
    when(mockRequest.getHeader("X-Forwarded-For")).thenReturn(null);
    when(mockRequest.getRemoteAddr()).thenReturn(firstTestIp);
    val clientIp = LoggedInInfo.obtainClientIpAddress(mockRequest);
    Assert.assertEquals(firstTestIp, clientIp);
  }

  @Test
  public void obtainClientIpAddress_whenXForwardedForHeader_thenFirstIpAddressIsReturned() {
    when(mockRequest.getHeader("X-Forwarded-For")).thenReturn(firstTestIp + ", " + secondTestIp);
    val clientIp = LoggedInInfo.obtainClientIpAddress(mockRequest);
    Assert.assertEquals(firstTestIp, clientIp);
  }

  @Test
  public void obtainClientIpAddress_whenXForwardedForHeaderIsEmpty_thenRemoteAddressIsReturned() {
    when(mockRequest.getHeader("X-Forwarded-For")).thenReturn("");
    when(mockRequest.getRemoteAddr()).thenReturn(secondTestIp);
    val clientIp = LoggedInInfo.obtainClientIpAddress(mockRequest);
    Assert.assertEquals(secondTestIp, clientIp);
  }
}