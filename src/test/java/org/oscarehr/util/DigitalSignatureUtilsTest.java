package org.oscarehr.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Answers.CALLS_REAL_METHODS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import lombok.val;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.oscarehr.common.dao.DigitalSignatureDao;
import org.oscarehr.common.dao.DigitalSignatureFavouriteDao;
import org.oscarehr.common.model.DigitalSignature;
import org.oscarehr.common.model.DigitalSignatureFavourite;
import org.oscarehr.common.model.Facility;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class DigitalSignatureUtilsTest {

  private final String FILE_NAME = "test.jpg";
  private final String LABEL = "test";
  private final int DEMOGRAPHIC_ID = 1;
  private final int FACILITY_ID = 1;
  private final String PROVIDER_NUMBER = "123456";
  private final String SIGNATURE_IMAGE = Arrays.toString(new byte[1024 * 256]);
  private final String SIGNATURE_REQUEST_ID = "1234567890";

  private FileInputStream fileInputStream;
  private LoggedInInfo loggedInInfo;
  private Facility facility;

  @Mock
  private DigitalSignatureDao digitalSignatureDao;
  @Mock
  private DigitalSignatureFavouriteDao digitalSignatureFavouriteDao;

  @Captor
  private ArgumentCaptor<DigitalSignatureFavourite> digitalSignatureFavouriteCaptor;

  private MockedStatic<SpringUtils> springUtils;
  private MockedStatic<DigitalSignatureUtils> digitalSignatureUtils;

  @BeforeEach
  public void before() throws IOException {
    loggedInInfo = mock(LoggedInInfo.class);
    facility = mock(Facility.class);

    when(facility.getId()).thenReturn(FACILITY_ID);
    when(loggedInInfo.getCurrentFacility()).thenReturn(facility);
    when(loggedInInfo.getLoggedInProviderNo()).thenReturn(PROVIDER_NUMBER);

    springUtils = mockStatic(SpringUtils.class);
    springUtils
        .when(() -> SpringUtils.getBean(eq("digitalSignatureDao")))
        .thenReturn(digitalSignatureDao);
    when(digitalSignatureDao.saveEntity(any(DigitalSignature.class)))
        .thenReturn(createTestDigitalSignature());
    springUtils
        .when(() -> SpringUtils.getBean(DigitalSignatureFavouriteDao.class))
        .thenReturn(digitalSignatureFavouriteDao);

    /*
     * We have to use a mock to get allow us to set up a spy on this static class. We mock most
     * methods to call the real thing, but we can mock specific methods to help testing.
     */
    fileInputStream = mock(FileInputStream.class);
    when(fileInputStream.read(ArgumentMatchers.any())).thenReturn(1);
    digitalSignatureUtils = mockStatic(DigitalSignatureUtils.class, CALLS_REAL_METHODS);
    digitalSignatureUtils
        .when(() ->
            DigitalSignatureUtils.createFileInputStream(anyString()))
        .thenReturn(fileInputStream);
  }

  @AfterEach
  public void after() {
    springUtils.close();
    digitalSignatureUtils.close();
  }

  @Test
  public void storeDigitalSignatureFromTempFileToDB_digitalSignaturesDisabled() {
    when(facility.isEnableDigitalSignatures()).thenReturn(false);
    DigitalSignature digitalSignature =
        DigitalSignatureUtils.storeDigitalSignatureFromTempFileToDB(
            loggedInInfo, SIGNATURE_REQUEST_ID, DEMOGRAPHIC_ID, LABEL);
    assertNull(digitalSignature);
  }

  @Test
  public void storeDigitalSignatureFromTempFileToDB_digitalSignaturesEnabled() throws IOException {
    when(facility.isEnableDigitalSignatures()).thenReturn(true);
    DigitalSignature digitalSignature =
        DigitalSignatureUtils.storeDigitalSignatureFromTempFileToDB(
            loggedInInfo, SIGNATURE_REQUEST_ID, DEMOGRAPHIC_ID, LABEL);
    assertDigitalSignature(digitalSignature, true);
  }

  @Test
  public void storeDigitalSignatureFromTempFileToDB_hasLabel() throws IOException {
    when(facility.isEnableDigitalSignatures()).thenReturn(true);
    DigitalSignature digitalSignature =
        DigitalSignatureUtils.storeDigitalSignatureFromTempFileToDB(
            loggedInInfo, SIGNATURE_REQUEST_ID, DEMOGRAPHIC_ID, LABEL);
    assertDigitalSignature(digitalSignature, true);
  }

  @Test
  public void storeDigitalSignatureFromTempFileToDB_noLabel() throws IOException {
    when(facility.isEnableDigitalSignatures()).thenReturn(true);
    DigitalSignature digitalSignature =
        DigitalSignatureUtils.storeDigitalSignatureFromTempFileToDB(
            loggedInInfo, SIGNATURE_REQUEST_ID, DEMOGRAPHIC_ID);
    assertDigitalSignature(digitalSignature, false);
  }

  @Test
  public void storeDigitalSignatureFromTempFileToDB_nullLabel() throws IOException {
    when(facility.isEnableDigitalSignatures()).thenReturn(true);
    DigitalSignature digitalSignature =
        DigitalSignatureUtils.storeDigitalSignatureFromTempFileToDB(
            loggedInInfo, SIGNATURE_REQUEST_ID, DEMOGRAPHIC_ID, null);
    assertDigitalSignature(digitalSignature, false);
  }

  @Test
  public void storeDigitalSignatureFromTempFileToDB_blankLabel() throws IOException {
    when(facility.isEnableDigitalSignatures()).thenReturn(true);
    DigitalSignature digitalSignature =
        DigitalSignatureUtils.storeDigitalSignatureFromTempFileToDB(
            loggedInInfo, SIGNATURE_REQUEST_ID, DEMOGRAPHIC_ID, "");
    assertDigitalSignature(digitalSignature, false);
  }

  @Test
  public void saveSignature_hasLabel() throws IOException {
    DigitalSignature digitalSignature =
        DigitalSignatureUtils.saveSignature(loggedInInfo, FILE_NAME, DEMOGRAPHIC_ID, LABEL);
    assertDigitalSignature(digitalSignature, true);
  }

  @Test
  public void saveSignature_noLabel() throws IOException {
    DigitalSignature digitalSignature =
        DigitalSignatureUtils.saveSignature(loggedInInfo, FILE_NAME, DEMOGRAPHIC_ID);
    assertDigitalSignature(digitalSignature, false);
  }

  @Test
  public void saveSignature_nullLabel() throws IOException {
    DigitalSignature digitalSignature =
        DigitalSignatureUtils.saveSignature(loggedInInfo, FILE_NAME, DEMOGRAPHIC_ID, null);
    assertDigitalSignature(digitalSignature, false);
  }

  @Test
  public void saveSignature_blankLabel() throws IOException {
    DigitalSignature digitalSignature =
        DigitalSignatureUtils.saveSignature(loggedInInfo, FILE_NAME, DEMOGRAPHIC_ID, "");
    assertDigitalSignature(digitalSignature, false);
  }

  private void assertDigitalSignature(DigitalSignature digitalSignature, boolean isFavourite)
      throws IOException {
    assertNotNull(digitalSignature);
    verify(fileInputStream, times(1)).read(ArgumentMatchers.any());
    assertEquals(DEMOGRAPHIC_ID, digitalSignature.getDemographicId());
    assertEquals(FACILITY_ID, digitalSignature.getFacilityId());
    assertEquals(PROVIDER_NUMBER, digitalSignature.getProviderNo());
    assertEquals(SIGNATURE_IMAGE, Arrays.toString(digitalSignature.getSignatureImage()));
    if (isFavourite) {
      assertEquals(LABEL, digitalSignature.getLabel());
      assertSignatureIsFavourited(digitalSignature);
      return;
    }
    assertNull(digitalSignature.getLabel());
    verify(digitalSignatureFavouriteDao, times(0))
        .saveEntity(any(DigitalSignatureFavourite.class));
  }

  private void assertSignatureIsFavourited(final DigitalSignature digitalSignature) {
    verify(digitalSignatureFavouriteDao, times(1))
        .saveEntity(digitalSignatureFavouriteCaptor.capture());
    val favouriteSignature = digitalSignatureFavouriteCaptor.getValue();
    assertEquals(digitalSignature.getId(), favouriteSignature.getSignatureId());
    assertFalse(favouriteSignature.isArchived());
  }

  private DigitalSignature createTestDigitalSignature() {
    val digitalSignature = new DigitalSignature();
    digitalSignature.setDemographicId(DEMOGRAPHIC_ID);
    digitalSignature.setFacilityId(FACILITY_ID);
    digitalSignature.setProviderNo(PROVIDER_NUMBER);
    digitalSignature.setSignatureImage(new byte[1024 * 256]);
    return digitalSignature;
  }
}
