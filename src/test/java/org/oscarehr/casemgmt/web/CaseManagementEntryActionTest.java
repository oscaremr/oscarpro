package org.oscarehr.casemgmt.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionMapping;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.casemgmt.web.formbeans.CaseManagementEntryFormBean;
import org.oscarehr.common.dao.DxDao;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

@ExtendWith(MockitoExtension.class)
public class CaseManagementEntryActionTest {

  private MockedStatic<SpringUtils> springUtilsMockedStatic;
  private MockedStatic<LoggedInInfo> loggedInInfoMockedStatic;

  @Mock
  ActionMapping actionMapping;

  @Mock
  CaseManagementEntryFormBean caseManagementEntryFormBean;

  @Mock
  HttpServletRequest request;

  @Mock
  HttpServletResponse response;

  @Mock
  DxDao dxDao;

  @Mock
  LoggedInInfo loggedInInfo;

  @Mock
  HttpSession session;

  @BeforeEach
  public void setUp() {
    springUtilsMockedStatic =  mockStatic(SpringUtils.class);
    loggedInInfoMockedStatic = mockStatic(LoggedInInfo.class);

    springUtilsMockedStatic
        .when(() -> SpringUtils.getBean(DxDao.class))
        .thenReturn(dxDao);

    springUtilsMockedStatic
        .when(() -> SpringUtils.getBean(eq("dxDao")))
        .thenReturn(dxDao);

    loggedInInfoMockedStatic
        .when(() -> LoggedInInfo.getLoggedInInfoFromSession(request))
        .thenAnswer(invocation -> loggedInInfo);

    when(request.getParameter("updateParent"))
        .thenReturn("true");
    when(request.getSession())
        .thenReturn(session);
    when(session.getAttribute("userrole"))
        .thenReturn("userrole");
    when(request.getParameter("demographicNo"))
        .thenReturn("1");
    when(session.getAttribute("caseManagementEntryForm1"))
        .thenReturn(caseManagementEntryFormBean);
  }

  @AfterEach
  public void close(){
    springUtilsMockedStatic.close();
    loggedInInfoMockedStatic.close();
  }

  @Test
  public void givenCaseManagementEntryAction_whenSaveAndExit_thenVerifyWindowClose() throws Exception {

    CaseManagementEntryAction action = new CaseManagementEntryAction();
    action.saveAndExit(
        actionMapping, caseManagementEntryFormBean, request, response
    );

    assertEquals("true", request.getParameter("updateParent"));
    verify(actionMapping, times(1)).findForward("windowClose");
  }
}