package org.oscarehr.casemgmt.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.quatro.dao.security.SecobjprivilegeDao;
import java.util.Properties;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.DynaActionForm;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.oscarehr.casemgmt.service.CaseManagementManager;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.Facility;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import oscar.util.OscarRoleObjectPrivilege;

@ExtendWith(MockitoExtension.class)
public class CaseManagementViewActionTest {

  @Mock
  ActionMapping actionMapping;

  @Mock
  DynaActionForm actionForm;

  @Mock
  HttpServletRequest request;

  @Mock
  HttpServletResponse response;

  @Mock
  HttpSession httpSession;

  @Mock
  SystemPreferencesDao systemPreferencesDao;

  @Mock
  SecobjprivilegeDao secobjprivilegeDao;

  @Mock
  CaseManagementManager caseManagementManager;

  private AutoCloseable closeable;
  private MockedStatic<SpringUtils> springUtils;
  private MockedStatic<LoggedInInfo> loggedInInfo;
  private MockedStatic<OscarRoleObjectPrivilege> oscarRoleObjectPrivilege;

  @BeforeEach
  public void initialize() {
    closeable = MockitoAnnotations.openMocks(this);
    mockObjects();
  }

  @AfterEach
  public void closeService() throws Exception {
    closeable.close();
    springUtils.close();
    loggedInInfo.close();
    oscarRoleObjectPrivilege.close();
  }

  @Test
  public void givenFilterNoteEnabled_whenListNotes_thenFilterNotes() throws Exception {
    when(systemPreferencesDao.findPreferenceByName(SystemPreferences.CAISI_NOTE_FILTER_ENABLED))
    .thenReturn(getCaisiNoteFilterPreferences("false"));
    
    CaseManagementViewAction caseManagementViewAction = new CaseManagementViewAction();
    caseManagementViewAction.setCaseManagementManager(caseManagementManager);

    caseManagementViewAction.listNotes(actionMapping, actionForm, request, response);

    verify(systemPreferencesDao, times(1))
        .findPreferenceByName(SystemPreferences.CAISI_NOTE_FILTER_ENABLED);
    verify(caseManagementManager, times(1))
        .filterNotes(any(LoggedInInfo.class), anyString(), anyCollection(), anyString());
  }

  @Test
  public void givenFilterNoteDisabled_whenListNotes_thenNotFilterNotes() throws Exception {
    when(systemPreferencesDao.findPreferenceByName(SystemPreferences.CAISI_NOTE_FILTER_ENABLED))
    .thenReturn(getCaisiNoteFilterPreferences("true"));
    
    CaseManagementViewAction caseManagementViewAction = new CaseManagementViewAction();
    caseManagementViewAction.setCaseManagementManager(caseManagementManager);

    caseManagementViewAction.listNotes(actionMapping, actionForm, request, response);

    verify(systemPreferencesDao, times(1))
        .findPreferenceByName(SystemPreferences.CAISI_NOTE_FILTER_ENABLED);
    verify(caseManagementManager, times(0))
        .filterNotes(any(LoggedInInfo.class), anyString(), anyCollection(), anyString());
  }

  private SystemPreferences getCaisiNoteFilterPreferences(String enable) {
    return new SystemPreferences(SystemPreferences.CAISI_NOTE_FILTER_ENABLED, enable);
  }

  private void mockObjects() {
    springUtils = mockStatic(SpringUtils.class);
    loggedInInfo = mockStatic(LoggedInInfo.class);
    oscarRoleObjectPrivilege = mockStatic(OscarRoleObjectPrivilege.class);

    when(request.getSession()).thenReturn(httpSession);
    when(request.getParameter("providerNo")).thenReturn("1");
    when(request.getParameter("demographicNo")).thenReturn("1");
    when(request.getParameterValues("issue_code"))
        .thenReturn(new String[] {"OMeds", "RiskFactors", "FamHistory", "MedHistory"});
    when(request.getRequestURL()).thenReturn(new StringBuffer());

    springUtils
        .when(() -> SpringUtils.getBean(SystemPreferencesDao.class))
        .thenReturn(systemPreferencesDao);
    springUtils
        .when(() -> SpringUtils.getBean(eq("secobjprivilegeDao")))
        .thenReturn(secobjprivilegeDao);

    oscarRoleObjectPrivilege
        .when(() -> OscarRoleObjectPrivilege.getPrivilegeProp(anyString()))
        .thenAnswer(
            (Answer<Vector<Object>>)
                invocation -> {
                  Vector<Object> ret = new Vector<>();
                  Vector<String> roleInObj = new Vector<>();
                  ret.add(new Properties());
                  ret.add(roleInObj);
                  return ret;
                });
    oscarRoleObjectPrivilege
        .when(
            () ->
                OscarRoleObjectPrivilege.checkPrivilege(
                    anyString(), any(Properties.class), anyList()))
        .thenReturn(true);

    loggedInInfo
        .when(() -> LoggedInInfo.getLoggedInInfoFromSession(request))
        .thenAnswer(
            (Answer<LoggedInInfo>)
                invocation -> {
                  LoggedInInfo loggedInInfo = new LoggedInInfo();
                  Facility facility = new Facility();
                  facility.setIntegratorEnabled(false);
                  loggedInInfo.setCurrentFacility(facility);
                  return loggedInInfo;
                });
  }
}
