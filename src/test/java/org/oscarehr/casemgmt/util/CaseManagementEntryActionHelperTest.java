package org.oscarehr.casemgmt.util;

import javax.servlet.http.HttpServletRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import oscar.oscarEncounter.pageUtil.EctSessionBean;

public class CaseManagementEntryActionHelperTest {

  private static final String DEFAULT_ENCOUNTER_TYPE = "video encounter with client";
  private static final String BEAN_ENCOUNTER_TYPE = "face to face encounter with client";
  private static final String REQUEST_ENCOUNTER_TYPE = "telephone encounter with client";

  @Mock
  HttpServletRequest request;

  @Mock
  EctSessionBean bean;

  @Before
  public void setup() {
    request = Mockito.mock(HttpServletRequest.class);
    bean = Mockito.mock(EctSessionBean.class);
    bean.encType = BEAN_ENCOUNTER_TYPE;
  }

  @Test
  public void testSetEncounterType_givenDefaultEncounter_setNoteEncounterTypeAsDefaultEncounterType() {
    Assert.assertEquals(DEFAULT_ENCOUNTER_TYPE,
        CaseManagementEntryActionHelper.setEncounterType(request, DEFAULT_ENCOUNTER_TYPE, bean));
  }

  @Test
  public void testSetEncounterType_givenRequestReason_setNoteEncounterTypeAsBeanEncounterType() {
    Mockito.when(request.getParameter("reason")).thenReturn(BEAN_ENCOUNTER_TYPE);

    Assert.assertEquals(BEAN_ENCOUNTER_TYPE,
        CaseManagementEntryActionHelper.setEncounterType(request, "", bean));
  }

  @Test
  public void testSetEncounterType_givenRequestEncounterType_setNoteEncounterTypeAsRequestEncounterType() {
    Mockito.when(request.getParameter("encType")).thenReturn(REQUEST_ENCOUNTER_TYPE);

    Assert.assertEquals(REQUEST_ENCOUNTER_TYPE,
        CaseManagementEntryActionHelper.setEncounterType(request, "", bean));
  }

  @Test
  public void testSetEncounterType_givenOnlyBeanEncounter_setNoteEncounterTypeAsBeanEncounterType() {
    Assert.assertEquals(BEAN_ENCOUNTER_TYPE,
        CaseManagementEntryActionHelper.setEncounterType(request, "", bean));
  }

  @Test
  public void testSetEncounterType_givenDefaultEncounterRequestEncounter_setNoteEncounterAsRequestEncounterType() {
    Mockito.when(request.getParameter("encType")).thenReturn(REQUEST_ENCOUNTER_TYPE);

    Assert.assertEquals(REQUEST_ENCOUNTER_TYPE,
        CaseManagementEntryActionHelper.setEncounterType(request, DEFAULT_ENCOUNTER_TYPE, bean));
  }

  @Test
  public void testSetEncounterType_givenRequestReasonBeanDefaultRequestEncounter_setNoteEncounterTypeAsBeanEncounterType() {
    Mockito.when(request.getParameter("encType")).thenReturn(REQUEST_ENCOUNTER_TYPE);
    Mockito.when(request.getParameter("reason")).thenReturn(BEAN_ENCOUNTER_TYPE);

    Assert.assertEquals(BEAN_ENCOUNTER_TYPE,
        CaseManagementEntryActionHelper.setEncounterType(request, DEFAULT_ENCOUNTER_TYPE, bean));
  }

  @Test
  public void testSetEncounterType_givenRequestReasonBeanDefault_setNoteEncounterTypeAsBeanEncounterType() {
    Mockito.when(request.getParameter("reason")).thenReturn(BEAN_ENCOUNTER_TYPE);

    Assert.assertEquals(BEAN_ENCOUNTER_TYPE,
        CaseManagementEntryActionHelper.setEncounterType(request, DEFAULT_ENCOUNTER_TYPE, bean));
  }

  @Test
  public void testSetEncounterType_givenRequestReasonBeanRequestEncounter_setNoteEncounterTypeAsBeanEncounterType() {
    Mockito.when(request.getParameter("reason")).thenReturn(BEAN_ENCOUNTER_TYPE);
    Mockito.when(request.getParameter("encType")).thenReturn(REQUEST_ENCOUNTER_TYPE);

    Assert.assertEquals(BEAN_ENCOUNTER_TYPE,
        CaseManagementEntryActionHelper.setEncounterType(request, "", bean));
  }

  @Test
  public void testSetEncounterType_givenBeanEncounterDefaultEncounterRequestEncounter_setNoteEncounterAsRequestEncounterType() {
    Mockito.when(request.getParameter("encType")).thenReturn(REQUEST_ENCOUNTER_TYPE);

    Assert.assertEquals(REQUEST_ENCOUNTER_TYPE,
        CaseManagementEntryActionHelper.setEncounterType(request, DEFAULT_ENCOUNTER_TYPE, bean));
  }

  @Test
  public void testSetEncounterType_givenBeanEncounterDefaultEncounter_setNoteEncounterTypeAsDefaultEncounterType() {
    Assert.assertEquals(DEFAULT_ENCOUNTER_TYPE,
        CaseManagementEntryActionHelper.setEncounterType(request, DEFAULT_ENCOUNTER_TYPE, bean));
  }

  @Test
  public void testSetEncounterType_givenBeanEncounterRequestEncounterType_setNoteEncounterTypeAsRequestEncounterType() {
    Mockito.when(request.getParameter("encType")).thenReturn(REQUEST_ENCOUNTER_TYPE);

    Assert.assertEquals(REQUEST_ENCOUNTER_TYPE,
        CaseManagementEntryActionHelper.setEncounterType(request, "", bean));
  }
}
