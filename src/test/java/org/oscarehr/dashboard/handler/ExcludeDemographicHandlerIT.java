/**
 * Copyright (c) 2013-2015. Department of Computer Science, University of Victoria. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Computer Science
 * LeadLab
 * University of Victoria
 * Victoria, Canada
 */

package org.oscarehr.dashboard.handler;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.Provider;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

public class ExcludeDemographicHandlerIT extends DaoTestFixtures {

		private final DemographicDao demographicDao = SpringUtils.getBean(DemographicDao.class);
		private ExcludeDemographicHandler excludeDemographicHandler;
		private String indicatorName;
		private final List<Integer> demoNos = new ArrayList<>();

    @BeforeEach
    public void before() throws Exception {
				super.beforeDemographic();
				super.beforeProvider();
				LoggedInInfo loggedInInfo = LoggedInInfo.getLoggedInInfoAsCurrentClassAndMethod();
        Provider provider = new Provider();
				String providerNo = "100";
				provider.setProviderNo(providerNo);
        for (int i = 2; i < 12; i++) {
					Demographic demographic = new Demographic();
        	EntityDataGenerator.generateTestDataForModelClass(demographic);
        	demographic.setDemographicNo(null);
        	demographic.setProvider(provider);
					demographic.setMyOscarUserName(null);
					demographic.setPortalUserId(null);
        	demographicDao.save(demographic);
        	demoNos.add(demographic.getDemographicNo());
        }
        loggedInInfo.setLoggedInProvider( provider );
        excludeDemographicHandler = new ExcludeDemographicHandler();
        excludeDemographicHandler.setLoggedinInfo(loggedInInfo);
    }

    @Test
    public void getDemoIds() {
    	indicatorName = "indicatorName_getDemoIds";
    	Assertions.assertEquals(0, excludeDemographicHandler.getDemoIds(indicatorName).size());
    }

    @Test
    public void getDemoExts() {
    	indicatorName = "indicatorName_getDemoExts";
    	Assertions.assertEquals(0, excludeDemographicHandler.getDemoExts(indicatorName).size());
    }

    @Test
    public void setDemoId() {
    	indicatorName = "myIndicatorName_setDemoId";
    	Assertions.assertEquals(10, demoNos.size());
    	excludeDemographicHandler.excludeDemoId(demoNos.get(5), indicatorName);
    	Assertions.assertEquals(1, excludeDemographicHandler.getDemoIds(indicatorName).size());
			Assertions.assertEquals(excludeDemographicHandler.getDemoIds(indicatorName).get(0),
					demoNos.get(5));
    }

    @Test
    public void setDemoIDList() {
    	indicatorName = "myIndicatorName_setDemoIDList";
    	excludeDemographicHandler.excludeDemoIds(demoNos, indicatorName);
    	List<Integer> demoIds = excludeDemographicHandler.getDemoIds(indicatorName);
    	Assertions.assertEquals(demoNos.size(), demoIds.size());
     	for (Integer el: demoNos) {
    		Assertions.assertTrue(demoIds.contains(el));
    	}
    }

    @Test
    public void unsetDemoIDList() {
    	indicatorName = "myIndicatorName_unsetDemoIDList";
    	excludeDemographicHandler.excludeDemoIds(demoNos, indicatorName);
    	List<Integer> demoIds = excludeDemographicHandler.getDemoIds(indicatorName);
    	Assertions.assertEquals(demoNos.size(), demoIds.size());
    	// also checks whether duplicated entries are removed
    	excludeDemographicHandler.unExcludeDemoIds(demoNos, indicatorName);
    	Assertions.assertEquals(0, excludeDemographicHandler.getDemoIds(indicatorName).size());
    }

    @Test
    public void setDemoIdJson() {
    	indicatorName = "myIndicatorName_setDemoIdJson";
    	String jsonStr = getJsonDemoNoStr(demoNos);
    	excludeDemographicHandler.excludeDemoIds(jsonStr, indicatorName);
    	List<Integer> demoIds = excludeDemographicHandler.getDemoIds(indicatorName);
    	for (int demoNo: demoNos) {
    		Assertions.assertTrue(demoIds.contains(demoNo));
    	}
    }

    @Test
    public void unsetDemoIdJson() {
    	indicatorName = "myIndicatorName_unsetDemoIdJson";
    	String jsonStr = getJsonDemoNoStr(demoNos);
    	excludeDemographicHandler.excludeDemoIds(jsonStr, indicatorName);
    	List<Integer> demoIds = excludeDemographicHandler.getDemoIds(indicatorName);
    	for (int demoNo: demoNos) {
    		Assertions.assertTrue(demoIds.contains(demoNo));
    	}
    	excludeDemographicHandler.unExcludeDemoIds(jsonStr, indicatorName);
    	Assertions.assertEquals(0, excludeDemographicHandler.getDemoIds(indicatorName).size());
    }

    private static String getJsonDemoNoStr(List<Integer> demoNums) {
        StringBuilder builder = new StringBuilder();
        for(Integer i : demoNums) {
            builder.append(i).append(",");
        }
        builder.deleteCharAt(builder.length()-1);  //remove trailing comma
			return builder.toString();
    }
}