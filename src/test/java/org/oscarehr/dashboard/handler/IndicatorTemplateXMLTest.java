/**
 * Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada
 */
package org.oscarehr.dashboard.handler;

import java.io.IOException;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.oscarehr.dashboard.query.DrillDownAction;
import org.oscarehr.dashboard.query.RangeInterface;
import org.oscarehr.dashboard.query.RangeInterface.Limit;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


@Disabled
public class IndicatorTemplateXMLTest {

	private static IndicatorTemplateXML indicatorTemplateXML;
	private static Document xmlDocument;
	
	@BeforeAll
	public static void setUpBeforeClass() {
		URL url = Thread.currentThread().getContextClassLoader().getResource("indicatorXMLTemplates/diabetes_hba1c_test.xml");
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setNamespaceAware(true);
	    
	    try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			xmlDocument = builder.parse( url.openStream() );
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
		indicatorTemplateXML = new IndicatorTemplateXML( xmlDocument );
	}
	
	@AfterAll
	public static void tearDownAfterClass() {
		indicatorTemplateXML = null;
		xmlDocument = null;
	}

	@Test
	public void testGetAuthor() {
		Assertions.assertEquals("Colcamex Resources Inc. Copyright 2016",
				indicatorTemplateXML.getAuthor());
	}

	@Test
	public void testGetCategory() {
		Assertions.assertEquals("CDM", indicatorTemplateXML.getCategory());
	}

	@Test
	public void testGetSubCategory() {
		Assertions.assertEquals("Diabetes", indicatorTemplateXML.getSubCategory());
	}

	@Test
	public void testGetFramework() {
		Assertions.assertEquals(
				"Based on and adapted from HQO's PCPM: Priority Measures for System and Practice Levels (Oct 2015)",
				indicatorTemplateXML.getFramework());
	}

	@Test
	public void testGetFrameworkVersion() {
		Assertions.assertEquals("10-01-2015", indicatorTemplateXML.getFrameworkVersion());
	}

	@Test
	public void testGetName() {
		Assertions.assertEquals("Diabetes with HbA1C Testing", indicatorTemplateXML.getName());
	}

	@Test
	public void testGetDefinition() {
		Assertions.assertEquals(
				"% of patients with diabetes aged 40 years and older who have had two or more HbA1C tests within the past 12 months",
				indicatorTemplateXML.getDefinition());
	}

	@Test
	public void testGetNotes() {
		Assertions.assertEquals(
				"This is a test template for the Diabetes with HbA1C Testing Indicator query",
				indicatorTemplateXML.getNotes());
	}
	
	@Test
	public void testGetIndicatorQueryVersion() {
		Assertions.assertEquals("07-15-2016", indicatorTemplateXML.getIndicatorQueryVersion());
	}

	@Test
	public void testGetIndicatorQuery() {
		Assertions.assertNotNull(indicatorTemplateXML.getIndicatorQuery());
	}

	@Test
	public void testGetIndicatorParameters() {
		Assertions.assertEquals("provider",
				indicatorTemplateXML.getIndicatorParameters().get(0).getId());
	}
	
	@Test
	public void testGetIndicatorParametersExcludedDemos() {
		Assertions.assertEquals("excludedPatient",
				indicatorTemplateXML.getIndicatorParameters().get(3).getId());
	}
	
	@Test
	public void testGetIndicatorParametersSize() {
		Assertions.assertEquals(4, indicatorTemplateXML.getIndicatorParameters().size());
	}

	@Test
	public void testGetDrillDownQuery() {
		Assertions.assertNotNull(indicatorTemplateXML.getDrilldownQuery());
	}
	
	@Test
	public void testGetDrilldownQueryVersion() {
		Assertions.assertEquals("07-20-2016", indicatorTemplateXML.getDrilldownQueryVersion());
	}
	
	@Test
	public void testGetDrilldownParameters() {
		Assertions.assertEquals("provider",
				indicatorTemplateXML.getDrilldownParameters("null").get(0).getId());
	}
	
	@Test
	public void testGetDrilldownParametersExcludedDemos() {
		Assertions.assertEquals("excludedPatient",
				indicatorTemplateXML.getDrilldownParameters("null").get(3).getId());
	}
	
	@Test
	public void testGetDrilldownParametersSize() {
		Assertions.assertEquals(4, indicatorTemplateXML.getDrilldownParameters("null").size());
	}
	
	@Test
	public void testGetDrilldownDisplayColumns() {
		Assertions.assertEquals("demographic",
				indicatorTemplateXML.getDrilldownDisplayColumns().get(0).getId());
	}
	
	@Test
	public void testGetDrilldownDisplayColumnsSize() {
		Assertions.assertEquals(5, indicatorTemplateXML.getDrilldownDisplayColumns().size());
	}
	
	@Test
	public void testGetDrilldownExportColumns() {
		Assertions.assertEquals("lastName",
				indicatorTemplateXML.getDrilldownExportColumns().get(2).getId());
	}
	
	@Test
	public void testGetDrilldownExportColumnsSize() {
		Assertions.assertEquals(5, indicatorTemplateXML.getDrilldownExportColumns().size());
	}
	
	@Test
	public void testGetIndicatorRanges() {
		Boolean verify = Boolean.FALSE;
		for( RangeInterface range : indicatorTemplateXML.getIndicatorRanges() ) {
			if( "a1c".equals( range.getId() ) ) {
				verify = Boolean.TRUE;
			}
		}
		Assertions.assertTrue(verify);
	}
	
	@Test
	public void testGetIndicatorRangesSize() {
		Assertions.assertEquals(5, indicatorTemplateXML.getIndicatorRanges().size());
	}
	
	@Test
	public void testGetIndicatorRangesUpperLimit() {
		Boolean verify = Boolean.FALSE;
		for( RangeInterface range : indicatorTemplateXML.getIndicatorRanges() ) {		
			if( Limit.RangeUpperLimit.name().equals( range.getClass().getSimpleName() ) ) {
				verify = Boolean.TRUE;
			}
		}
		Assertions.assertTrue(verify);
	}
	
	@Test
	public void testGetIndicatorRangesLowerLimit() {		
		Boolean verify = Boolean.FALSE;
		for( RangeInterface range : indicatorTemplateXML.getIndicatorRanges() ) {		
			if( Limit.RangeLowerLimit.name().equals( range.getClass().getSimpleName() ) ) {
				verify = Boolean.TRUE;
			}
		}
		Assertions.assertTrue(verify);
	}

	@Test
	public void testGetDrilldownActions() {
		Boolean verify = Boolean.FALSE;
		for( DrillDownAction action : indicatorTemplateXML.getDrilldownActions() ) {
			if( "dxUpdate".equals( action.getId() ) && "250".equals( action.getValue()) ) {
				verify = Boolean.TRUE;
			}
		}
		Assertions.assertTrue(verify);
	}
	
	@Test
	public void testGetDrilldownActionsSize() {
		Assertions.assertEquals(3, indicatorTemplateXML.getDrilldownActions().size());
	}

}
