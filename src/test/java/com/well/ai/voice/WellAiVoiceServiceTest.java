package com.well.ai.voice;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.oscarehr.common.dao.SystemPreferencesDao;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class WellAiVoiceServiceTest {
  @Mock
  SystemPreferencesDao systemPreferencesDao;

  WellAiVoiceService wellAiVoiceService;
  private AutoCloseable closeable;

  @Before
  public void before() throws Exception {
    closeable = MockitoAnnotations.openMocks(this);
    wellAiVoiceService = new WellAiVoiceService(systemPreferencesDao);
  }

  @After
  public void after() throws Exception {
    closeable.close();
  }

  private void settingUpMockBooleanPreference(String preferenceName, boolean expectCondition) {
    Mockito.doReturn(expectCondition)
        .when(systemPreferencesDao)
        .isReadBooleanPreferenceWithDefault(preferenceName, false);
  }

  private void settingUpMockStringPreference(String preferenceName, String expectString) {
    Mockito.doReturn(expectString)
        .when(systemPreferencesDao)
        .getPreferenceValueByName(preferenceName, "https://tali.ai/tali.js");
  }

  @Test
  public void testIsTaliEnabledFalse() {
    settingUpMockBooleanPreference(WellAiVoiceService.WELL_AI_VOICE_ENABLED, false);
    assertFalse(wellAiVoiceService.isWellAiVoiceEnabled());
  }

  @Test
  public void testIsTaliEnabledTrue() {
    settingUpMockBooleanPreference(WellAiVoiceService.WELL_AI_VOICE_ENABLED, true);
    assertTrue(wellAiVoiceService.isWellAiVoiceEnabled());
  }

  @Test
  public void givenPreference_whenGetWellAiVoiceUrl_thenReturnUrl() {
    settingUpMockStringPreference(WellAiVoiceService.WELL_AI_VOICE_URL, "https://tali.ai/tali.js");
    assertEquals(wellAiVoiceService.getWellAiVoiceUrl(), "https://tali.ai/tali.js");
  }
}