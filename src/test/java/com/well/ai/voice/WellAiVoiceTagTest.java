/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.ai.voice;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.oscarehr.managers.OktaManager;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertEquals;

public class WellAiVoiceTagTest {
  @Mock
  WellAiVoiceService wellAiVoiceService;
  @Mock
  OktaManager oktaManager;
  WellAiVoiceTag wellAiVoiceTag;

  @Before
  public void before() throws Exception {
    MockitoAnnotations.openMocks(this);
    wellAiVoiceTag = new WellAiVoiceTag();
    ReflectionTestUtils.setField(wellAiVoiceTag, "wellAiVoiceService", wellAiVoiceService);
    ReflectionTestUtils.setField(wellAiVoiceTag, "oktaManager", oktaManager);
  }

  private void wellAiVoiceServiceEnable(boolean statement) {
    Mockito.doReturn(statement)
        .when(wellAiVoiceService)
        .isWellAiVoiceEnabled();
  }

  private void oktaEnable(boolean statement) {
    Mockito.doReturn(statement)
        .when(oktaManager)
        .isOktaEnabled();
  }

  private void setOktaId(String id) {
    Mockito.doReturn(id)
        .when(oktaManager)
        .getClientId();
  }

  private void setWellAiVoiceUrl(String expectedUrl) {
    Mockito.doReturn(expectedUrl)
        .when(wellAiVoiceService)
        .getWellAiVoiceUrl();
  }

  @Test
  public void testGetIdentifierWellAiVoiceDisableCase() {
    wellAiVoiceServiceEnable(false);
    oktaEnable(true);
    setOktaId("1234567890");
    assertEquals(wellAiVoiceTag.getIdentifier(), "");
  }
  @Test
  public void testGetIdentifierOktaDisableCase() {
    wellAiVoiceServiceEnable(true);
    oktaEnable(false);
    setOktaId("1234567890");
    setWellAiVoiceUrl("https://tali.ai/tali.js");
    assertEquals(wellAiVoiceTag.getIdentifier(), String.format(WellAiVoiceTag.FORMAT,
        "https://tali.ai/tali.js", ""
    ));
  }

  @Test
  public void testGetIdentifierOktaEnableCase() {
    wellAiVoiceServiceEnable(true);
    oktaEnable(true);
    setOktaId("1234567890");
    setWellAiVoiceUrl("https://tali.ai/tali.js");
    assertEquals(wellAiVoiceTag.getIdentifier(), String.format(WellAiVoiceTag.FORMAT,
        "https://tali.ai/tali.js", "1234567890"
    ));
  }

  @Test
  public void givenUrlPreference_whenGetIdentifier_thenReturnUrl() {
    wellAiVoiceServiceEnable(true);
    oktaEnable(true);
    setOktaId("1234567890");
    setWellAiVoiceUrl("https://tali.ai/tali-staging.js");
    assertEquals(wellAiVoiceTag.getIdentifier(), String.format(WellAiVoiceTag.FORMAT,
        "https://tali.ai/tali-staging.js", "1234567890"
    ));
  }
}
