/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.util;

import com.well.integration.config.generators.ConfigGenerator;
import com.well.integration.config.generators.Generator;
import com.well.integration.config.generators.InsigGenerator;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

public class ClassFactoryTest {

    private ClassFactory<Generator> classFactory;

    @Before
    public void before() {
        classFactory = new ClassFactory<Generator>(
                "com.well.integration.config.generators.", "Generator",
                ConfigGenerator.class);
    }

    @Test
    public void testNewInstance() {
        assertThat(classFactory.newInstance("Insig"), instanceOf(InsigGenerator.class));
    }

    @Test
    public void testNewInstanceReturnsDefaultWhenNotFound() {
        assertThat(classFactory.newInstance("NotFound"), instanceOf(ConfigGenerator.class));
    }

    @Test
    public void testGetInstanceClass() {
        assertEquals(classFactory.getInstanceClass("Insig"), InsigGenerator.class);
    }

    @Test
    public void testGetInstanceClassReturnsDefaultWhenNotFound() {
        assertEquals(classFactory.getInstanceClass("NotFound"), ConfigGenerator.class);
    }

    @Test
    public void testGetDefaultInstance() {
        assertThat(classFactory.getDefaultInstance(), instanceOf(ConfigGenerator.class));
    }
}
