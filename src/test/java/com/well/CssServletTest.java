package com.well;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import oscar.OscarProperties;

public class CssServletTest {

  private static final String DOCUMENT_PATH_KEY = "BASE_DOCUMENT_DIR";
  private static final String DOCUMENT_PATH = Paths.get("srv", "OscarDocument").toString();

  private HttpServletRequest mockRequest;
  private HttpServletResponse mockResponse;

  private CssServlet servlet;

  private MockedStatic<OscarProperties> oscarPropertiesStatic;

  private String documentDir;
  private String reqPath;
  private Path cssRootPath;
  private Path cssFilePath;
  private File cssFile;

  @BeforeEach
  public void before() {
    mockRequest = mock(HttpServletRequest.class);
    mockResponse = mock(HttpServletResponse.class);
    servlet = spy(new CssServlet());

    OscarProperties oscarProperties = mock(OscarProperties.class);
    oscarPropertiesStatic = mockStatic(OscarProperties.class);
    oscarPropertiesStatic.when(OscarProperties::getInstance).thenReturn(oscarProperties);
    when(oscarProperties.getProperty(eq(DOCUMENT_PATH_KEY))).thenReturn(DOCUMENT_PATH);
  }

  @AfterEach
  public void after() {
    oscarPropertiesStatic.close();
  }

  @Test
  public void testDoGetHandlesErrors() throws Exception {
    when(mockRequest.getPathInfo()).thenReturn("/test.test");
    servlet.doGet(mockRequest, mockResponse);
    verify(servlet, never()).setHeaders(any(HttpServletResponse.class), anyString());
    verify(mockResponse, never()).getOutputStream();
  }

  @Test
  public void testDoGetHappyPath() throws Exception {
    OutputStream outputStream = new ByteArrayOutputStream();
    InputStream inputStream = new ByteArrayInputStream("1234".getBytes(StandardCharsets.UTF_8));
    setupFile("test.css");
    doReturn(false).when(servlet)
        .hasError(mockResponse, reqPath, cssRootPath, cssFilePath, cssFile);
    when(mockRequest.getPathInfo()).thenReturn("/test.css");
    doReturn(outputStream).when(servlet).getOutputStream(mockResponse);
    doReturn(inputStream).when(servlet).getCssStream(cssFile);
    servlet.doGet(mockRequest, mockResponse);
    verify(mockResponse, never()).sendError(anyInt());
    verify(servlet).setHeaders(mockResponse, "test.css");
    assertEquals("1234", outputStream.toString());
  }

  @Test
  public void givenDocumentDirProperty_whenGetDocumentDir_thenPropertyUsed() {
    assertEquals(DOCUMENT_PATH, servlet.getDocumentDir());
  }

  @Test
  public void testGetPathFromRequest() {
    when(mockRequest.getPathInfo()).thenReturn("test");
    assertEquals("test", servlet.getPath(mockRequest));
  }

  @Test
  public void testGetPathFromRequestTrimsLeadingSlash() {
    when(mockRequest.getPathInfo()).thenReturn("/test");
    assertEquals("test", servlet.getPath(mockRequest));
  }

  @Test
  public void givenDocumentDir_whenGetCssRootPath_thenCorrectPath() {
    setupFile("");
    String expected = Paths.get(DOCUMENT_PATH, "css").toString();
    assertEquals(expected, servlet.getCssRootPath(documentDir).toString());
  }

  @Test
  public void givenDocumentDir_whenGetCssFilePath_thenCorrectPath() {
    setupFile("");
    String expected = Paths.get(DOCUMENT_PATH, "css", "test.css").toFile().getAbsolutePath();
    assertEquals(expected, servlet.getCssFilePath(cssRootPath, "test.css").toString());
  }

  @Test
  public void testHasErrorMissingFile() throws Exception {
    when(mockRequest.getPathInfo()).thenReturn("/");
    setupFile("");
    assertTrue(servlet.hasError(
        mockResponse,
        reqPath,
        cssRootPath,
        cssFilePath,
        cssFile
    ));
    verify(mockResponse).sendError(HttpServletResponse.SC_BAD_REQUEST);
  }

  @Test
  public void testHasErrorDirectoryTraversalAttack() throws Exception {
    when(mockRequest.getPathInfo()).thenReturn("/");
    setupFile("../traversal.css");
    assertTrue(servlet.hasError(
        mockResponse,
        reqPath,
        cssRootPath,
        cssFilePath,
        cssFile
    ));
    verify(mockResponse).sendError(HttpServletResponse.SC_BAD_REQUEST);
  }

  @Test
  public void testHasErrorInvalidFileExtension() throws Exception {
    when(mockRequest.getPathInfo()).thenReturn("/");
    setupFile("text.txt");
    assertTrue(servlet.hasError(
        mockResponse,
        reqPath,
        cssRootPath,
        cssFilePath,
        cssFile
    ));
    verify(mockResponse).sendError(HttpServletResponse.SC_BAD_REQUEST);
  }

  @Test
  public void givenInvalidFile_whenHasError_thenNotFoundResponse() throws Exception {
    doReturn(Paths.get(DOCUMENT_PATH, "css"))
        .when(servlet).getCssRootPath(anyString());
    doReturn(Paths.get(DOCUMENT_PATH, "css", "test.css"))
        .when(servlet).getCssFilePath(any(Path.class), anyString());

    when(mockRequest.getPathInfo()).thenReturn("/");
    setupFile("test.css");
    assertTrue(servlet.hasError(
        mockResponse,
        reqPath,
        cssRootPath,
        cssFilePath,
        cssFile
    ));
    verify(mockResponse).sendError(HttpServletResponse.SC_NOT_FOUND);
  }

  @Test
  public void givenHappyPath_whenHasError_thenNoErrors() throws Exception {
    doReturn(Paths.get(DOCUMENT_PATH, "css"))
        .when(servlet).getCssRootPath(anyString());
    doReturn(Paths.get(DOCUMENT_PATH, "css", "test.css"))
        .when(servlet).getCssFilePath(any(Path.class), anyString());

    setupFile("test.css");
    cssFile = spy(cssFile);
    when(cssFile.exists()).thenReturn(true);
    when(mockRequest.getPathInfo()).thenReturn("/test.css");
    assertFalse(servlet.hasError(
        mockResponse,
        reqPath,
        cssRootPath,
        cssFilePath,
        cssFile
    ));
    verify(mockResponse, never()).sendError(anyInt());
  }

  @Test
  public void testSetHeadersMimeType() {
    servlet.setHeaders(mockResponse, "test");
    verify(mockResponse).setContentType("text/css");
  }

  @Test
  public void testSetHeadersFile() {
    servlet.setHeaders(mockResponse, "test");
    verify(mockResponse).setHeader(
        "Content-Disposition",
        "filename=\"test\"");
  }

  @Test
  public void testSetHeadersFileWithQuotes() {
    servlet.setHeaders(mockResponse, "t\"est");
    verify(mockResponse).setHeader(
        "Content-Disposition",
        "filename=\"test\"");
  }

  @Test
  public void testSetHeadersMimeType4() {
    servlet.setHeaders(mockResponse, "test");
    verify(mockResponse).setContentType("text/css");
  }

  private void setupFile(String file) {
    documentDir = servlet.getDocumentDir();
    reqPath = file;
    cssRootPath = servlet.getCssRootPath(documentDir);
    cssFilePath = servlet.getCssFilePath(cssRootPath, reqPath);
    cssFile = cssFilePath.toFile();
  }
}
