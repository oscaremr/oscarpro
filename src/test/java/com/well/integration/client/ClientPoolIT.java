package com.well.integration.client;

import com.well.integration.Integration;
import com.well.integration.IntegrationTestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.DaoTestFixtures;
import utils.OscarPropertiesUtil;

public class ClientPoolIT extends DaoTestFixtures {
    ClientPool manager;
    Integration integration;


    @BeforeAll
    public static void beforeClass() {
        OscarPropertiesUtil.getOscarProperties();
        IntegrationTestUtils.setupIntegration();
    }

    @BeforeEach
    public void before() {
        manager = new ClientPool();
        integration = IntegrationTestUtils.getIntegration();
    }

    @Test
    public void testGetClientEmpty() {
        Assertions.assertNotNull(manager.clients);
        Assertions.assertEquals(0, manager.clients.size());
    }

    @Test
    public void testGetClientAddOne() {
        Client client = manager.getClient(integration);
        Assertions.assertNotNull(client);
        Assertions.assertEquals(integration, client.integration);
        Assertions.assertEquals(1, manager.clients.size());
        client = manager.getClient(integration);
        Assertions.assertEquals(integration, client.integration);
        Assertions.assertEquals(1, manager.clients.size());
    }

    @Test
    public void testGetClientAlreadyHasOne() {
        IntegrationTestUtils.setupIntegration("TEST");
        Integration testIntegration = IntegrationTestUtils.getIntegration("TEST");
        manager.getClient(testIntegration);
        Client client = manager.getClient(integration);
        Assertions.assertEquals(integration, client.integration);
        Assertions.assertEquals(2, manager.clients.size());
        client = manager.getClient(testIntegration);
        Assertions.assertEquals(testIntegration, client.integration);
    }
}
