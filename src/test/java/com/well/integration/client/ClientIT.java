/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.client;

import static com.well.integration.IntegrationTestUtils.API_KEY;
import static com.well.integration.IntegrationTestUtils.API_KEY_NAME;
import static com.well.integration.IntegrationTestUtils.CLIENT_ID;
import static com.well.integration.IntegrationTestUtils.CLIENT_ID_NAME;
import static com.well.integration.IntegrationTestUtils.INTEGRATION_MODULE;
import static com.well.integration.IntegrationTestUtils.getRequestMock;
import static com.well.integration.IntegrationTestUtils.setupIntegration;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.well.integration.Integration;
import com.well.integration.IntegrationDao;
import com.well.integration.IntegrationException;
import com.well.integration.config.ConfigPropertyKey;
import com.well.integration.provision.ProvisionRequest;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HttpContext;
import org.junit.Before;
import org.junit.Test;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.util.SpringUtils;
import org.springframework.http.HttpMethod;
import oscar.OscarProperties;

public class ClientIT extends DaoTestFixtures {

    Client client;
    private ConfigPropertyKey keys;
    private IntegrationDao integrationDao = SpringUtils.getBean(IntegrationDao.class);
    private Integration integration;


    @Before
    public void before() {
        setupIntegration();
        integration = integrationDao.getIntegration(INTEGRATION_MODULE);
        keys = integration.getKeys();
        client = new Client(integration);
    }

    @Test
    public void testExecuteWithEntity() throws Exception {
        // Setup stubs required to execute client
        client = spy(client);
        client.client = mock(CloseableHttpClient.class);
        Request clientRequest = getRequestMock(INTEGRATION_MODULE);
        HttpRequestBase httpRequestBase = mock(HttpEntityEnclosingRequestBase.class);
        when(client.getRequestBase(clientRequest)).thenReturn(httpRequestBase);
        when(client.client.execute(any(HttpHost.class), any(HttpRequest.class), any(HttpContext.class)))
                .thenReturn(null);

        // Test that execution calls appropriate methods
        client.execute(clientRequest);
        verify(client, times(1)).getRequestBase(clientRequest);
        verify(client, times(1)).setCredentials(httpRequestBase);
        verify(client, times(1)).getURI(clientRequest);
        verify(httpRequestBase).addHeader("Content-Type", "application/json");
        verify(httpRequestBase, times(1)).setURI(any(URI.class));
        verify((HttpEntityEnclosingRequestBase)httpRequestBase, times(1))
                .setEntity(any(StringEntity.class));
        verify(client.client, times(1)).execute(any(HttpHost.class), any(HttpRequest.class), any(HttpContext.class));
    }

    @Test
    public void testCheckConfigMissingApiKey() {
        try {
            client.apiKey = null;
            client.checkConfig();
        }
        catch (IntegrationException e) {
            assertEquals("Missing API key.", e.getMessage());
        }
    }

    @Test
    public void testCheckConfigMissingClientId() {
        try {
            client.clientId = null;
            client.checkConfig();
        }
        catch (IntegrationException e) {
            assertEquals("Missing client ID.", e.getMessage());
        }
    }

    @Test
    public void testCheckConfigBadURL() {
        OscarProperties p = OscarProperties.getInstance();
        String originalUrl = p.getProperty(keys.BASE_URL);
        p.setProperty(keys.BASE_URL, "");
        try {
            client.checkConfig();
        }
        catch (IntegrationException e) {
            assertEquals("Invalid client configuration.", e.getMessage());
        }
        finally {
            p.setProperty(keys.BASE_URL, originalUrl);
        }
    }

    @Test
    public void testCheckConfigMissingClient() {
        try {
            client.client = null;
            client.checkConfig();
        }
        catch (IntegrationException e) {
            assertEquals("Http client is unavailable.", e.getMessage());
        }
    }

    @Test
    public void testCheckConfigMissingConnectionManager() {
        try {
            client.connectionManager = null;
            client.checkConfig();
        }
        catch (IntegrationException e) {
            assertEquals("Client connection manager is unavailable.", e.getMessage());
        }
    }

    @Test
    public void testGetIntegration() {
        Client client = new Client(integration);
        assertEquals(integration, client.getIntegration());
    }

    @Test
    public void testGetRequestBaseDelete() {
        Request request = getRequestMock(INTEGRATION_MODULE);
        request.method = HttpMethod.DELETE;
        assertThat(client.getRequestBase(request), instanceOf(HttpDelete.class));
    }

    @Test
    public void testGetRequestBaseGet() {
        Request request = getRequestMock(INTEGRATION_MODULE);
        request.method = HttpMethod.GET;
        assertThat(client.getRequestBase(request), instanceOf(HttpGet.class));
    }

    @Test
    public void testGetRequestBasePost() {
        Request request = getRequestMock(INTEGRATION_MODULE);
        request.method = HttpMethod.POST;
        assertThat(client.getRequestBase(request), instanceOf(HttpPost.class));
    }

    @Test
    public void testGetRequestBasePut() {
        Request request = getRequestMock(INTEGRATION_MODULE);
        request.method = HttpMethod.PUT;
        assertThat(client.getRequestBase(request), instanceOf(HttpPut.class));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetRequestBaseNull() {
        Request request = getRequestMock(INTEGRATION_MODULE);
        request.method = HttpMethod.TRACE;
        client.getRequestBase(request);
    }

    @Test
    public void testGetURIBaseURLWithPath() throws URISyntaxException {
        OscarProperties.getInstance().setProperty(INTEGRATION_MODULE + ".integration.base_url", "http://localhost:4010/api/v1");
        client = new Client(integration);
        ProvisionRequest request = ProvisionRequest.checkHealth(integration);
        URI uri = client.getURI(request);
        assertEquals("http", uri.getScheme());
        assertEquals("localhost", uri.getHost());
        assertEquals(4010, uri.getPort());
        assertEquals("/api/v1/provision", uri.getPath());
        assertEquals("healthCheck=true", uri.getQuery());
    }

    @Test
    public void testGetURIBaseURLWithoutPath() throws URISyntaxException {
        OscarProperties.getInstance().setProperty(INTEGRATION_MODULE + ".integration.base_url", "http://localhost:4010/");
        client = new Client(integration);
        ProvisionRequest request = ProvisionRequest.checkHealth(integration);
        URI uri = client.getURI(request);
        assertEquals("http", uri.getScheme());
        assertEquals("localhost", uri.getHost());
        assertEquals(4010, uri.getPort());
        assertEquals("/provision", uri.getPath());
        assertEquals("healthCheck=true", uri.getQuery());
    }

    @Test
    public void testGetURIBaseURLWithoutPort() throws URISyntaxException {
        OscarProperties.getInstance().setProperty(INTEGRATION_MODULE + ".integration.base_url", "http://localhost/");
        client = new Client(integration);
        ProvisionRequest request = ProvisionRequest.checkHealth(integration);
        URI uri = client.getURI(request);
        assertEquals("http", uri.getScheme());
        assertEquals("localhost", uri.getHost());
        assertEquals(-1, uri.getPort());
        assertEquals("/provision", uri.getPath());
        assertEquals("healthCheck=true", uri.getQuery());
    }



    @Test
    public void testSetCredentials() throws Exception {
        HttpRequest request = mock(HttpRequest.class);
        client.apiKey = API_KEY;
        client.clientId = CLIENT_ID;
        client = spy(client);
        client.setCredentials(request);
        verify(request).setHeaders(any(Header[].class));
        verify(client, times(1)).getHeaders();
    }

    @Test
    public void testGetHeaders() {
        client.apiKey = API_KEY;
        client.clientId = CLIENT_ID;
        Header[] expectedHeaders = {
                new BasicHeader(API_KEY_NAME, API_KEY),
                new BasicHeader(CLIENT_ID_NAME, CLIENT_ID)
        };
        Header[] actualHeaders = client.getHeaders();
        assertEquals(2, actualHeaders.length);
        testHeader(expectedHeaders[0], actualHeaders[0]);
        testHeader(expectedHeaders[1], actualHeaders[1]);
    }

    private void testHeader(Header expected, Header actual) {
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getValue(), actual.getValue());
    }
}