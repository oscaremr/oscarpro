/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.client;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ResponseTest {

    @Test
    public void testIsSuccess() {
        assertSuccess(true, 200);
        assertSuccess(true, 206);
        assertSuccess(false, 400);
        assertSuccess(false, 401);
        assertSuccess(false, 404);
        assertSuccess(false, 500);
    }

    @Test
    public void testHasError() {
        assertError(false, 200);
        assertError(false, 206);
        assertError(true, 400);
        assertError(true, 401);
        assertError(true, 404);
        assertError(true, 500);
    }

    protected void assertSuccess(boolean expected, int status) {
        Response response = getResponse(status);
        if (expected) {
            assertTrue(response.isSuccess());
        } else {
            assertFalse(response.isSuccess());
        }
    }

    protected void assertError(boolean expected, int status) {
        Response response = getResponse(status);
        if (expected) {
            assertTrue(response.hasError());
        } else {
            assertFalse(response.hasError());
        }
    }

    protected Response getResponse(int status) {
        return new Response(status);
    }
}