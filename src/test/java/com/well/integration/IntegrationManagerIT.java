/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration;

import static com.well.integration.IntegrationTestUtils.API_KEY;
import static com.well.integration.IntegrationTestUtils.API_KEY_NAME;
import static com.well.integration.IntegrationTestUtils.CLIENT_ID;
import static com.well.integration.IntegrationTestUtils.CLIENT_ID_NAME;
import static com.well.integration.IntegrationTestUtils.INTEGRATION_MODULE;
import static com.well.integration.IntegrationTestUtils.createTestProvider;
import static com.well.integration.IntegrationTestUtils.createTestServiceClient;
import static com.well.integration.IntegrationTestUtils.getCreateRequest;
import static com.well.integration.IntegrationTestUtils.isE2EEnabled;
import static com.well.integration.IntegrationTestUtils.restoreIntegrationTables;
import static com.well.integration.IntegrationTestUtils.setupIntegration;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import com.well.integration.client.Client;
import com.well.integration.config.ConfigPropertyKey;
import com.well.integration.config.GeneratorFactory;
import com.well.integration.config.parameters.ConfigParams;
import com.well.integration.config.parameters.InsigConfigParams;
import com.well.integration.provision.ProvisionRequest;
import com.well.integration.provision.ProvisionResponse;
import com.well.integration.provision.handlers.InsigHandler;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.MockedStatic.Verification;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.ServiceClient;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import oscar.log.LogAction;

public class IntegrationManagerIT extends DaoTestFixtures {

    IntegrationManager manager = SpringUtils.getBean(IntegrationManager.class);
    IntegrationDao integrationDao = SpringUtils.getBean(IntegrationDao.class);
    SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);

    Integration integration;
    ConfigParams createParams;
    private ConfigPropertyKey keys;
    static boolean e2eEnabled;

    @BeforeClass
    public static void beforeClass() throws Exception {
        restoreIntegrationTables();
        e2eEnabled = isE2EEnabled();
    }

    @Before
    public void before() throws Exception {

        setupIntegration();

        createParams = getCreateRequest(INTEGRATION_MODULE);
        integration = integrationDao.getIntegration(INTEGRATION_MODULE);
        keys = integration.getKeys();

    }

    @Test
    public void testSaveConfigHappyPath() throws Exception {
        if (!e2eEnabled) { return; }
        try (MockedStatic<LogAction> logAction = mockStatic(LogAction.class);
             MockedStatic<ProvisionRequest> mockedInsigProvisionRequest = mockStatic(ProvisionRequest.class)) {
            Client client = new Client(integration);
            when(ProvisionRequest.checkConnection(integration)).thenCallRealMethod();
            when(ProvisionRequest.checkHealth(integration)).thenCallRealMethod();
            when(ProvisionRequest.sendConfiguration(any(InsigHandler.class))).thenCallRealMethod();
            ProvisionResponse provisionResponse = manager.saveConfig(
                    "-1",
                    "testIP",
                    createParams,
                    false,
                    client);
            assertNotNull(provisionResponse);
            assertTrue(provisionResponse.isSuccess());
            assertTrue(provisionResponse.isProvisioned());
            mockedInsigProvisionRequest.verify(new Verification() {
                @Override
                public void apply() throws Throwable {
                    ProvisionRequest.checkConnection(integration);
                }
            });
            mockedInsigProvisionRequest.verify(new Verification() {
                @Override
                public void apply() throws Throwable {
                    ProvisionRequest.sendConfiguration(any(InsigHandler.class));
                }
            });
            mockedInsigProvisionRequest.verify(new Verification() {
                @Override
                public void apply() throws Throwable {
                    ProvisionRequest.checkHealth(integration);
                }
            });
            testLogMessage(logAction, "Successfully configured.");
        }
    }

    @Test(expected = IntegrationException.class)
    public void testSaveConfigNotEnabled() throws Exception {
        try (MockedStatic<LogAction> logAction = mockStatic(LogAction.class)) {
            systemPreferencesDao.mergeOrPersist(keys.ENABLED, "false");
            Client client = new Client(integration);
            manager.saveConfig("-1", "testIP", createParams, false, client);
            testLogMessage(logAction, "Insig integration is not enabled.");
        }
    }

    @Test(expected = IntegrationException.class)
    public void testSaveConfigNoApiKey() throws Exception {
        try (MockedStatic<LogAction> logAction = mockStatic(LogAction.class)) {
            OscarProperties.getInstance().setProperty(keys.API_KEY, "");
            Client client = new Client(integration);
            manager.saveConfig("-1", "testIP",  createParams, false, client);
            testLogMessage(logAction, "Insig API key is not configured in properties file.");
        }
    }

    @Test(expected = IntegrationException.class)
    public void testSaveConfigNoClientId() throws Exception {
        try (MockedStatic<LogAction> logAction = mockStatic(LogAction.class)) {
            OscarProperties.getInstance().setProperty(keys.CLIENT_ID, "");
            Client client = new Client(integration);
            manager.saveConfig("-1", "testIP", createParams, false, client);
            testLogMessage(logAction, "Insig client ID is not configured in properties file.");
        }
    }

    @Test(expected = IntegrationException.class)
    public void testSaveConfigNotAuthorized() throws Exception {
        try (MockedStatic<LogAction> logAction = mockStatic(LogAction.class)) {
            Client client = spy(new Client(integration));
            when(client.getHeaders()).thenReturn(new Header[] {
                    new BasicHeader("Prefer", "code=401"),
                    new BasicHeader(API_KEY_NAME, API_KEY),
                    new BasicHeader(CLIENT_ID_NAME, CLIENT_ID),
            });
            manager.saveConfig("-1", "testIP", createParams, false, client);
            testLogMessage(logAction, "Insig rejected system credentials.");
        }
    }

    @Test(expected = IntegrationException.class)
    public void testSaveConfigError() throws Exception {
        try (MockedStatic<LogAction> logAction = mockStatic(LogAction.class)) {
            Client client = spy(new Client(integration));
            when(client.getHeaders()).thenReturn(new Header[] {
                    new BasicHeader("Prefer", "code=500"),
                    new BasicHeader(API_KEY_NAME, API_KEY),
                    new BasicHeader(CLIENT_ID_NAME, CLIENT_ID),
            });
            manager.saveConfig("-1", "testIP", createParams, false, client);
            testLogMessage(logAction, "Unexpected error encountered.");
        }
    }

    @Test(expected = IntegrationException.class)
    public void testSaveConfigFailedConfiguration() throws Exception {
        try (MockedStatic<LogAction> logAction = mockStatic(LogAction.class)) {
            Client client = spy(new Client(integration));
            when(client.getHeaders()).thenReturn(new Header[] {
                    new BasicHeader("Prefer", "code=404"),
                    new BasicHeader(API_KEY_NAME, API_KEY),
                    new BasicHeader(CLIENT_ID_NAME, CLIENT_ID),
            });
            manager.saveConfig("-1", "testIP", createParams, false, client);
            testLogMessage(logAction, "Failed to configure credentials.");
        }
    }

    private void testLogMessage(MockedStatic<LogAction> mocked, final String message) {
        mocked.verify(new Verification() {
            @Override
            public void apply() throws Throwable {
                LogAction.addLog(
                        "-1",
                        "Integration Admin",
                        message,
                        "",
                        "testIP");
            }
        });
    }

    @Test
    public void testGenerateConfigRequestExistingProvider() throws Exception {
        Provider p = createTestProvider("Insig", "Insig");
        InsigConfigParams config = generateParameters();
        assertEquals(p.getProviderNo(), config.getSoapProviderNo());
    }

    @Test
    public void testGenerateConfigRequestCreateProvider() throws Exception {
        SchemaUtils.restoreTable("provider");
        InsigConfigParams config = generateParameters();
        assertEquals("create", config.getSoapProviderNo());
    }

    @Test
    public void testGenerateConfigRequestExistingClient() throws Exception {
        ServiceClient c = createTestServiceClient("Insig");
        InsigConfigParams config = generateParameters();
        assertEquals(c.getId().toString(), config.getRestClientId());
    }

    @Test
    public void testGenerateConfigRequestCreateClient() throws Exception {
        SchemaUtils.restoreTable("ServiceClient");
        InsigConfigParams config = generateParameters();
        assertEquals("create", config.getRestClientId());
    }

    @Test
    public void testGenerateConfigRequestThirdPartyLinksEnabled() throws Exception {
        systemPreferencesDao.mergeOrPersist("schedule_tp_link_enabled", "true");
        systemPreferencesDao.mergeOrPersist("schedule_tp_link_type", "NOT VIRTUAL");
        systemPreferencesDao.mergeOrPersist("schedule_tp_link_display", "NOT VC+");
        InsigConfigParams config = generateParameters();
        assertEquals(true, config.isTpLinkEnabled());
        assertEquals("NOT VIRTUAL", config.getTpLinkType());
        assertEquals("NOT VC+", config.getTpLinkDisplay());
    }

    @Test
    public void testGenerateConfigRequestThirdPartLinksDisabled() throws Exception {
        systemPreferencesDao.mergeOrPersist("schedule_tp_link_enabled", "false");
        systemPreferencesDao.mergeOrPersist("schedule_tp_link_type", "");
        systemPreferencesDao.mergeOrPersist("schedule_tp_link_display", "");
        InsigConfigParams config = generateParameters();
        assertEquals(true, config.isTpLinkEnabled());
        assertEquals("Virtual", config.getTpLinkType());
        assertEquals("VC+", config.getTpLinkDisplay());
    }

    private InsigConfigParams generateParameters() {
        return (InsigConfigParams) GeneratorFactory
                .getGenerator(integration)
                .generateConfig(manager, manager.integrationDao.getIntegration(INTEGRATION_MODULE));
    }
}
