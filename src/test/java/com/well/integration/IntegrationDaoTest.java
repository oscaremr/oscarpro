/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration;

import static com.well.integration.IntegrationTestUtils.INTEGRATION_MODULE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.util.SpringUtils;

public class IntegrationDaoTest extends DaoTestFixtures {

	IntegrationDao integrationDao = SpringUtils.getBean(IntegrationDao.class);
	SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);


	@BeforeAll
	public static void beforeClass() throws Exception {
		IntegrationTestUtils.restoreIntegrationTables();
	}

	@BeforeEach
	public void before() {
		IntegrationTestUtils.setupIntegration();
	}

	@Test
	public void testInitializeIntegrationsSingle() {
		Integration integration = new Integration("test", null, null);
		integrationDao.setIntegrationModules("test");
		Map<String, Integration> integrationMap = integrationDao.initializeIntegrations();
		assertEquals(1, integrationMap.size());
		assertEquals(integration, integrationMap.get("test"));
	}

	@Test
	public void testInitializeIntegrationsMultiple() {
		Integration integration1 = new Integration("test1", null, null);
		Integration integration2 = new Integration("test2", null, null);
		integrationDao.setIntegrationModules("test1,test2");
		Map<String, Integration> integrationMap = integrationDao.initializeIntegrations();
		assertEquals(2, integrationMap.size());
		assertEquals(integration1, integrationMap.get("test1"));
		assertEquals(integration2, integrationMap.get("test2"));
	}

	@Test
	public void testInitializeIntegrationsEmpty() {
		integrationDao.setIntegrationModules("");
		assertEquals(0, integrationDao.initializeIntegrations().size());
	}

	@Test
	public void testInitializeIntegrationsMultipleEmpty() {
		integrationDao.setIntegrationModules(",,,,");
		assertEquals(0, integrationDao.initializeIntegrations().size());
	}


	@Test
	public void testGetEnabled() {
		List<Integration> enabled = integrationDao.getEnabled();
		assertEquals(1, enabled.size());
		systemPreferencesDao.mergeOrPersist(enabled.get(0).getKeys().ENABLED, "false");
		enabled = integrationDao.getEnabled();
		assertEquals(0, enabled.size());
	}

	@Test
	public void testGetIntegrations() {
		Integration integration = new Integration(INTEGRATION_MODULE, null, null);
		List<Integration> integrations = integrationDao.getIntegrations();
		assertEquals(1, integrations.size());
		assertTrue(integrations.contains(integration));
	}

	@Test
	public void testGetIntegration() {
		Integration integration = new Integration("insig", null, null);
		assertEquals(integration, integrationDao.getIntegration("insig"));
		assertEquals(integration, integrationDao.getIntegration("INSIG"));
		assertEquals(integration, integrationDao.getIntegration("Insig"));
	}

	@Test
	public void testGetIntegrationMissing() {
		assertNull(integrationDao.getIntegration("missing"));
	}

	@Test
	public void testGetIntegrationModules() {
		assertEquals(INTEGRATION_MODULE, integrationDao.getIntegrationModules());
	}

	@Test
	public void testSetIntegrationModules() {
		integrationDao.setIntegrationModules("1,2,3,4");
		assertEquals("1,2,3,4", integrationDao.getIntegrationModules());
	}

	@Test
	public void testHasScheduleOptions() {
		Integration integration = new Integration("insig", null, null);
		assertTrue(integrationDao.hasScheduleOptions(integration));
		integration = new Integration("NotInsig", null, null);
		assertFalse(integrationDao.hasScheduleOptions(integration));
	}

	@Test
	public void testHasSubscriptionOptions() {
		Integration integration = new Integration("insig", null, null);
		assertTrue(integrationDao.hasSubscriptions(integration));
		integration = new Integration("NotInsig", null, null);
		assertFalse(integrationDao.hasSubscriptions(integration));
	}

	@Test
	public void testGetIntegrationAdminUrl() {
		Integration integration = new Integration("test", null, null);
		assertEquals("/well/admin/integration/#!/test?",
				integrationDao.getIntegrationAdminUrl(integration));
	}

	@Test
	public void testGetIntegrationAdminUrlWithSchedule() {
		Integration integration = new Integration("insig", null, null);
		assertTrue(integrationDao.getIntegrationAdminUrl(integration).contains("schedule=true"));
	}

	@Test
	public void testGetIntegrationAdminUrlWithSubscription() {
		Integration integration = new Integration("insig", null, null);
		assertTrue(integrationDao.getIntegrationAdminUrl(integration).contains("subscription=true"));
	}
}
