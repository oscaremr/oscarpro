/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.provision;

import static com.well.integration.IntegrationTestUtils.API_KEY;
import static com.well.integration.IntegrationTestUtils.API_KEY_NAME;
import static com.well.integration.IntegrationTestUtils.CLIENT_ID;
import static com.well.integration.IntegrationTestUtils.CLIENT_ID_NAME;
import static com.well.integration.IntegrationTestUtils.INTEGRATION_MODULE;
import static com.well.integration.IntegrationTestUtils.isE2EEnabled;
import static com.well.integration.IntegrationTestUtils.setupIntegration;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import com.well.integration.Integration;
import com.well.integration.IntegrationDao;
import com.well.integration.IntegrationException;
import com.well.integration.client.Client;
import com.well.integration.config.ConfigPropertyKey;
import com.well.integration.config.parameters.ConfigParams;
import com.well.integration.provision.ProvisionServlet.ServletResponse;
import java.io.IOException;
import javax.ws.rs.core.Response.Status;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.util.SpringUtils;
import org.springframework.mock.web.MockHttpServletResponse;
import oscar.OscarProperties;
import oscar.log.LogAction;

// Needs to be tested from local context to allow manipulation of properties
// and access to spring context.
public class ProvisionServletIT extends DaoTestFixtures {

    private static boolean e2eEnabled;
    public final String LOCAL_ADDRESS = "::1";

    IntegrationDao integrationDao = SpringUtils.getBean(IntegrationDao.class);
    SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
    private ConfigPropertyKey keys;
    private Integration integration;

    @BeforeClass
    public static void beforeClass() throws Exception {
        e2eEnabled = isE2EEnabled();
    }

    @Before
    public void before() {
        setupIntegration();
        integration = integrationDao.getIntegration(INTEGRATION_MODULE);
        keys = integration.getKeys();
    }

    @Test
    public void testGetResponseProvisioned() throws Exception {
        if (!e2eEnabled) { return; }
        Client client = mockE2EResponse(200);// provisioned and active
        Integration integration = integrationDao.getIntegration(INTEGRATION_MODULE);
        systemPreferencesDao.mergeOrPersist(integration.getKeys().CONFIGURED, "false");
        ProvisionServlet servlet = new ProvisionServlet();
        servlet.setIntegration(INTEGRATION_MODULE);
        ServletResponse response = servlet.getResponse(LOCAL_ADDRESS, client);
        assertEquals(Status.OK.getStatusCode(), response.status);
        assertEquals("OK", response.message);
    }

    @Test
    public void testGetResponseConfiguredSuccess() throws Exception {
        if (!e2eEnabled) { return; }
        Client client = mockE2EResponse(404, 200, 200);// provisioned and active
        Integration integration = integrationDao.getIntegration(INTEGRATION_MODULE);
        systemPreferencesDao.mergeOrPersist(integration.getKeys().CONFIGURED, "true");
        ProvisionServlet servlet = new ProvisionServlet();
        servlet.setIntegration(INTEGRATION_MODULE);
        ServletResponse response = servlet.getResponse(LOCAL_ADDRESS, client);
        assertEquals(Status.OK.getStatusCode(), response.status);
        assertEquals("OK", response.message);
    }

    @Test(expected = IntegrationException.class)
    public void testGetResponseConfiguredFailSave() throws Exception {
        Client client = mockE2EResponse(500);// provisioned and active
        ProvisionServlet servlet = new ProvisionServlet();
        servlet.setIntegration(INTEGRATION_MODULE);
        ConfigParams params = new ConfigParams();
        params.setIntegration(integrationDao.getIntegration(INTEGRATION_MODULE));
        servlet.manager = spy(servlet.manager);
        when(servlet.manager.saveConfig(null, "testIP", params, true, client))
                .thenThrow(IntegrationException.class);
        ServletResponse response = servlet.getResponse(LOCAL_ADDRESS, client);
        assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.status);
        assertEquals("Test error", response.message);
    }

    @Test
    public void testGetResponseConfiguredFailUnexpected() throws Exception {
        if (!e2eEnabled) { return; }
        Client client = mockE2EResponse(500);// provisioned and active
        ProvisionServlet servlet = new ProvisionServlet();
        servlet.setIntegration(INTEGRATION_MODULE);

        ServletResponse response = servlet.getResponse(LOCAL_ADDRESS, client);
        assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.status);
        assertEquals("Unexpected error encountered.", response.message);
    }

    @Test
    public void testGetResponseConfiguredFailMissingParameter() throws Exception {
        Client client = mockE2EResponse(500);// provisioned and active
        ProvisionServlet servlet = new ProvisionServlet();


        ServletResponse response = servlet.getResponse(LOCAL_ADDRESS, client);
        assertEquals(Status.BAD_REQUEST.getStatusCode(), response.status);
        assertEquals("Integration not found.", response.message);
    }

    @Test
    public void testGetResponseConnected() throws IntegrationException {
        if (!e2eEnabled) { return; }
        Client client = mockE2EResponse(200);// provisioned and active
        ProvisionServlet servlet = new ProvisionServlet();
        servlet.setIntegration(INTEGRATION_MODULE);
        ServletResponse response = servlet.getResponse(LOCAL_ADDRESS, client);
        assertEquals(Status.NO_CONTENT.getStatusCode(), response.status);
        assertEquals("Connection already active.", response.message);
    }

    @Test
    public void testGetResponseNotAvailable() {
        OscarProperties p = OscarProperties.getInstance();
        String originalUrl = p.getProperty(keys.BASE_URL);
        p.setProperty(keys.BASE_URL, "http://localhost:2021/should/not/exist/");
        try {
            Client client = new Client(integration);
            ProvisionServlet servlet = new ProvisionServlet();
            servlet.setIntegration(INTEGRATION_MODULE);
            ServletResponse response = servlet.getResponse(LOCAL_ADDRESS, client);
            assertEquals(Status.BAD_REQUEST.getStatusCode(), response.status);
            assertEquals("Unable to execute request.", response.message);
        }
        finally {
            p.setProperty(keys.BASE_URL, originalUrl);
        }
    }

    @Test
    public void testGetResponseNotAuthorized() throws IntegrationException {
        if (!e2eEnabled) { return; }
        Client client = mockE2EResponse(401);
        ProvisionServlet servlet = new ProvisionServlet();
        servlet.setIntegration(INTEGRATION_MODULE);
        ServletResponse response = servlet.getResponse(LOCAL_ADDRESS, client);
        assertEquals(Status.UNAUTHORIZED.getStatusCode(), response.status);
        assertEquals("Integration rejected system credentials.", response.message);
    }

    @Test
    public void testGetResponseFactoryException() throws IntegrationException {
        if (!e2eEnabled) { return; }
        try (MockedStatic<LogAction> ignored = mockStatic(LogAction.class)) {
            Client client = mockE2EResponse(404);
            ProvisionServlet servlet = spy(new ProvisionServlet());
            servlet.setIntegration(INTEGRATION_MODULE);
            when(servlet.getParameters()).thenReturn(null);
            ServletResponse response = servlet.getResponse(LOCAL_ADDRESS, client);
            assertEquals(Status.BAD_REQUEST.getStatusCode(), response.status);
            assertEquals("Missing config request.", response.message);
        }
    }

    @Test
    public void testGetResponseNotEnabled() throws IntegrationException {
        if (!e2eEnabled) { return; }
        systemPreferencesDao.mergeOrPersist(keys.ENABLED, "false");
        Client client = mockE2EResponse(404);// not provisioned
        ProvisionServlet servlet = new ProvisionServlet();
        servlet.setIntegration(INTEGRATION_MODULE);
        ServletResponse response = servlet.getResponse(LOCAL_ADDRESS, client);
        assertEquals(Status.BAD_REQUEST.getStatusCode(), response.status);
        assertEquals("Integration is not enabled.", response.message);
    }

    @Test
    public void testGetResponseNoApiKey() throws IntegrationException {
        OscarProperties.getInstance().setProperty(keys.API_KEY, "");
        Client client = mockE2EResponse(404);// not provisioned
        ProvisionServlet servlet = new ProvisionServlet();
        servlet.setIntegration(INTEGRATION_MODULE);
        ServletResponse response = servlet.getResponse(LOCAL_ADDRESS, client);
        assertEquals(Status.BAD_REQUEST.getStatusCode(), response.status);
        assertEquals("Missing API key.", response.message);
    }

    @Test
    public void testGetResponseBadIpAddress() {
        ProvisionServlet servlet = new ProvisionServlet();
        ServletResponse response = servlet.getResponse("23.23.23.23");
        assertEquals(Status.FORBIDDEN.getStatusCode(), response.status);
        assertEquals(Status.FORBIDDEN.getReasonPhrase(), response.message);
    }

    private Client mockE2EResponse(Integer ... status) throws IntegrationException {
        Integer[] codes = new Integer[3];
        for (int i = 0; i < 3; i++) {
            codes[i] = i < status.length ? status[i] : codes[0];
        }
        Client client = spy(new Client(integration));
        when(client.getHeaders())
                .thenReturn(new Header[] {
                        new BasicHeader("Prefer", "code=" + codes[0].toString()),
                        new BasicHeader(API_KEY_NAME, API_KEY),
                        new BasicHeader(CLIENT_ID_NAME, CLIENT_ID),
                })
                .thenReturn(new Header[] {
                        new BasicHeader("Prefer", "code=" + codes[1].toString()),
                        new BasicHeader(API_KEY_NAME, API_KEY),
                        new BasicHeader(CLIENT_ID_NAME, CLIENT_ID),
                })
                .thenReturn(new Header[] {
                        new BasicHeader("Prefer", "code=" + codes[2].toString()),
                        new BasicHeader(API_KEY_NAME, API_KEY),
                        new BasicHeader(CLIENT_ID_NAME, CLIENT_ID),
                });
        return client;
    }

    public static class ServletResponseTest {
        @Test
        public void testOk() {
            ServletResponse response = ServletResponse.ok();
            assertEquals(200, response.status);
            assertEquals("OK", response.message);
        }

        @Test
        public void testErrorStatus() {
            Status status = Status.CONFLICT;
            ServletResponse response = ServletResponse.error(status);
            assertEquals(status.getStatusCode(), response.status);
            assertEquals(status.getReasonPhrase(), response.message);
        }

        @Test
        public void testErrorThrowable() {
            Status status = Status.CONFLICT;
            ServletResponse response = ServletResponse.error(status, new Exception("NOT CONFLICT"));
            assertEquals(status.getStatusCode(), response.status);
            assertEquals("NOT CONFLICT", response.message);
        }

        @Test
        public void testWrite() throws IOException {
            MockHttpServletResponse httpResponse = new MockHttpServletResponse();
            ServletResponse ok = spy(ServletResponse.ok());
            doNothing().when(ok).setContentType(httpResponse);
            ok.write(httpResponse);
            assertEquals("{\"status\":200,\"message\":\"OK\"}", httpResponse.getContentAsString());
        }
    }
}