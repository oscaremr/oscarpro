/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.provision;

import com.well.integration.Integration;
import com.well.integration.provision.handlers.InsigHandler;
import com.well.integration.provision.handlers.ProvisionHandler;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

public class ProvisionFactoryTest {

    @Test
    public void testGetHandler() {
        Integration integration = new Integration("insig", null, null);
        assertThat(ProvisionFactory.getHandler(integration), instanceOf(InsigHandler.class));
    }

    @Test
    public void testGetHandlerNullIntegration() {
        assertThat(ProvisionFactory.getHandler(null), instanceOf(ProvisionHandler.class));
    }
}