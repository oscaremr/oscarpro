/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.provision.handlers;

import static com.well.integration.IntegrationTestUtils.INTEGRATION_MODULE;
import static com.well.integration.IntegrationTestUtils.setupIntegration;

import com.well.integration.IntegrationTestUtils;
import com.well.integration.config.parameters.InsigConfigParams;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class InsigHandlerIT extends ProvisionHandlerIT {

    @BeforeClass
    public static void beforeClass() throws Exception {
        IntegrationTestUtils.restoreIntegrationTables();
        setupIntegration();
    }

    @Before
    public void before() {
        this.integrationName = INTEGRATION_MODULE;
        super.before();
    }

    @Test
    public void testSavePreferencesLinkEnabled() {
        InsigHandler factory = IntegrationTestUtils.getFactoryMock(manager);
        factory.savePreferences();
        checkPreference("schedule_tp_link_enabled", "true");
        checkPreference("schedule_tp_link_type", "Virtual");
        checkPreference("schedule_tp_link_display", "VC+");
    }

    @Test
    public void testSavePreferencesLinkDisabled() {
        InsigHandler factory = IntegrationTestUtils.getFactoryMock(manager);
        factory.savePreferences();
        ((InsigConfigParams) factory.getParameters()).setTpLinkEnabled(false);
        factory.savePreferences();
        checkPreference("schedule_tp_link_enabled", "false");
        checkPreference("schedule_tp_link_type", "");
        checkPreference("schedule_tp_link_display", "");
    }
}