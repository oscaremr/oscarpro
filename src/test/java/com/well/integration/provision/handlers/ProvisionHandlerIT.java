/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.provision.handlers;

import static com.well.integration.IntegrationTestUtils.API_KEY;
import static com.well.integration.IntegrationTestUtils.INTEGRATION_MODULE;
import static com.well.integration.IntegrationTestUtils.getCreateRequest;
import static com.well.integration.IntegrationTestUtils.getFactoryMock;
import static com.well.integration.IntegrationTestUtils.restoreIntegrationTables;
import static com.well.integration.IntegrationTestUtils.setupIntegration;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.quatro.dao.security.SecuserroleDao;
import com.quatro.model.security.Secuserrole;
import com.well.integration.IntegrationDao;
import com.well.integration.IntegrationException;
import com.well.integration.IntegrationManager;
import com.well.integration.config.ConfigPropertyDao;
import com.well.integration.config.ConfigPropertyKey;
import com.well.integration.config.parameters.ConfigParams;
import com.well.integration.provision.handlers.Handler.ProvisionRequest;
import java.util.List;
import java.util.UUID;
import org.apache.commons.text.RandomStringGenerator;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.SecurityDao;
import org.oscarehr.common.dao.ServiceAccessTokenDao;
import org.oscarehr.common.dao.ServiceClientDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.Security;
import org.oscarehr.common.model.ServiceAccessToken;
import org.oscarehr.common.model.ServiceClient;
import org.oscarehr.common.model.SystemPreferences;
import org.oscarehr.phr.RegistrationHelper;
import org.oscarehr.util.SpringUtils;

public class ProvisionHandlerIT extends DaoTestFixtures {
    private static String originalIntegrations;

    protected IntegrationManager manager = SpringUtils.getBean(IntegrationManager.class);
    protected IntegrationDao integrationDao = SpringUtils.getBean(IntegrationDao.class);
    protected ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);
    protected SecurityDao securityDao = SpringUtils.getBean(SecurityDao.class);
    protected SecuserroleDao secuserroleDao = SpringUtils.getBean(SecuserroleDao.class);
    protected ServiceAccessTokenDao serviceAccessTokenDao = SpringUtils.getBean(ServiceAccessTokenDao.class);
    protected ServiceClientDao serviceClientDao = SpringUtils.getBean(ServiceClientDao.class);
    protected SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
    protected ConfigPropertyDao configPropertyDao = SpringUtils.getBean(ConfigPropertyDao.class);

    protected ConfigParams createParams;
    protected ConfigPropertyKey configPropertyKey;
    protected String integrationName = INTEGRATION_MODULE;

    @BeforeClass
    public static void beforeClass() throws Exception {
        restoreIntegrationTables();
        IntegrationDao integrationDao = SpringUtils.getBean(IntegrationDao.class);
        originalIntegrations = integrationDao.getIntegrationModules();
        integrationDao.setIntegrationModules(INTEGRATION_MODULE);
        setupIntegration();
    }

    @AfterClass
    public static void afterClass() throws Exception {
        IntegrationDao integrationDao = SpringUtils.getBean(IntegrationDao.class);
        originalIntegrations = integrationDao.getIntegrationModules();
        integrationDao.setIntegrationModules(originalIntegrations);
    }

    @Before
    public void before() {
        configPropertyKey = integrationDao.getIntegration(integrationName).getKeys();
        createParams = getCreateRequest(integrationName);
    }

    @Test
    public void testInvokeNullConfig() {
        ProvisionHandler handler = new ProvisionHandler(manager, null);
        try {
            handler.invoke();
            fail();
        } catch (IntegrationException e) {
            assertEquals("Missing config request.", e.getMessage());
        }
    }

    @Test
    public void testInvokeNullProvider() {
        createParams.setSoapProviderNo(null);
        ProvisionHandler handler = new ProvisionHandler(manager, createParams);
        try {
            handler.invoke();
            fail();
        } catch (IntegrationException e) {
            assertEquals("Null SOAP provider.", e.getMessage());
        }
    }

    @Test
    public void testInvokeRandomPassword() throws Exception {
        testRandomPassword(null);
        testRandomPassword("");
        testRandomPassword("    ");
    }

    @Test
    public void testInvokeUserPassword() throws Exception {
        createParams.setSoapProviderPassword("password");
        ProvisionHandler handler = getHandlerSpy();
        handler.invoke();
        assertEquals(createParams.getSoapProviderPassword(), handler.getPassword());
    }

    @Test
    public void testCreateProvider() throws Exception {
        ProvisionHandler handler = new ProvisionHandler(manager, createParams);
        String suggested = providerDao.suggestProviderNo();
        Provider p = handler.createProvider();

        // New provider created with suggested providerNo.
        assertEquals(suggested, p.getProviderNo());
        p = providerDao.getProvider(suggested);

        // Provider in DB meets expectation
        assertEquals("Virtual", p.getLastName());
        assertEquals("Care", p.getFirstName());
        assertEquals("doctor", p.getProviderType());
        assertEquals("Integration", p.getSpecialty());
        assertEquals("1", p.getStatus());
    }

    @Test
    public void testCreateSecurity() throws IntegrationException {
        ProvisionHandler handler = new ProvisionHandler(manager, createParams);
        Provider p = handler.createProvider();
        final String username = "username";
        final String password = "password";
        final String hashedPassword = "hashedPassword";
        try (MockedStatic<RegistrationHelper> ignored = mockStatic(RegistrationHelper.class,
                new Answer<String>() {
                    @Override
                    public String answer(InvocationOnMock invocationOnMock) {
                        return username;
                    }
                })) {
            Security s = handler.createSecurity(p, password);
            s = securityDao.find(s.getId());
            assertEquals(p.getProviderNo(), s.getProviderNo());
            // Username should be random.
            assertEquals(username, s.getUserName());
            // Password should be hashed.
            // OSCAR-4817 - removed until upgrade to java 8.
            // too difficult to test under Java 7 with Mockito
            // and power mock is incompatible with DaoTestFixtures
            // assertEquals(hashedPassword, s.getPassword());
            assertNull(s.getPin());
            assertEquals(0, (int) s.getBExpireset());
            assertEquals(0, (int) s.getBLocallockset());
            assertEquals(0, (int) s.getBRemotelockset());
            assertNull(s.getDateExpiredate());
            assertFalse(s.isForcePasswordReset());

            @SuppressWarnings("unchecked")
            List<Secuserrole> roleList = (List<Secuserrole>) secuserroleDao.findByProviderNo(p.getProviderNo());
            Secuserrole role = roleList.get(0);
            assertEquals("doctor", role.getRoleName());
            assertEquals(1, (int) role.getActiveyn());
        }
    }

    @Test
    public void testUpdateSecurity() throws Exception {
        ProvisionHandler handler = new ProvisionHandler(manager, createParams);
        Provider p = handler.createProvider();
        final String password = "password";
        final String hashedPassword = "hashedPassword";

        Security s = handler.createSecurity(p, password);
        // Ensure password is not already hashedPassword.
        assertNotSame(s.getPassword(), hashedPassword);
        s = handler.updateSecurity(p, password);
        s = securityDao.find(s.getId());
        assertEquals(p.getProviderNo(), s.getProviderNo());
        // Password should be hashed.
        // OSCAR-4817 - removed until upgrade to java 8.
        // too difficult to test under Java 7 with Mockito
        // and power mock is incompatible with DaoTestFixtures
        // assertEquals(hashedPassword, s.getPassword());
        // Should clear any lock, expiration or reset present.
        assertNull(s.getPin());
        assertEquals(0, (int) s.getBExpireset());
        assertEquals(0, (int) s.getBLocallockset());
        assertEquals(0, (int) s.getBRemotelockset());
        assertNull(s.getDateExpiredate());
        assertFalse(s.isForcePasswordReset());

        @SuppressWarnings("unchecked")
        List<Secuserrole> roleList = (List<Secuserrole>) secuserroleDao.findByProviderNo(p.getProviderNo());
        Secuserrole role = roleList.get(0);
        assertEquals("doctor", role.getRoleName());
        assertEquals(1, (int) role.getActiveyn());
    }

    @Test
    public void testExistingProviderMissingSecurity() throws IntegrationException {
        Provider p = new Provider();
        p.setLastName("lastName");
        p.setFirstName("firstName");
        p.setProviderType("doctor");
        p.setSex("F");
        p.setSpecialty("TestCase");
        p.setStatus("1");
        p.setPrescribeItType("doctor");
        p.setProviderNo(providerDao.suggestProviderNo());
        manager.providerDao.saveProvider(p);
        createParams.setSoapProviderNo(p.getProviderNo());
        ProvisionHandler handler = getHandlerSpy();
        handler.invoke();
        verify(handler, times(1)).createSecurity(any(Provider.class), anyString());
    }

    @Test
    public void testCreateServiceClient() {
        ProvisionHandler handler = new ProvisionHandler(manager, createParams);
        RandomStringGenerator mock = mock(RandomStringGenerator.class);
        handler.setRandomStringGenerator(mock);
        final String key = "clientKey";
        final String secret = "clientSecret";
        when(mock.generate(anyInt())).thenReturn(key).thenReturn(secret);
        ServiceClient client = handler.createServiceClient();
        client = serviceClientDao.find(client.getId());
        assertEquals("Insig Integration", client.getName());
        assertEquals(key, client.getKey());
        assertEquals(secret, client.getSecret());
        assertEquals("-1", client.getUri());
        assertEquals(-1, (int) client.getLifetime());
    }

    @Test
    public void testCreateAccessToken() throws IntegrationException {
        ProvisionHandler handler = new ProvisionHandler(manager, createParams);
        Provider p = handler.createProvider();
        ServiceClient client = handler.createServiceClient();
        final UUID tokenId = UUID.fromString("00000000-0000-0000-0000-000000000001");
        final UUID tokenSecret = UUID.fromString("00000000-0000-0000-0000-000000000002");
        try (MockedStatic<UUID> ignored = mockStatic(UUID.class, new Answer<Object>() {
            boolean first = true;

            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                if (!invocationOnMock.getMethod().getName().equalsIgnoreCase("randomUUID")) {
                    return invocationOnMock.callRealMethod();
                }
                if (first) {
                    first = false;
                    return tokenId;
                } else {
                    return tokenSecret;
                }
            }
        })) {
            ServiceAccessToken sat = handler.createAccessToken(p, client);
            sat = serviceAccessTokenDao.find(sat.getId());
            assertEquals(client.getId(), sat.getClientId());
            assertEquals(-1, (int) sat.getLifetime());
            assertEquals(tokenId.toString(), sat.getTokenId());
            assertEquals(tokenSecret.toString(), sat.getTokenSecret());
            assertEquals(p.getProviderNo(), sat.getProviderNo());
            assertEquals("", sat.getScopes());
        }
    }

    @Test
    public void testSavePreferencesApiKey() {
        ProvisionHandler handler = getFactoryMock(manager, integrationName);
        handler.setApiKey("1234");
        handler.savePreferences();
        checkPreference(configPropertyKey.REST_API_KEY, "1234");
    }

    @Test
    public void testSavePreferencesConfigured() {
        ProvisionHandler handler = getFactoryMock(manager, integrationName);
        handler.savePreferences();
        checkPreference(configPropertyKey.CONFIGURED, "true");
    }

    @Test
    public void testSavePreferencesRestClient() {
        ProvisionHandler handler = getFactoryMock(manager, integrationName);
        handler.savePreferences();
        checkPreference(configPropertyKey.REST_CLIENT_ID, "1");

    }

    @Test
    public void testSavePreferencesSoapProviderNo() {
        ProvisionHandler handler = getFactoryMock(manager, integrationName);
        handler.savePreferences();
        checkPreference(configPropertyKey.SOAP_PROVIDER_NO, "2");

    }

    @Test
    public void testInvokeCreateAPIKey() throws Exception {
        final UUID apiKey = UUID.fromString(API_KEY);
        ProvisionHandler handler = new ProvisionHandler();
        try (MockedStatic<UUID> ignored = mockStatic(UUID.class, new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                if (!invocationOnMock.getMethod().getName().equalsIgnoreCase("randomUUID")) {
                    return invocationOnMock.callRealMethod();
                }
                return apiKey;
            }
        })) {
            handler.createApiKey();
            assertEquals(apiKey.toString(), handler.getApiKey());
        }
    }

    @Test
    public void testInvokeCreateBoth() throws Exception {
        ProvisionHandler handler = getHandlerSpy();
        handler.invoke();
        verify(handler, times(1)).createProvider();
        verify(handler, times(1)).createSecurity((Provider) any(), anyString());
        verify(handler, times(1)).createServiceClient();
        verify(handler, times(1)).createAccessToken((Provider) any(), (ServiceClient) any());
        verify(handler, times(1)).savePreferences();
    }

    @Test
    public void testInvokeUseExistingProvider() throws Exception {
        ProvisionHandler handler = new ProvisionHandler(manager, createParams);
        handler.invoke();
        createParams.setSoapProviderNo(handler.getProvider().getProviderNo());
        ProvisionHandler mockedHandler = getHandlerSpy();
        mockedHandler.invoke();
        verify(mockedHandler, times(0)).createProvider();
        verify(mockedHandler, times(0)).createSecurity((Provider) any(), anyString());
        verify(mockedHandler, times(1)).createServiceClient();
        verify(mockedHandler, times(1)).createAccessToken((Provider) any(), (ServiceClient) any());
        verify(mockedHandler, times(1)).savePreferences();
        assertEquals(handler.getProvider().getProviderNo(), mockedHandler.getProvider().getProviderNo());
    }

    @Test
    public void testInvokeUseExistingClient() throws Exception {
        ProvisionHandler handler = new ProvisionHandler(manager, createParams);
        handler.invoke();
        createParams.setRestClientId(handler.getClient().getId().toString());
        ProvisionHandler mocked = getHandlerSpy();
        mocked.invoke();
        verify(mocked, times(1)).createProvider();
        verify(mocked, times(1)).createSecurity((Provider) any(), anyString());
        verify(mocked, times(0)).createServiceClient();
        verify(mocked, times(1)).createAccessToken((Provider) any(), (ServiceClient) any());
        verify(mocked, times(1)).savePreferences();
        assertEquals(handler.getClient().getId(), mocked.getClient().getId());
    }

    @Test
    public void testInvokeUseExistingBoth() throws Exception {
        ProvisionHandler handler = new ProvisionHandler(manager, createParams);
        handler.invoke();
        createParams.setSoapProviderNo(handler.getProvider().getProviderNo());
        createParams.setRestClientId(handler.getClient().getId().toString());
        ProvisionHandler mocked = getHandlerSpy();
        mocked.invoke();
        verify(mocked, times(0)).createProvider();
        verify(mocked, times(0)).createSecurity((Provider) any(), anyString());
        verify(mocked, times(0)).createServiceClient();
        verify(mocked, times(1)).createAccessToken((Provider) any(), (ServiceClient) any());
        verify(mocked, times(1)).savePreferences();

        assertEquals(handler.getProvider().getProviderNo(), mocked.getProvider().getProviderNo());
        assertEquals(handler.getClient().getId(), mocked.getClient().getId());
    }

    @Test
    public void testInvokeCreateSetsAllFields() throws IntegrationException {
        ProvisionHandler handler = new ProvisionHandler(manager, createParams);
        handler.invoke();
        testFieldExistence(handler);
    }

    @Test
    public void testInvokeExistingSetsAllFields() throws IntegrationException {
        ProvisionHandler handler = new ProvisionHandler(manager, createParams);
        handler.invoke();
        createParams.setSoapProviderNo(handler.getProvider().getProviderNo());
        createParams.setRestClientId(handler.getClient().getId().toString());
        handler = new ProvisionHandler(manager, createParams);
        handler.invoke();
        testFieldExistence(handler);
    }

    @Test
    public void testInvokeMissingProvider() {
        createParams.setSoapProviderNo("999999");
        ProvisionHandler handler = new ProvisionHandler(manager, createParams);
        try {
            handler.invoke();
            fail();
        } catch (IntegrationException e) {
            assertEquals("SOAP provider not found.", e.getMessage());
        }
    }

    @Test
    public void testInvokeMissingRestClient() {
        createParams.setRestClientId("999999");
        ProvisionHandler handler = new ProvisionHandler(manager, createParams);
        try {
            handler.invoke();
            fail();
        } catch (IntegrationException e) {
            assertEquals("Missing REST client.", e.getMessage());
        }
    }

    @Test
    public void getProvisionRequest() {
        ProvisionHandler handler = getFactoryMock(manager, integrationName);
        ProvisionRequest request = handler.getProvisionRequest();
        assertEquals("CONSUMER_KEY", request.getConsumerKey());
        assertEquals("CONSUMER_SECRET", request.getConsumerSecret());
        assertEquals("OAUTH_TOKEN", request.getOauthToken());
        assertEquals("OAUTH_SECRET", request.getOauthSecret());
        assertEquals("USER", request.getProviderUsername());
        assertEquals("PASSWORD", request.getProviderPassword());
        assertEquals("API_KEY", request.getApiKey());
    }

    protected void checkPreference(String key, String value) {
        SystemPreferences pref = systemPreferencesDao.findPreferenceByName(key);
        assertEquals(value, pref.getValue());
    }

    private void testRandomPassword(final String password) throws Exception {
        SchemaUtils.restoreTable("security");
        ProvisionHandler mocked = getHandlerSpy();
        createParams.setSoapProviderPassword(password);
        final String randomPassword = "randomPassword";
        try (MockedStatic<RegistrationHelper> ignored = mockStatic(RegistrationHelper.class,
                new Answer<String>() {
                    @Override
                    public String answer(InvocationOnMock invocationOnMock) {
                        return randomPassword;
                    }
                })) {
            mocked.invoke();
            assertEquals(randomPassword, mocked.getPassword());
        }
    }

    private void testFieldExistence(ProvisionHandler handler) {
        assertNotNull(handler.getPassword());
        assertNotNull(handler.getProvider());
        assertNotNull(handler.getSecurity());
        assertNotNull(handler.getServiceAccessToken());
        assertNotNull(handler.getClient());
    }

    private ProvisionHandler getHandlerSpy() throws IntegrationException {
        ProvisionHandler handler = spy(new ProvisionHandler(manager, createParams));
        handler.setKeys(configPropertyKey);
        return handler;
    }
}