/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.provision;

import com.well.integration.client.Response;
import com.well.integration.client.ResponseTest;
import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class ProvisionResponseTest extends ResponseTest {

    @Override
    @Test
    public void testIsSuccess() {
        assertSuccess(true, 200);
        assertSuccess(true, 206);
        assertSuccess(true, 404);
        assertSuccess(false, 400);
        assertSuccess(false, 401);
        assertSuccess(false, 500);
    }

    @Override
    @Test
    public void testHasError() {
        assertError(false, 200);
        assertError(false, 206);
        assertError(false, 404);
        assertError(true, 400);
        assertError(true, 401);
        assertError(true, 500);
    }

    @Test
    public void test404Provisioned() {
        ProvisionResponse response = new ProvisionResponse(404);
        response.setProvisioned(true);
        assertTrue(response.isSuccess());
        response.setProvisioned(false);
        assertTrue(response.isSuccess());
    }

    @Test
    public void test404NotProvisioned() {
        ProvisionResponse response = new ProvisionResponse(404);
        assertNull(response.isProvisioned());
        assertTrue(response.hasError());
    }

    @Override
    public Response getResponse(int status) {
        ProvisionResponse response = new ProvisionResponse(status);
        response.setProvisioned(false);
        return response;
    }
}