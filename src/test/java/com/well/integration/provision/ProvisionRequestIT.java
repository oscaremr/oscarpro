/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.provision;

import static com.well.integration.IntegrationTestUtils.INTEGRATION_MODULE;
import static com.well.integration.IntegrationTestUtils.getFactoryMock;
import static com.well.integration.IntegrationTestUtils.getRequestMock;
import static com.well.integration.IntegrationTestUtils.isE2EEnabled;
import static com.well.integration.IntegrationTestUtils.setupIntegration;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.well.integration.Integration;
import com.well.integration.IntegrationDao;
import com.well.integration.IntegrationException;
import com.well.integration.IntegrationManager;
import com.well.integration.client.Client;
import com.well.integration.client.Request;
import com.well.integration.config.ConfigPropertyKey;
import com.well.integration.provision.handlers.InsigHandler;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.util.SpringUtils;
import org.springframework.http.HttpMethod;

public class ProvisionRequestIT extends DaoTestFixtures {

    private static boolean e2eEnabled;

    Client client;
    IntegrationManager manager = SpringUtils.getBean(IntegrationManager.class);
    IntegrationDao integrationDao = SpringUtils.getBean(IntegrationDao.class);

    @Mock
    CloseableHttpResponse httpResponse;

    @Mock
    ProvisionRequest getResponseRequest;

    @Mock
    ProvisionRequest connectionRequest;

    @Mock
    ProvisionRequest provisionRequest;

    SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);

    private ConfigPropertyKey keys;
    private Integration integration;

    @BeforeClass
    public static void beforeClass() throws Exception {
        e2eEnabled = isE2EEnabled();
    }

    @Before
    public void before() throws IntegrationException {

        setupIntegration();
        integration = integrationDao.getIntegration(INTEGRATION_MODULE);

        keys = integration.getKeys();
        client = new Client(integration);

        MockitoAnnotations.openMocks(this);

        when(getResponseRequest.getResponse(httpResponse)).thenCallRealMethod();
        doCallRealMethod().when(getResponseRequest).setMethod(any(HttpMethod.class));

        when(connectionRequest.handleConnectionResponse(httpResponse)).thenCallRealMethod();
        doCallRealMethod().when(connectionRequest).setMethod(any(HttpMethod.class));
        connectionRequest.setMethod(HttpMethod.GET);

        when(provisionRequest.handleProvisionResponse(httpResponse)).thenCallRealMethod();
        doCallRealMethod().when(provisionRequest).setMethod(any(HttpMethod.class));
        provisionRequest.setMethod(HttpMethod.GET);
    }

    private void setHttpResponseStatus(int status) {
        StatusLine statusLine = mock(StatusLine.class);
        when(httpResponse.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(status);
    }

    // START E2E
    @Test
    public void testInsigHealthCheck() throws Exception {
        if (!e2eEnabled) { return; }
        Client client = new Client(integration);
        ProvisionRequest request = ProvisionRequest.checkHealth(integration);
        ProvisionResponse response = request.execute(client);
        assertTrue(request.getHealthCheck());
        assertNull(request.getProviderNo());
        assertNull(request.getHandler());
        assertEquals(200, response.getStatus());
    }

    @Test
    public void testConnectionValidation() throws Exception {
        if (!e2eEnabled) { return; }
        Client client = new Client(integration);
        ProvisionRequest request = ProvisionRequest.checkConnection(integration);
        ProvisionResponse response = request.execute(client);
        assertNull(request.getHealthCheck());
        assertNull(request.getProviderNo());
        assertNull(request.getHandler());
        assertEquals(200, response.getStatus());
    }

    @Test
    public void testCheckProvider() throws Exception {
        if (!e2eEnabled) { return; }
        Client client = new Client(integration);
        ProvisionRequest request = ProvisionRequest.checkProvider(integration, "123456");
        ProvisionResponse response = request.execute(client);
        assertNull(request.getHealthCheck());
        assertEquals("123456", request.getProviderNo());
        assertNull(request.getHandler());
        assertEquals(200, response.getStatus());
    }

    @Test
    public void testSendConfiguration() throws Exception {
        if (!e2eEnabled) { return; }
        Client client = new Client(integration);
        InsigHandler config = getFactoryMock(manager);
        ProvisionRequest request = ProvisionRequest.sendConfiguration(config);
        ProvisionResponse response = request.execute(client);
        assertNull(request.getHealthCheck());
        assertNull(request.getProviderNo());
        assertEquals(config, request.getHandler());
        assertEquals(200, response.getStatus());
    }

    @Test
    public void testGetBody() {
        InsigHandler config = mock(InsigHandler.class);
        Request request = getRequestMock(INTEGRATION_MODULE);
        String configJson = "{\"config\":{}}";
        InsigHandler.ProvisionRequest provisionRequest = mock(InsigHandler.ProvisionRequest.class);
        when(config.getProvisionRequest()).thenReturn(provisionRequest);
        when(provisionRequest.toString()).thenReturn(configJson);
        assertEquals(configJson, request.getBody());
    }

    @Test
    public void testGetParametersConnectionValidationRequest() {
        List<NameValuePair> parameters = ProvisionRequest.checkConnection(integration).getParameters();
        assertEquals(0, parameters.size());
    }

    @Test
    public void testGetParametersHealthCheckRequest() {
        List<NameValuePair> parameters = ProvisionRequest.checkHealth(integration).getParameters();
        assertEquals(1, parameters.size());
        assertEquals(new BasicNameValuePair("healthCheck", "true"), parameters.get(0));
    }

    @Test
    public void testGetParametersProviderRequest() {
        ProvisionRequest request = ProvisionRequest.checkProvider(integration, "123456");
        List<NameValuePair> parameters = request.getParameters();
        assertEquals(1, parameters.size());
        assertEquals(new BasicNameValuePair("providerNo", "123456"), parameters.get(0));
    }

    @Test
    public void testGetParametersConfigurationRequest() {
        Request request = getRequestMock(INTEGRATION_MODULE);
        assertEquals(0, request.getParameters().size());
    }

    @Test
    public void testGetResponseConnection() throws IntegrationException {
        getResponseRequest.setMethod(HttpMethod.GET);
        getResponseRequest.getResponse(httpResponse);
        verify(getResponseRequest, times(1)).handleConnectionResponse(httpResponse);
    }

    @Test
    public void testGetResponseProvision() throws IntegrationException {
        getResponseRequest.setMethod(HttpMethod.POST);
        getResponseRequest.getResponse(httpResponse);
        verify(getResponseRequest, times(1)).handleProvisionResponse(httpResponse);
    }

    @Test
    public void testGetResponseError() throws IntegrationException {
        getResponseRequest.setMethod(HttpMethod.TRACE);
        ProvisionResponse response = getResponseRequest.getResponse(httpResponse);
        verify(getResponseRequest, times(0)).handleConnectionResponse(httpResponse);
        verify(getResponseRequest, times(0)).handleProvisionResponse(httpResponse);
        assertEquals(400, response.getStatus());
        assertEquals("Unsupported method.", response.getMessage());
    }

    @Test
    public void testHandleConnectionResponseSuccess() throws IntegrationException {
        setHttpResponseStatus(200);
        ProvisionResponse response = connectionRequest.handleConnectionResponse(httpResponse);
        assertTrue(response.isSuccess());
        assertTrue(response.isProvisioned());
    }

    @Test
    public void testHandleConnectionResponseNotProvisioned() throws IntegrationException {
        setHttpResponseStatus(404);
        ProvisionResponse response = connectionRequest.handleConnectionResponse(httpResponse);
        assertTrue(response.isSuccess());
        assertFalse(response.isProvisioned());
    }

    @Test
    public void testHandleConnectionResponseInvalidCredentials() throws IntegrationException {
        setHttpResponseStatus(401);
        ProvisionResponse response = connectionRequest.handleConnectionResponse(httpResponse);
        assertFalse(response.isSuccess());
        assertEquals("Integration rejected system credentials.", response.getMessage());
    }

    @Test
    public void testHandleConnectionResponseError() throws IntegrationException {
        setHttpResponseStatus(500);
        ProvisionResponse response = connectionRequest.handleConnectionResponse(httpResponse);
        assertFalse(response.isSuccess());
        assertEquals("Unexpected error encountered.", response.getMessage());
    }

    @Test
    public void testHandleConnectionResponseProviderRegistrationSuccess() throws IntegrationException {
            setHttpResponseStatus(200);
            ProvisionRequest connectionRequest = ProvisionRequest.checkProvider(integration, "123456");
            ProvisionResponse response = connectionRequest.handleConnectionResponse(httpResponse);
            assertTrue(response.isSuccess());
            assertTrue(response.isProvisioned());
            assertTrue(response.isProviderRegistered());
    }

    @Test
    public void testHandleConnectionResponseProviderRegistrationFail() throws IntegrationException {
        setHttpResponseStatus(206);
        ProvisionRequest connectionRequest = ProvisionRequest.checkProvider(integration, "123456");
        ProvisionResponse response = connectionRequest.handleConnectionResponse(httpResponse);
        assertTrue(response.isSuccess());
        assertTrue(response.isProvisioned());
        assertFalse(response.isProviderRegistered());
    }

    @Test
    public void testHandleProvisionResponseCreated() throws IntegrationException {
        setHttpResponseStatus(201);
        ProvisionResponse response = provisionRequest.handleProvisionResponse(httpResponse);
        assertTrue(response.isProvisioned());
        assertTrue(response.isSuccess());
    }

    @Test
    public void testHandleProvisionResponseUpdated() throws IntegrationException {
        setHttpResponseStatus(200);
        ProvisionResponse response = provisionRequest.handleProvisionResponse(httpResponse);
        assertTrue(response.isProvisioned());
        assertTrue(response.isSuccess());
    }

    @Test
    public void testHandleProvisionResponseInvalidCredentials() throws IntegrationException {
        setHttpResponseStatus(401);
        ProvisionResponse response = provisionRequest.handleProvisionResponse(httpResponse);
        assertFalse(response.isSuccess());
        assertEquals("Integration rejected system credentials.", response.getMessage());
    }

    @Test
    public void testHandleProvisionResponseError() throws IntegrationException {
        setHttpResponseStatus(500);
        ProvisionResponse response = provisionRequest.handleProvisionResponse(httpResponse);
        assertFalse(response.isSuccess());
        assertEquals("Unexpected error encountered.", response.getMessage());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetBodyMissingConfig() {
        ProvisionRequest request = mock(ProvisionRequest.class);
        doCallRealMethod().when(request).getBody();
        request.getBody();
    }
}