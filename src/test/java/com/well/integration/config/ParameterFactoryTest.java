/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config;

import com.well.integration.Integration;
import com.well.integration.config.parameters.ConfigParams;
import com.well.integration.config.parameters.InsigConfigParams;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

public class ParameterFactoryTest {

    @Test
    public void testGetGenerator() {
        Integration integration = new Integration("insig", null, null);
        assertThat(ParameterFactory.getParams(integration, "{}"), instanceOf(InsigConfigParams.class));
    }

    @Test
    public void testGetGeneratorNullIntegration() {
        assertThat(ParameterFactory.getParams(null, "{}"), instanceOf(ConfigParams.class));
    }
}