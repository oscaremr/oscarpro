/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config;

import static com.well.integration.IntegrationTestUtils.INTEGRATION_MODULE;
import static com.well.integration.IntegrationTestUtils.setupIntegration;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.well.integration.IntegrationDao;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;

public class ConfigPropertyDaoTest extends DaoTestFixtures {

	protected SystemPreferencesDao prefDao = SpringUtils.getBean(SystemPreferencesDao.class);
	protected ConfigPropertyDao configPropertyDao = SpringUtils.getBean(ConfigPropertyDao.class);
	protected IntegrationDao integrationDao = SpringUtils.getBean(IntegrationDao.class);
	protected OscarProperties p = OscarProperties.getInstance();

	private ConfigPropertyKey keys;

	@BeforeClass
	public static void beforeClass() throws Exception {
		SchemaUtils.restoreTable("SystemPreferences");
		setupIntegration();
	}

	@Before
	public void before() {
		keys = integrationDao.getIntegration(INTEGRATION_MODULE).getKeys();
	}

	@Test
	public void testMissingApiKey() {
		p.setProperty(keys.API_KEY, "");
		ConfigProperty config = configPropertyDao.getConfig(keys);
		assertEquals("", config.getApiKey());
	}

	@Test
	public void testHasApiKey() {
		p.setProperty(keys.API_KEY, "12345");
		ConfigProperty config = configPropertyDao.getConfig(keys);
		assertEquals("12345", config.getApiKey());
	}

	@Test
	public void testMissingClientId() {
		p.setProperty(keys.CLIENT_ID, "");
		ConfigProperty config = configPropertyDao.getConfig(keys);
		assertEquals("", config.getClientId());
	}

	@Test
	public void testHasClientId() {
		p.setProperty(keys.CLIENT_ID, "12345");
		ConfigProperty config = configPropertyDao.getConfig(keys);
		assertEquals("12345", config.getClientId());
	}

	@Test
	public void testEnabled() {
		ConfigProperty config;
		prefDao.mergeOrPersist(keys.ENABLED, "false");
		config = configPropertyDao.getConfig(keys);
		assertFalse(config.isEnabled());
		prefDao.mergeOrPersist(keys.ENABLED, "true");
		config = configPropertyDao.getConfig(keys);
		assertTrue(config.isEnabled());
	}

	@Test
	public void testConfigured() {
		ConfigProperty config;
		prefDao.mergeOrPersist(keys.CONFIGURED, "false");
		config = configPropertyDao.getConfig(keys);
		assertFalse(config.isConfigured());
		prefDao.mergeOrPersist(keys.CONFIGURED, "true");
		config = configPropertyDao.getConfig(keys);
		assertTrue(config.isConfigured());
	}

	@Test
	public void testDisplayName() {
		prefDao.mergeOrPersist(keys.DISPLAY_NAME, "TEST");
		ConfigProperty config = configPropertyDao.getConfig(keys);
		assertEquals("TEST", config.getDisplayName());
	}

	@Test
	public void testSoapProviderNo() {
		prefDao.mergeOrPersist(keys.SOAP_PROVIDER_NO, "123456");
		ConfigProperty config = configPropertyDao.getConfig(keys);
		assertEquals("123456", config.getSoapProviderNo());
	}

	@Test
	public void testRestClientId() {
		prefDao.mergeOrPersist(keys.REST_CLIENT_ID, "98765");
		ConfigProperty config = configPropertyDao.getConfig(keys);
		assertEquals(new Integer(98765), config.getRestClientId());
	}

	@Test
	public void testMissingRestApiKey() {
		prefDao.mergeOrPersist(keys.REST_API_KEY, "");
		ConfigProperty config = configPropertyDao.getConfig(keys);
		assertEquals("", config.getRestApiKey());
	}

	@Test
	public void testHasRestApiKey() {
		prefDao.mergeOrPersist(keys.REST_API_KEY, "12345");
		ConfigProperty config = configPropertyDao.getConfig(keys);
		assertEquals("12345", config.getRestApiKey());
	}
}
