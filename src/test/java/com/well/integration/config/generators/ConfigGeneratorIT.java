/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config.generators;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import com.well.integration.IntegrationDao;
import com.well.integration.IntegrationManager;
import com.well.integration.IntegrationTestUtils;
import com.well.integration.config.parameters.ConfigParams;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.ServiceClient;
import org.oscarehr.util.SpringUtils;

public class ConfigGeneratorIT extends DaoTestFixtures {

    IntegrationManager manager = SpringUtils.getBean(IntegrationManager.class);
    IntegrationDao integrationDao = SpringUtils.getBean(IntegrationDao.class);
    ConfigGenerator generator;


    @BeforeClass
    public static void beforeClass() throws Exception {
        IntegrationTestUtils.restoreIntegrationTables();
        IntegrationTestUtils.setupIntegration();
    }

    @Before
    public void before() throws Exception {
        generator = new ConfigGenerator();
    }

    @Test
    public void testGenerateConfig() {
        List<String> names = Arrays.asList("Insig Integration", "Insig");
        ConfigGenerator generator = spy(new ConfigGenerator());
        ConfigParams config = generator.generateConfig(manager, integrationDao.getIntegration("insig"));
        verify(generator).populateCredentials(manager, names, config);
    }

    @Test
    public void testPopulateCredentialsProviderFound() throws Exception {
        Provider p = IntegrationTestUtils.createTestProvider("Insig", "Insig");
                ConfigParams config = generator.generateConfig(manager, integrationDao.getIntegration("insig"));
        assertEquals(p.getProviderNo(), config.getSoapProviderNo());
        assertEquals(p.getFirstName(), config.getSoapProviderFirstName());
        assertEquals(p.getLastName(), config.getSoapProviderLastName());
    }

    @Test
    public void testPopulateCredentialsProviderNotFound() throws Exception {
        SchemaUtils.restoreTable("provider");
        ConfigParams config = generator.generateConfig(manager, integrationDao.getIntegration("insig"));
        assertEquals("create", config.getSoapProviderNo());
        assertEquals("Integration", config.getSoapProviderFirstName());
        assertEquals("Insig", config.getSoapProviderLastName());
    }

    @Test
    public void testPopulateCredentialsServiceClientFound() throws Exception {
        SchemaUtils.restoreTable("ServiceClient");
        ServiceClient sc = IntegrationTestUtils.createTestServiceClient("Insig");
        ConfigParams config = generator.generateConfig(manager, integrationDao.getIntegration("insig"));
        assertEquals(sc.getId().toString(), config.getRestClientId());
        assertEquals(sc.getName(), config.getRestClientName());

    }

    @Test
    public void testPopulateCredentialsServiceClientNotFound() throws Exception {
        SchemaUtils.restoreTable("ServiceClient");
        ConfigParams config = generator.generateConfig(manager, integrationDao.getIntegration("insig"));
        assertEquals("create", config.getRestClientId());
        assertEquals("Insig Integration", config.getRestClientName());
    }
}
