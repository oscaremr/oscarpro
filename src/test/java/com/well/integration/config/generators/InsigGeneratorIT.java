/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config.generators;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import com.well.integration.IntegrationDao;
import com.well.integration.IntegrationManager;
import com.well.integration.IntegrationTestUtils;
import com.well.integration.config.parameters.ConfigParams;
import com.well.integration.config.parameters.InsigConfigParams;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.util.SpringUtils;

public class InsigGeneratorIT extends ConfigGeneratorIT {

    SystemPreferencesDao preferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
    IntegrationManager manager = SpringUtils.getBean(IntegrationManager.class);
    IntegrationDao integrationDao = SpringUtils.getBean(IntegrationDao.class);

    @BeforeClass
    public static void beforeClass() throws Exception {
        IntegrationTestUtils.restoreIntegrationTables();
        IntegrationTestUtils.setupIntegration();
    }

    @Override
    @Before
    public void before() throws Exception {
        generator = new InsigGenerator();
    }

    @Override
    @Test
    public void testGenerateConfig() {
        List<String> names = Arrays.asList("Insig Integration", "Insig");
        ConfigGenerator generator = spy(new ConfigGenerator());
        ConfigParams config = generator.generateConfig(manager, integrationDao.getIntegration("insig"));
        verify(generator).populateCredentials(manager, names, config);
    }

    @Override
    @Test
    public void testPopulateCredentialsProviderNotFound() throws Exception {
        SchemaUtils.restoreTable("provider");
        ConfigParams config = generator.generateConfig(manager, integrationDao.getIntegration("insig"));
        assertEquals("create", config.getSoapProviderNo());
        assertEquals("Care", config.getSoapProviderFirstName());
        assertEquals("Virtual", config.getSoapProviderLastName());
    }

    @Test
    public void populateThirdPartyLinkPreferencesFound() {
        preferencesDao.mergeOrPersist("schedule_tp_link_enabled", "true");
        preferencesDao.mergeOrPersist("schedule_tp_link_type", "test1");
        preferencesDao.mergeOrPersist("schedule_tp_link_display", "test2");
        InsigConfigParams config = (InsigConfigParams) generator.generateConfig(manager, integrationDao.getIntegration("insig"));
        assertTrue(config.isTpLinkEnabled());
        assertEquals("test1", config.getTpLinkType());
        assertEquals("test2", config.getTpLinkDisplay());
    }

    @Test
    public void populateThirdPartyLinkPreferencesNotFound() {
        preferencesDao.mergeOrPersist("schedule_tp_link_enabled", "false");
        InsigConfigParams config = (InsigConfigParams) generator.generateConfig(manager, integrationDao.getIntegration("insig"));
        assertTrue(config.isTpLinkEnabled());
        assertEquals("Virtual", config.getTpLinkType());
        assertEquals("VC+", config.getTpLinkDisplay());
    }
}
