/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ConfigPropertyKeyTest {

    private ConfigPropertyKey key;

    @Before
    public void before() {
        key = new ConfigPropertyKey("test");
    }

    @Test
    public void testPrefix() {
        assertEquals("test", key.getPrefix());
    }

    @Test
    public void testGetKey() {
        assertEquals("test.integration.api_key", key.getKey(ConfigPropertyKey.Keys.API_KEY));
        assertEquals("test.integration.api_key_name", key.getKey(ConfigPropertyKey.Keys.API_KEY_NAME));
        assertEquals("test.integration.base_url", key.getKey(ConfigPropertyKey.Keys.BASE_URL));
        assertEquals("test.integration.client_id", key.getKey(ConfigPropertyKey.Keys.CLIENT_ID));
        assertEquals("test.integration.client_id_name", key.getKey(ConfigPropertyKey.Keys.CLIENT_ID_NAME));
        assertEquals("test.integration.configured", key.getKey(ConfigPropertyKey.Keys.CONFIGURED));
        assertEquals("test.integration.display_name", key.getKey(ConfigPropertyKey.Keys.DISPLAY_NAME));
        assertEquals("test.integration.enabled", key.getKey(ConfigPropertyKey.Keys.ENABLED));
        assertEquals("test.integration.rest_api_key", key.getKey(ConfigPropertyKey.Keys.REST_API_KEY));
        assertEquals("test.integration.rest_client_id", key.getKey(ConfigPropertyKey.Keys.REST_CLIENT_ID));
        assertEquals("test.integration.soap_provider_no", key.getKey(ConfigPropertyKey.Keys.SOAP_PROVIDER_NO));
        assertEquals("test.integration.subscriptions.flushed", key.getKey(ConfigPropertyKey.Keys.SUBSCRIPTIONS_FLUSHED));
        assertEquals("test.integration.subscriptions.status", key.getKey(ConfigPropertyKey.Keys.SUBSCRIPTIONS_STATUS));
    }
    
    @Test
    public void testConstants() {
        assertEquals("test.integration.api_key", key.API_KEY);
        assertEquals("test.integration.api_key_name", key.API_KEY_NAME);
        assertEquals("test.integration.base_url", key.BASE_URL);
        assertEquals("test.integration.client_id", key.CLIENT_ID);
        assertEquals("test.integration.client_id_name", key.CLIENT_ID_NAME);
        assertEquals("test.integration.configured", key.CONFIGURED);
        assertEquals("test.integration.display_name", key.DISPLAY_NAME);
        assertEquals("test.integration.enabled", key.ENABLED);
        assertEquals("test.integration.rest_api_key", key.REST_API_KEY);
        assertEquals("test.integration.rest_client_id", key.REST_CLIENT_ID);
        assertEquals("test.integration.soap_provider_no", key.SOAP_PROVIDER_NO);
        assertEquals("test.integration.subscriptions.flushed", key.SUBSCRIPTIONS_FLUSHED);
        assertEquals("test.integration.subscriptions.status", key.SUBSCRIPTIONS_STATUS);
    }
    
    @Test
    public void testGetKeys() {
        String[] keys = {
                "test.integration.api_key",
                "test.integration.api_key_name",
                "test.integration.base_url",
                "test.integration.client_id",
                "test.integration.client_id_name",
                "test.integration.configured",
                "test.integration.display_name",
                "test.integration.enabled",
                "test.integration.rest_api_key",
                "test.integration.rest_client_id",
                "test.integration.soap_provider_no",
                "test.integration.subscriptions.flushed",
                "test.integration.subscriptions.status",
                "test.integration.subscriptions.pg_size",
                "test.integration.subscriptions.capacity",
        };
        assertArrayEquals(keys, key.getKeys().toArray(new String[keys.length]));
    }
}
