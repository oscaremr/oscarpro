/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration.config;

import org.junit.Test;
import org.oscarehr.common.dao.utils.EntityDataGenerator;

import static org.junit.Assert.*;

public class ConfigPropertyConverterTest {

    private ConfigPropertyConverter converter = new ConfigPropertyConverter();

    @Test(expected = UnsupportedOperationException.class)
    public void testGetAsDomainObjectUnsupported() {
        ConfigPropertyTo configTo = new ConfigPropertyTo();
        converter.getAsDomainObject(null, configTo);
    }

    @Test
    public void testGetAsTransferObject() throws Exception {
        ConfigProperty config = new ConfigProperty();
        EntityDataGenerator.generateTestDataForModelClass(config);
        ConfigPropertyTo configTo = converter.getAsTransferObject(null, config);
        // Only testing fields that are mapped directly
        assertEquals(configTo.isEnabled(), config.isEnabled());
        assertEquals(configTo.isConfigured(), config.isConfigured());
        assertEquals(configTo.getSoapProviderNo(), config.getSoapProviderNo());
        assertEquals(configTo.getRestClientId(), config.getRestClientId());
    }

    @Test
    public void testGetAsTransferObjectHasApiKey() {
        ConfigProperty config = new ConfigProperty();
        config.setApiKey("1234");
        config.setClientId("5678");
        ConfigPropertyTo configTo = converter.getAsTransferObject(null, config);
        assertTrue(configTo.getHasApiKey());
    }

    @Test
    public void testGetAsTransferObjectMissingApiKey() {
        ConfigProperty config = new ConfigProperty();
        ConfigPropertyTo configTo = converter.getAsTransferObject(null, config);
        config.setClientId("5678");

        // null api key
        assertFalse(configTo.getHasApiKey());

        // empty string
        config.setApiKey("");
        configTo = converter.getAsTransferObject(null, config);
        assertFalse(configTo.getHasApiKey());

        // white space
        config.setApiKey("  ");
        configTo = converter.getAsTransferObject(null, config);
        assertFalse(configTo.getHasApiKey());
    }

    @Test
    public void testGetAsTransferObjectMissingClientIdKey() {
        ConfigProperty config = new ConfigProperty();
        ConfigPropertyTo configTo = converter.getAsTransferObject(null, config);
        config.setApiKey("1234");

        // null api key
        assertFalse(configTo.getHasApiKey());

        // empty string
        config.setClientId("");
        configTo = converter.getAsTransferObject(null, config);
        assertFalse(configTo.getHasApiKey());

        // white space
        config.setClientId("  ");
        configTo = converter.getAsTransferObject(null, config);
        assertFalse(configTo.getHasApiKey());
    }
}

