/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.integration;


import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;

import com.well.integration.client.Request;
import com.well.integration.config.ConfigPropertyKey;
import com.well.integration.config.parameters.ConfigParams;
import com.well.integration.config.parameters.InsigConfigParams;
import com.well.integration.provision.ProvisionRequest;
import com.well.integration.provision.handlers.InsigHandler;
import com.well.integration.provision.handlers.ProvisionHandler;
import java.sql.SQLException;
import org.apache.commons.lang.StringUtils;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.ServiceClientDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.utils.ConfigUtils;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.model.Security;
import org.oscarehr.common.model.ServiceAccessToken;
import org.oscarehr.common.model.ServiceClient;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import utils.OscarPropertiesUtil;

public class IntegrationTestUtils {
    public static final String INTEGRATION_MODULE = "insig";

    public static final String API_KEY        = "f33dc0d3-dead-b33f-7ac0-a11acc01ade5";
    public static final String API_KEY_NAME   = "X-WELL-EMR-API-KEY";
    public static final String CLIENT_ID      = "f33dc0d3-b33f-dead-7ac0-a11acc01ade5";
    public static final String CLIENT_ID_NAME = "X-WELL-EMR-CLIENT-ID";

    public static void restoreIntegrationTables() throws Exception {
        SchemaUtils.restoreTable(true,
                "provider",
                "security",
                "secRole",
                "secUserRole",
                "ServiceAccessToken",
                "ServiceClient",
                "SystemPreferences"
        );
    }

    public static void setupIntegration() {
        setupIntegration(INTEGRATION_MODULE);
    }

    public static void setupIntegration(String module) {
        IntegrationDao integrationDao = SpringUtils.getBean(IntegrationDao.class);
        integrationDao.setIntegrationModules(module);
        Integration integration = integrationDao.getIntegration(module);
        ConfigPropertyKey keys = integration.getKeys();

        OscarProperties p = OscarProperties.getInstance();
        p.setProperty(keys.API_KEY,        API_KEY);
        p.setProperty(keys.API_KEY_NAME,   API_KEY_NAME);
        p.setProperty(keys.CLIENT_ID,      CLIENT_ID);
        p.setProperty(keys.CLIENT_ID_NAME, CLIENT_ID_NAME);
        p.setProperty(keys.BASE_URL, ConfigUtils.getProperty("integration.e2e_base_url"));

        SystemPreferencesDao systemPreferencesDao = SpringUtils.getBean(SystemPreferencesDao.class);
        systemPreferencesDao.mergeOrPersist(keys.DISPLAY_NAME, StringUtils.capitalize(module));
        systemPreferencesDao.mergeOrPersist(keys.ENABLED, "true");
        systemPreferencesDao.mergeOrPersist(keys.CONFIGURED, "true");
        systemPreferencesDao.mergeOrPersist(keys.REST_CLIENT_ID, "1");
        systemPreferencesDao.mergeOrPersist(keys.SOAP_PROVIDER_NO, "911911");

        integrationDao.initializeIntegrations();
    }

    public static ConfigParams getCreateRequest(String integration) {
        IntegrationDao integrationDao = SpringUtils.getBean(IntegrationDao.class);
        Integration integrationObject = integrationDao.getIntegration(integration);
        integrationObject = new Integration(INTEGRATION_MODULE,
                StringUtils.capitalize(INTEGRATION_MODULE), integrationObject.getKeys());
        InsigConfigParams request = new InsigConfigParams(integrationObject);
        request.setRestClientId("create");
        request.setRestClientName("InsigIntegration");
        request.setSoapProviderNo("create");
        request.setSoapProviderPassword("password");
        request.setSoapProviderFirstName("Care");
        request.setSoapProviderLastName("Virtual");
        request.setTpLinkEnabled(true);
        request.setTpLinkType("Virtual");
        request.setTpLinkDisplay("VC+");
        return request;
    }

    public static InsigHandler getFactoryMock(IntegrationManager manager) {
        return getFactoryMock(manager, INTEGRATION_MODULE);
    }
    public static InsigHandler getFactoryMock(IntegrationManager manager, String integration) {
        InsigHandler factory = new InsigHandler(manager, getCreateRequest(integration));
        ServiceClient client = new ServiceClient();

        client.setId(1);
        client.setKey("CONSUMER_KEY");
        client.setSecret("CONSUMER_SECRET");
        factory.setClient(client);
        Provider provider = new Provider();
        provider.setProviderNo("2");
        factory.setProvider(provider);
        ServiceAccessToken serviceAccessToken = new ServiceAccessToken();
        serviceAccessToken.setTokenId("OAUTH_TOKEN");
        serviceAccessToken.setTokenSecret("OAUTH_SECRET");
        factory.setServiceAccessToken(serviceAccessToken);
        Security security = new Security();
        security.setUserName("USER");
        factory.setSecurity(security);
        factory.setPassword("PASSWORD");
        factory.setApiKey("API_KEY");
        return factory;
    }

    public static Request getRequestMock(String integration) {
        ProvisionHandler configHandler = spy(new ProvisionHandler());
        configHandler.setParameters(IntegrationTestUtils.getCreateRequest(integration));
        doReturn(mock(InsigHandler.ProvisionRequest.class)).when(configHandler).getProvisionRequest();
        Request clientRequest = ProvisionRequest.sendConfiguration(configHandler);
        return clientRequest;
    }

    public static Provider createTestProvider(String firstName, String lastName) throws Exception {
        ProviderDao providerDao = SpringUtils.getBean(ProviderDao.class);

        SchemaUtils.restoreTable("provider");
        Provider p = new Provider();
        p.setProviderNo(providerDao.suggestProviderNo());
        p.setLastName(lastName);
        p.setFirstName(firstName);
        p.setProviderType("doctor");
        p.setSex("F");
        p.setSpecialty("Integration");
        p.setStatus("1");
        p.setPrescribeItType("doctor");
        providerDao.saveProvider(p);
        return p;
    }

    public static ServiceClient createTestServiceClient(String name) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        SchemaUtils.restoreTable("ServiceClient");
        ServiceClientDao serviceClientDao = SpringUtils.getBean(ServiceClientDao.class);
        ServiceClient client = new ServiceClient();
        client.setName(name);
        client.setKey("");
        client.setSecret("");
        client.setLifetime(-1);
        serviceClientDao.persist(client);
        return client;
    }

    public static boolean isE2EEnabled() {
        // Force oscar properties to initialize if necessary by retrieving
        // oscar properties via OscarPropertiesUtil
        return OscarPropertiesUtil.getOscarProperties().isPropertyActive("integration.e2e_enabled");
    }

    public static Integration getIntegration() {
        return getIntegration(INTEGRATION_MODULE);
    }

    public static Integration getIntegration(String module) {
        return new Integration(
                module,
                StringUtils.capitalize(module),
                new ConfigPropertyKey(module));
    }
}
