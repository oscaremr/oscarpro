/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package com.well.quatro.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.quatro.util.ServletRequestUtil;
import javax.servlet.ServletRequest;
import org.junit.jupiter.api.Test;

public class ServletRequestUtilTest {

  private final ServletRequest request;

  private final String parameter;

  public ServletRequestUtilTest() {
    this.request = mock(ServletRequest.class);
    this.parameter = "parameter";
  }

  @Test
  public void givenNullParameter_whenGetIntParameterFromRequest_thenReturnZero() {
    // Given
    when(request.getParameter(parameter)).thenReturn(null);
    // When
    int result = ServletRequestUtil.getIntParameterFromRequest(request, parameter);
    // Then
    assertEquals(0, result);
  }

  @Test
  public void givenParameter_whenGetParameterFromRequest_thenReturnIntParameter() {
    // Given
    when(request.getParameter(parameter)).thenReturn("1");
    // When
    int result = ServletRequestUtil.getIntParameterFromRequest(request, parameter);
    // Then
    assertEquals(1, result);
  }

}
