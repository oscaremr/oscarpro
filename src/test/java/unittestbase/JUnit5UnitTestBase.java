package unittestbase;

import org.junit.jupiter.api.Assertions;

/**
 * This is the base class that defines how the unit tests should be implemented using the JUnit 5
 * library.  All JUnit 5 unit tests should extend this class and use the specified packages...
 * <br/>
 *
 * <table>
 *   <tr>
 *     <th style="width:50%">Package</th>
 *     <th style="width:50%">Reason</th>
 *   </tr>
 *   <tr>
 *     <td>org.junit.*</td>
 *     <td>Avoid importing from this package, as it for JUnit 4.</td>
 *   </tr>
 *   <tr>
 *     <td>org.junit.jupiter.api.*</td>
 *     <td>Import from this package for Junit 5 annotations like @Test, @BeforeEach, @AfterEach, etc.</td>
 *   </tr>
 *   <tr>
 *     <td>org.junit.jupiter.api.extension.ExtendWith</td>
 *     <td>Use this annotation with MockitoExtension.class to enable Mockito support in JUnit 5 tests.</td>
 *   </tr>
 *   <tr>
 *     <td>org.powermock.api.mockito</td>
 *     <td>Avoid using this package, as it can cause issues in the build pipeline.</td>
 *    </tr>
 *   <tr>
 *     <td>org.mockito.Mockito</td>
 *     <td>Use this package for creating and working with mock objects in Mockito.</td>
 *   </tr>
 * </table>
 */

public abstract class JUnit5UnitTestBase extends Assertions{

}