package http;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import ca.oscarpro.common.http.OscarClassicHttpClient;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.cert.Certificate;
import javax.net.ssl.SSLContext;

import ca.oscarpro.common.util.CertificateUtils;
import lombok.val;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import oscar.OscarProperties;

@ExtendWith(MockitoExtension.class)
public class OscarClassicHttpClientTest {

  @TempDir
  private Path folder;

  OscarClassicHttpClient oscarClassicHttpClient = new OscarClassicHttpClient();

  private MockedStatic<CertificateUtils> certificateUtils;
  private MockedStatic<SSLContexts> mockedSSLContexts;
  private MockedStatic<KeyStore> keyStoreMockedStatic;
  private MockedStatic<OscarProperties> oscarPropertiesMockedStatic;

  @BeforeEach
  public void before() throws Exception {
    final String certificateHeader = "CERTIFICATE_HEADER";
    SSLContext sslContext = SSLContext.getInstance("TLS");
    sslContext.init(null, null, null);

    keyStoreMockedStatic = mockStatic(KeyStore.class);
    val keyStoreMock = mock(KeyStore.class);
    when(KeyStore.getInstance(anyString()))
        .thenAnswer((Answer<KeyStore>) invocation -> keyStoreMock);

    mockedSSLContexts = mockStatic(SSLContexts.class);
    when(SSLContexts.createDefault())
        .thenReturn(sslContext);

    val sslContextBuilder = mock(SSLContextBuilder.class);
    sslContextBuilder.loadKeyMaterial(any(KeyStore.class), any(char[].class));
    when(SSLContexts.custom())
        .thenAnswer((Answer<SSLContextBuilder>) invocation -> sslContextBuilder);

    certificateUtils = mockStatic(CertificateUtils.class);
    when(CertificateUtils.encodeCertificateAsBase64Url(any(Certificate.class)))
        .thenReturn(certificateHeader);
  }

  @AfterEach
  public void close(){
    certificateUtils.close();
    mockedSSLContexts.close();
    keyStoreMockedStatic.close();
    oscarPropertiesMockedStatic.close();
  }

  @Test
  public void givenKeystore_whenGetHttpClient_thenReturnValidClient() throws Exception {
    Path newFilePath = folder.resolve("keystore.jks");
    val keyStore = Files.createFile(newFilePath);

    oscarPropertiesMockedStatic = mockStatic(OscarProperties.class);
    val properties = mock(OscarProperties.class);
    when(OscarProperties.getInstance()).thenAnswer(
        (Answer<OscarProperties>) invocation -> properties);
    when(properties.getProperty(anyString()))
        .thenReturn(keyStore.toFile().getAbsolutePath());
    when(properties.getBooleanProperty(anyString(), anyString()))
        .thenReturn(true);
    val httpClient = oscarClassicHttpClient.getHttpClient();
    Assertions.assertNotNull(httpClient);
  }

  @Test
  public void givenEhrKeystoreDisabled_whenGetHttpClient_thenReturnValidClient() {
    oscarPropertiesMockedStatic = mockStatic(OscarProperties.class);
    val properties = mock(OscarProperties.class);
    when(OscarProperties.getInstance()).thenAnswer(
        (Answer<OscarProperties>) invocation -> properties);
    when(properties.getBooleanProperty(anyString(), anyString()))
        .thenReturn(false);
    val httpClient = oscarClassicHttpClient.getHttpClient();
    Assertions.assertNotNull(httpClient);
  }
}