/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package unittesting.org.oscarehr.managers;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import com.quatro.dao.security.SecobjprivilegeDao;
import com.quatro.dao.security.SecuserroleDao;
import com.quatro.model.security.Secobjprivilege;
import com.quatro.model.security.Secuserrole;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.oscarehr.common.model.Provider;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;

@ExtendWith(MockitoExtension.class)
public class SecurityInfoManagerTest {

  @InjectMocks
  private SecurityInfoManager securityInfoManager;

  private Provider provider;

  @Mock
  private HttpServletRequest request;

  @Mock
  private SecuserroleDao secUserRoleDao;

  @Mock
  private SecobjprivilegeDao secobjprivilegeDao;

  private MockedStatic<LoggedInInfo> loggedInInfoMockedStatic;

  @BeforeEach
  public void before() {
    provider = new Provider();
    provider.setProviderNo("1");

    List<Secobjprivilege> secObjPrivilegeList = new ArrayList<>();
    secObjPrivilegeList.add(setupPrivilege());

    List<Secuserrole> secUserRoleList = new ArrayList<>();
    secUserRoleList.add(setupSecUserRole());

    loggedInInfoMockedStatic = mockStatic(LoggedInInfo.class);
    loggedInInfoMockedStatic
        .when(() -> LoggedInInfo.getLoggedInInfoFromSession(request))
        .thenAnswer(
            (Answer<LoggedInInfo>) invocation -> {
              LoggedInInfo loggedInInfo = new LoggedInInfo();
              loggedInInfo.setLoggedInProvider(provider);
              return loggedInInfo;
            });

    when(secUserRoleDao.findByProviderNo(any())).thenReturn(secUserRoleList);
    when(secobjprivilegeDao.getByRoles(Mockito.anyList())).thenReturn(secObjPrivilegeList);
  }
  @AfterEach
  public void after() {
    loggedInInfoMockedStatic.close();
  }

  @Test
  public void testCheckRoleHasNoRights_TrueThrowsSecurityException() {
    assertThrows(
        SecurityException.class,
        () -> securityInfoManager.checkRoleHasNoRights(getLoggedInInfo(), "test"));
  }

  @Test
  public void testCheckRoleHasNoRights_DoesNotThrowSecurityException() {
    Secobjprivilege secObjPrivilege = new Secobjprivilege();
    secObjPrivilege.setObjectname_code("test");
    secObjPrivilege.setPrivilege_code("r");
    secObjPrivilege.setRoleusergroup("User Group");

    List<Secobjprivilege> secObjList = new ArrayList<>();
    secObjList.add(secObjPrivilege);

    when(secobjprivilegeDao.getByRoles(Mockito.anyList())).thenReturn(secObjList);

    assertDoesNotThrow(() -> securityInfoManager.checkRoleHasNoRights(getLoggedInInfo(), "test"));
  }

  private Secobjprivilege setupPrivilege() {
    Secobjprivilege secObjPrivilege = new Secobjprivilege();

    secObjPrivilege.setPriority(0);
    secObjPrivilege.setObjectname_code("test");
    secObjPrivilege.setProviderNo("1");
    secObjPrivilege.setPrivilege_code("o");
    secObjPrivilege.setRoleusergroup("User Group");

    return secObjPrivilege;
  }

  private LoggedInInfo getLoggedInInfo() {
    LoggedInInfo loggedInInfo = new LoggedInInfo();

    loggedInInfo.setLoggedInProvider(provider);

    return loggedInInfo;
  }

  private Secuserrole setupSecUserRole() {
    Secuserrole secUserRole = new Secuserrole();

    secUserRole.setRoleName("User Group");
    secUserRole.setProviderNo("1");

    return secUserRole;
  }
}
