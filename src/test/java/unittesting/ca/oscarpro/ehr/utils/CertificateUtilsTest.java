package unittesting.ca.oscarpro.ehr.utils;

import static ca.oscarpro.common.util.CertificateUtils.EHR_KEYSTORE_FILE;
import static ca.oscarpro.common.util.CertificateUtils.EHR_KEYSTORE_PASSWORD;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import ca.oscarpro.common.util.CertificateUtils;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateExpiredException;
import lombok.val;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import oscar.OscarProperties;

@ExtendWith(MockitoExtension.class)
public class CertificateUtilsTest {

  public static final String KEYSTORE_PASSWORD = "changeit";
  public static final String URL_ENCODED_CERTIFICATE =
          "-----BEGIN+CERTIFICATE-----%0AMIIDZTCCAk2gAwIBAgIEf%2Fh7mzANBgkqhkiG9w0B"
          + "AQsFADBiMQswCQYDVQQGEwJDQTEQMA4GA1UECBMHT250YXJpbzEQMA4GA1UEBxMHVG9yb25"
          + "0bzERMA8GA1UEChMIVGVzdCBMdGQxDTALBgNVBAsTBFRlc3QxDTALBgNVBAMTBFRlc3QwIB"
          + "cNMjMxMTE3MTQ1MzI3WhgPMjA1MTA0MDQxNDUzMjdaMGIxCzAJBgNVBAYTAkNBMRAwDgYDV"
          + "QQIEwdPbnRhcmlvMRAwDgYDVQQHEwdUb3JvbnRvMREwDwYDVQQKEwhUZXN0IEx0ZDENMAsG"
          + "A1UECxMEVGVzdDENMAsGA1UEAxMEVGVzdDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQo"
          + "CggEBAIleSn3my6DiXWe64bVhUEonF21ejYduKc3Z%2F%2FsBRo3MHRDMz3TUhHhwdVUtqT"
          + "OsZRVlmCdfCrYU3ZS2puVEUTiGyJ232f43XlDuoIwJIYEYVBWVe0jDLwjiETQUiwA5bC1oR"
          + "UYupgVykQmswOFi%2FO%2F0UCfxwKnnO9nLHMDSVTy%2FhLas5W3gqWe96ilgipFNN9%2Fk"
          + "e0L2PywBYNPm3bldrH7UnHP0aa0l%2BuSvpe2SnpdaVi18%2Fa2RuPDRUr3Hh5qGjerAqBI"
          + "1XnzvM3ryhm5BsG3e4fRpyD4k4Zm0jHludAbVp3kR0%2Bm5D7%2Bw1AQZlgBX1FybkoXxBD"
          + "yGlkG7rJa%2FTyUCAwEAAaMhMB8wHQYDVR0OBBYEFIb%2FXoz6r0GtMmumwMbDhy8bSUiDM"
          + "A0GCSqGSIb3DQEBCwUAA4IBAQBISVpj92r7n5e84fI7QGmdDM4NbYBZqmPsXjGQlJoITe2M"
          + "TPSIioB9KsBKljlZkEITwXJ51NDxBdZLRXvtdXkYyWRbh7K9z8TRGrIK3%2BCJ1Zq%2BPf2"
          + "s12K98mDFhZ%2BzEdM9jcUreBdjD1aIfbi4wOnYUzdKks3hRBjRpPhA%2BIhHc6nJ9Rx3He"
          + "DhCMolg75DN1%2FIuAHj0BX1u2NZstKe30JygGnDi6pgjm97MlnFfBCgQG6NzuHQHy63M6L"
          + "G1JHiNS%2BLMYYl%2BGd9Kudb8Qb1bB2WkETJGVkf2EeDV7SWv6N9PEP1rl6DAWYi9Xjq39"
          + "oR6q%2FhP9qzWeQR2FjhmUa9o1em%0A-----END+CERTIFICATE-----";

  public static final String INVALID_CERTIFICATE =
          "-----BEGIN+CERTIFICATE-----%0AMIIDYzCCAkugAwIBAgIEAPaKsjANBgkqhkiG9w0BA"
          + "QsFADBiMQswCQYDVQQGEwJDQTEQMA4GA1UECBMHT250YXJpbzEQMA4GA1UEBxMHVG9yb25"
          + "0bzERMA8GA1UEChMIVGVzdCBMdGQxDTALBgNVBAsTBFRlc3QxDTALBgNVBAMTBFRlc3QwH"
          + "hcNMjMxMDE2MTQzMjMzWhcNMjMxMDE3MTQzMjMzWjBiMQswCQYDVQQGEwJDQTEQMA4GA1U"
          + "ECBMHT250YXJpbzEQMA4GA1UEBxMHVG9yb250bzERMA8GA1UEChMIVGVzdCBMdGQxDTAL%"
          + "0D%0ABgNVBAsTBFRlc3QxDTALBgNVBAMTBFRlc3QwggEiMA0GCSqGSIb3DQEBAQUAA4IBD"
          + "wAwggEKAoIBAQCXIIJQQ%2Fwu2NR4Pj%2F04CadYltcBw6uXoz1jRevojJGMmOIvEOOMgy"
          + "v2WNOfhVcoLRkUO79t8rGYuuwjiE%2Bdr3VZe1ulOVt2pOTNKv5sZAWoSxPZfmmPFCmk9L"
          + "jTnuF0aoI66Dzq%2FReOMs7yPS8%2FkIaL%2BfhSigBVfcd85tAEz4Sa24SGBN1IJTS7XW"
          + "7NRmNixu5DDYW0zJWDzWcZb%2BaymBm35NsIOJ4JhABT%2Bx%2FpQIN4gFw%2BE%2BTFJp"
          + "iK6wUu2tBIs3R9o%2ByX8LUQlXmBClFcM15K1Pf%2FBqerkq7Rdzhp8ExTVOpoxrwLBUg2"
          + "RkBdlN0Kznc4NlF9PZNp3f3zUpZnCztAgMBAAGjITAfMB0GA1UdDgQWBBRXd09O%2B9fvc"
          + "CJQI3Ehuhy4hHgorTANBgkqhkiG9w0BAQsFAAOCAQEAVy6lh20j5RxMGpu2az8Mx%2Bjpz"
          + "2qPf%2FcbYFmCs91Cm5jHDXdyC8I4DvrJgSVCmy2uxSJ1yIIBnSR2jMP4DkuGpRnZ7JhXK"
          + "GdHULqLJG7o7bj7wkesce5zuARhFBP0O9LGyGG52jm%2B6ABiLxaLn6fGlspInXQStQk7u"
          + "HzlHMpJ5CxlGWW%2FqdFb6xnST1tbfFOCjqNXIvHeJkWOgrDPuBSRrW2kRjcqbl1819Al%"
          + "2FOgJRX31AwhaE22Esuf2L4yfSYrTN8nT9jvjRZpfbR2RTtnbP1QFmry1PvZWAPema0FAa"
          + "AF2BTH9Ty8UVrCJpwS27dZQ4EfeKVuXeAoMmUpLgncsHQ%3D%3D%0A-----END+CE"
          + "RTIFICATE-----";

  @Mock
  private OscarProperties mockOscarProperties;
  private MockedStatic<OscarProperties> mockStaticOscarProperties;

  @BeforeEach
  public void before() {
    mockStaticOscarProperties = mockStatic(OscarProperties.class);
    mockStaticOscarProperties.when(OscarProperties::getInstance).thenReturn(mockOscarProperties);
  }

  @AfterEach
  public void after() {
    mockStaticOscarProperties.close();
  }

  @Test
  public void givenUrlEncodedCert_whenDecodeUrlEncodedCertificate_thenReturnCertificate()
      throws Exception {
    val certificate = CertificateUtils.decodeUrlEncodedCertificate(URL_ENCODED_CERTIFICATE);
    assertNotNull(certificate);
    assertEquals(certificate.length, 1);
    assertEquals(
        "CN=Test,OU=Test,O=Test Ltd,L=Toronto,ST=Ontario,C=CA",
        certificate[0].getSubjectX500Principal().getName()
    );
  }

  @Test
  public void givenCertificate_whenEncodeCertificateAsBase64Url_thenReturnEncodedCertificate() throws Exception {
    val certificates = CertificateUtils.decodeUrlEncodedCertificate(URL_ENCODED_CERTIFICATE);
    val encodedCertificate = CertificateUtils.encodeCertificateAsBase64Url(certificates[0]);
    Assertions.assertNotNull(encodedCertificate);
    Assertions.assertEquals(URL_ENCODED_CERTIFICATE, encodedCertificate);
  }

  @Test
  public void givenValidCertificate_whenIsValidCertificate_thenVerifyCertificate() throws Exception {
    val certificates = CertificateUtils.decodeUrlEncodedCertificate(URL_ENCODED_CERTIFICATE);
    MockedStatic<KeyStore> mockStatic = mockStatic(KeyStore.class);
    val keyStoreMock = mock(KeyStore.class);
    mockStatic.when(() -> KeyStore.getInstance(anyString())).thenReturn(keyStoreMock);
    doNothing().when(keyStoreMock)
        .load(any(FileInputStream.class), any(char[].class));
    when(keyStoreMock.getCertificateAlias(any(Certificate.class)))
        .thenReturn("test-alias");
    when(keyStoreMock.getCertificate(anyString())).thenReturn(certificates[0]);

    try (MockedConstruction<FileInputStream> inputStreamMockedConstruction =
        mockConstruction(FileInputStream.class)) {

      when(mockOscarProperties.getProperty(eq(EHR_KEYSTORE_FILE)))
          .thenReturn("keystore.jks");
      when(mockOscarProperties.getProperty(eq(EHR_KEYSTORE_PASSWORD)))
          .thenReturn(KEYSTORE_PASSWORD);

      Assertions.assertDoesNotThrow(() -> {
        CertificateUtils.validateCertificate(certificates[0]);
      });
    }
  }

  @Test
  public void givenInvalidCertificate_whenIsValidCertificate_thenCertificateExpiredException() {
    assertThrows(
        CertificateExpiredException.class,
        () -> {
          val certificates = CertificateUtils.decodeUrlEncodedCertificate(INVALID_CERTIFICATE);
          CertificateUtils.validateCertificate(certificates[0]);
        });
  }
}