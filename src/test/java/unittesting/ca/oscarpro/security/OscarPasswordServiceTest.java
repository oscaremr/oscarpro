/**
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package unittesting.ca.oscarpro.security;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import ca.oscarpro.security.BannedPasswordService;
import ca.oscarpro.security.EncryptionException;
import ca.oscarpro.security.EncryptionProvider;
import ca.oscarpro.security.OscarPasswordService;
import ca.oscarpro.security.PasswordConfig;
import ca.oscarpro.security.PasswordValidationException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import lombok.val;
import lombok.var;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.oscarehr.common.dao.SecurityDao;
import org.oscarehr.common.model.Security;
import org.oscarehr.managers.OktaManager;
import org.springframework.test.util.ReflectionTestUtils;
import oscar.OscarProperties;

@ExtendWith(MockitoExtension.class)
public class OscarPasswordServiceTest extends PasswordConfig {

  private static final String PASSWORD_ENCRYPTED_V1
      = "T3SdVNEvzsOsM5ltN2fQm/fEvLCosvud2imOLUtbU7mzPk98V4rSFJ+CqSZoKDOW1rM6FbK88Afc3Q/HgcdTnQ==";
  private static final String PASSWORD_ENCRYPTED_V2
      = "gCG7iIh/0cu8oFnNW0s2eVW36WygJWHexj77xkGKfelS5KgTTJzVqvUmyukK5qn/ko42gFSJr+CVu1bWwdp2BA==";
  private static final String PASSWORD_ENCRYPTED_CURRENT = PASSWORD_ENCRYPTED_V2;
  private static final String PASSWORD_PLAIN = "Test1234!";
  private static final String PASSWORD_DIGEST
      = "75-48116-4966-102-7684-51123-18116-6681858-109-51-118-87";
  private static final String CIPHER_ALGORITHM = "PBEWITHSHA256AND128BITAES-CBC-BC";
  private static final String CIPHER_PASSWORD = "012345678901234567890123456789";
  private static final String CIPHER_SALT = "0123456789abcdef";
  private static final String PASSWORD_MIN_LENGTH = "8";
  private static final String PASSWORD_MIN_GROUPS = "3";
  private static final int VERSION_CURRENT = 2;

  private MockedStatic<OscarProperties> oscarPropertiesMockedStatic;
  private MockedStatic<BannedPasswordService> bannedPasswordServiceMockedStatic;

  OscarPasswordService instance;

  @Mock
  OscarProperties properties;

  @Mock
  OktaManager oktaManager;

  @Mock
  SecurityDao securityDao;

  @Mock
  EncryptionProvider encryptionProvider;

  @BeforeEach
  public void before() throws EncryptionException {
    // Initialized before test because PasswordService is
    // a singleton class.
    instance = spy(OscarPasswordService.class);
    val current = PasswordConfig.Version.getCurrent();
    current.setPropertiesFile(null);
    current.setCipherAlgorithm(CIPHER_ALGORITHM);
    current.setCipherPassword(CIPHER_PASSWORD);
    current.setCipherSalt(CIPHER_SALT);
    encryptionProvider = EncryptionProvider.initialize(current);
    oscarPropertiesMockedStatic = mockStatic(OscarProperties.class);
    val answer = (Answer<OscarProperties>) invocation -> properties;
    when(OscarProperties.getInstance()).thenAnswer(answer);
    ReflectionTestUtils.setField(OscarPasswordService.class, "oscarProperties", properties);
    ReflectionTestUtils.setField(OscarPasswordService.class, "securityDao", securityDao);
    ReflectionTestUtils.setField(OscarPasswordService.class, "oktaManager", oktaManager);
    bannedPasswordServiceMockedStatic = mockStatic(BannedPasswordService.class);
    when(BannedPasswordService.isPasswordBanned(anyString()))
        .thenAnswer(mockIsBannedPassword(false));
  }

  @AfterEach
  public void close(){
    oscarPropertiesMockedStatic.close();
    bannedPasswordServiceMockedStatic.close();
  }

  @Test
  public void givenPasswordEncryptionDisabled_whenPasswordPlain_thenMatchPassword() {
    mockIsPasswordEncryptionEnabled(false);
    var security = mockSecurity(1, PASSWORD_DIGEST, 0);
    assertTrue(OscarPasswordService.isMatch(PASSWORD_PLAIN, security));
    security = mockSecurity(1, PASSWORD_ENCRYPTED_V1, 1);
    assertTrue(OscarPasswordService.isMatch(PASSWORD_PLAIN, security));
    security = mockSecurity(1, PASSWORD_ENCRYPTED_V2, 2);
    assertTrue(OscarPasswordService.isMatch(PASSWORD_PLAIN, security));
  }

  @Test
  public void givenPasswordEncryptionEnabled_whenPasswordPlain_thenMatchPassword() {
    mockIsPasswordEncryptionEnabled(true);
    var security = mockSecurity(1, PASSWORD_DIGEST, 0);
    assertTrue(OscarPasswordService.isMatch(PASSWORD_PLAIN, security));
    security = mockSecurity(1, PASSWORD_ENCRYPTED_V1, 1);
    assertTrue(OscarPasswordService.isMatch(PASSWORD_PLAIN, security));
    security = mockSecurity(2, PASSWORD_ENCRYPTED_V2, 2);
    assertTrue(OscarPasswordService.isMatch(PASSWORD_PLAIN, security));
  }

  @Test
  public void givenPasswordEncryptionEnabled_whenPassword_thenMatchWithOlderPasswordVersion() throws EncryptionException {
    mockIsPasswordEncryptionEnabled(true);
    val security = mockSecurity(1, PASSWORD_ENCRYPTED_V1, 1);
    val instance = OscarPasswordService.getInstance();
    assertTrue(security.getPasswordVersion() < instance.getVersion(),
        "Test should be with security record encoded with prior version.");
    assertTrue(OscarPasswordService.isMatch(PASSWORD_PLAIN, security));
  }

  @Test
  public void givenPasswordEncryptionDisabled_whenTryUpgradePassword_thenNotUpgradePassword() {
    mockIsPasswordEncryptionEnabled(false);
    val modifiedSecurity = mockSecurityListMixed();
    OscarPasswordService.upgradePasswords();
    assertEquals(mockSecurityListMixed(), modifiedSecurity);
  }

  @Test
  public void givenPasswordEncryptionEnabled_whenTryUpgradePassword_thenNotUpgradePasswordIfAllAtLatest() {
    mockIsPasswordEncryptionEnabled(true);
    val modifiedSecurity = mockSecurityListMixed();
    when(securityDao.findWithOlderPasswordVersion()).thenReturn(modifiedSecurity);
    when(oktaManager.isOktaEnabled()).thenReturn(true);
    OscarPasswordService.upgradePasswords();
    assertEquals(mockSecurityListAtLatest(), modifiedSecurity);
  }

  @Test
  public void givenPasswordEncryptionEnabled_whenTryUpgradePassword_thenUpgradePasswordsFromInitialToLatest() {
    mockIsPasswordEncryptionEnabled(true);
    val modifiedSecurity = mockSecurityListAtInitial();
    when(securityDao.findWithOlderPasswordVersion()).thenReturn(modifiedSecurity);
    when(oktaManager.isOktaEnabled()).thenReturn(true);
    OscarPasswordService.upgradePasswords();
    assertEquals(mockSecurityListAtLatest(), modifiedSecurity);
  }

  @Test
  public void givenPasswordEncryptionEnabled_whenTryUpgradePassword_thenUpgradePasswordsAfterInitial() {
    mockIsPasswordEncryptionEnabled(true);
    val modifiedSecurity = mockSecurityListMixed();
    when(securityDao.findWithOlderPasswordVersion()).thenReturn(modifiedSecurity);
    when(oktaManager.isOktaEnabled()).thenReturn(true);
    OscarPasswordService.upgradePasswords();
    assertEquals(mockSecurityListAtLatest(), modifiedSecurity);
  }

  @Test
  public void givenPasswordEncryptionEnabled_whenTryUpgradePassword_thenUpgradePasswordsWhenOktaIsEnabled() {
    mockIsPasswordEncryptionEnabled(true);
    val originalSecurity = Collections.singletonList(mockSecurity(1, "*", 1));
    val modifiedSecurity = Collections.singletonList(mockSecurity(1, "*", 1));
    when(securityDao.findWithOlderPasswordVersion()).thenReturn(modifiedSecurity);
    when(oktaManager.isOktaEnabled()).thenReturn(true);
    OscarPasswordService.upgradePasswords();
    assertEquals(originalSecurity, modifiedSecurity);
  }

  @Test
  public void givenPasswordEncryptionEnabled_whenTrySetPassword_thenSetPasswordVersionEncryptionEnabled() throws EncryptionException {
    mockIsPasswordEncryptionEnabled(true);
    val security = new Security();
    OscarPasswordService.setPasswordVersion(security);
    assertEquals(OscarPasswordService.getInstance().getVersion(),
        security.getPasswordVersion().intValue());
  }

  @Test
  public void givenPasswordEncryptionDisabled_whenTrySetPassword_thenSetPasswordVersionEncryptionDisabled() {
    mockIsPasswordEncryptionEnabled(false);
    val security = new Security();
    OscarPasswordService.setPasswordVersion(security);
    assertEquals(new Integer(0), security.getPasswordVersion());
  }

  @Test
  public void givenPassword_whenValidatePassword_thenValidatePasswordSuccess() {
    mockOscarProperties();
    assertDoesNotThrow(() -> OscarPasswordService.validatePassword("Password1"));
  }

  @Test
  public void givenPasswordWithSpecialCharacter_whenValidatePassword_thenValidatePasswordSuccess() {
    mockOscarProperties();
    assertDoesNotThrow(() -> OscarPasswordService.validatePassword("Password!"));
  }

  @Test
  public void validatePassword_failLessThanMinimumLength() {
    when(properties.getProperty("password_min_length")).thenReturn(PASSWORD_MIN_LENGTH);
    var exception = assertThrows(
        PasswordValidationException.class,
        createPasswordValidationExecutable("Pass1")
    );
    var expectedMessage = "Password must be at least " + PASSWORD_MIN_LENGTH + " characters";
    var actualMessage = exception.getMessage();
    assertTrue(actualMessage.startsWith(expectedMessage));
  }

  @Test
  public void validatePassword_failWithTwoGroups() {
    mockOscarProperties();
    var exception = assertThrows(
        PasswordValidationException.class,
        createPasswordValidationExecutable("Password")
    );
    var expectedMessage = "Password must contain at least "
        + PASSWORD_MIN_GROUPS + " of the following:";
    var actualMessage = exception.getMessage();
    assertTrue(actualMessage.startsWith(expectedMessage));
  }

  private Answer<Boolean> mockIsBannedPassword(boolean isPasswordBanned) {
    return invocationOnMock -> isPasswordBanned;
  }

  private void mockIsPasswordEncryptionEnabled(boolean value) {
    OscarPasswordService.initialize(value, true);
  }

  private List<Security> mockSecurityListAtInitial() {
    return Arrays.asList(
        mockSecurity(1, PASSWORD_DIGEST, 0),
        mockSecurity(2, PASSWORD_DIGEST, 0),
        mockSecurity(3, PASSWORD_DIGEST, 0)
    );
  }

  private List<Security> mockSecurityListMixed() {
    return Arrays.asList(
      mockSecurity(1, PASSWORD_DIGEST, 0),
      mockSecurity(2, PASSWORD_ENCRYPTED_V1, 1),
      mockSecurity(3, PASSWORD_ENCRYPTED_V2, 2)
    );
  }

  private List<Security> mockSecurityListAtLatest() {
    return Arrays.asList(
      mockSecurity(1, PASSWORD_ENCRYPTED_CURRENT, VERSION_CURRENT),
      mockSecurity(2, PASSWORD_ENCRYPTED_CURRENT, VERSION_CURRENT),
      mockSecurity(3, PASSWORD_ENCRYPTED_CURRENT, VERSION_CURRENT)
    );
  }

  private Security mockSecurity(int securityNo, String password, int version) {
    val security = new Security();
    security.setSecurityNo(securityNo);
    security.setPassword(password);
    security.setPasswordVersion(version);
    return security;
  }

  private void mockOscarProperties() {
    when(properties.getProperty("password_min_length")).thenReturn(PASSWORD_MIN_LENGTH);
    when(properties.getProperty("password_min_groups")).thenReturn(PASSWORD_MIN_GROUPS);
    when(properties.getProperty("password_group_lower_chars"))
        .thenReturn("abcdefghijklmnopqrstuvwxyz");
    when(properties.getProperty("password_group_upper_chars"))
        .thenReturn("ABCDEFGHIJKLMNOPQRSTUVWXYZ");
    when(properties.getProperty("password_group_digits")).thenReturn("0123456789");
    when(properties.getProperty("password_group_special"))
        .thenReturn("\\! @\\#$%^&*()_+|~-\\=\\`{}[]\\:\";'<>?,./");
  }

  private Executable createPasswordValidationExecutable(final String password) {
    return () -> OscarPasswordService.validatePassword(password);
  }
}