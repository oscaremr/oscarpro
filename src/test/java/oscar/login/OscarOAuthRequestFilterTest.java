package oscar.login;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


public class OscarOAuthRequestFilterTest {

  public static final String EHR_ENDPOINT = "/api/ehr/some_endpoint";
  public static final String NON_LOCAL_IP = "192.168.1.1";
  public static final String NON_EHR_ENDPOINT = "/api/ehr/some_endpoint";
  public static final String LOCAL_IP = "127.0.0.1";
  private OscarOAuthRequestFilter filter;

  @Mock
  private HttpServletRequest servletRequest;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
    filter = new OscarOAuthRequestFilter();
    filter.servletRequest = servletRequest;
  }

  @Test
  void testHasEhrPrefixWithEhrPrefix() {
    when(servletRequest.getRequestURI()).thenReturn("/api/ehr/some_endpoint");
    assertTrue(filter.hasEhrPrefix());
  }

  @Test
  void testHasEhrPrefixWithoutEhrPrefix() {
    when(servletRequest.getRequestURI()).thenReturn("/non_ehr_endpoint");
    assertFalse(filter.hasEhrPrefix());
  }

  @Test
  void givenLocalIpAndEhrEndpoint_whenIsRequestFromOscarPro_thenReturnTrue() {
    when(servletRequest.getRequestURI()).thenReturn(NON_EHR_ENDPOINT);
    when(servletRequest.getRemoteAddr()).thenReturn(LOCAL_IP);
    assertTrue(filter.isRequestFromOscarPro());
  }

  @Test
  void givenNonLocalIpAndEhrEndpoint_whenIsRequestFromOscarPro_thenReturnFalse() {
    when(servletRequest.getRequestURI()).thenReturn(EHR_ENDPOINT);
    when(servletRequest.getRemoteAddr()).thenReturn(NON_LOCAL_IP);
    assertFalse(filter.isRequestFromOscarPro());
  }

  @Test
  void givenNullRemoteAddressAndEhrEndpoint_whenIsRequestFromOscarPro_thenReturnFalse() {
    when(servletRequest.getRequestURI()).thenReturn(EHR_ENDPOINT);
    when(servletRequest.getRemoteAddr()).thenReturn(null);
    assertFalse(filter.isRequestFromOscarPro());
  }
}
