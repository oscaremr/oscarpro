package oscar.login;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static oscar.login.LoginAction.LoginFailureType;

import java.util.Optional;
import org.junit.Test;

/**
 * Test class for the {@link LoginAction} class.
 */
public class LoginActionTest {

  @Test
  public void givenValidEnumStringBlocked_whenLoginFailureTypeToString_thenCorrectEnum() {
    Optional<LoginFailureType> loginFailureType = LoginFailureType.fromString("BLOCKED");

    assertTrue(loginFailureType.isPresent());
    assertEquals(LoginFailureType.BLOCKED, loginFailureType.get());
  }

  @Test
  public void givenValidEnumStringExpired_whenLoginFailureTypeToString_thenCorrectEnum() {
    Optional<LoginFailureType> loginFailureType = LoginFailureType.fromString("expired_login");

    assertTrue(loginFailureType.isPresent());
    assertEquals(LoginFailureType.EXPIRED_LOGIN, loginFailureType.get());
  }

  @Test
  public void givenEmptyEnumString_whenLoginFailureTypeToString_thenEmpty() {
    Optional<LoginFailureType> loginFailureType = LoginFailureType.fromString("");

    assertFalse(loginFailureType.isPresent());
  }

  @Test
  public void givenNullEnumString_whenLoginFailureTypeToString_thenEmpty() {
    Optional<LoginFailureType> loginFailureType = LoginFailureType.fromString(null);

    assertFalse(loginFailureType.isPresent());
  }
}
