/**
 * Copyright (c) 2021 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.login;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Date;
import org.apache.cxf.rs.security.oauth.data.Token;
import org.joda.time.LocalDateTime;
import org.joda.time.Minutes;
import org.junit.Before;
import org.junit.Test;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.ServiceAccessTokenDao;
import org.oscarehr.common.dao.ServiceRequestTokenDao;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.ServiceAccessToken;
import org.oscarehr.common.model.ServiceRequestToken;
import org.oscarehr.util.SpringUtils;

public class OscarOAuthDataProviderIT extends DaoTestFixtures {
  private final ServiceAccessTokenDao serviceAccessTokenDao = SpringUtils.getBean(ServiceAccessTokenDao.class);
  private final ServiceRequestTokenDao serviceRequestTokenDao = SpringUtils.getBean(ServiceRequestTokenDao.class);
  private final OscarOAuthDataProvider provider = SpringUtils.getBean(OscarOAuthDataProvider.class);

  private final String TOKEN_KEY = "b554da32-0d1e-4a8c-b973-eaef40fe4a20";

  @Before
  public void before() throws Exception {
    SchemaUtils.restoreTable(
        true, "ServiceAccessToken", "ServiceRequestToken", "log");
  }

  private Token createToken() {
    Token token = mock(Token.class);
    when(token.getTokenKey()).thenReturn(TOKEN_KEY);
    return token;
  }

  private void createAccessToken(Integer minutesSinceLastFail) {
    ServiceAccessToken sat = new ServiceAccessToken();
    sat.setTokenId(TOKEN_KEY);
    sat.setTokenSecret("");
    sat.setLifetime(-1);
    sat.setIssued(0);

    if (minutesSinceLastFail != null) {
      sat.setLastFailed(getPastTime(minutesSinceLastFail));
    }

    serviceAccessTokenDao.persist(sat);
  }

  private Date getPastTime(int minutesPast) {
    Calendar cal = Calendar.getInstance();
    cal.add(Calendar.MINUTE, -minutesPast);
    return cal.getTime();
  }

  private void createRequestToken() {
    ServiceRequestToken srt = new ServiceRequestToken();
    srt.setTokenId(TOKEN_KEY);
    srt.setTokenSecret("");
    srt.setCallback("");

    serviceRequestTokenDao.persist(srt);
  }

  @Test
  public void testRemoveToken_doesNotExist() {
    OscarOAuthDataProvider providerSpy = spy(provider);
    Token token = createToken();

    providerSpy.removeToken(token);
    verify(providerSpy, never()).hasFailedRecently(any(ServiceAccessToken.class));
  }

  @Test
  public void testRemoveToken_noFails() {
    createAccessToken(null);
    Token token = createToken();

    provider.removeToken(token);
    ServiceAccessToken sat = serviceAccessTokenDao.findByTokenId(token.getTokenKey());
    LocalDateTime now = new LocalDateTime();
    LocalDateTime lastFailed = LocalDateTime.fromDateFields(sat.getLastFailed());
    assertTrue(Minutes.minutesBetween(now, lastFailed).getMinutes() <= 1);
  }

  @Test
  public void testRemoveToken_noFailsAndRequestTokenExists() {
    createAccessToken(null);
    createRequestToken();
    Token token = createToken();

    provider.removeToken(token);
    ServiceAccessToken sat = serviceAccessTokenDao.findByTokenId(token.getTokenKey());
    ServiceRequestToken srt = serviceRequestTokenDao.findByTokenId(token.getTokenKey());
    LocalDateTime now = new LocalDateTime();
    LocalDateTime lastFailed = LocalDateTime.fromDateFields(sat.getLastFailed());
    assertTrue(Minutes.minutesBetween(now, lastFailed).getMinutes() <= 1);
    assertNotNull(srt);
  }

  @Test
  public void testRemoveToken_recentlyFailedAndRequestTokenExists() {
    createAccessToken(10);
    createRequestToken();
    Token token = createToken();

    provider.removeToken(token);
    ServiceAccessToken sat = serviceAccessTokenDao.findByTokenId(token.getTokenKey());
    assertNull(sat);
  }

  @Test
  public void testRemoveToken_recentlyFailed() {
    createAccessToken(10);
    Token token = createToken();

    provider.removeToken(token);
    ServiceAccessToken sat = serviceAccessTokenDao.findByTokenId(token.getTokenKey());
    assertNull(sat);
  }

  @Test
  public void testHasFailedRecently_minRange() {
    ServiceAccessToken sat = new ServiceAccessToken();
    sat.setLastFailed(getPastTime(0));
    assertTrue(provider.hasFailedRecently(sat));
  }

  @Test
  public void testHasFailedRecently_maxRange() {
    ServiceAccessToken sat = new ServiceAccessToken();
    sat.setLastFailed(getPastTime(15));
    assertTrue(provider.hasFailedRecently(sat));
  }

  @Test
  public void testHasFailedRecently_notRecentlyFailed() {
    ServiceAccessToken sat = new ServiceAccessToken();
    sat.setLastFailed(getPastTime(16));
    assertFalse(provider.hasFailedRecently(sat));
  }

  @Test
  public void testHasFailedRecently_notPreviouslyFailed() {
    ServiceAccessToken sat = new ServiceAccessToken();
    assertFalse(provider.hasFailedRecently(sat));
  }
}
