package oscar.oscarMDS.pageUtil;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.Hl7TextInfoDao;
import org.oscarehr.common.dao.IncomingLabRulesDao;
import org.oscarehr.common.dao.ProviderLabRoutingDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.TableModificationDao;
import org.oscarehr.util.SpringUtils;
import utils.OscarPropertiesUtil;

@ExtendWith(MockitoExtension.class)
public class ReportStatusUpdateActionTest {

  @Mock private IncomingLabRulesDao incomingLabRulesDao;
  @Mock private ProviderLabRoutingDao providerLabRoutingDao;
  @Mock private TableModificationDao tableModificationDao;
  @Mock private Hl7TextInfoDao hl7TextInfoDao;

  @Mock private HttpServletRequest request;
  @Mock private HttpServletResponse response;
  @Mock private SystemPreferencesDao systemPreferencesDao;
  private MockedStatic<SpringUtils> springUtils;

  private ReportStatusUpdateAction reportStatusUpdateAction;

  /**
   * Sets up the test environment before each test.
   */
  @BeforeEach
  public void before() {
    springUtils = mockStatic(SpringUtils.class);
    OscarPropertiesUtil.getOscarProperties();
    reportStatusUpdateAction = new ReportStatusUpdateAction();

    springUtils
        .when(() -> SpringUtils.getBean(ProviderLabRoutingDao.class))
        .thenReturn(providerLabRoutingDao);
    springUtils
        .when(() -> SpringUtils.getBean(IncomingLabRulesDao.class))
        .thenReturn(incomingLabRulesDao);
    springUtils
        .when(() -> SpringUtils.getBean(TableModificationDao.class))
        .thenReturn(tableModificationDao);
    springUtils
        .when(() -> SpringUtils.getBean(SystemPreferencesDao.class))
        .thenReturn(systemPreferencesDao);
    springUtils
        .when(() -> SpringUtils.getBean(Hl7TextInfoDao.class))
        .thenReturn(hl7TextInfoDao);
  }

  @AfterEach
  public void after() {
    springUtils.close();
  }

  @Test
  public void givenAddComment_whenWithComment_thenAdd() throws IOException {
    when(request.getParameter("segmentID")).thenReturn(Integer.toString(675));
    when(request.getParameter("providerNo")).thenReturn("999998");
    when(request.getParameter("status")).thenReturn("Comment");
    when(request.getParameter("comment")).thenReturn("comment");
    when(request.getParameter("labType")).thenReturn("HL7");
    PrintWriter mockPrintWriter = mock(PrintWriter.class);
    when(response.getWriter()).thenReturn(mockPrintWriter);
    assertNull(reportStatusUpdateAction.addComment(null, null, request, response));
  }

}
