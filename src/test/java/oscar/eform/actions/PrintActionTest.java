package oscar.eform.actions;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.oscarpro.common.http.OscarProHttpService;
import ca.oscarpro.common.util.ResponseUtils;
import java.io.ByteArrayInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.oscarehr.common.dao.EFormDataDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.EFormData;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import oscar.OscarProperties;

public class PrintActionTest {
  private MockedStatic<LoggedInInfo> loggedInInfoMockedStatic;
  private MockedStatic<OscarProperties> oscarPropertiesMockedStatic;
  @Mock private SecurityInfoManager securityInfoManager;

  @Mock private OscarProHttpService oscarProHttpService;

  @Mock private HttpServletRequest request;
  @Mock private HttpServletResponse response;
  @Mock private ActionMapping mapping;
  @Mock private SystemPreferencesDao systemPreferencesDao;
  @Mock private EFormDataDao eFormDataDao;
  @Mock private OscarProperties oscarProperties;
  private AutoCloseable closeable;

  private PrintAction printAction;
  @Mock private HttpResponse proPrintResponse;

  @BeforeEach
  public void setup() {
    closeable = MockitoAnnotations.openMocks(this);

    oscarPropertiesMockedStatic = mockStatic(OscarProperties.class);
    when(OscarProperties.getInstance())
        .thenAnswer((Answer<OscarProperties>) invocation -> oscarProperties);

    printAction =
        spy(new PrintAction(securityInfoManager, systemPreferencesDao, eFormDataDao, oscarProHttpService));

    when(securityInfoManager.hasPrivilege(any(LoggedInInfo.class), eq("_eform"), eq("r"), eq(null)))
        .thenReturn(true);

    when(request.getParameter("providerId")).thenReturn("1");
    when(request.getAttribute("fdid")).thenReturn("123");
    when(request.getServerPort()).thenReturn(8080);
    when(request.getServerPort()).thenReturn(8080);
    when(request.getContextPath()).thenReturn("/oscar");

    mockLoggedInProvider();
    mockGetGeneratedPDFFalse();
  }

  @AfterEach
  public void after() throws Exception {
    closeable.close();
    loggedInInfoMockedStatic.close();
    oscarPropertiesMockedStatic.close();
  }

  @Test
  public void givenEformWithAttachmentManagerAttachments_whenExecute_thenGeneratePdf() throws Exception {
    when(systemPreferencesDao.isReadBooleanPreferenceWithDefault(
            "attachment_manager.eform.enabled", false))
        .thenReturn(true);
    when(oscarProHttpService.makeLoggedInPostRequestToPro(anyString(), anyString(), eq(request)))
        .thenReturn(proPrintResponse);
    val entity = mock(HttpEntity.class);
    when(proPrintResponse.getEntity()).thenReturn(entity);
    val contentStream = new ByteArrayInputStream("/temp/PDF/testPdf.pdf".getBytes());
    when(entity.getContent()).thenReturn(contentStream);

    val data = new EFormData();
    when(eFormDataDao.find(eq(123))).thenReturn(data);
    when(request.getParameter(eq("printDocumentToEchartOnPdfFax"))).thenReturn("false");
    val successForward = mock(ActionForward.class);
    when(mapping.findForward(eq("success"))).thenReturn(successForward);

    when(request.getParameter("skipSave")).thenReturn("true");

    // Call the method under test
    try (MockedStatic<ResponseUtils> mockedResponseUtils =
        mockStatic(ResponseUtils.class)) {

      doNothing().when(ResponseUtils.class);
      ResponseUtils.writeFileDataToResponseOutputStream(
          any(HttpServletResponse.class), anyString());

      val forwardResult = printAction.execute(mapping, null, request, response);
      assertEquals(successForward, forwardResult);
      mockedResponseUtils.verify(
          () -> ResponseUtils.writeFileDataToResponseOutputStream(eq(response), isNull()),
          times(1));
      data.setCurrent(false);
      verify(eFormDataDao, times(1)).merge(any());
    }

  }

  private void mockGetGeneratedPDFFalse() {
    when(request.getParameter("getGeneratedPDF")).thenReturn("false");
    when(request.getParameter("M_IsExcellerisEOrder")).thenReturn("false");
    when(request.getParameter("M_IsDynacareEOrder")).thenReturn("false");
  }

  private void mockLoggedInProvider() {
    loggedInInfoMockedStatic = mockStatic(LoggedInInfo.class);
    final LoggedInInfo loggedInInfo = mock(LoggedInInfo.class);
    when(LoggedInInfo.getLoggedInInfoFromSession(request))
        .thenAnswer(
            new Answer<LoggedInInfo>() {
              @Override
              public LoggedInInfo answer(InvocationOnMock invocation) {
                return loggedInInfo;
              }
            });
  }
}
