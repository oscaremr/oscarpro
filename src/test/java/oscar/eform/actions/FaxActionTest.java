package oscar.eform.actions;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import ca.oscarpro.common.http.OscarProHttpService;
import java.io.File;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;

@ExtendWith(MockitoExtension.class)
public class FaxActionTest {

  private static final String FORM_ID = "123";
  private static final String VIEW_URI = "/test/legacy/uri";
  private static final String OSCAR_PRO_BASE_URL = "http://localhost:8080";
  private static final String FILE_NAME = "test.pdf";

  private static final String EXPECTED_REQUEST_DATA =
      "{\"id\":\"123\",\"type\":\"Eforms\",\"params\":{},\"supported\":true}";

  private static final String EFORM_PRINT_URL =
      "%s/api/v1/printables/print";

  private static MockedStatic<SpringUtils> springUtilsMockedStatic;
  private static MockedStatic<OscarProperties> oscarPropertiesMockedStatic;
  private static MockedStatic<EntityUtils> entityUtilsMockedStatic;

  @Mock
  private File tempFileMock;

  @Mock
  private HttpServletRequest requestMock;

  @Mock
  private HttpResponse responseMock;

  @Mock
  private HttpEntity httpEntityMock;

  @Mock
  private OscarProHttpService oscarProHttpServiceMock;

  @Mock
  private SystemPreferencesDao systemPreferencesDaoMock;

  private FaxAction faxAction;

  @BeforeEach
  public void setUp() {
    openMocks(this);

    springUtilsMockedStatic = mockStatic(SpringUtils.class);
    springUtilsMockedStatic.when(() -> SpringUtils.getBean(SystemPreferencesDao.class))
        .thenReturn(systemPreferencesDaoMock);
    springUtilsMockedStatic.when(() -> SpringUtils.getBean(OscarProHttpService.class))
        .thenReturn(oscarProHttpServiceMock);

    oscarPropertiesMockedStatic = mockStatic(OscarProperties.class);
    entityUtilsMockedStatic = mockStatic(EntityUtils.class);
    faxAction = spy(new FaxAction(requestMock));
  }

  @AfterEach
  public void tearDown() {
    springUtilsMockedStatic.close();
    oscarPropertiesMockedStatic.close();
    entityUtilsMockedStatic.close();
  }

  @Test
  public void givenAttachmentManagerEFormEnabled_whenConvertToPdf_thenSendRequestToPro()
      throws IOException {
    mockAttachmentManagerEFormEnabled(true);
    mockApiCall();

    // Mock entity
    when(responseMock.getEntity()).thenReturn(httpEntityMock);
    entityUtilsMockedStatic.when(() -> EntityUtils.toString(any())).thenReturn(FILE_NAME);

    // Mock file saving
    doNothing().when(faxAction).saveEFormToTempFile(any(), any(), any());

    // Call the method
    faxAction.convertToPdf(FORM_ID, VIEW_URI, tempFileMock, requestMock);

    // Verify that the request was sent to the OSCAR Pro server
    verify(oscarProHttpServiceMock, times(1))
        .makeLoggedInPostRequestToPro(String.format(EFORM_PRINT_URL, OSCAR_PRO_BASE_URL),
            EXPECTED_REQUEST_DATA, requestMock);

    // Verify that the file was saved to a temporary file
    verify(faxAction, times(1)).saveEFormToTempFile(
        FORM_ID, tempFileMock, FILE_NAME);

    // Verify that the legacy method was not called
    verify(faxAction, times(0)).legacyConvertToPdf(VIEW_URI, tempFileMock);
  }

  @Test
  public void givenAttachmentManagerEFormDisabled_whenConvertToPdf_thenDoNotSendRequestToPro()
      throws IOException {
    mockAttachmentManagerEFormEnabled(false);

    // Mock legacy saving
    doNothing().when(faxAction).legacyConvertToPdf(any(), any());

    // Call the method
    faxAction.convertToPdf(FORM_ID, VIEW_URI, tempFileMock, requestMock);

    // Verify that the request was not sent to the OSCAR Pro server
    verify(oscarProHttpServiceMock, times(0))
        .makeLoggedInPostRequestToPro(anyString(), any(), any());

    // Verify that the file was not saved to a temporary file
    verify(faxAction, times(0)).saveEFormToTempFile(
        FORM_ID, tempFileMock, FILE_NAME);

    // Verify that the legacy method was called
    verify(faxAction, times(1)).legacyConvertToPdf(VIEW_URI, tempFileMock);
  }

  @Test
  public void givenResponseIsNull_whenConvertToPdf_thenDoNotSaveFile() throws IOException {
    mockAttachmentManagerEFormEnabled(true);
    mockApiCall(null);

    // Call the method
    faxAction.convertToPdf(FORM_ID, VIEW_URI, tempFileMock, requestMock);

    // Verify that the file was not saved to a temporary file
    verify(faxAction, times(0)).saveEFormToTempFile(
        FORM_ID, tempFileMock, FILE_NAME);
  }

  @Test
  public void givenResponseEntityIsNull_whenConvertToPdf_thenDoNotSaveFile() throws IOException {
    mockAttachmentManagerEFormEnabled(true);
    mockApiCall();

    // Mock null entity
    when(responseMock.getEntity()).thenReturn(null);

    // Call the method
    faxAction.convertToPdf(FORM_ID, VIEW_URI, tempFileMock, requestMock);

    // Verify that the file was not saved to a temporary file
    verify(faxAction, times(0)).saveEFormToTempFile(
        FORM_ID, tempFileMock, FILE_NAME);
  }

  @Test
  public void givenIOException_whenConvertToPdf_thenDoNotSaveFile() throws IOException {
    mockAttachmentManagerEFormEnabled(true);
    mockApiCall();

    // Mock entity with IOException
    when(responseMock.getEntity()).thenReturn(httpEntityMock);
    entityUtilsMockedStatic.when(() -> EntityUtils.toString(any())).thenThrow(IOException.class);

    // Call the method
    faxAction.convertToPdf(FORM_ID, VIEW_URI, tempFileMock, requestMock);

    // Verify that the file was not saved to a temporary file
    verify(faxAction, times(0)).saveEFormToTempFile(
        FORM_ID, tempFileMock, FILE_NAME);
  }

  private void mockAttachmentManagerEFormEnabled(final boolean enabled) {
    when(systemPreferencesDaoMock.isReadBooleanPreferenceWithDefault(anyString(), anyBoolean()))
        .thenReturn(enabled);
  }

  private void mockApiCall() {
    mockApiCall(responseMock);
  }

  private void mockApiCall(final HttpResponse response) {
    oscarPropertiesMockedStatic.when(OscarProperties::getOscarProBaseUrl)
        .thenReturn(OSCAR_PRO_BASE_URL);
    when(oscarProHttpServiceMock.makeLoggedInPostRequestToPro(anyString(), any(), any()))
        .thenReturn(response);
  }

}