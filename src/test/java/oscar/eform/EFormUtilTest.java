package oscar.eform;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import lombok.val;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.oscarehr.common.dao.EFormDao;
import org.oscarehr.common.dao.EFormDataDao;
import org.oscarehr.common.model.EForm;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import oscar.oscarDB.DBHandler;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class EFormUtilTest {

  private static final String FORM_NAME = "testFormName";
  private static final String FORM_SUBJECT = "testFormSubject";
  private static final String FILE_NAME = "testFileName";
  private static final String HTML_STR = "testHtmlStr";
  private static final String CREATOR = "testCreator";
  private static final String ROLE_TYPE = "testRoleType";
  private static final boolean SHOW_LATEST_FORM_ONLY = true;
  private static final boolean PATIENT_INDEPENDENT = true;
  private static final boolean IS_ATTACHMENTS_ENABLED = true;


  private static MockedStatic<SpringUtils> utilsMock;

  private static MockedStatic<DBHandler> handlerMock;

  private static MockedStatic<OscarProperties> oscarPropertiesMock;

  @Captor
  private ArgumentCaptor<EForm> eFormCaptor;

  @Mock
  private EFormDao eFormDao;

  @Mock
  private EFormDataDao eFormDataDao;

  @BeforeEach
  public void setUp() {
    openMocks(this);

    utilsMock = mockStatic(SpringUtils.class);
    handlerMock = mockStatic(DBHandler.class);
    oscarPropertiesMock = mockStatic(OscarProperties.class);
    val eformUtil = new EFormUtil();
    when(SpringUtils.getBean(anyString())).thenReturn(eformUtil);

    OscarProperties oscarProperties = mock(OscarProperties.class);
    when(oscarProperties.getProperty(eq("eform_image"))).thenReturn("fake/path/");
    when(OscarProperties.getInstance()).thenReturn(oscarProperties);
  }

  @AfterEach
  public void tearDown() {
    utilsMock.close();
    handlerMock.close();
    oscarPropertiesMock.close();
  }

  @Test
  public void givenInvalidSql_whenGetValues_thenReturnNull() {
    val listOfNames = new ArrayList<>(Arrays.asList("testName1", "testName2"));
    val result = EFormUtil.getValues(listOfNames, "testSql");
    assertNull(result);
  }

  @Test
  public void givenDefaultMethodUse_whenListImages_thenCallOverloadedMethod() {
    val result = EFormUtil.listImages();
    // just here for coverage. better tools needed to properly verify that the overloaded method was called
    assertNotNull(result);
  }

  @Test
  public void givenEmptyEFormImagesFolder_whenListImages_thenReturnEmptyList() {
    File mockFile = mock(File.class);
    when(mockFile.list()).thenReturn(null);

    val result = EFormUtil.listImages(mockFile);
    assertNotNull(result);
    assertEquals(0, result.size());
  }

  @Test
  public void givenNonEmptyEFormImagesFolder_whenListImages_thenReturnSortedList() {
    File mockFile = mock(File.class);
    when(mockFile.list()).thenReturn(new String[]{"testfile3", "testfile1", "testfile2"});

    val result = EFormUtil.listImages(mockFile);
    assertNotNull(result);
    assertEquals(3, result.size());
    assertEquals("testfile1", result.get(0));
    assertEquals("testfile2", result.get(1));
    assertEquals("testfile3", result.get(2));
  }

  @Test
  public void givenAllInput_whenSaveEForm_thenPersistEForm() {
    utilsMock.when(() -> SpringUtils.getBean(EFormDao.class)).thenReturn(eFormDao);
    try {
      EFormUtil.saveEForm(FORM_NAME, FORM_SUBJECT, FILE_NAME, HTML_STR, CREATOR,
          SHOW_LATEST_FORM_ONLY, PATIENT_INDEPENDENT, IS_ATTACHMENTS_ENABLED, ROLE_TYPE);
    } catch (NullPointerException e) {
      // result eForm does not have id, resulting in NPE
    }
    verify(eFormDao, times(1)).persist(eFormCaptor.capture());
    val eForm = eFormCaptor.getValue();
    assertEForm(eForm);
    assertEquals(eForm.getCreator(), CREATOR);
  }

  @Test
  public void givenNoCreator_whenSaveEForm_thenPersistEForm() {
    utilsMock.when(() -> SpringUtils.getBean(EFormDao.class)).thenReturn(eFormDao);
    try {
      EFormUtil.saveEForm(FORM_NAME, FORM_SUBJECT, FILE_NAME, HTML_STR,
          SHOW_LATEST_FORM_ONLY, PATIENT_INDEPENDENT, IS_ATTACHMENTS_ENABLED, ROLE_TYPE);
    } catch (NullPointerException e) {
      // result eForm does not have id, resulting in NPE
    }
    verify(eFormDao, times(1)).persist(eFormCaptor.capture());
    val eForm = eFormCaptor.getValue();
    assertEForm(eForm);
    assertNull(eForm.getCreator());
  }

  @Test
  public void givenEForm_whenSaveEForm_thenPersistEForm() {
    utilsMock.when(() -> SpringUtils.getBean(EFormDao.class)).thenReturn(eFormDao);
    try {
      EFormUtil.saveEForm(createTestEForm());
    } catch (NullPointerException e) {
      // result eForm does not have id, resulting in NPE
    }
    verify(eFormDao, times(1)).persist(eFormCaptor.capture());
    val eForm = eFormCaptor.getValue();
    assertEForm(eForm);
    assertEquals(eForm.getCreator(), CREATOR);
  }

  @Test
  public void givenNoIsAttachmentsEnabledAndCreator_whenSaveEForm_thenPersistEForm() {
    utilsMock.when(() -> SpringUtils.getBean(EFormDao.class)).thenReturn(eFormDao);
    try {
      EFormUtil.saveEForm(FORM_NAME, FORM_SUBJECT, FILE_NAME, HTML_STR,
          SHOW_LATEST_FORM_ONLY, PATIENT_INDEPENDENT, ROLE_TYPE);
    } catch (NullPointerException e) {
      // result eForm does not have id, resulting in NPE
    }
    verify(eFormDao, times(1)).persist(eFormCaptor.capture());
    val eForm = eFormCaptor.getValue();
    assertEForm(eForm);
    assertNull(eForm.getCreator());
    assertTrue(eForm.isAttachmentsEnabled());
  }

  private void assertEForm(final EForm eForm) {
    assertEquals(FORM_NAME, eForm.getFormName());
    assertEquals(FORM_SUBJECT, eForm.getSubject());
    assertEquals(FILE_NAME, eForm.getFileName());
    assertEquals(HTML_STR, eForm.getFormHtml());
    assertEquals(ROLE_TYPE, eForm.getRoleType());
    assertEquals(SHOW_LATEST_FORM_ONLY, eForm.isShowLatestFormOnly());
    assertEquals(PATIENT_INDEPENDENT, eForm.isPatientIndependent());
    assertEquals(IS_ATTACHMENTS_ENABLED, eForm.isAttachmentsEnabled());
  }

  private oscar.eform.data.EForm createTestEForm() {
    utilsMock.when(() -> SpringUtils.getBean("EFormDataDao")).thenReturn(eFormDataDao);
    oscar.eform.data.EForm eForm = new oscar.eform.data.EForm();
    eForm.setFormName(FORM_NAME);
    eForm.setFormSubject(FORM_SUBJECT);
    eForm.setFormFileName(FILE_NAME);
    eForm.setFormHtml(HTML_STR);
    eForm.setRoleType(ROLE_TYPE);
    eForm.setShowLatestFormOnly(SHOW_LATEST_FORM_ONLY);
    eForm.setPatientIndependent(PATIENT_INDEPENDENT);
    eForm.setAttachmentsEnabled(IS_ATTACHMENTS_ENABLED);
    eForm.setFormCreator(CREATOR);
    return eForm;
  }
}
