package oscar.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Calendar;
import java.util.Date;
import lombok.val;
import org.junit.jupiter.api.Test;

class UtilDateUtilitiesTest {
  final Date pointInTime = getCalendar(2024, Calendar.FEBRUARY, 1);

  @Test
  void givenDateOfBirthNull_whenCalcAgeAtDate_thenReturnNull() {
    val result = UtilDateUtilities.calcAgeAtDate(null, pointInTime);

    assertNull(result);
  }

  @Test
  void givenPointInTimeNull_whenCalcAgeAtDate_thenReturnNull() {
    val dob = getCalendar(2022, Calendar.FEBRUARY, 1);

    val result = UtilDateUtilities.calcAgeAtDate(dob, null);

    assertNull(result);
  }

  @Test
  void givenTwoYearsOld_whenCalcAgeAtDate_thenReturnCorrectYears() {
    val dob = getCalendar(2022, Calendar.FEBRUARY, 1);

    val result = UtilDateUtilities.calcAgeAtDate(dob, pointInTime);

    assertEquals("2 years", result);
  }

  @Test
  void givenAgeUnderTwoYearsCloseToBirthDate_whenCalcAgeAtDate_thenReturnCorrectMonths() {
    val dob = getCalendar(2022, Calendar.FEBRUARY, 3);

    val result = UtilDateUtilities.calcAgeAtDate(dob, pointInTime);

    assertEquals("23 months", result);
  }

  @Test
  void givenAgeUnderOneYearCloseToBirthDate_whenCalcAgeAtDate_thenReturnCorrectMonths() {
    val dob = getCalendar(2023, Calendar.FEBRUARY, 3);

    val result = UtilDateUtilities.calcAgeAtDate(dob, pointInTime);

    assertEquals("11 months", result);
  }

  @Test
  void givenAgeUnderOneYear_whenMoreThanFourteenDays_thenReturnWeeks() {
    val dob = getCalendar(2024, Calendar.JANUARY, 15);

    val result = UtilDateUtilities.calcAgeAtDate(dob, pointInTime);

    assertEquals("2 weeks", result);
  }

  @Test
  void givenAgeUnderOneYear_whenLessThanFourteenDays_thenReturnDays() {
    val dob = getCalendar(2024, Calendar.JANUARY, 19);

    val result = UtilDateUtilities.calcAgeAtDate(dob, pointInTime);

    assertEquals("13 days", result);
  }

  private static Date getCalendar(final int year, final int month, final int date) {
    val dob = Calendar.getInstance();
    dob.set(year, month, date);
    return dob.getTime();
  }
}