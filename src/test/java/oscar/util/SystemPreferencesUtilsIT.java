/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via
 * "GNU General Public License v2.0 - GNU Project - Free Software Foundation ".
 */

package oscar.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.common.model.SystemPreferences;
import oscar.OscarProperties;

public class SystemPreferencesUtilsIT extends DaoTestFixtures {

  @BeforeEach
  public void before() throws Exception {
    SchemaUtils.restoreTable(false, "SystemPreferences");
  }

  @Test
  public void testIsReadBooleanPreferenceWithDefault() {
    createOneSystemPreference("false");

    boolean result = SystemPreferencesUtils
        .isReadBooleanPreferenceWithDefault("testPref", true);
    Assertions.assertFalse(result);
  }

  @Test
  public void testIsReadBooleanPreferenceWithDefaultNotFound() {
    boolean result = SystemPreferencesUtils.
        isReadBooleanPreferenceWithDefault("nonexistent_name", true);
    Assertions.assertTrue(result);
  }

  @Test
  public void testFindPreferencesByNames() {
    SystemPreferences testPreference = new SystemPreferences();
    testPreference.setName("testPref");
    testPreference.setValue("value1");
    testPreference.setUpdateDate(new Date());
    SystemPreferencesUtils.persist(testPreference);

    SystemPreferences preferenceEnabled = new SystemPreferences();
    preferenceEnabled.setName("preferenceEnabled");
    preferenceEnabled.setValue("true");
    preferenceEnabled.setUpdateDate(new Date());
    SystemPreferencesUtils.persist(preferenceEnabled);

    List<SystemPreferences> result = SystemPreferencesUtils.
        findPreferencesByNames(Arrays.asList("testPref", "preferenceEnabled"));
    Assertions.assertEquals(2, result.size());
    Assertions.assertEquals("testPref", result.get(0).getName());
    Assertions.assertEquals("value1", result.get(0).getValue());
    Assertions.assertEquals("preferenceEnabled", result.get(1).getName());
    Assertions.assertEquals("true", result.get(1).getValue());
  }

  @Test
  public void testFindPreferencesByNamesNotFound() {
    List<SystemPreferences> results = SystemPreferencesUtils
        .findPreferencesByNames(Collections.singletonList("nonexistent_name"));
    Assertions.assertEquals(0, results.size());
  }

  @Test
  public void testFindPreferenceByName() {
    createOneSystemPreference("value1");

    SystemPreferences result =
        SystemPreferencesUtils.findPreferenceByName("testPref");
    Assertions.assertEquals("value1", result.getValue());
    Assertions.assertEquals("testPref", result.getName());
  }

  @Test
  public void testFindPreferenceByNewName() {
    OscarProperties oscarProps = OscarProperties.getInstance();
    oscarProps.setProperty("faxEnable", "on");

    SystemPreferences result = SystemPreferencesUtils.findPreferenceByName("fax_enabled");
    Assertions.assertEquals("fax_enabled", result.getName());
    Assertions.assertEquals("true", result.getValue());
  }

  @Test
  public void testFindPreferenceByNameNotFound() {
    SystemPreferences result =
        SystemPreferencesUtils.findPreferenceByName("nonexistentName");
    Assertions.assertNull(result);
  }
  
  @Test
  public void testFindByKeysAsMap() {
    SystemPreferences testPreference = new SystemPreferences();
    testPreference.setName("testPref");
    testPreference.setValue("false");
    testPreference.setUpdateDate(new Date());
    SystemPreferencesUtils.persist(testPreference);

    SystemPreferences preferenceEnabled = new SystemPreferences();
    preferenceEnabled.setName("preferenceEnabled");
    preferenceEnabled.setValue("true");
    preferenceEnabled.setUpdateDate(new Date());
    SystemPreferencesUtils.persist(preferenceEnabled);

    Map<String, Boolean> result = SystemPreferencesUtils
        .findByKeysAsMap(Arrays.asList("testPref", "preferenceEnabled"));
    
    Assertions.assertEquals(false, result.get("testPref"));
    Assertions.assertEquals(true, result.get("preferenceEnabled"));
  }
  
  @Test
  public void testFindByKeysAsMapNotFound() {
    Map<String, Boolean> result = SystemPreferencesUtils
        .findByKeysAsMap(Collections.singletonList("nonexistentName"));
    
    Assertions.assertEquals(0, result.size());
  }
  
  @Test
  public void testFindByKeysAsPreferenceMap() {
    SystemPreferences testPreference = new SystemPreferences();
    testPreference.setName("testPref");
    testPreference.setValue("stringValue");
    testPreference.setUpdateDate(new Date());
    SystemPreferencesUtils.persist(testPreference);

    SystemPreferences preferenceEnabled = new SystemPreferences();
    preferenceEnabled.setName("preferenceEnabled");
    preferenceEnabled.setValue("true");
    preferenceEnabled.setUpdateDate(new Date());
    SystemPreferencesUtils.persist(preferenceEnabled);
    
    Map<String, SystemPreferences> result = SystemPreferencesUtils
            .findByKeysAsPreferenceMap(
                Arrays.asList("testPref", "preferenceEnabled"));
    
    Assertions.assertEquals("stringValue", result.get("testPref").getValue());
    Assertions.assertEquals("true", result.get("preferenceEnabled").getValue());
  }
  
  @Test
  public void testFindByKeysAsPreferenceMapNotFound() {
    Map<String, SystemPreferences> result = SystemPreferencesUtils
        .findByKeysAsPreferenceMap(
            Collections.singletonList("nonexistentName"));

    Assertions.assertEquals(0, result.size());
  }
  
  @Test
  public void testIsPreferenceValueEquals() {
    createOneSystemPreference("value");
    
    boolean result = SystemPreferencesUtils
        .isPreferenceValueEquals("testPref", "wrongValue");
    Assertions.assertFalse(result);
  }
  
  @Test
  public void testIsPreferenceValueEqualsNotFound() {
    boolean result = SystemPreferencesUtils
        .isPreferenceValueEquals("nonexistentName", "val");
    Assertions.assertFalse(result);
  }
  
  @Test
  public void testIsReadBooleanPreference() {
    createOneSystemPreference("true");
    
    boolean result = SystemPreferencesUtils
        .isReadBooleanPreference("testPref");
    
    Assertions.assertTrue(result);
  }
  
  @Test
  public void testIsReadBooleanPreferenceNotFound() {
    boolean result = SystemPreferencesUtils
        .isReadBooleanPreference("nonexistentName");
    Assertions.assertFalse(result);
  }
  
  @Test
  public void testMerge() {
    SystemPreferences preference = new SystemPreferences();
    preference.setName("name");
    preference.setValue("val");
    preference.setUpdateDate(new Date());
    
    SystemPreferencesUtils.merge(preference);
    SystemPreferences result = 
        SystemPreferencesUtils.findPreferenceByName("name");
    
    Assertions.assertNotNull(result);
  }
  
  @Test
  public void testPersist() {
    SystemPreferences preference = new SystemPreferences();
    preference.setName("name");
    preference.setValue("val");
    preference.setUpdateDate(new Date());

    SystemPreferencesUtils.persist(preference);
    SystemPreferences result =
        SystemPreferencesUtils.findPreferenceByName("name");

    Assertions.assertNotNull(result);
  }
  
  @Test
  public void testMergeOrPersistSystemPreference() {
    SystemPreferences preference = new SystemPreferences();
    preference.setName("name");
    preference.setValue("val");
    preference.setUpdateDate(new Date());

    SystemPreferencesUtils.mergeOrPersist(preference);
    SystemPreferences result =
        SystemPreferencesUtils.findPreferenceByName("name");

    Assertions.assertNotNull(result);
  }
  
  @Test
  public void testMergeOrPersistNameAndValue() {
    SystemPreferencesUtils.mergeOrPersist("name", "value");
    SystemPreferences result =
        SystemPreferencesUtils.findPreferenceByName("name");

    Assertions.assertNotNull(result);
  }
  
  @Test
  public void testIsAddNewDocumentTypeShown() {
    SystemPreferences testPreference = new SystemPreferences();
    testPreference.setName("isAddNewDocumentTypeShown");
    testPreference.setValue("true");
    testPreference.setUpdateDate(new Date());
    SystemPreferencesUtils.persist(testPreference);
    
    boolean result = SystemPreferencesUtils.isAddNewDocumentTypeShown();

    Assertions.assertTrue(result);
  }
  
  @Test
  public void testIsValidationOnSpecialistEnabled() {
    SystemPreferences testPreference = new SystemPreferences();
    testPreference.setName("enable_validation_on_specialist");
    testPreference.setValue("false");
    testPreference.setUpdateDate(new Date());
    SystemPreferencesUtils.persist(testPreference);

    boolean result = SystemPreferencesUtils.isValidationOnSpecialistEnabled();

    Assertions.assertFalse(result);
  }
  
  @Test
  public void testIsOneIdEnabledNotFound() {
    boolean result = SystemPreferencesUtils.isOneIdEnabled();

    Assertions.assertFalse(result);
  }
  
  @Test
  public void testIsClinicalConnectEnabled() {
    SystemPreferences testPreference = new SystemPreferences();
    testPreference.setName("oneid.clinical_connect.enabled");
    testPreference.setValue("true");
    testPreference.setUpdateDate(new Date());
    SystemPreferencesUtils.persist(testPreference);

    boolean result = SystemPreferencesUtils.isClinicalConnectEnabled();

    Assertions.assertTrue(result);
  }
  
  @Test
  public void testIsDhirEnabled() {
    SystemPreferences testPreference = new SystemPreferences();
    testPreference.setName("oneid.dhir.enabled");
    testPreference.setValue("true");
    testPreference.setUpdateDate(new Date());
    SystemPreferencesUtils.persist(testPreference);

    boolean result = SystemPreferencesUtils.isDhirEnabled();

    Assertions.assertTrue(result);
  }
  
  @Test
  public void testIsDhdrEnabled() {
    SystemPreferences testPreference = new SystemPreferences();
    testPreference.setName("oneid.dhdr.enabled");
    testPreference.setValue("false");
    testPreference.setUpdateDate(new Date());
    SystemPreferencesUtils.persist(testPreference);

    boolean result = SystemPreferencesUtils.isDhdrEnabled();

    Assertions.assertFalse(result);
  }
  
  @Test
  public void testIsKioskEnabled() {
    boolean result = SystemPreferencesUtils.isKioskEnabled();
    
    Assertions.assertFalse(result);
  }

  private void createOneSystemPreference(String value) {
    SystemPreferences testPreference = new SystemPreferences();
    testPreference.setName("testPref");
    testPreference.setValue(value);
    testPreference.setUpdateDate(new Date());
    SystemPreferencesUtils.persist(testPreference);
  }
  
}