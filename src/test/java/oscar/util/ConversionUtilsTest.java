package oscar.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import javax.swing.text.BadLocationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

/**
 * Tests for the ConversionUtils class methods.
 */
public class ConversionUtilsTest {
  @Test
  public void givenRtfString_whenConvertRtfToText_thenReturnText()
      throws IOException, BadLocationException {
    String rtfString = "{\\rtf1\\ansi\n{\\fonttbl\\f0\\fnil Monospaced;}\n\nTest text\\par}";
    String expected = "Test text\n";
    String actual = ConversionUtils.convertRtfToText(rtfString);
    assertEquals(expected, actual);
  }

  @Test
  public void givenEmptyString_whenConvertRtfToText_thenReturnEmptyString()
      throws IOException, BadLocationException {
    String expected = "";
    String actual = ConversionUtils.convertRtfToText(expected);
    assertEquals(expected, actual);
  }

  @Test
  public void givenBadRtfString_whenConvertRtfToText_thenThrowIoException() {
    // RTF with too many close braces
    final String rtfToParse = "\\%^$#@#!@&*%^&$^!@#@%$$^^()%( (%)(#$012}}";
    assertThrows(IOException.class, new Executable() {
      @Override
      public void execute() throws Throwable {
        ConversionUtils.convertRtfToText(rtfToParse);
      }
    });
  }

  @Test
  public void givenDateString_whenFormatPatternMismatch_thenNullDate() {
    assertNull(ConversionUtils.fromTimestampString("2024-06-17"));
  }
}