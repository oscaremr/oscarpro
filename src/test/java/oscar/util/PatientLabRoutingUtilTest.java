package oscar.util;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.PatientLabRoutingDao;
import org.oscarehr.common.model.PatientLabRouting;
import java.util.Collections;

@ExtendWith(MockitoExtension.class)
public class PatientLabRoutingUtilTest {
  
  @Mock
  private PatientLabRoutingDao patientLabRoutingDao;
  
  private PatientLabRoutingUtil patientLabRoutingUtil;
  
  @BeforeEach
  void setUp() {
    patientLabRoutingUtil = new PatientLabRoutingUtil(patientLabRoutingDao);
  }
  
  @Test
  void givenValidArguments_whenRouteToPatient_thenPersistSuccessfully() {
    String documentNumber = "12345";
    String documentType = "DOC";
    String demographicNumber = "67890";
    
    when(patientLabRoutingDao.findByLabNoAndLabType(anyInt(), anyString()))
        .thenReturn(Collections.emptyList());
    
    patientLabRoutingUtil.routeToPatient(documentNumber, documentType, demographicNumber);
    
    verify(patientLabRoutingDao, times(1)).findByLabNoAndLabType(anyInt(), anyString());
    verify(patientLabRoutingDao, times(1)).persist(any(PatientLabRouting.class));
  }
}