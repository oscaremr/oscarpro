/**
 * Copyright (c) 2021 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

public class StringUtilsTest {

  private String input;
  private String nullSafeValue;
  private String output;
  private Integer parseIntOutput;

  @Test
  public void nullSafeStringValueTest_NonNullInput_UseInput() {
    input = "Non-Null Input String";
    nullSafeValue = "N/A";
    output = StringUtils.nullSafeStringValue(input, nullSafeValue);
    Assertions.assertEquals("Non-Null Input String", output);
  }

  @Test
  public void nullSafeStringValueTest_NullInput_UseNullSafe() {
    input = null;
    nullSafeValue = "N/A";
    output = StringUtils.nullSafeStringValue(input, nullSafeValue);
    Assertions.assertEquals("N/A", output);
  }

  @Test
  public void nullSafeStringValueTest_EmptyInput_UseNullSafe() {
    input = "";
    nullSafeValue = "N/A";
    output = StringUtils.nullSafeStringValue(input, nullSafeValue);
    Assertions.assertEquals("N/A", output);
  }

  @Test
  public void nullSafeStringValueTest_NullNullSafeValue_Exception() {
    input = "Non-Null Input String";
    nullSafeValue = null;
    Assertions.assertThrows(NullPointerException.class, new Executable() {
      @Override
      public void execute() throws Throwable {
        //noinspection ResultOfMethodCallIgnored
        StringUtils.nullSafeStringValue(input, nullSafeValue);
      }
    });
  }
   
  @Test
  public void parseIntTest_SuccessfulNumaricValue() {
    input = "10";
    parseIntOutput = StringUtils.parseInt(input);
    Assertions.assertEquals(new Integer(10), parseIntOutput);
  }
  
  @Test
  public void parseIntTest_UnsuccessfulEmptyValue() {
    input = "";
    Assertions.assertNull(StringUtils.parseInt(input));
  }

  @Test
  public void parseIntTest_UnsuccessfulNonNumericValue() {
    input = "a";
    Assertions.assertNull(StringUtils.parseInt(input));
  }
  
  @Test
  public void parseIntTest_NullInput() {
    input = null;
    parseIntOutput = StringUtils.parseInt(input);
    Assertions.assertNull(parseIntOutput);
  }
}
