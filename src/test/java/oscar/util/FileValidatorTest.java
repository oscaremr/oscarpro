/**
 * Copyright (c) 2023 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.util;

import java.io.File;
import java.io.IOException;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.*;

public class FileValidatorTest {
    private static final String BASE_DIRECTORY = "/base/directory";
    private FileValidator fileValidator;
    private final String dotDotFileName = "../test.file";
    private final String forwardSlashFileName = "/etc/passwd";
    private final String backSlashFileName = "\\Windows\\system32";

    @BeforeEach
    public void setUp() throws IOException {
        fileValidator = new FileValidator(BASE_DIRECTORY);
    }

    @Test
    public void givenFileName_whenIncludesDotDot_thenThrowSecurityException() {
        assertThrows(SecurityException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                fileValidator.validateAndReturnFile(dotDotFileName);
            }
        });
    }

    @Test
    public void givenFileName_whenIncludesForwardSlash_thenThrowSecurityException() {
        assertThrows(SecurityException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                fileValidator.validateAndReturnFile(forwardSlashFileName);
            }
        });
    }

    @Test
    public void givenFileName_whenIncludesBackSlash_thenThrowSecurityException() {
        assertThrows(SecurityException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                fileValidator.validateAndReturnFile(backSlashFileName);
            }
        });
    }

    @Test
    public void givenFile_whenOutsideBaseDirectory_thenThrowSecurityException() {
        val outsideFilePath = new File(BASE_DIRECTORY).getParent() + "/outside.txt";
        assertThrows(SecurityException.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                fileValidator.validateAndReturnFile(outsideFilePath);
            }
        });
    }

    @Test
    public void givenFile_whenInsideBaseDirectory_thenAssertTrue() throws IOException {
        val result = fileValidator.validateAndReturnFile("validFile.txt");
        assertTrue(result.getPath().startsWith(BASE_DIRECTORY));
    }

}