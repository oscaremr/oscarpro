package oscar.util;

import org.junit.Test;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OneIDUtilTest {
	
	private HttpServletRequest generateLoginRedirectUrlRequest(String requestUrl, String servletPath) {
		HttpServletRequest request = mock(HttpServletRequest.class);
		when(request.getRequestURL()).thenReturn(new StringBuffer(requestUrl));
		when(request.getServletPath()).thenReturn(servletPath);
		
		return request;
	}

	private HttpServletRequest generateLoginMessageRequest(String email, String errorMessage) {
		HttpServletRequest request = mock(HttpServletRequest.class);
		when(request.getParameter("email")).thenReturn(email);
		when(request.getParameter("errorMessage")).thenReturn(errorMessage);

		return request;
	}
	
	@Test
	public void testGetLoginRedirectUrlFromRequestUrl() {
		HttpServletRequest request = generateLoginRedirectUrlRequest("https://www.test.com/oscar/index.jsp", "/index.jsp");
		String redirectUrl = OneIDUtil.getLoginRedirectUrl(request);
		assertEquals("https://www.test.com/oscar/ssoLogin.do", redirectUrl);
	}
	
	@Test
	public void testGetLoginRedirectUrlFromRequestUrlWithPort() {
		HttpServletRequest request = generateLoginRedirectUrlRequest("https://www.test.com:8080/oscar/index.jsp", "/index.jsp");
		String redirectUrl = OneIDUtil.getLoginRedirectUrl(request);
		assertEquals("https://www.test.com:8080/oscar/ssoLogin.do", redirectUrl);
	}

	@Test
	public void testGetLoginRedirectUrlFromRequestUrlWithQueryString() {
		HttpServletRequest request = generateLoginRedirectUrlRequest("https://www.test.com/oscar/index.jsp?login=failed", "/index.jsp?login=failed");
		String redirectUrl = OneIDUtil.getLoginRedirectUrl(request);
		assertEquals("https://www.test.com/oscar/ssoLogin.do", redirectUrl);
	}

	@Test
	public void testGetLoginMessageWithEmail() {
		HttpServletRequest request = generateLoginMessageRequest("test@test.com", null);
		when(request.getParameter("email")).thenReturn("test@test.com");
		String loginMessage = OneIDUtil.getLoginMessage(request);
		assertEquals("Hello test@test.com<br>Please login with your OSCAR credentials to link your accounts.", loginMessage);
	}

	@Test
	public void testGetLoginMessageWithErrorMessage() {
		HttpServletRequest request = generateLoginMessageRequest(null, "Test Message");
		String loginMessage = OneIDUtil.getLoginMessage(request);
		assertEquals("Test Message", loginMessage);
	}

	@Test
	public void testGetLoginMessageWithEmailAndErrorMessage() {
		HttpServletRequest request = generateLoginMessageRequest("test@test.com", "Test Message");
		String loginMessage = OneIDUtil.getLoginMessage(request);
		assertEquals("Hello test@test.com<br>Please login with your OSCAR credentials to link your accounts.", loginMessage);
	}

	@Test
	public void testGetLoginMessageWithNullEmailAndErrorMessage() {
		HttpServletRequest request = generateLoginMessageRequest(null, null);
		String loginMessage = OneIDUtil.getLoginMessage(request);
		assertEquals("", loginMessage);
	}

	@Test
	public void givenXssInjectionAttemptOnEmail_whenGetLoginMessage_thenReturnEscapedMessage() {
		HttpServletRequest request = generateLoginMessageRequest("<script>alert('xss')</script>", null);
		String loginMessage = OneIDUtil.getLoginMessage(request);
		assertEquals("Hello &lt;script&gt;alert('xss')&lt;/script&gt;<br>Please login with your OSCAR credentials to link your accounts.", loginMessage);
	}

	@Test
	public void givenXssInjectionAttemptOnErrorMessage_whenGetLoginMessage_thenReturnEscapedMessage() {
		HttpServletRequest request = generateLoginMessageRequest(null, "<script>alert('xss')</script>");
		String loginMessage = OneIDUtil.getLoginMessage(request);
		assertEquals("&lt;script&gt;alert('xss')&lt;/script&gt;", loginMessage);
	}
}
