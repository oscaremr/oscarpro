package oscar.oscarLab.ca.all.pageUtil;

import com.lowagie.text.Chunk;
import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.function.ThrowingRunnable;
import org.junit.jupiter.api.DisplayName;
import oscar.oscarLab.ca.all.pageUtil.OLISLabPDFUtils.Hl7EncodedSpan;

public class OLISLabPDFUtilsTest {
  static List<Map<String, Object>> preparedHl7textInputsAndExpectedChunkProperties = new ArrayList<>();

  @BeforeClass
  public static void beforeClass() {
    OLISLabPDFUtilsTest.prepareTextInputAndExpectedChunksPropertiesResultForHl7Text();
  }

  /**
   * @todo Asserting the chunk background(highlighted text color in yellow) here probably should be removed after `Hl7EncodedSpan.HIGHLIGHT.createChunk()` is also covered with unit test
   */
  @Test
  @DisplayName("Test Enum Hl7EncodedSpan Method createChunksFromText with list of inputs and expected chunks output with predefined text value and highlight color")
  public void testEnumHl7EncodedSpanMethodCreateChunksFromTextForListOfInputs() {
    // assert null input
    Assert.assertThrows("Confirm null input throws NullPointerException",
        NullPointerException.class,
        createChunksFromTextThrowingNullPointerExceptionRunnable(null)
    );
    // assert prepared scenarios
    int testCasePointer = 0;
    for (Map<String, Object> inputOutputConfigItem: preparedHl7textInputsAndExpectedChunkProperties) {
      String inputConfigItem = inputOutputConfigItem.get("input").toString();
      ArrayList<HashMap<String, Object>> expectedOutputConfigItem =
          (ArrayList<HashMap<String, Object>>) inputOutputConfigItem.get("output");

      List<Chunk> result = OLISLabPDFUtils.Hl7EncodedSpan.createChunksFromText(inputConfigItem);
      // assert actual and expected size of chunks - this will
      Assert.assertEquals("Test Case at position (" + testCasePointer + "): Confirm resulting chunks list is of expected size",
          expectedOutputConfigItem.size(), result.size());

      // assert every resulting chunk has expected property values
      for (int i = 0; i < result.size(); i++) {
        // always check content
        Assert.assertEquals(
            "Test Case at position (" + testCasePointer + "): Confirm resulting chunk at position (" + i + ") has expected content",
            expectedOutputConfigItem.get(i).get("content"),
            result.get(i).getContent()
        );
        // check background property only if in expected config was not set as null
        if (expectedOutputConfigItem.get(i).get("backgroundColor") != null) {
          Object[] resultItemBackgroundAttribute = result.get(i).hasAttributes() &&
              result.get(i).getAttributes().get(Chunk.BACKGROUND) != null
              ? (Object[]) result.get(i).getAttributes().get(Chunk.BACKGROUND)
              : null;
          Color resultItemBackgroundColor = (Color) resultItemBackgroundAttribute[0];

          Assert.assertEquals(
              "Test Case at position (" + testCasePointer + "): Confirm resulting chunk at position (" + i + ") has expected background color",
              expectedOutputConfigItem.get(i).get("backgroundColor"),
              resultItemBackgroundColor
          );
        }
      }

      ++testCasePointer;
    }
  }

  private static void prepareTextInputAndExpectedChunksPropertiesResultForHl7Text() {
    preparedHl7textInputsAndExpectedChunkProperties = new ArrayList<>();
    // empty string input, expected one element output with empty content
    preparedHl7textInputsAndExpectedChunkProperties.add(new HashMap<String, Object>() {
      {
        put("input", "");
        put("output", new ArrayList<HashMap<String, Object>>() {
          {
            add(new HashMap<String, Object>() {
              {
                put("content", "");
                put("backgroundColor", null);
              }
            });
          }
        });
      }
    });
    // input without tags to format, expected one chunk output
    preparedHl7textInputsAndExpectedChunkProperties.add(new HashMap<String, Object>() {
      {
        put("input", "Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus. Aenean ornare interdum nisl * + -, at gravida leo dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.");
        put("output", new ArrayList<HashMap<String, Object>>() {
          {
            add(new HashMap<String, Object>() {
              {
                put("content", "Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus. Aenean ornare interdum nisl * + -, at gravida leo dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.");
                put("backgroundColor", null);
              }
            });
          }
        });
      }
    });
    // input with one pair of tags covering whole input text, expected one chunk with highlighted text
    preparedHl7textInputsAndExpectedChunkProperties.add(new HashMap<String, Object>() {
      {
        put("input", "\\.H\\Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus. Aenean ornare interdum nisl * + -, at gravida leo dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.\\.N\\");
        put("output", new ArrayList<HashMap<String, Object>>() {
          {
            add(new HashMap<String, Object>() {
              {
                put("content", "Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus. Aenean ornare interdum nisl * + -, at gravida leo dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.");
                put("backgroundColor", Color.YELLOW);
              }
            });
          }
        });
      }
    });
    // input with two pairs of tags, side by side without content between them, covering whole input text
    // expected two chunks with highlighted text
    preparedHl7textInputsAndExpectedChunkProperties.add(new HashMap<String, Object>() {
      {
        put("input", "\\.H\\Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus. Aenean ornare interdum nisl *\\.N\\\\.H\\ + -, at gravida leo dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.\\.N\\");
        put("output", new ArrayList<HashMap<String, Object>>() {
          {
            add(new HashMap<String, Object>() {
              {
                put("content", "Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus. Aenean ornare interdum nisl *");
                put("backgroundColor", Color.YELLOW);
              }
            });
            add(new HashMap<String, Object>() {
              {
                put("content", " + -, at gravida leo dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.");
                put("backgroundColor", Color.YELLOW);
              }
            });
          }
        });
      }
    });
    // input with one pair of tags at the start of the input text, expected two chunks with first chunk highlighted
    preparedHl7textInputsAndExpectedChunkProperties.add(new HashMap<String, Object>() {
      {
        put("input", "\\.H\\Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus.\\.N\\ Aenean ornare interdum nisl * + -, at gravida leo dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.");
        put("output", new ArrayList<HashMap<String, Object>>() {
          {
            add(new HashMap<String, Object>() {
              {
                put("content", "Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus.");
                put("backgroundColor", Color.YELLOW);
              }
            });
            add(new HashMap<String, Object>() {
              {
                put("content", " Aenean ornare interdum nisl * + -, at gravida leo dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.");
                put("backgroundColor", null);
              }
            });
          }
        });
      }
    });
    // input with one pair of tags covering the end of the input text, expected two chunks with second chunk highlighted
    preparedHl7textInputsAndExpectedChunkProperties.add(new HashMap<String, Object>() {
      {
        put("input", "Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus. \\.H\\Aenean ornare interdum nisl * + -, at gravida leo dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.\\.N\\");
        put("output", new ArrayList<HashMap<String, Object>>() {
          {
            add(new HashMap<String, Object>() {
              {
                put("content", "Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus. ");
                put("backgroundColor", null);
              }
            });
            add(new HashMap<String, Object>() {
              {
                put("content", "Aenean ornare interdum nisl * + -, at gravida leo dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.");
                put("backgroundColor", Color.YELLOW);
              }
            });
          }
        });
      }
    });
    // input with one pair of tags inside input text, expected three chunks with the middle chunk highlighted
    preparedHl7textInputsAndExpectedChunkProperties.add(new HashMap<String, Object>() {
      {
        put("input", "Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus. \\.H\\Aenean ornare interdum nisl * + -, at gravida leo\\.N\\ dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.");
        put("output", new ArrayList<HashMap<String, Object>>() {
          {
            add(new HashMap<String, Object>() {
              {
                put("content", "Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus. ");
                put("backgroundColor", null);
              }
            });
            add(new HashMap<String, Object>() {
              {
                put("content", "Aenean ornare interdum nisl * + -, at gravida leo");
                put("backgroundColor", Color.YELLOW);
              }
            });
            add(new HashMap<String, Object>() {
              {
                put("content", " dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.");
                put("backgroundColor", null);
              }
            });
          }
        });
      }
    });
    // input with two pairs of tags, one covering start of the text, one covering the end,
    // expected three chunks with the first and the third one are highlighted
    preparedHl7textInputsAndExpectedChunkProperties.add(new HashMap<String, Object>() {
      {
        put("input", "\\.H\\Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit.\\.N\\ Quisque id efficitur tellus. Aenean ornare interdum nisl * + -, at gravida \\.H\\leo dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.\\.N\\");
        put("output", new ArrayList<HashMap<String, Object>>() {
          {
            add(new HashMap<String, Object>() {
              {
                put("content", "Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit.");
                put("backgroundColor", Color.YELLOW);
              }
            });
            add(new HashMap<String, Object>() {
              {
                put("content", " Quisque id efficitur tellus. Aenean ornare interdum nisl * + -, at gravida ");
                put("backgroundColor", null);
              }
            });
            add(new HashMap<String, Object>() {
              {
                put("content", "leo dapibus in. Aliquam efficitur vestibulum diam, sed tempor nulla scelerisque non.");
                put("backgroundColor", Color.YELLOW);
              }
            });
          }
        });
      }
    });
    // input with two pairs of tags, side by side, without additional text between them,
    // inside input text, expected four chunks with the two in the middle chunks highlighted
    preparedHl7textInputsAndExpectedChunkProperties.add(new HashMap<String, Object>() {
      {
        put("input", "Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus. \\.H\\Aenean ornare interdum nisl * + -, at gravida leo\\.N\\\\.H\\ dapibus in. Aliquam efficitur vestibulum diam, sed tempor\\.N\\ nulla scelerisque non.");
        put("output", new ArrayList<HashMap<String, Object>>() {
          {
            add(new HashMap<String, Object>() {
              {
                put("content", "Lorem ipsum dolor sit amet, consectetur 1,1212.23 @ab adipiscing elit. Quisque id efficitur tellus. ");
                put("backgroundColor", null);
              }
            });
            add(new HashMap<String, Object>() {
              {
                put("content", "Aenean ornare interdum nisl * + -, at gravida leo");
                put("backgroundColor", Color.YELLOW);
              }
            });
            add(new HashMap<String, Object>() {
              {
                put("content", " dapibus in. Aliquam efficitur vestibulum diam, sed tempor");
                put("backgroundColor", Color.YELLOW);
              }
            });
            add(new HashMap<String, Object>() {
              {
                put("content", " nulla scelerisque non.");
                put("backgroundColor", null);
              }
            });
          }
        });
      }
    });
  }

  private ThrowingRunnable createChunksFromTextThrowingNullPointerExceptionRunnable(final String hl7text) {
    return new ThrowingRunnable() {
      public void run() throws NullPointerException {
        OLISLabPDFUtils.Hl7EncodedSpan.createChunksFromText(hl7text);
      }
    };
  }
}