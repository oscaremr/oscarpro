package oscar.oscarLab.ca.all.pageUtil;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class LabUtilsTest {

  @ParameterizedTest(name = "Given msgType = {0} and orderStatus = {1}, expect {2}")
  @CsvSource({
      "PATHL7, I, Pending",
      "PATHL7, P, Preliminary",
      "PATHL7, A, Partial results",
      "PATHL7, F, Complete",
      "PATHL7, R, Retransmitted",
      "PATHL7, C, Corrected",
      "PATHL7, X, Deleted",
      "PATHL7, Z, Z",
      "PATHL7, , Unknown",
      "PATHL7, F, Complete",
      "other, I, Pending",
      "other, P, P",
      "other, A, Partial results",
      "other, F, Final",
      "other, R, Retransmitted",
      "other, C, Corrected",
      "other, X, Deleted",
      "other, Z, Z",
      "other, , Unknown"
  })
  @DisplayName("Test getPatHl7OrderStatusString method")
  public void testGetPatHl7OrderStatusString(String msgType, String orderStatus, String expected) {
    String actual = LabUtils.getHl7OrderStatusString(msgType, orderStatus);
    Assertions.assertEquals(expected, actual);
  }
}