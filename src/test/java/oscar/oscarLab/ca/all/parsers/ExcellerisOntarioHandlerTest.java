package oscar.oscarLab.ca.all.parsers;

import ca.oscarpro.test.TagConstants;
import ca.uhn.hl7v2.HL7Exception;
import java.util.Arrays;
import java.util.Collection;
import lombok.AllArgsConstructor;
import lombok.val;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Tag;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@Tag(TagConstants.ON)
@AllArgsConstructor
@RunWith(Parameterized.class)
public class ExcellerisOntarioHandlerTest extends ExcellerisOntarioHandler {
  private final String hl7;
  private final String accession;

  @Parameterized.Parameters
  public static Collection parameters() {
    return Arrays.asList(new Object[][]{
        {
          "MSH|^~\\&|PATHL7|EXC-RV^LIFELABS ONTARIO|HTTPCLIENT||20220413160733-0400||ORU^R01|MDC20220413160733357|D|2.3.1|||AL\n"
              + "PID||||2022-1M6060328|EMR CASE13^HEREDITARY 1GC^^^^^L||20000303|M|||||^H\n"
              + "PV1|1|OU\n"
              + "ORC|RE||2022-EMR060328-GEN\n"
              + "OBR|1|2022-EMR060328-GEN-48_HERON|2022-EMR060328-GEN-48_HERON|HERON^Hereditary Report|||20220106000000|||||||20220106||LL008055^ALMOHRI^HUDA||||||20220106000000||Genetics|F|||LL008055^ALMOHRI^HUDA~LLON80808^MDCAREQA^BOB\n"
              + "OBX|1|ED|PDF^PDF||",
          "2022-EMR060328"
        },
        {
          "MSH|^~\\&|PATHL7|EXC-RV^LIFELABS ONTARIO|HTTPCLIENT||20220413160318-0400||ORU^R01|MDC20220413160318593|D|2.3.1|||AL\n"
              + "PID|||1112223334^^^^JHN^^^^ON&Ontario&HL70347^^BC|2022-1M6070511|LP CASE6^1MANCC 1EMR^^^^^L||20000303|F|||||^H\n"
              + "PV1|1|OU\n"
              + "ORC|RE||2022-EMR070511-GEN\n"
              + "OBR|1|2022-EMR070511-GEN-48_PANON|2022-EMR070511-GEN-48_PANON|PANON^Panorama Report|||20220107000000|||||||20220107|BLD&BLOOD&HL70070|63800^MACNABB^N.||||||20220110000000||Genetics|F|||95723^A. SADRY^SHARON~63800^MACNABB^N.~LLON80808^MDCAREQA^BOB\n"
              + "OBX|1|ED|PDF^PDF||",
          "2022-EMR070511"
        },
        {
          "MSH|^~\\&|PATHL7|EXC-RV^LIFELABS ONTARIO|HTTPCLIENT||20220413160823-0400||ORU^R01|MDC20220413160823462|D|2.3.1|||AL\n"
              + "PID|||1112223334^^^^JHN^^^^ON&Ontario&HL70347^^BC|2022-1M6070511|LP CASE6^1MANCC 1EMR^^^^^L||20000303|F|||||^H\n"
              + "PV1|1|OU\n"
              + "ORC|RE||2022-EMR070511-GEN\n"
              + "OBR|1|2022-EMR070511-GEN-48_PANON|2022-EMR070511-GEN-48_PANON|PANON^Panorama Report|||20220107000000|||||||20220107|BLD&BLOOD&HL70070|63800^MACNABB^N.||||||20220107000000||Genetics|P|||95723^A. SADRY^SHARON~63800^MACNABB^N.~LLON80808^MDCAREQA^BOB\n"
              + "OBX|1|ED|PDF^PDF||",
          "2022-EMR070511"
        },
        {
          "MSH|^~\\&|PATHL7|EXC-RV^LIFELABS ONTARIO|HTTPCLIENT||20220413160823-0400||ORU^R01|MDC20220413160823462|D|2.3.1|||AL\n"
              + "PID|||1112223334^^^^JHN^^^^ON&Ontario&HL70347^^BC|2022-1M6070511|LP CASE6^1MANCC 1EMR^^^^^L||20000303|F|||||^H\n"
              + "PV1|1|OU\n"
              + "ORC|RE||2022-EMR070511-GEN\n"
              + "OBR|1|2022-EMR070511-GEN-48_PANON|2022-EMR070511-GEN-48_PANON|PANON^Panorama Report|||20220107000000|||||||20220107|BLD&BLOOD&HL70070|63800^MACNABB^N.||||||20220107000000||Genetics|P|||95723^A. SADRY^SHARON~63800^MACNABB^N.~LLON80808^MDCAREQA^BOB\n"
              + "OBX|1|ED|PDF^PDF||",
          "2022-EMR070511"
        }
    });
  }

  @Test
  public void testGetAccessionNum() throws HL7Exception {
    val handler = new ExcellerisOntarioHandler();
    handler.init(hl7);
    val accession = handler.getAccessionNum();
    Assert.assertNotNull(accession);
    Assert.assertEquals(accession, this.accession);
  }
}
