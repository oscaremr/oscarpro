package oscar.oscarLab.ca.all;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.oscarehr.common.dao.Hl7MeasurementAllowListDao;
import org.oscarehr.common.dao.Hl7TextInfoDao;
import org.oscarehr.common.dao.Hl7TextMessageDao;
import org.oscarehr.common.dao.MeasurementDao;
import org.oscarehr.common.dao.MeasurementsExtDao;
import org.oscarehr.common.model.Hl7TextMessage;
import org.oscarehr.common.model.Measurement;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import oscar.oscarLab.ca.all.parsers.Factory;
import oscar.oscarLab.ca.all.parsers.PATHL7Handler;

@ExtendWith(MockitoExtension.class)
public class Hl7textResultsDataTest {

  private static final String labNo = "1";
  private static final String demographicNo = "1";
  private static final String labType = "OLIS_HL7";

  @Mock
  Hl7MeasurementAllowListDao hl7MeasurementAllowListDao;

  @Mock
  MeasurementsExtDao measurementsExtDao;

  @Mock
  MeasurementDao measurementDao;

  @Mock
  Hl7TextMessageDao hl7TextMessageDao;

  @Mock
  Hl7TextInfoDao hl7TextInfoDao;

  @Mock
  private OscarProperties oscarPropertiesMock;

  @Mock
  PATHL7Handler handler;

  private AutoCloseable closeable;
  private MockedStatic<SpringUtils> springUtils;
  private MockedStatic<OscarProperties> oscarPropertiesStatic;
  private MockedStatic<Factory> factory;

  @BeforeEach
  public void initialize() throws Exception {
    closeable = MockitoAnnotations.openMocks(this);
    springUtils = mockStatic(SpringUtils.class);
    oscarPropertiesStatic = mockStatic(OscarProperties.class);
    factory = mockStatic(Factory.class);
    mockObjects();
  }

  @AfterEach
  public void closeService() throws Exception {
    closeable.close();
    springUtils.close();
    oscarPropertiesStatic.close();
    factory.close();
  }

  @Test
  public void populateMeasurementsTableTest() {
    Hl7textResultsData.populateMeasurementsTable(labNo, demographicNo);
    verify(measurementDao, times(1)).persist(any(Measurement.class));
    verify(measurementsExtDao, times(1)).batchPersist(anyList(), anyInt());
  }

  private void mockObjects() {

    springUtils
        .when(() -> SpringUtils.getBean(Hl7MeasurementAllowListDao.class))
        .thenReturn(hl7MeasurementAllowListDao);

    when(hl7MeasurementAllowListDao.getAllowedHl7Measurements())
        .thenReturn(getAllowedHl7Measurements());

    springUtils
        .when(() -> SpringUtils.getBean(MeasurementsExtDao.class))
        .thenReturn(measurementsExtDao);

    springUtils
        .when(() -> SpringUtils.getBean(Hl7TextMessageDao.class))
        .thenReturn(hl7TextMessageDao);

    springUtils
        .when(() -> SpringUtils.getBean(Hl7TextInfoDao.class))
        .thenReturn(hl7TextInfoDao);

    springUtils
        .when(() -> SpringUtils.getBean(MeasurementDao.class))
        .thenReturn(measurementDao);

    when(hl7TextMessageDao.find(anyInt()))
        .thenAnswer(
            (Answer<Hl7TextMessage>)
                invocation -> {
                  Hl7TextMessage message = new Hl7TextMessage();
                  message.setType(labType);
                  return message;
                });

    when(hl7TextInfoDao.findByLabIdViaMagic(anyInt())).thenReturn(new ArrayList<>());

    oscarPropertiesStatic.when(OscarProperties::getInstance).thenReturn(oscarPropertiesMock);
    when(oscarPropertiesMock.hasProperty(eq("hl7_measurements_names_to_always_add")))
        .thenReturn(false);

    factory.when(() -> Factory.getHandler(labNo)).thenReturn(handler);

    doAnswer(
            (Answer<Object>)
                invocation -> {
                  Measurement measurement = (Measurement) invocation.getArguments()[0];
                  measurement.setId(1);
                  return null;
                })
        .when(measurementDao)
        .persist(any(Measurement.class));
    when(handler.getOBRCount()).thenReturn(1);
    when(handler.getOBXCount(anyInt())).thenReturn(1);
    when(handler.getOBXResult(anyInt(), anyInt())).thenReturn("1-5");
    when(handler.getOBXName(anyInt(), anyInt())).thenReturn("WBC");
    when(handler.getOBXResultStatus(anyInt(), anyInt())).thenReturn("A");
    when(handler.getOBXReferenceRange(anyInt(), anyInt())).thenReturn("1-5");
  }

  private List<String> getAllowedHl7Measurements() {
    List<String> measurements = new ArrayList<>();
    measurements.add("negative");
    measurements.add("non reactive");
    measurements.add("non-reactive");
    measurements.add("nonreactive");
    measurements.add("positive");
    measurements.add("reactive");
    return measurements;
  }
}
