package oscar.oscarLab.ca.on;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static oscar.oscarLab.ca.on.CommonLabTestValues.findMappedValuesByIdentCode;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import lombok.val;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.oscarehr.common.dao.MeasurementDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.util.SpringUtils;
import oscar.oscarLab.ca.all.parsers.Factory;
import oscar.oscarLab.ca.all.parsers.PATHL7Handler;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CommonLabTestValuesTest {
  private static final String DEMOGRAPHIC_NO = "007";
  private static final String IDENT_CODE = "1234-5";
  private static final String labType = "HL7";
  private static final String testName = "WBC";
  private static final String identCode = "-WBC-U";
  private static final String dateTime = "2024-03-28 19:27:56";
  private final Date someDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTime);
  private static final Integer demographicNo = 7;

  @Mock
  MeasurementDao measurementDao;
  
  @Mock
  SystemPreferencesDao systemPreferencesDao;
  
  @Mock
  PATHL7Handler handler;

  private AutoCloseable closeable;
  private MockedStatic<SpringUtils> springUtils;
  private MockedStatic<Factory> factory;

  public CommonLabTestValuesTest() throws ParseException { }

  @BeforeEach
  public void initialize() throws Exception {
    closeable = MockitoAnnotations.openMocks(this);
    mockObjects();
  }

  @AfterEach
  public void closeService() throws Exception {
    closeable.close();
    springUtils.close();
    factory.close();
  }
  
  @Test
  public void test_findValuesForTest() {
    CommonLabTestValues.findValuesForTest(labType, demographicNo, testName, identCode);
    verify(handler, times(1)).getOBXResult(anyInt(), anyInt());
  }

  @Test
  public void givenNoMeasurements_whenFindMappedValuesByLoinc_thenReturnsEmpty() {
    // Arrange
    mockFindMeasurementsByDemographicIdAndLoincCodeResult();
    // Act
    val result = findMappedValuesByIdentCode(DEMOGRAPHIC_NO, IDENT_CODE);
    // Assert
    assertEquals(0, result.size());
    verify(measurementDao).findMeasurementsByDemographicIdAndIdentCode(
        7, IDENT_CODE, null, null);
  }

  @Test
  public void givenMinimalFields_whenFindMappedValuesByLoinc_thenReturnsExpected() {
    // Arrange
    mockFindMeasurementsByDemographicIdAndLoincCodeResult(
        new Object[]{ 1, "2.5", someDate, "name", "Sodium" }
    );
    // Act
    val result = findMappedValuesByIdentCode(DEMOGRAPHIC_NO, IDENT_CODE);
    // Assert
    assertEquals(1, result.size());
    val first = result.get(0);
    assertEquals(3, first.size());
    assertEquals("2.5", first.get("result"));
    assertEquals("Sodium", first.get("testName"));
    assertEquals(someDate, first.get("collDateDate"));
    verify(measurementDao).findMeasurementsByDemographicIdAndIdentCode(
        7, IDENT_CODE, null, null);
  }

  @Test
  public void givenMinAndMax_whenFindMappedValuesByLoinc_thenReturnsRange() {
    // Arrange
    mockFindMeasurementsByDemographicIdAndLoincCodeResult(
        new Object[]{ 1, "2.5", someDate, "name", "Sodium" },
        new Object[]{ 1, "2.5", someDate, "minimum", "2" },
        new Object[]{ 1, "2.5", someDate, "maximum", "5" }
    );
    // Act
    val result = findMappedValuesByIdentCode(DEMOGRAPHIC_NO, IDENT_CODE);
    // Assert
    assertEquals(1, result.size());
    val first = result.get(0);
    assertEquals(6, first.size());
    assertEquals("2.5", first.get("result"));
    assertEquals("Sodium", first.get("testName"));
    assertEquals(someDate, first.get("collDateDate"));
    assertEquals("2", first.get("minimum"));
    assertEquals("5", first.get("maximum"));
    assertEquals("2-5", first.get("range"));
    verify(measurementDao).findMeasurementsByDemographicIdAndIdentCode(
        7, IDENT_CODE, null, null);
  }

  @Test
  public void givenFields_whenFindMappedValuesByLoinc_thenNormalizeKeyNames() {
    // Arrange
    mockFindMeasurementsByDemographicIdAndLoincCodeResult(
        new Object[]{1, "2.5", someDate, "name", "Sodium"},
        new Object[]{ 1, "2.5", someDate, "abnormal", "N" },
        new Object[]{ 1, "2.5", someDate, "unit", "mol" },
        new Object[]{ 1, "2.5", someDate, "datetime", dateTime }
    );
    // Act
    val result = findMappedValuesByIdentCode(DEMOGRAPHIC_NO, IDENT_CODE);
    // Assert
    assertEquals(1, result.size());
    val first = result.get(0);
    assertEquals(6, first.size());
    assertEquals("2.5", first.get("result"));
    assertEquals("Sodium", first.get("testName"));
    assertEquals(someDate, first.get("collDateDate"));
    assertEquals("N", first.get("abn"));
    assertEquals("mol", first.get("units"));
    assertEquals(dateTime, first.get("collDate"));
    verify(measurementDao).findMeasurementsByDemographicIdAndIdentCode(
        7, IDENT_CODE, null, null);
  }

  private void mockFindMeasurementsByDemographicIdAndLoincCodeResult(Object[]... result) {
    when(measurementDao.findMeasurementsByDemographicIdAndIdentCode(
        anyInt(), anyString(), (Date) any(), (Date) any())
    ).thenReturn(Arrays.asList(result));
  }

  private void mockObjects() {
    springUtils = mockStatic(SpringUtils.class);
    factory = mockStatic(Factory.class);

    springUtils
        .when(() -> SpringUtils.getBean(MeasurementDao.class))
        .thenReturn(measurementDao);
    springUtils
        .when(() -> SpringUtils.getBean(SystemPreferencesDao.class))
        .thenReturn(systemPreferencesDao);
    factory.when(() -> Factory.getHandler(anyString())).thenReturn(handler);
    
    when(systemPreferencesDao.getPreferenceValueByName(anyString(),anyString())).thenReturn("true");
    when(measurementDao.findLabNumbersBasedOnLoinc(anyInt(), anyString())).thenReturn(getLabNumberList());
    when(handler.getOBRCount()).thenReturn(1);
    when(handler.getOBXCount(anyInt())).thenReturn(1);
    when(handler.getOBXName(anyInt(), anyInt())).thenReturn("WBC");
    when(handler.getOBXIdentifier(anyInt(), anyInt())).thenReturn("-WBC-U");
    when(handler.getOBXResult(anyInt(), anyInt())).thenReturn("1-5");
    when(handler.getTimeStamp(anyInt(), anyInt())).thenReturn("9999-99-99");
  }
  
  private List<Object> getLabNumberList() {
    List<Object> labList = new ArrayList<Object>();
    labList.add("1");
    return labList;
  }

}
