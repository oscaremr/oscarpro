package oscar.oscarLab.ca.on;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Date;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.Hl7TextInfoDao;
import org.oscarehr.common.dao.IncomingLabRulesDao;
import org.oscarehr.common.dao.ProviderLabRoutingDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.dao.TableModificationDao;
import org.oscarehr.common.model.ProviderLabRoutingModel;
import org.oscarehr.util.SpringUtils;
import utils.OscarPropertiesUtil;

@ExtendWith(MockitoExtension.class)
public class CommonLabResultDataTest {

  @Mock private IncomingLabRulesDao incomingLabRulesDao;
  @Mock private ProviderLabRoutingDao providerLabRoutingDao;
  @Mock private static SystemPreferencesDao systemPreferencesDao;
  @Mock private TableModificationDao tableModificationDao;
  @Mock private Hl7TextInfoDao hl7TextInfoDao;
  private MockedStatic<SpringUtils> springUtils;

  /**
   * Sets up the test environment before each test.
   */
  @BeforeEach
  public void init() {
    springUtils = mockStatic(SpringUtils.class);
    OscarPropertiesUtil.getOscarProperties();
    springUtils
        .when(() -> SpringUtils.getBean(SystemPreferencesDao.class))
        .thenReturn(systemPreferencesDao);
    springUtils
        .when(() -> SpringUtils.getBean(ProviderLabRoutingDao.class))
        .thenReturn(providerLabRoutingDao);
    springUtils
        .when(() -> SpringUtils.getBean(IncomingLabRulesDao.class))
        .thenReturn(incomingLabRulesDao);
    springUtils
        .when(() -> SpringUtils.getBean(TableModificationDao.class))
        .thenReturn(tableModificationDao);
    springUtils
        .when(() -> SpringUtils.getBean(Hl7TextInfoDao.class))
        .thenReturn(hl7TextInfoDao);

    when(systemPreferencesDao.isReadBooleanPreferenceWithDefault(
        "enable_epsilon_labs", false)).thenReturn(true);
    when(providerLabRoutingDao.findByLabNoAndLabTypeAndProviderNo(
        675, "HL7", "999998")).
        thenReturn(Arrays.asList(new ProviderLabRoutingModel(
            "999998", 675, "N", "aaaaaa", new Date(), "HL7")));
  }

  @AfterEach
  public void close() {
    springUtils.close();
  }

  @Test
  public void givenUpdateReportStatus_whenWithComment_thenUpdate() {
    ReportStatusUpdateResponse reportStatusUpdateResponse =
        CommonLabResultData.updateReportStatus(
            675, "999998", 'N', "test", "HL7", "");
    assertTrue(reportStatusUpdateResponse.isSuccess());
  }
}
