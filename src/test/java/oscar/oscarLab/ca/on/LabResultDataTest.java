/*
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.oscarLab.ca.on;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static oscar.oscarLab.ca.on.LabResultData.isFinalReport;
import org.junit.Before;
import org.junit.Test;

public class LabResultDataTest {
  private static final String testErrorMessage = "Output is not what is expected";
  private LabResultData lrd;

  @Before
  public void init() {
    lrd = new LabResultData();
  }

  @Test
  public void testForReportStatus_reportStatusIsNull() {
    assertFalse(testErrorMessage, isFinalReport(lrd));
  }

  @Test
  public void testForReportStatus_reportStatusIsF() {
    lrd.reportStatus = "F";
    assertTrue(testErrorMessage, isFinalReport(lrd));
  }

  @Test
  public void testForReportStatus_reportStatusIsComplete() {
    lrd.reportStatus = "complete";
    assertTrue(testErrorMessage, isFinalReport(lrd));
  }

  @Test
  public void testForReportStatus_reportStatusIsC() {
    lrd.reportStatus = "C";
    assertTrue(testErrorMessage, isFinalReport(lrd));
  }

  @Test
  public void testForReportStatus_reportStatusIsNotCompleteOrFOrC() {
    lrd.reportStatus = "partial";
    assertFalse(testErrorMessage, isFinalReport(lrd));
  }

  @Test
  public void testForLabType_LabTypeIsHrm() {
    lrd.labType = "HRM";
    assertTrue(testErrorMessage, isFinalReport(lrd));
  }
}