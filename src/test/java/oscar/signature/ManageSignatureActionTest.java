/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.signature;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.oscarehr.common.dao.DigitalSignatureDao;
import org.oscarehr.common.dao.DigitalSignatureFavouriteDao;
import org.oscarehr.common.model.DigitalSignature;
import org.oscarehr.common.model.DigitalSignatureFavourite;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;

@ExtendWith({MockitoExtension.class})
public class ManageSignatureActionTest{

  private final int ID = 1;
  private final boolean SIGNATURE_FAVORITE = false;
  private final boolean SIGNATURE_ARCHIVED = true;
  private final String LABEL = "test";
  private final String PROVIDERNO = "1";

  private MockedStatic<LoggedInInfo> loggedInInfoMockedStatic;
  private MockedStatic<SpringUtils> springUtilsMockedStatic;

  @Mock private HttpServletRequest request;
  @Mock private HttpServletResponse response;
  @Mock private DigitalSignatureDao digitalSignatureDao;
  @Mock private DigitalSignatureFavouriteDao digitalSignatureFavouriteDao;

  @Captor private ArgumentCaptor<DigitalSignature> digitalSignatureArgumentCaptor;
  @Captor private ArgumentCaptor<DigitalSignatureFavourite> digitalSignatureFavouriteArgumentCaptor;
  @Captor private ArgumentCaptor<String> jsonSignatureArgumentCaptor;

  ManageSignatureAction manageSignatureAction;

  @BeforeEach
  public void before() {
    manageSignatureAction = new ManageSignatureAction();
    springUtilsMockedStatic = mockStatic(SpringUtils.class);
    mockDigitalSignatureDao();
    mockDigitalSignatureFavouriteDao();
    mockProviderFavouriteSignatures();
  }

  @AfterEach
  public void close(){
    springUtilsMockedStatic.close();
    loggedInInfoMockedStatic.close();
  }

  @Test
  public void givenSignature_whenUpdateSignature_thenReturnUpdateSignature() throws IOException {
    // mock the request
    when(request.getParameter("id")).thenReturn(Integer.toString(ID));
    when(request.getParameter("label")).thenReturn(LABEL);
    when(request.getParameter("isArchived")).thenReturn(Boolean.toString(false));
    // mock response writer
    PrintWriter mockPrintWriter = mock(PrintWriter.class);
    when(response.getWriter()).thenReturn(mockPrintWriter);
    // return a signature to verify
    DigitalSignature signature = new DigitalSignature();
    when(digitalSignatureDao.find(ID)).thenReturn(signature);
    // test the action
    assertNull(manageSignatureAction.updateSignature(null, null, request, response));
    // verify the signature is merged and capture input
    verify(digitalSignatureDao).merge(digitalSignatureArgumentCaptor.capture());
    DigitalSignature capturedSignature = digitalSignatureArgumentCaptor.getValue();
    assertEquals(LABEL, capturedSignature.getLabel());
    // verify the favourite signature is saved and capture input
    verify(digitalSignatureFavouriteDao).merge(digitalSignatureFavouriteArgumentCaptor.capture());
    assertEquals(ID, digitalSignatureFavouriteArgumentCaptor.getValue().getSignatureId());
    assertFalse(digitalSignatureFavouriteArgumentCaptor.getValue().isArchived());
    // verify the response
    verify(response).setContentType("application/json");
    // verify the json response is correct
    verify(response).getWriter();
    verify(mockPrintWriter).write(jsonSignatureArgumentCaptor.capture());
    String jsonSignature = jsonSignatureArgumentCaptor.getValue();
    ObjectMapper objectMapper = new ObjectMapper();
    JsonNode jsonNode = objectMapper.readTree(jsonSignature);
    assertEquals(LABEL, jsonNode.get("label").asText());
  }

  @Test
  public void givenSignatures_whenGetFavouriteSignatures_thenReturnFavoriteSignatures() throws IOException {
    // mock response writer
    PrintWriter mockPrintWriter = mock(PrintWriter.class);
    when(response.getWriter()).thenReturn(mockPrintWriter);
    // return a signature to verify
    DigitalSignature signature = new DigitalSignature();
    signature.setId(ID);
    signature.setLabel(LABEL);
    when(request.getParameter("archived")).thenReturn("false");
    when(digitalSignatureDao.getLatestSignatures(SIGNATURE_FAVORITE))
        .thenReturn(Collections.singletonList(signature));
    // test the action
    assertNull(manageSignatureAction.getSignatures(null, null, request, response));
    // verify signatures are retrieved
    verify(digitalSignatureDao, times(1)).getLatestSignatures(SIGNATURE_FAVORITE);
    // verify the response
    verify(response).setContentType("application/json");
    // verify the json response is correct
    verify(response).getWriter();
    verify(mockPrintWriter).write(jsonSignatureArgumentCaptor.capture());
    String jsonSignature = jsonSignatureArgumentCaptor.getValue();
    ObjectMapper objectMapper = new ObjectMapper();
    JsonNode jsonNode = objectMapper.readTree(jsonSignature).get(0);
    assertEquals(LABEL, jsonNode.get("label").asText());
  }

  @Test
  public void givenSignatures_whenGetProviderFavouriteSignatures_thenReturnProviderFavoriteSignatures() throws IOException {
    // mock response writer
    PrintWriter mockPrintWriter = mock(PrintWriter.class);
    when(response.getWriter()).thenReturn(mockPrintWriter);
    // return a signature to verify
    DigitalSignature signature = new DigitalSignature();
    signature.setLabel(LABEL);
    signature.setProviderNo(PROVIDERNO);
    when(request.getParameter("archived")).thenReturn("false");
    when(LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo())
        .thenReturn(signature.getProviderNo());
    when(digitalSignatureDao.getProviderSignatures(signature.getProviderNo(), SIGNATURE_FAVORITE))
        .thenReturn(Collections.singletonList(signature));
    // test the action
    assertNull(manageSignatureAction.getProviderSignatures(null, null, request, response));
    // verify signatures are retrieved
    verify(digitalSignatureDao, times(1)).getProviderSignatures(PROVIDERNO, SIGNATURE_FAVORITE);
    // verify the response
    verify(response).setContentType("application/json");
    // verify the json response is correct
    verify(response).getWriter();
    verify(mockPrintWriter).write(jsonSignatureArgumentCaptor.capture());
    String jsonSignature = jsonSignatureArgumentCaptor.getValue();
    ObjectMapper objectMapper = new ObjectMapper();
    JsonNode jsonNode = objectMapper.readTree(jsonSignature).get(0);
    assertEquals(LABEL, jsonNode.get("label").asText());
  }

  @Test
  public void givenSignatures_whenGetArchivedSignatures_thenReturnArchivedSignatures() throws IOException {
    // mock response writer
    PrintWriter mockPrintWriter = mock(PrintWriter.class);
    when(response.getWriter()).thenReturn(mockPrintWriter);
    // return a signature to verify
    DigitalSignature signature = new DigitalSignature();
    signature.setId(ID);
    signature.setLabel(LABEL);
    when(request.getParameter("archived")).thenReturn("true");
    when(digitalSignatureDao.getLatestSignatures(SIGNATURE_ARCHIVED))
        .thenReturn(Collections.singletonList(signature));
    // test the action
    assertNull(manageSignatureAction.getSignatures(null, null, request, response));
    // verify signatures are retrieved
    verify(digitalSignatureDao, times(1)).getLatestSignatures(SIGNATURE_ARCHIVED);
    // verify the response
    verify(response).setContentType("application/json");
    // verify the json response is correct
    verify(response).getWriter();
    verify(mockPrintWriter).write(jsonSignatureArgumentCaptor.capture());
    String jsonSignature = jsonSignatureArgumentCaptor.getValue();
    ObjectMapper objectMapper = new ObjectMapper();
    JsonNode jsonNode = objectMapper.readTree(jsonSignature).get(0);
    assertEquals(LABEL, jsonNode.get("label").asText());
  }

  @Test
  public void givenSignatures_whenGetProviderArchivedSignatures_thenReturnArchivedSignatures() throws IOException {
    // mock response writer
    PrintWriter mockPrintWriter = mock(PrintWriter.class);
    when(response.getWriter()).thenReturn(mockPrintWriter);
    // return a signature to verify
    DigitalSignature signature = new DigitalSignature();
    signature.setLabel(LABEL);
    signature.setProviderNo(PROVIDERNO);
    when(LoggedInInfo.getLoggedInInfoFromSession(request).getLoggedInProviderNo())
        .thenReturn(signature.getProviderNo());
    when(request.getParameter("archived")).thenReturn("true");
    when(digitalSignatureDao.getProviderSignatures(signature.getProviderNo(), SIGNATURE_ARCHIVED))
    .thenReturn(Collections.singletonList(signature));
    // test the action
    assertNull(manageSignatureAction.getProviderSignatures(null, null, request, response));
    // verify signatures are retrieved
    verify(digitalSignatureDao, times(1)).getProviderSignatures(PROVIDERNO, SIGNATURE_ARCHIVED);
    // verify the response
    verify(response).setContentType("application/json");
    // verify the json response is correct
    verify(response).getWriter();
    verify(mockPrintWriter).write(jsonSignatureArgumentCaptor.capture());
    String jsonSignature = jsonSignatureArgumentCaptor.getValue();
    ObjectMapper objectMapper = new ObjectMapper();
    JsonNode jsonNode = objectMapper.readTree(jsonSignature).get(0);
    assertEquals(LABEL, jsonNode.get("label").asText());
  }

  private void mockDigitalSignatureDao() {
    val digitalSignatureDaoAnswer = new Answer<DigitalSignatureDao>() {
      @Override
      public DigitalSignatureDao answer(InvocationOnMock invocation) {
        return digitalSignatureDao;
      }
    };
    when(SpringUtils.getBean(DigitalSignatureDao.class))
        .thenAnswer(digitalSignatureDaoAnswer);
  }

  private void mockDigitalSignatureFavouriteDao() {
    val digitalSignatureFavouriteDaoAnswer =
        new Answer<DigitalSignatureFavouriteDao>() {
          @Override
          public DigitalSignatureFavouriteDao answer(InvocationOnMock invocation) {
            return digitalSignatureFavouriteDao;
          }
        };
    when(SpringUtils.getBean(DigitalSignatureFavouriteDao.class))
        .thenAnswer(digitalSignatureFavouriteDaoAnswer);
    lenient().when(digitalSignatureFavouriteDao.findFavouriteSignatureById(ID))
        .thenReturn(createTestDigitalSignatureFavourite());
  }

  private DigitalSignatureFavourite createTestDigitalSignatureFavourite() {
    val digitalSignatureFavourite = new DigitalSignatureFavourite();
    digitalSignatureFavourite.setSignatureId(ID);
    digitalSignatureFavourite.setArchived(true);
    return digitalSignatureFavourite;
  }

  private void mockProviderFavouriteSignatures(){
    loggedInInfoMockedStatic = mockStatic(LoggedInInfo.class);
    final LoggedInInfo loggedInInfo = mock(LoggedInInfo.class);
    when(LoggedInInfo.getLoggedInInfoFromSession(request)).thenAnswer(new Answer<LoggedInInfo>() {
      @Override
      public LoggedInInfo answer(InvocationOnMock invocation) {
        return loggedInInfo;
      }
    });
  }
}