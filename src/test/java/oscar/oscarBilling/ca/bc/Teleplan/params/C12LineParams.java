package oscar.oscarBilling.ca.bc.Teleplan.params;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class C12LineParams {
  private String datacenter;
  private String dataseq;
  private String payeeno;
  private String practitionerno;
  private String exp1;
  private String exp2;
  private String exp3;
  private String exp4;
  private String exp5;
  private String exp6;
  private String exp7;
  private String officefolioclaimno;
}
