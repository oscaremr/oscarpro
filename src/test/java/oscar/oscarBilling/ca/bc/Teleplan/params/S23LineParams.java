package oscar.oscarBilling.ca.bc.Teleplan.params;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class S23LineParams {
  private String datacenter;
  private String dataseq;
  private String payment;
  private String linecode;
  private String payeeno;
  private String mspctlno;
  private String ajc;
  private String aji;
  private String ajm;
  private String calcmethod;
  private String rpercent;
  private String opercent;
  private String gamount;
  private String ramount;
  private String oamount;
  private String balancefwd;
  private String adjmade;
  private String adjoutstanding;
}
