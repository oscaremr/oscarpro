package oscar.oscarBilling.ca.bc.Teleplan;

import ca.oscarpro.test.TagConstants;
import org.junit.jupiter.api.Tag;
import oscar.oscarBilling.ca.bc.Teleplan.params.C12LineParams;
import oscar.oscarBilling.ca.bc.Teleplan.params.S01LineParams;
import oscar.oscarBilling.ca.bc.Teleplan.params.S02LineParams;
import oscar.oscarBilling.ca.bc.Teleplan.params.S04LineParams;
import oscar.oscarBilling.ca.bc.Teleplan.params.S21LineParams;
import oscar.oscarBilling.ca.bc.Teleplan.params.S22LineParams;
import oscar.oscarBilling.ca.bc.Teleplan.params.S23LineParams;

@Tag(TagConstants.BC)
public abstract class TeleplanServiceTestBase {

  protected String buildVrcLine(
      String datacenter,
      String payment,
      String recordGroup,
      String recordCount,
      String timestamp)
  {
    // this is mostly in place to prevent the developer from accidentally creating an invalid test line
    assert datacenter.length() == 5;
    assert payment.length() == 8;
    assert recordGroup.length() == 1;
    assert recordCount.length() == 7;
    assert timestamp.length() == 20;

    String fillWhitespace122Chars = "                                                                              " +
            "                                            "; // length 122
    return "VRC" + datacenter + payment + recordGroup + recordCount + timestamp + fillWhitespace122Chars;
  }

  protected String buildC12Line(C12LineParams params) {
    // this is mostly in place to prevent the developer from accidentally creating an invalid test line
    assert params.getDatacenter().length() == 5;
    assert params.getDataseq().length() == 7;
    assert params.getPayeeno().length() == 5;
    assert params.getPractitionerno().length() == 5;
    assert params.getExp1().length() == 2;
    assert params.getExp2().length() == 2;
    assert params.getExp3().length() == 2;
    assert params.getExp4().length() == 2;
    assert params.getExp5().length() == 2;
    assert params.getExp6().length() == 2;
    assert params.getExp7().length() == 2;
    assert params.getOfficefolioclaimno().length() == 7;

    String fillWhitespace24Chars = "                        "; // length 24
    return "C12" + params.getDatacenter() + params.getDataseq() + params.getPayeeno() + params.getPractitionerno() +
        params.getExp1() + params.getExp2() + params.getExp3() + params.getExp4() + params.getExp5() +
        params.getExp6() + params.getExp7() + params.getOfficefolioclaimno() + fillWhitespace24Chars;
  }

  protected String buildS01Line(S01LineParams params) {
    // this is mostly in place to prevent the developer from accidentally creating an invalid test line
    assert params.getDatacenter().length() == 5;
    assert params.getDataseq().length() == 7;
    assert params.getPayment().length() == 8;
    assert params.getLinecode().length() == 1;
    assert params.getPayeeno().length() == 5;
    assert params.getMspctlno().length() == 6;
    assert params.getPractitionerno().length() == 5;
    assert params.getAjc1().length() == 2;
    assert params.getAja1().length() == 7;
    assert params.getAjc2().length() == 2;
    assert params.getAja2().length() == 7;
    assert params.getAjc3().length() == 2;
    assert params.getAja3().length() == 7;
    assert params.getAjc4().length() == 2;
    assert params.getAja4().length() == 7;
    assert params.getAjc5().length() == 2;
    assert params.getAja5().length() == 7;
    assert params.getAjc6().length() == 2;
    assert params.getAja6().length() == 7;
    assert params.getAjc7().length() == 2;
    assert params.getAja7().length() == 7;
    assert params.getOfficeno().length() == 7;
    assert params.getPaidamt().length() == 7;
    assert params.getMsprcddate().length() == 8;
    assert params.getPaidrate().length() == 2;
    assert params.getIcbcwcb().length() == 8;
    assert params.getInsurercode().length() == 2;

    String fillWhitespace28Chars = "                            "; // length 28
    return "S01" + params.getDatacenter() + params.getDataseq() +  params.getPayment() + params.getLinecode() +
        params.getPayeeno() + params.getMspctlno() + params.getPractitionerno() +
        params.getAjc1() + params.getAja1() + params.getAjc2() + params.getAja2() + params.getAjc3() +
        params.getAja3() + params.getAjc4() + params.getAja4() + params.getAjc5() + params.getAja5() +
        params.getAjc6() + params.getAja6() + params.getAjc7() + params.getAja7() + params.getOfficeno() +
        params.getPaidamt() + params.getMsprcddate() + params.getPaidrate() + params.getIcbcwcb() +
        params.getInsurercode() + fillWhitespace28Chars;
  }
  protected String buildS02Line(S02LineParams params) {
    // this is mostly in place to prevent the developer from accidentally creating an invalid test line
    assert params.getDatacenter().length() == 5;
    assert params.getDataseq().length() == 7;
    assert params.getPayment().length() == 8;
    assert params.getLinecode().length() == 1;
    assert params.getPayeeno().length() == 5;
    assert params.getMspctlno().length() == 6;
    assert params.getPractitionerno().length() == 5;
    assert params.getMsprcddate().length() == 8;
    assert params.getInitial().length() == 2;
    assert params.getSurname().length() == 18;
    assert params.getPhn().length() == 10;
    assert params.getPhndepno().length() == 2;
    assert params.getServicedate().length() == 8;
    assert params.getToday().length() == 2;
    assert params.getBillnoservices().length() == 3;
    assert params.getBillclafcode().length() == 2;
    assert params.getBillfeeschedule().length() == 5;
    assert params.getBillamt().length() == 7;
    assert params.getPaidnoservices().length() == 3;
    assert params.getPaidclafcode().length() == 2;
    assert params.getPaidfeeschedule().length() == 5;
    assert params.getPaidamt().length() == 7;
    assert params.getOfficeno().length() == 7;
    assert params.getExp1().length() == 2;
    assert params.getExp2().length() == 2;
    assert params.getExp3().length() == 2;
    assert params.getExp4().length() == 2;
    assert params.getExp5().length() == 2;
    assert params.getExp6().length() == 2;
    assert params.getExp7().length() == 2;
    assert params.getAjc1().length() == 2;
    assert params.getAja1().length() == 7;
    assert params.getAjc2().length() == 2;
    assert params.getAja2().length() == 7;
    assert params.getAjc3().length() == 2;
    assert params.getAja3().length() == 7;
    assert params.getAjc4().length() == 2;
    assert params.getAja4().length() == 7;
    assert params.getAjc5().length() == 2;
    assert params.getAja5().length() == 7;
    assert params.getAjc6().length() == 2;
    assert params.getAja6().length() == 7;
    assert params.getAjc7().length() == 2;
    assert params.getAja7().length() == 7;
    assert params.getPlanrefno().length() == 10;
    assert params.getClaimsource().length() == 1;
    assert params.getPreviouspaiddate().length() == 8;
    assert params.getInsurercode().length() == 2;
    assert params.getIcbcwcb().length() == 8;

    String fillWhitespace30Chars = "                              "; // length 30
    return "S02" + params.getDatacenter() + params.getDataseq() + params.getPayment() + params.getLinecode() +
        params.getPayeeno() + params.getMspctlno() + params.getPractitionerno() + params.getMsprcddate() +
        params.getInitial() + params.getSurname() + params.getPhn() + params.getPhndepno() + params.getServicedate() +
        params.getToday() + params.getBillnoservices() + params.getBillclafcode() + params.getBillfeeschedule() +
        params.getBillamt() + params.getPaidnoservices() + params.getPaidclafcode() + params.getPaidfeeschedule() +
        params.getPaidamt() + params.getOfficeno() + params.getExp1() + params.getExp2() + params.getExp3() +
        params.getExp4() + params.getExp5() + params.getExp6() + params.getExp7() + params.getAjc1() +
        params.getAja1() + params.getAjc2() + params.getAja2() + params.getAjc3() + params.getAja3() +
        params.getAjc4() + params.getAja4() + params.getAjc5() + params.getAja5() + params.getAjc6() +
        params.getAja6() + params.getAjc7() + params.getAja7() + params.getPlanrefno() + params.getClaimsource() +
        params.getPreviouspaiddate() + params.getInsurercode() + params.getIcbcwcb() + fillWhitespace30Chars;
  }

  protected String buildS04Line(S04LineParams params) {
    // this is mostly in place to prevent the developer from accidentally creating an invalid test line
    assert params.getDatacenter().length() == 5;
    assert params.getDataseq().length() == 7;
    assert params.getPayment().length() == 8;
    assert params.getLinecode().length() == 1;
    assert params.getPayeeno().length() == 5;
    assert params.getMspctlno().length() == 6;
    assert params.getPractitionerno().length() == 5;
    assert params.getMsprcddate().length() == 8;
    assert params.getOfficeno().length() == 7;
    assert params.getExp1().length() == 2;
    assert params.getExp2().length() == 2;
    assert params.getExp3().length() == 2;
    assert params.getExp4().length() == 2;
    assert params.getExp5().length() == 2;
    assert params.getExp6().length() == 2;
    assert params.getExp7().length() == 2;
    assert params.getIcbcwcb().length() == 8;
    assert params.getInsurercode().length() == 2;

    String fillWhitespace86Chars =
            "                                                                                      "; // length 86
    return "S04" + params.getDatacenter() + params.getDataseq() +  params.getPayment() + params.getLinecode() +
        params.getPayeeno() + params.getMspctlno() + params.getPractitionerno() + params.getMsprcddate() +
        params.getOfficeno() + params.getExp1() + params.getExp2() + params.getExp3() + params.getExp4() +
        params.getExp5() + params.getExp6() + params.getExp7() + params.getIcbcwcb() + params.getInsurercode() +
        fillWhitespace86Chars;
  }

  protected String buildS21Line(S21LineParams params) {
    // this is mostly in place to prevent the developer from accidentally creating an invalid test line
    assert params.getDatacenter().length() == 5;
    assert params.getDataseq().length() == 7;
    assert params.getPayment().length() == 8;
    assert params.getLinecode().length() == 1;
    assert params.getPayeeno().length() == 5;
    assert params.getMspctlno().length() == 6;
    assert params.getPayeename().length() == 25;
    assert params.getAmtbilled().length() == 9;
    assert params.getAmtpaid().length() == 9;
    assert params.getBalancefwd().length() == 9;
    assert params.getCheque().length() == 9;
    assert params.getNewbalance().length() == 9;

    String fillWhitespace60Chars = "                                                            "; // length 60
    return "S21" + params.getDatacenter() + params.getDataseq() + params.getPayment() + params.getLinecode() +
        params.getPayeeno() + params.getMspctlno() + params.getPayeename() + params.getAmtbilled() +
        params.getAmtpaid() + params.getBalancefwd() + params.getCheque() + params.getNewbalance() +
        fillWhitespace60Chars;
  }

  protected String buildS22Line(S22LineParams params) {
    // this is mostly in place to prevent the developer from accidentally creating an invalid test line
    assert params.getDatacenter().length() == 5;
    assert params.getDataseq().length() == 7;
    assert params.getPayment().length() == 8;
    assert params.getLinecode().length() == 1;
    assert params.getPayeeno().length() == 5;
    assert params.getMspctlno().length() == 6;
    assert params.getPractitionerno().length() == 5;
    assert params.getPractitionername().length() == 25;
    assert params.getAmtbilled().length() == 9;
    assert params.getAmtpaid().length() == 9;

    String fillWhitespace82Chars =
            "                                                                                  "; // length 82
    return "S22" + params.getDatacenter() + params.getDataseq() + params.getPayment() + params.getLinecode() +
        params.getPayeeno() + params.getMspctlno() + params.getPractitionerno() + params.getPractitionername() +
        params.getAmtbilled() + params.getAmtpaid() + fillWhitespace82Chars;
  }

  protected String buildS23Line(S23LineParams params) {
    // this is mostly in place to prevent the developer from accidentally creating an invalid test line
    assert params.getDatacenter().length() == 5;
    assert params.getDataseq().length() == 7;
    assert params.getPayment().length() == 8;
    assert params.getLinecode().length() == 1;
    assert params.getPayeeno().length() == 5;
    assert params.getMspctlno().length() == 6;
    assert params.getAjc().length() == 2;
    assert params.getAji().length() == 12;
    assert params.getAjm().length() == 20;
    assert params.getCalcmethod().length() == 1;
    assert params.getRpercent().length() == 5;
    assert params.getOpercent().length() == 5;
    assert params.getGamount().length() == 9;
    assert params.getRamount().length() == 9;
    assert params.getOamount().length() == 9;
    assert params.getBalancefwd().length() == 9;
    assert params.getAdjmade().length() == 9;
    assert params.getAdjoutstanding().length() == 9;

    String fillWhitespace31Chars = "                               "; // length 31
    return "S23" + params.getDatacenter() + params.getDataseq() + params.getPayment() + params.getLinecode() +
        params.getPayeeno() + params.getMspctlno() + params.getAjc() + params.getAji() + params.getAjm() +
        params.getCalcmethod() + params.getRpercent() + params.getOpercent() + params.getGamount() +
        params.getRamount() + params.getOamount() + params.getBalancefwd() + params.getAdjmade() +
        params.getAdjoutstanding() + fillWhitespace31Chars;
  }

  protected String buildS25Line(
      String datacenter,
      String dataseq,
      String payment,
      String linecode,
      String payeeno,
      String mspctlno,
      String practitionerno,
      String message) {
    // this is mostly in place to prevent the developer from accidentally creating an invalid test line
    assert datacenter.length() == 5;
    assert dataseq.length() == 7;
    assert payment.length() == 8;
    assert linecode.length() == 1;
    assert payeeno.length() == 5;
    assert mspctlno.length() == 6;
    assert practitionerno.length() == 5;
    assert message.length() == 80;

    String fillWhitespace45Chars = "                                             "; // length 45
    return "S25" + datacenter + dataseq + payment + linecode + payeeno + mspctlno +
        practitionerno + message + fillWhitespace45Chars;
  }
}
