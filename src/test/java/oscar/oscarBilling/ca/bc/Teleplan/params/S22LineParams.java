package oscar.oscarBilling.ca.bc.Teleplan.params;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class S22LineParams {
  private String datacenter;
  private String dataseq;
  private String payment;
  private String linecode;
  private String payeeno;
  private String mspctlno;
  private String practitionerno;
  private String practitionername;
  private String amtbilled;
  private String amtpaid;
}
