package oscar.oscarBilling.ca.bc.Teleplan.params;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class S21LineParams {
  private String datacenter;
  private String dataseq;
  private String payment;
  private String linecode;
  private String payeeno;
  private String mspctlno;
  private String payeename;
  private String amtbilled;
  private String amtpaid;
  private String balancefwd;
  private String cheque;
  private String newbalance;
}
