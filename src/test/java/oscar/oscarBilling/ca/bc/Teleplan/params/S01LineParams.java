package oscar.oscarBilling.ca.bc.Teleplan.params;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class S01LineParams {
  private String datacenter;
  private String dataseq;
  private String payment;
  private String linecode;
  private String payeeno;
  private String mspctlno;
  private String practitionerno;
  private String ajc1;
  private String aja1;
  private String ajc2;
  private String aja2;
  private String ajc3;
  private String aja3;
  private String ajc4;
  private String aja4;
  private String ajc5;
  private String aja5;
  private String ajc6;
  private String aja6;
  private String ajc7;
  private String aja7;
  private String officeno;
  private String paidamt;
  private String msprcddate;
  private String paidrate;
  private String icbcwcb;
  private String insurercode;
}
