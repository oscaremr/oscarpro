package oscar.oscarBilling.ca.bc.Teleplan;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.oscarehr.billing.CA.BC.dao.TeleplanC12Dao;
import org.oscarehr.billing.CA.BC.dao.TeleplanS00Dao;
import org.oscarehr.billing.CA.BC.dao.TeleplanS21Dao;
import org.oscarehr.billing.CA.BC.dao.TeleplanS22Dao;
import org.oscarehr.billing.CA.BC.dao.TeleplanS23Dao;
import org.oscarehr.billing.CA.BC.dao.TeleplanS25Dao;
import org.oscarehr.billing.CA.BC.dao.TeleplanVRCDao;
import org.oscarehr.billing.CA.BC.model.TeleplanC12;
import org.oscarehr.billing.CA.BC.model.TeleplanS00;
import org.oscarehr.billing.CA.BC.model.TeleplanS21;
import org.oscarehr.billing.CA.BC.model.TeleplanS22;
import org.oscarehr.billing.CA.BC.model.TeleplanS23;
import org.oscarehr.billing.CA.BC.model.TeleplanS25;
import org.oscarehr.billing.CA.BC.model.TeleplanVRC;
import oscar.oscarBilling.ca.bc.MSP.MSPReconcile;
import oscar.oscarBilling.ca.bc.Teleplan.params.C12LineParams;
import oscar.oscarBilling.ca.bc.Teleplan.params.S01LineParams;
import oscar.oscarBilling.ca.bc.Teleplan.params.S02LineParams;
import oscar.oscarBilling.ca.bc.Teleplan.params.S04LineParams;
import oscar.oscarBilling.ca.bc.Teleplan.params.S21LineParams;
import oscar.oscarBilling.ca.bc.Teleplan.params.S22LineParams;
import oscar.oscarBilling.ca.bc.Teleplan.params.S23LineParams;
import oscar.util.ConversionUtils;

/**
 * Test class for TeleplanService
 */
public class TeleplanServiceTest extends TeleplanServiceTestBase {
  public static final String FAKE_DATACENTER = "V9999";
  public static final String FAKE_SEQUENCE_NO = "0000001";
  public static final String FAKE_PAYEE_NO = "12555";
  public static final String FAKE_PAYEE_NAME = "OSCAR TEST MEDICAL CLINIC";
  public static final String FAKE_PRACTITIONER_NO = "66666";
  public static final String FAKE_PRACTITIONER_NAME = "DR. OSCAR TEST           ";
  public static final String FAKE_MSP_CTRL_NO = "4P9999";
  public static final String FAKE_OFFICE_FOLLIO_NO = "2222222";
  public static final String FAKE_OFFICE_NO = "4444444";
  public static final char FAKE_LINECODE = 'Z';

  public static final String FAKE_S21_BILL_AMT = "000254000";
  public static final String FAKE_S21_PAID_AMT = "000122500";
  public static final String FAKE_BALANCE_FORWARD = "000000000";
  public static final String FAKE_CHEQUE = "000127560";
  public static final String FAKE_NEW_BALANCE = "000000000";
  public static final String FAKE_PAYMENT_DATE = "20220101";
  public static final String FAKE_MSP_RCD_DATE = "20230621";
  public static final String FAKE_BILL_AMT = "0002500";
  public static final String FAKE_PAID_AMT = "0001500";
  public static final String FAKE_INSURER_CODE = "IC";
  public static final String FAKE_ICBC_WCB = "100000IP";

  public static final Integer S21_ID = 21;

  public static final String EXP1CODE = "E1";
  public static final String EXP2CODE = "E2";
  public static final String EXP3CODE = "E3";
  public static final String EXP4CODE = "E4";
  public static final String EXP5CODE = "E5";
  public static final String EXP6CODE = "E6";
  public static final String EXP7CODE = "E7";

  public static final String AJC1CODE = "A1";
  public static final String AJC2CODE = "A2";
  public static final String AJC3CODE = "A3";
  public static final String AJC4CODE = "A4";
  public static final String AJC5CODE = "A5";
  public static final String AJC6CODE = "A6";
  public static final String AJC7CODE = "A7";
  public static final String AJC1VALUE = "1111111";
  public static final String AJC2VALUE = "2222222";
  public static final String AJC3VALUE = "3333333";
  public static final String AJC4VALUE = "4444444";
  public static final String AJC5VALUE = "5555555";
  public static final String AJC6VALUE = "6666666";
  public static final String AJC7VALUE = "7777777";

  public static final String FAKE_PHN = "9151247483";
  public static final String FAKE_SURNAME = "TESTPATIENT       ";

  private TeleplanService teleplanService;
  private TeleplanVRCDao vrcDao;
  private TeleplanS21Dao s21Dao;
  private TeleplanS00Dao s00Dao;
  private TeleplanS23Dao s23Dao;
  private TeleplanS25Dao s25Dao;
  private TeleplanS22Dao s22Dao;
  private TeleplanC12Dao c12Dao;
  private TeleplanRemittanceParserData mockParserData;

  @Spy
  @InjectMocks
  private TeleplanService service;

  @Before
  public void setup() {
    MockitoAnnotations.initMocks(this);
    MSPReconcile mspMock = mock(MSPReconcile.class);
    vrcDao = mock(TeleplanVRCDao.class);
    s21Dao = mock(TeleplanS21Dao.class);
    s00Dao = mock(TeleplanS00Dao.class);
    s23Dao = mock(TeleplanS23Dao.class);
    s25Dao = mock(TeleplanS25Dao.class);
    s22Dao = mock(TeleplanS22Dao.class);
    c12Dao = mock(TeleplanC12Dao.class);

    teleplanService = new TeleplanService(s00Dao, s21Dao, s22Dao, s23Dao, s25Dao, c12Dao, vrcDao);
    when(mspMock.updateStat(anyString(), anyString())).thenReturn(true);
    mockParserData = mock(TeleplanRemittanceParserData.class);
    when(mockParserData.getMspReconcile()).thenReturn(mspMock);
    when(mockParserData.getFilename()).thenReturn("testfilename.txt");
  }

  @Test
  public void givenNewVrcRecord_whenReadVrc_thenCreateNewVrcRecord() {
    String vrcLine =
        buildVrcLine(
            FAKE_DATACENTER,
            FAKE_PAYMENT_DATE,
            "S",
            "0000123",
            "2024-01-01 00:00:000");
    mockForVrc(false);
    TeleplanVRC result = teleplanService.parseVrcEntry(vrcLine, mockParserData.getFilename());
    assertEquals(1, result.getId().intValue());
    verify(vrcDao, times(1)).persist(any(TeleplanVRC.class));
  }

  @Test
  public void givenExistingVrcRecord_whenReadVrc_thenReturnExistingRecord() {
    String vrcLine =
        buildVrcLine(
            FAKE_DATACENTER,
            FAKE_PAYMENT_DATE,
            "S",
            "0000123",
            "2024-01-01 00:00:000");
    mockForVrc(true);
    TeleplanVRC result = teleplanService.parseVrcEntry(vrcLine, mockParserData.getFilename());
    assertEquals(1, result.getId().intValue());
    verify(vrcDao, times(0)).persist(any(TeleplanVRC.class));
  }

  @Test
  public void givenNoVrcRecord_whenPreParseVrcLines_thenReturnEmptyList() throws IOException {
    BufferedReader mockBufferedReader = Mockito.mock(BufferedReader.class);
    when(mockBufferedReader.readLine()).thenReturn("C12", "C12", "C12", null);
    List<TeleplanVRC> result =
        teleplanService.preParseVrcEntries(mockBufferedReader, "testfilename" + ".txt");
    assertTrue(result.isEmpty());
  }

  @Test
  public void givenHasOneVrcRecord_whenPreParseVrcLines_thenReturnSizeOneVrcRecordList()
      throws IOException {
    BufferedReader mockBufferedReader = Mockito.mock(BufferedReader.class);
    when(mockBufferedReader.readLine())
        .thenReturn(
            buildVrcLine(
                FAKE_DATACENTER, FAKE_PAYMENT_DATE, "S", "0000123", "2024-01-01 00:00:000"),
            null);
    List<TeleplanVRC> result =
        teleplanService.preParseVrcEntries(mockBufferedReader, "testfilename" + ".txt");
    assertEquals(1, result.size());
  }

  @Test
  public void givenHasMultipleVrcRecords_whenPreParseVrcLines_thenReturnSizeMultipleVrcRecordList()
      throws IOException {
    BufferedReader mockBufferedReader = Mockito.mock(BufferedReader.class);
    when(mockBufferedReader.readLine())
        .thenReturn(
            buildVrcLine(
                FAKE_DATACENTER, FAKE_PAYMENT_DATE, "S", "0000123", "2024-01-01 00:00:000"),
            buildVrcLine(
                FAKE_DATACENTER, FAKE_PAYMENT_DATE, "S", "0000124", "2024-01-01 00:00:000"),
            buildVrcLine(
                FAKE_DATACENTER, FAKE_PAYMENT_DATE, "S", "0000125", "2024-01-01 00:00:000"),
            null);
    List<TeleplanVRC> result =
        teleplanService.preParseVrcEntries(mockBufferedReader, "testfilename.txt");
    assertEquals(3, result.size());
  }

  @Test
  public void givenC12RecordsAndNoVrcRecords_whenParseEntriesForRemittance_thenReturnNotNullResult() throws IOException {
    C12LineParams params =
        new C12LineParams(
            FAKE_DATACENTER,
            FAKE_SEQUENCE_NO,
            FAKE_PAYEE_NO,
            FAKE_PRACTITIONER_NO,
            EXP1CODE,
            EXP2CODE,
            EXP3CODE,
            EXP4CODE,
            EXP5CODE,
            EXP6CODE,
            EXP7CODE,
            FAKE_OFFICE_FOLLIO_NO);
    String c12Line = buildC12Line(params);

    List<TeleplanVRC> parsedVrcList = new ArrayList<>();
    BufferedReader input = new BufferedReader(new StringReader(c12Line));
    doReturn(mockParserData).when(service).createParserData("testfilename.txt");
    doReturn(mockParserData).when(service).parseFromC12Line(c12Line, mockParserData);
    TeleplanRemittanceParserData result =
        service.parseEntriesForRemittance(input, mockParserData.getFilename(),
            parsedVrcList);
    assertNotNull(result);
  }

  private void mockForVrc(boolean withVrcEntry) {
    Date date = ConversionUtils.fromDateString("2024-01-01 00:00:00", "yyyy-MM-dd HH:mm:ss");
    Timestamp ts = new Timestamp(date.getTime());
    if (withVrcEntry) {
      TeleplanVRC vrc = new TeleplanVRC();
      vrc.setId(1);
      vrc.setFilename(mockParserData.getFilename());
      vrc.setDataCentre("testDataCentre");
      vrc.setPaymentDate(date);
      vrc.setRecordGroup('S');
      vrc.setRecordCount(00042);
      vrc.setTimestamp(ts);
      when(vrcDao.findByFilenameDataCentreDateCount(
              anyString(), anyString(), any(Date.class), anyInt()))
          .thenReturn(vrc);
      doAnswer(new Answer() {
        @Override
        public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
          TeleplanVRC t = (TeleplanVRC) invocationOnMock.getArguments()[0];
          t.setId(2);
          return t;
        }
      })
      .when(vrcDao)
      .persist(any(TeleplanVRC.class));
    } else {
      when(vrcDao.findByFilenameDataCentreDateCount(
              anyString(), anyString(), any(Date.class), anyInt()))
          .thenReturn(null);
      doAnswer(new Answer() {
        @Override
        public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
          TeleplanVRC t = (TeleplanVRC) invocationOnMock.getArguments()[0];
          t.setId(1);
          return t;
        }
      })
      .when(vrcDao)
      .persist(any(TeleplanVRC.class));
    }
  }

  @Test
  public void givenS21Line_whenParseS21_thenReturnResult() {
    S21LineParams params = new S21LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PAYEE_NAME,
        FAKE_S21_BILL_AMT,
        FAKE_S21_PAID_AMT,
        FAKE_BALANCE_FORWARD,
        FAKE_CHEQUE,
        FAKE_NEW_BALANCE);
    String s21Line = buildS21Line(params);

    TeleplanS21 result = teleplanService.parseS21(s21Line, mockParserData);
    assertEquals(FAKE_DATACENTER, result.getDataCentre());
    assertEquals(FAKE_SEQUENCE_NO, result.getDataSeq());
    assertEquals(FAKE_PAYMENT_DATE, result.getPayment());
    assertEquals(FAKE_LINECODE, result.getLineCode().charValue());
    assertEquals(FAKE_PAYEE_NO, result.getPayeeNo());
    assertEquals(FAKE_MSP_CTRL_NO, result.getMspCtlNo());
    assertEquals(FAKE_S21_BILL_AMT, result.getAmountBilled());
    assertEquals(FAKE_S21_PAID_AMT, result.getAmountPaid());
    assertEquals(FAKE_PAYEE_NAME, result.getPayeeName());
    assertEquals(FAKE_BALANCE_FORWARD, result.getBalanceForward());
    assertEquals(FAKE_CHEQUE, result.getCheque());
    assertEquals(FAKE_NEW_BALANCE, result.getNewBalance());
    assertEquals('N', result.getStatus().charValue());
  }

  @Test
  public void givenS01Line_whenParseS00_thenReturnResult() {
    S01LineParams params = new S01LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        AJC1CODE,
        AJC1VALUE,
        AJC2CODE,
        AJC2VALUE,
        AJC3CODE,
        AJC3VALUE,
        AJC4CODE,
        AJC4VALUE,
        AJC5CODE,
        AJC5VALUE,
        AJC6CODE,
        AJC6VALUE,
        AJC7CODE,
        AJC7VALUE,
        FAKE_OFFICE_NO,
        FAKE_PAID_AMT,
        FAKE_MSP_RCD_DATE,
        "XX",
        "88888888",
        "ZZ");
    String s01Line = buildS01Line(params);
    when(mockParserData.getRaNo()).thenReturn(S21_ID.toString());

    TeleplanS00 result = teleplanService.parseS00(s01Line, mockParserData);
    assertEquals(FAKE_DATACENTER, result.getDataCentre());
    assertEquals(FAKE_SEQUENCE_NO, result.getDataSeq());
    assertEquals(FAKE_PAYMENT_DATE, result.getPayment());
    assertEquals(FAKE_LINECODE, result.getLineCode().charValue());
    assertEquals(FAKE_PAYEE_NO, result.getPayeeNo());
    assertEquals(FAKE_MSP_CTRL_NO, result.getMspCtlNo());
    assertEquals(FAKE_PRACTITIONER_NO, result.getPractitionerNo());
    assertEquals(AJC1CODE, result.getAjc1());
    assertEquals(AJC1VALUE, result.getAja1());
    assertEquals(AJC2CODE, result.getAjc2());
    assertEquals(AJC2VALUE, result.getAja2());
    assertEquals(AJC3CODE, result.getAjc3());
    assertEquals(AJC3VALUE, result.getAja3());
    assertEquals(AJC4CODE, result.getAjc4());
    assertEquals(AJC4VALUE, result.getAja4());
    assertEquals(AJC5CODE, result.getAjc5());
    assertEquals(AJC5VALUE, result.getAja5());
    assertEquals(AJC6CODE, result.getAjc6());
    assertEquals(AJC6VALUE, result.getAja6());
    assertEquals(AJC7CODE, result.getAjc7());
    assertEquals(AJC7VALUE, result.getAja7());
    assertEquals(FAKE_OFFICE_NO, result.getOfficeNo());
    assertEquals(FAKE_PAID_AMT, result.getPaidAmount());
    assertEquals(FAKE_MSP_RCD_DATE, result.getMspRcdDate());
    assertEquals("XX", result.getPaidRate());
    assertEquals("88888888", result.getIcBcWcb());
    assertEquals("ZZ", result.getInsurerCode());
    assertEquals("S01", result.getS00Type());
  }

  @Test
  public void givenS02Line_whenParseS02_thenReturnResult() {
    S02LineParams params = new S02LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        FAKE_MSP_RCD_DATE,
        "JJ",
        FAKE_SURNAME,
        FAKE_PHN,
        "00",
        "20230303",
        "00",
        "1  ",
        "BX",
        "53700",
        FAKE_BILL_AMT,
        "1  ",
        "PX",
        "53700",
        FAKE_PAID_AMT,
        FAKE_OFFICE_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
        AJC1CODE,
        AJC1VALUE,
        AJC2CODE,
        AJC2VALUE,
        AJC3CODE,
        AJC3VALUE,
        AJC4CODE,
        AJC4VALUE,
        AJC5CODE,
        AJC5VALUE,
        AJC6CODE,
        AJC6VALUE,
        AJC7CODE,
        AJC7VALUE,
        "0000000000",
        "A",
        "20220101",
        FAKE_INSURER_CODE,
        FAKE_ICBC_WCB);
    String s02Line = buildS02Line(params);
    when(mockParserData.getRaNo()).thenReturn(S21_ID.toString());

    TeleplanS00 result = teleplanService.parseS02(s02Line, mockParserData);
    assertEquals(FAKE_DATACENTER, result.getDataCentre());
    assertEquals(FAKE_SEQUENCE_NO, result.getDataSeq());
    assertEquals(FAKE_PAYMENT_DATE, result.getPayment());
    assertEquals(FAKE_LINECODE, result.getLineCode().charValue());
    assertEquals(FAKE_PAYEE_NO, result.getPayeeNo());
    assertEquals(FAKE_MSP_CTRL_NO, result.getMspCtlNo());
    assertEquals(FAKE_PRACTITIONER_NO, result.getPractitionerNo());
    assertEquals(FAKE_MSP_RCD_DATE, result.getMspRcdDate());
    assertEquals("JJ", result.getInitial());
    assertEquals(FAKE_SURNAME, result.getSurname());
    assertEquals(FAKE_PHN, result.getPhn());
    assertEquals("00", result.getPhnDepNo());
    assertEquals("20230303", result.getServiceDate());
    assertEquals("00", result.getToday());
    assertEquals("1  ", result.getBillNoServices());
    assertEquals("BX", result.getBillClafCode());
    assertEquals("53700", result.getBillFeeSchedule());
    assertEquals(FAKE_BILL_AMT, result.getBillAmount());
    assertEquals("1  ", result.getPaidNoServices());
    assertEquals("PX", result.getPaidClafCode());
    assertEquals("53700", result.getPaidFeeSchedule());
    assertEquals(FAKE_PAID_AMT, result.getPaidAmount());
    assertEquals(FAKE_OFFICE_NO, result.getOfficeNo());
    assertEquals(EXP1CODE, result.getExp1());
    assertEquals(EXP2CODE, result.getExp2());
    assertEquals(EXP3CODE, result.getExp3());
    assertEquals(EXP4CODE, result.getExp4());
    assertEquals(EXP5CODE, result.getExp5());
    assertEquals(EXP6CODE, result.getExp6());
    assertEquals(EXP7CODE, result.getExp7());
    assertEquals(AJC1CODE, result.getAjc1());
    assertEquals(AJC1VALUE, result.getAja1());
    assertEquals(AJC2CODE, result.getAjc2());
    assertEquals(AJC2VALUE, result.getAja2());
    assertEquals(AJC3CODE, result.getAjc3());
    assertEquals(AJC3VALUE, result.getAja3());
    assertEquals(AJC4CODE, result.getAjc4());
    assertEquals(AJC4VALUE, result.getAja4());
    assertEquals(AJC5CODE, result.getAjc5());
    assertEquals(AJC5VALUE, result.getAja5());
    assertEquals(AJC6CODE, result.getAjc6());
    assertEquals(AJC6VALUE, result.getAja6());
    assertEquals(AJC7CODE, result.getAjc7());
    assertEquals(AJC7VALUE, result.getAja7());
    assertEquals("0000000000", result.getPlanRefNo());
    assertEquals("A", result.getClaimSource());
    assertEquals("20220101", result.getPreviousPaidDate());
    assertEquals(FAKE_INSURER_CODE, result.getInsurerCode());
    assertEquals(FAKE_ICBC_WCB, result.getIcBcWcb());
    assertEquals("S02", result.getS00Type());
  }

  @Test
  public void givenS04Line_whenParseS04_thenReturnResult() {
    S04LineParams params = new S04LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        FAKE_MSP_RCD_DATE,
        FAKE_OFFICE_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
        FAKE_ICBC_WCB,
        FAKE_INSURER_CODE);
    String s04Line = buildS04Line(params);
    when(mockParserData.getRaNo()).thenReturn(S21_ID.toString());
    when(mockParserData.getRecFlag()).thenReturn(2);

    TeleplanS00 result = teleplanService.parseS04(s04Line, mockParserData);
    assertEquals(FAKE_DATACENTER, result.getDataCentre());
    assertEquals(FAKE_SEQUENCE_NO, result.getDataSeq());
    assertEquals(FAKE_PAYMENT_DATE, result.getPayment());
    assertEquals(FAKE_LINECODE, result.getLineCode().charValue());
    assertEquals(FAKE_PAYEE_NO, result.getPayeeNo());
    assertEquals(FAKE_MSP_CTRL_NO, result.getMspCtlNo());
    assertEquals(FAKE_PRACTITIONER_NO, result.getPractitionerNo());
    assertEquals(FAKE_MSP_RCD_DATE, result.getMspRcdDate());
    assertEquals(FAKE_OFFICE_NO, result.getOfficeNo());
    assertEquals(EXP1CODE, result.getExp1());
    assertEquals(EXP2CODE, result.getExp2());
    assertEquals(EXP3CODE, result.getExp3());
    assertEquals(EXP4CODE, result.getExp4());
    assertEquals(EXP5CODE, result.getExp5());
    assertEquals(EXP6CODE, result.getExp6());
    assertEquals(EXP7CODE, result.getExp7());
    assertEquals(FAKE_INSURER_CODE, result.getInsurerCode());
    assertEquals(FAKE_ICBC_WCB, result.getIcBcWcb());
    assertEquals("S04", result.getS00Type());
  }

  @Test
  public void givenS22Line_whenParseS22_thenReturnResult() {
    S22LineParams params = new S22LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        FAKE_PRACTITIONER_NAME,
        FAKE_S21_BILL_AMT,
        FAKE_S21_PAID_AMT);
    String s22Line = buildS22Line(params);
    when(mockParserData.getRaNo()).thenReturn(S21_ID.toString());

    TeleplanS22 s22 = teleplanService.parseS22(s22Line, mockParserData);
    assertEquals(FAKE_DATACENTER, s22.getDataCentre());
    assertEquals(FAKE_SEQUENCE_NO, s22.getDataSeq());
    assertEquals(FAKE_PAYMENT_DATE, s22.getPayment());
    assertEquals(FAKE_LINECODE, s22.getLineCode().charValue());
    assertEquals(FAKE_PAYEE_NO, s22.getPayeeNo());
    assertEquals(FAKE_MSP_CTRL_NO, s22.getMspCtlNo());
    assertEquals(FAKE_PRACTITIONER_NO, s22.getPractitionerNo());
    assertEquals(FAKE_PRACTITIONER_NAME, s22.getPractitionerName());
    assertEquals(FAKE_S21_BILL_AMT, s22.getAmountBilled());
    assertEquals(FAKE_S21_PAID_AMT, s22.getAmountPaid());
    assertEquals(S21_ID, s22.getS21Id());
  }

  @Test
  public void givenS23Line_whenParseS23_thenReturnResult() {
    S23LineParams params = new S23LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        AJC1CODE,
        "88888       ",
        "DR TEST JUNO USER   ",
        "G",
        "55555",
        "66666",
        "000000000",
        "000000000",
        "000000000",
        "000000000",
        "000000000",
        "000000000");
    String s23Line = buildS23Line(params);
    when(mockParserData.getRaNo()).thenReturn(S21_ID.toString());

    TeleplanS23 s23 = teleplanService.parseS23(s23Line, mockParserData);
    assertEquals("datacenter", FAKE_DATACENTER, s23.getDataCentre());
    assertEquals("sequence-no", FAKE_SEQUENCE_NO, s23.getDataSeq());
    assertEquals("payment", FAKE_PAYMENT_DATE, s23.getPayment());
    assertEquals("linecode", FAKE_LINECODE, s23.getLineCode().charValue());
    assertEquals("payee-no", FAKE_PAYEE_NO, s23.getPayeeNo());
    assertEquals("msp-ctl", FAKE_MSP_CTRL_NO, s23.getMspCtlNo());
    assertEquals("ajc", AJC1CODE, s23.getAjc());
    assertEquals("aji", "88888       ", s23.getAji());
    assertEquals("ajm", "DR TEST JUNO USER   ", s23.getAjm());
    assertEquals("calcmethod", "G", s23.getCalcMethod());
    assertEquals("rpercent", "55555", s23.getRPercent());
    assertEquals("opercent", "66666", s23.getOPercent());
    assertEquals("gamount", "000000000", s23.getGAmount());
    assertEquals("ramount", "000000000", s23.getRAmount());
    assertEquals("oamount", "000000000", s23.getOAmount());
    assertEquals("balancefwd", "000000000", s23.getBalanceForward());
    assertEquals("adjmade", "000000000", s23.getAdjMade());
    assertEquals("adjoutstanding", "000000000", s23.getAdjOutstanding());
    assertEquals("ra-no", S21_ID, s23.getS21Id());
  }

  @Test
  public void givenS25Line_whenParseS25_thenReturnResult() {
    String message =
        "TEST-TEST TEST-TEST TEST-TEST TEST-TEST TEST-TEST TEST-TEST TEST-TEST TEST-TEST!";
    String s25Line =
        buildS25Line(
            FAKE_DATACENTER,
            FAKE_SEQUENCE_NO,
            FAKE_PAYMENT_DATE,
            String.valueOf(FAKE_LINECODE),
            FAKE_PAYEE_NO,
            FAKE_MSP_CTRL_NO,
            FAKE_PRACTITIONER_NO,
            message);
    when(mockParserData.getRaNo()).thenReturn(S21_ID.toString());

    TeleplanS25 s25 = teleplanService.parseS25(s25Line, mockParserData);
    assertEquals("datacenter", FAKE_DATACENTER, s25.getDataCentre());
    assertEquals("sequence-no", FAKE_SEQUENCE_NO, s25.getDataSeq());
    assertEquals("payment", FAKE_PAYMENT_DATE, s25.getPayment());
    assertEquals("linecode", FAKE_LINECODE, s25.getLineCode().charValue());
    assertEquals("payee-no", FAKE_PAYEE_NO, s25.getPayeeNo());
    assertEquals("msp-ctl", FAKE_MSP_CTRL_NO, s25.getMspCtlNo());
    assertEquals("practitioner-no", FAKE_PRACTITIONER_NO, s25.getPractitionerNo());
    assertEquals("message", message, s25.getMessage());
    assertEquals("ra-no", S21_ID, s25.getS21Id());
  }

  @Test
  public void givenC12Line_whenParseC12_thenReturnResult() {
    C12LineParams params = new C12LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYEE_NO,
        FAKE_PRACTITIONER_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
        FAKE_OFFICE_FOLLIO_NO);
    String c12Line = buildC12Line(params);
    when(mockParserData.getRaNo()).thenReturn(S21_ID.toString());
    when(mockParserData.getRecFlag()).thenReturn(2);

    TeleplanC12 c12 = teleplanService.parseC12(c12Line, mockParserData);
    assertEquals(FAKE_DATACENTER, c12.getDataCentre());
    assertEquals(FAKE_SEQUENCE_NO, c12.getDataSeq());
    assertEquals(FAKE_PAYEE_NO, c12.getPayeeNo());
    assertEquals(FAKE_PRACTITIONER_NO, c12.getPractitionerNo());
    assertEquals(EXP1CODE, c12.getExp1());
    assertEquals(EXP2CODE, c12.getExp2());
    assertEquals(EXP3CODE, c12.getExp3());
    assertEquals(EXP4CODE, c12.getExp4());
    assertEquals(EXP5CODE, c12.getExp5());
    assertEquals(EXP6CODE, c12.getExp6());
    assertEquals(EXP7CODE, c12.getExp7());
    assertEquals(FAKE_OFFICE_FOLLIO_NO, c12.getOfficeFolioClaimNo());
    assertEquals(S21_ID, c12.getS21Id());
  }

  @Test
  public void givenS21LineWithoutDuplicate_whenParseFromS21Line_thenPersistS21() {
    S21LineParams params = new S21LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PAYEE_NAME,
        FAKE_S21_BILL_AMT,
        FAKE_S21_PAID_AMT,
        "000000000",
        "000127560",
        "000000000");
    String s21Line = buildS21Line(params);

    List<TeleplanS21> s21List = new ArrayList<>();
    TeleplanVRC currentVrc = new TeleplanVRC();
    currentVrc.setId(1);
    currentVrc.setFilename(mockParserData.getFilename());
    when(mockParserData.getRaNo()).thenReturn(S21_ID.toString());
    doCallRealMethod().when(mockParserData).setRecFlag(anyInt());
    when(s21Dao.findDuplicateS21Entries(any(TeleplanS21.class), anyInt())).thenReturn(s21List);
    doAnswer(new Answer() {
      @Override
      public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
        TeleplanS21 s21 = (TeleplanS21) invocationOnMock.getArguments()[0];
        s21.setId(1);
        return s21;
      }
    })
    .when(s21Dao)
    .persist(any(TeleplanS21.class));

    teleplanService.parseFromS21Line(currentVrc.getId(), s21Line, mockParserData);
    verify(s21Dao, times(0)).merge(any(TeleplanS21.class));
    verify(s21Dao, times(1)).persist(any(TeleplanS21.class));
  }

  @Test
  public void givenS21LineWithDuplicateNoVrc_whenParseFromS21Line_thenMergeS21() {
    S21LineParams params = new S21LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PAYEE_NAME,
        FAKE_S21_BILL_AMT,
        FAKE_S21_PAID_AMT,
        "000000000",
        "000127560",
        "000000000");
    String s21Line = buildS21Line(params);
    List<TeleplanS21> s21List = new ArrayList<>();
    TeleplanS21 s21 = new TeleplanS21();
    s21.setId(1);
    s21List.add(s21);
    TeleplanVRC currentVrc = new TeleplanVRC();
    currentVrc.setId(1);
    currentVrc.setFilename(mockParserData.getFilename());
    when(mockParserData.getRaNo()).thenReturn(S21_ID.toString());
    doCallRealMethod().when(mockParserData).setRecFlag(anyInt());
    when(s21Dao.findDuplicateS21Entries(any(TeleplanS21.class), anyInt())).thenReturn(s21List);
    doAnswer(new Answer() {
      @Override
      public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
        TeleplanS21 s21 = (TeleplanS21) invocationOnMock.getArguments()[0];
        s21.setVrcId(1);
        return s21;
      }
    })
    .when(s21Dao)
    .persist(any(TeleplanS21.class));

    teleplanService.parseFromS21Line(currentVrc.getId(), s21Line, mockParserData);
    verify(s21Dao, times(1)).merge(any(TeleplanS21.class));
    verify(s21Dao, times(0)).persist(any(TeleplanS21.class));
    assertEquals(1, s21.getVrcId().intValue());
  }

  @Test
  public void givenS21LineWithDuplicateWithVrc_whenParseFromS21Line_thenDoNothing() {
    S21LineParams params = new S21LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PAYEE_NAME,
        FAKE_S21_BILL_AMT,
        FAKE_S21_PAID_AMT,
        "000000000",
        "000127560",
        "000000000");
    String s21Line = buildS21Line(params);
    List<TeleplanS21> s21List = new ArrayList<>();
    TeleplanS21 s21 = new TeleplanS21();
    s21.setId(1);
    s21.setVrcId(1);
    s21List.add(s21);
    TeleplanVRC currentVrc = new TeleplanVRC();
    currentVrc.setId(1);
    currentVrc.setFilename(mockParserData.getFilename());
    when(mockParserData.getRaNo()).thenReturn(S21_ID.toString());
    doCallRealMethod().when(mockParserData).setRecFlag(anyInt());
    when(s21Dao.findDuplicateS21Entries(any(TeleplanS21.class), anyInt())).thenReturn(s21List);
    doAnswer(new Answer() {
      @Override
      public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
        TeleplanS21 s21 = (TeleplanS21) invocationOnMock.getArguments()[0];
        return s21;
      }
    })
    .when(s21Dao)
    .persist(any(TeleplanS21.class));

    teleplanService.parseFromS21Line(currentVrc.getId(), s21Line, mockParserData);
    verify(s21Dao, times(0)).merge(any(TeleplanS21.class));
    verify(s21Dao, times(0)).persist(any(TeleplanS21.class));
    assertEquals(1, s21.getVrcId().intValue());
  }

  @Test
  public void givenS01LineWithRecFlag1_whenParseFromS01Line_thenPersistS00() {
    S01LineParams params = new S01LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        AJC1CODE,
        AJC1VALUE,
        AJC2CODE,
        AJC2VALUE,
        AJC3CODE,
        AJC3VALUE,
        AJC4CODE,
        AJC4VALUE,
        AJC5CODE,
        AJC5VALUE,
        AJC6CODE,
        AJC6VALUE,
        AJC7CODE,
        AJC7VALUE,
        FAKE_OFFICE_NO,
        FAKE_PAID_AMT,
        FAKE_MSP_RCD_DATE,
        "XX",
        "88888888",
        "ZZ");
    String s01Line = buildS01Line(params);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(1);

    teleplanService.parseFromS01Line(s01Line, mockParserData);
    verify(s00Dao, times(1)).persist(any(TeleplanS00.class));
    verify(mockParserData, times(1)).updateMspReconcileStat(MSPReconcile.SETTLED, FAKE_OFFICE_NO);
  }

  @Test
  public void givenS01LineWithRecFlag0_whenParseFromS01Line_thenDoNothing() {
    S01LineParams params = new S01LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        AJC1CODE,
        AJC1VALUE,
        AJC2CODE,
        AJC2VALUE,
        AJC3CODE,
        AJC3VALUE,
        AJC4CODE,
        AJC4VALUE,
        AJC5CODE,
        AJC5VALUE,
        AJC6CODE,
        AJC6VALUE,
        AJC7CODE,
        AJC7VALUE,
        FAKE_OFFICE_NO,
        FAKE_PAID_AMT,
        FAKE_MSP_RCD_DATE,
        "XX",
        "88888888",
        "ZZ");
    String s01Line = buildS01Line(params);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(0);

    teleplanService.parseFromS01Line(s01Line, mockParserData);
    verify(s00Dao, times(0)).persist(any(TeleplanS00.class));
    verify(mockParserData, times(0)).updateMspReconcileStat(MSPReconcile.SETTLED, FAKE_OFFICE_NO);
  }

  @Test
  public void givenS02LineWithRecFlag1_whenParseFromS02Line_thenPersistS00() {
    S02LineParams params = new S02LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        FAKE_MSP_RCD_DATE,
        "JJ",
        FAKE_SURNAME,
        FAKE_PHN,
        "00",
        "20230303",
        "00",
        "1  ",
        "BX",
        "53700",
        FAKE_BILL_AMT,
        "1  ",
        "PX",
        "53700",
        FAKE_PAID_AMT,
        FAKE_OFFICE_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
        AJC1CODE,
        AJC1VALUE,
        AJC2CODE,
        AJC2VALUE,
        AJC3CODE,
        AJC3VALUE,
        AJC4CODE,
        AJC4VALUE,
        AJC5CODE,
        AJC5VALUE,
        AJC6CODE,
        AJC6VALUE,
        AJC7CODE,
        AJC7VALUE,
        "0000000000",
        "A",
        "20220101",
        FAKE_INSURER_CODE,
        FAKE_ICBC_WCB);
    String s02Line = buildS02Line(params);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(1);

    teleplanService.parseFromS02Line("S02", s02Line, mockParserData);
    verify(s00Dao, times(1)).persist(any(TeleplanS00.class));
    verify(mockParserData, times(1)).updateMspReconcileStat(MSPReconcile.PAIDWITHEXP, FAKE_OFFICE_NO);
  }

  @Test
  public void givenS02LineWithRecFlag0_whenParseFromS02Line_thenDoNothing() {
    S02LineParams params = new S02LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        FAKE_MSP_RCD_DATE,
        "JJ",
        FAKE_SURNAME,
        FAKE_PHN,
        "00",
        "20230303",
        "00",
        "1  ",
        "BX",
        "53700",
        FAKE_BILL_AMT,
        "1  ",
        "PX",
        "53700",
        FAKE_PAID_AMT,
        FAKE_OFFICE_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
        AJC1CODE,
        AJC1VALUE,
        AJC2CODE,
        AJC2VALUE,
        AJC3CODE,
        AJC3VALUE,
        AJC4CODE,
        AJC4VALUE,
        AJC5CODE,
        AJC5VALUE,
        AJC6CODE,
        AJC6VALUE,
        AJC7CODE,
        AJC7VALUE,
        "0000000000",
        "A",
        "20220101",
        FAKE_INSURER_CODE,
        FAKE_ICBC_WCB);
    String s02Line = buildS02Line(params);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(0);

    teleplanService.parseFromS02Line("S02", s02Line, mockParserData);
    verify(s00Dao, times(0)).persist(any(TeleplanS00.class));
    verify(mockParserData, times(0)).updateMspReconcileStat(MSPReconcile.PAIDWITHEXP, FAKE_OFFICE_NO);
  }

  @Test
  public void givenS03LineWithRecFlag1_whenParseFromS02Line_thenPersistS00() {
    S02LineParams params = new S02LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        FAKE_MSP_RCD_DATE,
        "JJ",
        FAKE_SURNAME,
        FAKE_PHN,
        "00",
        "20230303",
        "00",
        "1  ",
        "BX",
        "53700",
        FAKE_BILL_AMT,
        "1  ",
        "PX",
        "53700",
        FAKE_PAID_AMT,
        FAKE_OFFICE_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
        AJC1CODE,
        AJC1VALUE,
        AJC2CODE,
        AJC2VALUE,
        AJC3CODE,
        AJC3VALUE,
        AJC4CODE,
        AJC4VALUE,
        AJC5CODE,
        AJC5VALUE,
        AJC6CODE,
        AJC6VALUE,
        AJC7CODE,
        AJC7VALUE,
        "0000000000",
        "A",
        "20220101",
        FAKE_INSURER_CODE,
        FAKE_ICBC_WCB);
    String s03Line = buildS02Line(params);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(1);

    teleplanService.parseFromS02Line("S03", s03Line, mockParserData);
    verify(s00Dao, times(1)).persist(any(TeleplanS00.class));
    verify(mockParserData, times(1)).updateMspReconcileStat(MSPReconcile.REFUSED, FAKE_OFFICE_NO);
  }

  @Test
  public void givenS00LineWithRecFlag1_whenParseFromS02Line_thenPersistS00() {
    S02LineParams params = new S02LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        FAKE_MSP_RCD_DATE,
        "JJ",
        FAKE_SURNAME,
        FAKE_PHN,
        "00",
        "20230303",
        "00",
        "1  ",
        "BX",
        "53700",
        FAKE_BILL_AMT,
        "1  ",
        "PX",
        "53700",
        FAKE_PAID_AMT,
        FAKE_OFFICE_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
        AJC1CODE,
        AJC1VALUE,
        AJC2CODE,
        AJC2VALUE,
        AJC3CODE,
        AJC3VALUE,
        AJC4CODE,
        AJC4VALUE,
        AJC5CODE,
        AJC5VALUE,
        AJC6CODE,
        AJC6VALUE,
        AJC7CODE,
        AJC7VALUE,
        "0000000000",
        "A",
        "20220101",
        FAKE_INSURER_CODE,
        FAKE_ICBC_WCB);
    String s00Line = buildS02Line(params);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(1);

    teleplanService.parseFromS02Line("S00", s00Line, mockParserData);
    verify(s00Dao, times(1)).persist(any(TeleplanS00.class));
    verify(mockParserData, times(1)).updateMspReconcileStat(MSPReconcile.DATACENTERCHANGED, FAKE_OFFICE_NO);
  }

  @Test
  public void givenS04LineWithRecFlag1_whenParseFromS04Line_thenPersistS00() {
    S04LineParams params = new S04LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        FAKE_MSP_RCD_DATE,
        FAKE_OFFICE_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
        FAKE_ICBC_WCB,
        FAKE_INSURER_CODE
    );
    String s04Line = buildS04Line(params);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(1);

    teleplanService.parseFromS04Line(s04Line, mockParserData);
    verify(s00Dao, times(1)).persist(any(TeleplanS00.class));
    verify(mockParserData, times(1)).updateMspReconcileStat(MSPReconcile.HELD, FAKE_OFFICE_NO);
  }

  @Test
  public void givenS04LineWithRecFlag0_whenParseFromS04Line_thenDoNothing() {
    S04LineParams params = new S04LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        FAKE_MSP_RCD_DATE,
        FAKE_OFFICE_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
        FAKE_ICBC_WCB,
        FAKE_INSURER_CODE);
    String s04Line = buildS04Line(params);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(0);

    teleplanService.parseFromS04Line( s04Line, mockParserData);
    verify(s00Dao, times(0)).persist(any(TeleplanS00.class));
    verify(mockParserData, times(0)).updateMspReconcileStat(MSPReconcile.HELD, FAKE_OFFICE_NO);
  }

  @Test
  public void givenS22LineWithRecFlag1_whenParseFromS22Line_thenPersistS22() {
    S22LineParams params = new S22LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        FAKE_PRACTITIONER_NAME,
        FAKE_S21_BILL_AMT,
        FAKE_S21_PAID_AMT);
    String s22Line = buildS22Line(params);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(1);

    teleplanService.parseFromS22Line(s22Line, mockParserData);
    verify(s22Dao, times(1)).persist(any(TeleplanS22.class));
  }

  @Test
  public void givenS22LineWithRecFlag0_whenParseFromS22Line_thenDoNothing() {
    S22LineParams params = new S22LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        FAKE_PRACTITIONER_NO,
        FAKE_PRACTITIONER_NAME,
        FAKE_S21_BILL_AMT,
        FAKE_S21_PAID_AMT);
    String s22Line = buildS22Line(params);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(0);

    teleplanService.parseFromS22Line(s22Line, mockParserData);
    verify(s22Dao, times(0)).persist(any(TeleplanS22.class));
  }

  @Test
  public void givenS23LineWithRecFlag1_whenParseFromS23Line_thenPersistS23() {
    S23LineParams params = new S23LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        AJC1CODE,
        "88888       ",
        "DR TEST JUNO USER   ",
        "G",
        "55555",
        "66666",
        "000000000",
        "000000000",
        "000000000",
        "000000000",
        "000000000",
        "000000000");
    String s23Line = buildS23Line(params);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(1);

    teleplanService.parseFromS23Line(s23Line, mockParserData);
    verify(s23Dao, times(1)).persist(any(TeleplanS23.class));
  }

  @Test
  public void givenS23LineWithRecFlag0_whenParseFromS23Line_thenDoNothing() {
    S23LineParams params = new S23LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYMENT_DATE,
        String.valueOf(FAKE_LINECODE),
        FAKE_PAYEE_NO,
        FAKE_MSP_CTRL_NO,
        AJC1CODE,
        "88888       ",
        "DR TEST JUNO USER   ",
        "G",
        "55555",
        "66666",
        "000000000",
        "000000000",
        "000000000",
        "000000000",
        "000000000",
        "000000000");
    String s23Line = buildS23Line(params);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(0);

    teleplanService.parseFromS23Line(s23Line, mockParserData);
    verify(s23Dao, times(0)).persist(any(TeleplanS23.class));
  }

  @Test
  public void givenS25LineWithRecFlag1_whenParseFromS25Line_thenPersistS25() {
    String message =
        "TEST-TEST TEST-TEST TEST-TEST TEST-TEST TEST-TEST TEST-TEST TEST-TEST TEST-TEST!";
    String s25Line =
        buildS25Line(
            FAKE_DATACENTER,
            FAKE_SEQUENCE_NO,
            FAKE_PAYMENT_DATE,
            String.valueOf(FAKE_LINECODE),
            FAKE_PAYEE_NO,
            FAKE_MSP_CTRL_NO,
            FAKE_PRACTITIONER_NO,
            message);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(1);

    teleplanService.parseFromS25Line(s25Line, mockParserData);
    verify(s25Dao, times(1)).persist(any(TeleplanS25.class));
  }

  @Test
  public void givenS25LineWithRecFlag0_whenParseFromS25Line_thenDoNothing() {
    String message =
        "TEST-TEST TEST-TEST TEST-TEST TEST-TEST TEST-TEST TEST-TEST TEST-TEST TEST-TEST!";
    String s25Line =
        buildS25Line(
            FAKE_DATACENTER,
            FAKE_SEQUENCE_NO,
            FAKE_PAYMENT_DATE,
            String.valueOf(FAKE_LINECODE),
            FAKE_PAYEE_NO,
            FAKE_MSP_CTRL_NO,
            FAKE_PRACTITIONER_NO,
            message);
    when(mockParserData.getRaNo()).thenReturn(String.valueOf(1));
    when(mockParserData.getRecFlag()).thenReturn(0);

    teleplanService.parseFromS25Line(s25Line, mockParserData);
    verify(s25Dao, times(0)).persist(any(TeleplanS25.class));
  }

  @Test
  public void givenC12LineWithEmptyRaNoWithoutMatchingS21_whenParseFromC12Line_thenPersistS21AndC12() {
    C12LineParams params = new C12LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYEE_NO,
        FAKE_PRACTITIONER_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
    FAKE_OFFICE_FOLLIO_NO);
    String c12Line = buildC12Line(params);
    List<TeleplanS21> s21List = new ArrayList<>();
    when(s21Dao.findByFilenamePaymentPayeeNo(anyString(), anyString(), anyString()))
        .thenReturn(s21List);
    when(mockParserData.getRecFlag()).thenReturn(2);
    doCallRealMethod().when(mockParserData).getRaNo();
    doCallRealMethod().when(mockParserData).setRaNo(anyString());
    doAnswer(new Answer() {
      @Override
      public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
        TeleplanS21 s21 = (TeleplanS21) invocationOnMock.getArguments()[0];
        s21.setId(S21_ID);
        return s21;
      }
    })
    .when(s21Dao)
    .persist(any(TeleplanS21.class));
    mockParserData.setRaNo("");

    TeleplanRemittanceParserData parserData = teleplanService.parseFromC12Line(c12Line, mockParserData);
    verify(s21Dao, times(1)).persist(any(TeleplanS21.class));
    verify(c12Dao, times(1)).persist(any(TeleplanC12.class));
    verify(mockParserData, times(1))
            .updateMspReconcileStat(MSPReconcile.REJECTED, FAKE_OFFICE_FOLLIO_NO);
    assertEquals(S21_ID.toString(), parserData.getRaNo());
  }

  @Test
  public void givenC12LineWithEmptyRaNoWithMatchingS21_whenParseFromC12Line_thenPersistC12() {
    C12LineParams params = new C12LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYEE_NO,
        FAKE_PRACTITIONER_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
        FAKE_OFFICE_FOLLIO_NO);
    String c12Line = buildC12Line(params);
    List<TeleplanS21> s21List = new ArrayList<>();
    TeleplanS21 s21 = new TeleplanS21();
    s21.setId(S21_ID);
    s21List.add(s21);
    when(s21Dao.findByFilenamePaymentPayeeNo(anyString(), anyString(), anyString()))
            .thenReturn(s21List);
    when(mockParserData.getRecFlag()).thenReturn(2);
    doCallRealMethod().when(mockParserData).getRaNo();
    doCallRealMethod().when(mockParserData).setRaNo(anyString());
    mockParserData.setRaNo("");

    TeleplanRemittanceParserData parserData = teleplanService.parseFromC12Line(c12Line, mockParserData);
    verify(s21Dao, times(0)).persist(any(TeleplanS21.class));
    verify(c12Dao, times(1)).persist(any(TeleplanC12.class));
    verify(mockParserData, times(1))
            .updateMspReconcileStat(MSPReconcile.REJECTED, FAKE_OFFICE_FOLLIO_NO);
    assertEquals(S21_ID.toString(), parserData.getRaNo());
  }

  @Test
  public void givenC12LineWithEmptyRaNoWithMatchingS21RecFlag0_whenParseFromC12Line_thenPersistS21AndC12() {
    C12LineParams params = new C12LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYEE_NO,
        FAKE_PRACTITIONER_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
        FAKE_OFFICE_FOLLIO_NO);
    String c12Line = buildC12Line(params);
    List<TeleplanS21> s21List = new ArrayList<>();
    when(s21Dao.findByFilenamePaymentPayeeNo(anyString(), anyString(), anyString()))
            .thenReturn(s21List);
    doCallRealMethod().when(mockParserData).getRecFlag();
    doCallRealMethod().when(mockParserData).setRecFlag(anyInt());
    doCallRealMethod().when(mockParserData).getRaNo();
    doCallRealMethod().when(mockParserData).setRaNo(anyString());
    mockParserData.setRaNo("");
    mockParserData.setRecFlag(0);
    doAnswer(new Answer() {
      @Override
      public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
        TeleplanS21 s21 = (TeleplanS21) invocationOnMock.getArguments()[0];
        s21.setId(S21_ID);
        return s21;
      }
    })
    .when(s21Dao)
    .persist(any(TeleplanS21.class));

    TeleplanRemittanceParserData parserData = teleplanService.parseFromC12Line(c12Line, mockParserData);
    verify(s21Dao, times(1)).persist(any(TeleplanS21.class));
    verify(c12Dao, times(1)).persist(any(TeleplanC12.class));
    verify(mockParserData, times(1))
            .updateMspReconcileStat(MSPReconcile.REJECTED, FAKE_OFFICE_FOLLIO_NO);
    assertEquals(S21_ID.toString(), parserData.getRaNo());
  }

  @Test
  public void givenC12LineWithRaNoWithMatchingS21RecFlag0_whenParseFromC12Line_thenDoNothing() {
    C12LineParams params = new C12LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYEE_NO,
        FAKE_PRACTITIONER_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
        FAKE_OFFICE_FOLLIO_NO);
    String c12Line = buildC12Line(params);
    List<TeleplanS21> s21List = new ArrayList<>();
    when(s21Dao.findByFilenamePaymentPayeeNo(anyString(), anyString(), anyString()))
            .thenReturn(s21List);
    doCallRealMethod().when(mockParserData).getRecFlag();
    doCallRealMethod().when(mockParserData).setRecFlag(anyInt());
    doCallRealMethod().when(mockParserData).getRaNo();
    doCallRealMethod().when(mockParserData).setRaNo(anyString());
    mockParserData.setRaNo("1");
    mockParserData.setRecFlag(0);
    doAnswer(new Answer() {
      @Override
      public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
        TeleplanS21 s21 = (TeleplanS21) invocationOnMock.getArguments()[0];
        s21.setId(S21_ID);
        return s21;
      }
    })
    .when(s21Dao)
    .persist(any(TeleplanS21.class));

    teleplanService.parseFromC12Line(c12Line, mockParserData);
    verify(s21Dao, times(0)).persist(any(TeleplanS21.class));
    verify(c12Dao, times(0)).persist(any(TeleplanC12.class));
    verify(mockParserData, times(0))
            .updateMspReconcileStat(MSPReconcile.REJECTED, FAKE_OFFICE_FOLLIO_NO);
  }

  @Test
  public void givenC12LineWithRaNoWithMatchingS21RecFlag1_whenParseFromC12Line_thenPersistC12() {
    C12LineParams params = new C12LineParams(
        FAKE_DATACENTER,
        FAKE_SEQUENCE_NO,
        FAKE_PAYEE_NO,
        FAKE_PRACTITIONER_NO,
        EXP1CODE,
        EXP2CODE,
        EXP3CODE,
        EXP4CODE,
        EXP5CODE,
        EXP6CODE,
        EXP7CODE,
        FAKE_OFFICE_FOLLIO_NO);
    String c12Line = buildC12Line(params);
    List<TeleplanS21> s21List = new ArrayList<>();
    when(s21Dao.findByFilenamePaymentPayeeNo(anyString(), anyString(), anyString()))
            .thenReturn(s21List);
    doCallRealMethod().when(mockParserData).getRecFlag();
    doCallRealMethod().when(mockParserData).setRecFlag(anyInt());
    doCallRealMethod().when(mockParserData).getRaNo();
    doCallRealMethod().when(mockParserData).setRaNo(anyString());
    mockParserData.setRaNo("1");
    mockParserData.setRecFlag(1);
    doAnswer(new Answer() {
      @Override
      public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
        TeleplanS21 s21 = (TeleplanS21) invocationOnMock.getArguments()[0];
        s21.setId(S21_ID);
        return s21;
      }
    })
    .when(s21Dao)
    .persist(any(TeleplanS21.class));

    TeleplanRemittanceParserData parserData = teleplanService.parseFromC12Line(c12Line, mockParserData);
    verify(s21Dao, times(0)).persist(any(TeleplanS21.class));
    verify(c12Dao, times(1)).persist(any(TeleplanC12.class));
    verify(mockParserData, times(1))
            .updateMspReconcileStat(MSPReconcile.REJECTED, FAKE_OFFICE_FOLLIO_NO);
  }
}