package oscar.oscarBilling.ca.bc.Teleplan.params;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class S02LineParams {
  private String datacenter;
  private String dataseq;
  private String payment;
  private String linecode;
  private String payeeno;
  private String mspctlno;
  private String practitionerno;
  private String msprcddate;
  private String initial;
  private String surname;
  private String phn;
  private String phndepno;
  private String servicedate;
  private String today;
  private String billnoservices;
  private String billclafcode;
  private String billfeeschedule;
  private String billamt;
  private String paidnoservices;
  private String paidclafcode;
  private String paidfeeschedule;
  private String paidamt;
  private String officeno;
  private String exp1;
  private String exp2;
  private String exp3;
  private String exp4;
  private String exp5;
  private String exp6;
  private String exp7;
  private String ajc1;
  private String aja1;
  private String ajc2;
  private String aja2;
  private String ajc3;
  private String aja3;
  private String ajc4;
  private String aja4;
  private String ajc5;
  private String aja5;
  private String ajc6;
  private String aja6;
  private String ajc7;
  private String aja7;
  private String planrefno;
  private String claimsource;
  private String previouspaiddate;
  private String insurercode;
  private String icbcwcb;
}
