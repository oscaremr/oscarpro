/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.oscarBilling.ca.on.data;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import lombok.val;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.ProfessionalSpecialistDao;
import org.oscarehr.common.model.ProfessionalSpecialist;
import org.oscarehr.util.SpringUtils;

@ExtendWith(MockitoExtension.class)
public class JdbcBillingPageUtilTest {
  @Mock
  private ProfessionalSpecialistDao professionalSpecialistDao;

  private MockedStatic<SpringUtils> springUtils;

  @BeforeEach
  public void init() {
    springUtils = mockStatic(SpringUtils.class);
    springUtils
        .when(() -> SpringUtils.getBean(eq("professionalSpecialistDao")))
        .thenReturn(professionalSpecialistDao);
  }

  @AfterEach
  public void after() {
    springUtils.close();
  }

  @Test
  public void givenNullSpecialist_whenGetReferralDocSpecialityType_thenReturnEmptyString() {
    when(professionalSpecialistDao.getByReferralNo("1")).thenReturn(null);
    val jdbcBillingPageUtil = spy(new JdbcBillingPageUtil());
    val result = jdbcBillingPageUtil.getReferralDoctorSpecialityType("1");
    Assertions.assertEquals("", result);
  }

  @Test
  public void givenNullSpecialityType_whenGetReferralDocSpecialityType_thenReturnEmptyString() {
    val specialist = createSpecialistWithType(null);
    when(professionalSpecialistDao.getByReferralNo("1")).thenReturn(specialist);
    val jdbcBillingPageUtil = spy(new JdbcBillingPageUtil());
    val result = jdbcBillingPageUtil.getReferralDoctorSpecialityType("1");
    Assertions.assertEquals("", result);
  }

  @Test
  public void givenEmptySpecialityType_whenGetReferralDocSpecialityType_thenReturnEmptyString() {
    val specialist = createSpecialistWithType("");
    when(professionalSpecialistDao.getByReferralNo("1")).thenReturn(specialist);
    val jdbcBillingPageUtil = spy(new JdbcBillingPageUtil());
    val result = jdbcBillingPageUtil.getReferralDoctorSpecialityType("1");
    Assertions.assertEquals("", result);
  }

  @Test
  public void givenSpecialityType_whenGetReferralDocSpecialityType_thenReturnSpecialistType() {
    val specialist = createSpecialistWithType("specialist");
    when(professionalSpecialistDao.getByReferralNo("1")).thenReturn(specialist);
    val jdbcBillingPageUtil = spy(new JdbcBillingPageUtil());
    val result = jdbcBillingPageUtil.getReferralDoctorSpecialityType("1");
    Assertions.assertEquals("specialist", result);
  }

  private ProfessionalSpecialist createSpecialistWithType(String specialityType) {
    val specialist = new ProfessionalSpecialist();
    specialist.setSpecialtyType(specialityType);
    return specialist;
  }
}
