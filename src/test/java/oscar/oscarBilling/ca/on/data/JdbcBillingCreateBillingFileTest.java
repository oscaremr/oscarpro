package oscar.oscarBilling.ca.on.data;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.oscarehr.billing.CA.ON.dao.BillingONDiskNameDao;
import org.oscarehr.billing.CA.ON.dao.BillingONFilenameDao;
import org.oscarehr.billing.CA.ON.dao.BillingONHeaderDao;
import org.oscarehr.common.dao.BillingONCHeader1Dao;
import org.oscarehr.common.dao.BillingONItemDao;
import org.oscarehr.common.dao.InvoiceHistoryDao;
import org.oscarehr.common.dao.SiteDao;
import org.oscarehr.common.model.BillingONCHeader1;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.managers.DemographicManager;
import org.oscarehr.util.DateRange;
import org.oscarehr.util.SpringUtils;
import org.springframework.test.context.ActiveProfiles;

@ExtendWith(value = MockitoExtension.class)
@ActiveProfiles(value = "test")
@MockitoSettings(strictness = Strictness.LENIENT)
public class JdbcBillingCreateBillingFileTest {

  private JdbcBillingCreateBillingFile jdbcBillingCreateBillingFile;

  @Mock
  private DemographicManager demographicManager;
  @Mock
  private BillingONCHeader1Dao cheaderDao;
  @Mock
  private BillingONHeaderDao headerDao;
  @Mock
  private BillingONFilenameDao filenameDao;
  @Mock
  private InvoiceHistoryDao historyDao;
  @Mock
  private SiteDao siteDao;
  @Mock
  private BillingONDiskNameDao billingONDiskNameDao;
  @Mock
  private BillingONItemDao billingONItemDao;

  private MockedStatic<SpringUtils> springUtilsMock;

  private static final String PROVIDER_ID="999";
  private static final String PROVIDER_OHIP="666";
  private static final String SPEC_CODE="22";
  private static final String BILLING_GROUP="4";
  private static final Integer DEMOGRAPHIC_ID=1;


  @BeforeEach
  void setUp() {
    springUtilsMock = mockStatic(SpringUtils.class);
    springUtilsMock.when(() -> SpringUtils.getBean(eq(DemographicManager.class)))
        .thenReturn(demographicManager);
    springUtilsMock.when(() -> SpringUtils.getBean(eq(BillingONCHeader1Dao.class)))
        .thenReturn(cheaderDao);
    springUtilsMock.when(() -> SpringUtils.getBean(eq(BillingONHeaderDao.class)))
        .thenReturn(headerDao);
    springUtilsMock.when(() -> SpringUtils.getBean(eq(BillingONFilenameDao.class)))
        .thenReturn(filenameDao);
    springUtilsMock.when(() -> SpringUtils.getBean(eq(InvoiceHistoryDao.class)))
        .thenReturn(historyDao);
    springUtilsMock.when(() -> SpringUtils.getBean(eq(SiteDao.class)))
        .thenReturn(siteDao);
    springUtilsMock.when(() -> SpringUtils.getBean(eq(BillingONDiskNameDao.class)))
        .thenReturn(billingONDiskNameDao);
    springUtilsMock.when(() -> SpringUtils.getBean(eq(BillingONItemDao.class)))
        .thenReturn(billingONItemDao);

    jdbcBillingCreateBillingFile = new JdbcBillingCreateBillingFile();
    jdbcBillingCreateBillingFile.setEFlag("0");
    jdbcBillingCreateBillingFile.setDateRange(new DateRange(null, new Date()));
    jdbcBillingCreateBillingFile.setProviderNo(PROVIDER_ID);
    BillingBatchHeaderData billingBatchHeaderData = new BillingBatchHeaderData();
    billingBatchHeaderData.setSpec_id("   ");
    billingBatchHeaderData.setMoh_office(" ");
    billingBatchHeaderData.setGroup_num(BILLING_GROUP);
    billingBatchHeaderData.setProvider_reg_num(PROVIDER_OHIP);
    billingBatchHeaderData.setSpecialty(SPEC_CODE);
    jdbcBillingCreateBillingFile.setBatchHeaderObj(billingBatchHeaderData);

    Demographic mockDemo = mockDemographic();
    when(demographicManager.getDemographic(any(), anyString())).thenReturn(mockDemo);
  }

  @AfterEach
  void tearDown() {
    springUtilsMock.close();
  }

  /**
   * need to ensure that exceptions are thrown and not suppressed,
   * as other tests in this class rely on thrown exception checks
   */
  @Test
  public void givenDaoException_whenCreateBillingFileStr_thenExceptionsNotSuppressed() {
    // mock a method to fail
    when(cheaderDao.findByProviderStatusAndDateRangeAndDemographic(
        anyString(),
        anyList(),
        any(DateRange.class),
        anyInt(),
        anyBoolean()))
        .thenThrow(new RuntimeException());

    // assert that the failure is presented as an exception
    Assertions.assertThrows(RuntimeException.class, () ->
        jdbcBillingCreateBillingFile.createBillingFileStr(
            null,
            "0",
            new String[]{"O", "W", "I"},
            true,
            null,
            false,
            false,
            DEMOGRAPHIC_ID,
            false));

    // assert that our method that fails is actually called
    verify(cheaderDao, times(1))
        .findByProviderStatusAndDateRangeAndDemographic(
            anyString(),
            anyList(),
            any(DateRange.class),
            anyInt(),
            anyBoolean());

  }

  /**
   * Test that an empty run will work
   */
  @Test
  public void givenZeroBillingONCHeaderRecords_whenCreateBillingFileStr_thenNoErrors() {
    Assertions.assertDoesNotThrow(() ->
        jdbcBillingCreateBillingFile.createBillingFileStr(
            null,
            "0",
            new String[]{"O", "W", "I"},
            true,
            null,
            false,
            false,
            DEMOGRAPHIC_ID,
            false)
    );
  }

  /**
   * Test that a basic run will work
   */
  @Test
  public void givenSimpleBillingONCHeaderRecord_whenCreateBillingFileStr_thenNoErrors()
      throws ParseException {

    BillingONCHeader1 mockHeader = mockDefaultBillingONCHeader1();

    List<BillingONCHeader1> cHeaderResults = new ArrayList<>();
    cHeaderResults.add(mockHeader);

    when(cheaderDao.findByProviderStatusAndDateRangeAndDemographic(
        eq(PROVIDER_ID),
        anyList(),
        any(DateRange.class),
        eq(DEMOGRAPHIC_ID),
        anyBoolean()))
        .thenReturn(cHeaderResults);

    Assertions.assertDoesNotThrow(() ->
        jdbcBillingCreateBillingFile.createBillingFileStr(
            null,
            "0",
            new String[]{"O", "W", "I"},
            true,
            null,
            false,
            false,
            DEMOGRAPHIC_ID,
            false)
    );
  }

  /**
   * Test that a null referral number will not cause exceptions
   */
  @Test
  public void givenNullReferralNo_whenCreateBillingFileStr_thenNoErrors()
      throws ParseException {

    BillingONCHeader1 mockHeader = mockDefaultBillingONCHeader1();
    when(mockHeader.getRefNum()).thenReturn(null);

    List<BillingONCHeader1> cHeaderResults = new ArrayList<>();
    cHeaderResults.add(mockHeader);

    when(cheaderDao.findByProviderStatusAndDateRangeAndDemographic(
        eq(PROVIDER_ID),
        anyList(),
        any(DateRange.class),
        eq(DEMOGRAPHIC_ID),
        anyBoolean()))
        .thenReturn(cHeaderResults);

    Assertions.assertDoesNotThrow(() ->
        jdbcBillingCreateBillingFile.createBillingFileStr(
            null,
            "0",
            new String[]{"O", "W", "I"},
            true,
            null,
            false,
            false,
            DEMOGRAPHIC_ID,
            false)
    );
  }

  private BillingONCHeader1 mockDefaultBillingONCHeader1() throws ParseException {
    BillingONCHeader1 mockHeader = mock(BillingONCHeader1.class);
    when(mockHeader.getId()).thenReturn(1);
    when(mockHeader.getTranscId()).thenReturn("HE");
    when(mockHeader.getRecId()).thenReturn("H");
    when(mockHeader.getPayProgram()).thenReturn("HCP");
    when(mockHeader.getPayee()).thenReturn("P");
    when(mockHeader.getFaciltyNum()).thenReturn("0000");
    when(mockHeader.getAdmissionDate()).thenReturn(new Date(1000));
    when(mockHeader.getRefLabNum()).thenReturn("");
    when(mockHeader.getManReview()).thenReturn("");
    when(mockHeader.getProvince()).thenReturn("ON");
    when(mockHeader.getBillingDate()).thenReturn(new Date(1000));
    when(mockHeader.getBillingTime()).thenReturn(new Date(1000));
    when(mockHeader.getTotal()).thenReturn(new BigDecimal("0.00"));
    when(mockHeader.getPaid()).thenReturn(new BigDecimal("0.00"));
    when(mockHeader.getStatus()).thenReturn("O");
    when(mockHeader.getComment()).thenReturn("");
    when(mockHeader.getVisitType()).thenReturn("00");
    when(mockHeader.getClinic()).thenReturn(null);

    // mock provider info
    when(mockHeader.getProviderOhipNo()).thenReturn(PROVIDER_OHIP);
    when(mockHeader.getProviderRmaNo()).thenReturn("");
    when(mockHeader.getCreator()).thenReturn(PROVIDER_ID);
    when(mockHeader.getRefNum()).thenReturn("");

    // mock appointment info
    when(mockHeader.getAppointmentNo()).thenReturn(1);
    when(mockHeader.getLocation()).thenReturn("0000");
    when(mockHeader.getApptProviderNo()).thenReturn(PROVIDER_ID);
    when(mockHeader.getAsstProviderNo()).thenReturn("");

    // mock demographic info
    when(mockHeader.getDemographicNo()).thenReturn(DEMOGRAPHIC_ID);
    when(mockHeader.getDemographicName()).thenReturn("lname,fname");
    when(mockHeader.getHin()).thenReturn("");
    when(mockHeader.getVer()).thenReturn("");
    when(mockHeader.getDob()).thenReturn("20001001");
    when(mockHeader.getSex()).thenReturn("2");

    return mockHeader;
  }

  private Demographic mockDemographic()
  {
    Demographic demographic = mock(Demographic.class);
    when(demographic.getFirstName()).thenReturn("fname");
    when(demographic.getLastName()).thenReturn("lname");
    when(demographic.getHin()).thenReturn("");
    when(demographic.getVer()).thenReturn("");
    when(demographic.getHcType()).thenReturn("ON");
    when(demographic.getYearOfBirth()).thenReturn("2000");
    when(demographic.getMonthOfBirth()).thenReturn("10");
    when(demographic.getDateOfBirth()).thenReturn("01");
    when(demographic.getSex()).thenReturn("F");
    when(demographic.getProviderNo()).thenReturn(null);

    return demographic;
  }
}