/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.oscarBilling.ca.on.pageUtil;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.BillingONCHeader1Dao;
import org.oscarehr.common.dao.BillingONExtDao;
import org.oscarehr.common.dao.BillingONItemDao;
import org.oscarehr.common.dao.BillingONPaymentDao;
import org.oscarehr.common.dao.BillingONRepoDao;
import org.oscarehr.common.dao.DemographicDao;
import org.oscarehr.common.dao.InvoiceHistoryDao;
import org.oscarehr.common.model.BillingONCHeader1;
import org.oscarehr.common.model.BillingONExt;
import org.oscarehr.common.model.BillingONItem;
import org.oscarehr.common.model.Facility;
import org.oscarehr.common.model.Provider;
import org.oscarehr.common.service.BillingONService;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;
import oscar.util.DateUtils;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class BillingCorrectionActionTest {

  @Mock
  private BillingONCHeader1Dao billingONCHeader1Dao;

  @Mock
  private BillingONService billingONService;

  @Mock
  private BillingONPaymentDao billingONPaymentDao;

  @Mock
  private DemographicDao demographicDao;

  @Mock
  private InvoiceHistoryDao invoiceHistoryDao;

  @Mock
  private BillingONItemDao billingONItemDao;

  @Mock
  private BillingONExtDao billingONExtDao;

  @Mock
  private HttpServletRequest request;

  @Mock
  private HttpServletResponse response;

  @Mock
  private OscarProperties properties;

  @Mock
  private ActionMapping mapping;

  @Mock
  private BillingONRepoDao billingONRepoDao;

  @Mock
  private ProviderDao providerDao;

  @Mock
  private Provider mockProvider;

  @Mock
  private ActionForm form;

  private BillingONCHeader1 billingONCHeader1;

  private Provider provider;

  private Locale locale;

  private MockedStatic<SpringUtils> springUtils;
  private MockedStatic<LoggedInInfo> loggedInInfo;
  private MockedStatic<OscarProperties> oscarProperties;

  @BeforeEach
  public void before() throws Exception {
    billingONCHeader1 = setupBillingONCHeader1();
    provider = setupProvider();
    val forward = new ActionForward();
    locale = new Locale("en-US");

    mockObjects();

    when(request.getParameter("xml_billing_no")).thenReturn("1");
    when(request.getParameter("submit")).thenReturn("Save");
    when(request.getParameter("status")).thenReturn("O");
    when(request.getParameter("payProgram")).thenReturn("HCP");
    when(request.getParameter("rdohip")).thenReturn("");
    when(request.getParameter("visittype")).thenReturn("00");
    when(request.getParameter("xml_vdate")).thenReturn("");
    when(request.getParameter("manualReview")).thenReturn(null);
    when(request.getParameter("comment")).thenReturn("");
    when(request.getParameter("provider_no")).thenReturn("1");
    when(request.getParameter("xml_slicode")).thenReturn("0000");
    when(request.getParameter("site")).thenReturn(null);
    when(request.getParameter("hc_type")).thenReturn("ON");
    when(request.getLocale()).thenReturn(locale);
    when(billingONCHeader1Dao.find(anyInt())).thenReturn(billingONCHeader1);
    doNothing().when(billingONRepoDao).createBillingONCHeader1Entry(billingONCHeader1, locale);
    when(request.getParameter("m_review")).thenReturn(null);
    when(providerDao.getProvider("1")).thenReturn(provider);
    when(request.getParameter("oldStatus")).thenReturn("");
    when(request.getParameter("clinic_ref_code")).thenReturn("1972");
    when(request.getParameter("xml_appointment_date")).thenReturn("2023-09-08");
    when(mapping.findForward(anyString())).thenReturn(forward);
  }

  @AfterEach
  public void after() {
    springUtils.close();
    loggedInInfo.close();
    oscarProperties.close();
  }

  @Test
  public void testUpdateInvoiceChangeBillingDateAndFacilityNum_Success() {
    when(request.getParameter("xml_appointment_date")).thenReturn("2023-10-12");
    when(request.getParameter("clinic_ref_code")).thenReturn("0000");

    BillingCorrectionAction billingCorrectionAction = spy(new BillingCorrectionAction());
    ActionForward result = billingCorrectionAction.updateInvoice(mapping, form, request, response);

    val billingDateString = DateUtils.formatDate(billingONCHeader1.getBillingDate(), locale);

    assertEquals("2023-10-12", billingDateString);
    assertEquals("0000", billingONCHeader1.getFaciltyNum());
    verify(billingONCHeader1Dao, times(1)).find(1);
    verify(billingONCHeader1Dao, times(2)).merge(billingONCHeader1);
    verify(mapping, times(1)).findForward(eq("submitClose"));

  }

  @Test
  public void testChangeBillingDateInvalid_Fail() {
    val billingDateString = DateUtils.formatDate(billingONCHeader1.getBillingDate(), locale);

    when(request.getParameter("xml_appointment_date")).thenReturn("test");

    assertEquals("2023-09-08", billingDateString);

    BillingCorrectionAction billingCorrectionAction = spy(new BillingCorrectionAction());
    ActionForward result = billingCorrectionAction.updateInvoice(mapping, form, request, response);

    verify(mapping, times(1)).findForward(eq("failure"));
    assertEquals("2023-09-08", billingDateString);
  }


  @Test
  public void testInvalidBillingNumberInRequest_Fail() {
    when(request.getParameter("xml_billing_no")).thenReturn("fail");

    BillingCorrectionAction billingCorrectionAction = spy(new BillingCorrectionAction());
    ActionForward result = billingCorrectionAction.updateInvoice(mapping, form, request, response);

    verify(mapping, times(1)).findForward(eq("closeReload"));
  }

  @Test
  public void testChangeBillToNoExistingBillTo_Success() {
    when(request.getParameter("billTo")).thenReturn("test, demo");

    BillingCorrectionAction billingCorrectionAction = spy(new BillingCorrectionAction());
    ActionForward result = billingCorrectionAction.updateInvoice(mapping, form, request, response);

    verify(billingONExtDao, times(1)).persist(any(BillingONExt.class));
  }

  @Test
  public void testChangeBillToExistingBillTo_Success() {
    val billingONExt = new BillingONExt();
    billingONExt.setBillingNo(billingONCHeader1.getId());
    billingONExt.setKeyVal("billTo");
    billingONExt.setValue("old, demo");
    when(request.getParameter("billTo")).thenReturn("test, demo");
    when(billingONExtDao.getBillTo(billingONCHeader1)).thenReturn(billingONExt);

    BillingCorrectionAction billingCorrectionAction = spy(new BillingCorrectionAction());
    ActionForward result = billingCorrectionAction.updateInvoice(mapping, form, request, response);

    verify(billingONExtDao, times(1)).merge(any(BillingONExt.class));
    assertEquals("test, demo", billingONExt.getValue());
  }

  @Test
  public void testChangeInvoiceDueDateNoExistingInvoiceDueDate_Success() {
    val billingONExt = new BillingONExt();
    billingONExt.setBillingNo(billingONCHeader1.getId());
    billingONExt.setKeyVal("dueDate");
    billingONExt.setValue("2023-10-31");

    when(billingONExtDao.getDueDate(billingONCHeader1)).thenReturn(billingONExt);
    when(request.getParameter("invoiceDueDate")).thenReturn("2023-11-10");

    BillingCorrectionAction billingCorrectionAction = spy(new BillingCorrectionAction());
    ActionForward result = billingCorrectionAction.updateInvoice(mapping, form, request, response);

    verify(billingONExtDao, times(1)).merge(any(BillingONExt.class));
    assertEquals("2023-11-10", billingONExt.getValue());
  }


  @Test
  public void testChangeInvoiceDueDateExistingInvoiceDueDate_Success() {
    when(request.getParameter("invoiceDueDate")).thenReturn("2023-11-10");

    BillingCorrectionAction billingCorrectionAction = spy(new BillingCorrectionAction());
    ActionForward result = billingCorrectionAction.updateInvoice(mapping, form, request, response);

    verify(billingONExtDao, times(1)).persist(any(BillingONExt.class));
  }

  @Test
  public void testSubmitIsSaveAndCorrectAnother() {
    when(request.getParameter("submit")).thenReturn("Save&Correct Another");

    BillingCorrectionAction billingCorrectionAction = spy(new BillingCorrectionAction());
    ActionForward result = billingCorrectionAction.updateInvoice(mapping, form, request, response);

    verify(mapping, times(1)).findForward(eq("closeReload"));
  }

  @Test
  public void testSubmitIsUnlinkReferralDoctor() {
    when(request.getParameter("submit")).thenReturn("Unlink Referral Doctor");

    BillingCorrectionAction billingCorrectionAction = spy(new BillingCorrectionAction());
    ActionRedirect result = (ActionRedirect) billingCorrectionAction.updateInvoice(mapping, form, request, response);

    verify(mapping, times(1)).findForward(eq("success"));
    assertEquals("billing_no=1", result.getParameterString());
  }

  @Test
  public void testSubmitIsAdminSubmit() {
    when(request.getParameter("adminSubmit")).thenReturn("true");

    BillingCorrectionAction billingCorrectionAction = spy(new BillingCorrectionAction());
    ActionForward result = billingCorrectionAction.updateInvoice(mapping, form, request, response);

    verify(mapping, times(1)).findForward(eq("adminReload"));
  }

  @Test
  public void testChangeBillingItemFee_Success() {
    addBillingItems();

    when(request.getParameterValues("row")).thenReturn(new String[]{"0"});
    when(request.getParameter("servicecode0")).thenReturn("A001A");
    when(request.getParameter("billingunit0")).thenReturn("1");
    when(request.getParameter("dxCode0")).thenReturn("300");
    when(request.getParameter("billingamount0")).thenReturn("32.5");
    when(billingONService.updateTotal(billingONCHeader1)).thenCallRealMethod();

    BillingCorrectionAction billingCorrectionAction = spy(new BillingCorrectionAction());
    ActionForward result = billingCorrectionAction.updateInvoice(mapping, form, request, response);

    verify(mapping, times(1)).findForward(eq("submitClose"));
    verify(billingONService, times(1)).updateTotal(billingONCHeader1);
    assertEquals(new BigDecimal("32.50"), billingONCHeader1.getTotal());
  }

  @Test
  public void testAddNewBillingItem_Success() {
    addBillingItems();

    val newItem = new BillingONItem();
    newItem.setDx("301");
    newItem.setServiceCode("A002A");
    newItem.setServiceCount("1");
    newItem.setFee("15.00");
    newItem.setStatus("O");
    newItem.setCh1Id(1);

    when(request.getParameterValues("row")).thenReturn(new String[]{"0", "1"});
    when(request.getParameter("servicecode0")).thenReturn("A001A");
    when(request.getParameter("billingunit0")).thenReturn("1");
    when(request.getParameter("dxCode0")).thenReturn("300");
    when(request.getParameter("billingamount0")).thenReturn("35.70");
    when(request.getParameter("servicecode1")).thenReturn("A002A");
    when(request.getParameter("billingunit1")).thenReturn("1");
    when(request.getParameter("dxCode1")).thenReturn("301");
    when(request.getParameter("billingamount1")).thenReturn("15.00");
    when(billingONItemDao.saveEntity(any(BillingONItem.class))).thenReturn(newItem);
    when(billingONService.updateTotal(billingONCHeader1)).thenCallRealMethod();

    BillingCorrectionAction billingCorrectionAction = spy(new BillingCorrectionAction());
    ActionForward result = billingCorrectionAction.updateInvoice(mapping, form, request, response);

    verify(mapping, times(1)).findForward(eq("submitClose"));
    verify(billingONService, times(1)).updateTotal(billingONCHeader1);
    assertEquals(2, billingONCHeader1.getBillingItems().size());
    assertEquals(new BigDecimal("50.70"), billingONCHeader1.getTotal());
  }

  private BillingONCHeader1 setupBillingONCHeader1()
      throws NoSuchFieldException, IllegalAccessException {
    val date = new Date();
    val billingDate = new Date();

    billingDate.setDate(8);
    billingDate.setMonth(Calendar.SEPTEMBER);
    billingDate.setYear(123);
    val billingONCHeader1 = new BillingONCHeader1();

    val id = billingONCHeader1.getClass().getDeclaredField("id");
    id.setAccessible(true);
    id.set(billingONCHeader1, 1);

    billingONCHeader1.setHeaderId(1);
    billingONCHeader1.setTranscId("HE");
    billingONCHeader1.setRecId("H");
    billingONCHeader1.setHin("1414141414");
    billingONCHeader1.setDob("19870602");
    billingONCHeader1.setPayProgram("HCP");
    billingONCHeader1.setPayee("P");
    billingONCHeader1.setFaciltyNum("1972");
    billingONCHeader1.setLocation("0000");
    billingONCHeader1.setDemographicNo(1);
    billingONCHeader1.setProviderNo("1");
    billingONCHeader1.setProviderOhipNo("321654");
    billingONCHeader1.setAppointmentNo(1);
    billingONCHeader1.setDemographicName("TEST, DEMOGRAPHIC");
    billingONCHeader1.setSex("1");
    billingONCHeader1.setProvince("ON");
    billingONCHeader1.setBillingDate(billingDate);
    billingONCHeader1.setBillingTime(date);
    billingONCHeader1.setTotal(BigDecimal.valueOf(35.70));
    billingONCHeader1.setPaid(BigDecimal.valueOf(0));
    billingONCHeader1.setStatus("O");
    billingONCHeader1.setVisitType("00");
    billingONCHeader1.setApptProviderNo("999998");
    billingONCHeader1.setCreator("999998");
    billingONCHeader1.setRefNum("");
    billingONCHeader1.setManReview("");
    billingONCHeader1.setComment("");

    return billingONCHeader1;
  }

  private BillingONItem setupBillingItem() {
    val billingONItem = new BillingONItem();
    val billingDate = new Date();
    billingDate.setDate(8);
    billingDate.setMonth(Calendar.SEPTEMBER);
    billingDate.setYear(123);

    billingONItem.setFee("35.70");
    billingONItem.setDx("300");
    billingONItem.setServiceCode("A001A");
    billingONItem.setServiceCount("1");
    billingONItem.setCh1Id(1);
    billingONItem.setStatus("O");
    billingONItem.setServiceDate(billingDate);

    return billingONItem;
  }

  private Provider setupProvider() {
    val provider = new Provider();

    provider.setProviderNo("1");
    provider.setLastName("TEST");
    provider.setFirstName("PROVIDER");
    provider.setOhipNo("321654");

    return provider;
  }

  private void mockObjects() {
    springUtils = mockStatic(SpringUtils.class);
    loggedInInfo = mockStatic(LoggedInInfo.class);
    oscarProperties = mockStatic(OscarProperties.class);

    springUtils
        .when(() -> SpringUtils.getBean(eq("billingONPaymentDao")))
        .thenReturn(billingONPaymentDao);

    springUtils
        .when(() -> SpringUtils.getBean(eq("billingONService")))
        .thenReturn(billingONService);

    springUtils
        .when(() -> SpringUtils.getBean(eq("billingONCHeader1Dao")))
        .thenReturn(billingONCHeader1Dao);

    springUtils
        .when(() -> SpringUtils.getBean(eq(BillingONItemDao.class)))
        .thenReturn(billingONItemDao);

    springUtils
        .when(() -> SpringUtils.getBean(eq("billingONExtDao")))
        .thenReturn(billingONExtDao);

    springUtils
        .when(() -> SpringUtils.getBean(eq("demographicDao")))
        .thenReturn(demographicDao);

    springUtils
        .when(() -> SpringUtils.getBean(eq(InvoiceHistoryDao.class)))
        .thenReturn(invoiceHistoryDao);

    springUtils
        .when(() -> SpringUtils.getBean(eq("billingONRepoDao")))
        .thenReturn(billingONRepoDao);

    springUtils
        .when(() -> SpringUtils.getBean(eq("providerDao")))
        .thenReturn(providerDao);

    loggedInInfo
        .when(() -> LoggedInInfo.getLoggedInInfoFromSession(any(HttpServletRequest.class)))
        .thenAnswer(
            (invocation) -> {
              val loggedInInfo = new LoggedInInfo();
              val facility = new Facility();
              facility.setIntegratorEnabled(false);
              loggedInInfo.setCurrentFacility(facility);
              loggedInInfo.setLoggedInProvider(provider);
              return loggedInInfo;
            });

    oscarProperties.when(OscarProperties::getInstance).thenReturn(properties);
    when(mockProvider.getProviderNo()).thenReturn("1");
    when(properties.getProperty("DATE_FORMAT")).thenReturn("yyyy-MM-dd");
    when(properties.getProperty("TIME_FORMAT")).thenReturn("");
  }

  private void addBillingItems() {
    List<BillingONItem> billingItems = new ArrayList<>();
    billingItems.add(setupBillingItem());
    billingONCHeader1.setBillingItems(billingItems);
  }
}
