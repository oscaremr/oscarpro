/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.oscarBilling.ca.on.pageUtil;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.util.SpringUtils;
import oscar.OscarProperties;

@ExtendWith(MockitoExtension.class)
public class BillingSavePrepTest {

  private static final String EYEFORM_3_PROPERTY_VALUE = "eyeform3";
  private static final String EYEFORM_POC_PROPERTY_VALUE = "eyeform_poc";
  private final String CME_JS_PROPERTY = "cme_js";

  @Mock private OscarProperties oscarProperties;

  BillingSavePrep billingSavePrep;

  private MockedStatic<OscarProperties> mockedOscarProperties;
  private MockedStatic<SpringUtils> mockedSpringUtils;

  @BeforeEach
  public void before() {
    mockedOscarProperties = mockStatic(OscarProperties.class);
    mockedSpringUtils = mockStatic(SpringUtils.class);
    mockedOscarProperties.when(OscarProperties::getInstance).thenReturn(oscarProperties);
    billingSavePrep = new BillingSavePrep();
  }

  @AfterEach
  public void after() {
    mockedOscarProperties.close();
    mockedSpringUtils.close();
  }

  @Test
  public void isEyeFormEnabled_cmeJsIsEyeform3() {
    when(oscarProperties.getProperty(CME_JS_PROPERTY)).thenReturn(EYEFORM_3_PROPERTY_VALUE);
    assertTrue(billingSavePrep.isEyeFormEnabled());
  }

  @Test
  public void isEyeFormEnabled_cmeJsIsEyeformPoc() {
    when(oscarProperties.getProperty(CME_JS_PROPERTY)).thenReturn(EYEFORM_POC_PROPERTY_VALUE);
    assertTrue(billingSavePrep.isEyeFormEnabled());
  }
}
