package oscar.form.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Calendar;
import java.util.GregorianCalendar;
import lombok.val;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.model.Appointment;

class UrlUtilTest {

  @Test
  void getProBillingUrl() {
    val appointment = new Appointment();
    appointment.setId(11111);
    appointment.setDemographicNo(22222);
    val url = UrlUtil.getProBillingUrl("testBaseUrl", appointment, "&sign=true");
    val expectedUrl =
        "<a href=# onClick='popupPage(755, 1200, "
            + "\"/testBaseUrl/#/billing/"
            + "?demographicNo=22222"
            + "&appointmentNo=11111"
            + "&sign=true"
            + "\"); return false;'>Bill";
    assertEquals(expectedUrl, url);
  }

  @Test
  void getClassicBillingUrl() {
    val appointment = new Appointment();

    Calendar date = new GregorianCalendar();
    // reset hour, minutes, seconds and millis
    date.set(Calendar.YEAR, 2023);
    date.set(Calendar.MONTH, Calendar.MARCH);
    date.set(Calendar.DAY_OF_MONTH, 16);
    date.set(Calendar.HOUR_OF_DAY, 12);
    date.set(Calendar.MINUTE, 30);
    date.set(Calendar.SECOND, 0);
    date.set(Calendar.MILLISECOND, 0);
    val apptDateTime = date.getTime();

    appointment.setId(11111);
    appointment.setDemographicNo(22222);
    appointment.setProviderNo("33333");
    appointment.setAppointmentDate(apptDateTime);
    appointment.setStartTime(apptDateTime);
    val url = UrlUtil.getClassicBillingUrl("testDefaultView", "testDemographicNumber", appointment);
    val expectedUrl =
        "<a href=# onClick='popupPage(700, 1000, \""
            + "billingOB.jsp"
            + "?billForm=testDefaultView"
            + "&hotclick="
            + "&appointment_no=11111"
            + "&demographic_name="
            + "&demographic_no=22222"
            + "&user_no=33333"
            + "&apptProvider_no=testDemographicNumber"
            + "&appointment_date=2023-03-16"
            + "&start_time=12:30:00"
            + "&bNewForm=1"
            + "\"); return false;'>Bill ";
    assertEquals(expectedUrl, url);
  }
}
