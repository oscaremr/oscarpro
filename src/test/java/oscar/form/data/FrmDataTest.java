package oscar.form.data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.dao.EncounterFormDao;
import org.oscarehr.common.model.EncounterForm;
import org.oscarehr.util.SpringUtils;
import oscar.oscarDB.DBHandler;

import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Execution(ExecutionMode.SAME_THREAD)
public class FrmDataTest {
  private static final String PERINATAL_TABLE = "form_on_perinatal_2017";
  private static final String FORM_VALUE = "formONPerinatalRecord.js";
  private static final String MAX_VALUE = String.valueOf(Integer.MAX_VALUE);
  private static final String FORM_NAME = "Perinatal";
  private static final String DEMOGRAPHIC_NUMBER = "1";
  private static final String EXPECTED_QUERY = "SELECT form_id FROM " + PERINATAL_TABLE
      + " WHERE demographic_no=" + DEMOGRAPHIC_NUMBER
      + " order by create_date, formEdited desc limit 0,1";

  private final EncounterForm encounterForm = new EncounterForm();
  private final List<EncounterForm> encounterFormList = Arrays.asList(encounterForm);

  @Mock
  private EncounterFormDao encounterFormDao;
  @Mock
  private ResultSet resultSet;
  private final static MockedStatic<SpringUtils> SPRING_UTILS = mockStatic(SpringUtils.class);
  private final static MockedStatic<DBHandler> DB_HANDLER = mockStatic(DBHandler.class);

  @BeforeEach
  public void setup() throws SQLException {
    encounterForm.setFormTable(PERINATAL_TABLE);
    encounterForm.setFormValue(FORM_VALUE);

    SPRING_UTILS.when(() -> SpringUtils.getBean(any(String.class)))
        .thenReturn(encounterFormDao);
    DB_HANDLER.when(() -> DBHandler.GetSQL(EXPECTED_QUERY)).thenReturn(resultSet);

    when(encounterFormDao.findByFormName(any(String.class))).thenReturn(encounterFormList);
    when(resultSet.next()).thenReturn(true, false);
    when(resultSet.getString(any(String.class))).thenReturn(MAX_VALUE);
  }

  @AfterAll
  public static void close() {
    SPRING_UTILS.close();
    DB_HANDLER.close();
  }

  @Test
  public void givenForm_whenGetShortcutFormValue_thenReturnPerinatalFormInfo() throws SQLException {
      String[] shortcutFormValue = (new FrmData()).getShortcutFormValue(DEMOGRAPHIC_NUMBER, FORM_NAME);

      DB_HANDLER.verify(() -> DBHandler.GetSQL(EXPECTED_QUERY), times(1));
      assertEquals(FORM_VALUE, shortcutFormValue[0]);
      assertEquals(MAX_VALUE, shortcutFormValue[1]);
  }
}
