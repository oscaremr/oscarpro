package oscar.form.dao;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.SQLException;
import java.util.Properties;
import lombok.val;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.utils.SchemaUtils;
import org.oscarehr.util.SpringUtils;
import oscar.form.FrmRecordHelp;

@ExtendWith(MockitoExtension.class)
public class Rourke2009DAOTest {

  @Nested
  class Unit {

    @Mock
    private FrmRecordHelp frmRecordHelp;

    @InjectMocks
    private Rourke2009DAO rourke2009DAO;

    private Properties props;
    private String demographic_no;

    @BeforeEach
    void setUp() {
      props = new Properties();
      props.setProperty("p1_name", "John Doe");
      props.setProperty("p2_age", "5");
      props.setProperty("p3_weight", "20");
      props.setProperty("p4_height", "110");
      demographic_no = "123";
    }

    @Test
    void givenValidInput_whenSaveWithTransaction_thenVerifiesSaveFormRecord() throws SQLException {
      // Arrange
      when(frmRecordHelp.saveFormRecord(
          any(Properties.class), any(String.class), any(Boolean.class))).thenReturn(5);
      // Act
      rourke2009DAO.saveWithTransaction(props, demographic_no, frmRecordHelp);
      // Assert
      verify(frmRecordHelp).saveFormRecord(any(Properties.class),
          eq("SELECT * FROM formRourke2009_base WHERE demographic_no=123 AND ID=0"), eq(true));
      verify(frmRecordHelp).saveFormRecord(any(Properties.class),
          eq("SELECT * FROM formRourke2009_page1 WHERE ID=5"), eq(false));
      verify(frmRecordHelp).saveFormRecord(any(Properties.class),
          eq("SELECT * FROM formRourke2009_page2 WHERE ID=5"), eq(false));
      verify(frmRecordHelp).saveFormRecord(any(Properties.class),
          eq("SELECT * FROM formRourke2009_page3 WHERE ID=5"), eq(false));
      verify(frmRecordHelp).saveFormRecord(any(Properties.class),
          eq("SELECT * FROM formRourke2009_page4 WHERE ID=5"), eq(false));
    }
  }

  @Nested
  class Integration extends DaoTestFixtures {

    @Spy
    private FrmRecordHelp frmRecordHelpMock;

    protected Rourke2009DAO dao = SpringUtils.getBean(Rourke2009DAO.class);

    @BeforeEach
    public void before(
    ) throws Exception {
      SchemaUtils.restoreTable(
          false,
          "PeriodDef",
          "formRourke2009_base",
          "formRourke2009_page1",
          "formRourke2009_page2",
          "formRourke2009_page3",
          "formRourke2009_page4"
      );
    }

    @Test
    public void givenValidProperties_whenSaveWithTransaction_thenCorrectRecordCount(
    ) throws SQLException {
      // Arrange
      val props = getProps();
      val frmRecordHelp = new FrmRecordHelp();
      // Act
      dao.saveWithTransaction(props, "1", frmRecordHelp);
      val recordCount = dao.countRecordsInDatabase();
      // Assert
      assertEquals(5L, recordCount, "Expected record count after save operation");
    }

    @ParameterizedTest
    @CsvSource({"base", "page1", "page2", "page3", "page4"})
    public void givenExceptionOnSave_whenSaveWithTransaction_thenRollback(
        final String formPage
    ) throws SQLException {
      // Arrange
      val props = getProps();
      doAnswer(new Answer<Integer>() {
        @Override
        public Integer answer(InvocationOnMock invocation) throws Throwable {
          String sql = invocation.getArgument(1);
          if (sql.contains("formRourke2009_" + formPage)) {
            throw new SQLException("Simulated SQL Exception");
          }
          return (Integer) invocation.callRealMethod();
        }
      }).when(frmRecordHelpMock)
          .saveFormRecord(any(Properties.class), any(String.class), any(Boolean.class));
      val initialCount = dao.countRecordsInDatabase();
      assertThrows(SQLException.class, new Executable() {
        @Override
        public void execute() throws Throwable {
          // Act
          dao.saveWithTransaction(props, "1", frmRecordHelpMock);
        }
      }, "Expected SQLException was not thrown.");
      // Assert
      val finalCount = dao.countRecordsInDatabase();
      assertEquals(
          initialCount, finalCount, "Database state changed despite transaction rollback");
    }


    private Properties getProps() {
      val props = new Properties();
      // Base page
      props.setProperty("ID", "1");
      props.setProperty("demographic_no", "1");
      props.setProperty("provider_no", "1");
      props.setProperty("formCreated", "2021-01-01");
      props.setProperty("formEdited", "2021-01-02");
      props.setProperty("c_lastVisited", "Yes");
      props.setProperty("c_birthRemarks", "Remarks");
      props.setProperty("c_riskFactors", "Risk Factors");
      props.setProperty("c_famHistory", "Family History");
      props.setProperty("c_pName", "Patient Name");
      props.setProperty("c_birthDate", "2000-01-01");
      props.setProperty("c_length", "50");
      props.setProperty("c_headCirc", "35");
      props.setProperty("c_birthWeight", "3.5");
      props.setProperty("c_dischargeWeight", "3.6");
      props.setProperty("c_fsa", "FSA");
      props.setProperty("c_APGAR1min", "8");
      props.setProperty("c_APGAR5min", "9");
      props.setProperty("archived", "0");
      // Page one
      props.setProperty("p1_2ndhandsmoke", "1");
      props.setProperty("p1_alcohol", "0");
      props.setProperty("p1_drugs", "0");
      props.setProperty("p1_birthRemarksr1", "1");
      props.setProperty("p1_birthRemarksr2", "0");
      props.setProperty("p1_birthRemarksr3", "1");
      props.setProperty("p1_date1w", "2024-03-25");
      props.setProperty("p1_date2w", "2024-04-01");
      props.setProperty("p1_date1m", "2024-04-22");
      props.setProperty("p1_ht1w", "50");
      props.setProperty("p1_wt1w", "3.5");
      props.setProperty("p1_hc1w", "35");
      props.setProperty("p1_ht2w", "51");
      props.setProperty("p1_wt2w", "3.7");
      props.setProperty("p1_hc2w", "35.5");
      props.setProperty("p1_ht1m", "53");
      props.setProperty("p1_wt1m", "4.0");
      props.setProperty("p1_hc1m", "36");
      props.setProperty("p1_breastFeeding1wOk", "1");
      props.setProperty("p1_breastFeeding1wOkConcerns", "0");
      props.setProperty("p1_formulaFeeding1wOk", "1");
      props.setProperty("p1_formulaFeeding1wOkConcerns", "0");
      props.setProperty("p1_pConcern1w", "No concerns at 1 week");
      props.setProperty("p1_pConcern2w", "Some mild concerns at 2 weeks");
      // Page two
      props.setProperty("p2_date2m", "2024-05-01");
      props.setProperty("p2_date4m", "2024-07-01");
      props.setProperty("p2_date6m", "2024-09-01");
      props.setProperty("p2_ht2m", "60");
      props.setProperty("p2_wt2m", "5.5");
      props.setProperty("p2_hc2m", "40");
      props.setProperty("p2_ht4m", "65");
      props.setProperty("p2_wt4m", "6.5");
      props.setProperty("p2_hc4m", "42");
      props.setProperty("p2_ht6m", "70");
      props.setProperty("p2_wt6m", "7.5");
      props.setProperty("p2_hc6m", "44");
      props.setProperty("p2_pConcern2m", "Parent concern details for 2m");
      props.setProperty("p2_pConcern4m", "Parent concern details for 4m");
      props.setProperty("p2_pConcern6m", "Parent concern details for 6m");
      props.setProperty("p2_nutrition2m", "Nutrition details for 2m");
      props.setProperty("p2_nutrition4m", "Nutrition details for 4m");
      props.setProperty("p2_breastFeeding2mOk", "1");
      props.setProperty("p2_breastFeeding2mOkConcerns", "0");
      props.setProperty("p2_formulaFeeding2mOk", "1");
      props.setProperty("p2_formulaFeeding2mOkConcerns", "0");
      props.setProperty("p2_carSeatOk", "1");
      props.setProperty("p2_carSeatOkConcerns", "0");
      props.setProperty("p2_sleepPosOk", "1");
      props.setProperty("p2_sleepPosOkConcerns", "0");
      props.setProperty("p2_eyesOk", "1");
      props.setProperty("p2_eyesOkConcerns", "0");
      props.setProperty("p2_coosOk", "1");
      props.setProperty("p2_coosOkConcerns", "0");
      props.setProperty("p2_respondsOk", "1");
      props.setProperty("p2_respondsOkConcerns", "0");
      //Page Three
      props.setProperty("p3_date9m", "2024-11-01");
      props.setProperty("p3_date12m", "2025-02-01");
      props.setProperty("p3_date15m", "2025-05-01");

// Varchar for measurements
      props.setProperty("p3_ht9m", "72");
      props.setProperty("p3_wt9m", "8.5");
      props.setProperty("p3_hc9m", "46");
      props.setProperty("p3_ht12m", "76");
      props.setProperty("p3_wt12m", "9.5");
      props.setProperty("p3_hc12m", "47.5");
      props.setProperty("p3_ht15m", "80");
      props.setProperty("p3_wt15m", "10.5");
      props.setProperty("p3_hc15m", "48.5");
      props.setProperty("p3_pConcern9m", "Parent concern details for 9m");
      props.setProperty("p3_pConcern12m", "Parent concern details for 12m");
      props.setProperty("p3_pConcern15m", "Parent concern details for 15m");
      props.setProperty("p3_nutrition9m", "Nutrition details for 9m");
      props.setProperty("p3_nutrition12m", "Nutrition details for 12m");
      props.setProperty("p3_nutrition15m", "Nutrition details for 15m");
      props.setProperty("p3_breastFeeding9mOk", "1");
      props.setProperty("p3_breastFeeding9mOkConcerns", "0");
      props.setProperty("p3_formulaFeeding9mOk", "1");
      props.setProperty("p3_formulaFeeding9mOkConcerns", "0");
      props.setProperty("p3_carSeatOk", "1");
      props.setProperty("p3_carSeatOkConcerns", "0");
      props.setProperty("p3_poisonsOk", "1");
      props.setProperty("p3_poisonsOkConcerns", "0");
      props.setProperty("p3_activeOk", "1");
      props.setProperty("p3_activeOkConcerns", "0");
      props.setProperty("p3_readingOk", "1");
      props.setProperty("p3_readingOkConcerns", "0");
      props.setProperty("p3_sunExposureOk", "1");
      props.setProperty("p3_sunExposureOkConcerns", "0");
      //Page Four
      props.setProperty("p4_noParentsConcerns60mOk", "on");
      props.setProperty("p4_noParentsConcerns60mOkConcerns", "No");
      props.setProperty("p4_fontanellesClosedOk", "on");
      props.setProperty("p4_fontanellesClosedOkConcerns", "No");
      props.setProperty("p4_eyes18mOk", "on");
      props.setProperty("p4_eyes18mOkConcerns", "No");
      props.setProperty("p4_corneal18mOk", "on");
      props.setProperty("p4_corneal18mOkConcerns", "No");
      props.setProperty("p4_hearing18mOk", "on");
      props.setProperty("p4_hearing18mOkConcerns", "No");
      props.setProperty("p4_tonsil18mOk", "on");
      props.setProperty("p4_tonsil18mOkConcerns", "No");
      props.setProperty("p4_bloodpressure24mOk", "on");
      props.setProperty("p4_bloodpressure24mOkConcerns", "No");
      props.setProperty("p4_eyes24mOk", "on");
      props.setProperty("p4_eyes24mOkConcerns", "No");
      props.setProperty("p4_corneal24mOk", "on");
      props.setProperty("p4_corneal24mOkConcerns", "No");
      props.setProperty("p4_hearing24mOk", "on");
      props.setProperty("p4_hearing24mOkConcerns", "No");
      props.setProperty("p4_tonsil24mOk", "on");
      props.setProperty("p4_tonsil24mOkConcerns", "No");
      props.setProperty("p4_bloodpressure48mOk", "on");
      props.setProperty("p4_bloodpressure48mOkConcerns", "No");
      props.setProperty("p4_eyes48mOk", "on");
      props.setProperty("p4_eyes48mOkConcerns", "No");
      props.setProperty("p4_corneal48mOk", "on");
      props.setProperty("p4_corneal48mOkConcerns", "No");
      props.setProperty("p4_hearing48mOk", "on");
      props.setProperty("p4_hearing48mOkConcerns", "No");
      props.setProperty("p4_tonsil48mOk", "on");
      props.setProperty("p4_tonsil48mOkConcerns", "No");
      props.setProperty("p4_nippisingattained", "on");

      return props;
    }
  }
}
