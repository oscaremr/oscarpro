/**
 * Copyright (c) 2024 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.form.model;

import static org.junit.Assert.*;

import lombok.val;
import org.junit.Test;

public class DatabaseTagTest {

  @Test
  public void testGetSubjectAndField() {
    val databaseTag = new DatabaseTag(
        "testSubject", "testField", "testTitle", false);
    val result = databaseTag.getSubjectAndField();
    assertEquals("testSubject.testField", result);
  }
}