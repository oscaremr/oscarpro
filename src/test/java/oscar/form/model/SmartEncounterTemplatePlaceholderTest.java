/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.form.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import lombok.val;
import org.junit.Test;
import org.oscarehr.common.model.ProfessionalSpecialist;

public class SmartEncounterTemplatePlaceholderTest {

  @Test
  public void givenReferralPhysicianFields_whenGetMethodBySubjectAndField_thenReturnMethod()
      throws NoSuchMethodException {
    val getFirstNameMethod = ProfessionalSpecialist.class.getDeclaredMethod("getFirstName");
    val getLastNameMethod = ProfessionalSpecialist.class.getDeclaredMethod("getLastName");
    val resultFirstNameMethod = SmartEncounterTemplatePlaceholder
        .getMethodBySubjectAndField("demographic.referringPhysicianFirstName");
    val resultLastNameMethod = SmartEncounterTemplatePlaceholder
        .getMethodBySubjectAndField("demographic.referringPhysicianLastName");

    assertEquals(getFirstNameMethod, resultFirstNameMethod);
    assertEquals(getLastNameMethod, resultLastNameMethod);
  }

  @Test
  public void givenReferralPhysicianPlaceholders_whenGetPlaceholderSubjectAndField_thenReturnPlaceholder() {
    val resultFirstNamePlaceholder = SmartEncounterTemplatePlaceholder
        .getPlaceholderSubjectAndField("demographic.referringPhysicianFirstName");
    val resultLastNamePlaceholder = SmartEncounterTemplatePlaceholder
        .getPlaceholderSubjectAndField("demographic.referringPhysicianLastName");

    assertEquals(SmartEncounterTemplatePlaceholder.DEMOGRAPHIC_REFERRING_PHYSICIAN_FIRSTNAME,
        resultFirstNamePlaceholder);
    assertEquals(SmartEncounterTemplatePlaceholder.DEMOGRAPHIC_REFERRING_PHYSICIAN_LASTNAME,
        resultLastNamePlaceholder);
  }
}
