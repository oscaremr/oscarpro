package oscar.form.model;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

class DatabaseTagServiceTest {

  @Test
  void givenDatabaseService_whenInvokingAddDefaultDatabaseTags_thenListDefaultDatabaseTags() {
    DatabaseTagService d = new DatabaseTagService();
    d.addDefaultDatabaseTags();

    Set<String> expectedFields = 
        new HashSet<>(
            Arrays.asList(
                "todaysDate",
                "fullName",
                "firstName",
                "lastName",
                "label",
                "age",
                "gender",
                "subjectPronoun",
                "possessivePronoun",
                "referringPhysicianLabel",
                "referringPhysicianFullName",
                "referringPhysicianFirstName",
                "referringPhysicianLastName",
                "familyDoctorLabel",
                "familyDoctorFullName",
                "familyDoctorFirstName",
                "familyDoctorLastName",
                "fullName",
                "firstName",
                "lastName",
                "no",
                "date",
                "providerId",
                "providerName"));

    Set<String> actualFields = new HashSet<String>();
    for (DatabaseTag dt : d.getDatabaseTagMethodMap().keySet()) {
      actualFields.add(dt.getField());
    }
    Assertions.assertEquals(expectedFields, actualFields);
  }

  @Test
  void givenDatabaseService_whenInvokingAddCustomTagToMap_thenListAddedCustomTag() {
    DatabaseTagService d = new DatabaseTagService();
    String subject = "subject";
    String field = "field";
    String title = "title";
    Boolean textBlock = true;
    d.addCustomTagToMap(subject, field, title, textBlock);

    Set<String> actualSubject = new HashSet<String>();
    for (DatabaseTag dt : d.getDatabaseTagMethodMap().keySet()) {
      actualSubject.add(dt.getSubject());
    }

    Set<String> actualField = new HashSet<String>();
    for (DatabaseTag dt : d.getDatabaseTagMethodMap().keySet()) {
      actualField.add(dt.getField());
    }

    Set<String> actualTitle = new HashSet<String>();
    for (DatabaseTag dt : d.getDatabaseTagMethodMap().keySet()) {
      actualTitle.add(dt.getTitle());
    }

    assertTrue(actualSubject.contains(subject));
    assertTrue(actualField.contains(field));
    assertTrue(actualTitle.contains(title));
  }

}
