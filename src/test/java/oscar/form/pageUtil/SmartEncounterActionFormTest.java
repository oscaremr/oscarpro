package oscar.form.pageUtil;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import lombok.val;
import org.apache.commons.collections.MapUtils;
import org.apache.struts.action.ActionMapping;
import org.junit.Test;
import org.mockito.MockedStatic;
import org.mockito.MockedStatic.Verification;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.oscarehr.util.SpringUtils;
import oscar.form.dao.FormStringValueDao;
import oscar.form.model.FormSmartEncounter;
import oscar.form.model.FormStringValue;
import oscar.form.model.FormValuePK;

public class SmartEncounterActionFormTest {

  private final static FormStringValueDao formStringValueDao = mock(FormStringValueDao.class);
  public static final String ATTACHMENTS = "attachments";

  @Test
  public void testReset_successWithParameters() {
    val mapping = mock(ActionMapping.class);
    val request = mock(HttpServletRequest.class);
    val expectedHashmap = new HashMap<String, String>();
    expectedHashmap.put("demographic.name", "test, test");
    expectedHashmap.put("today", "2023-01-01");
    val parameterMap = new HashMap<String, String[]>();
    for (val entry : expectedHashmap.entrySet()) {
      parameterMap.put("placeholderValueMap[" + entry.getKey() + "]", new String[]{entry.getValue()});
    }
    when(request.getParameterMap()).thenReturn(parameterMap);

    val form = new SmartEncounterActionForm();
    form.reset(mapping, request);
    assertEquals(expectedHashmap, form.getPlaceholderValueMap());
  }

  @Test
  public void testReset_successWithoutParameters() {
    val mapping = mock(ActionMapping.class);
    val request = mock(HttpServletRequest.class);
    val expectedHashmap = new HashMap<String, String>();
    when(request.getParameterMap()).thenReturn(new HashMap<String, String[]>());

    val form = new SmartEncounterActionForm();
    form.reset(mapping, request);
    assertEquals(expectedHashmap, form.getPlaceholderValueMap());
  }

  @Test
  public void testGetHeaderAndFooterFields_successWithItems() {
    val headerAndFooterFields = new ArrayList<String>();
    headerAndFooterFields.add("demographic.name");
    headerAndFooterFields.add("demographic.age");
    val headerText = "${demographic.name}";
    val footerText = "${demographic.age}";
    val form = new SmartEncounterActionForm();
    form.setHeaderText(headerText);
    form.setFooterText(footerText);

    assertEquals(headerAndFooterFields, form.getHeaderAndFooterFields());
  }

  @Test
  public void testGetExistingPlaceholderValueMap_success() {
    val smartEncounterForm = createSmartEncounterActionFormWithPlaceholders();
    val formSmartEncounter = new FormSmartEncounter();
    formSmartEncounter.setId(1);
    val expectedMap = new HashMap<String, FormStringValue>();
    expectedMap.put("demographic.name", new FormStringValue(FormSmartEncounter.FORM_TABLE, 1, "demographic.name", "test, test"));
    expectedMap.put("demographic.age", new FormStringValue(FormSmartEncounter.FORM_TABLE, 1, "demographic.age", "30"));
    val placeholderValuesKeySet = smartEncounterForm.getPlaceholderValueMap().keySet();
    doReturn(MapUtils.EMPTY_MAP).when(smartEncounterForm).getExistingAndUpdatedHeaderAndFooterValueMap(eq(formSmartEncounter),
        eq(placeholderValuesKeySet));
    val result = smartEncounterForm.getExistingPlaceholderValueMap(formSmartEncounter);

    assertEquals(expectedMap.size(), result.size());
    for (val expectedEntry : expectedMap.entrySet()) {
      val expectedValue = expectedEntry.getValue();
      val resultValue = result.get(expectedEntry.getKey());
      assertEquals(expectedValue.getValue(), resultValue.getValue());
      assertEquals(expectedValue.getId().getFormName(), resultValue.getId().getFormName());
      assertEquals(expectedValue.getId().getFormId(), resultValue.getId().getFormId());
      assertEquals(expectedValue.getId().getFieldName(), resultValue.getId().getFieldName());
    }
  }

  private SmartEncounterActionForm createSmartEncounterActionFormWithPlaceholders() {
    val form = spy(SmartEncounterActionForm.class);
    form.setFormId(1);
    val placeholderValueMap = new HashMap<String, String>();
    placeholderValueMap.put("demographic.name", "test, test");
    placeholderValueMap.put("demographic.age", "30");
    form.setPlaceholderValueMap(placeholderValueMap);

    return form;
  }

  @Test
  public void testGetExistingAndUpdatedHeaderAndFooter_successWithSameFields() {
    val form = createAndPrepareSmartEncounterActionForm();
    val formStringValueMap = mockExistingFormStringValues();
    val formSmartEncounter = new FormSmartEncounter();
    try (MockedStatic<SpringUtils> mockedSpringUtils = mockSpringUtils();
        MockedStatic<SmartEncounterPlaceholderResolver> mockedSmartEncounterPlaceholderResolver
        = mockSmartEncounterPlaceholderResolver(new HashMap<String, FormStringValue>())) {
      val result = form.getExistingAndUpdatedHeaderAndFooterValueMap(formSmartEncounter,
          new HashSet<String>());
      assertEquals(formStringValueMap, result);
      mockedSmartEncounterPlaceholderResolver.verify(new Verification() {
        @Override
        public void apply() throws Throwable {
          SmartEncounterPlaceholderResolver.resolveForm(eq(formSmartEncounter), eq(
              new ArrayList<String>()));
        }
      });
    }

  }

  @Test
  public void testGetExistingAndUpdatedHeaderAndFooter_successWithExistingAndNewFields() {
    val form = createAndPrepareSmartEncounterActionForm();
    val existingValues = mockExistingFormStringValues();
    val newValues = mockNewFormStringValues();
    val formStringValueMap = new HashMap<String, FormStringValue>();
    formStringValueMap.putAll(existingValues);
    formStringValueMap.putAll(newValues);
    form.setFooterText("${demographic.age}, ${today}");

    val formSmartEncounter = new FormSmartEncounter();

    try (MockedStatic<SpringUtils> mockedSpringUtils = mockSpringUtils();
        MockedStatic<SmartEncounterPlaceholderResolver> mockedSmartEncounterPlaceholderResolver
        = mockSmartEncounterPlaceholderResolver(newValues)) {
      val result = form.getExistingAndUpdatedHeaderAndFooterValueMap(formSmartEncounter,
          new HashSet<String>());

      assertEquals(formStringValueMap, result);
      mockedSmartEncounterPlaceholderResolver.verify(new Verification() {
        @Override
        public void apply() throws Throwable {
          SmartEncounterPlaceholderResolver.resolveForm(eq(formSmartEncounter), eq(
              Collections.singletonList("today")));
        }
      });
    }
  }

  @Test
  public void testGetExistingAndUpdatedHeaderAndFooter_successWithOnlyNewFields() {
    val form = new SmartEncounterActionForm();
    val newValues = mockNewFormStringValues();
    form.setHeaderText("");
    form.setFooterText("${today}");

    val formSmartEncounter = new FormSmartEncounter();

    try (MockedStatic<SpringUtils> mockedSpringUtils = mockSpringUtils();
        MockedStatic<SmartEncounterPlaceholderResolver> mockedSmartEncounterPlaceholderResolver
            = mockSmartEncounterPlaceholderResolver(newValues)) {
      val result = form.getExistingAndUpdatedHeaderAndFooterValueMap(formSmartEncounter,
          new HashSet<String>());

      assertEquals(newValues, result);
      mockedSmartEncounterPlaceholderResolver.verify(new Verification() {
        @Override
        public void apply() throws Throwable {
          SmartEncounterPlaceholderResolver.resolveForm(eq(formSmartEncounter), eq(
              Collections.singletonList("today")));
        }
      });
    }
  }

  @Test
  public void testGetExistingAndUpdatedHeaderAndFooter_successWithMatchingMainBodyFields() {
    val form = createAndPrepareSmartEncounterActionForm();
    val formStringValueMap = mockExistingFormStringValues();
    val formSmartEncounter = new FormSmartEncounter();
    try (MockedStatic<SpringUtils> mockedSpringUtils = mockSpringUtils();
        MockedStatic<SmartEncounterPlaceholderResolver> mockedSmartEncounterPlaceholderResolver
            = mockSmartEncounterPlaceholderResolver(new HashMap<String, FormStringValue>())) {
      val result = form.getExistingAndUpdatedHeaderAndFooterValueMap(formSmartEncounter,
          formStringValueMap.keySet());
      assertTrue(result.isEmpty());
      mockedSmartEncounterPlaceholderResolver.verify(new Verification() {
        @Override
        public void apply() throws Throwable {
          SmartEncounterPlaceholderResolver.resolveForm(eq(formSmartEncounter), eq(
              new ArrayList<String>()));
        }
      });
    }
  }

  @Test
  public void testGetHeaderAndFooterFields_successWithoutPlaceholders() {
    val form = new SmartEncounterActionForm();
    form.setHeaderText("headerText");
    form.setFooterText("footerText");

    assertTrue(form.getHeaderAndFooterFields().isEmpty());
  }

  @Test
  public void testGetHeaderAndFooterFields_successNoHeaderOrFooter() {
    val form = new SmartEncounterActionForm();
    form.setHeaderText("");
    form.setFooterText("");

    assertTrue(form.getHeaderAndFooterFields().isEmpty());
  }

  @Test
  public void givenAttachment_whenToFormSmartEncounter_thenAttachmentIsSet() {
    val form = new SmartEncounterActionForm();
    form.setHtmlText("");
    form.setAttachments(ATTACHMENTS);

    FormSmartEncounter formSmartEncounter = form.toFormSmartEncounter();

    assertEquals(ATTACHMENTS, formSmartEncounter.getAttachments());
  }

  @Test
  public void givenEmptyArrayAttachment_whenToFormSmartEncounter_thenAttachmentIsNull() {
    val form = new SmartEncounterActionForm();
    form.setHtmlText("");
    form.setAttachments("[]");

    FormSmartEncounter formSmartEncounter = form.toFormSmartEncounter();

    assertNull(formSmartEncounter.getAttachments());
  }

  @Test
  public void givenEmptyAttachment_whenToFormSmartEncounter_thenAttachmentIsNull() {
    val form = new SmartEncounterActionForm();
    form.setHtmlText("");
    form.setAttachments("");

    FormSmartEncounter formSmartEncounter = form.toFormSmartEncounter();

    assertNull(formSmartEncounter.getAttachments());
  }

  @Test
  public void givenNullAttachment_whenToFormSmartEncounter_thenAttachmentIsNull() {
    val form = new SmartEncounterActionForm();
    form.setHtmlText("");
    form.setAttachments(null);

    FormSmartEncounter formSmartEncounter = form.toFormSmartEncounter();

    assertNull(formSmartEncounter.getAttachments());
  }

  private SmartEncounterActionForm createAndPrepareSmartEncounterActionForm() {
    val headerText = "${demographic.name}";
    val footerText = "${demographic.age}";
    val form = spy(SmartEncounterActionForm.class);
    form.setHeaderText(headerText);
    form.setFooterText(footerText);



    return form;
  }

  private Map<String, FormStringValue> mockExistingFormStringValues() {
    val stringValueMap = new HashMap<String, FormStringValue>();
    stringValueMap.put("demographic.name", new FormStringValue(new FormValuePK(FormSmartEncounter.FORM_TABLE, 1, "demographic.name"), "test, test"));
    stringValueMap.put("demographic.age", new FormStringValue(new FormValuePK(FormSmartEncounter.FORM_TABLE, 1, "demographic.age"), "30"));
    when(formStringValueDao.findAllForForm(any(FormSmartEncounter.class)))
        .thenReturn(stringValueMap);

    return stringValueMap;
  }

  private Map<String, FormStringValue> mockNewFormStringValues() {
    val newValues = new HashMap<String, FormStringValue>();
    newValues.put("today", new FormStringValue(
        new FormValuePK(FormSmartEncounter.FORM_TABLE, 1, "today"), "2023-01-01"));
    return newValues;
  }

  private MockedStatic<SpringUtils> mockSpringUtils() {
    return Mockito.mockStatic(SpringUtils.class,
        new Answer() {
          @Override
          public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
            if (invocationOnMock.getMethod().getName().equalsIgnoreCase("getbean")
                && invocationOnMock.getArgument(0).equals(FormStringValueDao.class)) {
              return formStringValueDao;
            }
            return invocationOnMock.callRealMethod();
          }
        });
  }

  private MockedStatic<SmartEncounterPlaceholderResolver> mockSmartEncounterPlaceholderResolver(
      final Map<String, FormStringValue> resolvedValues) {
    return Mockito.mockStatic(SmartEncounterPlaceholderResolver.class,
        new Answer() {
          @Override
          public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
            if (invocationOnMock.getMethod().getName().equalsIgnoreCase("resolveForm")) {
              return resolvedValues;
            }
            return invocationOnMock.callRealMethod();
          }
        });
  }
}
