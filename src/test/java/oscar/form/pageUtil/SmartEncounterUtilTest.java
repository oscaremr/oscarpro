/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.form.pageUtil;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import oscar.OscarProperties;

@ExtendWith(MockitoExtension.class)
public class SmartEncounterUtilTest {

  private final String VALID_MIME_TYPE = "image/anythingCanGoHere";
  private final String INVALID_MIME_TYPE = "application/json";
  private final String MOCK_PROPERTIES_FILE_PATH_ABSOLUTE = "C:\\the\\correct\\file\\directory";
  private final String MOCK_PROPERTIES_FILE_PATH_RELATIVE = "/the/correct/file/directory";
  private final String IMAGE_PATH_PROPERTY_NAME = "form_smart_encounter_images_path";
  private final String VALID_FILE_PATH_ABSOLUTE = "C:\\the\\correct\\file\\directory";
  private final String VALID_FILE_PATH_RELATIVE = "/the/correct/file/directory";
  private final String INVALID_FILE_PATH_ABSOLUTE = "C:\\nothing\\can\\go\\here";
  private final String INVALID_FILE_PATH_RELATIVE = "/nothing/can/go/here";
  private final String INVALID_FILE_PATH_WITH_BACKTRACKING_RELATIVE = "..\\..\\..\\nothing\\can\\go\\here";
  private final String INVALID_FILE_PATH_WITH_CORRECT_PREFIX_PATH_ABSOLUTE =
      "C:\\the\\correct\\file\\directory\\..\\..\\..\\nothing\\can\\go\\here";

  private MockedStatic<OscarProperties> oscarPropertiesMockedStatic;

  @Mock
  private OscarProperties oscarProperties;

  @Mock
  private HttpServletRequest request;

  @BeforeEach
  public void before() {
    oscarPropertiesMockedStatic = mockStatic(OscarProperties.class);
    when(OscarProperties.getInstance()).thenAnswer(
        (Answer<OscarProperties>) invocation -> oscarProperties);
  }

  @AfterEach
  public void close(){
    oscarPropertiesMockedStatic.close();
  }

  @Test
  public void givenMimeType_whenIsValidMimeType_thenValidMimeTypeisTrue() {
    assertTrue(SmartEncounterUtil.isValidMimeType(VALID_MIME_TYPE));
  }

  @Test
  public void givenMimeType_whenIsInvalidMimeType_thenValidMimeTypeisFalse() {
    assertFalse(SmartEncounterUtil.isValidMimeType(INVALID_MIME_TYPE));
  }

  @Test
  public void givenImagePath_whenIsValidAbsoluteFilePath_thenReturnAbsoluteFilePathValid() {
    when(oscarProperties.getProperty(IMAGE_PATH_PROPERTY_NAME))
        .thenAnswer((Answer<String>) invocation -> MOCK_PROPERTIES_FILE_PATH_ABSOLUTE);
    assertTrue(SmartEncounterUtil.isValidFilePath(VALID_FILE_PATH_ABSOLUTE));
  }

  @Test
  public void givenImagePath_whenIsValidRelativeFilePath_thenReturnRelativeFilePathValid() {
    when(oscarProperties.getProperty(IMAGE_PATH_PROPERTY_NAME))
        .thenAnswer((Answer<String>) invocation -> MOCK_PROPERTIES_FILE_PATH_RELATIVE);
    assertTrue(SmartEncounterUtil.isValidFilePath(VALID_FILE_PATH_RELATIVE));
  }

  @Test
  public void givenImagePath_whenIsNotValidAbsoluteFilePath_thenReturnAbsoluteFilePathInvalid() {
    when(oscarProperties.getProperty(IMAGE_PATH_PROPERTY_NAME))
        .thenAnswer((Answer<String>) invocation -> MOCK_PROPERTIES_FILE_PATH_ABSOLUTE);
    assertFalse(SmartEncounterUtil.isValidFilePath(INVALID_FILE_PATH_ABSOLUTE));
  }

  @Test
  public void givenImagePath_whenIsNotValidRelativeFilePath_thenReturnRelativeFilePathInvalid() {
    when(oscarProperties.getProperty(IMAGE_PATH_PROPERTY_NAME))
        .thenAnswer((Answer<String>) invocation -> MOCK_PROPERTIES_FILE_PATH_RELATIVE);
    assertFalse(SmartEncounterUtil.isValidFilePath(INVALID_FILE_PATH_RELATIVE));
  }

  @Test
  public void givenImagePath_whenIsNotValidFilePathAbsoluteWithPrefix_thenReturnFilePathAbsoluteWithPrefixInvalid() {
    when(oscarProperties.getProperty(IMAGE_PATH_PROPERTY_NAME))
        .thenAnswer((Answer<String>) invocation -> MOCK_PROPERTIES_FILE_PATH_ABSOLUTE);
    assertFalse(SmartEncounterUtil.isValidFilePath(INVALID_FILE_PATH_WITH_CORRECT_PREFIX_PATH_ABSOLUTE));
  }

  @Test
  public void givenImagePath_whenIsNotValidFilePathRelativeWithBacktracking_thenReturnFilePathRelativeWithBacktrackingInvalid() {
    when(oscarProperties.getProperty(IMAGE_PATH_PROPERTY_NAME))
        .thenAnswer((Answer<String>) invocation -> MOCK_PROPERTIES_FILE_PATH_RELATIVE);
    assertFalse(SmartEncounterUtil.isValidFilePath(INVALID_FILE_PATH_WITH_BACKTRACKING_RELATIVE));
  }

  @Test
  public void givenRequest_whenCreateErrorMessage_thenMessageEmpty() {
    String errorMessage = SmartEncounterUtil.createErrorMessage(request);
    assertTrue(errorMessage.isEmpty());
  }

  @Test
  public void givenRequest_whenInvalidMimeTypeAttribute_thenReturnInvalidMimeTypeErrorMessage() {
    when(request.getAttribute(SmartEncounterUtil.INVALID_MIME_TYPE)).thenReturn(true);
    String errorMessage = SmartEncounterUtil.createErrorMessage(request);
    assertTrue(errorMessage.contains(SmartEncounterUtil.INVALID_MIME_TYPE_MESSAGE));
  }

  @Test
  public void givenRequest_whenInvalidMimeTypeParameter_thenReturnInvalidMimeTypeErrorMessage() {
    when(request.getParameter(SmartEncounterUtil.INVALID_MIME_TYPE)).thenReturn("true");
    String errorMessage = SmartEncounterUtil.createErrorMessage(request);
    assertTrue(errorMessage.contains(SmartEncounterUtil.INVALID_MIME_TYPE_MESSAGE));
  }

  @Test
  public void givenRequest_whenInvalidMimeTypeAndInvalidFilePathAttributes_thenReturnInvalidFileTypeErrorMessage() {
    when(request.getAttribute(SmartEncounterUtil.INVALID_MIME_TYPE)).thenReturn(null);
    when(request.getAttribute(SmartEncounterUtil.INVALID_FILE_PATH)).thenReturn(true);
    String errorMessage = SmartEncounterUtil.createErrorMessage(request);
    assertTrue(errorMessage.contains(SmartEncounterUtil.INVALID_FILE_PATH_MESSAGE));
  }

  @Test
  public void givenRequest_whenInvalidMimeTypeAndInvalidFilePathParameters_thenReturnInvalidFilePathErrorMessage() {
    when(request.getParameter(SmartEncounterUtil.INVALID_MIME_TYPE)).thenReturn("false");
    when(request.getParameter(SmartEncounterUtil.INVALID_FILE_PATH)).thenReturn("true");
    String errorMessage = SmartEncounterUtil.createErrorMessage(request);
    assertTrue(errorMessage.contains(SmartEncounterUtil.INVALID_FILE_PATH_MESSAGE));
  }
}