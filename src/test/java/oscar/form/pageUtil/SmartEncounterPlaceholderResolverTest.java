/**
 * Copyright (c) 2023 WELL EMR Group Inc. This software is made available under the terms of the GNU
 * General Public License, Version 2, 1991 (GPLv2). License details are available via
 * "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.form.pageUtil;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mockStatic;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.oscarehr.common.model.Demographic;
import org.oscarehr.common.model.Provider;
import oscar.eform.APExecute;

@ExtendWith({MockitoExtension.class})
public class SmartEncounterPlaceholderResolverTest {

  private static final String CURRENT_USER_FIELD = "current_user";
  private static final String CURRENT_USER_VALUE = "CURRENT_USER";
  private static final String CURRENT_FORM_FIELD = "current_form_id";
  private static final String FALSE_FIELD = "should_be_false";
  private static final String EFORM_CURRENT_USER_FIELD = "eformAp.current_user";
  private static final String EFORM_CURRENT_FORM_FIELD = "eformAp.current_form_id";
  private static final String EFORM_NEW_CURRENT_FORM_FIELD = "eformAp.current_form_id";
  private static final String EFORM_NEW_CURRENT_FORM_FIELD_NO_SUBJECT = "current_form_id";
  private static final String PROVIDER_NO = "123456";
  private static final String CURRENT_USER_METHOD = "currentUser";

  @Test
  public void givenDemographicAndProvider_whenInvokeEFormApMethod_thenReturnCurrentUser()
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

    try (MockedStatic<SmartEncounterPlaceholderResolver> demoMocked = mockStatic(SmartEncounterPlaceholderResolver.class)) {
      Method currentUserMethod = APExecute.class.getDeclaredMethod(CURRENT_USER_METHOD, String.class, String.class);
      Demographic demographic = new Demographic();
      Provider provider = new Provider();
      provider.setProviderNo(PROVIDER_NO);
      demoMocked
          .when(() -> SmartEncounterPlaceholderResolver.invokeEformApMethod(currentUserMethod,
              EFORM_CURRENT_USER_FIELD, demographic, provider,0))
          .thenAnswer(invocation -> CURRENT_USER_VALUE);
      String currentUser = SmartEncounterPlaceholderResolver
          .invokeEformApMethod(currentUserMethod, EFORM_CURRENT_USER_FIELD, demographic, provider,0);
      assertEquals(currentUser, CURRENT_USER_VALUE);
    }
  }

  @Test
  public void givenDemographicAndProvider_whenInvokeEFormApMethod_thenReturnCurrentForm() throws Exception {
    Demographic demographic = new Demographic();
    Provider provider = new Provider();
    provider.setProviderNo(PROVIDER_NO);
    String result = SmartEncounterPlaceholderResolver.invokeEformApMethod
        (null, EFORM_CURRENT_FORM_FIELD, demographic, provider, 1);
    assertEquals(result, "1");
  }

  @Test
  public void givenDemographicAndProvider_whenInvokeEFormApMethod_thenReturnNewForm() throws Exception {
    Demographic demographic = new Demographic();
    Provider provider = new Provider();
    provider.setProviderNo(PROVIDER_NO);
    // new forms have an id of 0
    String result = SmartEncounterPlaceholderResolver.invokeEformApMethod
        (null, EFORM_NEW_CURRENT_FORM_FIELD, demographic, provider, 0);
    assertEquals(result, EFORM_NEW_CURRENT_FORM_FIELD_NO_SUBJECT + " (populates after save)");
  }

  @Test
  public void givenField_whenCurrentFormField_thenReturnTrue(){
    boolean isCurrentFormField = SmartEncounterPlaceholderResolver.isCurrentFormField(CURRENT_FORM_FIELD);
    assertTrue(isCurrentFormField);
  }

  @Test
  public void givenField_whenCurrentUserField_thenReturnFalse(){
    boolean isCurrentFormField = SmartEncounterPlaceholderResolver.isCurrentFormField(CURRENT_USER_FIELD);
    assertFalse(isCurrentFormField);
  }

  @Test
  public void givenField_whenNonExistsField_thenReturnFalse(){
    boolean isCurrentFormField = SmartEncounterPlaceholderResolver.isCurrentFormField(FALSE_FIELD);
    assertFalse(isCurrentFormField);
  }
}