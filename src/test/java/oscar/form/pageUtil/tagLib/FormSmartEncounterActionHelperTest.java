/**
 * Copyright (c) 2022 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.form.pageUtil.tagLib;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionRedirect;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import oscar.form.model.FormSmartEncounter;

public class FormSmartEncounterActionHelperTest {

  private final FormSmartEncounterActionHelper formSmartEncounterActionHelper = new FormSmartEncounterActionHelper();

  private final Integer FORM_ID = 1;
  private final Integer APPOINTMENT_NO = 1;
  private final Integer DEMOGRAPHIC_NO = 1;
  private final String ERROR_MESSAGE = "Exception encountered when generating PDF, please contact support";

  @Mock
  HttpServletResponse response;
  @Mock
  ActionMapping mapping;
  @Mock
  FormSmartEncounter formSmartEncounter;
  @Mock
  ByteArrayOutputStream byteArrayOutputStream;
  @Mock
  ServletOutputStream servletOutputStream;
  @Mock
  ActionForward forward;

  @Before
  public void setUp() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void setHeaderTest() {
    formSmartEncounterActionHelper.setResponseHeaders(response);

    verify(response).setHeader("Cache-Control", "max-age=0");
    verify(response).setDateHeader("Expires", 0);
    verify(response).setContentType("application/pdf");
    verify(response).setHeader("Content-disposition", "inline; filename=SmartEncounter.pdf");
  }

  @Test
  public void testGeneratePdfAndWriteToResponse_Success() throws IOException {
    when(response.getOutputStream()).thenReturn(servletOutputStream);

    val result = formSmartEncounterActionHelper.generatePdfAndWriteToResponse(response,
        mapping, formSmartEncounter, byteArrayOutputStream);

    verify(byteArrayOutputStream, times(1)).writeTo(servletOutputStream);
    assertNull(result);
  }

  @Test
  public void testGeneratePdfAndWriteToResponse_Exception() throws IOException {
    when(mapping.findForward("success")).thenReturn(forward);
    when(response.getOutputStream()).thenReturn(servletOutputStream);
    doThrow(new IOException()).when(byteArrayOutputStream).writeTo(servletOutputStream);

    when(formSmartEncounter.getDemographicNo()).thenReturn(DEMOGRAPHIC_NO);
    when(formSmartEncounter.getAppointmentNo()).thenReturn(APPOINTMENT_NO);
    when(formSmartEncounter.getId()).thenReturn(FORM_ID);

    val result = formSmartEncounterActionHelper.generatePdfAndWriteToResponse(response,
        mapping, formSmartEncounter, byteArrayOutputStream);

    val parameters = splitParameters(result.getParameterString());

    verify(byteArrayOutputStream, times(1)).writeTo(servletOutputStream);
    Assert.assertNotNull(result);
    Assert.assertEquals(String.valueOf(FORM_ID), parameters.get("formId"));
    Assert.assertEquals(String.valueOf(DEMOGRAPHIC_NO), parameters.get("demographic_no"));
    Assert.assertEquals(String.valueOf(APPOINTMENT_NO), parameters.get("appointment_no"));
    Assert.assertEquals("Exception encountered when generating PDF, please contact support", parameters.get("error_message"));
  }

  private HashMap<String, String> splitParameters(
      final String parameterString
  ) throws UnsupportedEncodingException {
    HashMap<String, String> parameters = new HashMap<>();
    String[] paramPairs = parameterString.split("&");

    for (String paramPair : paramPairs) {
      String[] keyValue = paramPair.split("=");

      if (keyValue.length == 2) {
        String key = keyValue[0];
        String value = keyValue[1];
        value = URLDecoder.decode(value, String.valueOf(StandardCharsets.UTF_8));
        parameters.put(key, value);
      }
    }

    return parameters;
  }
}