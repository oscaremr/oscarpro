/**
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.form.pageUtil;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import ca.oscarpro.common.http.OscarProHttpService;
import com.lowagie.text.DocumentException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.val;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.DbConnectionFilter;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;
import oscar.OscarDocumentCreator;
import oscar.OscarProperties;
import oscar.form.dao.FormSmartEncounterDao;
import oscar.form.model.FormSmartEncounter;
import oscar.util.ConcatPDF;

@ExtendWith(MockitoExtension.class)
public class FormSmartEncounterActionTest {

  private MockedStatic<DbConnectionFilter> dbConnectionFilterMockedStatic;
  private MockedStatic<LoggedInInfo> loggedInInfoMockedStatic;
  private MockedStatic<SpringUtils> springUtilsMockedStatic;

  private MockedConstruction<OscarDocumentCreator> mockControllerOscarDocumentCreator;
  private MockedConstruction<ITextRenderer> mockControllerITextRenderer;

  private static MockedStatic<OscarProperties> oscarPropertiesStatic;
  private static MockedStatic<ConcatPDF> concatPDFMockedStatic;

  @Mock
  private ActionMapping actionMapping;
  @Mock
  private HttpServletRequest request;
  @Mock
  private HttpServletResponse response;
  @Mock
  private SecurityInfoManager securityInfoManager;
  @Mock
  private Connection connection;
  @Mock
  private FormSmartEncounterDao formSmartEncounterDao;
  @Mock
  private FormSmartEncounter formSmartEncounter;
  @Mock
  HttpEntity proEntity;
  @Mock
  OscarProHttpService oscarProHttpService;
  @Mock
  HttpResponse proResponse;
  @Mock
  ServletOutputStream servletOutputStream;
  @Mock
  StatusLine proStatusLine;

  @BeforeAll
  public static void beforeAll() {
    oscarPropertiesStatic = mockStatic(OscarProperties.class);
    oscarPropertiesStatic
        .when(OscarProperties::getOscarProApiUrl)
        .thenReturn("http://localhost:8080/kaiemr/");

    concatPDFMockedStatic = mockStatic(ConcatPDF.class);
    concatPDFMockedStatic
        .when(() -> ConcatPDF.concat(any(ArrayList.class), any(ByteArrayOutputStream.class)))
        .thenAnswer(invocation -> null);
  }

  @BeforeEach
  public void before() throws Exception {
    createStaticMocks();
    createNewMocks();
    when(securityInfoManager.hasPrivilege(
        any(LoggedInInfo.class), eq("_demographic"), eq("w"), ArgumentMatchers.<String>isNull())).thenReturn(true);
    when(formSmartEncounterDao.find(anyInt())).thenReturn(formSmartEncounter);
  }

  @AfterEach
  public void close(){
    dbConnectionFilterMockedStatic.close();
    loggedInInfoMockedStatic.close();
    springUtilsMockedStatic.close();
    mockControllerOscarDocumentCreator.close();
    mockControllerITextRenderer.close();
  }

  @AfterAll
  public static void afterAll() {
    oscarPropertiesStatic.close();
    concatPDFMockedStatic.close();
  }

  @Test
  public void givenSmartEncounterActionFormData_whenFormSmartEncounterAction_thenPrintPdf() {
    FormSmartEncounterAction formSmartEncounterAction = spy(new FormSmartEncounterAction());
    val actionForm = createSmartEncounterActionFormPrint();
    formSmartEncounterAction.print(actionMapping, actionForm, request, response);
    verify(securityInfoManager, times(1)).hasPrivilege(
        any(LoggedInInfo.class), eq("_demographic"), eq("w"), ArgumentMatchers.<String>isNull());
    verify(formSmartEncounterDao, times(1)).find(anyInt());
    verify(formSmartEncounterDao, times(0)).saveEntity(any(FormSmartEncounter.class));
    verify(formSmartEncounterAction, times(1))
        .printForm(eq(actionMapping), eq(request), eq(response), any(FormSmartEncounter.class));
  }

  @Test
  public void givenSmartEncounterActionFormData_whenFormSmartEncounterAction_thenSaveAndPrintPdf() {
    when(formSmartEncounterDao.saveEntity(any(FormSmartEncounter.class))).thenReturn(mock(FormSmartEncounter.class));
    when(actionMapping.findForward(eq("print"))).thenReturn(new ActionForward());
    when(request.getParameterValues(eq("faxRecipient"))).thenReturn(new String[]{"1231231231"});
    FormSmartEncounterAction formSmartEncounterAction = spy(new FormSmartEncounterAction());
    val actionForm = createSmartEncounterActionFormSaveAndPrint();
    formSmartEncounterAction.saveAndPrint(actionMapping, actionForm, request, response);
    verify(securityInfoManager, times(1)).hasPrivilege(
        any(LoggedInInfo.class), eq("_demographic"), eq("w"), ArgumentMatchers.<String>isNull());
    verify(formSmartEncounterAction,
        times(1)).saveForm(any(SmartEncounterActionForm.class), any(HttpServletRequest.class));
    // 2 saveEntity cals are needed: one to save the form and another to update the form after an
    // id is generated. The generated id is used for current_form_id placeholders.
    verify(formSmartEncounterDao, times(2)).saveEntity(any(FormSmartEncounter.class));
    verify(formSmartEncounterAction, times(1))
        .findForward(eq(actionMapping), eq("print"), any(FormSmartEncounter.class), eq(request));
  }

  @Test
  public void givenSmartEncounterActionFormData_whenPrint_thenPrintPdfWithAttachments()
      throws DocumentException, IOException {
    FormSmartEncounterAction formSmartEncounterAction = spy(new FormSmartEncounterAction());
    val actionForm = createSmartEncounterActionFormPrint();

    when(formSmartEncounter.getAttachments()).thenReturn("attachment");
    when(formSmartEncounter.getHtmlTextWithValuesFilled()).thenReturn("");
    when(proEntity.getContent()).thenReturn(new ByteArrayInputStream("[\"filepath\"]".getBytes()));
    when(proStatusLine.getStatusCode()).thenReturn(HttpStatus.SC_OK);
    when(proResponse.getEntity()).thenReturn(proEntity);
    when(proResponse.getStatusLine()).thenReturn(proStatusLine);
    when(oscarProHttpService.makeLoggedInPostRequestToPro(any(), any(), any()))
        .thenReturn(proResponse);
    when(response.getOutputStream()).thenReturn(servletOutputStream);
    doNothing().when(formSmartEncounterAction).generatePdfFromHtml(any(String.class), any(String.class));

    formSmartEncounterAction.print(actionMapping, actionForm, request, response);

    verify(securityInfoManager, times(1)).hasPrivilege(
        any(LoggedInInfo.class), eq("_demographic"), eq("w"), ArgumentMatchers.<String>isNull());
    verify(formSmartEncounterDao, times(1)).find(anyInt());
    verify(formSmartEncounterDao, times(0)).saveEntity(any(FormSmartEncounter.class));
    verify(formSmartEncounterAction, times(1))
        .printFormWithAttachments(eq(actionMapping), eq(request), eq(response), any(FormSmartEncounter.class));
    verify(formSmartEncounterAction, times(0))
        .printForm(eq(actionMapping), eq(request), eq(response), any(FormSmartEncounter.class));
  }

  private ActionForm createSmartEncounterActionFormPrint() {
    val smartEncounterActionForm = mock(SmartEncounterActionForm.class);
    when(smartEncounterActionForm.getFormId()).thenReturn(1);
    return smartEncounterActionForm;
  }

  private ActionForm createSmartEncounterActionFormSaveAndPrint() {
    val smartEncounterActionForm = mock(SmartEncounterActionForm.class);
    when(smartEncounterActionForm.getFormId()).thenReturn(1);
    when(smartEncounterActionForm.getImages()).thenReturn(new String[]{});
    when(smartEncounterActionForm.toFormSmartEncounter()).thenReturn(mock(FormSmartEncounter.class));
    return smartEncounterActionForm;
  }

  private void createStaticMocks() throws SQLException {
    springUtilsMockedStatic = mockStatic(SpringUtils.class);
    loggedInInfoMockedStatic = mockStatic(LoggedInInfo.class);
    dbConnectionFilterMockedStatic = mockStatic(DbConnectionFilter.class);
    when(SpringUtils.getBean(SecurityInfoManager.class)).thenAnswer(
        (Answer<SecurityInfoManager>) invocation -> securityInfoManager);
    when(SpringUtils.getBean(FormSmartEncounterDao.class)).thenAnswer(
        (Answer<FormSmartEncounterDao>) invocation -> formSmartEncounterDao);
    when(LoggedInInfo.getLoggedInInfoFromSession(any(HttpServletRequest.class))).thenAnswer(
        (Answer<LoggedInInfo>) invocation -> new LoggedInInfo());
    when(DbConnectionFilter.getThreadLocalDbConnection()).thenAnswer(
        (Answer<Connection>) invocation -> connection);
    when(SpringUtils.getBean(OscarProHttpService.class)).thenAnswer(
        (Answer<OscarProHttpService>) invocation -> oscarProHttpService);
  }

  private void createNewMocks() {
    mockControllerOscarDocumentCreator = mockConstruction(OscarDocumentCreator.class);
    mockControllerITextRenderer = mockConstruction(ITextRenderer.class);
  }
}