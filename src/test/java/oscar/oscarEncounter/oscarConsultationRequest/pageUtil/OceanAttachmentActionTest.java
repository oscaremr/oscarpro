/*
 * Copyright (c) 2024 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.oscarEncounter.oscarConsultationRequest.pageUtil;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.oscarehr.common.dao.EReferAttachmentDao;
import org.oscarehr.common.model.EReferAttachment;
import org.oscarehr.common.model.EReferAttachmentData;

public class OceanAttachmentActionTest {

  private static final String DEMOGRAPHIC_NUMBER_PARAMETER = "demographicNumber";

  @Mock EReferAttachmentDao eReferAttachmentDao;
  @Mock HttpServletRequest request;
  @Mock HttpServletResponse response;
  @Mock ActionMapping mapping;
  @Mock ActionForm form;
  @Mock PrintWriter writer;

  private AutoCloseable closeable;

  private OceanAttachmentAction oceanAttachmentAction;

  @BeforeEach
  public void before() throws IOException {
    closeable = MockitoAnnotations.openMocks(this);
    when(response.getWriter()).thenReturn(writer);
    oceanAttachmentAction = new OceanAttachmentAction(eReferAttachmentDao);
  }

  @AfterEach
  public void after() throws Exception {
    closeable.close();
  }

  @Test
  public void givenValidDate_whenExecute_thenReturnCorrectResponse() throws Exception {
    when(request.getParameter(DEMOGRAPHIC_NUMBER_PARAMETER)).thenReturn("1");
    when(request.getParameter("documents")).thenReturn("D1|H2|L2|XYZ44");
    when(request.getParameter("type")).thenReturn("EREFERRAL");
    when(eReferAttachmentDao.saveEntity(any(EReferAttachment.class))).thenReturn(
        createTestEReferAttachment());

    oceanAttachmentAction.execute(mapping, form, request, response);

    ArgumentCaptor<EReferAttachment> argumentCaptor = ArgumentCaptor.forClass(
        EReferAttachment.class);
    verify(eReferAttachmentDao, times(1)).saveEntity(
        argumentCaptor.capture());
    EReferAttachment actualEReferAttachment = argumentCaptor.getValue();
    assertEquals(1, actualEReferAttachment.getDemographicNo());
    assertEquals("EREFERRAL", actualEReferAttachment.getType());
    assertEquals(4, actualEReferAttachment.getAttachments().size());

    List<EReferAttachmentData> actualAttachments = actualEReferAttachment.getAttachments();
    assertAttachment(1, "D", actualAttachments.get(0));
    assertAttachment(2, "H", actualAttachments.get(1));
    assertAttachment(2, "L", actualAttachments.get(2));
    assertAttachment(44, "XYZ", actualAttachments.get(3));

    ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);
    verify(writer).write(stringArgumentCaptor.capture());

    assertResponse("1", HttpServletResponse.SC_OK);
  }

  @Test
  public void givenInvalidDemographicNumber_whenExecute_thenErrorResponse() throws Exception {
    when(request.getParameter(DEMOGRAPHIC_NUMBER_PARAMETER)).thenReturn("invalidDemographicNumber");
    oceanAttachmentAction.execute(mapping, form, request, response);
    assertEReferAttachmentNotSaved();
    assertResponse("Invalid demographic number.", HttpServletResponse.SC_BAD_REQUEST);
  }

  @Test
  public void givenNullDemographicNumber_whenExecute_thenErrorResponse() throws Exception {
    when(request.getParameter(DEMOGRAPHIC_NUMBER_PARAMETER)).thenReturn(null);
    oceanAttachmentAction.execute(mapping, form, request, response);
    assertEReferAttachmentNotSaved();
    assertResponse("Invalid demographic number.", HttpServletResponse.SC_BAD_REQUEST);
  }

  @Test
  public void givenInvalidDocuments_whenExecute_thenErrorResponse() throws Exception {
    when(request.getParameter(DEMOGRAPHIC_NUMBER_PARAMETER)).thenReturn("1");
    when(request.getParameter("documents")).thenReturn("");
    oceanAttachmentAction.execute(mapping, form, request, response);
    assertEReferAttachmentNotSaved();
    assertResponse("Documents were not provided.", HttpServletResponse.SC_BAD_REQUEST);
  }

  @Test
  public void givenNullDocuments_whenExecute_thenErrorResponse() throws Exception {
    when(request.getParameter(DEMOGRAPHIC_NUMBER_PARAMETER)).thenReturn("1");
    when(request.getParameter("documents")).thenReturn(null);
    oceanAttachmentAction.execute(mapping, form, request, response);
    assertEReferAttachmentNotSaved();
    assertResponse("Documents were not provided.", HttpServletResponse.SC_BAD_REQUEST);
  }

  @Test
  public void givenInvalidAttachmentType_whenExecute_thenErrorResponse() throws Exception {
    when(request.getParameter(DEMOGRAPHIC_NUMBER_PARAMETER)).thenReturn("1");
    when(request.getParameter("documents")).thenReturn("doc1|doc2");
    when(request.getParameter("type")).thenReturn("invalidAttachmentType");
    oceanAttachmentAction.execute(mapping, form, request, response);
    assertEReferAttachmentNotSaved();
    assertResponse("Invalid attachment type.", HttpServletResponse.SC_BAD_REQUEST);
  }

  @Test
  public void givenNullAttachmentType_whenExecute_thenErrorResponse() throws Exception {
    when(request.getParameter(DEMOGRAPHIC_NUMBER_PARAMETER)).thenReturn("1");
    when(request.getParameter("documents")).thenReturn("doc1|doc2");
    when(request.getParameter("type")).thenReturn(null);
    oceanAttachmentAction.execute(mapping, form, request, response);
    assertEReferAttachmentNotSaved();
    assertResponse("Invalid attachment type.", HttpServletResponse.SC_BAD_REQUEST);
  }

  private EReferAttachment createTestEReferAttachment() {
    EReferAttachment eReferAttachment = new EReferAttachment();
    eReferAttachment.setId(1);
    eReferAttachment.setDemographicNo(1);
    eReferAttachment.setType("EREFERRAL");
    return eReferAttachment;
  }

  private void assertEReferAttachmentNotSaved() {
    verify(eReferAttachmentDao, times(0)).saveEntity(
        any(EReferAttachment.class));
  }

  private void assertResponse(final String expectedResponse, final int expectedStatus) {
    ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);
    verify(writer).write(stringArgumentCaptor.capture());
    String actualResponse = stringArgumentCaptor.getValue();
    assertEquals(expectedResponse, actualResponse);
    verify(response, times(1)).setStatus(expectedStatus);
  }

  private void assertAttachment(final int expectedLabId, final String expectedLabType,
      final EReferAttachmentData actualAttachment) {
    assertEquals(expectedLabId, actualAttachment.getLabId());
    assertEquals(expectedLabType, actualAttachment.getLabType());
  }
}