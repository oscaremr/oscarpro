package oscar.oscarEncounter.oscarConsultationRequest.pageUtil;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Collections;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lombok.val;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.oscarehr.common.dao.ConsultationRequestArchiveDao;
import org.oscarehr.common.dao.ConsultationRequestDao;
import org.oscarehr.common.dao.ConsultationRequestExtArchiveDao;
import org.oscarehr.common.dao.ConsultationRequestExtDao;
import org.oscarehr.common.dao.ProfessionalSpecialistDao;
import org.oscarehr.common.dao.SystemPreferencesDao;
import org.oscarehr.common.model.ConsultationRequest;
import org.oscarehr.common.model.ConsultationRequestArchive;
import org.oscarehr.common.model.ProfessionalSpecialist;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;
import oscar.log.LogService;

@ExtendWith({MockitoExtension.class})
public class EctConsultationFormRequestActionTest {

  private MockedStatic<LoggedInInfo> loggedInInfoMockedStatic;
  @Mock LogService logService;
  @Mock SecurityInfoManager securityInfoManager;
  @Mock ConsultationRequestDao consultationRequestDao;
  @Mock SystemPreferencesDao systemPreferencesDao;
  @Mock ConsultationRequestArchiveDao consultationRequestArchiveDao;
  @Mock ConsultationRequestExtDao consultationRequestExtDao;
  @Mock ConsultationRequestExtArchiveDao consultationRequestExtArchiveDao;
  @Mock ProfessionalSpecialistDao professionalSpecialistDao;

  @Mock HttpServletRequest request;
  @Mock HttpSession session;
  @Mock HttpServletResponse response;
  @Mock ActionMapping mapping;
  @Mock EctConsultationFormRequestForm form;
  @Mock ActionForward forward;

  private AutoCloseable closeable;

  private EctConsultationFormRequestAction ectConsultationFormRequestAction;

  @BeforeEach
  public void before() throws IOException {
    closeable = MockitoAnnotations.openMocks(this);
    ectConsultationFormRequestAction =
        new EctConsultationFormRequestAction(
            logService,
            securityInfoManager,
            consultationRequestDao,
            systemPreferencesDao,
            consultationRequestArchiveDao,
            consultationRequestExtDao,
            consultationRequestExtArchiveDao,
            professionalSpecialistDao);

    when(securityInfoManager.hasPrivilege(any(LoggedInInfo.class), eq("_con"), eq("w"), eq(null)))
        .thenReturn(true);
    mockLoggedInProvider();
    when(request.getParameterNames()).thenReturn(Collections.emptyEnumeration());
    when(systemPreferencesDao.isAttachmentManagerConsultationEnabled()).thenReturn(true);

    initConsultationFormRequest();

    val specialist = new ProfessionalSpecialist();
    specialist.setId(Integer.valueOf(form.getSpecialist()));
    when(professionalSpecialistDao.find(eq(1))).thenReturn(specialist);
  }

  @AfterEach
  public void after() throws Exception {
    closeable.close();
    loggedInInfoMockedStatic.close();
  }

  @Test
  public void givenValidNewConsult_whenExecute_thenSaveConsultationRequest()
      throws ServletException, IOException {

    // given valid consultation form request
    when(request.getParameter("newSignature")).thenReturn("false");
    when(mapping.findForward("success")).thenReturn(forward);

    form.setSubmission("Submit Consultation Request");

    // when execute method is called
    ectConsultationFormRequestAction.execute(mapping, form, request, response);

    // then daos are called and record saved
    ArgumentCaptor<ConsultationRequest> consultArgumentCaptor =
        ArgumentCaptor.forClass(ConsultationRequest.class);
    verify(consultationRequestDao, times(1)).persist(consultArgumentCaptor.capture());
    verify(consultationRequestDao, times(1)).merge(consultArgumentCaptor.capture());
    val savedConsult = consultArgumentCaptor.getValue();
    validateConsultationFormRequest(savedConsult, form);
  }

  @Test
  public void givenValidExistingConsult_whenExecute_thenUpdateConsultationRequest()
      throws ServletException, IOException {

    // given valid consultation form request
    when(request.getParameter("newSignature")).thenReturn("false");
    when(request.getParameter("specialProblem")).thenReturn("");
    when(session.getAttribute(eq("examination"))).thenReturn(null);
    when(mapping.findForward("success")).thenReturn(forward);

    when(request.getSession()).thenReturn(session);

    form.setRequestId("123");
    val mockExistingConsultationRequest = new ConsultationRequest();
    mockExistingConsultationRequest.setId(123);
    when(consultationRequestDao.find(eq(123))).thenReturn(mockExistingConsultationRequest);
    form.setSubmission("Update Consultation Request");

    // when execute method is called
    ectConsultationFormRequestAction.execute(mapping, form, request, response);

    // then daos are called and record saved
    ArgumentCaptor<ConsultationRequest> consultArgumentCaptor =
        ArgumentCaptor.forClass(ConsultationRequest.class);
    verify(consultationRequestDao, times(1)).merge(consultArgumentCaptor.capture());
    verify(consultationRequestArchiveDao, times(1))
        .saveEntity(any(ConsultationRequestArchive.class));
    val savedConsult = consultArgumentCaptor.getValue();
    validateConsultationFormRequest(savedConsult, form);
  }

  private void initConsultationFormRequest() {
    form = new EctConsultationFormRequestForm();
    form.setAllergies("allergies");
    form.setAppointmentDate("");
    form.setAppointmentHour("");
    form.setAppointmentMinute("");
    form.setAppointmentNotes("Appt notes");
    form.setAppointmentPm("AM");
    form.setClinicalInformation("Clinic info");
    form.setConcurrentProblems("Sig problems");
    form.setCurrentMedications("Current meds\r\n");
    form.setDemographicNo("9");
    form.setDocuments("");
    form.setPatientWillBook("0");
    form.setProviderNo("999998");
    form.setSpecialist("1");
    form.setReasonForConsultation("Reason for consult");
    form.setReferalDate("2024-05-22");
    form.setRequestId("null");
    form.setSendTo("test");
    form.setService("56");
    form.setStatus("1");
    form.setUrgency("2");
    form.setSiteName("");
    form.setSignatureImg(null);
    form.setFollowUpDate("2024-05-24");
    form.seteReferral(false);
    form.seteReferralService("");
    form.setLetterheadName("999998");
    form.setLetterheadAddress("Hamilton  Hamilton   Ontario  L0R 4K3");
    form.setLetterheadPhone("555-555-5555");
    form.setLetterheadFax("5555555555");
    form.setAttachments(
        "[{\"id\":\"4\",\"name\":\"ad\",\"type\":\"Document\",\"params\":{},\"previewUrl\":\"/kaiemr/api/document/getPagePreview/52?page=1\",\"date\":null,\"supported\":true,\"paramsAsJson\":\"{}\",\"printableAttachments\":[]}]");
  }

  private void validateConsultationFormRequest(
      final ConsultationRequest consultationRequest, final EctConsultationFormRequestForm form) {
    assertEquals(form.getAllergies(), consultationRequest.getAllergies());
    assertEquals(form.getConcurrentProblems(), consultationRequest.getConcurrentProblems());
    assertEquals(form.getCurrentMedications(), consultationRequest.getCurrentMeds());
    assertEquals(form.getDemographicNo(), String.valueOf(consultationRequest.getDemographicId()));
    assertEquals(form.getProviderNo(), consultationRequest.getProviderNo());
    assertEquals(form.getSpecialist(), String.valueOf(consultationRequest.getSpecialistId()));
    assertEquals(form.getReasonForConsultation(), consultationRequest.getReasonForReferral());
    assertEquals(form.getSendTo(), consultationRequest.getSendTo());
    assertEquals(form.getService(), String.valueOf(consultationRequest.getServiceId()));
    assertEquals(form.getStatus(), consultationRequest.getStatus());
    assertEquals(form.getUrgency(), consultationRequest.getUrgency());
    assertEquals(form.getSiteName(), consultationRequest.getSiteName());
    assertEquals(form.getLetterheadName(), consultationRequest.getLetterheadName());
    assertEquals(form.getLetterheadAddress(), consultationRequest.getLetterheadAddress());
    assertEquals(form.getLetterheadPhone(), consultationRequest.getLetterheadPhone());
    assertEquals(form.getLetterheadFax(), consultationRequest.getLetterheadFax());
    assertEquals(form.getAttachments(), consultationRequest.getAttachments());
  }

  private void mockLoggedInProvider() {
    loggedInInfoMockedStatic = Mockito.mockStatic(LoggedInInfo.class);
    final LoggedInInfo loggedInInfo = mock(LoggedInInfo.class);
    when(LoggedInInfo.getLoggedInInfoFromSession(request))
        .thenAnswer(
            new Answer<LoggedInInfo>() {
              @Override
              public LoggedInInfo answer(InvocationOnMock invocation) {
                return loggedInInfo;
              }
            });
  }
}
