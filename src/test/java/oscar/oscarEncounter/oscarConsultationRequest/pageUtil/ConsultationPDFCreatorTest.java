/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */
package oscar.oscarEncounter.oscarConsultationRequest.pageUtil;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import java.util.ResourceBundle;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.oscarehr.PMmodule.dao.ProviderDao;
import org.oscarehr.common.dao.PropertyDao;
import org.oscarehr.common.model.Provider;
import org.oscarehr.util.LoggedInInfo;
import org.oscarehr.util.SpringUtils;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class ConsultationPDFCreatorTest {

  private static final String LOGGED_IN_PROVIDER_NUMBER = "111111";
  private static final String LOGGED_IN_PROVIDER_PRACTITIONER_NUMBER = "222222";
  private static final String RESOURCE_STRING = "Test Resource";

  @Mock
  private ResourceBundle resourceBundle;

  @Mock
  private PropertyDao propertyDao;

  @Mock
  private ProviderDao providerDao;

  private ConsultationPDFCreator consultationPDFCreator;

  private MockedStatic<SpringUtils> springUtils;

  @BeforeEach
  public void before() {
    springUtils = mockStatic(SpringUtils.class);
    springUtils.when(() -> SpringUtils.getBean(PropertyDao.class)).thenReturn(propertyDao);
    springUtils.when(() -> SpringUtils.getBean(ProviderDao.class)).thenReturn(providerDao);

    consultationPDFCreator = new ConsultationPDFCreator();
    ReflectionTestUtils.setField(consultationPDFCreator, "oscarR", resourceBundle);
    when(resourceBundle.getString(anyString())).thenReturn(RESOURCE_STRING);
  }

  @AfterEach
  public void after() {
    springUtils.close();
  }

  @Test
  public void givenProviderWithPractitionerNumber_whenAddRequestingPhysician_thenPDFHasPractitionerNumber() {
    ConsultationPDFCreator consultationPDFCreatorSpy = spy(consultationPDFCreator);
    LoggedInInfo loggedInInfo = mock(LoggedInInfo.class);
    PdfPTable infoTable = mock(PdfPTable.class);
    PdfPCell cell = mock(PdfPCell.class);
    when(loggedInInfo.getLoggedInProvider()).thenReturn(createTestProvider(true));
    when(consultationPDFCreatorSpy.getResource(anyString())).thenReturn(RESOURCE_STRING);
    consultationPDFCreatorSpy.addRequestingPhysician(loggedInInfo, infoTable, cell, "provName");
    verify(infoTable, times(1)).addCell(any(PdfPCell.class));
  }

  @Test
  public void givenProviderWithNoPractitionerNumber_whenAddRequestingPhysician_thenPDFHasNoPractitionerNumber() {
    ConsultationPDFCreator consultationPDFCreatorSpy = spy(consultationPDFCreator);
    LoggedInInfo loggedInInfo = mock(LoggedInInfo.class);
    PdfPTable infoTable = mock(PdfPTable.class);
    when(loggedInInfo.getLoggedInProvider()).thenReturn(createTestProvider(false));
    when(consultationPDFCreatorSpy.getResource(anyString())).thenReturn(RESOURCE_STRING);
    consultationPDFCreatorSpy.addRequestingPhysician(loggedInInfo, infoTable, null, "provName");
    verify(infoTable, never()).addCell(any(PdfPCell.class));
  }

  private Provider createTestProvider(boolean hasPractitionerNumber) {
    Provider provider = new Provider();
    provider.setProviderNo(LOGGED_IN_PROVIDER_NUMBER);
    if (hasPractitionerNumber) {
      provider.setPractitionerNo(LOGGED_IN_PROVIDER_PRACTITIONER_NUMBER);
    }
    return provider;
  }
}
