package oscar.oscarEncounter.oscarConsultationRequest.pageUtil;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

import ca.oscarpro.common.http.OscarProHttpService;
import ca.oscarpro.service.OscarProConnectorService.OscarProConnectorServiceException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import javax.servlet.ServletOutputStream;
import lombok.val;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.oscarehr.common.dao.ConsultationRequestDao;
import org.oscarehr.common.model.ConsultationRequest;
import org.oscarehr.fax.util.PdfCoverPageCreator;
import org.oscarehr.managers.SecurityInfoManager;
import org.oscarehr.util.LoggedInInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

import java.io.IOException;
import oscar.OscarProperties;
import oscar.util.ConcatPDF;

@ExtendWith(MockitoExtension.class)
public class EctConsultationFormRequestPrintAction2Test {

  private static MockedStatic<LoggedInInfo> loggedInInfoMockedStatic;
  private static MockedStatic<OscarProperties> oscarPropertiesStatic;
  private static MockedStatic<ConcatPDF> concatPDFMockedStatic;
  @Mock HttpServletRequest request;
  @Mock HttpServletResponse response;
  @Mock HttpResponse proResponse;
  @Mock HttpEntity proEntity;
  @Mock ActionMapping mapping;

  @Mock SecurityInfoManager securityInfoManager;
  @Mock PdfCoverPageCreator pdfCoverPageCreator;
  @Mock ConsultationRequestDao consultationRequestDao;
  @Mock OscarProHttpService oscarProHttpService;
  @Mock ServletOutputStream servletOutputStream;

  private AutoCloseable closeable;
  private EctConsultationFormRequestPrintAction2 action;

  @BeforeAll
  public static void beforeAll() {
    loggedInInfoMockedStatic = mockStatic(LoggedInInfo.class);

    oscarPropertiesStatic = mockStatic(OscarProperties.class);
    oscarPropertiesStatic
        .when(OscarProperties::getOscarProBaseUrl)
        .thenReturn("http://localhost:8080/kaiemr/");

    concatPDFMockedStatic = mockStatic(ConcatPDF.class);
    concatPDFMockedStatic
        .when(() -> ConcatPDF.concat(any(ArrayList.class), any(ByteArrayOutputStream.class)))
        .thenAnswer(invocation -> null);
  }

  @BeforeEach
  public void before() throws IOException {
    closeable = MockitoAnnotations.openMocks(this);

    action = new EctConsultationFormRequestPrintAction2(
        securityInfoManager,
        pdfCoverPageCreator,
        consultationRequestDao,
        oscarProHttpService
    );
    when(securityInfoManager.hasPrivilege(any(LoggedInInfo.class), eq("_con"), eq("r"), eq(null)))
        .thenReturn(true);
    mockLoggedInProvider();

    when(request.getAttribute(eq("reqId"))).thenReturn("1");
    when(request.getParameter(eq("reqId"))).thenReturn("1");
    when(request.getParameter(eq("demographicNo"))).thenReturn("1");
    when(proEntity.getContent()).thenReturn(new ByteArrayInputStream("pdf".getBytes()));
    when(proResponse.getEntity()).thenReturn(proEntity);
    when(oscarProHttpService.makeLoggedInPostRequestToPro(
            anyString(), anyString(), any(HttpServletRequest.class)))
        .thenReturn(proResponse);
    when(response.getOutputStream()).thenReturn(servletOutputStream);
    val consultationRequest = new ConsultationRequest();
    when(consultationRequestDao.find(anyInt())).thenReturn(consultationRequest);
  }

  @AfterEach
  public void after() throws Exception {
    closeable.close();
  }

  @AfterAll
  public static void afterAll() {
    loggedInInfoMockedStatic.close();
    oscarPropertiesStatic.close();
    concatPDFMockedStatic.close();
  }

  @Test
  public void givenConsultNoCoverPage_whenExecute_thenCreatePdf()
      throws OscarProConnectorServiceException, IOException {
    when(request.getParameter(eq("coverpage"))).thenReturn("false");

    ActionForward result = action.execute(mapping, null, request, response);

    assertNull(result);
    ArgumentCaptor<ConsultationRequest> consultArgumentCaptor =
        ArgumentCaptor.forClass(ConsultationRequest.class);
    verify(consultationRequestDao, times(1)).find(consultArgumentCaptor.capture());
    verify(consultationRequestDao, times(1)).merge(consultArgumentCaptor.capture());
  }

  @Test
  public void givenConsultCoverPage_whenExecute_thenCreatePdf()
      throws OscarProConnectorServiceException {
    when(request.getParameter(eq("coverpage"))).thenReturn("true");
    when(request.getParameter(eq("note"))).thenReturn("test note");
    when(request.getParameter(eq("specialist"))).thenReturn("specialist name");

    when(pdfCoverPageCreator.createStandardCoverPage(anyString(), anyString()))
        .thenReturn("cover page".getBytes());


    ActionForward result = action.execute(mapping, null, request, response);

    assertNull(result);
    ArgumentCaptor<ConsultationRequest> consultArgumentCaptor =
        ArgumentCaptor.forClass(ConsultationRequest.class);
    verify(consultationRequestDao, times(1)).find(consultArgumentCaptor.capture());
    verify(consultationRequestDao, times(1)).merge(consultArgumentCaptor.capture());


  }

  private void mockLoggedInProvider() {
    final LoggedInInfo loggedInInfo = mock(LoggedInInfo.class);
    when(LoggedInInfo.getLoggedInInfoFromSession(request)).thenReturn(loggedInInfo);
  }
}