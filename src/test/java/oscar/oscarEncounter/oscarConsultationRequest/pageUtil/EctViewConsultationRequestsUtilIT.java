/*
 * Copyright (c) 2023 WELL EMR Group Inc.
 * This software is made available under the terms of the
 * GNU General Public License, Version 2, 1991 (GPLv2).
 * License details are available via "gnu.org/licenses/gpl-2.0.html".
 */

package oscar.oscarEncounter.oscarConsultationRequest.pageUtil;

import lombok.val;
import lombok.var;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.oscarehr.common.dao.ConsultationRequestDao;
import org.oscarehr.common.dao.DaoTestFixtures;
import org.oscarehr.common.dao.utils.EntityDataGenerator;
import org.oscarehr.common.model.ConsultationRequest;
import org.oscarehr.util.SpringUtils;

public class EctViewConsultationRequestsUtilIT extends DaoTestFixtures {

  protected final ConsultationRequestDao consultationRequestDao =
      SpringUtils.getBean(ConsultationRequestDao.class);

  @BeforeEach
  public void before() throws Exception {
    super.beforeDemographic();
    super.beforeProvider();
    super.beforeConsultations();
  }

  @Test
  public void testReasonForReferral() throws Exception {
    val demographicId = 2;
    var consultationRequest = new ConsultationRequest();
    EntityDataGenerator.generateTestDataForModelClass(consultationRequest);
    consultationRequest.setDemographicId(demographicId);
    consultationRequest.setReasonForReferral("reasonForReferralTest");
    consultationRequest.setSupersededById(null);
    consultationRequest = consultationRequestDao.saveEntity(consultationRequest);
    EctViewConsultationRequestsUtil util = new EctViewConsultationRequestsUtil();

    boolean test = util.estConsultationVecByDemographic(
        null,
        consultationRequest.getDemographicId().toString()
    );
    var reasonForConsultation = util.reasonForConsultation.get(0);

    Assertions.assertTrue(test);
    Assertions.assertEquals("reasonForReferralTest", reasonForConsultation);
  }
}
