package oscar.oscarEncounter.oscarConsultationRequest.pageUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.oscarehr.common.dao.EReferAttachmentDao;
import org.oscarehr.common.model.EReferAttachment;
import org.oscarehr.common.model.EReferAttachmentData;
import org.oscarehr.common.model.EReferAttachmentManagerData;
import org.oscarehr.common.model.OceanWorkflowTypeEnum;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EReferActionTest {

  @Mock
  HttpServletRequest request;
  @Mock
  HttpServletResponse response;
  @Mock
  ActionMapping mapping;
  @Mock
  ActionForm form;
  @Mock
  PrintWriter writer;
  @Mock
  EReferAttachmentDao eReferAttachmentDao;

  private AutoCloseable closeable;
  private EReferAction eReferAction;

  @BeforeEach
  public void setUp() throws IOException {
    closeable = MockitoAnnotations.openMocks(this);
    when(response.getWriter()).thenReturn(writer);
    eReferAction = new EReferAction(eReferAttachmentDao);
  }

  @AfterEach
  public void after() throws Exception {
    closeable.close();
  }

  @Test
  public void givenValidData_whenExecute_thenCorrectReponse() {
    when(request.getParameter("demographicNo")).thenReturn("1");
    when(request.getParameter("documents")).thenReturn("D1|D2|D3");
    when(request.getParameter("attachments")).thenReturn("");
    when(eReferAttachmentDao.saveEntity(any(EReferAttachment.class))).thenReturn(
        createTestEReferAttachment());

    eReferAction.execute(mapping, form, request, response);

    ArgumentCaptor<EReferAttachment> argumentCaptor = ArgumentCaptor.forClass(
        EReferAttachment.class);
    verify(eReferAttachmentDao, times(1)).saveEntity(argumentCaptor.capture());
    EReferAttachment actualEReferAttachment = argumentCaptor.getValue();
    Assertions.assertEquals(Integer.valueOf(1), actualEReferAttachment.getDemographicNo());
    Assertions.assertEquals(OceanWorkflowTypeEnum.EREFERRAL.name(),
        actualEReferAttachment.getType());
    Assertions.assertEquals(3, actualEReferAttachment.getAttachments().size());
  }

  @Test
  public void givenDocumentsAndEReferAttachment_whenHandleDocumentData_thenAttachmentsAreCorrect() {
    EReferAttachment eReferAttachment = new EReferAttachment(1);
    String documents = "D1|D2|D3";

    eReferAction.handleDocumentData(documents, eReferAttachment);

    List<EReferAttachmentData> attachments = eReferAttachment.getAttachments();
    Assertions.assertEquals(3, attachments.size());

    assertNull(eReferAttachment.getAttachmentManagerData());
  }

  @Test
  public void givenEReferAttachment_whenHandleAttachmentManagerData_thenCorrectDataHandled() {
    EReferAttachment eReferAttachment = new EReferAttachment(1);
    String attachments = "[{\"id\":1,\"type\":\"pdf\"},{\"id\":2,\"type\":\"jpg\"}]";

    eReferAction.handleAttachmentManagerData(attachments, eReferAttachment);

    List<EReferAttachmentManagerData> attachmentManagerData = eReferAttachment.getAttachmentManagerData();
    Assertions.assertEquals(2, attachmentManagerData.size());

    for (EReferAttachmentManagerData data : attachmentManagerData) {
      assertFalse(StringUtils.isEmpty(data.getPrintable()));
    }

    assertNull(eReferAttachment.getAttachments());
  }

  private EReferAttachment createTestEReferAttachment() {
    EReferAttachment eReferAttachment = new EReferAttachment();
    eReferAttachment.setId(1);
    eReferAttachment.setDemographicNo(1);
    eReferAttachment.setType("EREFERRAL");
    return eReferAttachment;
  }
}