--
-- create table for storing eOrders 
--
DROP TABLE IF EXISTS `DynacareEorder`;
CREATE TABLE `DynacareEorder`
(
    `id`           int(8) NOT NULL AUTO_INCREMENT,
    `eformDataId`  int(8) NOT NULL,
    `extOrderId`   varchar(255) DEFAULT NULL,
    `generatedPdf` mediumtext,
    `state`        varchar(32) NOT NULL,
    `barcode`      varchar(255) DEFAULT NULL,

    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY            `idx_eorder_eform_data_id` (`eformDataId`),
    KEY            `idx_eorder_ext_order_id` (`extOrderId`),
    KEY            `idx_eorder_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `DynacareCopyToProvider`;
CREATE TABLE `DynacareCopyToProvider`
(
    `id`              int(8) NOT NULL AUTO_INCREMENT,
    `salutation`      varchar(5),
    `lastName`        varchar(50) NOT NULL,
    `firstName`       varchar(40) NOT NULL,
    `middleName`      varchar(25),
    `address1`        varchar(100),
    `address2`        varchar(100),
    `city`            varchar(50),
    `stateOrProvince` varchar(20),
    `zipOrPostal`     varchar(20),
    `country`         varchar(30),
    `region`          varchar(5),
    `specialty`       varchar(4),
    `cpso`            varchar(10),
    `billingNumber`   varchar(15),

    PRIMARY KEY (`id`),
    UNIQUE KEY `id` (`id`),
    KEY               `idx_dynacare_cc_providers_last_name` (`lastName`),
    KEY               `idx_dynacare_eorder_cc_providers_first_name` (`firstName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
