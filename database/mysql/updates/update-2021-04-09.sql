CREATE TABLE Subscription
(
    id       INT AUTO_INCREMENT
        PRIMARY KEY,
    module   VARCHAR(32) NULL,
    moduleId VARCHAR(32) NULL
);

CREATE TABLE SubscriptionRecord
(
    id        INT AUTO_INCREMENT
        PRIMARY KEY,
    module    VARCHAR(32)   NULL,
    pending   INT DEFAULT 0 NOT NULL,
    sent      INT DEFAULT 0 NOT NULL,
    discarded INT DEFAULT 0 NOT NULL,
    CONSTRAINT subscriptionrecords_module_uindex
        UNIQUE (module)
);

INSERT IGNORE INTO SubscriptionRecord(`module`)
    VALUE ('Demographic'), ('Appointment');

INSERT IGNORE INTO SystemPreferences(`name`, `value`, `updateDate`)
    VALUES ('insig.integration.subscriptions.flushed','false', NOW()),
           ('insig.integration.subscriptions.capacity','1000', NOW()),
           ('insig.integration.subscriptions.pg_size','100', NOW());
