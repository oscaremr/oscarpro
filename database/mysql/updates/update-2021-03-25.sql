CREATE TABLE `PeriodDef`  (
  `name` varchar(100) NOT NULL,
  `expression` varchar(500) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `description` varchar(500) NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
);

CREATE TABLE `PeriodTaskLog` (
  `id` int(10) NOT NULL,
  `jobName` varchar(255) NOT NULL,
  `result` varchar(500) DEFAULT NULL,
  `executeTime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
);

INSERT INTO `secobjectname` VALUES ('_admin.clear_teleplan_alert', 'Clear all alerts of teleplan automated task', 0);

CREATE TABLE `TeleplanAlert`  (
  `id` int(10) NOT NULL,
  `jobName` varchar(255) NOT NULL,
  `result` varchar(500) NULL DEFAULT NULL,
  `msg` varchar(500) NULL DEFAULT NULL,
  `success` tinyint(1) NULL DEFAULT NULL,
  `createTime` datetime NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  `deleteTime` datetime NULL DEFAULT NULL,
  `deleteProviderNo` varchar(12) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
);