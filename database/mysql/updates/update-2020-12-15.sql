CREATE TABLE `eformdocs` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `document_no` VARCHAR(127) NOT NULL,
  `requestId` INT(10) NOT NULL,
  `doctype` CHAR(1) NOT NULL,
  `deleted` CHAR(1) ,
  `attach_date` DATE ,
  `provider_no` VARCHAR(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_eformdocs_documentNo` (`document_no`),
  KEY `idx_eformdocs_requestId` (`requestId`),
  KEY `idx_eformdocs_doctype` (`doctype`)
);
