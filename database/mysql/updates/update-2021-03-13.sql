CREATE TABLE IF NOT EXISTS MdRequiredCodes (
  code varchar(10) NOT NULL,
  id int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (id),
  UNIQUE KEY code_UNIQUE (code)
);

INSERT IGNORE INTO MdRequiredCodes (code) values ("A205A");
INSERT IGNORE INTO MdRequiredCodes (code) values ("A206A");
INSERT IGNORE INTO MdRequiredCodes (code) values ("A935A");

ALTER TABLE billingservice
ADD COLUMN refPhysicianFlag tinyint(1) NOT NULL DEFAULT 0;