ALTER TABLE demographic 
	ADD COLUMN `genderId` int(11) DEFAULT NULL, 
	ADD COLUMN `pronounId` int(11) DEFAULT NULL;

CREATE TABLE `DemographicGender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL,
  `editable` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
);

/*Data for the table `DemographicGender` */

insert  into `DemographicGender`(`id`,`value`,`editable`) values 
(1,'Male',0),
(2,'Female',0),
(3,'Transgender',0),
(4,'Transgender Male',0),
(5,'Transgender Female',0),
(6,'Non-Binary',0),
(7,'Two-Spirit',0),
(8,'Gender Fluid',0);


/*Table structure for table `demographic_pronoun` */

CREATE TABLE `DemographicPronoun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) NOT NULL,
  `editable` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
);

/*Data for the table `DemographicPronoun` */

insert  into `DemographicPronoun`(`id`,`value`,`editable`) values 
(1,'She/Her',0),
(2,'He/Him',0),
(3,'They/Them',0);