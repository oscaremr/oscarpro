CREATE TABLE IF NOT EXISTS trigger_log
(
    id                   int auto_increment
        primary key,
    trigger_id           int         null,
    trigger_action       varchar(60) null,
    trigger_action_value varchar(60) null,
    trigger_type         varchar(60) null,
    content_id           varchar(60) null,
    time_stamp           datetime    null
);

CREATE TABLE IF NOT EXISTS trigger_list_item
(
    id         int auto_increment
        primary key,
    list_id    int         null,
    item_value varchar(60) null
);

CREATE TABLE IF NOT EXISTS trigger_list
(
    id       int auto_increment
        primary key,
    name     varchar(60) null,
    archived tinyint(1)  null
);

CREATE TABLE IF NOT EXISTS trigger_data
(
    id                int auto_increment
        primary key,
    type              varchar(60)            null,
    name              varchar(60)            null,
    trigger_status    varchar(1) default 'A' not null,
    last_updated_date datetime               null,
    last_updated_by   varchar(11)            null,
    internal_id       varchar(255)           null
);

CREATE TABLE IF NOT EXISTS trigger_condition
(
    id             int auto_increment
        primary key,
    key_value      varchar(60) null,
    comparator     varchar(60) null,
    value          varchar(60) null,
    trigger_id     int         null,
    order_position int         null
);

CREATE TABLE IF NOT EXISTS trigger_action
(
    id             int auto_increment
        primary key,
    key_value      varchar(60) null,
    value          varchar(60) null,
    trigger_id     int         null,
    order_position int         null
);