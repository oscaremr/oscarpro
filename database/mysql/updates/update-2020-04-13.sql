CREATE TABLE `teleplan_eligibility_check` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `demographic_no` int(10) NOT NULL,
  `eligible` tinyint(1) NOT NULL,
  `date_checked` datetime NOT NULL,
  `checked_by` varchar(6) NOT NULL,
  PRIMARY KEY (`id`)
);
ALTER TABLE ctl_billingtype ADD sli_code varchar(4) DEFAULT '';