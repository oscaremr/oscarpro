CREATE TABLE IF NOT EXISTS demographicSite (
  id int(10) NOT NULL AUTO_INCREMENT,
  siteId int(10) NOT NULL,
  demographicId int(10) NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE demographicSite RENAME TO DemographicSite;
