alter table billing_on_favourite add column sli_code varchar(4) default '';

alter table Department add column phone varchar(25) default '' after name;
alter table Department add column fax varchar(25) default '' after phone;