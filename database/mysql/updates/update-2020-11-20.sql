INSERT IGNORE INTO `secObjectName` (`objectName`, `description`, `orgapplicable`)
VALUES
	('_newCasemgmt.reports', NULL, 0);

INSERT IGNORE INTO `secObjPrivilege` (`roleUserGroup`, `objectName`, `privilege`, `priority`, `provider_no`)
VALUES
	('doctor', '_newCasemgmt.reports', 'x', 0, '999998');