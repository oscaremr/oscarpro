alter table `billingmaster` add column gst decimal(7,2) default '0.00' not null;
alter table `billingmaster` add column gst_no varchar(12) null;