/* This is for THT clients who already have free_draw_data table
*/

CREATE TABLE IF NOT EXISTS `free_draw_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `demographic_no` int(10) NOT NULL,
  `appointment_no` int(12) NOT NULL,
  `name` varchar(255),
  `draw_data` mediumtext,
  `hand_write_notes` mediumtext,
  `thumbnailId` varchar(100) not null,
  `type` varchar(50) not null,
  `status` tinyint(1) NOT NULL,
  `provider_no` varchar(6) NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE free_draw_data CHANGE demographic_no demographicNo int(10) NOT NULL;
ALTER TABLE free_draw_data CHANGE appointment_no appointmentNo int(10) NOT NULL;
ALTER TABLE free_draw_data CHANGE draw_data drawData mediumtext;
ALTER TABLE free_draw_data CHANGE hand_write_notes handWriteNotes mediumtext;
ALTER TABLE free_draw_data CHANGE `type` dataType varchar(50) NOT NULL;
ALTER TABLE free_draw_data CHANGE provider_no providerNo varchar(50) NOT NULL;
ALTER TABLE free_draw_data CHANGE update_time updateTime datetime NOT NULL;

ALTER TABLE free_draw_data RENAME TO FreeDrawData;


/* This is for non-THT clients
*/

/*
CREATE TABLE FreeDrawData (
  id int(10) NOT NULL AUTO_INCREMENT,
  demographicNo int(10) NOT NULL,
  appointmentNo int(10) DEFAULT NULL,
  name varchar(255) DEFAULT NULL,
  drawData mediumtext,
  handWriteNotes mediumtext,
  thumbnailId varchar(100) NOT NULL,
  dataType varchar(50) NOT NULL,
  status tinyint(1) NOT NULL,
  providerNo varchar(6) NOT NULL,
  updateTime datetime NOT NULL,
  PRIMARY KEY (id)
);
*/
