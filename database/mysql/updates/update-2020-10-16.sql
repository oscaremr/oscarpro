CREATE TABLE IF NOT EXISTS `formBCAR2020` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `demographic_no` int(10) NOT NULL,
  `provider_no` varchar(6) NOT NULL,
  `formCreated` date DEFAULT NULL,
  `formEdited` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`id`)
);

CREATE TABLE IF NOT EXISTS `formBCAR2020Data` (
  `form_id` int(10) NOT NULL,
  `provider_no` varchar(6) NOT NULL,
  `page_no` int(1) NOT NULL,
  `field` varchar(255) NOT NULL,
  `val` varchar(255) NOT NULL DEFAULT '',
  `field_edited` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `form_data` (`form_id`,`page_no`,`field`)
);

INSERT INTO `encounterForm` (`form_name`, `form_value`, `form_table`, `hidden`)
VALUES
	('BC-AR 2020', '../form/formBCAR2020pg1.jsp?demographic_no=', 'formBCAR2020', 0);

CREATE TABLE `billing_auto_rules` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rule_name` varchar(80) ,
  `service_code` tinyint(1) NOT NULL COMMENT '0 = no, 1 = yes',
  `dx` tinyint(1) NOT NULL COMMENT '0 = no, 1 = yes',
  `referral_doctor_type` varchar(7)  COMMENT 'mrp or manual',
  `referral_doctor` varchar(6) ,
  `billing_doctor_type` varchar(7)  COMMENT 'mrp or manual',
  `billing_doctor` varchar(6) ,
  `visit_type` varchar(5) ,
  `visit_location` varchar(5) ,
  `sli_code` varchar(5) ,
  `created` datetime NOT NULL,
  `creator` varchar(6)  COMMENT 'provider_no',
  `modified` datetime NOT NULL,
  `last_modifier` varchar(6)    COMMENT 'provider_no',
  `status` tinyint(1) NOT NULL COMMENT '0 = inactive, 1 = active, 2 = init, 3 = pending, 4 = locked',
  PRIMARY KEY (`id`)
);


CREATE TABLE `billing_auto_rules_ext` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ext_key` varchar(50) ,
  `ext_value` varchar(255) ,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `rule_id` bigint(20) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '0 = inactive, 1 = active, 2 = init, 3 = pending, 4 = locked',
  PRIMARY KEY (`id`)
);


CREATE TABLE `billing_auto_rules_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appointment_type_id` bigint(20) NOT NULL,
  `rule_id` bigint(20) NOT NULL,
  `sequence` int(1) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `billing_auto_rules_map` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `appointment_type_id` varchar(20) NOT NULL,
  `rule_id` varchar(20)  NOT NULL,
  `sequence` int(1) NOT NULL,
  PRIMARY KEY (`id`)
);

alter table batch_billing add serNum varchar(3) not null default '1';
update batch_billing set serNum = 1 where serNum is null;

alter table batch_billing add admissionDate date default null;
alter table batch_billing add visitLocation varchar(4) default null;
update batch_billing set visitLocation='0000' where visitLocation is null;
