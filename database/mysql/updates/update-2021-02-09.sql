CREATE TABLE IF NOT EXISTS `formBCAR2020Text` (
  `form_id` int(10) NOT NULL,
  `provider_no` varchar(6) NOT NULL,
  `page_no` int(1) NOT NULL,
  `field` varchar(255) NOT NULL,
  `val` text NOT NULL,
  `field_edited` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `form_data` (`form_id`,`page_no`,`field`)
);