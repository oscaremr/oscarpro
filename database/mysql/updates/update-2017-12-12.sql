ALTER TABLE HRMDocumentToDemographic MODIFY COLUMN demographicNo int(10);
ALTER TABLE HRMDocumentToDemographic MODIFY COLUMN hrmDocumentId int(11);
ALTER TABLE HRMDocumentToProvider MODIFY COLUMN hrmDocumentId int(11);