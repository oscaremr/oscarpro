create table if not exists invoice_history(
  id                int(12) auto_increment primary key,
  ch1_id            int(12) not null,
  item_id           int(12) default 0 not null,
  field             varchar(20) not null,
  val               varchar(20),
  old_value         varchar(20),
  action            varchar(20),
  provider_no  varchar(6) not null,
  updated_timestamp timestamp default CURRENT_TIMESTAMP not null,
  disk_id           int(11) DEFAULT null
);
create index ch1_id on invoice_history (ch1_id);

ALTER TABLE billing_on_cheader1 ADD create_date_time datetime DEFAULT NULL AFTER creator;
UPDATE billing_on_cheader1 ch set ch.create_date_time = (SELECT log.dateTime FROM log where log.action='add' and log.content='bill' and log.contentId = CONCAT('billingId=', ch.id));