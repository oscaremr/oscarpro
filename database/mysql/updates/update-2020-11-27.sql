/**
 * OCA Admin - add manage email permissions
 */
 
INSERT INTO `secObjectName` (`objectName`, `description`, `orgapplicable`) VALUES ('_admin.email', 'Configure & Manage Emails', '0');

INSERT INTO `secObjPrivilege` VALUES('admin','_admin.email','x',0,'999998');
INSERT INTO `secObjPrivilege` VALUES('Clinic Admin','_admin.email','x',0,'999998');
  
  
CREATE TABLE `smtp_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255),
  `username` varchar(255),
  `password` varchar(255),
  `port` smallint,
  `useSecurity` tinyint(1),
  `replyToEmail` varchar(255),
  PRIMARY KEY (`id`)
);

ALTER TABLE smtp_config ADD COLUMN displayName VARCHAR(255) AFTER replyToEmail;

CREATE TABLE `email_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sentDate` DATETIME,
  `content` varchar(255),
  `emailType` varchar(255),
  `recipientId` int(11) NOT NULL,
  `status` varchar(255),
  PRIMARY KEY (`id`)
);

  
CREATE TABLE `email_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(255),
  `fileContent` blob NOT NULL,
  `emailLogId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

alter table email_log modify content mediumtext;
