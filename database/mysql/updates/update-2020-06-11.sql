CREATE TABLE `examTemplate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examTemplateName` varchar(128) NOT NULL,
  `lastUpdateProviderNo` varchar(16) NOT NULL,
  `lastUpdateDate` timestamp NOT NULL,
  `status` varchar(1),
  `examTemplateProviderNo` VARCHAR(6),
  PRIMARY KEY (`id`)
);

CREATE TABLE `examTemplateItem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examTemplateId` int(11) NOT NULL,
  `type` varchar(32) NOT NULL,
  `dataField` mediumtext NOT NULL,
  `dateObserved` timestamp NOT NULL,
  `dateEntered` timestamp NOT NULL,
  PRIMARY KEY (`id`)
);

ALTER TABLE `EyeformMacro`
   CHANGE `followupNo` `followupNo` VARCHAR(6) NULL;
ALTER TABLE `EyeformMacro`
  ADD COLUMN `followupComment` TEXT NULL AFTER `followupReason`;
ALTER TABLE `EyeformMacro`
  ADD COLUMN `siteId` INT(11) NULL AFTER `includeAdmissionDate`;
ALTER TABLE `EyeformMacro`
  ADD COLUMN `billingPhysician` VARCHAR(20) NULL AFTER `siteId`;
ALTER TABLE `EyeformMacro`
  ADD COLUMN `macroProviderNo` VARCHAR(6) NULL AFTER `billingPhysician`;
ALTER TABLE `EyeformMacro`
  ADD COLUMN `closeEncounter` TINYINT(1) NULL AFTER `macroProviderNo`;
ALTER TABLE `EyeformMacro`
  ADD COLUMN `procedureEye` VARCHAR(6) NULL AFTER `closeEncounter`;
ALTER TABLE `EyeformMacro`
  ADD COLUMN `procedureName` VARCHAR(50) NULL AFTER `procedureEye`;
ALTER TABLE `EyeformMacro`
  ADD COLUMN `procedureDoctor` VARCHAR(50) NULL AFTER `procedureName`;

ALTER TABLE `EyeformMacro`
  ADD COLUMN `procedureLocation` VARCHAR(50) NULL AFTER `procedureDoctor`;
ALTER TABLE `EyeformMacro`
  ADD COLUMN `procedureNote` VARCHAR(255) NULL AFTER `procedureLocation`;  
ALTER TABLE `EyeformMacro`
  ADD COLUMN `bookProcedureEye` VARCHAR(10) NULL AFTER `procedureNote`;
ALTER TABLE `EyeformMacro`
  ADD COLUMN `bookProcedureProc` VARCHAR(30) NULL AFTER `bookProcedureEye`;
ALTER TABLE `EyeformMacro`
  ADD COLUMN `bookProcedureLoc` VARCHAR(50) NULL AFTER `bookProcedureProc`;
ALTER TABLE `EyeformMacro`
  ADD COLUMN `bookProcedureUrgency` VARCHAR(10) NULL AFTER `bookProcedureLoc`;
ALTER TABLE `EyeformMacro`
  ADD COLUMN `bookProcedureComment` TEXT NULL AFTER `bookProcedureUrgency`;

ALTER TABLE `EyeformSpecsHistory`   
  ADD COLUMN `note` VARCHAR(255) NULL AFTER `appointmentNo`;

-- ALTER TABLE `EyeformConsultationReport`   
--   ADD COLUMN `siteId` INT(11) NULL AFTER `patientWillBook`;
ALTER TABLE `EyeformConsultationReport`   
  ADD COLUMN `contactId` VARCHAR(100) NULL AFTER `siteId`;
ALTER TABLE `EyeformConsultationReport`   
  ADD COLUMN `contact_type` INT(10) NULL AFTER `contactId`;
ALTER TABLE `EyeformConsultationReport`   
  ADD COLUMN `otherReferralId` INT(11) NULL AFTER `contact_type`;

CREATE TABLE `EyeformOcularProcedureNameList` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `procedureName` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`)
) ;

CREATE TABLE `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_name` varchar(100) NOT NULL,
  `time` timestamp NOT NULL,
  `data` text NOT NULL,
  `demo_id` int(11),
  `status` int(1),
  PRIMARY KEY (`id`)
);

INSERT into issue (code,description,role,update_date,priority,type,sortOrderId) values ('GlaucomaRiskFactors','Glaucoma Risk Factors','nurse',now(),NULL,'system',0);
INSERT INTO issue (CODE,description,role,update_date,priority,TYPE,sortOrderId) VALUES ('Misc','Misc','nurse',NOW(),NULL,'system',0);

INSERT INTO EyeformOcularProcedureNameList VALUES(1, ' ');
INSERT INTO EyeformOcularProcedureNameList VALUES(2, 'CE+IOL');
INSERT INTO EyeformOcularProcedureNameList VALUES(3, 'RELACS');
INSERT INTO EyeformOcularProcedureNameList VALUES(4, 'YAG PI');
INSERT INTO EyeformOcularProcedureNameList VALUES(5, 'YAG cap');
INSERT INTO EyeformOcularProcedureNameList VALUES(6, 'SLT');
INSERT INTO EyeformOcularProcedureNameList VALUES(7, 'laser retinopexy');
INSERT INTO EyeformOcularProcedureNameList VALUES(8, 'Lucentis');
INSERT INTO EyeformOcularProcedureNameList VALUES(9, 'Eylea');

update EyeformConsultationReport set otherReferralId=0 where otherReferralId is null;
update EyeformConsultationReport set contact_type=0 where contact_type is null;


CREATE TABLE `zeiss_oru_result` (
  `id` INT(12) NOT NULL AUTO_INCREMENT,
  `pid` INT(10) NOT NULL,
  `placer_order_number` VARCHAR(20) NOT NULL,
  `instrument` TEXT,
  `study_date` DATETIME DEFAULT NULL,
  `content_date` DATETIME DEFAULT NULL,
  `doc_title` TEXT,
  `img_type` TEXT,
  `laterality` VARCHAR(10) DEFAULT NULL,
  `study_uid` TEXT,
  `instance_uid` TEXT,
  `file_type` VARCHAR(50) NOT NULL,
  `file_path` TEXT,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`),
  KEY `placer_order_number` (`placer_order_number`)
) ENGINE=INNODB DEFAULT CHARSET=latin1
