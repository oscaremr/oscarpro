CREATE TABLE `MonerisUpload` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fileText` MEDIUMTEXT NULL,
  `fileName` VARCHAR(200) NULL,
  `uploadDate` DATETIME NULL,
  PRIMARY KEY (`id`));