
CREATE TABLE `DHIRTransactionLog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `initiatingProviderNo` varchar(25) DEFAULT NULL,
  `transactionType` varchar(25) DEFAULT NULL,
  `externalSystem` varchar(50) DEFAULT NULL,
  `demographicNo` int(10) DEFAULT NULL,
  `resultCode` int(10) DEFAULT NULL,
  `success` tinyint(1) DEFAULT NULL,
  `error` mediumtext,
  `headers` mediumtext,
  PRIMARY KEY (`id`)
);


 CREATE TABLE `OMDGatewayTransactionLog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `initiatingProviderNo` varchar(25) DEFAULT NULL,
  `transactionType` varchar(50) DEFAULT NULL,
  `externalSystem` varchar(50) DEFAULT NULL,
  `demographicNo` int(10) DEFAULT NULL,
  `resultCode` int(10) DEFAULT NULL,
  `success` tinyint(1) DEFAULT NULL,
  `error` longtext,
  `headers` longtext,
  `uao` varchar(50) DEFAULT NULL,
  `oscarSessionId` varchar(50) DEFAULT NULL,
  `contextSessionId` varchar(50) DEFAULT NULL,
  `secondsLeft` int(10) DEFAULT NULL,
  `dataSent` longtext,
  `uniqueSessionId` varchar(50) DEFAULT NULL,
  `dataRecieved` longtext,
  `ended` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `xRequestId` varchar(50) DEFAULT NULL,
  `xLobTxId` varchar(50) DEFAULT NULL,
  `xCorrelationId` varchar(50) DEFAULT NULL,
  `xGtwyClientId` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `OMDGatewayTransactionLog_uniqueSessionId_idx` (`uniqueSessionId`(40))
);


CREATE TABLE `CVCImmunization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `versionId` int(11) DEFAULT NULL,
  `snomedConceptId` varchar(255) DEFAULT NULL,
  `displayName` varchar(255) DEFAULT NULL,
  `picklistName` varchar(255) DEFAULT NULL,
  `generic` tinyint(1) DEFAULT NULL,
  `prevalence` int(11) DEFAULT NULL,
  `parentConceptId` varchar(255) DEFAULT NULL,
  `ispa` tinyint(1) DEFAULT NULL,
  `typicalDose` varchar(255) DEFAULT NULL,
  `typicalDoseUofM` varchar(255) DEFAULT NULL,
  `strength` varchar(255) DEFAULT NULL,
  `shelfStatus` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET utf8;

CREATE TABLE `CVCImmunizationName` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(30) DEFAULT NULL,
  `useSystem` varchar(255) DEFAULT NULL,
  `useCode` varchar(255) DEFAULT NULL,
  `useDisplay` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `cvcImmunizationId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET utf8;

CREATE TABLE `CVCMapping` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `oscarName` varchar(255) DEFAULT NULL,
  `cvcSnomedId` varchar(255) DEFAULT NULL,
  `preferCVC` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET utf8;

CREATE TABLE `CVCMedication` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `versionId` int(11) DEFAULT NULL,
  `din` int(11) DEFAULT NULL,
  `dinDisplayName` varchar(255) DEFAULT NULL,
  `snomedCode` varchar(255) DEFAULT NULL,
  `snomedDisplay` varchar(255) DEFAULT NULL,
  `status` varchar(40) DEFAULT NULL,
  `isBrand` tinyint(1) DEFAULT NULL,
  `manufacturerId` int(11) DEFAULT NULL,
  `manufacturerDisplay` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET utf8;

CREATE TABLE `CVCMedicationGTIN` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cvcMedicationId` int(11) NOT NULL,
  `gtin` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET utf8;


CREATE TABLE `CVCMedicationLotNumber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cvcMedicationId` int(11) NOT NULL,
  `lotNumber` varchar(255) NOT NULL,
  `expiryDate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) CHARACTER SET utf8;


CREATE TABLE `UAO` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `started` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `providerNo` varchar(25) DEFAULT NULL,
  `friendlyName` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `demographicNo` int(10) DEFAULT NULL,
  `resultCode` int(10) DEFAULT NULL,
  `defaultUAO` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `addedBy` varchar(25) DEFAULT NULL,
  `dateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
);

insert into secObjectName values ('_prevention.updateCVC',NULL,0);
insert into secObjPrivilege values('admin','_prevention.updateCVC','x',0,'999998');


alter table preventions add column snomedId varchar(255);

alter table preventions modify prevention_type varchar(255);




DELETE FROM LookupListItem where value='CNO' AND lookupListId = (select id from LookupList where name = 'practitionerNoType');
INSERT INTO `LookupListItem` VALUES (\N,(select id from LookupList where name = 'practitionerNoType'),'OCP','Ontario College of Pharmacists (OCP)',3,1,'oscar',now());
INSERT INTO `LookupListItem` VALUES (\N,(select id from LookupList where name = 'practitionerNoType'),'CNORNP','RNP - College of Nurses of Ontario (CNO)',3,1,'oscar',now());
INSERT INTO `LookupListItem` VALUES (\N,(select id from LookupList where name = 'practitionerNoType'),'CNORN','RN - College of Nurses of Ontario  (CNO)',3,1,'oscar',now());
INSERT INTO `LookupListItem` VALUES (\N,(select id from LookupList where name = 'practitionerNoType'),'CNORPN','RPN - College of Nurses of Ontario  (CNO)',3,1,'oscar',now());
INSERT INTO `LookupListItem` VALUES (\N,(select id from LookupList where name = 'practitionerNoType'),'CMO','College of Midwives of Ontario',3,1,'oscar',now());

INSERT INTO `OscarJobType` VALUES (\N,'CanadianVaccineCatalogueUpdater','Updates the local copy of the data','org.oscarehr.integration.born.CanadianVaccineCatalogueJob',1,now()),(\N,'BORN FHIR','','org.oscarehr.integration.born.BORNFhirJob',1,now());
INSERT INTO `OscarJob` VALUES (\N,'CanadianVaccineCatalogueUpdater','Updates the CVC data',(select id from OscarJobType where name='CanadianVaccineCatalogueUpdater'),'0 * 0 * * *','999998',0,now()),(\N,'BORN FHIR','',(select id from OscarJobType where name='BORN FHIR'),'0 * * * * *','999998',0,now());



CREATE TABLE `DHIRSubmissionLog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `demographicNo` int(11) DEFAULT NULL,
  `preventionId` int(11) DEFAULT NULL,
  `submitterProviderNo` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `transactionId` varchar(100) DEFAULT NULL,
  `bundleId` varchar(255) DEFAULT NULL,
  `response` mediumtext,
  `contentLocation` varchar(255) DEFAULT NULL,
  `clientRequestId` varchar(100) DEFAULT NULL,
  `clientResponseId` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
);